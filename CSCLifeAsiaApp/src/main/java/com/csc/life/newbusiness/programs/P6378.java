/*
 * File: P6378.java
 * Date: 30 August 2009 0:45:38
 * Author: Quipoz Limited
 *
 * Class transformed from P6378.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */

package com.csc.life.newbusiness.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTallyAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
//ILIFE-4168 Code and Model Promotion for VPMS externalization of LIFE Extended Validation Calculation for RUL Product end
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.dip.jvpms.runtime.base.IVpmsBaseSession;
import com.csc.dip.jvpms.runtime.base.IVpmsBaseSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsComputeResult;
import com.csc.dip.jvpms.runtime.base.VpmsJniSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsLoadFailedException;
import com.csc.dip.jvpms.runtime.base.VpmsTcpSessionFactory;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.CcdtTableDAM; //MIBT-365
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO;
import com.csc.fsu.accounting.dataaccess.model.Acblpf;
import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.agents.dataaccess.dao.AgntpfDAO;
import com.csc.fsu.agents.dataaccess.dao.AgqfpfDAO;
import com.csc.fsu.agents.dataaccess.model.Agntpf;
import com.csc.fsu.agents.dataaccess.model.Agqfpf;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.financials.dataaccess.dao.TtrcpfDAO;
import com.csc.fsu.financials.dataaccess.dao.model.Ttrcpf;
import com.csc.fsu.general.dataaccess.GrpsTableDAM;
import com.csc.fsu.general.dataaccess.MandlnbTableDAM;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.general.tablestructures.T3678rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.dao.AgslpfDAO;
import com.csc.life.agents.dataaccess.model.Agslpf;
import com.csc.life.agents.tablestructures.Tjl02rec;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.procedures.UwQuestionnaireUtil;
import com.csc.life.contractservicing.procedures.UwoccupationUtil;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.AcblenqTableDAM;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.interestbearing.dataaccess.HitrrnlTableDAM;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.AsgnpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.BnfypfDAO;
import com.csc.life.newbusiness.dataaccess.dao.BnkoutpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.CtrspfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.PcddpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.RlvrpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.UnltpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Asgnpf;
import com.csc.life.newbusiness.dataaccess.model.Bnfypf;
import com.csc.life.newbusiness.dataaccess.model.Bnkoutpf;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Ctrspf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.newbusiness.dataaccess.model.Rlvrpf;
import com.csc.life.newbusiness.dataaccess.model.Unltpf;
import com.csc.life.newbusiness.procedures.Hcrtfup;
import com.csc.life.newbusiness.procedures.LincCommonsUtil;
import com.csc.life.newbusiness.recordstructures.Hcrtfuprec;
import com.csc.life.newbusiness.screens.S6378ScreenVars;
import com.csc.life.newbusiness.tablestructures.NBProposalRec;
import com.csc.life.newbusiness.tablestructures.T3615rec;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.newbusiness.tablestructures.Tr52zrec;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MrtapfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Rlpdlon;
import com.csc.life.productdefinition.procedures.Vlpdrule;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;
import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;
import com.csc.life.productdefinition.recordstructures.Vlpdrulrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Td5g0rec;
import com.csc.life.productdefinition.tablestructures.Th611rec;
import com.csc.life.productdefinition.tablestructures.Th615rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.reassurance.procedures.Csncalc;
import com.csc.life.reassurance.recordstructures.Csncalcrec;
import com.csc.life.regularprocessing.dataaccess.UtrnrnlTableDAM;
import com.csc.life.terminationclaims.dataaccess.HbnfTableDAM;
import com.csc.life.underwriting.dataaccess.UndlTableDAM;
import com.csc.life.underwriting.dataaccess.dao.UndcpfDAO;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undcpf;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.life.underwriting.procedures.Jundwfup;
import com.csc.life.underwriting.recordstructures.Jundwfuprec;
import com.csc.life.underwriting.recordstructures.Undwsubrec;
import com.csc.life.underwriting.tablestructures.T6771rec;
import com.csc.life.underwriting.tablestructures.Tr675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Errmesg;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.utils.ListUtils;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.life.underwriting.dataaccess.dao.UndlpfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.life.underwriting.dataaccess.model.Undlpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.CreditCardUtility;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
 *REMARKS.
 *
 *              PRE-ISSUE VALIDATION
 *
 * Initialise
 * ----------
 *
 * PREPARATION
 *
 *   On the first  time  into  the  program,  read  the  financial
 *   accounting  rules   for   the  transaction  (T5645,  item  is
 *   transaction number passed in WSSP). Also read the sub-account
 *   type record for the  FIRST  posting (posting no. 01 on T5645)
 *   to get the sign (T3695).
 *
 * AMOUNTS ETC ON SCREEN
 *
 *   Retrieve the contract header (CHDRLNB) for the contract being
 *   worked on. Using these details, look up:
 *
 *        - the contract type description from T5688,
 *
 *        - the contract owner  (CLTS)  and  format  the  name for
 *             confirmation.
 *
 *   Calculate the amounts to be shown on the screen as follows:
 *
 *        Contract Suspense  -  read  the  sub-account balance for
 *             posting 01 (as  defined  on  T5645) for the current
 *             contract. Apply  the  sign for the sub-account type
 *             (i.e. if the  sign  is -ve, negate the balance read
 *             when displaying it on the screen).
 *
 *        Contract Fee  -  read  the  contract  definition details
 *             (T5688) for the  contract type held on the contract
 *             header. If the  contract  fee  method is not blank,
 *             look up the  subroutine  used  to calculate it from
 *             T5674 and call  it  with the required linkage area.
 *             This  subroutine   will  return  the  contract  fee
 *             amount. If there  is  no  contract fee method, this
 *             amount is zero. If not zero, adjust the fee by  the
 *             the number of frequencies required prior to  issue.
 *             To   do  this,  call  DATCON3  with   the  contract
 *             commencement  date,  billing commencement date  and
 *             billing  frequency  from the  contract header. This
 *             will  return a factor.  Multiply the fee amount  by
 *             this factor to give the fee to be displayed on  the
 *             screen.
 *
 *        Premium  -  read through the coverage/rider transactions
 *             (COVTLNB).  For  each  record  read, accumulate all
 *             single  premiums  payable  and all regular premiums
 *             payable.  A  premium  is a single premium amount if
 *             the  billing  frequency  for  the whole contract is
 *             '00',  or the coverage/rider definition (T5687) has
 *             a single premium indicator of 'Y'.  Otherwise it is
 *             regular  premium.  Once  all the premiums have been
 *             accumulated,  adjust  the  regular premiums (if not
 *             zero)  by  the number of frequencies required prior
 *             to  issue.  To  do  this,  call  DATCON3  with  the
 *             contract  commencement  date,  billing commencement
 *             date   and  billing  frequency  from  the  contract
 *             header.  This  will  return a factor.  Multiply the
 *             regular  premium  amount  by this factor and add it
 *             to    the   single   premium  amount  to  give  the
 *             "instalment"    premium  to  be  displayed  on  the
 *             screen.
 *
 *   Cross check the contract details  for consistency errors. For
 *   each error detected, call the error messages routine (ERRMSG)
 *   and load the message returned,  along  with  the  appropriate
 *   "key" details in the subfile. Load all pages of the subfile.
 *
 * CONTRACT LEVEL VALIDATION
 *
 *   Check  that  the  premium amount plus the contract fee amount
 *   displayed (the "required premium") are less  than or equal to
 *   the contract suspense amount paid  (NB  check  screen amounts
 *   not sub-account amounts).  If there  is insufficient contract
 *   suspense, check that  it  is  within  the  tolerance for this
 *   transaction.
 *
 *        - look up the tolerance applicable  (T5667,  item  key =
 *             transaction code/contract currency).
 *
 *        - apply  the  tolerance  percentage   for  the  contract
 *             billing frequency to the  required  premium  amount
 *             calculated.  If  this is less  than  the  tolerance
 *             amount limit, use this. Otherwise use the tolerance
 *             amount.
 *
 *        - check that the difference between the required premium
 *             and the suspense amount is less  than  or  equal to
 *             the tolerance amount.
 *
 *   Read  through  all  the  follow-ups set  up  for the contract
 *   (FLUP).  For  each  record  read,  check to see that none are
 *   still outstanding (status matching on T5661). - NOTE there is
 *   code to do this in P5004, 1200- SECTION.
 *
 *   The contract definition table (T5688 read above) defines the
 *   minimum  number of lives which must be on the contract.  Read
 *   through  the  lives  attached  to  the contract (LIFELNB) and
 *   count them.  Only count life records  which have a joint life
 *   number of '00' (the others are joint lives and do not count).
 *   Cross check the number read with the minimum required.
 *
 * LIFE LEVEL VALIDATION
 *
 *   For each life and joint life  read,  also cross validate that
 *   critical details on the contract  header  and the life record
 *   match.  If  they  do  not,   the   life/joint  life  must  be
 *   "re-visited".  These are:
 *
 *        1) Age next birthday at risk commencement. Take the date
 *             of  birth  from  the  life   record  and  the  risk
 *             commencement date from  the  contract  header. Feed
 *             these with a frequency of  1  to DATCON3. This will
 *             return a factor. Round this up to the nearest whole
 *             number.  This must be the  same  as  the  age  next
 *             birthday as held on the life record.
 *
 * COMPONENT LEVEL VALIDATION
 *
 *   Read  each  coverage/rider   transaction   record  (COVTLNB).
 *   Each  coverage/rider  must be checked  to  see  if  it  needs
 *   re-visiting. If it does for any  of the reasons listed below,
 *   skip the other checks and only output one message.
 *
 *   For the first coverage on a life,  look up the life and joint
 *   life (if it exists) to which it applies.
 *
 *        1) Cross check that the following  on the life record(s)
 *             are the same as on the component transaction:
 *             - age next birthday
 *             - sex
 *
 *        2) Cross check that the following on the contract header
 *             and coverage transaction are the same:
 *             - payment method
 *             - payment frequency
 *             - risk commencement date
 *
 *   If  there are any errors (and  hence  records  added  to  the
 *   subfile making SCRN-SUBFILE-RRN not = zero), this contract is
 *   not yet ready for issue.
 *
 * Validation
 * ----------
 *
 *   No actual screen validation is required.
 *
 * Updating
 * --------
 *
 *   If  there  were  no validation errors displayed on the screen
 *   (1000  section), set the available for issue  flag to 'Y' and
 *   KEEPS the contract header.
 *
 * Next
 * ----
 *
 *   Add one to the program pointer and exit.
 *
 *
 ******************Enhancements for Life Asia 1.0****************
 *
 * Enhancement 1:
 *
 * This module has been enhanced to cater for waiver of premium
 * (WOP) processing. The waiver of premium sum assured amount is
 * based on the accumulation of all the premiums of the
 * components on the proposal and the policy fee as specified on
 * table TR517. The validation of the sum assured on the WOP
 * component is as follows :
 *
 * - Read each component for the contract using COVTTRM. Access
 *   table TR517 with the component. If an item does not exist
 *   then continue reading the components until all components
 *   have been read for the contract.
 *
 * - If the item is found on TR517, indicating that we are now
 *   processing a WOP component, then calculate the WOP sum
 *   assured :
 *
 *     - Read the covrage records using COVTLNB logical.
 *     - Accumulate the WOP sum assured if :
 *
 *       (i) COVTLNB-LIFE and COVTTRM-LIFE are the same
 *           and TR517-ZRWVGLG-02 is 'N'. This flag is set to
 *           'N' when a waiver is only applicable to one of the
 *           live's components. If this is the case then a check
 *           is required on the life. If the flag is set to 'Y',
 *           then the check is not required.
 *
 *      (ii) The coverage is present on TR517 indicating that the
 *           premium may be waived on this component.
 *
 *     - Add the policy fee to the accumulated sum assured if
 *       specified on TR517.
 *
 * - If the accumulated sum assured is not the same as the
 *   sum assured on the WOP component, then issue the appropriate
 *   validation error messsge.
 *
 * ==============
 * Enhancement 2:
 *
 * This enhancement allows situation when issuing contracts,
 * suspense available in different currencies is checked to settle
 * the premium payable.  The sequence for checking suspense runs
 * from contract currency, billing currency, then other currency.
 *
 * Underwriting decision date must be captured for successful
 * issue.
 *
 ******************Enhancements for Life Asia 2.0****************
 *
 * Enhancement 1:
 *
 * If a non-traditional contract has been issued and HITR
 * records processed through UNITDEAL, it is possible to process
 * an AFI and then re-issue the contract without running
 * UNITDEAL. Thus we need a test here to prevent re-issuing
 * contracts with unprocessed HITR records.
 *
 * Enhancement 2:
 *
 * Call subroutine HCRTFUP to check/create automatic follow ups.
 *
 * Move the outstanding follow-up check to after the life level
 * check, since follow-ups may get created there.
 *
 *****************************************************************
 * </pre>
 */
public class P6378 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6378");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private ZonedDecimalData wsaaCntfee = new ZonedDecimalData(17, 2);
	protected PackedDecimalData wsaaFactor = new PackedDecimalData(11, 5);
	protected String wsaaOutstanflu = "";
	protected FixedLengthStringData wsaaOutflupFound = new FixedLengthStringData(1);
	protected FixedLengthStringData wsaaDocReqd = new FixedLengthStringData(1);
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).setUnsigned();
	private String wsaaErrorFlag = "";
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).init(ZERO);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsbbAnbInt = new ZonedDecimalData(11, 0).init(0).setUnsigned();
	private ZonedDecimalData sub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaToleranceApp = new ZonedDecimalData(4, 2);
	private ZonedDecimalData wsaaToleranceApp2 = new ZonedDecimalData(4, 2);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	protected ZonedDecimalData wsbbSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaCalcTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalcTolerance2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmountLimit = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmountLimit2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance2 = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotalPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalSuspense = new PackedDecimalData(17, 2);
	private String wsaaInsufficientSuspense = "N";
	protected PackedDecimalData wsaaTotamnt = new PackedDecimalData(17, 2);
	protected String wsaaSingPrmInd = "N";
	protected ZonedDecimalData wsaaSingpFee = new ZonedDecimalData(17, 2);
	private String wsaaSuspInd = "";

	private FixedLengthStringData wsaaT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 0);
	private FixedLengthStringData wsaaT5667Curr = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 4);
	private String wsaaAgtTerminatedFlag = "";

	private FixedLengthStringData wsaaBnytypes = new FixedLengthStringData(60);                    //ILIFE-6368 Start
	private FixedLengthStringData[] wsaaBnytype = FLSArrayPartOfStructure(30, 2, wsaaBnytypes, 0); //ILIFE-6368 End

	private FixedLengthStringData wsaaMandatorys = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaMandatory = FLSArrayPartOfStructure(30, 1, wsaaMandatorys, 0);

	private FixedLengthStringData wsaaFound = new FixedLengthStringData(1).init(SPACES);
	private Validator found = new Validator(wsaaFound, "Y");
	private Validator notFound = new Validator(wsaaFound, "N");
	private int wsaaItc = 0;
	private PackedDecimalData wsaaIuc = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaIvc = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaIwc = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaIzc = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaIxc = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaIyc = new PackedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaProdError = new FixedLengthStringData(24);
	private FixedLengthStringData wsaaProdtyp = new FixedLengthStringData(4).isAPartOf(wsaaProdError, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaProdError, 4, FILLER).init(" ");
	private FixedLengthStringData wsaaProdErr = new FixedLengthStringData(19).isAPartOf(wsaaProdError, 5);

	/*01  WSAA-T3625-KEY.                                         <004>*/
	private FixedLengthStringData wsaaPayrkey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrkey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrkey, 8);
	/*                                                            <022>
		01  FILLER REDEFINES WSAA-PREM-DATE1.                       <022>
		                                                            <022>
		01  FILLER REDEFINES WSAA-PREM-DATE2.                       <022>*/
	protected ZonedDecimalData wsaaRiskCessDate = new ZonedDecimalData(8, 0).init(0);
	protected ZonedDecimalData wsaaPremCessDate = new ZonedDecimalData(8, 0).init(0);
	protected FixedLengthStringData wsaaCovtCoverage = new FixedLengthStringData(2).init(" ");
	protected FixedLengthStringData wsaaCovtLife = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0);
	private String wsaaWaiveIt = "";
	private PackedDecimalData wsaaWaiveSumins = new PackedDecimalData(13, 2);
	private String wsaaWaiveCont = "";

	private FixedLengthStringData wsaaZrwvflgs = new FixedLengthStringData(4);
	private FixedLengthStringData[] wsaaZrwvflg = FLSArrayPartOfStructure(4, 1, wsaaZrwvflgs, 0);
	private FixedLengthStringData wsaaTr517Rec = new FixedLengthStringData(250);
	private PackedDecimalData wsaaTax = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotalTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntfeeTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaWavCntfeeTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSingpfeeTax = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaAgentError = new FixedLengthStringData(59);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaAgentError, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaAgentError, 8, FILLER).init(SPACES);
	private FixedLengthStringData wsaaErrmesg = new FixedLengthStringData(50).isAPartOf(wsaaAgentError, 9);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaTh611Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTh611Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaTh611Item, 0);
	private FixedLengthStringData wsaaTh611Currcode = new FixedLengthStringData(4).isAPartOf(wsaaTh611Item, 3);
	private PackedDecimalData wsaaModalPremium = new PackedDecimalData(15, 2);
	//ILIFE-4168 start
	private PackedDecimalData wsaaSumInsure = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaInstPrem = new PackedDecimalData(15, 2);
	//ILIFE-4168 end
	private String wsaaAgentSuspend = "N";

	private FixedLengthStringData wsaaBlackList = new FixedLengthStringData(1);
	private Validator noIssue = new Validator(wsaaBlackList, "I","X");
	/* WSAA-MAIN-LIFE-DETS */
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
	/* WSBB-JOINT-LIFE-DETS */
	private PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsbbSex = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaMainLife = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaPremStatuz = new FixedLengthStringData(1);
	private Validator premReqd = new Validator(wsaaPremStatuz, "Y");
	private FixedLengthStringData wsaaMainCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaMainCoverage = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaMainCessdate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaMainPcessdte = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaMainMortclass = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaClntnumIo = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaExtraMsgpfx = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaRevisitQuestionnaire = new FixedLengthStringData(1);
	private Validator wsaaRevisitQ = new Validator(wsaaRevisitQuestionnaire, "Y");
	protected FixedLengthStringData wsaaQuestset = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaParams = new FixedLengthStringData(138);
	private FixedLengthStringData wsaaErrorChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaParams, 0);
	private FixedLengthStringData wsaaErrorMsg = new FixedLengthStringData(130).isAPartOf(wsaaParams, 8).init(SPACES);


	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	/*    COPY WSSPSMART.*/
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private AcblenqTableDAM acblenqIO = new AcblenqTableDAM();
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClblTableDAM clblIO = new ClblTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();

	private DescTableDAM descIO = new DescTableDAM();
	private GrpsTableDAM grpsIO = new GrpsTableDAM();
	private HitrrnlTableDAM hitrrnlIO = new HitrrnlTableDAM();
	protected HpadTableDAM hpadIO = new HpadTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private MandlnbTableDAM mandlnbIO = new MandlnbTableDAM();
	protected RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
	// private UndlTableDAM undlIO = new UndlTableDAM(); //IBPLIFE-8671
	private UtrnrnlTableDAM utrnrnlIO = new UtrnrnlTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Vlpdrulrec vlpdrulrec = new Vlpdrulrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5645rec t5645rec = new T5645rec();
	private T3695rec t3695rec = new T3695rec();
	protected T5688rec t5688rec = new T5688rec();
	private T5674rec t5674rec = new T5674rec();
	private T5687rec t5687rec = new T5687rec();
	protected T5661rec t5661rec = getT5661rec();
	private T5667rec t5667rec = new T5667rec();
	private T3678rec t3678rec = new T3678rec();
	protected T6640rec t6640rec = new T6640rec();
	protected T6687rec t6687rec = new T6687rec();
	private T3620rec t3620rec = new T3620rec();
	private Tr517rec tr517rec = new Tr517rec();
	protected Th506rec th506rec = new Th506rec();
	private Th611rec th611rec = new Th611rec();
	private Tr675rec tr675rec = new Tr675rec();
	private T6771rec t6771rec = new T6771rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Tr52zrec tr52zrec = new Tr52zrec();
	private T6654rec t6654rec = new T6654rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	protected Errmesgrec errmesgrec = new Errmesgrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	protected Mgfeelrec mgfeelrec = new Mgfeelrec();
	protected Prasrec prasrec = new Prasrec();
	private Csncalcrec csncalcrec = new Csncalcrec();
	protected Hcrtfuprec hcrtfuprec = getHcrtfuprec();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Premiumrec premiumrec = new Premiumrec();
	private Chkrlrec chkrlrec = new Chkrlrec();
	private T5675rec t5675rec = new T5675rec();
	private Freqcpy freqcpy = new Freqcpy();
	private Undwsubrec undwsubrec = new Undwsubrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	protected Zrdecplrec zrdecplrec = new Zrdecplrec();
	private S6378ScreenVars sv = getLScreenVars();	//ScreenProgram.getScreenVars( S6378ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	protected WsaaBillingInformationInner wsaaBillingInformationInner = new WsaaBillingInformationInner();
	//MIBT-365
	private CcdtTableDAM ccdtIO = new CcdtTableDAM();
	private static final String ccdtrec = "CCDTREC";
	private FixedLengthStringData wsaaValidCcStatus = new FixedLengthStringData(2);
	private Validator validCcStatus = new Validator(wsaaValidCcStatus, "IF");
	Undlpf undlpf= new Undlpf(); //IBPLIFE-8671

	//TMLII-1285
	private List<String> valErrorCodes = new ArrayList<String>();

	//TSD-266 Starts
	//	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	boolean ispermission2 = false;
	private boolean chinaOccupPermission = false;
	//TSD-266 Ends
	//TSD-487
	private FixedLengthStringData wsbbNRStat = new FixedLengthStringData(2).init("NR");
	private FixedLengthStringData wsbbPRStat = new FixedLengthStringData(2).init("PR");
	private FixedLengthStringData wsbbMRStat = new FixedLengthStringData(2).init("MR");
	//TSD-487
	private FixedLengthStringData wsbbPSStat = new FixedLengthStringData(2).init("PS");
	/* ILIFE-2472 */
	//private String wsaaVldPayClt = "";
	//private String wsaa1Lifcnum = "";
	//private String wsaaAsgnPayClt = "";
	private FixedLengthStringData wsaaVldPayClt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaa1Lifcnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAsgnPayClt = new FixedLengthStringData(8);
	/* ILIFE-2472 Ends */
	//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
	private ExternalisedRules er = new ExternalisedRules();
	private String payrpfBillingFreq = "";
	private String payrpfBillingChnl = "";
	private PayrpfDAO payrpfFSUDAO = getApplicationContext().getBean("payrpfFSUDAO", PayrpfDAO.class);
	protected LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private PcddpfDAO pcddpfDAO = getApplicationContext().getBean("pcddpfDAO", PcddpfDAO.class);
	protected FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	protected CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private UndcpfDAO undcpfDAO = getApplicationContext().getBean("undcpfDAO", UndcpfDAO.class);
	private AsgnpfDAO asgnpfDAO = getApplicationContext().getBean("asgnpfDAO", AsgnpfDAO.class);
	private CtrspfDAO ctrspfDAO = getApplicationContext().getBean("ctrspfDAO", CtrspfDAO.class);
	private BnfypfDAO bnfypfDAO = getApplicationContext().getBean("bnfypfDAO", BnfypfDAO.class);
	private UnltpfDAO unltpfDAO = getApplicationContext().getBean("unltpfDAO", UnltpfDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);


	private Clntpf clntpfData = null;
	private boolean isbillday = false; //ILIFE-4128

	private boolean ctaxPermission = false;
	private Rlvrpf rlvrpf=null;
	private RlvrpfDAO rlvrpfDAO = getApplicationContext().getBean("rlvrpfDAO", RlvrpfDAO.class);

	//ILIFE-4492 Modified the enquiry method for reducing time	

	private TtrcpfDAO ttrcpfDAO = getApplicationContext().getBean("ttrcpfDAO", TtrcpfDAO.class);
	private UndqpfDAO undqpfDAO = getApplicationContext().getBean("undqpfDAO", UndqpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

	private List<Itempf> itdmpfList = null;
	private List<Covtpf> covtpfList = null;

	private Itempf itdmpf = null;
	protected Covtpf covtpf = null;
	protected Covtpf covttrm = null;

	private PackedDecimalData wsaaCompTax = new PackedDecimalData(17, 2);
	protected ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private UndlpfDAO undlpfDAO = getApplicationContext().getBean("undlpfDAO", UndlpfDAO.class);// IBPLIFE-8671
	protected Itempf itempf = null;
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	protected boolean wsaaEof;
	protected List<Covtpf> covttrmList = null;
	protected Iterator<Covtpf> iterator ;
	private static final String BA = "BA";
	private static final String TD5G0 = "TD5G0";
	private Td5g0rec td5g0rec = new Td5g0rec();
	private String agentBank; 
	private String teller;
	private String bankMang;
	private AgslpfDAO agslpfDAO = getApplicationContext().getBean("agslpfDAO", AgslpfDAO.class);
	private AgntpfDAO agntpfDAO = getApplicationContext().getBean("agntpfDAO", AgntpfDAO.class);
	private BnkoutpfDAO bnkoutpfDAO = getApplicationContext().getBean("bnkoutpfDAO", BnkoutpfDAO.class);
	private Bnkoutpf bnkoutpf = null;
	private boolean isLicenseFields= false;
	
	//Xuhuifang
	private static final Logger LOGGER = LoggerFactory.getLogger(ScreenProgCS.class);
	boolean ctcnl002Permission = false; // ICIL-93
	private String chdrStatCdoe="PS";
	//ILIFE-8299-Start
	boolean CMRPY012Permission = false;
	private static final String feaConfigCMRPY012Permission = "CMRPY012";
	//ILIFE-8299-END
	//ILJ-8-start
	private AgqfpfDAO agqfpfDAO = getApplicationContext().getBean("agqfpfDAO", AgqfpfDAO.class);
	private boolean japanLocPermission = false;
	private Tjl02rec tjl02rec = new Tjl02rec();
	private static final String VULCERT = "VULCERT ";
	private static final String FXCERT = "FXCERT  ";
	////ILJ-8-end
	
	private static final String  ContDt_FEATURE_ID = "NBPRP113";
	private boolean contDtCalcFlag = false;//ILJ-44
	//ILJ-62
	private boolean NBPRP115Permission = false;
	private NBProposalRec nbproposalRec = new NBProposalRec();
	
	
	private static final String  Undwjpn_FEATURE_ID = "UNWRT002";
	private boolean undwjpnFlag = false;
	protected Jundwfuprec jundwfuprec = new Jundwfuprec();
	private boolean lincFlag = false;
	private static final String lincFeature = "NBPRP116";
	
	private HbnfTableDAM hbnfIO = new HbnfTableDAM();
	//IBPLIFE-2274 Starts
	private static final String ONE_P_CASHLESS="NBPRP124"; 
	private boolean onePflag = false;	
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private T3615rec t3615rec = new T3615rec();
	private PackedDecimalData wsaa1PSuspense = new PackedDecimalData(17, 2);
	//IBPLIFE-2274 Ends
	List<Acblpf> acblpfList = new ArrayList<>();
	private UwoccupationUtil uwoccupationUtil = getApplicationContext().getBean("uwoccupationUtil", UwoccupationUtil.class);
	private UwQuestionnaireUtil uwQuestionnaireUtil = getApplicationContext().getBean("uwQuestionnaireUtil", UwQuestionnaireUtil.class);
	private UwlvTableDAM uwlvIO = null;
	
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private Clntpf clntpf;
	private com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao;
	private com.csc.smart400framework.dataaccess.model.Clntpf clnt;
	private boolean mrtaPermission = false;
	private Vpxchdrrec vpxchdrrec = null;
	private Vpxlextrec vpxlextrec = null;
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
	protected Hpadpf hpadpf = null;
	private MrtapfDAO mrtapfDAO = getApplicationContext().getBean("mrtapfDAO", MrtapfDAO.class);
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
  	protected Mrtapf mrtaIO;
	protected Th615rec th615rec = new Th615rec();
	private boolean covrValidated = false;
	//PINNACLE-2696
	private boolean CTISS014Permission = false;
	private static final String CTISS014 = "CTISS014";
		/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		loadPayerDetails1035,
		check1101,
		readCovt1102,
		cont1108,
		compValidation1120,
		exit1190,
		readseqCovtlnb1310,
		continue1380,
		exit1390,
		findMaxMinLimit1410,
		exit1420,
		readNextFollowUp1530,
		exit1590,
		readT67711642,
		exit1649,
		readNext1780,
		exit1790,
		checkGroup1d50,
		incrementSub1d85,
		callCovtlnbio5210,
		nextr5289,
		exit5290
	}

	public P6378() {
		super();
		screenVars = sv;
		new ScreenModel("S6378", AppVars.getInstance(), sv);
	}

	protected S6378ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S6378ScreenVars.class);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			processBoMainline(sv, sv.dataArea, parmArray);
			// added to bypass endp state of scrnparams.statuz
			if(isEQ(wsspcomn.sectionno, "2000")){
				if(isEQ(scrnparams.statuz, Varcom.endp)){
					scrnparams.statuz.set(Varcom.oK);
				}
			}
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	public void processBo1(Object... parmArray) {
		valErrorCodes = (ArrayList<String>)parmArray[5];
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			processBoMainline(sv, sv.dataArea, parmArray);
			// added to bypass endp state of scrnparams.statuz
			if(isEQ(wsspcomn.sectionno, "2000")){
				if(isEQ(scrnparams.statuz, Varcom.endp)){
					scrnparams.statuz.set(Varcom.oK);
				}
			}

		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpfData.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clntpfData.getSurname());
		/*LGNM-EXIT*/
	}

	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpfData.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpfData.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpfData.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpfData.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpfData.getSurname());
		}
		/*PLAIN-EXIT*/
	}

	protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpfData.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(clntpfData.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpfData.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clntpfData.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clntpfData.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clntpfData.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clntpfData.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clntpfData.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

	protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpfData.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpfData.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	 * <pre>
	 *  END OF CONFNAME **********************************************
	 *      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000()
	{
		ctcnl002Permission  = FeaConfg.isFeatureExist("2", "NBPRP084", appVars, "IT");     // ICIL-93
		CMRPY012Permission = FeaConfg.isFeatureExist("2", feaConfigCMRPY012Permission, appVars, "IT");// ILIFE-8299
		NBPRP115Permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP115", appVars, "IT");//ILJ-62
		onePflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), ONE_P_CASHLESS, appVars, "IT"); //IBPLIFE-2274
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		CTISS014Permission  = FeaConfg.isFeatureExist("2", CTISS014, appVars, "IT"); //PINNACLE-2696
		initialise1010();
	}
	//ILIFE-4168 Code and Model Promotion for VPMS externalization of LIFE Extended Validation Calculation for RUL Product Start
	protected void extendedValidation(){

		boolean extValidation = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBEXTV01", appVars, "IT");
		List<Covtpf> covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		Covtpf covtpf=covtpfList.get(0);
		//ILIFE-4970
		if(AppVars.getInstance().getAppConfig().isVpmsEnable() && extValidation && er.isExternalized(chdrlnbIO.cnttype.toString(), covtpf.getCrtable()))/* IJTI-1386 */
		{
			try {
				getExtValidationMSG();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	protected void getExtValidationMSG() throws VpmsLoadFailedException{

		int nofCover;
		String businessdate;
		Covtpf covtpf;
		String messageCount; 
		int count;
		String lang="E";
		boolean langflag=false;
		//ILIFE-4970 start	
		Lextpf lextpf=new Lextpf();
		Unltpf unltpf = new Unltpf() ;
		//ILIFE-4970 end
		IVpmsBaseSession session = null;
		VpmsComputeResult result;
		IVpmsBaseSessionFactory factory = new VpmsJniSessionFactory();

		List<Covtpf> covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

		if(AppConfig.getVpmsXeServer().equalsIgnoreCase("true")){
			factory = new VpmsTcpSessionFactory(AppConfig.getVpmsXeServerIp(), AppConfig.getVpmsXeServerPort());
		}

		wsaaClntnumIo.set(chdrlnbIO.getCownnum());
		a3000CallCltsio();

		if(AppVars.getInstance().getBusinessdate()!=null && !AppVars.getInstance().getBusinessdate().equals("")){
			String[] aList = AppVars.getInstance().getBusinessdate().split("/");
			businessdate=aList[2]+aList[1]+aList[0];
		}
		else
		{
			businessdate=AppConfig.getVpmsTransEffDate();
		} 

		nofCover=covtpfList.size();
		//ILIFE-4899 
		covtpf=covtpfList.get(0);
		session= factory.create(AppConfig.getVpmsModelDirectory()+"LANGUAGE/INTLIFE.VPM");
		session.setAttribute("Input Transaction Date", businessdate);
		session.setAttribute("Input TransEffDate", businessdate);
		session.setAttribute("Input Calling System", "");
		session.setAttribute("Input Calling Environment", "");
		session.setAttribute("Input Region", "");
		session.setAttribute("Input Locale", "");

		session.setAttribute("Input Contract Type", sv.cnttype.toString());
		session.setAttribute("Input Number of Lives", "1");
		session.setAttribute("Input Life Life ID", "1");
		session.setAttribute("Input Transaction", wsaaBatckey.batcBatctrcde.toString());
		session.setAttribute("Input Life Joint Life ID", "0");
		session.setAttribute("Input Number of Persons", "1");
		session.setAttribute("Input Client Role", "LF");
		session.setAttribute("Input Date of Birth", String.valueOf(clntpfData.getCltdob()));
		session.setAttribute("Input Currency", chdrlnbIO.getCntcurr().toString());
		//ILIFE-4899 start
		session.setAttribute("Input Term", String.valueOf(covtpf.getRcestrm()));
		session.setAttribute("Input Mortality", covtpf.getMortcls());
		session.setAttribute("Input Cover Totals Number of Covers", String.valueOf(nofCover));
		wsaaSumInsure.set(0);
		wsaaInstPrem.set(0);
		//ILIFE-6873 start
		langflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBEXTV02", appVars, "IT");
		if(langflag){
			lang="F";
		}
		session.setAttribute("Input Language", lang);
		//ILIFE-6873 end		
		if(!covtpfList.isEmpty()){
			covtpf=covtpfList.get(0);

			session.setAttribute("Input Coverage ID", covtpf.getCoverage());
			session.setAttribute("Input Rider ID", covtpf.getRider());
			session.setAttribute("Input Coverage Code", covtpf.getCrtable());
			wsaaSumInsure.add(PackedDecimalData.parseObject(covtpf.getSumins()));
			wsaaInstPrem.add(PackedDecimalData.parseObject(covtpf.getInstprem()));

			for(int i=1;i<covtpfList.size();i++){
				covtpf=covtpfList.get(i);
				session.setAttribute("Input Coverage ID[0,"+i+"]", covtpf.getCoverage());
				session.setAttribute("Input Rider ID[0,"+i+"]", covtpf.getRider());
				session.setAttribute("Input Coverage Code[0,"+i+"]", covtpf.getCrtable());
				wsaaSumInsure.add(PackedDecimalData.parseObject(covtpf.getSumins()));
				wsaaInstPrem.add(PackedDecimalData.parseObject(covtpf.getInstprem()));

			}

			session.setAttribute("Input Sum Insured", wsaaSumInsure.toString());
			session.setAttribute("Input Calc Prem", wsaaInstPrem.toString());
		}
		//session.setAttribute("Input Rating Totals Number of Rates", "0");

		session.setAttribute("Input Rating Totals Number of Rates", "1");
		//ILIFE-4899 end
		//ILIFE-4970 start	
		lextpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		lextpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		List<Lextpf> LextpfList= lextpfDAO.getLoadAgeLextpf(lextpf);
		if(LextpfList.size()>0){
			lextpf=LextpfList.get(0);
			session.setAttribute("Input Mort Load Reason", lextpf.getOpcda().trim());/* IJTI-1386 */
			session.setAttribute("Input Load Age",Integer.toString(lextpf.getAgerate()));
		}
		else{
			session.setAttribute("Input Mort Load Reason", "");
			session.setAttribute("Input Load Age","0");
		}
		unltpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		unltpf.setChdrnum(chdrlnbIO.getChdrnum().toString());

		List<Unltpf> unltpfList = unltpfDAO.getFundUnltpf(unltpf);
		int fundCount =0;
		if(!unltpfList.isEmpty()){
			unltpf=unltpfList.get(0);
			if(unltpf.getUalfnd01()!=null && !unltpf.getUalfnd01().trim().isEmpty()){
				session.setAttribute("Input Fund", unltpf.getUalfnd01());/* IJTI-1386 */
				fundCount++;
			}
			if(unltpf.getUalfnd02()!=null && !unltpf.getUalfnd02().trim().isEmpty()){
				fundCount++;
				session.setAttribute("Input Fund[0;"+(fundCount-1)+"]",unltpf.getUalfnd02());/* IJTI-1386 */
			}
			if(unltpf.getUalfnd03()!=null && !unltpf.getUalfnd03().trim().isEmpty()){
				fundCount++;
				session.setAttribute("Input Fund[0;"+(fundCount-1)+"]",unltpf.getUalfnd03());/* IJTI-1386 */
			}
			if(unltpf.getUalfnd04()!=null && !unltpf.getUalfnd04().trim().isEmpty()){
				fundCount++;
				session.setAttribute("Input Fund[0;"+(fundCount-1)+"]",unltpf.getUalfnd04());/* IJTI-1386 */
			}
			if(unltpf.getUalfnd05()!=null && !unltpf.getUalfnd05().trim().isEmpty()){
				fundCount++;
				session.setAttribute("Input Fund[0;"+(fundCount-1)+"]",unltpf.getUalfnd05());/* IJTI-1386 */
			}
			if(unltpf.getUalfnd06()!=null && !unltpf.getUalfnd06().trim().isEmpty()){
				fundCount++;
				session.setAttribute("Input Fund[0;"+(fundCount-1)+"]",unltpf.getUalfnd06());/* IJTI-1386 */
			}
			if(unltpf.getUalfnd07()!=null && !unltpf.getUalfnd07().trim().isEmpty()){
				fundCount++;
				session.setAttribute("Input Fund[0;"+(fundCount-1)+"]",unltpf.getUalfnd07());/* IJTI-1386 */
			}
			if(unltpf.getUalfnd08()!=null && !unltpf.getUalfnd08().trim().isEmpty()){
				fundCount++;
				session.setAttribute("Input Fund[0;"+(fundCount-1)+"]",unltpf.getUalfnd08());/* IJTI-1386 */
			}
			if(unltpf.getUalfnd09()!=null && !unltpf.getUalfnd09().trim().isEmpty()){
				fundCount++;
				session.setAttribute("Input Fund[0;"+(fundCount-1)+"]",unltpf.getUalfnd09());/* IJTI-1386 */
			}
			if(unltpf.getUalfnd10()!=null && !unltpf.getUalfnd10().trim().isEmpty()){
				fundCount++;
				session.setAttribute("Input Fund[0;"+(fundCount-1)+"]",unltpf.getUalfnd10());/* IJTI-1386 */
			}
		}

		session.setAttribute("Input Fund Totals Number of Funds", Integer.toString(fundCount));
		//ILIFE-43970
		result=session.computeUsingDefaults("Output Message Count");
		messageCount=result.getResult();
		count=Integer.parseInt(messageCount);

		for(int i=0;i<count;i++){
			result = session.computeUsingDefaults("Output Message Error Description("+i+")");
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.errmesg[1].set(result.toString());

			result = session.computeUsingDefaults("Output Message Coverage ID("+i+")");
			sv.coverage.set(result.toString());

			result = session.computeUsingDefaults("Output Message Rider ID("+i+")");
			sv.rider.set(result.toString());

			result = session.computeUsingDefaults("Output Message Life ID("+i+")");
			sv.life.set(result.toString());

			extValMsg();
		}

	}

	protected void extValMsg()
	{
		/* Go get the error message.*/
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaProg);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.set(SPACES);

		sv.erordsc.set(errmesgrec.errmesg[1]);
		/* Add the record to the subfile.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6378", sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

		wsaaErrorFlag = "Y";
	}
	//ILIFE-4168 Code and Model Promotion for VPMS externalization of LIFE Extended Validation Calculation for RUL Product end
	protected void initialise1010()
	{
		varcom.vrcmTranid.set(wsspcomn.tranid);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaErrorFlag = "N";
		wsaaSingPrmInd = "N";
		wsaaSingpFee.set(ZERO);
		/* MOVE 0                      TO WSAA-SINGP.                   */
		/* MOVE 0                      TO WSAA-INSTPRM.                 */
		/* MOVE 0                      TO WSAA-REGPREM.                 */
		wsaaFactor.set(0);
		wsaaTotalPremium.set(0);
		wsaaTotalSuspense.set(0);
		wsaaInsufficientSuspense = "N";
		sv.payrseqno.set(0);
		wsaaModalPremium.set(0);
		wsaaTax.set(0);
		wsaaTotalTax.set(0);
		wsaaCntfeeTax.set(0);
		wsaaWavCntfeeTax.set(0);
		wsaaSingpfeeTax.set(0);
		wsaaItc = 0;
		wsaaIuc.set(0);
		wsaaIvc.set(0);
		wsaaIwc.set(0);
		wsaaIxc.set(0);
		wsaaIyc.set(0);
		sv.taxamt01.set(0);
		sv.taxamt02.set(0);
		sv.taxamt01Out[Varcom.nd.toInt()].set("Y");
		sv.taxamt02Out[Varcom.nd.toInt()].set("Y");
		sv.taxamt01Out[Varcom.pr.toInt()].set("Y");
		sv.taxamt02Out[Varcom.pr.toInt()].set("Y");
		wsaaEof = false;
		wsbbSub.set(1);
		while ( !(isGT(wsbbSub, 9))) {
			wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBillcd[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaPremTax[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaBillchnl[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaBillcurr[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaMandref[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaGrupkey[wsbbSub.toInt()].set(SPACES);
			wsbbSub.add(1);
		}

		scrnparams.function.set(Varcom.sclr);
		processScreen("S6378", sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* MOVE ZERO                                                    */
		/*      TO S6378-CNTFEE                   .                     */
		/* MOVE ZERO                                                    */
		/*      TO S6378-CNTSUSP                  .                     */
		/* MOVE ZERO                                                    */
		/*      TO S6378-INST-PREM                .                     */
		sv.exrat.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.premCurr.set(ZERO);
		sv.cntfee.set(ZERO);
		sv.pufee.set(ZERO);
		sv.premsusp.set(ZERO);
		initCustomerSpecific();
		wsaaTotamnt.set(ZERO);
		wsspcomn.edterror.set(Varcom.oK);

		//TSD-266 start
		ispermission2 = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "MTL26601", appVars,smtpfxcpy.item.toString());
		//TSD-266 end
		ctaxPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPROP07", appVars, "IT"); //ALS-313
		isbillday = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP011", appVars, "IT");//ILIFE-4128
		chinaOccupPermission = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "NBPRP056", appVars, "IT");
		japanLocPermission = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "AGMTN019", appVars, "IT");
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), ContDt_FEATURE_ID, appVars, "IT");//ILJ-44
		undwjpnFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), Undwjpn_FEATURE_ID, appVars, "IT");
		lincFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), lincFeature, appVars, "IT");
		wsaaBatckey.set(wsspcomn.batchkey);
		readAccRuleTable1020();
		checkCovrRiderCustomerSpecific();
		
		if(contDtCalcFlag)  //ILJ-44
		{
			if (isEQ("P5074", wsspcomn.lastprog))
			{
				if(isEQ(wsspcomn.cltiiflg,"Y"))
				{
					scrnparams.errorCode.set(errorsInner.jl24);  
				}
			}
			
		}
		
		//ILJ-8- start
		if(japanLocPermission) {
			validateAgentQualification();
		}
		//ILJ-8- end
		Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
		boolean uwFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP123", appVars, smtpfxcpy.item.toString());
		if(uwFlag) {
			validateUWDecision();
		}
		return;
	}
	
	private void validateUWDecision() {

		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);

		List<Lifepf> lifepfList = lifepfDAO.searchLifeRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(),
				chdrlnbIO.getChdrnum().toString());

		if (lifepfList != null && !lifepfList.isEmpty()) {
			Clntpf clnt = new Clntpf();
			clnt.setClntpfx("CN");
			clnt.setClntcoy(wsspcomn.fsuco.toString());

			for (Lifepf lifepf : lifepfList) {
				clnt.setClntnum(lifepf.getLifcnum());
				clnt = clntpfDAO.selectActiveClient(clnt);
				List<Covtpf> covtpfDataList = covtpfDAO.getCovtlnbRecord(chdrlnbIO.getChdrcoy().toString(),
						chdrlnbIO.getChdrnum().toString(), lifepf.getLife());
				for (Covtpf covtpfTemp : covtpfDataList) {
					checkEachCovtpf(clnt, covtpfTemp, lifepf);
				}
			}
		}
		return;
	}
	private void checkEachCovtpf(Clntpf clnt, Covtpf covtpfTemp, Lifepf lifepf) {
		if (!validateOccu(clnt, covtpfTemp.getCrtable())) {
			StringBuilder sb = new StringBuilder();
			sb.append("Coverage ");
			sb.append(covtpfTemp.getCrtable());
			sb.append(" Violates UW decision");
			sv.life.set(SPACES);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.errorline.set(sb.toString());
			errorMessages1800();
			return;
		}
		
		if (!validateUWQusetion(lifepf, covtpfTemp.getCrtable())) {
			StringBuilder sb = new StringBuilder();
			sb.append("Coverage ");
			sb.append(covtpfTemp.getCrtable());
			sb.append(" Violates UW decision");
			sv.life.set(SPACES);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.errorline.set(sb.toString());
			errorMessages1800();
			return;
		}
	}
	private boolean validateOccu(Clntpf clntpf, String crtable) {
		String occu = clntpf.getOccpcode();
		String industry = clntpf.getStatcode();
		String company = wsspcomn.company.toString();
		String fsuco = wsspcomn.fsuco.toString();
		String cnttype = chdrlnbIO.getCnttype().toString();
		
		UWOccupationRec uwOccupationRec = new UWOccupationRec();

		uwOccupationRec = new UWOccupationRec();
		uwOccupationRec.indusType.set(industry);
		uwOccupationRec.occupCode.set(occu);
		uwOccupationRec.transEffdate.set(datcon1rec.intDate);
		int result = uwoccupationUtil.getUWResult(uwOccupationRec, company, fsuco, cnttype, crtable);
		
		if(result == 2) {
			if (!"Y".equals(getUserUwdecision())) {
				return false;
			}
		}
		return true;
	}
	
	private boolean validateUWQusetion(Lifepf lifepf, String crtable) {

		List<Undqpf> undqList = undqpfDAO.searchUnqpf(lifepf.getChdrcoy().toString().trim(),
				lifepf.getChdrnum().toString().trim(), lifepf.getLife().toString().trim(),
				lifepf.getJlife().toString().trim());
		List<String> questions = new ArrayList<String>();
		for (Undqpf u : undqList) {
			if ("Y".equals(u.getAnswer().trim())) {
				questions.add(u.getQuestidf());
			}
		}

		if (questions != null && !questions.isEmpty()) {
			UWQuestionnaireRec uwQuestionnaireRec = new UWQuestionnaireRec();
			uwQuestionnaireRec.setTransEffdate(datcon1rec.intDate.getbigdata());
			uwQuestionnaireRec.setContractType(chdrlnbIO.getCnttype().toString());

			for (String question : questions) {
				boolean result = checkEachQuestion(question, uwQuestionnaireRec, crtable);
				if(!result) {
					return false;
				}
			}
		}
		return true;
	}
	private boolean checkEachQuestion(String question, UWQuestionnaireRec uwQuestionnaireRec, String crtable) {
		UWQuestionnaireRec uwQuestionnaireRecReturn = uwQuestionnaireUtil.getUWMessage(uwQuestionnaireRec,
				wsspcomn.language.toString(), wsspcomn.company.toString(), question, crtable, "");
		if (!uwQuestionnaireRecReturn.getOutputUWDec().isEmpty()
				&& uwQuestionnaireRecReturn.getOutputUWDec().contains("Decline")) {
			if (!"Y".equals(getUserUwdecision())) {
				return false;
			}
		}
		return true;
	}
	private String getUserUwdecision() {
		if(uwlvIO == null) {
			uwlvIO = new UwlvTableDAM();
			uwlvIO.setCompany(wsspcomn.company);
			uwlvIO.setUserid(wsspcomn.userid);
			uwlvIO.setFormat("UWLVREC");
			uwlvIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, uwlvIO);
		}
		return uwlvIO.getUwdecision().toString().trim();
	}
	
	private void validateAgentQualification() 
	{
		//get item from itempf
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.tJL02.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if(null == itempf) {
			return;
		}else {
			tjl02rec.tjl02Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			List<String> qfListByCnttype = new ArrayList<String>();
			for(int i = 0; i <= tjl02rec.qlfy.length-1; i++) {
				if(null != tjl02rec.qlfy[i] && !tjl02rec.qlfy[i].toString().trim().isEmpty()) {
					qfListByCnttype.add(tjl02rec.qlfy[i].toString());
				}
			}

			if(null != chdrlnbIO.getAgntnum() && isNE(chdrlnbIO.getAgntnum(), SPACES)) {
				//validate for servicing agent
				agntIO.setAgntpfx("AG");
				agntIO.setAgntcoy(wsspcomn.company);
				agntIO.setAgntnum(chdrlnbIO.getAgntnum());
				agntIO.setFunction(Varcom.readr);
				agntIO.setFormat(formatsInner.agntrec);
				SmartFileCode.execute(appVars, agntIO);
				if (isNE(agntIO.getStatuz(), Varcom.oK)) {
					syserrrec.statuz.set(agntIO.getStatuz());
					syserrrec.params.set(agntIO.getParams());
					fatalError600();
				}
				List<Agqfpf> qfList1 = agqfpfDAO.getAgqfListbyAgentNumber(chdrlnbIO.getAgntnum().toString());
				
				if(null == qfList1 || qfList1.isEmpty()) {
					
					errorMessageIfNoQualification(qfListByCnttype);
					
				} else {
					errorMessageForQualification(qfListByCnttype, qfList1);
				}
				
				
				//validate commission agents
				List<Pcddpf> pcddpfList = null;	
				pcddpfList = pcddpfDAO.searchPcddRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

				for(Pcddpf pcddpf : pcddpfList){
					
					if (isEQ(pcddpf.getChdrcoy(), wsspcomn.company)
							&& isEQ(pcddpf.getAgntNum(), chdrlnbIO.getAgntnum())) {
						continue;
					}
					agntIO.setAgntpfx("AG");
					agntIO.setAgntcoy(wsspcomn.company);
					agntIO.setAgntnum(pcddpf.getAgntNum());
					agntIO.setFunction(Varcom.readr);
					agntIO.setFormat(formatsInner.agntrec);
					SmartFileCode.execute(appVars, agntIO);
					if (isNE(agntIO.getStatuz(), Varcom.oK)) {
						syserrrec.statuz.set(agntIO.getStatuz());
						syserrrec.params.set(agntIO.getParams());
						fatalError600();
					}
					List<Agqfpf> qfList2 = agqfpfDAO.getAgqfListbyAgentNumber(pcddpf.getAgntNum());
					
					if(null == qfList2 || qfList2.isEmpty()) {
						
						errorMessageIfNoQualification(qfListByCnttype);
						
					}else {
						
						errorMessageForQualification(qfListByCnttype, qfList2);
					}
				}
			}
		}
	}
	
	private void errorMessageForQualification(List<String> qfListByCnttype, List<Agqfpf> qfList1) 
	{
		int fxFlag = 0;
	    int vulFlag=  0;
	    int tjl02fxFlag = 0;
	    int tjl02vulFlag = 0;
		for(String s : qfListByCnttype) {
			for(int i=0;i <qfList1.size(); i++) {
				
				if(s.equals(FXCERT)) {
					tjl02fxFlag = 1;
					if(qfList1.get(i).getQualification().equalsIgnoreCase(s)) {
						fxFlag=1;
						//ILJ-19
						if(isLT(qfList1.get(i).getDateend(), chdrlnbIO.getOccdate())) {
							errmesgrec.errmesgRec.set(SPACES);
							errmesgrec.eror.set(errorsInner.jl19);
							agentErrorMessage1850();
						}
					}
				}
				else if(s.equals(VULCERT)) {
					tjl02vulFlag = 1;
					if(qfList1.get(i).getQualification().equalsIgnoreCase(s)) {
						vulFlag=1;
						//ILJ-19
						if(isLT(qfList1.get(i).getDateend(), chdrlnbIO.getOccdate())) {
							errmesgrec.errmesgRec.set(SPACES);
							errmesgrec.eror.set(errorsInner.jl20);
							agentErrorMessage1850();
						}
					}
				}
			}
		}
		if(tjl02fxFlag == 1 && fxFlag == 0) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.jl11);
			agentErrorMessage1850();					
		}
		if(tjl02vulFlag == 1 && vulFlag == 0) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.jl12);
			agentErrorMessage1850();
		}
	}

	private void errorMessageIfNoQualification(List<String> qfListByCnttype) {
		for(String s : qfListByCnttype) {
			if(s.equals(FXCERT)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.jl11);
				agentErrorMessage1850();
			}
			else if(s.equals(VULCERT)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.jl12);
				agentErrorMessage1850();
			}
		}
	}

	/**
	 * <pre>
	 *    Set screen fields
	 * Copy of P5009 program..  till 2000 section.
	 * </pre>
	 */
	protected void readAccRuleTable1020()
	{
		/*    Read the Program  table T5645 for the Financial Accounting*/
		/*    Rules for the transaction.*/

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t5645.toString());
		itempf.setItemitem(wsaaProg.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if(null == itempf) {
			/*     MOVE U016                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(errorsInner.g816);
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit1190);
			return;
		}
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		getSign1020();
		return;

	}

	protected void getSign1020()
	{
		/*    Read the table T3695 for the field sign of the 01 posting.*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t3695.toString());
		itempf.setItemitem(t5645rec.sacstype01.toString());
		itempf = itempfDAO. getItempfRecord(itempf);


		if(null == itempf) {
			scrnparams.errorCode.set(errorsInner.g228);
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit1190);
			return;
		}
		t3695rec.t3695Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		getDate1020();
		return;
	}

	protected void getDate1020()
	{
		/*    Get the current date from DATCON1.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);

		retreiveHeader1030();
		return;
	}

	protected void retreiveHeader1030()
	{
		/*    Read CHDRLNB (retrv) in order to obtain the contract header.*/
		chdrlnbIO.setFunction(Varcom.retrv);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		covttrmList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		/* Read HPAD file for Proposal Details                             */
		hpadIO.setDataArea(SPACES);
		hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlnbIO.getChdrnum());
		hpadIO.setFormat(formatsInner.hpadrec);
		hpadIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		/* Read table TH506 to see if underwriting decision date is        */
		/* mandatory and therefore check HPAD-HUWDCDATE is set             */

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.th506.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (null == itempf) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.th506.toString());
			if (itempf.getItemitem() !=  "***") {
				itempf.setItemitem("***");
				itempf = itempfDAO. getItempfRecord(itempf);	
			}
		}
		if (null != itempf) {
			th506rec.th506Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		
		
		stpCheckingCustomerSpecific();
		
		
		/* Check Temporary receipt number and date.                        */
		if (isNE(th506rec.cflg, "Y")) {
			//goTo(GotoLabel.loadPayerDetails1035);
			loadPayerDetails1035();
			return;
		}

		List<Ttrcpf> ttrcpfList = this.ttrcpfDAO.getTtrcpfByLTEfdte(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(), varcom.vrcmMaxDate.toInt());

		if(ttrcpfList == null || ttrcpfList.size() == 0){
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.tl15);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();	
		}
		else {
			if (isEQ(ttrcpfList.get(0).getTtmprcno(), SPACES)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.tl15);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
			else {
				if (isEQ(ttrcpfList.get(0).getTtmprcdte(), varcom.vrcmMaxDate)) {
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.tl16);
					wsaaExtraMsgpfx.set(SPACES);
					errorMessages1800();
				}
			}
		}

		loadPayerDetails1035();
		return;
	}
	private String getUUID() {
		String uuid = UUID.randomUUID().toString();
		uuid = uuid.replace("-", "");    
		return uuid;
		
	}
	
	public   int doPost(String url, JSONObject param) {
        HttpPost httpPost = null;
        int result = 0;
        try {
            HttpClient client = new DefaultHttpClient();
            httpPost = new HttpPost(url);
            if (param != null) {
                StringEntity se = new StringEntity(param.toString(), "utf-8");
                httpPost.setEntity(se); // add json data to post
                httpPost.setHeader("Content-Type", "application/json");
            }	            
            HttpResponse response = client.execute(httpPost);          
            if (response != null) {
            	result=response.getStatusLine().getStatusCode();
            }
            
        } catch (Exception ex) {
        	LOGGER.error("Interface error", ex);
        }
        return result;
    }

	
	

	protected void loadPayerDetails1035()
	{
		List<Payrpf> payrpfList = null;

		/* Load the WS table by reading all the payer                      */
		/* and payer role recored for this contract.                       */

		payrpfList = payrpfFSUDAO.searchPayrRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

		for(Payrpf payrpf : payrpfList){

			if(!(isNE(payrpf.getChdrcoy(), chdrlnbIO.getChdrcoy())
					|| isNE(payrpf.getChdrnum(), chdrlnbIO.getChdrnum()))){

				loadPayerDetails1b00(payrpf);
			}else{
				break;
			}

		}

		readContractLongdesc1040();
		return;
	}

	/**
	 * <pre>
	 ****Read T3625 to access the payment plan  details for the contrac
	 **** MOVE SPACES                 TO ITEM-DATA-KEY.
	 **** MOVE 'IT'                   TO ITEM-ITEMPFX.
	 **** MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.
	 **** MOVE T3625                  TO ITEM-ITEMTABL.
	 **** MOVE 'LP'                   TO WSAA-T3625-LP.
	 **** MOVE CHDRLNB-BILLCHNL       TO WSAA-T3625-BILLCHNL.
	 **** MOVE CHDRLNB-BILLFREQ       TO WSAA-T3625-BILLFREQ.
	 **** MOVE WSAA-T3625-KEY         TO ITEM-ITEMITEM.
	 **** MOVE 'READR'                TO ITEM-FUNCTION.
	 **** CALL 'ITEMIO' USING ITEM-PARAMS.
	 ****  F ITEM-STATUZ              NOT = O-K
	 ****                         AND NOT = MRNF
	 ****     MOVE ITEM-STATUZ        TO SYSR-STATUZ
	 ****     PERFORM 600-FATAL-ERROR.
	 **** IF ITEM-STATUZ              = MRNF
	 ****    MOVE SPACES              TO ERMS-ERRMESG-REC
	 ****    MOVE U023                TO ERMS-EROR
	 ****    PERFORM 1800-ERROR-MESSAGES.
	 **** MOVE ITEM-GENAREA           TO T3625-T3625-REC.
	 *1035-READ-MANDATE-AND-CLBL.                                 <014>
	 ****                                                         <014>
	 ****   If method of payment is direct debit read the         <014>
	 ****   relative mandate record & client bank A/C details     <014>
	 ****                                                         <014>
	 ****   IF CHDRLNB-BILLCHNL = 'B'                             <009>
	 ****  IF T3625-DDIND      = 'Y'                              <014>
	 ****     MOVE SPACES                 TO MANDLNB-PARAMS       <014>
	 ****     IF CHDRLNB-PAYRNUM = SPACES                         <014>
	 ****       MOVE CHDRLNB-COWNCOY      TO MANDLNB-PAYRCOY      <014>
	 ****       MOVE CHDRLNB-COWNNUM      TO MANDLNB-PAYRNUM      <014>
	 ****     ELSE                                                <014>
	 ****        MOVE CHDRLNB-PAYRCOY      TO MANDLNB-PAYRCOY     <014>
	 ****        MOVE CHDRLNB-PAYRNUM     TO MANDLNB-PAYRNUM      <014>
	 ****     END-IF                                              <014>
	 ****     MOVE CHDRLNB-MANDREF        TO MANDLNB-MANDREF      <014>
	 ****     MOVE READR                  TO MANDLNB-FUNCTION     <014>
	 ****     MOVE MANDLNBREC             TO MANDLNB-FORMAT       <014>
	 ****                                                         <014>
	 ****     CALL 'MANDLNBIO' USING MANDLNB-PARAMS               <014>
	 ****                                                         <014>
	 ****     IF MANDLNB-STATUZ          NOT = O-K                <014>
	 ****        AND MANDLNB-STATUZ      NOT = MRNF               <014>
	 ****       MOVE MANDLNB-PARAMS      TO SYSR-PARAMS           <014>
	 ****       PERFORM 600-FATAL-ERROR                           <014>
	 ****     END-IF                                              <014>
	 ****                                                         <014>
	 ****    IF MANDLNB-STATUZ = MRNF                             <014>
	 ****        MOVE SPACES              TO ERMS-ERRMESG-REC     <014>
	 ****        MOVE I007                TO ERMS-EROR            <014>
	 ****        PERFORM 1800-ERROR-MESSAGES                      <014>
	 ****    END-IF                                               <014>
	 ****                                                         <014>
	 **Read CLBL Bank Details.                                    <014>
	 ****                                                         <014>
	 ****    MOVE CHDRLNB-BANKKEY        TO CLBL-BANKKEY          <014>
	 ****    MOVE CHDRLNB-BANKACCKEY     TO CLBL-BANKACCKEY       <014>
	 ****    IF MANDLNB-STATUZ = O-K                              <014>
	 ****       MOVE MANDLNB-BANKKEY        TO CLBL-BANKKEY       <014>
	 ****       MOVE MANDLNB-BANKACCKEY     TO CLBL-BANKACCKEY    <014>
	 ****       MOVE READR                  TO CLBL-FUNCTION      <014>
	 ****                                                         <014>
	 ****       CALL 'CLBLIO' USING         CLBL-PARAMS           <014>
	 ****       IF CLBL-STATUZ              NOT = O-K AND         <014>
	 ****                                   NOT = MRNF            <014>
	 ****         MOVE CLBL-PARAMS         TO SYSR-PARAMS         <014>
	 ****         MOVE CLBL-STATUZ         TO SYSR-STATUZ         <014>
	 ****         PERFORM 600-FATAL-ERROR                         <014>
	 ****       END-IF                                            <014>
	 ****                                                         <014>
	 **If bank A/C not on file display message                    <014>
	 ****                                                         <014>
	 ****       IF CLBL-STATUZ              = MRNF                <014>
	 ****          MOVE SPACES              TO ERMS-ERRMESG-REC   <014>
	 ****          MOVE F826                TO ERMS-EROR          <014>
	 ****          PERFORM 1800-ERROR-MESSAGES                    <014>
	 ****       END-IF                                            <014>
	 **Read table T3678                                           <014>
	 ****       MOVE SPACES              TO ITEM-DATA-KEY         <014>
	 ****       MOVE 'IT'                TO ITEM-ITEMPFX          <014>
	 ****       MOVE WSSP-COMPANY        TO ITEM-ITEMCOY          <014>
	 ****       MOVE WSSP-FSUCO          TO ITEM-ITEMCOY          <014>
	 ****       MOVE T3678               TO ITEM-ITEMTABL         <014>
	 ****       MOVE MANDLNB-MANDSTAT    TO ITEM-ITEMITEM         <014>
	 ****       MOVE 'READR'             TO ITEM-FUNCTION         <014>
	 ****                                                         <014>
	 ****       CALL 'ITEMIO' USING ITEM-PARAMS                   <014>
	 ****       IF ITEM-STATUZ              NOT = O-K             <014>
	 ****          MOVE ITEM-PARAMS        TO SYSR-PARAMS         <014>
	 ****          PERFORM 600-FATAL-ERROR                        <014>
	 ****       END-IF                                            <014>
	 ****       MOVE ITEM-GENAREA TO T3678-T3678-REC              <014>
	 ****                                                         <014>
	 ****    END-IF                                               <014>
	 ****    GO TO 1108-CONT.                                     <014>
	 ****  END-IF.                                                <014>
	 * </pre>
	 */
	protected void readContractLongdesc1040()
	{
		/*    Read Contract header Long description  from table T5688 for*/
		/*    the contract type held on CHDRLND.*/
		descpf=descDAO.getdescData("IT", tablesInner.t5688.toString(), chdrlnbIO.getCnttype().toString(), chdrlnbIO.getChdrcoy().toString(), wsspcomn.language.toString());
		if (null == descpf) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		/*MOVE DESC-LONGDESC          TO S6378-CTYPEDES.               */
		sv.cownnum.set(chdrlnbIO.getCownnum());

		retreiveClientDetails1050();
		return;
	}

	protected void retreiveClientDetails1050()
	{
		/*    Read the Client details for the associated Life.*/
		Clntpf clntpfCriteria = new Clntpf();

		clntpfCriteria.setClntpfx("CN");
		clntpfCriteria.setClntcoy(wsspcomn.fsuco.toString());
		clntpfCriteria.setClntnum(chdrlnbIO.getCownnum().toString());

		clntpfData = clntpfDAO.selectActiveClient(clntpfCriteria); //ILIFE-6270
		retreiveClntDtailsCustomerSpecific();
		clientNameFormat1060();
		return;
	}

	protected void clientNameFormat1060()
	{
		/*    Format the Client name extracted. Special Format.*/
		/*    SEE Copy Confname in the procedure division.*/
		if(clntpfData != null){
		if ((clntpfData.getClntnum() == null)
				|| isNE(clntpfData.getValidflag(), 1)) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		  }
		
		else {
			/*        PERFORM PAYEENAME                                        */
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		 }
		}

		/*1070-CALC-SUSPENSE.                                              */
		/*    Code redundant now Suspense is checked for Contract          */
		/*    Currency; Billing Currency or any currency                   */
		/*    Read  the sub account balance for  the contract and apply*/
		/*    the sign for the sub account type.*/
		/*MOVE SPACES                 TO ACBL-PARAMS.                  */
		/* MOVE SPACES                 TO ACBL-DATA-AREA.          <017>*/
		/* MOVE ZEROS                  TO ACBL-RLDGACCT.           <015>*/
		/* MOVE WSSP-COMPANY           TO ACBL-RLDGCOY.            <015>*/
		/* MOVE CHDRLNB-CHDRNUM        TO ACBL-RLDGACCT.           <015>*/
		/* MOVE CHDRLNB-CNTCURR        TO ACBL-ORIGCURR.           <015>*/
		/* MOVE T5645-SACSCODE-01      TO ACBL-SACSCODE.           <015>*/
		/* MOVE T5645-SACSTYPE-01      TO ACBL-SACSTYP.            <015>*/
		/* MOVE READR                  TO ACBL-FUNCTION.           <015>*/
		/*                                                         <015>*/
		/* CALL 'ACBLIO'               USING ACBL-PARAMS.          <015>*/
		/*                                                         <015>*/
		/* IF  ACBL-STATUZ         NOT = O-K                       <015>*/
		/* AND                     NOT = MRNF                      <015>*/
		/*    MOVE ACBL-PARAMS         TO SYSR-PARAMS              <015>*/
		/*    PERFORM 600-FATAL-ERROR.                             <015>*/
		/*MOVE SPACES                 TO SACSLNB-PARAMS.               */
		/*MOVE WSSP-COMPANY           TO SACSLNB-CHDRCOY.              */
		/*MOVE CHDRLNB-CHDRNUM        TO SACSLNB-CHDRNUM.              */
		/*MOVE CHDRLNB-CNTCURR        TO SACSLNB-CNTCURR.              */
		/*MOVE T5645-SACSCODE-01      TO SACSLNB-SACSCODE.             */
		/*MOVE T5645-SACSTYPE-01      TO SACSLNB-SACSTYP.              */
		/*MOVE READR                  TO SACSLNB-FUNCTION.             */
		/*CALL 'SACSLNBIO'            USING SACSLNB-PARAMS.            */
		/*IF SACSLNB-STATUZ           NOT = O-K                        */
		/*                        AND NOT = MRNF                       */
		/*   MOVE SACSLNB-PARAMS      TO SYSR-PARAMS                   */
		/*   PERFORM 600-FATAL-ERROR.                                  */
		/*IF SACSLNB-STATUZ               = MRNF                       */
		/* IF ACBL-STATUZ                  = MRNF                  <015>*/
		/*   MOVE ZERO          TO S6378-CNTSUSP*/
		/*    MOVE ZERO          TO WSAA-CNT-SUSPENSE              <014>*/
		/*    MOVE ZERO          TO WSAA-TOTAL-SUSPENSE            <014>*/
		/* ELSE                                                         */
		/*    IF T3695-SIGN               = '-'                         */
		/*       MULTIPLY SACSLNB-SACSCURBAL BY -1                      */
		/*                             GIVING S6378-CNTSUSP             */
		/*    ELSE                                                      */
		/*       MOVE SACSLNB-SACSCURBAL  TO S6378-CNTSUSP.             */
		/*    IF T3695-SIGN               = '-'                    <014>*/
		/*      MULTIPLY SACSLNB-SACSCURBAL BY -1                 <014>*/
		/*       MULTIPLY ACBL-SACSCURBAL BY -1                    <015>*/
		/*                             GIVING WSAA-CNT-SUSPENSE    <014>*/
		/*       MOVE WSAA-CNT-SUSPENSE   TO WSAA-TOTAL-SUSPENSE   <014>*/
		/*    ELSE                                                 <014>*/
		/*      MOVE SACSLNB-SACSCURBAL  TO WSAA-CNT-SUSPENSE.    <014>*/
		/*      MOVE ACBL-SACSCURBAL     TO WSAA-CNT-SUSPENSE.         */
		/*       MOVE ACBL-SACSCURBAL     TO WSAA-CNT-SUSPENSE     <015>*/
		/*       MOVE WSAA-CNT-SUSPENSE   TO WSAA-TOTAL-SUSPENSE.  <014>*/
		/* Check if a Suspense payment has been made in the Contract       */
		/* Currency, Billing Currency or any currency.                     */
		wsaaSuspInd = "N";	
		acblenqIO.setDataArea(SPACES);
		acblenqIO.setRldgacct(ZERO);
		acblenqIO.setRldgcoy(wsspcomn.company);
		acblenqIO.setRldgacct(chdrlnbIO.getChdrnum());
		if(onePflag) {
			if(!t5645rec.sacscode02.equals(SPACES) || !t5645rec.sacstype02.equals(SPACES)) {
				acblenqIO.setSacscode(t5645rec.sacscode02);
				acblenqIO.setSacstyp(t5645rec.sacstype02);
			}
			else {
				acblenqIO.setSacscode(t5645rec.sacscode01);
				acblenqIO.setSacstyp(t5645rec.sacstype01);
			}
		}
		else{
			acblenqIO.setSacscode(t5645rec.sacscode01);
			acblenqIO.setSacstyp(t5645rec.sacstype01);	
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 3)
				|| isEQ(wsaaSuspInd, "Y")); wsaaSub.add(1)){
			checkSuspense1c100();
		}
		/* Look up Advance premium deposit for available amount            */
		initialize(rlpdlonrec.rec);
		rlpdlonrec.function.set(Varcom.info);
		rlpdlonrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		rlpdlonrec.chdrnum.set(chdrlnbIO.getChdrnum());
		rlpdlonrec.prmdepst.set(ZERO);
		rlpdlonrec.language.set(wsspcomn.language);
		callProgram(Rlpdlon.class, rlpdlonrec.rec);
		if(BTPRO028Permission) {
				String key = chdrlnbIO.getBillchnl().toString().trim() +  chdrlnbIO.getCnttype().toString().trim()+chdrlnbIO.getBillfreq().toString().trim();
				if(!readT6654(key)) {
					key = chdrlnbIO.getBillchnl().toString().trim().concat(chdrlnbIO.getCnttype().toString().trim()).concat("**");
					if(!readT6654(key)) {
						key = chdrlnbIO.getBillchnl().toString().trim().concat("*****");
						if(!readT6654(key)) {
							syserrrec.statuz.set(varcom.mrnf);
							syserrrec.params.set("");
							fatalError600();
						}
					}
				}
			}
			else {
			StringBuilder key = new StringBuilder(chdrlnbIO.getBillchnl().toString().trim());

			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.t6654.toString());
			itempf.setItemitem((key.append(chdrlnbIO.getCnttype().toString().trim())).toString());
			itempf = itempfDAO. getItempfRecord(itempf);
			//ILIFE-8583 : Starts
			if(null == itempf){
				key = new StringBuilder(chdrlnbIO.getBillchnl().toString().trim());
				itempf = new Itempf();
				itempf.setItempfx("IT");
				itempf.setItemcoy(wsspcomn.company.toString());
				itempf.setItemtabl(tablesInner.t6654.toString());
				itempf.setItemitem(key.append("***").toString());
				itempf = itempfDAO. getItempfRecord(itempf);
			}
			//ILIFE-8583 : Ends
			if(null != itempf)
				t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
			if(isEQ(t6654rec.renwdate1,"Y"))
			{
				rlpdlonrec.prmdepst.set(ZERO);
				wsaaTotalPremium.set(ZERO);
			}
		//ILIFE-4128 ends
		if (isNE(rlpdlonrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(rlpdlonrec.statuz);
			syserrrec.params.set(rlpdlonrec.rec);
			fatalError600();
		}
		if (isGT(rlpdlonrec.prmdepst, ZERO)) {
			wsaaSuspInd = "Y";
		}
		clientNameFormatCustomerSpecific();
		calcContractFee1080();
		return;
	}
	
  protected boolean readT6654(String key) {
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t6654.toString());
		itempf.setItemitem(key);
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null != itempf) {
			t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			return true;
		}
		return false;
	}

	protected void calcContractFee1080()
	{
		/*    Read table TR52D using CHDRLNB-REGISTER as key               */

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.tr52d.toString());
		itempf.setItemitem(chdrlnbIO.getRegister().toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (null == itempf) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.tr52d.toString());
			itempf.setItemitem("***");
			itempf = itempfDAO. getItempfRecord(itempf);
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempf.getGenarea()));
		/*    Read the contract definition details T5688 for the contract*/
		/*    type held on the contract header.*/	
		itdmpfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), tablesInner.t5688.toString(), chdrlnbIO.getCnttype().toString(), chdrlnbIO.getOccdate().toInt());

		if(itdmpfList == null || itdmpfList.size() == 0){

			scrnparams.errorCode.set(errorsInner.f290);
		}else {
			t5688rec.t5688Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		}
		/*    Calculate the contract fee by using the subroutine found in*/
		/*    table T5674. See 1200 Section.*/
		if (isNE(t5688rec.feemeth, SPACES)) {
			calcFee1200();
		}
		else {
			mgfeelrec.mgfee.set(ZERO);
			sv.cntfee.set(ZERO);
		}

		calcPremium1090();
		return;
	}

	protected void calcPremium1090()
	{
		List<Covtpf> covtpfList = null;
		/*    Read through the Coverage/Rider transactions COVTLNB.*/
		sv.cntcurr.set(chdrlnbIO.getCntcurr());
		sv.billcurr.set(wsaaBillingInformationInner.wsaaBillcurr[1]);
		/* MOVE CHDRLNB-BILLCURR       TO S6378-BILLCURR.               */
		/* MOVE CHDRLNB-BILLFREQ       TO S6378-BILLFREQ.               */
		/* MOVE CHDRLNB-BILLCHNL       TO S6378-MOP.                    */
		covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		covrValidated = false;
		for(Covtpf covtpf : covtpfList){
			calcPremium1300(covtpf);
			if(isEQ(covtpf.getCrtable(),"HBNF")) {
				hbnfIO.setRecKeyData(SPACES);
				hbnfIO.setChdrcoy(covtpf.getChdrcoy());
				hbnfIO.setChdrnum(covtpf.getChdrnum());
				hbnfIO.setLife(covtpf.getLife());
				hbnfIO.setCoverage(covtpf.getCoverage());
				hbnfIO.setRider(covtpf.getRider());
				hbnfIO.setFunction(varcom.readr);
				callHbnf1950();
				checkWaiver();
			}
		}
		if(isNE(t6654rec.renwdate1,"Y")){
			checkMinMaxLimit1400();
		}
		adjustRegularPrem1100();
		return;
	}

	protected void adjustRegularPrem1100()
	{
		/* Change the premium pro-rata to calculated on a payer            */
		/* by payer basis.                                                 */
		/* If The Calculated Regular Premium is equal to zeros then     */
		/* move zeros to the screen fields.                             */
		/* IF WSAA-REGPREM             = ZERO                           */
		/*    MOVE 0                   TO WSAA-INSTPRM                  */
		/*    GO TO 1105-TOTAL-PREMIUMS.                                */
		/* Get the frequency Factor from DATCON3 for Regular premiums.  */
		/* MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-1.              */
		/* MOVE CHDRLNB-BILLCD         TO DTC3-INT-DATE-2.         <014>*/
		/* MOVE CHDRLNB-BTDATE         TO DTC3-INT-DATE-2.         <014>*/
		/* MOVE CHDRLNB-BILLFREQ       TO DTC3-FREQUENCY.               */
		/* CALL 'DATCON3'              USING DTC3-DATCON3-REC.          */
		/* IF DTC3-STATUZ              NOT = O-K                        */
		/*    MOVE DTC3-STATUZ         TO SYSR-STATUZ                   */
		/*    PERFORM 600-FATAL-ERROR.                                  */
		/* Use the DATCON3 Frequency Factor to calculate the Instate-   */
		/* ment Premium.                                                */
		/* MOVE DTC3-FREQ-FACTOR       TO WSAA-FACTOR.                  */
		/*                                                         <014>*/
		/* We have to be sure that the date's involved are not     <014>*/
		/* problem dates(for details see amendment note)           <014>*/
		/*                                                         <014>*/
		/* MOVE CHDRLNB-OCCDATE         TO WSAA-PREM-DATE1.        <014>*/
		/* MOVE CHDRLNB-BILLCD          TO WSAA-PREM-DATE2.        <014>*/
		/*                                                         <014>*/
		/* PERFORM 1900-CHECK-FREQ-DATE.                           <014>*/
		/*                                                         <014>*/
		/* MULTIPLY WSAA-REGPREM       BY WSAA-FACTOR                   */
		/*                             GIVING WSAA-INSTPRM.             */
		/*1105-TOTAL-PREMIUMS.                                             */
		/* ADD WSAA-SINGP              TO WSAA-INSTPRM.                 */
		/* MOVE WSAA-INSTPRM           TO S6378-INST-PREM.              */
		/* MOVE S6378-CNTFEE           TO WSAA-CNTFEE.                  */
		/* IF WSAA-INSTPRM             = 0                              */
		/*    MOVE 0                   TO S6378-CNTFEE                  */
		/* ELSE                                                         */
		/*    MULTIPLY S6378-CNTFEE    BY WSAA-FACTOR                   */
		/*                             GIVING S6378-CNTFEE.             */
		/*Check that the premium amount + the contract fee amount         */
		/*is less than the contract suspense amount paid.                 */
		/* ADD S6378-CNTFEE             TO WSAA-INSTPRM.                */
		/* IF WSAA-INSTPRM        >       S6378-CNTSUSP                 */
		/*                                                         <014>*/
		/* IF WSAA-INSTPRM        NOT >   S6378-CNTSUSP            <014>*/
		/*    GO TO 1106-CONT.                                     <014>*/
		/*  Look up tolerance applicable                                   */
		/*  (code removed as Tolerance details are obtained in Section     */
		/*  1C200-SUSPENCE-AMOUNT for appropriate suspense currency).      */
		/* MOVE SPACES                 TO ITEM-DATA-KEY.           <003>*/
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.            <003>*/
		/* MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.            <003>*/
		/* MOVE T5667                  TO ITEM-ITEMTABL.           <003>*/
		/* MOVE WSKY-BATC-BATCTRCDE    TO WSAA-T5667-TRANCD.       <003>*/
		/* MOVE CHDRLNB-CNTCURR        TO WSAA-T5667-CURR.         <003>*/
		/* MOVE WSAA-T5667-KEY         TO ITEM-ITEMITEM.           <003>*/
		/* MOVE 'READR'                TO ITEM-FUNCTION.           <003>*/
		/* CALL 'ITEMIO' USING ITEM-PARAMS.                        <003>*/
		/* IF ITEM-STATUZ              NOT = O-K                   <003>*/
		/*                         AND NOT = MRNF                  <003>*/
		/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS              <003>*/
		/*     PERFORM 600-FATAL-ERROR.                            <003>*/
		/* IF ITEM-STATUZ              = MRNF                      <014>*/
		/*    MOVE              ZERO TO WSAA-TOLERANCE-APP,        <014>*/
		/*                              WSAA-AMOUNT-LIMIT.         <014>*/
		/*    GO TO 1105B-CALC-AMOUNT-AVAILABLE.                   <014>*/
		/*                                                         <014>*/
		/* MOVE ITEM-GENAREA TO T5667-T5667-REC.                   <014>*/
		/* IF ITEM-STATUZ              = MRNF                      <014>*/
		/*    MOVE              ZERO TO WSAA-TOLERANCE-APP,        <014>*/
		/*                              WSAA-AMOUNT-LIMIT          <014>*/
		/* ELSE                                                    <014>*/
		/*    MOVE ITEM-GENAREA TO T5667-T5667-REC.                <014>*/
		/* PERFORM 1A00-SEARCH-FOR-TOLERANCE VARYING WSAA-SUB      <014>*/
		/*                                   FROM 1 BY 1           <014>*/
		/*                                   UNTIL WSAA-SUB > 11  OR014>*/
		/*                                         CHDRLNB-BILLFREQ<=14>*/
		/*                                         T5667-FREQ(WSAA-SUB).*/
		/*                                                         <014>*/
		/* IF WSAA-SUB > 11                                        <014>*/
		/*    MOVE              ZERO TO WSAA-TOLERANCE-APP,        <014>*/
		/*                              WSAA-AMOUNT-LIMIT          <014>*/
		/*    GO TO 1105B-CALC-AMOUNT-AVAILABLE.                   <014>*/
		/*                                                         <014>*/
		/*1105A-CALC-TOLERANCE.                                       <014>*/
		/*                                                         <014>*/
		/* COMPUTE WSAA-CALC-TOLERANCE =                           <014>*/
		/*  (T5667-PRMTOL(WSAA-SUB) * S6378-INST-PREM) / 100.      <014>*/
		/*                                                         <014>*/
		/* Check % amount is less than Limit on T5667, if so use this14>*/
		/* else use Limit.                                         <014>*/
		/*                                                         <014>*/
		/* IF WSAA-CALC-TOLERANCE      < T5667-MAX-AMOUNT(WSAA-SUB)<014>*/
		/*    MOVE WSAA-CALC-TOLERANCE TO WSAA-TOLERANCE           <014>*/
		/* ELSE                                                    <014>*/
		/*    MOVE T5667-MAX-AMOUNT(WSAA-SUB) TO WSAA-TOLERANCE.   <014>*/
		/*                                                         <014>*/
		/* COMPUTE WSAA-DIFF        = WSAA-INSTPRM - S6378-CNTSUSP.<014>*/
		/* IF WSAA-DIFF                < WSAA-TOLERANCE            <014>*/
		/*    MOVE WSAA-DIFF           TO WSAA-TOLERANCE.          <014>*/
		/*                                                         <014>*/
		/*1105B-CALC-AMOUNT-AVAILABLE.                                <014>*/
		/*                                                         <014>*/
		/*alculate Total amount AVAILABLE                          <014>*/
		/*                                                         <014>*/
		/* COMPUTE WSAA-TOTAMNT = S6378-CNTSUSP                    <014>*/
		/*        + WSAA-TOLERANCE.                                <014>*/
		/*                                                         <014>*/
		/*Check if the amount required is greater than the amount   <014>*/
		/* available - if this is the case the contract cannot be  <014>*/
		/* issued.                                                 <014>*/
		/*                                                         <014>*/
		/* IF WSAA-INSTPRM              > WSAA-TOTAMNT             <014>*/
		/*    MOVE SPACES               TO ERMS-ERRMESG-REC             */
		/*    MOVE U026                 TO ERMS-EROR                    */
		/*    MOVE SPACES               TO S6378-LIFE                   */
		/*                                 S6378-JLIFE                  */
		/*                                 S6378-COVERAGE               */
		/*                                 S6378-RIDER                  */
		/*    PERFORM 1800-ERROR-MESSAGES.                              */
		/* Read the RTRN file to see if a cash receipt has been*/
		/* created for this contract.*/
		rtrnsacIO.setRldgcoy(wsspcomn.company);
		rtrnsacIO.setRldgacct(chdrlnbIO.getChdrnum());
		/* MOVE CHDRLNB-CNTCURR        TO RTRNSAC-ORIGCCY.         <014>*/
		if (isEQ(wsaaSuspInd, "Y")) {
			rtrnsacIO.setOrigccy(acblenqIO.getOrigcurr());
		}
		else {
			rtrnsacIO.setOrigccy(SPACES);
		}
		rtrnsacIO.setSacscode(t5645rec.sacscode01);
		rtrnsacIO.setSacstyp(t5645rec.sacstype01);
		rtrnsacIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, rtrnsacIO);
		if (isNE(rtrnsacIO.getStatuz(), Varcom.oK)
				&& isNE(rtrnsacIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(rtrnsacIO.getParams());
			fatalError600();
		}
		if (isEQ(rtrnsacIO.getStatuz(), Varcom.mrnf)) {
			rtrnsacIO.setEffdate(varcom.vrcmMaxDate);
		}
		/* Look up the tax relief method on T5688.                         */
		itdmpf = new Itempf();
		itdmpfList = itemDAO.getItdmByFrmdate(chdrlnbIO.getChdrcoy().toString(), tablesInner.t5688.toString(), chdrlnbIO.getCnttype().toString(), chdrlnbIO.getOccdate().toInt());

		if(itdmpfList != null && itdmpfList.size() !=0 ){
			itdmpf = itdmpfList.get(0);
		}

		t5688rec.t5688Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));
		/* Look up the subroutine on T6687.                                */


		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t6687.toString());
		itempf.setItemitem(t5688rec.taxrelmth.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (null != itempf) {
			t6687rec.t6687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Adjust the regular premiums (if not zero) by the number         */
		/* of frequencies required prior to issue.                         */
		/* If the contract has a tax relief method deduct the tax          */
		/* relief amount from the amount due.                              */
		/* Also check if there is enough money in suspense to issue        */
		/* this contract.                                                  */
		wsbbSub.set(1);
		while ( !(isGT(wsbbSub, 9)
				|| isEQ(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()], SPACES))) {
			adjustPremium1c00();
		}

		/* MOVE WSAA-TOTAL-PREMIUM     TO S6378-INST-PREM.         <014>*/
		/* MOVE WSAA-TOTAL-SUSPENSE    TO S6378-CNTSUSP.           <014>*/
		sv.instPrem.set(wsaaTotalPremium);
		sv.prmdepst.set(rlpdlonrec.prmdepst);
		/* If Suspense payment found, retrieve appropriate Tolerance       */
		/* details & check Suspense Amount is within Tolerance range.      */
		/* Otherwise check if Suspense is required, (initial Premium       */
		/* > zero), before displaying error.                               */
		suspenseAmount1c200();
		
		if (isEQ(wsaaSuspInd, "Y")  ||( "MO" .equals(wsspcomn.undAction.toString()) ) ) {

			checkAgentTerminate1c300();
			
			if(isEQ(wsaaInsufficientSuspense,"Y")) {
                if(isEQ(wsaaTotalSuspense,ZERO)) {
        			chdrlnbIO.setPstatcode(wsbbNRStat);
                                   }
                else {
                    chdrlnbIO.setPstatcode(wsbbPRStat);
                }
            } else {
                chdrlnbIO.setPstatcode(wsbbMRStat);
            }
		}
		else {
			 chdrlnbIO.setPstatcode(wsbbPSStat);
			if (isGT(wsaaTotalPremium, ZERO)) {
				wsaaInsufficientSuspense = "Y";
			}
		}
		String t6A0 = "T6A0";
		String tbgd = "TBGD";
		if (isNE(scrnparams.deviceInd, "*RMT") 
				&& isEQ(wsaaInsufficientSuspense, "Y")) {
		
			if(onePflag && !t6A0.equals(wsaaBatckey.batcBatctrcde.toString()) //IBPLIFE-2274
					&& !tbgd.equals(wsaaBatckey.batcBatctrcde.toString())){
				errmesgrec.errmesgRec.set(SPACES);
				/*     MOVE U026                 TO ERMS-EROR                    */
				errmesgrec.eror.set(errorsInner.h030);
				sv.life.set(SPACES);
				sv.jlife.set(SPACES);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(0);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
			if(!onePflag && isNE(t6654rec.renwdate1,"Y") && !CTISS014Permission){ //PINNACLE-2696
				errmesgrec.errmesgRec.set(SPACES);
				/*     MOVE U026                 TO ERMS-EROR                    */
				errmesgrec.eror.set(errorsInner.h030);
				sv.life.set(SPACES);
				sv.jlife.set(SPACES);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(0);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
		}
		//IBPLIFE-3145 Starts
		if(onePflag && isEQ(wsaaBatckey.batcBatctrcde,"T642") && isEQ(chdrlnbIO.getBillchnl(),'D')
				&& isEQ(t3615rec.onepcashless,"Y") && isNE(chdrlnbIO.getRcopt(),"Y")
				&& isNE(chdrlnbIO.getStatcode(),"SI")) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.rum8);
			sv.life.set(SPACES);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			sv.payrseqno.set(0);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		//IBPLIFE-3145 End
		/*  Check that the owner is alive.  
		 *                             */
		if(clntpfData != null){
		if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)
				&& isGT(chdrlnbIO.getOccdate(), clntpfData.getCltdod())) {
			errmesgrec.errmesgRec.set(SPACES);
			/*     MOVE F782               TO ERMS-EROR             <A06596>*/
			errmesgrec.eror.set(errorsInner.w343);
			sv.life.set(SPACES);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			sv.payrseqno.set(ZERO);
			wsaaExtraMsgpfx.set("OW");
			errorMessages1800();
		}
		}
		a2000CheckDeathDate();
		/*1106-CONT.                                                       */
		/*MOVE SPACES                 TO ITEM-DATA-KEY.           <014>*/
		/*MOVE 'IT'                   TO ITEM-ITEMPFX.            <014>*/
		/*MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.            <014>*/
		/*MOVE T3625                  TO ITEM-ITEMTABL.           <014>*/
		/*MOVE 'LP'                   TO WSAA-T3625-LP.           <014>*/
		/*MOVE CHDRLNB-BILLCHNL       TO WSAA-T3625-BILLCHNL.     <014>*/
		/*MOVE CHDRLNB-BILLFREQ       TO WSAA-T3625-BILLFREQ.     <014>*/
		/*MOVE WSAA-T3625-KEY         TO ITEM-ITEMITEM.           <014>*/
		/*MOVE 'READR'                TO ITEM-FUNCTION.           <014>*/
		/*                                                         <014>*/
		/*CALL 'ITEMIO' USING ITEM-PARAMS.                        <014>*/
		/*IF ITEM-STATUZ              NOT = O-K                   <014>*/
		/*                   AND NOT = MRNF                  <014>*/
		/*MOVE ITEM-STATUZ        TO SYSR-STATUZ              <014>*/
		/*PERFORM 600-FATAL-ERROR.                            <014>*/
		/*                                                         <014>*/
		/*IF ITEM-STATUZ              = MRNF                      <014>*/
		/*MOVE SPACES              TO ERMS-ERRMESG-REC         <014>*/
		/*MOVE U023                TO ERMS-EROR                <014>*/
		/*PERFORM 1800-ERROR-MESSAGES.                         <014>*/
		/*                                                         <014>*/
		/*MOVE ITEM-GENAREA           TO T3625-T3625-REC.         <014>*/
		/*                                                         <014>*/
		/*For Direct Debit, check details exist on CHDR              <014>*/
		/*                                                         <014>*/
		/* IF T3625-DDIND              = 'Y'                       <014>*/
		/* IF CHDRLNB-BILLCHNL         = 'B'                       <014>*/
		/*    IF CHDRLNB-BANKKEY       = SPACES OR                 <014>*/
		/*       CHDRLNB-BANKACCKEY    = SPACES                    <014>*/
		/*    IF CHDRLNB-MANDREF       = SPACES                    <014>*/
		/*       MOVE SPACES           TO ERMS-ERRMESG-REC         <014>*/
		/*       MOVE F826             TO ERMS-EROR                <014>*/
		/*       PERFORM 1800-ERROR-MESSAGES.                      <014>*/
		/*       GO TO 1108-CONT.                                  <014>*/
		/*                                                         <014>*/
		/*                                                         <014>*/
		/* IF T3625-DDIND            NOT  = 'Y'                    <014>*/
		/* IF CHDRLNB-BILLCHNL       NOT = 'B'                     <014>*/
		/*    GO TO 1107-CHECK-GROUP.                              <014>*/
		/*For non Direct Debit, check details don't exist on CHDR    <014>*/
		/*                                                         <014>*/
		/* IF T3625-DDIND         NOT  = 'Y'                       <014>*/
		/* IF CHDRLNB-BILLCHNL    NOT  = 'B'                       <014>*/
		/*    IF CLDRLNB-BANKKEY       NOT = SPACES OR             <014>*/
		/*       CHDRLNB-BANKACCKEY    NOT = SPACES                <014>*/
		/*    IF CHDRLNB-MANDREF       NOT = SPACES                <014>*/
		/*       MOVE SPACES           TO ERMS-ERRMESG-REC         <014>*/
		/*       MOVE I005             TO ERMS-EROR                <014>*/
		/*       PERFORM 1800-ERROR-MESSAGES                       <014>*/
		/*       GO TO 1108-CONT                                   <014>*/
		/*    ELSE                                                 <014>*/
		/*       GO TO 1108-CONT.                                  <014>*/
		/*1106A-VALIDATE-MANDATE.                                    <014>*/
		/*                                                         <014>*/
		/*Validate the mandate record against the contract header    <014>*/
		/*and the client bank account record.                        <014>*/
		/*                                                         <014>*/
		/* IF CLBL-STATUZ = O-K AND MANDLNB-STATUZ = O-K           <014>*/
		/*      IF CHDRLNB-BILLCD < MANDLNB-EFFDATE                <014>*/
		/*         MOVE SPACES              TO ERMS-ERRMESG-REC    <014>*/
		/*         MOVE I008                TO ERMS-EROR           <014>*/
		/*         PERFORM 1800-ERROR-MESSAGES                     <014>*/
		/*         END-IF                                          <014>*/
		/*                                                         <014>*/
		/*      IF  MANDLNB-MAND-AMT NOT = 0                       <014>*/
		/*       IF MANDLNB-MAND-AMT NOT = WSAA-REGPREM + WSAA-CNTFEE14>*/
		/*          MOVE SPACES              TO ERMS-ERRMESG-REC   <014>*/
		/*          MOVE I009                TO ERMS-EROR          <014>*/
		/*          PERFORM 1800-ERROR-MESSAGES                    <014>*/
		/*      END-IF                                             <014>*/
		/*     END-IF                                              <014>*/
		/*     IF CHDRLNB-PAYRNUM = SPACES                         <014>*/
		/*       MOVE CHDRLNB-COWNNUM        TO WSAA-PAYRNUM       <014>*/
		/*     ELSE                                                <014>*/
		/*        MOVE CHDRLNB-PAYRNUM       TO WSAA-PAYRNUM       <014>*/
		/*     END-IF                                              <014>*/
		/*     IF WSAA-PAYRNUM NOT = CLBL-CLNTNUM                  <014>*/
		/*       MOVE SPACES                 TO ERMS-ERRMESG-REC   <014>*/
		/*       MOVE I004                   TO ERMS-EROR          <014>*/
		/*       PERFORM 1800-ERROR-MESSAGES                       <014>*/
		/*     END-IF                                              <014>*/
		/*                                                         <014>*/
		/*     IF CHDRLNB-BILLCURR NOT = CLBL-CURRCODE             <014>*/
		/*        MOVE SPACES                TO ERMS-ERRMESG-REC   <014>*/
		/*        MOVE I010                  TO ERMS-EROR          <014>*/
		/*        PERFORM 1800-ERROR-MESSAGES                      <014>*/
		/*     END-IF                                              <014>*/
		/*                                                         <014>*/
		/*     IF T3678-GONOGOFLG = 'N'                            <014>*/
		/*        MOVE SPACES                TO ERMS-ERRMESG-REC   <014>*/
		/*        MOVE I014                  TO ERMS-EROR          <014>*/
		/*        PERFORM 1800-ERROR-MESSAGES                      <014>*/
		/*     END-IF                                              <014>*/
		/*                                                         <014>*/
		/*If*the Current From date of the bank account is greater than<the>*/
		/*effective date of the mandate (i.e. the bank account is not yet4>*/
		/*effective) then display an error message.                   <014>*/
		/*                                                         <014>*/
		/*                                                         <014>*/
		/*     IF CLBL-CURRFROM            > MANDLNB-EFFDATE       <014>*/
		/*        MOVE SPACES              TO ERMS-ERRMESG-REC     <014>*/
		/*        MOVE I020                TO ERMS-EROR            <014>*/
		/*        PERFORM 1800-ERROR-MESSAGES                      <014>*/
		/*     END-IF                                              <014>*/
		/*                                                         <014>*/
		/*If*the Current To date of the bank account is less than the <014>*/
		/*effective date of the mandate (i.e. the bank account is no  <014>*/
		/*longer in force) then display an error message.             <014>*/
		/*                                                         <014>*/
		/*     IF CLBL-CURRTO          NOT = ZEROES                <014>*/
		/*        IF CLBL-CURRTO           < MANDLNB-EFFDATE       <014>*/
		/*           MOVE SPACES           TO ERMS-ERRMESG-REC     <014>*/
		/*           MOVE I020             TO ERMS-EROR            <014>*/
		/*           PERFORM 1800-ERROR-MESSAGES                   <014>*/
		/*        END-IF                                           <014>*/
		/*     END-IF                                              <014>*/
		/* END-IF.                                                 <014>*/
		/*                                                         <014>*/
		/*1107-CHECK-GROUP.                                           <014>*/
		/*                                                         <014>*/
		/*For Group Billing, check details exist on CHDR             <014>*/
		/*                                                         <014>*/
		/* IF T3625-GRPIND             = 'Y' AND                   <014>*/
		/* IF CHDRLNB-BILLCHNL         = 'G' AND                   <014>*/
		/*    CHDRLNB-GRUPKEY          = SPACES                    <014>*/
		/*    MOVE SPACES              TO ERMS-ERRMESG-REC         <014>*/
		/*    MOVE E714                TO ERMS-EROR                <014>*/
		/*    PERFORM 1800-ERROR-MESSAGES                          <014>*/
		/*    GO TO 1108-CONT.                                     <014>*/
		/*                                                         <014>*/
		/*For non Group Billing, check details don't exist on CHDR   <014>*/
		/*                                                         <014>*/
		/* IF T3625-GRPIND        NOT  = 'Y' AND                   <014>*/
		/* IF CHDRLNB-BILLCHNL         NOT = 'G' AND               <014>*/
		/*    CHDRLNB-GRUPKEY          NOT = SPACES                <014>*/
		/*    MOVE SPACES              TO ERMS-ERRMESG-REC         <014>*/
		/*    MOVE E571                TO ERMS-EROR                <014>*/
		/*    PERFORM 1800-ERROR-MESSAGES                          <014>*/
		/*    GO TO 1108-CONT.                                     <014>*/
		/*                                                         <014>*/
		/* IF T3625-GRPIND        NOT  = 'Y'                       <014>*/
		/* IF CHDRLNB-BILLCHNL         NOT = 'G'                   <014>*/
		/*    GO TO 1108-CONT.                                     <014>*/
		/*                                                         <014>*/
		/*Read Group Details.                                        <014>*/
		/*                                                         <014>*/
		/* MOVE CHDRLNB-CHDRCOY        TO GRPS-GRUPCOY.            <014>*/
		/* MOVE CHDRLNB-GRUPKEY        TO GRPS-GRUPNUM.            <014>*/
		/*                                                         <014>*/
		/* MOVE READR                  TO GRPS-FUNCTION.           <014>*/
		/* CALL 'GRPSIO' USING         GRPS-PARAMS.                <014>*/
		/* IF GRPS-STATUZ              NOT = O-K AND               <014>*/
		/*                             NOT = MRNF                  <014>*/
		/*     MOVE GRPS-PARAMS        TO SYSR-PARAMS              <014>*/
		/*     MOVE GRPS-STATUZ        TO SYSR-STATUZ              <014>*/
		/*     PERFORM 600-FATAL-ERROR.                            <014>*/
		/*                                                         <014>*/
		/*For Group Billing, check details exist.                    <014>*/
		/*                                                         <014>*/
		/* IF GRPS-STATUZ              = MRNF                      <014>*/
		/*                                                         <014>*/
		/*    MOVE SPACES              TO ERMS-ERRMESG-REC         <014>*/
		/*    MOVE E714                TO ERMS-EROR                <014>*/
		/*    PERFORM 1800-ERROR-MESSAGES.                         <014>*/
		/*                                                         <014>*/
		/* All the billing details will now be validated on a payer        */
		/* by payer basis using the information on the payer records.      */
		wsbbSub.set(1);
		while ( !(isGT(wsbbSub, 9)
				|| isEQ(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()], SPACES))) {
			payerValidation1d00();
		}

		wsaaAgentSuspend = "N";

		covtpf = new Covtpf();
		covtpfList = covtpfDAO.searchCovtRecordByCoyNumDescUniquNo(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		callCovttrmio1101();
		return;
	}

	protected void callCovttrmio1101()
	{

		if (covtpfList == null || covtpfList.size() == 0) {
			//goTo(GotoLabel.check1101);
			check1101();
			return;
		}

		Iterator<Covtpf> itr = covtpfList.iterator();


		while(itr.hasNext()){
			covtpf = itr.next();
			Itempf itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItmfrm(new BigDecimal(chdrlnbIO.getOccdate().toString()));
			itempf.setItmto(new BigDecimal(chdrlnbIO.getOccdate().toString()));
			itempf.setItemtabl(tablesInner.tr517.toString());
			itempf.setItemitem(covtpf.getCrtable());
			itempf.setValidflag("1");
			List<Itempf> itempfList = itemDAO.findByItemDates(itempf);
				

			if(itempfList != null && itempfList.size() != 0){

				tr517rec.tr517Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));

				calcWaiveSumins5200(covtpf); //ILIFE-5221 by dpuhawan
				List<String> tr517List = null;
				tr517List = itemDAO.findItemByTable("IT", "2", "TR517");
				tr517List.replaceAll(String::trim);
				
				if (isEQ(tr517rec.zrwvflg03, "Y") 
						&& (CMRPY012Permission || tr517List.contains(covtpf.getCrtable()))){//ILIFE-5171 ILIFE-8299 			
					wsaaWaiveSumins.add(mgfeelrec.mgfee);
					wsaaWaiveSumins.add(wsaaWavCntfeeTax);								
				}
				if (isNE(wsaaWaiveSumins, covtpf.getSumins()))//ILIFE-5171
				{
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.rl06);
					sv.life.set(covtpf.getLife());
					sv.jlife.set(covtpf.getJlife());
					sv.coverage.set(covtpf.getCoverage());
					sv.rider.set(covtpf.getRider());
					sv.payrseqno.set(0);
					wsaaExtraMsgpfx.set(SPACES);
					errorMessages1800();
				}
			}
			/* Check that all of the agents linked to the contract are      */
			/* active.                                                      */
			if(!japanLocPermission) {
				checkAgents1f00();
			}
			
		
		}
		
		if(japanLocPermission) {
			checkAgents1f00();
		}
		check1101();
		return;

	}

	protected void check1101()
	{
		/* Initialise work fields.                                         */
		wsaaRiskCessDate.set(ZERO);
		wsaaPremCessDate.set(ZERO);
		wsaaCovtCoverage.set(SPACES);
		wsaaCovtLife.set(SPACES);
		/* Read COVT and check for risk-cess-date/premium-cess-date.       */
		/* As a policy can have more than 1 Coverage, read through         */
		/* all COVT records in order to valid all risk/premium cessation   */
		/* dates.                                                          */

		readCovt1102();
		return;
	}

	protected void readCovt1102()
	{
		

		iterator =   covttrmList.iterator();
		
		
		//if(iterator.hasNext())
			//covttrm = iterator.next();
		//else
			//wsaaEof = true;
		/*ILIFE-8192 STARTS*/
		if(iterator.hasNext() == false)
		{
			wsaaEof = true;
		}
		else {
			while (iterator.hasNext())
			{
				covttrm = iterator.next();
			}
		}
		
		/*ILIFE-8192 ENDS*/

		if(wsaaEof == true){
			//goTo(GotoLabel.cont1108);
			cont1108();
			return;	
		}

		/* Else, save Coverage details.                                    */
		wsaaRiskCessDate.set(covttrm.getRcesdte());
		wsaaPremCessDate.set(covttrm.getPcesdte());
		wsaaCovtCoverage.set(covttrm.getCoverage());
		wsaaCovtLife.set(covttrm.getLife());

		readRider1103();
		return;

	}

	protected void readRider1103()
	{
		/* Start reading Rider records.                                    */
		/* If no Rider found or key breaks, go to next step.               */

		if(iterator.hasNext()){
			covttrm = iterator.next();
		}
		else
		{wsaaEof = true;}

		if (wsaaEof == true){
			//goTo(GotoLabel.cont1108);
			cont1108();
			return;
		}
		/*                                                         <023>*/
		/*If new Coverage read, move Coverage as key.                <023>*/
		/*                                                         <023>*/
		/* IF COVTTRM-COVERAGE         NOT = WSAA-COVT-COVERAGE    <023>*/
		/*    MOVE CHDRLNB-CHDRCOY     TO COVTTRM-CHDRCOY          <023>*/
		/*    MOVE CHDRLNB-CHDRNUM     TO COVTTRM-CHDRNUM          <023>*/
		/*    MOVE '01'                TO COVTTRM-LIFE             <023>*/
		/*    MOVE COVTTRM-COVERAGE    TO WSAA-COVT-COVERAGE       <023>*/
		/*    MOVE '00'                TO COVTTRM-RIDER            <023>*/
		/*    MOVE ZEROES              TO COVTTRM-SEQNBR           <023>*/
		/*    GO TO 1102-READ-COVT                                 <023>*/
		/* END-IF.                                                 <023>*/
		/* Check if the Coverage or Life has changed                       */
		if (isNE(covttrm.getLife(), wsaaCovtLife)
				|| isNE(covttrm.getCoverage(), wsaaCovtCoverage)) {
			covttrm.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
			covttrm.setChdrnum(chdrlnbIO.getChdrnum().toString());
			wsaaCovtLife.set(covttrm.getLife());
			wsaaCovtCoverage.set(covttrm.getCoverage());
			covttrm.setRider("00");
			covttrm.setSeqnbr(0);
			//goTo(GotoLabel.readCovt1102);
			//ILIFE-6419  Start
			//readCovt1102();
			//ILIFE-6419  End
			return;
		}
		/* Else, compare Coverage/Rider cessation dates.                   */
		/* Check table T6640, new indicator field has been created on*/
		/* this table to allow a rider risk cession date > main covr's*/
		readT66405100();
		/* IF COVTTRM-RISK-CESS-DATE   > WSAA-RISK-CESS-DATE OR         */
		/*    COVTTRM-PREM-CESS-DATE   > WSAA-PREM-CESS-DATE            */
		if ((isGT(covttrm.getRcesdte(), wsaaRiskCessDate)
				|| isGT(covttrm.getPcesdte(), wsaaPremCessDate))
				&& (isNE(t6640rec.zrmandind, "Y"))) {
			sv.coverage.set(covttrm.getCoverage());
			sv.life.set(covttrm.getLife());
			sv.rider.set(covttrm.getRider());
			sv.payrseqno.set(0);
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.g161);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		/* Read next Rider.                                                */
		cont1108();
		return ;
	}

	protected void cont1108()
	{
		List<Bnfypf> bnfypfList = null;
		boolean isBnfyListValid = true;
		int bnfypfListSize = 0;
		/* Ensure that there are no unprocessed UTRN records.              */
		utrnrnlIO.setParams(SPACES);
		utrnrnlIO.setChdrnum(chdrlnbIO.getChdrnum());
		utrnrnlIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		utrnrnlIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, utrnrnlIO);
		if (isNE(utrnrnlIO.getStatuz(), Varcom.oK)
				&& isNE(utrnrnlIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(utrnrnlIO.getParams());
			fatalError600();
		}
		/* If unprocessed UTRN record exists, display error message.       */
		if (isNE(utrnrnlIO.getStatuz(), Varcom.mrnf)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.i030);
			sv.life.set(SPACES);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			sv.payrseqno.set(0);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		/* Ensure that there are no unprocessed HITR records.              */
		hitrrnlIO.setParams(SPACES);
		hitrrnlIO.setChdrnum(chdrlnbIO.getChdrnum());
		hitrrnlIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hitrrnlIO.setFormat(formatsInner.hitrrnlrec);
		hitrrnlIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, hitrrnlIO);
		if (isNE(hitrrnlIO.getStatuz(), Varcom.oK)
				&& isNE(hitrrnlIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(hitrrnlIO.getParams());
			fatalError600();
		}
		/* If unprocessed HITR record exists, display error message        */
		if (isNE(hitrrnlIO.getStatuz(), Varcom.mrnf)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.hl08);
			sv.life.set(SPACES);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			sv.payrseqno.set(0);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		wsaaMandatorys.set(SPACES);
		wsaaBnytypes.set(SPACES);
		wsaaIwc.set(ZERO);
		bnfypfList = bnfypfDAO.searchBnfypfRecord(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
		bnfypfListSize = bnfypfList.size();

		for(int index =0; (index < bnfypfListSize) && isBnfyListValid; index++){

			Bnfypf bnfypf = bnfypfList.get(index);
			readForBnfy1400(bnfypf, isBnfyListValid);
		}

		/*-- for every mandatory beneficiary defined (TR52Z)               */
		/*       check if there is a matching beneficiary                  */
		mandatoryBnfy1h00();
		wsaaItc += inspectTallyAll(wsaaMandatorys, "N");
		if (wsaaItc > 0) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.rfk2);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}

		lifeValidation1110();
		return;
	}

	/**
	 * <pre>
	 **** PERFORM 1400-READ-FOR-BNFY.                          <LA5070>
	 * First of all determine whether underwriting is required for the
	 * product type by reading TR675
	 * </pre>
	 */
	protected void lifeValidation1110()
	{
		List<Lifepf> lifepfList = null;
		List<Fluppf> fluppfList = null;

		lifepfList = lifepfDAO.searchLifeRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		lifeLevel1600(lifepfList);
		if(lincFlag)
			performLincValidations();
		/*  But are any of them outstanding?                               */
		wsaaOutflupFound.set(SPACES);
		wsaaDocReqd.set(SPACES);
		fluppfList = fluppfDAO.searchFlupRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

		if(ListUtils.isNotEmpty(fluppfList)){

			for(Fluppf fluppf : fluppfList){

				if (isNE(fluppf.getChdrcoy(), chdrlnbIO.getChdrcoy())
						|| isNE(fluppf.getChdrnum(), chdrlnbIO.getChdrnum())) {
					wsaaOutstanflu = "N";
					//goTo(GotoLabel.compValidation1120);
					compValidation1120();
					return;
				}
				/*  But are any of them outstanding?                               */
				wsaaOutflupFound.set(SPACES);
				wsaaDocReqd.set(SPACES);
				checkFollowUps1500(fluppf);
			}

			if (isEQ(wsaaOutflupFound, "Y")) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.h017);
				sv.life.set(SPACES);
				sv.jlife.set(SPACES);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(0);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();

			}
			if (isEQ(wsaaDocReqd, "Y")
					&& isEQ(hpadIO.getZdoctor(), SPACES)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.hl42);
				sv.life.set(SPACES);
				sv.jlife.set(SPACES);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(ZERO);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
			//}
		}
		compValidation1120();
		return;
	}

	protected void compValidation1120()
	{
		/* Check the component level validation.*/
		List<Covtpf> covtpfList = null;

		/* Check the component level validation.*/
		covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

		if (ListUtils.isEmpty(covtpfList)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.g066);
			sv.life.set(SPACES);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			sv.payrseqno.set(0);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		for(Covtpf covtpf : covtpfList){
			componentLevel1700(covtpf);
		}
		/* ILIFE-2472-START*/
		/*validPayoutClt();
		if (isEQ(wsaaVldPayClt, "N")) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.rfsl);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}*/
		/* ILIFE-2472-END */
		if (!ListUtils.isEmpty(covtpfList)) {
			a1700CrossCheckProduct();
			extendedValidation();
		}
		/**
		 * Agent Sales License pre issue validation  
		 */
		isLicenseFields = FeaConfg.isFeatureExist(aglfIO.getAgntcoy().toString().trim(), "AGMTN014", appVars, "IT");//BSD-ICIL-6
		
		if(isLicenseFields){
			agentLicenseCheck();	
		}
		
		return;
	}

	/* ILIFE-2472-START */
	protected void validPayoutClt() {
		wsaaVldPayClt.set("N");

		if (isEQ(chdrlnbIO.getPayclt(), SPACE)) {
			wsaaVldPayClt.set("Y");
			return;
		}

		if (isEQ(chdrlnbIO.getPayclt(),chdrlnbIO.getCownnum())) {
			wsaaVldPayClt.set("Y");
			return;
		}

		if (isEQ(chdrlnbIO.getPayclt(),wsaa1Lifcnum)) {
			wsaaVldPayClt.set("Y");
			return;
		}

		if (isEQ(chdrlnbIO.getPayclt(),wsaaBillingInformationInner.wsaaClntnum[01])) {
			wsaaVldPayClt.set("Y");
			return;
		}

		if (isEQ(wsaaAsgnPayClt, "Y")) {
			wsaaVldPayClt.set("Y");
			return;
		}

	}
	/* ILIFE-2472-END */
	protected void callCsncalc1150(Lifepf lifepf){
		call1151(lifepf);
	}

	protected void call1151(Lifepf lifepf){
		csncalcrec.csncalcRec.set(SPACES);
		csncalcrec.function.set("COVT");
		csncalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		csncalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		/* MOVE '01'                    TO CSNC-LIFE.           <CAS1.0>*/
		csncalcrec.life.set(lifepf.getLife());
		csncalcrec.cnttype.set(chdrlnbIO.getCnttype());
		csncalcrec.currency.set(chdrlnbIO.getCntcurr());
		csncalcrec.fsuco.set(wsspcomn.fsuco);
		csncalcrec.language.set(wsspcomn.language);
		csncalcrec.incrAmt.set(ZERO);
		csncalcrec.planSuffix.set(ZERO);
		csncalcrec.effdate.set(chdrlnbIO.getOccdate());
		csncalcrec.tranno.set(chdrlnbIO.getTranno());
		csncalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Csncalc.class, csncalcrec.csncalcRec);
		if (isNE(csncalcrec.statuz, Varcom.oK)
				&& isNE(csncalcrec.statuz, "FACL")) {
			syserrrec.statuz.set(csncalcrec.statuz);
			syserrrec.params.set(csncalcrec.csncalcRec);
			fatalError600();
		}
		if (isEQ(csncalcrec.statuz, "FACL")) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.r064);
			sv.life.set(csncalcrec.life);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			/*    MOVE CSNC-COVERAGE       TO S6378-COVERAGE        <CAS1.0>*/
			/*    MOVE CSNC-RIDER          TO S6378-RIDER           <CAS1.0>*/
			sv.payrseqno.set(0);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
	}

	protected void calcFee1200()
	{
		readSubroutineTable1210();
	}

	protected void readSubroutineTable1210()
	{
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t5674.toString());
		itempf.setItemitem(t5688rec.feemeth.toString());
		itempf = itempfDAO. getItempfRecord(itempf);


		if (null == itempf) {
			syserrrec.params.set(itemIO.getParams());
			scrnparams.errorCode.set(errorsInner.f151);
			wsspcomn.edterror.set("Y");
			return ;
		}
		t5674rec.t5674Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* Check subroutine NOT = SPACES before attempting call.           */
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		/* MOVE CHDRLNB-BILLFREQ       TO MGFL-BILLFREQ.                */
		mgfeelrec.billfreq.set(wsaaBillingInformationInner.wsaaBillfreq[1]);
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(wsspcomn.company);
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, Varcom.oK)
				&& isNE(mgfeelrec.statuz, Varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			fatalError600();
		}
		zrdecplrec.amountIn.set(mgfeelrec.mgfee);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		callRounding8000();
		mgfeelrec.mgfee.set(zrdecplrec.amountOut);
		if (isEQ(chdrlnbIO.getBillcd(), chdrlnbIO.getOccdate())) {
			sv.cntfee.set(ZERO);
		}
		else {
			readSubroutineCustomerSpecific();
			sv.cntfee.set(mgfeelrec.mgfee);
		}
		if (isGT(sv.cntfee, ZERO)) {
			wsaaCntfee.set(sv.cntfee);
			checkCalcContTax7100();
			wsaaCntfeeTax.set(wsaaTax);
			wsaaWavCntfeeTax.set(wsaaTax);

		}
	}
	
	protected void checkWaiver() {
		List<String> tr517List = null;
		tr517List = itemDAO.findItemByTable("IT", "2", "TR517");
		tr517List.replaceAll(String::trim);
		List<String> covtpf = new ArrayList<String>();
		List<Covtpf> covtpflist = null;
		covtpflist = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		for(Covtpf list: covtpflist){
			covtpf.add(list.getCrtable());
			}
		tr517List.retainAll(covtpf);
		if(!(tr517List.isEmpty())) {
			
			if (isEQ(hbnfIO.getStatuz(), varcom.mrnf) && isEQ(wsspcomn.flag,"N")) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.run9);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
			else {
				if(isEQ(hbnfIO.getWaiverprem(),"N")) {
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.run9);
					wsaaExtraMsgpfx.set(SPACES);
					errorMessages1800();
				}
					
			}
		
		}
		
		
	}
	protected void callHbnf1950()
	{
		/*READ-HBNF*/
		hbnfIO.setFormat(formatsInner.hbnfrec);
		SmartFileCode.execute(appVars, hbnfIO);
		if (isNE(hbnfIO.getStatuz(), varcom.oK)
		&& isNE(hbnfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hbnfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}
	protected void calcPremium1300(Covtpf covtpf)
	{
		readseqCovtlnb1310(covtpf);
	}

	protected void readseqCovtlnb1310(Covtpf covtpf)
	{
		/*    Read each record and accumulate all single and regular*/
		/*    premiums payable.*/
		if (isNE(chdrlnbIO.getChdrcoy(), covtpf.getChdrcoy())
				|| isNE(chdrlnbIO.getChdrnum(), covtpf.getChdrnum())) {
			//goTo(GotoLabel.exit1390);
			return;
		}
		/*1320-COVER-RIDER-DEFINITION.                                     */
		/*Check the Coverage/Rider Definition T5687 for a single       */
		/*Premium indicator of 'Y'.                                    */
		/*MOVE SPACES                 TO ITEM-PARAMS.                  */
		/*MOVE T5687                  TO ITEM-ITEMTABL.                */
		/*MOVE COVTLNB-CRTABLE        TO ITEM-ITEMITEM.                */
		/*MOVE 'READR'                TO ITEM-FUNCTION.                */
		/*MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.                 */
		/*MOVE 'IT'                   TO ITEM-ITEMPFX.                 */
		/*CALL 'ITEMIO' USING ITEM-PARAMS.                             */
		/*IF ITEM-STATUZ              NOT = O-K                        */
		/*                        AND NOT = MRNF                       */
		/*    MOVE ITEM-PARAMS        TO SYSR-PARAMS                   */
		/*    PERFORM 600-FATAL-ERROR.                                 */
		/*IF ITEM-STATUZ              = MRNF                           */
		/*    MOVE ITEM-PARAMS        TO SYSR-PARAMS                   */
		/*    MOVE E351               TO SCRN-ERROR-CODE               */
		/*    MOVE 'Y' TO WSSP-EDTERROR                                */
		/*    GO TO 1390-EXIT.                                         */
		/*MOVE ITEM-GENAREA TO T5687-T5687-REC.                        */
		/*1330-SINGLE-OR-REGULAR.                                          */
		/*    Single or Regular Premium?*/
		/*    Single and Regular Premiums are now held in separate fields  */
		/*    on COVT, so just move corresponding fields. If the COVT Inst */
		/*    amount is zero, then don't bother to apply the frequency     */
		/*    factor etc. Both fields may be non-zero!                     */
		/* IF CHDRLNB-BILLFREQ         = '00'                           */
		/* OR T5687-SINGLE-PREM-IND    = 'Y'                            */
		/* Add the premium into the relevant entry in the WS table         */
		/* depending  on the payer number on the COVT.                     */
		wsbbSub.set(covtpf.getPayrseqno());
		/* ADD COVTLNB-SINGP           TO WSAA-SINGP.                   */
		wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()].add(PackedDecimalData.parseObject(covtpf.getSingp()));
		if (isNE(covtpf.getSingp(), 0)) {
			wsaaSingPrmInd = "Y";
		}
		if (isEQ(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()], "00")) {
			wsaaModalPremium.add(PackedDecimalData.parseObject(covtpf.getSingp()));
		}
		if(!covrValidated ) {
			validationCheck(covtpf);
		}
		if (isEQ(covtpf.getInstprem(), 0)) {
			//goTo(GotoLabel.continue1380);
			continue1380(covtpf);
			return;
		}
		/* Get the frequency Factor from DATCON3 for Regular premiums.*/
		/*    MOVE CHDRLNB-BILLCD         TO DTC3-INT-DATE-2.*/
		/*    MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-1.*/
		/*    MOVE CHDRLNB-BILLFREQ       TO DTC3-FREQUENCY.*/
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC.*/
		/*    IF DTC3-STATUZ              NOT = O-K*/
		/*       GO TO 1190-EXIT.*/
		/*    Use the DATCON3 Frequency Factor to calculate the Instate-*/
		/*    ment Premium.*/
		/* MULTIPLY COVTLNB-SINGP      BY DTC3-FREQ-FACTOR              */
		/*    MULTIPLY COVTLNB-INSTPREM   BY DTC3-FREQ-FACTOR              */
		/*                                GIVING WSAA-INSTPRM.*/
		/*    ADD WSAA-INSTPRM         TO WSAA-REGPREM.*/
		/* If the Freq has been changed to SP and screens have not been    */
		/* revisited COVT will still show it has an INSTPREM when in fact  */
		/* this is not true. A IVFQ status will be returned from a later   */
		/* call to DATCON3 without the following code                      */
		/* IF CHDRLNB-BILLFREQ         = '00'                      <014>*/
		/*    GO TO 1380-CONTINUE.                                 <014>*/
		/* ADD COVTLNB-INSTPREM     TO WSAA-REGPREM.                    */
		
		if (isEQ(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()], "00")) {
			//goTo(GotoLabel.continue1380);
			continue1380(covtpf);
			return;
		}
		wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()].add(PackedDecimalData.parseObject(covtpf.getInstprem()));
		wsaaModalPremium.add(PackedDecimalData.parseObject(covtpf.getInstprem()));
		continue1380(covtpf);
		return;
	}

	protected void continue1380(Covtpf covtpf)
	{
		checkCalcCompTax7000(covtpf);
	}

	protected void checkMinMaxLimit1400()
	{
		para1400();
	}

	protected void para1400()
	{
		/*    Check premium against the limits set in TH611 according to   */
		/*    the range limits for billing frequency.                      */
		wsaaTh611Cnttype.set(chdrlnbIO.getCnttype());
		wsaaTh611Currcode.set(chdrlnbIO.getCntcurr());
		itdmpfList = itemDAO.getItdmByFrmdate(chdrlnbIO.getChdrcoy().toString(), tablesInner.th611.toString(), wsaaTh611Item.toString().trim(), chdrlnbIO.getOccdate().toInt());

		if(itdmpfList == null || itdmpfList.size() == 0){
			wsaaTh611Cnttype.set("***");
			itdmpfList = itemDAO.getItdmByFrmdate(chdrlnbIO.getChdrcoy().toString(), tablesInner.th611.toString(), wsaaTh611Item.toString().trim(), chdrlnbIO.getOccdate().toInt());
			itdmpf = itdmpfList.get(0);
		}else {
			itdmpf = itdmpfList.get(0);
			//goTo(GotoLabel.findMaxMinLimit1410);
			findMaxMinLimit1410();
			return;
		}
		if(itdmpfList == null || itdmpfList.size() == 0){
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.rl12);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
			//goTo(GotoLabel.exit1420);
			return;
		}
	}

	protected void findMaxMinLimit1410()
	{
		th611rec.th611Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));
		/*    Process to find MAX/MIN limits for frequency required.       */
		for (wsaaSub.set(1); !(isGT(wsaaSub, 8)
				|| isEQ(chdrlnbIO.getBillfreq(), th611rec.frequency[wsaaSub.toInt()])); wsaaSub.add(1)){
			findBillfreq950();
		}
		/*    If a range is not found or premium is out with the range     */
		/*    then error.                                                  */
		if (isGT(wsaaSub, 8)
				|| ((isLT(wsaaModalPremium, th611rec.cmin[wsaaSub.toInt()]))
						|| (isGT(wsaaModalPremium, th611rec.cmax[wsaaSub.toInt()])))) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.rl11);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
	}



	protected void findBillfreq950()
	{
		/*READ*/
		/**    Dummy paragraph to find billing frequency.                   */
		/*EXIT*/
	}

	protected void readForBnfy1400(Bnfypf bnfypf, boolean isBnfyListValid)
	{
		para11400(bnfypf, isBnfyListValid);
	}

	protected void para11400(Bnfypf bnfypf, boolean isBnfyListValid)
	{
		/* MOVE SPACES                 TO BNFYLNB-DATA-KEY.     <LA5070>*/
		/* MOVE WSSP-COMPANY           TO BNFYLNB-CHDRCOY.      <LA5070>*/
		/* MOVE CHDRLNB-CHDRNUM        TO BNFYLNB-CHDRNUM.      <LA5070>*/
		/* MOVE BEGN                   TO BNFYLNB-FUNCTION.     <LA5070>*/
		wsaaIwc.add(1);
		if ((isNE(bnfypf.getChdrcoy(), chdrlnbIO.getChdrcoy()))
				|| (isNE(bnfypf.getChdrnum(), chdrlnbIO.getChdrnum()))){
			isBnfyListValid= false;
			return;
		}
		if (isNE(bnfypf.getEffdate(), chdrlnbIO.getOccdate())) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e977);
			sv.life.set(SPACES);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			sv.payrseqno.set(0);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
			isBnfyListValid= false;
			return ;
		}
		/*  Check that the Beneficaries is not dead.                    */
		wsaaClntnumIo.set(bnfypf.getBnyclt());
		a3000CallCltsio();
		if ((clntpfData != null)) {
			if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)
					&& isGT(chdrlnbIO.getOccdate(), clntpfData.getCltdod())) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.w343);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(ZERO);
				wsaaExtraMsgpfx.set("BN");
				errorMessages1800();
			}
		}
		wsaaBnytype[wsaaIwc.toInt()].set(bnfypf.getBnytype());
	}

	protected void checkFollowUps1500(Fluppf fluppf)
	{
		readStatusTable1510(fluppf);
	}

	protected void readStatusTable1510(Fluppf fluppf)
	{

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t5661.toString());
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(fluppf.getFupCde());
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (null == itempf) {
			//syserrrec.params.set(itemIO.getParams());
			scrnparams.errorCode.set(errorsInner.e351);
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit1590);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/*  Assume the current status will not be found,*/
		/*     therefore, the follow-up is outstanding.*/
		wsaaOutstanflu = "Y";
		for (wsaaX.set(1); !(isGT(wsaaX, 10)
				|| isEQ(wsaaOutstanflu, "N")); wsaaX.add(1)){
			outstandingCheck1520(fluppf);
		}
		
		/*  Set indicator if outstanding Follow Up found. This will be     */
		/*  lost on next read as we no longer abort on first outstanding   */
		/*  Follow Up.                                                     */
		if (isEQ(wsaaOutstanflu, "Y")) {
			wsaaOutflupFound.set(wsaaOutstanflu);
		}
		/*  Check if the Follow Up also requires Doctor details.           */
		if (isEQ(t5661rec.zdocind, "Y")) {
			wsaaDocReqd.set("Y");
		}
		//goTo(GotoLabel.readNextFollowUp1530);
		readNextFollowUp1530(fluppf);
		return;

	}

	/**
	 * <pre>
	 *  If any one of the codes matches, it is not outstanding.
	 * </pre>
	 */
	protected void outstandingCheck1520(Fluppf fluppf)
	{
		if (isEQ(fluppf.getFupSts(), t5661rec.fuposs[wsaaX.toInt()])) {
			wsaaOutstanflu = "N";
		}
	}

	/**
	 * <pre>
	 *  If a match was not found, this is all we need to know.
	 * </pre>
	 */
	protected void readNextFollowUp1530(Fluppf fluppf)
	{
		/* IF WSAA-OUTSTANFLU = 'Y'                                     */
		if (isEQ(wsaaOutflupFound, "Y")
				&& isEQ(wsaaDocReqd, "Y")) {
			//fluplnbIO.setStatuz(Varcom.endp);
			return ;
		}
		/*  So far, an outstanding follow-up has not been found,*/
		/*    so read the next one.*/
	}

	/**
	 * <pre>
	 *     LIFE LEVEL VALIDATION
	 * </pre>
	 */
	protected void lifeLevel1600(List<Lifepf> lifepfList)
	{
		lifeLevel1610(lifepfList);
	}

	protected void lifeLevel1610(List<Lifepf> lifepfList){

		List<Covtpf> covtpfList = null;
		List<Undcpf> undcpfList = null;
		List<Covtpf> covtpfList1 = new ArrayList<Covtpf>();
		boolean isValid = true;
		int undcpfListSize = 0;
		wsaaLife.set(SPACE);

		// Check for first Life Record to set Error Message
		boolean firstLifeRecord = true;

		if(ListUtils.isEmpty(lifepfList)){

			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e355);
			sv.life.set(SPACES);
			sv.jlife.set(SPACES);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			sv.payrseqno.set(0);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}

		for(Lifepf lifepf : lifepfList){

			if ((isNE(lifepf.getChdrcoy(), wsspcomn.company)
					|| isNE(lifepf.getChdrnum(), chdrlnbIO.getChdrnum()))
					&& firstLifeRecord) {

				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.e355);
				sv.life.set(SPACES);
				sv.jlife.set(SPACES);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(0);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
			firstLifeRecord = false;

			if (isNE(lifepf.getChdrcoy(), wsspcomn.company)
					|| isNE(lifepf.getChdrnum(), chdrlnbIO.getChdrnum())) {
				return ;
			}
			/*
			 * fwang3
			 */
			if(chinaOccupPermission)
			{
				Clntpf clnt = new Clntpf();
				clnt.setClntpfx("CN");
				clnt.setClntcoy(wsspcomn.fsuco.toString());
				clnt.setClntnum(lifepf.getLifcnum());
				clnt = clntpfDAO.selectActiveClient(clnt);
				if (clnt != null && clnt.getOccpcode() != null && 
						lifepf.getOccup() != null && isNE(clnt.getOccpcode(), lifepf.getOccup())) {
					if (isEQ("P5003", wsspcomn.lastprog)) {
						errmesgrec.errmesgRec.set(SPACES);
						errmesgrec.eror.set(errorsInner.rreh);
						wsaaExtraMsgpfx.set(SPACES);
						errorMessages1800();
					} else if (isEQ("P5074", wsspcomn.lastprog)) {
						lifepf.setOccup(clnt.getOccpcode());
						lifepfDAO.updateOccup(lifepf);
					}
				}
			}
			/* Check age next birthday.*/
			/*    MOVE LIFELNB-CLTDOB         TO DTC3-INT-DATE-1.              */
			/*    MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-2.              */
			/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
			/*    CALL 'DATCON3'              USING DTC3-DATCON3-REC.          */
			/*    IF DTC3-STATUZ              NOT = O-K                        */
			/*       MOVE SPACES              TO ERMS-ERRMESG-REC              */
			/*       MOVE DTC3-STATUZ         TO ERMS-EROR                     */
			/*       MOVE LIFELNB-LIFE        TO S6378-LIFE                    */
			/*       MOVE SPACES              TO S6378-JLIFE                   */
			/*                                   S6378-COVERAGE                */
			/*                                   S6378-RIDER                   */
			/*       MOVE 0                   TO S6378-PAYRSEQNO          <014>*/
			/*       PERFORM 1800-ERROR-MESSAGES                               */
			/*       MOVE NEXTR               TO LIFELNB-FUNCTION              */
			/*       GO TO 1690-EXIT.                                          */
			/*     ADD 0.999, DTC3-FREQ-FACTOR GIVING WSBB-ANB-INT.            */
			/* Display error if RCD is greater than the date of                */
			/* death of the life assured(s).                                   */
			calcAge1900(lifepf);
			if (isNE(agecalcrec.statuz, Varcom.oK)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(agecalcrec.statuz);
				sv.life.set(lifepf.getLife());
				sv.jlife.set(SPACES);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(0);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
				continue;
			}
			wsbbAnbInt.set(wsaaAnb);
			if (isNE(wsbbAnbInt, lifepf.getAnbAtCcd())) {
				sv.life.set(lifepf.getLife());
				sv.jlife.set(lifepf.getJlife());
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(0);
				errmesgrec.errmesgRec.set(SPACES);
				/*       MOVE U024              TO ERMS-EROR                     */
				errmesgrec.eror.set(errorsInner.h015);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
			/* Check whether a cover exists for this life.*/
			covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
			for(Covtpf covt : covtpfList) {
				if(covt.getLife().equals(lifepf.getLife())) {
					covtpfList1.add(covt);
				}
			}
			
			if(ListUtils.isEmpty(covtpfList1)){
				sv.life.set(lifepf.getLife());
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.g066);
				sv.jlife.set(SPACES);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(0);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}else{
				//int index= Integer.parseInt(lifepf.getLife())-1;		//ILIFE-4422
				Covtpf Covtpf = covtpfList1.get(0);
				if(isNE(Covtpf.getChdrcoy(), chdrlnbIO.getChdrcoy())
						|| isNE(Covtpf.getChdrnum(), chdrlnbIO.getChdrnum())
						|| isNE(Covtpf.getLife(), lifepf.getLife())){
					if(isEQ(Covtpf.getLife(), SPACES) || isEQ(Covtpf.getRider(), SPACES)){
						sv.life.set(lifepf.getLife());
						errmesgrec.errmesgRec.set(SPACES);
						errmesgrec.eror.set(errorsInner.g066);
						sv.jlife.set(SPACES);
						sv.coverage.set(SPACES);
						sv.rider.set(SPACES);
						sv.payrseqno.set(0);
						wsaaExtraMsgpfx.set(SPACES);
						errorMessages1800();
					}
				}
			}

			/* Only call CSNCALC once per life (in joint life cases, there     */
			/* will be more than 1 LIFE record for the same life.)             */
			if (isNE(lifepf.getLife(), wsaaLife)) {
				callCsncalc1150(lifepf);
				wsaaLife.set(lifepf.getLife());
			}
			wsaaQuestset.set(SPACES);
			readTr6751640(lifepf);
			if (isNE(wsaaQuestset, SPACES)) {
				underwriting1650(lifepf);
			}
			if (wsaaRevisitQ.isTrue()) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.e591);
				sv.life.set(lifepf.getLife());
				sv.jlife.set(lifepf.getJlife());
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(ZERO);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}

			undcpfList = undcpfDAO.searchUndcpfRecord(lifepf.getChdrcoy(), lifepf.getChdrnum());/* IJTI-1386 */
			undcpfListSize = undcpfList.size();

			for(int index = 0; (index < undcpfListSize) && (isValid == true); index ++){

				Undcpf undcpf = undcpfList.get(index);

				checkUndc1660(lifepf, undcpf, isValid);
			}

			if (isNE(wsspcomn.flag, "I")
					&& isNE(undlpf.getUndwflag(), "Y")) { //IBPLIFE-8671
				a100ChdrOrRisk(lifepf);
				/*****     PERFORM 1G00-CALL-HCRTFUP                        <LA2103>*/
			}
			/* Since 1150-CALL-CSNCALC will distort the pointer of LIFLNB   */
			/* Use READR to correct the pointer.                            */
		}
	}
	/**
	 * <pre>
	 * At this stage no UNDQ records exist for the life so we need to
	 * check if this is OK or not. I.e. check if any of the questions
	 * on T6771 for this product are mandatory. If yes then do not
	 * perform any underwriting but display a message telling the user
	 * to revisit the questionnaire. All mandatory questions must be
	 * answered before any underwriting can be determined.
	 * This section calls the underwriting subroutine.
	 * </pre>
	 */
	protected void readTr6751640(Lifepf lifepf)
	{
		start1641(lifepf);
	}

	protected void start1641(Lifepf lifepf)
	{
		wsaaRevisitQuestionnaire.set(SPACES);

		itdmpfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), tablesInner.tr675.toString(), chdrlnbIO.getCnttype().toString(), chdrlnbIO.getOccdate().toInt());

		if (itdmpfList == null || itdmpfList.size() == 0) {
			//goTo(GotoLabel.exit1649);
			return;
		}

		tr675rec.tr675Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		wsaaQuestset.set(SPACES);
		/*  Get the Question Set from TR675                                */
		if (isEQ(lifepf.getCltsex(), "M")) {
			if (isLTE(lifepf.getAnbAtCcd(), tr675rec.age01)) {
				wsaaQuestset.set(tr675rec.questset01);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age01)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age02)) {
				wsaaQuestset.set(tr675rec.questset03);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age02)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age03)) {
				wsaaQuestset.set(tr675rec.questset05);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age03)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age04)) {
				wsaaQuestset.set(tr675rec.questset07);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age04)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age05)) {
				wsaaQuestset.set(tr675rec.questset09);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age05)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age06)) {
				wsaaQuestset.set(tr675rec.questset11);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
		}
		else {
			if (isLTE(lifepf.getAnbAtCcd(), tr675rec.age01)) {
				wsaaQuestset.set(tr675rec.questset02);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age01)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age02)) {
				wsaaQuestset.set(tr675rec.questset04);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age02)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age03)) {
				wsaaQuestset.set(tr675rec.questset06);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age03)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age04)) {
				wsaaQuestset.set(tr675rec.questset08);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age04)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age05)) {
				wsaaQuestset.set(tr675rec.questset10);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
			if (isGT(lifepf.getAnbAtCcd(), tr675rec.age05)
					&& isLTE(lifepf.getAnbAtCcd(), tr675rec.age06)) {
				wsaaQuestset.set(tr675rec.questset12);
				//goTo(GotoLabel.readT67711642);
				readT67711642();
				return;
			}
		}
		readT67711642();
		return;
	}

	protected void readT67711642()
	{
		/* If Underwriting is required on the product and the BMI Basis    */
		/* exists on TR675, read T6769 (Underwriting based on BMI) to get  */
		/* the BMI Factor for the given BMI Basis.                         */

		itdmpfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), tablesInner.t6771.toString(), wsaaQuestset.toString(), chdrlnbIO.getOccdate().toInt());

		if (itdmpfList == null || itdmpfList.size() == 0) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.rpid);
			sv.payrseqno.set(ZERO);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		else {
			t6771rec.t6771Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		}
		for (sub.set(1); !(isGT(sub, 40)); sub.add(1)){
			if (isEQ(t6771rec.questst[sub.toInt()], "M")) {
				wsaaRevisitQ.setTrue();
			}
		}
	}

	protected void underwriting1650(Lifepf lifepf){

		if(!undqpfDAO.isExistundqpfByCoyAndNumAndLifeAndJLife(lifepf.getChdrcoy(), lifepf.getChdrnum(), lifepf.getLife(), lifepf.getJlife())){
			if (wsaaRevisitQ.isTrue()) {
				return ;
			}
		}else{
			wsaaRevisitQuestionnaire.set(SPACES);
		}
		//IBPLIFE-8671 starts
		undlpf.setChdrcoy(lifepf.getChdrcoy().toString());
		undlpf.setChdrnum(lifepf.getChdrnum().toString());
		undlpf.setLife(lifepf.getLife().toString());
		undlpf.setJlife(lifepf.getJlife().toString());
		Undlpf undlpfTemp=new Undlpf(undlpf);
		List<Undlpf> listUndlpf=undlpfDAO.findByKey(undlpfTemp);
		if(listUndlpf!=null & !listUndlpf.isEmpty()) {
			undlpf=listUndlpf.get(0);
		}else {
			fatalError600();
		}
		//IBPLIFE-8671 end
		initialize(undwsubrec.undwsubRec);
		undwsubrec.branch.set(chdrlnbIO.getCntbranch());
		undwsubrec.language.set(wsspcomn.language);
		undwsubrec.currency.set(chdrlnbIO.getCntcurr());
		undwsubrec.chdrcoy.set(lifepf.getChdrcoy());
		undwsubrec.chdrnum.set(lifepf.getChdrnum());
		undwsubrec.life.set(lifepf.getLife());
		undwsubrec.jlife.set(lifepf.getJlife());
		undwsubrec.effdate.set(lifepf.getCurrfrom());
		undwsubrec.tranno.set(lifepf.getTranno());
		undwsubrec.clntnum.set(lifepf.getLifcnum());
		undwsubrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		undwsubrec.bmirule.set(undlpf.getBmirule());//IBPLIFE-8671
		undwsubrec.doctor.set(undlpf.getClntnum01());//IBPLIFE-8671
		undwsubrec.clntcoy.set(wsspcomn.fsuco);
		undwsubrec.user.set(varcom.vrcmUser);
		undwsubrec.cnttype.set(chdrlnbIO.getCnttype());
		/* If in Enquiry mode (WSSP-FLAG = 'I' do not call the             */
		/* underwriting subroutine, bypass it.                             */
		/* If the underwriting completed flag on UNDLPF = 'Y' then do not  */
		/* call the underwriting subroutine.                               */
		if (isNE(undlpf.getUndwflag(), "Y") // IBPLIFE-8671
				&& isNE(t6771rec.undwsubr, SPACES)) {
			callProgram(t6771rec.undwsubr, undwsubrec.undwsubRec);
			undlpf.setOvrrule(undwsubrec.ovrrule.toString());//IBPLIFE-8671
			undlpfDAO.updateByID(undlpf);//IBPLIFE-8671
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e964);
			sv.exratErr.set("E964");
			sv.life.set(undwsubrec.life);
			sv.jlife.set(undwsubrec.jlife);
			sv.coverage.set(SPACES);
			sv.rider.set(SPACES);
			sv.payrseqno.set(ZERO);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
			if (isNE(undwsubrec.errorCode, SPACES)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(undwsubrec.errorCode);
				sv.life.set(undwsubrec.life);
				sv.jlife.set(undwsubrec.jlife);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(ZERO);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
		}
	}

	protected void checkUndc1660(Lifepf lifepf, Undcpf undcpf, boolean isValid){

		start1660(lifepf, undcpf, isValid);
	}

	protected void start1660(Lifepf lifepf, Undcpf undcpf, boolean isValid)
	{
		if (isNE(undcpf.getChdrcoy(), lifepf.getChdrcoy())
				|| isNE(undcpf.getChdrnum(), lifepf.getChdrnum())
				|| isNE(undcpf.getLife(), lifepf.getLife())) {
			isValid = false;
			return;
		}

		if (isNE(undcpf.getSpecind(), SPACES)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.h362);
			sv.life.set(undcpf.getLife());
			sv.jlife.set(undcpf.getJlife());
			sv.coverage.set(undcpf.getCoverage());
			sv.rider.set(undcpf.getRider());
			sv.payrseqno.set(ZERO);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
	}

	/**
	 * <pre>
	 *     COMPONENT LEVEL VALIDATION
	 * </pre>
	 */
	protected void componentLevel1700(Covtpf covtpf)
	{
		componentLevel1710(covtpf);
	}

	protected void componentLevel1710(Covtpf covtpf)
	{
		List<Lifepf> lifepfListLife00 = null;
		List<Lifepf> lifepfListLife01 = null;
		Lifepf lifepfLife00 = null;
		Lifepf lifepfLife01 = null;
		Clntpf clntpfCriteria = null;
		/* Set up coverage and rider numbers for error messages*/
		sv.coverage.set(covtpf.getCoverage());
		if (isEQ(covtpf.getRider(), "00")
				|| isEQ(covtpf.getRider(), "  ")) {
			sv.rider.set(SPACES);
		}
		else {
			sv.rider.set(covtpf.getRider());
		}
		sv.life.set(covtpf.getLife());
		sv.jlife.set(SPACES);
		sv.payrseqno.set(0);
		/* Check the Coverage/Rider Definition T5687 for a single       */
		/* Premium indicator of 'Y'.                                    */

		itdmpfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), tablesInner.t5687.toString(), covtpf.getCrtable(), chdrlnbIO.getOccdate().toInt());/* IJTI-1386 */

		if (itdmpfList == null || itdmpfList.size() == 0) {
			syserrrec.params.set("2t5687"+covtpf.getCrtable()+chdrlnbIO.getOccdate());
			scrnparams.errorCode.set(errorsInner.e351);
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit1790);
			return;
		}
		t5687rec.t5687Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		/* Check pay method, payment frequency, and risk commencement*/
		/* date.*/
		/* IF COVTLNB-BILLCHNL         NOT = CHDRLNB-BILLCHNL           */
		/* OR COVTLNB-BILLFREQ         NOT = CHDRLNB-BILLFREQ           */
		wsbbSub.set(covtpf.getPayrseqno());
		if (isNE(covtpf.getBillchnl(), wsaaBillingInformationInner.wsaaBillchnl[wsbbSub.toInt()])
				|| (isNE(t5687rec.singlePremInd, "Y")
						&& isNE(covtpf.getBillfreq(), wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]))
				|| isNE(covtpf.getEffdate(), chdrlnbIO.getOccdate())
				|| isNE(covtpf.getPolinc(), chdrlnbIO.getPolinc())
				|| isNE(covtpf.getCntcurr(), chdrlnbIO.getCntcurr())) {
			errmesgrec.errmesgRec.set(SPACES);
			sv.payrseqno.set(0);
			errmesgrec.eror.set(errorsInner.g041);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		/* Check that life (joint life) exists for this component.*/
		lifepfListLife00 = lifepfDAO.searchLifeRecordByLife(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString(), covtpf.getLife(), "00");/* IJTI-1386 */
		lifepfLife00 = lifepfListLife00.get(0);
		/* Check sex for life.*/
		if (isNE(covtpf.getSex01(), lifepfLife00.getCltsex())) {
			errmesgrec.errmesgRec.set(SPACES);
			sv.payrseqno.set(0);
			errmesgrec.eror.set(errorsInner.h362);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		/* Check age next birthday.*/
		if (isNE(covtpf.getAnbccd01(), lifepfLife00.getAnbAtCcd())) {
			errmesgrec.errmesgRec.set(SPACES);
			sv.payrseqno.set(0);
			errmesgrec.eror.set(errorsInner.h361);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		/*    Read the Client details for the associated Life.*/
		clntpfCriteria = new Clntpf();

		clntpfCriteria.setClntpfx("CN");
		clntpfCriteria.setClntcoy(wsspcomn.fsuco.toString());
		clntpfCriteria.setClntnum(lifepfLife00.getLifcnum());

		clntpfData = clntpfDAO.selectActiveClient(clntpfCriteria);//ILIFE-6270	
        if(clntpfData != null){
		if (isNE(clntpfData.getCltdob(), lifepfLife00.getCltdob())) {
			errmesgrec.errmesgRec.set(SPACES);
			//TSD-266 Starts
			errmesgrec.eror.set(errorsInner.h360);
			/*ILIFE-2273 comment out code not need*/
			/*if (ispermission2) {
				if(isNE(cltsIO.getCltsex(),lifelnbIO.getCltsex())){
					errmesgrec.eror.set(errorsInner.rfpj);
				}
			}*/
			//TSD-266 Ends
			sv.payrseqno.set(0);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
	}
		/*ILIFE-2273 Start*/
		if (ispermission2) {
			if(clntpfData != null){
			if(isNE(clntpfData.getCltsex(),lifepfLife00.getCltsex())){
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.rfpj);
				sv.payrseqno.set(0);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
		}
	}
		/*ILIFE-2273 End*/
		/*  Check that the life assured is not dead.                    */
		if(clntpfData != null){
			if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)
					&& isGT(chdrlnbIO.getOccdate(), clntpfData.getCltdod())) {
				errmesgrec.errmesgRec.set(SPACES);
				/*     MOVE F782               TO ERMS-EROR             <A06596>*/
				errmesgrec.eror.set(errorsInner.w343);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(ZERO);
				wsaaExtraMsgpfx.set("LF");
				errorMessages1800();
			}
		}
		/* CHECK THAT THE DATE OF BIRTH MATCHES UP WITH THE*/
		/* AGE NEXT BIRTHDAY.*/
		/*    MOVE LIFELNB-CLTDOB         TO DTC3-INT-DATE-1.              */
		/*    MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-2.              */
		/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC.                       */
		/*    IF DTC3-STATUZ              = O-K                            */
		/* Round up the age.*/
		/*       ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB.              */
		calcAge1900(lifepfLife00);
		if (isNE(wsaaAnb, lifepfLife00.getAnbAtCcd())) {
			errmesgrec.errmesgRec.set(SPACES);
			sv.payrseqno.set(0);
			errmesgrec.eror.set(errorsInner.h360);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		/* Read joint life (to see if it is there).*/
		lifepfListLife01 = lifepfDAO.searchLifeRecordByLife(wsspcomn.company.toString(), 
				chdrlnbIO.getChdrnum().toString(), covtpf.getLife(), "01");/* IJTI-1386 */

		if(ListUtils.isEmpty(lifepfListLife01)){
			//goTo(GotoLabel.readNext1780);
			readNext1780();
			return;
		}
		sv.jlife.set("01");
		/* Check sex for life.*/
		lifepfLife01 = lifepfListLife01.get(0); /*ILIFE-3797*/
		if (isNE(covtpf.getSex02(), lifepfLife01.getCltsex())) {
			errmesgrec.errmesgRec.set(SPACES);
			sv.payrseqno.set(0);
			errmesgrec.eror.set(errorsInner.h362);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		/* Check age next birthday.*/
		if (isNE(covtpf.getAnbccd02(), lifepfLife01.getAnbAtCcd())) {
			errmesgrec.errmesgRec.set(SPACES);
			sv.payrseqno.set(0);
			errmesgrec.eror.set(errorsInner.h361);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		/*    Read the Client details for the associated Life.*/
		clntpfCriteria = new Clntpf();

		clntpfCriteria.setClntpfx("CN");
		clntpfCriteria.setClntcoy(wsspcomn.fsuco.toString());
		clntpfCriteria.setClntnum(lifepfLife01.getLifcnum());

		clntpfData = clntpfDAO.selectActiveClient(clntpfCriteria);//ILIFE-6270
		if(clntpfData != null){
			if (isNE(clntpfData.getCltdob(), lifepfLife01.getCltdob())) {
				errmesgrec.errmesgRec.set(SPACES);
				sv.payrseqno.set(0);
				//TSD-266 Starts
				errmesgrec.eror.set(errorsInner.h360);
				/*ILIFE-2273 comment out code not need*/
				/*if (ispermission2) {
				if(isNE(cltsIO.getCltsex(),lifelnbIO.getCltsex())){
					errmesgrec.eror.set(errorsInner.rfpj);
				}
			}*/
				//TSD-266 Ends
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
		}
		/*ILIFE-2273 Start*/
		if (ispermission2) {
			if(clntpfData != null){
				if(isNE(clntpfData.getCltsex(),lifepfLife01.getCltsex())){
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.rfpj);
					sv.payrseqno.set(0);
					wsaaExtraMsgpfx.set(SPACES);
					errorMessages1800();
				}
			}
		}
		/*ILIFE-2273 End*/
		/*  Check that the JOINT life assured is not dead.              */
		if(clntpfData != null){		
			if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)
					&& isGT(chdrlnbIO.getOccdate(), clntpfData.getCltdod())) {
				errmesgrec.errmesgRec.set(SPACES);
				/*    MOVE F782               TO ERMS-EROR             <A06596>*/
				errmesgrec.eror.set(errorsInner.w343);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(ZERO);
				wsaaExtraMsgpfx.set("JL");
				errorMessages1800();
			}
		}
		/* CHECK THAT THE DATE OF BIRTH MATCHES UP WITH THE*/
		/* AGE NEXT BIRTHDAY.*/
		/*    MOVE LIFELNB-CLTDOB         TO DTC3-INT-DATE-1.              */
		/*    MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-2.              */
		/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC.                       */
		/*    IF DTC3-STATUZ              = O-K                            */
		/* Round up the age.*/
		/*       ADD .99999 DTC3-FREQ-FACTOR GIVING WSAA-ANB.              */
		calcAge1900(lifepfLife01);
		if (isNE(wsaaAnb, lifepfLife01.getAnbAtCcd())) {
			errmesgrec.errmesgRec.set(SPACES);
			sv.payrseqno.set(0);
			errmesgrec.eror.set(errorsInner.h360);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}

		readNext1780();
		return;

	}

	protected void readNext1780()
	{
		return;
	}

	/**
	 * <pre>
	 *     CALL ERROR MESSAGE SUBROUTINE
	 * </pre>
	 */
	protected void errorMessages1800()
	{
		errorMessages1810();
	}

	protected void errorMessages1810()
	{
		/* Go get the error message.*/
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaProg);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.set(SPACES);
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		/* Move all details to the screen.*/
		//TMLII-268
		if(isNE(scrnparams.deviceInd, "*RMT")){
			if (isNE(wsaaExtraMsgpfx, SPACES)) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression(wsaaExtraMsgpfx, "  ");
				stringVariable1.addExpression(" -  ", "  ");
				stringVariable1.addExpression(errmesgrec.errmesg[1], "  ");
				stringVariable1.setStringInto(sv.erordsc);
			}
			else {
				sv.erordsc.set(errmesgrec.errmesg[1]);
			}
			sv.errcde.set(errmesgrec.eror);
			/* Add the record to the subfile.*/
			scrnparams.function.set(varcom.sadd);
			processScreen("S6378", sv);
			if (isNE(scrnparams.statuz, Varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}

		} 
		else {
			//TMLII-1285
			valErrorCodes.add(errmesgrec.eror.toString());
			syserrrec.statuz.set(errmesgrec.eror); 
			syserrrec.syserrStatuz.set(errmesgrec.eror);
			wsaaErrorChdrnum.set(chdrlnbIO.chdrnum);
			wsaaErrorMsg.set(errmesgrec.errmesg[1]);
			syserrrec.params.set(wsaaParams);
			syserrrec.syserrType.set("4");
			callProgram(Syserr.class, syserrrec.syserrRec);
		}
		wsaaErrorFlag = "Y";
		//TMLII-268 NB-08-001 end
	}

	protected void agentErrorMessage1850()
	{
		error1860();
	}

	protected void error1860()
	{
		/* Call ERRMESG to retrieve the error message for display.      */
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaProg);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.set(SPACES);
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		/* Clear the subfile line then string the Agent number plus the */
		/* error message into the error display.                        */
		sv.life.set(SPACES);
		sv.jlife.set(SPACES);
		sv.coverage.set(SPACES);
		sv.rider.set(SPACES);
		sv.payrseqno.set(ZERO);
		wsaaAgntnum.set(agntIO.getAgntnum());
		if(japanLocPermission) {
			wsaaErrmesg.set(errmesgrec.errmesg1[1]);
		}else {
			wsaaErrmesg.set(errmesgrec.errmesg[1]);
		}
		if(isNE(scrnparams.deviceInd, "*RMT")){
			sv.erordsc.set(wsaaAgentError);
			sv.errcde.set(errmesgrec.eror);
			/* Add the record to the subfile.                               */
			scrnparams.function.set(varcom.sadd);
			processScreen("S6378", sv);
			if (isNE(scrnparams.statuz, Varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		else {
			//TMLII-1285
			valErrorCodes.add(errmesgrec.eror.toString());
			syserrrec.statuz.set(errmesgrec.eror); 
			syserrrec.syserrStatuz.set(errmesgrec.eror);
			wsaaErrorChdrnum.set(chdrlnbIO.chdrnum);
			if(japanLocPermission) {
				wsaaErrmesg.set(errmesgrec.errmesg1[1]);
			}else {
				wsaaErrmesg.set(errmesgrec.errmesg[1]);
			}
			syserrrec.params.set(wsaaParams);
			syserrrec.syserrType.set("4");
			callProgram(Syserr.class, syserrrec.syserrRec);
		}
		//TMLII-268 NB-08-001 end
		wsaaErrorFlag = "Y";
	}

	protected void calcAge1900(Lifepf lifepf)
	{
		init1910(lifepf);
	}

	protected void init1910(Lifepf lifepf)
	{
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		agecalcrec.intDate1.set(lifepf.getCltdob());
		agecalcrec.intDate2.set(chdrlnbIO.getOccdate());
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			return ;
		}
		wsaaAnb.set(agecalcrec.agerating);
	}

	/**
	 * <pre>
	 *                                                            <022>
	 *1900-CHECK-FREQ-DATE  SECTION.                              <022>
	 *******************************                              <022>
	 *                                                            <022>
	 *                                                            <022>
	 *    we are only interested in monthly or half yearly        <022>
	 *    payment frequencies                                     <022>
	 *                                                            <022>
	 *    IF CHDRLNB-BILLFREQ  = '12'                             <022>
	 *        GO TO 1930-CHECK-MONTHLY-DATES.                     <022>
	 *                                                            <022>
	 *    IF CHDRLNB-BILLFREQ NOT = '02'                          <022>
	 *        GO TO 1990-EXIT.                                    <022>
	 *                                                            <022>
	 *1920-CHECK-HALF-ANNUAL-DATES.                               <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 02) AND                        <022>
	 *        (WSAA-PREM-DAY1    > 27) AND                        <022>
	 *        (WSAA-PREM-DAY1    < 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 31) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 08))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 03) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 30) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 08))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 04) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 31) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 10))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 05) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 30) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 11))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 06) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 31) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 12))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 08) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 28) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 02))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 09) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 31) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 03))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 10) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 30) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 04))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 11) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 31) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 05))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 12) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 30) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 06))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    GO TO 1990-EXIT.                                        <022>
	 *                                                            <022>
	 *                                                            <022>
	 *                                                            <022>
	 *                                                            <022>
	 *1930-CHECK-MONTHLY-DATES.                                   <022>
	 *                                                            <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 01) AND                        <022>
	 *        (WSAA-PREM-DAY1    > 27)  AND                       <022>
	 *        (WSAA-PREM-DAY1    < 31)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 28) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 02))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 03) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 30) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 04))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 05) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 30) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 06))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 08) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 30) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 09))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *    IF ((WSAA-PREM-MTH1    = 10) AND                        <022>
	 *        (WSAA-PREM-DAY1    = 30)) AND                       <022>
	 *       ((WSAA-PREM-DAY2    = 30) AND                        <022>
	 *        (WSAA-PREM-MTH2    = 11))                           <022>
	 *        MOVE 1             TO WSAA-FACTOR.                  <022>
	 *                                                            <022>
	 *1990-EXIT.                                                  <022>
	 * </pre>
	 */
	protected void searchForTolerance1a00()
	{
		/*A10-GO*/
		/*  IF CHDRLNB-BILLFREQ        NOT = T5667-FREQ (WSAA-SUB)      */
		if (isNE(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()], t5667rec.freq[wsaaSub.toInt()])) {
			return ;
		}
		wsaaToleranceApp.set(t5667rec.prmtol[wsaaSub.toInt()]);
		wsaaAmountLimit.set(t5667rec.maxAmount[wsaaSub.toInt()]);
		/*A20-EXIT*/
	}

	protected void loadPayerDetails1b00(Payrpf payrpf)
	{
		loadPayer1b10(payrpf);
	}

	protected void loadPayer1b10(Payrpf payrpf)
	{
		/* Read the client role file to get the payer number.              */
		clrfIO.setForepfx(chdrlnbIO.getChdrpfx());
		clrfIO.setForecoy(payrpf.getChdrcoy());
		wsaaChdrnum.set(payrpf.getChdrnum());
		wsaaPayrseqno.set(payrpf.getPayrseqno());
		clrfIO.setForenum(wsaaPayrkey);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), Varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		/* Load the working storage table using the payer sequence         */
		/* number as the subscript.                                        */
		wsbbSub.set(payrpf.getPayrseqno());
		wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(payrpf.getIncomeSeqNo());
		wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(payrpf.getBillfreq());
		wsaaBillingInformationInner.wsaaBillchnl[wsbbSub.toInt()].set(payrpf.getBillchnl());
		wsaaBillingInformationInner.wsaaBillcd[wsbbSub.toInt()].set(payrpf.getBillcd());
		wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].set(payrpf.getBtdate());
		wsaaBillingInformationInner.wsaaBillcurr[wsbbSub.toInt()].set(payrpf.getBillcurr());
		wsaaBillingInformationInner.wsaaMandref[wsbbSub.toInt()].set(payrpf.getMandref());
		/* MOVE PAYR-GRUPKEY       TO WSAA-GRUPKEY(WSBB-SUB).      <014>*/
		wsaaBillingInformationInner.wsaaGrupkey[wsbbSub.toInt()].set(payrpf.getGrupnum());
		wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(clrfIO.getClntnum());
		wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(clrfIO.getClntcoy());
		payrpfBillingFreq = payrpf.getBillfreq();
		payrpfBillingChnl = payrpf.getBillchnl();
	}

	protected void adjustPremium1c00()
	{
		adjustPremiumCustomerSpecific();
		checkSuspense1c50();
	}

	protected void adjustPremiumCustomerSpecific()
	{
		adjustPremium1c10();
	}
	protected void adjustPremium1c10()
	{
		/*    Get the frequency Factor from DATCON3.                       */
		if (isNE(wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()], ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(chdrlnbIO.getBillcd());
			datcon3rec.intDate2.set(wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()]);
			datcon3rec.frequency.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, Varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				fatalError600();
			}
			/*    If the risk commencment is 28, 29, 30, of january,           */
			/*    30 march, 30 may, 30 august or the 30 of october             */
			/*    the initial instalment required is incorrectly               */
			/*    calculated as 1 month + 1 day * premium instead of           */
			/*    1 month(frequency 12)                                        */
			/*    MOVE CHDRLNB-OCCDATE            TO WSAA-PREM-DATE1      <022>*/
			/*    MOVE CHDRLNB-BILLCD             TO WSAA-PREM-DATE2      <022>*/
			/*    PERFORM 1900-CHECK-FREQ-DATE                            <022>*/
			/* Calculate the instalment premium.                               */
			wsaaFactor.set(datcon3rec.freqFactor);
			//PINNACLE-2696
			if(wsaaFactor.equals(0) &&  isEQ(t6654rec.renwdate1,"Y") && CTISS014Permission) {
				wsaaFactor.set(1);
			}
			compute(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()], 5).set(mult(wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()], wsaaFactor));
		}
		/* Add in the single premium.                                      */
		wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()]);
		/* If the tax relief method is not spaces calculate the tax        */
		/* relief amount and deduct it from the premium.                   */
		if (isNE(t5688rec.taxrelmth, SPACES)) {
			prasrec.clntnum.set(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()]);
			prasrec.clntcoy.set(wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()]);
			prasrec.incomeSeqNo.set(wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()]);
			prasrec.cnttype.set(chdrlnbIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			/* Use the due date unless a receipt exists with a date later      */
			/* then the due date.                                              */
			if (isEQ(rtrnsacIO.getEffdate(), varcom.vrcmMaxDate)) {
				prasrec.effdate.set(chdrlnbIO.getOccdate());
			}
			else {
				if (isGT(chdrlnbIO.getOccdate(), rtrnsacIO.getEffdate())) {
					prasrec.effdate.set(chdrlnbIO.getOccdate());
				}
				else {
					prasrec.effdate.set(rtrnsacIO.getEffdate());
				}
			}
			prasrec.company.set(chdrlnbIO.getChdrcoy());
			prasrec.grossprem.set(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
			prasrec.statuz.set(Varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz, Varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				fatalError600();
			}
			wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].subtract(prasrec.taxrelamt);
		}
		wsaaTotalPremium.add(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
	
		/* Add the contract fee to the instalment premium for              */
		/* payer No. 1.                                                    */
		/* IF SINGLE PREMIUM IS APPLIED CHECK IF ANY FEE IS NEEDED         */
		singlePremiumFeeCustomerSpecific();
		compute(wsaaBillingInformationInner.wsaaPremTax[wsbbSub.toInt()], 6).setRounded(add((mult(wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()], wsaaFactor)), wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()]));
		compute(wsaaTotalTax, 3).setRounded(add(wsaaTotalTax, wsaaBillingInformationInner.wsaaPremTax[wsbbSub.toInt()]));
		if (isEQ(wsbbSub, 1)) {
			if (isEQ(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()], ZERO)) {
				sv.cntfee.set(ZERO);
			}
			else {
				compute(sv.cntfee, 5).set(mult(sv.cntfee, wsaaFactor));
			}
			sv.cntfee.add(wsaaSingpFee);
			wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(sv.cntfee);
			if (isNE(wsaaSingpFee, ZERO)) {
				wsaaCntfee.set(wsaaSingpFee);
				checkCalcContTax7100();
				wsaaSingpfeeTax.set(wsaaTax);
			}
			compute(wsaaCntfeeTax, 6).setRounded(mult(wsaaCntfeeTax, wsaaFactor));
			compute(wsaaTotalTax, 3).setRounded(add(add(wsaaTotalTax, wsaaSingpfeeTax), wsaaCntfeeTax));
		}
		zrdecplrec.amountIn.set(wsaaTotalTax);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		callRounding8000();
		wsaaTotalTax.set(zrdecplrec.amountOut);
		sv.taxamt01.set(wsaaTotalTax);
		wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(wsaaTotalTax);
		 
		compute(sv.totlprm, 2).set(add(wsaaTotalPremium,sv.cntfee));
		compute(sv.totlprm, 2).add(wsaaTotalTax);
	}
	protected void singlePremiumFeeCustomerSpecific() {
		if (isEQ(wsaaSingPrmInd, "Y") && isNE(t5688rec.feemeth, SPACES)) {
			singPremFee1e00();
		}
	}
	protected void checkSuspense1c50()
	{
		/*  Code removed as Contract Suspense now caters for Suspense      */
		/*  Amount in currency other than Contract Currency.               */
		/*  Check if there is enough money in suspense to issue            */
		/*  this contract.                                                 */
		/*  Not to be included until pahse B  ************     */
		/* Get the payer suspense.                                         */
		/*   MOVE SPACES                 TO SACSLNB-PARAMS.               */
		/*   MOVE WSSP-COMPANY           TO SACSLNB-CHDRCOY.              */
		/*   MOVE CHDRLNB-CHDRNUM        TO WSAA-CHDRNUM.                 */
		/*   MOVE WSBB-SUB               TO WSAA-PAYRSEQNO.               */
		/*   MOVE WSAA-PAYRKEY           TO SACSLNB-CHDRNUM.              */
		/*   MOVE CHDRLNB-CNTCURR        TO SACSLNB-CNTCURR.              */
		/*   MOVE T5645-SACSCODE-01      TO SACSLNB-SACSCODE.             */
		/*   MOVE T5645-SACSTYPE-01      TO SACSLNB-SACSTYP.              */
		/*   MOVE READR                  TO SACSLNB-FUNCTION.             */
		/*   CALL 'SACSLNBIO'            USING SACSLNB-PARAMS.            */
		/*   IF SACSLNB-STATUZ           NOT = O-K                        */
		/*                           AND NOT = MRNF                       */
		/*      MOVE SACSLNB-PARAMS      TO SYSR-PARAMS                   */
		/*      PERFORM 600-FATAL-ERROR.                                  */
		/*   IF SACSLNB-STATUZ           = MRNF                           */
		/*      MOVE ZERO                TO WSAA-PAYR-SUSPENSE            */
		/*   ELSE                                                         */
		/*      IF T3695-SIGN            = '-'                            */
		/*         MULTIPLY SACSLNB-SACSCURBAL BY -1                      */
		/*                               GIVING WSAA-PAYR-SUSPENSE        */
		/*      ELSE                                                      */
		/*         MOVE SACSLNB-SACSCURBAL TO WSAA-PAYR-SUSPENSE.         */
		/*      ADD WSAA-PAYR-SUSPENSE   TO WSAA-TOTAL-SUSPENSE.          */
		/* Look up tolerance applicable                                    */
		/* MOVE ZERO                TO WSAA-TOLERANCE-APP,         <014>*/
		/*                             WSAA-AMOUNT-LIMIT.          <014>*/
		/* MOVE 1                      TO WSAA-SUB.                <014>*/
		/* PERFORM UNTIL WSAA-SUB > 11                             <014>*/
		/*    IF WSAA-BILLFREQ(WSBB-SUB) = T5667-FREQ(WSAA-SUB)    <014>*/
		/*       MOVE T5667-PRMTOL(WSAA-SUB) TO WSAA-TOLERANCE-APP <014>*/
		/*       MOVE T5667-MAX-AMOUNT(WSAA-SUB) TO WSAA-AMOUNT-LIMIT<01*/
		/*       MOVE 11                     TO WSAA-SUB           <014>*/
		/*    END-IF                                               <014>*/
		/*    ADD 1 TO WSAA-SUB                                    <014>*/
		/* END-PERFORM.                                            <014>*/
		/* Calculate the  tolerance applicable.                            */
		/* COMPUTE WSAA-CALC-TOLERANCE =                           <014>*/
		/*  (WSAA-TOLERANCE-APP * WSAA-INSTPRM(WSBB-SUB) ) / 100.  <014>*/
		/*    Check % amount is less than Limit on T5667, if so use this   */
		/*    else use Limit.                                              */
		/* IF WSAA-CALC-TOLERANCE < WSAA-AMOUNT-LIMIT              <014>*/
		/*    MOVE WSAA-CALC-TOLERANCE TO WSAA-TOLERANCE           <014>*/
		/* ELSE                                                    <014>*/
		/*    MOVE WSAA-AMOUNT-LIMIT   TO WSAA-TOLERANCE.          <014>*/
		/* If there is not enough money in the payer suspense account      */
		/* plus tolerance to cover the premium due check to see if         */
		/* there is enough money in contract suspense to cover the         */
		/* remainder (or all if payr suspense was zero) of the             */
		/* premium due.                                                    */
		/* If the remainder of the premium amount due is greater then      */
		/* the contract suspense plus the tolerance display an error       */
		/* message.                                                        */
		/* If there is enough money in  contract suspense reduce the       */
		/* contract suspense amount by the remainder of the premium due.   */
		/* MOVE WSAA-INSTPRM(WSBB-SUB) TO WSAA-AMNT-DUE.           <014>*/
		/* IF WSAA-AMNT-DUE > WSAA-PAYR-SUSPENSE + WSAA-TOLERANCE  <014>*/
		/*    SUBTRACT WSAA-PAYR-SUSPENSE  FROM WSAA-AMNT-DUE      <014>*/
		/*    IF WSAA-AMNT-DUE >  WSAA-CNT-SUSPENSE + WSAA-TOLERANCE<014*/
		/*       MOVE 'Y'              TO WSAA-INSUFFICIENT-SUSPENSE<014*/
		/*    ELSE                                                 <014>*/
		/*       SUBTRACT WSAA-AMNT-DUE FROM WSAA-CNT-SUSPENSE     <014>*/
		/* END-IF.                                                 <014>*/
		wsbbSub.add(1);
		/*C90-EXIT*/
	}

	protected void checkSuspense1c100()
	{
		/*    Read the Suspense file to see if any money has been          */
		/*    received for this contract. The search order for Suspense    */
		/*    details is Contract Currency; Billing Currency; Any          */
		/*    Currency. If Suspense is found set appropriate values.       */
		/*    But only if the Suspense amount is not zero.                 */
		if (isEQ(wsaaSub, 1)) {
			acblenqIO.setOrigcurr(chdrlnbIO.getCntcurr());
			acblenqIO.setFunction(Varcom.readr);
		}
		else {
			if (isEQ(wsaaSub, 2)) {
				acblenqIO.setOrigcurr(chdrlnbIO.getBillcurr());
				acblenqIO.setFunction(Varcom.readr);
			}
			else {
				acblenqIO.setOrigcurr(SPACES);
				acblenqIO.setFunction(varcom.begn);
			}
		}
		SmartFileCode.execute(appVars, acblenqIO);
		if ((isNE(acblenqIO.getStatuz(), Varcom.oK))
				&& (isNE(acblenqIO.getStatuz(), Varcom.mrnf))
				&& (isNE(acblenqIO.getStatuz(), Varcom.endp))) {
			syserrrec.params.set(acblenqIO.getParams());
			syserrrec.statuz.set(acblenqIO.getStatuz());
			fatalError600();
		}
		if(onePflag) {
			if(!t5645rec.sacscode02.equals(SPACES) || !t5645rec.sacstype02.equals(SPACES)) {
				if ((isEQ(acblenqIO.getStatuz(), Varcom.oK))
						&& (isEQ(acblenqIO.getRldgcoy(), wsspcomn.company))
						&& (isEQ(acblenqIO.getRldgacct(), chdrlnbIO.getChdrnum()))
						&& (isEQ(acblenqIO.getSacscode(), t5645rec.sacscode02))
						&& (isEQ(acblenqIO.getSacstyp(), t5645rec.sacstype02))
						&& (isNE(acblenqIO.getSacscurbal(), ZERO))) {
					wsaaSuspInd = "Y";
					if (isEQ(t3695rec.sign, "-")) {
						compute(wsaaTotalSuspense, 2).set(mult(acblenqIO.getSacscurbal(), -1));
					}
					else {
						wsaaTotalSuspense.set(acblenqIO.getSacscurbal());
					}
				}
			}
			else {
				if ((isEQ(acblenqIO.getStatuz(), Varcom.oK))
						&& (isEQ(acblenqIO.getRldgcoy(), wsspcomn.company))
						&& (isEQ(acblenqIO.getRldgacct(), chdrlnbIO.getChdrnum()))
						&& (isEQ(acblenqIO.getSacscode(), t5645rec.sacscode01))
						&& (isEQ(acblenqIO.getSacstyp(), t5645rec.sacstype01))
						&& (isNE(acblenqIO.getSacscurbal(), ZERO))) {
					wsaaSuspInd = "Y";
					if (isEQ(t3695rec.sign, "-")) {
						compute(wsaaTotalSuspense, 2).set(mult(acblenqIO.getSacscurbal(), -1));
					}
					else {
						wsaaTotalSuspense.set(acblenqIO.getSacscurbal());
					}
				}
			}
		}
		else {
			if ((isEQ(acblenqIO.getStatuz(), Varcom.oK))
					&& (isEQ(acblenqIO.getRldgcoy(), wsspcomn.company))
					&& (isEQ(acblenqIO.getRldgacct(), chdrlnbIO.getChdrnum()))
					&& (isEQ(acblenqIO.getSacscode(), t5645rec.sacscode01))
					&& (isEQ(acblenqIO.getSacstyp(), t5645rec.sacstype01))
					&& (isNE(acblenqIO.getSacscurbal(), ZERO))) {
				wsaaSuspInd = "Y";
				if (isEQ(t3695rec.sign, "-")) {
					compute(wsaaTotalSuspense, 2).set(mult(acblenqIO.getSacscurbal(), -1));
				}
				else {
					wsaaTotalSuspense.set(acblenqIO.getSacscurbal());
				}
			}
		}
		if(onePflag){ /* IBPLIFE-2274 */
			check1PSuspense();
		}
	}

	protected void suspenseAmount1c200()
	{
		readTableT5667();
		validAmount1c210();
	}

	protected void readTableT5667(){
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t5667.toString());
		wsaaT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsaaT5667Curr.set(acblenqIO.getOrigcurr());
		itempf.setItemitem(wsaaT5667Key.toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (null == itempf) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		}
	}
	protected void validAmount1c210()
	{
		/*    If the Suspense amount is in a currency other than the       */
		/*    Contract currency, the Premium & Contract Fee must first     */
		/*    be converted before validating against the Suspense Amount.  */
		PackedDecimalData suspense = new PackedDecimalData(17, 2).init(0);
	    PackedDecimalData totalSuspense = new PackedDecimalData(17, 2).init(0);
	    List<Acblpf> acblList = acblDao.searchAcblenqRecord(chdrlnbIO.chdrcoy.toString(), chdrlnbIO.getChdrnum().toString());
		for(Acblpf acblpfobj : acblList ) {
			if(acblpfobj != null && acblpfobj.getSacscurbal() != null){				
				if (isEQ(t3695rec.sign, "-"))
					compute(suspense, 2).set(mult(acblpfobj.getSacscurbal(), -1));
				else
					suspense.set(acblpfobj.getSacscurbal());
				compute(totalSuspense, 2).set(add(totalSuspense, suspense));
			}
		}
	    if(isGT(totalSuspense,ZERO)) {
	    	sv.premsusp.set(totalSuspense);
	    	sv.billcurr.set(acblenqIO.getOrigcurr());
	    }
		if (isEQ(chdrlnbIO.getCntcurr(), sv.billcurr)) {
			sv.exrat.set(1);
			sv.premCurr.set(wsaaTotalPremium);
			sv.pufee.set(sv.cntfee);
			compute(wsaaTotamnt, 2).set(add(wsaaTotalTax, (add(wsaaTotalPremium, sv.cntfee))));
			sv.taxamt02.set(wsaaTotalTax);
		}
		else {
			convertSuspense1c220();
		}
		validAmountCustomerSpecific();
		wsaaToleranceApp.set(ZERO);
		wsaaAmountLimit.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 11)); wsaaSub.add(1)){
			if (isEQ(wsaaBillingInformationInner.wsaaBillfreq[1], t5667rec.freq[wsaaSub.toInt()])) {
				wsaaToleranceApp.set(t5667rec.prmtol[wsaaSub.toInt()]);
				wsaaToleranceApp2.set(t5667rec.prmtoln[wsaaSub.toInt()]);
				wsaaAmountLimit.set(t5667rec.maxAmount[wsaaSub.toInt()]);
				wsaaAmountLimit2.set(t5667rec.maxamt[wsaaSub.toInt()]);
				wsaaSub.set(11);
			}
		}
		/* Calculate the tolerance applicable.                             */
		compute(wsaaCalcTolerance, 2).set(div((mult(wsaaToleranceApp, wsaaTotamnt)), 100));
		/*    Check % amount is less than Limit on T5667, if so use this   */
		/*    else use Limit.                                              */
		if (isLT(wsaaCalcTolerance, wsaaAmountLimit)) {
			wsaaTolerance.set(wsaaCalcTolerance);
		}
		else {
			wsaaTolerance.set(wsaaAmountLimit);
		}
		wsaaTolerance2.set(0);
		if (isGT(wsaaAmountLimit2, 0)) {
			if (isEQ(wsaaAgtTerminatedFlag, "N")
					|| (isEQ(wsaaAgtTerminatedFlag, "Y")
							&& isEQ(t5667rec.sfind, "2"))) {
				compute(wsaaCalcTolerance2, 2).set(div((mult(wsaaToleranceApp2, wsaaTotamnt)), 100));
				if (isLT(wsaaCalcTolerance2, wsaaAmountLimit2)) {
					wsaaTolerance2.set(wsaaCalcTolerance2);
				}
				else {
					wsaaTolerance2.set(wsaaAmountLimit2);
				}
			}
		}
		/* If the premium amount due is greater than the Contract          */
		/* Suspense plus the tolerance display an error message.           */
		/*    IF WSAA-TOTAMNT>(WSAA-TOTAL-SUSPENSE+WSAA-TOLERANCE) <V4L001>*/
		if ((setPrecision(wsaaTotamnt, 2)
				&& isGT(wsaaTotamnt, add(add(wsaaTotalSuspense, wsaaTolerance), rlpdlonrec.prmdepst)))) {
			if ((setPrecision(wsaaTotamnt, 2)
					&& isGT(wsaaTotamnt, add(add(wsaaTotalSuspense, wsaaAmountLimit2), rlpdlonrec.prmdepst)))) {
				wsaaInsufficientSuspense = "Y";
			}
		}
	}

	protected void convertSuspense1c220()
	{
		convert1c220();
	}

	/**
	 * <pre>
	 * Convert first the Total Premium & then the Contract fee
	 * into the suspense currency.
	 * </pre>
	 */
	protected void convert1c220()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.rateUsed.set(ZERO);
		conlinkrec.cashdate.set(hpadIO.getHprrcvdt());
		conlinkrec.currIn.set(chdrlnbIO.getCntcurr());
		conlinkrec.currOut.set(sv.billcurr);
		conlinkrec.amountIn.set(wsaaTotalPremium);
		conlinkrec.company.set(chdrlnbIO.getChdrcoy());
		conlinkrec.function.set("SURR");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, Varcom.oK)) {
			syserrrec.params.set(conlinkrec.clnk002Rec);
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		zrdecplrec.amountIn.set(conlinkrec.amountOut);
		zrdecplrec.currency.set(sv.billcurr);
		callRounding8000();
		conlinkrec.amountOut.set(zrdecplrec.amountOut);
		wsaaTotamnt.add(conlinkrec.amountOut);
		sv.premCurr.set(conlinkrec.amountOut);
		sv.exrat.set(conlinkrec.rateUsed);
		/* Convert the Contract fee.                                       */
		if (isNE(sv.cntfee, ZERO)) {
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.rateUsed.set(ZERO);
			conlinkrec.amountIn.set(sv.cntfee);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, Varcom.oK)) {
				syserrrec.params.set(conlinkrec.clnk002Rec);
				syserrrec.statuz.set(conlinkrec.statuz);
				fatalError600();
			}
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(sv.billcurr);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
			wsaaTotamnt.add(conlinkrec.amountOut);
			sv.pufee.set(conlinkrec.amountOut);
		}
		/* Convert the Total tax.                                          */
		if (isGT(wsaaTotalTax, ZERO)) {
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.rateUsed.set(ZERO);
			conlinkrec.amountIn.set(wsaaTotalTax);
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, Varcom.oK)) {
				syserrrec.params.set(conlinkrec.clnk002Rec);
				syserrrec.statuz.set(conlinkrec.statuz);
				fatalError600();
			}
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			zrdecplrec.currency.set(sv.billcurr);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
			wsaaTotamnt.add(conlinkrec.amountOut);
			sv.taxamt02.set(conlinkrec.amountOut);
		}
	}

	protected void checkAgentTerminate1c300()
	{
		/* Retrieve Today's date.                                          */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(chdrlnbIO.getAgntcoy());
		aglfIO.setAgntnum(chdrlnbIO.getAgntnum());
		aglfIO.setFunction(Varcom.readr);
		aglfIO.setFormat(formatsInner.aglfrec);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), Varcom.oK)
				&& isNE(aglfIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			fatalError600();
		}
		if (isEQ(aglfIO.getStatuz(), Varcom.mrnf)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.g772);
			agentErrorMessage1850();
		}
		wsaaAgtTerminatedFlag = "N";
		/* IF AGLF-DTETRM           < DTC1-INT-DATE  OR        <LFA1064>*/
		if (isLTE(aglfIO.getDtetrm(), datcon1rec.intDate)
				|| isLT(aglfIO.getDteexp(), datcon1rec.intDate)
				|| isGT(aglfIO.getDteapp(), datcon1rec.intDate)) {
			wsaaAgtTerminatedFlag = "Y";
		}
	}

	/**
	 * <pre>
	 *     PAYER LEVEL VALIDATION
	 * </pre>
	 */
	protected void payerValidation1d00()
	{
		checkDateOfDeath1d05();
	}

	protected void checkDateOfDeath1d05()
	{
		Clntpf clntpfCriteria = null;	
		/*  If the payer is different from the owner, ensure that the   */
		/*  payer is alive by checking for a date of death on the       */
		/*  client file.                                                */
		if (isNE(chdrlnbIO.getCownnum(), wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()])) {

			clntpfCriteria = new Clntpf();

			clntpfCriteria.setClntpfx("CN");
			clntpfCriteria.setClntcoy(wsspcomn.fsuco.toString());
			clntpfCriteria.setClntnum(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].toString());

			clntpfData = clntpfDAO.selectActiveClient(clntpfCriteria);//ILIFE-6270
			if(clntpfData != null){
				if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)
						&& isGT(chdrlnbIO.getOccdate(), clntpfData.getCltdod())) {
					errmesgrec.errmesgRec.set(SPACES);
					/*        MOVE F782           TO ERMS-EROR             <A06596>*/
					errmesgrec.eror.set(errorsInner.w343);
					sv.payrseqno.set(wsbbSub);
					wsaaExtraMsgpfx.set("PY");
					errorMessages1800();
				}
			}
		}

		readT36251d10();
		return;

	}

	protected void readT36251d10()
	{
		/*                                                         <014>*/
		/*Read T3625 to access the payment plan  details for the co<014>*/
		/*                                                         <014>*/
		/* MOVE SPACES                 TO ITEM-DATA-KEY.           <014>*/
		/* MOVE 'IT'                   TO ITEM-ITEMPFX.            <014>*/
		/* MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.            <014>*/
		/* MOVE T3625                  TO ITEM-ITEMTABL.           <014>*/
		/* MOVE 'LP'                   TO WSAA-T3625-LP.           <014>*/
		/* MOVE WSAA-BILLCHNL(WSBB-SUB)TO WSAA-T3625-BILLCHNL.     <014>*/
		/* MOVE WSAA-BILLFREQ(WSBB-SUB)TO WSAA-T3625-BILLFREQ.     <014>*/
		/* MOVE WSAA-T3625-KEY         TO ITEM-ITEMITEM.           <014>*/
		/* MOVE 'READR'                TO ITEM-FUNCTION.           <014>*/
		/*                                                         <014>*/
		/* CALL 'ITEMIO' USING ITEM-PARAMS.                        <014>*/
		/* IF ITEM-STATUZ              NOT = O-K                   <014>*/
		/*                         AND NOT = MRNF                  <014>*/
		/*     MOVE ITEM-STATUZ        TO SYSR-STATUZ              <014>*/
		/*     PERFORM 600-FATAL-ERROR.                            <014>*/
		/*                                                         <014>*/
		/* IF ITEM-STATUZ              = MRNF                      <014>*/
		/*    MOVE SPACES              TO ERMS-ERRMESG-REC         <014>*/
		/*    MOVE U023                TO ERMS-EROR                <016>*/
		/*    MOVE F941                TO ERMS-EROR                <016>*/
		/*    MOVE WSBB-SUB            TO S6378-PAYRSEQNO          <014>*/
		/*    PERFORM 1800-ERROR-MESSAGES                          <014>*/
		/*    GO TO 1D85-INCREMENT-SUB.                            <014>*/
		/*                                                         <014>*/
		/* MOVE ITEM-GENAREA           TO T3625-T3625-REC.         <014>*/
		/*                                                            <014>*/
		/*   Read T3620 to access the payment plan  details for the contrac*/


		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t3620.toString());
		itempf.setItemitem(wsaaBillingInformationInner.wsaaBillchnl[wsbbSub.toInt()].toString());
		itempf = itempfDAO. getItempfRecord(itempf);

		if (null == itempf) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.f921);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
			//goTo(GotoLabel.incrementSub1d85);
			incrementSub1d85();
			return;
		}
		t3620rec.t3620Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		validateMandate1d30();
		return;
	}

	protected void validateMandate1d30()
	{
		/* For non Direct Debit, check details don't exist on CHDR         */
		/* IF T3625-DDIND          NOT = 'Y'                       <014>*/
		if (isEQ(t3620rec.ddind, SPACES)
				&& isEQ(t3620rec.crcind, SPACES)) {
			if (isNE(wsaaBillingInformationInner.wsaaMandref[wsbbSub.toInt()], SPACES)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.i005);
				sv.payrseqno.set(wsbbSub);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
		}
		/* If method of payment is Rol over, then roll over details is mandatory at proposal screen*/
		if(ctaxPermission) {
			if(isEQ(t3620rec.rollovrind,"Y")) {
				if(!checkRollover()) {
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.rpj3);
					sv.payrseqno.set(wsbbSub);
					wsaaExtraMsgpfx.set(SPACES);
					errorMessages1800();
				}
			}
		}
		/* If method of payment is direct debit read the                   */
		/*  relative mandate record & client bank A/C details              */
		/*  IF T3625-DDIND  NOT = 'Y'                              <014>*/
		if (isEQ(t3620rec.ddind, SPACES)
				&& isEQ(t3620rec.crcind, SPACES)) {
			//goTo(GotoLabel.checkGroup1d50);
			checkGroup1d50();
			return;

		}
		mandlnbIO.setParams(SPACES);
		mandlnbIO.setPayrcoy(wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()]);
		mandlnbIO.setPayrnum(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()]);
		mandlnbIO.setMandref(wsaaBillingInformationInner.wsaaMandref[wsbbSub.toInt()]);
		mandlnbIO.setFunction(Varcom.readr);
		mandlnbIO.setFormat(formatsInner.mandlnbrec);
		SmartFileCode.execute(appVars, mandlnbIO);
		if (isNE(mandlnbIO.getStatuz(), Varcom.oK)
				&& isNE(mandlnbIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(mandlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(mandlnbIO.getStatuz(), Varcom.mrnf)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.i007);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
			//	goTo(GotoLabel.checkGroup1d50);
			checkGroup1d50();
			return;
		}
		/* Check the correct mandate type is present.                      */
		if (isNE(mandlnbIO.getCrcind(), SPACES)
				&& isNE(t3620rec.ddind, SPACES)) {
			errmesgrec.eror.set(errorsInner.i007);
			sv.payrseqno.set(wsbbSub);
			errorMessages1800();
			//goTo(GotoLabel.checkGroup1d50);
			checkGroup1d50();
			return;
		}
		if (isEQ(mandlnbIO.getCrcind(), SPACES)
				&& isNE(t3620rec.crcind, SPACES)) {
			errmesgrec.eror.set(errorsInner.i007);
			sv.payrseqno.set(wsbbSub);
			errorMessages1800();
			//goTo(GotoLabel.checkGroup1d50);
			checkGroup1d50();
			return;
		}

		//MIBT-365
		//
		//Credit card Status check validation
		if(isEQ(mandlnbIO.getCrcind(), "C")){
			ccdtIO.setRecKeyData(SPACES);		
			ccdtIO.setFormat(ccdtrec);
			ccdtIO.setCrdtcard(mandlnbIO.getBankacckey());		
			ccdtIO.setCrcardexpy(9999);
			ccdtIO.setCrcardexpm(12);
			ccdtIO.setFunction(varcom.begn);
			/* Read CCDT to retrieve Credit Card Status*/
			SmartFileCode.execute(appVars, ccdtIO);
			if (isNE(ccdtIO.getStatuz(), Varcom.oK)
					&& isNE(ccdtIO.getStatuz(), Varcom.mrnf)) {
				syserrrec.statuz.set(ccdtIO.getStatuz());
				syserrrec.params.set(ccdtIO.getParams());
				fatalError600();
			}
			if (isEQ(ccdtIO.getStatuz(), Varcom.mrnf)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.rfh7);
				sv.payrseqno.set(wsbbSub);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
			wsaaValidCcStatus.set(ccdtIO.getCrcstat());
			/* If invalid Credit Card status issue an error*/
			if (!validCcStatus.isTrue()) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.rfh1);
				sv.payrseqno.set(wsbbSub);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
		}
		//

		/* Read CLBL Bank Details.                                         */
		clblIO.setBankkey(mandlnbIO.getBankkey());
		clblIO.setBankacckey(mandlnbIO.getBankacckey());
		String creditCardInformation=CreditCardUtility.getInstance().getCreditCard(clblIO.getBankacckey().toString().trim());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			clblIO.setBankacckey(creditCardInformation);
		}
		clblIO.setClntcoy(mandlnbIO.getPayrcoy());
		clblIO.setClntnum(mandlnbIO.getPayrnum());
		clblIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(), Varcom.oK)
				&& isNE(clblIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(clblIO.getParams());
			syserrrec.statuz.set(clblIO.getStatuz());
			fatalError600();
		}
		/* If bank A/C not on file display message                         */
		if (isEQ(clblIO.getStatuz(), Varcom.mrnf)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.f826);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
			//goTo(GotoLabel.checkGroup1d50);
			checkGroup1d50();
			return;
		}
		/* Read table T3678                                                */


		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.fsuco.toString());
		itempf.setItemtabl(tablesInner.t3678.toString());
		itempf.setItemitem(mandlnbIO.getMandstat().toString());
		itempf = itempfDAO. getItempfRecord(itempf);
		if(null != itempf)
			t3678rec.t3678Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		/* The mandate and bank A/C details exist so validate the          */
		/* mandate record against the contract header and client           */
		/* bank account record.                                            */
		if (isLT(wsaaBillingInformationInner.wsaaBillcd[wsbbSub.toInt()], mandlnbIO.getEffdate())) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.i008);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		if (isNE(mandlnbIO.getMandAmt(), 0)) {
			if (isNE(mandlnbIO.getMandAmt(), wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()])) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.i009);
				sv.payrseqno.set(wsbbSub);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
		}
		if (isNE(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()], clblIO.getClntnum())) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.i004);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		if (isNE(wsaaBillingInformationInner.wsaaBillcurr[wsbbSub.toInt()], clblIO.getCurrcode())
				&& isNE(mandlnbIO.getCrcind(), "C")) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.i010);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		// ILIFE-2476 START
		/*if (isEQ(t3678rec.gonogoflg, "N")) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.i014);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}*/
		// ILIFE-2476 END
		/*If the Current From date of the bank account is greater than the */
		/*effective date of the mandate (i.e. the bank account is not yet  */
		/*effective) then display an error message.                        */
		if (isGT(clblIO.getCurrfrom(), mandlnbIO.getEffdate())) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.i020);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		/*If the Current To date of the bank account is less than the      */
		/*effective date of the mandate (i.e. the bank account is no       */
		/*longer in force) then display an error message.                  */
		if (isNE(clblIO.getCurrto(), ZERO)) {
			if (isLT(clblIO.getCurrto(), mandlnbIO.getEffdate())) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.i020);
				sv.payrseqno.set(wsbbSub);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
		}

		checkGroup1d50();
		return;
	}

	protected boolean checkRollover(){
		rlvrpf = rlvrpfDAO.readRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if(rlvrpf!=null)
			return true;
		else
			return false;
	}

	protected void checkGroup1d50()
	{
		/* For non Group Billing, check details don't exist on CHDR        */
		/* IF T3625-GRPIND        NOT  = 'Y' AND                   <014>*/
		if (isEQ(t3620rec.grpind, SPACES)
				&& isNE(wsaaBillingInformationInner.wsaaGrupkey[wsbbSub.toInt()], SPACES)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e571);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
			//goTo(GotoLabel.incrementSub1d85);
			incrementSub1d85();
			return;
		}
		/* IF T3625-GRPIND  NOT = 'Y'                              <014>*/
		if (isEQ(t3620rec.grpind, SPACES)) {
			//goTo(GotoLabel.incrementSub1d85);
			incrementSub1d85();
			return;
		}
		/* For Group Billing, check details exist on CHDR                  */
		if (isEQ(wsaaBillingInformationInner.wsaaGrupkey[wsbbSub.toInt()], SPACES)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e714);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
			//goTo(GotoLabel.incrementSub1d85);
			incrementSub1d85();
			return;
		}
		/* Read Group Details.                                             */
		grpsIO.setGrupcoy(chdrlnbIO.getChdrcoy());
		grpsIO.setGrupnum(wsaaBillingInformationInner.wsaaGrupkey[wsbbSub.toInt()]);
		grpsIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, grpsIO);
		if (isNE(grpsIO.getStatuz(), Varcom.oK)
				&& isNE(grpsIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(grpsIO.getParams());
			syserrrec.statuz.set(grpsIO.getStatuz());
			fatalError600();
		}
		/* For Group Billing, check details exist.                         */
		if (isEQ(grpsIO.getStatuz(), Varcom.mrnf)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e714);
			sv.payrseqno.set(wsbbSub);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}

		incrementSub1d85();
		return;
	}

	protected void incrementSub1d85()
	{
		wsbbSub.add(1);
		/*D90-EXIT*/
	}

	protected void singPremFee1e00()
	{
		singPremFeePara1e00();
	}

	protected void singPremFeePara1e00()
	{
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set("00");
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(wsspcomn.company);
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, Varcom.oK)
				&& isNE(mgfeelrec.statuz, Varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			fatalError600();
		}
		singPremFeeCustomerSepcific();
		wsaaSingpFee.set(mgfeelrec.mgfee);
	}

	/**
	 * <pre>
	 *    Sections performed from the 1000 section above.
	 *      (including subfile load section)
	 * </pre>
	 */
	protected void checkAgents1f00()
	{
		check1f10();
	}

	protected void check1f10()
	{
		Clntpf clntpfCriteria = null;
		List<Pcddpf> pcddpfList = null;	
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/* Check that the servicing agent is alive by reading the       */
		/* client record and checking for a date of death.              */
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(chdrlnbIO.getAgntnum());
		agntIO.setFunction(Varcom.readr);
		agntIO.setFormat(formatsInner.agntrec);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), Varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		clntpfCriteria = new Clntpf();

		clntpfCriteria.setClntpfx(agntIO.getClntpfx().toString());
		clntpfCriteria.setClntcoy(agntIO.getClntcoy().toString());
		clntpfCriteria.setClntnum(agntIO.getClntnum().toString());

		clntpfData = clntpfDAO.selectActiveClient(clntpfCriteria);//ILIFE-6270

		if(clntpfData == null){
			syserrrec.statuz.set(varcom.mrnf);
			syserrrec.params.set("");
			fatalError600();
		}
		if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e510);
			agentErrorMessage1850();
		}
		/* Read the Agent Life record to obtain the appointed, expired  */
		/* and terminated dates for the agent.  If no record is found,  */
		/* display an error.  Check the agent is current by comparing   */
		/* the above dates against the effective date.                  */
		/* Check whether the agent is on black list.                    */
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(chdrlnbIO.getAgntcoy());
		aglfIO.setAgntnum(chdrlnbIO.getAgntnum());
		aglfIO.setFormat(formatsInner.aglfrec);
		aglfIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), Varcom.oK)
				&& isNE(aglfIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(aglfIO.getStatuz(), Varcom.mrnf)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e508);
			agentErrorMessage1850();
		}
		/* IF  AGLF-DTETRM             <  CHDRLNB-OCCDATE  OR  <LFA1064>*/
		if (isLTE(aglfIO.getDtetrm(), chdrlnbIO.getOccdate())
				|| isLT(aglfIO.getDteexp(), chdrlnbIO.getOccdate())
				|| isGT(aglfIO.getDteapp(), chdrlnbIO.getOccdate())) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e507);
			agentErrorMessage1850();
		}
		wsaaBlackList.set(aglfIO.getTagsusind());
		if (noIssue.isTrue()
				&& isEQ(wsaaAgentSuspend, "N")) {
			wsaaAgentSuspend = "Y";
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.tl01);
			agentErrorMessage1850();
		}
		
		//IJL-8
		if(japanLocPermission) {
			
			if(null == aglfIO.getTlaglicno() || isEQ(aglfIO.getTlaglicno(), SPACES)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.jl14);
				agentErrorMessage1850();
			}
			if (isNE(aglfIO.getTlicexpdt(), varcom.vrcmMaxDate)
					&& isLT(aglfIO.getTlicexpdt(), wsaaToday)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.tl43);
				agentErrorMessage1850();
			}
		} else {
			if (isNE(aglfIO.getTlicexpdt(), varcom.vrcmMaxDate)
				&& isLT(aglfIO.getTlicexpdt(), wsaaToday)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.tl43);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		}
		}
		
	
		/* Check if there are any other agents receiving a split of     */
		/* the commission.  For each one found, check that the agent    */
		/* concerned is both alive and active.                          */
		pcddpfList = pcddpfDAO.searchPcddRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

		for(Pcddpf pcddpf : pcddpfList){

			if (isNE(pcddpf.getChdrcoy(), chdrlnbIO.getChdrcoy())
					|| isNE(pcddpf.getChdrnum(), chdrlnbIO.getChdrnum())) {
				break;
			}

			checkSplitAgents1f50(pcddpf);
		}
	}

	protected void checkSplitAgents1f50(Pcddpf pcddpf)
	{
		check1f60(pcddpf);
	}

	protected void check1f60(Pcddpf pcddpf)
	{
		Clntpf clntpfCriteria = null;
		/* Check that the split commission agent is alive by reading       */
		/* the client record and checking for a date of death.             */
		if (isEQ(pcddpf.getChdrcoy(), wsspcomn.company)
				&& isEQ(pcddpf.getAgntNum(), chdrlnbIO.getAgntnum())) {
			return ;
		}
		agntIO.setAgntpfx("AG");
		agntIO.setAgntcoy(wsspcomn.company);
		agntIO.setAgntnum(pcddpf.getAgntNum());
		agntIO.setFunction(Varcom.readr);
		agntIO.setFormat(formatsInner.agntrec);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), Varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		clntpfCriteria = new Clntpf();

		clntpfCriteria.setClntpfx(agntIO.getClntpfx().toString());
		clntpfCriteria.setClntcoy(agntIO.getClntcoy().toString());
		clntpfCriteria.setClntnum(agntIO.getClntnum().toString());

		clntpfData = clntpfDAO.selectActiveClient(clntpfCriteria);//ILIFE-6270

		if(clntpfData == null){
			syserrrec.statuz.set(varcom.mrnf);
			syserrrec.params.set("");
			fatalError600();
		}
		if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e510);
			agentErrorMessage1850();
		}
		/* Read the Agent Life record to obtain the appointed, expired  */
		/* and terminated dates for the agent.  If no record is found,  */
		/* display an error.  Check the agent is current by comparing   */
		/* the above dates against the effective date.                  */
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(pcddpf.getChdrcoy());
		aglfIO.setAgntnum(pcddpf.getAgntNum());
		aglfIO.setFormat(formatsInner.aglfrec);
		aglfIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), Varcom.oK)
				&& isNE(aglfIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		if (isEQ(aglfIO.getStatuz(), Varcom.mrnf)) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e508);
			agentErrorMessage1850();
		}
		/* IF  AGLF-DTETRM             <  CHDRLNB-OCCDATE OR   <LFA1064>*/
		if (isLTE(aglfIO.getDtetrm(), chdrlnbIO.getOccdate())
				|| isLT(aglfIO.getDteexp(), chdrlnbIO.getOccdate())
				|| isGT(aglfIO.getDteapp(), chdrlnbIO.getOccdate())) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.e507);
			agentErrorMessage1850();
		}
		
		if(japanLocPermission) {
			if(null == aglfIO.getTlaglicno() || isEQ(aglfIO.getTlaglicno(), SPACES)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.jl14);
				agentErrorMessage1850();
			}
			if (isNE(aglfIO.getTlicexpdt(), varcom.vrcmMaxDate)
					&& isLT(aglfIO.getTlicexpdt(), wsaaToday)) {
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.tl43);
				agentErrorMessage1850();
			}
		}
		
		
	}

	protected void callHcrtfup1g00(Lifepf lifepf)
	{	
		//ILJ-108 starts
		if(undwjpnFlag){
			followUpsjpn(lifepf);
		}
		else{
			followUps1g10(lifepf);
		}
	}

	protected void followUps1g10(Lifepf lifepf)
	{
		if (isEQ(chkrlrec.lrkcls[wsaaSub.toInt()], SPACES)) {
			return ;
		}
		/* Check for automatic follow-ups by calling HCRTFUP               */
		hcrtfuprec.statuz.set(Varcom.oK);
		hcrtfuprec.chdrcoy.set(lifepf.getChdrcoy());
		hcrtfuprec.chdrnum.set(lifepf.getChdrnum());
		hcrtfuprec.cnttype.set(chdrlnbIO.getCnttype());
		hcrtfuprec.lrkcls.set(chkrlrec.lrkcls[wsaaSub.toInt()]);
		hcrtfuprec.life.set(lifepf.getLife());
		hcrtfuprec.jlife.set(lifepf.getJlife());
		hcrtfuprec.lifcnum.set(lifepf.getLifcnum());
		hcrtfuprec.anbccd.set(lifepf.getAnbAtCcd());
		hcrtfuprec.tranno.set(chdrlnbIO.getTranno());
		hcrtfuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		hcrtfuprec.language.set(wsspcomn.language);
		hcrtfuprec.fsuco.set(wsspcomn.fsuco);
		hcrtfuprec.user.set(varcom.vrcmUser);
		followUpsCustomerSpecific();
		hcrtfuprec.transactionTime.set(varcom.vrcmTime);
		hcrtfuprec.transactionDate.set(varcom.vrcmDate);
		callProgram(Hcrtfup.class, hcrtfuprec.crtfupRec);
		if (isNE(hcrtfuprec.statuz, Varcom.oK)) {
			syserrrec.params.set(hcrtfuprec.crtfupRec);
			syserrrec.statuz.set(hcrtfuprec.statuz);
			fatalError600();
		}
	}
	protected void followUpsjpn(Lifepf lifepf)
	{
		jundwfuprec.statuz.set(Varcom.oK);
		jundwfuprec.chdrcoy.set(lifepf.getChdrcoy());
		jundwfuprec.chdrnum.set(lifepf.getChdrnum());
		jundwfuprec.cnttype.set(chdrlnbIO.getCnttype());
		jundwfuprec.lrkcls.set(chkrlrec.lrkcls[wsaaSub.toInt()]);
		jundwfuprec.life.set(lifepf.getLife());
		jundwfuprec.jlife.set(lifepf.getJlife());
		jundwfuprec.lifcnum.set(lifepf.getLifcnum());
		jundwfuprec.anbccd.set(lifepf.getAnbAtCcd());
		jundwfuprec.tranno.set(chdrlnbIO.getTranno());
		jundwfuprec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		jundwfuprec.language.set(wsspcomn.language);
		jundwfuprec.fsuco.set(wsspcomn.fsuco);
		jundwfuprec.user.set(varcom.vrcmUser);
		jundwfuprec.transactionTime.set(varcom.vrcmTime);
		jundwfuprec.transactionDate.set(varcom.vrcmDate);
		callProgram(Jundwfup.class, jundwfuprec.crtfupRec);
		if (isNE(jundwfuprec.statuz, Varcom.oK)) {
			syserrrec.params.set(jundwfuprec.crtfupRec);
			syserrrec.statuz.set(jundwfuprec.statuz);
			fatalError600();
		}
	}
//ILJ-108 ends
	protected void mandatoryBnfy1h00()
	{
		manda1h10();
	}

	protected void manda1h10()
	{
		/*  Check for Mandatory Beneficiary's relation using TR52Z.        */
		wsaaIzc.set(ZERO);
		wsaaFound.set(SPACES);

		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.tr52z.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf = itempfDAO. getItempfRecord(itempf);
		if (null == itempf) {
			itempf = new Itempf();
			itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.tr52z.toString());
			itempf.setItemitem("***");
			itempf = itempfDAO. getItempfRecord(itempf);
		}
		if (null != itempf) {
			tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
		else {
			tr52zrec.tr52zRec.set(SPACES);
		}

		for (wsaaIuc.set(1); !(isGT(wsaaIuc, 10)); wsaaIuc.add(1)){
			if (isNE(tr52zrec.bnytype[wsaaIuc.toInt()], SPACES)
					&& isEQ(tr52zrec.manopt[wsaaIuc.toInt()], "Y")) {
				wsaaIzc.add(1);
				for (wsaaIvc.set(1); !(isGT(wsaaIvc, wsaaIwc)
						|| isEQ(wsaaMandatory[wsaaIzc.toInt()], "Y")); wsaaIvc.add(1)){
					wsaaMandatory[wsaaIzc.toInt()].set("N");
					if (isEQ(tr52zrec.bnytype[wsaaIuc.toInt()], wsaaBnytype[wsaaIvc.toInt()])) {
						wsaaMandatory[wsaaIzc.toInt()].set("Y");
					}
				}
			}
		}
		if (isGT(wsaaIuc, 10)) {
			wsaaIuc.set(1);
			while ( !(notFound.isTrue())) {
				continueCheck1i00();
			}

		}
	}

	protected void continueCheck1i00()
	{
		cont1i10();
	}

	protected void cont1i10()
	{
		if (isEQ(tr52zrec.gitem, SPACES)) {
			wsaaFound.set("N");
			return ;
		}

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.tr52z.toString());
		itempf.setItemitem(tr52zrec.gitem.toString());
		itempf = itempfDAO. getItempfRecord(itempf);
		if( null != itempf)
			tr52zrec.tr52zRec.set(StringUtil.rawToString(itempf.getGenarea()));
		for (wsaaIuc.set(1); !(isGT(wsaaIuc, 10)); wsaaIuc.add(1)){
			if (isNE(tr52zrec.bnytype[wsaaIuc.toInt()], SPACES)
					&& isEQ(tr52zrec.manopt[wsaaIuc.toInt()], "Y")) {
				wsaaIzc.add(1);
				for (wsaaIvc.set(1); !(isGT(wsaaIvc, wsaaIwc)
						|| isEQ(wsaaMandatory[wsaaIzc.toInt()], "Y")); wsaaIvc.add(1)){
					wsaaMandatory[wsaaIzc.toInt()].set("N");
					if (isEQ(tr52zrec.bnytype[wsaaIuc.toInt()], wsaaBnytype[wsaaIvc.toInt()])) {
						wsaaMandatory[wsaaIzc.toInt()].set("Y");
					}
				}
			}
		}
	}
	protected void stpCheckingCustomerSpecific(){	
		
		if (isEQ(hpadIO.getHuwdcdte(), varcom.vrcmMaxDate)
				&& isEQ(th506rec.mandatory02, "Y")) {
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.hl05);
			wsaaExtraMsgpfx.set(SPACES);
			errorMessages1800();
		
			//if underwriting decision date is not,save the data in database 
			//get every record to call interface
			//=============ICIL-611==================
				if(ctcnl002Permission){
					if(chdrlnbIO.getStatcode().equals(chdrStatCdoe)){
						AppVars appVars = AppVars.getInstance();
						appVars.addDiagnostic("This is call WP(underwriting) URL");
						String WPURL = AppConfig.getWPURL();
						JSONObject json=new JSONObject();
						json.put("comment", "String");
						json.put("functionType", "0");
						json.put("policyNo",chdrlnbIO.getChdrnum()+"");
						json.put("status", "Active");
						json.put("tranId", getUUID());
						int result = doPost(WPURL, json);
						//JOptionPane.showMessageDialog(null, result, "alert", JOptionPane.ERROR_MESSAGE); 
						if(result==200){
							LOGGER.info("successful");
							
						}else if(result==201){
							LOGGER.info("Created");
						}else if(result==401){
							LOGGER.info("Unauthorized");
						}else if(result==403){
							LOGGER.info("Forbidden");
						}else if(result==404){
							LOGGER.info("Not Found");
						}else if(result==405){
							LOGGER.info("Invalid input");
						}
					}
					
				}
				
		}
		
	}
	/**
	 * <pre>
	 *     RETRIEVE SCREEN FIELDS AND EDIT
	 * </pre>
	 */
	protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isGT(sv.taxamt01, ZERO)
				|| isGT(sv.taxamt02, ZERO)) {
			sv.taxamt01Out[varcom.nd.toInt()].set("N");
			sv.taxamt01Out[varcom.pr.toInt()].set("N");
			sv.taxamt02Out[varcom.nd.toInt()].set("N");
			sv.taxamt02Out[varcom.pr.toInt()].set("N");
		}
		scrnparams.function.set(varcom.prot);
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S6378IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6378-DATA-AREA                         */
		/*                         S6378-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(Varcom.oK);
		/*VALIDATE-SCREEN*/
		/**    Validate fields*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		scrnparams.function.set(varcom.srnch);
		processScreen("S6378", sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
				&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isEQ(scrnparams.statuz, Varcom.endp))) {
			validateSubfile2600();
		}

		/*EXIT*/
	}

	/**
	 * <pre>
	 *    Sections performed from the 2000 section above.
	 *          (Screen validation)
	 * </pre>
	 */
	protected void validateSubfile2600()
	{
		validation2610();
		readNextModifiedRecord2680();
	}

	protected void validation2610()
	{
		/**    Validate subfile fields*/
		/*UPDATE-ERROR-INDICATORS*/
		
		scrnparams.function.set(varcom.supd);
		processScreen("S6378", sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S6378", sv);
		if (isNE(scrnparams.statuz, Varcom.oK)
				&& isNE(scrnparams.statuz, Varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	 * <pre>
	 *    Sections performed from the 2600 section above.
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*  Update database files as required*/
		/* If there are no validation errors in the screen then the*/
		/* available issue flag is set to 'y'.*/
		/* If validation errors exist, then the available for issue        */
		/* flag is set to 'N'.                                             */
		if (isEQ(wsaaErrorFlag, "Y")) {
			chdrlnbIO.setAvlisu("N");
		}
		else {
			chdrlnbIO.setAvlisu("Y");
		}
		/*    GO TO 3090-EXIT.                                          */
		/* MOVE 'Y'                    TO CHDRLNB-AVLISU.               */
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
		if(contDtCalcFlag && isEQ("P5074", wsspcomn.lastprog)){
			List<Covtpf> covtpfNewList = null;
			Covtpf covtobj = new Covtpf();
			covtpfNewList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(),
					chdrlnbIO.getChdrnum().toString());
			for(Covtpf covtpfitem : covtpfNewList) {
				covtobj.setChdrcoy(covtpfitem.getChdrcoy());
				covtobj.setChdrnum(covtpfitem.getChdrnum());
				covtobj.setLife(covtpfitem.getLife());
				covtobj.setCoverage(covtpfitem.getCoverage());
				covtobj.setRider(covtpfitem.getRider());
				covtobj.setUniqueNumber(covtpfitem.getUniqueNumber());
				covtobj.setEffdate(wsspcomn.effdate.toInt());
				covtpfDAO.updateCovtpf(covtobj);
			}
		}
	}

	/**
	 * <pre>
	 *    Sections performed from the 3000 section above.
	 *     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		/* When the Fast track selection is being used, and an error has   */
		/* been found by Pre-issue validation the system must return to    */
		/* the previous program.                                           */
		if (isNE(errmesgrec.eror, SPACES)) {
			wsspcomn.programPtr.subtract(1);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			errmesgrec.eror.set(" ");
		}
		else {
			wsspcomn.programPtr.add(1);
		}
		/*EXIT*/
	}



	protected void readT66405100()
	{	
		itdmpfList = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), tablesInner.t6640.toString(), covttrm.getCrtable(), chdrlnbIO.getOccdate().toInt());

		if (itdmpfList == null || itdmpfList.size() == 0) {
			t6640rec.zrmandind.set(SPACES);
		}
		else {
			t6640rec.t6640Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		}
	}

	protected void calcWaiveSumins5200(Covtpf covttrm) //ILIFE-5221 by dpuhawan
	{
		para5210(covttrm);
	}

	protected void para5210(Covtpf covttrm)
	{
		wsaaTr517Rec.set(tr517rec.tr517Rec);
		wsaaWaiveSumins.set(ZERO);
		/* MOVE SPACES                 TO COVTLNB-PARAMS.       <CAS1.0>*/
		/* MOVE SPACES                 TO COVTLNB-LIFE          <CAS1.0>*/
		/*                                COVTLNB-COVERAGE      <CAS1.0>*/
		/*                                COVTLNB-RIDER.        <CAS1.0>*/
		/* MOVE ZEROES                 TO COVTLNB-SEQNBR.       <CAS1.0>*/

		callCovtlnbio5210(covttrm);
		return;
	}

	protected void callCovtlnbio5210(Covtpf covttrm) //ILIFE-5221 by dpuhawan
	{
		List<Covtpf> covtpfList = null;
		boolean exitMethod = false;

		covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

		tr517rec.tr517Rec.set(wsaaTr517Rec);

		for(Covtpf covtpf : covtpfList){

			if (isNE(covtpf.getChdrcoy(), chdrlnbIO.getChdrcoy())
					|| isNE(covtpf.getChdrnum(), chdrlnbIO.getChdrnum())) {
				//goTo(GotoLabel.exit5290);
				return;
			}
			if (isNE(covtpf.getLife(), covttrm.getLife()) //ILIFE-5221 by dpuhawan
					&& isEQ(tr517rec.zrwvflg02, "N")) {
				/*        GO TO 5290-EXIT                                  <LA1168>*/
				continue;
			}
			/* IF COVTLNB-COVERAGE         NOT = COVTTRM-COVERAGE   <LFA1058*/
			/*    MOVE ENDP                TO COVTLNB-STATUZ        <LFA1058*/
			/*    GO TO 5290-EXIT                                   <LFA1058*/
			/* END-IF.                                                      */
			if (isEQ(tr517rec.zrwvflg04, "Y")) {
				if (isNE(covtpf.getCoverage(), covttrm.getCoverage())) { //ILIFE-5221 by dpuhawan
					exitMethod = true;
					break;
				}
			}
			wsaaWaiveIt = "N";

			check5220(covtpf);
		}

		if(exitMethod){
			//goTo(GotoLabel.exit5290);
			return;
		}
	}

	protected void check5220(Covtpf covtpf)
	{
		for (sub1.set(1); !(isGT(sub1, 50)
				|| isEQ(wsaaWaiveIt, "Y")); sub1.add(1)){
			if (isEQ(covtpf.getCrtable(), tr517rec.ctable[sub1.toInt()])) {
				wsaaWaiveIt = "Y";
			}
		}
		if (isNE(wsaaWaiveIt, "Y")
				&& isNE(tr517rec.contitem, SPACES)) {
			readTr517500c();
			if (isEQ(wsaaWaiveCont, "Y")) {
				check5220(covtpf);
				return ;
			}
		}
		/*    IF WSAA-WAIVE-IT            = 'Y'                    <V42003>*/
		/*        ADD COVTLNB-INSTPREM    TO WSAA-WAIVE-SUMINS     <V42003>*/
		/*    END-IF.                                              <V42003>*/
		if (isEQ(tr517rec.zrwvflg04, "Y")
				&& isEQ(wsaaWaiveIt, "Y")) {
			if (isEQ(covtpf.getRider(), "00")) {
				/*        ADD COVTLNB-SUMINS          TO WSAA-WAIVE-SUMINS      */
				wsaaWaiveSumins.set(covtpf.getSumins());
				wsaaMainCrtable.set(covtpf.getCrtable());
				wsaaMainCoverage.set(covtpf.getCoverage());
				wsaaMainCessdate.set(covtpf.getRcesdte());
				wsaaMainPcessdte.set(covtpf.getPcesdte());
				wsaaMainMortclass.set(covtpf.getMortcls());
				wsaaMainLife.set(covtpf.getLife());
			}
			else {
				compute(wsaaWaiveSumins, 2).set(sub(wsaaWaiveSumins, covtpf.getSumins()));
				a300CalcBenefitAmount();
			}
		}
		else {
			if (isEQ(wsaaWaiveIt, "Y")) {
				wsaaWaiveSumins.add(PackedDecimalData.parseObject(covtpf.getInstprem()));
			checkCalcCompTax7000(covtpf);
			wsaaWaiveSumins.add(wsaaCompTax);
			}
		}
	}

	protected void readTr517500c()
	{
		wsaaWaiveCont = "N";
		wsaaZrwvflgs.set(tr517rec.zrwvflgs);
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItmfrm(new BigDecimal(chdrlnbIO.getOccdate().toString()));
		itempf.setItmto(new BigDecimal(chdrlnbIO.getOccdate().toString()));
		itempf.setItemtabl(tablesInner.tr517.toString());
		itempf.setItemitem( tr517rec.contitem.toString());
		itempf.setValidflag("1");
		List<Itempf> itempfList = itemDAO.findByItemDates(itempf);

		if (itempfList == null || itempfList.size() == 0) {
			return ;
		}
		wsaaWaiveCont = "Y";
		tr517rec.tr517Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		tr517rec.zrwvflgs.set(wsaaZrwvflgs);
	}

	protected void checkCalcCompTax7000(Covtpf covtpf)
	{
		start7010(covtpf);
	}

	protected void start7010(Covtpf covtpf)
	{
		wsaaCompTax.set(0);
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		// changed by yy for ILIFE-3960
		if (covtpf.getSingp().compareTo(BigDecimal.ZERO)==0
				&& covtpf.getInstprem().compareTo(BigDecimal.ZERO)==0) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtpf.getCrtable());
		readTr52e7200();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		/* Call TR52D tax subroutine                                       */
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(covtpf.getLife());
		txcalcrec.coverage.set(covtpf.getCoverage());
		txcalcrec.rider.set(covtpf.getRider());
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.crtable.set(covtpf.getCrtable());
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(chdrlnbIO.getOccdate());
		txcalcrec.transType.set("PREM");
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		if (isNE(covtpf.getInstprem(), ZERO)) {
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(covtpf.getZbinstprem());
			}
			else {
				txcalcrec.amountIn.set(covtpf.getInstprem());
			}
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, Varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
					|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[2]);
				}
			}
		}
		// changed by yy for ILIFE-3960
		if (covtpf.getSingp().compareTo(BigDecimal.ZERO)!=0) {
			txcalcrec.amountIn.set(covtpf.getSingp());
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, Varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
					|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[2]);
				}
			}
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
				|| isGT(txcalcrec.taxAmt[2], ZERO)) {
					if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
						wsaaCompTax.add(txcalcrec.taxAmt[1]);
					}
					if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
						wsaaCompTax.add(txcalcrec.taxAmt[2]);
					}
				}
	}

	protected void checkCalcContTax7100()
	{
		wsaaTax.set(0);
		if (isEQ(mgfeelrec.mgfee, ZERO)) {
			return ;
		}
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		readTr52e7200();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		//IBPLIFE-4262
		List<Covtpf> covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		Covtpf covtpf=covtpfList.get(0);
		
		/* Call TR52D tax subroutine                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.crtable.set(covtpf.getCrtable()); //IBPLIFE-4262
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.amountIn.set(wsaaCntfee);
		txcalcrec.effdate.set(chdrlnbIO.getOccdate());
		txcalcrec.transType.set("CNTF");
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
				|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

	protected void readTr52e7200()
	{
		tr52erec.tr52eRec.set(SPACES);
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItmfrm(new BigDecimal(chdrlnbIO.getOccdate().toString()));
		itempf.setItmto(new BigDecimal(chdrlnbIO.getOccdate().toString()));
		itempf.setItemtabl(tablesInner.tr52e.toString());
		itempf.setItemitem( wsaaTr52eKey.toString());
		itempf.setValidflag("1");
		List<Itempf> itempfList = itemDAO.findByItemDates(itempf);		 

		if ((itempfList == null || itempfList.size() == 0 )
				&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
		if (itempfList != null && itempfList.size() != 0 ) {
			tr52erec.tr52eRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}

	protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(Varcom.oK);
		/*    MOVE CHDRLNB-CNTCURR        TO ZRDP-CURRENCY.                */
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

	protected void a100ChdrOrRisk(Lifepf lifepf)
	{

		chkrlrec.company.set(chdrlnbIO.getChdrcoy());
		chkrlrec.cnttype.set(chdrlnbIO.getCnttype());
		chkrlrec.function.set("C/R?");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz, "R")) {
			chkrlrec.lrkclss.set(chdrlnbIO.getCnttype());
			wsaaSub.set(1);
			callHcrtfup1g00(lifepf);
			return ;
		}


		iterator =   covttrmList.iterator();		
		
		while (iterator.hasNext())
		{
			covttrm = iterator.next();			
			chkrlrec.crtable.set(covttrm.getCrtable());
			chkrlrec.function.set("READ");
			callProgram(Chkrlmt.class, chkrlrec.parmRec);
			if (isNE(chkrlrec.statuz, Varcom.oK)) {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				fatalError600();
			}
			for (wsaaSub.set(1); !(isGT(wsaaSub, 5)); wsaaSub.add(1)){
				callHcrtfup1g00(lifepf);
			}
		}
	}

	protected void a300CalcBenefitAmount()
	{
		/* To read T5687 2 times here, one for the main coverage the other */
		/* read is to det it back to the current rider. Same with T5675.   */
		/*  This is the first read using the main component                */
		List<Lifepf> lifepfListLife00 = null;
		List<Lifepf> lifepfListLife01 = new ArrayList<>();//IJTI-320
		Lifepf lifepfLife00 = null;
		Lifepf lifepfLife01 = null;

		itdmpfList = this.itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), tablesInner.t5687.toString(), wsaaMainCrtable.toString(), chdrlnbIO.getOccdate().toInt());

		if (itdmpfList == null || itdmpfList.size() == 0) {
			t5687rec.t5687Rec.set(SPACES);
			scrnparams.errorCode.set(errorsInner.f294);
		}
		else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		}
		/*  Read table T5675                                               */

		itemIO.setFunction(Varcom.readr);
		itemIO.setItemitem(t5687rec.premmeth);
		itemIO.setItemtabl(tablesInner.t5675);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)
				&& isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}


		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			premReqd.setTrue();
			return ;
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itemIO.getItemitem());
		}
		/* ILIFE-3142 End*/
		t5675rec.t5675Rec.set(itemIO.getGenarea());
		/*  Read Main Life file                                            */
		lifepfListLife00 = lifepfDAO.searchLifeRecordByLife(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString(), wsaaMainLife.toString(), "00");
		if(ListUtils.isEmpty(lifepfListLife00)){
			syserrrec.params.set("");
			fatalError600();
		}
		//IJTI-320 START
		if(!lifepfListLife01.isEmpty()){
			lifepfLife01 = lifepfListLife01.get(0);
			wsaaAnbAtCcd.set(lifepfLife01.getAnbAtCcd());
			wsaaSex.set(lifepfLife01.getCltsex());
		}
		//IJTI-320 END
		wsbbAnbAtCcd.set(ZERO);
		wsbbSex.set(SPACES);
		a400CallPremiumCalc();
	}

	protected void a400CallPremiumCalc()
	{

		premiumrec.function.set("CALC");
		premiumrec.crtable.set(wsaaMainCrtable);
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		premiumrec.lifeLife.set(wsaaMainLife);
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(wsaaMainCoverage);
		premiumrec.covrRider.set("00");
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(wsaaMainPcessdte);
		premiumrec.currcode.set(chdrlnbIO.getCntcurr());
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/*  (wsaa-sumin already adjusted for plan processing)              */
		premiumrec.cnttype.set(chdrlnbIO.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(wsaaMainCessdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		premiumrec.benCessTerm.set(ZERO);
		premiumrec.sumin.set(wsaaWaiveSumins);
		premiumrec.mortcls.set(wsaaMainMortclass);
		premiumrec.billfreq.set(payrpfBillingFreq);
		premiumrec.mop.set(payrpfBillingChnl);
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		premiumrec.calcPrem.set(ZERO);
		premiumrec.calcBasPrem.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(ZERO);
		premiumrec.intanny.set(ZERO);
		premiumrec.capcont.set(ZERO);
		premiumrec.dthpercn.set(ZERO);
		premiumrec.dthperco.set(ZERO);
		premiumrec.language.set(wsspcomn.language);
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		 */
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/	
		//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5675rec.premsubr.toString()))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);

			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			//ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations]
			//premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/*Ticket #IVE-792 - End		*/				
		//****Ticket #ILIFE-2005 end
		if (isEQ(premiumrec.statuz, varcom.bomb)) {
			syserrrec.params.set(premiumrec.premiumRec);
			syserrrec.statuz.set(premiumrec.statuz);
			fatalError600();
		}
		if (isNE(premiumrec.statuz, Varcom.oK)) {
			/*     GO TO A300-EXIT.                                  <S19FIX>*/
			return ;
		}
		wsaaWaiveSumins.set(premiumrec.calcPrem);
	}

	protected void a1700CrossCheckProduct()
	{
		a1710Begin();
	}

	protected void a1710Begin()
	{
		initialize(vlpdrulrec.validRec);
		vlpdrulrec.function.set("PROP");
		vlpdrulrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		vlpdrulrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		vlpdrulrec.chdrnum.set(chdrlnbIO.getChdrnum());
		vlpdrulrec.effdate.set(chdrlnbIO.getOccdate());
		vlpdrulrec.cnttype.set(chdrlnbIO.getCnttype());
		vlpdrulrec.srcebus.set(chdrlnbIO.getSrcebus());
		vlpdrulrec.language.set(wsspcomn.language);
		vlpdrulrec.cownnum.set(chdrlnbIO.getCownnum());
		vlpdrulrec.billfreq.set(chdrlnbIO.getBillfreq());
		callProgram(Vlpdrule.class, vlpdrulrec.validRec);
		if (isNE(vlpdrulrec.statuz, Varcom.oK)) {
			syserrrec.params.set(vlpdrulrec.validRec);
			syserrrec.statuz.set(vlpdrulrec.statuz);
			fatalError600();
		}
		for (wsaaIxc.set(1); !(isGT(wsaaIxc, 50)); wsaaIxc.add(1)){
			if (isEQ(vlpdrulrec.errLife[wsaaIxc.toInt()], SPACES)
					&& isEQ(vlpdrulrec.errCnttype[wsaaIxc.toInt()], SPACES)) {
				wsaaIxc.set(50);
			}
			else {
				for (wsaaIyc.set(1); !(isGT(wsaaIyc, 20)); wsaaIyc.add(1)){
					if (isEQ(vlpdrulrec.errCode[wsaaIxc.toInt()][wsaaIyc.toInt()], SPACES)) {
						wsaaIyc.set(20);
					}
					else {
						errmesgrec.errmesgRec.set(SPACES);
						errmesgrec.eror.set(vlpdrulrec.errCode[wsaaIxc.toInt()][wsaaIyc.toInt()]);
						sv.payrseqno.set(0);
						a1800ProductErrorMessage();
					}
				}
			}
		}
	}

	protected void a1800ProductErrorMessage()
	{
		a1810Error();
	}

	protected void a1810Error()
	{
		/* Call ERRMESG to retrieve the error message for display.      */
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaProg);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.set(SPACES);
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		/* Write error to subfile                                       */
		sv.life.set(vlpdrulrec.errLife[wsaaIxc.toInt()]);
		sv.jlife.set(vlpdrulrec.errJlife[wsaaIxc.toInt()]);
		sv.coverage.set(vlpdrulrec.errCoverage[wsaaIxc.toInt()]);
		sv.rider.set(vlpdrulrec.errRider[wsaaIxc.toInt()]);
		sv.payrseqno.set(ZERO);
		if (isEQ(vlpdrulrec.errDet[wsaaIxc.toInt()][wsaaIyc.toInt()], SPACES)) {
			if(isNE(scrnparams.deviceInd, "*RMT")){
				sv.erordsc.set(errmesgrec.errmesg[1]);
			} else {
				wsaaErrorMsg.set(errmesgrec.errmesg[1]);
			}
		}
		else {
			wsaaProdtyp.set(vlpdrulrec.errDet[wsaaIxc.toInt()][wsaaIyc.toInt()]);
			wsaaProdErr.set(errmesgrec.errmesg[1]);
			//TMLII-268 NB-08-001
			if(isNE(scrnparams.deviceInd, "*RMT")){
				sv.erordsc.set(wsaaProdError);
			} else {
				wsaaErrorMsg.set(wsaaProdError);
			}
		}
		//TMLII-268 NB-08-001 start
		if(isNE(scrnparams.deviceInd, "*RMT")){
			sv.errcde.set(errmesgrec.eror);
			/* Add the record to the subfile.                               */
			scrnparams.function.set(varcom.sadd);
			processScreen("S6378", sv);
			if (isNE(scrnparams.statuz, Varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		} else {
			//TMLII-1285
			valErrorCodes.add(errmesgrec.eror.toString());
			syserrrec.statuz.set(errmesgrec.eror); 
			syserrrec.syserrStatuz.set(errmesgrec.eror);
			wsaaErrorChdrnum.set(chdrlnbIO.chdrnum);
			syserrrec.params.set(wsaaParams);
			syserrrec.syserrType.set("4");
			callProgram(Syserr.class, syserrrec.syserrRec);
		}
		//TMLII-268 NB-08-001 end
		wsaaErrorFlag = "Y";
	}

	protected void a2000CheckDeathDate()
	{
		a2010Para();
		a2030StartAssignee();

		a2060StartTrustee();
	}

	protected void a2010Para()
	{
		/*  Check that the Joint Owner is not dead.                     */
		if (isNE(chdrlnbIO.getJownnum(), SPACES)) {
			wsaaClntnumIo.set(chdrlnbIO.getJownnum());
			a3000CallCltsio();
			if ((clntpfData != null)) {
				if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)
						&& isGT(chdrlnbIO.getOccdate(), clntpfData.getCltdod())) {
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.w343);
					sv.coverage.set(SPACES);
					sv.rider.set(SPACES);
					sv.payrseqno.set(ZERO);
					wsaaExtraMsgpfx.set("JO");
					errorMessages1800();
				}
			}
		}
		/*  Check that the Despatch address is not dead.                */
		if (isNE(chdrlnbIO.getDespnum(), SPACES)) {
			wsaaClntnumIo.set(chdrlnbIO.getDespnum());
			a3000CallCltsio();
			if ((clntpfData != null)) {
				if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)
						&& isGT(chdrlnbIO.getOccdate(), clntpfData.getCltdod())) {
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.w343);
					sv.coverage.set(SPACES);
					sv.rider.set(SPACES);
					sv.payrseqno.set(ZERO);
					wsaaExtraMsgpfx.set("DA");
					errorMessages1800();
				}
			}
		}
	}

	/**
	 * <pre>
	 ****  Check that the Assignee is not dead.
	 * </pre>
	 */
	protected void a2030StartAssignee()
	{
		List<Asgnpf> asgnpfList = null;
		boolean isAsgnpfValid = true;
		int asgnpfListSize = 0;
		wsaaAsgnPayClt.set("N");//ILIFE-2472

		asgnpfList = asgnpfDAO.searchAsgnpfRecord(chdrlnbIO.getChdrcoy().toString(), 
				chdrlnbIO.getChdrnum().toString());

		asgnpfListSize = asgnpfList.size();

		for(int index= 0; (index < asgnpfListSize) && (isAsgnpfValid); index++ ){
			Asgnpf asgnpf = asgnpfList.get(index);
			a2100CheckAssignee(asgnpf, isAsgnpfValid);
		}
	}

	/**
	 * <pre>
	 ****  Check that the Trustee is not dead.
	 * </pre>
	 */
	protected void a2060StartTrustee()
	{
		List<Ctrspf> ctrspfList =  null;
		boolean isCtrspfValid = true;
		int ctrspfListSize = 0;

		ctrspfList = ctrspfDAO.searchCtrspfRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		ctrspfListSize = ctrspfList.size();

		for(int index= 0; (index < ctrspfListSize) && (isCtrspfValid); index++ ){
			Ctrspf ctrspf = ctrspfList.get(index);
			a2200CheckTrustee(ctrspf, isCtrspfValid);
		}

		/*A2069-EXIT*/
	}

	/**
	 * <pre>
	 ****  Check that the Assignee is not dead.
	 * </pre>
	 */
	protected void a2100CheckAssignee(Asgnpf asgnpf, boolean isAsgnpfValid)
	{
		a2101Start(asgnpf, isAsgnpfValid);
	}

	protected void a2101Start(Asgnpf asgnpf, boolean isAsgnpfValid)
	{
		if(isNE(asgnpf.getChdrcoy(), chdrlnbIO.getChdrcoy())
				|| isNE(asgnpf.getChdrnum(), chdrlnbIO.getChdrnum())){
			isAsgnpfValid=  false;
			return;
		}
		else {
			/*ILIFE-2472-START*/
			if (isEQ(chdrlnbIO.getPayclt(), asgnpf.getAsgnnum())) {
				wsaaAsgnPayClt.set("Y");
			}
			/*ILIFE-2472-END*/
			wsaaClntnumIo.set(asgnpf.getAsgnnum());
			a3000CallCltsio();
			if ((clntpfData != null)) {
				if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)
						&& isGT(chdrlnbIO.getOccdate(), clntpfData.getCltdod())) {
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.w343);
					sv.coverage.set(SPACES);
					sv.rider.set(SPACES);
					sv.payrseqno.set(ZERO);
					wsaaExtraMsgpfx.set("NE");
					errorMessages1800();
				}
			}
		}
	}

	/**
	 * <pre>
	 ****  Check that the Trustee is not dead.
	 * </pre>
	 */
	protected void a2200CheckTrustee(Ctrspf ctrspf, boolean isCtrspfValid)
	{
		a2201Start(ctrspf, isCtrspfValid);
	}
	protected void a2201Start(Ctrspf ctrspf, boolean isCtrspfValid)
	{
		if(isNE(ctrspf.getChdrcoy(), chdrlnbIO.getChdrcoy())
				|| isNE(ctrspf.getChdrnum(), chdrlnbIO.getChdrnum())){
			isCtrspfValid = false;
		}
		else {
			wsaaClntnumIo.set(ctrspf.getClntnum());
			a3000CallCltsio();
			if ((clntpfData != null)) {
				if (isNE(clntpfData.getCltdod(), varcom.vrcmMaxDate)
						&& isGT(chdrlnbIO.getOccdate(), clntpfData.getCltdod())) {
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.w343);
					sv.coverage.set(SPACES);
					sv.rider.set(SPACES);
					sv.payrseqno.set(ZERO);
					wsaaExtraMsgpfx.set("TR");
					errorMessages1800();
				}
			}
		}
	}

	protected void a3000CallCltsio()
	{
		/*A3010-PARA*/
		Clntpf clntpfCriteria = new Clntpf();

		clntpfCriteria.setClntpfx("CN");
		clntpfCriteria.setClntcoy(wsspcomn.fsuco.toString());
		clntpfCriteria.setClntnum(wsaaClntnumIo.toString());

		clntpfData = clntpfDAO.selectActiveClient(clntpfCriteria);//ILIFE-6270
		/*A3090-EXIT*/
	}
	
	protected void agentLicenseCheck(){

					itempf = itemDAO.findItemByItem(wsspcomn.company.toString(),TD5G0,covtpf.getCrtable()+chdrlnbIO.getSrcebus());/* IJTI-1386 */

					if (itempf == null) {
						return ;
					}
					td5g0rec.td5g0Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					

					agentBank = td5g0rec.agentbank.toString();
					teller = td5g0rec.teller.toString();
					bankMang =  td5g0rec.bankmang.toString();
			
			if(chdrlnbIO.getSrcebus().toString().equals(BA)){
				validateLicWtBancass();
			}

			if(!chdrlnbIO.getSrcebus().toString().equals(BA)){
				validateLicWtOutBancass();
			}
	}
	
	protected void validateLicWtBancass(){

		if(chdrlnbIO.getChdrnum() !=null ){
			bnkoutpf = bnkoutpfDAO.getBankRecord(chdrlnbIO.getChdrnum().toString());

			if(agentBank != null && agentBank.equalsIgnoreCase("Y") ){
				if(bnkoutpf != null){
					checkBankOutlet();
				}
			}

			if(bankMang != null && bankMang.equalsIgnoreCase("Y") ){
				if(bnkoutpf != null){
					checkBankServiceMang();
				}
			}

			if(teller != null && teller.equalsIgnoreCase("Y")){
				if(bnkoutpf != null){
					checkBankTeller();
				}
			}

		}
	}
	protected void validateLicWtOutBancass(){
		
		if(agentBank !=null && agentBank.equalsIgnoreCase("Y")){
			
			List<Agslpf> agentList = agslpfDAO.getAgslpfByAgentAndLic(aglfIO.getAgntnum().toString(),td5g0rec.salelicensetype.toString().trim());
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
			if(agentList !=null && agentList.size() > 0){
				for (Agslpf agslpf : agentList) {
					Date date1;
					Date date2;
					try {
						date1 = sdf.parse(wsaaToday.toString());
						date2 = sdf.parse(agslpf.getTodate().toString());
						
						if (date1.compareTo(date2) > 0) {
							errmesgrec.errmesgRec.set(SPACES);
							errmesgrec.eror.set(errorsInner.rfbr);
							sv.life.set(SPACES);
							sv.jlife.set(SPACES);
							sv.coverage.set(SPACES);
							sv.rider.set(SPACES);
							sv.payrseqno.set(0);
							wsaaExtraMsgpfx.set(SPACES);
							errorMessages1800();
						} 
					} catch (ParseException e) {
						LOGGER.error("Exception occured in validateLicWtOutBancass() :-",e);
					}
				}
			}else{
				errmesgrec.errmesgRec.set(SPACES);
				errmesgrec.eror.set(errorsInner.rrdf);
				sv.life.set(SPACES);
				sv.jlife.set(SPACES);
				sv.coverage.set(SPACES);
				sv.rider.set(SPACES);
				sv.payrseqno.set(0);
				wsaaExtraMsgpfx.set(SPACES);
				errorMessages1800();
			}
		}
	}
	protected void checkBankOutlet(){
		if(bnkoutpf.getBnkout() !=null){
			Agntpf agntpf1 = agntpfDAO.getAgntRefCode(bnkoutpf.getBnkout()); // Bank Outlet
			if(agntpf1 != null){
				List<Agslpf> agentList1 = agslpfDAO.getAgslpfByAgentAndLic(agntpf1.getAgntnum(),td5g0rec.salelicensetype.toString().trim());/* IJTI-1386 */
				if(agentList1 !=null && agentList1.size() > 0){
					for (Agslpf agslpf : agentList1) {
						SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
						Date date1;
						Date date2;
						try {
							date1 = sdf.parse(wsaaToday.toString());
							date2 = sdf.parse(agslpf.getTodate().toString());
							if (date1.compareTo(date2) > 0) {
								errmesgrec.errmesgRec.set(SPACES);
								errmesgrec.eror.set(errorsInner.rrde);
								sv.life.set(SPACES);
								sv.jlife.set(SPACES);
								sv.coverage.set(SPACES);
								sv.rider.set(SPACES);
								sv.payrseqno.set(0);
								wsaaExtraMsgpfx.set(SPACES);
								errorMessages1800();
							} 
						} catch (ParseException e) {
							LOGGER.error("Exception occured in checkBankOutlet() :-",e);
						}
					}
				}else{
					errmesgrec.errmesgRec.set(SPACES);
					errmesgrec.eror.set(errorsInner.rrdd);
					sv.life.set(SPACES);
					sv.jlife.set(SPACES);
					sv.coverage.set(SPACES);
					sv.rider.set(SPACES);
					sv.payrseqno.set(0);
					wsaaExtraMsgpfx.set(SPACES);
					errorMessages1800();
				}
			}
		}
	
		
	}
	 protected void checkBankServiceMang(){
		 if(bnkoutpf.getBnksm() !=null){
			 Agntpf agntpf2 = agntpfDAO.getAgntRefCode(bnkoutpf.getBnksm()); // Bank Service Manger
			 if(agntpf2 != null ){					 
				 List<Agslpf> agentList2 = agslpfDAO.getAgslpfByAgentAndLic(agntpf2.getAgntnum(),td5g0rec.salelicensetype.toString().trim());/* IJTI-1386 */
				 if(agentList2 !=null && agentList2.size() > 0){
					 for (Agslpf agslpf : agentList2) {
						 SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
						 Date date1;
						 Date date2;
						 try {
							 date1 = sdf.parse(wsaaToday.toString());
							 date2 = sdf.parse(agslpf.getTodate().toString());
							 if (date1.compareTo(date2) > 0) {
								 errmesgrec.errmesgRec.set(SPACES);
								 errmesgrec.eror.set(errorsInner.rrdb);
								 sv.life.set(SPACES);
								 sv.jlife.set(SPACES);
								 sv.coverage.set(SPACES);
								 sv.rider.set(SPACES);
								 sv.payrseqno.set(0);
								 wsaaExtraMsgpfx.set(SPACES);
								 errorMessages1800();
							 } 
						 } catch (ParseException e) {
							 LOGGER.error("Exception occured in checkBankServiceMang() :-",e);
						 }
					 }
				 }else{
					 errmesgrec.errmesgRec.set(SPACES);
					 errmesgrec.eror.set(errorsInner.rrd9);
					 sv.life.set(SPACES);
					 sv.jlife.set(SPACES);
					 sv.coverage.set(SPACES);
					 sv.rider.set(SPACES);
					 sv.payrseqno.set(0);
					 wsaaExtraMsgpfx.set(SPACES);
					 errorMessages1800();
				 }

			 }
		 }
	 }
	 protected void checkBankTeller(){
		 if(bnkoutpf.getBnktel() !=null){
			 Agntpf agntpf3 = agntpfDAO.getAgntRefCode(bnkoutpf.getBnktel()); // Bank Teller
			 if(agntpf3 != null){
				 List<Agslpf> agentList3 = agslpfDAO.getAgslpfByAgentAndLic(agntpf3.getAgntnum(),td5g0rec.salelicensetype.toString().trim());/* IJTI-1386 */
				 if(agentList3 !=null && agentList3.size() > 0){
					 for (Agslpf agslpf : agentList3) {
						 SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
						 Date date1;
						 Date date2;
						 try {
							 date1 = sdf.parse(wsaaToday.toString());
							 date2 = sdf.parse(agslpf.getTodate().toString());
							 if (date1.compareTo(date2) > 0) {
								 errmesgrec.errmesgRec.set(SPACES);
								 errmesgrec.eror.set(errorsInner.rrdc);
								 sv.life.set(SPACES);
								 sv.jlife.set(SPACES);
								 sv.coverage.set(SPACES);
								 sv.rider.set(SPACES);
								 sv.payrseqno.set(0);
								 wsaaExtraMsgpfx.set(SPACES);
								 errorMessages1800();
							 } 
						 } catch (ParseException e) {
							 LOGGER.error("Exception occured in checkBankTeller() :-",e);
						 }
					 }
				 }else{
					 errmesgrec.errmesgRec.set(SPACES);
					 errmesgrec.eror.set(errorsInner.rrda);
					 sv.life.set(SPACES);
					 sv.jlife.set(SPACES);
					 sv.coverage.set(SPACES);
					 sv.rider.set(SPACES);
					 sv.payrseqno.set(0);
					 wsaaExtraMsgpfx.set(SPACES);
					 errorMessages1800();
				 }

			 }
		 }
	 }
	 
	 protected void performLincValidations() {
		 String t642 = "T642";
		 if(t642.equals(wsaaBatckey.batcBatctrcde.toString())){
			 String lincError;
			 LincCommonsUtil lincUtil = (LincCommonsUtil) getApplicationContext().getBean("lincCommonsUtil");
			 lincError = lincUtil.validateLincRecord(chdrlnbIO.getChdrnum().toString());
			 if(lincError != null) {
				 sv.life.set(SPACES);
				 sv.jlife.set(SPACES);
				 sv.coverage.set(SPACES);
				 sv.rider.set(SPACES);
				 sv.payrseqno.set(0);
				 wsaaExtraMsgpfx.set(SPACES);
				 errmesgrec.errmesgRec.set(SPACES);
				 errmesgrec.eror.set(lincError);
				 if(isNE(errmesgrec.eror, SPACES))
					 errorMessages1800();
			 }
		 }
	 }
	 
	 protected LincpfHelper getLincpfHelper() {
		 LincpfHelper lincpfHelper = new LincpfHelper();
		 lincpfHelper.setChdrcoy(wsspcomn.company.toString());
		 lincpfHelper.setChdrnum(sv.chdrnum.toString());
		 lincpfHelper.setTransactionDate(datcon1rec.intDate.toInt());
		 lincpfHelper.setLanguage(wsspcomn.language.toString());
		 lincpfHelper.setTranno(chdrlnbIO.getTranno().toInt());
		 lincpfHelper.setTermId(varcom.vrcmTermid.toString());
		 lincpfHelper.setTrdt(varcom.vrcmDate.toInt());
		 lincpfHelper.setTrtm(varcom.vrcmTime.toInt());
		 lincpfHelper.setUsrprf(varcom.vrcmUser.toString());
		 lincpfHelper.setJobnm(wsspcomn.userid.toString());
		 return lincpfHelper;
	 }
	 
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner {
		/* ERRORS */
		private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
		private FixedLengthStringData e351 = new FixedLengthStringData(4).init("E351");
		private FixedLengthStringData e355 = new FixedLengthStringData(4).init("E355");
		private FixedLengthStringData i005 = new FixedLengthStringData(4).init("I005");
		private FixedLengthStringData e571 = new FixedLengthStringData(4).init("E571");
		private FixedLengthStringData e714 = new FixedLengthStringData(4).init("E714");
		private FixedLengthStringData e964 = new FixedLengthStringData(4).init("E964");
		private FixedLengthStringData f151 = new FixedLengthStringData(4).init("F151");
		private FixedLengthStringData f290 = new FixedLengthStringData(4).init("F290");
		private FixedLengthStringData f826 = new FixedLengthStringData(4).init("F826");
		private FixedLengthStringData g041 = new FixedLengthStringData(4).init("G041");
		private FixedLengthStringData g066 = new FixedLengthStringData(4).init("G066");
		private FixedLengthStringData g228 = new FixedLengthStringData(4).init("G228");
		private FixedLengthStringData g816 = new FixedLengthStringData(4).init("G816");
		private FixedLengthStringData g161 = new FixedLengthStringData(4).init("G161");
		private FixedLengthStringData g772 = new FixedLengthStringData(4).init("G772");
		private FixedLengthStringData h015 = new FixedLengthStringData(4).init("H015");
		private FixedLengthStringData h017 = new FixedLengthStringData(4).init("H017");
		private FixedLengthStringData h030 = new FixedLengthStringData(4).init("H030");
		private FixedLengthStringData h360 = new FixedLengthStringData(4).init("H360");
		private FixedLengthStringData h361 = new FixedLengthStringData(4).init("H361");
		private FixedLengthStringData h362 = new FixedLengthStringData(4).init("H362");
		private FixedLengthStringData i008 = new FixedLengthStringData(4).init("I008");
		private FixedLengthStringData i009 = new FixedLengthStringData(4).init("I009");
		private FixedLengthStringData i010 = new FixedLengthStringData(4).init("I010");
		private FixedLengthStringData i004 = new FixedLengthStringData(4).init("I004");
		private FixedLengthStringData i007 = new FixedLengthStringData(4).init("I007");
		private FixedLengthStringData i014 = new FixedLengthStringData(4).init("I014");
		private FixedLengthStringData i020 = new FixedLengthStringData(4).init("I020");
		private FixedLengthStringData i030 = new FixedLengthStringData(4).init("I030");
		private FixedLengthStringData f921 = new FixedLengthStringData(4).init("F921");
		private FixedLengthStringData e507 = new FixedLengthStringData(4).init("E507");
		private FixedLengthStringData e508 = new FixedLengthStringData(4).init("E508");
		private FixedLengthStringData e510 = new FixedLengthStringData(4).init("E510");
		private FixedLengthStringData w343 = new FixedLengthStringData(4).init("W343");
		private FixedLengthStringData r064 = new FixedLengthStringData(4).init("R064");
		private FixedLengthStringData e977 = new FixedLengthStringData(4).init("E977");
		private FixedLengthStringData rl06 = new FixedLengthStringData(4).init("RL06");
		private FixedLengthStringData hl05 = new FixedLengthStringData(4).init("HL05");
		private FixedLengthStringData hl08 = new FixedLengthStringData(4).init("HL08");
		private FixedLengthStringData hl42 = new FixedLengthStringData(4).init("HL42");
		private FixedLengthStringData tl01 = new FixedLengthStringData(4).init("TL01");
		private FixedLengthStringData rl11 = new FixedLengthStringData(4).init("RL11");
		private FixedLengthStringData run9 = new FixedLengthStringData(4).init("RUN9");
		private FixedLengthStringData rl12 = new FixedLengthStringData(4).init("RL12");
		private FixedLengthStringData tl15 = new FixedLengthStringData(4).init("TL15");
		private FixedLengthStringData tl16 = new FixedLengthStringData(4).init("TL16");
		private FixedLengthStringData tl43 = new FixedLengthStringData(4).init("TL43");
		private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
		private FixedLengthStringData e591 = new FixedLengthStringData(4).init("E591");
		private FixedLengthStringData rpid = new FixedLengthStringData(4).init("RPID");
		private FixedLengthStringData rfk2 = new FixedLengthStringData(4).init("RFK2");
		//MIBT-365
		private FixedLengthStringData rfh1 = new FixedLengthStringData(4).init("RFH1");
		private FixedLengthStringData rfh7 = new FixedLengthStringData(4).init("RFH7");
		//TSD-266 Start
		private FixedLengthStringData rfpj = new FixedLengthStringData(4).init("RFPJ");
		private FixedLengthStringData pfpl = new FixedLengthStringData(4).init("RFPL");
		//TSD-266 Ends
		private FixedLengthStringData rfsl = new FixedLengthStringData(4).init("RFSL");
		private FixedLengthStringData rpj3 = new FixedLengthStringData(4).init("RPJ3");
		private FixedLengthStringData rfbr = new FixedLengthStringData(4).init("RFBR");
		private FixedLengthStringData rrdf = new FixedLengthStringData(4).init("RRDF");
		private FixedLengthStringData rrdd = new FixedLengthStringData(4).init("RRDD");
		private FixedLengthStringData rrd9 = new FixedLengthStringData(4).init("RRD9");
		private FixedLengthStringData rrda = new FixedLengthStringData(4).init("RRDA");
		private FixedLengthStringData rrde = new FixedLengthStringData(4).init("RRDE");
		private FixedLengthStringData rrdb = new FixedLengthStringData(4).init("RRDB");
		private FixedLengthStringData rrdc = new FixedLengthStringData(4).init("RRDC");
		private FixedLengthStringData rreh = new FixedLengthStringData(4).init("RREH");
		private FixedLengthStringData jl11 = new FixedLengthStringData(4).init("JL11");
		private FixedLengthStringData jl12 = new FixedLengthStringData(4).init("JL12");
		private FixedLengthStringData jl14 = new FixedLengthStringData(4).init("JL14");
		private FixedLengthStringData jl19 = new FixedLengthStringData(4).init("JL19");
		private FixedLengthStringData jl20 = new FixedLengthStringData(4).init("JL20");
		private FixedLengthStringData jl24 = new FixedLengthStringData(4).init("JL24"); //ILJ-44
		private FixedLengthStringData rul4 = new FixedLengthStringData(4).init("RUL4");
		private FixedLengthStringData rum8 = new FixedLengthStringData(4).init("RUM8");	//IBPLIFE-3145
		
}
	/*
	 * Class transformed  from Data Structure WSAA-BILLING-INFORMATION--INNER
	 */
	protected static final class WsaaBillingInformationInner {

		/* WSAA-BILLING-INFORMATION */
		private FixedLengthStringData[] wsaaBillingDetails = FLSInittedArray (9, 116);
		private ZonedDecimalData[] wsaaIncomeSeqNo = ZDArrayPartOfArrayStructure(2, 0, wsaaBillingDetails, 0, UNSIGNED_TRUE);
		private FixedLengthStringData[] wsaaBillfreq = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 2);
		private FixedLengthStringData[] wsaaBillchnl = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 4);
		private PackedDecimalData[] wsaaBillcd = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 6);
		private PackedDecimalData[] wsaaBtdate = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 11);
		private FixedLengthStringData[] wsaaBillcurr = FLSDArrayPartOfArrayStructure(3, wsaaBillingDetails, 16);
		private FixedLengthStringData[] wsaaClntcoy = FLSDArrayPartOfArrayStructure(1, wsaaBillingDetails, 19);
		private FixedLengthStringData[] wsaaClntnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 20);
		private FixedLengthStringData[] wsaaMandref = FLSDArrayPartOfArrayStructure(5, wsaaBillingDetails, 28);
		private FixedLengthStringData[] wsaaGrupkey = FLSDArrayPartOfArrayStructure(12, wsaaBillingDetails, 33);
		private PackedDecimalData[] wsaaRegprem = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 45);
		private PackedDecimalData[] wsaaSingp = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 54);
		private PackedDecimalData[] wsaaSpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 63);
		private PackedDecimalData[] wsaaRpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 72);
		private PackedDecimalData[] wsaaInstprm = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 81);
		private PackedDecimalData[] wsaaPremTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 90);
		private PackedDecimalData[] wsaaTaxrelamt = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 99);
		private FixedLengthStringData[] wsaaInrevnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 108);
		public PackedDecimalData[] getWsaaInstprm() {
			return wsaaInstprm;
		}
		public void setWsaaInstprm(PackedDecimalData[] wsaaInstprm) {
			this.wsaaInstprm = wsaaInstprm;
		}
		public FixedLengthStringData[] getWsaaBillfreq() {
			return wsaaBillfreq;
		}
		public ZonedDecimalData[] getWsaaIncomeSeqNo() {
			return wsaaIncomeSeqNo;
		}
		public void setWsaaIncomeSeqNo(ZonedDecimalData[] wsaaIncomeSeqNo) {
			this.wsaaIncomeSeqNo = wsaaIncomeSeqNo;
		}
		public void setWsaaBillfreq(FixedLengthStringData[] wsaaBillfreq) {
			this.wsaaBillfreq = wsaaBillfreq;
		}
		public PackedDecimalData[] getWsaaRegprem() {
			return wsaaRegprem;
		}
		public void setWsaaRegprem(PackedDecimalData[] wsaaRegprem) {
			this.wsaaRegprem = wsaaRegprem;
		}
		public PackedDecimalData[] getWsaaSingp() {
			return wsaaSingp;
		}
		public void setWsaaSingp(PackedDecimalData[] wsaaSingp) {
			this.wsaaSingp = wsaaSingp;
		}
		public FixedLengthStringData[] getWsaaClntnum() {
			return wsaaClntnum;
		}
		public void setWsaaClntnum(FixedLengthStringData[] wsaaClntnum) {
			this.wsaaClntnum = wsaaClntnum;
		}
		public FixedLengthStringData[] getWsaaClntcoy() {
			return wsaaClntcoy;
		}
		public void setWsaaClntcoy(FixedLengthStringData[] wsaaClntcoy) {
			this.wsaaClntcoy = wsaaClntcoy;
		}
		public PackedDecimalData[] getWsaaPremTax() {
			return wsaaPremTax;
		}
		public void setWsaaPremTax(PackedDecimalData[] wsaaPremTax) {
			this.wsaaPremTax = wsaaPremTax;
		}
		public PackedDecimalData[] getWsaaRpTax() {
			return wsaaRpTax;
		}
		public void setWsaaRpTax(PackedDecimalData[] wsaaRpTax) {
			this.wsaaRpTax = wsaaRpTax;
		}
		public PackedDecimalData[] getWsaaSpTax() {
			return wsaaSpTax;
		}
		public void setWsaaSpTax(PackedDecimalData[] wsaaSpTax) {
			this.wsaaSpTax = wsaaSpTax;
		}
	}
	/*
	 * Class transformed  from Data Structure TABLES--INNER
	 */
	private static final class TablesInner {
		/* TABLES */
		private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
		private FixedLengthStringData t3695 = new FixedLengthStringData(5).init("T3695");
		private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
		private FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");
		private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
		private FixedLengthStringData t5661 = new FixedLengthStringData(5).init("T5661");
		private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
		private FixedLengthStringData t3678 = new FixedLengthStringData(5).init("T3678");
		private FixedLengthStringData t6640 = new FixedLengthStringData(5).init("T6640");
		private FixedLengthStringData t6687 = new FixedLengthStringData(5).init("T6687");
		private FixedLengthStringData t3620 = new FixedLengthStringData(5).init("T3620");
		private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
		private FixedLengthStringData th506 = new FixedLengthStringData(5).init("TH506");
		private FixedLengthStringData th611 = new FixedLengthStringData(5).init("TH611");
		private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
		private FixedLengthStringData tr675 = new FixedLengthStringData(5).init("TR675");
		private FixedLengthStringData t6771 = new FixedLengthStringData(5).init("T6771");
		private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
		private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
		private FixedLengthStringData tr52z = new FixedLengthStringData(5).init("TR52Z");
		private FixedLengthStringData t6654 = new FixedLengthStringData(5).init("T6654");
		private FixedLengthStringData tJL02 = new FixedLengthStringData(5).init("TJL02");
		private FixedLengthStringData t3615 = new FixedLengthStringData(5).init("T3615");
	}
	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		/* FORMATS */
		private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData mandlnbrec = new FixedLengthStringData(10).init("MANDLNBREC");
		private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
	
		private FixedLengthStringData aglfrec = new FixedLengthStringData(10).init("AGLFREC");
		private FixedLengthStringData agntrec = new FixedLengthStringData(10).init("AGNTREC");
		private FixedLengthStringData hitrrnlrec = new FixedLengthStringData(10).init("HITRRNLREC");
		private FixedLengthStringData undlrec = new FixedLengthStringData(7).init("UNDLREC");
		private FixedLengthStringData hbnfrec = new FixedLengthStringData(10).init("HBNFREC");
	}
	
	public T5661rec getT5661rec() {
		return new T5661rec();
	}

	public void setT5661rec(T5661rec t5661rec) {
		this.t5661rec = t5661rec;
	}
	
	public Hcrtfuprec getHcrtfuprec() {
		return new Hcrtfuprec();
	}
	
	/* IBPLIFE-2274 */
	protected void check1PSuspense(){
		wsaaTotalSuspense.set(0);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.t3615.toString());
		itempf.setItemitem(chdrlnbIO.getSrcebus().toString());
		itempf.setValidflag("1");
		itempf = itemDAO.getItemRecordByItemkey(itempf);			
		if (itempf != null) {
			t3615rec.t3615Rec.set(StringUtil.rawToString(itempf.getGenarea()));			
		}	
		acblpfList = acblDao.searchAcblenqRecord(chdrlnbIO.chdrcoy.toString(), chdrlnbIO.getChdrnum().toString());
		
		for(Acblpf acblpfobj : acblpfList ) {
			if(acblpfobj != null && acblpfobj.getSacscurbal() != null){
				if(isEQ(chdrlnbIO.getBillchnl(),'D') && isNE(chdrlnbIO.getRcopt(),"Y")) {
					if(acblpfobj.getSacscode().equals("LP") &&  acblpfobj.getSacstyp().equals("SK")) {
						if (isEQ(t3695rec.sign, "-")) {
							compute(wsaa1PSuspense, 2).set(mult(acblpfobj.getSacscurbal(), -1));
						}
						else {
							wsaa1PSuspense.set(acblpfobj.getSacscurbal());
						}
					}
				} else {
					if (isEQ(t3695rec.sign, "-")) {
						compute(wsaa1PSuspense, 2).set(mult(acblpfobj.getSacscurbal(), -1));
					}
					else {
						wsaa1PSuspense.set(acblpfobj.getSacscurbal());
					}
				}
				compute(wsaaTotalSuspense, 2).set(add(wsaaTotalSuspense, wsaa1PSuspense));
			}
		}
		String t6A0 = "T6A0";
		String tbgd = "TBGD";
		String t642 = "T642";	//IBPLIFE-3145
		if (isNE(wsaaTotalSuspense,ZERO) && (t6A0.equals(wsaaBatckey.batcBatctrcde.toString()) 
				|| tbgd.equals(wsaaBatckey.batcBatctrcde.toString())
				|| (t642.equals(wsaaBatckey.batcBatctrcde.toString()) &&  isEQ(chdrlnbIO.getBillchnl(),'D')
						&& isNE(t3615rec.onepcashless,"Y") && isEQ(chdrlnbIO.getRcopt(),"Y")))) {	//IBPLIFE-3145
			errmesgrec.errmesgRec.set(SPACES);
			errmesgrec.eror.set(errorsInner.rul4);
			errorMessages1800();
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
	protected void validationCheck(Covtpf covtpf) {
		
		covrValidated = true;
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(covtpf.getCrtable());
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		premiumrec.lnkgSubRefNo.set(covtpf.getLnkgsubrefno());	
		if(covtpf.getLnkgno()==null || isEQ(covtpf.getLnkgno(),SPACE)) {
			premiumrec.linkcov.set(SPACE);
		}
		else {
			if(covtpf.getLnkgno() != null){
				LinkageInfoService linkgService = new LinkageInfoService();		
				FixedLengthStringData linkgCov = new FixedLengthStringData(linkgService.getLinkageInfo(covtpf.getLnkgno()));
				premiumrec.linkcov.set(linkgCov);
			}
		}
		premiumrec.liencd.set(covtpf.getLiencd());	
		premiumrec.lifeJlife.set(covtpf.getJlife());
		premiumrec.covrCoverage.set(covtpf.getCoverage());
		premiumrec.covrRider.set(covtpf.getRider());
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(covtpf.getPcesdte());
		premiumrec.lsex.set(covtpf.getSex01());
		premiumrec.lage.set(covtpf.getAnbccd01());
		premiumrec.jlsex.set(covtpf.getSex02());
		premiumrec.jlage.set(covtpf.getAnbccd02());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.cnttype.set(chdrlnbIO.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(covtpf.getRcesdte());
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		premiumrec.currcode.set(chdrlnbIO.getCntcurr());
		premiumrec.sumin.set(covtpf.getSumins());
		premiumrec.mortcls.set(covtpf.getMortcls());
		premiumrec.tpdtype.set(covtpf.getTpdtype());
		premiumrec.lnkgind.set(covtpf.getLnkgind());
		premiumrec.billfreq.set(payrpfBillingFreq);
		premiumrec.mop.set(payrpfBillingChnl);
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		premiumrec.calcPrem.set(covtpf.getInstprem());
		premiumrec.calcBasPrem.set(covtpf.getZbinstprem());
		premiumrec.calcLoaPrem.set(covtpf.getZlinstprem());
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(0);
		premiumrec.intanny.set(0);
		premiumrec.capcont.set(0);
		premiumrec.dthpercn.set(0);
		premiumrec.dthperco.set(0);
		premiumrec.language.set(wsspcomn.language);
		premiumrec.commissionPrem.set(ZERO);  
		premiumrec.zstpduty01.set(covtpf.getZstpduty01());
		premiumrec.zstpduty02.set(ZERO);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItmfrm(new BigDecimal(chdrlnbIO.getOccdate().toString()));
		itempf.setItmto(new BigDecimal(chdrlnbIO.getOccdate().toString()));
		itempf.setItemtabl(tablesInner.t5687.toString());
		itempf.setItemitem(covtpf.getCrtable());
		itempf.setValidflag("1");
		List<Itempf> itempfList = itemDAO.findByItemDates(itempf);
		if (itempfList != null && itempfList.size() != 0) {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));

			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tablesInner.t5675.toString());
			itempf.setItemitem(t5687rec.premmeth.toString());
			itempf.setValidflag("1");
			itempf = itemDAO.getItemRecordByItemkey(itempf);
			if (itempf != null) {
				t5675rec.t5675Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				premiumrec.premMethod.set(itempf.getItemitem());
			}
		}
		if((AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal(t5675rec.premsubr.toString()) && t5675rec.premsubr.toString().trim().equals("PMEX")))){
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			callReadRCVDPF(covtpf);
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			if(t5675rec.premsubr.toString().equals("RPRMRDT") || covtpf.getCrtable().toString().trim().equals("LCP1")){ 
				hpadpf=hpadpfDAO.getHpadData(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString());
				if(hpadpf != null){
				vpxlextrec.hpropdte.set(hpadpf.getHpropdte());
				}
				m800Start1();
				vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
			}
			premiumrec.cownnum.set(SPACES);
			premiumrec.occdate.set(ZERO);
			if(t5675rec.premsubr.toString().trim().equals("PMEX")){
				premiumrec.setPmexCall.set("Y");
				premiumrec.cownnum.set(chdrlnbIO.getCownnum());
				premiumrec.occdate.set(chdrlnbIO.getOccdate());
			}
			premiumrec.commTaxInd.set("Y");
			premiumrec.prevSumIns.set(ZERO);
			premiumrec.inputPrevPrem.set(ZERO);
			List<Lifepf> lifeList = lifepfDAO.getLifeData(covtpf.getChdrcoy().toString(), covtpf.getChdrnum().toString(), covtpf.getLife().toString(), "00");
			clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifeList.get(0).getLifcnum());
			clntDao = DAOFactory.getClntpfDAO();
			clnt=clntDao.getClientByClntnum(lifeList.get(0).getLifcnum().toString());
			if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
				premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
			}else{
				premiumrec.stateAtIncep.set(clntpf.getClntStateCd());
			}
            callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec,vpxacblrec.vpxacblRec);
            if (isEQ(premiumrec.statuz, varcom.bomb)) {	
    			syserrrec.statuz.set(premiumrec.statuz);	
    			fatalError600();	
    		}	
    		if (isNE(premiumrec.statuz, varcom.oK)) {	
    			errmesgrec.errmesgRec.set(SPACES);
    			errmesgrec.eror.set(premiumrec.statuz);
    			wsaaExtraMsgpfx.set(SPACES);
    			errorMessages1800();	
    		}
		}
	}

	protected void callReadRCVDPF(Covtpf covtpf){

		Rcvdpf rcvdPFObject= new Rcvdpf();
		rcvdPFObject.setChdrcoy(covtpf.getChdrcoy().toString());
		rcvdPFObject.setChdrnum(covtpf.getChdrnum().toString());
		rcvdPFObject.setLife(covtpf.getLife().toString());
		rcvdPFObject.setCoverage(covtpf.getCoverage().toString());
		rcvdPFObject.setRider(covtpf.getRider().toString());
		rcvdPFObject.setCrtable(covtpf.getCrtable().toString());
		rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
		if(rcvdPFObject == null){
			premiumrec.dialdownoption.set(100);
			premiumrec.prmbasis.set("");
		}
		else{
			if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("")){
				premiumrec.prmbasis.set(rcvdPFObject.getPrmbasis());
			}
			
			premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
		}	
	}
	protected void m800Start1()
	{
		mrtaPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP010", appVars, "IT") &&  isEQ(sv.cnttype,"MRT");
		mrtaIO = mrtapfDAO.getMrtaRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if (mrtaIO == null) {
			return;
		}
		if (!mrtaPermission) {
			List<Itempf> itempfList = itempfDAO.getAllItemitem("IT", chdrlnbIO.getChdrcoy().toString(), "TH615",
					mrtaIO.getMlresind()); 
			if (itempfList.size() == 0) {
				fatalError600();
			}
			th615rec.th615Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
			vpxlextrec.prat.set(mrtaIO.getPrat());
			compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
			vpxlextrec.coverc.set(vpxlextrec.temptoc);
			compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
			vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
			vpxlextrec.premind.set(th615rec.premind);
		}
	}

	
	protected void checkCovrRiderCustomerSpecific() {
		
	}

	protected void initCustomerSpecific(){
		
	}

	protected void retreiveClntDtailsCustomerSpecific() {
	}

	protected void clientNameFormatCustomerSpecific() {
		
	}
	
	protected void readSubroutineCustomerSpecific() {
		
	}
	
	protected void validAmountCustomerSpecific() {
		
	}
	
	protected void singPremFeeCustomerSepcific() {
		
	}

	protected void followUpsCustomerSpecific() {
		
	}


}