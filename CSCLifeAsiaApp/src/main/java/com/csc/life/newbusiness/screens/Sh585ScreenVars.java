package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH585
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh585ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(654);
	public FixedLengthStringData dataFields = new FixedLengthStringData(494).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData hxclnotes = new FixedLengthStringData(450).isAPartOf(dataFields, 1);
	public FixedLengthStringData[] hxclnote = FLSArrayPartOfStructure(6, 75, hxclnotes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(450).isAPartOf(hxclnotes, 0, FILLER_REDEFINE);
	public FixedLengthStringData hxclnote01 = DD.hxclnote.copy().isAPartOf(filler,0);
	public FixedLengthStringData hxclnote02 = DD.hxclnote.copy().isAPartOf(filler,75);
	public FixedLengthStringData hxclnote03 = DD.hxclnote.copy().isAPartOf(filler,150);
	public FixedLengthStringData hxclnote04 = DD.hxclnote.copy().isAPartOf(filler,225);
	public FixedLengthStringData hxclnote05 = DD.hxclnote.copy().isAPartOf(filler,300);
	public FixedLengthStringData hxclnote06 = DD.hxclnote.copy().isAPartOf(filler,375);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,451);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,459);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,489);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 494);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData hxclnotesErr = new FixedLengthStringData(24).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData[] hxclnoteErr = FLSArrayPartOfStructure(6, 4, hxclnotesErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(hxclnotesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hxclnote01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData hxclnote02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData hxclnote03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData hxclnote04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData hxclnote05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData hxclnote06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 534);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData hxclnotesOut = new FixedLengthStringData(72).isAPartOf(outputIndicators, 12);
	public FixedLengthStringData[] hxclnoteOut = FLSArrayPartOfStructure(6, 12, hxclnotesOut, 0);
	public FixedLengthStringData[][] hxclnoteO = FLSDArrayPartOfArrayStructure(12, 1, hxclnoteOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(72).isAPartOf(hxclnotesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hxclnote01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] hxclnote02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] hxclnote03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] hxclnote04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] hxclnote05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] hxclnote06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh585screenWritten = new LongData(0);
	public LongData Sh585protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh585ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, hxclnote01, hxclnote02, hxclnote03, hxclnote04, hxclnote05, hxclnote06};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, hxclnote01Out, hxclnote02Out, hxclnote03Out, hxclnote04Out, hxclnote05Out, hxclnote06Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, hxclnote01Err, hxclnote02Err, hxclnote03Err, hxclnote04Err, hxclnote05Err, hxclnote06Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh585screen.class;
		protectRecord = Sh585protect.class;
	}

}
