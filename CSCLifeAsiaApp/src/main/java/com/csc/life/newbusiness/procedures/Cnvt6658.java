/*
 * File: Cnvt6658.java
 * Date: December 3, 2013 2:17:25 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVT6658.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table T6658, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvt6658 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT6658");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t6658 = "T6658";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6658rec t6658rec = new T6658rec();
	private Varcom varcom = new Varcom();
	private T6658RecOldInner t6658RecOldInner = new T6658RecOldInner();

	public Cnvt6658() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(t6658);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T6658 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), t6658)) {
			getAppVars().addDiagnostic("Table T6658 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T6658 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT66582080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		t6658RecOldInner.t6658RecOld.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		t6658rec.addexist.set(t6658RecOldInner.t6658AddexistOld);
		t6658rec.addnew.set(t6658RecOldInner.t6658AddnewOld);
		t6658rec.billfreq.set(t6658RecOldInner.t6658BillfreqOld);
		t6658rec.comind.set(t6658RecOldInner.t6658ComindOld);
		t6658rec.compoundInd.set(t6658RecOldInner.t6658CompoundIndOld);
		t6658rec.fixdtrm.set(t6658RecOldInner.t6658FixdtrmOld);
		t6658rec.manopt.set(t6658RecOldInner.t6658ManoptOld);
		t6658rec.agemax.set(t6658RecOldInner.t6658MaxAgeOld);
		t6658rec.maxpcnt.set(t6658RecOldInner.t6658MaxpcntOld);
		t6658rec.maxRefusals.set(t6658RecOldInner.t6658MaxRefusalsOld);
		t6658rec.minctrm.set(t6658RecOldInner.t6658MinctrmOld);
		t6658rec.minpcnt.set(t6658RecOldInner.t6658MinpcntOld);
		t6658rec.nocommind.set(t6658RecOldInner.t6658NocommindOld);
		t6658rec.nostatin.set(t6658RecOldInner.t6658NostatinOld);
		t6658rec.optind.set(t6658RecOldInner.t6658OptindOld);
		t6658rec.refusalPeriod.set(t6658RecOldInner.t6658RefusalPeriodOld);
		t6658rec.simpleInd.set(t6658RecOldInner.t6658SimpleIndOld);
		t6658rec.statind.set(t6658RecOldInner.t6658StatindOld);
		t6658rec.subprog.set(t6658RecOldInner.t6658SubprogOld);
		t6658rec.premsubr.set(t6658RecOldInner.t6658PremsubrOld);
		t6658rec.trevsub.set(t6658RecOldInner.t6658TrevsubOld);
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(t6658rec.t6658Rec);
	}

protected void rewriteT66582080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), t6658)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T6658");
		}
	}
/*
 * Class transformed  from Data Structure T6658-REC-OLD--INNER
 */
private static final class T6658RecOldInner { 

	private FixedLengthStringData t6658RecOld = new FixedLengthStringData(500);
	private FixedLengthStringData t6658AddexistOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 0);
	private FixedLengthStringData t6658AddnewOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 1);
	private FixedLengthStringData t6658BillfreqOld = new FixedLengthStringData(2).isAPartOf(t6658RecOld, 2);
	private FixedLengthStringData t6658ComindOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 4);
	private FixedLengthStringData t6658CompoundIndOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 5);
	private ZonedDecimalData t6658FixdtrmOld = new ZonedDecimalData(2, 0).isAPartOf(t6658RecOld, 6);
	private FixedLengthStringData t6658ManoptOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 8);
	private ZonedDecimalData t6658MaxAgeOld = new ZonedDecimalData(2, 0).isAPartOf(t6658RecOld, 9);
	private ZonedDecimalData t6658MaxpcntOld = new ZonedDecimalData(5, 2).isAPartOf(t6658RecOld, 11);
	private ZonedDecimalData t6658MaxRefusalsOld = new ZonedDecimalData(2, 0).isAPartOf(t6658RecOld, 16);
	private ZonedDecimalData t6658MinctrmOld = new ZonedDecimalData(2, 0).isAPartOf(t6658RecOld, 18);
	private ZonedDecimalData t6658MinpcntOld = new ZonedDecimalData(5, 2).isAPartOf(t6658RecOld, 20);
	private FixedLengthStringData t6658NocommindOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 25);
	private FixedLengthStringData t6658NostatinOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 26);
	private FixedLengthStringData t6658OptindOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 27);
	private ZonedDecimalData t6658RefusalPeriodOld = new ZonedDecimalData(3, 0).isAPartOf(t6658RecOld, 28);
	private FixedLengthStringData t6658SimpleIndOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 31);
	private FixedLengthStringData t6658StatindOld = new FixedLengthStringData(1).isAPartOf(t6658RecOld, 32);
	private FixedLengthStringData t6658SubprogOld = new FixedLengthStringData(10).isAPartOf(t6658RecOld, 33);
	private FixedLengthStringData t6658PremsubrOld = new FixedLengthStringData(8).isAPartOf(t6658RecOld, 43);
	private FixedLengthStringData t6658TrevsubOld = new FixedLengthStringData(10).isAPartOf(t6658RecOld, 51);
}
}
