package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5002
 * @version 1.0 generated on 30/08/09 06:29
 * @author Quipoz
 */
public class S5002ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData chdrtype = DD.chdrtype.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5002screenWritten = new LongData(0);
	public LongData S5002protectWritten = new LongData(0);
	
	
	public static int[] screenPfInds = new int[] {1, 2, 3, 4, 6, 15, 16, 17, 18, 21, 22, 23, 24}; 
	
	
	public boolean hasSubfile() {
		return false;
	}


	public S5002ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrtypeOut,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"35",null, "-35","36", null, null, null, null, null, null, null, null});
	
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S5002screen.class;
		protectRecord = S5002protect.class;
	}
	
	public int getDataAreaSize()
	{
		return 62;
	}
	

	public int getDataFieldsSize()
	{
		return 14;
	}
	

	public int getErrorIndicatorSize()
	{
		return 12;
	}
	
	public int getOutputFieldSize()
	{
		return 36;
	}
	
	
	public BaseData[] getscreenFields()
	{
		return new BaseData[] {chdrsel, chdrtype, action};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {chdrselOut, chdrtypeOut, actionOut};	
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {chdrselErr, chdrtypeErr, actionErr};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateErrFields()
	{
		return  new BaseData[] {};
	}
	

	public static int[] getScreenPfInds()
	{
		return screenPfInds;
	}

}
