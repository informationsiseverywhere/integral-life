/*
 * File: Bh599.java
 * Date: 29 August 2009 21:38:02
 * Author: Quipoz Limited
 * 
 * Class transformed from BH599.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.HifxpfTableDAM;
import com.csc.life.newbusiness.recordstructures.Ph599par;
import com.csc.life.newbusiness.reports.Rh599Report;
import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*    This is the In-Force Business Comparison Report Over 24
*    Months Period. It will give out the Monthly Regular Premium
*    Business as well as Single Premium Business at individual
*    product level and/or at sales channel level.
*
*    Input File          : HIFX (In-Force Business Extract File)
*    Break Level Control : PH599PAR
*    Output Report       : RH599
*
*    N.B. Percentages (premium) on detail lines are based on
*         the formula as follows:
*         Product Premium Totals(Channel Premium Totals)
*         ---------------------------------------------- x 100%
*                      Company Premium Totals
*
*****************************************************************
* </pre>
*/
public class Bh599 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlhifxpf1rs = null;
	private java.sql.PreparedStatement sqlhifxpf1ps = null;
	private java.sql.Connection sqlhifxpf1conn = null;
	private String sqlhifxpf1 = "";
	private Rh599Report printerFile = new Rh599Report();
	private SortFileDAM sortFile = new SortFileDAM("RH599SRT");

	private FixedLengthStringData sortRec = new FixedLengthStringData(71);
	private FixedLengthStringData sortCntbranch = new FixedLengthStringData(2).isAPartOf(sortRec, 0);
	private FixedLengthStringData sortCnttype = new FixedLengthStringData(3).isAPartOf(sortRec, 2);
	private ZonedDecimalData sortSingp = new ZonedDecimalData(17, 2).isAPartOf(sortRec, 5);
	private ZonedDecimalData sortAnnprem = new ZonedDecimalData(17, 2).isAPartOf(sortRec, 22);
	private ZonedDecimalData sortSumins = new ZonedDecimalData(17, 2).isAPartOf(sortRec, 39);
	private FixedLengthStringData sortCntcurr = new FixedLengthStringData(3).isAPartOf(sortRec, 56);
	private ZonedDecimalData sortCntcount = new ZonedDecimalData(6, 0).isAPartOf(sortRec, 59);
	private PackedDecimalData sortMnth = new PackedDecimalData(2, 0).isAPartOf(sortRec, 65);
	private PackedDecimalData sortYear = new PackedDecimalData(4, 0).isAPartOf(sortRec, 67);
	private FixedLengthStringData sortType = new FixedLengthStringData(1).isAPartOf(sortRec, 70);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH599");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

		/*01  WSAA-TR386-KEY.                                      <LA3235>*/
	private FixedLengthStringData wsaaProductBranch = new FixedLengthStringData(1);
	private Validator productOnly = new Validator(wsaaProductBranch, "P");
	private Validator productBranch = new Validator(wsaaProductBranch, "B");
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIy = new ZonedDecimalData(3, 0).setUnsigned();

	private ZonedDecimalData wsaaCtrlbrk = new ZonedDecimalData(1, 0).setUnsigned();
	private Validator productTtl = new Validator(wsaaCtrlbrk, 1);
	private Validator branchTtl = new Validator(wsaaCtrlbrk, 2);
	private Validator companyTtl = new Validator(wsaaCtrlbrk, 3);
	private FixedLengthStringData wsaaPrevBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevCnttype = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaBranchnm = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaCntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaDummy = new FixedLengthStringData(3).init("*", true);

	private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
	private Validator regPrem = new Validator(wsaaPremType, "R");
	private Validator singPrem = new Validator(wsaaPremType, "S");
	private FixedLengthStringData wsaaSdate = new FixedLengthStringData(10);
		/* WSAA-TOTAL-PERCENT */
	private ZonedDecimalData wsaaAnnPrcnt = new ZonedDecimalData(5, 2);
	private ZonedDecimalData wsaaSingPrcnt = new ZonedDecimalData(5, 2);
	private static final int tableSize = 220;
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(220);

		/*01  WSAA-MESSAGES.                                       <LA3235>*/
	private FixedLengthStringData testCondition = new FixedLengthStringData(220).init("*", true);
	private FixedLengthStringData[] testChar = FLSArrayPartOfStructure(220, 1, testCondition, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(220).isAPartOf(testCondition, 0, FILLER_REDEFINE);
	private FixedLengthStringData testCompany = new FixedLengthStringData(55).isAPartOf(filler1, 0);
	private FixedLengthStringData testDate = new FixedLengthStringData(55).isAPartOf(filler1, 55);
	private FixedLengthStringData testChannel = new FixedLengthStringData(55).isAPartOf(filler1, 110);
	private FixedLengthStringData testProduct = new FixedLengthStringData(55).isAPartOf(filler1, 165);
	private FixedLengthStringData ap = new FixedLengthStringData(1).init("'");
		/*  Totals for product, channel and Company.*/
	private ZonedDecimalData wsaaStartDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEndDate = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaEffectiveDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaEffYr = new ZonedDecimalData(4, 0).isAPartOf(filler2, 0).setUnsigned();
	private ZonedDecimalData wsaaEffMth = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4).setUnsigned();
	private ZonedDecimalData wsaaEffDay = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6).setUnsigned();
	private ZonedDecimalData wsaaBsscEffdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaBsscEffdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBsscYr = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0).setUnsigned();
	private ZonedDecimalData wsaaBsscMth = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4).setUnsigned();
	private ZonedDecimalData wsaaClnkAmount = new ZonedDecimalData(13, 2);

	private FixedLengthStringData wsaaProductArray = new FixedLengthStringData(912);
	private FixedLengthStringData[] wsaaProductTotals = FLSArrayPartOfStructure(24, 38, wsaaProductArray, 0);
	private PackedDecimalData[] wsaaProdAnnprem = PDArrayPartOfArrayStructure(13, 2, wsaaProductTotals, 0);
	private PackedDecimalData[] wsaaProdSingp = PDArrayPartOfArrayStructure(13, 2, wsaaProductTotals, 7);
	private PackedDecimalData[] wsaaProdAnnCnt = PDArrayPartOfArrayStructure(7, 0, wsaaProductTotals, 14);
	private PackedDecimalData[] wsaaProdSingCnt = PDArrayPartOfArrayStructure(7, 0, wsaaProductTotals, 18);
	private PackedDecimalData[] wsaaProdSingsi = PDArrayPartOfArrayStructure(15, 2, wsaaProductTotals, 22);
	private PackedDecimalData[] wsaaProdRegsi = PDArrayPartOfArrayStructure(15, 2, wsaaProductTotals, 30);

	private FixedLengthStringData wsaaChannelArray = new FixedLengthStringData(912);
	private FixedLengthStringData[] wsaaChannelTotals = FLSArrayPartOfStructure(24, 38, wsaaChannelArray, 0);
	private PackedDecimalData[] wsaaChanAnnprem = PDArrayPartOfArrayStructure(13, 2, wsaaChannelTotals, 0);
	private PackedDecimalData[] wsaaChanSingp = PDArrayPartOfArrayStructure(13, 2, wsaaChannelTotals, 7);
	private PackedDecimalData[] wsaaChanAnnCnt = PDArrayPartOfArrayStructure(7, 0, wsaaChannelTotals, 14);
	private PackedDecimalData[] wsaaChanSingCnt = PDArrayPartOfArrayStructure(7, 0, wsaaChannelTotals, 18);
	private PackedDecimalData[] wsaaChanSingsi = PDArrayPartOfArrayStructure(15, 2, wsaaChannelTotals, 22);
	private PackedDecimalData[] wsaaChanRegsi = PDArrayPartOfArrayStructure(15, 2, wsaaChannelTotals, 30);

	private FixedLengthStringData wsaaCompanyArray = new FixedLengthStringData(912);
	private FixedLengthStringData[] wsaaCompanyTotals = FLSArrayPartOfStructure(24, 38, wsaaCompanyArray, 0);
	private PackedDecimalData[] wsaaCmpyAnnprem = PDArrayPartOfArrayStructure(13, 2, wsaaCompanyTotals, 0);
	private PackedDecimalData[] wsaaCmpySingp = PDArrayPartOfArrayStructure(13, 2, wsaaCompanyTotals, 7);
	private PackedDecimalData[] wsaaCmpyAnnCnt = PDArrayPartOfArrayStructure(7, 0, wsaaCompanyTotals, 14);
	private PackedDecimalData[] wsaaCmpySingCnt = PDArrayPartOfArrayStructure(7, 0, wsaaCompanyTotals, 18);
	private PackedDecimalData[] wsaaCmpySingsi = PDArrayPartOfArrayStructure(15, 2, wsaaCompanyTotals, 22);
	private PackedDecimalData[] wsaaCmpyRegsi = PDArrayPartOfArrayStructure(15, 2, wsaaCompanyTotals, 30);

		/* WSAA-MONTH-ARRAY */
	private FixedLengthStringData[] wsaaMonths = FLSInittedArray (24, 26);
	private FixedLengthStringData[] wsaaMonthDesc = FLSDArrayPartOfArrayStructure(16, wsaaMonths, 0);
	private PackedDecimalData[] wsaaMonth = PDArrayPartOfArrayStructure(2, 0, wsaaMonths, 16);
	private PackedDecimalData[] wsaaYear = PDArrayPartOfArrayStructure(4, 0, wsaaMonths, 18);
	private PackedDecimalData[] wsaaMthEndDate = PDArrayPartOfArrayStructure(8, 0, wsaaMonths, 21);
	private ZonedDecimalData wsaaDtc2Tmp = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaDatetext1 = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaMonthShort = new FixedLengthStringData(3).isAPartOf(wsaaDatetext1, 0);

	private FixedLengthStringData wsaaRepDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaRepYr = new ZonedDecimalData(4, 0).isAPartOf(wsaaRepDate, 0).setUnsigned();
	private ZonedDecimalData wsaaRepMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaRepDate, 4).setUnsigned();
	private ZonedDecimalData wsaaRepDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaRepDate, 6).setUnsigned();

	private FixedLengthStringData wsaaDatetext = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaMonthYear = new FixedLengthStringData(14).isAPartOf(wsaaDatetext, 5);

	private FixedLengthStringData wsaaDatetexc = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaHanYrMth = new FixedLengthStringData(16).isAPartOf(wsaaDatetexc, 0);
	private FixedLengthStringData wsaaHanDay = new FixedLengthStringData(6).isAPartOf(wsaaDatetexc, 16);
	private FixedLengthStringData wsaaRepDatetext = new FixedLengthStringData(22);
	private String wsaaFirstRec = "Y";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String th565 = "TH565";
	private static final String t5688 = "T5688";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private String wsaaEndOfSort = "";

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Ph599par ph599par = new Ph599par();
	private Th565rec th565rec = new Th565rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private PrinterRecInner printerRecInner = new PrinterRecInner();
	private SqlHifxpfInner sqlHifxpfInner = new SqlHifxpfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfFile2280, 
		exit2290, 
		singPrem3150, 
		initProductArray3180, 
		singPrem3250, 
		initChannelArray3280, 
		singPrem3350, 
		endOfReport3380
	}

	public Bh599() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		readTh5651010();
		defineCursor1015();
		setUpHeadingCompany1020();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		/* Open required files.*/
		printerFile.openOutput();
		ph599par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaFirstRec = "Y";
		initialize(wsaaProductArray);
		initialize(wsaaChannelArray);
		initialize(wsaaCompanyArray);
	}

protected void readTh5651010()
	{
		/*  Read TH565 for valid transaction codes, base currency and*/
		/*  periods.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th565);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th565rec.th565Rec.set(itemIO.getGenarea());
		/* Work out the End Date Range*/
		wsaaBsscEffdate.set(bsscIO.getEffectiveDate());
		if (isEQ(ph599par.year,ZERO)) {
			ph599par.year.set(wsaaBsscYr);
		}
		if (isEQ(ph599par.mnth,ZERO)) {
			ph599par.mnth.set(wsaaBsscMth);
		}
		wsaaEffYr.set(ph599par.year);
		wsaaEffMth.set(ph599par.mnth);
		wsaaEffDay.set(1);
		datcon1rec.intDate.set(wsaaEffectiveDate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		datcon2rec.intDate1.set(datcon1rec.intDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaEndDate.set(datcon2rec.intDate2);
		/* Work out the Start Date Range (2 Years Before)*/
		compute(wsaaEffYr, 0).set(sub(wsaaEffYr,2));
		datcon1rec.intDate.set(wsaaEffectiveDate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		wsaaStartDate.set(datcon1rec.intDate);
		datcon2rec.intDate2.set(datcon1rec.intDate);
		wsaaRepDate.set(ZERO);
		/* Store the Corresponding Date in Words Array.*/
		for (wsaaCnt.set(24); !(isEQ(wsaaCnt,0)); wsaaCnt.add(-1)){
			datcon6rec.language.set(bsscIO.getLanguage());
			datcon6rec.company.set(bsprIO.getCompany());
			datcon6rec.intDate1.set(datcon2rec.intDate2);
			wsaaEffectiveDate.set(datcon2rec.intDate2);
			wsaaDtc2Tmp.set(datcon2rec.intDate2);
			callProgram(Datcon6.class, datcon6rec.datcon6Rec);
			if (isEQ(datcon6rec.language,"S")
			|| isEQ(datcon6rec.language,"C")) {
				/* Chinese date format is Year lit Month lit Day lit               */
				wsaaDatetexc.set(datcon6rec.intDate2);
				wsaaMonthDesc[wsaaCnt.toInt()].set(wsaaHanYrMth);
			}
			else {
				wsaaDatetext.set(datcon6rec.intDate2);
				wsaaMonthDesc[wsaaCnt.toInt()].set(wsaaMonthYear);
			}
			/* Store month for report date                                     */
			if (isEQ(wsaaCnt,1)) {
				wsaaRepDate.set(datcon2rec.intDate2);
			}
			wsaaMonth[wsaaCnt.toInt()].set(wsaaEffMth);
			wsaaYear[wsaaCnt.toInt()].set(wsaaEffYr);
			if (isEQ(wsaaEffMth,12)) {
				wsaaEffMth.set(1);
				wsaaEffYr.add(1);
			}
			else {
				wsaaEffMth.add(1);
			}
			wsaaEffDay.set(1);
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(-1);
			datcon2rec.intDate1.set(wsaaEffectiveDate);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			datcon1rec.intDate.set(datcon2rec.intDate2);
			wsaaMthEndDate[wsaaCnt.toInt()].set(datcon2rec.intDate2);
			datcon1rec.function.set(varcom.conv);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			if (isNE(datcon1rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon1rec.datcon1Rec);
				fatalError600();
			}
			/*        MOVE DTC1-EXT-DATE      TO WSAA-MTH-END-DATE (WSAA-CNT)*/
			datcon2rec.datcon2Rec.set(SPACES);
			datcon2rec.frequency.set("12");
			datcon2rec.freqFactor.set(1);
			datcon2rec.intDate1.set(wsaaDtc2Tmp);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		}
		wsaaEffectiveDate.set(wsaaEndDate);
		/* Use DTC2 date, retain numeric mth from DTC2 instead of using    */
		/*   short month desc. Use the numeric day and year as per         */
		/*   original codes. Pass entire report date to Datcon6            */
		/*   for standard and proper date format for multi-lingual support */
		wsaaRepDay.set(wsaaEffDay);
		wsaaRepYr.set(wsaaEffYr);
		datcon6rec.intDate1.set(wsaaRepDate);
		datcon6rec.language.set(bsscIO.getLanguage());
		datcon6rec.company.set(bsprIO.getCompany());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		wsaaRepDatetext.set(datcon6rec.intDate2);
		/* Create the SQL Statement using the information entered*/
		/* on PH599.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("COMPANY = ");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(bsprIO.getCompany());
		stringVariable1.addExpression(ap);
		stringVariable1.setStringInto(testCompany);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(" AND ");
		stringVariable2.addExpression("EFFDATE BETWEEN ");
		stringVariable2.addExpression(wsaaStartDate);
		stringVariable2.addExpression(" AND ");
		stringVariable2.addExpression(wsaaEndDate);
		stringVariable2.setStringInto(testDate);
		if (isNE(ph599par.branch,SPACES)) {
			StringUtil stringVariable3 = new StringUtil();
			stringVariable3.addExpression(" AND ");
			stringVariable3.addExpression("CNTBRANCH = ");
			stringVariable3.addExpression(ap);
			stringVariable3.addExpression(ph599par.branch);
			stringVariable3.addExpression(ap);
			stringVariable3.setStringInto(testChannel);
		}
		if (isNE(ph599par.chdrtype,SPACES)) {
			StringUtil stringVariable4 = new StringUtil();
			stringVariable4.addExpression(" AND ");
			stringVariable4.addExpression("CNTTYPE = ");
			stringVariable4.addExpression(ap);
			stringVariable4.addExpression(ph599par.chdrtype);
			stringVariable4.addExpression(ap);
			stringVariable4.setStringInto(testProduct);
		}
		/*  Remove all '*' from the test condition.*/
		wsaaIy.set(1);
		for (wsaaIx.set(1); !(isGT(wsaaIx,tableSize)); wsaaIx.add(1)){
			if (isNE(testChar[wsaaIx.toInt()],"*")) {
				testChar[wsaaIy.toInt()].set(testChar[wsaaIx.toInt()]);
				wsaaIy.add(1);
			}
		}
		for (wsaaIx.set(wsaaIy); !(isGT(wsaaIx,tableSize)); wsaaIx.add(1)){
			testChar[wsaaIx.toInt()].set("*");
		}
		StringUtil stringVariable5 = new StringUtil();
		stringVariable5.addExpression("SELECT CNTBRANCH, ");
		stringVariable5.addExpression("CNTTYPE, ");
		stringVariable5.addExpression("SINGP, ");
		stringVariable5.addExpression("ANNPREM, ");
		stringVariable5.addExpression("SUMINS, ");
		stringVariable5.addExpression("CNTCURR,         ");
		stringVariable5.addExpression("CNTCOUNT, ");
		stringVariable5.addExpression("MNTH, ");
		stringVariable5.addExpression("\"YEAR\", ");
		/*smalchi2 for ILIFE-2341 STARTS*/
		stringVariable5.addExpression("TYPE_T FROM HIFXPF ");
		/*ENDS*/
		stringVariable5.addExpression("WHERE ");
		stringVariable5.addExpression(testCondition, "*");
		stringVariable5.setStringInto(wsaaQcmdexc);
	}

protected void defineCursor1015()
	{
		/*  Prepare cursor for query*/
		/*  Define the query required by declaring a cursor*/
		sqlhifxpf1 = wsaaQcmdexc.toString();
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlhifxpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new HifxpfTableDAM());
			sqlhifxpf1ps = getAppVars().prepareStatementEmbeded(sqlhifxpf1conn, sqlhifxpf1);
			sqlhifxpf1rs = getAppVars().executeQuery(sqlhifxpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaCompanynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaSdate.set(datcon1rec.extDate);
	}

	/**
	* <pre>
	* Read TR386 to retrieve report literal.                          
	*    MOVE SPACES                 TO ITEM-PARAMS.          <LA3235>
	*    MOVE ITEMREC                TO ITEM-FORMAT.          <LA3235>
	*    MOVE 'IT'                   TO ITEM-ITEMPFX.         <LA3235>
	*    MOVE BSPR-COMPANY           TO ITEM-ITEMCOY.         <LA3235>
	*    MOVE TR386                  TO ITEM-ITEMTABL.        <LA3235>
	**** MOVE BSSC-LANGUAGE          TO ITEM-ITEMITEM.        <FA1226>
	**** MOVE WSAA-PROG              TO ITEM-ITEMITEM(2:5).   <FA1226>
	*    MOVE BSSC-LANGUAGE          TO WSAA-TR386-LANG.      <LA3235>
	*    MOVE WSAA-PROG              TO WSAA-TR386-PGM.       <LA3235>
	*    MOVE WSAA-TR386-KEY         TO ITEM-ITEMITEM.        <LA3235>
	*    MOVE SPACES                 TO ITEM-ITEMSEQ.         <LA3235>
	**** MOVE READR                  TO ITEM-FUNCTION.        <FA1226>
	*    MOVE BEGN                   TO ITEM-FUNCTION.        <LA3235>
	*    MOVE O-K                    TO ITEM-STATUZ.          <LA3235>
	*    MOVE 1                      TO IY.                   <LA3235>
	*    INITIALIZE                  WSAA-MESSAGES.           <LA3235>
	*    PERFORM 1600-LOAD-TR386                              <LA3235>
	*        UNTIL ITEM-STATUZ = ENDP.                        <LA3235>
	**** CALL 'ITEMIO'               USING ITEM-PARAMS.       <FA1226>
	**** IF  ITEM-STATUZ             NOT = O-K                <FA1226>
	****     MOVE ITEM-STATUZ        TO SYSR-STATUZ           <FA1226>
	****     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <FA1226>
	****     PERFORM 600-FATAL-ERROR                          <FA1226>
	**** END-IF.                                              <FA1226>
	**** MOVE ITEM-GENAREA        TO TR386-TR386-REC.         <FA1226>
	*1090-EXIT.                                                       
	*    EXIT.                                                        
	*1600-LOAD-TR386 SECTION.                                 <LA3235>
	*1610-START.                                              <LA3235>
	*    CALL 'ITEMIO'               USING ITEM-PARAMS.       <LA3235>
	*    IF ITEM-ITEMCOY             NOT = BSPR-COMPANY       <LA3235>
	*    OR ITEM-ITEMTABL            NOT = TR386              <LA3235>
	*    OR ITEM-ITEMITEM            NOT = WSAA-TR386-KEY     <LA3235>
	*    OR ITEM-STATUZ              NOT = O-K                <LA3235>
	*        IF ITEM-FUNCTION        = BEGN                   <LA3235>
	*            MOVE WSAA-TR386-KEY TO ITEM-ITEMITEM         <LA3235>
	*            MOVE ITEM-PARAMS    TO SYSR-PARAMS           <LA3235>
	*            PERFORM 600-FATAL-ERROR                      <LA3235>
	*        ELSE                                             <LA3235>
	*            MOVE ENDP           TO ITEM-STATUZ           <LA3235>
	*            GO TO 1690-EXIT                              <LA3235>
	*       END-IF                                            <LA3235>
	*    END-IF.                                              <LA3235>
	*    MOVE ITEM-GENAREA           TO TR386-TR386-REC.      <LA3235>
	*    MOVE 1                      TO IX.                   <LA3235>
	*    PERFORM UNTIL IX            > 10                     <LA3235>
	*       IF  TR386-PROGDESC(IX)   NOT = SPACES             <LA3235>
	*       AND IY                   < 3                      <LA3235>
	*           MOVE TR386-PROGDESC(IX) TO WSAA-MESSAGE(IY)   <LA3235>
	*           ADD 1                TO IY                    <LA3235>
	*       END-IF                                            <LA3235>
	*       ADD 1                    TO IX                    <LA3235>
	*    END-PERFORM.                                         <LA3235>
	*    MOVE NEXTR                  TO ITEM-FUNCTION.        <LA3235>
	*1690-EXIT.                                               <LA3235>
	*    EXIT.                                                <LA3235>
	* </pre>
	*/
protected void readFile2000()
	{
		/*READ-FILE*/
		sortFile.openOutput();
		releaseData2100();
		sortFile.close();
		FileSort fs1 = new FileSort(sortFile);
		fs1.addSortKey(sortCntbranch, true);
		fs1.addSortKey(sortCnttype, true);
		fs1.sort();
		sortFile.openInput();
		retrieveData2400();
		sortFile.close();
		wsspEdterror.set(varcom.endp);
		/*EXIT*/
	}

protected void releaseData2100()
	{
		/*RELEASE*/
		wsaaEndOfSort = "N";
		while ( !(isEQ(wsaaEndOfSort,"Y"))) {
			releaseSort2200();
		}
		
		/*EXIT*/
	}

protected void releaseSort2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					relSort2210();
					accumulation2220();
				case endOfFile2280: 
					endOfFile2280();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void relSort2210()
	{
		/*   Fetch record*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlhifxpf1rs)) {
				getAppVars().getDBObject(sqlhifxpf1rs, 1, sqlHifxpfInner.sqlCntbranch);
				getAppVars().getDBObject(sqlhifxpf1rs, 2, sqlHifxpfInner.sqlCnttype);
				getAppVars().getDBObject(sqlhifxpf1rs, 3, sqlHifxpfInner.sqlSingp);
				getAppVars().getDBObject(sqlhifxpf1rs, 4, sqlHifxpfInner.sqlAnnprem);
				getAppVars().getDBObject(sqlhifxpf1rs, 5, sqlHifxpfInner.sqlSumins);
				getAppVars().getDBObject(sqlhifxpf1rs, 6, sqlHifxpfInner.sqlCntcurr);
				getAppVars().getDBObject(sqlhifxpf1rs, 7, sqlHifxpfInner.sqlCntcount);
				getAppVars().getDBObject(sqlhifxpf1rs, 8, sqlHifxpfInner.sqlMnth);
				getAppVars().getDBObject(sqlhifxpf1rs, 9, sqlHifxpfInner.sqlYear);
				getAppVars().getDBObject(sqlhifxpf1rs, 10, sqlHifxpfInner.sqlType);
			}
			else {
				goTo(GotoLabel.endOfFile2280);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
	}

	/**
	* <pre>
	* Find out the month/year slot into which the company totals
	* are to be added for working out percentage at a later stage.
	* </pre>
	*/
protected void accumulation2220()
	{
		wsaaFirstRec = "N";
		sortSingp.set(0);
		sortAnnprem.set(0);
		sortSumins.set(0);
		sortCntcount.set(0);
		for (wsaaIx.set(1); !(isGT(wsaaIx,24)); wsaaIx.add(1)){
			if (isEQ(wsaaMonth[wsaaIx.toInt()], sqlHifxpfInner.sqlMnth)
			&& isEQ(wsaaYear[wsaaIx.toInt()], sqlHifxpfInner.sqlYear)) {
				if (isNE(sqlHifxpfInner.sqlCntcurr, th565rec.currcode)) {
					initialize(conlinkrec.clnk002Rec);
					if (isNE(sqlHifxpfInner.sqlSingp, 0)) {
						conlinkrec.amountIn.set(sqlHifxpfInner.sqlSingp);
						currConversion2300();
						wsaaCmpySingp[wsaaIx.toInt()].add(wsaaClnkAmount);
						sortSingp.set(conlinkrec.amountOut);
					}
					if (isNE(sqlHifxpfInner.sqlAnnprem, 0)) {
						conlinkrec.amountIn.set(sqlHifxpfInner.sqlAnnprem);
						currConversion2300();
						wsaaCmpyAnnprem[wsaaIx.toInt()].add(wsaaClnkAmount);
						sortAnnprem.set(conlinkrec.amountOut);
					}
				}
				else {
					wsaaClnkAmount.set(sqlHifxpfInner.sqlSingp);
					wsaaCmpySingp[wsaaIx.toInt()].add(wsaaClnkAmount);
					wsaaClnkAmount.set(sqlHifxpfInner.sqlAnnprem);
					wsaaCmpyAnnprem[wsaaIx.toInt()].add(wsaaClnkAmount);
					sortSingp.set(sqlHifxpfInner.sqlSingp);
					sortAnnprem.set(sqlHifxpfInner.sqlAnnprem);
				}
				if (isEQ(sqlHifxpfInner.sqlType, "S")) {
					wsaaCmpySingCnt[wsaaIx.toInt()].add(sqlHifxpfInner.sqlCntcount);
					if (isNE(sqlHifxpfInner.sqlCntcurr, th565rec.currcode)) {
						initialize(conlinkrec.clnk002Rec);
						if (isNE(sqlHifxpfInner.sqlSumins, 0)) {
							conlinkrec.amountIn.set(sqlHifxpfInner.sqlSumins);
							currConversion2300();
							wsaaCmpySingsi[wsaaIx.toInt()].add(wsaaClnkAmount);
							sortSumins.set(conlinkrec.amountOut);
						}
					}
					else {
						wsaaClnkAmount.set(sqlHifxpfInner.sqlSumins);
						wsaaCmpySingsi[wsaaIx.toInt()].add(wsaaClnkAmount);
						sortSumins.set(sqlHifxpfInner.sqlSumins);
					}
				}
				else {
					wsaaCmpyAnnCnt[wsaaIx.toInt()].add(sqlHifxpfInner.sqlCntcount);
					if (isNE(sqlHifxpfInner.sqlCntcurr, th565rec.currcode)) {
						initialize(conlinkrec.clnk002Rec);
						if (isNE(sqlHifxpfInner.sqlSumins, 0)) {
							conlinkrec.amountIn.set(sqlHifxpfInner.sqlSumins);
							currConversion2300();
							wsaaCmpyRegsi[wsaaIx.toInt()].add(wsaaClnkAmount);
							sortSumins.set(conlinkrec.amountOut);
						}
					}
					else {
						wsaaClnkAmount.set(sqlHifxpfInner.sqlSumins);
						wsaaCmpyRegsi[wsaaIx.toInt()].add(wsaaClnkAmount);
						sortSumins.set(sqlHifxpfInner.sqlSumins);
					}
				}
				wsaaIx.set(25);
			}
		}
		/*  Set Up Sort Fields From SQL.*/
		sortCntbranch.set(sqlHifxpfInner.sqlCntbranch);
		sortCnttype.set(sqlHifxpfInner.sqlCnttype);
		sortCntcurr.set(sqlHifxpfInner.sqlCntcurr);
		sortCntcount.set(sqlHifxpfInner.sqlCntcount);
		sortType.set(sqlHifxpfInner.sqlType);
		sortMnth.set(sqlHifxpfInner.sqlMnth);
		sortYear.set(sqlHifxpfInner.sqlYear);
		sortFile.write(sortRec);
		goTo(GotoLabel.exit2290);
	}

protected void endOfFile2280()
	{
		wsaaEndOfSort = "Y";
	}

protected void currConversion2300()
	{
		currConvert2310();
	}

protected void currConvert2310()
	{
		conlinkrec.currIn.set(sqlHifxpfInner.sqlCntcurr);
		conlinkrec.currOut.set(th565rec.currcode);
		conlinkrec.cashdate.set(wsaaMthEndDate[wsaaIx.toInt()]);
		conlinkrec.company.set(bsprIO.getCompany());
		conlinkrec.function.set("CVRT");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
		wsaaClnkAmount.set(conlinkrec.amountOut);
	}

protected void retrieveData2400()
	{
		/*RETRV*/
		if (isEQ(wsaaFirstRec,"Y")
		&& isEQ(wsaaEndOfSort,"Y")) {
			return ;
		}
		wsaaFirstRec = "Y";
		wsaaEndOfSort = "N";
		while ( !(isEQ(wsaaEndOfSort,"Y"))) {
			returnSort2430();
		}
		
		/*EXIT*/
	}

protected void returnSort2430()
	{
			return2440();
		}

protected void return2440()
	{
		sortFile.read(sortRec);
		if (sortFile.isAtEnd()) {
			wsaaEndOfSort = "Y";
		}
		if (isEQ(wsaaEndOfSort,"Y")) {
			sortCntbranch.set(SPACES);
		}
		if (isEQ(wsaaFirstRec,"Y")) {
			wsaaPrevCnttype.set(sortCnttype);
			contractDesc3800();
			wsaaPrevBranch.set(sortCntbranch);
			branchDesc3700();
		}
		if (isEQ(wsaaFirstRec,"N")) {
			if (isEQ(wsaaEndOfSort,"Y")) {
				if (isEQ(ph599par.ctrlbrk02,"Y")
				&& isEQ(ph599par.chdrtype,SPACES)) {
					printProductTotals3100();
				}
				if (isEQ(ph599par.ctrlbrk01,"Y")
				&& isEQ(ph599par.branch,SPACES)) {
					printBranchTotals3200();
				}
				printCompanyTotals3300();
				return ;
			}
			if (productOnly.isTrue()) {
				wsaaPrevBranch.set(sortCntbranch);
				branchDesc3700();
			}
			if (isNE(sortCntbranch,wsaaPrevBranch)) {
				if (isEQ(ph599par.chdrtype,SPACES)) {
					if (isEQ(ph599par.ctrlbrk01,"Y")) {
						productBranch.setTrue();
						if (isEQ(ph599par.ctrlbrk02,"Y")) {
							printProductTotals3100();
							wsaaPrevCnttype.set(sortCnttype);
							contractDesc3800();
						}
						printBranchTotals3200();
					}
					else {
						productOnly.setTrue();
					}
					if (isNE(sortCntbranch,SPACES)
					&& productBranch.isTrue()) {
						wsaaPrevBranch.set(sortCntbranch);
						branchDesc3700();
					}
				}
				else {
					if (isEQ(ph599par.ctrlbrk01,"Y")) {
						printBranchTotals3200();
						wsaaPrevBranch.set(sortCntbranch);
						branchDesc3700();
					}
				}
			}
			if (isNE(sortCnttype,wsaaPrevCnttype)
			&& isEQ(ph599par.chdrtype,SPACES)) {
				if (isEQ(ph599par.ctrlbrk02,"Y")) {
					printProductTotals3100();
				}
				wsaaPrevCnttype.set(sortCnttype);
				contractDesc3800();
			}
		}
		wsaaFirstRec = "N";
		for (wsaaIx.set(1); !(isGT(wsaaIx,24)); wsaaIx.add(1)){
			if (isEQ(wsaaMonth[wsaaIx.toInt()],sortMnth)
			&& isEQ(wsaaYear[wsaaIx.toInt()],sortYear)) {
				if (isNE(sortSingp,0)) {
					wsaaProdSingp[wsaaIx.toInt()].add(sortSingp);
					wsaaChanSingp[wsaaIx.toInt()].add(sortSingp);
				}
				if (isNE(sortAnnprem,0)) {
					wsaaProdAnnprem[wsaaIx.toInt()].add(sortAnnprem);
					wsaaChanAnnprem[wsaaIx.toInt()].add(sortAnnprem);
				}
				if (isEQ(sortType,"S")) {
					wsaaProdSingsi[wsaaIx.toInt()].add(sortSumins);
					wsaaChanSingsi[wsaaIx.toInt()].add(sortSumins);
					wsaaProdSingCnt[wsaaIx.toInt()].add(sortCntcount);
					wsaaChanSingCnt[wsaaIx.toInt()].add(sortCntcount);
				}
				else {
					wsaaProdRegsi[wsaaIx.toInt()].add(sortSumins);
					wsaaChanRegsi[wsaaIx.toInt()].add(sortSumins);
					wsaaProdAnnCnt[wsaaIx.toInt()].add(sortCntcount);
					wsaaChanAnnCnt[wsaaIx.toInt()].add(sortCntcount);
				}
				wsaaIx.set(25);
			}
		}
	}

protected void edit2500()
	{
		/*EDIT*/
		/**  Check record is required for processing.*/
		/**  Softlock the record if it is to be updated.*/
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/** Update database records.*/
		/*EXIT*/
	}

protected void printProductTotals3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					product3110();
				case singPrem3150: 
					singPrem3150();
				case initProductArray3180: 
					initProductArray3180();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void product3110()
	{
		wsaaPremType.set(SPACES);
		for (wsaaIx.set(1); !(isGT(wsaaIx,24)
		|| regPrem.isTrue()); wsaaIx.add(1)){
			if (isEQ(wsaaPremType,SPACES)) {
				if (isGT(wsaaProdAnnCnt[wsaaIx.toInt()],0)) {
					regPrem.setTrue();
				}
			}
		}
		if (isEQ(wsaaPremType,SPACES)) {
			goTo(GotoLabel.singPrem3150);
		}
		printHeading3400();
		/*  Write Regular Premium Details checking for page overflow*/
		for (wsaaIx.set(1); !(isGT(wsaaIx,12)); wsaaIx.add(1)){
			if (newPageReq.isTrue()) {
				contotrec.totno.set(ct01);
				contotrec.totval.set(1);
				callContot001();
				printHeading3400();
			}
			initDtl3900();
			compute(wsaaIy, 0).set(add(wsaaIx,12));
			printerRecInner.yymmtxt.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.amnt.set(wsaaProdAnnprem[wsaaIx.toInt()]);
			printerRecInner.hpolcnt.set(wsaaProdAnnCnt[wsaaIx.toInt()]);
			printerRecInner.amnt02.set(wsaaProdRegsi[wsaaIx.toInt()]);
			compute(printerRecInner.tgtpcnt, 3).setRounded(div((mult(wsaaProdAnnprem[wsaaIx.toInt()], 100)), wsaaCmpyAnnprem[wsaaIx.toInt()]));
			printerRecInner.yymmtxt01.set(wsaaMonthDesc[wsaaIy.toInt()]);
			printerRecInner.amnt01.set(wsaaProdAnnprem[wsaaIy.toInt()]);
			printerRecInner.hpolcnt01.set(wsaaProdAnnCnt[wsaaIy.toInt()]);
			printerRecInner.amnt03.set(wsaaProdRegsi[wsaaIy.toInt()]);
			compute(printerRecInner.tgtpcnt01, 3).setRounded(div((mult(wsaaProdAnnprem[wsaaIy.toInt()], 100)), wsaaCmpyAnnprem[wsaaIy.toInt()]));
			printLine4100();
		}
		newPageReq.setTrue();
		printerRecInner.printerRec.set(SPACES);
	}

protected void singPrem3150()
	{
		/*  Write Single Premium Details checking for page overflow*/
		wsaaPremType.set(SPACES);
		for (wsaaIx.set(1); !(isGT(wsaaIx,24)
		|| singPrem.isTrue()); wsaaIx.add(1)){
			if (isEQ(wsaaPremType,SPACES)) {
				if (isGT(wsaaProdSingCnt[wsaaIx.toInt()],0)) {
					singPrem.setTrue();
				}
			}
		}
		if (!singPrem.isTrue()) {
			goTo(GotoLabel.initProductArray3180);
		}
		for (wsaaIx.set(1); !(isGT(wsaaIx,12)); wsaaIx.add(1)){
			if (newPageReq.isTrue()) {
				contotrec.totno.set(ct01);
				contotrec.totval.set(1);
				callContot001();
				printHeading3400();
			}
			initDtl3900();
			compute(wsaaIy, 0).set(add(wsaaIx,12));
			printerRecInner.yymmtxt.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.amnt.set(wsaaProdSingp[wsaaIx.toInt()]);
			printerRecInner.hpolcnt.set(wsaaProdSingCnt[wsaaIx.toInt()]);
			printerRecInner.amnt02.set(wsaaProdSingsi[wsaaIx.toInt()]);
			compute(printerRecInner.tgtpcnt, 3).setRounded(div((mult(wsaaProdSingp[wsaaIx.toInt()], 100)), wsaaCmpySingp[wsaaIx.toInt()]));
			printerRecInner.yymmtxt01.set(wsaaMonthDesc[wsaaIy.toInt()]);
			printerRecInner.amnt01.set(wsaaProdSingp[wsaaIy.toInt()]);
			printerRecInner.hpolcnt01.set(wsaaProdSingCnt[wsaaIy.toInt()]);
			printerRecInner.amnt03.set(wsaaProdSingsi[wsaaIy.toInt()]);
			compute(printerRecInner.tgtpcnt01, 3).setRounded(div((mult(wsaaProdSingp[wsaaIy.toInt()], 100)), wsaaCmpySingp[wsaaIy.toInt()]));
			printLine4100();
		}
	}

protected void initProductArray3180()
	{
		initialize(wsaaProductArray);
		productTtl.setTrue();
		/*EXIT*/
	}

protected void printBranchTotals3200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					branch3210();
				case singPrem3250: 
					singPrem3250();
				case initChannelArray3280: 
					initChannelArray3280();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void branch3210()
	{
		branchTtl.setTrue();
		wsaaPremType.set(SPACES);
		for (wsaaIx.set(1); !(isGT(wsaaIx,24)
		|| regPrem.isTrue()); wsaaIx.add(1)){
			if (isEQ(wsaaPremType,SPACES)) {
				if (isGT(wsaaChanAnnCnt[wsaaIx.toInt()],0)) {
					regPrem.setTrue();
				}
			}
		}
		if (isEQ(wsaaPremType,SPACES)) {
			goTo(GotoLabel.singPrem3250);
		}
		printHeading3400();
		/*  Write Regular Premium Branch Totals (Check for page overflow)*/
		for (wsaaIx.set(1); !(isGT(wsaaIx,12)); wsaaIx.add(1)){
			if (newPageReq.isTrue()) {
				contotrec.totno.set(ct01);
				contotrec.totval.set(1);
				callContot001();
				printHeading3400();
			}
			initDtl3900();
			compute(wsaaIy, 0).set(add(wsaaIx,12));
			printerRecInner.yymmtxt.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.amnt.set(wsaaChanAnnprem[wsaaIx.toInt()]);
			printerRecInner.hpolcnt.set(wsaaChanAnnCnt[wsaaIx.toInt()]);
			printerRecInner.amnt02.set(wsaaChanRegsi[wsaaIx.toInt()]);
			compute(printerRecInner.tgtpcnt, 3).setRounded(div((mult(wsaaChanAnnprem[wsaaIx.toInt()], 100)), wsaaCmpyAnnprem[wsaaIx.toInt()]));
			printerRecInner.yymmtxt01.set(wsaaMonthDesc[wsaaIy.toInt()]);
			printerRecInner.amnt01.set(wsaaChanAnnprem[wsaaIy.toInt()]);
			printerRecInner.hpolcnt01.set(wsaaChanAnnCnt[wsaaIy.toInt()]);
			printerRecInner.amnt03.set(wsaaChanRegsi[wsaaIy.toInt()]);
			compute(printerRecInner.tgtpcnt01, 3).setRounded(div((mult(wsaaChanAnnprem[wsaaIy.toInt()], 100)), wsaaCmpyAnnprem[wsaaIy.toInt()]));
			printLine4100();
		}
		newPageReq.setTrue();
		printerRecInner.printerRec.set(SPACES);
	}

protected void singPrem3250()
	{
		/*  Write Single Premium Branch Totals (Check for page overflow)*/
		wsaaPremType.set(SPACES);
		for (wsaaIx.set(1); !(isGT(wsaaIx,24)
		|| singPrem.isTrue()); wsaaIx.add(1)){
			if (isEQ(wsaaPremType,SPACES)) {
				if (isGT(wsaaChanSingCnt[wsaaIx.toInt()],0)) {
					singPrem.setTrue();
				}
			}
		}
		if (!singPrem.isTrue()) {
			goTo(GotoLabel.initChannelArray3280);
		}
		for (wsaaIx.set(1); !(isGT(wsaaIx,12)); wsaaIx.add(1)){
			if (newPageReq.isTrue()) {
				contotrec.totno.set(ct01);
				contotrec.totval.set(1);
				callContot001();
				printHeading3400();
			}
			initDtl3900();
			compute(wsaaIy, 0).set(add(wsaaIx,12));
			printerRecInner.yymmtxt.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.amnt.set(wsaaChanSingp[wsaaIx.toInt()]);
			printerRecInner.hpolcnt.set(wsaaChanSingCnt[wsaaIx.toInt()]);
			printerRecInner.amnt02.set(wsaaChanSingsi[wsaaIx.toInt()]);
			compute(printerRecInner.tgtpcnt, 3).setRounded(div((mult(wsaaChanSingp[wsaaIx.toInt()], 100)), wsaaCmpySingp[wsaaIx.toInt()]));
			printerRecInner.yymmtxt01.set(wsaaMonthDesc[wsaaIy.toInt()]);
			printerRecInner.amnt01.set(wsaaChanSingp[wsaaIy.toInt()]);
			printerRecInner.hpolcnt01.set(wsaaChanSingCnt[wsaaIy.toInt()]);
			printerRecInner.amnt03.set(wsaaChanSingsi[wsaaIy.toInt()]);
			compute(printerRecInner.tgtpcnt01, 3).setRounded(div((mult(wsaaChanSingp[wsaaIy.toInt()], 100)), wsaaCmpySingp[wsaaIy.toInt()]));
			printLine4100();
		}
	}

protected void initChannelArray3280()
	{
		initialize(wsaaChannelArray);
		productTtl.setTrue();
		/*EXIT*/
	}

protected void printCompanyTotals3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					comapny3310();
				case singPrem3350: 
					singPrem3350();
				case endOfReport3380: 
					endOfReport3380();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void comapny3310()
	{
		companyTtl.setTrue();
		wsaaPremType.set(SPACES);
		for (wsaaIx.set(1); !(isGT(wsaaIx,24)
		|| regPrem.isTrue()); wsaaIx.add(1)){
			if (isEQ(wsaaPremType,SPACES)) {
				if (isGT(wsaaCmpyAnnCnt[wsaaIx.toInt()],0)) {
					regPrem.setTrue();
				}
			}
		}
		if (isEQ(wsaaPremType,SPACES)) {
			goTo(GotoLabel.singPrem3350);
		}
		printHeading3400();
		/*  Write Regular Premium Company Totals (Check for page overflow)*/
		for (wsaaIx.set(1); !(isGT(wsaaIx,12)); wsaaIx.add(1)){
			if (newPageReq.isTrue()) {
				contotrec.totno.set(ct01);
				contotrec.totval.set(1);
				callContot001();
				printHeading3400();
			}
			initDtl3900();
			compute(wsaaIy, 0).set(add(wsaaIx,12));
			printerRecInner.yymmtxt.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.amnt.set(wsaaCmpyAnnprem[wsaaIx.toInt()]);
			printerRecInner.hpolcnt.set(wsaaCmpyAnnCnt[wsaaIx.toInt()]);
			printerRecInner.amnt02.set(wsaaCmpyRegsi[wsaaIx.toInt()]);
			printerRecInner.tgtpcnt.set(100);
			printerRecInner.yymmtxt01.set(wsaaMonthDesc[wsaaIy.toInt()]);
			printerRecInner.amnt01.set(wsaaCmpyAnnprem[wsaaIy.toInt()]);
			printerRecInner.hpolcnt01.set(wsaaCmpyAnnCnt[wsaaIy.toInt()]);
			printerRecInner.amnt03.set(wsaaCmpyRegsi[wsaaIy.toInt()]);
			printerRecInner.tgtpcnt01.set(100);
			printLine4100();
		}
		newPageReq.setTrue();
		printerRecInner.printerRec.set(SPACES);
	}

protected void singPrem3350()
	{
		/*  Write Single Premium Company Total checking for page overflow*/
		wsaaPremType.set(SPACES);
		for (wsaaIx.set(1); !(isGT(wsaaIx,24)
		|| singPrem.isTrue()); wsaaIx.add(1)){
			if (isEQ(wsaaPremType,SPACES)) {
				if (isGT(wsaaCmpySingCnt[wsaaIx.toInt()],0)) {
					singPrem.setTrue();
				}
			}
		}
		if (!singPrem.isTrue()) {
			goTo(GotoLabel.endOfReport3380);
		}
		for (wsaaIx.set(1); !(isGT(wsaaIx,12)); wsaaIx.add(1)){
			if (newPageReq.isTrue()) {
				contotrec.totno.set(ct01);
				contotrec.totval.set(1);
				callContot001();
				printHeading3400();
			}
			initDtl3900();
			compute(wsaaIy, 0).set(add(wsaaIx,12));
			printerRecInner.yymmtxt.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.amnt.set(wsaaCmpySingp[wsaaIx.toInt()]);
			printerRecInner.hpolcnt.set(wsaaCmpySingCnt[wsaaIx.toInt()]);
			printerRecInner.amnt02.set(wsaaCmpySingsi[wsaaIx.toInt()]);
			printerRecInner.tgtpcnt.set(100);
			printerRecInner.yymmtxt01.set(wsaaMonthDesc[wsaaIy.toInt()]);
			printerRecInner.amnt01.set(wsaaCmpySingp[wsaaIy.toInt()]);
			printerRecInner.hpolcnt01.set(wsaaCmpySingCnt[wsaaIy.toInt()]);
			printerRecInner.amnt03.set(wsaaCmpySingsi[wsaaIy.toInt()]);
			printerRecInner.tgtpcnt01.set(100);
			printLine4100();
		}
	}

protected void endOfReport3380()
	{
		printerFile.printRh599e01(printerRecInner.printerRec);
		/*EXIT*/
	}

protected void printHeading3400()
	{
		heading3410();
	}

protected void heading3410()
	{
		printerRecInner.printerRec.set(SPACES);
		if (regPrem.isTrue()) {
			printerFile.printRh599h01(printerRecInner.printerRec);
		}
		else {
			printerFile.printRh599h02(printerRecInner.printerRec);
		}
		printerRecInner.printerRec.set(SPACES);
		/* MOVE WSAA-REP-DAY           TO DAY01      OF RH599H03-O.     */
		/* MOVE WSAA-MONTH-SHORT       TO MTHSDSC    OF RH599H03-O.     */
		/* MOVE WSAA-REP-YR            TO YRSINF     OF RH599H03-O.     */
		/* Use full date format instead of non-standard date format        */
		/*  for multi-lingual support                                      */
		printerRecInner.datetexc.set(wsaaRepDatetext);
		printerRecInner.company.set(bsprIO.getCompany());
		printerRecInner.companynm.set(wsaaCompanynm);
		printerRecInner.sdate.set(wsaaSdate);
		printerRecInner.branch.set(wsaaPrevBranch);
		printerRecInner.branchnm.set(wsaaBranchnm);
		printerRecInner.cnttyp.set(wsaaPrevCnttype);
		printerRecInner.cntdesc.set(wsaaCntdesc);
		indOn.setTrue(11);
		indOn.setTrue(13);
		if (companyTtl.isTrue()) {
			if (isEQ(ph599par.chdrtype,SPACES)) {
				printerRecInner.cnttyp.set(wsaaDummy);
				printerRecInner.cntdesc.set(SPACES);
				/*          MOVE 'All Products'    TO CNTDESC    OF RH599H03-O  */
				/*<FA1226>  MOVE TR386-PROGDESC-1  TO CNTDESC    OF RH599H03-O  */
				/*             MOVE WSAA-MESSAGE (1)  TO CNTDESC    OF RH599H03-O  */
				indOff.setTrue(11);
				indOn.setTrue(12);
			}
			if (isEQ(ph599par.branch,SPACES)) {
				printerRecInner.branch.set(wsaaDummy);
				printerRecInner.branchnm.set(SPACES);
				/*          MOVE 'All Channels'    TO BRANCHNM   OF RH599H03-O  */
				/*<FA1226>  MOVE TR386-PROGDESC-2  TO BRANCHNM   OF RH599H03-O  */
				/*             MOVE WSAA-MESSAGE (2)  TO BRANCHNM   OF RH599H03-O  */
				indOff.setTrue(13);
				indOn.setTrue(14);
			}
		}
		if (branchTtl.isTrue()) {
			if (isEQ(ph599par.chdrtype,SPACES)) {
				printerRecInner.cnttyp.set(wsaaDummy);
				printerRecInner.cntdesc.set(SPACES);
				/*          MOVE 'All Products'    TO CNTDESC    OF RH599H03-O  */
				/*<FA1226>  MOVE TR386-PROGDESC-1  TO CNTDESC    OF RH599H03-O  */
				/*             MOVE WSAA-MESSAGE (1)  TO CNTDESC    OF RH599H03-O  */
				indOff.setTrue(11);
				indOn.setTrue(12);
			}
		}
		/*    WRITE PRINTER-REC       FORMAT IS 'RH599H03'.                */
		printerFile.printRh599h03(printerRecInner.printerRec, indicArea);
		if (regPrem.isTrue()) {
			printerFile.printRh599h04(printerRecInner.printerRec);
		}
		else {
			printerFile.printRh599h05(printerRecInner.printerRec);
		}
		printerFile.printRh599h06(printerRecInner.printerRec);
		wsaaOverflow.set("N");
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void branchDesc3700()
	{
		brDesc3710();
	}

protected void brDesc3710()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(wsaaPrevBranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaBranchnm.set(descIO.getLongdesc());
	}

protected void contractDesc3800()
	{
		cntDesc3810();
	}

protected void cntDesc3810()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(wsaaPrevCnttype);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaCntdesc.set(descIO.getLongdesc());
	}

protected void initDtl3900()
	{
		/*INIT*/
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.amnt.set(ZERO);
		printerRecInner.tgtpcnt.set(ZERO);
		printerRecInner.hpolcnt.set(ZERO);
		printerRecInner.amnt01.set(ZERO);
		printerRecInner.tgtpcnt01.set(ZERO);
		printerRecInner.hpolcnt01.set(ZERO);
		/* MOVE SPACES             TO FORLANG   OF RH599D01-O         */
		/*                            FORLANG01 OF RH599D01-O.        */
		printerRecInner.yymmtxt.set(SPACES);
		printerRecInner.yymmtxt01.set(SPACES);
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlhifxpf1conn, sqlhifxpf1ps, sqlhifxpf1rs);
		/*  Close any open files.*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void printLine4100()
	{
		/*PRINT*/
		printerFile.printRh599d01(printerRecInner.printerRec);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(th565rec.currcode);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure PRINTER-REC--INNER
 */
private static final class PrinterRecInner { 

	private FixedLengthStringData printerRec = new FixedLengthStringData(128);
	private FixedLengthStringData rh599Record = new FixedLengthStringData(128).isAPartOf(printerRec, 0);
	private FixedLengthStringData rh599h03O = new FixedLengthStringData(128).isAPartOf(rh599Record, 0, REDEFINE);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22).isAPartOf(rh599h03O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rh599h03O, 22);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rh599h03O, 23);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(rh599h03O, 53);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(rh599h03O, 63);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(rh599h03O, 65);
	private FixedLengthStringData cnttyp = new FixedLengthStringData(3).isAPartOf(rh599h03O, 95);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(rh599h03O, 98);
	private FixedLengthStringData rh599d01O = new FixedLengthStringData(120).isAPartOf(rh599Record, 0, REDEFINE);
	private FixedLengthStringData yymmtxt = new FixedLengthStringData(16).isAPartOf(rh599d01O, 0);
	private ZonedDecimalData amnt = new ZonedDecimalData(17, 2).isAPartOf(rh599d01O, 16);
	private ZonedDecimalData tgtpcnt = new ZonedDecimalData(5, 2).isAPartOf(rh599d01O, 33);
	private ZonedDecimalData hpolcnt = new ZonedDecimalData(5, 0).isAPartOf(rh599d01O, 38);
	private FixedLengthStringData yymmtxt01 = new FixedLengthStringData(16).isAPartOf(rh599d01O, 43);
	private ZonedDecimalData amnt01 = new ZonedDecimalData(17, 2).isAPartOf(rh599d01O, 59);
	private ZonedDecimalData tgtpcnt01 = new ZonedDecimalData(5, 2).isAPartOf(rh599d01O, 76);
	private ZonedDecimalData hpolcnt01 = new ZonedDecimalData(5, 0).isAPartOf(rh599d01O, 81);
	private ZonedDecimalData amnt02 = new ZonedDecimalData(17, 2).isAPartOf(rh599d01O, 86);
	private ZonedDecimalData amnt03 = new ZonedDecimalData(17, 2).isAPartOf(rh599d01O, 103);
}
/*
 * Class transformed  from Data Structure SQL-HIFXPF--INNER
 */
private static final class SqlHifxpfInner { 

		/* SQL-HIFXPF */
	private FixedLengthStringData sqlHifxrec = new FixedLengthStringData(45);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlHifxrec, 0);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlHifxrec, 2);
	private PackedDecimalData sqlSingp = new PackedDecimalData(17, 2).isAPartOf(sqlHifxrec, 5);
	private PackedDecimalData sqlAnnprem = new PackedDecimalData(17, 2).isAPartOf(sqlHifxrec, 14);
	private PackedDecimalData sqlSumins = new PackedDecimalData(17, 2).isAPartOf(sqlHifxrec, 23);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlHifxrec, 32);
	private PackedDecimalData sqlCntcount = new PackedDecimalData(6, 0).isAPartOf(sqlHifxrec, 35);
	private PackedDecimalData sqlMnth = new PackedDecimalData(2, 0).isAPartOf(sqlHifxrec, 39);
	private PackedDecimalData sqlYear = new PackedDecimalData(4, 0).isAPartOf(sqlHifxrec, 41);
	private FixedLengthStringData sqlType = new FixedLengthStringData(1).isAPartOf(sqlHifxrec, 44);
}
}
