/******************************************************************************
 * File Name 		: AsgnpfDAOImpl.java
 * Author			: nloganathan5
 * Creation Date	: 28 October 2016
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for ASGNPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.AsgnpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Asgnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AsgnpfDAOImpl extends BaseDAOImpl<Asgnpf> implements AsgnpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(AsgnpfDAOImpl.class);

	public List<Asgnpf> searchAsgnpfRecord(String chdrcoy, String chdrnum) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, ASGNPFX, ASGNNUM, ASSIGNAME, ");
		sqlSelect.append("REASONCD, COMMFROM, COMMTO, TRANNO, SEQNO, ");
		sqlSelect.append("TRANCDE, TERMID, TRDT, TRTM, USER_T ");
		sqlSelect.append("FROM ASGNPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, SEQNO ASC, UNIQUE_NUMBER DESC ");


		PreparedStatement psAsgnpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlAsgnpfRs = null;
		List<Asgnpf> outputList = new ArrayList<Asgnpf>();

		try {

			psAsgnpfSelect.setString(1, chdrcoy);
			psAsgnpfSelect.setString(2, chdrnum);

			sqlAsgnpfRs = executeQuery(psAsgnpfSelect);

			while (sqlAsgnpfRs.next()) {

				Asgnpf asgnpf = new Asgnpf();

				asgnpf.setUniqueNumber(sqlAsgnpfRs.getInt("UNIQUE_NUMBER"));
				asgnpf.setChdrcoy(getCharFromString(sqlAsgnpfRs, "CHDRCOY"));
				asgnpf.setChdrnum(sqlAsgnpfRs.getString("CHDRNUM"));
				asgnpf.setAsgnpfx(sqlAsgnpfRs.getString("ASGNPFX"));
				asgnpf.setAsgnnum(sqlAsgnpfRs.getString("ASGNNUM"));
				asgnpf.setAssigname(sqlAsgnpfRs.getString("ASSIGNAME"));
				asgnpf.setReasoncd(sqlAsgnpfRs.getString("REASONCD"));
				asgnpf.setCommfrom(sqlAsgnpfRs.getInt("COMMFROM"));
				asgnpf.setCommto(sqlAsgnpfRs.getInt("COMMTO"));
				asgnpf.setTranno(sqlAsgnpfRs.getInt("TRANNO"));
				asgnpf.setSeqno(sqlAsgnpfRs.getInt("SEQNO"));
				asgnpf.setTrancde(sqlAsgnpfRs.getString("TRANCDE"));
				asgnpf.setTermid(sqlAsgnpfRs.getString("TERMID"));
				asgnpf.setTrdt(sqlAsgnpfRs.getInt("TRDT"));
				asgnpf.setTrtm(sqlAsgnpfRs.getInt("TRTM"));
				asgnpf.setUserT(sqlAsgnpfRs.getInt("USER_T"));

				outputList.add(asgnpf);
			}

		} catch (SQLException e) {
				LOGGER.error("searchAsgnpfRecord()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(psAsgnpfSelect, sqlAsgnpfRs);
		}

		return outputList;
	}
	
	private Character getCharFromString(ResultSet sqlpCovt1Rs , String columnName) throws SQLException{
		
		Character characterOutput = null;
		
		String stringOutput = sqlpCovt1Rs.getString(columnName);
		
		if(null != stringOutput){
			characterOutput = stringOutput.charAt(0);
		}
		
		return characterOutput;
	}
}
