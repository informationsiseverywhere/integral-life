package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HpaxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:33
 * Class transformed from HPAXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HpaxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 16;
	public FixedLengthStringData hpaxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hpaxpfRecord = hpaxrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hpaxrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hpaxrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(hpaxrec);
	public PackedDecimalData zsufcdte = DD.zsufcdte.copy().isAPartOf(hpaxrec);
	public FixedLengthStringData procflg = DD.procflg.copy().isAPartOf(hpaxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HpaxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for HpaxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HpaxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HpaxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpaxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HpaxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpaxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HPAXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"VALIDFLAG, " +
							"ZSUFCDTE, " +
							"PROCFLG, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     validflag,
                                     zsufcdte,
                                     procflg,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		validflag.clear();
  		zsufcdte.clear();
  		procflg.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHpaxrec() {
  		return hpaxrec;
	}

	public FixedLengthStringData getHpaxpfRecord() {
  		return hpaxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHpaxrec(what);
	}

	public void setHpaxrec(Object what) {
  		this.hpaxrec.set(what);
	}

	public void setHpaxpfRecord(Object what) {
  		this.hpaxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hpaxrec.getLength());
		result.set(hpaxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}