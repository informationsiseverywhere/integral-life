/*
 * File: Ph567.java
 * Date: 30 August 2009 1:08:44
 * Author: Quipoz Limited
 * 
 * Class transformed from PH567.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.newbusiness.screens.Sh567ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*                  Schedule Reprint Submenu
*                  ========================
*
*  This program accepts a valid contract number, checks it is of
*  a valid status to perform a schedule reprint function.
*
* Initialisation
* --------------
*
*  Retrieve todays date (DATCON1) and store for later use.
*
*
* Validation
* ----------
*
*  Key 1 - Contract proposal number (CHDRMNA)
*
*       Y = mandatory, must exist on file.
*            - CHDRMNA  -  Life  new  business  contract  header
*                 logical view  (LIFO,  keyed  on  company   and
*                 contract number, select service unit = LP).
*            - must be correct status for transaction (T5679).
*            - servicing branch must be sign-on branch.
*
*
*       Blank = irrelevant.
*
*
* Updating
* --------
*
*  Soft lock the contract header minor alterations. If the
*  contract is already locked, display an error message.
*
*
****************************************************************** ****
* </pre>
*/
public class Ph567 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH567");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e455 = "E455";
	private static final String e544 = "E544";
	private static final String e767 = "E767";
	private static final String f321 = "F321";
	private static final String f910 = "F910";
	private static final String f918 = "F918";
		/* TABLES */
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrmnarec = "CHDRMNAREC";
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private Sh567ScreenVars sv = ScreenProgram.getScreenVars( Sh567ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		validateKey22220, 
		exit2290, 
		search2510, 
		exit12090, 
		batching3080, 
		exit3090
	}

	public Ph567() {
		super();
		screenVars = sv;
		new ScreenModel("Sh567", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn, wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr, wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		if (isEQ(wsaaToday, 0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'SH567IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SH567-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr, SPACES)) {
			if (isNE(scrnparams.statuz, "BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz, varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateKey12210();
				case validateKey22220: 
					validateKey22220();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1, SPACES)) {
			goTo(GotoLabel.validateKey22220);
		}
		chdrmnaIO.setChdrcoy(wsspcomn.company);
		chdrmnaIO.setChdrnum(sv.chdrsel);
		chdrmnaIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrmnaIO);
		}
		else {
			chdrmnaIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmnaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmnaIO.getStatuz(), varcom.mrnf)
		&& isEQ(subprogrec.key1, "Y")) {
			sv.chdrselErr.set(e544);
		}
		if (isEQ(chdrmnaIO.getStatuz(), varcom.oK)
		&& isEQ(subprogrec.key1, "N")) {
			sv.chdrselErr.set(f918);
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				goTo(GotoLabel.exit2290);
			}
		}
		/*    For transactions on existing proposals,*/
		/*       check the contract selected is of the correct status*/
		/*       and from correct branck.*/
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)) {
			checkStatus2400();
		}
		if (isEQ(subprogrec.key1, "Y")
		&& isEQ(sv.chdrselErr, SPACES)
		&& isNE(wsspcomn.branch, chdrmnaIO.getCntbranch())) {
			sv.chdrselErr.set(e455);
		}
	}

protected void validateKey22220()
	{
		if (isEQ(subprogrec.key2, SPACES)
		|| isEQ(subprogrec.key2, "N")) {
			return ;
		}
		chdrmnaIO.setChdrcoy(wsspcomn.company);
		chdrmnaIO.setChdrnum(sv.chdrsel);
		chdrmnaIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel, SPACES)) {
			SmartFileCode.execute(appVars, chdrmnaIO);
		}
		else {
			chdrmnaIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)
		&& isNE(chdrmnaIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrmnaIO.getStatuz(), varcom.mrnf)) {
			sv.chdrselErr.set(e544);
		}
	}

protected void checkStatus2400()
	{
		readStatusTable2410();
	}

protected void readStatusTable2410()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
			wsaaSub.set(ZERO);
			lookForStat2500();
		}
		else {
			t5679rec.t5679Rec.set(SPACES);
			scrnparams.errorCode.set(f321);
			wsspcomn.edterror.set("Y");
		}
	}

protected void lookForStat2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case search2510: 
					search2510();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void search2510()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 12)) {
			sv.chdrselErr.set(e767);
			wsspcomn.edterror.set("Y");
		}
		else {
			if (isNE(chdrmnaIO.getStatcode(), t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				goTo(GotoLabel.search2510);
			}
		}
		/*EXIT*/
	}

protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd, "Y")) {
			sv.actionErr.set(e073);
		}
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*        GO TO 2990-EXIT.                                         */
		goTo(GotoLabel.exit12090);
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz, varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateWssp3010();
					keeps3070();
				case batching3080: 
					batching3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz, "BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		/*  Update WSSP Key details*/
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		/*    Soft lock contract.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		if (isNE(sv.chdrsel, SPACES)) {
			sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		}
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrselErr.set(f910);
		}
	}

protected void keeps3070()
	{
		/*   Store the contract header for use by the transaction programs*/
		chdrmnaIO.setFunction("KEEPS");
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd, "Y")
		&& isEQ(sv.errorIndicators, SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz, varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz, "BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
}
