/*
 * File: Bh560.java
 * Date: 29 August 2009 21:33:10
 * Author: Quipoz Limited
 * 
 * Class transformed from BH560.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceLeading;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Srvunitcpy;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.reports.Rh560Report;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.BprdTableDAM;
import com.csc.smart.dataaccess.BsprTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Contot;
import com.csc.smart.recordstructures.Contotrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   This SQL batch program does not run under MAINB as it makes
*   use of the internal COBOL sort facility to perform all
*   processing within INPUT/OUTPUT procedures.
*
*   It also uses SQL to select the appropriate records.
*
*   The sort criteria is as follows (all ascending) :
*
*   Contract Servicing Branch
*    Agent Area Code
*       Agent Number
*        Sort Type (See below).
*
*   The function of this program is to retrieve records which
*   fall within 2 categories :
*
*     i)    Sort Type 1.
*           Retrieve all proposals.
*
*     ii)   Sort Type 2.
*           Select over the last week, using the Effective Date as
*           the end of the week, any Policy which has been Issued,
*           or any CFI, Decline, Withdraw or Postpone action.
*
*   The program has the following structure :
*
*   1000-INITIALISE.
*
*   2000-SELECT. (Sort Input procedure)
*     2000A-READ-CHDR
*     2000B-READ-PTRN
*
*   3000-PRINT-REPORT. (Sort Output Procedure)
*     3000A-PROCESS-FILE
*       3000AA-KEY-CHANGE
*       3000AB-OUTPUT-DETAILS
*         3000ABA-READ-COVT
*         3000ABB-CALC-FEE
*         3000ABC-READ-COVR
*         3000ABD-READ-LIFE
*         3000ABE-READ-FLUP
*
*   4000-CLOSE.
*
*   8000-CALL-CONTOT.  (Called to increment appropriate count).
*
*   9000-SQL-ERROR.    (Called when an SQL error is detected).
*
*   9100-FATAL-ERROR.  (Called when a DB error is detected).
*
*                + + + + + + + + + + + + + + + + + + + +
*
*   1000-INITIALISE.
*   ----------------
*     Open the output file & set up static headings (i.e. Company
*     and Date).
*     Read T5679 using System Parameter 1 and set up a working
*     store table with valid contract stati.
*
*     Read TH565 using System Parameter 1 and set up a working
*     store table with valid Batch Transaction Codes.
*
*     Use the Effective Date to establish the date range.
*
*   2000-SELECT.
*   ------------
*     Declare CHDR_CURSOR to select Contracts with a status equal
*     to that in the working store table.
*
*     Open CHDR_CURSOR
*
*     Perform 2000A-READ-CHDR
*        until WSSP-EDTERROR set to ENDP.
*
*     Close CHDR_CURSOR.
*
*     Declare PTRN_CURSOR to select Transactions with a Batch
*     Transacton Code equal to that in the working store table,
*     which fall within the date range.
*
*     Open PTRN_CURSOR
*
*     Perform 2000B-READ-PTRN
*        until WSSP-EDTERROR set to ENDP.
*
*     Close PTRN_CURSOR.
*
*   2000A-READ-CHDR.
*   ----------------
*     Fetch CHDR details
*        when not found set WSSP-EDTERROR to ENDP
*
*     Read AGLF using Agents details from fetched CHDR record.
*
*     Update Sort record.
*
*     Release Sort record and update Proposal Unissued count.
*
*   2000B-READ-PTRN.
*   ----------------
*     Fetch PTRN details
*        when not found set WSSP-EDTERROR to ENDP
*
*     Read CHDR record from CHDRNUM on fetched PTRN record.
*     Read T5679 using fetched PTRN Batch Transaction code.
*
*     If the current Contract Status matches Contract Risk on
*     T5679 :
*
*        Read AGLF using Agents details from CHDR record
*
*        Update Sort record
*
*        Release Sort record and update either Contract Issued
*        or Proposal Terminated count.
*
*   3000-PRINT-REPORT.
*   ------------------
*     Return Sort record
*        on file end set WSAA-EOF = 'Y'
*                    output page header.
*
*     Perform 3000A-PROCESS-FILE
*        until WSAA-EOF = 'Y'
*     End-Perform.
*
*     Output End of Report Banner.
*
*   3000A-PROCESS-FILE.
*   -------------------
*     Increment count of number of records read.
*
*     Perform 3000AA-KEY-CHANGE.
*
*     If a new page has been reached output page header.
*
*     Retain current Sort Record values.
*     Set all indicators to ON.
*
*     Perform 3000AB-OUTPUT-DETAILS.
*
*     Return Sort record
*        on file end set WSAA-EOF = 'Y'
*
*   3000AA-KEY-CHANGE.
*   ------------------
*     If there is a change in Contract Servicing Branch
*        read T1692 and update Branch heading details.
*
*     If there is a change in Agent Area Code
*        read T5696 and update Agent Area Code details.
*
*     If there is a change in Agent Division
*        read T9522 and update Agent Division details.
*
*     If the Agent Number has changed
*        read AGNT using the Sort Agent Number
*        call NAMADRS routine to format Agent name and update
*        Agent name on report heading.
*
*     For all the above cases, when a change is detected, set
*     WSAA-OVERFLOW to 'Y' to indicate a page break.
*
*   3000AB-OUTPUT-DETAILS.
*   ----------------------
*     Note, we will always output atleast 2 detail lines to the
*     printer file. Thereafter, remaining Lives on the contract,
*     and/or, outstanding Follow Ups are printed.
*
*     Read CHDRLNB using Sort Contract Number.
*     Read TH506 using the Contract Type to retrieve Basic
*     Plan details.
*
*     If the current Sort record is a Proposal
*     (Validflag on the CHDR record = 3)
*        Set COVTLNB values to read first valid Coverage
*        Perform 3000ABA-READ-COVT
*           until COVTLNB-STATUZ = ENDP
*        Perform 3000ABB-CALC-FEE
*     Else
*        Set COVRENQ values to read first valid Coverage
*        Perform 3000ABC-READ-COVR
*           until COVRENQ-STATUZ = ENDP
*        Use CHDRENQ-INSTAMT06 to provide Regular premium amount
*     End-If.
*
*     Set print detail line monetary fields.
*
*     Set LIFELNB values to read first valid Life
*
*     Perform 3000ABD-READ-LIFE
*     Call 9100-FATAL-ERROR if LIFELNB-STATUZ = ENDP
*          (atleast one life must be present).
*
*     Read HPAD record for this contract.
*
*     If the Sort Type is '1' (we are processing an unissued propo
*        Set FLUPLNB values to read first valid Follow Up
*        Perform 3000ABE-READ-FLUP
*           until FLUPLNB-STATUZ   = ENDP   or
*                 WSAA-VALID-FLUP  = 'Y'
*     Else
*        Set FLUPLNB-STATUZ to ENDP
*        Read T3623 using Contract Status for status description
*        update print detail line
*     End-If.
*
*     Output the first detail line
*     Set indicators to suppress printing monetary totals.
*
*     Format the HPAD Proposal received Date and update the print
*     detail line.
*
*     Perform 3000ABD-READ-LIFE (get next Life after Primary Life)
*
*     If FLUPLNB-STATUZ  NOT = ENDP
*        Perform 3000ABE-READ-FLUP (Get next outstanding Follow Up
*           until FLUPLNB-STATUZ   = ENDP    or
*                 WSAA-VALID-FLUP  = 'Y'
*     Else
*        Set indicator to suppress printing Follow Up description
*     End-If.
*
*     Output the second line of details.
*
*     Perform until LIFELNB-STATUZ = ENDP    AND
*                   FLUPLNB-STATUZ = ENDP
*
*       If LIFELNB-STATUZ NOT = ENDP  (> 2 lives on the contract)
*          Perform 3000ABD-READ-LIFE
*
*       If FLUPLNB-STATUZ NOT = ENDP  (> 2 outstanding Follow Ups)
*          Perform 3000ABE-READ-FLUP
*             until FLUPLNB-STATUZ    = ENDP    or
*                   WSAA-VALID-FLUP   = 'Y'
*
*         Output remaining details.
*     End-Perform.
*
*   3000ABA-READ-COVT.
*   ------------------
*     Read COVTLNB record.
*
*     If the Coverage is not for this Contract
*        set COVTLNB-STATUZ to ENDP
*     Else
*        If the Coverage matches the Basic Plan (TH506)
*           Add the COVTLNB Sum Assured amount to working store to
*        End-If
*
*        Add Coverage Single Premium & Regular Premium amounts
*        to appropriate working store fields
*     End-If.
*
*   3000ABB-CALC-FEE.
*   ------------------
*     Read T5688 using the Contract Type from CHDRLNB.
*
*     If there is a value in T5688 Fee Method
*        Read T5674 using T5688 Fee Method
*
*        If there is a fee subroutine on T5674
*           Set the Management Fee linkage values
*           Call T5674 Fee Subroutine
*           Add the fee to the Regular Premium Working store total
*        End-If
*     End-If.
*
*   3000ABC-READ-COVR.
*   ------------------
*     Read COVRENQ record.
*
*     If the Coverage is not for this Contract
*        set COVTENQ-STATUZ to ENDP
*     Else
*        If the Coverage matches the Basic Plan (TH506)
*           Add the COVRENQ Sum Assured amount to working store to
*        End-If
*
*        Add Coverage Single Premium amount to working store total
*     End-If.
*
*   3000ABD-READ-LIFE.
*   ------------------
*     Read LIFELNB record.
*
*     If the Life is not insured under this Contract/Proposal
*        set LIFELNB-STATUZ to ENDP
*     Else
*        Read CLTS record using LIFELNB values
*        Call NAMADRS routine to format Client name
*        Update Client name and ID number on detail line
*     End-If.
*
*   3000ABE-READ-FLUP.
*   ------------------
*     Read FLUPLNB record.
*
*     If the Follow Up is not relevant for this Proposal
*        Set FLUPLNB-STATUZ to ENDP
*        Call DATCON3 to determine the number of days from the
*        Proposal Received Date to the Effective Date
*        Update the detail line with the number of elapsed days
*     Else
*        Read T5661 using the FLUPLNB-FUPCODE
*        Set indicator to denote the Follow up is valid
*
*        Compare the Status on FLUPLNB with the range of
*        Completed Stati on T5661
*        When a match is found, set the indicator back to
*        denote the Follow Up is invalid
*
*        If the Follow Up is valid, get the Follow up description
*        and update the detail line.
*     End-If.
*
*   4000-CLOSE.
*   -----------
*     Close Printer file.
*
*   8000-CALL-CONTOT.
*   -----------------
*     Call CONTOT to increment appropriate record count.
*
*   9000-SQL-ERROR.
*   ---------------
*     If an SQL error is detected SYSR-STATUZ and SYSR-PARAMS
*     are updated with SQL values before performing 9100-FATAL-ERR
*
*   9100-FATAL-ERROR.
*   -----------------
*     Call SYSERR to log details from SYSR-STATUZ and SYSR-PARAMS.
*     Move 'BOMB' to WSSP-EDTERROR
*                    LSAA-STATUZ.
*     Exit Program.
*
*
*   Control totals:
*     01  -  Number of printed Proposals unissued.
*     02  -  Number of printed Contracts issued.
*     03  -  Number of printed Contracts/proposals terminated.
*     04  -  Total number of records read.
*
*
******************************************************************
*
* </pre>
*/
public class Bh560 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlchdrCursorrs = null;
	private java.sql.PreparedStatement sqlchdrCursorps = null;
	private java.sql.Connection sqlchdrCursorconn = null;
	private String sqlchdrCursor = "";
	private java.sql.ResultSet sqlptrnCursorrs = null;
	private java.sql.PreparedStatement sqlptrnCursorps = null;
	private java.sql.Connection sqlptrnCursorconn = null;
	private String sqlptrnCursor = "";
	private Rh560Report printerFile = new Rh560Report();
	private SortFileDAM chdrSortFile = new SortFileDAM("LU00");
	private FixedLengthStringData printerRecord = new FixedLengthStringData(250);

	private FixedLengthStringData cntSortRec = new FixedLengthStringData(22);
	private FixedLengthStringData sortKey = new FixedLengthStringData(22).isAPartOf(cntSortRec, 0);
	private FixedLengthStringData sortCntbranch = new FixedLengthStringData(2).isAPartOf(sortKey, 0);
	private FixedLengthStringData sortAracde = new FixedLengthStringData(3).isAPartOf(sortKey, 2);
	private FixedLengthStringData sortAgntnum = new FixedLengthStringData(8).isAPartOf(sortKey, 5);
	private FixedLengthStringData sortChdrnum = new FixedLengthStringData(8).isAPartOf(sortKey, 13);
	private FixedLengthStringData sortType = new FixedLengthStringData(1).isAPartOf(sortKey, 21);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH560");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private PackedDecimalData wsaaDateFrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaDateTo = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(11, 2);

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private FixedLengthStringData wsaaLastRec = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaLastCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaLastRec, 0);
	private FixedLengthStringData wsaaLastAracde = new FixedLengthStringData(3).isAPartOf(wsaaLastRec, 2);
	private FixedLengthStringData wsaaLastAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaLastRec, 5);
	private FixedLengthStringData wsaaLastChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaLastRec, 13);
	private FixedLengthStringData wsaaLastType = new FixedLengthStringData(1).isAPartOf(wsaaLastRec, 21);
	private ZonedDecimalData wsaaOvrprtfNumChar = new ZonedDecimalData(8, 5).init(73);
	private PackedDecimalData wsaaOvrprtfLen = new PackedDecimalData(15, 5);
	private static final String wsaaNmadFmt = "ADRSE";
	private ZonedDecimalData wsaaOsdays = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaServunit = new FixedLengthStringData(2);
	private PackedDecimalData wsaaSingp = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(11, 0);
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaValidFlup = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaSqlAChdrDets = new FixedLengthStringData(21);
	private FixedLengthStringData wsaaSqlAChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlAChdrDets, 0);
	private FixedLengthStringData wsaaSqlAChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaSqlAChdrDets, 1);
	private FixedLengthStringData wsaaSqlAAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaSqlAChdrDets, 9);
	private FixedLengthStringData wsaaSqlACntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlAChdrDets, 17);
	private FixedLengthStringData wsaaSqlAServunit = new FixedLengthStringData(2).isAPartOf(wsaaSqlAChdrDets, 19);

	private FixedLengthStringData wsaaSqlBPtrnDets = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaSqlBBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaSqlBPtrnDets, 0);
	private FixedLengthStringData wsaaSqlBChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlBPtrnDets, 4);
	private PackedDecimalData wsaaSqlBPtrneff = new PackedDecimalData(8, 0).isAPartOf(wsaaSqlBPtrnDets, 5);
	private FixedLengthStringData wsaaSqlBValidflag = new FixedLengthStringData(1).isAPartOf(wsaaSqlBPtrnDets, 10);
	private FixedLengthStringData wsaaSqlBChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaSqlBPtrnDets, 11);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);
		/* TABLES */
	private static final String th506 = "TH506";
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3623 = "T3623";
	private static final String t5661 = "T5661";
	private static final String t5674 = "T5674";
	private static final String t5679 = "T5679";
	private static final String t5688 = "T5688";
	private static final String t5696 = "T5696";
	private static final String th565 = "TH565";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AglfTableDAM aglfIO = new AglfTableDAM();
	private AgntTableDAM agntIO = new AgntTableDAM();
	private BprdTableDAM bprdIO = new BprdTableDAM();
	private BsprTableDAM bsprIO = new BsprTableDAM();
	private BsscTableDAM bsscIO = new BsscTableDAM();
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Contotrec contotrec = new Contotrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Srvunitcpy srvunitcpy = new Srvunitcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Th506rec th506rec = new Th506rec();
	private T5661rec t5661rec = new T5661rec();
	private T5674rec t5674rec = new T5674rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private Th565rec th565rec = new Th565rec();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaCntStatiArrayInner wsaaCntStatiArrayInner = new WsaaCntStatiArrayInner();
	private WsaaOvrprtfRecInner wsaaOvrprtfRecInner = new WsaaOvrprtfRecInner();
	private WsaaRh560Inner wsaaRh560Inner = new WsaaRh560Inner();
	private WsaaTrancodeArrayInner wsaaTrancodeArrayInner = new WsaaTrancodeArrayInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2000a, 
		exit2000a, 
		eof2000b, 
		exit2000b
	}

	public Bh560() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			main000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main000()
	{
		/*START*/
		initialise1000();
		chdrSortFile.openOutput();
		select2000();
		chdrSortFile.close();
		FileSort fs1 = new FileSort(chdrSortFile);
		fs1.addSortKey(sortKey, true);
		fs1.sort();
		chdrSortFile.openInput();
		printReport3000();
		chdrSortFile.close();
		close4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		start1000();
		setUpHeadingCompany1000();
		setUpHeadingDates1000();
		defineCursors1000();
	}

protected void start1000()
	{
		/* Override print file attributes using defined values*/
		wsaaOvrprtfLen.set(wsaaOvrprtfNumChar);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaOvrprtfRecInner.wsaaOvrprtfRec, wsaaOvrprtfLen);
		/* Open output file & retrieve linkage values.*/
		printerFile.openOutput();
		bsscIO.setDataArea(lsaaBsscrec);
		bsprIO.setDataArea(lsaaBsprrec);
		bprdIO.setDataArea(lsaaBprdrec);
		bupaIO.setDataArea(lsaaBuparec);
		wsaaCntStatiArrayInner.wsaaCntStatiArray.set(SPACES);
		wsaaLastRec.set(SPACES);
		wsaaTrancodeArrayInner.wsaaTrancodeArray.set(SPACES);
	}

protected void setUpHeadingCompany1000()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(errorsInner.g875);
			syserrrec.params.set(descIO.getParams());
			fatalError9100();
		}
		wsaaRh560Inner.rh01Company.set(bsprIO.getCompany());
		wsaaRh560Inner.rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1000()
	{
		/* Format Report Date using DATCON6.*/
		datcon6rec.language.set(bsscIO.getLanguage());
		datcon6rec.company.set(bsprIO.getCompany());
		datcon6rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon6rec.statuz);
			syserrrec.params.set(datcon6rec.datcon6Rec);
			fatalError9100();
		}
		wsaaRh560Inner.rh01Rptdate.set(datcon6rec.intDate2);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaRh560Inner.rh01Sdate.set(datcon1rec.extDate);
	}

protected void defineCursors1000()
	{
		/* Read T5679 for valid contract stati.*/
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(errorsInner.f321);
			syserrrec.params.set(itemIO.getParams());
			fatalError9100();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Set up the SQL string to select valid contract stati.*/
		wsaaSub2.set(1);
		for (wsaaSub1.set(1); !(isGT(wsaaSub1, 12)); wsaaSub1.add(1)){
			if (isEQ(isNE(t5679rec.cnRiskStat[wsaaSub1.toInt()], SPACES), true)){
				wsaaCntStatiArrayInner.wsaaCntStatus[wsaaSub2.toInt()].set(t5679rec.cnRiskStat[wsaaSub1.toInt()]);
				wsaaSub2.add(1);
			}
		}
		wsaaServunit.set(srvunitcpy.lifeVal);
		wsaaCompany.set(bsprIO.getCompany());
		/* Read TH565 for Transaction Code range.*/
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th565);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(errorsInner.g641);
			syserrrec.params.set(itemIO.getParams());
			fatalError9100();
		}
		th565rec.th565Rec.set(itemIO.getGenarea());
		/* Set up the SQL variables to select valid PTRN's.*/
		wsaaSub2.set(1);
		for (wsaaSub1.set(1); !(isGT(wsaaSub1, 10)); wsaaSub1.add(1)){
			if (isEQ(isNE(th565rec.ztrcde[wsaaSub1.toInt()], SPACES), true)){
				wsaaTrancodeArrayInner.wsaaTcdeValue[wsaaSub2.toInt()].set(th565rec.ztrcde[wsaaSub1.toInt()]);
				wsaaSub2.add(1);
			}
		}
		/* Establish date range variables to read valid PTRN records.*/
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		datcon2rec.frequency.set("52");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			fatalError9100();
		}
		wsaaDateTo.set(datcon2rec.intDate1);
		wsaaDateFrom.set(datcon2rec.intDate2);
	}

protected void select2000()
	{
		read2000();
	}

	/**
	* <pre>
	* Select all valid CHDR records, then execute the second
	* sequence of SQL statements to select valid PTRN's.
	* </pre>
	*/
protected void read2000()
	{
		wsspEdterror.set(varcom.oK);
		/* Declare cursor to read contracts by status.*/
		sqlchdrCursor = " SELECT  CHDRCOY, CHDRNUM, AGNTNUM, CNTBRANCH, SERVUNIT" +
" FROM   " + getAppVars().getTableNameOverriden("CHDRPF") + " " +
" WHERE (CHDRCOY = ?" +
" AND STATCODE IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" +
" AND SERVUNIT = ?)";
		/* Open cursor to read CHDR records.*/
		sqlerrorflag = false;
		try {
			sqlchdrCursorconn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlchdrCursorps = getAppVars().prepareStatementEmbeded(sqlchdrCursorconn, sqlchdrCursor, "CHDRPF");
			getAppVars().setDBString(sqlchdrCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlchdrCursorps, 2, wsaaCntStatiArrayInner.wsaaCnRiskStat01);
			getAppVars().setDBString(sqlchdrCursorps, 3, wsaaCntStatiArrayInner.wsaaCnRiskStat02);
			getAppVars().setDBString(sqlchdrCursorps, 4, wsaaCntStatiArrayInner.wsaaCnRiskStat03);
			getAppVars().setDBString(sqlchdrCursorps, 5, wsaaCntStatiArrayInner.wsaaCnRiskStat04);
			getAppVars().setDBString(sqlchdrCursorps, 6, wsaaCntStatiArrayInner.wsaaCnRiskStat05);
			getAppVars().setDBString(sqlchdrCursorps, 7, wsaaCntStatiArrayInner.wsaaCnRiskStat06);
			getAppVars().setDBString(sqlchdrCursorps, 8, wsaaCntStatiArrayInner.wsaaCnRiskStat07);
			getAppVars().setDBString(sqlchdrCursorps, 9, wsaaCntStatiArrayInner.wsaaCnRiskStat08);
			getAppVars().setDBString(sqlchdrCursorps, 10, wsaaCntStatiArrayInner.wsaaCnRiskStat09);
			getAppVars().setDBString(sqlchdrCursorps, 11, wsaaCntStatiArrayInner.wsaaCnRiskStat10);
			getAppVars().setDBString(sqlchdrCursorps, 12, wsaaCntStatiArrayInner.wsaaCnRiskStat11);
			getAppVars().setDBString(sqlchdrCursorps, 13, wsaaCntStatiArrayInner.wsaaCnRiskStat12);
			getAppVars().setDBString(sqlchdrCursorps, 14, wsaaServunit);
			sqlchdrCursorrs = getAppVars().executeQuery(sqlchdrCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
		while ( !(isEQ(wsspEdterror, varcom.endp))) {
			readChdr2000a();
		}
		
		/* Close cursor to read CHDR records.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlchdrCursorconn, sqlchdrCursorps, sqlchdrCursorrs);
		wsspEdterror.set(varcom.oK);
		/* Declare cursor to read PTRN's by Transation Code &*/
		/* Effective Date.*/
		sqlptrnCursor = " SELECT  BATCTRCDE, CHDRCOY, PTRNEFF, VALIDFLAG, CHDRNUM" +
" FROM   " + getAppVars().getTableNameOverriden("PTRNPF") + " " +
" WHERE (CHDRCOY = ?" +
" AND BATCTRCDE IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" +
" AND (PTRNEFF BETWEEN ? AND ?)" +
" AND VALIDFLAG <> '2')";
		/* Open cursor to read PTRN records.*/
		sqlerrorflag = false;
		try {
			sqlptrnCursorconn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.PtrnpfTableDAM());
			sqlptrnCursorps = getAppVars().prepareStatementEmbeded(sqlptrnCursorconn, sqlptrnCursor, "PTRNPF");
			getAppVars().setDBString(sqlptrnCursorps, 1, wsaaCompany);
			getAppVars().setDBString(sqlptrnCursorps, 2, wsaaTrancodeArrayInner.wsaaTrcde01);
			getAppVars().setDBString(sqlptrnCursorps, 3, wsaaTrancodeArrayInner.wsaaTrcde02);
			getAppVars().setDBString(sqlptrnCursorps, 4, wsaaTrancodeArrayInner.wsaaTrcde03);
			getAppVars().setDBString(sqlptrnCursorps, 5, wsaaTrancodeArrayInner.wsaaTrcde04);
			getAppVars().setDBString(sqlptrnCursorps, 6, wsaaTrancodeArrayInner.wsaaTrcde05);
			getAppVars().setDBString(sqlptrnCursorps, 7, wsaaTrancodeArrayInner.wsaaTrcde06);
			getAppVars().setDBString(sqlptrnCursorps, 8, wsaaTrancodeArrayInner.wsaaTrcde07);
			getAppVars().setDBString(sqlptrnCursorps, 9, wsaaTrancodeArrayInner.wsaaTrcde08);
			getAppVars().setDBString(sqlptrnCursorps, 10, wsaaTrancodeArrayInner.wsaaTrcde09);
			getAppVars().setDBString(sqlptrnCursorps, 11, wsaaTrancodeArrayInner.wsaaTrcde10);
			getAppVars().setDBNumber(sqlptrnCursorps, 12, wsaaDateFrom);
			getAppVars().setDBNumber(sqlptrnCursorps, 13, wsaaDateTo);
			sqlptrnCursorrs = getAppVars().executeQuery(sqlptrnCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
		while ( !(isEQ(wsspEdterror, varcom.endp))) {
			readPtrn2000b();
		}
		
		/* Close cursor to read PTRN records.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlptrnCursorconn, sqlptrnCursorps, sqlptrnCursorrs);
	}

protected void readChdr2000a()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read2000a();
					readAglf2000a();
				case eof2000a: 
					eof2000a();
				case exit2000a: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read2000a()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlchdrCursorrs)) {
				getAppVars().getDBObject(sqlchdrCursorrs, 1, wsaaSqlAChdrcoy);
				getAppVars().getDBObject(sqlchdrCursorrs, 2, wsaaSqlAChdrnum);
				getAppVars().getDBObject(sqlchdrCursorrs, 3, wsaaSqlAAgntnum);
				getAppVars().getDBObject(sqlchdrCursorrs, 4, wsaaSqlACntbranch);
				getAppVars().getDBObject(sqlchdrCursorrs, 5, wsaaSqlAServunit);
			}
			else {
				goTo(GotoLabel.eof2000a);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
	}

protected void readAglf2000a()
	{
		aglfIO.setParams(SPACES);
		aglfIO.setAgntcoy(wsaaSqlAChdrcoy);
		aglfIO.setAgntnum(wsaaSqlAAgntnum);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError9100();
		}
		/* Set up & release sort record.*/
		sortCntbranch.set(wsaaSqlACntbranch);
		sortAracde.set(aglfIO.getAracde());
		sortAgntnum.set(wsaaSqlAAgntnum);
		sortChdrnum.set(wsaaSqlAChdrnum);
		sortType.set("1");
		chdrSortFile.write(cntSortRec);
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot8000();
		goTo(GotoLabel.exit2000a);
	}

protected void eof2000a()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void readPtrn2000b()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read2000b();
					readChdrlnb2000b();
				case eof2000b: 
					eof2000b();
				case exit2000b: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read2000b()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlptrnCursorrs)) {
				getAppVars().getDBObject(sqlptrnCursorrs, 1, wsaaSqlBBatctrcde);
				getAppVars().getDBObject(sqlptrnCursorrs, 2, wsaaSqlBChdrcoy);
				getAppVars().getDBObject(sqlptrnCursorrs, 3, wsaaSqlBPtrneff);
				getAppVars().getDBObject(sqlptrnCursorrs, 4, wsaaSqlBValidflag);
				getAppVars().getDBObject(sqlptrnCursorrs, 5, wsaaSqlBChdrnum);
			}
			else {
				goTo(GotoLabel.eof2000b);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
	}

protected void readChdrlnb2000b()
	{
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(wsaaSqlBChdrcoy);
		chdrlnbIO.setChdrnum(wsaaSqlBChdrnum);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError9100();
		}
		/* Read T5679.*/
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsaaSqlBChdrcoy);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaSqlBBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(errorsInner.f321);
			syserrrec.params.set(itemIO.getParams());
			fatalError9100();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Compare current contract status with PTRN action &*/
		/* output a sort record if there has been no change.*/
		if (isEQ(chdrlnbIO.getStatcode(), t5679rec.setCnRiskStat)) {
			aglfIO.setParams(SPACES);
			aglfIO.setAgntcoy(chdrlnbIO.getChdrcoy());
			aglfIO.setAgntnum(chdrlnbIO.getAgntnum());
			aglfIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, aglfIO);
			if (isNE(aglfIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(aglfIO.getStatuz());
				syserrrec.params.set(aglfIO.getParams());
				fatalError9100();
			}
			sortCntbranch.set(chdrlnbIO.getCntbranch());
			sortAracde.set(aglfIO.getAracde());
			sortAgntnum.set(chdrlnbIO.getAgntnum());
			sortChdrnum.set(chdrlnbIO.getChdrnum());
			sortType.set("2");
			chdrSortFile.write(cntSortRec);
			/* If the transaction was to Issue a contract, update*/
			/* count 2, otherwise update count 3.*/
			if (isEQ(wsaaSqlBBatctrcde, "T642")) {
				contotrec.totno.set(ct02);
				contotrec.totval.set(1);
				callContot8000();
			}
			else {
				contotrec.totno.set(ct03);
				contotrec.totval.set(1);
				callContot8000();
			}
		}
		goTo(GotoLabel.exit2000b);
	}

protected void eof2000b()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void printReport3000()
	{
		start3000();
	}

protected void start3000()
	{
		chdrSortFile.read(cntSortRec);
		if (chdrSortFile.isAtEnd()) {
			wsaaEof.set("Y");
			printerFile.printRh560h01(wsaaRh560Inner.wsaaRh560H01, indicArea);
		}
		/* Process each of the sort records.*/
		if (!endOfFile.isTrue()) {
			while ( !(endOfFile.isTrue())) {
				processFile3000a();
			}
			
			if (newPageReq.isTrue()) {
				printerRecord.set(SPACES);
				printerFile.printRh560h01(wsaaRh560Inner.wsaaRh560H01, indicArea);
				wsaaOverflow.set("N");
			}
		}
		/* Output the 'End Report' banner.*/
		printerFile.printRh560f01(wsaaRh560Inner.wsaaRh560F01, indicArea);
	}

protected void processFile3000a()
	{
		start3000a();
	}

	/**
	* <pre>
	* Check to see if a page break has been reached.
	* </pre>
	*/
protected void start3000a()
	{
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot8000();
		keyChange3000aa();
		if (newPageReq.isTrue()) {
			printerFile.printRh560h01(wsaaRh560Inner.wsaaRh560H01, indicArea);
			wsaaOverflow.set("N");
		}
		wsaaLastRec.set(cntSortRec);
		for (wsaaSub1.set(1); !(isGT(wsaaSub1, 99)); wsaaSub1.add(1)){
			indOn.setTrue(wsaaSub1);
		}
		/* Output sort record details & read next record.*/
		outputDetails3000ab();
		chdrSortFile.read(cntSortRec);
		if (chdrSortFile.isAtEnd()) {
			wsaaEof.set("Y");
		}
	}

protected void keyChange3000aa()
	{
		start3000aa();
	}

	/**
	* <pre>
	* See if Branch has changed.
	* </pre>
	*/
protected void start3000aa()
	{
		if (isNE(sortCntbranch, wsaaLastCntbranch)) {
			descIO.setDescpfx(smtpfxcpy.item);
			descIO.setDesccoy(bsprIO.getCompany());
			descIO.setDesctabl(t1692);
			descIO.setDescitem(sortCntbranch);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(bsscIO.getLanguage());
			descIO.setFunction(varcom.readr);
			descIO.setFormat(formatsInner.descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(errorsInner.g874);
				syserrrec.params.set(descIO.getParams());
				fatalError9100();
			}
			wsaaRh560Inner.rh01Cntbranch.set(sortCntbranch);
			wsaaRh560Inner.rh01Branchnm.set(descIO.getLongdesc());
			wsaaOverflow.set("Y");
		}
		/* Check to see if the Agent District has changed.*/
		if (isNE(sortAracde, wsaaLastAracde)) {
			descIO.setDescpfx(smtpfxcpy.item);
			descIO.setDesccoy(bsprIO.getCompany());
			descIO.setDesctabl(t5696);
			descIO.setDescitem(sortAracde);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(bsscIO.getLanguage());
			descIO.setFunction(varcom.readr);
			descIO.setFormat(formatsInner.descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(errorsInner.f176);
				syserrrec.params.set(descIO.getParams());
				fatalError9100();
			}
			wsaaRh560Inner.rh01Distcode.set(sortAracde);
			wsaaRh560Inner.rh01Distdesc.set(descIO.getLongdesc());
			wsaaOverflow.set("Y");
		}
		/* If the Agent has changed, read the AGNT file to get the*/
		/* Client number then format the agents' name.*/
		if (isNE(sortAgntnum, wsaaLastAgntnum)) {
			agntIO.setParams(SPACES);
			agntIO.setAgntpfx(fsupfxcpy.agnt);
			agntIO.setAgntcoy(bsprIO.getCompany());
			agntIO.setAgntnum(sortAgntnum);
			agntIO.setFormat(formatsInner.agntrec);
			agntIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, agntIO);
			if (isNE(agntIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(agntIO.getStatuz());
				syserrrec.params.set(agntIO.getParams());
				fatalError9100();
			}
			wsaaRh560Inner.rh01Agntnum.set(agntIO.getAgntnum());
			namadrsrec.clntPrefix.set(agntIO.getClntpfx());
			namadrsrec.clntCompany.set(agntIO.getClntcoy());
			namadrsrec.clntNumber.set(agntIO.getClntnum());
			namadrsrec.language.set(bsscIO.getLanguage());
			namadrsrec.function.set("LGNMN");
			callProgram(Namadrs.class, namadrsrec.namadrsRec);
			if (isNE(namadrsrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(namadrsrec.statuz);
				syserrrec.params.set(namadrsrec.namadrsRec);
				fatalError9100();
			}
			wsaaRh560Inner.rh01Agtname.set(namadrsrec.name);
			wsaaOverflow.set("Y");
		}
	}

protected void outputDetails3000ab()
	{
		start3000ab();
	}

	/**
	* <pre>
	* Retrieve shared information first, before branching off to
	* output either proposal or terminated proposal details.
	* </pre>
	*/
protected void start3000ab()
	{
		/* Read CHDRLNB for contract details.*/
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(bsprIO.getCompany());
		chdrlnbIO.setChdrnum(sortChdrnum);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError9100();
		}
		/* Read TH506 for basic plan.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th506);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(errorsInner.hl24);
			syserrrec.params.set(itemIO.getParams());
			fatalError9100();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			itemIO.setFormat(formatsInner.itemrec);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(errorsInner.hl24);
				syserrrec.params.set(itemIO.getParams());
				fatalError9100();
			}
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		/* If processing a proposal, (CHDR-VALIDFLAG = 3), read*/
		/* all COVT records to calculate monetary amounts, else*/
		/* read COVR records for this information.*/
		wsaaSumins.set(ZERO);
		wsaaSingp.set(ZERO);
		wsaaInstprem.set(ZERO);
		if (isEQ(chdrlnbIO.getValidflag(), "3")) {
			covtlnbIO.setParams(SPACES);
			covtlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			covtlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
			covtlnbIO.setLife("01");
			covtlnbIO.setCoverage("01");
			covtlnbIO.setRider("00");
			covtlnbIO.setSeqnbr(ZERO);
			covtlnbIO.setFormat(formatsInner.covtlnbrec);
			covtlnbIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
			covtlnbIO.setStatuz(varcom.oK);
			while ( !(isEQ(covtlnbIO.getStatuz(), varcom.endp))) {
				readCovt3000aba();
			}
			
			calcFee3000abb();
		}
		else {
			covrenqIO.setParams(SPACES);
			covrenqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			covrenqIO.setChdrnum(chdrlnbIO.getChdrnum());
			covrenqIO.setPlanSuffix(ZERO);
			covrenqIO.setFormat(formatsInner.covrenqrec);
			covrenqIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
			covrenqIO.setStatuz(varcom.oK);
			while ( !(isEQ(covrenqIO.getStatuz(), varcom.endp))) {
				readCovr3000abc();
			}
			
			wsaaInstprem.add(chdrlnbIO.getSinstamt06());
		}
		/* Set up monetary amounts output on the print file.*/
		wsaaRh560Inner.rd01Polid.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlnbIO.getCnttype());
		stringVariable1.addExpression(chdrlnbIO.getChdrnum());
		stringVariable1.setStringInto(wsaaRh560Inner.rd01Polid);
		wsaaRh560Inner.rd01Cntcurr.set(chdrlnbIO.getCntcurr());
		wsaaRh560Inner.rd01Sumins.set(wsaaSumins);
		compute(wsaaRh560Inner.rd01ModalPrem, 2).set(add(wsaaSingp, wsaaInstprem));
		wsaaBillfreq9.set(chdrlnbIO.getBillfreq());
		compute(wsaaRh560Inner.rd01AnnualPrem, 2).set(mult(wsaaInstprem, wsaaBillfreq9));
		/* Read the first life for this contract.*/
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		lifelnbIO.setStatuz(varcom.oK);
		readLife3000abd();
		/* If a life is not found for this contract, output an error.*/
		if (isEQ(lifelnbIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(errorsInner.h143);
			syserrrec.params.set(chdrlnbIO.getChdrnum());
			fatalError9100();
		}
		/* Read HPAD file for later use.*/
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlnbIO.getChdrnum());
		hpadIO.setFormat(formatsInner.hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError9100();
		}
		/* For an outstanding proposal, check for outstanding*/
		/* Follow Ups, else determine the contract status, (we also*/
		/* set FLUPLNB-STATUZ to ENDP for exclusion in later tests).*/
		if (isEQ(sortType, "1")) {
			fluplnbIO.setParams(SPACES);
			fluplnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			fluplnbIO.setChdrnum(chdrlnbIO.getChdrnum());
			fluplnbIO.setFupno(ZERO);
			fluplnbIO.setFormat(formatsInner.fluplnbrec);
			fluplnbIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			fluplnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			fluplnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
			wsaaValidFlup.set(SPACES);
			while ( !(isEQ(fluplnbIO.getStatuz(), varcom.endp)
			|| isEQ(wsaaValidFlup, "Y"))) {
				readFlup3000abe();
			}
			
		}
		else {
			fluplnbIO.setStatuz(varcom.endp);
			descIO.setDescpfx(smtpfxcpy.item);
			descIO.setDesccoy(chdrlnbIO.getChdrcoy());
			descIO.setDesctabl(t3623);
			descIO.setDescitem(chdrlnbIO.getStatcode());
			descIO.setItemseq(SPACES);
			descIO.setLanguage(bsscIO.getLanguage());
			descIO.setFunction(varcom.readr);
			descIO.setFormat(formatsInner.descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(errorsInner.h924);
				syserrrec.params.set(descIO.getParams());
				fatalError9100();
			}
			wsaaRh560Inner.rd01Descr.set(descIO.getLongdesc());
		}
		/* Output the first detail line.*/
		if (newPageReq.isTrue()) {
			printerFile.printRh560h01(wsaaRh560Inner.wsaaRh560H01, indicArea);
			wsaaOverflow.set("N");
		}
		printerFile.printRh560d01(wsaaRh560Inner.wsaaRh560D01, indicArea);
		/* Set indicators and output second line on the report.*/
		indOff.setTrue(30);
		indOff.setTrue(33);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(hpadIO.getHprrcvdt());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError9100();
		}
		datcon1rec.extDate.set(inspectReplaceLeading(datcon1rec.extDate, "0", SPACES));
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("(");
		stringVariable2.addExpression(datcon1rec.extDate);
		stringVariable2.addExpression(")");
		stringVariable2.setStringInto(wsaaRh560Inner.rd01Polid);
		readLife3000abd();
		if (isNE(fluplnbIO.getStatuz(), varcom.endp)) {
			wsaaValidFlup.set(SPACES);
			while ( !(isEQ(fluplnbIO.getStatuz(), varcom.endp)
			|| isEQ(wsaaValidFlup, "Y"))) {
				readFlup3000abe();
			}
			
		}
		else {
			indOff.setTrue(34);
		}
		if (newPageReq.isTrue()) {
			printerFile.printRh560h01(wsaaRh560Inner.wsaaRh560H01, indicArea);
			wsaaOverflow.set("N");
		}
		printerFile.printRh560d01(wsaaRh560Inner.wsaaRh560D01, indicArea);
		/* Output remaining details.*/
		indOff.setTrue(31);
		while ( !(isEQ(lifelnbIO.getStatuz(), varcom.endp)
		&& isEQ(fluplnbIO.getStatuz(), varcom.endp))) {
			if (isNE(lifelnbIO.getStatuz(), varcom.endp)) {
				readLife3000abd();
			}
			if (isNE(fluplnbIO.getStatuz(), varcom.endp)) {
				wsaaValidFlup.set(SPACES);
				while ( !(isEQ(fluplnbIO.getStatuz(), varcom.endp)
				|| isEQ(wsaaValidFlup, "Y"))) {
					readFlup3000abe();
				}
				
			}
			else {
				indOff.setTrue(34);
			}
			/* We use indicator 34 to denote Follow Up processing is complete*/
			/* as we need to print an additional line after the Follow Ups*/
			/* with the number of days the proposal is outstanding.*/
			if (isNE(lifelnbIO.getStatuz(), varcom.endp)
			|| indOn.isTrue(34)) {
				if (newPageReq.isTrue()) {
					printerFile.printRh560h01(wsaaRh560Inner.wsaaRh560H01, indicArea);
					wsaaOverflow.set("N");
				}
				printerFile.printRh560d01(wsaaRh560Inner.wsaaRh560D01, indicArea);
			}
		}
		
	}

protected void readCovt3000aba()
	{
		start3000aba();
	}

protected void start3000aba()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
		&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError9100();
		}
		if (isNE(covtlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		else {
			if (isEQ(th506rec.crtable, SPACES)
			&& isEQ(covtlnbIO.getRider(), "00")) {
				th506rec.crtable.set(covtlnbIO.getCrtable());
			}
			if (isEQ(covtlnbIO.getCrtable(), th506rec.crtable)) {
				wsaaSumins.add(covtlnbIO.getSumins());
			}
			wsaaSingp.add(covtlnbIO.getSingp());
			wsaaInstprem.add(covtlnbIO.getInstprem());
			covtlnbIO.setFunction(varcom.nextr);
		}
	}

protected void calcFee3000abb()
	{
		start3000abb();
	}

protected void start3000abb()
	{
		/* Read T5688 for fee method, then T5674 for the fee*/
		/* calculation subroutine.  Call the subroutine and add*/
		/* the management fees to the instalment premium.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5688);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(errorsInner.e308);
			syserrrec.params.set(itemIO.getParams());
			fatalError9100();
		}
		t5688rec.t5688Rec.set(itemIO.getGenarea());
		/* Exit section if no fee method is present.*/
		if (isEQ(t5688rec.feemeth, SPACES)) {
			return ;
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(errorsInner.f151);
			syserrrec.params.set(itemIO.getParams());
			fatalError9100();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		/* Exit section if no fee subroutine exists.*/
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		/* Having got this far, management fees are applicable.*/
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set(chdrlnbIO.getBillfreq());
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(bsprIO.getCompany());
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, varcom.oK)
		&& isNE(mgfeelrec.statuz, varcom.endp)) {
			syserrrec.statuz.set(mgfeelrec.statuz);
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			fatalError9100();
		}
		wsaaInstprem.add(mgfeelrec.mgfee);
	}

protected void readCovr3000abc()
	{
		start3000abc();
	}

protected void start3000abc()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(), varcom.oK)
		&& isNE(covrenqIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covrenqIO.getStatuz());
			syserrrec.params.set(covrenqIO.getParams());
			fatalError9100();
		}
		if (isNE(covrenqIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(covrenqIO.getStatuz(), varcom.endp)) {
			covrenqIO.setStatuz(varcom.endp);
		}
		else {
			if (isEQ(th506rec.crtable, SPACES)
			&& isEQ(covrenqIO.getRider(), "00")) {
				th506rec.crtable.set(covrenqIO.getCrtable());
			}
			if (isEQ(covrenqIO.getCrtable(), th506rec.crtable)) {
				wsaaSumins.add(covrenqIO.getSumins());
			}
			wsaaSingp.add(covrenqIO.getSingp());
			covrenqIO.setFunction(varcom.nextr);
		}
	}

protected void readLife3000abd()
	{
		start3000abd();
	}

protected void start3000abd()
	{
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError9100();
		}
		if (isNE(lifelnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(lifelnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(lifelnbIO.getStatuz(), varcom.endp)) {
			lifelnbIO.setStatuz(varcom.endp);
			indOff.setTrue(32);
			return ;
		}
		/* Read CLTS for details of the life insured.*/
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(chdrlnbIO.getCowncoy());
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(formatsInner.cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError9100();
		}
		namadrsrec.clntPrefix.set(cltsIO.getClntpfx());
		namadrsrec.clntCompany.set(cltsIO.getClntcoy());
		namadrsrec.clntNumber.set(cltsIO.getClntnum());
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.function.set(wsaaNmadFmt);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError9100();
		}
		if (isEQ(lifelnbIO.getJlife(), "00")) {
			wsaaRh560Inner.rd01Clntname.set(namadrsrec.name);
		}
		else {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(namadrsrec.name);
			stringVariable1.setStringInto(wsaaRh560Inner.rd01Clntname);
		}
		wsaaRh560Inner.rd01IdNumber.set(cltsIO.getSecuityno());
		lifelnbIO.setFunction(varcom.nextr);
	}

protected void readFlup3000abe()
	{
		start3000abe();
	}

protected void start3000abe()
	{
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(), varcom.oK)
		&& isNE(fluplnbIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError9100();
		}
		/* When all Follow Ups have been read, output the number of*/
		/* days between the run date & the Proposal Received Date.*/
		if (isNE(fluplnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(fluplnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(fluplnbIO.getStatuz(), varcom.endp)) {
			fluplnbIO.setStatuz(varcom.endp);
			datcon3rec.intDate1.set(hpadIO.getHprrcvdt());
			datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
			datcon3rec.frequency.set("DY");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError9100();
			}
			wsaaOsdays.set(datcon3rec.freqFactor);
			wsaaOsdays.set(inspectReplaceLeading(wsaaOsdays, "0", SPACES));
			wsaaRh560Inner.rd01Descr.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("(");
			stringVariable1.addExpression(wsaaOsdays);
			stringVariable1.addExpression(" Days)");
			stringVariable1.setStringInto(wsaaRh560Inner.rd01Descr);
			return ;
		}
		/* Read T5661 for the Follow Up code.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(t5661);
		/*    MOVE FLUPLNB-FUPCODE           TO ITEM-ITEMITEM.             */
		wsaaT5661Lang.set(bsscIO.getLanguage());
		wsaaT5661Fupcode.set(fluplnbIO.getFupcode());
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(errorsInner.g641);
			syserrrec.params.set(itemIO.getParams());
			fatalError9100();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		/* Ensure Follow Up code is valid for this Letter Type and*/
		/* is still outstanding before outputting text*/
		wsaaValidFlup.set("Y");
		for (wsaaSub1.set(1); !(isGT(wsaaSub1, 10)
		|| isEQ(wsaaValidFlup, "N")); wsaaSub1.add(1)){
			if (isEQ(fluplnbIO.getFupstat(), t5661rec.fuposs[wsaaSub1.toInt()])) {
				wsaaValidFlup.set("N");
			}
		}
		if (isEQ(wsaaValidFlup, "Y")) {
			descIO.setDescpfx(smtpfxcpy.item);
			descIO.setDesccoy(chdrlnbIO.getChdrcoy());
			descIO.setDesctabl(t5661);
			/*       MOVE FLUPLNB-FUPCODE     TO DESC-DESCITEM                 */
			wsaaT5661Lang.set(bsscIO.getLanguage());
			wsaaT5661Fupcode.set(fluplnbIO.getFupcode());
			descIO.setDescitem(wsaaT5661Key);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(bsscIO.getLanguage());
			descIO.setFunction(varcom.readr);
			descIO.setFormat(formatsInner.descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(errorsInner.e402);
				syserrrec.params.set(descIO.getParams());
				fatalError9100();
			}
			wsaaRh560Inner.rd01Descr.set(descIO.getLongdesc());
		}
		fluplnbIO.setFunction(varcom.nextr);
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void callContot8000()
	{
		/*CONTOT*/
		callProgram(Contot.class, contotrec.contotRec);
		/*EXIT*/
	}

protected void sqlError9000()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(errorsInner.esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError9100();
	}

protected void fatalError9100()
	{
		/*FATAL*/
		if ((isNE(syserrrec.statuz, SPACES))
		|| (isNE(syserrrec.syserrStatuz, SPACES))) {
			syserrrec.syserrType.set("4");
		}
		else {
			syserrrec.syserrType.set("3");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
		wsspEdterror.set(varcom.bomb);
		lsaaStatuz.set(varcom.bomb);
		/*EXIT*/
		stopRun();
	}
/*
 * Class transformed  from Data Structure WSAA-OVRPRTF-REC--INNER
 */
private static final class WsaaOvrprtfRecInner { 

		/* For details of parameters, pls refer to OVERPRTF cmd in OS400   
		 you may change LPI, PRTQLTY & other params depending on printer 
		 If chg params, chg NUM-CHAR according to length of ovrprt-rec.  */
	private FixedLengthStringData wsaaOvrprtfRec = new FixedLengthStringData(73);
	private FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(wsaaOvrprtfRec, 0, FILLER).init("OVRPRTF FILE");
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaOvrprtfRec, 12, FILLER).init("(");
	private FixedLengthStringData wsaaOvrpPrtf = new FixedLengthStringData(8).isAPartOf(wsaaOvrprtfRec, 13).init("RH560");
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaOvrprtfRec, 21, FILLER).init(")");
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaOvrprtfRec, 22, FILLER).init(" PAGESIZE(");
	private ZonedDecimalData wsaaOvrpPagelength = new ZonedDecimalData(2, 0).isAPartOf(wsaaOvrprtfRec, 32).init(60).setUnsigned();
	private FixedLengthStringData filler4 = new FixedLengthStringData(1).isAPartOf(wsaaOvrprtfRec, 34, FILLER).init(" ");
	private ZonedDecimalData wsaaOvrpPagewidth = new ZonedDecimalData(3, 0).isAPartOf(wsaaOvrprtfRec, 35).init(198).setUnsigned();
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaOvrprtfRec, 38, FILLER).init(" *ROWCOL)");
	private FixedLengthStringData filler6 = new FixedLengthStringData(7).isAPartOf(wsaaOvrprtfRec, 47, FILLER).init(" LPI(6)");
	private FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(wsaaOvrprtfRec, 54, FILLER).init(" CPI(15)");
	private FixedLengthStringData filler8 = new FixedLengthStringData(11).isAPartOf(wsaaOvrprtfRec, 62, FILLER).init(" OVRFLW(60)");
}
/*
 * Class transformed  from Data Structure WSAA-CNT-STATI-ARRAY--INNER
 */
private static final class WsaaCntStatiArrayInner { 

	private FixedLengthStringData wsaaCntStatiArray = new FixedLengthStringData(24);
	private FixedLengthStringData wsaaCntStati = new FixedLengthStringData(24).isAPartOf(wsaaCntStatiArray, 0);
	private FixedLengthStringData[] wsaaCntStatus = FLSArrayPartOfStructure(12, 2, wsaaCntStati, 0);
	private FixedLengthStringData wsaaSqlStati = new FixedLengthStringData(24).isAPartOf(wsaaCntStati, 0, REDEFINE);
	private FixedLengthStringData wsaaCnRiskStat01 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 0);
	private FixedLengthStringData wsaaCnRiskStat02 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 2);
	private FixedLengthStringData wsaaCnRiskStat03 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 4);
	private FixedLengthStringData wsaaCnRiskStat04 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 6);
	private FixedLengthStringData wsaaCnRiskStat05 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 8);
	private FixedLengthStringData wsaaCnRiskStat06 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 10);
	private FixedLengthStringData wsaaCnRiskStat07 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 12);
	private FixedLengthStringData wsaaCnRiskStat08 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 14);
	private FixedLengthStringData wsaaCnRiskStat09 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 16);
	private FixedLengthStringData wsaaCnRiskStat10 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 18);
	private FixedLengthStringData wsaaCnRiskStat11 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 20);
	private FixedLengthStringData wsaaCnRiskStat12 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 22);
}
/*
 * Class transformed  from Data Structure WSAA-TRANCODE-ARRAY--INNER
 */
private static final class WsaaTrancodeArrayInner { 

	private FixedLengthStringData wsaaTrancodeArray = new FixedLengthStringData(40);
	private FixedLengthStringData wsaaTrancodes = new FixedLengthStringData(40).isAPartOf(wsaaTrancodeArray, 0);
	private FixedLengthStringData[] wsaaTcdeValue = FLSArrayPartOfStructure(10, 4, wsaaTrancodes, 0);
	private FixedLengthStringData wsaaSqlTrancodes = new FixedLengthStringData(40).isAPartOf(wsaaTrancodes, 0, REDEFINE);
	private FixedLengthStringData wsaaTrcde01 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 0);
	private FixedLengthStringData wsaaTrcde02 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 4);
	private FixedLengthStringData wsaaTrcde03 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 8);
	private FixedLengthStringData wsaaTrcde04 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 12);
	private FixedLengthStringData wsaaTrcde05 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 16);
	private FixedLengthStringData wsaaTrcde06 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 20);
	private FixedLengthStringData wsaaTrcde07 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 24);
	private FixedLengthStringData wsaaTrcde08 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 28);
	private FixedLengthStringData wsaaTrcde09 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 32);
	private FixedLengthStringData wsaaTrcde10 = new FixedLengthStringData(4).isAPartOf(wsaaSqlTrancodes, 36);
}
/*
 * Class transformed  from Data Structure WSAA-RH560--INNER
 */
private static final class WsaaRh560Inner { 

		/* WSAA-RH560 */
	private FixedLengthStringData wsaaRh560H01 = new FixedLengthStringData(183);
	private FixedLengthStringData rh560h01O = new FixedLengthStringData(183).isAPartOf(wsaaRh560H01, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rh560h01O, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rh560h01O, 1);
	private FixedLengthStringData rh01Rptdate = new FixedLengthStringData(22).isAPartOf(rh560h01O, 31);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rh560h01O, 53);
	private FixedLengthStringData rh01Cntbranch = new FixedLengthStringData(2).isAPartOf(rh560h01O, 63);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rh560h01O, 65);
	private FixedLengthStringData rh01Distcode = new FixedLengthStringData(3).isAPartOf(rh560h01O, 95);
	private FixedLengthStringData rh01Distdesc = new FixedLengthStringData(30).isAPartOf(rh560h01O, 98);
	private FixedLengthStringData rh01Agntnum = new FixedLengthStringData(8).isAPartOf(rh560h01O, 128);
	private FixedLengthStringData rh01Agtname = new FixedLengthStringData(47).isAPartOf(rh560h01O, 136);

	private FixedLengthStringData wsaaRh560D01 = new FixedLengthStringData(121);
	private FixedLengthStringData rh560d01O = new FixedLengthStringData(121).isAPartOf(wsaaRh560D01, 0);
	private FixedLengthStringData rd01Polid = new FixedLengthStringData(12).isAPartOf(rh560d01O, 0);
	private ZonedDecimalData rd01ModalPrem = new ZonedDecimalData(18, 2).isAPartOf(rh560d01O, 12);
	private FixedLengthStringData rd01Descr = new FixedLengthStringData(30).isAPartOf(rh560d01O, 30);
	private FixedLengthStringData rd01Clntname = new FixedLengthStringData(30).isAPartOf(rh560d01O, 60);
	private FixedLengthStringData rd01IdNumber = new FixedLengthStringData(10).isAPartOf(rh560d01O, 90);
	private FixedLengthStringData rd01Cntcurr = new FixedLengthStringData(3).isAPartOf(rh560d01O, 100);
	private ZonedDecimalData rd01Sumins = new ZonedDecimalData(7, 0).isAPartOf(rh560d01O, 103);
	private ZonedDecimalData rd01AnnualPrem = new ZonedDecimalData(11, 2).isAPartOf(rh560d01O, 110);

	private FixedLengthStringData wsaaRh560F01 = new FixedLengthStringData(1);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData agntrec = new FixedLengthStringData(10).init("AGNTREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData covrenqrec = new FixedLengthStringData(10).init("COVRENQREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData fluplnbrec = new FixedLengthStringData(10).init("FLUPLNBREC");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData esql = new FixedLengthStringData(4).init("ESQL");
	private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
	private FixedLengthStringData e402 = new FixedLengthStringData(4).init("E402");
	private FixedLengthStringData f151 = new FixedLengthStringData(4).init("F151");
	private FixedLengthStringData f176 = new FixedLengthStringData(4).init("F176");
	private FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
	private FixedLengthStringData g641 = new FixedLengthStringData(4).init("G641");
	private FixedLengthStringData g874 = new FixedLengthStringData(4).init("G874");
	private FixedLengthStringData g875 = new FixedLengthStringData(4).init("G875");
	private FixedLengthStringData h143 = new FixedLengthStringData(4).init("H143");
	private FixedLengthStringData h924 = new FixedLengthStringData(4).init("H924");
	private FixedLengthStringData hl24 = new FixedLengthStringData(4).init("HL24");
}
}
