package com.csc.life.newbusiness.dataaccess;

import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChdrafiTableDAM.java
 * Date: Sun, 30 Aug 2009 03:31:24
 * Class transformed from CHDRAFI.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChdrafiTableDAM extends ChdrpfTableDAM {

	public ChdrafiTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CHDRAFI");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRPFX, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "VALIDFLAG, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "AVLISU, " +
		            "SERVUNIT, " +
		            "CNTTYPE, " +
		            "TRANNO, " +
		            "TRANID, " +
		            "OCCDATE, " +
		            "COWNCOY, " +
		            "COWNNUM, " +
		            "JOWNNUM, " +
		            "PAYRCOY, " +
		            "PAYRNUM, " +
		            "DESPCOY, " +
		            "DESPNUM, " +
		            "ASGNCOY, " +
		            "ASGNNUM, " +
		            "CNTBRANCH, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "CNTCURR, " +
		            "BILLFREQ, " +
		            "BILLCHNL, " +
		            "BTDATE, " +
		            "PTDATE, " +
		            "REG, " +
		            "POLINC, " +
		            "NXTSFX, " +
		            "SINSTFROM, " +
		            "SINSTTO, " +
		            "SINSTAMT01, " +
		            "SINSTAMT02, " +
		            "SINSTAMT03, " +
		            "SINSTAMT04, " +
		            "SINSTAMT05, " +
		            "SINSTAMT06, " +
		            "INSTFROM, " +
		            "INSTTO, " +
		            "INSTTOT01, " +
		            "INSTTOT02, " +
		            "INSTTOT03, " +
		            "INSTTOT04, " +
		            "INSTTOT05, " +
		            "INSTTOT06, " +
		            "INSTPAST01, " +
		            "INSTPAST02, " +
		            "INSTPAST03, " +
		            "INSTPAST04, " +
		            "INSTPAST05, " +
		            "INSTPAST06, " +
		            "NOFOUTINST, " +
		            "OUTSTAMT, " +
		            "BILLDATE01, " +
		            "BILLDATE02, " +
		            "BILLDATE03, " +
		            "BILLDATE04, " +
		            "BILLAMT01, " +
		            "BILLAMT02, " +
		            "BILLAMT03, " +
		            "BILLAMT04, " +
		            "ISAM01, " +
		            "ISAM02, " +
		            "ISAM03, " +
		            "ISAM04, " +
		            "ISAM05, " +
		            "ISAM06, " +
		            "LASTSWDATE, " +
		            "TFRSWUSED, " +
		            "TFRSWLEFT, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "POLSUM, " +
		            "MANDREF, " +
		            "STATCODE, " +
		            "STATDATE, " +
		            "STATTRAN, " +
		            "PSTCDE, " +
		            "PSTDAT, " +
		            "PSTTRN, " +
		            "CCDATE, " +
		            "STMDTE, " +
		            "BILLCD, " +
		            "SRCEBUS, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "ZMANDREF,	" + // ILIFE-2472
					"REQNTYPE," + // ILIFE-2472
		            "PAYCLT," + //ILIFE-2472 PH2
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrpfx,
                               chdrcoy,
                               chdrnum,
                               validflag,
                               currfrom,
                               currto,
                               avlisu,
                               servunit,
                               cnttype,
                               tranno,
                               tranid,
                               occdate,
                               cowncoy,
                               cownnum,
                               jownnum,
                               payrcoy,
                               payrnum,
                               despcoy,
                               despnum,
                               asgncoy,
                               asgnnum,
                               cntbranch,
                               agntcoy,
                               agntnum,
                               cntcurr,
                               billfreq,
                               billchnl,
                               btdate,
                               ptdate,
                               register,
                               polinc,
                               nxtsfx,
                               sinstfrom,
                               sinstto,
                               sinstamt01,
                               sinstamt02,
                               sinstamt03,
                               sinstamt04,
                               sinstamt05,
                               sinstamt06,
                               instfrom,
                               instto,
                               insttot01,
                               insttot02,
                               insttot03,
                               insttot04,
                               insttot05,
                               insttot06,
                               instpast01,
                               instpast02,
                               instpast03,
                               instpast04,
                               instpast05,
                               instpast06,
                               nofoutinst,
                               outstamt,
                               billdate01,
                               billdate02,
                               billdate03,
                               billdate04,
                               billamt01,
                               billamt02,
                               billamt03,
                               billamt04,
                               inststamt01,
                               inststamt02,
                               inststamt03,
                               inststamt04,
                               inststamt05,
                               inststamt06,
                               lastSwitchDate,
                               freeSwitchesUsed,
                               freeSwitchesLeft,
                               facthous,
                               bankkey,
                               bankacckey,
                               polsum,
                               mandref,
                               statcode,
                               statdate,
                               stattran,
                               pstatcode,
                               pstatdate,
                               pstattran,
                               ccdate,
                               statementDate,
                               billcd,
                               srcebus,
                               userProfile,
                               jobName,
                               datime,
                               zmandref, //ILIFE-2472
                               reqntype, //ILIFE-2472
                               payclt, //ILIFE-2472 PH2
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(52);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller20.setInternal(chdrcoy.toInternal());
	nonKeyFiller30.setInternal(chdrnum.toInternal());
	nonKeyFiller100.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		//FixedLengthStringData nonKeyData = new FixedLengthStringData(568);
		//FixedLengthStringData nonKeyData = new FixedLengthStringData(574);//ILIFE-2472
		FixedLengthStringData nonKeyData = new FixedLengthStringData(582);//ILIFE-2472 PH2
		nonKeyData.set(
					getChdrpfx().toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getValidflag().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getAvlisu().toInternal()
					+ getServunit().toInternal()
					+ getCnttype().toInternal()
					+ nonKeyFiller100.toInternal()
					+ getTranid().toInternal()
					+ getOccdate().toInternal()
					+ getCowncoy().toInternal()
					+ getCownnum().toInternal()
					+ getJownnum().toInternal()
					+ getPayrcoy().toInternal()
					+ getPayrnum().toInternal()
					+ getDespcoy().toInternal()
					+ getDespnum().toInternal()
					+ getAsgncoy().toInternal()
					+ getAsgnnum().toInternal()
					+ getCntbranch().toInternal()
					+ getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getCntcurr().toInternal()
					+ getBillfreq().toInternal()
					+ getBillchnl().toInternal()
					+ getBtdate().toInternal()
					+ getPtdate().toInternal()
					+ getRegister().toInternal()
					+ getPolinc().toInternal()
					+ getNxtsfx().toInternal()
					+ getSinstfrom().toInternal()
					+ getSinstto().toInternal()
					+ getSinstamt01().toInternal()
					+ getSinstamt02().toInternal()
					+ getSinstamt03().toInternal()
					+ getSinstamt04().toInternal()
					+ getSinstamt05().toInternal()
					+ getSinstamt06().toInternal()
					+ getInstfrom().toInternal()
					+ getInstto().toInternal()
					+ getInsttot01().toInternal()
					+ getInsttot02().toInternal()
					+ getInsttot03().toInternal()
					+ getInsttot04().toInternal()
					+ getInsttot05().toInternal()
					+ getInsttot06().toInternal()
					+ getInstpast01().toInternal()
					+ getInstpast02().toInternal()
					+ getInstpast03().toInternal()
					+ getInstpast04().toInternal()
					+ getInstpast05().toInternal()
					+ getInstpast06().toInternal()
					+ getNofoutinst().toInternal()
					+ getOutstamt().toInternal()
					+ getBilldate01().toInternal()
					+ getBilldate02().toInternal()
					+ getBilldate03().toInternal()
					+ getBilldate04().toInternal()
					+ getBillamt01().toInternal()
					+ getBillamt02().toInternal()
					+ getBillamt03().toInternal()
					+ getBillamt04().toInternal()
					+ getInststamt01().toInternal()
					+ getInststamt02().toInternal()
					+ getInststamt03().toInternal()
					+ getInststamt04().toInternal()
					+ getInststamt05().toInternal()
					+ getInststamt06().toInternal()
					+ getLastSwitchDate().toInternal()
					+ getFreeSwitchesUsed().toInternal()
					+ getFreeSwitchesLeft().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getPolsum().toInternal()
					+ getMandref().toInternal()
					+ getStatcode().toInternal()
					+ getStatdate().toInternal()
					+ getStattran().toInternal()
					+ getPstatcode().toInternal()
					+ getPstatdate().toInternal()
					+ getPstattran().toInternal()
					+ getCcdate().toInternal()
					+ getStatementDate().toInternal()
					+ getBillcd().toInternal()
					+ getSrcebus().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getZmandref().toInternal()  //ILIFE-2472
					+ getReqntype().toInternal()//ILIFE-2472
					+ getPayclt().toInternal()); //ILIFE-2472 PH2
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrpfx);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, avlisu);
			what = ExternalData.chop(what, servunit);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, cowncoy);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, jownnum);
			what = ExternalData.chop(what, payrcoy);
			what = ExternalData.chop(what, payrnum);
			what = ExternalData.chop(what, despcoy);
			what = ExternalData.chop(what, despnum);
			what = ExternalData.chop(what, asgncoy);
			what = ExternalData.chop(what, asgnnum);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, polinc);
			what = ExternalData.chop(what, nxtsfx);
			what = ExternalData.chop(what, sinstfrom);
			what = ExternalData.chop(what, sinstto);
			what = ExternalData.chop(what, sinstamt01);
			what = ExternalData.chop(what, sinstamt02);
			what = ExternalData.chop(what, sinstamt03);
			what = ExternalData.chop(what, sinstamt04);
			what = ExternalData.chop(what, sinstamt05);
			what = ExternalData.chop(what, sinstamt06);
			what = ExternalData.chop(what, instfrom);
			what = ExternalData.chop(what, instto);
			what = ExternalData.chop(what, insttot01);
			what = ExternalData.chop(what, insttot02);
			what = ExternalData.chop(what, insttot03);
			what = ExternalData.chop(what, insttot04);
			what = ExternalData.chop(what, insttot05);
			what = ExternalData.chop(what, insttot06);
			what = ExternalData.chop(what, instpast01);
			what = ExternalData.chop(what, instpast02);
			what = ExternalData.chop(what, instpast03);
			what = ExternalData.chop(what, instpast04);
			what = ExternalData.chop(what, instpast05);
			what = ExternalData.chop(what, instpast06);
			what = ExternalData.chop(what, nofoutinst);
			what = ExternalData.chop(what, outstamt);
			what = ExternalData.chop(what, billdate01);
			what = ExternalData.chop(what, billdate02);
			what = ExternalData.chop(what, billdate03);
			what = ExternalData.chop(what, billdate04);
			what = ExternalData.chop(what, billamt01);
			what = ExternalData.chop(what, billamt02);
			what = ExternalData.chop(what, billamt03);
			what = ExternalData.chop(what, billamt04);
			what = ExternalData.chop(what, inststamt01);
			what = ExternalData.chop(what, inststamt02);
			what = ExternalData.chop(what, inststamt03);
			what = ExternalData.chop(what, inststamt04);
			what = ExternalData.chop(what, inststamt05);
			what = ExternalData.chop(what, inststamt06);
			what = ExternalData.chop(what, lastSwitchDate);
			what = ExternalData.chop(what, freeSwitchesUsed);
			what = ExternalData.chop(what, freeSwitchesLeft);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, polsum);
			what = ExternalData.chop(what, mandref);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, statdate);
			what = ExternalData.chop(what, stattran);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, pstatdate);
			what = ExternalData.chop(what, pstattran);
			what = ExternalData.chop(what, ccdate);
			what = ExternalData.chop(what, statementDate);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);			
			what = ExternalData.chop(what, datime);		
			what = ExternalData.chop(what, zmandref); //ILIFE-2472
			what = ExternalData.chop(what, reqntype); //ILIFE-2472	
			what = ExternalData.chop(what, payclt); //ILIFE-2472 PH2
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(Object what) {
		chdrpfx.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getAvlisu() {
		return avlisu;
	}
	public void setAvlisu(Object what) {
		avlisu.set(what);
	}	
	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public FixedLengthStringData getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(Object what) {
		cowncoy.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getJownnum() {
		return jownnum;
	}
	public void setJownnum(Object what) {
		jownnum.set(what);
	}	
	public FixedLengthStringData getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(Object what) {
		payrcoy.set(what);
	}	
	public FixedLengthStringData getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(Object what) {
		payrnum.set(what);
	}	
	public FixedLengthStringData getDespcoy() {
		return despcoy;
	}
	public void setDespcoy(Object what) {
		despcoy.set(what);
	}	
	public FixedLengthStringData getDespnum() {
		return despnum;
	}
	public void setDespnum(Object what) {
		despnum.set(what);
	}	
	public FixedLengthStringData getAsgncoy() {
		return asgncoy;
	}
	public void setAsgncoy(Object what) {
		asgncoy.set(what);
	}	
	public FixedLengthStringData getAsgnnum() {
		return asgnnum;
	}
	public void setAsgnnum(Object what) {
		asgnnum.set(what);
	}	
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}	
	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}	
	public PackedDecimalData getPolinc() {
		return polinc;
	}
	public void setPolinc(Object what) {
		setPolinc(what, false);
	}
	public void setPolinc(Object what, boolean rounded) {
		if (rounded)
			polinc.setRounded(what);
		else
			polinc.set(what);
	}	
	public PackedDecimalData getNxtsfx() {
		return nxtsfx;
	}
	public void setNxtsfx(Object what) {
		setNxtsfx(what, false);
	}
	public void setNxtsfx(Object what, boolean rounded) {
		if (rounded)
			nxtsfx.setRounded(what);
		else
			nxtsfx.set(what);
	}	
	public PackedDecimalData getSinstfrom() {
		return sinstfrom;
	}
	public void setSinstfrom(Object what) {
		setSinstfrom(what, false);
	}
	public void setSinstfrom(Object what, boolean rounded) {
		if (rounded)
			sinstfrom.setRounded(what);
		else
			sinstfrom.set(what);
	}	
	public PackedDecimalData getSinstto() {
		return sinstto;
	}
	public void setSinstto(Object what) {
		setSinstto(what, false);
	}
	public void setSinstto(Object what, boolean rounded) {
		if (rounded)
			sinstto.setRounded(what);
		else
			sinstto.set(what);
	}	
	public PackedDecimalData getSinstamt01() {
		return sinstamt01;
	}
	public void setSinstamt01(Object what) {
		setSinstamt01(what, false);
	}
	public void setSinstamt01(Object what, boolean rounded) {
		if (rounded)
			sinstamt01.setRounded(what);
		else
			sinstamt01.set(what);
	}	
	public PackedDecimalData getSinstamt02() {
		return sinstamt02;
	}
	public void setSinstamt02(Object what) {
		setSinstamt02(what, false);
	}
	public void setSinstamt02(Object what, boolean rounded) {
		if (rounded)
			sinstamt02.setRounded(what);
		else
			sinstamt02.set(what);
	}	
	public PackedDecimalData getSinstamt03() {
		return sinstamt03;
	}
	public void setSinstamt03(Object what) {
		setSinstamt03(what, false);
	}
	public void setSinstamt03(Object what, boolean rounded) {
		if (rounded)
			sinstamt03.setRounded(what);
		else
			sinstamt03.set(what);
	}	
	public PackedDecimalData getSinstamt04() {
		return sinstamt04;
	}
	public void setSinstamt04(Object what) {
		setSinstamt04(what, false);
	}
	public void setSinstamt04(Object what, boolean rounded) {
		if (rounded)
			sinstamt04.setRounded(what);
		else
			sinstamt04.set(what);
	}	
	public PackedDecimalData getSinstamt05() {
		return sinstamt05;
	}
	public void setSinstamt05(Object what) {
		setSinstamt05(what, false);
	}
	public void setSinstamt05(Object what, boolean rounded) {
		if (rounded)
			sinstamt05.setRounded(what);
		else
			sinstamt05.set(what);
	}	
	public PackedDecimalData getSinstamt06() {
		return sinstamt06;
	}
	public void setSinstamt06(Object what) {
		setSinstamt06(what, false);
	}
	public void setSinstamt06(Object what, boolean rounded) {
		if (rounded)
			sinstamt06.setRounded(what);
		else
			sinstamt06.set(what);
	}	
	public PackedDecimalData getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(Object what) {
		setInstfrom(what, false);
	}
	public void setInstfrom(Object what, boolean rounded) {
		if (rounded)
			instfrom.setRounded(what);
		else
			instfrom.set(what);
	}	
	public PackedDecimalData getInstto() {
		return instto;
	}
	public void setInstto(Object what) {
		setInstto(what, false);
	}
	public void setInstto(Object what, boolean rounded) {
		if (rounded)
			instto.setRounded(what);
		else
			instto.set(what);
	}	
	public PackedDecimalData getInsttot01() {
		return insttot01;
	}
	public void setInsttot01(Object what) {
		setInsttot01(what, false);
	}
	public void setInsttot01(Object what, boolean rounded) {
		if (rounded)
			insttot01.setRounded(what);
		else
			insttot01.set(what);
	}	
	public PackedDecimalData getInsttot02() {
		return insttot02;
	}
	public void setInsttot02(Object what) {
		setInsttot02(what, false);
	}
	public void setInsttot02(Object what, boolean rounded) {
		if (rounded)
			insttot02.setRounded(what);
		else
			insttot02.set(what);
	}	
	public PackedDecimalData getInsttot03() {
		return insttot03;
	}
	public void setInsttot03(Object what) {
		setInsttot03(what, false);
	}
	public void setInsttot03(Object what, boolean rounded) {
		if (rounded)
			insttot03.setRounded(what);
		else
			insttot03.set(what);
	}	
	public PackedDecimalData getInsttot04() {
		return insttot04;
	}
	public void setInsttot04(Object what) {
		setInsttot04(what, false);
	}
	public void setInsttot04(Object what, boolean rounded) {
		if (rounded)
			insttot04.setRounded(what);
		else
			insttot04.set(what);
	}	
	public PackedDecimalData getInsttot05() {
		return insttot05;
	}
	public void setInsttot05(Object what) {
		setInsttot05(what, false);
	}
	public void setInsttot05(Object what, boolean rounded) {
		if (rounded)
			insttot05.setRounded(what);
		else
			insttot05.set(what);
	}	
	public PackedDecimalData getInsttot06() {
		return insttot06;
	}
	public void setInsttot06(Object what) {
		setInsttot06(what, false);
	}
	public void setInsttot06(Object what, boolean rounded) {
		if (rounded)
			insttot06.setRounded(what);
		else
			insttot06.set(what);
	}	
	public PackedDecimalData getInstpast01() {
		return instpast01;
	}
	public void setInstpast01(Object what) {
		setInstpast01(what, false);
	}
	public void setInstpast01(Object what, boolean rounded) {
		if (rounded)
			instpast01.setRounded(what);
		else
			instpast01.set(what);
	}	
	public PackedDecimalData getInstpast02() {
		return instpast02;
	}
	public void setInstpast02(Object what) {
		setInstpast02(what, false);
	}
	public void setInstpast02(Object what, boolean rounded) {
		if (rounded)
			instpast02.setRounded(what);
		else
			instpast02.set(what);
	}	
	public PackedDecimalData getInstpast03() {
		return instpast03;
	}
	public void setInstpast03(Object what) {
		setInstpast03(what, false);
	}
	public void setInstpast03(Object what, boolean rounded) {
		if (rounded)
			instpast03.setRounded(what);
		else
			instpast03.set(what);
	}	
	public PackedDecimalData getInstpast04() {
		return instpast04;
	}
	public void setInstpast04(Object what) {
		setInstpast04(what, false);
	}
	public void setInstpast04(Object what, boolean rounded) {
		if (rounded)
			instpast04.setRounded(what);
		else
			instpast04.set(what);
	}	
	public PackedDecimalData getInstpast05() {
		return instpast05;
	}
	public void setInstpast05(Object what) {
		setInstpast05(what, false);
	}
	public void setInstpast05(Object what, boolean rounded) {
		if (rounded)
			instpast05.setRounded(what);
		else
			instpast05.set(what);
	}	
	public PackedDecimalData getInstpast06() {
		return instpast06;
	}
	public void setInstpast06(Object what) {
		setInstpast06(what, false);
	}
	public void setInstpast06(Object what, boolean rounded) {
		if (rounded)
			instpast06.setRounded(what);
		else
			instpast06.set(what);
	}	
	public PackedDecimalData getNofoutinst() {
		return nofoutinst;
	}
	public void setNofoutinst(Object what) {
		setNofoutinst(what, false);
	}
	public void setNofoutinst(Object what, boolean rounded) {
		if (rounded)
			nofoutinst.setRounded(what);
		else
			nofoutinst.set(what);
	}	
	public PackedDecimalData getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(Object what) {
		setOutstamt(what, false);
	}
	public void setOutstamt(Object what, boolean rounded) {
		if (rounded)
			outstamt.setRounded(what);
		else
			outstamt.set(what);
	}	
	public PackedDecimalData getBilldate01() {
		return billdate01;
	}
	public void setBilldate01(Object what) {
		setBilldate01(what, false);
	}
	public void setBilldate01(Object what, boolean rounded) {
		if (rounded)
			billdate01.setRounded(what);
		else
			billdate01.set(what);
	}	
	public PackedDecimalData getBilldate02() {
		return billdate02;
	}
	public void setBilldate02(Object what) {
		setBilldate02(what, false);
	}
	public void setBilldate02(Object what, boolean rounded) {
		if (rounded)
			billdate02.setRounded(what);
		else
			billdate02.set(what);
	}	
	public PackedDecimalData getBilldate03() {
		return billdate03;
	}
	public void setBilldate03(Object what) {
		setBilldate03(what, false);
	}
	public void setBilldate03(Object what, boolean rounded) {
		if (rounded)
			billdate03.setRounded(what);
		else
			billdate03.set(what);
	}	
	public PackedDecimalData getBilldate04() {
		return billdate04;
	}
	public void setBilldate04(Object what) {
		setBilldate04(what, false);
	}
	public void setBilldate04(Object what, boolean rounded) {
		if (rounded)
			billdate04.setRounded(what);
		else
			billdate04.set(what);
	}	
	public PackedDecimalData getBillamt01() {
		return billamt01;
	}
	public void setBillamt01(Object what) {
		setBillamt01(what, false);
	}
	public void setBillamt01(Object what, boolean rounded) {
		if (rounded)
			billamt01.setRounded(what);
		else
			billamt01.set(what);
	}	
	public PackedDecimalData getBillamt02() {
		return billamt02;
	}
	public void setBillamt02(Object what) {
		setBillamt02(what, false);
	}
	public void setBillamt02(Object what, boolean rounded) {
		if (rounded)
			billamt02.setRounded(what);
		else
			billamt02.set(what);
	}	
	public PackedDecimalData getBillamt03() {
		return billamt03;
	}
	public void setBillamt03(Object what) {
		setBillamt03(what, false);
	}
	public void setBillamt03(Object what, boolean rounded) {
		if (rounded)
			billamt03.setRounded(what);
		else
			billamt03.set(what);
	}	
	public PackedDecimalData getBillamt04() {
		return billamt04;
	}
	public void setBillamt04(Object what) {
		setBillamt04(what, false);
	}
	public void setBillamt04(Object what, boolean rounded) {
		if (rounded)
			billamt04.setRounded(what);
		else
			billamt04.set(what);
	}	
	public PackedDecimalData getInststamt01() {
		return inststamt01;
	}
	public void setInststamt01(Object what) {
		setInststamt01(what, false);
	}
	public void setInststamt01(Object what, boolean rounded) {
		if (rounded)
			inststamt01.setRounded(what);
		else
			inststamt01.set(what);
	}	
	public PackedDecimalData getInststamt02() {
		return inststamt02;
	}
	public void setInststamt02(Object what) {
		setInststamt02(what, false);
	}
	public void setInststamt02(Object what, boolean rounded) {
		if (rounded)
			inststamt02.setRounded(what);
		else
			inststamt02.set(what);
	}	
	public PackedDecimalData getInststamt03() {
		return inststamt03;
	}
	public void setInststamt03(Object what) {
		setInststamt03(what, false);
	}
	public void setInststamt03(Object what, boolean rounded) {
		if (rounded)
			inststamt03.setRounded(what);
		else
			inststamt03.set(what);
	}	
	public PackedDecimalData getInststamt04() {
		return inststamt04;
	}
	public void setInststamt04(Object what) {
		setInststamt04(what, false);
	}
	public void setInststamt04(Object what, boolean rounded) {
		if (rounded)
			inststamt04.setRounded(what);
		else
			inststamt04.set(what);
	}	
	public PackedDecimalData getInststamt05() {
		return inststamt05;
	}
	public void setInststamt05(Object what) {
		setInststamt05(what, false);
	}
	public void setInststamt05(Object what, boolean rounded) {
		if (rounded)
			inststamt05.setRounded(what);
		else
			inststamt05.set(what);
	}	
	public PackedDecimalData getInststamt06() {
		return inststamt06;
	}
	public void setInststamt06(Object what) {
		setInststamt06(what, false);
	}
	public void setInststamt06(Object what, boolean rounded) {
		if (rounded)
			inststamt06.setRounded(what);
		else
			inststamt06.set(what);
	}	
	public PackedDecimalData getLastSwitchDate() {
		return lastSwitchDate;
	}
	public void setLastSwitchDate(Object what) {
		setLastSwitchDate(what, false);
	}
	public void setLastSwitchDate(Object what, boolean rounded) {
		if (rounded)
			lastSwitchDate.setRounded(what);
		else
			lastSwitchDate.set(what);
	}	
	public PackedDecimalData getFreeSwitchesUsed() {
		return freeSwitchesUsed;
	}
	public void setFreeSwitchesUsed(Object what) {
		setFreeSwitchesUsed(what, false);
	}
	public void setFreeSwitchesUsed(Object what, boolean rounded) {
		if (rounded)
			freeSwitchesUsed.setRounded(what);
		else
			freeSwitchesUsed.set(what);
	}	
	public PackedDecimalData getFreeSwitchesLeft() {
		return freeSwitchesLeft;
	}
	public void setFreeSwitchesLeft(Object what) {
		setFreeSwitchesLeft(what, false);
	}
	public void setFreeSwitchesLeft(Object what, boolean rounded) {
		if (rounded)
			freeSwitchesLeft.setRounded(what);
		else
			freeSwitchesLeft.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public PackedDecimalData getPolsum() {
		return polsum;
	}
	public void setPolsum(Object what) {
		setPolsum(what, false);
	}
	public void setPolsum(Object what, boolean rounded) {
		if (rounded)
			polsum.setRounded(what);
		else
			polsum.set(what);
	}	
	public FixedLengthStringData getMandref() {
		return mandref;
	}
	public void setMandref(Object what) {
		mandref.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public PackedDecimalData getStatdate() {
		return statdate;
	}
	public void setStatdate(Object what) {
		setStatdate(what, false);
	}
	public void setStatdate(Object what, boolean rounded) {
		if (rounded)
			statdate.setRounded(what);
		else
			statdate.set(what);
	}	
	public PackedDecimalData getStattran() {
		return stattran;
	}
	public void setStattran(Object what) {
		setStattran(what, false);
	}
	public void setStattran(Object what, boolean rounded) {
		if (rounded)
			stattran.setRounded(what);
		else
			stattran.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public PackedDecimalData getPstatdate() {
		return pstatdate;
	}
	public void setPstatdate(Object what) {
		setPstatdate(what, false);
	}
	public void setPstatdate(Object what, boolean rounded) {
		if (rounded)
			pstatdate.setRounded(what);
		else
			pstatdate.set(what);
	}	
	public PackedDecimalData getPstattran() {
		return pstattran;
	}
	public void setPstattran(Object what) {
		setPstattran(what, false);
	}
	public void setPstattran(Object what, boolean rounded) {
		if (rounded)
			pstattran.setRounded(what);
		else
			pstattran.set(what);
	}	
	public PackedDecimalData getCcdate() {
		return ccdate;
	}
	public void setCcdate(Object what) {
		setCcdate(what, false);
	}
	public void setCcdate(Object what, boolean rounded) {
		if (rounded)
			ccdate.setRounded(what);
		else
			ccdate.set(what);
	}	
	public PackedDecimalData getStatementDate() {
		return statementDate;
	}
	public void setStatementDate(Object what) {
		setStatementDate(what, false);
	}
	public void setStatementDate(Object what, boolean rounded) {
		if (rounded)
			statementDate.setRounded(what);
		else
			statementDate.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//ILIFE-2472
	public FixedLengthStringData getZmandref() {
		return zmandref;
	}
	//ILIFE-2472
	public void setZmandref(Object what) {
		zmandref.set(what);
	}
	//ILIFE-2472	
	public FixedLengthStringData getReqntype() {
		return reqntype;
	}
	//ILIFE-2472	
	public void setReqntype(Object what) {
		reqntype.set(what);
	}
	
	//ILIFE-2472 PH2
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	//ILIFE-2472 PH2	
	public void setPayclt(Object what) {
		payclt.set(what);
	}
	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSinstamts() {
		return new FixedLengthStringData(sinstamt01.toInternal()
										+ sinstamt02.toInternal()
										+ sinstamt03.toInternal()
										+ sinstamt04.toInternal()
										+ sinstamt05.toInternal()
										+ sinstamt06.toInternal());
	}
	public void setSinstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSinstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, sinstamt01);
		what = ExternalData.chop(what, sinstamt02);
		what = ExternalData.chop(what, sinstamt03);
		what = ExternalData.chop(what, sinstamt04);
		what = ExternalData.chop(what, sinstamt05);
		what = ExternalData.chop(what, sinstamt06);
	}
	public PackedDecimalData getSinstamt(BaseData indx) {
		return getSinstamt(indx.toInt());
	}
	public PackedDecimalData getSinstamt(int indx) {

		switch (indx) {
			case 1 : return sinstamt01;
			case 2 : return sinstamt02;
			case 3 : return sinstamt03;
			case 4 : return sinstamt04;
			case 5 : return sinstamt05;
			case 6 : return sinstamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSinstamt(BaseData indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(BaseData indx, Object what, boolean rounded) {
		setSinstamt(indx.toInt(), what, rounded);
	}
	public void setSinstamt(int indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSinstamt01(what, rounded);
					 break;
			case 2 : setSinstamt02(what, rounded);
					 break;
			case 3 : setSinstamt03(what, rounded);
					 break;
			case 4 : setSinstamt04(what, rounded);
					 break;
			case 5 : setSinstamt05(what, rounded);
					 break;
			case 6 : setSinstamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getIsams() {
		return new FixedLengthStringData(inststamt01.toInternal()
										+ inststamt02.toInternal()
										+ inststamt03.toInternal()
										+ inststamt04.toInternal()
										+ inststamt05.toInternal()
										+ inststamt06.toInternal());
	}
	public void setIsams(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getIsams().getLength()).init(obj);
	
		what = ExternalData.chop(what, inststamt01);
		what = ExternalData.chop(what, inststamt02);
		what = ExternalData.chop(what, inststamt03);
		what = ExternalData.chop(what, inststamt04);
		what = ExternalData.chop(what, inststamt05);
		what = ExternalData.chop(what, inststamt06);
	}
	public PackedDecimalData getIsam(BaseData indx) {
		return getIsam(indx.toInt());
	}
	public PackedDecimalData getIsam(int indx) {

		switch (indx) {
			case 1 : return inststamt01;
			case 2 : return inststamt02;
			case 3 : return inststamt03;
			case 4 : return inststamt04;
			case 5 : return inststamt05;
			case 6 : return inststamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setIsam(BaseData indx, Object what) {
		setIsam(indx, what, false);
	}
	public void setIsam(BaseData indx, Object what, boolean rounded) {
		setIsam(indx.toInt(), what, rounded);
	}
	public void setIsam(int indx, Object what) {
		setIsam(indx, what, false);
	}
	public void setIsam(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInststamt01(what, rounded);
					 break;
			case 2 : setInststamt02(what, rounded);
					 break;
			case 3 : setInststamt03(what, rounded);
					 break;
			case 4 : setInststamt04(what, rounded);
					 break;
			case 5 : setInststamt05(what, rounded);
					 break;
			case 6 : setInststamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInsttots() {
		return new FixedLengthStringData(insttot01.toInternal()
										+ insttot02.toInternal()
										+ insttot03.toInternal()
										+ insttot04.toInternal()
										+ insttot05.toInternal()
										+ insttot06.toInternal());
	}
	public void setInsttots(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInsttots().getLength()).init(obj);
	
		what = ExternalData.chop(what, insttot01);
		what = ExternalData.chop(what, insttot02);
		what = ExternalData.chop(what, insttot03);
		what = ExternalData.chop(what, insttot04);
		what = ExternalData.chop(what, insttot05);
		what = ExternalData.chop(what, insttot06);
	}
	public PackedDecimalData getInsttot(BaseData indx) {
		return getInsttot(indx.toInt());
	}
	public PackedDecimalData getInsttot(int indx) {

		switch (indx) {
			case 1 : return insttot01;
			case 2 : return insttot02;
			case 3 : return insttot03;
			case 4 : return insttot04;
			case 5 : return insttot05;
			case 6 : return insttot06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInsttot(BaseData indx, Object what) {
		setInsttot(indx, what, false);
	}
	public void setInsttot(BaseData indx, Object what, boolean rounded) {
		setInsttot(indx.toInt(), what, rounded);
	}
	public void setInsttot(int indx, Object what) {
		setInsttot(indx, what, false);
	}
	public void setInsttot(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInsttot01(what, rounded);
					 break;
			case 2 : setInsttot02(what, rounded);
					 break;
			case 3 : setInsttot03(what, rounded);
					 break;
			case 4 : setInsttot04(what, rounded);
					 break;
			case 5 : setInsttot05(what, rounded);
					 break;
			case 6 : setInsttot06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInstpasts() {
		return new FixedLengthStringData(instpast01.toInternal()
										+ instpast02.toInternal()
										+ instpast03.toInternal()
										+ instpast04.toInternal()
										+ instpast05.toInternal()
										+ instpast06.toInternal());
	}
	public void setInstpasts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInstpasts().getLength()).init(obj);
	
		what = ExternalData.chop(what, instpast01);
		what = ExternalData.chop(what, instpast02);
		what = ExternalData.chop(what, instpast03);
		what = ExternalData.chop(what, instpast04);
		what = ExternalData.chop(what, instpast05);
		what = ExternalData.chop(what, instpast06);
	}
	public PackedDecimalData getInstpast(BaseData indx) {
		return getInstpast(indx.toInt());
	}
	public PackedDecimalData getInstpast(int indx) {

		switch (indx) {
			case 1 : return instpast01;
			case 2 : return instpast02;
			case 3 : return instpast03;
			case 4 : return instpast04;
			case 5 : return instpast05;
			case 6 : return instpast06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInstpast(BaseData indx, Object what) {
		setInstpast(indx, what, false);
	}
	public void setInstpast(BaseData indx, Object what, boolean rounded) {
		setInstpast(indx.toInt(), what, rounded);
	}
	public void setInstpast(int indx, Object what) {
		setInstpast(indx, what, false);
	}
	public void setInstpast(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInstpast01(what, rounded);
					 break;
			case 2 : setInstpast02(what, rounded);
					 break;
			case 3 : setInstpast03(what, rounded);
					 break;
			case 4 : setInstpast04(what, rounded);
					 break;
			case 5 : setInstpast05(what, rounded);
					 break;
			case 6 : setInstpast06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBilldates() {
		return new FixedLengthStringData(billdate01.toInternal()
										+ billdate02.toInternal()
										+ billdate03.toInternal()
										+ billdate04.toInternal());
	}
	public void setBilldates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBilldates().getLength()).init(obj);
	
		what = ExternalData.chop(what, billdate01);
		what = ExternalData.chop(what, billdate02);
		what = ExternalData.chop(what, billdate03);
		what = ExternalData.chop(what, billdate04);
	}
	public PackedDecimalData getBilldate(BaseData indx) {
		return getBilldate(indx.toInt());
	}
	public PackedDecimalData getBilldate(int indx) {

		switch (indx) {
			case 1 : return billdate01;
			case 2 : return billdate02;
			case 3 : return billdate03;
			case 4 : return billdate04;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBilldate(BaseData indx, Object what) {
		setBilldate(indx, what, false);
	}
	public void setBilldate(BaseData indx, Object what, boolean rounded) {
		setBilldate(indx.toInt(), what, rounded);
	}
	public void setBilldate(int indx, Object what) {
		setBilldate(indx, what, false);
	}
	public void setBilldate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBilldate01(what, rounded);
					 break;
			case 2 : setBilldate02(what, rounded);
					 break;
			case 3 : setBilldate03(what, rounded);
					 break;
			case 4 : setBilldate04(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBillamts() {
		return new FixedLengthStringData(billamt01.toInternal()
										+ billamt02.toInternal()
										+ billamt03.toInternal()
										+ billamt04.toInternal());
	}
	public void setBillamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBillamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, billamt01);
		what = ExternalData.chop(what, billamt02);
		what = ExternalData.chop(what, billamt03);
		what = ExternalData.chop(what, billamt04);
	}
	public PackedDecimalData getBillamt(BaseData indx) {
		return getBillamt(indx.toInt());
	}
	public PackedDecimalData getBillamt(int indx) {

		switch (indx) {
			case 1 : return billamt01;
			case 2 : return billamt02;
			case 3 : return billamt03;
			case 4 : return billamt04;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBillamt(BaseData indx, Object what) {
		setBillamt(indx, what, false);
	}
	public void setBillamt(BaseData indx, Object what, boolean rounded) {
		setBillamt(indx.toInt(), what, rounded);
	}
	public void setBillamt(int indx, Object what) {
		setBillamt(indx, what, false);
	}
	public void setBillamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBillamt01(what, rounded);
					 break;
			case 2 : setBillamt02(what, rounded);
					 break;
			case 3 : setBillamt03(what, rounded);
					 break;
			case 4 : setBillamt04(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		chdrpfx.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		validflag.clear();
		currfrom.clear();
		currto.clear();
		avlisu.clear();
		servunit.clear();
		cnttype.clear();
		nonKeyFiller100.clear();
		tranid.clear();
		occdate.clear();
		cowncoy.clear();
		cownnum.clear();
		jownnum.clear();
		payrcoy.clear();
		payrnum.clear();
		despcoy.clear();
		despnum.clear();
		asgncoy.clear();
		asgnnum.clear();
		cntbranch.clear();
		agntcoy.clear();
		agntnum.clear();
		cntcurr.clear();
		billfreq.clear();
		billchnl.clear();
		btdate.clear();
		ptdate.clear();
		register.clear();
		polinc.clear();
		nxtsfx.clear();
		sinstfrom.clear();
		sinstto.clear();
		sinstamt01.clear();
		sinstamt02.clear();
		sinstamt03.clear();
		sinstamt04.clear();
		sinstamt05.clear();
		sinstamt06.clear();
		instfrom.clear();
		instto.clear();
		insttot01.clear();
		insttot02.clear();
		insttot03.clear();
		insttot04.clear();
		insttot05.clear();
		insttot06.clear();
		instpast01.clear();
		instpast02.clear();
		instpast03.clear();
		instpast04.clear();
		instpast05.clear();
		instpast06.clear();
		nofoutinst.clear();
		outstamt.clear();
		billdate01.clear();
		billdate02.clear();
		billdate03.clear();
		billdate04.clear();
		billamt01.clear();
		billamt02.clear();
		billamt03.clear();
		billamt04.clear();
		inststamt01.clear();
		inststamt02.clear();
		inststamt03.clear();
		inststamt04.clear();
		inststamt05.clear();
		inststamt06.clear();
		lastSwitchDate.clear();
		freeSwitchesUsed.clear();
		freeSwitchesLeft.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		polsum.clear();
		mandref.clear();
		statcode.clear();
		statdate.clear();
		stattran.clear();
		pstatcode.clear();
		pstatdate.clear();
		pstattran.clear();
		ccdate.clear();
		statementDate.clear();
		billcd.clear();
		srcebus.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		zmandref.clear(); //ILIFE-2472
  		reqntype.clear(); //ILIFE-2472
  		payclt.clear();//ILIFE-2472 PH2
	}


}