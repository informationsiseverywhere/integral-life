package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface PcddpfDAO extends BaseDAO<Pcddpf>{

	public List<Pcddpf> searchPcddRecordByChdrNum(String chdrcoy, String chdrNum) throws SQLRuntimeException;
	public Map<String, List<Pcddpf>> searchPcddRecordByChdrnum(List<String> chdrnumList);
}
