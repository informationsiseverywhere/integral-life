package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZstrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:27:03
 * Class transformed from ZSTRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZstrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 141;
	public FixedLengthStringData zstrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zstrpfRecord = zstrrec;
	
	public FixedLengthStringData chdrpfx = DD.chdrpfx.copy().isAPartOf(zstrrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zstrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zstrrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(zstrrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(zstrrec);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(zstrrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(zstrrec);
	public PackedDecimalData commyr = DD.commyr.copy().isAPartOf(zstrrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(zstrrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(zstrrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(zstrrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(zstrrec);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(zstrrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(zstrrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(zstrrec);
	public PackedDecimalData singp = DD.singp.copy().isAPartOf(zstrrec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(zstrrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(zstrrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(zstrrec);
	public PackedDecimalData cntfee = DD.cntfee.copy().isAPartOf(zstrrec);
	public PackedDecimalData occdate = DD.occdate.copy().isAPartOf(zstrrec);
	public FixedLengthStringData zoutstg = DD.zoutstg.copy().isAPartOf(zstrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(zstrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(zstrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(zstrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZstrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for ZstrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZstrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ZstrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZstrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ZstrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZstrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZSTRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRPFX, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"AGNTNUM, " +
							"ARACDE, " +
							"CNTBRANCH, " +
							"COMMYR, " +
							"CNTTYPE, " +
							"STATCODE, " +
							"PSTATCODE, " +
							"CNTCURR, " +
							"SRCEBUS, " +
							"BATCTRCDE, " +
							"EFFDATE, " +
							"SINGP, " +
							"INSTPREM, " +
							"SUMINS, " +
							"BILLFREQ, " +
							"CNTFEE, " +
							"OCCDATE, " +
							"ZOUTSTG, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrpfx,
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     agntnum,
                                     aracde,
                                     cntbranch,
                                     commyr,
                                     cnttype,
                                     statcode,
                                     pstatcode,
                                     cntcurr,
                                     srcebus,
                                     batctrcde,
                                     effdate,
                                     singp,
                                     instprem,
                                     sumins,
                                     billfreq,
                                     cntfee,
                                     occdate,
                                     zoutstg,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrpfx.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		agntnum.clear();
  		aracde.clear();
  		cntbranch.clear();
  		commyr.clear();
  		cnttype.clear();
  		statcode.clear();
  		pstatcode.clear();
  		cntcurr.clear();
  		srcebus.clear();
  		batctrcde.clear();
  		effdate.clear();
  		singp.clear();
  		instprem.clear();
  		sumins.clear();
  		billfreq.clear();
  		cntfee.clear();
  		occdate.clear();
  		zoutstg.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getZstrrec() {
  		return zstrrec;
	}

	public FixedLengthStringData getZstrpfRecord() {
  		return zstrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setZstrrec(what);
	}

	public void setZstrrec(Object what) {
  		this.zstrrec.set(what);
	}

	public void setZstrpfRecord(Object what) {
  		this.zstrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zstrrec.getLength());
		result.set(zstrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}