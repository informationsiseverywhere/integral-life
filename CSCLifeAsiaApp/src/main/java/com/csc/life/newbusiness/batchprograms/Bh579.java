/*
 * File: Bh579.java
 * Date: 29 August 2009 21:35:51
 * Author: Quipoz Limited
 *
 * Class transformed from BH579.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.tablestructures.T3609rec;
import com.csc.fsu.printing.dataaccess.LetcmntTableDAM;
import com.csc.fsu.printing.recordstructures.Letcokcpy;
import com.csc.life.productdefinition.dataaccess.dao.FluupfDAO;
import com.csc.life.productdefinition.dataaccess.model.Fluupf;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  BH579 -     Generate Follow-Up Standard Letter
*
*  This program reads the FLUPPF file.
*
*  A follow-up letter is printed for each FLUPPF with print
*  status = 'L' or 'M' and a LETC record is written
*  at the same time.
*
*  The reminder issue date is updated once the standard
*  letter is printed.
*
****************************************************************** ***
* </pre>
*/
public class Bh579 extends Mainb {
	private static final Logger LOGGER = LoggerFactory.getLogger(Bh579.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH579");
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaFupstat = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaLettype = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);


		/*  Control indicators*/
	private FixedLengthStringData wsaaFormtype = new FixedLengthStringData(10).init(SPACES);
	
	private static  int size = 1;
	private PackedDecimalData ix = new PackedDecimalData(4, 0).setUnsigned();
	private PackedDecimalData iy = new PackedDecimalData(4, 0).setUnsigned();


	private FixedLengthStringData wsaaDuplicateLetc = new FixedLengthStringData(1);
	private Validator duplicateLetc = new Validator(wsaaDuplicateLetc, "Y");

		/* INDIC-AREA */
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private LetcmntTableDAM letcmntIO = new LetcmntTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private T3609rec t3609rec = new T3609rec();
	private T5661rec t5661rec = new T5661rec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Letcokcpy letcokcpy = new Letcokcpy();
	private FormatsInner formatsInner = new FormatsInner();
	private FixedLengthStringData[] wsaaLetc = FLSInittedArray (size, 257);
	private FixedLengthStringData[] wsaaLetcCompany = FLSDArrayPartOfArrayStructure(1, wsaaLetc, 0);
	private FixedLengthStringData[] wsaaLetcLettype = FLSDArrayPartOfArrayStructure(8, wsaaLetc, 1);
	private FixedLengthStringData[] wsaaLetcClntcoy = FLSDArrayPartOfArrayStructure(1, wsaaLetc, 9);
	private FixedLengthStringData[] wsaaLetcClntnum = FLSDArrayPartOfArrayStructure(8, wsaaLetc, 10);
	private PackedDecimalData[] wsaaLetcRequestDte = PDArrayPartOfArrayStructure(8, 0, wsaaLetc, 18);
	private FixedLengthStringData[] wsaaLetcRdocpfx = FLSDArrayPartOfArrayStructure(2, wsaaLetc, 23);
	private FixedLengthStringData[] wsaaLetcRdoccoy = FLSDArrayPartOfArrayStructure(1, wsaaLetc, 25);
	private FixedLengthStringData[] wsaaLetcRdocnum = FLSDArrayPartOfArrayStructure(9, wsaaLetc, 26);
	private PackedDecimalData[] wsaaLetcTranno = PDArrayPartOfArrayStructure(5, 0, wsaaLetc, 35);
	private FixedLengthStringData[] wsaaLetcChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaLetc, 38);
	private FixedLengthStringData[] wsaaLetcChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaLetc, 39);
	private FixedLengthStringData[] wsaaLetcDespnum = FLSDArrayPartOfArrayStructure(8, wsaaLetc, 47);
	private FixedLengthStringData[] wsaaLetcBranch = FLSDArrayPartOfArrayStructure(2, wsaaLetc, 55);
	private FixedLengthStringData[] wsaaLetcOtherKeys = FLSDArrayPartOfArrayStructure(200, wsaaLetc, 57);

	int i;
	private int intBatchID;
	private int intBatchExtractSize;	
	private int intBatchStep;
	private String strCompany;
	private FluupfDAO  fluDAO =  getApplicationContext().getBean("FluupfDAO", FluupfDAO.class);
	private List<Fluupf> flupList=new ArrayList<>();
	private Fluupf flupf=new Fluupf();
	private Iterator<Fluupf> iteratorList;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private int ct01Value;
	private Map<String, List<Itempf>> t5661Map ;  
	private List<Fluupf> updateflupList=new ArrayList<>();
	private Map<String,List<Chdrpf>> chdrlifIOMap ;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Chdrpf chdrlifIO = new Chdrpf(); 
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private List<String> clntnumList = new ArrayList<>();
	private Map<String, Clntpf> clntpfMap ;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		endOfFile2080,
		exit2090
	}

	public Bh579() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.error("mainline()", e);
		}
	}


protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
	//p6671par.parmRecord.set(bupaIO.getParmarea());
	strCompany = bsprIO.getCompany().toString().trim();
	t5661Map = itemDAO.loadSmartTable("IT", strCompany, "T5661");
	if (t5661Map == null || t5661Map.isEmpty()) {
		return;
	}
	intBatchID = 0;
	intBatchStep = 0;
	flupList = new ArrayList<Fluupf>();
	
	if (bprdIO.systemParam01.isNumeric())
		if (bprdIO.systemParam01.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam01.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
		 
	String Fstatus1="L";
	String Fstatus2="M";
	int extractRows = fluDAO.populateFluppfTemp(strCompany, intBatchExtractSize, Fstatus1, Fstatus2);
			
	if (extractRows > 0) {
		wsaaLetc = FLSInittedArray (flupList.size(), 257);
		performDataChunk();	
		
	 	if (!flupList.isEmpty())
	 	{
	 		
	 		wsaaLetc = FLSInittedArray (flupList.size(), 257);
	 		wsaaLetcCompany = FLSDArrayPartOfArrayStructure(1, wsaaLetc, 0);
	 		wsaaLetcLettype = FLSDArrayPartOfArrayStructure(8, wsaaLetc, 1);
	 		wsaaLetcClntcoy = FLSDArrayPartOfArrayStructure(1, wsaaLetc, 9);
	 		wsaaLetcClntnum = FLSDArrayPartOfArrayStructure(8, wsaaLetc, 10);
	 		wsaaLetcRequestDte = PDArrayPartOfArrayStructure(8, 0, wsaaLetc, 18);
	 		wsaaLetcRdocpfx = FLSDArrayPartOfArrayStructure(2, wsaaLetc, 23);
	 		wsaaLetcRdoccoy = FLSDArrayPartOfArrayStructure(1, wsaaLetc, 25);
	 		wsaaLetcRdocnum = FLSDArrayPartOfArrayStructure(9, wsaaLetc, 26);
	 		wsaaLetcTranno = PDArrayPartOfArrayStructure(5, 0, wsaaLetc, 35);
	 	    wsaaLetcChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaLetc, 38);
	 		wsaaLetcChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaLetc, 39);
	 		wsaaLetcDespnum = FLSDArrayPartOfArrayStructure(8, wsaaLetc, 47);
	 		wsaaLetcBranch = FLSDArrayPartOfArrayStructure(2, wsaaLetc, 55);
	 		 wsaaLetcOtherKeys = FLSDArrayPartOfArrayStructure(200, wsaaLetc, 57);
			 initialise1010();
		 }			
	 }
 	else{
		 return;
	}
	}

protected void performDataChunk(){
	 
	intBatchID = intBatchStep;		
	if (flupList.size() > 0)
	{
		endOfFile2080();
		flupList.clear();
	}
	 
	flupList = fluDAO.loadDataByBatch(intBatchID);
	if (flupList.isEmpty())	
		wsspEdterror.set(varcom.endp);
	else {
		iteratorList = flupList.iterator();
		List<Fluupf> flupLists = flupList;
		List<String> chdrnumList = new ArrayList<>(flupLists.size());
		for(Fluupf flpf : flupLists){
			chdrnumList.add(flpf.getChdrnum());
		}
		chdrlifIOMap = chdrpfDAO.searchChdr(strCompany, chdrnumList);
		String clntComp = null;
		for(Map.Entry<String, List<Chdrpf>> entity : chdrlifIOMap.entrySet()){
			Chdrpf chd = entity.getValue().get(0);
			clntnumList.add(chd.getCownnum());
			clntComp = String.valueOf(chd.getCowncoy());
		}
		clntpfMap = clntpfDAO.searchClntRecord("CN", clntComp, clntnumList);
	}	
	
	ix.set(ZERO);
}

protected void initialise1010()
	{
	
		try
		{
			wsspEdterror.set(varcom.oK);
			/*SET-UP-HEADING-DATES*/
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			datcon2rec.intDatex1.set(datcon1rec.intDate);
			wsaaToday.set(datcon1rec.intDate);
			wsaaCompany.set(bsprIO.getCompany());
			for (ix.set(1); !(isGT(ix, flupList.size())); ix.add(1)){
				initArrayLetter9000();
			}
			ix.set(ZERO);
		}
		catch(Exception ex)
		{
			LOGGER.error("initialise1010()", ex);
		}
		
		
	}

 

protected void readFile2000()
{
	try {
		readFile2010();
	}
	catch (GOTOException e){
		LOGGER.error("readFile2000()", e);
	}
} 

protected void readFile2010()
{
	if (!flupList.isEmpty()){	
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!flupList.isEmpty()){			
				flupf=new Fluupf();
				flupf = iteratorList.next();
				 		
			}
		}else {		
			flupf = new Fluupf();
			flupf = iteratorList.next();
			 
		}	
	}else
	{
		wsspEdterror.set(varcom.endp);
	}

}

protected void endOfFile2080()
	{
	
	try
	{
		 
		triggerArrayLetter8000();
	}
	catch(Exception ex)
	{
		LOGGER.error("endOfFile2080()", ex);
	}
}

protected void edit2500()
	{
		edit2510();
		
	}

protected void edit2510()
	{
		/* Check record is required for processing.*/
		/* Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		/*    MOVE FUPCDE                     TO ITEM-ITEMITEM.            */
		wsaaT5661Lang.set(bsscIO.getLanguage());
		wsaaT5661Fupcode.set(flupf.getFupcde() == null ? "" : flupf.getFupcde().toUpperCase());//ILB-1054
		Itempf itempf = null;
		if(t5661Map.get(wsaaT5661Key.trim())!=null && !t5661Map.get(wsaaT5661Key.trim()).isEmpty()){				
			itempf = t5661Map.get(wsaaT5661Key.trim()).get(0); 
		}
		if(itempf!=null){
			t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			if (isEQ(flupf.getFupsts(), "L")) {
				wsaaFupstat.set(t5661rec.zletstat);
				wsaaLettype.set(t5661rec.zlettype);
			}
			else {
				/* For Reminder Letter Printing*/
				/*  Calculate the date of issuing reminder letter*/
				/*         MOVE 'DY'           TO DTC2-FREQUENCY                */
				/*        MOVE T5661-ZLEADAYS TO DTC2-FREQ-FACTOR              */
				/*         MOVE T5661-ZELPDAYS TO DTC2-FREQ-FACTOR      <U02>   */
				/*         CALL 'DATCON2' USING DTC2-DATCON2-REC                */
				/*         MOVE DTC2-INT-DATE-2                                 */
				/*                             TO WSAA-REMDT                    */
				/*     IF  WSAA-REMDT NOT < FUPDT                               */
				if (isGTE(bsscIO.getEffectiveDate(), flupf.getFupdt())) {
					wsaaFupstat.set(t5661rec.zletstatr);
					wsaaLettype.set(t5661rec.zlettyper);
				}
				else {
					wsspEdterror.set(SPACES);
				}
			}
		}else{
			fatalError600();
		}
	}

protected void update3000()
	{
		updateFluppf3010();
		ct01Value++;
	}

protected void updateFluppf3010()
	{
		/*  Update follow up status and the reminder date for FLUPPF.*/
		/*    MOVE SPACES              TO FLUP-PARAMS.                     */
		/*    MOVE CHDRCOY             TO FLUP-CHDRCOY.                    */
		/*    MOVE CHDRNUM             TO FLUP-CHDRNUM.                    */
		/*    MOVE FUPNO               TO FLUP-FUPNO.                      */
		/*    MOVE FLUPREC OF FORMATS  TO FLUP-FORMAT.             <LA4463>*/
		/*    MOVE READH               TO FLUP-FUNCTION.                   */
		/*    CALL 'FLUPIO'            USING FLUP-PARAMS.                  */
		/*    IF FLUP-STATUZ          NOT =  O-K                           */
		/*        MOVE FLUP-PARAMS        TO SYSR-PARAMS                   */
		/*        MOVE FLUP-STATUZ        TO SYSR-STATUZ                   */
		/*        PERFORM 600-FATAL-ERROR.                                 */
		/*    MOVE WSAA-FUPSTAT        TO FLUP-FUPSTAT.                    */
		/* MOVE WSAA-REMDT          TO FLUP-FUPREMDT.           <LA4463>*/
		/*    MOVE REWRT               TO FLUP-FUNCTION.                   */
		/*    MOVE FLUPREC OF FORMATS  TO FLUP-FORMAT.             <LA4463>*/
		/*    CALL 'FLUPIO'            USING FLUP-PARAMS.                  */
		/*    IF FLUP-STATUZ          NOT =  O-K                           */
		/*        MOVE FLUP-PARAMS        TO SYSR-PARAMS                   */
		/*        MOVE FLUP-STATUZ        TO SYSR-STATUZ                   */
		/*        PERFORM 600-FATAL-ERROR.                                 */
		
		flupf.setFupsts(wsaaFupstat.toString());		
		updateflupList.add(flupf);
		
		/* Update fields when a new contract is read.                      */
		/*  IF WSAA-LAST-CHDRNUM     NOT = CHDRNUM               <U93>   */
		/*  OR WSAA-LAST-FUPCDE      NOT = FUPCDE                <U93>   */
		/*     MOVE CHDRNUM             TO WSAA-LAST-CHDRNUM     <U93>   */
		/*     MOVE FUPCDE              TO WSAA-LAST-FUPCDE      <U93>   */
		/*     MOVE 'Y'                 TO WSAA-DUP-ALLOWED      <U93>   */
		/*  END-IF.                                              <U93>   */
		readChdrlif5200();
		retrieveForms5500();
		/*  PERFORM 6000-WRITE-LETTER.                                   */
		writeArrayLetter7000();
	}

protected void commit3500()
	{
		fluDAO.updateFlupRecord(updateflupList);
		contotrec.totval.set(ct01Value);
		contotrec.totno.set(1);
		callContot001();
		ct01Value = 0;
		
		
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
		/**    EXIT.*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
		/**    EXIT.*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*   Close the cursor*/
		//getAppVars().freeDBConnectionIgnoreErr(sqlfluppf1conn, sqlfluppf1ps, sqlfluppf1rs);
		/*  Close any open files.*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void readChdrlif5200()
	{
		/*START*/
		List<Chdrpf> chdrList = chdrlifIOMap.get(flupf.getChdrnum());
		if(chdrList!=null && !chdrList.isEmpty())
			chdrlifIO =  chdrList.get(0);
		else{
			syserrrec.params.set(flupf.getChdrnum());
			fatalError600();
		}
		/*EXIT*/
	}

protected void retrieveForms5500()
	{
		start5501();
	}

protected void start5501()
	{
		String clntKey = chdrlifIO.getCowncoy() + clntpfDAO.getSplitSign() + chdrlifIO.getCownnum();
		if(!clntpfMap.containsKey(clntKey)){
			syserrrec.params.set(chdrlifIO.getCownnum());
			fatalError600();
		}
		/* IF CLTS-STATCODE         NOT = SPACES                        */
		/*     PERFORM 5600-READ-T3628                                  */
		/*     IF FORMTYPE-FOUND                                        */
		/*         GO TO 5599-EXIT                                      */
		/*     END-IF                                                   */
		/* END-IF.                                                      */
		if (isEQ(wsaaLettype, SPACES)) {
			return ;
		}
		
		
		Map<String, List<Itempf>> t3609ListMap = itemDAO.loadSmartTable("IT",
				chdrlifIO.getChdrcoy().toString(), "T3609");/* IJTI-1523 */
		String keyItemitem = wsaaLettype.toString();
		List<Itempf> itempfList = new ArrayList<Itempf>();

		if (t3609ListMap.containsKey(keyItemitem.trim())) {
			itempfList = t3609ListMap.get(keyItemitem.trim());
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
	
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				t3609rec.t3609Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		}else {
			t3609rec.t3609Rec.set(SPACES);
		}
		
		if (isEQ(t3609rec.zformtyp, SPACES)) {
			wsaaFormtype.set("*STD");
		}
		else {
			wsaaFormtype.set(t3609rec.zformtyp);
		}
	}

	/**
	* <pre>
	*5600-READ-T3628 SECTION.
	*5601-START.
	**** MOVE SPACES                 TO ITEM-PARAMS.
	**** MOVE 'IT'                   TO ITEM-ITEMPFX.
	**** MOVE BSPR-FSUCO             TO ITEM-ITEMCOY.         <P37>
	**** MOVE T3628                  TO ITEM-ITEMTABL.
	**** MOVE CLTS-STATCODE          TO ITEM-ITEMITEM.
	**** MOVE READR                  TO ITEM-FUNCTION.
	**** CALL 'ITEMIO'               USING ITEM-PARAMS.
	**** IF  ITEM-STATUZ          NOT = O-K
	****     MOVE ITEM-PARAMS        TO SYSR-PARAMS
	****     MOVE ITEM-STATUZ        TO SYSR-STATUZ
	****     PERFORM 600-FATAL-ERROR
	**** END-IF.
	**** MOVE ITEM-GENAREA           TO T3628-T3628-REC.
	**** PERFORM VARYING WSAA-SUB FROM 1 BY 1
	****                             UNTIL WSAA-SUB > 10
	****   IF T3628-ZLETTYP(WSAA-SUB) = WSAA-LETTYPE
	****       IF T3628-ZFORMTYP(WSAA-SUB)
	****                          NOT = SPACES
	****           MOVE 'Y'          TO WSAA-FORMTYPE-FOUND
	****           MOVE T3628-ZFORMTYP(WSAA-SUB)
	****                             TO WSAA-FORMTYPE
	****       END-IF
	****   END-IF
	**** END-PERFORM.
	* </pre>
	*/
protected void writeLetter6000()
	{
		start6010();
	}

protected void start6010()
	{
		if (isEQ(wsaaLettype, SPACES)) {
			return ;
		}
		/* Do not produce a LETC for the same contract unless              */
		/* specifically requested).                                        */
		if (isEQ(t5661rec.zdalind, SPACES)) {
			letcmntIO.setDataKey(SPACES);
			letcmntIO.setStatuz(varcom.oK);
			letcmntIO.setRequestCompany(flupf.getChdrcoy());
			letcmntIO.setLetterType(wsaaLettype);
			letcmntIO.setClntcoy(chdrlifIO.getCowncoy());
			letcmntIO.setRdocnum(chdrlifIO.getChdrnum());
			letcmntIO.setClntnum(chdrlifIO.getCownnum());
			letcmntIO.setLetterSeqno(9999999);
			letcmntIO.setFormat(formatsInner.letcrec);
			letcmntIO.setFunction(varcom.endr);
			SmartFileCode.execute(appVars, letcmntIO);
			if (isEQ(letcmntIO.getStatuz(), varcom.oK)
			&& isEQ(letcmntIO.getRequestCompany(), flupf.getChdrcoy())
			&& isEQ(letcmntIO.getLetterType(), wsaaLettype)
			&& isEQ(letcmntIO.getClntcoy(), chdrlifIO.getCowncoy())
			&& isEQ(letcmntIO.getRdocnum(), chdrlifIO.getChdrnum())
			&& isEQ(letcmntIO.getClntnum(), chdrlifIO.getCownnum())) {
				/*        MOVE LETCMNT-OTHER-KEYS TO WSBB-LETOKEYS         <U93>*/
				letcokcpy.lredReadOtherKeys.set(letcmntIO.getOtherKeys());
				/*        IF  WSBB-LETO-LIFE      = LIFE                   <U93>*/
				if (isEQ(letcokcpy.lredLfLife, flupf.getLife())) {
					return ;
				}
			}
			/****     IF WSAA-DUP-ALLOWED       = 'N'                   <U93>   */
			/****        GO TO 6099-EXIT                                <U93>   */
			/****     ELSE                                              <U93>   */
			/****        MOVE 'N'              TO WSAA-DUP-ALLOWED      <U93>   */
			/****     END-IF                                            <U93>   */
			/****  ELSE                                                 <U93>   */
			/****     MOVE 'N'                 TO WSAA-DUP-ALLOWED      <U93>   */
		}
		/*  Write LETC via LETRQST and update the seqno by 1*/
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(flupf.getChdrcoy());
		letrqstrec.letterType.set(wsaaLettype);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.despnum.set(chdrlifIO.getDespnum());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.otherKeys.set(SPACES);
		/* MOVE CHDRLIF-CHDRCOY        TO WSAA-LETO-CHDRCOY.            */
		/* MOVE CHDRLIF-CHDRNUM        TO WSAA-LETO-CHDRNUM.            */
		/* MOVE LIFE                   TO WSAA-LETO-LIFE.               */
		/* MOVE JLIFE                  TO WSAA-LETO-JLIFE.         <Q91>*/
		/* MOVE FUPTYP                 TO WSAA-LETO-FUPTYP.             */
		/* MOVE WSAA-FORMTYPE          TO WSAA-LETO-FORMTYPE.           */
		/* MOVE BSSC-LANGUAGE          TO WSAA-LETO-LANGUAGE.           */
		/* MOVE FUPCDE                 TO WSAA-LETO-FLUPCODE.           */
		/*    MOVE BPRD-DEFAULT-BRANCH    TO WSAA-LETO-BRANCH.*/
		/* MOVE FUPNO                  TO WSAA-LETO-FUPNO.              */
		letcokcpy.recCode.set("LF");
		letcokcpy.lfChdrcoy.set(chdrlifIO.getChdrcoy());
		letcokcpy.lfChdrnum.set(chdrlifIO.getChdrnum());
		letcokcpy.lfLife.set(flupf.getLife());
		letcokcpy.lfJlife.set(flupf.getJlife());
		letcokcpy.lfFuptyp.set(flupf.getFuptyp());
		letcokcpy.lfFormtype.set(wsaaFormtype);
		letcokcpy.lfLanguage.set(bsscIO.getLanguage());
		letcokcpy.lfFlupcode.set(flupf.getFupcde());
		letcokcpy.lfFupno.set(flupf.getFupno());
		/* MOVE WSAA-LETOKEYS          TO LETRQST-OTHER-KEYS.           */
		letrqstrec.otherKeys.set(letcokcpy.saveOtherKeys);
		letrqstrec.function.set("ADD");
		if (isNE(wsaaLettype, SPACES)) {
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz, varcom.oK)) {
				syserrrec.params.set(letrqstrec.params);
				syserrrec.statuz.set(letrqstrec.statuz);
				fatalError600();
			}
			/*****     PERFORM 6500-WRITE-ZLTX                                  */
		}
	}

	/**
	* <pre>
	* Section not called. ZLTX records do not appear to be used
	* anywhere and contain no data.
	*6500-WRITE-ZLTX SECTION.
	*6010-START.
	**** MOVE SPACES                 TO ZLTX-PARAMS.
	**** PERFORM VARYING WSAA-SUB2 FROM 1 BY 1
	****                             UNTIL WSAA-SUB2 > 8
	****   MOVE ZEROES               TO ZLTX-ZNUMFLD(WSAA-SUB2)
	**** END-PERFORM.
	**** MOVE LETRQST-LETCKEY        TO ZLTX-DATA-KEY.
	**** MOVE CHDRCOY                TO ZLTX-CHDRCOY.
	**** MOVE CHDRLIF-CHDRNUM        TO ZLTX-CHDRNUM.
	**** MOVE ZLTXREC                TO ZLTX-FORMAT.
	**** MOVE WRITR                  TO ZLTX-FUNCTION.
	**** CALL 'ZLTXIO'               USING ZLTX-PARAMS.
	**** IF  ZLTX-STATUZ          NOT = O-K
	****   MOVE ZLTX-PARAMS          TO SYSR-PARAMS
	****   MOVE ZLTX-STATUZ          TO SYSR-STATUZ
	****   PERFORM 600-FATAL-ERROR
	**** END-IF.
	* </pre>
	*/
protected void writeArrayLetter7000()
	{
		start7010();
	}

protected void start7010()
	{
		if (isEQ(wsaaLettype, SPACES)) {
			return ;
		}
		ix.add(1);
		//if (isGT(ix, size)) {
			//syserrrec.statuz.set(e103);
			//fatalError600();
		//}
		
		if (isGT(ix, flupList.size())) 
		{
			return;
		}
		
		/* Only one letter is printed for the same letter type.            */
		for (iy.set(1); !((setPrecision(iy, 0)
		&& isGT(iy, sub(ix, 1)))); iy.add(1)){
			if (isEQ(chdrlifIO.getChdrpfx(), wsaaLetcRdocpfx[iy.toInt()])
			&& isEQ(chdrlifIO.getChdrcoy(), wsaaLetcRdoccoy[iy.toInt()])
			&& isEQ(chdrlifIO.getChdrnum(), wsaaLetcRdocnum[iy.toInt()])
			&& isEQ(wsaaLettype, wsaaLetcLettype[iy.toInt()])) {
				ix.subtract(1);
				return ;
			}
		}
		letcokcpy.recCode.set("LF");
		letcokcpy.lfChdrcoy.set(chdrlifIO.getChdrcoy());
		letcokcpy.lfChdrnum.set(chdrlifIO.getChdrnum());
		letcokcpy.lfLife.set(flupf.getLife());
		letcokcpy.lfJlife.set(flupf.getJlife());
		letcokcpy.lfFuptyp.set(flupf.getFuptyp());
		letcokcpy.lfFormtype.set(wsaaFormtype);
		letcokcpy.lfLanguage.set(bsscIO.getLanguage());
		letcokcpy.lfFlupcode.set(flupf.getFupcde());
		letcokcpy.lfFupno.set(flupf.getFupno());
		wsaaLetcCompany[ix.toInt()].set(flupf.getChdrcoy());
		wsaaLetcLettype[ix.toInt()].set(wsaaLettype);
		wsaaLetcClntcoy[ix.toInt()].set(chdrlifIO.getCowncoy());
		wsaaLetcClntnum[ix.toInt()].set(chdrlifIO.getCownnum());
		wsaaLetcRequestDte[ix.toInt()].set(wsaaToday);
		wsaaLetcRdocpfx[ix.toInt()].set(chdrlifIO.getChdrpfx());
		wsaaLetcRdoccoy[ix.toInt()].set(chdrlifIO.getChdrcoy());
		wsaaLetcRdocnum[ix.toInt()].set(chdrlifIO.getChdrnum());
		wsaaLetcTranno[ix.toInt()].set(chdrlifIO.getTranno());
		wsaaLetcChdrcoy[ix.toInt()].set(chdrlifIO.getChdrcoy());
		wsaaLetcChdrnum[ix.toInt()].set(chdrlifIO.getChdrnum());
		wsaaLetcDespnum[ix.toInt()].set(chdrlifIO.getDespnum());
		wsaaLetcBranch[ix.toInt()].set(chdrlifIO.getCntbranch());
		wsaaLetcOtherKeys[ix.toInt()].set(letcokcpy.saveOtherKeys);
	}

protected void triggerArrayLetter8000()
	{
	try
	{
		/*START*/
		for (ix.set(1); !(isGT(ix, flupList.size())
		|| isEQ(wsaaLetcCompany[ix.toInt()], SPACES)); ix.add(1)){
			checkDuplicateLetc8100();
			if (!duplicateLetc.isTrue()) {
				writeLetcRecord8200();
			}
		}
		/*EXIT*/
	}
	catch(Exception ex)
	{
		LOGGER.error("triggerArrayLetter8000()", ex);
	}
}

protected void checkDuplicateLetc8100()
	{
		duplcateLetc8110();
	}

protected void duplcateLetc8110()
	{
		wsaaDuplicateLetc.set(SPACES);
		letcmntIO.setParams(SPACES);
		letcmntIO.setRequestCompany(wsaaLetcCompany[ix.toInt()]);
		letcmntIO.setLetterType(wsaaLetcLettype[ix.toInt()]);
		letcmntIO.setClntcoy(wsaaLetcClntcoy[ix.toInt()]);
		letcmntIO.setRdocnum(wsaaLetcRdocnum[ix.toInt()]);
		letcmntIO.setClntnum(wsaaLetcClntnum[ix.toInt()]);	
		letcmntIO.setLetterSeqno(9999999);
		letcmntIO.setFunction(varcom.endr);
		letcmntIO.setFormat(formatsInner.letcrec);
		SmartFileCode.execute(appVars, letcmntIO);
		if (isEQ(letcmntIO.getStatuz(), varcom.oK)
		&& isEQ(letcmntIO.getRequestCompany(), wsaaLetcCompany[ix.toInt()])
		&& isEQ(letcmntIO.getLetterType(), wsaaLetcLettype[ix.toInt()])
		&& isEQ(letcmntIO.getClntcoy(), wsaaLetcClntcoy[ix.toInt()])
		&& isEQ(letcmntIO.getRdocnum(), wsaaLetcRdocnum[ix.toInt()])
		&& isEQ(letcmntIO.getClntnum(), wsaaLetcClntnum[ix.toInt()])) {
			letcokcpy.lredReadOtherKeys.set(letcmntIO.getOtherKeys());
			letcokcpy.saveOtherKeys.set(wsaaLetcOtherKeys[ix.toInt()]);
			if (isEQ(letcokcpy.lredLfLife, letcokcpy.lfLife)
			&& isEQ(letcokcpy.lredLfJlife, letcokcpy.lfJlife)) {
				wsaaDuplicateLetc.set("Y");
			}
		}
	}

protected void writeLetcRecord8200()
	{
		try
		{
			writeLetc8210();
		}
		catch(Exception ex)
		{
			LOGGER.error("writeLetcRecord8200()", ex);
		}
	}
protected void writeLetc8210()
	{
	 
	try
	{
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(wsaaLetcCompany[ix.toInt()]);
		letrqstrec.letterType.set(wsaaLetcLettype[ix.toInt()]);
		letrqstrec.clntcoy.set(wsaaLetcClntcoy[ix.toInt()]);
		letrqstrec.clntnum.set(wsaaLetcClntnum[ix.toInt()]);
		letrqstrec.letterRequestDate.set(wsaaLetcRequestDte[ix.toInt()]);
		letrqstrec.rdocpfx.set(wsaaLetcRdocpfx[ix.toInt()]);
		letrqstrec.rdoccoy.set(wsaaLetcRdoccoy[ix.toInt()]);
		letrqstrec.rdocnum.set(wsaaLetcRdocnum[ix.toInt()]);
		letrqstrec.tranno.set(wsaaLetcTranno[ix.toInt()]);
		letrqstrec.chdrcoy.set(wsaaLetcChdrcoy[ix.toInt()]);
		letrqstrec.chdrnum.set(wsaaLetcChdrnum[ix.toInt()]);
		letrqstrec.despnum.set(wsaaLetcDespnum[ix.toInt()]);
		letrqstrec.branch.set(wsaaLetcBranch[ix.toInt()]);
		letrqstrec.otherKeys.set(wsaaLetcOtherKeys[ix.toInt()]);
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
	}
	catch (Exception e)
	{
		LOGGER.error("writeLetc8210()", e);
	}
}

protected void initArrayLetter9000()
	{
		/*START*/
	try
	{
		wsaaLetcCompany[ix.toInt()].set(SPACES);
		wsaaLetcLettype[ix.toInt()].set(SPACES);
		wsaaLetcClntcoy[ix.toInt()].set(SPACES);
		wsaaLetcClntnum[ix.toInt()].set(SPACES);
		wsaaLetcRdocpfx[ix.toInt()].set(SPACES);
		wsaaLetcRdoccoy[ix.toInt()].set(SPACES);
		wsaaLetcRdocnum[ix.toInt()].set(SPACES);
		wsaaLetcChdrcoy[ix.toInt()].set(SPACES);
		wsaaLetcChdrnum[ix.toInt()].set(SPACES);
		wsaaLetcDespnum[ix.toInt()].set(SPACES);
		wsaaLetcBranch[ix.toInt()].set(SPACES);
		wsaaLetcOtherKeys[ix.toInt()].set(SPACES);
		wsaaLetcRequestDte[ix.toInt()].set(ZERO);
		wsaaLetcTranno[ix.toInt()].set(ZERO);
		/*EXIT*/
	}
	catch(Exception ex)
	{
		LOGGER.error("initArrayLetter9000()", ex);	
	}
}
/*
 * Class transformed  from Data Structure WSAA-LETC-ARRAY--INNER
 */

/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {

		/* The formats BUPA BSSC BPRD BSPR and BMSG are required by MAINB
		  processing and should not be deleted.*/
	private FixedLengthStringData formats = new FixedLengthStringData(110);
	private FixedLengthStringData letcrec = new FixedLengthStringData(10).isAPartOf(formats, 100).init("LETCREC");
}
/*
 * Class transformed  from Data Structure SQL-FLUPPF--INNER
 */

}
