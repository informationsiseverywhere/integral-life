package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5013
 * @version 1.0 generated on 30/08/09 06:30
 * @author Quipoz
 */
public class S5013ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(482);
	public FixedLengthStringData dataFields = new FixedLengthStringData(194).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,52);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,99);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,107);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData occups = new FixedLengthStringData(8).isAPartOf(dataFields, 156);
	public FixedLengthStringData[] occup = FLSArrayPartOfStructure(2, 4, occups, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(occups, 0, FILLER_REDEFINE);
	public FixedLengthStringData occup01 = DD.occup.copy().isAPartOf(filler,0);
	public FixedLengthStringData occup02 = DD.occup.copy().isAPartOf(filler,4);
	public FixedLengthStringData pursuits = new FixedLengthStringData(24).isAPartOf(dataFields, 164);
	public FixedLengthStringData[] pursuit = FLSArrayPartOfStructure(4, 6, pursuits, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(pursuits, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01 = DD.pursuit.copy().isAPartOf(filler1,0);
	public FixedLengthStringData pursuit02 = DD.pursuit.copy().isAPartOf(filler1,6);
	public FixedLengthStringData pursuit03 = DD.pursuit.copy().isAPartOf(filler1,12);
	public FixedLengthStringData pursuit04 = DD.pursuit.copy().isAPartOf(filler1,18);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,188);
	public FixedLengthStringData smokings = new FixedLengthStringData(4).isAPartOf(dataFields, 190);
	public FixedLengthStringData[] smoking = FLSArrayPartOfStructure(2, 2, smokings, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(smokings, 0, FILLER_REDEFINE);
	public FixedLengthStringData smoking01 = DD.smoking.copy().isAPartOf(filler2,0);
	public FixedLengthStringData smoking02 = DD.smoking.copy().isAPartOf(filler2,2);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 194);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData occupsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] occupErr = FLSArrayPartOfStructure(2, 4, occupsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(occupsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData occup01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData occup02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData pursuitsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] pursuitErr = FLSArrayPartOfStructure(4, 4, pursuitsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(pursuitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData pursuit02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData pursuit03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData pursuit04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData smokingsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] smokingErr = FLSArrayPartOfStructure(2, 4, smokingsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(smokingsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData smoking01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData smoking02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 266);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData occupsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] occupOut = FLSArrayPartOfStructure(2, 12, occupsOut, 0);
	public FixedLengthStringData[][] occupO = FLSDArrayPartOfArrayStructure(12, 1, occupOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(24).isAPartOf(occupsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] occup01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] occup02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData pursuitsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] pursuitOut = FLSArrayPartOfStructure(4, 12, pursuitsOut, 0);
	public FixedLengthStringData[][] pursuitO = FLSDArrayPartOfArrayStructure(12, 1, pursuitOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(48).isAPartOf(pursuitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pursuit01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] pursuit02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] pursuit03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] pursuit04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData smokingsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] smokingOut = FLSArrayPartOfStructure(2, 12, smokingsOut, 0);
	public FixedLengthStringData[][] smokingO = FLSDArrayPartOfArrayStructure(12, 1, smokingOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(smokingsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] smoking01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] smoking02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(268);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(58).isAPartOf(subfileArea, 0);
	public ZonedDecimalData agerate = DD.agerate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public ZonedDecimalData extCessTerm = DD.ecestrm.copyToZonedDecimal().isAPartOf(subfileFields,3);
	public ZonedDecimalData insprm = initialize_insprm();
	public FixedLengthStringData opcda = DD.opcda.copy().isAPartOf(subfileFields,12);
	public ZonedDecimalData oppc = DD.oppc.copyToZonedDecimal().isAPartOf(subfileFields,14);
	public FixedLengthStringData reasind = DD.reasind.copy().isAPartOf(subfileFields,19);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,20);
	public ZonedDecimalData seqnbr = DD.seqnbr.copyToZonedDecimal().isAPartOf(subfileFields,21);
	public FixedLengthStringData shortdesc = DD.shortdesc.copy().isAPartOf(subfileFields,24);
	public ZonedDecimalData zmortpct = DD.zmortpct.copyToZonedDecimal().isAPartOf(subfileFields,34);
	public ZonedDecimalData znadjperc = DD.znadjperc.copyToZonedDecimal().isAPartOf(subfileFields,37);
	/*BRD-306 START */
	public ZonedDecimalData premadj = DD. zpremiumAdjusted.copyToZonedDecimal().isAPartOf(subfileFields, 42);
	public FixedLengthStringData uwoverwrite = DD.uwoverwrite.copy().isAPartOf(subfileFields,57);
	/*BRD-306 END */
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(52).isAPartOf(subfileArea, 58);
	public FixedLengthStringData agerateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData ecestrmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData insprmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData opcdaErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData oppcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData reasindErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData seqnbrErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData shortdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData zmortpctErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData znadjpercErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	/*BRD-306 START */
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData uwoverwriteErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	/*BRD-306 END */
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(156).isAPartOf(subfileArea, 110);
	public FixedLengthStringData[] agerateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] ecestrmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] insprmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] opcdaOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] oppcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] reasindOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] seqnbrOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] shortdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] zmortpctOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] znadjpercOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	/*BRD-306 START */
	public FixedLengthStringData[] premadjOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] uwoverwriteOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	/*BRD-306 END */
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 266);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5013screensflWritten = new LongData(0);
	public LongData S5013screenctlWritten = new LongData(0);
	public LongData S5013screenWritten = new LongData(0);
	public LongData S5013protectWritten = new LongData(0);
	public GeneralTable s5013screensfl = new GeneralTable(AppVars.getInstance());
	public int uwFlag = 0;
	public int skipautouw = 0; 
	public int pageFlag = 0; 
	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5013screensfl;
	}

	public S5013ScreenVars() {
		super();
		initialiseScreenVars();
	}
	
	protected ZonedDecimalData initialize_insprm(){
		return DD.insprm.copyToZonedDecimal().isAPartOf(subfileFields,6);
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(opcdaOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agerateOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(oppcOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(insprmOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ecestrmOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"06","07","-06","07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasindOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(znadjpercOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zmortpctOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(uwoverwriteOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {seqnbr, opcda, shortdesc, agerate, oppc, insprm, extCessTerm, select, reasind, znadjperc, zmortpct,premadj,uwoverwrite};
		screenSflOutFields = new BaseData[][] {seqnbrOut, opcdaOut, shortdescOut, agerateOut, oppcOut, insprmOut, ecestrmOut, selectOut, reasindOut, znadjpercOut, zmortpctOut,premadjOut,uwoverwriteOut};
		screenSflErrFields = new BaseData[] {seqnbrErr, opcdaErr, shortdescErr, agerateErr, oppcErr, insprmErr, ecestrmErr, selectErr, reasindErr, znadjpercErr, zmortpctErr,premadjErr,uwoverwriteErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, life, coverage, rider, lifcnum, linsname, smoking01, occup01, pursuit01, pursuit02, jlifcnum, jlinsname, smoking02, occup02, pursuit03, pursuit04, crtable, crtabdesc};
		screenOutFields = new BaseData[][] {chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, smoking01Out, occup01Out, pursuit01Out, pursuit02Out, jlifcnumOut, jlinsnameOut, smoking02Out, occup02Out, pursuit03Out, pursuit04Out, crtableOut, crtabdescOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, smoking01Err, occup01Err, pursuit01Err, pursuit02Err, jlifcnumErr, jlinsnameErr, smoking02Err, occup02Err, pursuit03Err, pursuit04Err, crtableErr, crtabdescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5013screen.class;
		screenSflRecord = S5013screensfl.class;
		screenCtlRecord = S5013screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5013protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5013screenctl.lrec.pageSubfile);
	}
}
