package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;

import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;



import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;

import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;

public class Nlgcalc extends SMARTCodeModel {
	
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();
	private Varcom varcom = new Varcom();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("NLGCALC");
	private Syserrrec syserrrec = new Syserrrec();
	public FixedLengthStringData wsaaProposal = new FixedLengthStringData(1);
	public FixedLengthStringData wsaaTrandesc = new FixedLengthStringData(30);
	public FixedLengthStringData wsaaTrcde = new FixedLengthStringData(4);
	public FixedLengthStringData wsaaValidFlag = new FixedLengthStringData(1);
	public FixedLengthStringData wsaaPfx = new FixedLengthStringData(2);
	public ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0);
	public FixedLengthStringData wsaaValidflg = new FixedLengthStringData(1);
	
	
	private Lifepf lifepf = new Lifepf();
	
	private NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	DescpfDAO descpfDAO =  DAOFactory.getDescpfDAO();
	
	private static final String t1688 = "T1688";
	
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Agecalcrec agecalcrec = new Agecalcrec();
	Descpf descpf = new Descpf();
	private Datcon1rec datcon1rec = new Datcon1rec();
	
	private Nlgtpf nlgtpf;
	private Nlgtpf nlgtpf1;
	
	public Nlgcalc() {
		super();
	}

	public void mainline(Object... parmArray)
	{
		nlgcalcrec.nlgcalcRec = convertAndSetParam(nlgcalcrec.nlgcalcRec, parmArray, 0);
		try {
			main1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main1000()
	{
		init1010();
		
		exit1090();
	}
	
protected void init1010()
{
	nlgcalcrec.status.set(varcom.oK);
	nlgcalcrec.currAge.set("ZEROS");
	nlgcalcrec.yrsInf.set("ZEROS");
	wsaaProposal.set("N");
	nlgcalcrec.fsuco.set("9");
	wsaaTrcde.set(nlgcalcrec.batctrcde);
	callDatcon1();
	nlgtpf = new Nlgtpf();
	nlgtpf1= new Nlgtpf();
	lifepf = lifepfDAO.getLifeRecord(nlgcalcrec.chdrcoy.toString(), nlgcalcrec.chdrnum.toString(), "01", "00");
	if (lifepf == null ) {
		syserrrec.params.set(lifepf);
		fatalError600();
	}
	nlgcalcrec.cltdob.set(lifepf.getCltdob());
	if (isEQ(nlgcalcrec.transMode,"L")){
		nlgcalcrec.nlgFlag.set("N");
		update3020();
		return;
	}
	wsaaValidflg.set(1);
	nlgtpf = nlgtpfDAO.getNlgRecord(nlgcalcrec.chdrcoy.toString(),nlgcalcrec.chdrnum.toString(),wsaaValidflg.toString());
	if(nlgtpf == null){
		wsaaProposal.set("Y");
	}
	calcpolicyYr1100();
	calAttAge1200();
	process2000();
}
protected void callDatcon1()
{
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	if (isNE(datcon1rec.statuz,varcom.oK)) {
		syserrrec.params.set(datcon1rec.datcon1Rec);
		syserrrec.statuz.set(datcon1rec.statuz);
		fatalError600();
	}
}
protected void  calcpolicyYr1100(){
	
	datcon3rec.intDate1.set(nlgcalcrec.occdate);
	datcon3rec.intDate2.set(datcon1rec.intDate);
	datcon3rec.frequency.set("01");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		fatalError600();
	}
	nlgcalcrec.yrsInf.set(datcon3rec.freqFactor);

}
protected void  calAttAge1200(){
	
		if (isGT(nlgcalcrec.cltdob,ZERO)){
			callAgecalc1210();
		}
}
protected void callAgecalc1210(){
	initialize(agecalcrec.agecalcRec);
	agecalcrec.function.set("CALCP");
	agecalcrec.language.set(nlgcalcrec.language);
	agecalcrec.cnttype.set(nlgcalcrec.cnttype);
	agecalcrec.intDate1.set(nlgcalcrec.cltdob);
	agecalcrec.intDate2.set(datcon1rec.intDate);
	agecalcrec.company.set(nlgcalcrec.fsuco);
	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isNE(agecalcrec.statuz,varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		syserrrec.params.set(agecalcrec.agecalcRec);
		fatalError600();
	}
	nlgcalcrec.currAge.set(agecalcrec.agerating);
}
protected void process2000(){
	
	nlgcalcrec.nlgFlag.set("Y");
	if (isEQ(wsaaProposal ,"Y")){
		upadte2080();
		return;
	}
	nlgcalcrec.totTopup.set(nlgtpf.getAmnt01());
	nlgcalcrec.totWdrAmt.set(nlgtpf.getAmnt02());
	nlgcalcrec.ovduePrem.set(nlgtpf.getAmnt03());
	nlgcalcrec.unpaidPrem.set(nlgtpf.getAmnt04());
	//wsaaTranno.set(nlgtpf.getTranno());
			
	if (isEQ(nlgcalcrec.function, "TOPUP")){
		calcTopup2100();
	}
	if (isEQ(nlgcalcrec.function, "PSURR")){
		Surramt2200();
	}
	if (isEQ(nlgcalcrec.function, "OVDUE")){
		calcOverdue2300();
	}
	if (isEQ(nlgcalcrec.function, "PHRST")){
		calcUnpaid2400();
	}
	if (isEQ(nlgcalcrec.function, "COLCT")){
		setOverdueUnpaid();
	}
	if (isEQ(nlgcalcrec.function, "LINST")){
		setOverdueUnpaid2500();
	}
	compute(nlgcalcrec.nlgBalance, 0).set(add(add(add(nlgcalcrec.totTopup, nlgcalcrec.totWdrAmt),nlgcalcrec.ovduePrem),nlgcalcrec.unpaidPrem));

	if (isLT(nlgcalcrec.nlgBalance,0)){
		nlgcalcrec.nlgFlag.set("N");
	}
	if (isEQ(nlgcalcrec.function, "CHECK")){
		return;
	}
	//wsaaTranno.add(1);
	//nlgcalcrec.tranno.set(wsaaTranno);
	upadte2080();
}

protected void calcTopup2100(){
	
	compute(nlgcalcrec.totTopup, 0).set(add(nlgcalcrec.totTopup, nlgcalcrec.inputAmt));
	
}
protected void Surramt2200(){
	
	
	compute(nlgcalcrec.totWdrAmt, 0).set(add(nlgcalcrec.totWdrAmt, nlgcalcrec.inputAmt));
	
	
}

protected void calcOverdue2300(){
    compute(nlgcalcrec.ovduePrem, 0).set(add(nlgcalcrec.ovduePrem,nlgcalcrec.inputAmt));
}

protected void calcUnpaid2400(){
	
	compute(nlgcalcrec.unpaidPrem, 0).set(add(nlgcalcrec.unpaidPrem, nlgcalcrec.inputAmt));
	
	
}
protected void setOverdueUnpaid(){
	
	nlgcalcrec.ovduePrem.set(ZERO);
	nlgcalcrec.unpaidPrem.set(ZERO);
}
protected void setOverdueUnpaid2500(){
	
	nlgcalcrec.ovduePrem.set(ZERO);
	nlgcalcrec.unpaidPrem.set(ZERO);
}

protected void upadte2080(){
		updateNlgt3000();
		
}

protected void updateNlgt3000(){

			update3020();
}	
protected void update3020(){
	if(nlgtpf1 == null)
		nlgtpf1= new Nlgtpf();
	nlgtpf1.setChdrcoy(nlgcalcrec.chdrcoy.toString());
	nlgtpf1.setChdrnum(nlgcalcrec.chdrnum.toString());
	nlgtpf1.setTranno(nlgcalcrec.tranno.toInt());
	nlgtpf1.setEffdate(nlgcalcrec.effdate.toInt());
	nlgtpf1.setBatcactyr(nlgcalcrec.batcactyr.toInt());
	nlgtpf1.setBatcactmn(nlgcalcrec.batcactmn.toInt());
	nlgtpf1.setBatctrcde(nlgcalcrec.batctrcde.toString());
	getTansDesc3100();
	nlgtpf1.setTrandesc(wsaaTrandesc.toString());
	nlgtpf1.setTranamt(nlgcalcrec.inputAmt.getbigdata());
	nlgtpf1.setFromdate(nlgcalcrec.frmdate.toInt());
	nlgtpf1.setTodate(nlgcalcrec.todate.toInt());
	nlgtpf1.setNlgflag(nlgcalcrec.nlgFlag.toString());
	nlgtpf1.setYrsinf(nlgcalcrec.yrsInf.toInt());
	nlgtpf1.setAge(nlgcalcrec.currAge.toInt());
	nlgtpf1.setNlgbal(nlgcalcrec.nlgBalance.getbigdata());
	nlgtpf1.setAmnt01(nlgcalcrec.totTopup.getbigdata());
	nlgtpf1.setAmnt02(nlgcalcrec.totWdrAmt.getbigdata());
	nlgtpf1.setAmnt03(nlgcalcrec.ovduePrem.getbigdata());
	nlgtpf1.setAmnt04(nlgcalcrec.unpaidPrem.getbigdata());
	nlgtpf1.setValidflag("1");
	nlgtpfDAO.insertNlgtpf(nlgtpf1);
	
	
}
protected void getTansDesc3100(){
	
	descpf= descpfDAO.getItemTblItemByLang("IT", t1688, nlgcalcrec.batctrcde.toString(),nlgcalcrec.language.toString(), nlgcalcrec.chdrcoy.toString());//IAF-
	if(descpf != null)
	{
		wsaaTrandesc.set(descpf.getLongdesc());
	}

}
protected void exit1090()
{

}

protected void fatalError600()
{
	error6000();
	exit6010();
}
protected void error6000()
{
	if (isEQ(syserrrec.statuz, varcom.bomb)) {
		return ;
	}
	syserrrec.syserrStatuz.set(syserrrec.statuz);
	if (isNE(syserrrec.syserrType, "2")
	&& isEQ(agecalcrec.function, "CALCP")) {
		syserrrec.syserrType.set("1");
	}
	if (isNE(syserrrec.syserrType, "4")
	&& isEQ(agecalcrec.function, "CALCB")) {
		syserrrec.syserrType.set("3");
	}
	callProgram(Syserr.class, syserrrec.syserrRec);
}

protected void exit6010()
	{
	nlgcalcrec.status.set(varcom.bomb);
		/*EXIT*/
		exitProgram();
	}
}
