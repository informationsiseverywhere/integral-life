package com.csc.life.newbusiness.dataaccess.model;

import java.io.Serializable;

public class Hpadpf implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3470308421134815960L;
	private long uniqueNumber;// NUMBER(18) not null,
	private String chdrcoy; // CHAR(1 CHAR),
	private String chdrnum; // CHAR(8 CHAR),
	private String validflag; // CHAR(1 CHAR),
	private int hpropdte; // NUMBER(8),
	private int hprrcvdt; // NUMBER(8),
	private int hissdte; // NUMBER(8),
	private int huwdcdte; // NUMBER(8),
	private int hoissdte; // NUMBER(8),
	private String zdoctor; // CHAR(8 CHAR),
	private String znfopt; // CHAR(3 CHAR),
	private int zsufcdte; // NUMBER(8),
	private String procflg; // CHAR(1 CHAR),
	private String usrprf; // CHAR(10 CHAR),
	private String jobnm; // CHAR(10 CHAR),
	private String datime; // TIMESTAMP(6),
	private String dlvrmode; // CHAR(4 CHAR),
	private int despdate; // NUMBER(8),
	private int packdate; // NUMBER(8),
	private int remdte; // NUMBER(8),
	private int deemdate; // NUMBER(8),
	private int nxtdte; // NUMBER(8),
	private String incexc; // CHAR(1 CHAR)
	private String refundOverpay; //fwang3 ICIL-4
	private int rskcommdate; // NUMBER(8), ILJ-40
	private int decldate; // NUMBER(8),  ILJ-40
	private int firstprmrcpdate; // NUMBER(8),  ILJ-42
	private int cnfirmtnIntentDate; // NUMBER(8),  ILJ-62
	private int tntapdate; // NUMBER(8),  IBPLIFE-2100
	
	public Hpadpf() {
		chdrcoy = "";
		chdrnum = "";
		validflag = "";
		hpropdte = 0;
		hprrcvdt = 0;
		hissdte = 0;
		huwdcdte = 0;
		hoissdte = 0;
		zdoctor = "";
		znfopt = "";
		zsufcdte = 0;
		procflg = "";
		usrprf = "";
		jobnm = "";
		datime = "";
		dlvrmode = "";
		despdate = 0;
		packdate = 0;
		remdte = 0;
		deemdate = 0;
		nxtdte = 0;
		incexc = "";
		refundOverpay = "";
		rskcommdate = 0;
		decldate = 0;
		firstprmrcpdate = 0;
		cnfirmtnIntentDate = 0;
		tntapdate = 0;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getValidflag() {
		return validflag;
	}

	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	public int getHpropdte() {
		return hpropdte;
	}

	public void setHpropdte(int hpropdte) {
		this.hpropdte = hpropdte;
	}

	public int getHprrcvdt() {
		return hprrcvdt;
	}

	public void setHprrcvdt(int hprrcvdt) {
		this.hprrcvdt = hprrcvdt;
	}

	public int getHissdte() {
		return hissdte;
	}

	public void setHissdte(int hissdte) {
		this.hissdte = hissdte;
	}

	public int getHuwdcdte() {
		return huwdcdte;
	}

	public void setHuwdcdte(int huwdcdte) {
		this.huwdcdte = huwdcdte;
	}

	public int getHoissdte() {
		return hoissdte;
	}

	public void setHoissdte(int hoissdte) {
		this.hoissdte = hoissdte;
	}

	public String getZdoctor() {
		return zdoctor;
	}

	public void setZdoctor(String zdoctor) {
		this.zdoctor = zdoctor;
	}

	public String getZnfopt() {
		return znfopt;
	}

	public void setZnfopt(String znfopt) {
		this.znfopt = znfopt;
	}

	public int getZsufcdte() {
		return zsufcdte;
	}

	public void setZsufcdte(int zsufcdte) {
		this.zsufcdte = zsufcdte;
	}

	public String getProcflg() {
		return procflg;
	}

	public void setProcflg(String procflg) {
		this.procflg = procflg;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public String getDatime() {
		return datime;
	}

	public void setDatime(String datime) {
		this.datime = datime;
	}

	public String getDlvrmode() {
		return dlvrmode;
	}

	public void setDlvrmode(String dlvrmode) {
		this.dlvrmode = dlvrmode;
	}

	public int getDespdate() {
		return despdate;
	}

	public void setDespdate(int despdate) {
		this.despdate = despdate;
	}

	public int getPackdate() {
		return packdate;
	}

	public void setPackdate(int packdate) {
		this.packdate = packdate;
	}

	public int getRemdte() {
		return remdte;
	}

	public void setRemdte(int remdte) {
		this.remdte = remdte;
	}

	public int getDeemdate() {
		return deemdate;
	}

	public void setDeemdate(int deemdate) {
		this.deemdate = deemdate;
	}

	public int getNxtdte() {
		return nxtdte;
	}

	public void setNxtdte(int nxtdte) {
		this.nxtdte = nxtdte;
	}

	public String getIncexc() {
		return incexc;
	}

	public void setIncexc(String incexc) {
		this.incexc = incexc;
	}

	public String getRefundOverpay() {
		return refundOverpay;
	}

	public void setRefundOverpay(String refundOverpay) {
		this.refundOverpay = refundOverpay;
	}

	public int getRskcommdate() {
		return rskcommdate;
	}

	public void setRskcommdate(int rskcommdate) {
		this.rskcommdate = rskcommdate;
	}

	public int getDecldate() {
		return decldate;
	}

	public void setDecldate(int decldate) {
		this.decldate = decldate;
	}

	public int getFirstprmrcpdate() {
		return firstprmrcpdate;
	}

	public void setFirstprmrcpdate(int firstprmrcpdate) {
		this.firstprmrcpdate = firstprmrcpdate;
	}

	public int getCnfirmtnIntentDate() {
		return cnfirmtnIntentDate;
	}

	public void setCnfirmtnIntentDate(int cnfirmtnIntentDate) {
		this.cnfirmtnIntentDate = cnfirmtnIntentDate;
	}

	public int getTntapdate() {
		return tntapdate;
	}

	public void setTntapdate(int tntapdate) {
		this.tntapdate = tntapdate;
	}

	
}
