/*
 * File: P5074.java
 * Date: 30 August 2009 0:02:11
 * Author: Quipoz Limited
 * 
 * Class transformed from P5074.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.Calendar;
import java.util.List;
import java.text.SimpleDateFormat;

import com.csc.common.constants.SmartTableConstants;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO; //ILIFE-8709
import com.csc.fsu.accounting.dataaccess.model.Acblpf; //ILIFE-8709
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO; //ILIFE-8709
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf; //ILIFE-8709
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.cls.Sflockwait;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.ZrdecplcPojo; //ILIFE-8709
import com.csc.fsu.general.procedures.ZrdecplcUtils; //ILIFE-8709
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO; //ILIFE-8709
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf; //ILIFE-8709
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.procedures.P5074at;
import com.csc.life.newbusiness.screens.S5074ScreenVars;
import com.csc.life.newbusiness.tablestructures.T3615rec;
import com.csc.life.newbusiness.tablestructures.Tjl05rec;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.procedures.FirstRcpPrem;
import com.csc.life.productdefinition.procedures.Rlpdlon;
import com.csc.life.productdefinition.recordstructures.FirstRcpPremRec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Rlpdlonrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.BatcTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.utils.ListUtils;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO; //ILIFE-8709
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf; //ILIFE-8709
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*               Contract Issue
*               ==============
*Initialise
*----------
*
*  Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
*  The details of the contract being worked on will be stored in
* the CHDRLNB I/O module.  Retrieve the details.
*
*      Look up the following:
*           Contract type description from T5688,
*           Contract status description from T3623.
*           Premium status description from T3588,
*           Servicing branch description from T1692,
*           The owner's  client  (CLTS)  details.   Format  the
*                client name for confirmation.
*           Count the number of life (LIFELNB) records attached
*                to the proposal.  For each life and joint life
*                record read, check to see if any are a life on
*                another proposal (use LFCLLNB),
*           The agent (AGNT)  to  get  the  client  number, and
*                hence  the client (agent's) name.  Format  the
*                client name for confirmation.
*
* Look up the  contract  definition  details from T5688. If the
* minimum  number of  policies  in  the  plan  is  blank,  plan
* processing is not  applicable  to the contract. In this case,
* non-display  the  prompt  for  the  number of policies in the
* plan.
*
*PREMIUM AMOUNTS
*
* Read  the  financial  accounting  rules for  the  transaction
* (T5645, item is transaction number passed in WSSP). Also read
* the sub-account type record for the  second  posting (posting
* no. 01 on T5645) to get the sign (T3695).
*
* Calculate the amounts to be shown on the screen as follows:
*
*      Contract Fee  -  read  the  contract  definition details
*           (T5688) for the  contract type held on the contract
*           header. If the  contract  fee  method is not blank,
*           look up the  subroutine  used  to calculate it from
*           T5674 and call  it  with the required linkage area.
*           This  subroutine   will  return  the  contract  fee
*           amount. If there  is  no  contract fee method, this
*           amount is zero.
*
*      Premium  -  read through the coverage/rider transactions
*           (COVTLNB).  For  each  record  read, accumulate all
*           single  premiums  payable  and all regular premiums
*           payable.  A  premium  is a single premium amount if
*           the  billing  frequency  for  the whole contract is
*           '00',  or the coverage/rider definition (T5687) has
*           a single premium indicator of 'Y'.  Otherwise it is
*           regular  premium.  Once  all the premiums have been
*           accumulated,  adjust  the  regular premiums (if not
*           zero)  by  the number of frequencies required prior
*           to  issue.  To  do  this,  call  DATCON3  with  the
*           contract  commencement  date,  billing commencement
*           date   and  billing  frequency  from  the  contract
*           header.  This  will  return a factor.  Multiply the
*           regular  premium  amount  by this factor and add it
*           to    the   single   premium  amount  to  give  the
*           "instatement"   premium  to  be  displayed  on  the
*           screen.
*
*      Premium  amount  required  - premium plus  contract  fee
*           calculated above.
*
*      Contract Suspense  -  read  the  sub-account balance for
*           posting 01 (as  defined  on  T5645) for the current
*           contract. Apply  the  sign for the sub-account type
*           (i.e. if the  sign  is -ve, negate the balance read
*           when displaying it on the screen).
*
*      Tolerance amount - if the premium required  is less than
*           or  equal  to  the  contract suspense  amount  (use
*           screen amount to give correct sign), this  is zero.
*           Otherwise calculation is as follows:
*           - look up the tolerance applicable (T5667, item key
*                = transaction code/contract currency).
*           - apply the tolerance percentage for  the  contract
*                billing  frequency  to  the  required  premium
*                amount calculated.  If this is less  than  the
*                tolerance  amount limit, use  this.  Otherwise
*                use the tolerance amount.
*           - output  the  lesser  of  the   tolerance   amount
*                calculated  above and the  difference  between
*                the   contract   suspense  and   the   premium
*                required.
*
*      Amount  available  - calculated as the  suspense  amount
*           plus the tolerance amount.
*
* If the amount required is greater than  the amount available,
* output an  error message. In this case,  the  contract cannot
* be issued.
*
*PRE-ISSUE VALIDATION
*
* Check the "avalilable for issue" indicator  on  the  contract
* header. If this is not 'Y', the contract  connot  be  issued.
* Output  an error message attached to the  contract  number  -
* "Not ready - F5?". (The indicators in the DDS  will enable F5
* to be pressed when an error code is output for this field.)
*
*
*Validation
*----------
*
* If  ENTER  was  pressed and the contract  cannot  be  issued,
* re-display the screen.
*
*Updating
*--------
*
* Skip this section  if  returning  from  an optional selection
* (current  stack  position  action  flag = '*')  or  CALC  was
* pressed.
*
* Write the contract header details stored in  the  CHDRLNB I/O
* module.
*
* Transfer  the  contract soft lock to  AT.
*
* Call ATREQ, passing the following in the AT request area:
*           AT module - P5074AT
*           Batch key - from WSSP
*           Requesting program - WSAA-PROG
*           Transaction ID - from VARCOM
*           User - from VARCOM
*           Terminal - from VARCOM
*           Request date - todays date (TDAY from DATCON1)
*           Time - from VARCOM
*           Language - from WSSP
*           Primary key - contract number
*           Transaction area - total  amount  to be transafered
*                from  contract  suspense  (calculated  as  the
*                total-amount-required   less   the  tolerance-
*                amount from the screen)
*
*Next Program
*------------
*
* Set WSSP-NEXTPROG to the current program (WSAA-PROG).
*
* If returning from the optional CALC, (stack action = '*'):
*
*      Re-load the saved programs onto the stack, and blank out
*           the '*'.
*
*      Retrieve  the contract header and check  the  "available
*           for issue" indicator. If it is not  "Y",  output an
*           error   message  saying  errors  in   proposal   or
*           something like that.
*
*      If the premium amount check failed in  the 1000 section,
*           output that error code again as well.
*
*      Set the next program name to the current screen name (to
*           re-display the screen) and exit.
*
* If CALC was pressed, use secondary switching to  pick  up the
* programs required.
*
*      Save the next set of programs (8) from the program stack
*           and flag the current positions with '*'.
*
*      Call GENSSWCH with an action of 'C'.
*
*      Load the  program  stack with the programs  returned  by
*           GENSSWCH, add 1 to the pointer and exit.
*
* If nothing is  selected,  continue  by just adding one to the
* program pointer.
*
******************Enhancements for Life Asia 1.0****************
*
* Referring to enhancement in P6378 where on issuing contracts,
* suspense available in different currencies is checked to settle
* the premium payable.  The enhancement here is to display
* suspense and tolerance limits in collection currency rather
* than always in contract currency.
*
*****************************************************************
* </pre>
*/
public class P5074 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
//	public int numberOfParameters = 0; //commentted to resolve findbug
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5074");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	protected PackedDecimalData wsaaFactor = new PackedDecimalData(11, 5);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	protected ZonedDecimalData wsbbSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaToleranceApp = new ZonedDecimalData(4, 2);
	private ZonedDecimalData wsaaToleranceApp2 = new ZonedDecimalData(4, 2);
	protected String wsaaSingPrmInd = "N";
	protected PackedDecimalData wsaaSingpFee = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaCalcTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCalcTolerance2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTolerance2 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPayrSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAmntDue = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotTolerance = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotTolerance2 = new PackedDecimalData(17, 2);
	private String wsaaInsufficientSuspense = "N";
	private String wsaaSuspInd = "";
	
	private FixedLengthStringData wsbbNRStat = new FixedLengthStringData(2).init("NR");
	private FixedLengthStringData wsbbPRStat = new FixedLengthStringData(2).init("PR");
	private FixedLengthStringData wsbbMRStat = new FixedLengthStringData(2).init("MR");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private ZonedDecimalData wsaaAmountLimit = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaAmountLimit2 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 0);
	private FixedLengthStringData wsaaT5667Curr = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 4);
	protected PackedDecimalData wsaaCntfee = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTax = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotalTax = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotPremTax = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaCntfeeTax = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaSingpfeeTax = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

		/*                                                         <V4L001>
		 WSAA-TOTAMNT1 is the APA amount required to pay premium due.    
		                                                         <V4L001>*/
	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(209);
	private PackedDecimalData wsaaTotamnt = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 0);
	private PackedDecimalData wsaaTotamnt1 = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 9);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 18);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 19);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 23);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 27);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 31);
	private FixedLengthStringData filler2 = new FixedLengthStringData(174).isAPartOf(wsaaTransactionRec, 35, FILLER).init(SPACES);
	private String g817 = "G817";
		/*                                                            <018>
		01  FILLER REDEFINES WSAA-PREM-DATE1.                       <018>
		01  FILLER REDEFINES WSAA-PREM-DATE2.                       <018>*/
	private FixedLengthStringData wsaaLifeAssDeadFlag = new FixedLengthStringData(1);
	private Validator lifeAssuredDead = new Validator(wsaaLifeAssDeadFlag, "Y");

	private FixedLengthStringData wsaaPayrkey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrkey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrkey, 8);
	private ZonedDecimalData wsaaBatclckCount = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);

	private FixedLengthStringData wsaaKill = new FixedLengthStringData(1).init(SPACES);
	private Validator firstTime = new Validator(wsaaKill, "N");

	private FixedLengthStringData wserOwnersel = new FixedLengthStringData(1);

	private FixedLengthStringData wserAgntsel = new FixedLengthStringData(1);

	private FixedLengthStringData wserCntbranch = new FixedLengthStringData(1);

	private FixedLengthStringData wserBilling = new FixedLengthStringData(1);

	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsaaReassuranceReqd = new FixedLengthStringData(1);
	private Validator reassuranceReqd = new Validator(wsaaReassuranceReqd, "Y");
	private String wsaaReassuranceChecked = "";
		/*Sub Account Balances - Contract Enquiry.*/
		/*Joind FSU and Life agent headers - new b*/
	private BatcTableDAM batcIO = new BatcTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client Roles by Foreign Key*/
		/*Client logical file with new fields*/
		/*Table items, date - maintenance view*/


	private RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T3695rec t3695rec = new T3695rec();
	private T5645rec t5645rec = new T5645rec();
	private T5667rec t5667rec = new T5667rec();
	private T5674rec t5674rec = new T5674rec();
	protected T5688rec t5688rec = new T5688rec();
	private T6687rec t6687rec = new T6687rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Tjl05rec tjl05rec = new Tjl05rec();//ILJ-44
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Gensswrec gensswrec = new Gensswrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	//private Atreqrec atreqrec = new Atreqrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Prasrec prasrec = new Prasrec();
	private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	private Wssplife wssplife = new Wssplife();
//	private S5074ScreenVars sv = ScreenProgram.getScreenVars( S5074ScreenVars.class);
	private S5074ScreenVars sv =   getLScreenVars();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	protected WsaaBillingInformationInner wsaaBillingInformationInner = new WsaaBillingInformationInner();
	private PayrpfDAO payrpfFSUDAO = getApplicationContext().getBean("payrpfFSUDAO", PayrpfDAO.class);
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	/* Clients as lives assured on proposals & Life and joint life details - new busine */
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	/* Logical File: SMART table reference data */
//	private ItempfDAO itempfDAO = DAOFactory.getItempfDAO();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);//ILIFE-3955
	
	//ILJ-49 Starts	
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	
	private Itempf itempf = null;
	private static final String t5667 = "T5667";
	private List<Itempf> itdmpfList = null;
//Table 
	private static final String t1692 = "T1692";
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String tr52e = "TR52E";
	private static final String tjl05 = "TJL05"; //ILJ-44
	//ILJ-44	
	private boolean contDtCalcFlag = false;
	private Hpadpf hpadPf = new Hpadpf();
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
	private String wsaaRiskUpdated = "";
	private ZonedDecimalData wsaaOccDate = new ZonedDecimalData(8, 0).setUnsigned();
	
	private ZonedDecimalData wsaaOldRiskDate = new ZonedDecimalData(8, 0).setUnsigned(); 
	private ZonedDecimalData wsaaNewRiskDate = new ZonedDecimalData(8, 0).setUnsigned(); 
	
	protected FixedLengthStringData wsaaNewDate = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaNewDateYyyy = new FixedLengthStringData(4).isAPartOf(wsaaNewDate, 0); 
	protected FixedLengthStringData wsaaNewDateMm = new FixedLengthStringData(2).isAPartOf(wsaaNewDate, 4);
	protected FixedLengthStringData wsaaNewDateDd = new FixedLengthStringData(2).isAPartOf(wsaaNewDate, 6);
	
	SimpleDateFormat integralDateFormat = new SimpleDateFormat("yyyyMMdd");
	//end
 //ILIFE-8709 start
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private Clrfpf clrfpf = new Clrfpf();
	private Clntpf clts = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Descpf descpf = new Descpf();
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO",AglfpfDAO.class);
	private Aglfpf aglfpf  = new Aglfpf();
	private Acblpf acblpf = new Acblpf();
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
 //ILIFE-8709 end
	private T3615rec t3615rec = new T3615rec();//IBPLIFE-2470
	private String t3615 = "T3615"; //IBPLIFE-2470
	private boolean onePflag = false; //IBPLIFE-2264
	private static final String  ONE_P_CASHLESS="NBPRP124"; //IBPLIFE-2264
	Atmodrec atmodrec = new Atmodrec();		
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		readseqCovtlnb1410, 
		checkBatcAgain3080, 
		exit3090, 
		calc4020, 
		exit4090
	}

	public P5074() {
		super();
		screenVars = sv;
		new ScreenModel("S5074", AppVars.getInstance(), sv);
	}

	protected S5074ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5074ScreenVars.class);
	}
	
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			//TMLII-268
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			//TMLII-268
			processBoMainline(sv, sv.dataArea, parmArray);
			//TMLII-1285 start
			/* Section 2000 gives G817 error in two conditions
			 * 1) if scrnparams.statuz is not equal to varcom.calc, wsaaReassuranceChecked indicator will not be set
			 *    to Y. Hence, program will also check for reassuranceReqd.isTrue(). If yes, it gives G817 error. Value 
			 *    for reassuranceReqd.isTrue() is true if contract type is active in re-assurance product bypass table 
			 *    T5447. And value for scrnparams.statuz is not varcom.calc, if user clicks on Continue rather than Refresh.
			 * 2) if chdrlnbIO.getAvlisu() is not equal to Y. Means, contract is not ready for issue.
			 * 
			 * In case of online contract issue, user clicks on Refresh button and therefore wsaaReassuranceChecked is set to Y
			 * and program doesn't give G817 error for first condition and exits from 2000 section. Another thing is, when user 
			 * clicks on refresh program calls pre-issue validation program P6378. When control returns from P6378, program checks
			 * for chdrlnbIO.getAvlisu() and if value is not Y, it gives G817 error which is condition 2.
			 * 
			 * In case when P5074 is called using policy issue batch L2BCHISSUE, P6378 is already called to check validation errors
			 * and therefore it is not required to call P6378 again. Hence, G817 error for above two conditions can be bypassed.
			 * 
			 * */
			if(isEQ(wsspcomn.sectionno, "2000")){
				if(isEQ(sv.chdrnumErr, g817)){
					sv.chdrnumErr.set(SPACES);
					wsspcomn.edterror.set(Varcom.oK);
				}
			}
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
	protected void initialise1000(){

		try {
			initialise1010();
			blankOutFields1015();
			getSign1020();
			calcContractFee1030();
			calculatePremium1050();
			calculateApa1064();
		}
		catch (GOTOException e){
		}
	}

	protected void initialise1010(){
		
		
	
		List<Payrpf> payrpfList = null;
		List<Itempf> tr52DItemList = null;
		
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
		onePflag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), ONE_P_CASHLESS, appVars, "IT");
				if(!contDtCalcFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					//ILJ-44
					sv.concommflgOut[varcom.nd.toInt()].set("Y");
					sv.rskcommdateOut[varcom.nd.toInt()].set("Y");
					sv.decldateOut[varcom.nd.toInt()].set("Y");
					sv.premRcptDateOut[varcom.nd.toInt()].set("Y");
					}
				else
					{
					sv.rskcommdateOut[varcom.pr.toInt()].set("Y");
						}
						//end
		//ILJ-49 End
		wsaaTotamnt.set(0);
		wsaaTotalPremium.set(0);
		wsaaTotalSuspense.set(0);
		wsaaSingPrmInd = "N";
		wsaaSingpFee.set(ZERO);
		wsaaTax.set(ZERO);
		wsaaTotalTax.set(ZERO);
		wsaaCntfeeTax.set(ZERO);
		wsaaSingpfeeTax.set(ZERO);
		wsaaTotPremTax.set(ZERO);
		wsaaCntfee.set(ZERO);
		sv.taxamt.set(ZERO);
		sv.taxamtOut[Varcom.pr.toInt()].set("Y");
		sv.taxamtOut[Varcom.nd.toInt()].set("Y");
		wsbbSub.set(1);
		while ( !(isGT(wsbbSub,9))) {
			wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaPremTax[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(SPACES);
			wsbbSub.add(1);
		}
		if(contDtCalcFlag){//ILJ-44
			sv.rskcommdate.set(varcom.vrcmMaxDate);
			sv.decldate.set(varcom.vrcmMaxDate);
			sv.premRcptDate.set(varcom.vrcmMaxDate);
			sv.concommflg.set(SPACES);
		}
		sv.billcd.set(varcom.vrcmMaxDate);
		sv.lassured.set(ZERO);
		sv.cntfee.set(ZERO);
		sv.cntsusp.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.polinc.set(ZERO);
		sv.tolerance.set(ZERO);
		sv.totamnt.set(ZERO);
		sv.totlprm.set(ZERO);
		sv.occdate.set(varcom.vrcmMaxDate);
		varcom.vrcmTerm.set(wsspcomn.term);
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Read table TR52D using CHDRLNB-REGISTER                         */
		//ILIFE-3955 STARTS
		itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrlnbIO.getRegister().toString());
		itempf.setItempfx(SmartTableConstants.ITEMPFX_IT);
		itempf.setItemseq(SmartTableConstants.ITEMSEQ_EMPTY);
		itempf.setItemtabl(SmartTableConstants.TABLE_TR52D);
		tr52DItemList = itemDAO.findByItemSeq(itempf);

		//ENDS		
		/* If no record found for given ITEM then search for *** */
		if(ListUtils.isEmpty(tr52DItemList)){
			
			//ILIFE-3955 STARTS
			itempf = new Itempf();
			
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemitem("***");
			itempf.setItempfx(SmartTableConstants.ITEMPFX_IT);
			itempf.setItemseq(SmartTableConstants.ITEMSEQ_EMPTY);
			itempf.setItemtabl(SmartTableConstants.TABLE_TR52D);
			tr52DItemList = itemDAO.findByItemSeq(itempf);
			//ENDS
		}

		
		/* Exit if no records found*/
		if(ListUtils.isEmpty(tr52DItemList)){
			syserrrec.params.set("");
			syserrrec.statuz.set(Varcom.mrnf);
			fatalError600();

		}
		
		tr52drec.tr52dRec.set(StringUtil.rawToString(tr52DItemList.get(0).getGenarea()));
		
		if (isNE(chdrlnbIO.getOccdate(),ZERO)) {
			sv.occdate.set(chdrlnbIO.getOccdate());
		}
		sv.billfreq.set(chdrlnbIO.getBillfreq());
		sv.mop.set(chdrlnbIO.getBillchnl());
		
		/* Read HPAD file for Proposal Details                             */
		//Added for ILJ-44
		if(contDtCalcFlag) {
			hpadPf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
			hpadPf.setChdrnum(chdrlnbIO.getChdrnum().toString());
			hpadPf = this.hpadpfDAO.getHpadData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
			if(null == hpadPf){
				fatalError600();
			}
			sv.rskcommdate.set(hpadPf.getRskcommdate());
			wsaaOldRiskDate.set(sv.rskcommdate);
			sv.decldate.set(hpadPf.getDecldate());
			sv.concommflg.set(chdrlnbIO.getConcommflg());
	
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.company.toString());
			itempf.setItemtabl(tjl05);
			itempf.setItemitem(chdrlnbIO.getCnttype().toString());
			itempf = itemDAO.getItemRecordByItemkey(itempf);
			if (itempf == null) {
				itempf = new Itempf();
				itempf.setItempfx("IT");
				itempf.setItemcoy(wsspcomn.company.toString());
				itempf.setItemtabl(tjl05);
				itempf.setItemitem("***");
				itempf = itemDAO.getItemRecordByItemkey(itempf);
				if (itempf == null) {
					tjl05rec.tjl05Rec.set(SPACES);
				}
			}
			if (itempf != null) {
				tjl05rec.tjl05Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			}
		}
		else {
 //ILIFE-8709 start
		hpadPf = hpadpfDAO.getHpadData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		
 //ILIFE-8709 end
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		
		t5688rec.polmin.set(ZERO);

		
		/* Search for PAYRPF record for given Contract Number & Company */
		payrpfList = payrpfFSUDAO.searchPayrRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		List<Clrfpf> clrfpfs = clrfpfDAO.getClrfForPayer(payrpfList, chdrlnbIO.getChdrpfx().toString());
		for(Payrpf payrpf : payrpfList){
			
			if(!(isNE(payrpf.getChdrcoy(), chdrlnbIO.getChdrcoy())
					||isNE(payrpf.getChdrnum(),chdrlnbIO.getChdrnum()))){
				wsbbSub.set(payrpf.getPayrseqno());
				wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(payrpf.getIncomeSeqNo());
				wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(payrpf.getBillfreq());
				wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].set(payrpf.getBtdate());
				for (Clrfpf clrfpf : clrfpfs) {
					if (clrfpf.getForecoy().equals(payrpf.getChdrcoy())
							&& clrfpf.getForenum().trim().equals(payrpf.getChdrnum() + payrpf.getPayrseqno())) {
						wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(clrfpf.getClntnum());
						wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(clrfpf.getClntcoy());
						break;
					}
				}
				//loadPayerDetails1a00(payrpf);
			}else{
				break;
			}
			
		}
		sv.billcd.set(chdrlnbIO.getBillcd());
		sv.cntcurr.set(chdrlnbIO.getCntcurr());
		sv.billcurr.set(chdrlnbIO.getBillcurr());
		sv.register.set(chdrlnbIO.getRegister());
		sv.srcebus.set(chdrlnbIO.getSrcebus());
		sv.agntsel.set(chdrlnbIO.getAgntnum());
		sv.campaign.set(chdrlnbIO.getCampaign());
		sv.stcal.set(chdrlnbIO.getChdrstcda());
		sv.stcbl.set(chdrlnbIO.getChdrstcdb());
		sv.stccl.set(chdrlnbIO.getChdrstcdc());
		sv.stcdl.set(chdrlnbIO.getChdrstcdd());
		sv.stcel.set(chdrlnbIO.getChdrstcde());
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.ownersel.set(chdrlnbIO.getCownnum());
		/*    MOVE S5074-OCCDATE          TO CHDRLNB-OCCDATE.*/
		/*    MOVE S5074-MOP              TO CHDRLNB-BILLCHNL.*/
		/*    MOVE S5074-MOP              TO CHDRLNB-COLLCHNL.*/
		/*    MOVE S5074-REPTYPE          TO CHDRLNB-REPTYPE.*/
		/*    MOVE S5074-LREPNUM          TO CHDRLNB-REPNUM.*/
		/*    MOVE S5074-CNTBRANCH        TO CHDRLNB-CNTBRANCH.*/
		/*    MOVE 'AG'                  TO CHDRLNB-AGNTPFX.*/
		/*    MOVE WSSP-COMPANY          TO CHDRLNB-AGNTCOY.*/
		/*    MOVE S5074-CAMPAIGN         TO CHDRLNB-CAMPAIGN.*/
		if(!contDtCalcFlag){
			sv.occdate.set(chdrlnbIO.getOccdate());
		}
		sv.mop.set(chdrlnbIO.getBillchnl());
		sv.reptype.set(chdrlnbIO.getReptype());
		sv.lrepnum.set(chdrlnbIO.getRepnum());
		sv.cntbranch.set(chdrlnbIO.getCntbranch());
		sv.campaign.set(chdrlnbIO.getCampaign());
 //ILIFE-8709 start
		descpf=descDAO.getdescData("IT",t5688,chdrlnbIO.getCnttype().toString(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
 //ILIFE-8709 end
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}
			
	protected void blankOutFields1015(){

	
		List<Lifepf> lifepfForChdrNumList = null;
		List<Itempf> t5645ItemList = null;
		
		wserOwnersel.set("Y");
		wserAgntsel.set("Y");
		wserCntbranch.set("Y");
		wserBilling.set("Y");
 //ILIFE-8709 start
		descpf=descDAO.getdescData("IT", t3623 ,chdrlnbIO.getStatcode().toString(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.statdsc.fill("?");
		}
		else {
			sv.statdsc.set(descpf.getLongdesc());
		}
		
		descpf=descDAO.getdescData("IT", t3588 ,chdrlnbIO.getPstatcode().toString(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.premStatDesc.fill("?");
		}
		else {
			sv.premStatDesc.set(descpf.getLongdesc());
		}
		
		sv.cntbranch.set(wsspcomn.branch);
		descpf=descDAO.getdescData("IT", t1692 ,wsspcomn.branch.toString(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf == null) {
			sv.sbrdesc.fill("?");
		}
		else {
			sv.sbrdesc.set(descpf.getLongdesc());
		}
 //ILIFE-8709 end
		wserOwnersel.set(SPACES);
		wserAgntsel.set(SPACES);
		wserCntbranch.set(SPACES);
		wserBilling.set(SPACES);
		formatClientName1700();
		if (isNE(sv.ownerselErr,SPACES)) {
			wserOwnersel.set("Y");
		}
		if (isNE(sv.cntbranchErr,SPACES)) {
			wserCntbranch.set("Y");
		}
		formatAgentName1900();
		if (isNE(sv.agntselErr,SPACES)) {
			wserAgntsel.set("Y");
		}
		/*   Count the number of life records attached to the proposal*/
		wsaaLifeAssDeadFlag.set("N");
		
		sv.conprosal.set("N");
		
		/* Search for LIFEPF records associated with given Contract Number */
		lifepfForChdrNumList = lifepfDAO.searchLifeRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
		
		for(Lifepf lifepfForChdrNum : lifepfForChdrNumList){
			
			if(!((isNE(lifepfForChdrNum.getChdrcoy(),wsspcomn.company)))
					|| (isNE(lifepfForChdrNum.getChdrnum(),chdrlnbIO.getChdrnum()))){


				countLives1300(lifepfForChdrNum);
			}else{
				break;
			}
		}
		
		readContractTable1600();
		if (isEQ(t5688rec.polmin,ZERO)) {
			sv.polincOut[Varcom.nd.toInt()].set("Y");
			sv.polincOut[Varcom.pr.toInt()].set("Y");
		}
		else {
			if (isEQ(chdrlnbIO.getPolinc(),ZERO)) {
				sv.polinc.set(t5688rec.poldef);
			}
		}
		wsaaBatckey.set(wsspcomn.batchkey);
		//ILIFE-3955 STARTS
		itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItempfx(SmartTableConstants.ITEMPFX_IT);
		itempf.setItemseq(SmartTableConstants.ITEMSEQ_EMPTY);
		itempf.setItemtabl(SmartTableConstants.TABLE_T5645);

		/* Search in ITEMPF for T5645 */
		t5645ItemList = itemDAO.findByItemSeq(itempf);
		//ENDS
		// GOTO Exit when no records found
		if(ListUtils.isEmpty(t5645ItemList)){
			/*     MOVE U016                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(errorsInner.g816);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1090);
		}
		
		// Set into REC
		t5645rec.t5645Rec.set(StringUtil.rawToString(t5645ItemList.get(0).getGenarea()));
	}

	protected void getSign1020(){

		List<Itempf> t3695ItemList = null;
		//ILIFE-3955 STARTS
		itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(t5645rec.sacstype01.toString());
		itempf.setItempfx(SmartTableConstants.ITEMPFX_IT);
		itempf.setItemseq(SmartTableConstants.ITEMSEQ_EMPTY);
		itempf.setItemtabl(SmartTableConstants.TABLE_T3695);
		
		/* Search in ITEMPF for Table T3695 */
		t3695ItemList = itemDAO.findByItemSeq(itempf);
//ENDS

		// GOTO Exit when no records found
		if(ListUtils.isEmpty(t3695ItemList)){
			scrnparams.errorCode.set(errorsInner.g228);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit1090);
		}
		//Set into REC
		t3695rec.t3695Rec.set(StringUtil.rawToString(t3695ItemList.get(0).getGenarea()));
	}

	protected void calcContractFee1030(){

	
		if (isNE(t5688rec.feemeth,SPACES)) {
			calcFee1200();

		}else {
			sv.cntfee.set(ZERO);
		}
		
		if (isNE(wsspcomn.edterror,SPACES)) {
			goTo(GotoLabel.exit1090);
		}
	}

	protected void calculatePremium1050(){
		calcPremium1400(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

		/*1052-ADJUST-REGULAR-PREM.                                        */
		/* Change the premium pro-rata to calculated on a payer            */
		/* by payer basis.                                                 */
		/* If The Calculated Regular Premium is equal to zeros then     */
		/* move zeros to the screen fields.                             */
		/*    IF WSAA-REGPREM             = ZERO                        */
		/*       MOVE ZEROS               TO S5074-INST-PREM            */
		/*       GO TO 1090-EXIT.                                       */
		/* Get the frequency Factor from DATCON3 for Regular premiums.  */
		/* IF CHDRLNB-BILLFREQ = '00'                                   */
		/*    MOVE 1                   TO DTC3-FREQ-FACTOR              */
		/*    GO TO 1053-CONT.                                          */
		/* MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-1.              */
		/* Billing Commencement Date 'BILLCD' is                           */
		/* replaced by Billed To Date 'BTDATE' and                         */
		/* the same pro-rating calculations are                            */
		/* performed as before.                                            */
		/* MOVE CHDRLNB-BILLCD         TO DTC3-INT-DATE-2.         <012>*/
		/* MOVE CHDRLNB-BTDATE         TO DTC3-INT-DATE-2.         <012>*/
		/* MOVE CHDRLNB-BILLFREQ       TO DTC3-FREQUENCY.               */
		/* CALL 'DATCON3' USING DTC3-DATCON3-REC.                       */
		/* IF DTC3-STATUZ              NOT = O-K                        */
		/*    GO TO 1090-EXIT.                                          */
		/*1053-CONT.                                                       */
		/* Use the DATCON3 Frequency Factor to calculate the Instate-   */
		/* ment Premium.                                                */
		/*                                                         <012>*/
		/*                                                         <012>*/
		/*                                                         <012>*/
		/* This is not always the case with the folling dates      <012>*/
		/* Datcon3 return an invalid frequency factor, section     <012>*/
		/* section 1800-check-date-freq gets around this problem   <012>*/
		/* situation.                                              <012>*/
		/*                                                         <012>*/
		/*                                                         <012>*/
		/* R.C.D         P.T.D              R.C.D           P.T.D  <012>*/
		/* 28/02         31/08              29/02           31/08  <012>*/
		/* 30/03         30/09              30/04           31/10  <012>*/
		/* 30/05         30/11              30/06           31/12  <012>*/
		/* 30/08         28/02              30/09           31/03  <012>*/
		/* 30/10         30/04              30/11           31/05  <012>*/
		/* 30/12         30/06                                     <012>*/
		/*                                                         <012>*/
		/*                                                         <012>*/
		/* MOVE DTC3-FREQ-FACTOR       TO WSAA-FACTOR.                  */
		/* MOVE CHDRLNB-OCCDATE        TO WSAA-PREM-DATE1.         <012>*/
		/* MOVE CHDRLNB-BILLCD         TO WSAA-PREM-DATE2.         <012>*/
		/* call new section to decide what the wsaa-factor should  <012>*/
		/* be depending on the risk commencment date of the policy <012>*/
		/*                                                         <012>*/
		/* PERFORM 1800-CHECK-DATE-FREQ.                           <012>*/
		/* MULTIPLY WSAA-REGPREM       BY WSAA-FACTOR                   */
		/*                             GIVING WSAA-INSTPRM.             */
		/* ADD WSAA-SINGP              TO WSAA-INSTPRM.                 */
		/* MOVE WSAA-INSTPRM           TO S5074-INST-PREM.              */
		/*                                                         <012>*/
		/*Pro-rate management fee.                                   <012>*/
		/*                                                         <012>*/
		/* MULTIPLY S5074-CNTFEE       BY WSAA-FACTOR              <012>*/
		/*   GIVING                    S5074-CNTFEE.               <012>*/
		/*1054-CALC-PREMIUM-AMT-REQUIRED.                                  */
		/* COMPUTE S5074-TRANAMT-1 = S5074-CNTFEE +                     */
		/*                           S5074-INST-PREM.                   */
		/*1060-CALC-SUSPENSE.*/
		/*    Read  the sub account balance for  the contract and apply*/
		/*    the sign for the sub account type.*/
		/*    Code replaced with call to Section to check for Suspense     */
		/*    in Contract Currency, Billing Currency or any currency.      */
		/* MOVE SPACES                 TO ACBL-DATA-AREA.          <013>*/
		/* MOVE ZEROS                  TO ACBL-RLDGACCT.           <013>*/
		/* MOVE WSSP-COMPANY           TO ACBL-RLDGCOY.            <013>*/
		/* MOVE CHDRLNB-CHDRNUM        TO ACBL-RLDGACCT.           <013>*/
		/* MOVE CHDRLNB-CNTCURR        TO ACBL-ORIGCURR.           <013>*/
		/* MOVE T5645-SACSCODE-01      TO ACBL-SACSCODE.           <013>*/
		/* MOVE T5645-SACSTYPE-01      TO ACBL-SACSTYP.            <013>*/
		/* MOVE READR                  TO ACBL-FUNCTION.           <013>*/
		/* CALL 'ACBLIO'               USING ACBL-PARAMS.          <013>*/
		/* IF  (ACBL-STATUZ         NOT = O-K)                     <013>*/
		/* AND (ACBL-STATUZ         NOT = MRNF)                    <013>*/
		/*    MOVE ACBL-PARAMS      TO SYSR-PARAMS                 <013>*/
		/*    PERFORM 600-FATAL-ERROR.                             <013>*/
		/*MOVE SPACES                 TO SACSLNB-DATA-AREA.            */
		/*MOVE WSSP-COMPANY           TO SACSLNB-CHDRCOY.              */
		/*MOVE CHDRLNB-CHDRNUM        TO SACSLNB-CHDRNUM.              */
		/*MOVE CHDRLNB-CNTCURR        TO SACSLNB-CNTCURR.              */
		/*MOVE T5645-SACSCODE-01      TO SACSLNB-SACSCODE.             */
		/*MOVE T5645-SACSTYPE-01      TO SACSLNB-SACSTYP.              */
		/*MOVE READR                  TO SACSLNB-FUNCTION.             */
		/*CALL 'SACSLNBIO' USING SACSLNB-PARAMS.                       */
		/*IF (SACSLNB-STATUZ      NOT = O-K) AND                       */
		/*   (SACSLNB-STATUZ      NOT = MRNF)                          */
		/*   MOVE SACSLNB-PARAMS      TO SYSR-PARAMS                   */
		/*   PERFORM 600-FATAL-ERROR.                                  */
		/* IF SACSLNB-STATUZ               = MRNF                       */
		/*    MOVE ZERO TO S5074-CNTSUSP                                */
		/* ELSE                                                         */
		/*    IF T3695-SIGN               = '-'                         */
		/*       MULTIPLY SACSLNB-SACSCURBAL BY -1                      */
		/*                             GIVING S5074-CNTSUSP             */
		/*    ELSE                                                      */
		/*        MOVE SACSLNB-SACSCURBAL  TO S5074-CNTSUSP.            */
		/* IF ACBL-STATUZ                  = MRNF                  <013>*/
		/*    MOVE ZERO                    TO WSAA-CNT-SUSPENSE    <013>*/
		/* ELSE                                                    <013>*/
		/*    IF T3695-SIGN               = '-'                    <013>*/
		/*       MULTIPLY ACBL-SACSCURBAL BY -1                    <013>*/
		/*                             GIVING WSAA-CNT-SUSPENSE    <013>*/
		/*    ELSE                                                 <013>*/
		/*        MOVE ACBL-SACSCURBAL    TO WSAA-CNT-SUSPENSE.    <013>*/
		/*IF SACSLNB-STATUZ               = MRNF                  <012>*/
		/*   MOVE ZERO TO WSAA-CNT-SUSPENSE                       <012>*/
		/*ELSE                                                    <012>*/
		/*   IF T3695-SIGN               = '-'                    <012>*/
		/*      MULTIPLY SACSLNB-SACSCURBAL BY -1                 <012>*/
		/*                            GIVING WSAA-CNT-SUSPENSE    <012>*/
		/*   ELSE                                                 <012>*/
		/*       MOVE SACSLNB-SACSCURBAL  TO WSAA-CNT-SUSPENSE.   <012>*/
		/* MOVE WSAA-CNT-SUSPENSE          TO WSAA-TOTAL-SUSPENSE.      */
		/* Check if a Suspense payment has been made in the Contract       */
		/* Currency, Billing Currency or any currency.                     */
		wsaaCntSuspense.set(ZERO);
		wsaaSuspInd = "N";
		sv.hcurrcdOut[Varcom.nd.toInt()].set("Y");
 //ILIFE-8709 start
		acblpf.setRldgacct(chdrlnbIO.getChdrnum().toString());
		acblpf.setRldgcoy(wsspcomn.company.toString());
		acblpf.setSacscode(t5645rec.sacscode01.toString());
		acblpf.setSacstyp(t5645rec.sacstype01.toString());
		for (wsaaSub.set(1); !(isGT(wsaaSub,3)
		|| isEQ(wsaaSuspInd,"Y")); wsaaSub.add(1)){
			checkSuspense1aa00();
		}
	 //ILIFE-8709 end
	}

	protected void calculateApa1064(){

		List<Itempf> t5667ItemList = null;
		List<Itempf> t6687ItemList = null;
		List<Itempf> t5447ItemList = null;
		
		checkApa1ab00();
		if (isGT(rlpdlonrec.prmdepst,ZERO)) {
			wsaaSuspInd = "Y";
		}
		/*1065-CALCULATE-TOLERANCE.                                        */
		/* IF (S5074-TRANAMT-1         < S5074-CNTSUSP) OR              */
		/*    (S5074-TRANAMT-1         = S5074-CNTSUSP)                 */
		/*    MOVE ZERO TO S5074-TOLERANCE                              */
		/*    GO TO 1080-CALC-AMOUNT-AVAILABLE.                         */
		/*  look up tolerance applicable*/





		/* MOVE WSKY-BATC-BATCTRCDE    TO WSAA-T5667-TRANNO.            */
		wsaaT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		/* MOVE CHDRLNB-CNTCURR        TO WSAA-T5667-CURR.              */
		if (isEQ(wsaaSuspInd,"Y")) {
			wsaaT5667Curr.set(acblpf.getOrigcurr()); //ILIFE-8709
		}
		else {
			wsaaT5667Curr.set(SPACES);
		}


		/* Search in ITEMPF for table T5667 */
		//ILIFE-3955 STARTS
		itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaT5667Key.toString());
		itempf.setItempfx(SmartTableConstants.ITEMPFX_IT);
		itempf.setItemseq(SmartTableConstants.ITEMSEQ_EMPTY);
		itempf.setItemtabl(SmartTableConstants.TABLE_T5667);
		t5667ItemList = itemDAO.findByItemSeq(itempf);
		//ENDS
		/* IF ITEM-STATUZ              = MRNF                           */
		/*    MOVE              ZERO TO WSAA-TOLERANCE-APP,             */
		/*                              WSAA-AMOUNT-LIMIT,              */
		/*    GO TO 1080-CALC-AMOUNT-AVAILABLE.                         */
		/* MOVE ITEM-GENAREA TO T5667-T5667-REC.                        */

		if (ListUtils.isEmpty(t5667ItemList)) {
			wsaaToleranceApp.set(ZERO);
			wsaaAmountLimit.set(ZERO);
			wsaaToleranceApp2.set(ZERO);
			wsaaAmountLimit2.set(ZERO);
		}
		else {
			t5667rec.t5667Rec.set(StringUtil.rawToString(t5667ItemList.get(0).getGenarea()));
		}
		/* PERFORM 1100-SEARCH-FOR-TOLERANCE VARYING WSAA-SUB           */
		/*    FROM 1 BY 1 UNTIL WSAA-SUB > 11 OR CHDRLNB-BILLFREQ       */
		/*    = T5667-FREQ (WSAA-SUB).                                  */
		/* IF WSAA-SUB                 > 11                             */
		/*    MOVE ZERO                TO WSAA-TOLERANCE-APP,           */
		/*                                WSAA-AMOUNT-LIMIT             */
		/*    GO TO 1080-CALC-AMOUNT-AVAILABLE.                         */
		/*1070-CALC-TOLERANCE.                                             */
		/* COMPUTE WSAA-CALC-TOLERANCE =                                */
		/*  (T5667-PRMTOL(WSAA-SUB)  * S5074-INST-PREM) / 100.          */
		/* Check % amount is less than Limit on T5667, if so use th<012>*/
		/* else use Limit.                                         <012>*/
		/* IF WSAA-CALC-TOLERANCE      < T5667-MAX-AMOUNT(WSAA-SUB)     */
		/*    MOVE T5667-MAX-AMOUNT(WSAA-SUB) TO S5074-TOLERANCE   <012>*/
		/*    MOVE WSAA-CALC-TOLERANCE TO S5074-TOLERANCE          <012>*/
		/* ELSE                                                         */
		/*    MOVE WSAA-CALC-TOLERANCE TO S5074-TOLERANCE.         <012>*/
		/*    MOVE T5667-MAX-AMOUNT(WSAA-SUB) TO S5074-TOLERANCE.  <012>*/
		/* COMPUTE WSAA-DIFF        = S5074-INST-PREM - S5074-CNTSUSP   */
		/* COMPUTE WSAA-DIFF        = S5074-TRANAMT-1 - S5074-CNTSUSP   */
		/* IF WSAA-DIFF                < S5074-TOLERANCE                */
		/*    MOVE WSAA-DIFF           TO S5074-TOLERANCE.              */
		/*1080-CALC-AMOUNT-AVAILABLE.                                      */
		/*Calculate Total amount AVAILABLE                               */
		/* COMPUTE S5074-TOTAMNT = S5074-CNTSUSP                   <012>*/
		/*        + S5074-TOLERANCE.                               <012>*/
		/* IF S5074-CNTSUSP            < S5074-TRANAMT-1           <012>*/
		/*    ADD S5074-CNTSUSP                                    <012>*/
		/*        S5074-TOLERANCE      GIVING WSAA-SUSP-TOL        <012>*/
		/*    IF WSAA-SUSP-TOL         NOT < S5074-TRANAMT-1       <012>*/
		/*       MOVE WSAA-SUSP-TOL    TO S5074-TOTAMNT            <012>*/
		/*    ELSE                                                 <012>*/
		/*    MOVE S5074-CNTSUSP       TO S5074-TOTAMNT            <012>*/
		/* ELSE                                                    <012>*/
		/* MOVE S5074-CNTSUSP          TO S5074-TOTAMNT.           <012>*/
		/* Check if the amount required is greater than the amount        */
		/* available - if this is the case the contract cannot be       */
		/* issued.                                                      */
		/* IF S5074-TRANAMT-1 >                                         */
		/*    S5074-TOTAMNT                                             */
		/*    MOVE F785 TO S5074-CHDRNUM-ERR                            */
		/*    MOVE 'Y' TO WSSP-EDTERROR.                                */
		/* IF CHDRLNB-AVLISU       NOT = 'Y'                            */
		/*    MOVE U018 TO S5074-CHDRNUM-ERR.                           */
		/*    MOVE 'Y' TO WSSP-EDTERROR.                                */
		/* Read the RTRN file to see if a cash receipt has been            */
		/* created for this contract.                                      */
		rtrnsacIO.setRldgcoy(wsspcomn.company);
		rtrnsacIO.setRldgacct(chdrlnbIO.getChdrnum());
		/* MOVE CHDRLNB-CNTCURR        TO RTRNSAC-ORIGCCY.         <012>*/
		if (isEQ(wsaaSuspInd,"Y")) {
			rtrnsacIO.setOrigccy(acblpf.getOrigcurr()); //ILIFE-8709
		}
		else {
			rtrnsacIO.setOrigccy(SPACES);
		}
		rtrnsacIO.setSacscode(t5645rec.sacscode01);
		rtrnsacIO.setSacstyp(t5645rec.sacstype01);
		rtrnsacIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, rtrnsacIO);
		if (isNE(rtrnsacIO.getStatuz(),Varcom.oK)
		&& isNE(rtrnsacIO.getStatuz(),Varcom.mrnf)) {
			syserrrec.params.set(rtrnsacIO.getParams());
			fatalError600();
		}
		if (isEQ(rtrnsacIO.getStatuz(),Varcom.mrnf)) {
			rtrnsacIO.setEffdate(varcom.vrcmMaxDate);
		}
		/* Look up the tax relief method on T5688.                         */
		//ILIFE-4492
	
		
		itempf=new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5688);
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItmfrm(new BigDecimal(sv.occdate.toInt()));
		itempf.setItmto(new BigDecimal(wsaaToday.toInt())); //IBPLIFE-4381
		itdmpfList = itemDAO.findByItemDates(itempf);
		
		if(itdmpfList == null || itdmpfList.isEmpty()){
			sv.occdateErr.set(errorsInner.f290);
		}else{
			t5688rec.t5688Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		}

		/* Look up the subroutine on T6687.                                */
		//ILIFE-3955 STARTS
		itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(t5688rec.taxrelmth.toString());
		itempf.setItempfx(SmartTableConstants.ITEMPFX_IT);
		itempf.setItemseq(SmartTableConstants.ITEMSEQ_EMPTY);
		itempf.setItemtabl(SmartTableConstants.TABLE_T6687);
		t6687ItemList = itemDAO.findByItemSeq(itempf);
		//ENDS

		if (ListUtils.isNotEmpty(t6687ItemList)) {
			t6687rec.t6687Rec.set(StringUtil.rawToString(t6687ItemList.get(0).getGenarea()));
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Adjust the regular premiums (if not zero) by the number         */
		/* of frequencies required prior to issue.                         */
		/* If the contract has a tax relief method deduct the tax          */
		/* relief amount from the amount due.                              */
		/* Also check if there is enough money in suspense to issue        */
		/* this contract.                                                  */
		wsaaTotTolerance.set(0);
		wsaaTotTolerance2.set(0);
		wsbbSub.set(1);
		while ( !(isGT(wsbbSub,9)
		|| isEQ(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()], SPACES))) {
			adjustPremium1b00();
		}
		if (isEQ(wsaaSuspInd,"Y"))
		{
			t5667ItemList=itemDAO.getAllitemsbyCurrency("IT",wsspcomn.company.toString(),t5667, wsaaT5667Key.toString());
			if (t5667ItemList.size()> 0) 
			{
				if (isGT(wsaaTotalPremium, ZERO)) {
					wsaaInsufficientSuspense = "Y";
				}
				if(isEQ(wsaaInsufficientSuspense,"Y") && isLT(wsaaTotalSuspense,wsaaTotalPremium)) {
					chdrlnbIO.setPstatcode(wsbbPRStat);
				} else {
					chdrlnbIO.setPstatcode(wsbbMRStat);
				}
			}
		}
		else
		{
			chdrlnbIO.setPstatcode(wsbbNRStat);
		}
		sv.instPrem.set(wsaaTotalPremium);
		sv.cntsusp.set(wsaaTotalSuspense);
		sv.prmdepst.set(rlpdlonrec.prmdepst);
 //ILIFE-8709 start
		descpf=descDAO.getdescData("IT", t3588 ,chdrlnbIO.getPstatcode().toString(),wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf != null) {
			sv.premStatDesc.set(descpf.getLongdesc());
		}
		else {
			sv.premStatDesc.fill("?");
		}
 //ILIFE-8709 end
		/*COMPUTE S5074-TRANAMT-1 = WSAA-TOTAL-PREMIUM +       <D96NUM>*/
		/*                                                         <D96NUM>*/
		compute(sv.totlprm, 2).set(add(wsaaTotalPremium,sv.cntfee));
		totalPremiumCustomerSpecific();
		/*IF WSAA-TOTAL-SUSPENSE > WSAA-TOTAL-PREMIUM  OR         <012>*/
		/*   WSAA-TOTAL-SUSPENSE = WSAA-TOTAL-PREMIUM             <012>*/
		/*IF WSAA-TOTAL-SUSPENSE > S5074-TRANAMT-1     OR      <D96NUM>*/
		/*   WSAA-TOTAL-SUSPENSE = S5074-TRANAMT-1             <D96NUM>*/
		/* Cannot use S5074-TRANAMT-1 to compare with Suspense as          */
		/* this uses INSTPREM + CONTFEE & Suspense could be in             */
		/* another currency. Use W/S value instead.                        */
		/*                                                         <D96NUM>*/
		/* IF WSAA-TOTAL-SUSPENSE > S5074-TOTLPRM    OR         <D96NUM>*/
		/*    WSAA-TOTAL-SUSPENSE = S5074-TOTLPRM               <D96NUM>*/
		/*     MOVE 0                  TO S5074-TOLERANCE          <012>*/
		/* ELSE                                                    <012>*/
		/* COMPUTE WSAA-DIFF = WSAA-TOTAL-PREMIUM -             <012>*/
		/* COMPUTE WSAA-DIFF = S5074-TRANAMT-1    -          <D96NUM>*/
		/*    COMPUTE WSAA-DIFF = S5074-TOTLPRM    -            <D96NUM>*/
		/*                        WSAA-TOTAL-SUSPENSE              <012>*/
		/*    IF WSAA-DIFF < WSAA-TOT-TOLERANCE                    <012>*/
		/*       MOVE WSAA-DIFF           TO S5074-TOLERANCE       <012>*/
		/*    ELSE                                                 <012>*/
		/*       MOVE WSAA-TOT-TOLERANCE  TO S5074-TOLERANCE       <012>*/
		/*    END-IF                                               <012>*/
		/* END-IF.                                                 <012>*/
		/*COMPUTE S5074-TRANAMT-1 = WSAA-TOTAL-PREMIUM +          <012>*/
		/*                         S5074-CNTFEE.                 <012>*/
		/*IF S5074-CNTSUSP            < S5074-TRANAMT-1        <D96NUM>*/
		/*                                                         <D96NUM>*/
		/* IF S5074-CNTSUSP            < S5074-TOTLPRM          <D96NUM>*/
		/*    ADD S5074-CNTSUSP                                    <012>*/
		/*        S5074-TOLERANCE      GIVING WSAA-SUSP-TOL        <012>*/
		/*   IF WSAA-SUSP-TOL         NOT < S5074-TRANAMT-1    <D96NUM>*/
		/*    IF WSAA-SUSP-TOL         NOT < S5074-TOTLPRM      <D96NUM>*/
		/*       MOVE WSAA-SUSP-TOL    TO S5074-TOTAMNT            <012>*/
		/*    ELSE                                                 <012>*/
		/*       MOVE S5074-CNTSUSP       TO S5074-TOTAMNT         <012>*/
		/* ELSE                                                    <012>*/
		/*    MOVE S5074-CNTSUSP          TO S5074-TOTAMNT.        <012>*/
		/*    IF WSAA-TOTAL-SUSPENSE   NOT < WSAA-PREM-SUSPCCY     <V4L001>*/
		/*        MOVE 0                  TO S5074-TOLERANCE       <V4L001>*/
		/*    ELSE                                                 <V4L001>*/
		/*       COMPUTE WSAA-DIFF         = WSAA-PREM-SUSPCCY  -  <V4L001>*/
		/*                                   WSAA-TOTAL-SUSPENSE   <V4L001>*/
		/*       IF WSAA-DIFF              < WSAA-TOT-TOLERANCE    <V4L001>*/
		/*          MOVE WSAA-DIFF           TO S5074-TOLERANCE    <V4L001>*/
		/*       ELSE                                              <V4L001>*/
		/*          MOVE WSAA-TOT-TOLERANCE  TO S5074-TOLERANCE    <V4L001>*/
		/*       END-IF                                            <V4L001>*/
		/*    END-IF.                                              <V4L001>*/
		/*                                                         <V4L001>*/
		/*    IF S5074-CNTSUSP             < WSAA-PREM-SUSPCCY     <V4L001>*/
		/*       ADD S5074-CNTSUSP                                 <V4L001>*/
		/*           S5074-TOLERANCE  GIVING S5074-TOTAMNT         <V4L001>*/
		/*    ELSE                                                 <V4L001>*/
		/*       MOVE S5074-CNTSUSP       TO S5074-TOTAMNT         <V4L001>*/
		/*    END-IF                                               <V4L001>*/
		/*                                                         <V4L001>*/
		/* To incorporate Advance premium for available premium.           */
		if ((setPrecision(sv.totlprm, 2)
		&& isGTE((add(sv.cntsusp,sv.prmdepst)),sv.totlprm))) {
			sv.tolerance.set(ZERO);
		}
		else {
			compute(wsaaDiff, 2).set(sub(sub(sv.totlprm,sv.cntsusp),sv.prmdepst));
			if (isLT(wsaaDiff,wsaaTotTolerance)) {
				sv.tolerance.set(wsaaDiff);
			}
			else {
				/*        MOVE WSAA-TOT-TOLERANCE                        <V42013>*/
				/*                              TO S5074-TOLERANCE       <V42013>*/
				if (isEQ(wsaaTotTolerance2,0)) {
					sv.tolerance.set(wsaaTotTolerance);
				}
				else {
					if (isLT(wsaaDiff,wsaaTotTolerance2)) {
						sv.tolerance.set(wsaaDiff);
					}
					else {
						sv.tolerance.set(wsaaTotTolerance2);
					}
				}
			}
		}
		compute(sv.totamnt, 2).set(add(sv.tolerance,(add(sv.cntsusp,sv.prmdepst))));
		/*  Check if the amount required is greater than the amount        */
		/*    available - if this is the case the contract cannot be       */
		/*    issued.                                                      */
		/*    IF WSAA-INSUFFICIENT-SUSPENSE = 'Y'                  <V4L001>*/
		/*       MOVE F785 TO S5074-CHDRNUM-ERR                    <V4L001>*/
		/*       MOVE 'Y' TO WSSP-EDTERROR.                        <V4L001>*/
		if(contDtCalcFlag) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
			wsspcomn.effdate.set(wsaaToday);
		}
		if (isLT(sv.totamnt,sv.totlprm)) {
			sv.chdrnumErr.set(errorsInner.f785);
			wsspcomn.edterror.set("Y");
		} else if(contDtCalcFlag) {
			calcRskCommDate();
			for (int i = 1; i <= 10; i++) {
				if (isEQ(tjl05rec.billfreq[i].toString(),sv.billfreq.toString()) && isEQ(tjl05rec.mop[i], sv.mop)) {
						
					if(isEQ(wsaaOldRiskDate,wsaaNewRiskDate))
					{
						wsaaNewDate.set(wsaaOldRiskDate);
						wsaaRiskUpdated="N";
					}
					else
					{
						wsaaNewDate.set(wsaaNewRiskDate);
						wsaaRiskUpdated="Y";
					}
					
				        Calendar Date1 = Calendar.getInstance();
				        Calendar Date2 = Calendar.getInstance();
				        try
				        {
				        	Date1.setTime(integralDateFormat.parse(wsaaNewDate.toString()));
				    	    Date2.set(Date1.get(Calendar.YEAR), Date1.get(Calendar.MONTH)+1, 01);
				    	    
				        }catch (ParseException e) {
							
							e.printStackTrace();
						}
				    	
				        String Date3 = integralDateFormat.format(Date2.getTime());
				        wsaaOccDate.set(Date3);
				        
				if(isEQ(wsaaRiskUpdated,"Y"))	
				{
					if(isEQ(tjl05rec.fwcondt[i].toString(),"Y") && isEQ(tjl05rec.concommflg[i], "Y"))
					{
							if(isEQ(sv.concommflg,"Y"))
							{
								sv.occdate.set(wsaaNewRiskDate);					
							}
							else
							{
								sv.occdate.set(wsaaOccDate);
							}
						 
					}
					else if (isEQ(tjl05rec.fwcondt[i].toString(),"Y") && isEQ(tjl05rec.concommflg[i], "N"))
					{
						sv.occdate.set(wsaaOccDate);
						
					}
					else if (isEQ(tjl05rec.fwcondt[i].toString(),"N") && isEQ(tjl05rec.concommflg[i], "Y"))
					{
						sv.occdate.set(wsaaNewRiskDate);	
					}
					else if (isEQ(tjl05rec.fwcondt[i].toString(),"N") && isEQ(tjl05rec.concommflg[i], "N"))
					{
						sv.occdate.set(wsaaNewRiskDate);
					}
				 }
				
				if(isEQ(sv.occdate,chdrlnbIO.getOccdate()))
				{
					wsspcomn.cltiiflg.set("N");
				}
				else
				{
					wsspcomn.cltiiflg.set("Y");
				}	
			  }
		   }
		}
		/* Check the reassurance bypass table, T5447. If the contract      */
		/* type is not found, pre-issue validation is required to          */
		/* prevent retention limits being exceeded where concurrent        */
		/* proposals exist for the same life assured.                      */
		//ILIFE-3955 STARTS
		itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItempfx(SmartTableConstants.ITEMPFX_IT);
		itempf.setItemseq(SmartTableConstants.ITEMSEQ_EMPTY);
		itempf.setItemtabl(SmartTableConstants.TABLE_T5447);
		t5447ItemList = itemDAO.findByItemSeq(itempf);
		//ENDS
		if (ListUtils.isEmpty(t5447ItemList)) {
			wsaaReassuranceReqd.set("N");
		}
		else {
			wsaaReassuranceReqd.set("Y");
		}
		/*                                                      <FUPLET>*/
		/* IF CHDRLNB-AVLISU       NOT = 'Y'                    <FUPLET>*/
		/* OR REASSURANCE-REQD                                  <FUPLET>*/
		/*    MOVE U018 TO S5074-CHDRNUM-ERR.                   <FUPLET>*/
		/*    MOVE G817 TO S5074-CHDRNUM-ERR                    <FUPLET>*/
		/*    MOVE 'Y' TO WSSP-EDTERROR.                        <FUPLET>*/
		//TMLII-268 NB-08-001 start
		if(isNE(scrnparams.deviceInd, "*RMT")){
			sv.chdrnumErr.set(g817);
			wsspcomn.edterror.set("Y");
		}
		//TMLII-268 NB-08-001 end
	}

	protected void totalPremiumCustomerSpecific(){
		
	compute(sv.totlprm, 2).add(add(wsaaTotPremTax, wsaaCntfeeTax));
	sv.totlprm.add(wsaaSingpfeeTax);
	}
	
	protected void searchForTolerance1100(){

		/*GO*/
		/*  IF CHDRLNB-BILLFREQ    NOT = T5667-FREQ (WSAA-SUB)          */
		if (isNE(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()], t5667rec.freq[wsaaSub.toInt()])) {
			return ;
		}
		wsaaToleranceApp.set(t5667rec.prmtol[wsaaSub.toInt()]);
		wsaaAmountLimit.set(t5667rec.maxAmount[wsaaSub.toInt()]);
		/*EXIT*/
	}

	protected void calcFee1200(){

			readSubroutineTable1210();

	}

	protected void readSubroutineTable1210(){

		
		List<Itempf> t5674ItemList = null;
		
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/

		//ILIFE-3955 STARTS
		itempf = new Itempf();
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(t5688rec.feemeth.toString());
		itempf.setItempfx(SmartTableConstants.ITEMPFX_IT);
		itempf.setItemseq(SmartTableConstants.ITEMSEQ_EMPTY);
		itempf.setItemtabl(SmartTableConstants.TABLE_T5674);
		t5674ItemList = itemDAO.findByItemSeq(itempf);
		//ENDS
		if (ListUtils.isEmpty(t5674ItemList)) {
			syserrrec.params.set("");
			scrnparams.errorCode.set(errorsInner.f151);
			wsspcomn.edterror.set("Y");
			return ;
		}
		//Set into REC
		t5674rec.t5674Rec.set(StringUtil.rawToString(t5674ItemList.get(0).getGenarea()));
		
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		/* MOVE CHDRLNB-BILLFREQ       TO MGFL-BILLFREQ.                */
		mgfeelrec.billfreq.set(wsaaBillingInformationInner.wsaaBillfreq[1]);
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(wsspcomn.company);
		/* Check subroutine NOT = SPACES before attempting call.           */
		if (isEQ(t5674rec.commsubr,SPACES)) {
			return ;
		}
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if ((isNE(mgfeelrec.statuz,Varcom.oK))
		&& (isNE(mgfeelrec.statuz,Varcom.endp))) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getBillcd(),chdrlnbIO.getOccdate())) {
			sv.cntfee.set(ZERO);
		}
		else {
			setFeeValCustomerSpecific();
			callRounding8000();
			sv.cntfee.set(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		}
		/* MOVE MGFL-MGFEE             TO S5074-CNTFEE.                 */
		if (isGT(sv.cntfee, ZERO)) {
			wsaaCntfee.set(sv.cntfee);
			checkCalcContTax7100();
			wsaaCntfeeTax.set(wsaaTax);
		}
	}
 protected void   setFeeValCustomerSpecific(){
	 
	 zrdecplcPojo.setAmountIn(mgfeelrec.mgfee.getbigdata()); //ILIFE-8709
   }
	
	protected void countLives1300(Lifepf lifepfForChdrNum){

		count1300(lifepfForChdrNum);


	}

	protected void count1300(Lifepf lifepfForChdrNum){

		
		List<Lifepf> lifeRecordByLifcNumList =  null;
	 //ILIFE-8709 start	
		clts.setClntpfx(fsupfxcpy.clnt.toString());
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts.setClntnum(lifepfForChdrNum.getLifcnum());
		clts = clntpfDAO.selectActiveClient(clts);
		if(clts==null) {
			syserrrec.params.set(lifepfForChdrNum.getLifcnum());
			fatalError600();
		}
		if (isNE(clts.getCltdod(), varcom.vrcmMaxDate)) {
			lifeAssuredDead.setTrue();
		}
 //ILIFE-8709 end
		sv.lassured.add(1);
		if (isEQ(sv.conprosal,"Y")) {
			return ;
		}

		/*  Check for a current proposal for the same client no.*/

		lifeRecordByLifcNumList =  lifepfDAO.searchLifeRecordByLifcNum(wsspcomn.company.toString(), lifepfForChdrNum.getLifcnum());

				for(Lifepf lifeRecordByLifcNum : lifeRecordByLifcNumList){
			
			if(isNE(lifeRecordByLifcNum.getChdrcoy(),wsspcomn.company)
				|| isNE(lifeRecordByLifcNum.getLifcnum(),lifepfForChdrNum.getLifcnum())) {



				
					break;
					
			}else if(isNE(lifeRecordByLifcNum.getChdrnum(),lifepfForChdrNum.getChdrnum())){
				
				sv.conprosal.set("Y");

				break;
			}
		}




		
	}

	protected void calcPremium1400(String covtChdrCoy, String covtChdrNum){

		
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case readseqCovtlnb1410: 
					readseqCovtlnb1410(covtChdrCoy, covtChdrNum);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void readseqCovtlnb1410(String covtChdrCoy, String covtChdrNum){

	
		List<Covtpf> covtpfList = null;
		
		/*    Read each record and accumulate all single and regular*/
		/*    premiums payable.*/

		covtpfList = covtpfDAO.searchCovtRecordByCoyNum(covtChdrCoy, covtChdrNum);
		
		if(covtpfList.size() == 0){
			return;
		}
		
		for(Covtpf covtpf : covtpfList){
			if(((isNE(chdrlnbIO.getChdrcoy(),covtpf.getChdrcoy()))
					|| (isNE(chdrlnbIO.getChdrnum(),covtpf.getChdrnum())))){


				return;
			}
			
			wsbbSub.set(covtpf.getPayrseqno());
			wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()].add(PackedDecimalData.parseObject(covtpf.getInstprem()));
			wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()].add(PackedDecimalData.parseObject(covtpf.getSingp()));
			
			if (isNE(covtpf.getSingp(),0)) {
				wsaaSingPrmInd = "Y";
			}
			checkCalcCompTax7000(covtpf);
		}
		

	}

	protected void readContractTable1600(){

		
		itempf=new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5688);
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItmfrm(new BigDecimal(sv.occdate.toInt()));
		itempf.setItmto(new BigDecimal(wsaaToday.toInt())); //IBPLIFE-4381
		itdmpfList = itemDAO.findByItemDates(itempf);
		
		if(itdmpfList == null || itdmpfList.size() == 0){
			sv.occdateErr.set(errorsInner.f290);
		}else{
			t5688rec.t5688Rec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		}

	}

	protected void formatClientName1700(){

	
		readClientRecord1710();

	}

	protected void readClientRecord1710(){

		
		if (isEQ(chdrlnbIO.getCownnum(),SPACES)) {
			sv.ownerselErr.set(errorsInner.e186);
			return ;
		}
//ILIFE-8709 start		
		clts.setClntpfx("CN");
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts.setClntnum(chdrlnbIO.getCownnum().toString());
		sv.ownersel.set(chdrlnbIO.getCownnum());
		clts = clntpfDAO.selectActiveClient(clts);
		if(clts==null) {
			sv.ownerselErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
//ILIFE-8709 end
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

	/**
	* <pre>
	*                                                            <018>
	*                                                            <018>
	*1800-CHECK-DATE-FREQ  SECTION.                              <018>
	*******************************                              <018>
	*                                                            <018>
	*                                                            <018>
	*    we are only interested in monthly or half yearly        <018>
	*    payment frequencies                                     <018>
	*                                                            <018>
	*    IF CHDRLNB-BILLFREQ  = '12'                             <018>
	*        GO TO 1830-CHECK-MONTHLY-DATES.                     <018>
	*                                                            <018>
	*    IF CHDRLNB-BILLFREQ NOT = '02'                          <018>
	*        GO TO 1890-EXIT.                                    <018>
	*                                                            <018>
	*1820-CHECK-HALF-ANNUAL-DATES.                               <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 02) AND                        <018>
	*        (WSAA-PREM-DAY1    > 27) AND                        <018>
	*        (WSAA-PREM-DAY1    < 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 31) AND                        <018>
	*        (WSAA-PREM-MTH2    = 08))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 03) AND                        <011>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <011>
	*       ((WSAA-PREM-DAY2    = 30) AND                        <011>
	*        (WSAA-PREM-MTH2    = 08))                           <011>
	*        MOVE 1             TO WSAA-FACTOR.                  <011>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 03) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 30) AND                        <018>
	*        (WSAA-PREM-MTH2    = 09))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 04) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 31) AND                        <018>
	*        (WSAA-PREM-MTH2    = 10))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 05) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 30) AND                        <018>
	*        (WSAA-PREM-MTH2    = 11))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 06) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 31) AND                        <018>
	*        (WSAA-PREM-MTH2    = 12))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 08) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 28) AND                        <018>
	*        (WSAA-PREM-MTH2    = 02))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 09) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 31) AND                        <018>
	*        (WSAA-PREM-MTH2    = 03))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 10) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 30) AND                        <018>
	*        (WSAA-PREM-MTH2    = 04))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 11) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 31) AND                        <018>
	*        (WSAA-PREM-MTH2    = 05))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 12) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 30) AND                        <018>
	*        (WSAA-PREM-MTH2    = 06))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    GO TO 1890-EXIT.                                        <018>
	*                                                            <018>
	*                                                            <018>
	*                                                            <018>
	*                                                            <018>
	*1830-CHECK-MONTHLY-DATES.                                   <018>
	*                                                            <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 01) AND                        <018>
	*        (WSAA-PREM-DAY1    > 27)  AND                       <018>
	*        (WSAA-PREM-DAY1    < 31)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 28) AND                        <018>
	*        (WSAA-PREM-MTH2    = 02))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 03) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 30) AND                        <018>
	*        (WSAA-PREM-MTH2    = 04))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 05) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 30) AND                        <018>
	*        (WSAA-PREM-MTH2    = 06))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 08) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 30) AND                        <018>
	*        (WSAA-PREM-MTH2    = 09))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*    IF ((WSAA-PREM-MTH1    = 10) AND                        <018>
	*        (WSAA-PREM-DAY1    = 30)) AND                       <018>
	*       ((WSAA-PREM-DAY2    = 30) AND                        <018>
	*        (WSAA-PREM-MTH2    = 11))                           <018>
	*        MOVE 1             TO WSAA-FACTOR.                  <018>
	*                                                            <018>
	*1890-EXIT.                                                  <018>
	*     EXIT.                                                  <018>
	*                                                            <018>
	*                                                            <018>
	* </pre>
	*/
	protected void formatAgentName1900(){

	
		readAgent1910();

	}

	protected void readAgent1910(){

		
		if (isEQ(sv.agntsel,SPACES)) {
			sv.agntselErr.set(errorsInner.e186);
			return ;
		}
//ILIFE-8709 start
		aglfpf = aglfpfDAO.searchAglflnb( wsspcomn.company.toString(), sv.agntsel.toString());
		if (aglfpf == null) {
			sv.agntselErr.set(errorsInner.e305);
			return ;
		}
		
		if ((isNE(sv.occdate,ZERO))
		&& (isNE(sv.occdate,varcom.vrcmMaxDate))) {
			if (isLT(aglfpf.getDtetrm(),sv.occdate)
			|| isLT(aglfpf.getDteexp(),sv.occdate)
			|| isGT(aglfpf.getDteapp(),sv.occdate)) {
				sv.agntselErr.set(errorsInner.e475);
			}
		}
		if (isNE(aglfpf.getAgntbr(),sv.cntbranch)) {
			sv.agntselErr.set(errorsInner.e455);
		}
		clts.setClntpfx("CN");
		clts.setClntcoy(wsspcomn.fsuco.toString());
		clts.setClntnum(aglfpf.getClntnum());
		clts = clntpfDAO.selectActiveClient(clts);
		if(clts==null) {
			syserrrec.params.set(aglfpf.getClntnum());
			fatalError600();
//ILIFE-8709 end
		}
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}

	protected void loadPayerDetails1a00(Payrpf payrpf){

		loadPayer1a10(payrpf);
	}

	protected void loadPayer1a10(Payrpf payrpf){

		
		/* Read the client role file to get the payer number.              */
//ILIFE-8709 start
		
		clrfpf = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrlnbIO.getChdrpfx().toString(), payrpf.getChdrcoy(),payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1)), "PY");
		if (clrfpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(payrpf.getChdrcoy().concat(payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1))));
			fatalError600();
		}
		/* Load the working storage table using the payer sequence         */
		/* number as the subscript.                                        */
		wsbbSub.set(payrpf.getPayrseqno());
		wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(payrpf.getIncomeSeqNo());
		wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(payrpf.getBillfreq());
		wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].set(payrpf.getBtdate());
		wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(clrfpf.getClntnum());
		wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(clrfpf.getClntcoy());
//ILIFE-8709 end
	}

	protected void checkSuspense1aa00(){

	
		locateSuspense1aa00();
	}

	protected void locateSuspense1aa00(){

		
		/*    Read the Suspense file to see if any money has been          */
		/*    received for this contract. The search order for Suspense    */
		/*    details is Contract Currency; Billing Currency; Any          */
		/*    Currency. If Suspense is found set appropriate values.       */
		/*    But only if the amount is not zero.                          */
	//ILIFE-8709 start	
			if (isEQ(wsaaSub,1)) {
				acblpf.setOrigcurr(chdrlnbIO.getCntcurr().toString());
			}
			else {
				if (isEQ(wsaaSub,2)) {
					acblpf.setOrigcurr(chdrlnbIO.getBillcurr().toString());
				}
				else {
					acblpf.setOrigcurr(SPACES.stringValue());
				}
			}
			List<Acblpf> acbl;
			if(onePflag) {
				 acbl = acblDao.searchAcblenqRecord(chdrlnbIO.chdrcoy.toString(), chdrlnbIO.getChdrnum().toString());
				 wsaaSuspInd = "Y";
				 for(Acblpf acblpfobj : acbl ) {
					 if (isEQ(t3695rec.sign, "-")) {
							compute(wsaaCntSuspense, 2).set(mult(acblpfobj.getSacscurbal(), -1));
						}
						else {
							wsaaCntSuspense.set(acblpfobj.getSacscurbal());
						}			
						compute(wsaaTotalSuspense, 2).set(add(wsaaTotalSuspense, wsaaCntSuspense));
				 }
			}
			else {
				 acbl=acblDao.selectAcblData(acblpf);
			
				if(!(acbl.isEmpty())){
					acblpf=acbl.get(0);	
				if(acblpf.getSacscurbal().floatValue()!=0) {
					wsaaSuspInd = "Y";
					if (isEQ(t3695rec.sign,"-")) {
						compute(wsaaCntSuspense, 2).set(mult(acblpf.getSacscurbal(),-1));
					}
					else {
						wsaaCntSuspense.set(acblpf.getSacscurbal());
					}
					wsaaTotalSuspense.set(wsaaCntSuspense);
					if (isNE(acblpf.getOrigcurr(),sv.cntcurr)) {
						sv.hcurrcd.set(acblpf.getOrigcurr());
						sv.hcurrcdOut[Varcom.nd.toInt()].set("N");
					}
				}
			}
		}
//ILIFE-8709 end
	}

	protected void checkApa1ab00(){

	
		linkup1ab10();
	}

	protected void linkup1ab10(){

		
		initialize(rlpdlonrec.rec);
		rlpdlonrec.function.set(Varcom.info);
		rlpdlonrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		rlpdlonrec.chdrnum.set(chdrlnbIO.getChdrnum());
		rlpdlonrec.prmdepst.set(ZERO);
		rlpdlonrec.language.set(wsspcomn.language);
		callProgram(Rlpdlon.class, rlpdlonrec.rec);
		if (isNE(rlpdlonrec.statuz,Varcom.oK)) {
			syserrrec.statuz.set(rlpdlonrec.statuz);
			syserrrec.params.set(rlpdlonrec.rec);
			fatalError600();
		}
	}

	protected void adjustPremium1b00(){

		
		adjustPremium1b10();
		checkSuspense1b50();
	}

	protected void adjustPremium1b10(){

		
		/*    Get the frequency Factor from DATCON3.                       */
		if (isNE(wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()], ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()]);
			datcon3rec.frequency.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,Varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				fatalError600();
			}
			/*    If the risk commencment is 28, 29, 30, of january,           */
			/*    30 march, 30 may, 30 august or the 30 of october             */
			/*    the initial instalment required is incorrectly               */
			/*    calculated as 1 month + 1 day * premium instead of           */
			/*    1 month(frequency 12)                                        */
			/*    MOVE CHDRLNB-OCCDATE            TO WSAA-PREM-DATE1      <018>*/
			/*    MOVE CHDRLNB-BILLCD             TO WSAA-PREM-DATE2      <018>*/
			/*    PERFORM 1800-CHECK-DATE-FREQ                            <018>*/
			/* Calculate the instalment premium.                               */
			wsaaFactor.set(datcon3rec.freqFactor);
			compute(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()], 5).set(mult(wsaaBillingInformationInner.wsaaRegprem[wsbbSub.toInt()], wsaaFactor));
		}
		/* Add in the single premium.                                      */
		wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(wsaaBillingInformationInner.wsaaSingp[wsbbSub.toInt()]);
		/* If the tax relief method is not spaces calculate the tax        */
		/* relief amount and deduct it from the premium.                   */
		if (isNE(t5688rec.taxrelmth,SPACES)) {
			prasrec.clntnum.set(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()]);
			prasrec.clntcoy.set(wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()]);
			prasrec.incomeSeqNo.set(wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()]);
			prasrec.cnttype.set(chdrlnbIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			/* Use the due date unless a receipt exists with a date later      */
			/* then the due date.                                              */
			if (isEQ(rtrnsacIO.getEffdate(),varcom.vrcmMaxDate)) {
				prasrec.effdate.set(chdrlnbIO.getOccdate());
			}
			else {
				if (isGT(chdrlnbIO.getOccdate(),rtrnsacIO.getEffdate())) {
					prasrec.effdate.set(chdrlnbIO.getOccdate());
				}
				else {
					prasrec.effdate.set(rtrnsacIO.getEffdate());
				}
			}
			prasrec.company.set(chdrlnbIO.getChdrcoy());
			prasrec.grossprem.set(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
			prasrec.statuz.set(Varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz,Varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				fatalError600();
			}
			zrdecplcPojo.setAmountIn(prasrec.taxrelamt.getbigdata());//ILIFE-8709
			callRounding8000();
			wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].subtract(zrdecplcPojo.getAmountOut().doubleValue());//ILIFE-8709
			/*****     SUBTRACT PRAS-TAXRELAMT     FROM WSAA-INSTPRM(WSBB-SUB)  */
		}
		wsaaTotalPremium.add(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
		/* Add the contract fee to the instalment premium for              */
		/* payer No. 1.                                                    */
		/* IF SINGLE PREMIUM IS APPLIED CHECK IF ANY FEE IS NEEDED         */
		singPremFeeCustomerSpecific();
		compute(wsaaBillingInformationInner.wsaaPremTax[wsbbSub.toInt()], 6).setRounded(add((mult(wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()], wsaaFactor)), wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()]));
		compute(wsaaTotPremTax, 3).setRounded(add(wsaaTotPremTax, wsaaBillingInformationInner.wsaaPremTax[wsbbSub.toInt()]));
		setFeeVal01CustomerSpecific();
		sv.taxamt.set(wsaaTotalTax);
		if (isGT(sv.taxamt, ZERO)) {
			sv.taxamtOut[Varcom.pr.toInt()].set("N");
			sv.taxamtOut[Varcom.nd.toInt()].set("N");
		}
	}
	protected void singPremFeeCustomerSpecific() {
	if (isEQ(wsaaSingPrmInd,"Y")
			&& isNE(t5688rec.feemeth,SPACES)) {
				singPremFee1c00();
			}
	}
	protected void 	setFeeVal01CustomerSpecific(){
	if (isEQ(wsbbSub,1)) {
		if (isEQ(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()], ZERO)) {
			sv.cntfee.set(ZERO);
		}
		else {
			compute(sv.cntfee, 5).set(mult(sv.cntfee,wsaaFactor));
		}
		sv.cntfee.add(wsaaSingpFee);
		wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()].add(sv.cntfee);
		if (isNE(wsaaSingpFee, ZERO)) {
			wsaaCntfee.set(wsaaSingpFee);
			checkCalcContTax7100();
			wsaaSingpfeeTax.set(wsaaTax);
		}
		compute(wsaaCntfeeTax, 6).setRounded(mult(wsaaCntfeeTax, wsaaFactor));
		compute(wsaaTotalTax, 3).setRounded(add(add(wsaaTotPremTax, wsaaSingpfeeTax), wsaaCntfeeTax));
	}
	
	}
	
	
	protected void checkSuspense1b50(){

		
		/*  Check if there is enough money in suspense to issue            */
		/*  this contract.                                                 */
		/* Not to be included until phase B ***************        */
		/* Get the payer suspense.                                         */
		/*   MOVE SPACES                 TO SACSLNB-PARAMS.               */
		/*   MOVE WSSP-COMPANY           TO SACSLNB-CHDRCOY.              */
		/*   MOVE CHDRLNB-CHDRNUM        TO WSAA-CHDRNUM.                 */
		/*   MOVE WSBB-SUB               TO WSAA-PAYRSEQNO.               */
		/*   MOVE WSAA-PAYRKEY           TO SACSLNB-CHDRNUM.              */
		/*   MOVE CHDRLNB-CNTCURR        TO SACSLNB-CNTCURR.              */
		/*   MOVE T5645-SACSCODE-01      TO SACSLNB-SACSCODE.             */
		/*   MOVE T5645-SACSTYPE-01      TO SACSLNB-SACSTYP.              */
		/*   MOVE READR                  TO SACSLNB-FUNCTION.             */
		/*   CALL 'SACSLNBIO'            USING SACSLNB-PARAMS.            */
		/*   IF SACSLNB-STATUZ           NOT = O-K                        */
		/*                           AND NOT = MRNF                       */
		/*      MOVE SACSLNB-PARAMS      TO SYSR-PARAMS                   */
		/*      PERFORM 600-FATAL-ERROR.                                  */
		/*   IF SACSLNB-STATUZ           = MRNF                           */
		/*      MOVE ZERO                TO WSAA-PAYR-SUSPENSE            */
		/*   ELSE                                                         */
		/*      IF T3695-SIGN            = '-'                            */
		/*         MULTIPLY SACSLNB-SACSCURBAL BY -1                      */
		/*                               GIVING WSAA-PAYR-SUSPENSE        */
		/*      ELSE                                                      */
		/*         MOVE SACSLNB-SACSCURBAL TO WSAA-PAYR-SUSPENSE.         */
		/*      ADD WSAA-PAYR-SUSPENSE   TO WSAA-TOTAL-SUSPENSE.          */
		wsaaPayrSuspense.set(ZERO);
		/* Look up tolerance applicable                                    */
		/* Must convert premium into Suspense currency, if Suspense        */
		/* different from Contract currency, so that tolerance levels      */
		/* are calculated correctly against the Suspense amount.           */
		wsaaToleranceApp.set(ZERO);
		wsaaAmountLimit.set(ZERO);
		wsaaToleranceApp2.set(ZERO);
		wsaaAmountLimit2.set(ZERO);
		if (isEQ(wsaaSuspInd,"N")) {
			wsaaAmntDue.set(ZERO);
		}
		else {
			if (isEQ(chdrlnbIO.getCntcurr(),acblpf.getOrigcurr())) {
				wsaaAmntDue.set(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
			}
			else {
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountOut.set(ZERO);
				conlinkrec.rateUsed.set(ZERO);
				if(contDtCalcFlag) {//ILJ-44
				conlinkrec.cashdate.set(hpadPf.getHpropdte());
				}else {
				conlinkrec.cashdate.set(hpadPf.getHpropdte());//ILIFE-8709
				}
				conlinkrec.currIn.set(chdrlnbIO.getCntcurr());
				conlinkrec.currOut.set(acblpf.getOrigcurr());//ILIFE-8709
				conlinkrec.amountIn.set(wsaaBillingInformationInner.wsaaInstprm[wsbbSub.toInt()]);
				conlinkrec.company.set(chdrlnbIO.getChdrcoy());
				conlinkrec.function.set("SURR");
				callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
				if (isNE(conlinkrec.statuz,Varcom.oK)) {
					syserrrec.params.set(conlinkrec.clnk002Rec);
					syserrrec.statuz.set(conlinkrec.statuz);
					fatalError600();
				}
				wsaaAmntDue.set(conlinkrec.amountOut);
			}
		}
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub,11))) {
			if (isEQ(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()], t5667rec.freq[wsaaSub.toInt()])) {
				wsaaToleranceApp.set(t5667rec.prmtol[wsaaSub.toInt()]);
				wsaaToleranceApp2.set(t5667rec.prmtoln[wsaaSub.toInt()]);
				wsaaAmountLimit.set(t5667rec.maxAmount[wsaaSub.toInt()]);
				wsaaAmountLimit2.set(t5667rec.maxamt[wsaaSub.toInt()]);
				wsaaSub.set(11);
			}
			wsaaSub.add(1);
		}
		
		/* Calculate the tolerance applicable.                             */
		compute(wsaaCalcTolerance, 2).set(div((mult(wsaaToleranceApp,wsaaAmntDue)),100));
		zrdecplcPojo.setAmountIn(wsaaCalcTolerance.getbigdata());//ILIFE-8709
		callRounding8000();
		wsaaCalcTolerance.set(zrdecplcPojo.getAmountOut());//ILIFE-8709
		/*    Check % amount is less than Limit on T5667, if so use this   */
		/*    else use Limit.                                              */
		/*    Check for 1st premium shortfall tolerance limit.             */
		if (isLT(wsaaCalcTolerance,wsaaAmountLimit)) {
			wsaaTolerance.set(wsaaCalcTolerance);
		}
		else {
			wsaaTolerance.set(wsaaAmountLimit);
		}
		wsaaTotTolerance.add(wsaaTolerance);
		/*    Check for 2nd premium shortfall tolerance limit.             */
		compute(wsaaCalcTolerance2, 2).set(div((mult(wsaaToleranceApp2,wsaaAmntDue)),100));
		zrdecplcPojo.setAmountIn(wsaaCalcTolerance2.getbigdata());//ILIFE-8709
		callRounding8000();
		wsaaCalcTolerance2.set(zrdecplcPojo.getAmountOut());//ILIFE-8709
		if (isLT(wsaaCalcTolerance2,wsaaAmountLimit2)) {
			wsaaTolerance2.set(wsaaCalcTolerance2);
		}
		else {
			wsaaTolerance2.set(wsaaAmountLimit2);
		}
		wsaaTotTolerance2.add(wsaaTolerance2);
		/* If there is not enough money in the payer suspense account      */
		/* plus tolerance to cover the premium due check to see if         */
		/* there is enough money in contract suspense to cover the         */
		/* remainder (or all if payr suspense was zero) of the             */
		/* premium due.                                                    */
		/* If the remainder of the premium amount due is greater then      */
		/* the contract suspense plus the tolerance display an error       */
		/* message.                                                        */
		/* If there is enough money in  contract suspense reduce the       */
		/* contract suspense amount by the remainder of the premium due.   */
		/* MOVE WSAA-INSTPRM(WSBB-SUB) TO WSAA-AMNT-DUE.           <012>*/
		if ((setPrecision(wsaaAmntDue, 2)
		&& isGT(wsaaAmntDue,add(wsaaPayrSuspense,wsaaTolerance)))) {
			wsaaAmntDue.subtract(wsaaPayrSuspense);
			if ((setPrecision(wsaaAmntDue, 2)
			&& isGT(wsaaAmntDue,add(wsaaCntSuspense,wsaaTolerance)))) {
				/* Nothing to do. */
			}
			else {
				wsaaCntSuspense.subtract(wsaaAmntDue);
			}
		}
		wsbbSub.add(1);
	}

	protected void singPremFee1c00(){

		
		if (isEQ(t5674rec.commsubr,SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set("00");
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(wsspcomn.company);
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,Varcom.oK)
		&& isNE(mgfeelrec.statuz,Varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			fatalError600();
		}
		setFeeVal02CustomerSpecific();
		callRounding8000();
		wsaaSingpFee.set(zrdecplcPojo.getAmountOut());//ILIFE-8709
	}

	protected void setFeeVal02CustomerSpecific() {

		zrdecplcPojo.setAmountIn(mgfeelrec.mgfee.getbigdata());//ILIFE-8709

	}	
	protected void largename(){

		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(clts.getSurname());
		/*LGNM-EXIT*/
	}

	protected void plainname(){

	
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(clts.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clts.getSurname());
		}
		/*PLAIN-EXIT*/
	}

	protected void payeename(){

		
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clts.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(clts.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clts.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(clts.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(clts.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(clts.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(clts.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(clts.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

	protected void corpname(){

		
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clts.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clts.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}
	
	
	
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
	protected void preScreenEdit(){

		
		/*PRE-START*/
		/* If returning from an optional selection skip this section.      */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(Varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000(){
		
		/*    CALL 'S5074IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5074-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(scrnparams.statuz,Varcom.calc)) {
			wsaaReassuranceChecked = "Y";
			return ;
		}
		if (isNE(wsaaReassuranceChecked,"Y")
		&& reassuranceReqd.isTrue()) {
			sv.chdrnumErr.set(errorsInner.g817);
		}
		if (isNE(chdrlnbIO.getAvlisu(),"Y")) {
			/*     MOVE U018 TO S5074-CHDRNUM-ERR.                           */
			sv.chdrnumErr.set(errorsInner.g817);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}


	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
	protected void update3000(){

		
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					updateDatabase3010();
				case checkBatcAgain3080: 
					checkBatcAgain3080();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void updateDatabase3010(){

		
		/* if returning from an optional selection skip this section*/
		/* or F9 (CALC) wsa pressed.*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")
		|| isEQ(scrnparams.statuz,Varcom.calc)) {
			goTo(GotoLabel.exit3090);
		}
		if(contDtCalcFlag && isNE(wsaaOldRiskDate,sv.rskcommdate)) {	
			
			hpadpfDAO.updateRiskCommDate(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(),sv.rskcommdate.toInt());
		}
		/*  Move fields from the screen to the contract header.*/
		chdrlnbIO.setCownpfx("CN");
		chdrlnbIO.setCowncoy(wsspcomn.fsuco);
		chdrlnbIO.setPolinc(sv.polinc);
		chdrlnbIO.setCownnum(sv.ownersel);
		chdrlnbIO.setOccdate(sv.occdate);
		chdrlnbIO.setBillfreq(sv.billfreq);
		chdrlnbIO.setBillchnl(sv.mop);
		chdrlnbIO.setCollchnl(sv.mop);
		chdrlnbIO.setBillcd(sv.billcd);
		chdrlnbIO.setCntcurr(sv.cntcurr);
		chdrlnbIO.setBillcurr(sv.billcurr);
		chdrlnbIO.setRegister(sv.register);
		chdrlnbIO.setSrcebus(sv.srcebus);
		chdrlnbIO.setReptype(sv.reptype);
		chdrlnbIO.setRepnum(sv.lrepnum);
		chdrlnbIO.setCntbranch(sv.cntbranch);
		chdrlnbIO.setAgntpfx("AG");
		chdrlnbIO.setAgntcoy(wsspcomn.company);
		chdrlnbIO.setAgntnum(sv.agntsel);
		chdrlnbIO.setCampaign(sv.campaign);
		chdrlnbIO.setChdrstcda(sv.stcal);
		chdrlnbIO.setChdrstcdb(sv.stcbl);
		chdrlnbIO.setChdrstcdc(sv.stccl);
		chdrlnbIO.setChdrstcdd(sv.stcdl);
		chdrlnbIO.setChdrstcde(sv.stcel);
		/*    Calling CHDRLNBIO with 'KEEPS' so that CHRDPF gets           */
		/*    updated                                                      */
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFunction("WRITS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*  transfer the contract soft lock to AT
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if ((isNE(sftlockrec.statuz,Varcom.oK))
		&& (isNE(sftlockrec.statuz,"LOCK"))) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3090);
		}*/
		
		atmodrec.atmodRec.set(SPACES);
		atmodrec.company.set(wsspcomn.company);
		atmodrec.language.set(wsspcomn.language);
		atmodrec.batchKey.set(wsspcomn.batchkey);
		//atmodrec.reqProg.set(wsaaProg);
		//atmodrec.reqUser.set(varcom.vrcmUser);
		//atreqrec.reqTerm.set(varcom.vrcmTerm);
		//atreqrec.reqDate.set(wsaaToday);
		//atreqrec.reqTime.set(varcom.vrcmTime);
		//atreqrec.language.set(wsspcomn.language);
		wsaaPrimaryChdrnum.set(chdrlnbIO.getChdrnum());
		atmodrec.primaryKey.set(wsaaPrimaryKey);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTerm);
		/* COMPUTE WSAA-TOTAMNT  =  S5074-TRANAMT-1 -           <D96NUM>*/
		/*    COMPUTE WSAA-TOTAMNT  =  S5074-TOTLPRM  -            <V4L001>*/
		/*                             S5074-TOLERANCE.                    */
		compute(wsaaTotamnt1, 2).set(sub(sub(sv.totlprm,sv.cntsusp),sv.tolerance));
		compute(wsaaTotamnt, 2).set(sub(sv.totlprm,sv.tolerance));
		atmodrec.transArea.set(wsaaTransactionRec);
	}

	protected void checkBatcAgain3080(){

	
		batcIO.setDataKey(wsspcomn.batchkey);
		batcIO.setFunction("READH");
		SmartFileCode.execute(appVars, batcIO);
		if (isNE(batcIO.getStatuz(),Varcom.oK)
		&& isNE(batcIO.getStatuz(),"HELD")) {
			syserrrec.params.set(batcIO.getParams());
			syserrrec.statuz.set(batcIO.getStatuz());
			fatalError600();
		}
		if (isEQ(batcIO.getStatuz(),"HELD")) {
			if (isLT(wsaaBatclckCount,20)) {
				wsaaBatclckCount.add(1);
				callProgram(Sflockwait.class);
				goTo(GotoLabel.checkBatcAgain3080);
			}
			else {
				syserrrec.params.set(batcIO.getParams());
				syserrrec.statuz.set(errorsInner.e241);
				fatalError600();
			}
		}
		if (isEQ(batcIO.getStatuz(),Varcom.oK)) {
			batcIO.setFunction("RLSE");
			SmartFileCode.execute(appVars, batcIO);
			if (isNE(batcIO.getStatuz(),Varcom.oK)) {
				syserrrec.params.set(batcIO.getParams());
				syserrrec.statuz.set(batcIO.getStatuz());
				fatalError600();
			}
		}
		atmodrec.statuz.set(Varcom.oK);
		callProgram(P5074at.class, atmodrec.atmodRec);
		if (isNE(atmodrec.statuz,Varcom.oK)) {
			syserrrec.params.set(atmodrec.atmodRec);
			callProgram(Syserr.class, syserrrec.syserrRec);
			//atmodrec.acctPerd.set(SPACES);
			atmodrec.transArea.set(SPACES);
			syserrrec.params.set(atmodrec.atmodRec);
			fatalError600();
		}
	}

	protected void whereNext4000(){

		
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					nextProgram4010();
				case calc4020: 
					calc4020();
				case exit4090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void nextProgram4010(){

		
		wsspcomn.nextprog.set(wsaaProg);
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.calc4020);
		}
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			restoreProgram4200();
		}
		chdrlnbIO.setFunction(Varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),Varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getAvlisu(),"Y")) {
			sv.chdrnumErr.set(errorsInner.f785);
			wsspcomn.edterror.set("Y");
		}
		/* Check the premium amount.*/
		/* IF S5074-TRANAMT-1          > S5074-TOTAMNT          <D96NUM>*/
		if (isGT(sv.totlprm,sv.totamnt)
		&& isEQ(sv.hcurrcdOut[Varcom.nd.toInt()],"Y")) {
			sv.chdrnumErr.set(errorsInner.f785);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
	}

	/**
	* <pre>
	*  if calc was pressed use secondary switching to pick up
	*      the programs required
	* </pre>
	*/
	
	protected void calc4020(){

		
		if (isNE(scrnparams.statuz,"CALC")) {
			wsspcomn.programPtr.add(1);
			return ;
		}
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
		wsaaKill.set("Y");
		wsspcomn.nextprog.set(wsaaProg);
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			saveProgram4100();
		}
		gensswrec.function.set("C");
		callGenssw4300();
	}

	protected void saveProgram4100(){

		
		/*SAVE*/
		wsaaSecProg[wsaaY.toInt()].set(wsspcomn.secProg[wsaaX.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

	protected void restoreProgram4200(){

		
		/*RESTORE*/
		wsspcomn.secProg[wsaaX.toInt()].set(wsaaSecProg[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

	protected void callGenssw4300(){

		
		callSubroutine4310();

	}

	protected void callSubroutine4310(){

		
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz,Varcom.oK)
		&& isNE(gensswrec.statuz,Varcom.mrnf)) {
			syserrrec.params.set(gensswrec.gensswRec);
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz,Varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*     MOVE V045                TO SCRN-ERROR-CODE               */
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			return ;
		}
		/*    load from gensw to wssp*/
		compute(wsaaX, 0).set(add(1,wsspcomn.programPtr));
		wsaaY.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			loadProgram4400();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	protected void loadProgram4400(){

		
		/*RESTORE1*/
		wsspcomn.secProg[wsaaX.toInt()].set(gensswrec.progOut[wsaaY.toInt()]);
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT1*/
	}

	protected void checkCalcCompTax7000(Covtpf covtpf){

		start7010(covtpf);
	}

	protected void start7010(Covtpf covtpf){

		
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		if (isEQ(covtpf.getSingp(), ZERO)
		&& isEQ(covtpf.getInstprem(), ZERO)) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtpf.getCrtable());
		readTr52e7200();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		/* Call TR52D tax subroutine                                       */
		if (isNE(tr52erec.taxind01, "Y")) {
			return ;
		}
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(covtpf.getLife());
		txcalcrec.coverage.set(covtpf.getCoverage());
		txcalcrec.rider.set(covtpf.getRider());
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.crtable.set(covtpf.getCrtable());
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(chdrlnbIO.getOccdate());
		txcalcrec.transType.set("PREM");
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		if (isNE(covtpf.getInstprem(), ZERO)) {
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(covtpf.getZbinstprem());
			}
			else {
				txcalcrec.amountIn.set(covtpf.getInstprem());
			}
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, Varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[2]);
				}
			}
		}
		if (isNE(covtpf.getSingp(), ZERO)) {
			txcalcrec.amountIn.set(covtpf.getSingp());
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, Varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[2]);
				}
			}
		}
	}

	protected void checkCalcContTax7100(){

		
		start7110();
	}

	protected void start7110(){

		
		wsaaTax.set(0);
		if (isEQ(mgfeelrec.mgfee, ZERO)) {
			return ;
		}
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		readTr52e7200();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e7200();
		}
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		//IBPLIFE-4262
		List<Covtpf> covtpfList = covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		Covtpf covtpf=covtpfList.get(0);
		
		/* Call TR52D tax subroutine                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.crtable.set(covtpf.getCrtable()); //IBPLIFE-4262
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.effdate.set(chdrlnbIO.getOccdate());
		txcalcrec.transType.set("CNTF");
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.amountIn.set(wsaaCntfee);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

	protected void readTr52e7200(){
		//ILIFE-4492
		String[] itdmTable = {tr52e};
		String[] itdmItem = {wsaaTr52eKey.toString()};
		
		itdmpfList = itemDAO.getIdtmItems(itdmTable, itdmItem, wsspcomn.company.toString(), chdrlnbIO.getOccdate().toInt());
		
		if(itdmpfList==null || itdmpfList.size() == 0){
			if(isEQ(subString(wsaaTr52eKey, 2, 7), "*******")){
				syserrrec.params.set(wsaaTr52eKey);
				syserrrec.statuz.set("MRNF");
				fatalError600();
			}
		}else{
			tr52erec.tr52eRec.set(StringUtil.rawToString(itdmpfList.get(0).getGenarea()));
		}
		
	}

	protected void callRounding8000(){

		
		/*CALL*/
//ILIFE-8709 start
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setStatuz(Varcom.oK.toString());
		zrdecplcPojo.setCompany(wsspcomn.company.toString());
		zrdecplcPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			fatalError600();
//ILIFE-8709 end
		}
	}
	/*
	 * Class transformed  from Data Structure WSAA-BILLING-INFORMATION--INNER
	 */
	public static final class WsaaBillingInformationInner { 

	
			/* WSAA-BILLING-INFORMATION */
		private FixedLengthStringData[] wsaaBillingDetails = FLSInittedArray (9, 116);
		private ZonedDecimalData[] wsaaIncomeSeqNo = ZDArrayPartOfArrayStructure(2, 0, wsaaBillingDetails, 0, UNSIGNED_TRUE);
		private FixedLengthStringData[] wsaaBillfreq = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 2);
		private FixedLengthStringData[] wsaaBillchnl = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 4);
		private PackedDecimalData[] wsaaBillcd = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 6);
		private PackedDecimalData[] wsaaBtdate = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 11);
		private FixedLengthStringData[] wsaaBillcurr = FLSDArrayPartOfArrayStructure(3, wsaaBillingDetails, 16);
		private FixedLengthStringData[] wsaaClntcoy = FLSDArrayPartOfArrayStructure(1, wsaaBillingDetails, 19);
		private FixedLengthStringData[] wsaaClntnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 20);
		private FixedLengthStringData[] wsaaMandref = FLSDArrayPartOfArrayStructure(5, wsaaBillingDetails, 28);
		private FixedLengthStringData[] wsaaGrupkey = FLSDArrayPartOfArrayStructure(12, wsaaBillingDetails, 33);
		private PackedDecimalData[] wsaaRegprem = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 45);
		private PackedDecimalData[] wsaaSingp = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 54);
		private PackedDecimalData[] wsaaSpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 63);
		private PackedDecimalData[] wsaaRpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 72);
		public PackedDecimalData[] wsaaInstprm = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 81);
		private PackedDecimalData[] wsaaPremTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 90);
		private PackedDecimalData[] wsaaTaxrelamt = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 99);
		private FixedLengthStringData[] wsaaInrevnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 108);
	}
	
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
		private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
		private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
		private FixedLengthStringData e305 = new FixedLengthStringData(4).init("E305");
		private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
		private FixedLengthStringData e475 = new FixedLengthStringData(4).init("E475");
		private FixedLengthStringData f290 = new FixedLengthStringData(4).init("F290");
		private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
		private FixedLengthStringData f151 = new FixedLengthStringData(4).init("F151");
		private FixedLengthStringData f785 = new FixedLengthStringData(4).init("F785");
		private FixedLengthStringData f919 = new FixedLengthStringData(4).init("F919");
		private FixedLengthStringData g228 = new FixedLengthStringData(4).init("G228");
		private FixedLengthStringData g816 = new FixedLengthStringData(4).init("G816");
		private FixedLengthStringData g817 = new FixedLengthStringData(4).init("G817");
		private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
		private FixedLengthStringData e241 = new FixedLengthStringData(4).init("E241");
	}

	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner { 

		private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
		private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");


	}

	public ChdrlnbTableDAM getChdrlnbIO() {
		return chdrlnbIO;
	}

	public void setChdrlnbIO(ChdrlnbTableDAM chdrlnbIO) {
		this.chdrlnbIO = chdrlnbIO;
	}
	public Mgfeelrec getMgfeelrec() {
		return mgfeelrec;
	}

	public void setMgfeelrec(Mgfeelrec mgfeelrec) {
		this.mgfeelrec = mgfeelrec;
	}
	protected void calcRskCommDate()
	{	
		ZonedDecimalData wsaaOccDate = new ZonedDecimalData(8, 0).setUnsigned();
		wsaaOccDate.set(chdrlnbIO.getOccdate());
		FirstRcpPremRec firstRcpPremRec = new FirstRcpPremRec();
		firstRcpPremRec.setChdrlnbIO(getChdrlnbIO());
		firstRcpPremRec.setBatcBatctrcde(wsaaBatckey.batcBatctrcde);	    	
		firstRcpPremRec.setOccdate(wsaaOccDate);
		int firstpremdate;

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy("2");
		itempf.setItemtabl(t3615);
		itempf.setItemitem(chdrlnbIO.getSrcebus().toString());
		itempf.setValidflag("1");
		itempf = itemDAO.getItemRecordByItemkey(itempf);			
		if (itempf != null) {
			t3615rec.t3615Rec.set(StringUtil.rawToString(itempf.getGenarea()));			
		}
		
		firstRcpPremRec.setOnePCashlessFlag(t3615rec.onepcashless);
		
		if(onePflag) {
			
			if(isEQ(chdrlnbIO.getBillchnl(),'D') && isEQ(t3615rec.onepcashless,'Y')) {
				Hpadpf hpadpf = hpadpfDAO.getHpadData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
				firstpremdate = hpadpf.getRskcommdate();
			}
			else{
				callProgram(FirstRcpPrem.class, firstRcpPremRec);
				firstpremdate = firstRcpPremRec.getFirstpremdate();	
			}
		}
		else {
			callProgram(FirstRcpPrem.class, firstRcpPremRec);
			firstpremdate = firstRcpPremRec.getFirstpremdate();	
		}
		if(firstpremdate != 0){
			sv.premRcptDate.set(firstpremdate);
		}
		
		if(onePflag && isEQ(chdrlnbIO.getBillchnl(),'D') && isNE(t3615rec.onepcashless,'Y') 
				&& isNE(chdrlnbIO.rcopt,'Y')) {
				
			if (isGT(hpadPf.getHpropdte(), hpadPf.getDecldate())) {
				if(isGT(hpadPf.getHpropdte(), firstpremdate)){
					sv.rskcommdate.set(hpadPf.getHpropdte());
				}else{
					sv.rskcommdate.set(firstpremdate);
				}
			}else{
				if(isGT(hpadPf.getDecldate(), firstpremdate)){
					sv.rskcommdate.set(hpadPf.getDecldate());
				}else{
					sv.rskcommdate.set(firstpremdate);
				}
			}
		}
		wsaaNewRiskDate.set(sv.rskcommdate);
		wsspcomn.effdate.set(sv.rskcommdate);
	}
}

