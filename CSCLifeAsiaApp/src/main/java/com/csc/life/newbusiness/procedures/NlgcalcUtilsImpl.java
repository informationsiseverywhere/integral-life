package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;

import org.springframework.context.ApplicationContext;

import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Datcon3Pojo;
import com.csc.fsu.general.procedures.Datcon3Utils;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescpfDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;

public class NlgcalcUtilsImpl implements NlgcalcUtils {

	private static final String t1688 = "T1688";

	private Syserrrec syserrrec = new Syserrrec();
	private String wsaaProposal;
	private String wsaaTrandesc;
	private String wsaaValidflg;

	private Lifepf lifepf = new Lifepf();
	private Descpf descpf = new Descpf();
	private Nlgtpf nlgtpf;
	private Nlgtpf nlgtpf1;

	private NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private DescpfDAO descpfDAO = DAOFactory.getDescpfDAO();

	private Datcon1rec datcon1rec = new Datcon1rec();

	private Datcon3Utils datcon3Utils = getApplicationContext().getBean("datcon3Utils", Datcon3Utils.class);
	private AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class);
	private String agecalcFuncFalg = "";

	public NlgcalcUtilsImpl() {
		super();
	}

	public void mainline(NlgcalcPojo nlgcalcPojo) {
		nlgcalcPojo.setStatus(Varcom.oK.toString());
		nlgcalcPojo.setCurrAge(0);
		nlgcalcPojo.setYrsInf(0);
		wsaaProposal = "N";
		nlgcalcPojo.setFsuco("9");
		callDatcon1(nlgcalcPojo);
		nlgtpf = new Nlgtpf();
		nlgtpf1 = new Nlgtpf();
		lifepf = lifepfDAO.getLifeRecord(nlgcalcPojo.getChdrcoy(), nlgcalcPojo.getChdrnum(), "01", "00");
		if (lifepf == null) {
			syserrrec.params.set(lifepf);
			fatalError600(nlgcalcPojo);
		}
		nlgcalcPojo.setCltdob(lifepf.getCltdob());
		if ("L".equals(nlgcalcPojo.getTransMode())) {
			nlgcalcPojo.setNlgFlag("N");
			updateNlgt3000(nlgcalcPojo);
			return;
		}
		wsaaValidflg = "1";
		nlgtpf = nlgtpfDAO.getNlgRecord(nlgcalcPojo.getChdrcoy(), nlgcalcPojo.getChdrnum(), wsaaValidflg);/* IJTI-1523 */
		if (nlgtpf == null) {
			wsaaProposal = "Y";
		}
		calcpolicyYr1100(nlgcalcPojo);
		calAttAge1200(nlgcalcPojo);
		process2000(nlgcalcPojo);
	}

	protected void callDatcon1(NlgcalcPojo nlgcalcPojo) {
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, Varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600(nlgcalcPojo);
		}
	}

	protected void calcpolicyYr1100(NlgcalcPojo nlgcalcPojo) {

		Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
		datcon3Pojo.setIntDate1(nlgcalcPojo.getOccdate() + "");
		datcon3Pojo.setIntDate2(datcon1rec.intDate.toString());
		datcon3Pojo.setFrequency("01");
		datcon3Utils.calDatcon3(datcon3Pojo);
		String statuz = datcon3Pojo.getStatuz();

		if (isNE(statuz, Varcom.oK)) {
			syserrrec.statuz.set(statuz);
			fatalError600(nlgcalcPojo);
		}
		nlgcalcPojo.setYrsInf(datcon3Pojo.getFreqFactor().intValue());
	}

	protected void calAttAge1200(NlgcalcPojo nlgcalcPojo) {

		if (nlgcalcPojo.getCltdob() > 0) {
			callAgecalc1210(nlgcalcPojo);
		}
	}

	protected void callAgecalc1210(NlgcalcPojo nlgcalcPojo) {

		AgecalcPojo agecalcPojo = new AgecalcPojo();
		agecalcPojo.setFunction("CALCP");
		agecalcFuncFalg = "CALCP";
		agecalcPojo.setLanguage(nlgcalcPojo.getLanguage());
		agecalcPojo.setCnttype(nlgcalcPojo.getCnttype());
		agecalcPojo.setIntDate1(nlgcalcPojo.getCltdob() + "");
		agecalcPojo.setIntDate2(datcon1rec.intDate.toString());
		agecalcPojo.setCompany(nlgcalcPojo.getFsuco());
		agecalcUtils.calcAge(agecalcPojo);
		if (isNE(agecalcPojo.getStatuz(), Varcom.oK)) {
			syserrrec.params.set(agecalcPojo.toString());
			syserrrec.statuz.set(agecalcPojo.getStatuz());
			fatalError600(nlgcalcPojo);
		}
		nlgcalcPojo.setCurrAge(agecalcPojo.getAgerating());
	}

	protected void process2000(NlgcalcPojo nlgcalcPojo) {

		nlgcalcPojo.setNlgFlag("Y");
		if (isEQ(wsaaProposal, "Y")) {
			updateNlgt3000(nlgcalcPojo);
			return;
		}
		nlgcalcPojo.setTotTopup(nlgtpf.getAmnt01());
		nlgcalcPojo.setTotWdrAmt(nlgtpf.getAmnt02());
		nlgcalcPojo.setOvduePrem(nlgtpf.getAmnt03());
		nlgcalcPojo.setUnpaidPrem(nlgtpf.getAmnt04());
		// wsaaTranno.set(nlgtpf.getTranno());

		if ("TOPUP".equals(nlgcalcPojo.getFunction())) {
			calcTopup2100(nlgcalcPojo);
		}
		if ("PSURR".equals(nlgcalcPojo.getFunction())) {
			Surramt2200(nlgcalcPojo);
		}
		if ("OVDUE".equals(nlgcalcPojo.getFunction())) {
			calcOverdue2300(nlgcalcPojo);
		}
		if ("PHRST".equals(nlgcalcPojo.getFunction())) {
			calcUnpaid2400(nlgcalcPojo);
		}
		if ("COLCT".equals(nlgcalcPojo.getFunction())) {
			setOverdueUnpaid(nlgcalcPojo);
		}
		if ("LINST".equals(nlgcalcPojo.getFunction())) {
			setOverdueUnpaid2500(nlgcalcPojo);
		}
		nlgcalcPojo.setNlgBalance(nlgcalcPojo.getTotTopup().add(nlgcalcPojo.getTotWdrAmt())
				.add(nlgcalcPojo.getOvduePrem()).add(nlgcalcPojo.getUnpaidPrem()));
		if (nlgcalcPojo.getNlgBalance().compareTo(BigDecimal.ZERO) < 0) {
			nlgcalcPojo.setNlgFlag("N");
		}
		if ("CHECK".equals(nlgcalcPojo.getFunction())) {
			return;
		}
		// wsaaTranno.add(1);
		// nlgcalcPojo.setTranno.set(wsaaTranno);
		updateNlgt3000(nlgcalcPojo);
	}

	protected void calcTopup2100(NlgcalcPojo nlgcalcPojo) {
		nlgcalcPojo.setTotTopup(nlgcalcPojo.getTotTopup().add(nlgcalcPojo.getInputAmt()));
	}

	protected void Surramt2200(NlgcalcPojo nlgcalcPojo) {
		nlgcalcPojo.setTotWdrAmt(nlgcalcPojo.getTotWdrAmt().add(nlgcalcPojo.getInputAmt()));
	}

	protected void calcOverdue2300(NlgcalcPojo nlgcalcPojo) {
		nlgcalcPojo.setOvduePrem(nlgcalcPojo.getOvduePrem().add(nlgcalcPojo.getInputAmt()));
	}

	protected void calcUnpaid2400(NlgcalcPojo nlgcalcPojo) {
		nlgcalcPojo.setUnpaidPrem(nlgcalcPojo.getUnpaidPrem().add(nlgcalcPojo.getInputAmt()));
	}

	protected void setOverdueUnpaid(NlgcalcPojo nlgcalcPojo) {
		nlgcalcPojo.setOvduePrem(BigDecimal.ZERO);
		nlgcalcPojo.setUnpaidPrem(BigDecimal.ZERO);
	}

	protected void setOverdueUnpaid2500(NlgcalcPojo nlgcalcPojo) {
		nlgcalcPojo.setOvduePrem(BigDecimal.ZERO);
		nlgcalcPojo.setUnpaidPrem(BigDecimal.ZERO);
	}

	protected void updateNlgt3000(NlgcalcPojo nlgcalcPojo) {

		if (nlgtpf1 == null)
			nlgtpf1 = new Nlgtpf();
		nlgtpf1.setChdrcoy(nlgcalcPojo.getChdrcoy());
		nlgtpf1.setChdrnum(nlgcalcPojo.getChdrnum());
		nlgtpf1.setTranno(nlgcalcPojo.getTranno());
		nlgtpf1.setEffdate(nlgcalcPojo.getEffdate());
		nlgtpf1.setBatcactyr(nlgcalcPojo.getBatcactyr());
		nlgtpf1.setBatcactmn(nlgcalcPojo.getBatcactmn());
		nlgtpf1.setBatctrcde(nlgcalcPojo.getBatctrcde());
		getTansDesc3100(nlgcalcPojo);
		nlgtpf1.setTrandesc(wsaaTrandesc);
		nlgtpf1.setTranamt(nlgcalcPojo.getInputAmt());
		nlgtpf1.setFromdate(nlgcalcPojo.getFrmdate());
		nlgtpf1.setTodate(nlgcalcPojo.getTodate());
		nlgtpf1.setNlgflag(nlgcalcPojo.getNlgFlag());
		nlgtpf1.setYrsinf(nlgcalcPojo.getYrsInf());
		nlgtpf1.setAge(nlgcalcPojo.getCurrAge());
		nlgtpf1.setNlgbal(nlgcalcPojo.getNlgBalance());
		nlgtpf1.setAmnt01(nlgcalcPojo.getTotTopup());
		nlgtpf1.setAmnt02(nlgcalcPojo.getTotWdrAmt());
		nlgtpf1.setAmnt03(nlgcalcPojo.getOvduePrem());
		nlgtpf1.setAmnt04(nlgcalcPojo.getUnpaidPrem());
		nlgtpf1.setValidflag("1");
		nlgtpfDAO.insertNlgtpf(nlgtpf1);
	}

	protected void getTansDesc3100(NlgcalcPojo nlgcalcPojo) {

		descpf = descpfDAO.getItemTblItemByLang("IT", t1688, nlgcalcPojo.getBatctrcde(), nlgcalcPojo.getLanguage(),
				nlgcalcPojo.getChdrcoy());
		if (descpf != null) {
			wsaaTrandesc = descpf.getLongdesc();
		}
	}

	protected void fatalError600(NlgcalcPojo nlgcalcPojo) {
		error6000();
		nlgcalcPojo.setStatus(Varcom.bomb.toString());
		/* EXIT */
		throw new COBOLExitProgramException();
	}

	protected void error6000() {
		if (isEQ(syserrrec.statuz, Varcom.bomb)) {
			return;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2") && isEQ(agecalcFuncFalg, "CALCP")) {
			syserrrec.syserrType.set("1");
		}
		if (isNE(syserrrec.syserrType, "4") && isEQ(agecalcFuncFalg, "CALCB")) {
			syserrrec.syserrType.set("3");
		}
		Syserr syserr = new Syserr();
		syserr.mainline(new Object[] { syserrrec.syserrRec });
	}

	private ApplicationContext getApplicationContext() {
		return IntegralApplicationContext.getApplicationContext();
	}
}
