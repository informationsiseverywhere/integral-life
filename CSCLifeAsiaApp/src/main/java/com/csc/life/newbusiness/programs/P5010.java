/*
 * File: P5010.java
 * Date: 29 August 2009 23:54:37
 * Author: Quipoz Limited
 * 
 * Class transformed from P5010.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTallyAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.AnsopfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Ansopf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Clntrlsrec;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.McrlTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Chdrkey;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.BnfylnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.BnfypfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnfypf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.procedures.Antisocl;
import com.csc.life.newbusiness.recordstructures.Antisoclkey;
import com.csc.life.newbusiness.screens.S5010ScreenVars;
import com.csc.life.newbusiness.tablestructures.NBProposalRec;
import com.csc.life.newbusiness.tablestructures.Tr52yrec;
import com.csc.life.newbusiness.tablestructures.Tr52zrec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5662rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*  Retrieve the contract header information from the CHDRLNB I/O
* module.
*  Look up the following:
*       - the contract type description from T5688.
*  Up  to  ten  beneficiaries  are  allowed per contract, and no
* more,  so  a  fixed  subfile size can be used. Initialise the
* subfile  with  blank records (INIT function), and then update
* any  records  with  existing details as required. Details are
*loaded from the beneficiaries file (BNFYLNB). For each record
* read:
*       - use the  next  available  relative  record  number  to
*       retrieve  a  blank subfile record (i.e. starting at
*       one).
*       - look up the  client name (CLTS), formatting the client
*       name for confirmation.
*       - hold the client number in a "hidden" field to indicate
*       that the record was  loaded  from the database (and
*       so is not new  -  required  during  validation  and
*       updating).
*       - hold  the  original percentage in a "hidden" field for
*       later validation.
*  Set  the  current percentage allocated to 0 if no beneficiary
* records existed, otherwise set it to 100.
* Validation
* ----------
*  If in enquiry  mode  (WSSP-FLAG  =  'I')  protect  the entire
*Screen prior to output.
*  If  the  'KILL'  function  key  was  pressed,  skip  all  the
* validation  except  that for 100%. (Otherwise, if a duplicate
*client  number  is  entered,  the database will not have 100%
*allocated and could be left in this invalid state.)
*  If in enquiry mode, skip all the validation.
*  Validate each changed  subfile  record according to the rules
* defined in the  screen and field help. Leave duplicate client
* number  validation  till  later.
*  Adjust   the   total  percentage  allocated  by  the  current
* percentage  less  the original  "hidden" percentage.  Set the
* "hidden"  percentage  to  the  new current figure.  Flag each
* record  validated  in  this  as  requiring updating on the
* database for use later.
*  If  there were no errors detected, check that the accumulated
* percentages is still exactly 100%.
* Updating
* --------
*  If the 'KILL' function  key  was  pressed,  or  if in enquiry
* mode, skip the updating only.
*  Add, modify or  delete  beneficiary  record  depending  on if
* details  have  been  keyed in, blanked out or changed. Ignore
* any  subfile  records  which were not validated (hidden field
* flag). Note, that if a  client  number  is  changed  (can  be
* worked out from the  hidden  field), the original record must
* be deleted and a new one added - client no is the key.
*  An  attempt  to  enter  the  same  client number twice can be
* picked  up  here  (DUPR returned from I/O module).  Highlight
* a   duplicate  as  an  error.  In  this  case,  subtract  the
* percentage  amount  from the accumulated total and change the
* "hidden"  percentage  to zero (i.e. it does not count towards
* the total).
*  When adding a new beneficiary record, initialise fields which
* are not from the screen as follows:
*             Current from = business date
*             Current to = 9's
*             Valid flag = '1',
*             Transaction no. = contract trans. no.,
*            Company = contract company.
*
*****************************************************************
* </pre>
*/
public class P5010 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5010");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaFlag = "N";
	private ZonedDecimalData ib = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData ic = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	protected ZonedDecimalData ix = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData iy = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaMultBnfy = new FixedLengthStringData(20);
	private ZonedDecimalData[] wsaaBnfyCount = ZDArrayPartOfStructure(10, 2, 0, wsaaMultBnfy, 0, UNSIGNED_TRUE);
	private FixedLengthStringData wsaaCrtBnfyreln = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaExitFlag = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaUpdate = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaPctSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaClntnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFound = new FixedLengthStringData(1).init(SPACES);
	private Validator found = new Validator(wsaaFound, "Y");
	private Validator notFound = new Validator(wsaaFound, "N");

	private FixedLengthStringData wsaaBeneTypeFound = new FixedLengthStringData(1).init("N");
	private Validator beneTypeFound = new Validator(wsaaBeneTypeFound, "Y");

	private FixedLengthStringData wsaaValidRelation = new FixedLengthStringData(1).init(SPACES);
	private Validator validRelation = new Validator(wsaaValidRelation, "Y");
	private Validator noValidRelation = new Validator(wsaaValidRelation, "N");

	private FixedLengthStringData wsaaLifeAssuredErr = new FixedLengthStringData(1).init("N");
	private Validator lifeAssuredErr = new Validator(wsaaLifeAssuredErr, "Y");

	private FixedLengthStringData wsaaSelfRelation = new FixedLengthStringData(1).init(SPACES);
	private Validator selfRelation = new Validator(wsaaSelfRelation, "Y");
	private Validator noSelfRelation = new Validator(wsaaSelfRelation, "N");

	private FixedLengthStringData wsaaMandatorys = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaMandatory = FLSArrayPartOfStructure(30, 1, wsaaMandatorys, 0);
	private int wsaaStackPointer = 0;
	private int wsaaScreenPointer = 0;
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(4, 0).setUnsigned();
	protected ZonedDecimalData wsaaNofRead = new ZonedDecimalData(5, 0).setUnsigned();
	private String wsaaDuprFlag = "N";
	private int wsaaStackRecAllow = 0;
	private int wsaaLastSeqNo = 0;
	private ZonedDecimalData wsaaBnypcTotal = new ZonedDecimalData(5, 2).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaBnyCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaNoDets = new FixedLengthStringData(1);
	private String wsaaCltrelnFunction = "";
	private static final int wsaaSubfileSize = 30;
	private static final int wsaaDatabaseSize = 30;

	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(3);
	private Validator wsaaAdd = new Validator(wsaaUpdateFlag, "ADD");
	private Validator wsaaDelete = new Validator(wsaaUpdateFlag, "DEL");
	private Validator wsaaModify = new Validator(wsaaUpdateFlag, "MOD");
	private Validator wsaaChange = new Validator(wsaaUpdateFlag, "CHG");

	private FixedLengthStringData wsaaEofFlag = new FixedLengthStringData(1);
	private Validator wsaaEof = new Validator(wsaaEofFlag, "Y");

	private FixedLengthStringData wsaaNoMoreInsertFlag = new FixedLengthStringData(1);
	private Validator wsaaNoMoreInsert = new Validator(wsaaNoMoreInsertFlag, "Y");

	private FixedLengthStringData wsaaActionOk = new FixedLengthStringData(1);
	private Validator actionOk = new Validator(wsaaActionOk, "Y");
	private ZonedDecimalData wsbbTableSub = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsbbTableRsub = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaClntArray = new FixedLengthStringData(420);
		/*                             OCCURS 10.               <V76L01>*/
	protected FixedLengthStringData[] wsaaClient = FLSArrayPartOfStructure(30, 10, wsaaClntArray, 0);
	protected FixedLengthStringData[] wsaaType = FLSArrayPartOfStructure(30, 2, wsaaClntArray, 300);
	private FixedLengthStringData[] wsaaSequence = FLSArrayPartOfStructure(30, 2, wsaaClntArray, 360);//ILIFE-7195

	private FixedLengthStringData wsaaPctTypes = new FixedLengthStringData(90);
	private FixedLengthStringData[] wsaaPctType = FLSArrayPartOfStructure(10, 2, wsaaPctTypes, 0);
	private FixedLengthStringData[] wsaaPctSeq = FLSArrayPartOfStructure(10, 2, wsaaPctTypes, 20);
	private ZonedDecimalData[] wsaaPctTot = ZDArrayPartOfStructure(10, 5, 2, wsaaPctTypes, 40, UNSIGNED_TRUE);//ILIFE-7195
	protected BinaryData wsaaDuprSub = new BinaryData(5, 0);
	protected BinaryData wsaaClntSub = new BinaryData(2, 0);
		/* TABLES */
	private static final String t3584 = "T3584";
	private static final String t5662 = "T5662";
	private static final String tr52y = "TR52Y";
	protected static final String tr52z = "TR52Z";
	private static final String cltsrec = "CLTSREC";
	private static final String bnfylnbrec = "BNFYLNBREC";
	private static final String mcrlrec = "MCRLREC";
	private BnfylnbTableDAM bnfylnbIO = new BnfylnbTableDAM();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private McrlTableDAM mcrlIO = new McrlTableDAM();
	private Tr52yrec tr52yrec = new Tr52yrec();
	protected Tr52zrec tr52zrec = new Tr52zrec();
	private T5662rec t5662rec = new T5662rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Clntrlsrec clntrlsrec = new Clntrlsrec();
	private Clntkey wsaaClntkey = new Clntkey();
	private Chdrkey wsaaChdrkey = new Chdrkey();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private S5010ScreenVars sv = getPScreenVars();// ScreenProgram.getScreenVars( S5010ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	protected WsbbStackArrayInner wsbbStackArrayInner = new WsbbStackArrayInner();
	private int cessdate=0;
	private boolean benesequence = false;
	//ILIFE-7646
	private String benValue;
	private static final String BEN_FEATURE_ID="NBPRP091";
	private boolean benausFlag=false;
	boolean bentypeflag = false;
	//ILJ-62
	private boolean NBPRP115Permission = false;
	private NBProposalRec nbproposalRec = new NBProposalRec();
	private ExternalisedRules er = new ExternalisedRules();

	private T5661rec t5661rec = new T5661rec();
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	
	 private Antisoclkey antisoclkey = new Antisoclkey();
	 private ZonedDecimalData fupno = new ZonedDecimalData(2, 0).setUnsigned();
	 private ZonedDecimalData fupOldno = new ZonedDecimalData(2, 0).setUnsigned();
	 List<Fluppf> fluppfList;
	 private List<String> followList = new ArrayList<String>();
		
	private String beneficiary="ZBN";
	private AnsopfDAO ansopfDAO = getApplicationContext().getBean("AnsopfDAO", AnsopfDAO.class);
	private Ansopf ansopf = null;
	private List<Ansopf> ansopflist = new ArrayList<Ansopf>();
		
	private static final String t5661 = "T5661";
	private FixedLengthStringData name = new FixedLengthStringData(240);
	private boolean NBPRP117permission = false;
	public ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private Fluppf fluppf;
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private Clntpf clntpf = null;
	private Descpf fupDescpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private BnfypfDAO bnfypfDAO = getApplicationContext().getBean("bnfypfDAO", BnfypfDAO.class);
	private FixedLengthStringData beneficiaryNo = new FixedLengthStringData(8);
	List<String> benNo = new ArrayList<String>();
	

/**
 * Contains all possible labels used by goTo action.
 */
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		next1220, 
		read1230, 
		continue1260, 
		checkStatuz2070, 
		exit2090, 
		continue2180, 
		readNext2180, 
		b790Exit
	}

	public P5010() {
		super();
		screenVars = sv;
		new ScreenModel("S5010", AppVars.getInstance(), sv);
	}
	protected S5010ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( S5010ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		try {
			initialise1010();
			retreiveHeader1030();
			readContractLongdesc1040();
			initialStack1050();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaMandatorys.set(SPACES);
		ix.set(ZERO);
		iy.set(ZERO);
		iz.set(ZERO);
		wsaaCount.set(ZERO);
		wsaaMultBnfy.set(ZERO);
		ib.set(ZERO);
		
		if ( isNE(wsspcomn.cessdte, SPACES)) {
			cessdate = Integer.parseInt(wsspcomn.cessdte.trim());
		}
		benesequence = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP087", appVars, "IT");
		if(benesequence){
			sv.actionflag.set("Y");
		}
		else{
			sv.actionflag.set("N");
		}
		wsaaLifeAssuredErr.set("N");
		/* Dummy subfile initalisation for prototype - rePLace with SCLR*/
		scrnparams.function.set(varcom.sinit);
		processScreen("S5010", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Get today's date.                                            */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		NBPRP115Permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP115", appVars, "IT");//ILJ-62
		NBPRP117permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP117", appVars, "IT"); 
	}

protected void retreiveHeader1030()
	{
		/*    READ Contract header.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void readContractLongdesc1040()
	{
		/*    READ Contract header Long description.*/
		descIO.setDataArea(SPACES);
		descIO.setDesccoy(chdrlnbIO.getChdrcoy());
		descIO.setDesctabl("T5688");
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setFunction(varcom.readr);
		descIO.setDescpfx("IT");
		descIO.setLanguage(wsspcomn.language);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		/*  Read CLTS to get contract owner long name                      */
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrlnbIO.getCownnum());
		sv.cownnum.set(chdrlnbIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.ownername.set(wsspcomn.longconfname);
		/*  Read LIFE to get life insured                                  */
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			lifeAssuredErr.setTrue();
			goTo(GotoLabel.exit1090);
		}
		/*  Read CLTS to get life insured long name                        */
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/*  Read Contract Beneficiary Mandatory Table - TR52Z.             */
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52z);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52z);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52zrec.tr52zRec.set(itemIO.getGenarea());
		/*    Perform section to load first page of subfile*/
		/*   SET UP KEY*/
		bnfylnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		bnfylnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		bnfylnbIO.setBnytype(SPACES);
		bnfylnbIO.setBnyclt(SPACES);
	}

protected void initialStack1050()
	{
		loadStack1200();
		/*LOAD-SUBFILE-FIELDS*/
		/*   INITIALISE SUBFILE*/
		moveStackToScreen1400();
	}

protected void cltsio1900()
	{
		/*CLTSIO*/
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadStack1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1200();
				case next1220: 
					next1220();
				case read1230: 
					read1230();
					retreiveClientDetails1240();
				case continue1260: 
					continue1260();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1200()
	{
		wsbbStackArrayInner.wsbbStackArray.set(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, wsaaDatabaseSize)); wsaaIndex.add(1)){
			initialZero1300();
		}
		wsaaNofRead.set(0);
		/*   READ BNFYLNB FILE AND LOAD TO STACK*/
		bnfylnbIO.setFunction(varcom.begn);
		goTo(GotoLabel.read1230);
	}

protected void next1220()
	{
		bnfylnbIO.setFunction(varcom.nextr);
	}

protected void read1230()
	{
		callBnfylnbio5000();
		if (isEQ(bnfylnbIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.continue1260);
		}
		wsaaNofRead.add(1);
	}

protected void retreiveClientDetails1240()
	{
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(bnfylnbIO.getBnyclt());
		cltsIO.setFunction(varcom.readr);
		cltsio1900();
		plainname();
		wsbbStackArrayInner.wsbbBnysel[wsaaNofRead.toInt()].set(bnfylnbIO.getBnyclt());
		/* MOVE WSSP-LONGCONFNAME      TO WSBB-BNYNAME(WSAA-NOF-READ).  */
		wsbbStackArrayInner.wsbbClntnm[wsaaNofRead.toInt()].set(wsspcomn.longconfname);
		/* MOVE BNFYLNB-BNYRLN         TO WSBB-BNYRLN(WSAA-NOF-READ).   */
		wsbbStackArrayInner.wsbbBnyrln[wsaaNofRead.toInt()].set(bnfylnbIO.getCltreln());
		wsbbStackArrayInner.wsbbBnytype[wsaaNofRead.toInt()].set(bnfylnbIO.getBnytype());
		wsbbStackArrayInner.wsbbBnycd[wsaaNofRead.toInt()].set(bnfylnbIO.getBnycd());
		wsbbStackArrayInner.wsbbBnypc[wsaaNofRead.toInt()].set(bnfylnbIO.getBnypc());
		wsbbStackArrayInner.wsbbEffdate[wsaaNofRead.toInt()].set(bnfylnbIO.getEffdate());
		wsbbStackArrayInner.wsbbEnddate[wsaaNofRead.toInt()].set(bnfylnbIO.getEnddate());
		wsbbStackArrayInner.wsbbRelto[wsaaNofRead.toInt()].set(bnfylnbIO.getRelto());
		wsbbStackArrayInner.wsbbRevcflg[wsaaNofRead.toInt()].set(bnfylnbIO.getRevcflg());
		wsbbStackArrayInner.wsbbSelfind[wsaaNofRead.toInt()].set(bnfylnbIO.getSelfind());
		if(benesequence){
		wsbbStackArrayInner.wsbbSequence[wsaaNofRead.toInt()].set(bnfylnbIO.getSequence());//fwang3
		}
		
		goTo(GotoLabel.next1220);
	}

protected void continue1260()
	{
		if (isEQ(wsaaNofRead, 0)) {
			wsaaBnypcTotal.set(ZERO);
		}
		else {
			wsaaBnypcTotal.set("100");
		}
		/*EXIT*/
	}

protected void initialZero1300()
	{
		/*PARA*/
		wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbBnypc[wsaaIndex.toInt()].set(ZERO);
		wsbbStackArrayInner.wsbbBnyrln[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbBnycd[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()].set(SPACES);
		if(benesequence){
		wsbbStackArrayInner.wsbbSequence[wsaaIndex.toInt()].set(SPACES);
		}
		/* MOVE SPACES   TO WSBB-BNYNAME (WSAA-INDEX).          <A05873>*/
		wsbbStackArrayInner.wsbbClntnm[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()].set(varcom.vrcmMaxDate);
		wsbbStackArrayInner.wsbbEnddate[wsaaIndex.toInt()].set(varcom.vrcmMaxDate);
		wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()].set(SPACES);
		wsbbStackArrayInner.wsbbRelto[wsaaIndex.toInt()].set(SPACES);
		/*  MOVE SPACE                  TO WSBB-RELTO.           <S19FIX>*/
		wsbbStackArrayInner.wsbbChange[wsaaIndex.toInt()].set("N");
		/*EXIT*/
	}

protected void moveStackToScreen1400()
	{
		/*PARA*/
		scrnparams.function.set(varcom.sclr);
		callScreenIo5100();
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, wsaaSubfileSize)); wsaaIndex.add(1)){
			moveToScreen1500();
		}
		/*EXIT*/
	}

protected void moveToScreen1500()
	{
		moveToScreen1520();
	}

protected void moveToScreen1520()
	{
		sv.bnyselOut[varcom.pr.toInt()].set(SPACES);
		sv.bnytypeOut[varcom.pr.toInt()].set(SPACES);
		sv.cltrelnOut[varcom.pr.toInt()].set(SPACES);
		sv.bnycdOut[varcom.pr.toInt()].set(SPACES);
		sv.bnypcOut[varcom.pr.toInt()].set(SPACES);
		sv.effdateOut[varcom.pr.toInt()].set(SPACES);
		sv.enddateOut[varcom.pr.toInt()].set(SPACES);
		if(benesequence){
		sv.sequenceOut[varcom.pr.toInt()].set(SPACES);
		}
		/*  MOVE WSBB-BNYRLN (WSAA-INDEX) TO S5010-BNYRLN.               */
		sv.cltreln.set(wsbbStackArrayInner.wsbbBnyrln[wsaaIndex.toInt()]);
		sv.bnytype.set(wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()]);
		sv.relto.set(wsbbStackArrayInner.wsbbRelto[wsaaIndex.toInt()]);
		sv.revcflg.set(wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()]);
		sv.bnycd.set(wsbbStackArrayInner.wsbbBnycd[wsaaIndex.toInt()]);
		sv.bnypc.set(wsbbStackArrayInner.wsbbBnypc[wsaaIndex.toInt()]);
		sv.bnysel.set(wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()]);
		if(benesequence){
		sv.sequence.set(wsbbStackArrayInner.wsbbSequence[wsaaIndex.toInt()]);
		}
		/* MOVE WSBB-BNYNAME(WSAA-INDEX) TO S5010-BNYNAME.              */
		/* MOVE WSBB-CLNTNM (WSAA-INDEX) TO S5010-CLNTNM.       <V76L01>*/
		sv.clntsname.set(wsbbStackArrayInner.wsbbClntnm[wsaaIndex.toInt()]);
		sv.effdate.set(wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()]);
		sv.enddate.set(wsbbStackArrayInner.wsbbEnddate[wsaaIndex.toInt()]);
		scrnparams.subfileRrn.set(wsaaIndex);
		if (isEQ(wsbbStackArrayInner.wsbbRevcflg[wsaaIndex.toInt()], "N")) {
			sv.bnyselOut[varcom.pr.toInt()].set("Y");
			sv.bnytypeOut[varcom.pr.toInt()].set("Y");
			sv.cltrelnOut[varcom.pr.toInt()].set("Y");
			sv.bnycdOut[varcom.pr.toInt()].set("Y");
			sv.bnypcOut[varcom.pr.toInt()].set("Y");
			sv.effdateOut[varcom.pr.toInt()].set("Y");
			sv.enddateOut[varcom.pr.toInt()].set("Y");
			if(benesequence){
			sv.sequenceOut[varcom.pr.toInt()].set("Y");
			}
		}
		scrnparams.function.set(varcom.sadd);
		callScreenIo5100();
		if (isEQ(wsaaNofRead, 0)) {
			wsaaBnypcTotal.add(wsbbStackArrayInner.wsbbBnypc[wsaaIndex.toInt()]);
		}
	}

protected void callBnfylnbio5000()
	{
		para5000();
	}

protected void para5000()
	{
		bnfylnbIO.setFormat(bnfylnbrec);
		SmartFileCode.execute(appVars, bnfylnbIO);
		if (isNE(bnfylnbIO.getStatuz(), varcom.oK)
		&& isNE(bnfylnbIO.getStatuz(), varcom.mrnf)
		&& isNE(bnfylnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(bnfylnbIO.getParams());
			fatalError600();
		}
		if (isEQ(bnfylnbIO.getStatuz(), varcom.mrnf)) {
			bnfylnbIO.setStatuz(varcom.endp);
		}
		if ((isNE(bnfylnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy()))
		|| (isNE(bnfylnbIO.getChdrnum(), chdrlnbIO.getChdrnum()))) {
			bnfylnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(bnfylnbIO.getStatuz(), varcom.endp)) {
			return ;
		}
	}

protected void callScreenIo5100()
	{
		/*PARA*/
		processScreen("S5010", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(varcom.bomb);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		if (lifeAssuredErr.isTrue()) {
			scrnparams.errorCode.set(errorsInner.rfk4);
			return ;
		}
		wsspcomn.edterror.set(varcom.oK);
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validateScreen2010();
				case checkStatuz2070: 
					checkStatuz2070();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5010IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5010-DATA-AREA                         */
		/*                         S5010-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			/*    PERFORM 2200-CHECK-TOTALS                                 */
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/* When life assured is not defined, keep displaying the error     */
		/* message.                                                        */
		if (lifeAssuredErr.isTrue()) {
			scrnparams.errorCode.set(errorsInner.rfk4);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkStatuz2070);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
			/*****    PERFORM 2400-CALC                                 <CAS1.0>*/
		}
		wsaaNoDets.set(SPACES);
		wsaaFlag = "N";
		wsaaMultBnfy.set(ZERO);
		initialize(wsaaPctTypes);
		wsaaPctSub.set(1);
		scrnparams.function.set(varcom.sstrt);
		/*    Validate fields*/
		processScreen("S5010", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, "ENDP")) {
			goTo(GotoLabel.checkStatuz2070);
		}
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2100();
		}
		
		/* IF S5010-ERROR-INDICATORS = SPACES                           */
		/*    PERFORM 2200-CHECK-TOTALS.                                */
		if (isEQ(sv.errorIndicators, SPACES)) {
			clientDuplicate2500();
		}
		/* If entered, share percentages must be present against           */
		/* all beneficiaries.                                              */
		if (isNE(wsaaNoDets, SPACES)
		&& isGT(wsaaBnypcTotal, ZERO)
		&& isEQ(wsaaFlag, "Y")) {
			scrnparams.errorCode.set(errorsInner.e518);
			wsspcomn.edterror.set("Y");
		}
		/* Calculate default share percentages if none are entered.        */
		/*  IF WSAA-BNY-CNT          NOT = ZERO    AND           <V76L01>*/
		/*     WSAA-BNYPC-TOTAL          = ZERO    AND           <V76L01>*/
		/*     S5010-ERROR-SUBFILE       = SPACE                 <V76L01>*/
		/*     COMPUTE WSAA-QUOTA        = 100 / WSAA-BNY-CNT    <V76L01>*/
		/*     MOVE O-K                 TO SCRN-STATUZ           <V76L01>*/
		/*     MOVE SSTRT               TO SCRN-FUNCTION         <V76L01>*/
		/*     PERFORM 2600-DEFAULT-SHARE                        <V76L01>*/
		/*        UNTIL SCRN-STATUZ      = ENDP                  <V76L01>*/
		/*  END-IF.                                              <V76L01>*/
		/*                                                       <V76L01>*/
		/*   IF WSAA-BNYPC-TOTAL     NOT = 100                   <V76L01>*/
		/*   AND WSAA-FLAG = 'Y'                                 <V76L01>*/
		/*      MOVE E631               TO SCRN-ERROR-CODE       <V76L01>*/
		/*      MOVE 'Y'                TO WSSP-EDTERROR         <V76L01>*/
		/*  END-IF.                                              <V76L01>*/
		/*   Check for mandatory beneficiary type in TR52Z.                */
		a100MandatoryBnfy();
		/*   Check for percentage under each beneficiary type.             */
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			if (isNE(wsaaPctTot[ix.toInt()], 100)
			&& isNE(wsaaPctTot[ix.toInt()], ZERO)) {
				if(benesequence)
					scrnparams.errorCode.set(errorsInner.rrej); // fwang3 fix ICIL-166
				else
					scrnparams.errorCode.set(errorsInner.e631);	
				wsspcomn.edterror.set("Y");
			}
		}
	}

protected void checkStatuz2070()
	{
		/*  Do not allow any action on the screen if an error was found    */
		/*  in the above VALIDATE section.                                 */
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void validateSubfile2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateSubfile2120();
					updateErrorIndicators2170();
				case continue2180: 
					continue2180();
				case readNext2180: 
					readNext2180();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
protected void callDatcon21600()
{
	/*PARA*/
	datcon2rec.frequency.set("01");
	callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	if (isEQ(datcon2rec.statuz, varcom.bomb)) {
		syserrrec.statuz.set(datcon2rec.statuz);
		fatalError600();
	}
	/*EXIT*/
}

protected void validateSubfile2120()
	{
		/*    MOVE SRNCH                  TO SCRN-FUNCTION.                */
		/*    CALL 'S5010IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5010-DATA-AREA                         */
		/*                         S5010-SUBFILE-AREA.                     */
		/*    IF SCRN-STATUZ NOT = O-K                                     */
		/*                     AND ENDP                                    */
		/*       MOVE SCRN-STATUZ         TO SYSR-STATUZ                   */
		/*       PERFORM 600-FATAL-ERROR.                                  */
		/*    IF SCRN-STATUZ     = ENDP                                    */
		/*       GO TO 2190-EXIT.                                          */
		/*  IF DETAILS ARE BLANKED OUT, ADJUST TOTALS, READ NEXT*/
		/*    IF ((S5010-BNYRLN = SPACES) AND                              */
		/*        (S5010-BNYCD  = SPACES) AND                              */
		/*        (S5010-BNYPC  = ZEROES) AND                              */
		/*        (S5010-EFFDATE = VRCM-MAX-DATE) AND              <V76L01>*/
		/*        (S5010-BNYSEL = SPACES))                                 */
		
		
		
		
		if (isEQ(sv.cltreln, SPACES)
		&& isEQ(sv.bnycd, SPACES)
		&& isEQ(sv.bnypc, ZERO)
		&& isEQ(sv.bnysel, SPACES)
		&& isEQ(sv.effdate, varcom.vrcmMaxDate)
		&& isEQ(sv.bnytype, SPACES)) {
			if(benesequence){
				if(isEQ(sv.sequence, SPACES)) {
					goTo(GotoLabel.continue2180);
				}
			}
			else
				goTo(GotoLabel.continue2180);
		}
		/*    IF SCRN-STATUZ     = CALC                                    */
		/*                    OR   O-K                                     */
		calc2400();
		/* IF SCRN-STATUZ              = CALC                   <CAS1.0>*/
		/*    MOVE 'Y'                 TO WSSP-EDTERROR         <CAS1.0>*/
		/* END-IF.                                              <CAS1.0>*/
		/*    CHECK RELATIONSHIP*/
		/* MOVE SPACES                 TO DESC-DATA-KEY.                */
		/* MOVE 'IT'                   TO DESC-DESCPFX.                 */
		/* MOVE WSSP-COMPANY           TO DESC-DESCCOY.                 */
		/* MOVE T5663                  TO DESC-DESCTABL.                */
		/* MOVE S5010-BNYRLN           TO DESC-DESCITEM.                */
		/* MOVE WSSP-LANGUAGE          TO DESC-LANGUAGE.                */
		/* MOVE READR                  TO DESC-FUNCTION.                */
		/*  CALL 'DESCIO'               USING DESC-PARAMS.       <V76L01>*/
		/* IF DESC-STATUZ              NOT = O-K AND            <V76L01>*/
		/*    DESC-STATUZ              NOT = MRNF               <V76L01>*/
		/*     MOVE DESC-STATUZ        TO SYSR-STATUZ           <V76L01>*/
		/*     PERFORM 600-FATAL-ERROR.                         <V76L01>*/
		/*   IF DESC-STATUZ              = MRNF                   <V76L01>*/
		/*       MOVE E468               TO S5010-BNYRLN-ERR.     <V76L01>*/
		/* Check relationship. Validation check is replaced by T3584    */
		/* IF S5010-BNYRLN         NOT =  SPACES AND            <V76L01>*/
		if (isNE(sv.cltreln, SPACES)
		&& isNE(sv.bnytype, SPACES)
		&& isNE(sv.bnypc, ZERO)
		&& isNE(sv.bnysel, SPACES)
		&& (isEQ(sv.effdate, varcom.vrcmMaxDate)
		|| isEQ(sv.effdate, ZERO))) {
			/*    MOVE DTC1-INT-DATE       TO S5010-EFFDATE         <V72L04>*/
			sv.effdate.set(chdrlnbIO.getOccdate());
		}
		
		if (isEQ(sv.bnytype, SPACES)) {
			sv.bnytypeErr.set(errorsInner.e186);
		}
		if(benesequence){
		if (isEQ(sv.sequence, SPACES) && isNE(sv.bnysel, SPACES)) {
			sv.sequenceErr.set(errorsInner.e186);
		}
		if (isNE(sv.sequence, SPACES) && isEQ(sv.bnysel, SPACES)) {
			sv.bnyselErr.set(errorsInner.e186);
		}
		}
		if (isEQ(sv.cltreln, SPACES)) {
			sv.cltrelnErr.set(errorsInner.e186);
		}
		if (isEQ(sv.bnypc, ZERO)) {
			sv.bnypcErr.set(errorsInner.e186);
		}
		if (isEQ(sv.effdate, varcom.vrcmMaxDate)
		|| isEQ(sv.effdate, ZERO)) {
			sv.effdateErr.set(errorsInner.h046);
		}
		if(isNE(sv.enddate, SPACES)&& isNE(sv.enddate, varcom.vrcmMaxDate)&&isLT(sv.enddate,sv.effdate)){
				sv.enddateErr.set(errorsInner.rfwr);
			}
		else{
		if(isLT(sv.effdate,chdrlnbIO.getOccdate())){
			sv.effdateErr.set(errorsInner.rppi);
		}
		if(isEQ(sv.bnytype,"BD")){
		datcon2rec.intDate1.set(sv.effdate);
		datcon2rec.freqFactor.set(3);
		callDatcon21600();
		if(isEQ(sv.enddate, SPACES)||isEQ(sv.enddate, varcom.vrcmMaxDate)){
			sv.enddate.set(datcon2rec.intDate2);
		}
		else if(isNE(sv.enddate, SPACES)&& isNE(sv.enddate, varcom.vrcmMaxDate)&&isLT(sv.enddate,datcon2rec.intDate2)){
					sv.enddateErr.set(errorsInner.rfwq); 
				}
		if(isNE(sv.enddate, SPACES)&& isNE(sv.enddate, varcom.vrcmMaxDate) && isGT(sv.enddate, cessdate)){
			sv.enddateErr.set(errorsInner.rfwt);
		}
		}
		else{
				if(isNE(sv.enddate, SPACES)&& isNE(sv.enddate, varcom.vrcmMaxDate)&&isLT(sv.enddate,datcon2rec.intDate2)){
						sv.enddateErr.set(errorsInner.rfwq);
					}
				if(isNE(sv.enddate,SPACES) && isNE(sv.enddate, varcom.vrcmMaxDate)  && isGT(sv.enddate, cessdate))
				{
					sv.enddateErr.set(errorsInner.rfwt);
				}
		}
		}
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		
		//ILJ-62
		if(NBPRP115Permission) {
			
			nbproposalRec = new NBProposalRec();
			nbproposalRec.setCnttype(sv.cnttype.toString());
			nbproposalRec.setCrtable("");	
			nbproposalRec.setNoField(1);
			nbproposalRec.setScreeId("S5010");
			nbproposalRec.getFieldName().add("Share %");
			nbproposalRec.getFieldValue().add(String.valueOf(sv.bnypc.toFloat()));
			
			if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("NBPROPOSALVALD")))
			{
				callProgram("NBPROPOSALVALD",nbproposalRec);
				for(int i = 0; i < nbproposalRec.getOutputFieldName().size() ; i++) {
					if(nbproposalRec.getOutputFieldName().get(i).equals("Share %")) {
						if(isNE(nbproposalRec.getOuputErrorCode().get(i),"") && isEQ(sv.bnypcErr, SPACES) ) {
							wsspcomn.edterror.set("Y");
							sv.bnypcErr.set(nbproposalRec.getOuputErrorCode().get(i));
						}
					}
				}
			}
		}
		
	}

protected void updateErrorIndicators2170()
	{
		scrnparams.function.set(varcom.supd);
		processScreen("S5010", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
			/*    GO TO 2190-EXIT.                                          */
			goTo(GotoLabel.readNext2180);
		}
	}

protected void continue2180()
	{
		// Temp solution - ticket IJ-445 : Life::Defect::UAT::Contract Beneficiaries system error
		if (wsbbStackArrayInner.wsbbBnypc[scrnparams.subfileRrn.toInt()] != null) {
			wsaaBnypcTotal.subtract(wsbbStackArrayInner.wsbbBnypc[scrnparams.subfileRrn.toInt()]);
			wsaaBnypcTotal.add(sv.bnypc);
			wsbbStackArrayInner.wsbbBnypc[scrnparams.subfileRrn.toInt()].set(sv.bnypc);
			wsbbStackArrayInner.wsbbChange[scrnparams.subfileRrn.toInt()].set("Y");
		}
	}

protected void readNext2180()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5010", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Call to this section removed.                                   
	*2200-CHECK-TOTALS   SECTION.                                     
	*2220-CHECK-TOTALS.                                               
	****  IF SCRN-STATUZ = KILL AND                                   
	****     WSAA-NOF-READ = ZERO                                     
	****     GO TO 2220-EXIT.                                         
	****  IF WSAA-BNYPC-TOTAL NOT = 100  AND                          
	****     WSAA-BNYPC-TOTAL NOT = 0                                 
	****     MOVE E631 TO SCRN-ERROR-CODE                             
	****     MOVE 'Y' TO WSSP-EDTERROR.                               
	*2220-EXIT.                                                       
	****  EXIT.                                                       
	* </pre>
	*/
protected void readT56622300()
	{
		readT56622320();
	}

protected void readT56622320()
	{
		/*  READ CONTRACT DEFINITION TABLE*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5662);
		itdmIO.setItemitem(sv.bnycd);
		if (isNE(chdrlnbIO.getOccdate(), ZERO)) {
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		}
		else {
			itdmIO.setItmfrm(ZERO);
		}
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemitem(), sv.bnycd)) {
			sv.bnycdErr.set(errorsInner.e470);
			return ;
		}
		t5662rec.t5662Rec.set(itdmIO.getGenarea());
		sv.bnypc.set(t5662rec.bnypc);
	}

protected void calc2400()
	{
		calc2420();
	}

protected void calc2420()
	{
		wsaaFlag = "Y";
		if (isNE(sv.bnycd, SPACES)) {
			readT56622300();
		}
		if ((isGT(sv.bnypc, 100))) {
			sv.bnypcErr.set(errorsInner.f348);
		}
		if ((isEQ(sv.bnycd, SPACES))
		&& (isEQ(sv.bnypc, 0))) {
			wsaaNoDets.set("Y");
		}
		/*    MOVE E518              TO S5010-BNYCD-ERR         <CAS1.0>*/
		/*    MOVE E518              TO S5010-BNYPC-ERR.        <CAS1.0>*/
		if (isEQ(sv.bnysel, SPACES)) {
			sv.bnyselErr.set(errorsInner.e186);
			wsspcomn.longconfname.set(SPACES);
			sv.clntsname.set(SPACES);
			return ;
		}
		if (isEQ(sv.bnytype, SPACES)) {
			sv.bnytypeErr.set(errorsInner.e186);
			return ;
		}
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.bnysel);
		cltsio2900();
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			/*     MOVE E339               TO S5010-BNYSEL-ERR              */
			sv.bnyselErr.set(errorsInner.e058);
			wsspcomn.longconfname.set(SPACES);
		}
		else {
			plainname();
			if (isLTE(cltsIO.getCltdod(), chdrlnbIO.getOccdate())) {
				sv.bnyselErr.set(errorsInner.f782);
			}
		}
		/* MOVE WSSP-LONGCONFNAME      TO S5010-BNYNAME.                */
		/* MOVE WSSP-LONGCONFNAME      TO S5010-CLNTNM.         <V76L01>*/
		sv.clntsname.set(wsspcomn.longconfname);
		if (isNE(sv.cltreln, SPACES)) {
			a200ReadT3584();
		}
		b100CallTr52y();
		sv.relto.set(tr52yrec.relto);
		
		if (isEQ(sv.relto, "O")
		|| isEQ(sv.relto, "B")) {
			wsaaClntnum.set(chdrlnbIO.getCownnum());
		}
		if (isEQ(sv.relto, "L")) {
			wsaaClntnum.set(lifelnbIO.getLifcnum());
		}
		chkAlrdyRlnshp2600();
		/* Check for a duplicate client number.                         */
		for (wsaaDuprSub.set(1); !(isGT(wsaaDuprSub, scrnparams.subfileRrn)); wsaaDuprSub.add(1)){
			if (isEQ(sv.bnysel, wsaaClient[wsaaDuprSub.toInt()])
			&& isEQ(sv.bnytype, wsaaType[wsaaDuprSub.toInt()])
			&& isNE(sv.bnysel, SPACES)) {
				sv.bnyselErr.set(errorsInner.e048);
				wsspcomn.edterror.set("Y");
				return ;
			}
		}
		/* Check the relationship against blank and allow self flag        */
		/* in TR52Z.                                                       */
		if (isEQ(sv.cltreln, SPACES)) {
			b300ReadTr52z();
			if (found.isTrue()
			&& isNE(tr52zrec.selfind[wsbbTableSub.toInt()], "Y")) {
				sv.cltrelnErr.set(errorsInner.e186);
			}
		}
		wsaaClient[scrnparams.subfileRrn.toInt()].set(sv.bnysel);
		if (isNE(sv.bnytype, SPACES)
		&& isNE(sv.bnysel, SPACES)) {
			wsaaType[scrnparams.subfileRrn.toInt()].set(sv.bnytype);
			if(benesequence){
			wsaaSequence[scrnparams.subfileRrn.toInt()].set(sv.sequence);//ILIFE-7195
			}
		}
		ib.set(ZERO);
		for (wsaaPctSub.set(1); !(isGT(wsaaPctSub, 10)); wsaaPctSub.add(1)) {
			if (isEQ(sv.bnytype, wsaaPctType[wsaaPctSub.toInt()])){
				if(benesequence){
					if(isEQ(sv.sequence, this.wsaaPctSeq[wsaaPctSub.toInt()])) {// ILIFE-7195
				wsaaPctTot[wsaaPctSub.toInt()].add(sv.bnypc);
				wsaaBnfyCount[wsaaPctSub.toInt()].add(1);
				ib.set(wsaaPctSub);
				wsaaPctSub.set(11);
			   }
			}
			else{
					wsaaPctTot[wsaaPctSub.toInt()].add(sv.bnypc);
					wsaaBnfyCount[wsaaPctSub.toInt()].add(1);
					ib.set(wsaaPctSub);
					wsaaPctSub.set(11);
				}
			}		
			else {
				if (isEQ(wsaaPctType[wsaaPctSub.toInt()], SPACES)) {
					wsaaPctType[wsaaPctSub.toInt()].set(sv.bnytype);
					wsaaPctTot[wsaaPctSub.toInt()].set(sv.bnypc);
					if(benesequence){
					wsaaPctSeq[wsaaPctSub.toInt()].set(sv.sequence);// ILIFE-7195
					}
					wsaaBnfyCount[wsaaPctSub.toInt()].set(1);
					ib.set(wsaaPctSub);
					wsaaPctSub.set(11);
				}
			}
		}
		
		if (isGT(wsaaBnfyCount[ib.toInt()], 1)
		&& isEQ(tr52yrec.multind, "N")) {
			sv.bnyselErr.set(errorsInner.rfk6);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.relto, "N")) {
			b200MandRlsp();
		}
	}

protected void clientDuplicate2500()
	{
		checkForDuplicates2500();
	}

protected void checkForDuplicates2500()
	{
		/* Set up sequential read of the client numbers in the subfile     */
		/* to check for duplicate numbers.                                 */
//		wsaaClntArray.set(SPACES); //MIBT-191
		wsaaDuprSub.set(1);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5010", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		//ILIFE-7646
		benValue = SPACE;
		
		if(isEQ(benValue, SPACE)&& isNE(sv.bnytype, SPACES)) {
			benValue = sv.bnytype.trim();
		}
		//ILIFE-7646
		/*    Move the client number to the table.                         */
		wsaaClient[wsaaDuprSub.toInt()].set(sv.bnysel);
		/*   Read through the subfile - checking the selected client       */
		/*   against the table of prviously stored numbers.                */
		for (wsaaDuprSub.set(2); !(isGT(wsaaDuprSub, 10)); wsaaDuprSub.add(1)){
			checkClientTable2550();
			if (isNE(sv.bnyselErr, SPACES) || isNE(sv.bnytypeErr, SPACES)) {
				/*       MOVE E048                TO SCRN-ERROR-CODE    <CAS1.0>*/
				wsspcomn.edterror.set("Y");
				/*       MOVE 11                  TO WSAA-DUPR-SUB      <CAS1.0>*/
				scrnparams.function.set(varcom.supd);
				processScreen("S5010", sv);
				if (isNE(scrnparams.statuz, varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
			}
		}
	}

protected void checkClientTable2550()
	{
		/*CHECK-TABLE*/
		scrnparams.function.set(varcom.srdn);
		processScreen("S5010", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* Move the client number to the table.                            */
		wsaaClient[wsaaDuprSub.toInt()].set(sv.bnysel);
		/* Check the client number against previously stored numbers.      */
		for (wsaaClntSub.set(1); !(isEQ(wsaaClntSub, wsaaDuprSub)); wsaaClntSub.add(1)){
			if (isEQ(sv.bnysel, wsaaClient[wsaaClntSub.toInt()])) {
				if (isNE(sv.bnysel, SPACES)) {
					sv.bnyselErr.set(errorsInner.e048);
					/*****     GO TO 2550-EXIT                                  <CAS1.0>*/
				}
			}
		}
		//ILIFE-7646
		benausFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),BEN_FEATURE_ID, appVars, "IT");
		if(benausFlag) {
		if(isNE(sv.bnytype, SPACE) && !benValue.equals(sv.bnytype.toString())) {
				sv.bnytypeErr.set(errorsInner.rrlu);
			}
		}
		//ILIFE-7646
		/*EXIT*/
	}

	/**
	* <pre>
	*2600-DEFAULT-SHARE SECTION.                              <V76L01>
	*2600-START.                                              <V76L01>
	*                                                         <V76L01>
	*    CALL 'S5010IO'           USING SCRN-SCREEN-PARAMS    <V76L01>
	*                                   S5010-DATA-AREA       <V76L01>
	*                                   S5010-SUBFILE-AREA.   <V76L01>
	*                                                         <V76L01>
	*    IF SCRN-STATUZ           NOT = O-K   AND             <V76L01>
	*       SCRN-STATUZ           NOT = ENDP                  <V76L01>
	*       MOVE SCRN-STATUZ         TO SYSR-STATUZ           <V76L01>
	*       PERFORM 600-FATAL-ERROR                           <V76L01>
	*    END-IF.                                              <V76L01>
	*                                                         <V76L01>
	*    IF SCRN-STATUZ               = ENDP                  <V76L01>
	*       GO TO 2600-EXIT                                   <V76L01>
	*    END-IF.                                              <V76L01>
	*                                                         <V76L01>
	*    IF S5010-BNYSEL          NOT = SPACE                 <V76L01>
	*       MOVE WSAA-QUOTA          TO S5010-BNYPC           <V76L01>
	*                            WSBB-BNYPC (SCRN-SUBFILE-RRN)<V76L01>
	*       ADD  WSAA-QUOTA          TO WSAA-BNYPC-TOTAL      <V76L01>
	*       MOVE SUPD                TO SCRN-FUNCTION         <V76L01>
	*                                                         <V76L01>
	*       CALL 'S5010IO'        USING SCRN-SCREEN-PARAMS    <V76L01>
	*                                   S5010-DATA-AREA       <V76L01>
	*                                   S5010-SUBFILE-AREA    <V76L01>
	*                                                         <V76L01>
	*       IF SCRN-STATUZ        NOT = O-K                   <V76L01>
	*          MOVE SCRN-STATUZ      TO SYSR-STATUZ           <V76L01>
	*          PERFORM 600-FATAL-ERROR                        <V76L01>
	*       END-IF                                            <V76L01>
	*    END-IF.                                              <V76L01>
	*                                                         <V76L01>
	*    MOVE SRDN                   TO SCRN-FUNCTION.        <V76L01>
	*                                                         <V76L01>
	*2600-EXIT.                                               <V76L01>
	*    EXIT.                                                <V76L01>
	* </pre>
	*/
protected void chkAlrdyRlnshp2600()
	{
		start2610();
	}

protected void start2610()
	{
		mcrlIO.setParams(SPACES);
		mcrlIO.setMcltcoy(wsspcomn.fsuco);
		mcrlIO.setClntcoy(wsspcomn.fsuco);
		mcrlIO.setMcltnum(wsaaClntnum);
		mcrlIO.setClntnum(sv.bnysel);
		mcrlIO.setFormat(mcrlrec);
		mcrlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mcrlIO);
		if (isNE(mcrlIO.getStatuz(), varcom.oK)
		&& isNE(mcrlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(mcrlIO.getParams());
			syserrrec.statuz.set(mcrlIO.getStatuz());
			fatalError600();
		}
		if (isEQ(mcrlIO.getStatuz(), varcom.oK)
		&& isEQ(sv.cltreln, SPACES)) {
			sv.cltreln.set(mcrlIO.getCltreln());
			return ;
		}
		if (isEQ(mcrlIO.getStatuz(), varcom.oK)
		&& isNE(sv.cltreln, mcrlIO.getCltreln())
		&& isNE(sv.cltreln, SPACES)) {
			sv.cltrelnErr.set(errorsInner.rfk0);
			wsspcomn.edterror.set("Y");
		}
	}

protected void a100MandatoryBnfy()
	{
		a110Start();
	}

protected void a110Start()
	{
		/*  Check for Mandatory Beneficiary's relation using TR52Z.        */
		wsaaMandatorys.set(SPACES);
		wsaaFound.set(SPACES);
		wsaaCount.set(ZERO);
		iz.set(ZERO);
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52z);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52z);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52zrec.tr52zRec.set(itemIO.getGenarea());
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			if (isNE(tr52zrec.bnytype[ix.toInt()], SPACES)
			&& isEQ(tr52zrec.manopt[ix.toInt()], "Y")) {
				iz.add(1);
				for (iy.set(1); !(isGT(iy, 30)
				|| isEQ(wsaaMandatory[iz.toInt()], "Y")); iy.add(1)){
					wsaaMandatory[iz.toInt()].set("N");
					if (isEQ(tr52zrec.bnytype[ix.toInt()], wsaaType[iy.toInt()])) {
						wsaaMandatory[iz.toInt()].set("Y");
					}
				}
			}
		}
		if (isGT(ix, 10)) {
			ix.set(1);
			while ( !(notFound.isTrue())) {
				a300ContinueCheck();
			}
			
		}
		wsaaCount.add(inspectTallyAll(wsaaMandatorys, "N"));
		if (isGT(wsaaCount, 0)) {
			sv.chdrnumErr.set(errorsInner.rfk2);
			wsspcomn.edterror.set("Y");
		}
		/*Fix for MIBT-191
		clear the obeject which containing client and type details*/
		wsaaClntArray.set(SPACES);
	}

protected void a200ReadT3584()
	{
		a210Begin();
	}

protected void a210Begin()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3584);
		descIO.setDescitem(sv.cltreln);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.cltrelnErr.set(errorsInner.w038);
		}
	}

protected void a300ContinueCheck()
	{
		a310Begin();
	}

protected void a310Begin()
	{
		if (isEQ(tr52zrec.gitem, SPACES)) {
			wsaaFound.set("N");
			return ;
		}
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52z);
		itemIO.setItemitem(tr52zrec.gitem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		tr52zrec.tr52zRec.set(itemIO.getGenarea());
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			if (isNE(tr52zrec.bnytype[ix.toInt()], SPACES)
			&& isEQ(tr52zrec.manopt[ix.toInt()], "Y")) {
				iz.add(1);
				for (iy.set(1); !(isGT(iy, 30)
				|| isEQ(wsaaMandatory[iz.toInt()], "Y")); iy.add(1)){
					wsaaMandatory[iz.toInt()].set("N");
					if (isEQ(tr52zrec.bnytype[ix.toInt()], wsaaType[iy.toInt()])) {
						wsaaMandatory[iz.toInt()].set("Y");
					}
				}
			}
		}
	}

protected void b100CallTr52y()
	{
		b100Begin();
	}

protected void b100Begin()
	{
		/* Read Beneficiary Type Table - TR52Y.                            */
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52y);
		itemIO.setItemitem(sv.bnytype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		tr52yrec.tr52yRec.set(itemIO.getGenarea());
	}

protected void b200MandRlsp()
	{
		b210Start();
	}

protected void b210Start()
	{
		/*  Check for Mandatory Beneficiary's relation using TR52Z.        */
		wsaaCrtBnfyreln.set("N");
		wsaaBeneTypeFound.set("N");
		wsaaValidRelation.set("N");
		if (isNE(sv.bnytype, SPACES)) {
			b300ReadTr52z();
		}
		if (isEQ(wsaaCrtBnfyreln, "N")
		&& validRelation.isTrue()
		&& isNE(sv.bnysel, wsaaClntnum)) {
			sv.cltrelnErr.set(errorsInner.rfk1);
		}
		if (beneTypeFound.isTrue()
		&& isEQ(tr52zrec.selfind[wsbbTableSub.toInt()], "Y")) {
			if (isEQ(sv.bnysel, wsaaClntnum)) {
				sv.cltreln.set("SELF");
			}
			else {
				if (isEQ(sv.cltreln, "SELF")) {
					sv.bnyselErr.set(errorsInner.rfk1);
				}
			}
		}
		if (beneTypeFound.isTrue()
		&& isNE(tr52zrec.selfind[wsbbTableSub.toInt()], "Y")) {
			if (isEQ(sv.bnysel, wsaaClntnum)
			&& noSelfRelation.isTrue()) {
				sv.bnyselErr.set(errorsInner.rfk3);
			}
		}
		if (beneTypeFound.isTrue()) {
			wsbbStackArrayInner.wsbbSelfind[scrnparams.subfileRrn.toInt()].set(tr52zrec.selfind[wsbbTableSub.toInt()]);
		}
		else {
			wsbbStackArrayInner.wsbbSelfind[scrnparams.subfileRrn.toInt()].set(SPACES);
		}
		if (isEQ(wsaaCrtBnfyreln, "Y")) {
			wsbbStackArrayInner.wsbbRevcflg[scrnparams.subfileRrn.toInt()].set(tr52zrec.revcflg[wsbbTableRsub.toInt()]);
			sv.revcflg.set(tr52zrec.revcflg[wsbbTableRsub.toInt()]);
		}
		else {
			wsbbStackArrayInner.wsbbRevcflg[scrnparams.subfileRrn.toInt()].set(SPACES);
		}
	}

protected void b300ReadTr52z()
	{
		b310Begin();
	}

protected void b310Begin()
	{
		wsaaFound.set("Y");
		bentypeflag = false;
		/* Read the beneficiary type details from TR52Z for the            */
		/* contract type held on CHDRLNB.                                  */
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52z);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setParams(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52z);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52zrec.tr52zRec.set(itemIO.getGenarea());
		/* Find the benef type in the table by checking the current table  */
		/* and if not found checking subsequent tables if they exist.      */
		wsbbTableSub.set(1);
		wsbbTableRsub.set(1);
		while ( !(isEQ(tr52zrec.bnytype[wsbbTableSub.toInt()], sv.bnytype)
		|| isNE(wsaaFound, "Y"))) {
			b400FindBnytype();
			if(!bentypeflag) {
				sv.bnytypeErr.set(errorsInner.rfl8);
			}
		}
		
		
		if (isEQ(sv.bnytype, tr52zrec.bnytype[wsbbTableSub.toInt()])) {
			beneTypeFound.setTrue();
		}
		/* Compute the subscript for the first relat and find the relat.   */
		/* Note the position of the bene type(found already above)         */
		/* determines the start position of the associated relationship.   */
		if (isNE(sv.cltreln, SPACES)
		&& beneTypeFound.isTrue()) {
			compute(wsbbTableRsub, 0).set(add(mult((sub(wsbbTableSub, 1)), 5), 1));
			while ( !(isEQ(tr52zrec.cltreln[wsbbTableRsub.toInt()], sv.cltreln)
			|| notFound.isTrue())) {
				b700FindRelation();
			}
			
		}
		if (isNE(sv.cltreln, SPACES)) {
			if (isEQ(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])) {
				validRelation.setTrue();
				wsaaCrtBnfyreln.set("Y");
			}
		}
		if (isNE(sv.cltreln, SPACES)) {
			wsaaSelfRelation.set("N");
			if (isEQ(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])
			&& isEQ(tr52zrec.cltreln[wsbbTableRsub.toInt()], "SELF")) {
				selfRelation.setTrue();
			}
		}
	}

protected void b400FindBnytype()
	{
		/*B410-BEGIN*/
		/* Actually find the BNYTYPE.                                      */
		for (wsbbTableSub.set(1); !(isGT(wsbbTableSub, 10)); wsbbTableSub.add(1)){
			b500Nothing();
		}
		/* If the type has not been found look in the continuation table   */
		if (isGT(wsbbTableSub, 10)
		|| isNE(tr52zrec.bnytype[wsbbTableSub.toInt()], sv.bnytype)) {
			wsbbTableSub.set(1);
			b600ContinuationTable();
		}
		/*B490-EXIT*/
	}

protected void b500Nothing()
	{
		/*B510-ABSOLUTLY-NOTHING*/
		if(isEQ(tr52zrec.bnytype[wsbbTableSub.toInt()], sv.bnytype)) {
			bentypeflag = true;
		}
		/*B590-EXIT*/
	}

protected void b600ContinuationTable()
	{
		b610ContinuationTable();
	}

protected void b610ContinuationTable()
	{
		/* Read the beneficiary type details from continuation table       */
		/* Last entry on the table is found and the beneficiary type       */
		/* looked for has not been found.                                  */
		if (isEQ(tr52zrec.gitem, SPACES)) {
			wsaaFound.set("N");
			return ;
		}
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52z);
		itemIO.setItemitem(tr52zrec.gitem);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		tr52zrec.tr52zRec.set(itemIO.getGenarea());
	}

protected void b700FindRelation()
	{
		try {
			b710Begin();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void b710Begin()
	{
		/* Search through each line of relationship in turn                */
		while ( !(isEQ(wsbbTableRsub, 50)
		|| isEQ(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])
		|| notFound.isTrue())) {
			b770SearchLine();
		}
		
		/*  If not found on this table entry, try the next one.            */
		if (isNE(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])) {
			wsbbTableSub.set(1);
			wsbbTableRsub.set(1);
			b600ContinuationTable();
		}
		goTo(GotoLabel.b790Exit);
	}

	/**
	* <pre>
	*  Check a single line of riders (performed paragraphs).          
	* </pre>
	*/
protected void b770SearchLine()
	{
		for (int loopVar1 = 0; !(loopVar1 == 5); loopVar1 += 1){
			b780CheckElement();
		}
		if (isNE(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])) {
			if (isNE(wsbbTableSub, 10)) {
				wsbbTableSub.add(1);
				if (isNE(tr52zrec.bnytype[wsbbTableSub.toInt()], SPACES)) {
					wsaaFound.set("N");
				}
			}
		}
	}

protected void b780CheckElement()
	{
		if (isNE(tr52zrec.cltreln[wsbbTableRsub.toInt()], SPACES)) {
			validRelation.setTrue();
		}
		if (isNE(sv.cltreln, tr52zrec.cltreln[wsbbTableRsub.toInt()])
		&& isNE(wsbbTableRsub, 50)) {
			wsbbTableRsub.add(1);
		}
	}

protected void cltsio2900()
	{
		/*CLTSIO*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT1*/
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/* Remove old beneficiary from client role                         */
		bnfylnbIO.setParams(SPACES);
		bnfylnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		bnfylnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		bnfylnbIO.setFunction(varcom.begn);
		while ( !(isEQ(bnfylnbIO.getStatuz(), varcom.endp))) {
			removeBnClrr3600();
		}
		
		bnfylnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		bnfylnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, wsaaSubfileSize)); wsaaIndex.add(1)){
			update3100();
		}
		/* Add latest beneficiary to client role                           */
		scrnparams.function.set(varcom.sstrt);
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			addBnClrr3900();
		}
		
		if(NBPRP117permission)
		{
			List<Bnfypf> bnfylnbIOList = bnfypfDAO.searchBnfypfRecord(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
			if (bnfylnbIOList!=null && !bnfylnbIOList.isEmpty()) {
				for (Bnfypf bnfylnbIO : bnfylnbIOList) {
					benNo.add(bnfylnbIO.getBnyclt().trim());
				}
					
				List<Fluppf> fluppfobj = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
				if (fluppfobj!=null && !fluppfobj.isEmpty()) {	
					for(Fluppf fluppf: fluppfobj)
				    {
						if(isEQ(fluppf.getFupCde().trim(),beneficiary.trim()))	
				     	{
							if(!benNo.contains(fluppf.getFupRmk().substring(23, 31)))
							{
								fluppfDAO.deleteRecord(chdrlnbIO.getChdrnum().toString(), beneficiary.trim(), fluppf.getFupRmk().substring(23, 31));
					   						
							}
							/*
							else
							{
								fluppfDAO.deleteRecord(chdrlnbIO.getChdrnum().toString(), beneficiary.trim(), fluppf.getFupRmk().substring(23, 31));
			    		
							}*/
				     	}
				    }
				}
				/*}*/
			}
		}
		
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	* </pre>
	*/
protected void update3100()
	{
		/*UPDATE*/
		if (isNE(wsbbStackArrayInner.wsbbChange[wsaaIndex.toInt()], "Y")) {
			checkIfRcdChanged3110();
			return ;
		}
		checkChange3200();
		updateRecord3300();
		/*EXIT*/
	}

protected void checkIfRcdChanged3110()
	{
		/*PARA*/
		if (isNE(wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()], chdrlnbIO.getOccdate())
		&& isNE(wsbbStackArrayInner.wsbbEffdate[wsaaIndex.toInt()], varcom.vrcmMaxDate)) {
			bnfylnbIO.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()]);
			bnfylnbIO.setEffdate(chdrlnbIO.getOccdate());
			bnfylnbIO.setFunction(varcom.rewrt);
			callBnfylnbio3800();
		}
		/*EXIT*/
	}

protected void checkChange3200()
	{
		checkChange3220();
	}

protected void checkChange3220()
	{
		wsaaUpdateFlag.set(SPACES);
		scrnparams.subfileRrn.set(wsaaIndex);
		scrnparams.function.set(varcom.sread);
		callScreenIo5100();
		/* INDEX IS LESS THAN NUMBER OF FILE READS*/
		/* SO SCREEN FIELDS HAVE BEEN SPACED OUT = DELETE*/
		if (((isEQ(sv.bnycd, " "))
		&& (isEQ(sv.cltreln, SPACES))
		&& (isEQ(sv.bnysel, SPACES))
		&& (isEQ(sv.bnypc, 0))
		&& (isEQ(sv.bnytype, SPACES))
		&& (isEQ(sv.effdate, varcom.vrcmMaxDate)))
		&& (isLTE(wsaaIndex, wsaaNofRead))) {
			sv.relto.set(SPACES);
			wsaaUpdateFlag.set("DEL");
			return ;
		}
		/* AGENT SCREEN FIELD DIFFERENT FROM HIDDEN = ADD*/
		if ((isNE(sv.bnysel, wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()]))
		&& (isGT(wsaaIndex, wsaaNofRead))) {
			wsaaUpdateFlag.set("ADD");
			return ;
		}
		/* BEN'Y SCREEN FIELD DIFFERENT FROM HIDDEN = DELETE OLD*/
		/*                                            ADD NEW*/
		if (isEQ(sv.bnysel, SPACES)
		&& isEQ(wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()], SPACES)) {
			wsaaUpdateFlag.set(SPACES);
			return ;
		}
		if ((isNE(sv.bnysel, wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()]))
		&& (isLTE(wsaaIndex, wsaaNofRead))) {
			wsaaUpdateFlag.set("CHG");
			return ;
		}
		if (isEQ(sv.bnysel, wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()])) {
			wsaaUpdateFlag.set("MOD");
			return ;
		}
	}

protected void updateRecord3300()
	{
		para3300();
	}

protected void para3300()
	{
		if (isEQ(wsaaUpdateFlag, SPACES)) {
			return ;
		}
		if (wsaaDelete.isTrue()) {
			bnfylnbIO.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()]);
			bnfylnbIO.setBnytype(wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()]);
			/*     MOVE SPACES TO S5010-BNYNAME                             */
			/*     MOVE SPACES TO S5010-CLNTNM                      <V76L01>*/
			sv.clntsname.set(SPACES);
			bnfylnbIO.setFunction(varcom.begn);
			callBnfylnbio3800();
			/*     MOVE DELET TO BNFYLNB-FUNCTION                           */
			bnfylnbIO.setFunction(varcom.deltd);
			callBnfylnbio3800();
			return ;
		}
		if (wsaaModify.isTrue()) {
			bnfylnbIO.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()]);
			bnfylnbIO.setBnytype(wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()]);
			bnfylnbIO.setFunction(varcom.begn);
			callBnfylnbio5000();
			bnfylnbIO.setBnycd(sv.bnycd);
			bnfylnbIO.setBnypc(sv.bnypc);
			/*     MOVE S5010-BNYRLN           TO BNFYLNB-BNYRLN            */
			bnfylnbIO.setCltreln(sv.cltreln);
			bnfylnbIO.setCurrfrom(datcon1rec.intDate);
			/*     MOVE CHDRLNB-OCCDATE         TO BNFYLNB-EFFDATE  <A05873>*/
			bnfylnbIO.setEffdate(sv.effdate);
			bnfylnbIO.setBnytype(sv.bnytype);
			bnfylnbIO.setEnddate(sv.enddate);
			if(benesequence){
			bnfylnbIO.setSequence(sv.sequence);//fwang3 ICIL-4
			}
			/*        PERFORM B100-CALL-TR52Y                                  */
			/*        MOVE TR52Y-RELTO            TO BNFYLNB-RELTO             */
			bnfylnbIO.setRelto(sv.relto);
			/*     MOVE REWRT TO BNFYLNB-FUNCTION                           */
			bnfylnbIO.setFunction(varcom.writd);
			callBnfylnbio3800();
			
			if(NBPRP117permission)
			  {
					beneficiaryNo.set(wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()].toString());
					datcon1rec.function.set(varcom.tday);
					Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
					if (isNE(datcon1rec.statuz,varcom.oK)) {
						syserrrec.params.set(datcon1rec.datcon1Rec);
						syserrrec.statuz.set(datcon1rec.statuz);
						fatalError600();
					}
					wsaaToday.set(datcon1rec.intDate);
					wsaaT5661Lang.set(wsspcomn.language);
					wsaaT5661Fupcode.set(beneficiary.trim());
			
					readT5661();
					
					fupDescpf=descDAO.getdescData("IT", "T5661", wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
					
					clntpf = new Clntpf();
					clntpf.setClntpfx("CN");
					clntpf.setClntcoy(wsspcomn.fsuco.toString());
					clntpf.setClntnum(beneficiaryNo.toString().trim());
					clntpf = clntpfDAO.getCltsRecordByKey(clntpf); 
					
					initialize(antisoclkey.antisocialKey);
					antisoclkey.kjSName.set(clntpf.getLsurname());
					antisoclkey.kjGName.set(clntpf.getLgivname());
					antisoclkey.clntype.set(clntpf.getClttype());

					
					callProgram(Antisocl.class, antisoclkey.antisocialKey);
					
					List<Fluppf> fluppfobj = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
					
					if (isNE(antisoclkey.statuz, varcom.oK)) {
						
						
						fupOldno.set(0);
						fupno.set(0);
						
						if(fluppfobj.size()>0)
						{
							
							int i=0;
					    	for(Fluppf fluppf: fluppfobj)
						    {
					    		
						    	if(isEQ(fluppf.getFupCde().trim(),beneficiary.trim()))	
						     	{
						    		i++;
						    		fluppfDAO.deleteRecord(chdrlnbIO.getChdrnum().toString(), beneficiary.trim(), sv.bnysel.toString().trim());
						    		
									List<Fluppf> fluppfobj1 = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
									if(fluppfobj1.size()>=1)
									{
									  fupOldno.set(fluppfobj1.get(fluppfobj1.size()-1).getFupNo());
									  fupno.add(fupOldno);
									}
									else
									{
										fupno.add(fupOldno);
									}
						    		updateFluppf();
						    		break;
							    }
						    }
						    	if(i==0)
						    	{
						    		fupOldno.set(fluppfobj.get(fluppfobj.size()-1).getFupNo());
									fupno.add(fupOldno);
						    		updateFluppf();
						    	}
					    	
						}
						else
						{
							fupno.add(fupOldno);
							updateFluppf();
						}
						
						
				  }
			  }
			return ;
		}
		if (wsaaAdd.isTrue()) {
			addSplit3400();
			return ;
		}
		if (wsaaChange.isTrue()) {
			bnfylnbIO.setBnyclt(wsbbStackArrayInner.wsbbBnysel[wsaaIndex.toInt()]);
			bnfylnbIO.setBnytype(wsbbStackArrayInner.wsbbBnytype[wsaaIndex.toInt()]);
			if (isEQ(wsaaDuprFlag, "N")) {
				bnfylnbIO.setFunction(varcom.readh);
				callBnfylnbio3800();
				bnfylnbIO.setFunction(varcom.delet);
				callBnfylnbio3800();
			}
		}
		wsaaDuprFlag = "N";
		addSplit3400();
	}

protected void addSplit3400()
	{
		addSplit3420();
		init3450();
	}

protected void addSplit3420()
	{
		bnfylnbIO.setParams(SPACES);
		bnfylnbIO.setFunction(varcom.writr);
		bnfylnbIO.setStatuz(varcom.oK);
		bnfylnbIO.setFormat(bnfylnbrec);
		bnfylnbIO.setBnyclt(sv.bnysel);
		/* MOVE S5010-BNYRLN  TO BNFYLNB-BNYRLN.                        */
		bnfylnbIO.setCltreln(sv.cltreln);
		bnfylnbIO.setBnytype(sv.bnytype);
		bnfylnbIO.setRelto(sv.relto);
		bnfylnbIO.setBnycd(sv.bnycd);
		bnfylnbIO.setBnypc(sv.bnypc);
		bnfylnbIO.setSelfind(wsbbStackArrayInner.wsbbSelfind[wsaaIndex.toInt()]);
		bnfylnbIO.setRevcflg(sv.revcflg);
		bnfylnbIO.setEffdate(sv.effdate);
		bnfylnbIO.setEnddate(sv.enddate);
		if(benesequence){
		bnfylnbIO.setSequence(sv.sequence); //fwang3 ICIL-4
		}
		
	}

protected void init3450()
	{
		bnfylnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		bnfylnbIO.setTranno(chdrlnbIO.getTranno());
		bnfylnbIO.setValidflag("1");
		bnfylnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		/* MOVE CHDRLNB-CURRTO TO BNFYLNB-CURRTO.                       */
		/* MOVE CHDRLNB-CURRFROM TO BNFYLNB-CURRFROM.                   */
		bnfylnbIO.setCurrto(varcom.vrcmMaxDate);
		bnfylnbIO.setCurrfrom(datcon1rec.intDate);
		/* MOVE CHDRLNB-OCCDATE         TO BNFYLNB-EFFDATE.     <A05873>*/
		bnfylnbIO.setTermid(varcom.vrcmTermid);
		bnfylnbIO.setUser(varcom.vrcmUser);
		bnfylnbIO.setTransactionTime(varcom.vrcmTime);
		bnfylnbIO.setTransactionDate(varcom.vrcmDate);
		callBnfylnbio3800();
		
		
		if(NBPRP117permission)
		  {
				
				datcon1rec.function.set(varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				if (isNE(datcon1rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon1rec.datcon1Rec);
					syserrrec.statuz.set(datcon1rec.statuz);
					fatalError600();
				}
				wsaaToday.set(datcon1rec.intDate);
				wsaaT5661Lang.set(wsspcomn.language);
				wsaaT5661Fupcode.set(beneficiary.trim());
		
				readT5661();
				
				fupDescpf=descDAO.getdescData("IT", "T5661", wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
				
				clntpf = new Clntpf();
				clntpf.setClntpfx("CN");
				clntpf.setClntcoy(wsspcomn.fsuco.toString());
				clntpf.setClntnum(sv.bnysel.toString());
				clntpf = clntpfDAO.getCltsRecordByKey(clntpf); 
				
				initialize(antisoclkey.antisocialKey);
				antisoclkey.kjSName.set(clntpf.getLsurname());
				antisoclkey.kjGName.set(clntpf.getLgivname());
				antisoclkey.clntype.set(clntpf.getClttype());

				
				callProgram(Antisocl.class, antisoclkey.antisocialKey);
				
				List<Fluppf> fluppfobj = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
				
				
				if (isNE(antisoclkey.statuz, varcom.oK)) {
					
					fupOldno.set(0);
					fupno.set(0);
					
					if(fluppfobj.size()>0)
					{
						
						int i=0;
				    	for(Fluppf fluppf: fluppfobj)
					    {
				    		
					    	if(isEQ(fluppf.getFupCde().trim(),beneficiary.trim()))	
					     	{
					    		i++;
					    		fluppfDAO.deleteRecord(chdrlnbIO.getChdrnum().toString(), beneficiary.trim(), sv.bnysel.toString().trim());
					    		
								List<Fluppf> fluppfobj1 = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
								if(fluppfobj1.size()>=1)
								{
								  fupOldno.set(fluppfobj1.get(fluppfobj1.size()-1).getFupNo());
								  fupno.add(fupOldno);
								}
								else
								{
									fupno.add(fupOldno);
								}
					    		insertFluppf();
					    		break;
						    }
					    }
					    	if(i==0)
					    	{
					    		fupOldno.set(fluppfobj.get(fluppfobj.size()-1).getFupNo());
								fupno.add(fupOldno);
								insertFluppf();
					    	}
				    	
					}
					else
					{
						fupno.add(fupOldno);
						insertFluppf();
					}
					
					
			  }
		  }
	}


protected void insertFluppf()
{
	name.set(clntpf.getLsurname().trim().replace(".", "").replace("-", "").concat(clntpf.getLgivname().trim().replace(".", "").replace("-", "")));
	ansopflist = ansopfDAO.getRecord(name.toString().trim());
	
	
	if(ansopflist.size()>0)
	{
		for(Ansopf ansopf : ansopflist)
		 {
			
			fupno.add(1);
			fluppf = new Fluppf();
			
			fluppf.setChdrcoy(wsspcomn.company.charat(0));
			fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
			fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
			fluppf.setFupTyp(clntpf.getClttype().charAt(0));
			fluppf.setLife("01");
			fluppf.setjLife("");
			fluppf.setFupNo(fupno.toInt());
			fluppf.setFupCde(beneficiary.trim());
			fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
			fluppf.setFupDt(wsaaToday.toInt());
			fluppf.setFupRmk(fupDescpf.getLongdesc().trim() + ansopf.getAnsonum() +"/"+sv.bnysel);
			fluppf.setClamNum("");
			fluppf.setUserT(varcom.vrcmUser.toInt());
			fluppf.setTermId(varcom.vrcmTermid.toString());
			fluppf.setTrdt(varcom.vrcmDate.toInt());
			fluppf.setTrtm(varcom.vrcmTime.toInt());
			fluppf.setzAutoInd(' ');
			fluppf.setUsrPrf(varcom.vrcmUser.toString());
			fluppf.setJobNm(wsspcomn.userid.toString());
			fluppf.setEffDate(wsaaToday.toInt());
			fluppf.setCrtUser(varcom.vrcmUser.toString());
			fluppf.setCrtDate(wsaaToday.toInt());
			fluppf.setLstUpUser(varcom.vrcmUser.toString());
			fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
			fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
			fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
			fluppfDAO.insertFlupRecord(fluppf);
					 
		}
	}
}


protected void callBnfylnbio3800()
	{
		callBnfylnbioPara3800();
	}

protected void callBnfylnbioPara3800()
	{
		bnfylnbIO.setFormat(bnfylnbrec);
		SmartFileCode.execute(appVars, bnfylnbIO);
		if (isNE(bnfylnbIO.getStatuz(), varcom.oK)
		&& isNE(bnfylnbIO.getStatuz(), varcom.dupr)) {
			syserrrec.params.set(bnfylnbIO.getParams());
			fatalError600();
		}
		if (isEQ(bnfylnbIO.getStatuz(), varcom.dupr)) {
			sv.bnyselErr.set(errorsInner.e048);
			wsspcomn.edterror.set("Y");
			wsaaDuprFlag = "Y";
			wsaaBnypcTotal.subtract(wsbbStackArrayInner.wsbbBnypc[scrnparams.subfileRrn.toInt()]);
			wsbbStackArrayInner.wsbbBnypc[scrnparams.subfileRrn.toInt()].set(ZERO);
			scrnparams.function.set(varcom.supd);
			callScreenIo5100();
		}
	}

protected void readT5661()
{
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(t5661);
	itempf.setItemitem(wsaaT5661Key.toString());
	itempf = itempfDAO.getItempfRecord(itempf);

	if (itempf == null) {
		syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat("T5661").concat(wsaaT5661Key.toString()));
		fatalError600();
	}
	t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));

}

protected void updateFluppf()
{
	name.set(clntpf.getLsurname().trim().replace(".", "").replace("-", "").concat(clntpf.getLgivname().trim().replace(".", "").replace("-", "")));
	ansopflist = ansopfDAO.getRecord(name.toString().trim());
	
	
	if(ansopflist.size()>0)
	{
		for(Ansopf ansopf : ansopflist)
		 {
			
			fupno.add(1);
			fluppf = new Fluppf();
			
			fluppf.setChdrcoy(wsspcomn.company.charat(0));
			fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
			fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
			fluppf.setFupTyp(clntpf.getClttype().charAt(0));
			fluppf.setLife("01");
			fluppf.setjLife("");
			fluppf.setFupNo(fupno.toInt());
			fluppf.setFupCde(beneficiary.trim());
			fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
			fluppf.setFupDt(wsaaToday.toInt());
			fluppf.setFupRmk(fupDescpf.getLongdesc().trim()  + " " + ansopf.getAnsonum() +"/"+ beneficiaryNo.trim());
			fluppf.setClamNum("");
			fluppf.setUserT(varcom.vrcmUser.toInt());
			fluppf.setTermId(varcom.vrcmTermid.toString());
			fluppf.setTrdt(varcom.vrcmDate.toInt());
			fluppf.setTrtm(varcom.vrcmTime.toInt());
			fluppf.setzAutoInd(' ');
			fluppf.setUsrPrf(varcom.vrcmUser.toString());
			fluppf.setJobNm(wsspcomn.userid.toString());
			fluppf.setEffDate(wsaaToday.toInt());
			fluppf.setCrtUser(varcom.vrcmUser.toString());
			fluppf.setCrtDate(wsaaToday.toInt());
			fluppf.setLstUpUser(varcom.vrcmUser.toString());
			fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
			fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
			fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
			fluppfDAO.insertFlupRecord(fluppf);
					 
		}
	}
}

protected void removeBnClrr3600()
	{
		start3610();
	}

protected void start3610()
	{
		SmartFileCode.execute(appVars, bnfylnbIO);
		if (isNE(bnfylnbIO.getStatuz(), varcom.oK)
		&& isNE(bnfylnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(bnfylnbIO.getParams());
			fatalError600();
		}
		if (isEQ(bnfylnbIO.getStatuz(), varcom.endp)
		|| isNE(bnfylnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(bnfylnbIO.getChdrnum(), chdrlnbIO.getChdrnum())) {
			bnfylnbIO.setStatuz(varcom.endp);
			return ;
		}
		bnfylnbIO.setFunction(varcom.nextr);
		/* Remove beneficiary client role                                  */
		wsaaClntkey.clntClntpfx.set(fsupfxcpy.clnt);
		wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
		wsaaClntkey.clntClntnum.set(bnfylnbIO.getBnyclt());
		wsaaChdrkey.chdrChdrpfx.set(fsupfxcpy.chdr);
		wsaaChdrkey.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		wsaaChdrkey.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		wsaaCltrelnFunction = "DEL";
		callCltreln3700();
	}

protected void callCltreln3700()
	{
		start3710();
	}

protected void start3710()
	{
		cltrelnrec.clntpfx.set(wsaaClntkey.clntClntpfx);
		cltrelnrec.clntcoy.set(wsaaClntkey.clntClntcoy);
		cltrelnrec.clntnum.set(wsaaClntkey.clntClntnum);
		cltrelnrec.clrrrole.set(clntrlsrec.bnfy);
		cltrelnrec.forepfx.set(wsaaChdrkey.chdrChdrpfx);
		cltrelnrec.forecoy.set(wsaaChdrkey.chdrChdrcoy);
		cltrelnrec.forenum.set(wsaaChdrkey.chdrChdrnum);
		cltrelnrec.function.set(wsaaCltrelnFunction);
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)
		&& isNE(cltrelnrec.statuz, "F968")
		&& isNE(cltrelnrec.statuz, "F962")) {
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

protected void addBnClrr3900()
	{
		start3910();
	}

protected void start3910()
	{
		processScreen("S5010", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.function.set(varcom.srdn);
		if (isEQ(scrnparams.statuz, "ENDP")
		|| isEQ(sv.bnysel, SPACES)) {
			return ;
		}
		/* Add beneficiary client role                                     */
		wsaaClntkey.clntClntpfx.set(fsupfxcpy.clnt);
		wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
		wsaaClntkey.clntClntnum.set(sv.bnysel);
		wsaaChdrkey.chdrChdrpfx.set(fsupfxcpy.chdr);
		wsaaChdrkey.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		wsaaChdrkey.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		wsaaCltrelnFunction = "ADD";
		callCltreln3700();
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		wsspcomn.cessdte.set(cessdate);
		/*EXIT*/
	}

	public int getCessdate() {
		return cessdate;
	}
	public void setCessdate(int cessdate) {
		this.cessdate = cessdate;
	}
	
	public ChdrlnbTableDAM getChdrlnbIO() {
		return chdrlnbIO;
	}
	public void setChdrlnbIO(ChdrlnbTableDAM chdrlnbIO) {
		this.chdrlnbIO = chdrlnbIO;
	}
	public Datcon2rec getDatcon2rec() {
		return datcon2rec;
	}
	public void setDatcon2rec(Datcon2rec datcon2rec) {
		this.datcon2rec = datcon2rec;
	}
	public String getWsaaFlag() {
		return wsaaFlag;
	}
	public void setWsaaFlag(String wsaaFlag) {
		this.wsaaFlag = wsaaFlag;
	}
	public FixedLengthStringData getWsaaNoDets() {
		return wsaaNoDets;
	}
	public void setWsaaNoDets(FixedLengthStringData wsaaNoDets) {
		this.wsaaNoDets = wsaaNoDets;
	}
	public CltsTableDAM getCltsIO() {
		return cltsIO;
	}
	public void setCltsIO(CltsTableDAM cltsIO) {
		this.cltsIO = cltsIO;
	}
	public Tr52yrec getTr52yrec() {
		return tr52yrec;
	}
	public void setTr52yrec(Tr52yrec tr52yrec) {
		this.tr52yrec = tr52yrec;
	}
	public FixedLengthStringData getWsaaClntnum() {
		return wsaaClntnum;
	}
	public void setWsaaClntnum(FixedLengthStringData wsaaClntnum) {
		this.wsaaClntnum = wsaaClntnum;
	}
	public LifelnbTableDAM getLifelnbIO() {
		return lifelnbIO;
	}
	public void setLifelnbIO(LifelnbTableDAM lifelnbIO) {
		this.lifelnbIO = lifelnbIO;
	}
	public BinaryData getWsaaDuprSub() {
		return wsaaDuprSub;
	}
	public void setWsaaDuprSub(BinaryData wsaaDuprSub) {
		this.wsaaDuprSub = wsaaDuprSub;
	}
	public Validator getFound() {
		return found;
	}
	public void setFound(Validator found) {
		this.found = found;
	}
	public Tr52zrec getTr52zrec() {
		return tr52zrec;
	}
	public void setTr52zrec(Tr52zrec tr52zrec) {
		this.tr52zrec = tr52zrec;
	}
	public ZonedDecimalData getWsbbTableSub() {
		return wsbbTableSub;
	}
	public void setWsbbTableSub(ZonedDecimalData wsbbTableSub) {
		this.wsbbTableSub = wsbbTableSub;
	}
	public FixedLengthStringData[] getWsaaClient() {
		return wsaaClient;
	}
	public void setWsaaClient(FixedLengthStringData[] wsaaClient) {
		this.wsaaClient = wsaaClient;
	}
	public FixedLengthStringData[] getWsaaType() {
		return wsaaType;
	}
	public void setWsaaType(FixedLengthStringData[] wsaaType) {
		this.wsaaType = wsaaType;
	}
	public ZonedDecimalData getIb() {
		return ib;
	}
	public void setIb(ZonedDecimalData ib) {
		this.ib = ib;
	}
	public ZonedDecimalData getWsaaPctSub() {
		return wsaaPctSub;
	}
	public void setWsaaPctSub(ZonedDecimalData wsaaPctSub) {
		this.wsaaPctSub = wsaaPctSub;
	}
	public FixedLengthStringData[] getWsaaPctType() {
		return wsaaPctType;
	}
	public void setWsaaPctType(FixedLengthStringData[] wsaaPctType) {
		this.wsaaPctType = wsaaPctType;
	}
	public ZonedDecimalData[] getWsaaPctTot() {
		return wsaaPctTot;
	}
	public void setWsaaPctTot(ZonedDecimalData[] wsaaPctTot) {
		this.wsaaPctTot = wsaaPctTot;
	}
	public ZonedDecimalData[] getWsaaBnfyCount() {
		return wsaaBnfyCount;
	}
	public void setWsaaBnfyCount(ZonedDecimalData[] wsaaBnfyCount) {
		this.wsaaBnfyCount = wsaaBnfyCount;
	}
/*
 * Class transformed  from Data Structure WSBB-STACK-ARRAY--INNER
 */
private static final class WsbbStackArrayInner { 

	private FixedLengthStringData wsbbStackArray = new FixedLengthStringData(2370+60);
	private FixedLengthStringData wsbbBnytypeArray = new FixedLengthStringData(60).isAPartOf(wsbbStackArray, 0);
	private FixedLengthStringData[] wsbbBnytype = FLSArrayPartOfStructure(30, 2, wsbbBnytypeArray, 0);
	private FixedLengthStringData wsbbBnyrlnArray = new FixedLengthStringData(120).isAPartOf(wsbbStackArray, 60);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbBnyrln = FLSArrayPartOfStructure(30, 4, wsbbBnyrlnArray, 0);
	private FixedLengthStringData wsbbReltoArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 180);
	private FixedLengthStringData[] wsbbRelto = FLSArrayPartOfStructure(30, 1, wsbbReltoArray, 0);
	private FixedLengthStringData wsbbRevcflgArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 210);
	private FixedLengthStringData[] wsbbRevcflg = FLSArrayPartOfStructure(30, 1, wsbbRevcflgArray, 0);
	private FixedLengthStringData wsbbBnycdArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 240);
		/*                             OCCURS 10.               <V76L01>*/
	private FixedLengthStringData[] wsbbBnycd = FLSArrayPartOfStructure(30, 1, wsbbBnycdArray, 0);
	private FixedLengthStringData wsbbBnypcArray = new FixedLengthStringData(150).isAPartOf(wsbbStackArray, 270);
		/*                             OCCURS 10.               <V76L01>*/
	private ZonedDecimalData[] wsbbBnypc = ZDArrayPartOfStructure(30, 5, 2, wsbbBnypcArray, 0, UNSIGNED_TRUE);
	private FixedLengthStringData wsbbBnyselArray = new FixedLengthStringData(300).isAPartOf(wsbbStackArray, 420);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbBnysel = FLSArrayPartOfStructure(30, 10, wsbbBnyselArray, 0);
	/*
	 * fwang3 ICIL-4
	 */
	protected FixedLengthStringData wsbbSequenceArray = new FixedLengthStringData(60).isAPartOf(wsbbStackArray, 420+300);
	protected FixedLengthStringData[] wsbbSequence = FLSArrayPartOfStructure(30, 2, wsbbSequenceArray, 0);
	
		/* 03  WSBB-BNYNAME-ARRAY.                                      
		                             OCCURS 10.                       */
	private FixedLengthStringData wsbbClntnmArray = new FixedLengthStringData(1110).isAPartOf(wsbbStackArray, 720+60);
		/*                             OCCURS 10.               <V76L01>*/
	private FixedLengthStringData[] wsbbClntnm = FLSArrayPartOfStructure(30, 37, wsbbClntnmArray, 0);
	private FixedLengthStringData wsbbEffdateArray = new FixedLengthStringData(240).isAPartOf(wsbbStackArray, 1830+60);
		/*                             OCCURS 10.               <V76L01>*/
	private PackedDecimalData[] wsbbEffdate = PDArrayPartOfStructure(30, 8, 0, wsbbEffdateArray, 0);
	private FixedLengthStringData wsbbEnddateArray = new FixedLengthStringData(240).isAPartOf(wsbbStackArray, 2070+60);
	/*                             OCCURS 10.               <V76L01>*/
private PackedDecimalData[] wsbbEnddate = PDArrayPartOfStructure(30, 8, 0, wsbbEnddateArray, 0);
	private FixedLengthStringData wsbbChangeArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2310+60);
		/*                             OCCURS 10.                       */
	private FixedLengthStringData[] wsbbChange = FLSArrayPartOfStructure(30, 1, wsbbChangeArray, 0);
	private FixedLengthStringData wsbbSelfindArray = new FixedLengthStringData(30).isAPartOf(wsbbStackArray, 2340+60);
	private FixedLengthStringData[] wsbbSelfind = FLSArrayPartOfStructure(30, 1, wsbbSelfindArray, 0);
	
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e048 = new FixedLengthStringData(4).init("E048");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e518 = new FixedLengthStringData(4).init("E518");
	private FixedLengthStringData e631 = new FixedLengthStringData(4).init("E631");
	private FixedLengthStringData f782 = new FixedLengthStringData(4).init("F782");
	private FixedLengthStringData e058 = new FixedLengthStringData(4).init("E058");
	private FixedLengthStringData e470 = new FixedLengthStringData(4).init("E470");
	private FixedLengthStringData f348 = new FixedLengthStringData(4).init("F348");
	private FixedLengthStringData h046 = new FixedLengthStringData(4).init("H046");
	private FixedLengthStringData rfk0 = new FixedLengthStringData(4).init("RFK0");
	private FixedLengthStringData rfk1 = new FixedLengthStringData(4).init("RFK1");
	private FixedLengthStringData rfk2 = new FixedLengthStringData(4).init("RFK2");
	private FixedLengthStringData rfk3 = new FixedLengthStringData(4).init("RFK3");
	private FixedLengthStringData rfk4 = new FixedLengthStringData(4).init("RFK4");
	private FixedLengthStringData rfk6 = new FixedLengthStringData(4).init("RFK6");
	private FixedLengthStringData w038 = new FixedLengthStringData(4).init("W038");
	private FixedLengthStringData rfwq = new FixedLengthStringData(4).init("RFWQ");
	private FixedLengthStringData rfwr = new FixedLengthStringData(4).init("RFWR");
	private FixedLengthStringData rfwt = new FixedLengthStringData(4).init("RFWT");
	private FixedLengthStringData rppi = new FixedLengthStringData(4).init("RPPI");
	private FixedLengthStringData rrej = new FixedLengthStringData(4).init("RREJ");
	private FixedLengthStringData rrlu = new FixedLengthStringData(4).init("RRLU");//ILIFE-7646
	private FixedLengthStringData rfl8 = new FixedLengthStringData(4).init("RFL8");
}
}
