/*
 * File: Bh561.java
 * Date: 29 August 2009 21:33:49
 * Author: Quipoz Limited
 *
 * Class transformed from BH561.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.PtrnenqTableDAM;
import com.csc.fsu.general.recordstructures.Ptrnxtrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrincTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrtrxTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrafiTableDAM;
import com.csc.life.newbusiness.dataaccess.ZcovrtxTableDAM;
import com.csc.life.newbusiness.dataaccess.ZstreffTableDAM;
import com.csc.life.newbusiness.dataaccess.ZstrrevTableDAM;
import com.csc.life.newbusiness.procedures.Zptrnxt;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.newbusiness.tablestructures.Th566rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  ZSTRPF EXTRACT PROGRAM.
*
*  This program perfroms extraction of data from various data
*  file like PTRN, CHDR, AGNT, COVR and updates extract records
*  in ZSTRPF for use in various new business report printing.
*
*  1000-Initialise section.
*  ------------------------
*  - call DATCON1 subroutine with function TDAY and set up
*    WSAA-TODAY.
*
*  2000-Read-File section.
*  -----------------------
*  - call ZPTRNXT subroutine to read PTRN records.
*  - if ZPTRNXT returns statii other than O-K or ENDP, display
*    error HL51 - Error in ZPTRNXT.
*  - if ZPTRNXT returns statuz of ENDP, set WSSP-EDTERROR to
*    ENDP and exit section.
*  - add control total 1 for total number of record extracted by
*    ZPTRNXT.
*  - Move PTNX-PTRN-DATA (returned by ZPTRNXT) to PTRN-DATA-AREA.
*  - if PTRN-VALIDFLAG is '2', skip record, get next ptrn record,
*    adding 1 to control total 2.
*
*  2500-Edit section.
*  ------------------
*  - Set WSSP-EDTERROR to O-K.
*  - Read CHDRTRX for contract which matches that extracted in
*    ZPTRNXT, i.e. CHDRCOY, CHDRNUM and the tranno which is
*    equal or next higher than than of PTRN record.
*  - Read AGLF using keys from CHDRTRX.
*  - Read TH566 for transaction direction.
*  - Read TH506 for basic plan coverage.
*  - For forward transaction:
*    .  Loop through ZCOVRTX using keys from CHDRTRX and accumulat
*       the sum insured, single premium and instalment premium.
*       .  For each ZCOVRTX record, read the previous transaction
*          number record in COVR using logical COVRAFI, if exist,
*          the validflag of this previous record should be '2'.
*          Accumulate the previous amounts accordingly.
*    .  Compute:
*         Final SUMINS = total SUMINS - total previous SUMINS.
*         Final SINGP  = total SINGP - total previous SINGP.
*         Final INSTPREM = total INSTPREM - total previous INSTPRE
*    .  Write a ZSTRPF record using logical ZSTREFF with values
*       of final sum insured, single premium and instalment premiu
*  - For reverse transaction:
*    .  Loop through PTRNENQ using contract company, number and
*       transaction number from ZPTRNXT.  Obtain the first valid
*       flag '2' record.
*    .  Continue to loop through PTRNENQ until a change in key
*       and or until valid flag is not = '2'.  Store the tranno
*       of the last valid flag '2' record in WSAA-TRANNO.
*    .  Using this transaction number and the contract company
*       and number from ZPTRNXT, loop through ZSTRREV, for each
*       record in ZSTRREV, write a corresponding record in
*       ZSTRPF using logical ZSTREFF with the sum insured, single
*       premium and instalment premium values multiplied by -1.
*
* Control Totals:
* ---------------
* CT01  -  Number of ZPTRNXT records returned
* CT02  -  Number of ZPTRNXT skipped (validflag = 2)
* CT03  -  Number of record skipped (contract header not found)
* CT04  -  Number of record skipped (COVR not exist)
* CT05  -  Number of ZSTR written
* CT06  -  Number of records not reversed (no PTRN validflag '2')
* CT07  -  Number of ZPTRNXT record with reversal done
* CT08  -  Number of ZPTRNXT record not reversed (no ZSTR)
*
*****************************************************************
* </pre>
*/
public class Bh561 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH561");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaEffectiveDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaEffYr = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffectiveDate, 0);
	private ZonedDecimalData wsaaEffMth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffectiveDate, 4);
	private ZonedDecimalData wsaaEffDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffectiveDate, 6);
	private ZonedDecimalData wsaaSingp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaInstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSumins = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaPrevSingp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaPrevInstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaPrevSumins = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaPrevTranno = new ZonedDecimalData(5, 0);
	private ZonedDecimalData wsaaFinalSingp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFinalInstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFinalSumins = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaTranno = new ZonedDecimalData(5, 0);
	private String wsaaSkipCovr = "";
	private String wsaaSkipReverse = "";

	private FixedLengthStringData wsaaFirstRead = new FixedLengthStringData(1);
	private Validator firstRead = new Validator(wsaaFirstRead, "Y");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* FORMATS */
	private String aglf = "AGLFREC";
	private String chdrtrxrec = "CHDRTRXREC";
	private String covrafirec = "COVRAFIREC";
	private String itemrec = "ITEMREC";
	private String ptrnrec = "PTRNREC";
	private String zcovrtxrec = "ZCOVRTXREC";
	private String zstreffrec = "ZSTREFFREC";
	private String zstrrevrec = "ZSTRREVREC";
		/* ERRORS */
	private String hl51 = "HL51";
	private String hl23 = "HL23";
		/* TABLES */
	private String th566 = "TH566";
	private String th506 = "TH506";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private int ct08 = 8;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Contract Header by Transaction Number*/
	private ChdrtrxTableDAM chdrtrxIO = new ChdrtrxTableDAM();
		/*Alter from Inception view of COVR file*/
	private CovrafiTableDAM covrafiIO = new CovrafiTableDAM();
		/*Coverage/Rider details - Increases*/
	private CovrincTableDAM covrincIO = new CovrincTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*Contract Enquiry - Transactions File.*/
	private PtrnenqTableDAM ptrnenqIO = new PtrnenqTableDAM();
	private Ptrnxtrec ptrnxtrec = new Ptrnxtrec();
	private Th506rec th506rec = new Th506rec();
	private Th566rec th566rec = new Th566rec();
		/*Coverage/Rider Dets - New Business Repor*/
	private ZcovrtxTableDAM zcovrtxIO = new ZcovrtxTableDAM();
		/*New Bus. Reporting by transaction code*/
	private ZstreffTableDAM zstreffIO = new ZstreffTableDAM();
		/*New Bus. Report by contract & tran. numb*/
	private ZstrrevTableDAM zstrrevIO = new ZstrrevTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readFile2010,
		exit2090,
		exit2590,
		exit2690,
		exit2809,
		exit2859,
		exit2869,
		exit2879,
		exit3109,
		exit3159,
		exit3259
	}

	public Bh561() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case readFile2010: {
					readFile2010();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		ptrnxtrec.ptrnxtRec.set(SPACES);
		callProgram(Zptrnxt.class, ptrnxtrec.ptrnxtRec, lsaaBsscrec, lsaaBprdrec);
		if (isNE(ptrnxtrec.statuz,varcom.oK)
		&& isNE(ptrnxtrec.statuz,varcom.endp)) {
			syserrrec.statuz.set(hl51);
			ptrnxtrec.statuz.set(hl51);
			syserrrec.params.set(ptrnxtrec.ptrnxtRec);
			fatalError600();
		}
		if (isEQ(ptrnxtrec.statuz,varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		ptrnIO.setDataArea(ptrnxtrec.ptrnData);
		if (isEQ(ptrnIO.getValidflag(),"2")) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.readFile2010);
		}
	}

protected void edit2500()
	{
		try {
			edit2510();
		}
		catch (GOTOException e){
		}
	}

protected void edit2510()
	{
		wsspEdterror.set(varcom.oK);
		readChdr2600();
		if (isEQ(chdrtrxIO.getStatuz(),varcom.endp)) {
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.exit2590);
		}
		readAgnt2700();
		transactionDirection2750();
		readTableTh5062760();
		if (isEQ(th566rec.ztranind,"F")) {
			readCovr2800();
			if (isEQ(wsaaSkipCovr,"Y")) {
				goTo(GotoLabel.exit2590);
			}
			setupZstrpf2900();
			contractFee2870();
			writeZstrpf2910();
		}
		else {
			findLastForwardTxn3100();
			if (isEQ(wsaaSkipReverse,"Y")) {
				goTo(GotoLabel.exit2590);
			}
			writeReverseZstrs3200();
		}
	}

protected void readChdr2600()
	{
		try {
			chdr2600();
		}
		catch (GOTOException e){
		}
	}

protected void chdr2600()
	{
		chdrtrxIO.setDataKey(SPACES);
		chdrtrxIO.setStatuz(varcom.oK);
		chdrtrxIO.setChdrcoy(ptrnIO.getChdrcoy());
		chdrtrxIO.setChdrnum(ptrnIO.getChdrnum());
		chdrtrxIO.setTranno(ptrnIO.getTranno());
		chdrtrxIO.setFormat(chdrtrxrec);
		chdrtrxIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrtrxIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrtrxIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)
		&& isNE(chdrtrxIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(chdrtrxIO.getStatuz());
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrtrxIO.getStatuz(),varcom.endp)
		|| isNE(chdrtrxIO.getChdrcoy(),ptrnIO.getChdrcoy())
		|| isNE(chdrtrxIO.getChdrnum(),ptrnIO.getChdrnum())) {
			chdrtrxIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2690);
		}
	}

protected void readAgnt2700()
	{
		/*AGENT*/
		aglfIO.setDataKey(SPACES);
		aglfIO.setAgntcoy(chdrtrxIO.getAgntcoy());
		aglfIO.setAgntnum(chdrtrxIO.getAgntnum());
		aglfIO.setFormat(aglf);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void transactionDirection2750()
	{
		transDir2750();
	}

protected void transDir2750()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrtrxIO.getChdrcoy());
		itemIO.setItemtabl(th566);
		itemIO.setItemitem(ptrnIO.getBatctrcde());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th566rec.th566Rec.set(itemIO.getGenarea());
		if (isNE(th566rec.ztranind,"F")
		&& isNE(th566rec.ztranind,"R")) {
			syserrrec.statuz.set(hl23);
			itemIO.setStatuz(hl23);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void readTableTh5062760()
	{
		readTh5062760();
	}

protected void readTh5062760()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrtrxIO.getChdrcoy());
		itemIO.setItemtabl(th506);
		itemIO.setItemitem(chdrtrxIO.getCnttype());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		if (isEQ(th506rec.crtable,SPACES)) {
			findBasicPlan2770();
		}
	}

protected void findBasicPlan2770()
	{
		find2771();
	}

protected void find2771()
	{
		covrincIO.setParams(SPACES);
		covrincIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		covrincIO.setChdrnum(chdrtrxIO.getChdrnum());
		covrincIO.setPlanSuffix(ZERO);
		covrincIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrincIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrincIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covrincIO.getStatuz(),varcom.endp))) {
			SmartFileCode.execute(appVars, covrincIO);
			if (isNE(covrincIO.getStatuz(),varcom.oK)
			&& isNE(covrincIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(covrincIO.getParams());
				fatalError600();
			}
			if (isNE(covrincIO.getChdrcoy(),chdrtrxIO.getChdrcoy())
			|| isNE(covrincIO.getChdrnum(),chdrtrxIO.getChdrnum())
			|| isEQ(covrincIO.getStatuz(),varcom.endp)) {
				covrincIO.setStatuz(varcom.endp);
			}
			else {
				if (isEQ(covrincIO.getRider(),"00")) {
					th506rec.crtable.set(covrincIO.getCrtable());
					covrincIO.setStatuz(varcom.endp);
				}
				else {
					covrincIO.setFunction(varcom.nextr);
				}
			}
		}

	}

protected void readCovr2800()
	{
		try {
			covr2800();
		}
		catch (GOTOException e){
		}
	}

protected void covr2800()
	{
		wsaaSumins.set(ZERO);
		wsaaSingp.set(ZERO);
		wsaaInstprem.set(ZERO);
		wsaaPrevSumins.set(ZERO);
		wsaaPrevInstprem.set(ZERO);
		wsaaPrevSingp.set(ZERO);
		zcovrtxIO.setStatuz(varcom.oK);
		zcovrtxIO.setDataKey(SPACES);
		zcovrtxIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		zcovrtxIO.setChdrnum(chdrtrxIO.getChdrnum());
		zcovrtxIO.setTranno(ptrnIO.getTranno());
		zcovrtxIO.setPlanSuffix(ZERO);
		zcovrtxIO.setFormat(zcovrtxrec);
		zcovrtxIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zcovrtxIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zcovrtxIO.setFitKeysSearch("CHDRCOY", "CHDRNUM","TRANNO");
		wsaaFirstRead.set("Y");
		wsaaSkipCovr = "N";
		while ( !(isEQ(zcovrtxIO.getStatuz(),varcom.endp))) {
			loopThroughCovrs2850();
		}

		if (isEQ(wsaaSkipCovr,"Y")) {
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
			goTo(GotoLabel.exit2809);
		}
		compute(wsaaFinalInstprem, 2).set(sub(wsaaInstprem,wsaaPrevInstprem));
		compute(wsaaFinalSumins, 2).set(sub(wsaaSumins,wsaaPrevSumins));
		compute(wsaaFinalSingp, 2).set(sub(wsaaSingp,wsaaPrevSingp));
	}

protected void loopThroughCovrs2850()
	{
		try {
			loopCovrs2850();
		}
		catch (GOTOException e){
		}
	}

protected void loopCovrs2850()
	{
		SmartFileCode.execute(appVars, zcovrtxIO);
		if (isNE(zcovrtxIO.getStatuz(),varcom.oK)
		&& isNE(zcovrtxIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zcovrtxIO.getStatuz());
			syserrrec.params.set(zcovrtxIO.getParams());
			fatalError600();
		}
		if (isEQ(zcovrtxIO.getStatuz(),varcom.endp)
		|| isNE(zcovrtxIO.getChdrcoy(),chdrtrxIO.getChdrcoy())
		|| isNE(zcovrtxIO.getChdrnum(),chdrtrxIO.getChdrnum())
		|| isNE(zcovrtxIO.getTranno(),ptrnIO.getTranno())) {
			if (firstRead.isTrue()) {
				wsaaSkipCovr = "Y";
			}
			zcovrtxIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2859);
		}
		if (firstRead.isTrue()) {
			wsaaFirstRead.set("N");
		}
		if (isEQ(zcovrtxIO.getCrtable(),th506rec.crtable)) {
			wsaaSumins.add(zcovrtxIO.getSumins());
		}
		if (isEQ(chdrtrxIO.getBillfreq(),"00")) {
			wsaaSingp.add(zcovrtxIO.getInstprem());
		}
		else {
			wsaaInstprem.add(zcovrtxIO.getInstprem());
		}
		wsaaSingp.add(zcovrtxIO.getSingp());
		previousCovr2860();
		zcovrtxIO.setFunction(varcom.nextr);
	}

protected void previousCovr2860()
	{
		try {
			prevCovr2860();
		}
		catch (GOTOException e){
		}
	}

protected void prevCovr2860()
	{
		covrafiIO.setDataKey(SPACES);
		covrafiIO.setStatuz(varcom.oK);
		covrafiIO.setChdrcoy(zcovrtxIO.getChdrcoy());
		covrafiIO.setChdrnum(zcovrtxIO.getChdrnum());
		covrafiIO.setLife(zcovrtxIO.getLife());
		covrafiIO.setCoverage(zcovrtxIO.getCoverage());
		covrafiIO.setRider(zcovrtxIO.getRider());
		covrafiIO.setPlanSuffix(zcovrtxIO.getPlanSuffix());
		setPrecision(covrafiIO.getTranno(), 0);
		covrafiIO.setTranno(sub(zcovrtxIO.getTranno(),1));
		covrafiIO.setFunction(varcom.endr);
		covrafiIO.setFormat(covrafirec);
		SmartFileCode.execute(appVars, covrafiIO);
		if (isNE(covrafiIO.getStatuz(),varcom.oK)
		&& isNE(covrafiIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrafiIO.getParams());
			syserrrec.statuz.set(covrafiIO.getStatuz());
			fatalError600();
		}
		if (isEQ(covrafiIO.getStatuz(),varcom.endp)
		|| isNE(covrafiIO.getChdrcoy(),zcovrtxIO.getChdrcoy())
		|| isNE(covrafiIO.getChdrnum(),zcovrtxIO.getChdrnum())
		|| isNE(covrafiIO.getLife(),zcovrtxIO.getLife())
		|| isNE(covrafiIO.getCoverage(),zcovrtxIO.getCoverage())
		|| isNE(covrafiIO.getRider(),zcovrtxIO.getRider())
		|| isNE(covrafiIO.getPlanSuffix(),zcovrtxIO.getPlanSuffix())) {
			goTo(GotoLabel.exit2869);
		}
		if (isNE(covrafiIO.getValidflag(),"2")) {
			syserrrec.statuz.set(covrafiIO.getStatuz());
			syserrrec.params.set(covrafiIO.getParams());
			fatalError600();
		}
		if (isEQ(covrafiIO.getCrtable(),th506rec.crtable)) {
			wsaaPrevSumins.add(covrafiIO.getSumins());
		}
		if (isEQ(chdrtrxIO.getBillfreq(),"00")) {
			wsaaPrevSingp.add(covrafiIO.getInstprem());
		}
		else {
			wsaaPrevInstprem.add(covrafiIO.getInstprem());
		}
		wsaaPrevSingp.add(covrafiIO.getSingp());
	}

protected void contractFee2870()
	{
		try {
			cntfee2870();
		}
		catch (GOTOException e){
		}
	}

protected void cntfee2870()
	{
		zstreffIO.setCntfee(ZERO);
		zstreffIO.setCntfee(chdrtrxIO.getSinstamt02());
		chdrtrxIO.setStatuz(varcom.oK);
		setPrecision(chdrtrxIO.getTranno(), 0);
		chdrtrxIO.setTranno(sub(chdrtrxIO.getTranno(),1));
		chdrtrxIO.setFormat(chdrtrxrec);
		chdrtrxIO.setFunction(varcom.endr);
		SmartFileCode.execute(appVars, chdrtrxIO);
		if (isNE(chdrtrxIO.getStatuz(),varcom.oK)
		&& isNE(chdrtrxIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(chdrtrxIO.getStatuz());
			syserrrec.params.set(chdrtrxIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrtrxIO.getStatuz(),varcom.endp)
		|| isNE(chdrtrxIO.getChdrcoy(),ptrnIO.getChdrcoy())
		|| isNE(chdrtrxIO.getChdrnum(),ptrnIO.getChdrnum())) {
			chdrtrxIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2879);
		}
		setPrecision(zstreffIO.getCntfee(), 2);
		zstreffIO.setCntfee(sub(zstreffIO.getCntfee(),chdrtrxIO.getSinstamt02()));
	}

protected void setupZstrpf2900()
	{
		writeZstr2900();
	}

protected void writeZstr2900()
	{
		zstreffIO.setDataArea(SPACES);
		zstreffIO.setChdrpfx(chdrtrxIO.getChdrpfx());
		zstreffIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		zstreffIO.setChdrnum(chdrtrxIO.getChdrnum());
		zstreffIO.setTranno(ptrnIO.getTranno());
		zstreffIO.setAgntnum(chdrtrxIO.getAgntnum());
		zstreffIO.setAracde(aglfIO.getAracde());
		zstreffIO.setCntbranch(chdrtrxIO.getCntbranch());
		zstreffIO.setCnttype(chdrtrxIO.getCnttype());
		zstreffIO.setStatcode(chdrtrxIO.getStatcode());
		zstreffIO.setPstatcode(chdrtrxIO.getPstatcode());
		zstreffIO.setCntcurr(chdrtrxIO.getCntcurr());
		zstreffIO.setSrcebus(chdrtrxIO.getSrcebus());
		zstreffIO.setBillfreq(chdrtrxIO.getBillfreq());
		zstreffIO.setBatctrcde(ptrnIO.getBatctrcde());
		zstreffIO.setEffdate(bsscIO.getEffectiveDate());
		zstreffIO.setSingp(wsaaFinalSingp);
		zstreffIO.setInstprem(wsaaFinalInstprem);
		zstreffIO.setSumins(wsaaFinalSumins);
		zstreffIO.setCommyr(wsaaEffYr);
		zstreffIO.setOccdate(chdrtrxIO.getOccdate());
		zstreffIO.setFormat(zstreffrec);
		zstreffIO.setFunction(varcom.writr);
	}

protected void writeZstrpf2910()
	{
		/*WRITE-ZSTR*/
		SmartFileCode.execute(appVars, zstreffIO);
		if (isNE(zstreffIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zstreffIO.getStatuz());
			syserrrec.params.set(zstreffIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct05);
		contotrec.totval.set(1);
		callContot001();
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void findLastForwardTxn3100()
	{
		try {
			lastForward3100();
		}
		catch (GOTOException e){
		}
	}

protected void lastForward3100()
	{
		wsaaTranno.set(ZERO);
		ptrnenqIO.setDataArea(SPACES);
		ptrnenqIO.setStatuz(varcom.oK);
		ptrnenqIO.setChdrcoy(ptrnIO.getChdrcoy());
		ptrnenqIO.setChdrnum(ptrnIO.getChdrnum());
		ptrnenqIO.setTranno(ptrnIO.getTranno());
		ptrnenqIO.setBatctrcde(SPACES);
		ptrnenqIO.setFormat(ptrnrec);
		ptrnenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ptrnenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaSkipReverse = "N";
		while ( !(isEQ(ptrnenqIO.getValidflag(),"2")
		|| isEQ(ptrnenqIO.getStatuz(),varcom.endp))) {
			callPtrnenq3150();
		}

		if (isEQ(ptrnenqIO.getStatuz(),varcom.endp)) {
			contotrec.totno.set(ct06);
			contotrec.totval.set(1);
			callContot001();
			wsaaSkipReverse = "Y";
			goTo(GotoLabel.exit3109);
		}
		while ( !(isEQ(ptrnenqIO.getStatuz(),varcom.endp)
		|| isNE(ptrnenqIO.getValidflag(),"2"))) {
			wsaaTranno.set(ptrnenqIO.getTranno());
			callPtrnenq3150();
		}

	}

protected void callPtrnenq3150()
	{
		try {
			ptrnenqio3150();
		}
		catch (GOTOException e){
		}
	}

protected void ptrnenqio3150()
	{
		SmartFileCode.execute(appVars, ptrnenqIO);
		if (isNE(ptrnenqIO.getStatuz(),varcom.oK)
		&& isNE(ptrnenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(ptrnenqIO.getStatuz());
			syserrrec.params.set(ptrnenqIO.getParams());
			fatalError600();
		}
		if (isEQ(ptrnenqIO.getStatuz(),varcom.endp)
		|| isNE(ptrnenqIO.getChdrcoy(),ptrnIO.getChdrcoy())
		|| isNE(ptrnenqIO.getChdrnum(),ptrnIO.getChdrnum())) {
			ptrnenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3159);
		}
		ptrnenqIO.setFunction(varcom.nextr);
	}

protected void writeReverseZstrs3200()
	{
		writeRevZstrs3200();
	}

protected void writeRevZstrs3200()
	{
		zstrrevIO.setParams(SPACES);
		zstrrevIO.setChdrcoy(ptrnIO.getChdrcoy());
		zstrrevIO.setChdrnum(ptrnIO.getChdrnum());
		zstrrevIO.setTranno(ptrnIO.getTranno());
		zstrrevIO.setFormat(zstrrevrec);
		zstrrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zstrrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zstrrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");

		wsaaFirstRead.set("Y");
		while ( !(isEQ(zstrrevIO.getStatuz(),varcom.endp))) {
			reversals3250();
		}

		if (firstRead.isTrue()) {
			contotrec.totno.set(ct08);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			contotrec.totno.set(ct07);
			contotrec.totval.set(1);
			callContot001();
		}
	}

protected void reversals3250()
	{
		try {
			reverse3250();
		}
		catch (GOTOException e){
		}
	}

protected void reverse3250()
	{
		SmartFileCode.execute(appVars, zstrrevIO);
		if (isNE(zstrrevIO.getStatuz(),varcom.oK)
		&& isNE(zstrrevIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zstrrevIO.getStatuz());
			syserrrec.params.set(zstrrevIO.getParams());
			fatalError600();
		}
		if (isEQ(zstrrevIO.getStatuz(),varcom.endp)
		|| isNE(zstrrevIO.getChdrcoy(),ptrnIO.getChdrcoy())
		|| isNE(zstrrevIO.getChdrnum(),ptrnIO.getChdrnum())
		|| isLT(zstrrevIO.getTranno(),wsaaTranno)) {
			zstrrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3259);
		}
		if (firstRead.isTrue()) {
			wsaaFirstRead.set("N");
		}
		zstreffIO.setParams(SPACES);
		zstreffIO.setChdrpfx(chdrtrxIO.getChdrpfx());
		zstreffIO.setChdrcoy(chdrtrxIO.getChdrcoy());
		zstreffIO.setChdrnum(chdrtrxIO.getChdrnum());
		zstreffIO.setTranno(ptrnIO.getTranno());
		zstreffIO.setAgntnum(chdrtrxIO.getAgntnum());
		zstreffIO.setAracde(aglfIO.getAracde());
		zstreffIO.setCntbranch(chdrtrxIO.getCntbranch());
		zstreffIO.setCnttype(chdrtrxIO.getCnttype());
		zstreffIO.setStatcode(chdrtrxIO.getStatcode());
		zstreffIO.setPstatcode(chdrtrxIO.getPstatcode());
		zstreffIO.setCntcurr(chdrtrxIO.getCntcurr());
		zstreffIO.setSrcebus(chdrtrxIO.getSrcebus());
		zstreffIO.setBillfreq(chdrtrxIO.getBillfreq());
		zstreffIO.setBatctrcde(ptrnIO.getBatctrcde());
		zstreffIO.setEffdate(bsscIO.getEffectiveDate());
		setPrecision(zstreffIO.getSingp(), 2);
		zstreffIO.setSingp(mult(zstrrevIO.getSingp(),-1));
		setPrecision(zstreffIO.getInstprem(), 2);
		zstreffIO.setInstprem(mult(zstrrevIO.getInstprem(),-1));
		setPrecision(zstreffIO.getSumins(), 2);
		zstreffIO.setSumins(mult(zstrrevIO.getSumins(),-1));
		setPrecision(zstreffIO.getCntfee(), 2);
		zstreffIO.setCntfee(mult(zstrrevIO.getCntfee(),-1));
		zstreffIO.setCommyr(wsaaEffYr);
		zstreffIO.setOccdate(chdrtrxIO.getOccdate());
		zstreffIO.setFormat(zstreffrec);
		zstreffIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zstreffIO);
		if (isNE(zstreffIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(zstreffIO.getStatuz());
			syserrrec.params.set(zstreffIO.getParams());
			fatalError600();
		}
		zstrrevIO.setFunction(varcom.nextr);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
