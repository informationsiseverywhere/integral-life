package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:04
 * Description:
 * Copybook name: TH539REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th539rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th539Rec = new FixedLengthStringData(652);
  	public FixedLengthStringData insprms = new FixedLengthStringData(594).isAPartOf(th539Rec, 0);
  	public ZonedDecimalData[] insprm = ZDArrayPartOfStructure(99, 6, 0, insprms, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(594).isAPartOf(insprms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData insprm01 = new ZonedDecimalData(6, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData insprm02 = new ZonedDecimalData(6, 0).isAPartOf(filler, 6);
  	public ZonedDecimalData insprm03 = new ZonedDecimalData(6, 0).isAPartOf(filler, 12);
  	public ZonedDecimalData insprm04 = new ZonedDecimalData(6, 0).isAPartOf(filler, 18);
  	public ZonedDecimalData insprm05 = new ZonedDecimalData(6, 0).isAPartOf(filler, 24);
  	public ZonedDecimalData insprm06 = new ZonedDecimalData(6, 0).isAPartOf(filler, 30);
  	public ZonedDecimalData insprm07 = new ZonedDecimalData(6, 0).isAPartOf(filler, 36);
  	public ZonedDecimalData insprm08 = new ZonedDecimalData(6, 0).isAPartOf(filler, 42);
  	public ZonedDecimalData insprm09 = new ZonedDecimalData(6, 0).isAPartOf(filler, 48);
  	public ZonedDecimalData insprm10 = new ZonedDecimalData(6, 0).isAPartOf(filler, 54);
  	public ZonedDecimalData insprm11 = new ZonedDecimalData(6, 0).isAPartOf(filler, 60);
  	public ZonedDecimalData insprm12 = new ZonedDecimalData(6, 0).isAPartOf(filler, 66);
  	public ZonedDecimalData insprm13 = new ZonedDecimalData(6, 0).isAPartOf(filler, 72);
  	public ZonedDecimalData insprm14 = new ZonedDecimalData(6, 0).isAPartOf(filler, 78);
  	public ZonedDecimalData insprm15 = new ZonedDecimalData(6, 0).isAPartOf(filler, 84);
  	public ZonedDecimalData insprm16 = new ZonedDecimalData(6, 0).isAPartOf(filler, 90);
  	public ZonedDecimalData insprm17 = new ZonedDecimalData(6, 0).isAPartOf(filler, 96);
  	public ZonedDecimalData insprm18 = new ZonedDecimalData(6, 0).isAPartOf(filler, 102);
  	public ZonedDecimalData insprm19 = new ZonedDecimalData(6, 0).isAPartOf(filler, 108);
  	public ZonedDecimalData insprm20 = new ZonedDecimalData(6, 0).isAPartOf(filler, 114);
  	public ZonedDecimalData insprm21 = new ZonedDecimalData(6, 0).isAPartOf(filler, 120);
  	public ZonedDecimalData insprm22 = new ZonedDecimalData(6, 0).isAPartOf(filler, 126);
  	public ZonedDecimalData insprm23 = new ZonedDecimalData(6, 0).isAPartOf(filler, 132);
  	public ZonedDecimalData insprm24 = new ZonedDecimalData(6, 0).isAPartOf(filler, 138);
  	public ZonedDecimalData insprm25 = new ZonedDecimalData(6, 0).isAPartOf(filler, 144);
  	public ZonedDecimalData insprm26 = new ZonedDecimalData(6, 0).isAPartOf(filler, 150);
  	public ZonedDecimalData insprm27 = new ZonedDecimalData(6, 0).isAPartOf(filler, 156);
  	public ZonedDecimalData insprm28 = new ZonedDecimalData(6, 0).isAPartOf(filler, 162);
  	public ZonedDecimalData insprm29 = new ZonedDecimalData(6, 0).isAPartOf(filler, 168);
  	public ZonedDecimalData insprm30 = new ZonedDecimalData(6, 0).isAPartOf(filler, 174);
  	public ZonedDecimalData insprm31 = new ZonedDecimalData(6, 0).isAPartOf(filler, 180);
  	public ZonedDecimalData insprm32 = new ZonedDecimalData(6, 0).isAPartOf(filler, 186);
  	public ZonedDecimalData insprm33 = new ZonedDecimalData(6, 0).isAPartOf(filler, 192);
  	public ZonedDecimalData insprm34 = new ZonedDecimalData(6, 0).isAPartOf(filler, 198);
  	public ZonedDecimalData insprm35 = new ZonedDecimalData(6, 0).isAPartOf(filler, 204);
  	public ZonedDecimalData insprm36 = new ZonedDecimalData(6, 0).isAPartOf(filler, 210);
  	public ZonedDecimalData insprm37 = new ZonedDecimalData(6, 0).isAPartOf(filler, 216);
  	public ZonedDecimalData insprm38 = new ZonedDecimalData(6, 0).isAPartOf(filler, 222);
  	public ZonedDecimalData insprm39 = new ZonedDecimalData(6, 0).isAPartOf(filler, 228);
  	public ZonedDecimalData insprm40 = new ZonedDecimalData(6, 0).isAPartOf(filler, 234);
  	public ZonedDecimalData insprm41 = new ZonedDecimalData(6, 0).isAPartOf(filler, 240);
  	public ZonedDecimalData insprm42 = new ZonedDecimalData(6, 0).isAPartOf(filler, 246);
  	public ZonedDecimalData insprm43 = new ZonedDecimalData(6, 0).isAPartOf(filler, 252);
  	public ZonedDecimalData insprm44 = new ZonedDecimalData(6, 0).isAPartOf(filler, 258);
  	public ZonedDecimalData insprm45 = new ZonedDecimalData(6, 0).isAPartOf(filler, 264);
  	public ZonedDecimalData insprm46 = new ZonedDecimalData(6, 0).isAPartOf(filler, 270);
  	public ZonedDecimalData insprm47 = new ZonedDecimalData(6, 0).isAPartOf(filler, 276);
  	public ZonedDecimalData insprm48 = new ZonedDecimalData(6, 0).isAPartOf(filler, 282);
  	public ZonedDecimalData insprm49 = new ZonedDecimalData(6, 0).isAPartOf(filler, 288);
  	public ZonedDecimalData insprm50 = new ZonedDecimalData(6, 0).isAPartOf(filler, 294);
  	public ZonedDecimalData insprm51 = new ZonedDecimalData(6, 0).isAPartOf(filler, 300);
  	public ZonedDecimalData insprm52 = new ZonedDecimalData(6, 0).isAPartOf(filler, 306);
  	public ZonedDecimalData insprm53 = new ZonedDecimalData(6, 0).isAPartOf(filler, 312);
  	public ZonedDecimalData insprm54 = new ZonedDecimalData(6, 0).isAPartOf(filler, 318);
  	public ZonedDecimalData insprm55 = new ZonedDecimalData(6, 0).isAPartOf(filler, 324);
  	public ZonedDecimalData insprm56 = new ZonedDecimalData(6, 0).isAPartOf(filler, 330);
  	public ZonedDecimalData insprm57 = new ZonedDecimalData(6, 0).isAPartOf(filler, 336);
  	public ZonedDecimalData insprm58 = new ZonedDecimalData(6, 0).isAPartOf(filler, 342);
  	public ZonedDecimalData insprm59 = new ZonedDecimalData(6, 0).isAPartOf(filler, 348);
  	public ZonedDecimalData insprm60 = new ZonedDecimalData(6, 0).isAPartOf(filler, 354);
  	public ZonedDecimalData insprm61 = new ZonedDecimalData(6, 0).isAPartOf(filler, 360);
  	public ZonedDecimalData insprm62 = new ZonedDecimalData(6, 0).isAPartOf(filler, 366);
  	public ZonedDecimalData insprm63 = new ZonedDecimalData(6, 0).isAPartOf(filler, 372);
  	public ZonedDecimalData insprm64 = new ZonedDecimalData(6, 0).isAPartOf(filler, 378);
  	public ZonedDecimalData insprm65 = new ZonedDecimalData(6, 0).isAPartOf(filler, 384);
  	public ZonedDecimalData insprm66 = new ZonedDecimalData(6, 0).isAPartOf(filler, 390);
  	public ZonedDecimalData insprm67 = new ZonedDecimalData(6, 0).isAPartOf(filler, 396);
  	public ZonedDecimalData insprm68 = new ZonedDecimalData(6, 0).isAPartOf(filler, 402);
  	public ZonedDecimalData insprm69 = new ZonedDecimalData(6, 0).isAPartOf(filler, 408);
  	public ZonedDecimalData insprm70 = new ZonedDecimalData(6, 0).isAPartOf(filler, 414);
  	public ZonedDecimalData insprm71 = new ZonedDecimalData(6, 0).isAPartOf(filler, 420);
  	public ZonedDecimalData insprm72 = new ZonedDecimalData(6, 0).isAPartOf(filler, 426);
  	public ZonedDecimalData insprm73 = new ZonedDecimalData(6, 0).isAPartOf(filler, 432);
  	public ZonedDecimalData insprm74 = new ZonedDecimalData(6, 0).isAPartOf(filler, 438);
  	public ZonedDecimalData insprm75 = new ZonedDecimalData(6, 0).isAPartOf(filler, 444);
  	public ZonedDecimalData insprm76 = new ZonedDecimalData(6, 0).isAPartOf(filler, 450);
  	public ZonedDecimalData insprm77 = new ZonedDecimalData(6, 0).isAPartOf(filler, 456);
  	public ZonedDecimalData insprm78 = new ZonedDecimalData(6, 0).isAPartOf(filler, 462);
  	public ZonedDecimalData insprm79 = new ZonedDecimalData(6, 0).isAPartOf(filler, 468);
  	public ZonedDecimalData insprm80 = new ZonedDecimalData(6, 0).isAPartOf(filler, 474);
  	public ZonedDecimalData insprm81 = new ZonedDecimalData(6, 0).isAPartOf(filler, 480);
  	public ZonedDecimalData insprm82 = new ZonedDecimalData(6, 0).isAPartOf(filler, 486);
  	public ZonedDecimalData insprm83 = new ZonedDecimalData(6, 0).isAPartOf(filler, 492);
  	public ZonedDecimalData insprm84 = new ZonedDecimalData(6, 0).isAPartOf(filler, 498);
  	public ZonedDecimalData insprm85 = new ZonedDecimalData(6, 0).isAPartOf(filler, 504);
  	public ZonedDecimalData insprm86 = new ZonedDecimalData(6, 0).isAPartOf(filler, 510);
  	public ZonedDecimalData insprm87 = new ZonedDecimalData(6, 0).isAPartOf(filler, 516);
  	public ZonedDecimalData insprm88 = new ZonedDecimalData(6, 0).isAPartOf(filler, 522);
  	public ZonedDecimalData insprm89 = new ZonedDecimalData(6, 0).isAPartOf(filler, 528);
  	public ZonedDecimalData insprm90 = new ZonedDecimalData(6, 0).isAPartOf(filler, 534);
  	public ZonedDecimalData insprm91 = new ZonedDecimalData(6, 0).isAPartOf(filler, 540);
  	public ZonedDecimalData insprm92 = new ZonedDecimalData(6, 0).isAPartOf(filler, 546);
  	public ZonedDecimalData insprm93 = new ZonedDecimalData(6, 0).isAPartOf(filler, 552);
  	public ZonedDecimalData insprm94 = new ZonedDecimalData(6, 0).isAPartOf(filler, 558);
  	public ZonedDecimalData insprm95 = new ZonedDecimalData(6, 0).isAPartOf(filler, 564);
  	public ZonedDecimalData insprm96 = new ZonedDecimalData(6, 0).isAPartOf(filler, 570);
  	public ZonedDecimalData insprm97 = new ZonedDecimalData(6, 0).isAPartOf(filler, 576);
  	public ZonedDecimalData insprm98 = new ZonedDecimalData(6, 0).isAPartOf(filler, 582);
  	public ZonedDecimalData insprm99 = new ZonedDecimalData(6, 0).isAPartOf(filler, 588);
  	public ZonedDecimalData instpr = new ZonedDecimalData(6, 0).isAPartOf(th539Rec, 594);
  	public ZonedDecimalData mfacthm = new ZonedDecimalData(5, 4).isAPartOf(th539Rec, 600);
  	public ZonedDecimalData mfacthy = new ZonedDecimalData(5, 4).isAPartOf(th539Rec, 605);
  	public ZonedDecimalData mfactm = new ZonedDecimalData(5, 4).isAPartOf(th539Rec, 610);
  	public ZonedDecimalData mfactq = new ZonedDecimalData(5, 4).isAPartOf(th539Rec, 615);
  	public ZonedDecimalData mfactw = new ZonedDecimalData(5, 4).isAPartOf(th539Rec, 620);
  	public ZonedDecimalData mfact2w = new ZonedDecimalData(5, 4).isAPartOf(th539Rec, 625);
  	public ZonedDecimalData mfact4w = new ZonedDecimalData(5, 4).isAPartOf(th539Rec, 630);
  	public ZonedDecimalData mfacty = new ZonedDecimalData(5, 4).isAPartOf(th539Rec, 635);
  	public ZonedDecimalData premUnit = new ZonedDecimalData(3, 0).isAPartOf(th539Rec, 640);
  	public ZonedDecimalData unit = new ZonedDecimalData(9, 0).isAPartOf(th539Rec, 643);


	public void initialize() {
		COBOLFunctions.initialize(th539Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th539Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}