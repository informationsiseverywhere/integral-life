package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;

import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CovtpfDAOImpl extends BaseDAOImpl<Covtpf> implements CovtpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CovtpfDAOImpl.class);
	@Override
	public Covtpf getCovtridData(String chdrcoy, String chdrnum, String life,String coverage, String crtable) {
		Covtpf covtpf = null;
		PreparedStatement psCovtSelect = null;
		ResultSet sqlcovtpf1rs = null;
		StringBuilder sqlCovtSelectBuilder = new StringBuilder();
		sqlCovtSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
		sqlCovtSelectBuilder.append(" JOBNM, UNIQUE_NUMBER ");
		sqlCovtSelectBuilder.append(" FROM COVTRID WHERE CHDRCOY = ? AND CHDRNUM = ? and LIFE=? and COVERAGE=? and CRTABLE=? ");
		sqlCovtSelectBuilder.append(" ORDER BY ");
		sqlCovtSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		psCovtSelect = getPrepareStatement(sqlCovtSelectBuilder.toString());
		try {
			psCovtSelect.setString(1, chdrcoy);
			psCovtSelect.setString(2, chdrnum);
			psCovtSelect.setString(3, life);
			psCovtSelect.setString(4, coverage);
			psCovtSelect.setString(5, crtable);
			sqlcovtpf1rs = executeQuery(psCovtSelect);
			while (sqlcovtpf1rs.next()) {
				covtpf = new Covtpf();
				covtpf.setChdrcoy(sqlcovtpf1rs.getString(1));
				covtpf.setChdrnum(sqlcovtpf1rs.getString(2));
				covtpf.setLife(sqlcovtpf1rs.getString(3));
				covtpf.setCoverage(sqlcovtpf1rs.getString(4));
				covtpf.setRider(sqlcovtpf1rs.getString(5));
				covtpf.setCrtable(sqlcovtpf1rs.getString(6));
				covtpf.setUsrprf(sqlcovtpf1rs.getString(7));
				covtpf.setJobnm(sqlcovtpf1rs.getString(8));
				covtpf.setUniqueNumber(sqlcovtpf1rs.getLong(9));
			}

		} catch (SQLException e) {
			LOGGER.error("getCovrridData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlcovtpf1rs);
		}
		return covtpf;
	}
	
	
	public List<Covtpf> getCovtridData(String chdrcoy, String chdrnum, String life,String coverage) {
		Covtpf covtpf = null;
		PreparedStatement psCovtSelect = null;
		ResultSet sqlcovtpf1rs = null;
		List<Covtpf> covtridList = new ArrayList<Covtpf>();
		StringBuilder sqlCovtSelectBuilder = new StringBuilder();
		sqlCovtSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
		sqlCovtSelectBuilder.append(" JOBNM, UNIQUE_NUMBER ");
		sqlCovtSelectBuilder.append(" FROM COVTRID WHERE CHDRCOY = ? AND CHDRNUM= ? and LIFE=? and COVERAGE=? ");
		sqlCovtSelectBuilder.append(" ORDER BY ");
		sqlCovtSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, UNIQUE_NUMBER DESC");
		psCovtSelect = getPrepareStatement(sqlCovtSelectBuilder.toString());
		try {
			/*psCovtSelect.setString(1, chdrcoy.trim());
			psCovtSelect.setString(2, chdrnum.trim());
			psCovtSelect.setString(3, life.trim());
			psCovtSelect.setString(4, coverage.trim());*/
			psCovtSelect.setString(1, StringUtil.fillSpace(chdrcoy.trim(), DD.chdrcoy.length)); 
			psCovtSelect.setString(2, StringUtil.fillSpace(chdrnum.trim(), DD.chdrnum.length)); 
			psCovtSelect.setString(3, StringUtil.fillSpace(life.trim(), DD.life.length)); 
			psCovtSelect.setString(4, StringUtil.fillSpace(coverage.trim(), DD.coverage.length)); 
			sqlcovtpf1rs = executeQuery(psCovtSelect);
			while (sqlcovtpf1rs.next()) {
				covtpf = new Covtpf();
				covtpf.setChdrcoy(sqlcovtpf1rs.getString(1));
				covtpf.setChdrnum(sqlcovtpf1rs.getString(2));
				covtpf.setLife(sqlcovtpf1rs.getString(3));
				covtpf.setCoverage(sqlcovtpf1rs.getString(4));
				covtpf.setRider(sqlcovtpf1rs.getString(5));
				covtpf.setCrtable(sqlcovtpf1rs.getString(6));
				covtpf.setUsrprf(sqlcovtpf1rs.getString(7));
				covtpf.setJobnm(sqlcovtpf1rs.getString(8));
				covtpf.setUniqueNumber(sqlcovtpf1rs.getLong(9));
				covtridList.add(covtpf);
			}

		} catch (SQLException e) {
			LOGGER.error("getCovrridData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlcovtpf1rs);
		}
		return covtridList;
	}
	public List<Covtpf> getCovtcovData(String chdrcoy, String chdrnum, String life, String crtable) {
		Covtpf covtpf = null;
		PreparedStatement psCovtSelect = null;
		ResultSet sqlcovtpf1rs = null;
		StringBuilder sqlCovtSelectBuilder = new StringBuilder();
		List<Covtpf> list = new ArrayList<Covtpf>();
		sqlCovtSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
		sqlCovtSelectBuilder.append(" JOBNM, UNIQUE_NUMBER ");
		sqlCovtSelectBuilder.append(" FROM COVTCOV WHERE CHDRCOY = ? AND CHDRNUM = ? and LIFE=?  and CRTABLE=? ");

		psCovtSelect = getPrepareStatement(sqlCovtSelectBuilder.toString());

		try {
			psCovtSelect.setString(1, chdrcoy);
			psCovtSelect.setString(2, chdrnum);
			psCovtSelect.setString(3, life);
			psCovtSelect.setString(4, crtable);
			sqlcovtpf1rs = executeQuery(psCovtSelect);
			while (sqlcovtpf1rs.next()) {
				covtpf = new Covtpf();
				covtpf.setChdrcoy(sqlcovtpf1rs.getString(1));
				covtpf.setChdrnum(sqlcovtpf1rs.getString(2));
				covtpf.setLife(sqlcovtpf1rs.getString(3));
				covtpf.setCoverage(sqlcovtpf1rs.getString(4));
				covtpf.setRider(sqlcovtpf1rs.getString(5));
				covtpf.setCrtable(sqlcovtpf1rs.getString(6));
				covtpf.setUsrprf(sqlcovtpf1rs.getString(7));
				covtpf.setJobnm(sqlcovtpf1rs.getString(8));
				covtpf.setUniqueNumber(sqlcovtpf1rs.getLong(9));
				list.add(covtpf);
			}

		} catch (SQLException e) {
			LOGGER.error("getCovrridData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlcovtpf1rs);
		}
		return list;
	}

	public List<Covtpf> getCovtpfData(String chdrcoy,String chdrnum,String life,String coverage,String rider, int planSuffix){
		Covtpf covtpf = null;
		PreparedStatement psCovtSelect = null;
		ResultSet sqlcovtpf1rs = null;
		StringBuilder sqlCovtSelectBuilder = new StringBuilder();
		List<Covtpf> covtList = new ArrayList<Covtpf>();
		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlCovtSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
		sqlCovtSelectBuilder.append(" JOBNM, UNIQUE_NUMBER,PLNSFX, ");
		sqlCovtSelectBuilder.append(" TERMID, TRDT, TRTM, CRTABLE, USER_T, ");
		sqlCovtSelectBuilder.append(" RCESDTE, PCESDTE, RCESAGE, PCESAGE, RCESTRM,");
		sqlCovtSelectBuilder.append(" PCESTRM, SUMINS, LIENCD, MORTCLS, JLIFE,");
		sqlCovtSelectBuilder.append(" RSUNIN, RUNDTE, POLINC, NUMAPP, EFFDATE,");
		sqlCovtSelectBuilder.append(" SEX01, SEX02, ANBCCD01, ANBCCD02, BILLFREQ,");
		sqlCovtSelectBuilder.append(" BILLCHNL, SEQNBR, SINGP, INSTPREM,");
		sqlCovtSelectBuilder.append(" PAYRSEQNO, CNTCURR, BCESAGE, BCESTRM, BCESDTE,");
		sqlCovtSelectBuilder.append(" BAPPMETH, ZBINSTPREM, ZLINSTPREM, ZDIVOPT, PAYCOY,");
		sqlCovtSelectBuilder.append(" PAYCLT, PAYMTH, BANKKEY, BANKACCKEY, PAYCURR,");
		sqlCovtSelectBuilder.append(" FACTHOUS, DATIME, LOADPER, ");
		sqlCovtSelectBuilder.append(" RATEADJ, FLTMORT, PREMADJ, AGEADJ ");
		sqlCovtSelectBuilder.append(" FROM COVTPF WHERE CHDRCOY = ? AND CHDRNUM = ? and LIFE=? and COVERAGE=?");
		sqlCovtSelectBuilder.append(" and rider=? and PLNSFX=?");
		sqlCovtSelectBuilder.append(" ORDER BY ");
		sqlCovtSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC ");

		psCovtSelect = getPrepareStatement(sqlCovtSelectBuilder.toString());

		try {
			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			psCovtSelect.setString(1, chdrcoy);
			psCovtSelect.setString(2, chdrnum);
			psCovtSelect.setString(3, life);
			psCovtSelect.setString(4, coverage);
			psCovtSelect.setString(5, rider);
			psCovtSelect.setInt(6, planSuffix);

			// ---------------------------------
			// Execute Query
			// ---------------------------------
			sqlcovtpf1rs = executeQuery(psCovtSelect);
			while (sqlcovtpf1rs.next()) {
				covtpf = new Covtpf();
				covtpf.setUniqueNumber(sqlcovtpf1rs.getInt("UNIQUE_NUMBER"));
				covtpf.setChdrcoy(sqlcovtpf1rs.getString("CHDRCOY"));
				covtpf.setChdrnum(sqlcovtpf1rs.getString("CHDRNUM"));
				covtpf.setLife(sqlcovtpf1rs.getString("LIFE"));
				covtpf.setCoverage(sqlcovtpf1rs.getString("COVERAGE"));
				covtpf.setRider(sqlcovtpf1rs.getString("RIDER"));
				covtpf.setTermid(sqlcovtpf1rs.getString("TERMID"));
				covtpf.setTrdt(sqlcovtpf1rs.getInt("TRDT"));
				covtpf.setTrtm(sqlcovtpf1rs.getInt("TRTM"));
				covtpf.setUserT(sqlcovtpf1rs.getInt("USER_T"));
				covtpf.setCrtable(sqlcovtpf1rs.getString("CRTABLE"));
				covtpf.setRcesdte(sqlcovtpf1rs.getInt("RCESDTE"));
				covtpf.setPcesdte(sqlcovtpf1rs.getInt("PCESDTE"));
				covtpf.setRcesage(sqlcovtpf1rs.getInt("RCESAGE"));
				covtpf.setPcesage(sqlcovtpf1rs.getInt("PCESAGE"));
				covtpf.setRcestrm(sqlcovtpf1rs.getInt("RCESTRM"));
				covtpf.setPcestrm(sqlcovtpf1rs.getInt("PCESTRM"));
				covtpf.setSumins(sqlcovtpf1rs.getBigDecimal("SUMINS"));
				covtpf.setLiencd(sqlcovtpf1rs.getString("LIENCD")!= null ? sqlcovtpf1rs.getString("LIENCD"):" "); //IBPLIFE-3298
				covtpf.setMortcls(sqlcovtpf1rs.getString("MORTCLS"));
				covtpf.setJlife(sqlcovtpf1rs.getString("JLIFE")!= null ? sqlcovtpf1rs.getString("JLIFE"):" ");//IBPLIFE-3298
				covtpf.setRsunin(sqlcovtpf1rs.getString("RSUNIN"));
				covtpf.setRundte(sqlcovtpf1rs.getInt("RUNDTE"));
				covtpf.setPolinc(sqlcovtpf1rs.getInt("POLINC"));
				covtpf.setNumapp(sqlcovtpf1rs.getInt("NUMAPP"));
				covtpf.setEffdate(sqlcovtpf1rs.getInt("EFFDATE"));
				covtpf.setSex01(sqlcovtpf1rs.getString("SEX01"));
				covtpf.setSex02(sqlcovtpf1rs.getString("SEX02"));
				covtpf.setAnbccd01(sqlcovtpf1rs.getInt("ANBCCD01"));
				covtpf.setAnbccd02(sqlcovtpf1rs.getInt("ANBCCD02"));
				covtpf.setBillfreq(sqlcovtpf1rs.getString("BILLFREQ"));
				covtpf.setBillchnl(sqlcovtpf1rs.getString("BILLCHNL"));
				covtpf.setSeqnbr(sqlcovtpf1rs.getInt("SEQNBR"));
				covtpf.setSingp(sqlcovtpf1rs.getBigDecimal("SINGP"));
				covtpf.setInstprem(sqlcovtpf1rs.getBigDecimal("INSTPREM"));
				covtpf.setPlnsfx(sqlcovtpf1rs.getInt("PLNSFX"));
				covtpf.setPayrseqno(sqlcovtpf1rs.getInt("PAYRSEQNO"));
				covtpf.setCntcurr(sqlcovtpf1rs.getString("CNTCURR"));
				covtpf.setBcesage(sqlcovtpf1rs.getInt("BCESAGE"));
				covtpf.setBcestrm(sqlcovtpf1rs.getInt("BCESTRM"));
				covtpf.setBcesdte(sqlcovtpf1rs.getInt("BCESDTE"));
				covtpf.setBappmeth(sqlcovtpf1rs.getString("BAPPMETH")!= null ? sqlcovtpf1rs.getString("BAPPMETH"):" ");//IBPLIFE-3298
				covtpf.setZbinstprem(sqlcovtpf1rs.getBigDecimal("ZBINSTPREM"));
				covtpf.setZlinstprem(sqlcovtpf1rs.getBigDecimal("ZLINSTPREM"));
				covtpf.setZdivopt(sqlcovtpf1rs.getString("ZDIVOPT"));
				covtpf.setPaycoy(sqlcovtpf1rs.getString("PAYCOY"));
				covtpf.setPayclt(sqlcovtpf1rs.getString("PAYCLT"));
				covtpf.setPaymth(sqlcovtpf1rs.getString("PAYMTH"));
				covtpf.setBankkey(sqlcovtpf1rs.getString("BANKKEY"));
				covtpf.setBankacckey(sqlcovtpf1rs.getString("BANKACCKEY"));
				covtpf.setPaycurr(sqlcovtpf1rs.getString("PAYCURR"));
				covtpf.setFacthous(sqlcovtpf1rs.getString("FACTHOUS"));
				covtpf.setUsrprf(sqlcovtpf1rs.getString("USRPRF"));
				covtpf.setJobnm(sqlcovtpf1rs.getString("JOBNM"));
				covtpf.setDatime(sqlcovtpf1rs.getString("DATIME"));
				covtpf.setLoadper(sqlcovtpf1rs.getBigDecimal("LOADPER"));
				covtpf.setRateadj(sqlcovtpf1rs.getBigDecimal("RATEADJ"));
				covtpf.setFltmort(sqlcovtpf1rs.getBigDecimal("FLTMORT"));
				covtpf.setPremadj(sqlcovtpf1rs.getBigDecimal("PREMADJ"));
				covtpf.setAgeadj(sqlcovtpf1rs.getBigDecimal("AGEADJ"));
				covtList.add(covtpf);
			}

		} catch (SQLException e) {
			LOGGER.error("getCovrridData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlcovtpf1rs);
		}
		return covtList;
	}
	
	public List<Covtpf> getCovtpfData(String chdrcoy,String chdrnum,String life,String coverage,String rider){
		Covtpf covtpf = null;
		PreparedStatement psCovtSelect = null;
		ResultSet sqlcovtpf1rs = null;
		StringBuilder sqlCovtSelectBuilder = new StringBuilder();
		List<Covtpf> covtList = new ArrayList<Covtpf>();
		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlCovtSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
		sqlCovtSelectBuilder.append(" JOBNM, UNIQUE_NUMBER,PLNSFX,RCESDTE,PCESDTE");
		
		sqlCovtSelectBuilder.append(" FROM COVTPF WHERE CHDRCOY = ? AND CHDRNUM = ? and LIFE=? ");
		sqlCovtSelectBuilder.append(" and COVERAGE=? and rider=? ");
		sqlCovtSelectBuilder.append(" ORDER BY ");
		sqlCovtSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC");

		psCovtSelect = getPrepareStatement(sqlCovtSelectBuilder.toString());

		try {
			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			psCovtSelect.setString(1, chdrcoy);
			psCovtSelect.setString(2, chdrnum);
			psCovtSelect.setString(3, life);
			psCovtSelect.setString(4, coverage);
			psCovtSelect.setString(5, rider);

			// ---------------------------------
			// Execute Query
			// ---------------------------------
			sqlcovtpf1rs = executeQuery(psCovtSelect);


			while (sqlcovtpf1rs.next()) {

				covtpf = new Covtpf();

				covtpf.setChdrcoy(sqlcovtpf1rs.getString(1));
				covtpf.setChdrnum(sqlcovtpf1rs.getString(2));
				covtpf.setLife(sqlcovtpf1rs.getString(3));
				covtpf.setCoverage(sqlcovtpf1rs.getString(4));
				covtpf.setRider(sqlcovtpf1rs.getString(5));
				covtpf.setCrtable(sqlcovtpf1rs.getString(6));
				covtpf.setUsrprf(sqlcovtpf1rs.getString(7));
				covtpf.setJobnm(sqlcovtpf1rs.getString(8));
				covtpf.setUniqueNumber(sqlcovtpf1rs.getLong(9));
				covtpf.setPlnsfx(sqlcovtpf1rs.getInt(10));
				covtpf.setRcesdte(sqlcovtpf1rs.getInt(11));
				covtpf.setPcesdte(sqlcovtpf1rs.getInt(12));
				covtList.add(covtpf);
			}

		} catch (SQLException e) {
			LOGGER.error("getCovrridData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlcovtpf1rs);
		}
		return covtList;
	}

	// Introduced for ILIFE-3439
	public List<Covtpf> searchCovtRecordByCoyNum(String chdrcoy, String chdrNum) throws SQLRuntimeException{

		StringBuilder sqlCovtSelect = new StringBuilder();

		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlCovtSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlCovtSelect.append("CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, ");
		sqlCovtSelect.append("TERMID, TRDT, TRTM, USER_T, CRTABLE, ");
		sqlCovtSelect.append("RCESDTE, PCESDTE, RCESAGE, PCESAGE, RCESTRM, ");
		sqlCovtSelect.append("PCESTRM, SUMINS, LIENCD, MORTCLS, JLIFE, ");
		sqlCovtSelect.append("RSUNIN, RUNDTE, POLINC, NUMAPP, EFFDATE, ");
		sqlCovtSelect.append("SEX01, SEX02, ANBCCD01, ANBCCD02, BILLFREQ, ");
		sqlCovtSelect.append("BILLCHNL, SEQNBR, SINGP, INSTPREM, PLNSFX, ");
		sqlCovtSelect.append("PAYRSEQNO, CNTCURR, BCESAGE, BCESTRM, BCESDTE, ");
		sqlCovtSelect.append("BAPPMETH, ZBINSTPREM, ZLINSTPREM, ZDIVOPT, PAYCOY, ");
		sqlCovtSelect.append("PAYCLT, PAYMTH, BANKKEY, BANKACCKEY, PAYCURR, ");
		sqlCovtSelect.append("FACTHOUS, USRPRF, JOBNM, DATIME, LOADPER, ");
		sqlCovtSelect.append("RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZSTPDUTY01, LNKGNO, TPDTYPE, LNKGSUBREFNO, LNKGIND ");
		sqlCovtSelect.append("FROM COVTPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlCovtSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER ASC ");

		PreparedStatement psCovtSelect = getPrepareStatement(sqlCovtSelect.toString());
		ResultSet sqlpCovt1Rs = null;
		List<Covtpf> covtSearchResult = new ArrayList<Covtpf>();
		try {

			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			psCovtSelect.setString(1, chdrcoy);
			psCovtSelect.setString(2, chdrNum);

			// ---------------------------------
			// Execute Query
			// ---------------------------------
			sqlpCovt1Rs = executeQuery(psCovtSelect);

			// ---------------------------------
			// Iterate result set
			// ---------------------------------
			while (sqlpCovt1Rs.next()) {

				Covtpf covtpf = new Covtpf();

				covtpf.setUniqueNumber(sqlpCovt1Rs.getInt("UNIQUE_NUMBER"));
				covtpf.setChdrcoy(sqlpCovt1Rs.getString("CHDRCOY"));
				covtpf.setChdrnum(sqlpCovt1Rs.getString("CHDRNUM"));
				covtpf.setLife(sqlpCovt1Rs.getString("LIFE"));
				covtpf.setCoverage(sqlpCovt1Rs.getString("COVERAGE"));
				covtpf.setRider(sqlpCovt1Rs.getString("RIDER"));
				covtpf.setTermid(sqlpCovt1Rs.getString("TERMID"));
				covtpf.setTrdt(sqlpCovt1Rs.getInt("TRDT"));
				covtpf.setTrtm(sqlpCovt1Rs.getInt("TRTM"));
				covtpf.setUserT(sqlpCovt1Rs.getInt("USER_T"));
				covtpf.setCrtable(sqlpCovt1Rs.getString("CRTABLE"));
				covtpf.setRcesdte(sqlpCovt1Rs.getInt("RCESDTE"));
				covtpf.setPcesdte(sqlpCovt1Rs.getInt("PCESDTE"));
				covtpf.setRcesage(sqlpCovt1Rs.getInt("RCESAGE"));
				covtpf.setPcesage(sqlpCovt1Rs.getInt("PCESAGE"));
				covtpf.setRcestrm(sqlpCovt1Rs.getInt("RCESTRM"));
				covtpf.setPcestrm(sqlpCovt1Rs.getInt("PCESTRM"));
				covtpf.setSumins(sqlpCovt1Rs.getBigDecimal("SUMINS"));
				covtpf.setLiencd(sqlpCovt1Rs.getString("LIENCD"));
				covtpf.setMortcls(sqlpCovt1Rs.getString("MORTCLS"));
				covtpf.setJlife(sqlpCovt1Rs.getString("JLIFE"));
				covtpf.setRsunin(sqlpCovt1Rs.getString("RSUNIN"));
				covtpf.setRundte(sqlpCovt1Rs.getInt("RUNDTE"));
				covtpf.setPolinc(sqlpCovt1Rs.getInt("POLINC"));
				covtpf.setNumapp(sqlpCovt1Rs.getInt("NUMAPP"));
				covtpf.setEffdate(sqlpCovt1Rs.getInt("EFFDATE"));
				covtpf.setSex01(sqlpCovt1Rs.getString("SEX01"));
				covtpf.setSex02(sqlpCovt1Rs.getString("SEX02"));
				covtpf.setAnbccd01(sqlpCovt1Rs.getInt("ANBCCD01"));
				covtpf.setAnbccd02(sqlpCovt1Rs.getInt("ANBCCD02"));
				covtpf.setBillfreq(sqlpCovt1Rs.getString("BILLFREQ"));
				covtpf.setBillchnl(sqlpCovt1Rs.getString("BILLCHNL"));
				covtpf.setSeqnbr(sqlpCovt1Rs.getInt("SEQNBR"));
				covtpf.setSingp(sqlpCovt1Rs.getBigDecimal("SINGP"));
				covtpf.setInstprem(sqlpCovt1Rs.getBigDecimal("INSTPREM"));
				covtpf.setPlnsfx(sqlpCovt1Rs.getInt("PLNSFX"));
				covtpf.setPayrseqno(sqlpCovt1Rs.getInt("PAYRSEQNO"));
				covtpf.setCntcurr(sqlpCovt1Rs.getString("CNTCURR"));
				covtpf.setBcesage(sqlpCovt1Rs.getInt("BCESAGE"));
				covtpf.setBcestrm(sqlpCovt1Rs.getInt("BCESTRM"));
				covtpf.setBcesdte(sqlpCovt1Rs.getInt("BCESDTE"));
				covtpf.setBappmeth(sqlpCovt1Rs.getString("BAPPMETH"));
				covtpf.setZbinstprem(sqlpCovt1Rs.getBigDecimal("ZBINSTPREM"));
				covtpf.setZlinstprem(sqlpCovt1Rs.getBigDecimal("ZLINSTPREM"));
				covtpf.setZdivopt(sqlpCovt1Rs.getString("ZDIVOPT"));
				covtpf.setPaycoy(sqlpCovt1Rs.getString("PAYCOY"));
				covtpf.setPayclt(sqlpCovt1Rs.getString("PAYCLT"));
				covtpf.setPaymth(sqlpCovt1Rs.getString("PAYMTH"));
				covtpf.setBankkey(sqlpCovt1Rs.getString("BANKKEY"));
				covtpf.setBankacckey(sqlpCovt1Rs.getString("BANKACCKEY"));
				covtpf.setPaycurr(sqlpCovt1Rs.getString("PAYCURR"));
				covtpf.setFacthous(sqlpCovt1Rs.getString("FACTHOUS"));
				covtpf.setUsrprf(sqlpCovt1Rs.getString("USRPRF"));
				covtpf.setJobnm(sqlpCovt1Rs.getString("JOBNM"));
				covtpf.setDatime(sqlpCovt1Rs.getString("DATIME"));
				covtpf.setLoadper(sqlpCovt1Rs.getBigDecimal("LOADPER"));
				covtpf.setRateadj(sqlpCovt1Rs.getBigDecimal("RATEADJ"));
				covtpf.setFltmort(sqlpCovt1Rs.getBigDecimal("FLTMORT"));
				covtpf.setPremadj(sqlpCovt1Rs.getBigDecimal("PREMADJ"));
				covtpf.setAgeadj(sqlpCovt1Rs.getBigDecimal("AGEADJ"));
				covtpf.setZstpduty01(sqlpCovt1Rs.getBigDecimal("ZSTPDUTY01"));
				covtpf.setLnkgno(sqlpCovt1Rs.getString("LNKGNO"));
				covtpf.setTpdtype(sqlpCovt1Rs.getString("TPDTYPE"));
				covtpf.setLnkgsubrefno(sqlpCovt1Rs.getString("LNKGSUBREFNO"));
				covtpf.setLnkgind(sqlpCovt1Rs.getString("LNKGIND"));

				covtSearchResult.add(covtpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovtRecordByCoyNum()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlpCovt1Rs);
		}
		return covtSearchResult;
	}

	public List<Covtpf> getCovtCovDetails(Covtpf covtpfData){
		Covtpf covtpf = null;
		PreparedStatement psCovtSelect = null;
		ResultSet sqlcovtpf1rs = null;
		StringBuilder sqlCovtSelectBuilder = new StringBuilder();
		List<Covtpf> covtList = new ArrayList<Covtpf>();
		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlCovtSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE");
		sqlCovtSelectBuilder.append(" FROM COVTCOV WHERE CHDRCOY = ? AND CHDRNUM = ? and LIFE=?");
		sqlCovtSelectBuilder.append(" ORDER BY ");
		sqlCovtSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC");

		psCovtSelect = getPrepareStatement(sqlCovtSelectBuilder.toString());

		try {
			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			psCovtSelect.setString(1, covtpfData.getChdrcoy());
			psCovtSelect.setString(2, covtpfData.getChdrnum());
			psCovtSelect.setString(3, covtpfData.getLife());

			// ---------------------------------
			// Execute Query
			// ---------------------------------
			sqlcovtpf1rs = executeQuery(psCovtSelect);


			while (sqlcovtpf1rs.next()) {

				covtpf = new Covtpf();

				covtpf.setChdrcoy(sqlcovtpf1rs.getString(1));
				covtpf.setChdrnum(sqlcovtpf1rs.getString(2));
				covtpf.setLife(sqlcovtpf1rs.getString(3));
				covtpf.setCoverage(sqlcovtpf1rs.getString(4));
				covtpf.setRider(sqlcovtpf1rs.getString(5));
				covtpf.setCrtable(sqlcovtpf1rs.getString(6));

				covtList.add(covtpf);
			}

		} catch (SQLException e) {
			LOGGER.error("getCovrridData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlcovtpf1rs);
		}
		return covtList;
	}
	//ILIFE-4492	
	public List<Covtpf> searchCovtRecordByCoyNumDescUniquNo(String chdrcoy, String chdrNum) throws SQLRuntimeException{

		StringBuilder sqlCovtSelect = new StringBuilder();

		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlCovtSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlCovtSelect.append("CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, ");
		sqlCovtSelect.append("TERMID, TRDT, TRTM, USER_T, CRTABLE, ");
		sqlCovtSelect.append("RCESDTE, PCESDTE, RCESAGE, PCESAGE, RCESTRM, ");
		sqlCovtSelect.append("PCESTRM, SUMINS, LIENCD, MORTCLS, JLIFE, ");
		sqlCovtSelect.append("RSUNIN, RUNDTE, POLINC, NUMAPP, EFFDATE, ");
		sqlCovtSelect.append("SEX01, SEX02, ANBCCD01, ANBCCD02, BILLFREQ, ");
		sqlCovtSelect.append("BILLCHNL, SEQNBR, SINGP, INSTPREM, PLNSFX, ");
		sqlCovtSelect.append("PAYRSEQNO, CNTCURR, BCESAGE, BCESTRM, BCESDTE, ");
		sqlCovtSelect.append("BAPPMETH, ZBINSTPREM, ZLINSTPREM, ZDIVOPT, PAYCOY, ");
		sqlCovtSelect.append("PAYCLT, PAYMTH, BANKKEY, BANKACCKEY, PAYCURR, ");
		sqlCovtSelect.append("FACTHOUS, USRPRF, JOBNM, DATIME, LOADPER, ");
		sqlCovtSelect.append("RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZCLSTATE, ZSTPDUTY01, LNKGNO, TPDTYPE, LNKGSUBREFNO, LNKGIND, ");
		sqlCovtSelect.append("GMIB, GMDB, GMWB, RISKPREM, SINGPREMTYPE, COMMPREM "); //ILIFE-8709
		sqlCovtSelect.append("FROM COVTPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlCovtSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psCovtSelect = getPrepareStatement(sqlCovtSelect.toString());
		ResultSet sqlpCovt1Rs = null;
		List<Covtpf> covtSearchResult = new ArrayList<Covtpf>();
		try {

			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			psCovtSelect.setString(1, chdrcoy);
			psCovtSelect.setString(2, chdrNum);

			// ---------------------------------
			// Execute Query
			// ---------------------------------
			sqlpCovt1Rs = executeQuery(psCovtSelect);

			// ---------------------------------
			// Iterate result set
			// ---------------------------------
			while (sqlpCovt1Rs.next()) {

				Covtpf covtpf = new Covtpf();

				covtpf.setUniqueNumber(sqlpCovt1Rs.getInt("UNIQUE_NUMBER"));
				covtpf.setChdrcoy(sqlpCovt1Rs.getString("CHDRCOY"));
				covtpf.setChdrnum(sqlpCovt1Rs.getString("CHDRNUM"));
				covtpf.setLife(sqlpCovt1Rs.getString("LIFE"));
				covtpf.setCoverage(sqlpCovt1Rs.getString("COVERAGE"));
				covtpf.setRider(sqlpCovt1Rs.getString("RIDER"));
				covtpf.setTermid(sqlpCovt1Rs.getString("TERMID"));
				covtpf.setTrdt(sqlpCovt1Rs.getInt("TRDT"));
				covtpf.setTrtm(sqlpCovt1Rs.getInt("TRTM"));
				covtpf.setUserT(sqlpCovt1Rs.getInt("USER_T"));
				covtpf.setCrtable(sqlpCovt1Rs.getString("CRTABLE"));
				covtpf.setRcesdte(sqlpCovt1Rs.getInt("RCESDTE"));
				covtpf.setPcesdte(sqlpCovt1Rs.getInt("PCESDTE"));
				covtpf.setRcesage(sqlpCovt1Rs.getInt("RCESAGE"));
				covtpf.setPcesage(sqlpCovt1Rs.getInt("PCESAGE"));
				covtpf.setRcestrm(sqlpCovt1Rs.getInt("RCESTRM"));
				covtpf.setPcestrm(sqlpCovt1Rs.getInt("PCESTRM"));
				covtpf.setSumins(sqlpCovt1Rs.getBigDecimal("SUMINS"));
				covtpf.setLiencd(sqlpCovt1Rs.getString("LIENCD"));
				covtpf.setMortcls(sqlpCovt1Rs.getString("MORTCLS"));
				covtpf.setJlife(sqlpCovt1Rs.getString("JLIFE")!= null ? sqlpCovt1Rs.getString("JLIFE"):" "); //ILIFE-8709
				covtpf.setRsunin(sqlpCovt1Rs.getString("RSUNIN"));
				covtpf.setRundte(sqlpCovt1Rs.getInt("RUNDTE"));
				covtpf.setPolinc(sqlpCovt1Rs.getInt("POLINC"));
				covtpf.setNumapp(sqlpCovt1Rs.getInt("NUMAPP"));
				covtpf.setEffdate(sqlpCovt1Rs.getInt("EFFDATE"));
				covtpf.setSex01(sqlpCovt1Rs.getString("SEX01"));
				covtpf.setSex02(sqlpCovt1Rs.getString("SEX02"));
				covtpf.setAnbccd01(sqlpCovt1Rs.getInt("ANBCCD01"));
				covtpf.setAnbccd02(sqlpCovt1Rs.getInt("ANBCCD02"));
				covtpf.setBillfreq(sqlpCovt1Rs.getString("BILLFREQ"));
				covtpf.setBillchnl(sqlpCovt1Rs.getString("BILLCHNL"));
				covtpf.setSeqnbr(sqlpCovt1Rs.getInt("SEQNBR"));
				covtpf.setSingp(sqlpCovt1Rs.getBigDecimal("SINGP"));
				covtpf.setInstprem(sqlpCovt1Rs.getBigDecimal("INSTPREM"));
				covtpf.setPlnsfx(sqlpCovt1Rs.getInt("PLNSFX"));
				covtpf.setPayrseqno(sqlpCovt1Rs.getInt("PAYRSEQNO"));
				covtpf.setCntcurr(sqlpCovt1Rs.getString("CNTCURR"));
				covtpf.setBcesage(sqlpCovt1Rs.getInt("BCESAGE"));
				covtpf.setBcestrm(sqlpCovt1Rs.getInt("BCESTRM"));
				covtpf.setBcesdte(sqlpCovt1Rs.getInt("BCESDTE"));
				covtpf.setBappmeth(sqlpCovt1Rs.getString("BAPPMETH"));
				covtpf.setZbinstprem(sqlpCovt1Rs.getBigDecimal("ZBINSTPREM"));
				covtpf.setZlinstprem(sqlpCovt1Rs.getBigDecimal("ZLINSTPREM"));
				covtpf.setZdivopt(sqlpCovt1Rs.getString("ZDIVOPT"));
				covtpf.setPaycoy(sqlpCovt1Rs.getString("PAYCOY"));
				covtpf.setPayclt(sqlpCovt1Rs.getString("PAYCLT"));
				covtpf.setPaymth(sqlpCovt1Rs.getString("PAYMTH"));
				covtpf.setBankkey(sqlpCovt1Rs.getString("BANKKEY"));
				covtpf.setBankacckey(sqlpCovt1Rs.getString("BANKACCKEY"));
				covtpf.setPaycurr(sqlpCovt1Rs.getString("PAYCURR"));
				covtpf.setFacthous(sqlpCovt1Rs.getString("FACTHOUS"));
				covtpf.setUsrprf(sqlpCovt1Rs.getString("USRPRF"));
				covtpf.setJobnm(sqlpCovt1Rs.getString("JOBNM"));
				covtpf.setDatime(sqlpCovt1Rs.getString("DATIME"));
				covtpf.setLoadper(sqlpCovt1Rs.getBigDecimal("LOADPER") 
						!= null ? sqlpCovt1Rs.getBigDecimal("LOADPER"): BigDecimal.ZERO);
				covtpf.setRateadj(sqlpCovt1Rs.getBigDecimal("RATEADJ")
						!= null ? sqlpCovt1Rs.getBigDecimal("RATEADJ"): BigDecimal.ZERO);
				covtpf.setFltmort(sqlpCovt1Rs.getBigDecimal("FLTMORT")
						!= null ? sqlpCovt1Rs.getBigDecimal("FLTMORT"): BigDecimal.ZERO);
				covtpf.setPremadj(sqlpCovt1Rs.getBigDecimal("PREMADJ") 
						!= null ? sqlpCovt1Rs.getBigDecimal("PREMADJ"): BigDecimal.ZERO);
				covtpf.setAgeadj(sqlpCovt1Rs.getBigDecimal("AGEADJ") 
						!= null ? sqlpCovt1Rs.getBigDecimal("AGEADJ"): BigDecimal.ZERO);
				covtpf.setZclstate(sqlpCovt1Rs.getString("ZCLSTATE")!= null ? sqlpCovt1Rs.getString("ZCLSTATE"):"");
				covtpf.setZstpduty01(sqlpCovt1Rs.getBigDecimal("ZSTPDUTY01")
						!= null ? sqlpCovt1Rs.getBigDecimal("ZSTPDUTY01"): BigDecimal.ZERO);
				covtpf.setTpdtype(sqlpCovt1Rs.getString("TPDTYPE")!= null ? sqlpCovt1Rs.getString("TPDTYPE"):"");
				covtpf.setLnkgsubrefno(sqlpCovt1Rs.getString("LNKGSUBREFNO")!= null ? sqlpCovt1Rs.getString("LNKGSUBREFNO"):"");
				covtpf.setLnkgind(sqlpCovt1Rs.getString("LNKGIND")!= null ? sqlpCovt1Rs.getString("LNKGIND"):"");
				covtpf.setLnkgno(sqlpCovt1Rs.getString("LNKGNO")!= null ? sqlpCovt1Rs.getString("LNKGNO"):""); //ILIFE-8709
				covtpf.setGmdb(sqlpCovt1Rs.getString("GMDB")!= null ? sqlpCovt1Rs.getString("GMDB"):""); //ILIFE-8709
				covtpf.setGmib(sqlpCovt1Rs.getString("GMIB")!= null ? sqlpCovt1Rs.getString("GMIB"):""); //ILIFE-8709
				covtpf.setGmwb(sqlpCovt1Rs.getString("GMWB")!= null ? sqlpCovt1Rs.getString("GMWB"):"");
				covtpf.setRiskprem(sqlpCovt1Rs.getBigDecimal("RISKPREM") 
						!= null ? sqlpCovt1Rs.getBigDecimal("RISKPREM"): BigDecimal.ZERO);
				covtpf.setSingpremtype(sqlpCovt1Rs.getString("SINGPREMTYPE")!= null ? sqlpCovt1Rs.getString("SINGPREMTYPE"):"");
				covtpf.setCommPrem(sqlpCovt1Rs.getBigDecimal("COMMPREM")!= null ? sqlpCovt1Rs.getBigDecimal("COMMPREM"):BigDecimal.ZERO);
				covtSearchResult.add(covtpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovtRecordByCoyNumDescUniquNo()", e); //ILIFE-8709
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlpCovt1Rs);
		}
		return covtSearchResult;
	}

	public List<Covtpf> getCovtlnbRecord(String chdrcoy, String chdrnum,
			String life)
	{
		return getCovtlnbRecord(chdrcoy,chdrnum,life,null);
	}
	public List<Covtpf> getCovtlnbRecord(String chdrcoy, String chdrnum, String life, String jlife) {
		StringBuilder sql1 = new StringBuilder();
		sql1.append("select * from covtlnb where chdrcoy = ? and chdrnum = ? and life = ?  ");
		if(jlife!= null)
		{
			sql1.append(" AND JLIFE = ?  ");
		}
		sql1.append(" order by chdrcoy asc, chdrnum asc, life asc, coverage asc, rider asc,seqnbr asc, unique_number asc");

		PreparedStatement ps = getPrepareStatement(sql1.toString());
		ResultSet sqlcovrpf1rs = null;
		List<Covtpf> covtlnblist = new ArrayList<Covtpf>();
		try {
			ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, life);
			if(jlife!= null)
			{
				ps.setString(4, jlife);
			}
			sqlcovrpf1rs = executeQuery(ps);



			while (sqlcovrpf1rs.next()) {
				Covtpf covtpf = new Covtpf();
				covtpf.setChdrcoy(sqlcovrpf1rs.getString("CHDRCOY"));
				covtpf.setChdrnum(sqlcovrpf1rs.getString("CHDRNUM"));
				covtpf.setCoverage(sqlcovrpf1rs.getString("COVERAGE"));
				covtpf.setCrtable(sqlcovrpf1rs.getString("CRTABLE"));
				covtpf.setEffdate(sqlcovrpf1rs.getInt("EFFDATE"));
				covtpf.setLife(sqlcovrpf1rs.getString("LIFE"));
				covtpf.setRider(sqlcovrpf1rs.getString("RIDER"));
				covtpf.setSumins(sqlcovrpf1rs.getBigDecimal("SUMINS"));
				covtpf.setInstprem(sqlcovrpf1rs.getBigDecimal("INSTPREM"));
				covtpf.setRcesdte(sqlcovrpf1rs.getInt("RCESDTE"));
				covtpf.setRcestrm(sqlcovrpf1rs.getInt("RCESTRM"));
				covtpf.setPcestrm(sqlcovrpf1rs.getInt("PCESTRM"));
				covtpf.setCntcurr(sqlcovrpf1rs.getString("CNTCURR"));				
				covtlnblist.add(covtpf);
			}

		}
		catch (SQLException e) {
			LOGGER.error("getCovrlnbRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

		return covtlnblist;
	}

	public Covtpf getCovtlnbData(String chdrcoy,String chdrnum,String life,String coverage,String rider){
		Covtpf covtpf = null;
		PreparedStatement psCovtSelect = null;
		ResultSet sqlcovtpf1rs = null;
		StringBuilder sqlCovtSelectBuilder = new StringBuilder();
		if (rider == "") {
			rider = "00";
		}

		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlCovtSelectBuilder.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, CRTABLE, USRPRF,");
		sqlCovtSelectBuilder.append(" JOBNM, UNIQUE_NUMBER, SUMINS,EFFDATE ");
		sqlCovtSelectBuilder.append(" FROM COVTLNB WHERE CHDRCOY = ? AND CHDRNUM = ? and LIFE=? and COVERAGE=? and rider=? ");
		sqlCovtSelectBuilder.append(" ORDER BY ");
		sqlCovtSelectBuilder.append(" CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER ASC");

		psCovtSelect = getPrepareStatement(sqlCovtSelectBuilder.toString());

		try {
			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			psCovtSelect.setString(1, chdrcoy);
			psCovtSelect.setString(2, chdrnum);
			psCovtSelect.setString(3, life);
			psCovtSelect.setString(4, coverage);
			psCovtSelect.setString(5, rider);


			// ---------------------------------
			// Execute Query
			// ---------------------------------
			sqlcovtpf1rs = executeQuery(psCovtSelect);


			while (sqlcovtpf1rs.next()) {

				covtpf = new Covtpf();

				covtpf.setChdrcoy(sqlcovtpf1rs.getString(1));
				covtpf.setChdrnum(sqlcovtpf1rs.getString(2));
				covtpf.setLife(sqlcovtpf1rs.getString(3));
				covtpf.setCoverage(sqlcovtpf1rs.getString(4));
				covtpf.setRider(sqlcovtpf1rs.getString(5));
				covtpf.setCrtable(sqlcovtpf1rs.getString(6));
				covtpf.setUsrprf(sqlcovtpf1rs.getString(7));
				covtpf.setJobnm(sqlcovtpf1rs.getString(8));
				covtpf.setUniqueNumber(sqlcovtpf1rs.getLong(9));
				covtpf.setSumins(sqlcovtpf1rs.getBigDecimal(10));
				covtpf.setEffdate(sqlcovtpf1rs.getInt("EFFDATE"));
			}

		} catch (SQLException e) {
			LOGGER.error("getCovrridData()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlcovtpf1rs);
		}
		return covtpf;
	}

	public int bulkUpdateCovtData(Map<String, Covtpf> covtUpdateMap){
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE COVTPF SET INSTPREM = ?, ZBINSTPREM = ?, ZLINSTPREM = ?, ZSTPDUTY01 = ?, RISKPREM = ?, COMMPREM = ? ");
		sb.append("WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ?");
		PreparedStatement ps=null;
		int[] count;
		try{
			ps=getConnection().prepareStatement(sb.toString());
			for(Map.Entry<String, Covtpf> covtpf: covtUpdateMap.entrySet()){
				ps.setBigDecimal(1, covtpf.getValue().getInstprem());
				ps.setBigDecimal(2, covtpf.getValue().getZbinstprem());
				ps.setBigDecimal(3, covtpf.getValue().getZlinstprem());
				ps.setBigDecimal(4, covtpf.getValue().getZstpduty01());
				ps.setBigDecimal(5, covtpf.getValue().getRiskprem());
				ps.setBigDecimal(6, covtpf.getValue().getCommPrem());
				ps.setString(7, covtpf.getValue().getChdrcoy());
				ps.setString(8, covtpf.getKey().substring(0, 8));
				ps.setString(9, covtpf.getKey().substring(8, 10));
				ps.setString(10, covtpf.getKey().substring(10, 12));
				ps.setString(11, covtpf.getKey().substring(12, 14));
			
				ps.addBatch();
			}
			count = ps.executeBatch();
			if(count.length > 0)
			return count.length;
		}
		catch(SQLException e){
			LOGGER.error("bulkUpdateCovtData()", e); /* IJTI-1479 */
            throw new SQLRuntimeException(e);
		}
		finally {
            close(ps, null);
        }
		return 0;
	}
	
	/*ILIFE-8098 Start */
	public List<Covtpf> getSingpremtype(String coy, String chdrnum) {

		StringBuilder sqlCovrSelect1 = new StringBuilder();

		sqlCovrSelect1.append("SELECT TOP 1 SINGPREMTYPE,SINGP FROM COVTPF ");
		sqlCovrSelect1.append(" WHERE CHDRCOY=? AND CHDRNUM=? AND  SINGPREMTYPE != '' OR  SINGPREMTYPE != Null");
		sqlCovrSelect1.append(" ORDER BY UNIQUE_NUMBER DESC ");

		PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
		ResultSet sqlcovrpf1rs = null;
		Covtpf covtpf = null; 
		 List<Covtpf> list = new ArrayList<Covtpf>();
		try {
			psCovrSelect.setInt(1, Integer.parseInt(coy));
			psCovrSelect.setString(2, chdrnum);
			sqlcovrpf1rs = executeQuery(psCovrSelect);

			while (sqlcovrpf1rs.next()) {
				covtpf=new Covtpf();
				covtpf.setSingpremtype(sqlcovrpf1rs.getString("SINGPREMTYPE"));
				covtpf.setSingp(sqlcovrpf1rs.getBigDecimal("SINGP"));
				list.add(covtpf);
			}

		} catch (SQLException e) {
			LOGGER.error("getSingpremtype()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovrSelect, sqlcovrpf1rs);
		}
		return list;
	}
 //ILIFE-8709 start
	@Override
	public void deleteCovtpfRecord(Covtpf covtpf) {
		  String sql = "DELETE FROM COVTPF WHERE UNIQUE_NUMBER=? ";
		  	LOGGER.info(sql);
			PreparedStatement ps = null;
			ResultSet rs = null;
			try{
				ps = getPrepareStatement(sql);	

				    ps.setLong(1,covtpf.getUniqueNumber());
				    ps.executeUpdate();				
				
			}catch (SQLException e) {
				LOGGER.error("deleteCovrpfRecord()",e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}				
	}
 //ILIFE-8709 end

	public int updateCovtData(Covtpf covtpf){
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE COVTPF SET INSTPREM = ?, ZBINSTPREM = ?, ZLINSTPREM = ?, ZSTPDUTY01 = ?, RISKPREM = ?, ");
		sb.append(" CHDRCOY =?, CHDRNUM =? , LIFE =? , COVERAGE =?, RIDER=?, PLNSFX =?, SEQNBR =?, TERMID =?, TRDT =?,");
		sb.append(" TRTM =?, USER_T=?, CRTABLE =?, RCESDTE=? , PCESDTE=?, RCESAGE=?, PCESAGE=?, RCESTRM=?, PCESTRM=?, ");
		sb.append(" SUMINS=?, MORTCLS=?, LIENCD=?, RSUNIN= ? ,RUNDTE= ? ,POLINC= ?,JLIFE= ? ,EFFDATE= ?,SEX01= ? ,SEX02= ?,");
		sb.append(" ANBCCD01= ? ,ANBCCD02= ?,BILLFREQ= ? ,BILLCHNL= ? ,SINGP= ? , PAYRSEQNO= ? ,BCESAGE= ?, BCESTRM= ?,");
		sb.append(" BCESDTE= ? ,BAPPMETH= ? ,ZDIVOPT= ? ,PAYCOY= ?, PAYCLT= ? ,PAYMTH= ? ,BANKKEY= ? ,BANKACCKEY= ?, ");
		sb.append(" PAYCURR= ? ,FACTHOUS= ? ,USRPRF= ? ,JOBNM= ? ,DATIME= ? ,LOADPER= ? ,RATEADJ= ? ,FLTMORT= ?,PREMADJ= ?,");
		sb.append(" AGEADJ= ? ,ZCLSTATE= ?,LNKGNO= ?,LNKGSUBREFNO= ?,TPDTYPE= ?,LNKGIND= ?,SINGPREMTYPE= ?,CASHVALARER= ?");		
		sb.append(" WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? ");
		sb.append(" AND UNIQUE_NUMBER = ?");
		PreparedStatement ps=null;
		int count;
		try{
			ps=getConnection().prepareStatement(sb.toString());
			setUpdateData(ps, covtpf);
			count = ps.executeUpdate();
			if(count > 0)
			return count;
		}
		catch(SQLException e){
			LOGGER.error("updateCovtData()", e);
            throw new SQLRuntimeException(e);
		}
		finally {
            close(ps, null);
        }
		return 0;
	}
	void setUpdateData(PreparedStatement ps,Covtpf covtpf)throws SQLException{
		ps.setBigDecimal(1, covtpf.getInstprem());
		ps.setBigDecimal(2, covtpf.getZbinstprem());
		ps.setBigDecimal(3, covtpf.getZlinstprem());
		ps.setBigDecimal(4, covtpf.getZstpduty01());
		ps.setBigDecimal(5, covtpf.getRiskprem());
		ps.setString(6, covtpf.getChdrcoy());
		ps.setString(7, covtpf.getChdrnum());
		ps.setString(8, covtpf.getLife());
		ps.setString(9, covtpf.getCoverage());
		ps.setString(10, covtpf.getRider());
		ps.setInt(11, covtpf.getPlnsfx());
		ps.setInt(12, covtpf.getSeqnbr());
		ps.setString(13, covtpf.getTermid());
		ps.setInt(14, covtpf.getTrdt());
		ps.setInt(15, covtpf.getTrtm());
		ps.setInt(16, covtpf.getUserT());
		ps.setString(17, covtpf.getCrtable());
		ps.setInt(18, covtpf.getRcesdte());
		ps.setInt(19, covtpf.getPcesdte());
		ps.setInt(20, covtpf.getRcesage());
		ps.setInt(21, covtpf.getPcesage());
		ps.setInt(22, covtpf.getRcestrm());	
		ps.setInt(23, covtpf.getPcestrm());	
		ps.setBigDecimal(24, covtpf.getSumins());	
		ps.setString(25, covtpf.getMortcls());	
		ps.setString(26, covtpf.getLiencd());	
		ps.setString(27, covtpf.getRsunin());	
		ps.setInt(28, covtpf.getRundte());	
		ps.setInt(29, covtpf.getPolinc());	
		ps.setString(30, covtpf.getJlife());	
		ps.setInt(31, covtpf.getEffdate());	
		ps.setString(32, covtpf.getSex01());	
		ps.setString(33, covtpf.getSex02());	
		ps.setInt(34, covtpf.getAnbccd01());	
		ps.setInt(35, covtpf.getAnbccd02());	
		ps.setString(36, covtpf.getBillfreq());	
		ps.setString(37, covtpf.getBillchnl());	
		ps.setBigDecimal(38, covtpf.getSingp());	
		ps.setInt(39, covtpf.getPayrseqno());	
		ps.setInt(40, covtpf.getBcesage());	
		ps.setInt(41, covtpf.getBcestrm());	
		ps.setInt(42, covtpf.getBcesdte());
		ps.setString(43, covtpf.getBappmeth());
		ps.setString(44, covtpf.getZdivopt());
		ps.setString(45, covtpf.getPaycoy());
		ps.setString(46, covtpf.getPayclt());
		ps.setString(47, covtpf.getPaymth());
		ps.setString(48, covtpf.getBankkey());
		ps.setString(49, covtpf.getBankacckey());
		ps.setString(50, covtpf.getPaycurr());
		ps.setString(51, covtpf.getFacthous());
		ps.setString(52, getUsrprf());
		ps.setString(53, getJobnm());
		ps.setTimestamp(54, new Timestamp(System.currentTimeMillis()));
		ps.setBigDecimal(55, covtpf.getLoadper());
		ps.setBigDecimal(56, covtpf.getRateadj());
		ps.setBigDecimal(57, covtpf.getFltmort());
		ps.setBigDecimal(58, covtpf.getPremadj());
		ps.setBigDecimal(59, covtpf.getAgeadj());
		ps.setString(60, covtpf.getZclstate());
		ps.setString(61, covtpf.getLnkgno());
		ps.setString(62, covtpf.getLnkgsubrefno());
		ps.setString(63, covtpf.getTpdtype());
		ps.setString(64, covtpf.getLnkgind());
		ps.setString(65, covtpf.getSingpremtype());
		ps.setBigDecimal(66, covtpf.getCashvalarer());
		ps.setString(67, covtpf.getChdrcoy());
		ps.setString(68, covtpf.getChdrnum());
		ps.setString(69, covtpf.getLife());
		ps.setString(70, covtpf.getCoverage());
		ps.setString(71, covtpf.getRider());
		ps.setInt(72, covtpf.getPlnsfx());
		ps.setLong(73, covtpf.getUniqueNumber());
	}
	public int insertCovtData(Covtpf covtpf){
		StringBuilder sb = new StringBuilder();
		sb.append(" INSERT INTO COVTPF ( INSTPREM , ZBINSTPREM , ZLINSTPREM , ZSTPDUTY01 , RISKPREM , ");
		sb.append(" CHDRCOY , CHDRNUM  , LIFE  , COVERAGE , RIDER, PLNSFX , SEQNBR , TERMID , TRDT , TRTM , USER_T, ");
		sb.append(" CRTABLE , RCESDTE , PCESDTE, RCESAGE, PCESAGE, RCESTRM, PCESTRM, SUMINS, MORTCLS, LIENCD, ");
		sb.append(" RSUNIN ,RUNDTE ,POLINC,JLIFE ,EFFDATE ,SEX01 ,SEX02 ,ANBCCD01 ,ANBCCD02,BILLFREQ ,BILLCHNL ,SINGP ,");
		sb.append(" PAYRSEQNO ,BCESAGE, BCESTRM, BCESDTE ,BAPPMETH ,ZDIVOPT ,PAYCOY, PAYCLT ,PAYMTH ,BANKKEY , ");
		sb.append(" BANKACCKEY, PAYCURR ,FACTHOUS ,USRPRF ,JOBNM ,DATIME ,LOADPER ,RATEADJ ,FLTMORT ,PREMADJ ,AGEADJ , ");
		sb.append(" ZCLSTATE, LNKGNO ,LNKGSUBREFNO, TPDTYPE, LNKGIND, SINGPREMTYPE, CASHVALARER ) ");	
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps=null;
		int count;
		try{
			ps=getConnection().prepareStatement(sb.toString());
			setInsertData(ps, covtpf);
			count = ps.executeUpdate();
			if(count > 0)
			return count;
		}
		catch(SQLException e){
			LOGGER.error("insertCovtData()", e);
            throw new SQLRuntimeException(e);
		}
		finally {
            close(ps, null);
        }
		return 0;
	}
	
	void setInsertData(PreparedStatement ps,Covtpf covtpf)throws SQLException{
		ps.setBigDecimal(1, covtpf.getInstprem());
		ps.setBigDecimal(2, covtpf.getZbinstprem());
		ps.setBigDecimal(3, covtpf.getZlinstprem());
		ps.setBigDecimal(4, covtpf.getZstpduty01());
		ps.setBigDecimal(5, covtpf.getRiskprem());
		ps.setString(6, covtpf.getChdrcoy());
		ps.setString(7, covtpf.getChdrnum());
		ps.setString(8, covtpf.getLife());
		ps.setString(9, covtpf.getCoverage());
		ps.setString(10, covtpf.getRider());
		ps.setInt(11, covtpf.getPlnsfx());
		ps.setInt(12, covtpf.getSeqnbr());
		ps.setString(13, covtpf.getTermid());
		ps.setInt(14, covtpf.getTrdt());
		ps.setInt(15, covtpf.getTrtm());
		ps.setInt(16, covtpf.getUserT());
		ps.setString(17, covtpf.getCrtable());
		ps.setInt(18, covtpf.getRcesdte());
		ps.setInt(19, covtpf.getPcesdte());
		ps.setInt(20, covtpf.getRcesage());
		ps.setInt(21, covtpf.getPcesage());
		ps.setInt(22, covtpf.getRcestrm());	
		ps.setInt(23, covtpf.getPcestrm());	
		ps.setBigDecimal(24, covtpf.getSumins());	
		ps.setString(25, covtpf.getMortcls());	
		ps.setString(26, covtpf.getLiencd());	
		ps.setString(27, covtpf.getRsunin());	
		ps.setInt(28, covtpf.getRundte());	
		ps.setInt(29, covtpf.getPolinc());	
		ps.setString(30, covtpf.getJlife());	
		ps.setInt(31, covtpf.getEffdate());	
		ps.setString(32, covtpf.getSex01());	
		ps.setString(33, covtpf.getSex02());	
		ps.setInt(34, covtpf.getAnbccd01());	
		ps.setInt(35, covtpf.getAnbccd02());	
		ps.setString(36, covtpf.getBillfreq());	
		ps.setString(37, covtpf.getBillchnl());	
		ps.setBigDecimal(38, covtpf.getSingp());	
		ps.setInt(39, covtpf.getPayrseqno());	
		ps.setInt(40, covtpf.getBcesage());	
		ps.setInt(41, covtpf.getBcestrm());	
		ps.setInt(42, covtpf.getBcesdte());
		ps.setString(43, covtpf.getBappmeth());
		ps.setString(44, covtpf.getZdivopt());
		ps.setString(45, covtpf.getPaycoy());
		ps.setString(46, covtpf.getPayclt());
		ps.setString(47, covtpf.getPaymth());
		ps.setString(48, covtpf.getBankkey());
		ps.setString(49, covtpf.getBankacckey());
		ps.setString(50, covtpf.getPaycurr());
		ps.setString(51, covtpf.getFacthous());
		ps.setString(52, getUsrprf());
		ps.setString(53, getJobnm());
		ps.setTimestamp(54, new Timestamp(System.currentTimeMillis()));
		ps.setBigDecimal(55, covtpf.getLoadper());
		ps.setBigDecimal(56, covtpf.getRateadj());
		ps.setBigDecimal(57, covtpf.getFltmort());
		ps.setBigDecimal(58, covtpf.getPremadj());
		ps.setBigDecimal(59, covtpf.getAgeadj());
		ps.setString(60, covtpf.getZclstate());
		ps.setString(61, covtpf.getLnkgno());
		ps.setString(62, covtpf.getLnkgsubrefno());
		ps.setString(63, covtpf.getTpdtype());
		ps.setString(64, covtpf.getLnkgind());
		ps.setString(65, covtpf.getSingpremtype());
		ps.setBigDecimal(66, covtpf.getCashvalarer());
	}

	@Override
	public int updateCovtpf(Covtpf covtpf) {
		StringBuilder sb = new StringBuilder();
		sb.append("UPDATE COVTPF SET EFFDATE= ? WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? AND "
				+ "COVERAGE = ? AND RIDER = ?  AND UNIQUE_NUMBER = ?");
		PreparedStatement ps=null;
		int count;
		try{
			ps=getConnection().prepareStatement(sb.toString());
			ps.setInt(1, covtpf.getEffdate());	
			ps.setString(2, covtpf.getChdrcoy());
			ps.setString(3, covtpf.getChdrnum());
			ps.setString(4, covtpf.getLife());
			ps.setString(5, covtpf.getCoverage());
			ps.setString(6, covtpf.getRider());
			ps.setLong(7, covtpf.getUniqueNumber());
			count = ps.executeUpdate();
			if(count > 0)
				return count;
		}
		catch(SQLException e){
            throw new SQLRuntimeException(e);
		}
		finally {
            close(ps, null);
        }
		return 0;
	}
		

 //ILB-947 start
	@Override
	public Map<String, List<Covtpf>> searchCovt(String coy, List<String> chdrnumList) {


		StringBuilder sqlCovtSelect = new StringBuilder();

		// ---------------------------------
		// Construct Query
		// ---------------------------------
		sqlCovtSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlCovtSelect.append("CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, ");
		sqlCovtSelect.append("TERMID, TRDT, TRTM, USER_T, CRTABLE, ");
		sqlCovtSelect.append("RCESDTE, PCESDTE, RCESAGE, PCESAGE, RCESTRM, ");
		sqlCovtSelect.append("PCESTRM, SUMINS, LIENCD, MORTCLS, JLIFE, ");
		sqlCovtSelect.append("RSUNIN, RUNDTE, POLINC, NUMAPP, EFFDATE, ");
		sqlCovtSelect.append("SEX01, SEX02, ANBCCD01, ANBCCD02, BILLFREQ, ");
		sqlCovtSelect.append("BILLCHNL, SEQNBR, SINGP, INSTPREM, PLNSFX, ");
		sqlCovtSelect.append("PAYRSEQNO, CNTCURR, BCESAGE, BCESTRM, BCESDTE, ");
		sqlCovtSelect.append("BAPPMETH, ZBINSTPREM, ZLINSTPREM, ZDIVOPT, PAYCOY, ");
		sqlCovtSelect.append("PAYCLT, PAYMTH, BANKKEY, BANKACCKEY, PAYCURR, ");
		sqlCovtSelect.append("FACTHOUS, USRPRF, JOBNM, DATIME, LOADPER, ");
		sqlCovtSelect.append("RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZCLSTATE, ZSTPDUTY01, LNKGNO, TPDTYPE, LNKGSUBREFNO, LNKGIND, ");
		sqlCovtSelect.append("GMIB, GMDB, GMWB, RISKPREM, SINGPREMTYPE ");
		sqlCovtSelect.append("FROM COVTPF WHERE CHDRCOY = ? AND ");
		sqlCovtSelect.append(getSqlInStr("CHDRNUM", chdrnumList));
		sqlCovtSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, SEQNBR ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psCovtSelect = getPrepareStatement(sqlCovtSelect.toString());
		ResultSet sqlpCovt1Rs = null;
		Map<String, List<Covtpf>> covtMap = new HashMap<String, List<Covtpf>>();
		try {

			// ---------------------------------
			// Set Parameters dynamically
			// ---------------------------------
			psCovtSelect.setString(1, coy);
			// ---------------------------------
			// Execute Query
			// ---------------------------------
			sqlpCovt1Rs = executeQuery(psCovtSelect);

			// ---------------------------------
			// Iterate result set
			// ---------------------------------
			while (sqlpCovt1Rs.next()) {

				Covtpf covtpf = new Covtpf();

				covtpf.setUniqueNumber(sqlpCovt1Rs.getInt("UNIQUE_NUMBER"));
				covtpf.setChdrcoy(sqlpCovt1Rs.getString("CHDRCOY"));
				covtpf.setChdrnum(sqlpCovt1Rs.getString("CHDRNUM"));
				covtpf.setLife(sqlpCovt1Rs.getString("LIFE"));
				covtpf.setCoverage(sqlpCovt1Rs.getString("COVERAGE"));
				covtpf.setRider(sqlpCovt1Rs.getString("RIDER"));
				covtpf.setTermid(sqlpCovt1Rs.getString("TERMID"));
				covtpf.setTrdt(sqlpCovt1Rs.getInt("TRDT"));
				covtpf.setTrtm(sqlpCovt1Rs.getInt("TRTM"));
				covtpf.setUserT(sqlpCovt1Rs.getInt("USER_T"));
				covtpf.setCrtable(sqlpCovt1Rs.getString("CRTABLE"));
				covtpf.setRcesdte(sqlpCovt1Rs.getInt("RCESDTE"));
				covtpf.setPcesdte(sqlpCovt1Rs.getInt("PCESDTE"));
				covtpf.setRcesage(sqlpCovt1Rs.getInt("RCESAGE"));
				covtpf.setPcesage(sqlpCovt1Rs.getInt("PCESAGE"));
				covtpf.setRcestrm(sqlpCovt1Rs.getInt("RCESTRM"));
				covtpf.setPcestrm(sqlpCovt1Rs.getInt("PCESTRM"));
				covtpf.setSumins(sqlpCovt1Rs.getBigDecimal("SUMINS"));
				covtpf.setLiencd(sqlpCovt1Rs.getString("LIENCD"));
				covtpf.setMortcls(sqlpCovt1Rs.getString("MORTCLS"));
				covtpf.setJlife(sqlpCovt1Rs.getString("JLIFE")!= null ? sqlpCovt1Rs.getString("JLIFE"):" "); //ILIFE-8709
				covtpf.setRsunin(sqlpCovt1Rs.getString("RSUNIN"));
				covtpf.setRundte(sqlpCovt1Rs.getInt("RUNDTE"));
				covtpf.setPolinc(sqlpCovt1Rs.getInt("POLINC"));
				covtpf.setNumapp(sqlpCovt1Rs.getInt("NUMAPP"));
				covtpf.setEffdate(sqlpCovt1Rs.getInt("EFFDATE"));
				covtpf.setSex01(sqlpCovt1Rs.getString("SEX01"));
				covtpf.setSex02(sqlpCovt1Rs.getString("SEX02"));
				covtpf.setAnbccd01(sqlpCovt1Rs.getInt("ANBCCD01"));
				covtpf.setAnbccd02(sqlpCovt1Rs.getInt("ANBCCD02"));
				covtpf.setBillfreq(sqlpCovt1Rs.getString("BILLFREQ"));
				covtpf.setBillchnl(sqlpCovt1Rs.getString("BILLCHNL"));
				covtpf.setSeqnbr(sqlpCovt1Rs.getInt("SEQNBR"));
				covtpf.setSingp(sqlpCovt1Rs.getBigDecimal("SINGP"));
				covtpf.setInstprem(sqlpCovt1Rs.getBigDecimal("INSTPREM"));
				covtpf.setPlnsfx(sqlpCovt1Rs.getInt("PLNSFX"));
				covtpf.setPayrseqno(sqlpCovt1Rs.getInt("PAYRSEQNO"));
				covtpf.setCntcurr(sqlpCovt1Rs.getString("CNTCURR"));
				covtpf.setBcesage(sqlpCovt1Rs.getInt("BCESAGE"));
				covtpf.setBcestrm(sqlpCovt1Rs.getInt("BCESTRM"));
				covtpf.setBcesdte(sqlpCovt1Rs.getInt("BCESDTE"));
				covtpf.setBappmeth(sqlpCovt1Rs.getString("BAPPMETH"));
				covtpf.setZbinstprem(sqlpCovt1Rs.getBigDecimal("ZBINSTPREM"));
				covtpf.setZlinstprem(sqlpCovt1Rs.getBigDecimal("ZLINSTPREM"));
				covtpf.setZdivopt(sqlpCovt1Rs.getString("ZDIVOPT"));
				covtpf.setPaycoy(sqlpCovt1Rs.getString("PAYCOY"));
				covtpf.setPayclt(sqlpCovt1Rs.getString("PAYCLT"));
				covtpf.setPaymth(sqlpCovt1Rs.getString("PAYMTH"));
				covtpf.setBankkey(sqlpCovt1Rs.getString("BANKKEY"));
				covtpf.setBankacckey(sqlpCovt1Rs.getString("BANKACCKEY"));
				covtpf.setPaycurr(sqlpCovt1Rs.getString("PAYCURR"));
				covtpf.setFacthous(sqlpCovt1Rs.getString("FACTHOUS"));
				covtpf.setUsrprf(sqlpCovt1Rs.getString("USRPRF"));
				covtpf.setJobnm(sqlpCovt1Rs.getString("JOBNM"));
				covtpf.setDatime(sqlpCovt1Rs.getString("DATIME"));
				covtpf.setLoadper(sqlpCovt1Rs.getBigDecimal("LOADPER"));
				covtpf.setRateadj(sqlpCovt1Rs.getBigDecimal("RATEADJ"));
				covtpf.setFltmort(sqlpCovt1Rs.getBigDecimal("FLTMORT"));
				covtpf.setPremadj(sqlpCovt1Rs.getBigDecimal("PREMADJ"));
				covtpf.setAgeadj(sqlpCovt1Rs.getBigDecimal("AGEADJ"));
				covtpf.setZclstate(sqlpCovt1Rs.getString("ZCLSTATE"));
				covtpf.setZstpduty01(sqlpCovt1Rs.getBigDecimal("ZSTPDUTY01"));
				covtpf.setLnkgno(sqlpCovt1Rs.getString("LNKGNO"));
				covtpf.setTpdtype(sqlpCovt1Rs.getString("TPDTYPE"));
				covtpf.setLnkgsubrefno(sqlpCovt1Rs.getString("LNKGSUBREFNO"));
				covtpf.setLnkgind(sqlpCovt1Rs.getString("LNKGIND"));
				covtpf.setLnkgno(sqlpCovt1Rs.getString("GMIB")); //ILIFE-8709
				covtpf.setTpdtype(sqlpCovt1Rs.getString("GMDB")); //ILIFE-8709
				covtpf.setLnkgsubrefno(sqlpCovt1Rs.getString("GMWB")); //ILIFE-8709
				covtpf.setLnkgind(sqlpCovt1Rs.getString("RISKPREM")); //ILIFE-8709
				covtpf.setLnkgind(sqlpCovt1Rs.getString("SINGPREMTYPE")); //ILIFE-8709
				if (covtMap.containsKey(covtpf.getChdrnum())) {
                    covtMap.get(covtpf.getChdrnum()).add(covtpf);
                } else {
                    List<Covtpf> covtList = new ArrayList<>();
                    covtList.add(covtpf);
                    covtMap.put(covtpf.getChdrnum(), covtList);
                }
            }

		} catch (SQLException e) {
			LOGGER.error("searchCovt()", e); 
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovtSelect, sqlpCovt1Rs);
		}
		return covtMap;
	
	}
 //ILB-947 end
}

