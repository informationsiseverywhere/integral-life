package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:34
 * Class transformed from COVRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CovrpfTableDAM extends PFAdapterDAM {

	
	public int pfRecLen = 725;//ILIFE-8509
	public FixedLengthStringData covrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData covrpfRecord = covrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(covrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(covrrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(covrrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(covrrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(covrrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(covrrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(covrrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(covrrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(covrrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(covrrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(covrrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(covrrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(covrrec);
	public FixedLengthStringData statreasn = DD.statreasn.copy().isAPartOf(covrrec);
	public PackedDecimalData crrcd = DD.crrcd.copy().isAPartOf(covrrec);
	public PackedDecimalData anbAtCcd = DD.anbccd.copy().isAPartOf(covrrec);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(covrrec);
	public FixedLengthStringData reptcd01 = DD.reptcd.copy().isAPartOf(covrrec);
	public FixedLengthStringData reptcd02 = DD.reptcd.copy().isAPartOf(covrrec);
	public FixedLengthStringData reptcd03 = DD.reptcd.copy().isAPartOf(covrrec);
	public FixedLengthStringData reptcd04 = DD.reptcd.copy().isAPartOf(covrrec);
	public FixedLengthStringData reptcd05 = DD.reptcd.copy().isAPartOf(covrrec);
	public FixedLengthStringData reptcd06 = DD.reptcd.copy().isAPartOf(covrrec);
	public PackedDecimalData crInstamt01 = DD.crinst.copy().isAPartOf(covrrec);
	public PackedDecimalData crInstamt02 = DD.crinst.copy().isAPartOf(covrrec);
	public PackedDecimalData crInstamt03 = DD.crinst.copy().isAPartOf(covrrec);
	public PackedDecimalData crInstamt04 = DD.crinst.copy().isAPartOf(covrrec);
	public PackedDecimalData crInstamt05 = DD.crinst.copy().isAPartOf(covrrec);
	public FixedLengthStringData premCurrency = DD.prmcur.copy().isAPartOf(covrrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(covrrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(covrrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(covrrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(covrrec);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(covrrec);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(covrrec);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(covrrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(covrrec);
	public PackedDecimalData riskCessDate = DD.rcesdte.copy().isAPartOf(covrrec);
	public PackedDecimalData premCessDate = DD.pcesdte.copy().isAPartOf(covrrec);
	public PackedDecimalData benCessDate = DD.bcesdte.copy().isAPartOf(covrrec);
	public PackedDecimalData nextActDate = DD.nxtdte.copy().isAPartOf(covrrec);
	public PackedDecimalData riskCessAge = DD.rcesage.copy().isAPartOf(covrrec);
	public PackedDecimalData premCessAge = DD.pcesage.copy().isAPartOf(covrrec);
	public PackedDecimalData benCessAge = DD.bcesage.copy().isAPartOf(covrrec);
	public PackedDecimalData riskCessTerm = DD.rcestrm.copy().isAPartOf(covrrec);
	public PackedDecimalData premCessTerm = DD.pcestrm.copy().isAPartOf(covrrec);
	public PackedDecimalData benCessTerm = DD.bcestrm.copy().isAPartOf(covrrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(covrrec);
	public FixedLengthStringData sicurr = DD.sicurr.copy().isAPartOf(covrrec);
	public PackedDecimalData varSumInsured = DD.varsi.copy().isAPartOf(covrrec);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(covrrec);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(covrrec);
	public FixedLengthStringData ratingClass = DD.ratcls.copy().isAPartOf(covrrec);
	public FixedLengthStringData indexationInd = DD.indxin.copy().isAPartOf(covrrec);
	public FixedLengthStringData bonusInd = DD.bnusin.copy().isAPartOf(covrrec);
	public FixedLengthStringData deferPerdCode = DD.dpcd.copy().isAPartOf(covrrec);
	public PackedDecimalData deferPerdAmt = DD.dpamt.copy().isAPartOf(covrrec);
	public FixedLengthStringData deferPerdInd = DD.dpind.copy().isAPartOf(covrrec);
	public PackedDecimalData totMthlyBenefit = DD.tmben.copy().isAPartOf(covrrec);
	public PackedDecimalData estMatValue01 = DD.emv.copy().isAPartOf(covrrec);
	public PackedDecimalData estMatValue02 = DD.emv.copy().isAPartOf(covrrec);
	public PackedDecimalData estMatDate01 = DD.emvdte.copy().isAPartOf(covrrec);
	public PackedDecimalData estMatDate02 = DD.emvdte.copy().isAPartOf(covrrec);
	public PackedDecimalData estMatInt01 = DD.emvint.copy().isAPartOf(covrrec);
	public PackedDecimalData estMatInt02 = DD.emvint.copy().isAPartOf(covrrec);
	public FixedLengthStringData campaign = DD.campaign.copy().isAPartOf(covrrec);
	public PackedDecimalData statSumins = DD.stsmin.copy().isAPartOf(covrrec);
	public PackedDecimalData rtrnyrs = DD.rtrnyrs.copy().isAPartOf(covrrec);
	public FixedLengthStringData reserveUnitsInd = DD.rsunin.copy().isAPartOf(covrrec);
	public PackedDecimalData reserveUnitsDate = DD.rundte.copy().isAPartOf(covrrec);
	public FixedLengthStringData chargeOptionsInd = DD.chgopt.copy().isAPartOf(covrrec);
	public FixedLengthStringData fundSplitPlan = DD.fundsp.copy().isAPartOf(covrrec);
	public PackedDecimalData premCessAgeMth = DD.pcamth.copy().isAPartOf(covrrec);
	public PackedDecimalData premCessAgeDay = DD.pcaday.copy().isAPartOf(covrrec);
	public PackedDecimalData premCessTermMth = DD.pctmth.copy().isAPartOf(covrrec);
	public PackedDecimalData premCessTermDay = DD.pctday.copy().isAPartOf(covrrec);
	public PackedDecimalData riskCessAgeMth = DD.rcamth.copy().isAPartOf(covrrec);
	public PackedDecimalData riskCessAgeDay = DD.rcaday.copy().isAPartOf(covrrec);
	public PackedDecimalData riskCessTermMth = DD.rctmth.copy().isAPartOf(covrrec);
	public PackedDecimalData riskCessTermDay = DD.rctday.copy().isAPartOf(covrrec);
	public FixedLengthStringData jlLsInd = DD.jllsid.copy().isAPartOf(covrrec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(covrrec);
	public PackedDecimalData singp = DD.singp.copy().isAPartOf(covrrec);
	public PackedDecimalData rerateDate = DD.rrtdat.copy().isAPartOf(covrrec);
	public PackedDecimalData rerateFromDate = DD.rrtfrm.copy().isAPartOf(covrrec);
	public PackedDecimalData benBillDate = DD.bbldat.copy().isAPartOf(covrrec);
	public PackedDecimalData annivProcDate = DD.cbanpr.copy().isAPartOf(covrrec);
	public PackedDecimalData convertInitialUnits = DD.cbcvin.copy().isAPartOf(covrrec);
	public PackedDecimalData reviewProcessing = DD.cbrvpr.copy().isAPartOf(covrrec);
	public PackedDecimalData unitStatementDate = DD.cbunst.copy().isAPartOf(covrrec);
	public PackedDecimalData cpiDate = DD.cpidte.copy().isAPartOf(covrrec);
	public PackedDecimalData initUnitCancDate = DD.icandt.copy().isAPartOf(covrrec);
	public PackedDecimalData extraAllocDate = DD.exaldt.copy().isAPartOf(covrrec);
	public PackedDecimalData initUnitIncrsDate = DD.iincdt.copy().isAPartOf(covrrec);
	public PackedDecimalData coverageDebt = DD.crdebt.copy().isAPartOf(covrrec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(covrrec);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(covrrec);
	public PackedDecimalData zbinstprem = DD.zbinstprem.copy().isAPartOf(covrrec);
	public PackedDecimalData zlinstprem = DD.zlinstprem.copy().isAPartOf(covrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(covrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(covrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(covrrec);
	//TSD-306 Start
	public PackedDecimalData loadper = DD.loadper.copy().isAPartOf(covrrec);
	public PackedDecimalData rateadj = DD.rateadj.copy().isAPartOf(covrrec);
	public PackedDecimalData fltmort = DD.fltmort.copy().isAPartOf(covrrec);
	public PackedDecimalData premadj = DD.premadj.copy().isAPartOf(covrrec);
	public PackedDecimalData ageadj = DD.adjustageamt.copy().isAPartOf(covrrec);
	//TSD-306 End
	public PackedDecimalData zstpduty01 = DD.zstpduty.copy().isAPartOf(covrrec);
	public FixedLengthStringData zclstate=DD.zclstate.copy().isAPartOf(covrrec);
	public FixedLengthStringData gmib=DD.bnfgrp.copy().isAPartOf(covrrec);
	public FixedLengthStringData gmdb=DD.functn.copy().isAPartOf(covrrec);
	public FixedLengthStringData gmwb=DD.functn.copy().isAPartOf(covrrec);
	public FixedLengthStringData lnkgno = DD.lnkgno.copy().isAPartOf(covrrec); //ILIFE-6941
	public FixedLengthStringData lnkgsubrefno = DD.lnkgsubrefno.copy().isAPartOf(covrrec); //ILIFE-6941
	public FixedLengthStringData tpdtype = DD.tpdtype.copy().isAPartOf(covrrec);//ILIFE-7118
	public FixedLengthStringData lnkgind=DD.jpjlnkind.copy().isAPartOf(covrrec);
	public FixedLengthStringData singpremtype=DD.sngprmtyp.copy().isAPartOf(covrrec);//ILIFE-7805
	public PackedDecimalData riskprem = DD.riskprem.copy().isAPartOf(covrrec); //ILIFE-7845
	public FixedLengthStringData reinstated = DD.reinstated.copy().isAPartOf(covrrec);//ILIFE-8509
	public PackedDecimalData prorateprem = DD.prorateprem.copy().isAPartOf(covrrec);
	public PackedDecimalData commPrem = DD.coyprem.copy().isAPartOf(covrrec);  //IBPLIFE-5237
	/**
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CovrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CovrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CovrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CovrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CovrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("COVRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"CURRFROM, " +
							"CURRTO, " +
							"STATCODE, " +
							"PSTATCODE, " +
							"STATREASN, " +
							"CRRCD, " +
							"ANBCCD, " +
							"SEX, " +
							"REPTCD01, " +
							"REPTCD02, " +
							"REPTCD03, " +
							"REPTCD04, " +
							"REPTCD05, " +
							"REPTCD06, " +
							"CRINST01, " +
							"CRINST02, " +
							"CRINST03, " +
							"CRINST04, " +
							"CRINST05, " +
							"PRMCUR, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"STFUND, " +
							"STSECT, " +
							"STSSECT, " +
							"CRTABLE, " +
							"RCESDTE, " +
							"PCESDTE, " +
							"BCESDTE, " +
							"NXTDTE, " +
							"RCESAGE, " +
							"PCESAGE, " +
							"BCESAGE, " +
							"RCESTRM, " +
							"PCESTRM, " +
							"BCESTRM, " +
							"SUMINS, " +
							"SICURR, " +
							"VARSI, " +
							"MORTCLS, " +
							"LIENCD, " +
							"RATCLS, " +
							"INDXIN, " +
							"BNUSIN, " +
							"DPCD, " +
							"DPAMT, " +
							"DPIND, " +
							"TMBEN, " +
							"EMV01, " +
							"EMV02, " +
							"EMVDTE01, " +
							"EMVDTE02, " +
							"EMVINT01, " +
							"EMVINT02, " +
							"CAMPAIGN, " +
							"STSMIN, " +
							"RTRNYRS, " +
							"RSUNIN, " +
							"RUNDTE, " +
							"CHGOPT, " +
							"FUNDSP, " +
							"PCAMTH, " +
							"PCADAY, " +
							"PCTMTH, " +
							"PCTDAY, " +
							"RCAMTH, " +
							"RCADAY, " +
							"RCTMTH, " +
							"RCTDAY, " +
							"JLLSID, " +
							"INSTPREM, " +
							"SINGP, " +
							"RRTDAT, " +
							"RRTFRM, " +
							"BBLDAT, " +
							"CBANPR, " +
							"CBCVIN, " +
							"CBRVPR, " +
							"CBUNST, " +
							"CPIDTE, " +
							"ICANDT, " +
							"EXALDT, " +
							"IINCDT, " +
							"CRDEBT, " +
							"PAYRSEQNO, " +
							"BAPPMETH, " +
							"ZBINSTPREM, " +
							"ZLINSTPREM, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"LOADPER, " + //TSD-306
						    "RATEADJ, " + //TSD-306
						    "FLTMORT, " + //TSD-306
						    "PREMADJ, " + //TSD-306
						    "AGEADJ, " + //TSD-306
						    "ZSTPDUTY01, " +
						    "ZCLSTATE, " +
						    "GMIB, " +
						    "GMDB, " +
						    "GMWB, " +
						    "LNKGNO, " +
						    "LNKGSUBREFNO, " +
						    "TPDTYPE,"+ //ILIFE-7118
						    "LNKGIND," +
						    "SINGPREMTYPE," +//ILIFE-7805
						    "RISKPREM," + //ILIFE-7845
						    "REINSTATED," +
						    "PRORATEPREM," +
						    "COMMPREM," +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     validflag,
                                     tranno,
                                     currfrom,
                                     currto,
                                     statcode,
                                     pstatcode,
                                     statreasn,
                                     crrcd,
                                     anbAtCcd,
                                     sex,
                                     reptcd01,
                                     reptcd02,
                                     reptcd03,
                                     reptcd04,
                                     reptcd05,
                                     reptcd06,
                                     crInstamt01,
                                     crInstamt02,
                                     crInstamt03,
                                     crInstamt04,
                                     crInstamt05,
                                     premCurrency,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     statFund,
                                     statSect,
                                     statSubsect,
                                     crtable,
                                     riskCessDate,
                                     premCessDate,
                                     benCessDate,
                                     nextActDate,
                                     riskCessAge,
                                     premCessAge,
                                     benCessAge,
                                     riskCessTerm,
                                     premCessTerm,
                                     benCessTerm,
                                     sumins,
                                     sicurr,
                                     varSumInsured,
                                     mortcls,
                                     liencd,
                                     ratingClass,
                                     indexationInd,
                                     bonusInd,
                                     deferPerdCode,
                                     deferPerdAmt,
                                     deferPerdInd,
                                     totMthlyBenefit,
                                     estMatValue01,
                                     estMatValue02,
                                     estMatDate01,
                                     estMatDate02,
                                     estMatInt01,
                                     estMatInt02,
                                     campaign,
                                     statSumins,
                                     rtrnyrs,
                                     reserveUnitsInd,
                                     reserveUnitsDate,
                                     chargeOptionsInd,
                                     fundSplitPlan,
                                     premCessAgeMth,
                                     premCessAgeDay,
                                     premCessTermMth,
                                     premCessTermDay,
                                     riskCessAgeMth,
                                     riskCessAgeDay,
                                     riskCessTermMth,
                                     riskCessTermDay,
                                     jlLsInd,
                                     instprem,
                                     singp,
                                     rerateDate,
                                     rerateFromDate,
                                     benBillDate,
                                     annivProcDate,
                                     convertInitialUnits,
                                     reviewProcessing,
                                     unitStatementDate,
                                     cpiDate,
                                     initUnitCancDate,
                                     extraAllocDate,
                                     initUnitIncrsDate,
                                     coverageDebt,
                                     payrseqno,
                                     bappmeth,
                                     zbinstprem,
                                     zlinstprem,
                                     userProfile,
                                     jobName,
                                     datime,
                                     loadper, //TSD-306
                                 	 rateadj, //TSD-306
                                 	 fltmort, //TSD-306
                                 	 premadj, //TSD-306
                                     ageadj, //TSD-306
                                     zstpduty01,
                                     zclstate,
                                     gmib,
                                     gmdb,
                                     gmwb,
                                     lnkgno,
                                     lnkgsubrefno,
                                     tpdtype, //ILIFE-7118
                                     lnkgind,
                                     singpremtype, //ILIFE-7805
                                     riskprem, //ILIFE-7845
                                     reinstated,//ILIFE-8509
                                     prorateprem,
                                     commPrem,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		validflag.clear();
  		tranno.clear();
  		currfrom.clear();
  		currto.clear();
  		statcode.clear();
  		pstatcode.clear();
  		statreasn.clear();
  		crrcd.clear();
  		anbAtCcd.clear();
  		sex.clear();
  		reptcd01.clear();
  		reptcd02.clear();
  		reptcd03.clear();
  		reptcd04.clear();
  		reptcd05.clear();
  		reptcd06.clear();
  		crInstamt01.clear();
  		crInstamt02.clear();
  		crInstamt03.clear();
  		crInstamt04.clear();
  		crInstamt05.clear();
  		premCurrency.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		statFund.clear();
  		statSect.clear();
  		statSubsect.clear();
  		crtable.clear();
  		riskCessDate.clear();
  		premCessDate.clear();
  		benCessDate.clear();
  		nextActDate.clear();
  		riskCessAge.clear();
  		premCessAge.clear();
  		benCessAge.clear();
  		riskCessTerm.clear();
  		premCessTerm.clear();
  		benCessTerm.clear();
  		sumins.clear();
  		sicurr.clear();
  		varSumInsured.clear();
  		mortcls.clear();
  		liencd.clear();
  		ratingClass.clear();
  		indexationInd.clear();
  		bonusInd.clear();
  		deferPerdCode.clear();
  		deferPerdAmt.clear();
  		deferPerdInd.clear();
  		totMthlyBenefit.clear();
  		estMatValue01.clear();
  		estMatValue02.clear();
  		estMatDate01.clear();
  		estMatDate02.clear();
  		estMatInt01.clear();
  		estMatInt02.clear();
  		campaign.clear();
  		statSumins.clear();
  		rtrnyrs.clear();
  		reserveUnitsInd.clear();
  		reserveUnitsDate.clear();
  		chargeOptionsInd.clear();
  		fundSplitPlan.clear();
  		premCessAgeMth.clear();
  		premCessAgeDay.clear();
  		premCessTermMth.clear();
  		premCessTermDay.clear();
  		riskCessAgeMth.clear();
  		riskCessAgeDay.clear();
  		riskCessTermMth.clear();
  		riskCessTermDay.clear();
  		jlLsInd.clear();
  		instprem.clear();
  		singp.clear();
  		rerateDate.clear();
  		rerateFromDate.clear();
  		benBillDate.clear();
  		annivProcDate.clear();
  		convertInitialUnits.clear();
  		reviewProcessing.clear();
  		unitStatementDate.clear();
  		cpiDate.clear();
  		initUnitCancDate.clear();
  		extraAllocDate.clear();
  		initUnitIncrsDate.clear();
  		coverageDebt.clear();
  		payrseqno.clear();
  		bappmeth.clear();
  		zbinstprem.clear();
  		zlinstprem.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
		//TSD-306 Start
  		loadper.clear();
		rateadj.clear();
		fltmort.clear();
		premadj.clear();
		ageadj.clear();
		//TSD-306 End
		zstpduty01.clear();
		zclstate.clear();
		gmib.clear();
		gmdb.clear();
		gmwb.clear();
		/*ILIFE-6941 start*/
		lnkgno.clear();
		lnkgsubrefno.clear();
		/*ILIFE-6941 end*/
		tpdtype.clear();//ILIFE-7118
		lnkgind.clear();
		singpremtype.clear();//ILIFE-7805
		riskprem.clear(); //ILIFE-7845
		reinstated.clear();//ILIFE-8509
		prorateprem.clear();
		commPrem.clear(); 
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCovrrec() {
  		return covrrec;
	}

	public FixedLengthStringData getCovrpfRecord() {
  		return covrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCovrrec(what);
	}

	public void setCovrrec(Object what) {
  		this.covrrec.set(what);
	}

	public void setCovrpfRecord(Object what) {
  		this.covrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(covrrec.getLength());
		result.set(covrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}