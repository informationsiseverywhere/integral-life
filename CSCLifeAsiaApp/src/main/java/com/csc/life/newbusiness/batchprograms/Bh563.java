/*
 * File: Bh563.java
 * Date: 29 August 2009 21:34:46
 * Author: Quipoz Limited
 * 
 * Class transformed from BH563.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.ZstragpTableDAM;
import com.csc.life.newbusiness.dataaccess.ZstragsTableDAM;
import com.csc.life.newbusiness.dataaccess.ZstragtTableDAM;
import com.csc.life.newbusiness.reports.Rh563Report;
import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.life.newbusiness.tablestructures.Th566rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*    Weekly New Business Production Report.
*
*    This is the program aimed to produce New Business
*    Transactions on a weekly basis. This report will hold such
*    information as Sum Assured, Single and Regular Premium
*    amounts as well as Product and Agency Details.
*
*    This Program reads the New Business Reporting Extract File
*    by using logical files(ZSTRAGT,ZSTRAGP,ZSTRAGS).Transaction
*    Codes on ZSTR will validated against the Transaction Codes
*    on Table (TH565 - Allowable Transaction Code). For each
*    record returned, various values should be accumulated
*    until there is a change in one of the key values i.e. a
*    change in contract type in the ZSTRAGP, a detail
*    line for Annual Premium will be written. It's total line
*    will be written when there is a change in the sales channel
*    in the ZSTRAGT. Similarly for the case of Single Premium,
*    a change in contract type in the ZSTRAGS, a detail line
*    will be written for Single Premium. The total line for
*    single premium will br written when there is a change in
*    the sales channel in the ZSTRAGT.
*
*****************************************************************
* </pre>
*/
public class Bh563 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rh563Report printerFile = new Rh563Report();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH563");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not   
		   be deleted.                                                   */
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCurrencynm = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaSdate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaDatetext = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaDatetextD1 = new FixedLengthStringData(1).isAPartOf(wsaaDatetext, 0);
		/*01  WSAA-CALENDAR-MONTHS.                                        
		01  FILLER                      REDEFINES WSAA-CALENDAR-MONTHS.  
		01  WSAA-QUARTER-MONTHS.                                         
		01  FILLER                      REDEFINES WSAA-QUARTER-MONTHS.   */
	private ZonedDecimalData wsaaMaxValue = new ZonedDecimalData(17, 2).init(999999999999999.99);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaEffectiveDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaEffYr = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0);
	private ZonedDecimalData wsaaEffMth = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
	private ZonedDecimalData wsaaZstrEffDate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(wsaaZstrEffDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaZstrEffmth = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
	private ZonedDecimalData wsaaStartOfYear = new ZonedDecimalData(8, 0);

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaStartOfYear, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaStartYr = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0);
	private ZonedDecimalData wsaaStartMth = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4);
	private ZonedDecimalData wsaaStartDay = new ZonedDecimalData(2, 0).isAPartOf(filler3, 6);
	private ZonedDecimalData wsaaCurrentQuarter = new ZonedDecimalData(8, 0);

	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaCurrentQuarter, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaQtrYr = new ZonedDecimalData(4, 0).isAPartOf(filler4, 0);
	private ZonedDecimalData wsaaQtrMth = new ZonedDecimalData(2, 0).isAPartOf(filler4, 4);
	private ZonedDecimalData wsaaQtrDay = new ZonedDecimalData(2, 0).isAPartOf(filler4, 6);
	private ZonedDecimalData wsaaToQuarter = new ZonedDecimalData(8, 0);

	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaToQuarter, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaQtrYrTo = new ZonedDecimalData(4, 0).isAPartOf(filler5, 0);
	private ZonedDecimalData wsaaQtrMthTo = new ZonedDecimalData(2, 0).isAPartOf(filler5, 4);
	private ZonedDecimalData wsaaQtrDayTo = new ZonedDecimalData(2, 0).isAPartOf(filler5, 6);
	private ZonedDecimalData wsaaQuarter = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaQuarterT = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaOddQuarter = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaClnkCurrIn = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaClnkCompany = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaBillingFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillingFreq, 0).setUnsigned();
	private PackedDecimalData wsaaOccdate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaFirstRead = new FixedLengthStringData(1);
	private Validator firstRead = new Validator(wsaaFirstRead, "Y");

	private FixedLengthStringData wsaaFirstReadAnn = new FixedLengthStringData(1);
	private Validator firstReadAnn = new Validator(wsaaFirstReadAnn, "Y");

	private FixedLengthStringData wsaaFirstReadSgp = new FixedLengthStringData(1);
	private Validator firstReadSgp = new Validator(wsaaFirstReadSgp, "Y");

	private FixedLengthStringData wsaaTrcdeMatch = new FixedLengthStringData(1);
	private Validator trancodeMatch = new Validator(wsaaTrcdeMatch, "Y");
	private Validator trancodeNotMatch = new Validator(wsaaTrcdeMatch, "N");
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAracde = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPremium = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-ANNUALS-TOTALS */
	private PackedDecimalData wsaaAnnMtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAnnPrcMtd = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaAnnCntMtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAnnQtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAnnPrcQtd = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaAnnCntQtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAnnYtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAnnPrcYtd = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaAnnCntYtd = new PackedDecimalData(5, 0);
		/* WSAA-SINGLES-TOTALS */
	private PackedDecimalData wsaaSgpMtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaSgpPrcMtd = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaSgpCntMtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSgpQtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaSgpPrcQtd = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaSgpCntQtd = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaSgpYtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaSgpPrcYtd = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaSgpCntYtd = new PackedDecimalData(5, 0);
		/* WSAA-AGP-ANN-TOTALS */
	private PackedDecimalData wsaaAgpAnnMtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAgpAnnMtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAgpAnnQtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAgpAnnQtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAgpAnnYtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAgpAnnYtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAgpMtdPrcAnn = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaAgpQtdPrcAnn = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaAgpYtdPrcAnn = new PackedDecimalData(5, 2);
		/* WSAA-AGP-SGP-TOTALS */
	private PackedDecimalData wsaaAgpSgpMtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAgpSgpMtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAgpSgpQtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAgpSgpQtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAgpSgpYtd = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaAgpSgpYtdCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaAgpMtdPrcSgp = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaAgpQtdPrcSgp = new PackedDecimalData(5, 2);
	private PackedDecimalData wsaaAgpYtdPrcSgp = new PackedDecimalData(5, 2);

	private FixedLengthStringData wsaaPrintFlag = new FixedLengthStringData(1);
	private Validator annualPremium = new Validator(wsaaPrintFlag, "A");
	private Validator singlePremium = new Validator(wsaaPrintFlag, "S");
	private Validator allPrint = new Validator(wsaaPrintFlag, "Y");
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String zstragtrec = "ZSTRAGTREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String th565 = "TH565";
	private static final String t5688 = "T5688";
	private static final String th566 = "TH566";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ZstragpTableDAM zstragpIO = new ZstragpTableDAM();
	private ZstragsTableDAM zstragsIO = new ZstragsTableDAM();
	private ZstragtTableDAM zstragtIO = new ZstragtTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Th565rec th565rec = new Th565rec();
	private Th566rec th566rec = new Th566rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private PrinterRecInner printerRecInner = new PrinterRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		acumulate2010, 
		nextZstragt2080, 
		exit2090, 
		printTtl2830, 
		validate2860, 
		nextZstragp2880, 
		exit2890, 
		printTtlSgp2830a, 
		endp2840a, 
		validate2860a, 
		nextZstrags2880a, 
		exit2890a
	}

	public Bh563() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.                */
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1030();
		workingStorageFields1040();
		readTh5651050();
		setUpHeadingCurrency1060();
		calendarDesc1061();
	}

protected void initialise1010()
	{
		/* Open required files.                                            */
		printerFile.openOutput();
		wsaaOverflow.set("Y");
		wsspEdterror.set(varcom.oK);
	}

protected void setUpHeadingDates1030()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaSdate.set(datcon1rec.extDate);
		datcon6rec.language.set(bsscIO.getLanguage());
		datcon6rec.company.set(bsprIO.getCompany());
		datcon6rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		wsaaDatetext.set(datcon6rec.intDate2);
		if (isEQ(wsaaDatetextD1, SPACES)) {
			wsaaDatetextD1.set("0");
		}
	}

protected void workingStorageFields1040()
	{
		/*  Initialize the working storage fields                          */
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		indOff.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(13);
		indOff.setTrue(14);
		indOff.setTrue(15);
		indOff.setTrue(16);
		indOff.setTrue(17);
		indOff.setTrue(18);
		indOff.setTrue(19);
		indOff.setTrue(20);
		indOff.setTrue(21);
		indOff.setTrue(22);
		if (isEQ(wsaaEffMth, 1)){
			indOn.setTrue(11);
		}
		else if (isEQ(wsaaEffMth, 2)){
			indOn.setTrue(12);
		}
		else if (isEQ(wsaaEffMth, 3)){
			indOn.setTrue(13);
		}
		else if (isEQ(wsaaEffMth, 4)){
			indOn.setTrue(14);
		}
		else if (isEQ(wsaaEffMth, 5)){
			indOn.setTrue(15);
		}
		else if (isEQ(wsaaEffMth, 6)){
			indOn.setTrue(16);
		}
		else if (isEQ(wsaaEffMth, 7)){
			indOn.setTrue(17);
		}
		else if (isEQ(wsaaEffMth, 8)){
			indOn.setTrue(18);
		}
		else if (isEQ(wsaaEffMth, 9)){
			indOn.setTrue(19);
		}
		else if (isEQ(wsaaEffMth, 10)){
			indOn.setTrue(20);
		}
		else if (isEQ(wsaaEffMth, 11)){
			indOn.setTrue(21);
		}
		else if (isEQ(wsaaEffMth, 12)){
			indOn.setTrue(22);
		}
		wsaaAnnMtd.set(ZERO);
		wsaaAnnCntMtd.set(ZERO);
		wsaaAnnQtd.set(ZERO);
		wsaaAnnCntQtd.set(ZERO);
		wsaaAnnYtd.set(ZERO);
		wsaaAnnCntYtd.set(ZERO);
		wsaaSgpMtd.set(ZERO);
		wsaaSgpCntMtd.set(ZERO);
		wsaaSgpQtd.set(ZERO);
		wsaaSgpCntQtd.set(ZERO);
		wsaaSgpYtd.set(ZERO);
		wsaaSgpCntYtd.set(ZERO);
		wsaaAgpAnnMtd.set(ZERO);
		wsaaAgpAnnMtdCnt.set(ZERO);
		wsaaAgpAnnQtd.set(ZERO);
		wsaaAgpAnnQtdCnt.set(ZERO);
		wsaaAgpAnnYtd.set(ZERO);
		wsaaAgpAnnYtdCnt.set(ZERO);
		wsaaAgpSgpMtd.set(ZERO);
		wsaaAgpSgpMtdCnt.set(ZERO);
		wsaaAgpSgpQtd.set(ZERO);
		wsaaAgpSgpQtdCnt.set(ZERO);
		wsaaAgpSgpYtd.set(ZERO);
		wsaaAgpSgpYtdCnt.set(ZERO);
		wsaaPremium.set(ZERO);
		wsaaQuarter.set(ZERO);
		wsaaOddQuarter.set(ZERO);
		wsaaX.set(ZERO);
		wsaaAgpMtdPrcAnn.set(ZERO);
		wsaaAgpQtdPrcAnn.set(ZERO);
		wsaaAgpYtdPrcAnn.set(ZERO);
		wsaaAgpMtdPrcSgp.set(ZERO);
		wsaaAgpQtdPrcSgp.set(ZERO);
		wsaaAgpYtdPrcSgp.set(ZERO);
		wsaaBranch.set(SPACES);
		wsaaCnttype.set(SPACES);
		wsaaFirstRead.set("Y");
	}

protected void readTh5651050()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th565);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th565rec.th565Rec.set(itemIO.getGenarea());
		if (isLT(wsaaEffMth, th565rec.zmthfrm01)) {
			compute(wsaaStartYr, 0).set(sub(wsaaEffYr, 1));
		}
		else {
			wsaaStartYr.set(wsaaEffYr);
		}
		wsaaStartMth.set(th565rec.zmthfrm01);
		wsaaStartDay.set(1);
		for (wsaaX.set(1); !(isGT(wsaaX, 4)
		|| isGT(wsaaQuarter, 0)); wsaaX.add(1)){
			if (isLT(th565rec.zmthfrm[wsaaX.toInt()], th565rec.zmthto[wsaaX.toInt()])) {
				if (isGTE(wsaaEffMth, th565rec.zmthfrm[wsaaX.toInt()])
				&& isLTE(wsaaEffMth, th565rec.zmthto[wsaaX.toInt()])) {
					wsaaQuarter.set(wsaaX);
				}
			}
			else {
				wsaaOddQuarter.set(wsaaX);
			}
		}
		if (isEQ(wsaaQuarter, ZERO)) {
			wsaaQuarter.set(wsaaOddQuarter);
		}
		if (isLT(wsaaEffMth, th565rec.zmthfrm[wsaaQuarter.toInt()])) {
			compute(wsaaQtrYr, 0).set(sub(wsaaEffYr, 1));
		}
		else {
			wsaaQtrYr.set(wsaaEffYr);
		}
		wsaaQtrMth.set(th565rec.zmthfrm[wsaaQuarter.toInt()]);
		if (isEQ(wsaaQuarter, 4)) {
			wsaaQuarterT.set(1);
		}
		else {
			compute(wsaaQuarterT, 0).set(add(wsaaQuarter, 1));
		}
		wsaaQtrMthTo.set(th565rec.zmthfrm[wsaaQuarterT.toInt()]);
		wsaaQtrDay.set(1);
		wsaaQtrDayTo.set(1);
		wsaaQtrYrTo.set(wsaaQtrYr);
		if (isLT(wsaaQtrMthTo, wsaaQtrMth)) {
			compute(wsaaQtrYrTo, 0).set(add(wsaaQtrYr, 1));
		}
	}

protected void setUpHeadingCurrency1060()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(th565rec.currcode);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		/*  IF DESC-STATUZ           NOT = O-K                   <LA2679>*/
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setDescitem("***");
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
		}
		wsaaCurrcode.set(th565rec.currcode);
		wsaaCurrencynm.set(descIO.getLongdesc());
	}

protected void calendarDesc1061()
	{
		/***** MOVE SPACES                 TO WSAA-TR386-ID.        <FA1226>*/
		/**    PERFORM 1100-READ-TR386.                             <LA3235>*/
		/***** MOVE TR386-PROGDESC-1       TO WSAA-JANUARY.         <FA1226>*/
		/***** MOVE TR386-PROGDESC-2       TO WSAA-FEBRUARY.        <FA1226>*/
		/***** MOVE TR386-PROGDESC-3       TO WSAA-MARCH.           <FA1226>*/
		/***** MOVE TR386-PROGDESC-4       TO WSAA-APRIL.           <FA1226>*/
		/***** MOVE TR386-PROGDESC-5       TO WSAA-MAY.             <FA1226>*/
		/***** MOVE '1'                    TO WSAA-TR386-ID.        <FA1226>*/
		/***** PERFORM 1100-READ-TR386.                             <FA1226>*/
		/***** MOVE TR386-PROGDESC-1       TO WSAA-JUNE.            <FA1226>*/
		/***** MOVE TR386-PROGDESC-2       TO WSAA-JULY.            <FA1226>*/
		/***** MOVE TR386-PROGDESC-3       TO WSAA-AUGUST.          <FA1226>*/
		/***** MOVE TR386-PROGDESC-4       TO WSAA-SEPTEMBER.       <FA1226>*/
		/***** MOVE TR386-PROGDESC-5       TO WSAA-OCTOBER.         <FA1226>*/
		/***** MOVE '2'                    TO WSAA-TR386-ID.        <FA1226>*/
		/***** PERFORM 1100-READ-TR386.                             <FA1226>*/
		/***** MOVE TR386-PROGDESC-1       TO WSAA-NOVEMBER.        <FA1226>*/
		/***** MOVE TR386-PROGDESC-2       TO WSAA-DECEMBER.        <FA1226>*/
		/*INIT-ZSTRAGT*/
		/*  Read ZSTRAGT for the first time to obtain the first branch code*/
		zstragtIO.setDataArea(SPACES);
		zstragtIO.setChdrcoy(bsprIO.getCompany());
		zstragtIO.setEffdate(bsscIO.getEffectiveDate());
		zstragtIO.setFormat(zstragtrec);
		zstragtIO.setFunction(varcom.begn);
		/*EXIT*/
	}

	/**
	* <pre>
	*1100-READ-TR386 SECTION.                                 <LA3235>
	*1110-READ-TR386.                                         <LA3235>
	*--- Read TR386 table to get screen literals                      
	*    MOVE ITEMREC                TO ITEM-FORMAT.          <LA3235>
	*    MOVE 'IT'                   TO ITEM-ITEMPFX.         <LA3235>
	*    MOVE BSPR-COMPANY           TO ITEM-ITEMCOY.         <LA3235>
	*    MOVE TR386                  TO ITEM-ITEMTABL.        <LA3235>
	*    MOVE BSSC-LANGUAGE          TO WSAA-TR386-LANG.      <LA3235>
	*    MOVE WSAA-PROG              TO WSAA-TR386-PGM.       <LA3235>
	*    MOVE WSAA-TR386-KEY         TO ITEM-ITEMITEM.        <LA3235>
	*    MOVE SPACES                 TO ITEM-ITEMSEQ.         <LA3235>
	**** MOVE READR                  TO ITEM-FUNCTION.        <FA1226>
	*    MOVE BEGN                   TO ITEM-FUNCTION.        <LA3235>
	*    MOVE O-K                    TO ITEM-STATUZ.          <LA3235>
	*    MOVE 1                      TO IY.                   <LA3235>
	*    PERFORM 1200-LOAD-TR386                              <LA3235>
	*        UNTIL ITEM-STATUZ = ENDP.                        <LA3235>
	**** CALL  'ITEMIO'           USING ITEM-PARAMS.          <FA1226>
	**** IF  ITEM-STATUZ          NOT = O-K                   <FA1226>
	****     MOVE ITEM-STATUZ        TO SYSR-STATUZ           <FA1226>
	****     MOVE ITEM-PARAMS        TO SYSR-PARAMS           <FA1226>
	****     PERFORM 600-FATAL-ERROR                          <FA1226>
	**** END-IF.                                              <FA1226>
	**** MOVE ITEM-GENAREA        TO TR386-TR386-REC.         <FA1226>
	*1190-EXIT.                                               <LA3235>
	*    EXIT.                                                <LA3235>
	*1200-LOAD-TR386 SECTION.                                 <LA3235>
	*1210-START.                                              <LA3235>
	*    CALL 'ITEMIO'               USING ITEM-PARAMS.       <LA3235>
	*    IF ITEM-ITEMCOY             NOT = BSPR-COMPANY       <LA3235>
	*    OR ITEM-ITEMTABL            NOT = TR386              <LA3235>
	*    OR ITEM-ITEMITEM            NOT = WSAA-TR386-KEY     <LA3235>
	*    OR ITEM-STATUZ              NOT = O-K                <LA3235>
	*        IF ITEM-FUNCTION        = BEGN                   <LA3235>
	*            MOVE WSAA-TR386-KEY TO ITEM-ITEMITEM         <LA3235>
	*            MOVE ITEM-PARAMS    TO SYSR-PARAMS           <LA3235>
	*            PERFORM 600-FATAL-ERROR                      <LA3235>
	*        ELSE                                             <LA3235>
	*            MOVE ENDP           TO ITEM-STATUZ           <LA3235>
	*            GO TO 1290-EXIT                              <LA3235>
	*       END-IF                                            <LA3235>
	*    END-IF.                                              <LA3235>
	*    MOVE ITEM-GENAREA           TO TR386-TR386-REC.      <LA3235>
	*    MOVE 1                      TO IX.                   <LA3235>
	*    PERFORM UNTIL IX            > 10                     <LA3235>
	*       IF  TR386-PROGDESC(IX)   NOT = SPACES             <LA3235>
	*       AND IY                   < 13                     <LA3235>
	*           MOVE TR386-PROGDESC(IX) TO WSAA-CALENDAR-MONTH(IY)    
	*           ADD 1                TO IY                    <LA3235>
	*       END-IF                                            <LA3235>
	*       ADD 1                    TO IX                    <LA3235>
	*    END-PERFORM.                                         <LA3235>
	*    MOVE NEXTR                  TO ITEM-FUNCTION.        <LA3235>
	*1290-EXIT.                                               <LA3235>
	*    EXIT.                                                <LA3235>
	* </pre>
	*/
protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case acumulate2010: 
					acumulate2010();
				case nextZstragt2080: 
					nextZstragt2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		zstragtio2100z();
		if (isNE(zstragtIO.getChdrcoy(), bsprIO.getCompany())) {
			zstragtIO.setStatuz(varcom.endp);
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(zstragtIO.getStatuz(), varcom.endp)) {
			if (isNE(wsaaCompany, SPACES)
			&& isNE(wsaaBranch, SPACES)
			&& isNE(wsaaAracde, SPACES)
			&& isNE(wsaaAgntnum, SPACES)) {
				annualPremium.setTrue();
				while ( !(allPrint.isTrue())) {
					changeOfKey2700();
				}
				
			}
			printerRecInner.printerRec.set(SPACES);
			printerFile.printRh563e01(printerRecInner.printerRec);
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		trancodeNotMatch.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 10)
		|| isEQ(wsaaTrcdeMatch, "Y")); wsaaSub.add(1)){
			if (isEQ(zstragtIO.getBatctrcde(), th565rec.ztrcde[wsaaSub.toInt()])) {
				trancodeMatch.setTrue();
			}
		}
		if (trancodeNotMatch.isTrue()
		|| isLT(zstragtIO.getEffdate(), wsaaStartOfYear)) {
			goTo(GotoLabel.nextZstragt2080);
		}
		if (firstRead.isTrue()) {
			goTo(GotoLabel.acumulate2010);
		}
		/* Ignore the records of ZSTRAGT if INSTPREM & SINGP both          */
		/* equal to zeroes.                                                */
		if (isEQ(zstragtIO.getInstprem(), 0)
		&& isEQ(zstragtIO.getSingp(), 0)) {
			goTo(GotoLabel.nextZstragt2080);
		}
		/* Different AGENT encounter/Out of valid evaluating year          */
		if (isNE(zstragtIO.getChdrcoy(), wsaaCompany)
		|| isNE(zstragtIO.getCntbranch(), wsaaBranch)
		|| isNE(zstragtIO.getAracde(), wsaaAracde)
		|| isNE(zstragtIO.getAgntnum(), wsaaAgntnum)
		|| isLT(zstragtIO.getEffdate(), wsaaStartOfYear)) {
			annualPremium.setTrue();
			while ( !(allPrint.isTrue())) {
				changeOfKey2700();
			}
			
			wsaaCnttype.set(SPACES);
		}
	}

protected void acumulate2010()
	{
		wsaaFirstRead.set("N");
		keyChange2300z();
		wsspEdterror.set(varcom.oK);
		/*  If effective date of record is greater than BSSC-EFFECTIVE-DATE*/
		/*  ignore this record as belong to a previous run.                */
		if (isGT(zstragtIO.getEffdate(), bsscIO.getEffectiveDate())) {
			goTo(GotoLabel.nextZstragt2080);
		}
		/*  Read TH566 for policy count - whether to add, subtract         */
		/*  or do nothing.                                                 */
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItemitem(zstragtIO.getBatctrcde());
		readTh5665100();
		/*  Convert the billing frequency to numeric for computation.      */
		wsaaBillingFreq.set(zstragtIO.getBillfreq());
		/*  Set up risk commencement date for reading exchange rate.       */
		wsaaOccdate.set(zstragtIO.getOccdate());
		/*  When ZSTRAGT-EFFDATE and BSSC-EFFECTIVE-DATE falls within the  */
		/*  same calendar month, accumulate the MTD, QTD and YTD premiums. */
		wsaaZstrEffDate.set(zstragtIO.getEffdate());
		if (isEQ(wsaaZstrEffmth, wsaaEffMth)) {
			if (isNE(zstragtIO.getInstprem(), ZERO)) {
				compute(wsaaPremium, 2).set(mult((add(zstragtIO.getInstprem(), zstragtIO.getCntfee())), wsaaBillfreq));
				compCurr2500x();
				wsaaAnnMtd.add(wsaaPremium);
				wsaaAnnQtd.add(wsaaPremium);
				wsaaAnnYtd.add(wsaaPremium);
				wsaaAnnCntMtd.add(th566rec.cnt);
				wsaaAnnCntQtd.add(th566rec.cnt);
				wsaaAnnCntYtd.add(th566rec.cnt);
			}
			if (isNE(zstragtIO.getSingp(), ZERO)) {
				compute(wsaaPremium, 2).set(add(zstragtIO.getSingp(), zstragtIO.getCntfee()));
				compCurr2500x();
				wsaaSgpMtd.add(wsaaPremium);
				wsaaSgpQtd.add(wsaaPremium);
				wsaaSgpYtd.add(wsaaPremium);
				wsaaSgpCntMtd.add(th566rec.cnt);
				wsaaSgpCntQtd.add(th566rec.cnt);
				wsaaSgpCntYtd.add(th566rec.cnt);
			}
			goTo(GotoLabel.nextZstragt2080);
		}
		/*   When ZSTRAGT-EFFDATE falls within the current quarter,        */
		/*   accumulate QTD and YTD.                                       */
		if (isGTE(zstragtIO.getEffdate(), wsaaCurrentQuarter)
		&& isLT(zstragtIO.getEffdate(), wsaaToQuarter)) {
			if (isNE(zstragtIO.getInstprem(), ZERO)) {
				compute(wsaaPremium, 2).set(mult((add(zstragtIO.getInstprem(), zstragtIO.getCntfee())), wsaaBillfreq));
				compCurr2500x();
				wsaaAnnQtd.add(wsaaPremium);
				wsaaAnnYtd.add(wsaaPremium);
				wsaaAnnCntQtd.add(th566rec.cnt);
				wsaaAnnCntYtd.add(th566rec.cnt);
			}
			if (isNE(zstragtIO.getSingp(), ZERO)) {
				compute(wsaaPremium, 2).set(add(zstragtIO.getSingp(), zstragtIO.getCntfee()));
				compCurr2500x();
				wsaaSgpQtd.add(wsaaPremium);
				wsaaSgpYtd.add(wsaaPremium);
				wsaaSgpCntQtd.add(th566rec.cnt);
				wsaaSgpCntYtd.add(th566rec.cnt);
			}
			goTo(GotoLabel.nextZstragt2080);
		}
		/*   When ZSTRAGT-EFFDATE is not in the current quarter, accumulate*/
		/*   only the YTD.                                                 */
		if (isLT(zstragtIO.getEffdate(), wsaaCurrentQuarter)) {
			if (isNE(zstragtIO.getInstprem(), ZERO)) {
				compute(wsaaPremium, 2).set(mult((add(zstragtIO.getInstprem(), zstragtIO.getCntfee())), wsaaBillfreq));
				compCurr2500x();
				wsaaAnnYtd.add(wsaaPremium);
				wsaaAnnCntYtd.add(th566rec.cnt);
			}
			if (isNE(zstragtIO.getSingp(), ZERO)) {
				compute(wsaaPremium, 2).set(add(zstragtIO.getSingp(), zstragtIO.getCntfee()));
				compCurr2500x();
				wsaaSgpYtd.add(wsaaPremium);
				wsaaSgpCntYtd.add(th566rec.cnt);
			}
		}
	}

protected void nextZstragt2080()
	{
		zstragtIO.setFunction(varcom.nextr);
	}

protected void edit2500()
	{
		/*EXIT*/
	}

protected void printHeader2600()
	{
		header2610();
		setUpHeadingCompany2620();
		setUpHeadingBranch2620();
	}

protected void header2610()
	{
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.currcode.set(wsaaCurrcode);
		printerRecInner.currencynm.set(wsaaCurrencynm);
		printerRecInner.sdate.set(wsaaSdate);
		/* MOVE WSAA-DATETEXT     TO DATETEXT   OF RH563H01-O.  <MLS002>*/
		printerRecInner.datetexc.set(wsaaDatetext);
	}

protected void setUpHeadingCompany2620()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		printerRecInner.company.set(wsaaCompany);
		printerRecInner.companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch2620()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsaaCompany);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(wsaaBranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		printerRecInner.branch.set(wsaaBranch);
		printerRecInner.branchnm.set(descIO.getLongdesc());
		printerRecInner.aracde.set(wsaaAracde);
		printerRecInner.agntnum.set(wsaaAgntnum);
		printerFile.printRh563h01(printerRecInner.printerRec);
		printerRecInner.printerRec.set(SPACES);
		if (annualPremium.isTrue()) {
			printerFile.printRh563h02(printerRecInner.printerRec);
		}
		if (singlePremium.isTrue()) {
			printerFile.printRh563h03(printerRecInner.printerRec);
		}
		printerRecInner.printerRec.set(SPACES);
		/*    MOVE WSAA-CALENDAR-MONTH (WSAA-EFF-MTH)              <LA3235>*/
		/*                                TO DATETEXT01 OF RH563H04-O.     */
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(wsaaCurrentQuarter);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		printerRecInner.effdate.set(datcon1rec.extDate);
		/*   MOVE WSAA-QUARTER-MONTH (WSAA-QTR-MTH)                       */
		/*                               TO AORIGCUR   OF RH563H04-O.     */
		/*    WRITE PRINTER-REC           FORMAT IS 'RH563H04'.    <LA3235>*/
		/*    WRITE PRINTER-REC           FORMAT IS 'RH562H04'     <LA5013>*/
		printerFile.printRh563h04(printerRecInner.printerRec, indicArea);
		wsaaOverflow.set("N");
	}

protected void changeOfKey2700()
	{
		change2710();
	}

protected void change2710()
	{
		firstReadAnn.setTrue();
		zstragpIO.setParams(SPACES);
		zstragpIO.setChdrcoy(bsprIO.getCompany());
		zstragpIO.setCntbranch(wsaaBranch);
		zstragpIO.setAracde(wsaaAracde);
		zstragpIO.setAgntnum(wsaaAgntnum);
		zstragpIO.setEffdate(wsaaEffdate);
		zstragpIO.setInstprem(wsaaMaxValue);
		zstragpIO.setCnttype(SPACES);
		zstragpIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zstragpIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zstragpIO.setFitKeysSearch("CHDRCOY");
		while ( !(isEQ(zstragpIO.getStatuz(),varcom.endp))) {
			loopThruZstragp2800();
		}
		
		firstReadSgp.setTrue();
		singlePremium.setTrue();
		wsaaCnttype.set(SPACES);
		zstragsIO.setParams(SPACES);
		zstragsIO.setChdrcoy(bsprIO.getCompany());
		zstragsIO.setCntbranch(wsaaBranch);
		zstragsIO.setAracde(wsaaAracde);
		zstragsIO.setAgntnum(wsaaAgntnum);
		zstragsIO.setEffdate(wsaaEffdate);
		zstragsIO.setSingp(wsaaMaxValue);
		zstragsIO.setCnttype(SPACES);
		zstragsIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zstragsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		zstragsIO.setFitKeysSearch("CHDRCOY");
		while ( !(isEQ(zstragsIO.getStatuz(),varcom.endp))) {
			loopThruZstrags2800a();
		}
		
		allPrint.setTrue();
		if (isEQ(zstragsIO.getStatuz(), varcom.endp)) {
			writeFooter3770();
			wsspEdterror.set(varcom.endp);
			wsaaFirstRead.set("Y");
			wsaaOverflow.set("Y");
		}
	}

protected void loopThruZstragp2800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					loop2810();
					diffContractType2820();
				case printTtl2830: 
					printTtl2830();
				case validate2860: 
					validate2860();
				case nextZstragp2880: 
					nextZstragp2880();
				case exit2890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loop2810()
	{
		zstragpio2800z();
		if (isNE(zstragpIO.getChdrcoy(), bsprIO.getCompany())) {
			zstragpIO.setStatuz(varcom.endp);
		}
		if (isEQ(zstragpIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.printTtl2830);
		}
		if (firstReadAnn.isTrue()) {
			/*    AND ZSTRAGP-INSTPREM NOT = 0                                 */
			if (isNE(wsaaAnnMtd, 0)
			|| isNE(wsaaAnnQtd, 0)
			|| isNE(wsaaAnnYtd, 0)) {
				printHeader2600();
				wsaaFirstReadAnn.set("N");
			}
		}
		if (isEQ(zstragpIO.getInstprem(), 0)
		&& isEQ(zstragpIO.getSingp(), 0)) {
			goTo(GotoLabel.nextZstragp2880);
		}
		if (isNE(zstragpIO.getChdrcoy(), wsaaCompany)
		|| isNE(zstragpIO.getCntbranch(), wsaaBranch)
		|| isNE(zstragpIO.getAracde(), wsaaAracde)
		|| isNE(zstragpIO.getAgntnum(), wsaaAgntnum)) {
			if (annualPremium.isTrue()) {
				if (isNE(wsaaAnnMtd, 0)
				|| isNE(wsaaAnnQtd, 0)
				|| isNE(wsaaAnnYtd, 0)) {
					goTo(GotoLabel.printTtl2830);
				}
			}
		}
	}

protected void diffContractType2820()
	{
		if (isEQ(zstragpIO.getInstprem(), 0)) {
			goTo(GotoLabel.nextZstragp2880);
		}
		if (isEQ(wsaaCnttype, SPACES)) {
			wsaaCnttype.set(zstragpIO.getCnttype());
		}
		if (isNE(zstragpIO.getCnttype(), wsaaCnttype)) {
			if (isNE(wsaaAgpAnnMtd, 0)
			|| isNE(wsaaAgpAnnQtd, 0)
			|| isNE(wsaaAgpAnnYtd, 0)) {
				computePercentages2900();
				detail3000p();
			}
		}
		if (isEQ(zstragtIO.getStatuz(), varcom.endp)
		&& isEQ(zstragpIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.validate2860);
		}
	}

protected void printTtl2830()
	{
		if (isNE(wsaaAgpAnnMtd, 0)
		|| isNE(wsaaAgpAnnQtd, 0)
		|| isNE(wsaaAgpAnnYtd, 0)) {
			computePercentages2900();
			detail3000p();
		}
		wsaaCnttype.set(SPACES);
		if (isNE(wsaaAnnMtd, 0)
		|| isNE(wsaaAnnQtd, 0)
		|| isNE(wsaaAnnYtd, 0)) {
			resetProductAmount3300();
			writeTotalAnnual3710();
		}
		/*ENDP*/
		zstragpIO.setStatuz(varcom.endp);
		if (isEQ(zstragpIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2890);
		}
	}

protected void validate2860()
	{
		trancodeNotMatch.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 10)
		|| isEQ(wsaaTrcdeMatch, "Y")); wsaaSub.add(1)){
			if (isEQ(zstragpIO.getBatctrcde(), th565rec.ztrcde[wsaaSub.toInt()])) {
				trancodeMatch.setTrue();
			}
		}
		if (trancodeNotMatch.isTrue()
		|| isLT(zstragpIO.getEffdate(), wsaaStartOfYear)) {
			goTo(GotoLabel.nextZstragp2880);
		}
		/*  Skip records with effective dates > BSSC-EFFECTIVE-DATE as     */
		/*  they are for previous runs.                                    */
		if (isGT(zstragpIO.getEffdate(), bsscIO.getEffectiveDate())) {
			goTo(GotoLabel.nextZstragp2880);
		}
		/*  Read TH566 for policy count - whether to add, subtract         */
		/*  or do nothing.                                                 */
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItemitem(zstragpIO.getBatctrcde());
		readTh5665100();
		/*  Convert the billing frequency to numeric for computation.      */
		wsaaBillingFreq.set(zstragpIO.getBillfreq());
		/*  Set up risk commencement date for reading exchange rate.       */
		wsaaOccdate.set(zstragpIO.getOccdate());
		/*  If ZSTRAGP-ZEFFDATE and BSSC-EFFECTIVE-DATE are in the same    */
		/*  calender month,include premium in the month to date,           */
		/*  quarter to date and year to date for each product.             */
		wsaaZstrEffDate.set(zstragpIO.getEffdate());
		if (isEQ(wsaaZstrEffmth, wsaaEffMth)) {
			if (annualPremium.isTrue()) {
				compute(wsaaPremium, 2).set(mult((add(zstragpIO.getInstprem(), zstragpIO.getCntfee())), wsaaBillfreq));
				compCurr2800x();
				wsaaAgpAnnMtd.add(wsaaPremium);
				wsaaAgpAnnQtd.add(wsaaPremium);
				wsaaAgpAnnYtd.add(wsaaPremium);
				wsaaAgpAnnMtdCnt.add(th566rec.cnt);
				wsaaAgpAnnQtdCnt.add(th566rec.cnt);
				wsaaAgpAnnYtdCnt.add(th566rec.cnt);
			}
			goTo(GotoLabel.nextZstragp2880);
		}
		/*  If ZSTRAGP-EFFDATE is in the current quarter, include premium  */
		/*  in the quarter to date and year to date amount for each product*/
		if (isGTE(zstragpIO.getEffdate(), wsaaCurrentQuarter)
		&& isLT(zstragpIO.getEffdate(), wsaaToQuarter)) {
			if (annualPremium.isTrue()) {
				compute(wsaaPremium, 2).set(mult((add(zstragpIO.getInstprem(), zstragpIO.getCntfee())), wsaaBillfreq));
				compCurr2800x();
				wsaaAgpAnnQtd.add(wsaaPremium);
				wsaaAgpAnnYtd.add(wsaaPremium);
				wsaaAgpAnnQtdCnt.add(th566rec.cnt);
				wsaaAgpAnnYtdCnt.add(th566rec.cnt);
			}
			goTo(GotoLabel.nextZstragp2880);
		}
		/*  If ZSTRAGP-EFFDATE is not in the current quarter, include the  */
		/*  premium in only the year to date amount.                       */
		if (isLT(zstragpIO.getEffdate(), wsaaCurrentQuarter)) {
			if (annualPremium.isTrue()) {
				compute(wsaaPremium, 2).set(mult((add(zstragpIO.getInstprem(), zstragpIO.getCntfee())), wsaaBillfreq));
				compCurr2800x();
				wsaaAgpAnnYtd.add(wsaaPremium);
				wsaaAgpAnnYtdCnt.add(th566rec.cnt);
			}
		}
	}

protected void nextZstragp2880()
	{
		zstragpIO.setFunction(varcom.nextr);
	}

protected void loopThruZstrags2800a()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					loop2810a();
					diffContractType2820a();
				case printTtlSgp2830a: 
					printTtlSgp2830a();
				case endp2840a: 
					endp2840a();
				case validate2860a: 
					validate2860a();
				case nextZstrags2880a: 
					nextZstrags2880a();
				case exit2890a: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loop2810a()
	{
		zstragsio2900z();
		if (isNE(zstragsIO.getChdrcoy(), bsprIO.getCompany())) {
			zstragsIO.setStatuz(varcom.endp);
		}
		if (isEQ(zstragsIO.getStatuz(), varcom.endp)) {
			if (singlePremium.isTrue()) {
				if (isNE(wsaaSgpMtd, 0)
				|| isNE(wsaaSgpQtd, 0)
				|| isNE(wsaaSgpYtd, 0)) {
					goTo(GotoLabel.printTtlSgp2830a);
				}
			}
		}
		if (isEQ(zstragsIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.endp2840a);
		}
		if (firstReadSgp.isTrue()) {
			if (isNE(wsaaSgpMtd, 0)
			|| isNE(wsaaSgpQtd, 0)
			|| isNE(wsaaSgpYtd, 0)) {
				printHeader2600();
				wsaaFirstReadSgp.set("N");
			}
		}
		if (isEQ(zstragsIO.getInstprem(), 0)
		&& isEQ(zstragsIO.getSingp(), 0)) {
			goTo(GotoLabel.nextZstrags2880a);
		}
		if (isNE(zstragsIO.getChdrcoy(), wsaaCompany)
		|| isNE(zstragsIO.getCntbranch(), wsaaBranch)
		|| isNE(zstragsIO.getAracde(), wsaaAracde)
		|| isNE(zstragsIO.getAgntnum(), wsaaAgntnum)) {
			if (singlePremium.isTrue()) {
				if (isNE(wsaaSgpMtd, 0)
				|| isNE(wsaaSgpQtd, 0)
				|| isNE(wsaaSgpYtd, 0)) {
					goTo(GotoLabel.printTtlSgp2830a);
				}
			}
		}
	}

protected void diffContractType2820a()
	{
		if (isEQ(zstragsIO.getSingp(), 0)) {
			goTo(GotoLabel.nextZstrags2880a);
		}
		if (isEQ(wsaaCnttype, SPACES)) {
			wsaaCnttype.set(zstragsIO.getCnttype());
		}
		if (isNE(zstragsIO.getCnttype(), wsaaCnttype)) {
			if (isEQ(zstragsIO.getChdrcoy(), wsaaCompany)
			&& isEQ(zstragsIO.getCntbranch(), wsaaBranch)
			&& isEQ(zstragsIO.getAracde(), wsaaAracde)
			&& isEQ(zstragsIO.getAgntnum(), wsaaAgntnum)) {
				if (isNE(wsaaAgpSgpMtd, 0)
				|| isNE(wsaaAgpSgpQtd, 0)
				|| isNE(wsaaAgpSgpYtd, 0)) {
					computePercentages2900();
					detail3000p();
				}
			}
		}
		if (isEQ(zstragtIO.getStatuz(), varcom.endp)
		&& isEQ(zstragsIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.validate2860a);
		}
	}

protected void printTtlSgp2830a()
	{
		if (isNE(wsaaAgpSgpMtd, 0)
		|| isNE(wsaaAgpSgpQtd, 0)
		|| isNE(wsaaAgpSgpYtd, 0)) {
			computePercentages2900();
			detail3000p();
		}
		wsaaCnttype.set(SPACES);
		resetProductAmount3300();
		writeTotalSingle3730();
	}

protected void endp2840a()
	{
		zstragsIO.setStatuz(varcom.endp);
		if (isEQ(zstragsIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit2890a);
		}
	}

protected void validate2860a()
	{
		trancodeNotMatch.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 10)
		|| isEQ(wsaaTrcdeMatch, "Y")); wsaaSub.add(1)){
			if (isEQ(zstragsIO.getBatctrcde(), th565rec.ztrcde[wsaaSub.toInt()])) {
				trancodeMatch.setTrue();
			}
		}
		if (trancodeNotMatch.isTrue()
		|| isLT(zstragsIO.getEffdate(), wsaaStartOfYear)) {
			goTo(GotoLabel.nextZstrags2880a);
		}
		/*  Skip records with effective dates > BSSC-EFFECTIVE-DATE as     */
		/*  they are for previous runs.                                    */
		if (isGT(zstragsIO.getEffdate(), bsscIO.getEffectiveDate())) {
			goTo(GotoLabel.nextZstrags2880a);
		}
		/*  Read TH566 for policy count - whether to add, subtract         */
		/*  or do nothing.                                                 */
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItemitem(zstragsIO.getBatctrcde());
		readTh5665100();
		/*  Set up risk commencement date for reading exchange rate.       */
		wsaaOccdate.set(zstragsIO.getOccdate());
		/*  If ZSTRAGS-ZEFFDATE and BSSC-EFFECTIVE-DATE are in the same    */
		/*  calender month,include premium in the month to date,           */
		/*  quarter to date and year to date for each product.             */
		wsaaZstrEffDate.set(zstragsIO.getEffdate());
		if (isEQ(wsaaZstrEffmth, wsaaEffMth)) {
			if (singlePremium.isTrue()) {
				compute(wsaaPremium, 2).set(add(zstragsIO.getSingp(), zstragsIO.getCntfee()));
				compCurr2900x();
				wsaaAgpSgpMtd.add(wsaaPremium);
				wsaaAgpSgpQtd.add(wsaaPremium);
				wsaaAgpSgpYtd.add(wsaaPremium);
				wsaaAgpSgpMtdCnt.add(th566rec.cnt);
				wsaaAgpSgpQtdCnt.add(th566rec.cnt);
				wsaaAgpSgpYtdCnt.add(th566rec.cnt);
			}
			goTo(GotoLabel.nextZstrags2880a);
		}
		/*  If ZSTRAGS-EFFDATE is in the current quarter, include premium  */
		/*  in the quarter to date and year to date amount for each product*/
		if (isGTE(zstragsIO.getEffdate(), wsaaCurrentQuarter)
		&& isLT(zstragsIO.getEffdate(), wsaaToQuarter)) {
			if (singlePremium.isTrue()) {
				compute(wsaaPremium, 2).set(add(zstragsIO.getSingp(), zstragsIO.getCntfee()));
				compCurr2900x();
				wsaaAgpSgpQtd.add(wsaaPremium);
				wsaaAgpSgpYtd.add(wsaaPremium);
				wsaaAgpSgpQtdCnt.add(th566rec.cnt);
				wsaaAgpSgpYtdCnt.add(th566rec.cnt);
			}
			goTo(GotoLabel.nextZstrags2880a);
		}
		/*  If ZSTRAGS-EFFDATE is not in the current quarter, include the  */
		/*  premium in only the year to date amount.                       */
		if (isLT(zstragsIO.getEffdate(), wsaaCurrentQuarter)) {
			if (singlePremium.isTrue()) {
				compute(wsaaPremium, 2).set(add(zstragsIO.getSingp(), zstragsIO.getCntfee()));
				compCurr2900x();
				wsaaAgpSgpYtd.add(wsaaPremium);
				wsaaAgpSgpYtdCnt.add(th566rec.cnt);
			}
		}
	}

protected void nextZstrags2880a()
	{
		zstragsIO.setFunction(varcom.nextr);
	}

protected void computePercentages2900()
	{
		/*COMPUTE-PERCENT*/
		if (annualPremium.isTrue()) {
			compute(wsaaAgpMtdPrcAnn, 3).setRounded(div((mult(wsaaAgpAnnMtd, 100)), wsaaAnnMtd));
			compute(wsaaAgpQtdPrcAnn, 3).setRounded(div((mult(wsaaAgpAnnQtd, 100)), wsaaAnnQtd));
			compute(wsaaAgpYtdPrcAnn, 3).setRounded(div((mult(wsaaAgpAnnYtd, 100)), wsaaAnnYtd));
		}
		if (singlePremium.isTrue()) {
			compute(wsaaAgpMtdPrcSgp, 3).setRounded(div((mult(wsaaAgpSgpMtd, 100)), wsaaSgpMtd));
			compute(wsaaAgpQtdPrcSgp, 3).setRounded(div((mult(wsaaAgpSgpQtd, 100)), wsaaSgpQtd));
			compute(wsaaAgpYtdPrcSgp, 3).setRounded(div((mult(wsaaAgpSgpYtd, 100)), wsaaSgpYtd));
		}
		/*EXIT*/
	}

protected void detail3000p()
	{
		writeDetail3080p();
	}

protected void writeDetail3080p()
	{
		if (newPageReq.isTrue()) {
			wsaaFirstRead.set("Y");
			wsaaOverflow.set("Y");
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printHeader2600();
		}
		printerRecInner.printerRec.set(SPACES);
		getProduct3200();
		if (annualPremium.isTrue()) {
			writeProductAnnual3700();
			wsaaCnttype.set(zstragpIO.getCnttype());
		}
		if (singlePremium.isTrue()) {
			writeProductSingle3720();
			wsaaCnttype.set(zstragsIO.getCnttype());
		}
		resetProductAmount3300();
	}

protected void update3000()
	{
		/*EXIT*/
	}

protected void getProduct3200()
	{
		getProd3200();
	}

protected void getProd3200()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(wsaaCnttype);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		printerRecInner.cntdesc.set(descIO.getLongdesc());
	}

protected void resetProductAmount3300()
	{
		/*RESET-PROD-AMT*/
		wsaaAgpAnnMtd.set(ZERO);
		wsaaAgpAnnMtdCnt.set(ZERO);
		wsaaAgpAnnQtd.set(ZERO);
		wsaaAgpAnnQtdCnt.set(ZERO);
		wsaaAgpAnnYtd.set(ZERO);
		wsaaAgpAnnYtdCnt.set(ZERO);
		wsaaAgpMtdPrcAnn.set(ZERO);
		wsaaAgpQtdPrcAnn.set(ZERO);
		wsaaAgpYtdPrcAnn.set(ZERO);
		wsaaAgpSgpMtd.set(ZERO);
		wsaaAgpSgpMtdCnt.set(ZERO);
		wsaaAgpSgpQtd.set(ZERO);
		wsaaAgpSgpQtdCnt.set(ZERO);
		wsaaAgpSgpYtd.set(ZERO);
		wsaaAgpSgpYtdCnt.set(ZERO);
		wsaaAgpMtdPrcSgp.set(ZERO);
		wsaaAgpQtdPrcSgp.set(ZERO);
		wsaaAgpYtdPrcSgp.set(ZERO);
		/*EXIT*/
	}

protected void commit3500()
	{
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*EXIT*/
	}

protected void writeProductAnnual3700()
	{
		prodAnn3700();
	}

protected void prodAnn3700()
	{
		printerRecInner.amhii01.set(wsaaAgpAnnMtd);
		printerRecInner.amhii02.set(wsaaAgpAnnQtd);
		printerRecInner.amhii03.set(wsaaAgpAnnYtd);
		printerRecInner.znofpol01.set(wsaaAgpAnnMtdCnt);
		printerRecInner.znofpol02.set(wsaaAgpAnnQtdCnt);
		printerRecInner.znofpol03.set(wsaaAgpAnnYtdCnt);
		printerRecInner.prcnt01.set(wsaaAgpMtdPrcAnn);
		printerRecInner.prcnt02.set(wsaaAgpQtdPrcAnn);
		printerRecInner.prcnt03.set(wsaaAgpYtdPrcAnn);
		printerFile.printRh563d01(printerRecInner.printerRec);
	}

protected void writeTotalAnnual3710()
	{
		branchAnn3710();
	}

protected void branchAnn3710()
	{
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.amhii011.set(wsaaAnnMtd);
		printerRecInner.amhii021.set(wsaaAnnQtd);
		printerRecInner.amhii031.set(wsaaAnnYtd);
		printerRecInner.znofpol011.set(wsaaAnnCntMtd);
		printerRecInner.znofpol021.set(wsaaAnnCntQtd);
		printerRecInner.znofpol031.set(wsaaAnnCntYtd);
		printerRecInner.prcnt011.set(100);
		printerRecInner.prcnt021.set(100);
		printerRecInner.prcnt031.set(100);
		wsaaAnnMtd.set(ZERO);
		wsaaAnnQtd.set(ZERO);
		wsaaAnnYtd.set(ZERO);
		wsaaAnnCntMtd.set(ZERO);
		wsaaAnnCntQtd.set(ZERO);
		wsaaAnnCntYtd.set(ZERO);
		printerFile.printRh563t01(printerRecInner.printerRec);
	}

protected void writeProductSingle3720()
	{
		productSgl3720();
	}

protected void productSgl3720()
	{
		printerRecInner.amhii01.set(wsaaAgpSgpMtd);
		printerRecInner.amhii02.set(wsaaAgpSgpQtd);
		printerRecInner.amhii03.set(wsaaAgpSgpYtd);
		printerRecInner.znofpol01.set(wsaaAgpSgpMtdCnt);
		printerRecInner.znofpol02.set(wsaaAgpSgpQtdCnt);
		printerRecInner.znofpol03.set(wsaaAgpSgpYtdCnt);
		printerRecInner.prcnt01.set(wsaaAgpMtdPrcSgp);
		printerRecInner.prcnt02.set(wsaaAgpQtdPrcSgp);
		printerRecInner.prcnt03.set(wsaaAgpYtdPrcSgp);
		printerFile.printRh563d01(printerRecInner.printerRec);
	}

protected void writeTotalSingle3730()
	{
		totalSgl3730();
	}

protected void totalSgl3730()
	{
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.amhii011.set(wsaaSgpMtd);
		printerRecInner.amhii021.set(wsaaSgpQtd);
		printerRecInner.amhii031.set(wsaaSgpYtd);
		printerRecInner.znofpol011.set(wsaaSgpCntMtd);
		printerRecInner.znofpol021.set(wsaaSgpCntQtd);
		printerRecInner.znofpol031.set(wsaaSgpCntYtd);
		printerRecInner.prcnt011.set(100);
		printerRecInner.prcnt021.set(100);
		printerRecInner.prcnt031.set(100);
		printerFile.printRh563t01(printerRecInner.printerRec);
		wsaaSgpMtd.set(ZERO);
		wsaaSgpQtd.set(ZERO);
		wsaaSgpYtd.set(ZERO);
		wsaaSgpCntMtd.set(ZERO);
		wsaaSgpCntQtd.set(ZERO);
		wsaaSgpCntYtd.set(ZERO);
	}

protected void writeFooter3770()
	{
		/*FOOTER*/
		printerRecInner.printerRec.set(SPACES);
		printerFile.printRh563f01(printerRecInner.printerRec);
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void zstragtio2100z()
	{
		/*Z-STD*/
		SmartFileCode.execute(appVars, zstragtIO);
		if (isNE(zstragtIO.getStatuz(), varcom.oK)
		&& isNE(zstragtIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zstragtIO.getStatuz());
			syserrrec.params.set(zstragtIO.getParams());
			fatalError600();
		}
		/*Z-EXIT*/
	}

protected void keyChange2300z()
	{
		/*Z-KEY*/
		wsaaCompany.set(zstragtIO.getChdrcoy());
		wsaaBranch.set(zstragtIO.getCntbranch());
		wsaaAracde.set(zstragtIO.getAracde());
		wsaaAgntnum.set(zstragtIO.getAgntnum());
		wsaaEffdate.set(zstragtIO.getEffdate());
		/*Z-EXIT*/
	}

protected void compCurr2500x()
	{
		/*X-ZSTRAGT-CURR*/
		if (isNE(zstragtIO.getCntcurr(), th565rec.currcode)) {
			wsaaClnkCurrIn.set(zstragtIO.getCntcurr());
			wsaaClnkCompany.set(zstragtIO.getChdrcoy());
			convertPremium5000();
		}
		/*X-EXIT*/
	}

protected void compCurr2800x()
	{
		/*X-ZSTRAGP-CURR*/
		if (isNE(zstragpIO.getCntcurr(), th565rec.currcode)) {
			wsaaClnkCurrIn.set(zstragpIO.getCntcurr());
			wsaaClnkCompany.set(zstragpIO.getChdrcoy());
			convertPremium5000();
		}
		/*X-EXIT*/
	}

protected void zstragpio2800z()
	{
		/*Z-STD*/
		SmartFileCode.execute(appVars, zstragpIO);
		if (isNE(zstragpIO.getStatuz(), varcom.oK)
		&& isNE(zstragpIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zstragpIO.getStatuz());
			syserrrec.params.set(zstragpIO.getParams());
			fatalError600();
		}
		/*Z-EXIT*/
	}

protected void compCurr2900x()
	{
		/*X-ZSTRAGS-CURR*/
		if (isNE(zstragsIO.getCntcurr(), th565rec.currcode)) {
			wsaaClnkCurrIn.set(zstragsIO.getCntcurr());
			wsaaClnkCompany.set(zstragsIO.getChdrcoy());
			convertPremium5000();
		}
		/*X-EXIT*/
	}

protected void zstragsio2900z()
	{
		/*Z-STD*/
		SmartFileCode.execute(appVars, zstragsIO);
		if (isNE(zstragsIO.getStatuz(), varcom.oK)
		&& isNE(zstragsIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zstragsIO.getStatuz());
			syserrrec.params.set(zstragsIO.getParams());
			fatalError600();
		}
		/*Z-EXIT*/
	}

protected void convertPremium5000()
	{
		convertPrem5000();
	}

protected void convertPrem5000()
	{
		initialize(conlinkrec.clnk002Rec);
		conlinkrec.function.set("CVRT");
		conlinkrec.currIn.set(wsaaClnkCurrIn);
		conlinkrec.currOut.set(th565rec.currcode);
		conlinkrec.amountIn.set(wsaaPremium);
		conlinkrec.cashdate.set(wsaaOccdate);
		conlinkrec.company.set(wsaaClnkCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/* PERFORM 8000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			callRounding8000();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
		wsaaPremium.set(conlinkrec.amountOut);
	}

protected void readTh5665100()
	{
		th5665100();
	}

protected void th5665100()
	{
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th566);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th566rec.th566Rec.set(itemIO.getGenarea());
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(th565rec.currcode);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure PRINTER-REC--INNER
 */
private static final class PrinterRecInner { 

	private FixedLengthStringData printerRec = new FixedLengthStringData(139);
	private FixedLengthStringData rh563Record = new FixedLengthStringData(139).isAPartOf(printerRec, 0);
	private FixedLengthStringData rh563h01O = new FixedLengthStringData(139).isAPartOf(rh563Record, 0, REDEFINE);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22).isAPartOf(rh563h01O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rh563h01O, 22);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rh563h01O, 23);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(rh563h01O, 53);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(rh563h01O, 63);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(rh563h01O, 65);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(rh563h01O, 95);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30).isAPartOf(rh563h01O, 98);
	private FixedLengthStringData aracde = new FixedLengthStringData(3).isAPartOf(rh563h01O, 128);
	private FixedLengthStringData agntnum = new FixedLengthStringData(8).isAPartOf(rh563h01O, 131);
	private FixedLengthStringData rh563h02O = new FixedLengthStringData(1).isAPartOf(rh563Record, 0, REDEFINE);
	private FixedLengthStringData dummy = new FixedLengthStringData(1).isAPartOf(rh563h02O, 0);
	private FixedLengthStringData rh563h03O = new FixedLengthStringData(1).isAPartOf(rh563Record, 0, REDEFINE);
	private FixedLengthStringData dummy1 = new FixedLengthStringData(1).isAPartOf(rh563h03O, 0);
	private FixedLengthStringData rh563h04O = new FixedLengthStringData(10).isAPartOf(rh563Record, 0, REDEFINE);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(rh563h04O, 0);
	private FixedLengthStringData rh563d01O = new FixedLengthStringData(99).isAPartOf(rh563Record, 0, REDEFINE);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(rh563d01O, 0);
	private ZonedDecimalData amhii01 = new ZonedDecimalData(13, 2).isAPartOf(rh563d01O, 30);
	private ZonedDecimalData prcnt01 = new ZonedDecimalData(5, 2).isAPartOf(rh563d01O, 43);
	private ZonedDecimalData znofpol01 = new ZonedDecimalData(5, 0).isAPartOf(rh563d01O, 48);
	private ZonedDecimalData amhii02 = new ZonedDecimalData(13, 2).isAPartOf(rh563d01O, 53);
	private ZonedDecimalData prcnt02 = new ZonedDecimalData(5, 2).isAPartOf(rh563d01O, 66);
	private ZonedDecimalData znofpol02 = new ZonedDecimalData(5, 0).isAPartOf(rh563d01O, 71);
	private ZonedDecimalData amhii03 = new ZonedDecimalData(13, 2).isAPartOf(rh563d01O, 76);
	private ZonedDecimalData prcnt03 = new ZonedDecimalData(5, 2).isAPartOf(rh563d01O, 89);
	private ZonedDecimalData znofpol03 = new ZonedDecimalData(5, 0).isAPartOf(rh563d01O, 94);
	private FixedLengthStringData rh563t01O = new FixedLengthStringData(69).isAPartOf(rh563Record, 0, REDEFINE);
	private ZonedDecimalData amhii011 = new ZonedDecimalData(13, 2).isAPartOf(rh563t01O, 0);
	private ZonedDecimalData prcnt011 = new ZonedDecimalData(5, 2).isAPartOf(rh563t01O, 13);
	private ZonedDecimalData znofpol011 = new ZonedDecimalData(5, 0).isAPartOf(rh563t01O, 18);
	private ZonedDecimalData amhii021 = new ZonedDecimalData(13, 2).isAPartOf(rh563t01O, 23);
	private ZonedDecimalData prcnt021 = new ZonedDecimalData(5, 2).isAPartOf(rh563t01O, 36);
	private ZonedDecimalData znofpol021 = new ZonedDecimalData(5, 0).isAPartOf(rh563t01O, 41);
	private ZonedDecimalData amhii031 = new ZonedDecimalData(13, 2).isAPartOf(rh563t01O, 46);
	private ZonedDecimalData prcnt031 = new ZonedDecimalData(5, 2).isAPartOf(rh563t01O, 59);
	private ZonedDecimalData znofpol031 = new ZonedDecimalData(5, 0).isAPartOf(rh563t01O, 64);
	private FixedLengthStringData rh563f01O = new FixedLengthStringData(1).isAPartOf(rh563Record, 0, REDEFINE);
	private FixedLengthStringData dummy2 = new FixedLengthStringData(1).isAPartOf(rh563f01O, 0);
}
}
