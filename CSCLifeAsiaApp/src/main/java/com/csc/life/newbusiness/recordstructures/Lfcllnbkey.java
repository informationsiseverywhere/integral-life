package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:04
 * Description:
 * Copybook name: LFCLLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lfcllnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lfcllnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lfcllnbKey = new FixedLengthStringData(64).isAPartOf(lfcllnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData lfcllnbChdrcoy = new FixedLengthStringData(1).isAPartOf(lfcllnbKey, 0);
  	public FixedLengthStringData lfcllnbLifcnum = new FixedLengthStringData(8).isAPartOf(lfcllnbKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(lfcllnbKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lfcllnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lfcllnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}