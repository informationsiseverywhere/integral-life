/*
 * File: Bh604.java
 * Date: 29 August 2009 21:39:25
 * Author: Quipoz Limited
 *
 * Class transformed from BH604.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Ptrnxtrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.procedures.Zptrnxt;
import com.csc.life.newbusiness.reports.Rh604Report;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   This program selects policies which have had a terminating
*   action performed against them within the selected accounting
*   month/year, and summarises the Annual Premium, Single Premium
*   and Sum Assured.
*   The details are sorted and presented for the appropriate
*   Company under the following criteria :
*
*       Company
*          Transaction Code (Termination Reason)
*             Branch
*                Contract Type.
*
*   Earlier processes make use of the SMART batching programs
*   B0237 and B0236 to close and extract the relevant batches.
*   (See T1697 - Item BH604 for Termination Transaction codes).
*
*   The COBOL sort facility is utilised with input & output
*   procedures to order and output the details.
*
*   The sort criteria is as follows (all ascending) :
*
*   Transaction Code
*    Contract Servicing Branch
*     Contract Type.
*
*   The program has the following structure :
*
*   1000-INITIALISE.
*
*   2000-READ-FILE.
*
*   2500-EDIT.
*      2500A-SELECT             (Sort Input  Procedure)
*      2500B-PRINT-REPORT       (Sort Output Procedure)
*        2500B1-PRINT-DETAILS
*        2500B2-OUTPUT-TOTALS
*        2500B3-UPDATE-TOTALS
*        2500B4-OUTPUT-TRANCODE
*        2500B5-ALL-CNTTYPE
*
*   3000-UPDATE.
*
*   4000-CLOSE.
*                + + + + + + + + + + + + + + + + + + + +
*
*   1000-INITIALISE.
*   ----------------
*      Get the Company Description, date, etc and set
*      static details on the print report headings.
*      Read T5679 now to avoid multiple reads of the same item.
*
*   2000-READ-FILE.
*   ---------------
*      Set WSSP-EDTERROR flag to O-K.
*
*   2500-EDIT-FILE.
*   ---------------
*      Perform 2500A-SELECT
*         until WSSP-EDTERROR set to ENDP.
*      Perform 2500B-PRINT-REPORT.
*
*   2500A-SELECT.
*   -------------
*      Use routine ZPTRNXT to retreive next PTRN.
*
*      If returned statuz = ENDP
*         set WSSP-EDTERROR to ENDP and exit.
*
*      Read CHDRLNB using PTRN details.
*
*      Check contract status against T5679 to determine if
*      contract level processing will be applied.
*
*      Read TH506 using CHDRLNB values.
*
*      Read first COVRENQ record for this contract.
*
*      For each COVRENQ record for this contract :
*
*         If contract level processing :
*            When appropriate add the Sum Assured amount
*            to the Sort Record
*            Use the Billing Frequency to update the Premium
*            amount on the Sort Record from either the Single
*            Premium or Instalment Premium COVRENQ field
*         Else
*            Check the status and Premium status of the
*            Coverage/Rider against T5679
*            If valid
*               add the Sum Assured amount to the Sort Record,
*               update the Premium amount on the Sort Record
*               from either the Single or Instalment Premium
*               field on the Coverage
*            End-If
*         End-If
*         Read next COVRENQ
*      End-For.
*
*      Exit if the PTRN has had no impact, (Sort Record
*      amounts will still be zero).
*
*      When not a Single Premium, multiply Sort Record
*      Premium amount by the Billing Frequency on
*      the CHDRLNB record.
*
*      For contract level processing, add any fees to the
*      Sort Record Premium amount
*
*      Convert the Sort Record Premium amount to the report
*      requested currency, when this differs from the
*      Contract currency.
*
*      Set the remaining values on the sort Record.
*
*      Release the Sort Record.
*
*      Add the amounts to the Working Store arrays for
*      first, the Branch total for this Transaction Code,
*      and then the Transaction Code grand total.
*
*   2500B-PRINT-REPORT
*   ------------------
*     Open the output file.
*
*     Return Sort record
*        on file end set WSAA-EOF = 'Y'
*
*     Perform 2500B1-PRINT-DETAILS
*        until end of file
*
*     If the file was not empty
*        Perform 2500B2-OUTPUT-DETAILS (last record details)
*     Else
*        Output Page Headings
*     End-If.
*
*     Output End of Report Banner.
*
*   2500B1A-PRINT-DETAILS
*   ---------------------
*     When there is a change in the Branch or Transaction Code
*        Perform 2500B2-OUTPUT-TOTALS.
*
*     If the Contract Type has changed
*        Perform 2500B3-UPDATE-TOTALS
*        Output a detail record (Contract Type values)
*        Reset totals.
*
*     Increment the relevant amounts on the detail record
*     depending on the Sort Record Billing Frequency.
*
*     Return the next sort Record
*        Set WSAA-EOF if the end has been reached.
*
*   2500B2-OUTPUT-DETAILS
*   ---------------------
*     When this is not the first Sort Record
*        Perform 2500B3-UPDATE-TOTALS
*        Output a detail record (Contract Type values)
*
*        Update the total record with the Branch total
*        values and output these details
*
*        If the Transaction Code has changed or
*           the end of the file has been reached
*           Perform 2500B4-OUTPUT-TRANCODE.
*
*     Retain Contract Type, Branch & Transaction Code values.
*
*     If the end of the file has not been reached
*        Get the Branch & Transaction Code Description
*        Output Page headings
*        Reset totals.
*
*   2500B3-UPDATE-TOTALS
*   --------------------
*     Locate and add current Branch Contract Type totals
*     to Transaction Code Contract Type totals.
*
*     Retrieve Branch total to calculate & update detail
*     record percentage values for this Contract Type within
*     the Branch.
*
*   2500B4-OUTPUT-TRANCODE
*   ----------------------
*     Locate Transaction Code grand totals.
*     Output Page Headings.
*     Perform 2500B5-ALL-CNTTYPE
*        until all Contract Types have been processed.
*
*     Update the total record with the Transaction Code
*     grand total values and output these details.
*
*   2500B5-ALL-CNTTYPE
*   ------------------
*   Locate each of the Contract Type totals for this
*   Transaction Code, for each one :
*
*      update detail record from Contract Type totals,
*      calculate & update detail record percentage values
*      for this Contract Type within all Contract Types
*      for this Transaction code
*
*      Output a detail record (Contract Type total within
*      the Transaction code).
*
*   3000-UPDATE.
*   ------------
*      No updates are performed.
*
*   4000-CLOSE.
*   -----------
*      Close the output file.
*
*****************************************************************
* </pre>
*/
public class Bh604 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rh604Report printerFile = new Rh604Report();
	private SortFileDAM termSortFile = new SortFileDAM("LU00");
	private FixedLengthStringData printerRecord = new FixedLengthStringData(160);

	private FixedLengthStringData termSortRec = new FixedLengthStringData(33);
	private FixedLengthStringData termSortKey = new FixedLengthStringData(9).isAPartOf(termSortRec, 0);
	private FixedLengthStringData termSortTrancode = new FixedLengthStringData(4).isAPartOf(termSortKey, 0);
	private FixedLengthStringData termSortCntbranch = new FixedLengthStringData(2).isAPartOf(termSortKey, 4);
	private FixedLengthStringData termSortCnttype = new FixedLengthStringData(3).isAPartOf(termSortKey, 6);
	private FixedLengthStringData termSortOtherData = new FixedLengthStringData(24).isAPartOf(termSortRec, 9);
	private FixedLengthStringData termSortBillfreq = new FixedLengthStringData(2).isAPartOf(termSortOtherData, 0);
	private ZonedDecimalData termSortSumins = new ZonedDecimalData(11, 2).isAPartOf(termSortOtherData, 2).setUnsigned();
	// length increased from (9,2) to (11,2) for ticket ILIFE-3000
	private ZonedDecimalData termSortPrem = new ZonedDecimalData(11, 2).isAPartOf(termSortOtherData, 13).setUnsigned();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH604");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaDateText = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaMthYr = new FixedLengthStringData(14).isAPartOf(wsaaDateText, 5);

	private FixedLengthStringData wsaaDateTexc = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaHanYrMth = new FixedLengthStringData(16).isAPartOf(wsaaDateTexc, 0);
	private String wsaaCntLvl = "";
	private String wsaaRiderStatFnd = "";
	private String wsaaRiderPstatFnd = "";
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaBillfreqRed = new FixedLengthStringData(2).isAPartOf(wsaaBillfreq, 0, REDEFINE);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreqRed, 0).setUnsigned();
	private FixedLengthStringData wsaaPrevCnttype = new FixedLengthStringData(3).init(SPACES);
	private FixedLengthStringData wsaaPrevCntbranch = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaPrevTrancode = new FixedLengthStringData(4).init(SPACES);
	private IntegerData wsaaTcdeTotIx = new IntegerData();
	private PackedDecimalData wsaaTcdeSpPct = new PackedDecimalData(5, 2).setUnsigned();
	private PackedDecimalData wsaaTcdeApPct = new PackedDecimalData(5, 2).setUnsigned();
	private PackedDecimalData wsaaTcdeSpPolcnt = new PackedDecimalData(5, 0).setUnsigned();
	private PackedDecimalData wsaaTcdeApPolcnt = new PackedDecimalData(5, 0).setUnsigned();

	private FixedLengthStringData wsaaBchSrchKey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaBchSrchCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaBchSrchKey, 0);
	private FixedLengthStringData wsaaBchSrchTrancode = new FixedLengthStringData(4).isAPartOf(wsaaBchSrchKey, 2);

	private FixedLengthStringData wsaaTcdeSrchKey = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTcdeSrchTrancode = new FixedLengthStringData(4).isAPartOf(wsaaTcdeSrchKey, 0);
	private FixedLengthStringData wsaaTcdeSrchCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTcdeSrchKey, 4);
	private FixedLengthStringData wsaaSrchEnd = new FixedLengthStringData(1);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final String bscdrec = "BSCDREC";
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String covrenqrec = "COVRENQREC";
	private static final String itemrec = "ITEMREC";
	private static final String descrec = "DESCREC";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String t5679 = "T5679";
	private static final String th506 = "TH506";
	private static final String th565 = "TH565";
		/* ERRORS */
	private static final String i006 = "I006";
	private static final String h951 = "H951";
	private static final String hl59 = "HL59";
	private static final String hl60 = "HL60";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;

	private FixedLengthStringData wsaaFirsttime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirsttime, "Y");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rh604H01 = new FixedLengthStringData(155);
	private FixedLengthStringData rh604h01O = new FixedLengthStringData(155).isAPartOf(rh604H01, 0);
	private FixedLengthStringData rh01Datetext = new FixedLengthStringData(19).isAPartOf(rh604h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rh604h01O, 19);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rh604h01O, 20);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rh604h01O, 50);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rh604h01O, 60);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rh604h01O, 62);
	private FixedLengthStringData rh01TrcdeDesc = new FixedLengthStringData(30).isAPartOf(rh604h01O, 92);
	private FixedLengthStringData rh01Currcode = new FixedLengthStringData(3).isAPartOf(rh604h01O, 122);
	private FixedLengthStringData rh01Currencynm = new FixedLengthStringData(30).isAPartOf(rh604h01O, 125);

	private FixedLengthStringData rh604H02 = new FixedLengthStringData(1);

	private FixedLengthStringData rh604D01 = new FixedLengthStringData(79);
	private FixedLengthStringData rh604d01O = new FixedLengthStringData(79).isAPartOf(rh604D01, 0);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(rh604d01O, 0);
	private ZonedDecimalData rd01Ap = new ZonedDecimalData(14, 2).isAPartOf(rh604d01O, 3);
	private ZonedDecimalData rd01ApSa = new ZonedDecimalData(14, 2).isAPartOf(rh604d01O, 17);
	private ZonedDecimalData rd01ApPolcnt = new ZonedDecimalData(5, 0).isAPartOf(rh604d01O, 31);
	private ZonedDecimalData rd01ApPct = new ZonedDecimalData(5, 2).isAPartOf(rh604d01O, 36);
	private ZonedDecimalData rd01Sp = new ZonedDecimalData(14, 2).isAPartOf(rh604d01O, 41);
	private ZonedDecimalData rd01SpSa = new ZonedDecimalData(14, 2).isAPartOf(rh604d01O, 55);
	private ZonedDecimalData rd01SpPolcnt = new ZonedDecimalData(5, 0).isAPartOf(rh604d01O, 69);
	private ZonedDecimalData rd01SpPct = new ZonedDecimalData(5, 2).isAPartOf(rh604d01O, 74);

	private FixedLengthStringData rh604T01 = new FixedLengthStringData(76);
	private FixedLengthStringData rh604t01O = new FixedLengthStringData(76).isAPartOf(rh604T01, 0);
	private ZonedDecimalData rt01Ap = new ZonedDecimalData(14, 2).isAPartOf(rh604t01O, 0);
	private ZonedDecimalData rt01ApSa = new ZonedDecimalData(14, 2).isAPartOf(rh604t01O, 14);
	private ZonedDecimalData rt01ApPolcnt = new ZonedDecimalData(5, 0).isAPartOf(rh604t01O, 28);
	private ZonedDecimalData rt01ApPct = new ZonedDecimalData(5, 2).isAPartOf(rh604t01O, 33);
	private ZonedDecimalData rt01Sp = new ZonedDecimalData(14, 2).isAPartOf(rh604t01O, 38);
	private ZonedDecimalData rt01SpSa = new ZonedDecimalData(14, 2).isAPartOf(rh604t01O, 52);
	private ZonedDecimalData rt01SpPolcnt = new ZonedDecimalData(5, 0).isAPartOf(rh604t01O, 66);
	private ZonedDecimalData rt01SpPct = new ZonedDecimalData(5, 2).isAPartOf(rh604t01O, 71);

	private FixedLengthStringData rh604F01 = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaBchIx = new IntegerData();
	private IntegerData wsaaTcdeIx = new IntegerData();
	private BscdTableDAM bscdIO = new BscdTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Ptrnxtrec ptrnxtrec = new Ptrnxtrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5679rec t5679rec = new T5679rec();
	private Th506rec th506rec = new Th506rec();
	private Th565rec th565rec = new Th565rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private WsaaBchTotalsArrayInner wsaaBchTotalsArrayInner = new WsaaBchTotalsArrayInner();
	private WsaaTcdeTotalsArrayInner wsaaTcdeTotalsArrayInner = new WsaaTcdeTotalsArrayInner();

	public Bh604() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		setUpHeadingCompany1010();
		setUpHeadingDates1040();
		setUpHeadingCurrency1050();
	}

protected void setUpHeadingCompany1010()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon6rec.language.set(bsscIO.getLanguage());
		datcon6rec.company.set(bsprIO.getCompany());
		datcon6rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon6rec.statuz);
			syserrrec.params.set(datcon6rec.datcon6Rec);
			fatalError600();
		}
		if (isEQ(datcon6rec.language,"S")
		|| isEQ(datcon6rec.language,"C")) {
			wsaaDateTexc.set(datcon6rec.intDate2);
			rh01Datetext.set(wsaaHanYrMth);
		}
		else {
			wsaaDateText.set(datcon6rec.intDate2);
			rh01Datetext.set(wsaaMthYr);
		}
	}

protected void setUpHeadingCurrency1050()
	{
		/* Use Batch Schedule Authority Code to read TH565.                */
		bscdIO.setScheduleName(bsscIO.getScheduleName());
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(bscdIO.getStatuz());
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		/*-- Read TH565                                                    */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th565);
		itemIO.setItemitem(bscdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th565rec.th565Rec.set(itemIO.getGenarea());
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		/* MOVE BPRD-SYSTEM-PARAM03    TO DESC-DESCITEM.                */
		descIO.setDescitem(th565rec.currcode);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/* MOVE BPRD-SYSTEM-PARAM03    TO RH01-CURRCODE.                */
		rh01Currcode.set(th565rec.currcode);
		rh01Currencynm.set(descIO.getLongdesc());
		/* Use Batch Schedule Authority Code to read T5679.*/
		bscdIO.setScheduleName(bsscIO.getScheduleName());
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(bscdIO.getStatuz());
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bscdIO.getAuthCode());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		/*  As this program uses the COBOL Sort facility, all*/
		/*  processing is performed within the 2500 Section.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		termSortFile.openOutput();
		select2500a();
		termSortFile.close();
		FileSort fs1 = new FileSort(termSortFile);
		fs1.addSortKey(termSortKey, true);
		fs1.sort();
		termSortFile.openInput();
		printReport2500b();
		termSortFile.close();
		/*EXIT*/
	}

protected void select2500a()
	{
		/*A-START*/
		while ( !(isEQ(wsspEdterror,varcom.endp))) {
			sortInput2500a1();
		}

		/*A-EXIT*/
	}

protected void sortInput2500a1()
	{
			start2500a1();
		}

protected void start2500a1()
	{
		/* Call ZPTRNXT for next Terminated Transaction.*/
		ptrnxtrec.ptrnxtRec.set(SPACES);
		callProgram(Zptrnxt.class, ptrnxtrec.ptrnxtRec, lsaaBsscrec, lsaaBprdrec);
		if (isNE(ptrnxtrec.statuz,varcom.oK)
		&& isNE(ptrnxtrec.statuz,varcom.endp)) {
			syserrrec.statuz.set(hl59);
			syserrrec.params.set(ptrnxtrec.ptrnxtRec);
			fatalError600();
		}
		if (isEQ(ptrnxtrec.statuz,varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		/*  Move PTNX-PTRN-DATA return from ZPTRNXT to PTRN copybook.*/
		ptrnIO.setDataArea(ptrnxtrec.ptrnData);
		/*  Skip this record if it is no longer valid (valid flag  = '2')*/
		if (isEQ(ptrnIO.getValidflag(),"2")) {
			return ;
		}
		chdrlnbIO.setChdrcoy(bsprIO.getCompany());
		chdrlnbIO.setChdrnum(ptrnIO.getChdrnum());
		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* See if the Termination was performed at Contract Level*/
		wsaaCntLvl = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()],chdrlnbIO.getStatcode())) {
				wsaaCntLvl = "Y";
				wsaaSub.set(12);
			}
		}
		initialize(termSortRec);
		/* Read TH506 for basic plan.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th506);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(hl60);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covrenqIO.setChdrnum(chdrlnbIO.getChdrnum());
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrenqIO.getStatuz());
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		while ( !(isNE(covrenqIO.getChdrnum(),ptrnIO.getChdrnum())
		|| isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			if (isEQ(wsaaCntLvl,"Y")) {
				if (isEQ(covrenqIO.getCrtable(),th506rec.crtable)
				|| (isEQ(th506rec.crtable,SPACES)
				&& isEQ(covrenqIO.getRider(),"00"))) {
					termSortSumins.add(covrenqIO.getSumins());
				}
				if (isEQ(chdrlnbIO.getBillfreq(),"00")) {
					termSortPrem.add(covrenqIO.getSingp());
				}
				else {
					termSortPrem.add(covrenqIO.getInstprem());
				}
			}
			else {
				wsaaRiderStatFnd = "N";
				wsaaRiderPstatFnd = "N";
				for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){
					if (isEQ(t5679rec.ridRiskStat[wsaaSub.toInt()],covrenqIO.getStatcode())) {
						wsaaRiderStatFnd = "Y";
					}
					if (isEQ(t5679rec.ridPremStat[wsaaSub.toInt()],covrenqIO.getPstatcode())) {
						wsaaRiderPstatFnd = "Y";
					}
				}
				if (isEQ(wsaaRiderStatFnd,"Y")
				&& isEQ(wsaaRiderPstatFnd,"Y")) {
					termSortSumins.add(covrenqIO.getSumins());
					if (isEQ(chdrlnbIO.getBillfreq(),"00")) {
						termSortPrem.add(covrenqIO.getSingp());
					}
					else {
						termSortPrem.add(covrenqIO.getInstprem());
					}
				}
			}
			covrenqIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, covrenqIO);
			if (isNE(covrenqIO.getStatuz(),varcom.oK)
			&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
				syserrrec.statuz.set(covrenqIO.getStatuz());
				syserrrec.params.set(covrenqIO.getParams());
				fatalError600();
			}
		}

		/* Ignore if the Policy is unaffected, (i.e. Lapse*/
		/* Transaction written within Overdue Processing but the*/
		/* policy was not lapsed).*/
		if (isEQ(termSortPrem,ZERO)) {
			return ;
		}
		/* If we get here we have a valid Termination Transaction.*/
		/* Output a sort record & increment the correct Branch and*/
		/* Transaction Code Totals.*/
		wsaaBillfreq.set(chdrlnbIO.getBillfreq());
		if (isNE(chdrlnbIO.getBillfreq(),"00")) {
			compute(termSortPrem, 2).set(mult(termSortPrem,wsaaBillfreq9));
		}
		if (isEQ(wsaaCntLvl,"Y")) {
			termSortPrem.add(chdrlnbIO.getSinstamt02());
		}
		/* IF CHDRLNB-CNTCURR       NOT = BPRD-SYSTEM-PARAM03           */
		if (isNE(chdrlnbIO.getCntcurr(), th565rec.currcode)) {
			initialize(conlinkrec.clnk002Rec);
			conlinkrec.company.set(chdrlnbIO.getChdrcoy());
			conlinkrec.currIn.set(chdrlnbIO.getCntcurr());
			/*    MOVE BPRD-SYSTEM-PARAM03 TO CLNK-CURR-OUT                 */
			conlinkrec.currOut.set(th565rec.currcode);
			conlinkrec.amountIn.set(termSortPrem);
			conlinkrec.cashdate.set(ptrnIO.getPtrneff());
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.function.set("SURR");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(conlinkrec.statuz);
				syserrrec.params.set(conlinkrec.clnk002Rec);
				fatalError600();
			}
			if (isNE(conlinkrec.amountOut, 0)) {
				zrdecplrec.amountIn.set(conlinkrec.amountOut);
				a000CallRounding();
				conlinkrec.amountOut.set(zrdecplrec.amountOut);
			}
			termSortPrem.set(conlinkrec.amountOut);
		}
		termSortTrancode.set(ptrnIO.getBatctrcde());
		termSortCntbranch.set(chdrlnbIO.getCntbranch());
		termSortCnttype.set(chdrlnbIO.getCnttype());
		termSortBillfreq.set(chdrlnbIO.getBillfreq());
		termSortFile.write(termSortRec);
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/* Search the Branch totals array, adding the key if it does*/
		/* not exist, before incrementing the relevant totals.*/
		wsaaBchSrchCntbranch.set(chdrlnbIO.getCntbranch());
		wsaaBchSrchTrancode.set(ptrnIO.getBatctrcde());
		wsaaBchIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaBchIx, wsaaBchTotalsArrayInner.wsaaBchTotals.length); wsaaBchIx.add(1)){
				if (isEQ(wsaaBchTotalsArrayInner.wsaaBchKey[wsaaBchIx.toInt()], wsaaBchSrchKey)) {
					break searchlabel1;
				}
				if (isEQ(wsaaBchTotalsArrayInner.wsaaBchKey[wsaaBchIx.toInt()], HIVALUE)) {
					wsaaBchTotalsArrayInner.wsaaBchKey[wsaaBchIx.toInt()].set(wsaaBchSrchKey);
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(i006);
			syserrrec.params.set(wsaaBchSrchKey);
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getBillfreq(),"00")) {
			wsaaBchTotalsArrayInner.wsaaBchSingPrem[wsaaBchIx.toInt()].add(termSortPrem);
			wsaaBchTotalsArrayInner.wsaaBchSingSumins[wsaaBchIx.toInt()].add(termSortSumins);
			wsaaBchTotalsArrayInner.wsaaBchSingCount[wsaaBchIx.toInt()].add(1);
		}
		else {
			wsaaBchTotalsArrayInner.wsaaBchAnnPrem[wsaaBchIx.toInt()].add(termSortPrem);
			wsaaBchTotalsArrayInner.wsaaBchAnnSumins[wsaaBchIx.toInt()].add(termSortSumins);
			wsaaBchTotalsArrayInner.wsaaBchAnnCount[wsaaBchIx.toInt()].add(1);
		}
		/* Perfrom a similar search on the Transaction Code array,*/
		/* incrementing the relevant totals.*/
		/* CNTTYPE '***' = Total of all Contract Types for this*/
		/*                 Transaction Code.*/
		wsaaTcdeSrchTrancode.set(ptrnIO.getBatctrcde());
		wsaaTcdeSrchCnttype.set("***");
		wsaaTcdeIx.set(1);
		 searchlabel2:
		{
			for (; isLT(wsaaTcdeIx, wsaaTcdeTotalsArrayInner.wsaaTcdeTotals.length); wsaaTcdeIx.add(1)){
				if (isEQ(wsaaTcdeTotalsArrayInner.wsaaTcdeKey[wsaaTcdeIx.toInt()], wsaaTcdeSrchKey)) {
					break searchlabel2;
				}
				if (isEQ(wsaaTcdeTotalsArrayInner.wsaaTcdeKey[wsaaTcdeIx.toInt()], HIVALUE)) {
					wsaaTcdeTotalsArrayInner.wsaaTcdeKey[wsaaTcdeIx.toInt()].set(wsaaTcdeSrchKey);
					break searchlabel2;
				}
			}
			syserrrec.statuz.set(i006);
			syserrrec.params.set(wsaaTcdeSrchKey);
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getBillfreq(),"00")) {
			wsaaTcdeTotalsArrayInner.wsaaTcdeSingPrem[wsaaTcdeIx.toInt()].add(termSortPrem);
			wsaaTcdeTotalsArrayInner.wsaaTcdeSingSumins[wsaaTcdeIx.toInt()].add(termSortSumins);
			wsaaTcdeTotalsArrayInner.wsaaTcdeSingCount[wsaaTcdeIx.toInt()].add(1);
		}
		else {
			wsaaTcdeTotalsArrayInner.wsaaTcdeAnnPrem[wsaaTcdeIx.toInt()].add(termSortPrem);
			wsaaTcdeTotalsArrayInner.wsaaTcdeAnnSumins[wsaaTcdeIx.toInt()].add(termSortSumins);
			wsaaTcdeTotalsArrayInner.wsaaTcdeAnnCount[wsaaTcdeIx.toInt()].add(1);
		}
	}

protected void printReport2500b()
	{
		/*B-START*/
		printerFile.openOutput();
		termSortFile.read(termSortRec);
		if (termSortFile.isAtEnd()) {
			wsaaEof.set("Y");
		}
		while ( !(endOfFile.isTrue())) {
			printDetails2500b1();
		}

		if (!firstTime.isTrue()) {
			outputTotals2500b2();
		}
		else {
			printerFile.printRh604h01(rh604H01, indicArea);
			printerFile.printRh604h02(rh604H02, indicArea);
		}
		printerFile.printRh604f01(rh604F01, indicArea);
		/*B-EXIT*/
	}

protected void printDetails2500b1()
	{
		start2500b1();
	}

protected void start2500b1()
	{
		if (isNE(wsaaPrevTrancode,termSortTrancode)
		|| isNE(wsaaPrevCntbranch,termSortCntbranch)) {
			outputTotals2500b2();
		}
		/* Output Contract Type totals, if only the Contract Type*/
		/* has changed.*/
		if (isNE(wsaaPrevCnttype,termSortCnttype)) {
			updateTotals2500b3();
			printerFile.printRh604d01(rh604D01, indicArea);
			initialize(rh604D01);
			wsaaPrevCnttype.set(termSortCnttype);
			rd01Cnttype.set(termSortCnttype);
		}
		/* Accumulate current Contract Type values.*/
		if (isEQ(termSortBillfreq,"00")) {
			rd01Sp.add(termSortPrem);
			rd01SpSa.add(termSortSumins);
			rd01SpPolcnt.add(1);
		}
		else {
			rd01Ap.add(termSortPrem);
			rd01ApSa.add(termSortSumins);
			rd01ApPolcnt.add(1);
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		termSortFile.read(termSortRec);
		if (termSortFile.isAtEnd()) {
			wsaaEof.set("Y");
		}
	}

protected void outputTotals2500b2()
	{
		start2500b2();
	}

protected void start2500b2()
	{
		if (firstTime.isTrue()) {
			wsaaFirsttime.set("N");
		}
		else {
			updateTotals2500b3();
			/* Output last Contract Type details for this Branch.*/
			printerFile.printRh604d01(rh604D01, indicArea);
			/* Now output Branch Totals.*/
			rt01Ap.set(wsaaBchTotalsArrayInner.wsaaBchAnnPrem[wsaaBchIx.toInt()]);
			rt01ApSa.set(wsaaBchTotalsArrayInner.wsaaBchAnnSumins[wsaaBchIx.toInt()]);
			rt01ApPolcnt.set(wsaaBchTotalsArrayInner.wsaaBchAnnCount[wsaaBchIx.toInt()]);
			rt01ApPct.set(wsaaBchTotalsArrayInner.wsaaBchAnnPct[wsaaBchIx.toInt()]);
			rt01Sp.set(wsaaBchTotalsArrayInner.wsaaBchSingPrem[wsaaBchIx.toInt()]);
			rt01SpSa.set(wsaaBchTotalsArrayInner.wsaaBchSingSumins[wsaaBchIx.toInt()]);
			rt01SpPolcnt.set(wsaaBchTotalsArrayInner.wsaaBchSingCount[wsaaBchIx.toInt()]);
			rt01SpPct.set(wsaaBchTotalsArrayInner.wsaaBchSingPct[wsaaBchIx.toInt()]);
			printerFile.printRh604t01(rh604T01, indicArea);
			/* Output Transaction Code totals if this has changed.*/
			if (isNE(wsaaPrevTrancode,termSortTrancode)
			|| endOfFile.isTrue()) {
				outputTrancode2500b4();
			}
		}
		wsaaPrevCnttype.set(termSortCnttype);
		wsaaPrevCntbranch.set(termSortCntbranch);
		wsaaPrevTrancode.set(termSortTrancode);
		/* Get the Branch & Transaction Code description before*/
		/* outputing the page heading.*/
		if (!endOfFile.isTrue()) {
			descIO.setDescpfx(smtpfxcpy.item);
			descIO.setDesccoy(bsprIO.getCompany());
			descIO.setDesctabl(t1692);
			descIO.setDescitem(termSortCntbranch);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(bsscIO.getLanguage());
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(descIO.getStatuz());
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			rh01Branch.set(termSortCntbranch);
			rh01Branchnm.set(descIO.getLongdesc());
			descIO.setDescpfx(smtpfxcpy.item);
			descIO.setDesccoy(bsprIO.getCompany());
			descIO.setDesctabl(t1688);
			descIO.setDescitem(termSortTrancode);
			descIO.setItemseq(SPACES);
			descIO.setLanguage(bsscIO.getLanguage());
			descIO.setFunction(varcom.readr);
			descIO.setFormat(descrec);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(descIO.getStatuz());
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
			rh01TrcdeDesc.set(descIO.getLongdesc());
			printerFile.printRh604h01(rh604H01, indicArea);
			printerFile.printRh604h02(rh604H02, indicArea);
			initialize(rh604D01);
			rd01Cnttype.set(termSortCnttype);
		}
	}

protected void updateTotals2500b3()
	{
		start2500b3();
	}

protected void start2500b3()
	{
		/* Update the Transaction Code array for this Contract Type,*/
		/* adding the key if this is the first occurrance.*/
		wsaaTcdeSrchTrancode.set(wsaaPrevTrancode);
		wsaaTcdeSrchCnttype.set(wsaaPrevCnttype);
		wsaaTcdeIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaTcdeIx, wsaaTcdeTotalsArrayInner.wsaaTcdeTotals.length); wsaaTcdeIx.add(1)){
				if (isEQ(wsaaTcdeTotalsArrayInner.wsaaTcdeKey[wsaaTcdeIx.toInt()], wsaaTcdeSrchKey)) {
					break searchlabel1;
				}
				if (isEQ(wsaaTcdeTotalsArrayInner.wsaaTcdeKey[wsaaTcdeIx.toInt()], HIVALUE)) {
					wsaaTcdeTotalsArrayInner.wsaaTcdeKey[wsaaTcdeIx.toInt()].set(wsaaTcdeSrchKey);
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(i006);
			syserrrec.params.set(wsaaTcdeSrchKey);
			fatalError600();
		}
		wsaaTcdeTotalsArrayInner.wsaaTcdeAnnPrem[wsaaTcdeIx.toInt()].add(rd01Ap);
		wsaaTcdeTotalsArrayInner.wsaaTcdeAnnSumins[wsaaTcdeIx.toInt()].add(rd01ApSa);
		wsaaTcdeTotalsArrayInner.wsaaTcdeAnnCount[wsaaTcdeIx.toInt()].add(rd01ApPolcnt);
		wsaaTcdeTotalsArrayInner.wsaaTcdeSingPrem[wsaaTcdeIx.toInt()].add(rd01Sp);
		wsaaTcdeTotalsArrayInner.wsaaTcdeSingSumins[wsaaTcdeIx.toInt()].add(rd01SpSa);
		wsaaTcdeTotalsArrayInner.wsaaTcdeSingCount[wsaaTcdeIx.toInt()].add(rd01SpPolcnt);
		/* Get the Branch total and update the Percentage values.*/
		wsaaBchSrchCntbranch.set(wsaaPrevCntbranch);
		wsaaBchSrchTrancode.set(wsaaPrevTrancode);
		wsaaBchIx.set(1);
		 searchlabel2:
		{
			for (; isLT(wsaaBchIx, wsaaBchTotalsArrayInner.wsaaBchTotals.length); wsaaBchIx.add(1)){
				if (isEQ(wsaaBchTotalsArrayInner.wsaaBchKey[wsaaBchIx.toInt()], wsaaBchSrchKey)) {
					break searchlabel2;
				}
				if (isEQ(wsaaBchTotalsArrayInner.wsaaBchKey[wsaaBchIx.toInt()], HIVALUE)) {
					syserrrec.statuz.set(h951);
					syserrrec.params.set(wsaaBchSrchKey);
					fatalError600();
					break searchlabel2;
				}
			}
			syserrrec.statuz.set(h951);
			syserrrec.params.set(wsaaBchSrchKey);
			fatalError600();
		}
		compute(rd01ApPct, 3).setRounded((mult((div(rd01ApPolcnt, wsaaBchTotalsArrayInner.wsaaBchAnnCount[wsaaBchIx.toInt()])), 100)));
		compute(rd01SpPct, 3).setRounded((mult((div(rd01SpPolcnt, wsaaBchTotalsArrayInner.wsaaBchSingCount[wsaaBchIx.toInt()])), 100)));
		wsaaBchTotalsArrayInner.wsaaBchAnnPct[wsaaBchIx.toInt()].add(rd01ApPct);
		wsaaBchTotalsArrayInner.wsaaBchSingPct[wsaaBchIx.toInt()].add(rd01SpPct);
	}

protected void outputTrancode2500b4()
	{
		start2500b4();
	}

protected void start2500b4()
	{
		/* Locate and output the grand total for this Transaction Code.*/
		wsaaTcdeSrchTrancode.set(wsaaPrevTrancode);
		wsaaTcdeSrchCnttype.set("***");
		wsaaTcdeIx.set(1);
		 searchlabel1:
		{
			for (; isLT(wsaaTcdeIx, wsaaTcdeTotalsArrayInner.wsaaTcdeTotals.length); wsaaTcdeIx.add(1)){
				if (isEQ(wsaaTcdeTotalsArrayInner.wsaaTcdeKey[wsaaTcdeIx.toInt()], wsaaTcdeSrchKey)) {
					wsaaTcdeApPolcnt.set(wsaaTcdeTotalsArrayInner.wsaaTcdeAnnCount[wsaaTcdeIx.toInt()]);
					wsaaTcdeSpPolcnt.set(wsaaTcdeTotalsArrayInner.wsaaTcdeSingCount[wsaaTcdeIx.toInt()]);
					break searchlabel1;
				}
				if (isEQ(wsaaTcdeTotalsArrayInner.wsaaTcdeKey[wsaaTcdeIx.toInt()], HIVALUE)) {
					syserrrec.statuz.set(h951);
					syserrrec.params.set(wsaaTcdeSrchKey);
					fatalError600();
					break searchlabel1;
				}
			}
			syserrrec.statuz.set(h951);
			syserrrec.params.set(wsaaTcdeSrchKey);
			fatalError600();
		}
		wsaaTcdeTotIx.set(wsaaTcdeIx);
		wsaaTcdeSpPct.set(ZERO);
		wsaaTcdeApPct.set(ZERO);
		rh01Branch.set("**");
		rh01Branchnm.set(SPACES);
		printerFile.printRh604h01(rh604H01, indicArea);
		printerFile.printRh604h02(rh604H02, indicArea);
		wsaaTcdeSrchCnttype.set(SPACES);
		wsaaSrchEnd.set(SPACES);
		wsaaTcdeIx.set(1);
		while ( !(isEQ(wsaaSrchEnd,"Y"))) {
			allCnttype2500b5();
		}

		/* Finally output the grand total for this Transaction Code.*/
		wsaaTcdeIx.set(wsaaTcdeTotIx);
		rt01Ap.set(wsaaTcdeTotalsArrayInner.wsaaTcdeAnnPrem[wsaaTcdeIx.toInt()]);
		rt01ApSa.set(wsaaTcdeTotalsArrayInner.wsaaTcdeAnnSumins[wsaaTcdeIx.toInt()]);
		rt01ApPolcnt.set(wsaaTcdeTotalsArrayInner.wsaaTcdeAnnCount[wsaaTcdeIx.toInt()]);
		rt01ApPct.set(wsaaTcdeApPct);
		rt01Sp.set(wsaaTcdeTotalsArrayInner.wsaaTcdeSingPrem[wsaaTcdeIx.toInt()]);
		rt01SpSa.set(wsaaTcdeTotalsArrayInner.wsaaTcdeSingSumins[wsaaTcdeIx.toInt()]);
		rt01SpPolcnt.set(wsaaTcdeTotalsArrayInner.wsaaTcdeSingCount[wsaaTcdeIx.toInt()]);
		rt01SpPct.set(wsaaTcdeSpPct);
		printerFile.printRh604t01(rh604T01, indicArea);
	}

protected void allCnttype2500b5()
	{
		start2500b5();
	}

protected void start2500b5()
	{
		/* Print each of the Contract Type totals for this*/
		/* Transaction Code.*/
		 searchlabel1:
		{
			for (; isLT(wsaaTcdeIx, wsaaTcdeTotalsArrayInner.wsaaTcdeTotals.length); wsaaTcdeIx.add(1)){
				if (((isEQ(wsaaTcdeTotalsArrayInner.wsaaTcdeTrancode[wsaaTcdeIx.toInt()], wsaaTcdeSrchTrancode))
				&& (isNE(wsaaTcdeIx,wsaaTcdeTotIx)))) {
					rd01Cnttype.set(wsaaTcdeTotalsArrayInner.wsaaTcdeCnttype[wsaaTcdeIx.toInt()]);
					rd01Ap.set(wsaaTcdeTotalsArrayInner.wsaaTcdeAnnPrem[wsaaTcdeIx.toInt()]);
					rd01ApSa.set(wsaaTcdeTotalsArrayInner.wsaaTcdeAnnSumins[wsaaTcdeIx.toInt()]);
					rd01ApPolcnt.set(wsaaTcdeTotalsArrayInner.wsaaTcdeAnnCount[wsaaTcdeIx.toInt()]);
					compute(rd01ApPct, 3).setRounded((mult((div(wsaaTcdeTotalsArrayInner.wsaaTcdeAnnCount[wsaaTcdeIx.toInt()], wsaaTcdeApPolcnt)), 100)));
					wsaaTcdeApPct.add(rd01ApPct);
					rd01Sp.set(wsaaTcdeTotalsArrayInner.wsaaTcdeSingPrem[wsaaTcdeIx.toInt()]);
					rd01SpSa.set(wsaaTcdeTotalsArrayInner.wsaaTcdeSingSumins[wsaaTcdeIx.toInt()]);
					rd01SpPolcnt.set(wsaaTcdeTotalsArrayInner.wsaaTcdeSingCount[wsaaTcdeIx.toInt()]);
					compute(rd01SpPct, 3).setRounded((mult((div(wsaaTcdeTotalsArrayInner.wsaaTcdeSingCount[wsaaTcdeIx.toInt()], wsaaTcdeSpPolcnt)), 100)));
					wsaaTcdeSpPct.add(rd01SpPct);
					printerFile.printRh604d01(rh604D01, indicArea);
					wsaaTcdeIx.add(1);
					break searchlabel1;
				}
				if (isEQ(wsaaTcdeTotalsArrayInner.wsaaTcdeKey[wsaaTcdeIx.toInt()], HIVALUE)) {
					wsaaSrchEnd.set("Y");
					break searchlabel1;
				}
			}
			wsaaSrchEnd.set("Y");
		}
	}

protected void update3000()
	{
		/*UPDATE*/
		/** No Update Processing.*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(th565rec.currcode);
		zrdecplrec.batctrcde.set(bscdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-BCH-TOTALS-ARRAY--INNER
 */
private static final class WsaaBchTotalsArrayInner {

		/* WSAA-BCH-TOTALS-ARRAY */
	private FixedLengthStringData[] wsaaBchTotals = FLSInittedArray (100, 72);
	private FixedLengthStringData[] wsaaBchKey = FLSDArrayPartOfArrayStructure(6, wsaaBchTotals, 0);
	private FixedLengthStringData[] wsaaBchCntbranch = FLSDArrayPartOfArrayStructure(2, wsaaBchKey, 0, HIVALUE);
	private FixedLengthStringData[] wsaaBchTrancode = FLSDArrayPartOfArrayStructure(4, wsaaBchKey, 2, HIVALUE);
	private FixedLengthStringData[] wsaaBchData = FLSDArrayPartOfArrayStructure(66, wsaaBchTotals, 6);
	private ZonedDecimalData[] wsaaBchAnnPrem = ZDArrayPartOfArrayStructure(11, 2, wsaaBchData, 0, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaBchAnnSumins = ZDArrayPartOfArrayStructure(11, 2, wsaaBchData, 11, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaBchAnnCount = ZDArrayPartOfArrayStructure(5, 0, wsaaBchData, 22, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaBchAnnPct = ZDArrayPartOfArrayStructure(6, 3, wsaaBchData, 27, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaBchSingPrem = ZDArrayPartOfArrayStructure(11, 2, wsaaBchData, 33, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaBchSingSumins = ZDArrayPartOfArrayStructure(11, 2, wsaaBchData, 44, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaBchSingCount = ZDArrayPartOfArrayStructure(5, 0, wsaaBchData, 55, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaBchSingPct = ZDArrayPartOfArrayStructure(6, 3, wsaaBchData, 60, 0.0, UNSIGNED_TRUE);
}
/*
 * Class transformed  from Data Structure WSAA-TCDE-TOTALS-ARRAY--INNER
 */
private static final class WsaaTcdeTotalsArrayInner {

		/* WSAA-TCDE-TOTALS-ARRAY */
	private FixedLengthStringData[] wsaaTcdeTotals = FLSInittedArray (500, 61);
	private FixedLengthStringData[] wsaaTcdeKey = FLSDArrayPartOfArrayStructure(7, wsaaTcdeTotals, 0);
	private FixedLengthStringData[] wsaaTcdeTrancode = FLSDArrayPartOfArrayStructure(4, wsaaTcdeKey, 0, HIVALUE);
	private FixedLengthStringData[] wsaaTcdeCnttype = FLSDArrayPartOfArrayStructure(3, wsaaTcdeKey, 4, HIVALUE);
	private FixedLengthStringData[] wsaaTcdeData = FLSDArrayPartOfArrayStructure(54, wsaaTcdeTotals, 7);
	private ZonedDecimalData[] wsaaTcdeAnnPrem = ZDArrayPartOfArrayStructure(11, 2, wsaaTcdeData, 0, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaTcdeAnnSumins = ZDArrayPartOfArrayStructure(11, 2, wsaaTcdeData, 11, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaTcdeAnnCount = ZDArrayPartOfArrayStructure(5, 0, wsaaTcdeData, 22, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaTcdeSingPrem = ZDArrayPartOfArrayStructure(11, 2, wsaaTcdeData, 27, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaTcdeSingSumins = ZDArrayPartOfArrayStructure(11, 2, wsaaTcdeData, 38, 0.0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaTcdeSingCount = ZDArrayPartOfArrayStructure(5, 0, wsaaTcdeData, 49, 0.0, UNSIGNED_TRUE);
}
}
