package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr57kscreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 23, 11, 73}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr57kScreenVars sv = (Sr57kScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr57kscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr57kScreenVars screenVars = (Sr57kScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.cnttyp.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.chdrstatus.setClassString("");
		screenVars.premstatus.setClassString("");
		screenVars.cownnum.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.lifenum.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.reqntype01.setClassString("");
		screenVars.reqntype02.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.crcind.setClassString("");
		screenVars.mopdesc.setClassString("");
		screenVars.mopdesc02.setClassString("");
		screenVars.ptdate.setClassString("");
	}

/**
 * Clear all the variables in Sr57kscreen
 */
	public static void clear(VarModel pv) {
		Sr57kScreenVars screenVars = (Sr57kScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.cnttyp.clear();
		screenVars.ctypedes.clear();
		screenVars.chdrstatus.clear();
		screenVars.premstatus.clear();
		screenVars.cownnum.clear();
		screenVars.ownername.clear();
		screenVars.lifenum.clear();
		screenVars.lifename.clear();
		screenVars.reqntype01.clear();
		screenVars.reqntype02.clear();
		screenVars.ddind.clear();
		screenVars.crcind.clear();
		screenVars.mopdesc.clear();
		screenVars.mopdesc02.clear();
		screenVars.ptdate.clear();
	}
}
