package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:51
 * @author Quipoz
 */
public class St515screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		St515ScreenVars sv = (St515ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.St515screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		St515ScreenVars screenVars = (St515ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.unit.setClassString("");
		screenVars.tyearno01.setClassString("");
		screenVars.tyearno02.setClassString("");
		screenVars.tyearno03.setClassString("");
		screenVars.tyearno04.setClassString("");
		screenVars.tyearno05.setClassString("");
		screenVars.tyearno06.setClassString("");
		screenVars.tyearno07.setClassString("");
		screenVars.tyearno08.setClassString("");
		screenVars.tyearno09.setClassString("");
		screenVars.tyearno10.setClassString("");
		screenVars.tyearno11.setClassString("");
		screenVars.tyearno12.setClassString("");
		screenVars.tyearno13.setClassString("");
		screenVars.tyearno14.setClassString("");
		screenVars.tyearno15.setClassString("");
		screenVars.tyearno16.setClassString("");
		screenVars.tyearno17.setClassString("");
		screenVars.tyearno18.setClassString("");
		screenVars.tyearno19.setClassString("");
		screenVars.tyearno20.setClassString("");
		screenVars.tyearno21.setClassString("");
		screenVars.tyearno22.setClassString("");
		screenVars.tyearno23.setClassString("");
		screenVars.tyearno24.setClassString("");
		screenVars.tyearno25.setClassString("");
		screenVars.tyearno26.setClassString("");
		screenVars.tyearno27.setClassString("");
		screenVars.tyearno28.setClassString("");
		screenVars.tyearno29.setClassString("");
		screenVars.tyearno30.setClassString("");
		screenVars.tyearno31.setClassString("");
		screenVars.tyearno32.setClassString("");
		screenVars.tyearno33.setClassString("");
		screenVars.tyearno34.setClassString("");
		screenVars.tyearno35.setClassString("");
		screenVars.tyearno36.setClassString("");
		screenVars.tyearno37.setClassString("");
		screenVars.tyearno38.setClassString("");
		screenVars.tyearno39.setClassString("");
		screenVars.tyearno40.setClassString("");
		screenVars.tyearno41.setClassString("");
		screenVars.tyearno42.setClassString("");
		screenVars.tyearno43.setClassString("");
		screenVars.tyearno44.setClassString("");
		screenVars.tyearno45.setClassString("");
		screenVars.tyearno46.setClassString("");
		screenVars.tyearno47.setClassString("");
		screenVars.tyearno48.setClassString("");
		screenVars.tyearno49.setClassString("");
		screenVars.tyearno50.setClassString("");
		screenVars.tyearno51.setClassString("");
		screenVars.tyearno52.setClassString("");
		screenVars.tyearno53.setClassString("");
		screenVars.tyearno54.setClassString("");
		screenVars.tyearno55.setClassString("");
		screenVars.tyearno56.setClassString("");
		screenVars.tyearno57.setClassString("");
		screenVars.tyearno58.setClassString("");
		screenVars.tyearno59.setClassString("");
		screenVars.tyearno60.setClassString("");
		screenVars.tyearno61.setClassString("");
		screenVars.tyearno62.setClassString("");
		screenVars.tyearno63.setClassString("");
		screenVars.tyearno64.setClassString("");
		screenVars.tyearno65.setClassString("");
		screenVars.tyearno66.setClassString("");
		screenVars.tyearno67.setClassString("");
		screenVars.tyearno68.setClassString("");
		screenVars.tyearno69.setClassString("");
		screenVars.tyearno70.setClassString("");
		screenVars.tyearno71.setClassString("");
		screenVars.tyearno72.setClassString("");
		screenVars.tyearno73.setClassString("");
		screenVars.tyearno74.setClassString("");
		screenVars.tyearno75.setClassString("");
		screenVars.tyearno76.setClassString("");
		screenVars.tyearno77.setClassString("");
		screenVars.tyearno78.setClassString("");
		screenVars.tyearno79.setClassString("");
		screenVars.tyearno80.setClassString("");
		screenVars.tyearno81.setClassString("");
		screenVars.tyearno82.setClassString("");
		screenVars.tyearno83.setClassString("");
		screenVars.tyearno84.setClassString("");
		screenVars.tyearno85.setClassString("");
		screenVars.tyearno86.setClassString("");
		screenVars.tyearno87.setClassString("");
		screenVars.tyearno88.setClassString("");
		screenVars.tyearno89.setClassString("");
		screenVars.tyearno90.setClassString("");
		screenVars.tyearno91.setClassString("");
		screenVars.tyearno92.setClassString("");
		screenVars.tyearno93.setClassString("");
		screenVars.tyearno94.setClassString("");
		screenVars.tyearno95.setClassString("");
		screenVars.tdayno01.setClassString("");
		screenVars.tdayno02.setClassString("");
		screenVars.tdayno03.setClassString("");
		screenVars.tdayno04.setClassString("");
		screenVars.tdayno05.setClassString("");
		screenVars.tdayno06.setClassString("");
		screenVars.tdayno07.setClassString("");
		screenVars.tdayno08.setClassString("");
		screenVars.tdayno09.setClassString("");
		screenVars.tdayno10.setClassString("");
		screenVars.tdayno11.setClassString("");
		screenVars.tdayno12.setClassString("");
		screenVars.tdayno13.setClassString("");
		screenVars.tdayno14.setClassString("");
		screenVars.tdayno15.setClassString("");
		screenVars.tdayno16.setClassString("");
		screenVars.tdayno17.setClassString("");
		screenVars.tdayno18.setClassString("");
		screenVars.tdayno19.setClassString("");
		screenVars.tdayno20.setClassString("");
		screenVars.tdayno21.setClassString("");
		screenVars.tdayno22.setClassString("");
		screenVars.tdayno23.setClassString("");
		screenVars.tdayno24.setClassString("");
		screenVars.tdayno25.setClassString("");
		screenVars.tdayno26.setClassString("");
		screenVars.tdayno27.setClassString("");
		screenVars.tdayno28.setClassString("");
		screenVars.tdayno29.setClassString("");
		screenVars.tdayno30.setClassString("");
		screenVars.tdayno31.setClassString("");
		screenVars.tdayno32.setClassString("");
		screenVars.tdayno33.setClassString("");
		screenVars.tdayno34.setClassString("");
		screenVars.tdayno35.setClassString("");
		screenVars.tdayno36.setClassString("");
		screenVars.tdayno37.setClassString("");
		screenVars.tdayno38.setClassString("");
		screenVars.tdayno39.setClassString("");
		screenVars.tdayno40.setClassString("");
		screenVars.tdayno41.setClassString("");
		screenVars.tdayno42.setClassString("");
		screenVars.tdayno43.setClassString("");
		screenVars.tdayno44.setClassString("");
		screenVars.tdayno45.setClassString("");
		screenVars.tdayno46.setClassString("");
		screenVars.tdayno47.setClassString("");
		screenVars.tdayno48.setClassString("");
		screenVars.tdayno49.setClassString("");
		screenVars.tdayno50.setClassString("");
		screenVars.tdayno51.setClassString("");
		screenVars.tdayno52.setClassString("");
		screenVars.tdayno53.setClassString("");
		screenVars.tdayno54.setClassString("");
		screenVars.tdayno55.setClassString("");
		screenVars.tdayno56.setClassString("");
		screenVars.tdayno57.setClassString("");
		screenVars.tdayno58.setClassString("");
		screenVars.tdayno59.setClassString("");
		screenVars.tdayno60.setClassString("");
		screenVars.tdayno61.setClassString("");
		screenVars.tdayno62.setClassString("");
		screenVars.tdayno63.setClassString("");
		screenVars.tdayno64.setClassString("");
		screenVars.tdayno65.setClassString("");
		screenVars.tdayno66.setClassString("");
		screenVars.tdayno67.setClassString("");
		screenVars.tdayno68.setClassString("");
		screenVars.tdayno69.setClassString("");
		screenVars.tdayno70.setClassString("");
		screenVars.tdayno71.setClassString("");
		screenVars.tdayno72.setClassString("");
		screenVars.tdayno73.setClassString("");
		screenVars.tdayno74.setClassString("");
		screenVars.tdayno75.setClassString("");
		screenVars.tdayno76.setClassString("");
		screenVars.tdayno77.setClassString("");
		screenVars.tdayno78.setClassString("");
		screenVars.tdayno79.setClassString("");
		screenVars.tdayno80.setClassString("");
		screenVars.tdayno81.setClassString("");
		screenVars.tdayno82.setClassString("");
		screenVars.tdayno83.setClassString("");
		screenVars.tdayno84.setClassString("");
		screenVars.tdayno85.setClassString("");
		screenVars.tdayno86.setClassString("");
		screenVars.tdayno87.setClassString("");
		screenVars.tdayno88.setClassString("");
		screenVars.tdayno89.setClassString("");
		screenVars.tdayno90.setClassString("");
		screenVars.tdayno91.setClassString("");
		screenVars.tdayno92.setClassString("");
		screenVars.tdayno93.setClassString("");
		screenVars.tdayno94.setClassString("");
		screenVars.tdayno95.setClassString("");
	}

/**
 * Clear all the variables in St515screen
 */
	public static void clear(VarModel pv) {
		St515ScreenVars screenVars = (St515ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.unit.clear();
		screenVars.tyearno01.clear();
		screenVars.tyearno02.clear();
		screenVars.tyearno03.clear();
		screenVars.tyearno04.clear();
		screenVars.tyearno05.clear();
		screenVars.tyearno06.clear();
		screenVars.tyearno07.clear();
		screenVars.tyearno08.clear();
		screenVars.tyearno09.clear();
		screenVars.tyearno10.clear();
		screenVars.tyearno11.clear();
		screenVars.tyearno12.clear();
		screenVars.tyearno13.clear();
		screenVars.tyearno14.clear();
		screenVars.tyearno15.clear();
		screenVars.tyearno16.clear();
		screenVars.tyearno17.clear();
		screenVars.tyearno18.clear();
		screenVars.tyearno19.clear();
		screenVars.tyearno20.clear();
		screenVars.tyearno21.clear();
		screenVars.tyearno22.clear();
		screenVars.tyearno23.clear();
		screenVars.tyearno24.clear();
		screenVars.tyearno25.clear();
		screenVars.tyearno26.clear();
		screenVars.tyearno27.clear();
		screenVars.tyearno28.clear();
		screenVars.tyearno29.clear();
		screenVars.tyearno30.clear();
		screenVars.tyearno31.clear();
		screenVars.tyearno32.clear();
		screenVars.tyearno33.clear();
		screenVars.tyearno34.clear();
		screenVars.tyearno35.clear();
		screenVars.tyearno36.clear();
		screenVars.tyearno37.clear();
		screenVars.tyearno38.clear();
		screenVars.tyearno39.clear();
		screenVars.tyearno40.clear();
		screenVars.tyearno41.clear();
		screenVars.tyearno42.clear();
		screenVars.tyearno43.clear();
		screenVars.tyearno44.clear();
		screenVars.tyearno45.clear();
		screenVars.tyearno46.clear();
		screenVars.tyearno47.clear();
		screenVars.tyearno48.clear();
		screenVars.tyearno49.clear();
		screenVars.tyearno50.clear();
		screenVars.tyearno51.clear();
		screenVars.tyearno52.clear();
		screenVars.tyearno53.clear();
		screenVars.tyearno54.clear();
		screenVars.tyearno55.clear();
		screenVars.tyearno56.clear();
		screenVars.tyearno57.clear();
		screenVars.tyearno58.clear();
		screenVars.tyearno59.clear();
		screenVars.tyearno60.clear();
		screenVars.tyearno61.clear();
		screenVars.tyearno62.clear();
		screenVars.tyearno63.clear();
		screenVars.tyearno64.clear();
		screenVars.tyearno65.clear();
		screenVars.tyearno66.clear();
		screenVars.tyearno67.clear();
		screenVars.tyearno68.clear();
		screenVars.tyearno69.clear();
		screenVars.tyearno70.clear();
		screenVars.tyearno71.clear();
		screenVars.tyearno72.clear();
		screenVars.tyearno73.clear();
		screenVars.tyearno74.clear();
		screenVars.tyearno75.clear();
		screenVars.tyearno76.clear();
		screenVars.tyearno77.clear();
		screenVars.tyearno78.clear();
		screenVars.tyearno79.clear();
		screenVars.tyearno80.clear();
		screenVars.tyearno81.clear();
		screenVars.tyearno82.clear();
		screenVars.tyearno83.clear();
		screenVars.tyearno84.clear();
		screenVars.tyearno85.clear();
		screenVars.tyearno86.clear();
		screenVars.tyearno87.clear();
		screenVars.tyearno88.clear();
		screenVars.tyearno89.clear();
		screenVars.tyearno90.clear();
		screenVars.tyearno91.clear();
		screenVars.tyearno92.clear();
		screenVars.tyearno93.clear();
		screenVars.tyearno94.clear();
		screenVars.tyearno95.clear();
		screenVars.tdayno01.clear();
		screenVars.tdayno02.clear();
		screenVars.tdayno03.clear();
		screenVars.tdayno04.clear();
		screenVars.tdayno05.clear();
		screenVars.tdayno06.clear();
		screenVars.tdayno07.clear();
		screenVars.tdayno08.clear();
		screenVars.tdayno09.clear();
		screenVars.tdayno10.clear();
		screenVars.tdayno11.clear();
		screenVars.tdayno12.clear();
		screenVars.tdayno13.clear();
		screenVars.tdayno14.clear();
		screenVars.tdayno15.clear();
		screenVars.tdayno16.clear();
		screenVars.tdayno17.clear();
		screenVars.tdayno18.clear();
		screenVars.tdayno19.clear();
		screenVars.tdayno20.clear();
		screenVars.tdayno21.clear();
		screenVars.tdayno22.clear();
		screenVars.tdayno23.clear();
		screenVars.tdayno24.clear();
		screenVars.tdayno25.clear();
		screenVars.tdayno26.clear();
		screenVars.tdayno27.clear();
		screenVars.tdayno28.clear();
		screenVars.tdayno29.clear();
		screenVars.tdayno30.clear();
		screenVars.tdayno31.clear();
		screenVars.tdayno32.clear();
		screenVars.tdayno33.clear();
		screenVars.tdayno34.clear();
		screenVars.tdayno35.clear();
		screenVars.tdayno36.clear();
		screenVars.tdayno37.clear();
		screenVars.tdayno38.clear();
		screenVars.tdayno39.clear();
		screenVars.tdayno40.clear();
		screenVars.tdayno41.clear();
		screenVars.tdayno42.clear();
		screenVars.tdayno43.clear();
		screenVars.tdayno44.clear();
		screenVars.tdayno45.clear();
		screenVars.tdayno46.clear();
		screenVars.tdayno47.clear();
		screenVars.tdayno48.clear();
		screenVars.tdayno49.clear();
		screenVars.tdayno50.clear();
		screenVars.tdayno51.clear();
		screenVars.tdayno52.clear();
		screenVars.tdayno53.clear();
		screenVars.tdayno54.clear();
		screenVars.tdayno55.clear();
		screenVars.tdayno56.clear();
		screenVars.tdayno57.clear();
		screenVars.tdayno58.clear();
		screenVars.tdayno59.clear();
		screenVars.tdayno60.clear();
		screenVars.tdayno61.clear();
		screenVars.tdayno62.clear();
		screenVars.tdayno63.clear();
		screenVars.tdayno64.clear();
		screenVars.tdayno65.clear();
		screenVars.tdayno66.clear();
		screenVars.tdayno67.clear();
		screenVars.tdayno68.clear();
		screenVars.tdayno69.clear();
		screenVars.tdayno70.clear();
		screenVars.tdayno71.clear();
		screenVars.tdayno72.clear();
		screenVars.tdayno73.clear();
		screenVars.tdayno74.clear();
		screenVars.tdayno75.clear();
		screenVars.tdayno76.clear();
		screenVars.tdayno77.clear();
		screenVars.tdayno78.clear();
		screenVars.tdayno79.clear();
		screenVars.tdayno80.clear();
		screenVars.tdayno81.clear();
		screenVars.tdayno82.clear();
		screenVars.tdayno83.clear();
		screenVars.tdayno84.clear();
		screenVars.tdayno85.clear();
		screenVars.tdayno86.clear();
		screenVars.tdayno87.clear();
		screenVars.tdayno88.clear();
		screenVars.tdayno89.clear();
		screenVars.tdayno90.clear();
		screenVars.tdayno91.clear();
		screenVars.tdayno92.clear();
		screenVars.tdayno93.clear();
		screenVars.tdayno94.clear();
		screenVars.tdayno95.clear();
	}
}
