package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:28
 * Description:
 * Copybook name: TH596REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th596rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th596Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData cnRiskStats = new FixedLengthStringData(24).isAPartOf(th596Rec, 0);
  	public FixedLengthStringData[] cnRiskStat = FLSArrayPartOfStructure(12, 2, cnRiskStats, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(cnRiskStats, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cnRiskStat01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData cnRiskStat02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData cnRiskStat03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData cnRiskStat04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData cnRiskStat05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData cnRiskStat06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData cnRiskStat07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData cnRiskStat08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData cnRiskStat09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData cnRiskStat10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData cnRiskStat11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData cnRiskStat12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData fupcdess = new FixedLengthStringData(240).isAPartOf(th596Rec, 24);
  	public FixedLengthStringData[] fupcdes = FLSArrayPartOfStructure(60, 4, fupcdess, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(240).isAPartOf(fupcdess, 0, FILLER_REDEFINE);
  	public FixedLengthStringData fupcdes01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData fupcdes02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData fupcdes03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData fupcdes04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData fupcdes05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData fupcdes06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData fupcdes07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData fupcdes08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData fupcdes09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData fupcdes10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData fupcdes11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
  	public FixedLengthStringData fupcdes12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
  	public FixedLengthStringData fupcdes13 = new FixedLengthStringData(4).isAPartOf(filler1, 48);
  	public FixedLengthStringData fupcdes14 = new FixedLengthStringData(4).isAPartOf(filler1, 52);
  	public FixedLengthStringData fupcdes15 = new FixedLengthStringData(4).isAPartOf(filler1, 56);
  	public FixedLengthStringData fupcdes16 = new FixedLengthStringData(4).isAPartOf(filler1, 60);
  	public FixedLengthStringData fupcdes17 = new FixedLengthStringData(4).isAPartOf(filler1, 64);
  	public FixedLengthStringData fupcdes18 = new FixedLengthStringData(4).isAPartOf(filler1, 68);
  	public FixedLengthStringData fupcdes19 = new FixedLengthStringData(4).isAPartOf(filler1, 72);
  	public FixedLengthStringData fupcdes20 = new FixedLengthStringData(4).isAPartOf(filler1, 76);
  	public FixedLengthStringData fupcdes21 = new FixedLengthStringData(4).isAPartOf(filler1, 80);
  	public FixedLengthStringData fupcdes22 = new FixedLengthStringData(4).isAPartOf(filler1, 84);
  	public FixedLengthStringData fupcdes23 = new FixedLengthStringData(4).isAPartOf(filler1, 88);
  	public FixedLengthStringData fupcdes24 = new FixedLengthStringData(4).isAPartOf(filler1, 92);
  	public FixedLengthStringData fupcdes25 = new FixedLengthStringData(4).isAPartOf(filler1, 96);
  	public FixedLengthStringData fupcdes26 = new FixedLengthStringData(4).isAPartOf(filler1, 100);
  	public FixedLengthStringData fupcdes27 = new FixedLengthStringData(4).isAPartOf(filler1, 104);
  	public FixedLengthStringData fupcdes28 = new FixedLengthStringData(4).isAPartOf(filler1, 108);
  	public FixedLengthStringData fupcdes29 = new FixedLengthStringData(4).isAPartOf(filler1, 112);
  	public FixedLengthStringData fupcdes30 = new FixedLengthStringData(4).isAPartOf(filler1, 116);
  	public FixedLengthStringData fupcdes31 = new FixedLengthStringData(4).isAPartOf(filler1, 120);
  	public FixedLengthStringData fupcdes32 = new FixedLengthStringData(4).isAPartOf(filler1, 124);
  	public FixedLengthStringData fupcdes33 = new FixedLengthStringData(4).isAPartOf(filler1, 128);
  	public FixedLengthStringData fupcdes34 = new FixedLengthStringData(4).isAPartOf(filler1, 132);
  	public FixedLengthStringData fupcdes35 = new FixedLengthStringData(4).isAPartOf(filler1, 136);
  	public FixedLengthStringData fupcdes36 = new FixedLengthStringData(4).isAPartOf(filler1, 140);
  	public FixedLengthStringData fupcdes37 = new FixedLengthStringData(4).isAPartOf(filler1, 144);
  	public FixedLengthStringData fupcdes38 = new FixedLengthStringData(4).isAPartOf(filler1, 148);
  	public FixedLengthStringData fupcdes39 = new FixedLengthStringData(4).isAPartOf(filler1, 152);
  	public FixedLengthStringData fupcdes40 = new FixedLengthStringData(4).isAPartOf(filler1, 156);
  	public FixedLengthStringData fupcdes41 = new FixedLengthStringData(4).isAPartOf(filler1, 160);
  	public FixedLengthStringData fupcdes42 = new FixedLengthStringData(4).isAPartOf(filler1, 164);
  	public FixedLengthStringData fupcdes43 = new FixedLengthStringData(4).isAPartOf(filler1, 168);
  	public FixedLengthStringData fupcdes44 = new FixedLengthStringData(4).isAPartOf(filler1, 172);
  	public FixedLengthStringData fupcdes45 = new FixedLengthStringData(4).isAPartOf(filler1, 176);
  	public FixedLengthStringData fupcdes46 = new FixedLengthStringData(4).isAPartOf(filler1, 180);
  	public FixedLengthStringData fupcdes47 = new FixedLengthStringData(4).isAPartOf(filler1, 184);
  	public FixedLengthStringData fupcdes48 = new FixedLengthStringData(4).isAPartOf(filler1, 188);
  	public FixedLengthStringData fupcdes49 = new FixedLengthStringData(4).isAPartOf(filler1, 192);
  	public FixedLengthStringData fupcdes50 = new FixedLengthStringData(4).isAPartOf(filler1, 196);
  	public FixedLengthStringData fupcdes51 = new FixedLengthStringData(4).isAPartOf(filler1, 200);
  	public FixedLengthStringData fupcdes52 = new FixedLengthStringData(4).isAPartOf(filler1, 204);
  	public FixedLengthStringData fupcdes53 = new FixedLengthStringData(4).isAPartOf(filler1, 208);
  	public FixedLengthStringData fupcdes54 = new FixedLengthStringData(4).isAPartOf(filler1, 212);
  	public FixedLengthStringData fupcdes55 = new FixedLengthStringData(4).isAPartOf(filler1, 216);
  	public FixedLengthStringData fupcdes56 = new FixedLengthStringData(4).isAPartOf(filler1, 220);
  	public FixedLengthStringData fupcdes57 = new FixedLengthStringData(4).isAPartOf(filler1, 224);
  	public FixedLengthStringData fupcdes58 = new FixedLengthStringData(4).isAPartOf(filler1, 228);
  	public FixedLengthStringData fupcdes59 = new FixedLengthStringData(4).isAPartOf(filler1, 232);
  	public FixedLengthStringData fupcdes60 = new FixedLengthStringData(4).isAPartOf(filler1, 236);
  	public FixedLengthStringData hcondate = new FixedLengthStringData(1).isAPartOf(th596Rec, 264);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(th596Rec, 265);
  	public FixedLengthStringData trcodes = new FixedLengthStringData(112).isAPartOf(th596Rec, 266);
  	public FixedLengthStringData[] trcode = FLSArrayPartOfStructure(28, 4, trcodes, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(112).isAPartOf(trcodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData trcode01 = new FixedLengthStringData(4).isAPartOf(filler2, 0);
  	public FixedLengthStringData trcode02 = new FixedLengthStringData(4).isAPartOf(filler2, 4);
  	public FixedLengthStringData trcode03 = new FixedLengthStringData(4).isAPartOf(filler2, 8);
  	public FixedLengthStringData trcode04 = new FixedLengthStringData(4).isAPartOf(filler2, 12);
  	public FixedLengthStringData trcode05 = new FixedLengthStringData(4).isAPartOf(filler2, 16);
  	public FixedLengthStringData trcode06 = new FixedLengthStringData(4).isAPartOf(filler2, 20);
  	public FixedLengthStringData trcode07 = new FixedLengthStringData(4).isAPartOf(filler2, 24);
  	public FixedLengthStringData trcode08 = new FixedLengthStringData(4).isAPartOf(filler2, 28);
  	public FixedLengthStringData trcode09 = new FixedLengthStringData(4).isAPartOf(filler2, 32);
  	public FixedLengthStringData trcode10 = new FixedLengthStringData(4).isAPartOf(filler2, 36);
  	public FixedLengthStringData trcode11 = new FixedLengthStringData(4).isAPartOf(filler2, 40);
  	public FixedLengthStringData trcode12 = new FixedLengthStringData(4).isAPartOf(filler2, 44);
  	public FixedLengthStringData trcode13 = new FixedLengthStringData(4).isAPartOf(filler2, 48);
  	public FixedLengthStringData trcode14 = new FixedLengthStringData(4).isAPartOf(filler2, 52);
  	public FixedLengthStringData trcode15 = new FixedLengthStringData(4).isAPartOf(filler2, 56);
  	public FixedLengthStringData trcode16 = new FixedLengthStringData(4).isAPartOf(filler2, 60);
  	public FixedLengthStringData trcode17 = new FixedLengthStringData(4).isAPartOf(filler2, 64);
  	public FixedLengthStringData trcode18 = new FixedLengthStringData(4).isAPartOf(filler2, 68);
  	public FixedLengthStringData trcode19 = new FixedLengthStringData(4).isAPartOf(filler2, 72);
  	public FixedLengthStringData trcode20 = new FixedLengthStringData(4).isAPartOf(filler2, 76);
  	public FixedLengthStringData trcode21 = new FixedLengthStringData(4).isAPartOf(filler2, 80);
  	public FixedLengthStringData trcode22 = new FixedLengthStringData(4).isAPartOf(filler2, 84);
  	public FixedLengthStringData trcode23 = new FixedLengthStringData(4).isAPartOf(filler2, 88);
  	public FixedLengthStringData trcode24 = new FixedLengthStringData(4).isAPartOf(filler2, 92);
  	public FixedLengthStringData trcode25 = new FixedLengthStringData(4).isAPartOf(filler2, 96);
  	public FixedLengthStringData trcode26 = new FixedLengthStringData(4).isAPartOf(filler2, 100);
  	public FixedLengthStringData trcode27 = new FixedLengthStringData(4).isAPartOf(filler2, 104);
  	public FixedLengthStringData trcode28 = new FixedLengthStringData(4).isAPartOf(filler2, 108);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(122).isAPartOf(th596Rec, 378, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th596Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th596Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}