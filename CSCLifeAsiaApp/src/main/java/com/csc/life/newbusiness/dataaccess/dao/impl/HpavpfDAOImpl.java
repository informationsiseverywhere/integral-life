package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.HpavpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpavpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;
import java.sql.PreparedStatement;



public class HpavpfDAOImpl extends BaseDAOImpl<Hpavpf> implements HpavpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HpavpfDAOImpl.class);
	
	
	public List<Hpavpf> searchHpavpfRecord(String chdrcoy) {
		List<Hpavpf> hpavResult = new ArrayList<Hpavpf>();
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT CHDRCOY,CHDRNUM,LINSNAME, HOISSDTE, ANETPRE, ASPTDTY, CURR FROM hpavpf WHERE RTRIM(CHDRCOY)=?  ");
		sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		try {
			ps.setString(1, chdrcoy);
			
			
			rs = executeQuery(ps);
			Hpavpf hpav = null;
			while (rs.next()) {
				hpav = new Hpavpf();
				hpav.setChdrcoy(rs.getString(1));
				hpav.setChdrnum(rs.getString(2));
				hpav.setLinsname(rs.getString(3));
				hpav.setHoissdte(rs.getInt(4));
				hpav.setAnetpre(rs.getInt(5));
				hpav.setAsptdty(rs.getInt(6));
				hpav.setCurr(rs.getString(7));
				
				hpavResult.add(hpav);
			}

		} catch (SQLException e) {
			LOGGER.error("HpavpfDAOImpl()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return hpavResult;
	}
	
	
	
	
	
	
	
	
	
	
	
	}
	

