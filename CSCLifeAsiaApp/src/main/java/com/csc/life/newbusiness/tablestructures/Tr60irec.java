package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Tr60irec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr60iRec = new FixedLengthStringData(3);
	public FixedLengthStringData growth = DD.gcdblind.copy().isAPartOf(tr60iRec,0);
	public FixedLengthStringData stepup = DD.gcdthclm.copy().isAPartOf(tr60iRec,1);
	public FixedLengthStringData higherGrowth = DD.gcsetlmd.copy().isAPartOf(tr60iRec,2);

	public void initialize() {
		COBOLFunctions.initialize(tr60iRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tr60iRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}