/*
 * File: Bh602.java
 * Date: 29 August 2009 21:38:44
 * Author: Quipoz Limited
 * 
 * Class transformed from BH602.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.newbusiness.dataaccess.HmtrTableDAM;
import com.csc.life.newbusiness.reports.Rh602Report;
import com.csc.life.newbusiness.tablestructures.Th601rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*    Monthly New Business Turnaround Report
*    ======================================
*
*    This report is aimed to show turnaround times detail for New
*    Business Transaction on a monthly basis. The outstanding
*    cases from the previous month are brought forward to the
*    current month, to which are added all of the new cases
*    submitted this month. The current month's transactions are
*    split into 3 categories - Completed, Cancelled, Outstanding.
*    These are subtracted from the month's brought forward
*    transactions to produce what will be the subsequent month's
*    brought forward transaction.
*
*  This program reads the extract file HTTAPF produced by B9090
*  and sort the records in the following order:
*       Company
*       Channel
*       Product
*
*    In both the completed and outstanding cases, "period" is
*    being keep track : -
*
*    For approved and issued cases, period between the date the
*    proposal was received (PTRN-PTRNEFF) and the issue date
*    (HPTNMPS-PTRNEFF).
*
*    For outstanding cases, period between the date the proposal
*    was received, and the effective date of the Report Run.
*
*****************************************************************
* </pre>
*/
public class Bh602 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlhttaCursorrs = null;
	private java.sql.PreparedStatement sqlhttaCursorps = null;
	private java.sql.Connection sqlhttaCursorconn = null;
	private String sqlhttaCursor = "";
	private Rh602Report printerFile = new Rh602Report();

	private FixedLengthStringData printerRec = new FixedLengthStringData(120);
	private FixedLengthStringData rh602Record = new FixedLengthStringData(120).isAPartOf(printerRec, 0);
	private FixedLengthStringData rh602h01O = new FixedLengthStringData(120).isAPartOf(rh602Record, 0, REDEFINE);
	private FixedLengthStringData forsdesc = new FixedLengthStringData(10).isAPartOf(rh602h01O, 0);
	private ZonedDecimalData year = new ZonedDecimalData(4, 0).isAPartOf(rh602h01O, 10);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rh602h01O, 14);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rh602h01O, 15);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(rh602h01O, 45);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(rh602h01O, 55);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(rh602h01O, 57);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rh602h01O, 87);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(rh602h01O, 90);
	private FixedLengthStringData rh602h02O = new FixedLengthStringData(1).isAPartOf(rh602Record, 0, REDEFINE);
	private FixedLengthStringData dummy = new FixedLengthStringData(1).isAPartOf(rh602h02O, 0);
	private FixedLengthStringData rh602h03O = new FixedLengthStringData(16).isAPartOf(rh602Record, 0, REDEFINE);
	private ZonedDecimalData lapday01 = new ZonedDecimalData(2, 0).isAPartOf(rh602h03O, 0);
	private ZonedDecimalData lapday02 = new ZonedDecimalData(2, 0).isAPartOf(rh602h03O, 2);
	private ZonedDecimalData lapday03 = new ZonedDecimalData(2, 0).isAPartOf(rh602h03O, 4);
	private ZonedDecimalData lapday04 = new ZonedDecimalData(2, 0).isAPartOf(rh602h03O, 6);
	private ZonedDecimalData lapday05 = new ZonedDecimalData(2, 0).isAPartOf(rh602h03O, 8);
	private ZonedDecimalData lapday06 = new ZonedDecimalData(2, 0).isAPartOf(rh602h03O, 10);
	private ZonedDecimalData lapday07 = new ZonedDecimalData(2, 0).isAPartOf(rh602h03O, 12);
	private ZonedDecimalData lapday08 = new ZonedDecimalData(2, 0).isAPartOf(rh602h03O, 14);
	private FixedLengthStringData rh602h04O = new FixedLengthStringData(20).isAPartOf(rh602Record, 0, REDEFINE);
	private ZonedDecimalData weeks01 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 0);
	private ZonedDecimalData weeks02 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 2);
	private ZonedDecimalData weeks03 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 4);
	private ZonedDecimalData weeks04 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 6);
	private ZonedDecimalData weeks05 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 8);
	private ZonedDecimalData weeks06 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 10);
	private ZonedDecimalData weeks07 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 12);
	private ZonedDecimalData weeks08 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 14);
	private ZonedDecimalData weeks09 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 16);
	private ZonedDecimalData weeks10 = new ZonedDecimalData(2, 0).isAPartOf(rh602h04O, 18);
	private FixedLengthStringData rh602d01O = new FixedLengthStringData(5).isAPartOf(rh602Record, 0, REDEFINE);
	private ZonedDecimalData zpolcnt = new ZonedDecimalData(5, 0).isAPartOf(rh602d01O, 0);
	private FixedLengthStringData rh602d02O = new FixedLengthStringData(35).isAPartOf(rh602Record, 0, REDEFINE);
	private ZonedDecimalData zpolcnt11 = new ZonedDecimalData(5, 0).isAPartOf(rh602d02O, 0);
	private ZonedDecimalData zpolcnt12 = new ZonedDecimalData(5, 0).isAPartOf(rh602d02O, 5);
	private ZonedDecimalData zpolcnt13 = new ZonedDecimalData(5, 0).isAPartOf(rh602d02O, 10);
	private ZonedDecimalData zpolcnt14 = new ZonedDecimalData(5, 0).isAPartOf(rh602d02O, 15);
	private ZonedDecimalData zpolcnt15 = new ZonedDecimalData(5, 0).isAPartOf(rh602d02O, 20);
	private ZonedDecimalData zpolcnt16 = new ZonedDecimalData(5, 0).isAPartOf(rh602d02O, 25);
	private ZonedDecimalData zpolcnt17 = new ZonedDecimalData(5, 0).isAPartOf(rh602d02O, 30);
	private FixedLengthStringData rh602d03O = new FixedLengthStringData(30).isAPartOf(rh602Record, 0, REDEFINE);
	private ZonedDecimalData zpolcnt21 = new ZonedDecimalData(5, 0).isAPartOf(rh602d03O, 0);
	private ZonedDecimalData zpolcnt22 = new ZonedDecimalData(5, 0).isAPartOf(rh602d03O, 5);
	private ZonedDecimalData zpolcnt23 = new ZonedDecimalData(5, 0).isAPartOf(rh602d03O, 10);
	private ZonedDecimalData zpolcnt24 = new ZonedDecimalData(5, 0).isAPartOf(rh602d03O, 15);
	private ZonedDecimalData zpolcnt25 = new ZonedDecimalData(5, 0).isAPartOf(rh602d03O, 20);
	private ZonedDecimalData zpolcnt26 = new ZonedDecimalData(5, 0).isAPartOf(rh602d03O, 25);
	private FixedLengthStringData rh602e01O = new FixedLengthStringData(1).isAPartOf(rh602Record, 0, REDEFINE);
	private FixedLengthStringData dummy1 = new FixedLengthStringData(1).isAPartOf(rh602e01O, 0);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH602");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaHeaderMonth = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaR = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaDurationFound = new FixedLengthStringData(1);
	private Validator durationFound = new Validator(wsaaDurationFound, "Y");
		/* WSAA-OUTPUT-FIELDS */
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaSdate = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaAccumulator = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaFirstRecord = new FixedLengthStringData(1);
	private Validator firstRecord = new Validator(wsaaFirstRecord, "Y");

	private FixedLengthStringData wsaaRowFound = new FixedLengthStringData(1);
	private Validator rowFound = new Validator(wsaaRowFound, "Y");

	private FixedLengthStringData wsaaTtlRowFound = new FixedLengthStringData(1);
	private Validator ttlRowFound = new Validator(wsaaTtlRowFound, "Y");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaPrevCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrevCntbranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevCnttype = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

	private FixedLengthStringData wsaaSqlHttapf = new FixedLengthStringData(27);
	private FixedLengthStringData wsaaSqlChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlHttapf, 0);
	private FixedLengthStringData wsaaSqlChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaSqlHttapf, 1);
	private FixedLengthStringData wsaaSqlCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlHttapf, 9);
	private FixedLengthStringData wsaaSqlAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaSqlHttapf, 11);
	private FixedLengthStringData wsaaSqlCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlHttapf, 19);
	private FixedLengthStringData wsaaSqlHrow = new FixedLengthStringData(2).isAPartOf(wsaaSqlHttapf, 22);
	private PackedDecimalData wsaaSqlDaysvrnce = new PackedDecimalData(5, 0).isAPartOf(wsaaSqlHttapf, 24);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaPrevDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaPrevYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrevDate, 0).setUnsigned();
	private ZonedDecimalData wsaaPrevMnth = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrevDate, 4).setUnsigned();
	private ZonedDecimalData wsaaPrevDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrevDate, 6).setUnsigned();

	private FixedLengthStringData wsaaEffectiveDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffectiveDate, 0).setUnsigned();
	private ZonedDecimalData wsaaMnth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffectiveDate, 4).setUnsigned();
	private ZonedDecimalData wsaaDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffectiveDate, 6).setUnsigned();

	private FixedLengthStringData wsaaOsTArray = new FixedLengthStringData(150);
	private FixedLengthStringData[] wsaaOsLineT = FLSArrayPartOfStructure(5, 30, wsaaOsTArray, 0);
	private ZonedDecimalData[][] wsaaOsCntT = ZDArrayPartOfArrayStructure(6, 5, 0, wsaaOsLineT, 0, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaOsSArray = new FixedLengthStringData(150);
	private FixedLengthStringData[] wsaaOsLineS = FLSArrayPartOfStructure(5, 30, wsaaOsSArray, 0);
	private ZonedDecimalData[][] wsaaOsCntS = ZDArrayPartOfArrayStructure(6, 5, 0, wsaaOsLineS, 0, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaOsPArray = new FixedLengthStringData(150);
	private FixedLengthStringData[] wsaaOsLineP = FLSArrayPartOfStructure(5, 30, wsaaOsPArray, 0);
	private ZonedDecimalData[][] wsaaOsCntP = ZDArrayPartOfArrayStructure(6, 5, 0, wsaaOsLineP, 0, UNSIGNED_TRUE);
	private ZonedDecimalData wsaaNoOfOsRows = new ZonedDecimalData(2, 0).init(5).setUnsigned();

	private FixedLengthStringData wsaaOsRows = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaRowN = new FixedLengthStringData(2).isAPartOf(wsaaOsRows, 0).init("N ");
	private FixedLengthStringData wsaaRowP = new FixedLengthStringData(2).isAPartOf(wsaaOsRows, 2).init("P ");
	private FixedLengthStringData wsaaRowQ = new FixedLengthStringData(2).isAPartOf(wsaaOsRows, 4).init("Q ");
	private FixedLengthStringData wsaaRowR = new FixedLengthStringData(2).isAPartOf(wsaaOsRows, 6).init("R ");
	private FixedLengthStringData wsaaRowS = new FixedLengthStringData(2).isAPartOf(wsaaOsRows, 8).init("S ");

	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaOsRows, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaOsRowId = FLSArrayPartOfStructure(5, 2, filler4, 0);
	private int wsaaNoOfTtlRows = 7;

	private FixedLengthStringData wsaaTtlRows = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaTtlRowD = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 0).init("D ");
	private FixedLengthStringData wsaaTtlRowE = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 2).init("E ");
	private FixedLengthStringData wsaaTtlRowG = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 4).init("G ");
	private FixedLengthStringData wsaaTtlRowH = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 6).init("H ");
	private FixedLengthStringData wsaaTtlRowI = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 8).init("I ");
	private FixedLengthStringData wsaaTtlRowJ = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 10).init("J ");
	private FixedLengthStringData wsaaTtlRowS = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 12).init("S ");

	private FixedLengthStringData filler5 = new FixedLengthStringData(14).isAPartOf(wsaaTtlRows, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaTtlRowId = FLSArrayPartOfStructure(7, 2, filler5, 0);
	private ZonedDecimalData wsaaBfRowNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCompRowNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIssRowNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCanRowNo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaOsRowNo = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaIsTArray = new FixedLengthStringData(70);
	private FixedLengthStringData[] wsaaIsLineT = FLSArrayPartOfStructure(2, 35, wsaaIsTArray, 0);
	private ZonedDecimalData[][] wsaaIsCntT = ZDArrayPartOfArrayStructure(7, 5, 0, wsaaIsLineT, 0, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaIsSArray = new FixedLengthStringData(70);
	private FixedLengthStringData[] wsaaIsLineS = FLSArrayPartOfStructure(2, 35, wsaaIsSArray, 0);
	private ZonedDecimalData[][] wsaaIsCntS = ZDArrayPartOfArrayStructure(7, 5, 0, wsaaIsLineS, 0, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaIsPArray = new FixedLengthStringData(70);
	private FixedLengthStringData[] wsaaIsLineP = FLSArrayPartOfStructure(2, 35, wsaaIsPArray, 0);
	private ZonedDecimalData[][] wsaaIsCntP = ZDArrayPartOfArrayStructure(7, 5, 0, wsaaIsLineP, 0, UNSIGNED_TRUE);
	private int wsaaNoOfIssueRows = 2;

	private FixedLengthStringData wsaaIssueRows = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRowG = new FixedLengthStringData(2).isAPartOf(wsaaIssueRows, 0).init("G ");
	private FixedLengthStringData wsaaRowH = new FixedLengthStringData(2).isAPartOf(wsaaIssueRows, 2).init("H ");

	private FixedLengthStringData filler6 = new FixedLengthStringData(4).isAPartOf(wsaaIssueRows, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaIssueRowId = FLSArrayPartOfStructure(2, 2, filler6, 0);

	private FixedLengthStringData wsaaTtlForCo = new FixedLengthStringData(1);
	private Validator cttlForCo = new Validator(wsaaTtlForCo, "C");
	private Validator sttlForCo = new Validator(wsaaTtlForCo, "S");
	private Validator ttlForCo = new Validator(wsaaTtlForCo, "T");

	private FixedLengthStringData wsaaTArray = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaCntT = ZDArrayPartOfStructure(7, 5, 0, wsaaTArray, 0, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaSArray = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaCntS = ZDArrayPartOfStructure(7, 5, 0, wsaaSArray, 0, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaPArray = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaCntP = ZDArrayPartOfStructure(7, 5, 0, wsaaPArray, 0, UNSIGNED_TRUE);
	private int wsaaNoOfTRows = 7;

	private FixedLengthStringData wsaaTRows = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaRowA = new FixedLengthStringData(2).isAPartOf(wsaaTRows, 0).init("A ");
	private FixedLengthStringData wsaaRowB = new FixedLengthStringData(2).isAPartOf(wsaaTRows, 2).init("B ");
	private FixedLengthStringData wsaaRowC = new FixedLengthStringData(2).isAPartOf(wsaaTRows, 4).init("C ");
	private FixedLengthStringData wsaaRowD = new FixedLengthStringData(2).isAPartOf(wsaaTRows, 6).init("D ");
	private FixedLengthStringData wsaaRowE = new FixedLengthStringData(2).isAPartOf(wsaaTRows, 8).init("E ");
	private FixedLengthStringData wsaaRowI = new FixedLengthStringData(2).isAPartOf(wsaaTRows, 10).init("I ");
	private FixedLengthStringData wsaaRowJ = new FixedLengthStringData(2).isAPartOf(wsaaTRows, 12).init("J ");

	private FixedLengthStringData filler7 = new FixedLengthStringData(14).isAPartOf(wsaaTRows, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaTRowId = FLSArrayPartOfStructure(7, 2, filler7, 0);

	private FixedLengthStringData wsaaLineFArray = new FixedLengthStringData(44);
	private ZonedDecimalData[] wsaaCntF = ZDArrayPartOfStructure(8, 2, 0, wsaaLineFArray, 0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaDayMin = ZDArrayPartOfStructure(7, 2, 0, wsaaLineFArray, 16, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaDayMax = ZDArrayPartOfStructure(7, 2, 0, wsaaLineFArray, 30, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaLineKArray = new FixedLengthStringData(56);
	private ZonedDecimalData[] wsaaCntK = ZDArrayPartOfStructure(10, 2, 0, wsaaLineKArray, 0, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaWeeksMin = ZDArrayPartOfStructure(6, 3, 0, wsaaLineKArray, 20, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaWeeksMax = ZDArrayPartOfStructure(6, 3, 0, wsaaLineKArray, 38, UNSIGNED_TRUE);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(150);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(150);

	private FixedLengthStringData wsaaHttaFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler8 = new FixedLengthStringData(4).isAPartOf(wsaaHttaFn, 0, FILLER).init("HTTA");
	private FixedLengthStringData wsaaHttaRunid = new FixedLengthStringData(2).isAPartOf(wsaaHttaFn, 4);
	private ZonedDecimalData wsaaHttaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHttaFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler9 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaCalendarMonths = new FixedLengthStringData(120);
	private FixedLengthStringData wsaaJanuary = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 0).init("JANUARY   ");
	private FixedLengthStringData wsaaFebruary = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 10).init("FEBRUARY  ");
	private FixedLengthStringData wsaaMarch = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 20).init("MARCH     ");
	private FixedLengthStringData wsaaApril = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 30).init("APRIL     ");
	private FixedLengthStringData wsaaMay = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 40).init("MAY       ");
	private FixedLengthStringData wsaaJune = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 50).init("JUNE      ");
	private FixedLengthStringData wsaaJuly = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 60).init("JULY      ");
	private FixedLengthStringData wsaaAugust = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 70).init("AUGUST    ");
	private FixedLengthStringData wsaaSeptember = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 80).init("SEPTEMBER ");
	private FixedLengthStringData wsaaOctober = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 90).init("OCTOBER   ");
	private FixedLengthStringData wsaaNovember = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 100).init("NOVEMBER  ");
	private FixedLengthStringData wsaaDecember = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 110).init("DECEMBER  ");

	private FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(wsaaCalendarMonths, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaCalendarMonth = FLSArrayPartOfStructure(12, 10, filler11, 0);
	private String wsaaBranchnm = "All Branches";
	private String wsaaAllCnttype = "***";
	private String wsaaCntdesc = "All Products                  ";
	private String wsaaUnknownCntdesc = "????????????                  ";
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String hmtrrec = "HMTRREC";
		/* ERRORS */
	private String ivrm = "IVRM";
	private String esql = "ESQL";
		/* TABLES */
	private String t1692 = "T1692";
	private String t1693 = "T1693";
	private String t5688 = "T5688";
	private String th601 = "TH601";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Mthly NB Turnaround B/F Figures*/
	private HmtrTableDAM hmtrIO = new HmtrTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Th601rec th601rec = new Th601rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2080, 
		exit2090, 
		exit3090, 
		z190Exit
	}

	public Bh602() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		varcom.vrcmDate.set(getCobolDate());
		wsaaHttaRunid.set(bprdIO.getSystemParam04());
		wsaaHttaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(HTTAPF) TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaHttaFn.toString());
		stringVariable1.append(") ");
		stringVariable1.append("MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		stringVariable1.append(" SEQONLY(*YES 1000)");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th601);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th601rec.th601Rec.set(itemIO.getGenarea());
		initialize(wsaaTArray);
		initialize(wsaaIsTArray);
		initialize(wsaaOsTArray);
		initialize(wsaaSArray);
		initialize(wsaaIsSArray);
		initialize(wsaaOsSArray);
		initialize(wsaaPArray);
		initialize(wsaaIsPArray);
		initialize(wsaaOsPArray);
		initialize(wsaaLineFArray);
		initialize(wsaaLineKArray);
		for (wsaaSub.set(1); !(isGT(wsaaSub,8)); wsaaSub.add(1)){
			wsaaCntF[wsaaSub.toInt()].set(th601rec.lapday[wsaaSub.toInt()]);
		}
		wsaaX.set(1);
		for (wsaaSub.set(1); !(isGT(wsaaSub,8)); wsaaSub.add(1)){
			if (isLT(wsaaSub,6)) {
				wsaaDayMin[wsaaX.toInt()].set(wsaaCntF[wsaaSub.toInt()]);
				wsaaDayMax[wsaaX.toInt()].set(wsaaCntF[wsaaSub.toInt()]);
				wsaaX.add(1);
			}
			if (isEQ(wsaaSub,6)
			|| isEQ(wsaaSub,8)) {
				wsaaDayMin[wsaaX.toInt()].set(wsaaCntF[wsaaSub.toInt()]);
				wsaaDayMax[wsaaX.toInt()].set(99);
			}
			if (isEQ(wsaaSub,7)) {
				wsaaDayMax[wsaaX.toInt()].set(wsaaCntF[wsaaSub.toInt()]);
				wsaaX.add(1);
			}
		}
		wsaaDayMin[wsaaX.toInt()].add(1);
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			wsaaCntK[wsaaSub.toInt()].set(th601rec.weeks[wsaaSub.toInt()]);
		}
		wsaaX.set(1);
		wsaaWeeksMin[wsaaX.toInt()].set(1);
		compute(wsaaWeeksMax[wsaaX.toInt()], 0).set(mult(wsaaCntK[wsaaX.toInt()],7));
		for (wsaaX.set(1); !(isGT(wsaaX,5)); wsaaX.add(1)){
			compute(wsaaY, 0).set(add(wsaaX,1));
			compute(wsaaZ, 0).set(mult(wsaaX,2));
			compute(wsaaWeeksMin[wsaaY.toInt()], 0).set(mult(wsaaCntK[wsaaZ.toInt()],7));
			if (isEQ(wsaaZ,10)) {
				wsaaWeeksMin[wsaaY.toInt()].add(1);
				wsaaWeeksMax[wsaaY.toInt()].set(999);
			}
			else {
				wsaaZ.add(1);
				compute(wsaaWeeksMax[wsaaY.toInt()], 0).set(mult(wsaaCntK[wsaaZ.toInt()],7));
				wsaaWeeksMax[wsaaY.toInt()].add(1);
			}
		}
		wsaaRowFound.set(SPACES);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfTRows)
		|| rowFound.isTrue()); wsaaCnt.add(1)){
			if (isEQ(wsaaTRowId[wsaaCnt.toInt()],"A ")) {
				wsaaBfRowNo.set(wsaaCnt);
			}
			if (isEQ(wsaaTRowId[wsaaCnt.toInt()],"D ")) {
				wsaaCompRowNo.set(wsaaCnt);
			}
			if (isEQ(wsaaTRowId[wsaaCnt.toInt()],"E ")) {
				wsaaIssRowNo.set(wsaaCnt);
			}
			if (isEQ(wsaaTRowId[wsaaCnt.toInt()],"I ")) {
				wsaaCanRowNo.set(wsaaCnt);
			}
			if (isEQ(wsaaTRowId[wsaaCnt.toInt()],"J ")) {
				wsaaOsRowNo.set(wsaaCnt);
			}
			if (isGT(wsaaCompRowNo,0)
			&& isGT(wsaaIssRowNo,0)
			&& isGT(wsaaOsRowNo,0)
			&& isGT(wsaaCanRowNo,0)
			&& isGT(wsaaBfRowNo,0)) {
				rowFound.setTrue();
			}
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub,99)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		wsaaPrevDate.set(bsscIO.getEffectiveDate());
		wsaaHeaderMonth.set(wsaaCalendarMonth[wsaaMnth.toInt()]);
		if (isEQ(wsaaPrevMnth,1)) {
			compute(wsaaPrevYear, 0).set(sub(wsaaPrevYear,1));
			wsaaPrevMnth.set(12);
		}
		else {
			compute(wsaaPrevMnth, 0).set(sub(wsaaPrevMnth,1));
		}
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaSdate.set(datcon1rec.extDate);
		wsaaToday.set(datcon1rec.intDate);
		firstRecord.setTrue();
		sqlhttaCursor = " SELECT  CHDRCOY, CHDRNUM, CNTBRANCH, AGNTNUM, CNTTYPE, HROW, DAYSVRNCE" +
" FROM   " + appVars.getTableNameOverriden("HTTAPF") + " " +
" ORDER BY CHDRCOY, CNTBRANCH, CNTTYPE, HROW";
		sqlerrorflag = false;
		try {
			sqlhttaCursorconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.HttapfTableDAM());
			sqlhttaCursorps = appVars.prepareStatementEmbeded(sqlhttaCursorconn, sqlhttaCursor, "HTTAPF");
			sqlhttaCursorrs = appVars.executeQuery(sqlhttaCursorps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlhttaCursorrs.next()) {
				appVars.getDBObject(sqlhttaCursorrs, 1, wsaaSqlChdrcoy);
				appVars.getDBObject(sqlhttaCursorrs, 2, wsaaSqlChdrnum);
				appVars.getDBObject(sqlhttaCursorrs, 3, wsaaSqlCntbranch);
				appVars.getDBObject(sqlhttaCursorrs, 4, wsaaSqlAgntnum);
				appVars.getDBObject(sqlhttaCursorrs, 5, wsaaSqlCnttype);
				appVars.getDBObject(sqlhttaCursorrs, 6, wsaaSqlHrow);
				appVars.getDBObject(sqlhttaCursorrs, 7, wsaaSqlDaysvrnce);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		if (isLT(wsaaSqlDaysvrnce,0)) {
			goTo(GotoLabel.exit2090);
		}
		if (firstRecord.isTrue()) {
			wsaaPrevCompany.set(wsaaSqlChdrcoy);
			wsaaPrevCntbranch.set(wsaaSqlCntbranch);
			wsaaPrevCnttype.set(wsaaSqlCnttype);
			wsaaFirstRecord.set("N");
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		appVars.freeDBConnectionIgnoreErr(sqlhttaCursorconn, sqlhttaCursorps, sqlhttaCursorrs);
		if (isEQ(wsaaFirstRecord,"N")) {
			z100ObtainOtherRows();
			cttlForCo.setTrue();
			addSubttl5400();
			wsaaCnttype.set(wsaaPrevCnttype);
			header3300();
			printTotals3200();
			sttlForCo.setTrue();
			addTtl5500();
			header3300();
			printTotals3200();
			ttlForCo.setTrue();
			header3300();
			printTotals3200();
			dummy1.set(SPACES);
			printerFile.printRh602e01(printerRec);
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			update3010();
		}
		catch (GOTOException e){
		}
	}

protected void update3010()
	{
		if (isLT(wsaaSqlDaysvrnce,0)) {
			goTo(GotoLabel.exit3090);
		}
		if (isNE(wsaaSqlCnttype,wsaaPrevCnttype)
		|| isNE(wsaaSqlCntbranch,wsaaPrevCntbranch)) {
			z100ObtainOtherRows();
			cttlForCo.setTrue();
			addSubttl5400();
			wsaaCnttype.set(wsaaPrevCnttype);
			header3300();
			printTotals3200();
			wsaaPrevCnttype.set(wsaaSqlCnttype);
		}
		if (isNE(wsaaSqlCntbranch,wsaaPrevCntbranch)) {
			addTtl5500();
			sttlForCo.setTrue();
			header3300();
			printTotals3200();
			wsaaPrevCntbranch.set(wsaaSqlCntbranch);
		}
		if (isNE(wsaaSqlChdrcoy,wsaaPrevCompany)) {
			wsaaPrevCompany.set(wsaaSqlChdrcoy);
		}
		if (isEQ(wsaaSqlHrow,"B ")
		|| isEQ(wsaaSqlHrow,"C ")
		|| isEQ(wsaaSqlHrow,"I ")) {
			wsaaRowFound.set(SPACES);
			for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfTRows)
			|| rowFound.isTrue()); wsaaCnt.add(1)){
				if (isEQ(wsaaSqlHrow,wsaaTRowId[wsaaCnt.toInt()])) {
					rowFound.setTrue();
					wsaaCntP[wsaaCnt.toInt()].add(1);
					if (isNE(wsaaSqlHrow,"I ")) {
						wsaaCntP[wsaaCompRowNo.toInt()].add(1);
					}
				}
			}
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaSqlHrow,"G ")
		|| isEQ(wsaaSqlHrow,"H ")) {
			wsaaRowFound.set(SPACES);
			for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfIssueRows)
			|| rowFound.isTrue()); wsaaCnt.add(1)){
				if (isEQ(wsaaSqlHrow,wsaaIssueRowId[wsaaCnt.toInt()])) {
					rowFound.setTrue();
					z200DaysProcessed();
					wsaaCntP[wsaaIssRowNo.toInt()].add(1);
				}
			}
			goTo(GotoLabel.exit3090);
		}
		wsaaRowFound.set(SPACES);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfOsRows)
		|| rowFound.isTrue()); wsaaCnt.add(1)){
			if (isEQ(wsaaSqlHrow,wsaaOsRowId[wsaaCnt.toInt()])) {
				rowFound.setTrue();
				z300WeeksProcessed();
			}
		}
	}

protected void z100ObtainOtherRows()
	{
		try {
			z110OtherRows();
			z150WriteCfwd();
		}
		catch (GOTOException e){
		}
	}

protected void z110OtherRows()
	{
		hmtrIO.setParams(SPACES);
		hmtrIO.setChdrcoy(bsprIO.getCompany());
		hmtrIO.setCntbranch(wsaaPrevCntbranch);
		hmtrIO.setCnttype(wsaaPrevCnttype);
		hmtrIO.setYear(wsaaPrevYear);
		hmtrIO.setMnth(wsaaPrevMnth);
		hmtrIO.setFormat(hmtrrec);
		hmtrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hmtrIO);
		if (isNE(hmtrIO.getStatuz(),varcom.oK)
		&& isNE(hmtrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hmtrIO.getStatuz());
			syserrrec.params.set(hmtrIO.getParams());
			fatalError600();
		}
		if (isEQ(hmtrIO.getStatuz(),varcom.mrnf)) {
			wsaaCntP[wsaaBfRowNo.toInt()].set(0);
			wsaaAccumulator.set(0);
		}
		else {
			wsaaCntP[wsaaBfRowNo.toInt()].set(hmtrIO.getCntcount());
			wsaaAccumulator.set(hmtrIO.getCntcount());
		}
		wsaaCntP[wsaaCompRowNo.toInt()].add(wsaaAccumulator);
	}

protected void z150WriteCfwd()
	{
		hmtrIO.setYear(wsaaYear);
		hmtrIO.setMnth(wsaaMnth);
		hmtrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hmtrIO);
		if (isNE(hmtrIO.getStatuz(),varcom.oK)
		&& isNE(hmtrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hmtrIO.getStatuz());
			syserrrec.params.set(hmtrIO.getParams());
			fatalError600();
		}
		if (isEQ(hmtrIO.getStatuz(),varcom.mrnf)) {
			if (isEQ(wsaaCntP[wsaaOsRowNo.toInt()],0)) {
				goTo(GotoLabel.z190Exit);
			}
			hmtrIO.setCntcount(wsaaCntP[wsaaOsRowNo.toInt()]);
			hmtrIO.setUpdateDate(wsaaToday);
			hmtrIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hmtrIO);
			if (isNE(hmtrIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(hmtrIO.getStatuz());
				syserrrec.params.set(hmtrIO.getParams());
				fatalError600();
			}
		}
		else {
			hmtrIO.setFunction(varcom.updat);
			if (isLT(hmtrIO.getUpdateDate(),wsaaToday)) {
				if (isEQ(wsaaCntP[wsaaOsRowNo.toInt()],0)) {
					hmtrIO.setFunction(varcom.delet);
				}
				hmtrIO.setCntcount(wsaaCntP[wsaaOsRowNo.toInt()]);
				hmtrIO.setUpdateDate(wsaaToday);
			}
			SmartFileCode.execute(appVars, hmtrIO);
			if (isNE(hmtrIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(hmtrIO.getStatuz());
				syserrrec.params.set(hmtrIO.getParams());
				fatalError600();
			}
		}
	}

protected void z200DaysProcessed()
	{
		/*Z210-DAYS*/
		wsaaDurationFound.set(SPACES);
		for (wsaaY.set(1); !(isGT(wsaaY,7)
		|| durationFound.isTrue()); wsaaY.add(1)){
			if ((isGTE(wsaaSqlDaysvrnce,wsaaDayMin[wsaaY.toInt()]))
			&& (isLTE(wsaaSqlDaysvrnce,wsaaDayMax[wsaaY.toInt()]))) {
				durationFound.setTrue();
				wsaaIsCntP[wsaaCnt.toInt()][wsaaY.toInt()].add(1);
			}
		}
		/*Z290-EXIT*/
	}

protected void z300WeeksProcessed()
	{
		/*Z310-WEEKS*/
		wsaaDurationFound.set(SPACES);
		for (wsaaY.set(1); !(isGT(wsaaY,6)
		|| durationFound.isTrue()); wsaaY.add(1)){
			if ((isGTE(wsaaSqlDaysvrnce,wsaaWeeksMin[wsaaY.toInt()]))
			&& (isLTE(wsaaSqlDaysvrnce,wsaaWeeksMax[wsaaY.toInt()]))) {
				durationFound.setTrue();
				wsaaOsCntP[wsaaCnt.toInt()][wsaaY.toInt()].add(1);
				wsaaOsCntP[wsaaNoOfOsRows.toInt()][wsaaY.toInt()].add(1);
				wsaaCntP[wsaaOsRowNo.toInt()].add(1);
			}
		}
		/*Z390-EXIT*/
	}

protected void printTotals3200()
	{
		writeDetail3280();
	}

protected void writeDetail3280()
	{
		if (newPageReq.isTrue()) {
			printerFile.printRh602h01(printerRec);
			wsaaOverflow.set("N");
		}
		printerFile.printRh602u01(printerRec);
		dummy.set(SPACES);
		printerFile.printRh602h02(printerRec);
		printerFile.printRh602u01(printerRec);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaIssRowNo)); wsaaCnt.add(1)){
			if (isEQ(wsaaTtlForCo,"C")){
				moveDetail5600();
			}
			else if (isEQ(wsaaTtlForCo,"S")){
				moveSubttl5700();
			}
			else if (isEQ(wsaaTtlForCo,"T")){
				moveTtl5800();
			}
		}
		lapday01.set(wsaaCntF[1]);
		lapday02.set(wsaaCntF[2]);
		lapday03.set(wsaaCntF[3]);
		lapday04.set(wsaaCntF[4]);
		lapday05.set(wsaaCntF[5]);
		lapday06.set(wsaaCntF[6]);
		lapday07.set(wsaaCntF[7]);
		lapday08.set(wsaaCntF[8]);
		if (isGT(wsaaCntF[1],1)) {
			indOn.setTrue(71);
		}
		else {
			indOn.setTrue(72);
		}
		printerFile.printRh602h03(printerRec, indicArea);
		indOff.setTrue(71);
		indOff.setTrue(72);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfIssueRows)); wsaaCnt.add(1)){
			if (isEQ(wsaaTtlForCo,"C")){
				moveDetail5600a();
			}
			else if (isEQ(wsaaTtlForCo,"S")){
				moveSubttl5700a();
			}
			else if (isEQ(wsaaTtlForCo,"T")){
				moveTtl5800a();
			}
		}
		for (wsaaCnt.set(wsaaCanRowNo); !(isGT(wsaaCnt,wsaaOsRowNo)); wsaaCnt.add(1)){
			if (isEQ(wsaaTtlForCo,"C")){
				moveDetail5600();
			}
			else if (isEQ(wsaaTtlForCo,"S")){
				moveSubttl5700();
			}
			else if (isEQ(wsaaTtlForCo,"T")){
				moveTtl5800();
			}
		}
		weeks01.set(wsaaCntK[1]);
		weeks02.set(wsaaCntK[2]);
		weeks03.set(wsaaCntK[3]);
		weeks04.set(wsaaCntK[4]);
		weeks05.set(wsaaCntK[5]);
		weeks06.set(wsaaCntK[6]);
		weeks07.set(wsaaCntK[7]);
		weeks08.set(wsaaCntK[8]);
		weeks09.set(wsaaCntK[9]);
		weeks10.set(wsaaCntK[10]);
		if (isGT(wsaaCntK[1],1)) {
			indOn.setTrue(73);
		}
		else {
			indOn.setTrue(74);
		}
		printerFile.printRh602h04(printerRec, indicArea);
		printerFile.printRh602u01(printerRec);
		indOff.setTrue(73);
		indOff.setTrue(74);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfOsRows)); wsaaCnt.add(1)){
			if (isEQ(wsaaTtlForCo,"C")){
				moveDetail5600b();
			}
			else if (isEQ(wsaaTtlForCo,"S")){
				moveSubttl5700b();
			}
			else if (isEQ(wsaaTtlForCo,"T")){
				moveTtl5800b();
			}
		}
		if (isEQ(wsaaTtlForCo,"C")){
			initialize(wsaaPArray);
			initialize(wsaaIsPArray);
			initialize(wsaaOsPArray);
		}
		else if (isEQ(wsaaTtlForCo,"S")){
			initialize(wsaaSArray);
			initialize(wsaaIsSArray);
			initialize(wsaaOsSArray);
		}
		else if (isEQ(wsaaTtlForCo,"T")){
			initialize(wsaaTArray);
			initialize(wsaaIsTArray);
			initialize(wsaaOsTArray);
		}
		wsaaOverflow.set("Y");
	}

protected void header3300()
	{
		setUpHeadingCompany3320();
		setUpHeadingBranch3330();
	}

protected void setUpHeadingCompany3320()
	{
		wsaaCompany.set(wsaaPrevCompany);
		wsaaBranch.set(wsaaPrevCntbranch);
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		company.set(wsaaCompany);
		companynm.set(descIO.getLongdesc());
		forsdesc.set(wsaaHeaderMonth);
		sdate.set(wsaaSdate);
		year.set(wsaaYear);
	}

protected void setUpHeadingBranch3330()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsaaCompany);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(wsaaBranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (ttlForCo.isTrue()) {
			branch.set(SPACES);
			branchnm.set(wsaaBranchnm);
		}
		else {
			branch.set(wsaaBranch);
			branchnm.set(descIO.getLongdesc());
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsaaCompany);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(wsaaCnttype);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			cntdesc.set(wsaaUnknownCntdesc);
		}
		if (ttlForCo.isTrue()
		|| sttlForCo.isTrue()) {
			cnttype.set(wsaaAllCnttype);
			cntdesc.set(wsaaCntdesc);
		}
		else {
			cnttype.set(wsaaCnttype);
			cntdesc.set(descIO.getLongdesc());
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		wsaaQcmdexc.set(SPACES);
		wsaaQcmdexc.set("DLTOVR FILE(HTTAPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void addSubttl5400()
	{
		/*SUM*/
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfTRows)); wsaaCnt.add(1)){
			wsaaCntS[wsaaCnt.toInt()].add(wsaaCntP[wsaaCnt.toInt()]);
		}
		for (wsaaR.set(1); !(isGT(wsaaR,wsaaNoOfIssueRows)); wsaaR.add(1)){
			for (wsaaCnt.set(1); !(isGT(wsaaCnt,7)); wsaaCnt.add(1)){
				wsaaIsCntS[wsaaR.toInt()][wsaaCnt.toInt()].add(wsaaIsCntP[wsaaR.toInt()][wsaaCnt.toInt()]);
			}
		}
		for (wsaaR.set(1); !(isGT(wsaaR,wsaaNoOfOsRows)); wsaaR.add(1)){
			for (wsaaCnt.set(1); !(isGT(wsaaCnt,6)); wsaaCnt.add(1)){
				wsaaOsCntS[wsaaR.toInt()][wsaaCnt.toInt()].add(wsaaOsCntP[wsaaR.toInt()][wsaaCnt.toInt()]);
			}
		}
		/*EXIT*/
	}

protected void addTtl5500()
	{
		/*SUM*/
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfTRows)); wsaaCnt.add(1)){
			wsaaCntT[wsaaCnt.toInt()].add(wsaaCntS[wsaaCnt.toInt()]);
		}
		for (wsaaR.set(1); !(isGT(wsaaR,wsaaNoOfIssueRows)); wsaaR.add(1)){
			for (wsaaCnt.set(1); !(isGT(wsaaCnt,7)); wsaaCnt.add(1)){
				wsaaIsCntT[wsaaR.toInt()][wsaaCnt.toInt()].add(wsaaIsCntS[wsaaR.toInt()][wsaaCnt.toInt()]);
			}
		}
		for (wsaaR.set(1); !(isGT(wsaaR,wsaaNoOfOsRows)); wsaaR.add(1)){
			for (wsaaCnt.set(1); !(isGT(wsaaCnt,6)); wsaaCnt.add(1)){
				wsaaOsCntT[wsaaR.toInt()][wsaaCnt.toInt()].add(wsaaOsCntS[wsaaR.toInt()][wsaaCnt.toInt()]);
			}
		}
		/*EXIT*/
	}

protected void moveDetail5600()
	{
		/*DTL*/
		zpolcnt.set(wsaaCntP[wsaaCnt.toInt()]);
		wsaaTtlRowFound.set(SPACES);
		for (wsaaSub.set(1); !(ttlRowFound.isTrue()
		|| isGT(wsaaSub,wsaaNoOfTtlRows)); wsaaSub.add(1)){
			if (isEQ(wsaaTRowId[wsaaCnt.toInt()],wsaaTtlRowId[wsaaSub.toInt()])) {
				ttlRowFound.setTrue();
			}
		}
		wsaaR.set(wsaaCnt);
		printRow5900();
		/*EXIT*/
	}

protected void moveDetail5600a()
	{
		dtl5610a();
	}

protected void dtl5610a()
	{
		zpolcnt11.set(wsaaIsCntP[wsaaCnt.toInt()][1]);
		zpolcnt12.set(wsaaIsCntP[wsaaCnt.toInt()][2]);
		zpolcnt13.set(wsaaIsCntP[wsaaCnt.toInt()][3]);
		zpolcnt14.set(wsaaIsCntP[wsaaCnt.toInt()][4]);
		zpolcnt15.set(wsaaIsCntP[wsaaCnt.toInt()][5]);
		zpolcnt16.set(wsaaIsCntP[wsaaCnt.toInt()][6]);
		zpolcnt17.set(wsaaIsCntP[wsaaCnt.toInt()][7]);
		compute(wsaaX, 0).set(mult(wsaaCnt,12));
		indOn.setTrue(wsaaX);
		if (isEQ(wsaaCnt,1)) {
			printerFile.printRh602u01(printerRec);
		}
		printerFile.printRh602d02(printerRec, indicArea);
		printerFile.printRh602u01(printerRec);
		indOff.setTrue(wsaaX);
	}

protected void moveDetail5600b()
	{
		dtl5610b();
	}

protected void dtl5610b()
	{
		zpolcnt21.set(wsaaOsCntP[wsaaCnt.toInt()][1]);
		zpolcnt22.set(wsaaOsCntP[wsaaCnt.toInt()][2]);
		zpolcnt23.set(wsaaOsCntP[wsaaCnt.toInt()][3]);
		zpolcnt24.set(wsaaOsCntP[wsaaCnt.toInt()][4]);
		zpolcnt25.set(wsaaOsCntP[wsaaCnt.toInt()][5]);
		zpolcnt26.set(wsaaOsCntP[wsaaCnt.toInt()][6]);
		wsaaTtlRowFound.set(SPACES);
		for (wsaaSub.set(1); !(ttlRowFound.isTrue()
		|| isGT(wsaaSub,wsaaNoOfTtlRows)); wsaaSub.add(1)){
			if (isEQ(wsaaOsRowId[wsaaCnt.toInt()],wsaaTtlRowId[wsaaSub.toInt()])) {
				ttlRowFound.setTrue();
			}
		}
		compute(wsaaR, 0).set(mult((add(wsaaCnt,1)),7));
		printRow6000();
	}

protected void moveSubttl5700()
	{
		/*STL*/
		zpolcnt.set(wsaaCntS[wsaaCnt.toInt()]);
		wsaaTtlRowFound.set(SPACES);
		for (wsaaSub.set(1); !(ttlRowFound.isTrue()
		|| isGT(wsaaSub,wsaaNoOfTtlRows)); wsaaSub.add(1)){
			if (isEQ(wsaaTRowId[wsaaCnt.toInt()],wsaaTtlRowId[wsaaSub.toInt()])) {
				ttlRowFound.setTrue();
			}
		}
		wsaaR.set(wsaaCnt);
		printRow5900();
		/*EXIT*/
	}

protected void moveSubttl5700a()
	{
		subttl5710a();
	}

protected void subttl5710a()
	{
		zpolcnt11.set(wsaaIsCntS[wsaaCnt.toInt()][1]);
		zpolcnt12.set(wsaaIsCntS[wsaaCnt.toInt()][2]);
		zpolcnt13.set(wsaaIsCntS[wsaaCnt.toInt()][3]);
		zpolcnt14.set(wsaaIsCntS[wsaaCnt.toInt()][4]);
		zpolcnt15.set(wsaaIsCntS[wsaaCnt.toInt()][5]);
		zpolcnt16.set(wsaaIsCntS[wsaaCnt.toInt()][6]);
		zpolcnt17.set(wsaaIsCntS[wsaaCnt.toInt()][7]);
		compute(wsaaX, 0).set(mult(wsaaCnt,12));
		indOn.setTrue(wsaaX);
		if (isEQ(wsaaCnt,1)) {
			printerFile.printRh602u01(printerRec);
		}
		printerFile.printRh602d02(printerRec, indicArea);
		printerFile.printRh602u01(printerRec);
		indOff.setTrue(wsaaX);
	}

protected void moveSubttl5700b()
	{
		subttl5710b();
	}

protected void subttl5710b()
	{
		zpolcnt21.set(wsaaOsCntS[wsaaCnt.toInt()][1]);
		zpolcnt22.set(wsaaOsCntS[wsaaCnt.toInt()][2]);
		zpolcnt23.set(wsaaOsCntS[wsaaCnt.toInt()][3]);
		zpolcnt24.set(wsaaOsCntS[wsaaCnt.toInt()][4]);
		zpolcnt25.set(wsaaOsCntS[wsaaCnt.toInt()][5]);
		zpolcnt26.set(wsaaOsCntS[wsaaCnt.toInt()][6]);
		wsaaTtlRowFound.set(SPACES);
		for (wsaaSub.set(1); !(ttlRowFound.isTrue()
		|| isGT(wsaaSub,wsaaNoOfTtlRows)); wsaaSub.add(1)){
			if (isEQ(wsaaOsRowId[wsaaCnt.toInt()],wsaaTtlRowId[wsaaSub.toInt()])) {
				ttlRowFound.setTrue();
			}
		}
		compute(wsaaR, 0).set(mult((add(wsaaCnt,1)),7));
		printRow6000();
	}

protected void moveTtl5800()
	{
		/*TTL*/
		zpolcnt.set(wsaaCntT[wsaaCnt.toInt()]);
		wsaaTtlRowFound.set(SPACES);
		for (wsaaSub.set(1); !(ttlRowFound.isTrue()
		|| isGT(wsaaSub,wsaaNoOfTtlRows)); wsaaSub.add(1)){
			if (isEQ(wsaaTRowId[wsaaCnt.toInt()],wsaaTtlRowId[wsaaSub.toInt()])) {
				ttlRowFound.setTrue();
			}
		}
		wsaaR.set(wsaaCnt);
		printRow5900();
		/*EXIT*/
	}

protected void moveTtl5800a()
	{
		ttl5810a();
	}

protected void ttl5810a()
	{
		zpolcnt11.set(wsaaIsCntT[wsaaCnt.toInt()][1]);
		zpolcnt12.set(wsaaIsCntT[wsaaCnt.toInt()][2]);
		zpolcnt13.set(wsaaIsCntT[wsaaCnt.toInt()][3]);
		zpolcnt14.set(wsaaIsCntT[wsaaCnt.toInt()][4]);
		zpolcnt15.set(wsaaIsCntT[wsaaCnt.toInt()][5]);
		zpolcnt16.set(wsaaIsCntT[wsaaCnt.toInt()][6]);
		zpolcnt17.set(wsaaIsCntT[wsaaCnt.toInt()][7]);
		compute(wsaaX, 0).set(mult(wsaaCnt,12));
		indOn.setTrue(wsaaX);
		if (isEQ(wsaaCnt,1)) {
			printerFile.printRh602u01(printerRec);
		}
		printerFile.printRh602d02(printerRec, indicArea);
		printerFile.printRh602u01(printerRec);
		indOff.setTrue(wsaaX);
	}

protected void moveTtl5800b()
	{
		ttl5810b();
	}

protected void ttl5810b()
	{
		zpolcnt21.set(wsaaOsCntT[wsaaCnt.toInt()][1]);
		zpolcnt22.set(wsaaOsCntT[wsaaCnt.toInt()][2]);
		zpolcnt23.set(wsaaOsCntT[wsaaCnt.toInt()][3]);
		zpolcnt24.set(wsaaOsCntT[wsaaCnt.toInt()][4]);
		zpolcnt25.set(wsaaOsCntT[wsaaCnt.toInt()][5]);
		zpolcnt26.set(wsaaOsCntT[wsaaCnt.toInt()][6]);
		wsaaTtlRowFound.set(SPACES);
		for (wsaaSub.set(1); !(ttlRowFound.isTrue()
		|| isGT(wsaaSub,wsaaNoOfTtlRows)); wsaaSub.add(1)){
			if (isEQ(wsaaOsRowId[wsaaCnt.toInt()],wsaaTtlRowId[wsaaSub.toInt()])) {
				ttlRowFound.setTrue();
			}
		}
		compute(wsaaR, 0).set(mult((add(wsaaCnt,1)),7));
		printRow6000();
	}

protected void printRow5900()
	{
		/*ROW*/
		indOn.setTrue(wsaaR);
		if (ttlRowFound.isTrue()) {
			printerFile.printRh602u01(printerRec);
			printerFile.printRh602d01(printerRec, indicArea);
			printerFile.printRh602u01(printerRec);
		}
		else {
			printerFile.printRh602d01(printerRec, indicArea);
		}
		indOff.setTrue(wsaaR);
		/*EXIT*/
	}

protected void printRow6000()
	{
		/*ROW*/
		indOn.setTrue(wsaaR);
		if (ttlRowFound.isTrue()) {
			printerFile.printRh602u01(printerRec);
			printerFile.printRh602d03(printerRec, indicArea);
			printerFile.printRh602u01(printerRec);
		}
		else {
			printerFile.printRh602d03(printerRec, indicArea);
		}
		indOff.setTrue(wsaaR);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
