package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:01
 * Description:
 * Copybook name: INCTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Inctkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData inctFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData inctKey = new FixedLengthStringData(256).isAPartOf(inctFileKey, 0, REDEFINE);
  	public FixedLengthStringData inctChdrcoy = new FixedLengthStringData(1).isAPartOf(inctKey, 0);
  	public FixedLengthStringData inctChdrnum = new FixedLengthStringData(8).isAPartOf(inctKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(inctKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(inctFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		inctFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}