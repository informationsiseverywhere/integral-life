package com.csc.life.newbusiness.screens;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.util.QPUtilities;
import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.tablemodel.GeneralTable;

/**
 * Screen variables for Sr57j
 * @version 1.0 generated on 12/3/13 4:19 AM
 * @author CSC
 */
public class Sr57jScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(529);
	public FixedLengthStringData dataFields = new FixedLengthStringData(273).isAPartOf(dataArea, 0);
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,80);
	public ZonedDecimalData billcd = DD.billcd.copyToZonedDecimal().isAPartOf(dataFields,90);
	public FixedLengthStringData branchdesc = DD.branchdesc.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData ccmndref = DD.ccpomndref.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,133);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(dataFields,136);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,138);
	public FixedLengthStringData numsel = DD.numsel.copy().isAPartOf(dataFields,168);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,178);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,225);
	public FixedLengthStringData cdlongdesc = DD.longdesc.copy().isAPartOf(dataFields,233);
	public FixedLengthStringData mandstat = DD.mandstat.copy().isAPartOf(dataFields,263);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,265);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 273);
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData branchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ccmndrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData facthousErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData numselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData cdlongdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mandstatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 337);
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] branchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ccmndrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] facthousOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] numselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] cdlongdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mandstatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sr57jscreenWritten = new LongData(0);
	public LongData Sr57jwindowWritten = new LongData(0);
	public LongData Sr57jprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr57jScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ccmndrefOut,new String[] {"01","50","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bankacckeyOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {currcode, numsel, billcd, payrnum, payorname, ccmndref, facthous, longdesc, bankkey, bankdesc, branchdesc, bankacckey, bankaccdsc};
		screenOutFields = new BaseData[][] {currcodeOut, numselOut, billcdOut, payrnumOut, payornameOut, ccmndrefOut, facthousOut, longdescOut, bankkeyOut, bankdescOut, branchdescOut, bankacckeyOut, bankaccdscOut};
		screenErrFields = new BaseData[] {currcodeErr, numselErr, billcdErr, payrnumErr, payornameErr, ccmndrefErr, facthousErr, longdescErr, bankkeyErr, bankdescErr, branchdescErr, bankacckeyErr, bankaccdscErr};
		screenDateFields = new BaseData[] {billcd, effdate};
		screenDateErrFields = new BaseData[] {billcdErr, effdateErr};
		screenDateDispFields = new BaseData[] {billcdDisp, effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr57jscreen.class;
		protectRecord = Sr57jprotect.class;
	}

}
