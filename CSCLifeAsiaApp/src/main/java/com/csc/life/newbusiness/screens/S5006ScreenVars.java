package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import java.util.HashMap;
import java.util.Map;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5006
 * @version 1.0 generated on 30/08/09 06:29
 * @author Quipoz
 */
public class S5006ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(316);
	public FixedLengthStringData dataFields = new FixedLengthStringData(156).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,106);
	public FixedLengthStringData lifeorcov = DD.lifeorcov.copy().isAPartOf(dataFields,108);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,109);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(40).isAPartOf(dataArea, 156);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifeorcovErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 196);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifeorcovOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(122+101+8+24);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(40+101).isAPartOf(subfileArea, 0);
	public FixedLengthStringData ctable = DD.ctable.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData hrequired = DD.hrequired.copy().isAPartOf(subfileFields,4);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(subfileFields,5);
	public FixedLengthStringData rtable = DD.rtable.copy().isAPartOf(subfileFields,35);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,39);
	
	public FixedLengthStringData uworflag = DD.uworflag.copy().isAPartOf(subfileFields,39+1);
	public FixedLengthStringData uworreason = DD.uworreason.copy().isAPartOf(subfileFields,39+2);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(20+8).isAPartOf(subfileArea, 40+101);
	public FixedLengthStringData ctableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData hrequiredErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData rtableErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	
	public FixedLengthStringData uworflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16+4);
	public FixedLengthStringData uworreasonErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16+8);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(60+24).isAPartOf(subfileArea, 60+101+8);
	public FixedLengthStringData[] ctableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] hrequiredOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] rtableOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	
	public FixedLengthStringData[] uworflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48+12);
	public FixedLengthStringData[] uworreasonOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48+24);
	
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 120+101+8+24);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5006screensflWritten = new LongData(0);
	public LongData S5006screenctlWritten = new LongData(0);
	public LongData S5006screenWritten = new LongData(0);
	public LongData S5006protectWritten = new LongData(0);
	public GeneralTable s5006screensfl = new GeneralTable(AppVars.getInstance());
	
	private Map<String, Integer> uwResultMap = new HashMap<>();
	private Map<String, Integer> uwQuestionResultMap = new HashMap<>();
	public int uwFlag = 0;
	public int mainCovrDisFlag = -1;
	
	public Map<String, Integer> getUwResultMap() {
		return uwResultMap;
	}

	public void setUwResultMap(Map<String, Integer> uwResultMap) {
		this.uwResultMap = uwResultMap;
	}

	public Map<String, Integer> getUwQuestionResultMap() {
		return uwQuestionResultMap;
	}

	public void setUwQuestionResultMap(Map<String, Integer> uwQuestionResultMap) {
		this.uwQuestionResultMap = uwQuestionResultMap;
	}

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5006screensfl;
	}

	public S5006ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"02","01","-02",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {select, ctable, rtable, longdesc, hrequired,uworflag,uworreason};
		screenSflOutFields = new BaseData[][] {selectOut, ctableOut, rtableOut, longdescOut, hrequiredOut, uworflagOut, uworreasonOut};
		screenSflErrFields = new BaseData[] {selectErr, ctableErr, rtableErr, longdescErr, hrequiredErr,uworflagErr,uworreasonErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, life, lifcnum, linsname, jlife, jlifcnum, jlinsname, lifeorcov};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, lifeOut, lifcnumOut, linsnameOut, jlifeOut, jlifcnumOut, jlinsnameOut, lifeorcovOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, lifeErr, lifcnumErr, linsnameErr, jlifeErr, jlifcnumErr, jlinsnameErr, lifeorcovErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5006screen.class;
		screenSflRecord = S5006screensfl.class;
		screenCtlRecord = S5006screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5006protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5006screenctl.lrec.pageSubfile);
	}
}
