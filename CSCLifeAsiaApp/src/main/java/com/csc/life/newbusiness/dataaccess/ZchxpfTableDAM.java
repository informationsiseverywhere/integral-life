package com.csc.life.newbusiness.dataaccess;

import com.quipoz.framework.datatype.*;
import com.quipoz.framework.util.QPUtilities;
import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;


/**
 * 	
 * File: ZchxpfTableDAM.java
 * Date: Mon Jun 17 21:03:50 SGT 2013
 * Class transformed from ZCHXPF
 * Author: csc Limited
 *
 * Copyright (2012) CSC Asia, all rights reserved
 *
 */
public class ZchxpfTableDAM extends PFAdapterDAM {
	
	public int pfRecLen = 35;
	public FixedLengthStringData zchxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData zchxpfRecord = zchxrec;
	
	public FixedLengthStringData chdrpfx = DD.chdrpfx.copy().isAPartOf(zchxrec); 
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(zchxrec); 
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(zchxrec); 
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(zchxrec);
	public FixedLengthStringData pstcde = DD.pstcde.copy().isAPartOf(zchxrec);


	
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public ZchxpfTableDAM() {
			super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for ClntpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public ZchxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for ClntpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZchxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for ClntpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public ZchxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("ZCHXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
		QUALIFIEDCOLUMNS = 
								"CHDRPFX, " + 
								"CHDRCOY, " + 
								"CHDRNUM, " + 
								"STATCODE, "+
								"PSTCDE, "+
								"UNIQUE_NUMBER";
							
	}

	public void setColumns() {
		qualifiedColumns = new BaseData[] { 
								chdrpfx,
								chdrcoy, 
								chdrnum, 
								statcode,
								pstcde,
								unique_number  };

	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
		chdrpfx.clear(); 
		chdrcoy.clear(); 
		chdrnum.clear(); 
		statcode.clear();
		pstcde.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getChdzrec() {
  		return zchxrec;
	}

	public FixedLengthStringData getChdzpfRecord() {
  		return zchxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setChdzrec(what);
	}

	public void setChdzrec(Object what) {
  		this.zchxrec.set(what);
	}

	public void setChdzpfRecord(Object what) {
  		this.zchxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(zchxrec.getLength());
		result.set(zchxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}}
