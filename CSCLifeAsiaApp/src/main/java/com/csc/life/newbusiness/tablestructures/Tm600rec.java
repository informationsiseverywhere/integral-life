package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:49
 * Description:
 * Copybook name: TM600REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tm600rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tm600Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData genlcdex = new FixedLengthStringData(14).isAPartOf(tm600Rec, 0);
  	public FixedLengthStringData genlcdey = new FixedLengthStringData(14).isAPartOf(tm600Rec, 14);
  	public FixedLengthStringData genldesc = new FixedLengthStringData(30).isAPartOf(tm600Rec, 28);
  	public ZonedDecimalData occpct = new ZonedDecimalData(5, 2).isAPartOf(tm600Rec, 58);
  	public ZonedDecimalData statfrom = new ZonedDecimalData(15, 0).isAPartOf(tm600Rec, 63);
  	public ZonedDecimalData statto = new ZonedDecimalData(15, 0).isAPartOf(tm600Rec, 78);
  	public FixedLengthStringData ynflag = new FixedLengthStringData(1).isAPartOf(tm600Rec, 93);
  	public FixedLengthStringData filler = new FixedLengthStringData(406).isAPartOf(tm600Rec, 94, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tm600Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tm600Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}