package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:36
 * Description:
 * Copybook name: HIFXKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hifxkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hifxFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hifxKey = new FixedLengthStringData(64).isAPartOf(hifxFileKey, 0, REDEFINE);
  	public FixedLengthStringData hifxCompany = new FixedLengthStringData(1).isAPartOf(hifxKey, 0);
  	public PackedDecimalData hifxYear = new PackedDecimalData(4, 0).isAPartOf(hifxKey, 1);
  	public PackedDecimalData hifxMnth = new PackedDecimalData(2, 0).isAPartOf(hifxKey, 4);
  	public FixedLengthStringData hifxCntbranch = new FixedLengthStringData(2).isAPartOf(hifxKey, 6);
  	public FixedLengthStringData hifxCnttype = new FixedLengthStringData(3).isAPartOf(hifxKey, 8);
  	public FixedLengthStringData hifxCntcurr = new FixedLengthStringData(3).isAPartOf(hifxKey, 11);
  	public FixedLengthStringData hifxFieldType = new FixedLengthStringData(1).isAPartOf(hifxKey, 14);
  	public FixedLengthStringData filler = new FixedLengthStringData(49).isAPartOf(hifxKey, 15, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hifxFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hifxFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}