/*
 * File: Th552pt.java
 * Date: 30 August 2009 2:35:54
 * Author: Quipoz Limited
 * 
 * Class transformed from TH552PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.Th552rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH552.
*
*
*****************************************************************
* </pre>
*/
public class Th552pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Th552rec th552rec = new Th552rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th552pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th552rec.th552Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo005.set(th552rec.currcode);
		generalCopyLinesInner.fieldNo022.set(th552rec.zsuminfr01);
		generalCopyLinesInner.fieldNo023.set(th552rec.zsuminto01);
		generalCopyLinesInner.fieldNo024.set(th552rec.defFupMeth01);
		generalCopyLinesInner.fieldNo006.set(th552rec.ageIssageFrm01);
		generalCopyLinesInner.fieldNo007.set(th552rec.ageIssageTo01);
		generalCopyLinesInner.fieldNo008.set(th552rec.ageIssageFrm02);
		generalCopyLinesInner.fieldNo009.set(th552rec.ageIssageTo02);
		generalCopyLinesInner.fieldNo010.set(th552rec.ageIssageFrm03);
		generalCopyLinesInner.fieldNo011.set(th552rec.ageIssageTo03);
		generalCopyLinesInner.fieldNo012.set(th552rec.ageIssageFrm04);
		generalCopyLinesInner.fieldNo013.set(th552rec.ageIssageTo04);
		generalCopyLinesInner.fieldNo014.set(th552rec.ageIssageFrm05);
		generalCopyLinesInner.fieldNo015.set(th552rec.ageIssageTo05);
		generalCopyLinesInner.fieldNo016.set(th552rec.ageIssageFrm06);
		generalCopyLinesInner.fieldNo017.set(th552rec.ageIssageTo06);
		generalCopyLinesInner.fieldNo018.set(th552rec.ageIssageFrm07);
		generalCopyLinesInner.fieldNo019.set(th552rec.ageIssageTo07);
		generalCopyLinesInner.fieldNo020.set(th552rec.ageIssageFrm08);
		generalCopyLinesInner.fieldNo021.set(th552rec.ageIssageTo08);
		generalCopyLinesInner.fieldNo025.set(th552rec.defFupMeth02);
		generalCopyLinesInner.fieldNo026.set(th552rec.defFupMeth03);
		generalCopyLinesInner.fieldNo027.set(th552rec.defFupMeth04);
		generalCopyLinesInner.fieldNo028.set(th552rec.defFupMeth05);
		generalCopyLinesInner.fieldNo029.set(th552rec.defFupMeth06);
		generalCopyLinesInner.fieldNo030.set(th552rec.defFupMeth07);
		generalCopyLinesInner.fieldNo031.set(th552rec.defFupMeth08);
		generalCopyLinesInner.fieldNo032.set(th552rec.zsuminfr02);
		generalCopyLinesInner.fieldNo033.set(th552rec.zsuminto02);
		generalCopyLinesInner.fieldNo034.set(th552rec.defFupMeth09);
		generalCopyLinesInner.fieldNo035.set(th552rec.defFupMeth10);
		generalCopyLinesInner.fieldNo036.set(th552rec.defFupMeth11);
		generalCopyLinesInner.fieldNo037.set(th552rec.defFupMeth12);
		generalCopyLinesInner.fieldNo038.set(th552rec.defFupMeth13);
		generalCopyLinesInner.fieldNo039.set(th552rec.defFupMeth14);
		generalCopyLinesInner.fieldNo040.set(th552rec.defFupMeth15);
		generalCopyLinesInner.fieldNo041.set(th552rec.defFupMeth16);
		generalCopyLinesInner.fieldNo042.set(th552rec.zsuminfr03);
		generalCopyLinesInner.fieldNo043.set(th552rec.zsuminto03);
		generalCopyLinesInner.fieldNo044.set(th552rec.defFupMeth17);
		generalCopyLinesInner.fieldNo045.set(th552rec.defFupMeth18);
		generalCopyLinesInner.fieldNo046.set(th552rec.defFupMeth19);
		generalCopyLinesInner.fieldNo047.set(th552rec.defFupMeth20);
		generalCopyLinesInner.fieldNo048.set(th552rec.defFupMeth21);
		generalCopyLinesInner.fieldNo049.set(th552rec.defFupMeth22);
		generalCopyLinesInner.fieldNo050.set(th552rec.defFupMeth23);
		generalCopyLinesInner.fieldNo051.set(th552rec.defFupMeth24);
		generalCopyLinesInner.fieldNo052.set(th552rec.zsuminfr04);
		generalCopyLinesInner.fieldNo053.set(th552rec.zsuminto04);
		generalCopyLinesInner.fieldNo054.set(th552rec.defFupMeth25);
		generalCopyLinesInner.fieldNo055.set(th552rec.defFupMeth26);
		generalCopyLinesInner.fieldNo056.set(th552rec.defFupMeth27);
		generalCopyLinesInner.fieldNo057.set(th552rec.defFupMeth28);
		generalCopyLinesInner.fieldNo058.set(th552rec.defFupMeth29);
		generalCopyLinesInner.fieldNo059.set(th552rec.defFupMeth30);
		generalCopyLinesInner.fieldNo060.set(th552rec.defFupMeth31);
		generalCopyLinesInner.fieldNo061.set(th552rec.defFupMeth32);
		generalCopyLinesInner.fieldNo062.set(th552rec.zsuminfr05);
		generalCopyLinesInner.fieldNo063.set(th552rec.zsuminto05);
		generalCopyLinesInner.fieldNo064.set(th552rec.defFupMeth33);
		generalCopyLinesInner.fieldNo065.set(th552rec.defFupMeth34);
		generalCopyLinesInner.fieldNo066.set(th552rec.defFupMeth35);
		generalCopyLinesInner.fieldNo067.set(th552rec.defFupMeth36);
		generalCopyLinesInner.fieldNo068.set(th552rec.defFupMeth37);
		generalCopyLinesInner.fieldNo069.set(th552rec.defFupMeth38);
		generalCopyLinesInner.fieldNo070.set(th552rec.defFupMeth39);
		generalCopyLinesInner.fieldNo071.set(th552rec.defFupMeth40);
		generalCopyLinesInner.fieldNo072.set(th552rec.zsuminfr06);
		generalCopyLinesInner.fieldNo073.set(th552rec.zsuminto06);
		generalCopyLinesInner.fieldNo074.set(th552rec.defFupMeth41);
		generalCopyLinesInner.fieldNo075.set(th552rec.defFupMeth42);
		generalCopyLinesInner.fieldNo076.set(th552rec.defFupMeth43);
		generalCopyLinesInner.fieldNo077.set(th552rec.defFupMeth44);
		generalCopyLinesInner.fieldNo078.set(th552rec.defFupMeth45);
		generalCopyLinesInner.fieldNo079.set(th552rec.defFupMeth46);
		generalCopyLinesInner.fieldNo080.set(th552rec.defFupMeth47);
		generalCopyLinesInner.fieldNo081.set(th552rec.defFupMeth48);
		generalCopyLinesInner.fieldNo082.set(th552rec.zsuminfr07);
		generalCopyLinesInner.fieldNo083.set(th552rec.zsuminto07);
		generalCopyLinesInner.fieldNo084.set(th552rec.defFupMeth49);
		generalCopyLinesInner.fieldNo085.set(th552rec.defFupMeth50);
		generalCopyLinesInner.fieldNo086.set(th552rec.defFupMeth51);
		generalCopyLinesInner.fieldNo087.set(th552rec.defFupMeth52);
		generalCopyLinesInner.fieldNo088.set(th552rec.defFupMeth53);
		generalCopyLinesInner.fieldNo089.set(th552rec.defFupMeth54);
		generalCopyLinesInner.fieldNo090.set(th552rec.defFupMeth55);
		generalCopyLinesInner.fieldNo091.set(th552rec.defFupMeth56);
		generalCopyLinesInner.fieldNo092.set(th552rec.zsuminfr08);
		generalCopyLinesInner.fieldNo093.set(th552rec.zsuminto08);
		generalCopyLinesInner.fieldNo094.set(th552rec.defFupMeth57);
		generalCopyLinesInner.fieldNo095.set(th552rec.defFupMeth58);
		generalCopyLinesInner.fieldNo096.set(th552rec.defFupMeth59);
		generalCopyLinesInner.fieldNo097.set(th552rec.defFupMeth60);
		generalCopyLinesInner.fieldNo099.set(th552rec.defFupMeth62);
		generalCopyLinesInner.fieldNo100.set(th552rec.defFupMeth63);
		generalCopyLinesInner.fieldNo101.set(th552rec.defFupMeth64);
		generalCopyLinesInner.fieldNo098.set(th552rec.defFupMeth61);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(47).isAPartOf(wsaaPrtLine001, 29, FILLER).init("Medical Underwriting Requirement          SH552");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(16);
	private FixedLengthStringData filler7 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Currency:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine003, 13);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 19, FILLER).init("Age");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(79);
	private FixedLengthStringData filler10 = new FixedLengthStringData(79).isAPartOf(wsaaPrtLine005, 0, FILLER).init("Sum Assured       F---T   F---T   F---T   F---T   F---T   F---T   F---T   F---T");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(80);
	private FixedLengthStringData filler11 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine006, 0, FILLER).init("From     To");
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 17).setPattern("ZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 21).setPattern("ZZZ");
	private FixedLengthStringData filler13 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 25).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 29).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 33).setPattern("ZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 37).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 41).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 45).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 49).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 53).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 61).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 65).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 69).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 73).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(79);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 0).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine007, 9).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 17, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 19);
	private FixedLengthStringData filler29 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 27);
	private FixedLengthStringData filler30 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 35);
	private FixedLengthStringData filler31 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 43);
	private FixedLengthStringData filler32 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 51);
	private FixedLengthStringData filler33 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 59);
	private FixedLengthStringData filler34 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 67);
	private FixedLengthStringData filler35 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 75);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(79);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 0).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine008, 9).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 17, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 19);
	private FixedLengthStringData filler38 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 27);
	private FixedLengthStringData filler39 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 35);
	private FixedLengthStringData filler40 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 43);
	private FixedLengthStringData filler41 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 51);
	private FixedLengthStringData filler42 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 59);
	private FixedLengthStringData filler43 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 67);
	private FixedLengthStringData filler44 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 75);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(79);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 0).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine009, 9).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 17, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 19);
	private FixedLengthStringData filler47 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 27);
	private FixedLengthStringData filler48 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 35);
	private FixedLengthStringData filler49 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 43);
	private FixedLengthStringData filler50 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 51);
	private FixedLengthStringData filler51 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 59);
	private FixedLengthStringData filler52 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 67);
	private FixedLengthStringData filler53 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 75);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(79);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 0).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine010, 9).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 17, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 19);
	private FixedLengthStringData filler56 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 27);
	private FixedLengthStringData filler57 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 35);
	private FixedLengthStringData filler58 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 43);
	private FixedLengthStringData filler59 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 51);
	private FixedLengthStringData filler60 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 59);
	private FixedLengthStringData filler61 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 67);
	private FixedLengthStringData filler62 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 75);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(79);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 0).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine011, 9).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 17, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 19);
	private FixedLengthStringData filler65 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 27);
	private FixedLengthStringData filler66 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 35);
	private FixedLengthStringData filler67 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 43);
	private FixedLengthStringData filler68 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 51);
	private FixedLengthStringData filler69 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 59);
	private FixedLengthStringData filler70 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 67);
	private FixedLengthStringData filler71 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 75);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(79);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 0).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine012, 9).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 17, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 19);
	private FixedLengthStringData filler74 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 27);
	private FixedLengthStringData filler75 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 35);
	private FixedLengthStringData filler76 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 43);
	private FixedLengthStringData filler77 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 51);
	private FixedLengthStringData filler78 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 59);
	private FixedLengthStringData filler79 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 67);
	private FixedLengthStringData filler80 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 75);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(79);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 0).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine013, 9).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 17, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 19);
	private FixedLengthStringData filler83 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 27);
	private FixedLengthStringData filler84 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 35);
	private FixedLengthStringData filler85 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 43);
	private FixedLengthStringData filler86 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 51);
	private FixedLengthStringData filler87 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 59);
	private FixedLengthStringData filler88 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 67);
	private FixedLengthStringData filler89 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 75);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(79);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 0).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 8, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(8, 0).isAPartOf(wsaaPrtLine014, 9).setPattern("ZZZZZZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 17, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 19);
	private FixedLengthStringData filler92 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 27);
	private FixedLengthStringData filler93 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo096 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 35);
	private FixedLengthStringData filler94 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo097 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 43);
	private FixedLengthStringData filler95 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo098 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 51);
	private FixedLengthStringData filler96 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo099 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 59);
	private FixedLengthStringData filler97 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 63, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo100 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 67);
	private FixedLengthStringData filler98 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo101 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 75);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(28);
	private FixedLengthStringData filler99 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
}
}
