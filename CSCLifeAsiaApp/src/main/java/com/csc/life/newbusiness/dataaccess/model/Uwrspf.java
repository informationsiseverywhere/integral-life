package com.csc.life.newbusiness.dataaccess.model;

public class Uwrspf {
    private long uniqueNumber; 
	private String chdrcoy;
	private String chdrnum;
	private String crtable;
	private String reason;
	private int tranno;
	private String trancde;
	private String termid;
	private int trdt;
	private int trtm;
	private int user;
	

	public Uwrspf() {
		super();
	}

	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public String getTrancde() {
		return trancde;
	}

	public void setTrancde(String trancde) {
		this.trancde = trancde;
	}

	public String getTermid() {
		return termid;
	}

	public void setTermid(String termid) {
		this.termid = termid;
	}

	public int getTrdt() {
		return trdt;
	}

	public void setTrdt(int trdt) {
		this.trdt = trdt;
	}

	public int getTrtm() {
		return trtm;
	}

	public void setTrtm(int trtm) {
		this.trtm = trtm;
	}

	public int getUser() {
		return user;
	}

	public void setUser(int user) {
		this.user = user;
	}
}