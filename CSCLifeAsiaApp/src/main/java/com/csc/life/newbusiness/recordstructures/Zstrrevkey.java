package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:26
 * Description:
 * Copybook name: ZSTRREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zstrrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zstrrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zstrrevKey = new FixedLengthStringData(64).isAPartOf(zstrrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData zstrrevChdrcoy = new FixedLengthStringData(1).isAPartOf(zstrrevKey, 0);
  	public FixedLengthStringData zstrrevChdrnum = new FixedLengthStringData(8).isAPartOf(zstrrevKey, 1);
  	public PackedDecimalData zstrrevTranno = new PackedDecimalData(5, 0).isAPartOf(zstrrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(zstrrevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zstrrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zstrrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}