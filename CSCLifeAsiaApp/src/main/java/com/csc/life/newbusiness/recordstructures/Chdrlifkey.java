package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:20
 * Description:
 * Copybook name: CHDRLIFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrlifkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrlifFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData chdrlifKey = new FixedLengthStringData(256).isAPartOf(chdrlifFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrlifChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrlifKey, 0);
  	public FixedLengthStringData chdrlifChdrnum = new FixedLengthStringData(8).isAPartOf(chdrlifKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(chdrlifKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrlifFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrlifFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}