/*
 * File: Ph506.java
 * Date: 30 August 2009 1:04:17
 * Author: Quipoz Limited
 * 
 * Class transformed from PH506.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.procedures.Th506pt;
import com.csc.life.newbusiness.screens.Sh506ScreenVars;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
***********************************************************************
* </pre>
*/
public class Ph506 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH506");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected Th506rec th506rec = getTh506rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh506ScreenVars sv = getLScreenVars(); //getScreenVars( Sh506ScreenVars.class);
	private boolean btchpr01Permission = false; //BTCHPR01

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Ph506() {
		super();
		screenVars = sv;
		new ScreenModel("Sh506", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setParams(SPACES);
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
		//smalchi2
				btchpr01Permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "BTCHPR01", appVars, "IT"); //BTCHPR01
				if(btchpr01Permission)
				{
					sv.btchpr01Flag.set("Y");
				}
				else
				{
					sv.btchpr01Flag.set("N");
				}
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		th506rec.cnt.set(ZERO);
		th506rec.expdays.set(ZERO);
		th506rec.nofbbisf.set(ZERO);
		th506rec.nofbbwrn.set(ZERO);
		th506rec.yearInforce.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.branchs.set(th506rec.branchs);
		sv.cflg.set(th506rec.cflg);
		sv.cnt.set(th506rec.cnt);
		sv.confirm.set(th506rec.confirm);
		sv.crtable.set(th506rec.crtable);
		sv.expdays.set(th506rec.expdays);
		sv.freqcy.set(th506rec.freqcy);
		sv.ind.set(th506rec.ind);
		sv.indic.set(th506rec.indic);
		sv.mandatorys.set(th506rec.mandatorys);
		sv.nofbbisf.set(th506rec.nofbbisf);
		sv.nofbbwrn.set(th506rec.nofbbwrn);
		sv.znfopts.set(th506rec.znfopts);
		sv.zzsrce.set(th506rec.zzsrce);
		sv.calcmeth.set(th506rec.calcmeth);
		sv.yearInforce.set(th506rec.yearInforce);
		//TMLII-294 AC-01-007
		sv.zdmsion.set(th506rec.zdmsion);
		sv.fatcaFlag.set(th506rec.fatcaFlag);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(th506rec.th506Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.branchs,th506rec.branchs)) {
			th506rec.branchs.set(sv.branchs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.cflg,th506rec.cflg)) {
			th506rec.cflg.set(sv.cflg);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.cnt,th506rec.cnt)) {
			th506rec.cnt.set(sv.cnt);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.confirm,th506rec.confirm)) {
			th506rec.confirm.set(sv.confirm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.crtable,th506rec.crtable)) {
			th506rec.crtable.set(sv.crtable);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.expdays,th506rec.expdays)) {
			th506rec.expdays.set(sv.expdays);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.freqcy,th506rec.freqcy)) {
			th506rec.freqcy.set(sv.freqcy);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ind,th506rec.ind)) {
			th506rec.ind.set(sv.ind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.indic,th506rec.indic)) {
			th506rec.indic.set(sv.indic);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.mandatorys,th506rec.mandatorys)) {
			th506rec.mandatorys.set(sv.mandatorys);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.nofbbisf,th506rec.nofbbisf)) {
			th506rec.nofbbisf.set(sv.nofbbisf);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.nofbbwrn,th506rec.nofbbwrn)) {
			th506rec.nofbbwrn.set(sv.nofbbwrn);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.znfopts,th506rec.znfopts)) {
			th506rec.znfopts.set(sv.znfopts);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zzsrce,th506rec.zzsrce)) {
			th506rec.zzsrce.set(sv.zzsrce);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.calcmeth,th506rec.calcmeth)) {
			th506rec.calcmeth.set(sv.calcmeth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.yearInforce,th506rec.yearInforce)) {
			th506rec.yearInforce.set(sv.yearInforce);
			wsaaUpdateFlag = "Y";
		}
		//TMLII-294 AC-01-007 START
		if (isNE(sv.zdmsion,th506rec.zdmsion)) {
			th506rec.zdmsion.set(sv.zdmsion);
			wsaaUpdateFlag = "Y";
		}
		//TMLII-294 AC-01-007 END
		
		if (isNE(sv.fatcaFlag,th506rec.fatcaFlag)) {
			th506rec.fatcaFlag.set(sv.fatcaFlag);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

public Th506rec getTh506rec() {
	return new Th506rec();
} 

protected Sh506ScreenVars getLScreenVars() {
	return ScreenProgram.getScreenVars(Sh506ScreenVars.class);
}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Th506pt.class, wsaaTablistrec);
		/*EXIT*/
	}

}
