/*
 * File: T5705pt.java
 * Date: 30 August 2009 2:26:49
 * Author: Quipoz Limited
 * 
 * Class transformed from T5705PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.T5705rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5705.
*
*
*
***********************************************************************
* </pre>
*/
public class T5705pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine001, 22, FILLER).init("Fast Track - Contract/Life Defaults              S5705");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(60);
	private FixedLengthStringData filler9 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Contract Currency:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 23);
	private FixedLengthStringData filler10 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine004, 26, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine004, 37, FILLER).init("Billing Currency:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 57);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(59);
	private FixedLengthStringData filler12 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine005, 0, FILLER).init("  Contract Register:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 23);
	private FixedLengthStringData filler13 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine005, 26, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine005, 37, FILLER).init("Servicing Branch:");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 57);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(59);
	private FixedLengthStringData filler15 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine006, 0, FILLER).init("  Source of Business:");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 23);
	private FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler17 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine006, 37, FILLER).init("Source of Income:");
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(63);
	private FixedLengthStringData filler18 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  Number of Policies:");
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 23).setPattern("ZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine007, 37, FILLER).init("Campaign Number:");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 57);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(58);
	private FixedLengthStringData filler21 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine008, 0, FILLER).init("  Billing Frequency:");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 23);
	private FixedLengthStringData filler22 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine008, 37, FILLER).init("Method of Payment:");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 57);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(67);
	private FixedLengthStringData filler24 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine009, 0, FILLER).init("  Risk Commence Date:");
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 23);
	private FixedLengthStringData filler25 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine009, 33, FILLER).init("    Default Agent:");
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 57);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(57);
	private FixedLengthStringData filler26 = new FixedLengthStringData(32).isAPartOf(wsaaPrtLine010, 0, FILLER).init("  Cross Reference Type/Number:");
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 32);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 34, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(20).isAPartOf(wsaaPrtLine010, 37);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(49);
	private FixedLengthStringData filler28 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine011, 0, FILLER).init("  Statistical Codes:");
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 22);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 25, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 28);
	private FixedLengthStringData filler30 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 34);
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 40);
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 46);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(60);
	private FixedLengthStringData filler33 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler34 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine012, 28, FILLER).init("Medical Evidence:");
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 56);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(57);
	private FixedLengthStringData filler35 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine013, 0, FILLER).init("  Smoking:");
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 18);
	private FixedLengthStringData filler36 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 19, FILLER).init(SPACES);
	private FixedLengthStringData filler37 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine013, 28, FILLER).init("Mortality Class Indicator:");
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 56);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(33);
	private FixedLengthStringData filler38 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine014, 0, FILLER).init("  Pursuit Codes:");
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 18);
	private FixedLengthStringData filler39 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 27);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5705rec t5705rec = new T5705rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5705pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5705rec.t5705Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t5705rec.cntcurr);
		fieldNo008.set(t5705rec.billcurr);
		fieldNo009.set(t5705rec.register);
		fieldNo010.set(t5705rec.cntbranch);
		fieldNo011.set(t5705rec.srcebus);
		fieldNo012.set(t5705rec.incomeSeqNo);
		fieldNo013.set(t5705rec.plansuff);
		fieldNo014.set(t5705rec.campaign);
		fieldNo015.set(t5705rec.billfreq);
		fieldNo016.set(t5705rec.mop);
		datcon1rec.intDate.set(t5705rec.rcdate);
		callDatcon1100();
		fieldNo017.set(datcon1rec.extDate);
		fieldNo018.set(t5705rec.agntsel);
		fieldNo019.set(t5705rec.reptype);
		fieldNo020.set(t5705rec.lrepnum);
		fieldNo021.set(t5705rec.stcal);
		fieldNo022.set(t5705rec.stcbl);
		fieldNo023.set(t5705rec.stccl);
		fieldNo024.set(t5705rec.stcdl);
		fieldNo025.set(t5705rec.stcel);
		fieldNo026.set(t5705rec.selection);
		fieldNo027.set(t5705rec.smkrqd);
		fieldNo028.set(t5705rec.mortality);
		fieldNo029.set(t5705rec.pursuit01);
		fieldNo030.set(t5705rec.pursuit02);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
