package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:41
 * Description:
 * Copybook name: MBNSARCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mbnsarckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mbnsarcFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData mbnsarcKey = new FixedLengthStringData(256).isAPartOf(mbnsarcFileKey, 0, REDEFINE);
  	public FixedLengthStringData mbnsarcChdrcoy = new FixedLengthStringData(1).isAPartOf(mbnsarcKey, 0);
  	public FixedLengthStringData mbnsarcChdrnum = new FixedLengthStringData(8).isAPartOf(mbnsarcKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(mbnsarcKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mbnsarcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mbnsarcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}