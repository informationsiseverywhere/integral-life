package com.csc.life.newbusiness.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:05:02
 * Description:
 * Copybook name: ISUALLREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Isuallrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData isuallRec = new FixedLengthStringData(105);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(isuallRec, 0);
  	public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(isuallRec, 4);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(isuallRec, 5);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(isuallRec, 13);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(isuallRec, 15);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(isuallRec, 17);
  	public PackedDecimalData planSuffix = new PackedDecimalData(4, 0).isAPartOf(isuallRec, 19);
  	public PackedDecimalData freqFactor = new PackedDecimalData(11, 5).isAPartOf(isuallRec, 22);
  	public FixedLengthStringData batchkey = new FixedLengthStringData(19).isAPartOf(isuallRec, 28);
  	public FixedLengthStringData contkey = new FixedLengthStringData(14).isAPartOf(batchkey, 0);
  	public FixedLengthStringData batcpfx = new FixedLengthStringData(2).isAPartOf(contkey, 0);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(contkey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(contkey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(contkey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(contkey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(contkey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batchkey, 14);
  	public PackedDecimalData transactionDate = new PackedDecimalData(6, 0).isAPartOf(isuallRec, 47);
  	public PackedDecimalData transactionTime = new PackedDecimalData(6, 0).isAPartOf(isuallRec, 51);
  	public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(isuallRec, 55);
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(isuallRec, 59);
  	public FixedLengthStringData convertUnlt = new FixedLengthStringData(1).isAPartOf(isuallRec, 63);
  	public PackedDecimalData covrInstprem = new PackedDecimalData(17, 2).isAPartOf(isuallRec, 64);
  	public PackedDecimalData covrSingp = new PackedDecimalData(17, 2).isAPartOf(isuallRec, 73);
  	public PackedDecimalData newTranno = new PackedDecimalData(5, 0).isAPartOf(isuallRec, 82);
  	public PackedDecimalData effdate = new PackedDecimalData(8, 0).isAPartOf(isuallRec, 85);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(isuallRec, 90);
  	public FixedLengthStringData oldcovr = new FixedLengthStringData(2).isAPartOf(isuallRec, 95);
  	public FixedLengthStringData oldrider = new FixedLengthStringData(2).isAPartOf(isuallRec, 97);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(isuallRec, 99);
  	public PackedDecimalData runDate = new PackedDecimalData(8, 0).isAPartOf(isuallRec, 100);


	public void initialize() {
		COBOLFunctions.initialize(isuallRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		isuallRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}