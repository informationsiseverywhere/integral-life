package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH557
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh557ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(336);
	public FixedLengthStringData dataFields = new FixedLengthStringData(224).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData zflpxtras = new FixedLengthStringData(180).isAPartOf(dataFields, 44);
	public FixedLengthStringData[] zflpxtra = FLSArrayPartOfStructure(3, 60, zflpxtras, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(180).isAPartOf(zflpxtras, 0, FILLER_REDEFINE);
	public FixedLengthStringData zflpxtra01 = DD.zflpxtra.copy().isAPartOf(filler,0);
	public FixedLengthStringData zflpxtra02 = DD.zflpxtra.copy().isAPartOf(filler,60);
	public FixedLengthStringData zflpxtra03 = DD.zflpxtra.copy().isAPartOf(filler,120);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 224);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData zflpxtrasErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] zflpxtraErr = FLSArrayPartOfStructure(3, 4, zflpxtrasErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(zflpxtrasErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zflpxtra01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData zflpxtra02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData zflpxtra03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 252);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData zflpxtrasOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] zflpxtraOut = FLSArrayPartOfStructure(3, 12, zflpxtrasOut, 0);
	public FixedLengthStringData[][] zflpxtraO = FLSDArrayPartOfArrayStructure(12, 1, zflpxtraOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(zflpxtrasOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zflpxtra01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] zflpxtra02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] zflpxtra03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh557screenWritten = new LongData(0);
	public LongData Sh557protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh557ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, zflpxtra01, zflpxtra02, zflpxtra03};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, zflpxtra01Out, zflpxtra02Out, zflpxtra03Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, zflpxtra01Err, zflpxtra02Err, zflpxtra03Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh557screen.class;
		protectRecord = Sh557protect.class;
	}

}
