package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SR52N
 * @version 1.0 generated on 12/3/13 4:18 AM
 * @author CSC
 */
public class Sr52nScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(43);
	public FixedLengthStringData dataFields = new FixedLengthStringData(11).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 11);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 19);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr52nscreenWritten = new LongData(0);
	public LongData Sr52nprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr52nScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {chdrsel, action};
		screenOutFields = new BaseData[][] {chdrselOut, actionOut};
		screenErrFields = new BaseData[] {chdrselErr, actionErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = Sr52nscreen.class;
		protectRecord = Sr52nprotect.class;
	}

}
