package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S5007
 * @version 1.0 generated on 30/08/09 06:29
 * @author Quipoz
 */
public class S5007ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(263);
	public FixedLengthStringData dataFields = new FixedLengthStringData(151).isAPartOf(dataArea, 0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData sellagent = DD.sellagent.copy().isAPartOf(dataFields,143);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 151);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData sellagentErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 179);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] sellagentOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(70);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(20).isAPartOf(subfileArea, 0);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData splitBpts = DD.splitb.copyToZonedDecimal().isAPartOf(subfileFields,10);
	public ZonedDecimalData splitBcomm = DD.splitc.copyToZonedDecimal().isAPartOf(subfileFields,15);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(12).isAPartOf(subfileArea, 20);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData splitbErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData splitcErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(36).isAPartOf(subfileArea, 32);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] splitbOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] splitcOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 68);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();


	public LongData S5007screensflWritten = new LongData(0);
	public LongData S5007screenctlWritten = new LongData(0);
	public LongData S5007screenWritten = new LongData(0);
	public LongData S5007protectWritten = new LongData(0);
	public GeneralTable s5007screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s5007screensfl;
	}

	public S5007ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(agntselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(splitcOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(splitbOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {agntsel, splitBcomm, splitBpts};
		screenSflOutFields = new BaseData[][] {agntselOut, splitcOut, splitbOut};
		screenSflErrFields = new BaseData[] {agntselErr, splitcErr, splitbErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername, sellagent, agentname};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, sellagentOut, agentnameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, sellagentErr, agentnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S5007screen.class;
		screenSflRecord = S5007screensfl.class;
		screenCtlRecord = S5007screenctl.class;
		initialiseSubfileArea();
		protectRecord = S5007protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S5007screenctl.lrec.pageSubfile);
	}
}
