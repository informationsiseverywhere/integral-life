package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HttapfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:36
 * Class transformed from HTTAPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HttapfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 27;
	public FixedLengthStringData httarec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData httapfRecord = httarec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(httarec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(httarec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(httarec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(httarec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(httarec);
	public FixedLengthStringData hrow = DD.hrow.copy().isAPartOf(httarec);
	public PackedDecimalData daysvrnce = DD.daysvrnce.copy().isAPartOf(httarec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HttapfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for HttapfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HttapfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HttapfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HttapfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HttapfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HttapfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HTTAPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"CNTBRANCH, " +
							"AGNTNUM, " +
							"CNTTYPE, " +
							"HROW, " +
							"DAYSVRNCE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     cntbranch,
                                     agntnum,
                                     cnttype,
                                     hrow,
                                     daysvrnce,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		cntbranch.clear();
  		agntnum.clear();
  		cnttype.clear();
  		hrow.clear();
  		daysvrnce.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHttarec() {
  		return httarec;
	}

	public FixedLengthStringData getHttapfRecord() {
  		return httapfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHttarec(what);
	}

	public void setHttarec(Object what) {
  		this.httarec.set(what);
	}

	public void setHttapfRecord(Object what) {
  		this.httapfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(httarec.getLength());
		result.set(httarec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}