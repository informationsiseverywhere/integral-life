package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5004screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	//public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 20, 21, 22, 23, 24,38,33}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5004ScreenVars sv = (S5004ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.S5004screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5004ScreenVars screenVars = (S5004ScreenVars)pv;
		/*screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.heading.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.statdsc.setClassString("");
		screenVars.premStatDesc.setClassString("");
		screenVars.ownersel.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.conprosal.setClassString("");
		screenVars.hpropdteDisp.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.indxflg.setClassString("");
		screenVars.hprrcvdtDisp.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.incomeSeqNo.setClassString("");
		screenVars.huwdcdteDisp.setClassString("");
		screenVars.billcdDisp.setClassString("");
		screenVars.plansuff.setClassString("");
		screenVars.hoissdteDisp.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.billcurr.setClassString("");
		screenVars.znfopt.setClassString("");
		screenVars.register.setClassString("");
		screenVars.srcebus.setClassString("");
		screenVars.dlvrmode.setClassString("");
		screenVars.reptype.setClassString("");
		screenVars.lrepnum.setClassString("");
		screenVars.cntbranch.setClassString("");
		screenVars.sbrdesc.setClassString("");
		screenVars.agntsel.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.campaign.setClassString("");
		screenVars.stcal.setClassString("");
		screenVars.stcbl.setClassString("");
		screenVars.stccl.setClassString("");
		screenVars.stcdl.setClassString("");
		screenVars.stcel.setClassString("");
		screenVars.jownind.setClassString("");
		screenVars.asgnind.setClassString("");
		screenVars.apcind.setClassString("");
		screenVars.addtype.setClassString("");
		screenVars.payind.setClassString("");
		screenVars.comind.setClassString("");
		screenVars.crcind.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.fupflg.setClassString("");
		screenVars.bnfying.setClassString("");
		screenVars.grpind.setClassString("");
		screenVars.aiind.setClassString("");
		screenVars.zsredtrm.setClassString("");
		screenVars.ttmprcno.setClassString("");
		screenVars.ttmprcdteDisp.setClassString("");
		screenVars.transhist.setClassString("");
		screenVars.ctrsind.setClassString("");
		screenVars.ind.setClassString("");
		//ILIFE-2472 Starts
		screenVars.reqntype.setClassString("");
		screenVars.zdpind.setClassString("");
		screenVars.zdcind.setClassString("");
		screenVars.fatcastatusdesc.setClassString("");
		screenVars.fatcastatus.setClassString("");
		//ILIFE-2472 Ends
		screenVars.zctaxind.setClassString("");//ALS-313
		screenVars.schmno.setClassString("");//ALS-318
		screenVars.schmnme.setClassString("");//ALS-318
		screenVars.billday.setClassString("");//ILIFE-3735
		screenVars.zroloverind.setClassString("");//ALS-700
		screenVars.nlgflg.setClassString("");//ILIFE-3997
		screenVars.refundOverpay.setClassString("");
		//ICIL-147 start
        screenVars.bnkout.setClassString("");
		screenVars.bnksm.setClassString("");
		screenVars.bnktel.setClassString("");
		screenVars.bnkoutname.setClassString("");
		screenVars.bnksmname.setClassString("");
		screenVars.bnktelname.setClassString("");
		screenVars.zmultOwner.setClassString("");
*/		//ICIL-147 end
		

		ScreenRecord.setClassStringFormatting(pv);
	}

/**
 * Clear all the variables in S5004screen
 */
	public static void clear(VarModel pv) {
		S5004ScreenVars screenVars = (S5004ScreenVars) pv;
		/*screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.heading.clear();
		screenVars.chdrnum.clear();
		screenVars.statdsc.clear();
		screenVars.premStatDesc.clear();
		screenVars.ownersel.clear();
		screenVars.ownername.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.conprosal.clear();
		screenVars.hpropdteDisp.clear();
		screenVars.hpropdte.clear();
		screenVars.billfreq.clear();
		screenVars.indxflg.clear();
		screenVars.hprrcvdtDisp.clear();
		screenVars.hprrcvdt.clear();
		screenVars.mop.clear();
		screenVars.incomeSeqNo.clear();
		screenVars.huwdcdteDisp.clear();
		screenVars.huwdcdte.clear();
		screenVars.billcdDisp.clear();
		screenVars.billcd.clear();
		screenVars.plansuff.clear();
		screenVars.hoissdteDisp.clear();
		screenVars.hoissdte.clear();
		screenVars.cntcurr.clear();
		screenVars.billcurr.clear();
		screenVars.znfopt.clear();
		screenVars.register.clear();
		screenVars.srcebus.clear();
		screenVars.dlvrmode.clear();
		screenVars.reptype.clear();
		screenVars.lrepnum.clear();
		screenVars.cntbranch.clear();
		screenVars.sbrdesc.clear();
		screenVars.agntsel.clear();
		screenVars.agentname.clear();
		screenVars.campaign.clear();
		screenVars.stcal.clear();
		screenVars.stcbl.clear();
		screenVars.stccl.clear();
		screenVars.stcdl.clear();
		screenVars.stcel.clear();
		screenVars.jownind.clear();
		screenVars.asgnind.clear();
		screenVars.apcind.clear();
		screenVars.addtype.clear();
		screenVars.payind.clear();
		screenVars.comind.clear();
		screenVars.crcind.clear();
		screenVars.ddind.clear();
		screenVars.fupflg.clear();
		screenVars.bnfying.clear();
		screenVars.grpind.clear();
		screenVars.aiind.clear();
		screenVars.zsredtrm.clear();
		screenVars.ttmprcno.clear();
		screenVars.ttmprcdteDisp.clear();
		screenVars.ttmprcdte.clear();
		screenVars.transhist.clear();
		screenVars.ctrsind.clear();
		screenVars.ind.clear();
		//ILIFE-2472 Starts
		screenVars.reqntype.clear();
		screenVars.zdpind.clear();
		screenVars.zdcind.clear();
		//ILIFE-2472 Ends
		screenVars.fatcastatusdesc.clear();
		screenVars.fatcastatus.clear();
		screenVars.zctaxind.clear();//ALS-313
		screenVars.schmno.clear();//ALS-318
		screenVars.schmnme.clear();//ALS-318
		screenVars.billday.clear();//ILIFE-3735
		screenVars.zroloverind.clear();//ALS-700
		screenVars.nlgflg.clear();//ILIFE-3997
		screenVars.refundOverpay.clear();
		//ICIL-147 start
	    screenVars.bnkout.clear();
		screenVars.bnksm.clear();
		screenVars.bnktel.clear();
		screenVars.bnkoutname.clear();
		screenVars.bnksmname.clear();
		screenVars.bnktelname.clear();
		screenVars.zmultOwner.clear();//ALS-4555*/
		//ICIL-147 end
		ScreenRecord.clear(pv);
	}
}
