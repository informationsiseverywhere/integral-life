package com.csc.life.newbusiness.dataaccess.dao;

import java.sql.SQLException;

import com.csc.life.newbusiness.dataaccess.model.Rlvrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface RlvrpfDAO extends BaseDAO<Rlvrpf>{
	public void insertRecord(Rlvrpf rlvrpf);
	public Rlvrpf readRecord(String chdrcoy,String chdrnum);
	public boolean updateRecord(Rlvrpf rlvrpf);
	public void deleteRecord(Rlvrpf rlvrpf) ;//ILIFE-4003
	//ILIFE-8123-starts 
	public Rlvrpf checkRecord(String chdrcoy,String chdrnum,String life,String coverage);
	public boolean updateRlvrpf(Rlvrpf rlvrpf);
	boolean updateRollovRecord(Rlvrpf rlvrpf);
	//ILIFE-8123-ends
}
