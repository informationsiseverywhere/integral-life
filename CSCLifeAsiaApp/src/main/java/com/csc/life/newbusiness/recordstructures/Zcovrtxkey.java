package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:17
 * Description:
 * Copybook name: ZCOVRTXKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zcovrtxkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zcovrtxFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zcovrtxKey = new FixedLengthStringData(64).isAPartOf(zcovrtxFileKey, 0, REDEFINE);
  	public FixedLengthStringData zcovrtxChdrcoy = new FixedLengthStringData(1).isAPartOf(zcovrtxKey, 0);
  	public FixedLengthStringData zcovrtxChdrnum = new FixedLengthStringData(8).isAPartOf(zcovrtxKey, 1);
  	public PackedDecimalData zcovrtxTranno = new PackedDecimalData(5, 0).isAPartOf(zcovrtxKey, 9);
  	public FixedLengthStringData zcovrtxLife = new FixedLengthStringData(2).isAPartOf(zcovrtxKey, 12);
  	public FixedLengthStringData zcovrtxCoverage = new FixedLengthStringData(2).isAPartOf(zcovrtxKey, 14);
  	public FixedLengthStringData zcovrtxRider = new FixedLengthStringData(2).isAPartOf(zcovrtxKey, 16);
  	public PackedDecimalData zcovrtxPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(zcovrtxKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(zcovrtxKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zcovrtxFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zcovrtxFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}