package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH563.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh563Report extends SMARTReportLayout { 

	private FixedLengthStringData agntnum = new FixedLengthStringData(8);
	private ZonedDecimalData amhii01 = new ZonedDecimalData(13, 2);
	private ZonedDecimalData amhii02 = new ZonedDecimalData(13, 2);
	private ZonedDecimalData amhii03 = new ZonedDecimalData(13, 2);
	private FixedLengthStringData aracde = new FixedLengthStringData(3);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22);
	private FixedLengthStringData dummy = new FixedLengthStringData(1);
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData prcnt01 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData prcnt02 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData prcnt03 = new ZonedDecimalData(5, 2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData znofpol01 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData znofpol02 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData znofpol03 = new ZonedDecimalData(5, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh563Report() {
		super();
	}


	/**
	 * Print the XML for Rh563d01
	 */
	public void printRh563d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		cntdesc.setFieldName("cntdesc");
		cntdesc.setInternal(subString(recordData, 1, 30));
		amhii01.setFieldName("amhii01");
		amhii01.setInternal(subString(recordData, 31, 13));
		prcnt01.setFieldName("prcnt01");
		prcnt01.setInternal(subString(recordData, 44, 5));
		znofpol01.setFieldName("znofpol01");
		znofpol01.setInternal(subString(recordData, 49, 5));
		amhii02.setFieldName("amhii02");
		amhii02.setInternal(subString(recordData, 54, 13));
		prcnt02.setFieldName("prcnt02");
		prcnt02.setInternal(subString(recordData, 67, 5));
		znofpol02.setFieldName("znofpol02");
		znofpol02.setInternal(subString(recordData, 72, 5));
		amhii03.setFieldName("amhii03");
		amhii03.setInternal(subString(recordData, 77, 13));
		prcnt03.setFieldName("prcnt03");
		prcnt03.setInternal(subString(recordData, 90, 5));
		znofpol03.setFieldName("znofpol03");
		znofpol03.setInternal(subString(recordData, 95, 5));
		printLayout("Rh563d01",			// Record name
			new BaseData[]{			// Fields:
				cntdesc,
				amhii01,
				prcnt01,
				znofpol01,
				amhii02,
				prcnt02,
				znofpol02,
				amhii03,
				prcnt03,
				znofpol03
			}
		);

	}

	/**
	 * Print the XML for Rh563e01
	 */
	public void printRh563e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("Rh563e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh563f01
	 */
	public void printRh563f01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dummy.setFieldName("dummy");
		dummy.setInternal(subString(recordData, 1, 1));
		printLayout("Rh563f01",			// Record name
			new BaseData[]{			// Fields:
				dummy
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for Rh563h01
	 */
	public void printRh563h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datetexc.setFieldName("datetexc");
		datetexc.setInternal(subString(recordData, 1, 22));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 23, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 24, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 54, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 64, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 66, 30));
		time.setFieldName("time");
		time.set(getTime());
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 96, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 99, 30));
		aracde.setFieldName("aracde");
		aracde.setInternal(subString(recordData, 129, 3));
		agntnum.setFieldName("agntnum");
		agntnum.setInternal(subString(recordData, 132, 8));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rh563h01",			// Record name
			new BaseData[]{			// Fields:
				datetexc,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				currcode,
				currencynm,
				aracde,
				agntnum,
				pagnbr
			}
		);

		currentPrintLine.set(8);
	}

	/**
	 * Print the XML for Rh563h02
	 */
	public void printRh563h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dummy.setFieldName("dummy");
		dummy.setInternal(subString(recordData, 1, 1));
		printLayout("Rh563h02",			// Record name
			new BaseData[]{			// Fields:
				dummy
			}
		);

	}

	/**
	 * Print the XML for Rh563h03
	 */
	public void printRh563h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dummy.setFieldName("dummy");
		dummy.setInternal(subString(recordData, 1, 1));
		printLayout("Rh563h03",			// Record name
			new BaseData[]{			// Fields:
				dummy
			}
		);

	}

	/**
	 * Print the XML for Rh563h04
	 */
	public void printRh563h04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(22).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 1, 10));
		printLayout("Rh563h04",			// Record name
			new BaseData[]{			// Fields:
				effdate
			}
			, new Object[] {			// indicators
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind12", indicArea.charAt(12)},
				new Object[]{"ind13", indicArea.charAt(13)},
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind15", indicArea.charAt(15)},
				new Object[]{"ind16", indicArea.charAt(16)},
				new Object[]{"ind17", indicArea.charAt(17)},
				new Object[]{"ind18", indicArea.charAt(18)},
				new Object[]{"ind19", indicArea.charAt(19)},
				new Object[]{"ind20", indicArea.charAt(20)},
				new Object[]{"ind21", indicArea.charAt(21)},
				new Object[]{"ind22", indicArea.charAt(22)}
			}
		);

		currentPrintLine.add(3);
	}

	/**
	 * Print the XML for Rh563t01
	 */
	public void printRh563t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		amhii01.setFieldName("amhii01");
		amhii01.setInternal(subString(recordData, 1, 13));
		prcnt01.setFieldName("prcnt01");
		prcnt01.setInternal(subString(recordData, 14, 5));
		znofpol01.setFieldName("znofpol01");
		znofpol01.setInternal(subString(recordData, 19, 5));
		amhii02.setFieldName("amhii02");
		amhii02.setInternal(subString(recordData, 24, 13));
		prcnt02.setFieldName("prcnt02");
		prcnt02.setInternal(subString(recordData, 37, 5));
		znofpol02.setFieldName("znofpol02");
		znofpol02.setInternal(subString(recordData, 42, 5));
		amhii03.setFieldName("amhii03");
		amhii03.setInternal(subString(recordData, 47, 13));
		prcnt03.setFieldName("prcnt03");
		prcnt03.setInternal(subString(recordData, 60, 5));
		znofpol03.setFieldName("znofpol03");
		znofpol03.setInternal(subString(recordData, 65, 5));
		printLayout("Rh563t01",			// Record name
			new BaseData[]{			// Fields:
				amhii01,
				prcnt01,
				znofpol01,
				amhii02,
				prcnt02,
				znofpol02,
				amhii03,
				prcnt03,
				znofpol03
			}
		);

		currentPrintLine.add(2);
	}


}
