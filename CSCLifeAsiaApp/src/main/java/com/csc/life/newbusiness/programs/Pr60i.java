
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.screens.Sr60iScreenVars;
import com.csc.life.newbusiness.tablestructures.Tr60irec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


public class Pr60i extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR60I");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	private Tr60irec tr60irec = new Tr60irec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr60iScreenVars sv = ScreenProgram.getScreenVars( Sr60iScreenVars.class);
	private static final String e315 = "E315";
	private static final String rfxh = "RFXH";
	private static final String rfxi = "RFXI";

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		exit2090, 
		other3080, 
		exit3090
	}

	public Pr60i() {
		super();
		screenVars = sv;
		new ScreenModel("Sr60i", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		tr60irec.tr60iRec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC     **/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		tr60irec.growth.set(SPACE);
		tr60irec.stepup.set(SPACE);
		tr60irec.higherGrowth.set(SPACE);
	}

protected void generalArea1045()
	{
	    sv.growth.set(tr60irec.growth);
	    sv.stepup.set(tr60irec.stepup);
	    sv.higherGrowth.set(tr60irec.higherGrowth);
	    
	    if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		/*CONFIRMATION-FIELDS*/
		/*OTHER*/
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
		/**     RETRIEVE SCREEN FIELDS AND EDIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
				case exit2090: 
					exit2090();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
			wsspcomn.edterror.set(varcom.oK);
			/*VALIDATE*/
			if (isEQ(wsspcomn.flag, "I")) {
				goTo(GotoLabel.exit2090);
			}
			if (isNE(sv.growth,'Y')
			&& (isNE(sv.growth,'N'))){
				sv.growthErr.set(e315);
			}
			if (isNE(sv.stepup,'Y')
			&& (isNE(sv.stepup,'N'))){
				sv.stepupErr.set(e315);
			}
			if (isNE(sv.higherGrowth,'Y')
			&& (isNE(sv.higherGrowth,'N'))){
				sv.higherGrowthErr.set(e315);
			}
			
			if (isEQ(sv.growth,'N')
			&&(isEQ(sv.stepup,'N')
			&&(isEQ(sv.higherGrowth,'N')))){
				sv.growthErr.set(rfxh);
				sv.stepupErr.set(rfxh);
				sv.higherGrowthErr.set(rfxh);
			}
			else{
				if (isEQ(sv.growth,'Y')){
					if (isEQ(sv.stepup,'Y')){
						sv.growthErr.set(rfxi);
						sv.stepupErr.set(rfxi);
					}
					if (isEQ(sv.higherGrowth,'Y')){
						sv.growthErr.set(rfxi);
						sv.higherGrowthErr.set(rfxi);
					}
					if (isEQ(sv.stepup,'Y')
					&&(isEQ(sv.higherGrowth,'Y'))){
						sv.growthErr.set(rfxi);
						sv.stepupErr.set(rfxi);
						sv.higherGrowthErr.set(rfxi);
					}
				}
				else{
					if (isEQ(sv.stepup,'Y')
					&&(isEQ(sv.higherGrowth,'Y'))){
						sv.stepupErr.set(rfxi);
						sv.higherGrowthErr.set(rfxi);
					}
				}
						
			}	

	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
		/**     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(tr60irec.tr60iRec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		/*CHECK*/
		if (isNE(sv.growth, tr60irec.growth)) {
			tr60irec.growth.set(sv.growth);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.stepup, tr60irec.stepup)) {
			tr60irec.stepup.set(sv.stepup);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.higherGrowth, tr60irec.higherGrowth)) {
			tr60irec.higherGrowth.set(sv.higherGrowth);
			wsaaUpdateFlag = "Y";
		}
		
		/*EXIT*/
		/**     DECIDE WHICH TRANSACTION PROGRAM IS NEXT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
		/**     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT*/
		/**      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.*/
	}

}
