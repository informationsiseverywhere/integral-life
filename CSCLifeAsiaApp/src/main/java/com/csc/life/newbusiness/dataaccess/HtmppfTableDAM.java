package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HtmppfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:36
 * Class transformed from HTMPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HtmppfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 59;
	public FixedLengthStringData htmprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData htmppfRecord = htmprec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(htmprec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(htmprec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(htmprec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(htmprec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(htmprec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(htmprec);
	public PackedDecimalData occdate = DD.occdate.copy().isAPartOf(htmprec);
	public FixedLengthStringData hrow = DD.hrow.copy().isAPartOf(htmprec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(htmprec);
	public PackedDecimalData annprem = DD.annprem.copy().isAPartOf(htmprec);
	public PackedDecimalData singp = DD.singp.copy().isAPartOf(htmprec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HtmppfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for HtmppfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HtmppfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HtmppfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HtmppfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HtmppfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HtmppfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HTMPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"CNTBRANCH, " +
							"AGNTNUM, " +
							"CNTTYPE, " +
							"CNTCURR, " +
							"OCCDATE, " +
							"HROW, " +
							"SUMINS, " +
							"ANNPREM, " +
							"SINGP, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     cntbranch,
                                     agntnum,
                                     cnttype,
                                     cntcurr,
                                     occdate,
                                     hrow,
                                     sumins,
                                     annprem,
                                     singp,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		cntbranch.clear();
  		agntnum.clear();
  		cnttype.clear();
  		cntcurr.clear();
  		occdate.clear();
  		hrow.clear();
  		sumins.clear();
  		annprem.clear();
  		singp.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHtmprec() {
  		return htmprec;
	}

	public FixedLengthStringData getHtmppfRecord() {
  		return htmppfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHtmprec(what);
	}

	public void setHtmprec(Object what) {
  		this.htmprec.set(what);
	}

	public void setHtmppfRecord(Object what) {
  		this.htmppfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(htmprec.getLength());
		result.set(htmprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}