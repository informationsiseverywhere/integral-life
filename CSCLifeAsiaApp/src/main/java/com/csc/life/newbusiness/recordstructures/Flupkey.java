package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:40
 * Description:
 * Copybook name: FLUPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Flupkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData flupFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData flupKey = new FixedLengthStringData(256).isAPartOf(flupFileKey, 0, REDEFINE);
  	public FixedLengthStringData flupChdrcoy = new FixedLengthStringData(1).isAPartOf(flupKey, 0);
  	public FixedLengthStringData flupChdrnum = new FixedLengthStringData(8).isAPartOf(flupKey, 1);
  	public PackedDecimalData flupFupno = new PackedDecimalData(2, 0).isAPartOf(flupKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(flupKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(flupFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		flupFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}