/******************************************************************************
 * File Name 		: Ctrspf.java
 * Author			: nloganathan5
 * Creation Date	: 28 October 2016
 * Project			: Integral Life
 * Description		: The Model Class for CTRSPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.model;

import java.io.Serializable;
import java.util.Date;

public class Ctrspf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "CTRSPF";

	//member variables for columns
	private long uniqueNumber;
	private Character chdrcoy;
	private String chdrnum;
	private Integer seqno;
	private String clntnum;
	private Integer datefrm;
	private Integer dateto;
	private String reasoncd;
	private String usrprf;
	private String jobnm;
	private Date datime;

	// Constructor
	public Ctrspf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public Character getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public Integer getSeqno(){
		return this.seqno;
	}
	public String getClntnum(){
		return this.clntnum;
	}
	public Integer getDatefrm(){
		return this.datefrm;
	}
	public Integer getDateto(){
		return this.dateto;
	}
	public String getReasoncd(){
		return this.reasoncd;
	}
	public String getUsrprf(){
		return this.usrprf;
	}
	public String getJobnm(){
		return this.jobnm;
	}
	public Date getDatime(){
		return new Date(this.datime.getTime());//IJTI-316
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( Character chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setSeqno( Integer seqno ){
		 this.seqno = seqno;
	}
	public void setClntnum( String clntnum ){
		 this.clntnum = clntnum;
	}
	public void setDatefrm( Integer datefrm ){
		 this.datefrm = datefrm;
	}
	public void setDateto( Integer dateto ){
		 this.dateto = dateto;
	}
	public void setReasoncd( String reasoncd ){
		 this.reasoncd = reasoncd;
	}
	public void setUsrprf( String usrprf ){
		 this.usrprf = usrprf;
	}
	public void setJobnm( String jobnm ){
		 this.jobnm = jobnm;
	}
	public void setDatime( Date datime ){
		this.datime = new Date(datime.getTime());//IJTI-314
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("SEQNO:		");
		output.append(getSeqno());
		output.append("\r\n");
		output.append("CLNTNUM:		");
		output.append(getClntnum());
		output.append("\r\n");
		output.append("DATEFRM:		");
		output.append(getDatefrm());
		output.append("\r\n");
		output.append("DATETO:		");
		output.append(getDateto());
		output.append("\r\n");
		output.append("REASONCD:		");
		output.append(getReasoncd());
		output.append("\r\n");
		output.append("USRPRF:		");
		output.append(getUsrprf());
		output.append("\r\n");
		output.append("JOBNM:		");
		output.append(getJobnm());
		output.append("\r\n");
		output.append("DATIME:		");
		output.append(getDatime());
		output.append("\r\n");

		return output.toString();
	}

}
