/*
 * File: P5123.java
 * Date: 30 August 2009 0:09:29
 * Author: Quipoz Limited
 * 
 * Class transformed from P5123.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.csc.dip.jvpms.runtime.base.IVpmsBaseSession;
import com.csc.dip.jvpms.runtime.base.IVpmsBaseSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsComputeResult;
import com.csc.dip.jvpms.runtime.base.VpmsJniSessionFactory;
import com.csc.dip.jvpms.runtime.base.VpmsLoadFailedException;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.procedures.Chkrlmt;
import com.csc.life.enquiries.recordstructures.Chkrlrec;
import com.csc.life.general.dataaccess.PovrTableDAM;
import com.csc.life.general.tablestructures.T6005rec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovttrmTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.newbusiness.recordstructures.Rlrdtrmrec;
import com.csc.life.newbusiness.screens.S5123ScreenVars;
import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.life.productdefinition.dataaccess.MbnsTableDAM;
import com.csc.life.productdefinition.dataaccess.MinsTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MbnspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MrtapfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Mbnspf;
import com.csc.life.productdefinition.dataaccess.model.Mrtapf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5585rec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6598rec;
import com.csc.life.productdefinition.tablestructures.Ta610rec;
import com.csc.life.productdefinition.tablestructures.Th615rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.terminationclaims.dataaccess.dao.CovppfDAO;
import com.csc.life.terminationclaims.dataaccess.model.Covppf;
import com.csc.life.underwriting.dataaccess.UndcTableDAM;
import com.csc.life.underwriting.tablestructures.T6768rec;
import com.csc.life.underwriting.tablestructures.Tr675rec;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppConfig;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*      STANDARD TERM LIFE COVERAGE
*
* This screen/program P5123 is used to capture the coverage and
* rider details for traditional generic components.
*
* Initialise
* ----------
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* Read CHDRLNB (RETRV)  in  order to obtain the contract header
* information.  If  the  number of policies in the plan is zero
* or  one, then Plan-processing does not apply. If there is any
* other  numeric  value,  this  value  indicates  the number of
* policies in the Plan.
*
* If Plan processing is  not  to be performed, then protect and
* non-display the following three fields:-
*
* a) No. of Policies in Plan (default to 1)
* b) No. Available (default to 1)
* c) No. Applicable (default to 1)
*
* and processing for Variations will not occur.
*
* The key  for Coverage/rider to be worked on will be available
* from  COVTLNB.  This is obtained by using the RETRV function.
* Check if there are any transactions by doing a BEGN  (to read
* the Coverage/rider summary) using the COVTTRM logical view.
*
* The COVTTRM key will be:-
*
*  Company, Contract-no, Life, Coverage-no, rider-no, sequence-no
*
* If  Plan  processing is to occur, then calculate the "credit"
* as follows:
*
*  - subtract  the  'No  of  policies in the Plan' from the
*       first  COVTTRM  record  read  above (if no COVTTRM,
*       this is zero) from the 'No of policies in the Plan'
*       (from the Contract-Header).
*
*  - a positive  credit means that additional policies must
*       be added to the plan.
*
*  - a negative  credit  means  that  some policies must be
*       removed from the plan.
*
* If this is a single policy plan
*    if it is the first time (no COVTTRM records)
*     or there is no credit
*   - plan processing is not required.
*
* Set  the  number of  policies  available  to  the  number  of
* policies on the plan.  If  COVTTRM  records are to be written
* for the first time  (as determined above), default the number
* applicable to the number available.
*
* Read  the  contract  definition  details  from  T5688 for the
* contract  type  held  on  CHDRLNB. Access the version of this
* item for the original commencement date of the risk.
*
* Read  the  general  coverage/rider details from T5687 and the
* traditional/term  edit rules from T5608 for the coverage type
* held  on  COVTLNB.  Access the version of these items for the
* original commencement date of the risk.
*
* Read the  tolerance  limit  from  T5667  (key  as for T5608).
* Although this  is  a dated table,  just  read  the latest one
* (using ITEM).
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*  - read  the life details using LIFELNB (life number from
*       COVTLNB, joint life number '00').  Look up the name
*       from the  client  details  (CLTS)  and  format as a
*       "confirmation name".
*
*  - read the joint life details using LIFELNB (life number
*       from COVTLNB,  joint  life number '01').  If found,
*       look up the name from the client details (CLTS) and
*       format as a "confirmation name".
*
* To  determine  which premium calculation method to use decide
* whether or  not it is a Single or Joint-life case (IF it is a
* joint  life  case,  the  joint  life record was found above).
* Then:
*
*  - if the benefit billing method is not blank, non-display
*       and protect the premium field (so do not bother with
*       the rest of this).
*
*  - if it  is  a  single-life case, use  the single-method
*       from  T5687. The age to be used for validation will
*       be the age of the main life.
*
*  - if the joint-life indicator (from T5687) is blank, and
*       if  it  is  a Joint-life case, use the joint-method
*       from  T5687. The age to be used for validation will
*       be the age of the main life.
*
*  - if the  Joint-life  indicator  is  'N',  then  use the
*       Single-method.  But, if there is a joint-life (this
*       must be  a  rider  to have got this far) prompt for
*       the joint  life  indicator  to determine which life
*       the rider is to attach to.  In all other cases, the
*       joint life  indicator  should  be non-displayed and
*       protected.  The  age to be used for validation will
*       be the age  of the main or joint life, depending on
*       the one selected.
*
*  - use the  premium-method  selected  from  T5687, if not
*       blank,  to access T5675.  This gives the subroutine
*       to use for the calculation.
*
* COVERAGE/RIDER DETAILS
*
* The fields to  be displayed on the screen are determined from
* the entry in table T5608 read earlier as follows:
*
*  - if the  maximum  and  minimum  sum assured amounts are
*       both  zero, non-display and protect the sum assured
*       amount.  If  the  minimum  and maximum are both the
*       same, display the amount protected. Otherwise allow
*       input.
*
*  - if all   the   valid   mortality  classes  are  blank,
*       non-display  and  protect  this  field. If there is
*       only  one  mortality  class, display and protect it
*       (no validation will be required).
*
*  - if all the valid lien codes are blank, non-display and
*       protect  this field. Otherwise, this is an optional
*       field.
*
*  - if the  cessation  AGE  section  on  T5608  is  blank,
*       protect the two age related fields.
*
*  - if the  cessation  TERM  section  on  T5608  is blank,
*       protect the two term related fields.
*
*  - using  the  age  next  birthday  (ANB at RCD) from the
*       applicable  life  (see above), look up Issue Age on
*       the AGE and TERM  sections.  If  the  age fits into
*       a "slot" in  one  of these sections,  and the  risk
*       cessation  limits   are  the  same,   default   and
*       protect the risk cessation fields. Also do the same
*       for the premium  cessation  details.  In this case,
*       also  calculate  the  risk  and  premium  cessation
*       dates.
*
* OPTIONS AND EXTRAS
*
* If options and extras are  not  allowed (as defined by T5608)
* non-display and protect the fields.
*
* Otherwise,  read the  options  and  extras  details  for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the Options/Extras indicator (to show that there are some).
*
* REASSURANCE
*
* If Reassurance is not allowed ( as defined by T5687 ), then
* non-display and protect the field ( S5123-RATYPIND ).
*
* Otherwise,  read  the  Reassurance  details  for  the
* current coverage/rider.  If any  records  exist, put a '+' in
* the  Reassurance  indicator (to show that there are some).
*
* ENQUIRY MODE
*
* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to
* protect  all  input  capable  fields  except  the  indicators
* controlling  where  to  switch  to  next  (options and extras
* indicator).
*
* Validation
* ----------
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* If 'KILL'  is  requested  and  the  current credit is zero or
* equal to the number of policies in the plan (all or nothing),
* then skip the  validation.  Otherwise,  highlight  this as an
* error and then skip the remainder of the validation.
*
* If  in  enquiry  mode,  skip  all field validation EXCEPT the
* options/extras indicator.
*
* Before  the  premium amount is calculated, the screen must be
* valid.  So  all  editing  is  completed before the premium is
* calculated.
*
* Table  T5608  (previously read) is used to obtain the editing
* rules.  Edit the screen according to the rules defined by the
* help. In particular:-
*
*  1) Check  the  sum-assured,  if  applicable, against the
*       limits.
*
*  2) Check the consistency of the risk age and term fields
*       and  premium  age and term fields. Either, risk age
*       and   premium  age  must  be  used or risk term and
*       premium  term  must  be  used.  They  must  not  be
*       combined. Note that  these  only need validating if
*       they were not defaulted.
*       NOTE: Risk age and Prem  term  may  be  mixed  i.e.
*       validation no  longer  requires the use of risk age
*       and prem age or risk term and prem term.
*
*  3) Mortality-Class,  if the mortality class appears on a
*       coverage/rider  screen, it  is  a  compulsory field
*       because it will  be used in calculating the premium
*       amount.  The mortality class entered must be one of
*       the ones in the edit rules table.
*
* Calculate the following:-
*
*       - risk cessation date
*       - premium cessation date
*
* OPTIONS AND EXTRAS
*
* If  options/extras already exist, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed. If
* options  and  extras  are  requested,  DO  NOT  CALCULATE THE
* PREMIUM THIS TIME AROUND.
*
* REASSURANCE
*
* If  Reassurance  already exists, there will be a '+' in this
* field.  A  request  to access the details is made by entering
* 'X'.  No  other  values  (other  than  blank) are allowed.
*
* PREMIUM CALCULATION
*
* The  premium amount is  required  on  all  products  and  all
* validation  must  be  successfully  completed  before  it  is
* calculated. If there is no  premium  method defined (i.e. the
* relevant code was blank), the premium amount must be entered.
* Otherwise, it is optional and always calculated.
*
* To calculate  it,  call  the  relevant calculation subroutine
* worked out above passing:
*
*       - 'CALC' for the function
*       - Coverage code (crtable)
*       - Company
*       - Contract number
*       - Life number
*       - Joint life number
*            (if  the   screen  indicator  is  set  to  'J'
*            ,otherwise pass '00')
*       - Coverage number
*       - Rider number (0 for coverages)
*       - Effective date (Premium / Risk)
*       - Termination date (i.e. term cessation date)
*       - Currency
*       - Sum ASsured
*       - Calculation "Basis" (i.e. mortality code)
*       - Payment frequency (from the contract header)
*       - Method of payment (from the contract header)
*       - Premium amount ("instalment")
*       - Return Status
*       - Rating Date (effective date for tables)
*
*  Subroutine may look up:
*
*       -  Life and Joint-life details
*       -  Options/Extras
*
* Having calculated it, the  entered value, if any, is compared
* with it to check that  it  is within acceptable limits of the
* automatically calculated figure.  If  it  is  less  than  the
* amount calculated and  within  tolerance, then  the  manually
* entered amount is allowed.  If  the entered value exceeds the
* calculated one, the calculated value is used.
*
* To check the tolerance amount against the limit read above.
*
* If 'CALC' was entered then re-display the screen.
*
* Updating
* --------
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* Updating occurs with  the Creation, Deletion or Updating of a
* COVTTRM transaction record.
*
* If the 'KILL' function key was pressed or if in enquiry mode,
* skip the updating.
*
* Before  updating any  records,  calculate  the  new  "credit"
* amount.
*
*  - add to  the  current  credit  amount,  the  number  of
*       policies previously  applicable  from  the  COVTTRM
*       record (zero if  there was no COVTTRM) and subtract
*       the number applicable entered on the screen.
*
*  - a positive  credit means that additional policies must
*       be added to the plan.
*
*  - a negative  credit  means  that  some policies must be
*       removed from the plan.
*
* Before creating a  new COVTTRM record initialise the coverage
* fields with a similar  process  as performed by 'NEWCOVR', do
* not call 'NEWCOVR',  perform  the  initialisation  within the
* program. (please document what is actually done.)
*
* If the number  applicable  is greater than zero, write/update
* the COVTTRM record.  If the number applicable is zero, delete
* the COVTTRM record.
*
*
* Next Program
* ------------
*
* The  first  thing  to   consider   is   the  handling  of  an
* options/extras request. If the indicator is 'X', a request to
* visit options and extras has been made. In this case:
*
*  - change the options/extras request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with  an  action  of 'A' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  options/extras  indicator  will  be  '?'.  To
* handle the return from options and extras:
*
*  - calculate  the  premium  as  described  above  in  the
*       'Validation' section, and  check  that it is within
*       the tolerance limit,
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
* If Reassurance has been requested (the indicator = 'X'), then..
*
*  - change the Reassurance request indicator to '?',
*
*  - save the next 8 programs from the program stack,
*
*  - call GENSSWCH with  an  action  of 'B' to retrieve the
*       program switching required,  and  move  them to the
*       stack,
*
*  - set the current stack "action" to '*',
*
*  - add one to the program pointer and exit.
*
* On return from this  request, the current stack "action" will
* be '*' and the  Reassurance  indicator  will  be  '?'.  To
* handle the return from Reassurance:
*
*  - blank  out  the  stack  "action",  restore  the  saved
*       programs to the program stack,
*
*  - set  WSSP-NEXTPROG to  the current screen  name  (thus
*       returning to re-display the screen).
*
* The  following  processing  for  the  4000  section  will  be
* standard  for  all  scrolling  generic  component  processing
* programs.
*
* If control is passed to this  part of the 4000 section on the
* way  out of the program, i.e.  after  screen  I/O,  then  the
* current stack position action  flag  will  be  blank.  If the
* 4000  section  is   being   performed  after  returning  from
* processing another program  then  the  current stack position
* action flag will be '*'.
*
* Processing on the way out:
*
*  A) If one of the Roll  keys  has  been  pressed  and the
*       'Number  Applicable'   has  been  changed  set  the
*       current select action field  to  '*',  add 1 to the
*       program pointer and exit.
*
*  B) If one of the Roll  keys  has  been  pressed  and the
*       'Number Applicable' has  not been changed then loop
*       round  within   the   program   and   display   the
*       next/previous   screen   as   requested  (including
*       reads).   When   displaying   a   previous  record,
*       calculate  the  number  available  as  the  current
*       number plus the  number  applicable  to  the record
*       about to be  displayed.  When  displaying  the next
*       record, or a blank  screen  to  enter a new record,
*       calculate  the  number  available  as  the  current
*       number less the  number  applicable  on the current
*       screen. If displaying  a  blank  screen  for  a new
*       record, default the number applicable to the number
*       available. If this is less than zero, default it to
*       zero.
*
*  C) If 'Enter' has been  pressed and "Credit" is non-zero
*       set the current select  action  field to '*', add 1
*       to the program pointer and exit.
*
*  D) If 'Enter' has been  pressed and "Credit" is zero add
*       1 to the program pointer and exit.
*
*  E) If 'KILL'  has  been  requested, then  move spaces to
*       the current program entry in the program stack  and
*       exit.
*
* Processing on the way in:
*
*  A) If one of the Roll  keys  has  been pressed then loop
*       round within the  program  and  display the next or
*       previous  screen  as  requested  (including  reads,
*       available count etc., see above).
*
*  B) If 'Enter'  has  been  pressed  and  "Credit"  has  a
*       positive value then  loop  round within the program
*       and  display the next screen.  Calculate  available
*       count as the current available  less current screen
*       applicable. Set the number applicable to the number
*       available, but not less than zero.
*
*  C) If 'Enter'  has  been  pressed  and  "Credit"  has  a
*       negative  value,  loop  round  within  the  program
*       displaying the  details  from the first transaction
*       record,  with  the  'Number Applicable' highlighted
*       and give  a  message  indicating that more policies
*       have been defined than are on the Contract Header.
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* The new basic and loaded premium fields, on the COVTPF are          *
* now updated in this Program.                                        *
*
*****************************************************************
*              AMNEDMENT  HISTORY                               *
*****************************************************************
* DATE.....   BY..   AMENDMENT...............  NUMBER
*
* DD/MM/YY    X.X.   xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  NNN
* 08.05.89    A.L.   New field, INSTPREM, on COVR and COVT    001
*                    files. Must be initialised with Zeros.
*                    We must use either INSTPREM or SINGP
*                    depending on which has been previously
*                    set up or the frequency of the contract
*                    being dealt with.
*
* 01.09.89    T.S    Set up new linkage fields CPRM-DURATION,
*                    CPRM-LSEX, CPRM-JLSEX, CPRM-LAGE and
*                    CPRM-JL-AGE for the premium calculation
*                    subroutine.
*                                                                 <003>
* 16/10/89    F.M    Save, Load and Restore options in the        <003>
*                    4000 section have been changed from          <003>
*                    paragraphs to sections.                      <003>
*                                                                 <003>
*                    The processing required to cater for         <003>
*                    Reassurance, control will be handed over     <003>
*                    to p6262 after the updates by this           <003>
*                    program and not before as with the           <003>
*                    selection for Special Terms.                 <003>
*                                                                 <003>
* 13/11/89    J.F.   F11 Kill Key processing modified.        004
*                                                                 <003>
* 27/11/89    I.W    The program was changed to allow the Error   <004>
*                    Message 'Too many Policies' to be displayed  <004>
*                    correctly and the appropriate Screen Field   <004>
*                    'NumApp' to be highlighted when the Number   <004>
*                    Appliciple becomes greater than Total        <004>
*                    policies in Plan.                            <004>
*
* 01/12/89    F.M.   Add code to cater for non display of          <006>
*                    reassurance if reassurance indicator on       <006>
*                    Table T5687 is a space or is an 'N'.          <006>
*
* 09.04.90    A.L.   Should always us FSU Company when dealing    <007>
*                    with clients.
*
* 11.04.90    P.F.   Removed validation requiring that both       <008>
*                    Risk and Prem age or Risk and Prem term
*                    be used. Now age and term can be mixed
*                    (SDF-450)
*
* 30/04/90    B.C    Add code to Check for existance of a         <009>
*                    reassurance record and move '+' to the
*                    indicator. (NM FIX)
*
* 30/04/90    B.C.   Init. COVTTRM_SINGP for correct display      <010>
*                    in 'Pre Issue Validation' ie: was adding
*                    previous coverage to Waiver. (NM FIX).
*
* 25.06.90    BPM    Change the GENSWCH Call-check to handle  011
*                    a MRNF return from T1675.  This must
*                    cause the program to redisplay with a
*                    suitable error rather than fall over.  The
*                    program must allow the initial stack order
*                    to be resumed.
*                    Error V045 was added to Error Table.
*
*  16/07/90   F.M.   Move errors to bottom of list.               <012>
*
*  18/01/91   C.G    S.D.F 1227.                                  <013>
*                    If zeros is entered in the Number of
*                    policies applicable register this to the
*                    user as an error.
*
* 24.03.91    N.F.   SDF1626                                      <014>
*                    Firstly check we are dealing with a rider,
*                    then check to ensure a coverage has
*                    been set-up before the rider. Previously
*                    if the user KILLed out of the coverage
*                    the system carried on to the rider
*                    screen, now it goes to the next coverage
*                    selected or back to the work with screen.
*                    This only applies to optional coverages.
* 25.06.91    N.F.   Initialise new COVTTRM field (PAYRSEQNO)     <015>
*                    to 1.
*                    Retrieve Billing Details from PAYR instead
*                    of CHDRLNB.
*
* 14.05.92    M.F.   SDF NO. : 3134.                              <016>
*                    When using the Special Terms option use
*                    field WSSP-BIG-AMT from WSSPLIFE copybook
*                    to store the Premium Term in order to use
*                    this as a comparison for validation in the
*                    secondary program P5013.
* 23.06.92    M.D.   SDF NO. : 3181.                              <017>
*                    Read T5667 with transaction code and
*                    currency. (Before using the 5 byte
*                    edit code rule from T5671 plus three byte
*                    currency).
*
*                    AQR. 1801/883.
* 26/06/92    J.L.   Move the Chdrlnb cntcurr to the Covttrm  018
*                    cntcurr field.
*
* 26/06/92    P.H    Move '+' to RATYPIND field if details exist. <019>
*
* 20/11/92    M.P.   AQR 3670.                                    020
*                    The title should be increased to 30
*                    characters to account for any component name.
*                    Amended IBM-S38 to AS400 for source and
*                    computer types.
*
* 07/01/93    J.B.   AQR 1225.                                021
*                    Refer to AQR 1227 change <013>.
*                    The check for zeros entered in number
*                    of polices should be done after the
*                    check for zeros if a Plan.
*                    If the number applicable equals the
*                    number available and we have more
*                    COVT's for another coverage, force the
*                    program to roll to the next coverage.
*                    Move error U012 validation to the 2500
*                    Section.
*                    If after reading and holding a COVTUNL
*                    record in the 3000 section, the statuz
*                    is master record not found, exit this
*                    section as there is no record to be
*                    deleted.
*
*
* 25/01/93    D.W.   AQR 3763. Life Development L02-05/93.    022
*                    Amend RACT record processing to use the
*                    RACTLNB logical, which only selects
*                    Validflag '3' RACT records - ie. the
*                    ones that are proposals, since the
*                    contract hasn't been issued yet.
*
* 16/12/93    COS.   SDF 4590.                                023
*                    When using the Special Terms option use
*                    field WSSP-FUPNO from WSSPLIFE copybook
*                    to store the Premium age in order to use
*                    this as a comparison for validation in the
*                    secondary program P5013.
*
* 25/01/94    D.D.   AQR 4933                                 024
*                    PREMIUMREC copybook has been expanded
*                    to incorporate new fields captured on
*                    the New Annuity Details screen for
*                    Regular Benefit components.
*                    The new fields are:-
*                        CPRM-FREQANN
*                        CPRM-ADVANCE
*                        CPRM-ARREARS
*                        CPRM-INTANNY
*                        CPRM-CAPCONT
*                        CPRM-WITHPROP
*                        CPRM-WITHOPROP
*                        CPRM-GUARPERD
*                        CPRM-PPIND
*                        CPRM-NOMLIFE
*                        CPRM-DTHPERCN
*                        CPRM-DTHPERCO
*                    these have all been initialised to
*                    spaces.
*
* 20/06/94    S.V.   AQR 5256
*                    Program Remarks Amended
*
* 07/02/95    W.O.   AQR 5662                                 025
*                    Replace client allocated error codes:
*                    U012 -> H437
*                    U019 -> G818
*                    U029 -> H040
*                    V045 -> H093
*                    U028 -> H039
*
* 22/11/94    J.S.   AQR 5514.                                026
*                    Read the new Premium Breakdown file and
*                    display the option to select the enquiry
*                    on premium breakdown if a record is found
*                    KEEPS the CHDR/POVR records if 'Breakdown
*                    Enquiry' is selected.
*                    Add the contract type, risk and benefit
*                    terms to copy-book PREMIUMREC. Fill the
*                    values in accordingly noting that the
*                    'DURATION' related to the PREM term and
*                    for the moment BENEFIT term will be
*                    defaulted to the PREM term.
*                    Note that the terms will be re-visited..
*
* 01/03/95    M.A.   AQR 5514.                                027
*                    Initialize COVT-Benefit flds.
*
* 04/04/95    M.A.   AQR 5514.                                028
*                    Bonus application method for SUM products
*                    added.  Bonus application methods are held
*                    on T6004 and checked against T6005 for
*                    coverage/rider validation.  The application
*                    method is then stored in COVT.
*
* DATE.....   BY..   AMENDMENT..            .............  NUMBER
***********************************************************************
*                                                                     *
* </pre>
*/
public class P5123 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5123");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaFreqFactor = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaRiskCessTerm = new ZonedDecimalData(3, 0).setUnsigned();
	private final int wsaaMaxOcc = 8;
	private final int wsaaMaxMort = 6;
	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaKillFlag = new FixedLengthStringData(1);
	private Validator forcedKill = new Validator(wsaaKillFlag, "Y");

	private FixedLengthStringData wsaaCtrltime = new FixedLengthStringData(1);
	private Validator firsttime = new Validator(wsaaCtrltime, "Y");
	private Validator nonfirst = new Validator(wsaaCtrltime, "N");
	private String premReqd = "N";
	private FixedLengthStringData wsaaUndwrule = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaItemtabl = new FixedLengthStringData(5).init(SPACES);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaPlanproc = new FixedLengthStringData(1);
	private Validator nonplan = new Validator(wsaaPlanproc, "N");
	protected Validator plan = new Validator(wsaaPlanproc, "Y");

	private FixedLengthStringData wsaaLifeind = new FixedLengthStringData(1);
	protected Validator jointlif = new Validator(wsaaLifeind, "J");
	private Validator singlif = new Validator(wsaaLifeind, "S");

	private FixedLengthStringData wsaaT6768Found = new FixedLengthStringData(1);
	private Validator t6768NotFound = new Validator(wsaaT6768Found, "N");

	private FixedLengthStringData wsaaYaFlag = new FixedLengthStringData(1);
	private Validator yaFound = new Validator(wsaaYaFlag, "Y");

	private FixedLengthStringData wsaaXaFlag = new FixedLengthStringData(1);
	private Validator xaFound = new Validator(wsaaXaFlag, "Y");

	private FixedLengthStringData wsaaFirstTaxCalc = new FixedLengthStringData(1);
	private Validator notFirstTaxCalc = new Validator(wsaaFirstTaxCalc, "N");
	private PackedDecimalData wsaaNumavail = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaCredit = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaWorkCredit = new PackedDecimalData(5, 0);
	protected PackedDecimalData wsaaSumin = new PackedDecimalData(15, 0);
	private PackedDecimalData wsaaFirstSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSaveSeqnbr = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaTaxamt = new PackedDecimalData(17, 2).init(0);
	private FixedLengthStringData wsaaZsredtrm = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaZsbsmeth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSubprog = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaPremind = new FixedLengthStringData(1);
	private ZonedDecimalData wsddRiskCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessAge = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddRiskCessTerm = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsddPremCessTerm = new ZonedDecimalData(3, 0);
		/* WSAA-MAIN-LIFE-DETS */
	protected PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsaaSex = new FixedLengthStringData(1);
		/* WSBB-JOINT-LIFE-DETS */
	protected PackedDecimalData wsbbAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsbbCltdob = new PackedDecimalData(8, 0);
	protected FixedLengthStringData wsbbSex = new FixedLengthStringData(1);
		/* WSZZ-RATED-LIFE-DETS */
	private PackedDecimalData wszzAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wszzCltdob = new PackedDecimalData(8, 0);
	private PackedDecimalData wszzRiskCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessAge = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzRiskCessTerm = new PackedDecimalData(11, 5);
	private PackedDecimalData wszzPremCessTerm = new PackedDecimalData(11, 5);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);

	private FixedLengthStringData wsbbTranCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTranscd = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 0);
	private FixedLengthStringData wsbbCrtable = new FixedLengthStringData(4).isAPartOf(wsbbTranCrtable, 4);

	private FixedLengthStringData wsbbTranCurrency = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbTran = new FixedLengthStringData(5).isAPartOf(wsbbTranCurrency, 0);
	private FixedLengthStringData wsbbCurrency = new FixedLengthStringData(3).isAPartOf(wsbbTranCurrency, 5);

	private FixedLengthStringData wsbbT5667Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsbbT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 0);
	private FixedLengthStringData wsbbT5667Curr = new FixedLengthStringData(4).isAPartOf(wsbbT5667Key, 4);

	private FixedLengthStringData wsaaT6768Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6768Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT6768Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT6768Curr = new FixedLengthStringData(3).isAPartOf(wsaaT6768Key, 4).init(SPACES);
	private FixedLengthStringData wsaaT6768Sex = new FixedLengthStringData(1).isAPartOf(wsaaT6768Key, 7).init(SPACES);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(32, 1, wsaaHeading, 0);
	private FixedLengthStringData wsaaHedline = new FixedLengthStringData(30);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(30, 1, wsaaHedline, 0);
    private FixedLengthStringData wsaaUnderwritingReqd = new FixedLengthStringData(1).init("N");
	private Validator underwritingReqd = new Validator(wsaaUnderwritingReqd, "Y");
	private ZonedDecimalData wsaaAge00Adjusted = new ZonedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaAge01Adjusted = new ZonedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaAdjustedAge = new ZonedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaAgeDifference = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaUndwAge = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaXa = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaYa = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaZa = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaSub00 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub01 = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData x = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub1 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData sub2 = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTol = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaDiff = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaCovtlnbParams = new FixedLengthStringData(219);
	private FixedLengthStringData wsaaCovtlnbDataKey = new FixedLengthStringData(64).isAPartOf(wsaaCovtlnbParams, 49);
	private FixedLengthStringData wsaaCovtlnbCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCovtlnbDataKey, 11);

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);
	private FixedLengthStringData wsaaOccupationClass = new FixedLengthStringData(2);
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
    private CovttrmTableDAM covttrmIO = new CovttrmTableDAM();
	private MbnsTableDAM mbnsIO = new MbnsTableDAM();
	private MinsTableDAM minsIO = new MinsTableDAM();
	private PovrTableDAM povrIO = new PovrTableDAM();
	private UndcTableDAM undcIO = new UndcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Gensswrec gensswrec = new Gensswrec();
	private T5585rec t5585rec = new T5585rec();
	private T2240rec t2240rec = new T2240rec();
	protected T5608rec t5608rec = getT5608rec();
	
	public T5608rec getT5608rec(){
		return new T5608rec();
	}
	private T5667rec t5667rec = new T5667rec();
	private T5671rec t5671rec = new T5671rec();
	protected T5675rec t5675rec = new T5675rec();
	protected T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6005rec t6005rec = new T6005rec();
	private T6768rec t6768rec = new T6768rec();
	protected Tr675rec tr675rec = getTr675rec();

	public Tr675rec getTr675rec() {
		return new Tr675rec();
	}
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	protected Premiumrec premiumrec = getPremiumrec();
	
		
	public Premiumrec getPremiumrec() {
		return new Premiumrec();
	}
	private T6598rec t6598rec = new T6598rec();
	private Rlrdtrmrec rlrdtrmrec = new Rlrdtrmrec();
	protected Th615rec th615rec = new Th615rec();
	private Chkrlrec chkrlrec = new Chkrlrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Wssplife wssplife = new Wssplife();
	/*private S5123ScreenVars sv = ScreenProgram.getScreenVars( S5123ScreenVars.class);*/
	private S5123ScreenVars sv =   getLScreenVars();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaDefaultsInner wsaaDefaultsInner = new WsaaDefaultsInner();
	/*ILIFE-2808 Code Promotion for VPMS externalization of LIFE RTA product calculations Start */
	private ExternalisedRules er = new ExternalisedRules();
	/*ILIFE-2808 End*/
	private boolean loadingFlag = false;/* BRD-306 */

	//ILIFE-3421:Start
	private boolean premiumflag = false;
	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO",RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject= new Rcvdpf();
	//ILIFE-3421:End
	private boolean prmbasisFlag=false;
	//brd-139-starts-vanusha
	private boolean mrtaPermission = false;
	private boolean benSchdPermission = false;//ILIFE-7521
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData NoOfmonths = new ZonedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaRiskTerm = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaIndex1;
	private String mrtaprat;
	private String surrval;
	private String sumins;
	private String mrtaLoandur;
	//ILIFE-7118- starts
	private String tpd1="TPD1";
	private String tps1="TPS1";
	private boolean tpdtypeFlag=false;
	private static final String  TPD_FEATURE_ID="NBPRP060";
	//ILIFE-7118-ends
	//brd-139-ends
	private String IntCalType; //ILIFE-3709
	private MbnspfDAO mbnsDAO = getApplicationContext().getBean("mbnspfDAO",MbnspfDAO.class);
	private Mbnspf mbnspf=null;
	private boolean dialdownFlag = false;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private Exclpf exclpf=null;
	private List<Exclpf> exclpfList;
	private boolean exclFlag = false;
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Payrpf payrpf = null;
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private com.csc.smart400framework.dataaccess.model.Clntpf clnt;//ILIFE-8502
	private com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao;	//ILIFE-8502
	protected CltsTableDAM cltsIO = new CltsTableDAM();
	private Clntpf clntpf;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private List<Itempf> itempfListtr529;
	private List<Itempf> itempfListtr530;
  	private Itempf itempf = new Itempf();
  	private Racdpf racdpf=new Racdpf();
  	private RacdpfDAO recdpfDao = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
  	private List<Racdpf> racdpfList;
  	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
  	private List<Lifepf> lifeList;
  	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
  	protected Hpadpf hpadIO;
  	private MrtapfDAO mrtapfDAO = getApplicationContext().getBean("mrtapfDAO", MrtapfDAO.class);
  	protected Mrtapf mrtaIO;
  	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
  	private Lextpf lextpf=new Lextpf();
  	private boolean lnkgFlag = false;
  	private boolean liencdFlag = false;
  	
  	
  	public List<String> premiumList = new ArrayList<String>();
  	Premiumrec premiumrecNew = new Premiumrec();
  	Covtpf covtpf = new Covtpf();
  	CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO",CovtpfDAO.class);
  	List<Covtpf> covtList = new ArrayList<Covtpf>();
	private static final String premMthd="PMXX";//ILIFE-7343
	boolean isEndMat = false;
	
	/*
	 * Stamp Duty- start
	 */
	private boolean stampDutyflag = false;
	private String stateCode="";
	private static final String chdrenqrec = "CHDRENQREC";
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private static final String TA66 = "TA66";
	private static final String T600 = "T600";
	private static final String TR530 = "TR530";
	private static final String itemcoy = "2";
	private static final String TR529 = "TR529";
	private String aepaydet = "-";
	protected String wsaaOccup = new String();
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);

	/*
	 * Stamp Duty-end
	 */
	
	//ILIFE-7845
			private boolean riskPremflag = false;
			private static final String  RISKPREM_FEATURE_ID="NBPRP094";
			//ILIFE-7845
	/*ILIFE-7934 : Start*/
	private boolean mulProdflag = false;
	private static final String IL_PROD_SETUP_FEATURE_ID="NBPRP096";
	private static final String OIR = "OIR";
	private static final String OIS = "OIS";
	private static final String SIR = "SIR";
	private static final String SIS = "SIS";	
	/*ILIFE-7934 : End*/
	
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);

	private int fupno = 0;
	private List<Fluppf> fluplnbList = new ArrayList<Fluppf>();
	private T5661rec t5661rec = new T5661rec();
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private PackedDecimalData wsaaCount = new PackedDecimalData(2, 0).init(0).setUnsigned();
	private Ta610rec ta610rec = new Ta610rec();
	boolean isFollowUpRequired  = false;
	private Fluppf fluppf;
	boolean NBPRP056Permission  = false;
	private T3644rec t3644rec = new T3644rec();
	private static final String tA610 = "TA610";
	private static final String t3644 = "T3644";
	private static final String t5661 = "T5661";
	private static final String NBPRP056="NBPRP056";
	private List<Fluppf> fluppfAvaList =  new ArrayList<Fluppf>();	//ICIL-1494
	private List<Itempf> ta610List = new ArrayList<Itempf>();	//ICIL-1494
	//ILJ-43
	private boolean contDtCalcFlag = false;
	private String cntDteFeature = "NBPRP113";
	//end
	  	
	//IBPLIFE-2132
	private boolean contnewBFlag = false;
	private String cntnewBFeature = "NBPRP126";
	private Covppf covppf= new Covppf();
	private CovppfDAO covppfDAO = getApplicationContext().getBean("covppfDAO", CovppfDAO.class);
	//end
	private PackedDecimalData wsaaCommissionPrem = new PackedDecimalData(17, 2).init(0).setUnsigned();   //IBPLIFE-5237
	private String tempFlag = "";//IJS-227 PINNACLE-1455
	private Covrpf covrpf = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
  	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont1010, 
		cont1012, 
		cont1015, 
		premmeth1020, 
		cont1030, 
		exit1490, 
		riskCessTerm1510, 
		exit1540, 
		premCessTerm1560, 
		exit1590, 
		exit1790, 
		nextColumn1820, 
		termAge1830, 
		moveDefaults1850, 
		preExit, 
		redisplay2480, 
		exit2090, 
		checkRcessFields2530, 
		ageAnniversary2541, 
		term2542, 
		termExact2543, 
		check2544, 
		checkOccurance2545, 
		checkTermFields2550, 
		checkComplete2555, 
		checkMortcls2560, 
		loop2565, 
		checkLiencd2570, 
		loop2575, 
		checkMore2580, 
		exit2590, 
		calc2710, 
		exit2790, 
		exit3490, 
		exit3790, 
		adjust3b10, 
		adjust3c10, 
		go3d10, 
		cont4710, 
		cont4715, 
		cont4717, 
		cont4720, 
		exit4790, 
		rolu4805, 
		cont4810, 
		cont4820, 
		readCovttrm4830, 
		cont4835, 
		cont4837, 
		cont4840, 
		exit4890, 
		a290Exit
	}

	public P5123() {
		super();
		screenVars = sv;
		new ScreenModel("S5123", AppVars.getInstance(), sv);
	}

	protected S5123ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5123ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			//TMLII-268
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			//TMLII-268
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1001();
				case cont1010: 
					cont1010();
					plan1010();
				case cont1012: 
					cont1012();
				case cont1015: 
					cont1015();
				case premmeth1020: 
					premmeth1020();
				case cont1030: 
					cont1030();
					cont1060();
					prot1070();
				case exit1490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1001()
	{
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");//ILJ-43
		contnewBFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntnewBFeature, appVars, "IT");//IBPLIFE-2132
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			if (isNE(wsspcomn.flag, "I")) {  //MTL130
				 sv.instPrem.set(0);		//MTL130
				 calcPremium2700();			//MTL130
				 sv.instPremErr.set(" ");    //MTL130
				 
			}
			goTo(GotoLabel.exit1490);
		}
		
		aepaydet = "-";
		sv.dataArea.set(SPACES);
		// IBPLIFE-2132
		if (!contnewBFlag) {
			sv.contnewBScreenflag.set("N");
		}
		else
		{
			sv.contnewBScreenflag.set("Y");	
		}
		// end
		premiumrec.premiumRec.set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
		wssplife.fupno.set(0);
		// ILJ-43
		if (!contDtCalcFlag) {
			sv.riskCessAgeOut[varcom.nd.toInt()].set("Y");
		}
		// end
		initialize(rlrdtrmrec.rlrdtrmRec);
		initialize(wsaaZsredtrm);
		initialize(wsaaSubprog);
		initialize(wsaaPremind);
		initialize(wsaaZsbsmeth);
		initialize(premiumrec.premiumRec);
		premiumrec.riskPrem.set(ZERO);//ILIFE-7845
		/*    Initialise & Setup default values for screen fields.*/
		sv.anbAtCcd.set(0);
		sv.instPrem.set(0);
		sv.zbinstprem.set(0);
		sv.zlinstprem.set(0);
		prmbasisFlag=false;
		sv.dialdownoption.set(SPACES);
		/*BRD-306 START */
		sv.loadper.set(0);
		sv.adjustageamt.set(0);
		sv.zstpduty01.set(0);
		sv.rateadj.set(0);
		sv.fltmort.set(0);
		sv.premadj.set(0);
		/*BRD-306 END */
		sv.numapp.set(0);
		sv.numavail.set(0);
		sv.premCessAge.set(0);
		sv.premCessTerm.set(0);
		sv.polinc.set(0);
		sv.riskCessAge.set(0);
		sv.riskCessTerm.set(0);
		sv.taxamt.set(0);
		sv.sumin.set(0);
		wsaaCtrltime.set("N");
		sv.premCessDate.set(varcom.vrcmMaxDate);
		sv.riskCessDate.set(varcom.vrcmMaxDate);
		/* Move ZERO to WSSP area to be used in linkage                    */
		wssplife.bigAmt.set(0);
		wssplife.occdate.set(0);
		initialize(wsaaZsredtrm);
		wsaaTaxamt.set(0);
		sv.prmbasis.set(SPACES);//ILIFE-3421
		wsaaFirstTaxCalc.set("Y");
		sv.tpdtype.set(SPACES);//ILIFE-7118
		/* Read CHDRLNB (RETRV)  in  order to obtain the contract header*/
		/* information.  If  the  number of policies in the plan is zero*/
		/* or  one  then Plan-processing does not apply. If there is any*/
		/* other  numeric  value,  this  value  indicates  the number of*/
		/* policies in the Plan.*/
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		
		
	
		isEndMat = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "PRPRO001", appVars, "IT");	
		if(!isEndMat){
			sv.aepaydetOut[varcom.nd.toInt()].set("Y");
			aepaydet = "+";
		}
		
		
		
		/*BRD-306 START */
		sv.cnttype.set(chdrlnbIO.getCnttype());
		//ILIFE-7521
		benSchdPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP093", appVars, "IT");
		//BRD-139-STARTS by vanusha
				wsaaIndex1 = 0;
				initialize(wsaaRiskTerm);
				mrtaPermission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP010", appVars, "IT") &&  isEQ(sv.cnttype,"MRT");/*ILIFE-4548*/
				//BRD-139-ends by vanusha
		/*    Obtain the Contract Type description from T5688.*/
		descpf=descDAO.getdescData("IT", tablesInner.t5688.toString(), chdrlnbIO.getCnttype().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
	         sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		/*BRD-306 END */
		/* Read T2240 for age definition.                                  */
		
		/* MOVE CHDRLNB-CNTTYPE        TO ITEM-ITEMITEM.           <C02>*/
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrlnbIO.getCnttype());
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.fsuco.toString(), tablesInner.t2240.toString(), wsaaT2240Key.toString());
		if (itempfList.size()==0) {
			
			/*     MOVE '***'              TO ITEM-ITEMITEM            <C02>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.fsuco.toString(), tablesInner.t2240.toString(), wsaaT2240Key.toString().trim());
		}
		t2240rec.t2240Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
			else {
				if (isNE(t2240rec.agecode04,SPACES)) {
					sv.zagelit.set(t2240rec.zagelit04);
				}
			}
			}
		}
		/* Read TR52D for Taxcode                                          */
		sv.taxamtOut[varcom.nd.toInt()].set("Y");
		sv.taxindOut[varcom.nd.toInt()].set("Y");
		sv.taxamtOut[varcom.pr.toInt()].set("Y");
		sv.taxindOut[varcom.pr.toInt()].set("Y");
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(), tablesInner.tr52d.toString(), chdrlnbIO.getRegister().toString());
		if (itempfList.size()==0) {
			itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(), tablesInner.tr52d.toString(), "***");
		}
		tr52drec.tr52dRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		if (isNE(tr52drec.txcode, SPACES)) {
			sv.taxamtOut[varcom.nd.toInt()].set("N");
			sv.taxindOut[varcom.nd.toInt()].set("N");
			sv.taxamtOut[varcom.pr.toInt()].set("N");		
			sv.taxindOut[varcom.pr.toInt()].set("N");
		}
		/* Read the PAYR record to get the Billing Details.                */
		payrpf = payrpfDAO.getpayrRecord(wsspcomn.company.toString(),chdrlnbIO.getChdrnum().toString(),1);
		/* If Plan processing is  not  to be performed, then protect and*/
		/* non-display the following three fields:-*/
		/*  a) No. of Policies in Plan (default to 1)*/
		/*  b) No. Available (default to 1)*/
		/*  c) No. Applicable (default to 1)*/
		/* and processing for Variations will not occur.*/
		if (isEQ(chdrlnbIO.getPolinc(), 0)) {
			wsaaPlanproc.set("N");
		}
		else {
			wsaaPlanproc.set("Y");
		}
		if (nonplan.isTrue()) {
			sv.polinc.set(1);
			sv.numavail.set(1);
			sv.numapp.set(1);
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
		}
		/* The key  for Coverage/rider to be worked on will be available*/
		/* from  COVTLNB.  This is obtained by using the RETRV function.*/
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		//IBPLIFE-2132
		if(contnewBFlag){
		    if (isEQ(wsaaToday, 0)) {
		        datcon1rec.function.set(varcom.tday);
		        Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		        wsaaToday.set(datcon1rec.intDate);
		    }
		   
			//sv.dateeff.set(wsspcomn.effdate.toInt());
		    if (isEQ(wsspcomn.flag, "M") || isEQ(wsspcomn.flag, "I") || isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "X")) {
		    	
		    	covppf=covppfDAO.getCovppfCrtable(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(), covtlnbIO.getCrtable().toString(),
		    			covtlnbIO.getCoverage().toString(),covtlnbIO.getRider().toString());
		    	if(covppf != null){
		    		sv.covrprpse.set(covppf.getCovrprpse());
		    		sv.trcode.set(covppf.getTranCode());
		    		sv.dateeff.set(wsaaToday);
					descpf=descDAO.getdescData("IT", "T1688", sv.trcode.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
				    if (null!=descpf) {
				        sv.trcdedesc.set(descpf.getLongdesc());
				    }
				    else{
				    	sv.trcdedesc.set(SPACE);
				    }
		    	}
		    }
		}
		//end
		if (isEQ(covtlnbIO.getRider(), "00")) {
			goTo(GotoLabel.cont1010);
		}
		/*    If we are dealing with a coverage we should skip this part   */
		wsaaCovtlnbParams.set(covtlnbIO.getParams());
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setDataKey(wsaaCovtlnbDataKey);
		covtlnbIO.setRider(0);
		covtlnbIO.setSeqnbr(0);
		covtlnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.endp)
		&& isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isNE(covtlnbIO.getCoverage(), wsaaCovtlnbCoverage)) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			wsaaKillFlag.set("Y");
			goTo(GotoLabel.exit1490);
		}
		/* TO HAVE GOT THIS FAR MEANS A COVERAGE MUST EXIST             */
		/* FOR THE RIDER WE ARE EITHER ACCESSING OR CREATING.           */
		covtlnbIO.setParams(wsaaCovtlnbParams);
		
	}

protected void cont1010()
	{
		/* Check if there are any transactions by doing a BEGN  (to read*/
		/* the Coverage/rider summary) using the COVTTRM logical view.*/
		/* The COVTTRM key is :-*/
		covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
		covttrmIO.setLife(covtlnbIO.getLife());
		covttrmIO.setCoverage(covtlnbIO.getCoverage());
		covttrmIO.setRider(covtlnbIO.getRider());
		covttrmIO.setSeqnbr(0);
		covttrmIO.setFunction(varcom.begn);
		covttrmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covttrmIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE" , "RIDER");
	
		
		SmartFileCode.execute(appVars, covttrmIO);		
		if (isNE(covttrmIO.getStatuz(), varcom.oK)
		&& isNE(covttrmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		if (isNE(covttrmIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covttrmIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covttrmIO.getLife(), covtlnbIO.getLife())
		|| isNE(covttrmIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covttrmIO.getRider(), covtlnbIO.getRider())) {
			covttrmIO.setStatuz(varcom.endp);
		}
		if (isEQ(covttrmIO.getStatuz(), varcom.endp)) {
			covttrmIO.setNonKey(SPACES);
			covttrmIO.setAnbccd(1, 0);
			covttrmIO.setAnbccd(2, 0);
			covttrmIO.setSingp(0);
			covttrmIO.setInstprem(0);
			covttrmIO.setZbinstprem(0);
			covttrmIO.setZlinstprem(0);
			covttrmIO.setNumapp(0);
			covttrmIO.setPremCessAge(0);
			covttrmIO.setPremCessTerm(0);
			covttrmIO.setPolinc(0);
			covttrmIO.setRiskCessAge(0);
			covttrmIO.setRiskCessTerm(0);
			covttrmIO.setSumins(0);
			covttrmIO.setBenCessAge(0);
			covttrmIO.setBenCessTerm(0);
			wsaaCtrltime.set("Y");
		}
		/* Does Underwriting apply to this Proposal?                       */
		/* Read TR675 to find out.                                         */
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItemtabl(tablesInner.tr675.toString());
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() == 0) {
			wsaaUnderwritingReqd.set("N");
		}
		else {
			wsaaUnderwritingReqd.set("Y");
			tr675rec.tr675Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}

protected void plan1010()
	{
		if (firsttime.isTrue()) {
			covttrmIO.setPolinc(0);
			covttrmIO.setNumapp(0);
			covttrmIO.setSeqnbr(1);
		}
		if(isEndMat)
		{
			itempfListtr530=itempfDAO.getAllItemitem("IT",itemcoy, TR530, covtlnbIO.getCrtable().toString()); /* IJTI-1479 */
			itempfListtr529=itempfDAO.getAllItemitem("IT",itemcoy, TR529, covtlnbIO.getCrtable().toString()); /* IJTI-1479 */
			
			if ((itempfListtr530.size()==0) && (itempfListtr529.size()==0) )  {
				
				sv.aepaydetOut[varcom.nd.toInt()].set("Y");
				aepaydet = "+";
			}
		}
		/* If  Plan  processing is to occur, then calculate the "credit"*/
		/* as follows:*/
		/* - subtract  the  'No  of  policies in the Plan' from the*/
		/*   first  COVTTRM  record  read  above (if no COVTTRM,*/
		/*       this is zero) from the 'No of policies in the Plan'*/
		/*       (from the Contract-Header).*/
		
		/*ILIFE-6941 start*/
		lnkgFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),
				"NBPRP055", appVars, "IT");
		if (lnkgFlag == true) {
			sv.lnkgnoOut[varcom.nd.toInt()].set("Y");
			sv.lnkgsubrefnoOut[varcom.nd.toInt()].set("Y");			
		}
		/*ILIFE-6941 end*/
		
		/*ILIFE-7118-starts*/
		tpdtypeFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),TPD_FEATURE_ID, appVars, "IT");	
		
		if((tpdtypeFlag) && (tpd1.equals(covtlnbIO.getCrtable().trim())||tps1.equals(covtlnbIO.getCrtable().trim()))) {
			sv.tpdtypeOut[varcom.nd.toInt()].set("N");
		}else {
			sv.tpdtypeOut[varcom.nd.toInt()].set("Y");
		}
		/*ILIFE-7118-ends*/
		
		//ILIFE-7028 - Start
		liencdFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString().trim(),
				"NBPRP057", appVars, "IT");
		if(liencdFlag){
			sv.liencdOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.liencdOut[varcom.nd.toInt()].set("N");
		}
		//ILIFE-7028 - End
		if (plan.isTrue()) {
			compute(wsaaCredit, 0).set((sub(chdrlnbIO.getPolinc(), covttrmIO.getPolinc())));
		}
		/* If this is a single policy plan*/
		/*    if it is the first time (no COVTTRM records)*/
		/*     or there is no credit*/
		/*   - plan processing is not required.*/
		if (isEQ(chdrlnbIO.getPolinc(), 1)
		&& (isEQ(covttrmIO.getPolinc(), 0)
		|| isEQ(covttrmIO.getPolinc(), 1))) {
			wsaaPlanproc.set("N");
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
		}
		dialdownFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPRP012", appVars, "IT");//BRD-NBP-011
		//ILIFE-3421:Start
		premiumflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP03", appVars, "IT");

		if(premiumflag || dialdownFlag){
			callReadRCVDPF();
		}
		if(!premiumflag){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}else{
			sv.prmbasisOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject!=null){
				if(rcvdPFObject.getPrmbasis()!=null && !rcvdPFObject.getPrmbasis().trim().equals("") && isEQ(sv.prmbasis,SPACES)){
					sv.prmbasis.set(rcvdPFObject.getPrmbasis());
				}
			}
		}
		//BRD-NBP-011 starts				
		if(!dialdownFlag)
		{	sv.dialdownoptionOut[varcom.nd.toInt()].set("Y");			
		}
		else
		{
			sv.dialdownoptionOut[varcom.nd.toInt()].set("N");
			if(rcvdPFObject != null){
				if(rcvdPFObject.getDialdownoption()!=null && !rcvdPFObject.getDialdownoption().trim().equals("") ){
					sv.dialdownoption.set(rcvdPFObject.getDialdownoption());
				}
			}
		}
		//BRD-NBP-011 ends
		//ILIFE-3421:End

		/* Set  the  number of  policies  available  to  the  number  of*/
		/* policies on the plan.  If  COVTTRM  records are to be written*/
		/* for the first time  (as determined above), default the number*/
		/* applicable to the number available.*/
		/* BRD-306 starts */
		loadingFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP04", appVars, "IT");
		if(!loadingFlag){
			sv.adjustageamtOut[varcom.nd.toInt()].set("Y");
			sv.premadjOut[varcom.nd.toInt()].set("Y");
			sv.zbinstpremOut[varcom.nd.toInt()].set("Y");
		}
		/* BRD-306 ends */
		exclFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPRP020", appVars, "IT");
		if(!exclFlag){
			sv.exclindOut[varcom.nd.toInt()].set("Y");
		}
		else{
			checkExcl();
		}
		
		wsaaFirstSeqnbr.set(covttrmIO.getSeqnbr());
		wsaaNumavail.set(chdrlnbIO.getPolinc());
		if (firsttime.isTrue()) {
			covttrmIO.setNumapp(wsaaNumavail);
			goTo(GotoLabel.cont1012);
		}
		/* ELSE                                                         */
		sv.riskCessDate.set(covttrmIO.getRiskCessDate());
		sv.premCessDate.set(covttrmIO.getPremCessDate());
		sv.riskCessAge.set(covttrmIO.getRiskCessAge());
		sv.premCessAge.set(covttrmIO.getPremCessAge());
		sv.riskCessTerm.set(covttrmIO.getRiskCessTerm());
		sv.premCessTerm.set(covttrmIO.getPremCessTerm());
		if (isNE(covttrmIO.getSingp(), 0)) {
			sv.instPrem.set(covttrmIO.getSingp());
			/*ILIFE-6941 start*/
			sv.lnkgno.set(covttrmIO.getLnkgno());
			sv.lnkgsubrefno.set(covttrmIO.getLnkgsubrefno());			
			/*ILIFE-6941 end*/
			if (isEQ(covttrmIO.getInstprem(), 0)) {
				sv.zbinstprem.set(covttrmIO.getZbinstprem());
				sv.zlinstprem.set(covttrmIO.getZlinstprem());
			}
		}
		else {
			sv.zbinstprem.set(covttrmIO.getZbinstprem());
			sv.zlinstprem.set(covttrmIO.getZlinstprem());
			sv.instPrem.set(covttrmIO.getInstprem());
			/*ILIFE-6941 start*/
			sv.lnkgno.set(covttrmIO.getLnkgno());
			sv.lnkgsubrefno.set(covttrmIO.getLnkgsubrefno());
			/*ILIFE-6941 end*/
		}
		sv.lnkgind.set(covttrmIO.getLnkgind());
		/*BRD-306 START */
		sv.loadper.set(covttrmIO.getLoadper());
		sv.rateadj.set(covttrmIO.getRateadj());
		sv.fltmort.set(covttrmIO.getFltmort());
		sv.premadj.set(covttrmIO.getPremadj());
		sv.adjustageamt.set(covttrmIO.getAgeadj());
		/*BRD-306 END */
		/*
		 * Stamp Duty- start
		 */
		stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
			
		}else{
			sv.zstpduty01Out[varcom.nd.toInt()].set("N");
			if(isNE(covttrmIO.getZstpduty01(),ZERO)){
				sv.zstpduty01.set(covttrmIO.getZstpduty01());
		}
			
		}
		/*
		 * Stamp Duty- end
		 */
		sv.sumin.set(covttrmIO.getSumins());
		sv.mortcls.set(covttrmIO.getMortcls());
		sv.liencd.set(covttrmIO.getLiencd());
		sv.tpdtype.set(covttrmIO.getTpdtype());//ILIFE-7118
		if (isEQ(covttrmIO.getJlife(), "01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set("L");
		}
		sv.bappmeth.set(covttrmIO.getBappmeth());
		getPurePrem1011CustomeSpecific();
		
	}
protected void checkExcl()
{
	exclpfList=exclpfDAO.getExclpfRecord(covtlnbIO.getChdrnum().toString(),covtlnbIO.getCrtable().toString(),covtlnbIO.getLife().toString(),covtlnbIO.getCoverage().toString(),covtlnbIO.getRider().toString());
	if (null!= exclpfList && exclpfList.size()>0) {
		sv.exclind.set("+");
	}
	else {
		sv.exclind.set(" ");
	}
}
protected void cont1012()
	{
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covttrmIO.getNumapp());
		/* Read  the  contract  definition  details  from  T5688 for the*/
		/* contract  type  held  on  CHDRLNB. Access the version of this*/
		/* item for the original commencement date of the risk.*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItemtabl(tablesInner.t5688.toString());
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		
		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() != 0) {
		t5688rec.t5688Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		/* Read  the  general  coverage/rider details from T5687 and the*/
		/* traditional/term  edit rules from T5608 for the coverage type*/
		/* held  on  COVTLNB.  Access  the version of this  item for the*/
		/* original commencement date of the risk.*/
		itempf.setItemtabl(tablesInner.t5687.toString());
		itempf.setItemitem(covtlnbIO.getCrtable().toString());
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempfList = itempfDAO.findByItemDates(itempf);
		if (itempfList.size()==0) {
			t5687rec.t5687Rec.set(SPACES);
			scrnparams.errorCode.set(errorsInner.f294);
		}
		else {
			t5687rec.t5687Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
			wsaaZsredtrm.set(t5687rec.zsredtrm);
			wsaaZsbsmeth.set(t5687rec.zsbsmeth);
			if(isNE(t5687rec.lnkgind,"Y"))
			{
				sv.lnkgindOut[varcom.nd.toInt()].set("Y");
			}
		}
		/* IF T5687-RIIND              = 'N' OR SPACES          <R96REA>*/
		/*    MOVE SPACES              TO S5123-RATYPIND        <R96REA>*/
		/*    MOVE 'Y'                 TO S5123-RATYPIND-OUT(ND)<R96REA>*/
		/*    MOVE 'Y'                 TO S5123-RATYPIND-OUT(PR)<R96REA>*/
		/*    MOVE 'Y'                 TO S5123-RATYPIND-OUT(PR)<R96REA>*/
		/* ELSE                                                 <R96REA>*/
		/*    PERFORM 1100-CHECK-RACT.                          <R96REA>*/
		checkRacd1100();
		if (isEQ(wsaaZsredtrm, "Y")) {
			m100CheckMbns();
			m400CheckMins();
			m900CheckRedtrm();
		}
		else {
			sv.optsmodeOut[varcom.nd.toInt()].set("Y");
			sv.optsmodeOut[varcom.pr.toInt()].set("Y");
			sv.payflagOut[varcom.nd.toInt()].set("Y");
			sv.payflagOut[varcom.pr.toInt()].set("Y");
			sv.zsredtrmOut[varcom.nd.toInt()].set("Y");
			sv.zsredtrmOut[varcom.pr.toInt()].set("Y");
		}
		setupBonus1200();
		descpf=descDAO.getdescData("IT", tablesInner.t5687.toString(), covtlnbIO.getCrtable().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf!=null) {
			wsaaHedline.set(descpf.getLongdesc());
		}
		else {
			wsaaHedline.fill("?");
		}
		loadHeading1700();
		
		wsbbTranscd.set(wsaaBatckey.batcBatctrcde);
		wsbbCrtable.set(covtlnbIO.getCrtable());
		itempfList=itempfDAO.getAllItemitem("IT",covtlnbIO.getChdrcoy().toString(), tablesInner.t5671.toString(), wsbbTranCrtable.toString());
		t5671rec.t5671Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		if (isEQ(t5671rec.pgm[1], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
			wsbbTran.set(t5671rec.edtitm[1]);
		}
		else {
			if (isEQ(t5671rec.pgm[2], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
				wsbbTran.set(t5671rec.edtitm[2]);
			}
			else {
				if (isEQ(t5671rec.pgm[3], wsspcomn.secProg[wsspcomn.programPtr.toInt()])) {
					wsbbTran.set(t5671rec.edtitm[3]);
				}
				else {
					wsbbTran.set(t5671rec.edtitm[4]);
				}
			}
		}
		wsbbCurrency.set(payrpf.getCntcurr());
		/*MOVE CHDRLNB-CNTCURR        TO WSBB-CURRENCY.                */
		itempf.setItemtabl(tablesInner.t5608.toString());
		itempf.setItemitem(wsbbTranCurrency.toString());
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempfList = itempfDAO.findByItemDates(itempf);
		if (itempfList.size()>0) {
			t5608rec.t5608Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		else {
			t5608rec.t5608Rec.set(SPACES);
			t5608rec.ageIssageFrms.fill("0");
			t5608rec.ageIssageTos.fill("0");
			t5608rec.termIssageFrms.fill("0");
			t5608rec.termIssageTos.fill("0");
			t5608rec.premCessageFroms.fill("0");
			t5608rec.premCessageTos.fill("0");
			t5608rec.premCesstermFroms.fill("0");
			t5608rec.premCesstermTos.fill("0");
			t5608rec.riskCessageFroms.fill("0");
			t5608rec.riskCessageTos.fill("0");
			t5608rec.riskCesstermFroms.fill("0");
			t5608rec.riskCesstermTos.fill("0");
			t5608rec.sumInsMax.set(0);
			t5608rec.sumInsMin.set(0);
			if (isEQ(scrnparams.errorCode, SPACES)) {
				scrnparams.errorCode.set(errorsInner.f335);
			}
		}
		/* Read the latest premium tollerance allowed.*/
		/*MOVE WSBB-TRAN-CURRENCY     TO ITEM-ITEMITEM.                */
		wsbbT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		wsbbT5667Curr.set(wsbbCurrency);
		itempfList=itempfDAO.getAllItemitem("IT",chdrlnbIO.getChdrcoy().toString(), tablesInner.t5667.toString(), wsbbT5667Key.toString());
		if (itempfList.size()==0) {
			t5667rec.t5667Rec.set(SPACES);
		}
		else {
			t5667rec.t5667Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		/* LIFE ASSURED AND JOINT LIFE DETAILS*/
		/* To obtain the life assured and joint-life details (if any) do*/
		/* the following;-*/
		/*  - read  the life details using LIFELNB (life number from*/
		/*       COVTLNB, joint life number '00').  Look up the name*/
		/*       from the  client  details  (CLTS)  and  format as a*/
		/*       "confirmation name".*/
		
		lifeList = lifepfDAO.getLifeData(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(), covtlnbIO.getLife().toString(), "00");
		/*    Save Main Life details within Working Storage for later use.*/
		wsaaAnbAtCcd.set(lifeList.get(0).getAnbAtCcd());
		wsaaCltdob.set(lifeList.get(0).getCltdob());
		wsaaSex.set(lifeList.get(0).getCltsex());
		wsaaOccup = lifeList.get(0).getOccup();
		/*    Read CLTS record for Life and format name.*/
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifeList.get(0).getLifcnum()); /* IJTI-1479 */
		sv.lifcnum.set(lifeList.get(0).getLifcnum());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/*ILIFE-7934: Starts*/
		mulProdflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), IL_PROD_SETUP_FEATURE_ID, appVars, "IT");
		if(mulProdflag && (isEQ(chdrlnbIO.getCnttype(),OIR)||isEQ(chdrlnbIO.getCnttype(),OIS)
				||isEQ(chdrlnbIO.getCnttype(),SIR)||isEQ(chdrlnbIO.getCnttype(),SIS))){
			sv.mortcls.set(lifeList.get(0).getSmoking());
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/*ILIFE-7934: Ends*/
		/*
		 * Stamp Duty- start
		 */
		stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(stampDutyflag){
			if(clntpf.getClntStateCd()!= null){
				stateCode=clntpf.getClntStateCd().substring(3).trim();
			}
			/*Clntpf clntpfData = new Clntpf();
			clntpfData.setCltaddr05(clntpf.getCltaddr05().toString());
			clntpfData.setClntStateCd(clntpf.getClntStateCd());
			clntpfDAO.setCacheObject(clntpfData);*/
		}
		/*
		 * Stamp Duty- end
		 */
		/*  - read the joint life details using LIFELNB (life number*/
		/*       from COVTLNB,  joint  life number '01').  If found,*/
		/*       look up the name from the client details (CLTS) and*/
		/*       format as a "confirmation name".*/
		
		lifeList = lifepfDAO.getLifeData(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(), covtlnbIO.getLife().toString(), "01");
		if (lifeList.size()==0) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			wsbbSex.set(SPACES);
			wsbbAnbAtCcd.set(0);
			goTo(GotoLabel.cont1015);
		}
		wsbbAnbAtCcd.set(lifeList.get(0).getAnbAtCcd());
		wsbbCltdob.set(lifeList.get(0).getCltdob());
		wsbbSex.set(lifeList.get(0).getCltsex());
		/*    Read CLTS record for Life and format name.*/
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifeList.get(0).getLifcnum()); /* IJTI-1479 */
		sv.jlifcnum.set(lifeList.get(0).getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	
	}

protected void cont1015()
	{
		/* To  determine  which premium calculation method to use decide*/
		/* whether or  not  it  is  a Single or Joint-life case (it is a*/
		/* joint  life  case,  the  joint  life record was found above).*/
		/* Then:*/
		/*  - if it  is  a  single-life  case use, the single-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the joint-life indicator (from T5687) is blank, and*/
		/*       if  it  is  a Joint-life case, use the joint-method*/
		/*       from  T5687. The age to be used for validation will*/
		/*       be the age of the main life.*/
		/*  - if the  Joint-life  indicator  is  'N',  then  use the*/
		/*       Single-method.  But, if there is a joint-life (this*/
		/*       must be  a  rider  to have got this far) prompt for*/
		/*       the joint  life  indicator  to determine which life*/
		/*       the rider is to attach to.  In all other cases, the*/
		/*       joint life  indicator  should  be non-displayed and*/
		/*       protected.  The  age to be used for validation will*/
		/*       be the age  of the main or joint life, depending on*/
		/*       the one selected.*/
		if (lifeList.size()==0) {
			wsaaLifeind.set("S");
		}
		else {
			wsaaLifeind.set("J");
		}
		if (singlif.isTrue()) {
			itempf.setItemitem(t5687rec.premmeth.toString());
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.premmeth1020);
		}
		if (isEQ(t5687rec.jlifePresent, SPACES)) {
			itempf.setItemitem(t5687rec.jlPremMeth.toString());
			//sv.selectOut[varcom.nd.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			goTo(GotoLabel.premmeth1020);
		}
		/*  For a rider attaching to one life only for a joint life*/
		/*  case, if this is not the first time, set the life selection*/
		/*  indicator.*/
		itempf.setItemitem(t5687rec.premmeth.toString());
		if (nonfirst.isTrue()) {
			if (isEQ(covttrmIO.getJlife(), "01")) {
				sv.select.set("J");
			}
			else {
				sv.select.set("L");
			}
		}
	}

protected void premmeth1020()
	{
		/*  - use the  premium-method  selected  from  T5687, if not*/
		/*       blank00000 access T5675.  This gives the subroutine*/
		/*       to use for the calculation.*/
		/*  - if the benefit billing method is not blank, non-display*/
		/*       and protect the premium field (so do not bother with*/
		/*       the rest of this).*/
		premReqd = "N";
		if (isNE(t5687rec.bbmeth, SPACES)) {
			sv.instPremOut[varcom.pr.toInt()].set("Y");
			sv.instPremOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.cont1030);
		}
		if (isEQ(itempf.getItemitem(), SPACES)) {
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		itempfList=itempfDAO.getAllItemitem("IT",chdrlnbIO.getChdrcoy().toString(), tablesInner.t5675.toString(), itempf.getItemitem());
			if (itempfList.size()==0) {
		    premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
		/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
		if(AppVars.getInstance().getAppConfig().isVpmsEnable())
		{
			premiumrec.premMethod.set(itempf.getItemitem());
		}
		/* ILIFE-3142 End*/
		t5675rec.t5675Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		/*ILIFE-7343*/
		if(isEQ(t5675rec.premsubr,premMthd))
		{
			premReqd = "Y";
			goTo(GotoLabel.cont1030);
		}
	}

protected void cont1030()
	{
		sv.anbAtCcd.set(wsaaAnbAtCcd);
		sv.chdrnum.set(covtlnbIO.getChdrnum());
		sv.coverage.set(covtlnbIO.getCoverage());
		/*MOVE CHDRLNB-CNTCURR        TO S5123-CURRCD.                 */
		sv.currcd.set(payrpf.getCntcurr());
		sv.life.set(covtlnbIO.getLife());
		sv.rider.set(covtlnbIO.getRider());
		sv.statFund.set(t5687rec.statFund);
		sv.statSect.set(t5687rec.statSect);
		sv.statSubsect.set(t5687rec.statSubSect);
		
		
		
		/* The fields to  be displayed on the screen are determined from*/
		/* the entry in table T5608 read earlier as follows:*/
		/*  - if the  maximum  and  minimum  sum insured amounts are*/
		/*       both  0, non-display and protect the sum insured*/
		/*       amount.  If  the  minimum  and maximum are both the*/
		/*       same, display the amount protected. Otherwise allow*/
		/*       input.*/
		/* NOTE - the sum insured applies to the PLAN, so if it is to*/
		/*        be defaulted, scale it down according to the number*/
		/*        of policies applicable.*/
		if (isEQ(t5608rec.sumInsMax, 0)
		&& isEQ(t5608rec.sumInsMin, 0)) {
			sv.suminOut[varcom.nd.toInt()].set("Y");
			sv.suminOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t5608rec.sumInsMax, t5608rec.sumInsMin)
		&& isNE(t5608rec.sumInsMax, 0)) {
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.sumin.set(t5608rec.sumInsMax);
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(sv.sumin, sv.numapp), sv.numavail)));
			}
		}
		/*  - if all   the   valid   mortality  classes  are  blank,*/
		/*       non-display  and  protect  this  field. If there is*/
		/*       only  one  mortality  class, display and protect it*/
		/*       (no validation will be required).*/
		if (isEQ(t5608rec.mortclss, SPACES)) {
			/*sv.mortclsOut[varcom.nd.toInt()].set("Y");*/
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		
		//ILIFE-3421:Start
		if(isEQ(t5608rec.prmbasis, SPACES)){
			sv.prmbasisOut[varcom.nd.toInt()].set("Y");
		}
		else{
			prmbasisFlag=true;
		}
		//ILIFE-3421:End
		
		if (isNE(t5608rec.mortcls01, SPACES)
		&& isEQ(t5608rec.mortcls02, SPACES)
		&& isEQ(t5608rec.mortcls03, SPACES)
		&& isEQ(t5608rec.mortcls04, SPACES)
		&& isEQ(t5608rec.mortcls05, SPACES)
		&& isEQ(t5608rec.mortcls06, SPACES)) {
			sv.mortcls.set(t5608rec.mortcls01);
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
		}
		/*  - if all the valid lien codes are blank, non-display and*/
		/*       protect  this field. Otherwise, this is an optional*/
		/*       field.*/
		if (isEQ(t5608rec.liencds, SPACES)) {
			/*sv.liencdOut[varcom.nd.toInt()].set("Y");*/
			sv.liencdOut[varcom.pr.toInt()].set("Y");
		}
		/*  - using  the  age  next  birthday  (ANB at RCD) from the*/
		/*       applicable  life  (see above), look up Issue Age on*/
		/*       the AGE and TERM  sections.  If  the  age fits into*/
		/*       a "slot" in  one  of  these sections,  and the risk*/
		/*       cessation  limits   are  the   same,   default  and*/
		/*       protect the risk cessation fields. Also do the same*/
		/*       for the premium  cessation  details.  In this case,*/
		/*       also  calculate  the  risk  and  premium  cessation*/
		/*       dates.*/
		wszzCltdob.set(wsaaCltdob);
		wszzAnbAtCcd.set(wsaaAnbAtCcd);
		if (nonfirst.isTrue()
		&& isEQ(covttrmIO.getJlife(), "01")) {
			wszzCltdob.set(wsbbCltdob);
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
		}
		checkDefaults1800();
		if ((wsaaDefaultsInner.defaultRa.isTrue()
		|| wsaaDefaultsInner.defaultRt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5608rec.eaage, SPACES))) {
			riskCessDate1500();
		}
		if ((wsaaDefaultsInner.defaultPa.isTrue()
		|| wsaaDefaultsInner.defaultPt.isTrue())
		&& (firsttime.isTrue()
		|| isNE(t5608rec.eaage, SPACES))) {
			premCessDate1550();
		}
		if (isNE(t5608rec.eaage, SPACES)) {
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void cont1060()
	{
		/* OPTIONS AND EXTRAS*/
		/* If options and extras are  not  allowed (as defined by T5608)*/
		/* non-display and protect the fields.*/
		/* Otherwise,  read the  options  and  extras  details  for  the*/
		/* current coverage/rider.  If any  records  exist, put a '+' in*/
		/* the Options/Extras indicator (to show that there are some).*/
		if (isEQ(t5608rec.specind, "N")) {
			sv.optextindOut[varcom.nd.toInt()].set("Y");
		}
		else {
			/*    PERFORM 1900-CHECK-LEXT.                                  */
			checkLext1900();
			//Ticket #ILIFE-1330 start by akhan203
			/*if (isEQ(wsspcomn.flag, "I")
			&& isEQ(sv.optextind, SPACES)){ 
				sv.optextindOut[varcom.pr.toInt()].set("Y");
			}
*/	//Ticket #ILIFE-1330 end
			}
	}

	/**
	* <pre>
	* ENQUIRY MODE
	* Check WSSP-FLAG, if  'I' (enquiry mode), set the indicator to
	* protect  all  input  capable  fields  except  the  indicators
	* controlling  where  to  switch  to  next  (options and extras
	* indicator).
	* </pre>
	*/
protected void prot1070()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			
			sv.suminOut[varcom.pr.toInt()].set("Y");
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
			sv.mortclsOut[varcom.pr.toInt()].set("Y");
			sv.liencdOut[varcom.pr.toInt()].set("Y");
			sv.instPremOut[varcom.pr.toInt()].set("Y");
			sv.numappOut[varcom.pr.toInt()].set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			sv.rcesdteOut[varcom.pr.toInt()].set("Y");
			sv.prmbasisOut[varcom.pr.toInt()].set("Y");//ILIFE-3421
			sv.dialdownoptionOut[varcom.pr.toInt()].set("Y");//BRD-NBP-011
			sv.lnkgnoOut[varcom.pr.toInt()].set("Y");
			sv.lnkgsubrefnoOut[varcom.pr.toInt()].set("Y");
			sv.tpdtypeOut[varcom.pr.toInt()].set("Y"); //ALS-6234
			if(isEQ(t5687rec.lnkgind,"Y"))
			{
				sv.lnkgindOut[varcom.pr.toInt()].set("Y");
			}
		}
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	*1100-CHECK-RACT SECTION.                                 <R96REA>
	*************************                                 <R96REA>
	*1110-READ-RACT.                                          <R96REA>
	****                                                      <R96REA>
	****                                                      <R96REA>
	**** MOVE COVTLNB-CHDRCOY        TO RACT-CHDRCOY.            <009>
	**** MOVE COVTLNB-CHDRNUM        TO RACT-CHDRNUM.            <009>
	**** MOVE COVTLNB-LIFE           TO RACT-LIFE.               <009>
	**** MOVE COVTLNB-COVERAGE       TO RACT-COVERAGE.           <009>
	**** MOVE COVTLNB-RIDER          TO RACT-RIDER.              <009>
	**** MOVE SPACES                 TO RACT-RASNUM              <009>
	****                                RACT-RATYPE              <009>
	****                                RACT-VALIDFLAG.          <009>
	**** MOVE BEGN                   TO RACT-FUNCTION.           <009>
	**** MOVE RACTREC                TO RACT-FORMAT.             <009>
	****                                                         <009>
	**** CALL 'RACTIO'               USING RACT-PARAMS.          <009>
	****                                                         <009>
	**** IF RACT-STATUZ          NOT = O-K                       <009>
	****                     AND NOT = ENDP                      <009>
	****    MOVE RACT-PARAMS         TO SYSR-PARAMS              <009>
	****    MOVE RACT-STATUZ         TO SYSR-STATUZ              <009>
	****    PERFORM 600-FATAL-ERROR.                             <009>
	****                                                         <009>
	**** IF RACT-CHDRCOY         NOT = COVTLNB-CHDRCOY           <009>
	**** OR RACT-CHDRNUM         NOT = COVTLNB-CHDRNUM           <009>
	**** OR RACT-LIFE            NOT = COVTLNB-LIFE              <009>
	**** OR RACT-COVERAGE        NOT = COVTLNB-COVERAGE          <009>
	**** OR RACT-RIDER           NOT = COVTLNB-RIDER             <009>
	**** OR RACT-VALIDFLAG       NOT = '1'                       <009>
	**** OR RACT-STATUZ              = ENDP                      <009>
	****    MOVE ENDP                TO RACT-STATUZ.             <009>
	****                                                         <009>
	**** IF RACT-STATUZ              = ENDP                      <009>
	****    IF S5123-RATYPIND        = 'X'                       <009>
	****        NEXT SENTENCE                                    <009>
	****    ELSE                                                 <009>
	****        MOVE ' '             TO S5123-RATYPIND           <009>
	**** ELSE                                                    <009>
	****    MOVE '+'                 TO S5123-RATYPIND.          <009>
	****                                                      <R96REA>
	**** MOVE COVTLNB-CHDRCOY        TO RACTLNB-CHDRCOY.      <R96REA>
	**** MOVE COVTLNB-CHDRNUM        TO RACTLNB-CHDRNUM.      <R96REA>
	**** MOVE COVTLNB-LIFE           TO RACTLNB-LIFE.         <R96REA>
	**** MOVE COVTLNB-COVERAGE       TO RACTLNB-COVERAGE.     <R96REA>
	**** MOVE COVTLNB-RIDER          TO RACTLNB-RIDER.        <R96REA>
	**** MOVE SPACES                 TO RACTLNB-RASNUM.       <R96REA>
	**** MOVE SPACES                 TO RACTLNB-RATYPE.       <R96REA>
	**** MOVE SPACES                 TO RACTLNB-VALIDFLAG.    <R96REA>
	**** MOVE BEGN                   TO RACTLNB-FUNCTION.     <R96REA>
	**** MOVE RACTLNBREC             TO RACTLNB-FORMAT.       <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTLNBIO'            USING RACTLNB-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           NOT = O-K                <R96REA>
	****                             AND NOT = ENDP           <R96REA>
	****     MOVE RACTLNB-PARAMS     TO SYSR-PARAMS           <R96REA>
	****     MOVE RACTLNB-STATUZ     TO SYSR-STATUZ           <R96REA>
	****     PERFORM 600-FATAL-ERROR.                         <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-CHDRCOY          NOT = COVTLNB-CHDRCOY    <R96REA>
	****    OR RACTLNB-CHDRNUM       NOT = COVTLNB-CHDRNUM    <R96REA>
	****    OR RACTLNB-LIFE          NOT = COVTLNB-LIFE       <R96REA>
	****    OR RACTLNB-COVERAGE      NOT = COVTLNB-COVERAGE   <R96REA>
	****    OR RACTLNB-RIDER         NOT = COVTLNB-RIDER      <R96REA>
	****    OR RACTLNB-VALIDFLAG     NOT = '3'                <R96REA>
	****    OR RACTLNB-STATUZ        = ENDP                   <R96REA>
	****     MOVE ENDP               TO RACTLNB-STATUZ.       <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           = ENDP                   <R96REA>
	****     IF S5123-RATYPIND       = 'X'                    <R96REA>
	****         NEXT SENTENCE                                <R96REA>
	****     ELSE                                             <R96REA>
	****         MOVE ' '            TO S5123-RATYPIND        <R96REA>
	****     END-IF                                           <R96REA>
	**** ELSE                                                 <R96REA>
	****     MOVE '+'                TO S5123-RATYPIND        <R96REA>
	**** END-IF.                                              <R96REA>
	****                                                      <R96REA>
	*1190-EXIT.                                               <R96REA>
	**** EXIT.                                                <R96REA>
	* </pre>
	*/
protected void checkRacd1100()
	{
		readRacd1110();
	}

	/**
	* <pre>
	*************************                                 <R96REA>
	* </pre>
	*/
protected void readRacd1110()
	{
		racdpf.setChdrcoy(covtlnbIO.getChdrcoy().toString());
		racdpf.setChdrnum(covtlnbIO.getChdrnum().toString());
		racdpf.setLife(covtlnbIO.getLife().toString());
		racdpf.setCoverage(covtlnbIO.getCoverage().toString());
		racdpf.setRider(covtlnbIO.getRider().toString());
		racdpf.setSeqno(0);
		racdpf.setPlanSuffix(0);
		racdpf.setCestype("2");
		racdpfList=recdpfDao.readRacdRecord(racdpf);
		if (racdpfList.size()==0) {
			if (isEQ(sv.ratypind, "X")) {
				/*NEXT_SENTENCE*/
			}
			else {
				sv.ratypind.set(SPACES);
				sv.ratypindOut[varcom.nd.toInt()].set("Y");
				sv.ratypindOut[varcom.pr.toInt()].set("Y");
			}
		}
		else {
			sv.ratypind.set("+");
			sv.ratypindOut[varcom.nd.toInt()].set("N");
			sv.ratypindOut[varcom.pr.toInt()].set("N");
		}
	}

	/**
	* <pre>
	*                                                         <R96REA>
	* </pre>
	*/
protected void m100CheckMbns()
	{
		m100Start();
	}

protected void m100Start()
	{
		mbnsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		mbnsIO.setChdrnum(chdrlnbIO.getChdrnum());
		mbnsIO.setLife(covtlnbIO.getLife());
		mbnsIO.setCoverage(covtlnbIO.getCoverage());
		mbnsIO.setRider(covtlnbIO.getRider());
		mbnsIO.setYrsinf(0);
		mbnsIO.setFormat(formatsInner.mbnsrec);
		mbnsIO.setFunction(varcom.begn);
		mbnsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mbnsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)
		&& isNE(mbnsIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			syserrrec.params.set(mbnsIO.getParams());
			fatalError600();
		}
		if (isEQ(mbnsIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(mbnsIO.getRider(), covtlnbIO.getRider())
		|| isNE(mbnsIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(mbnsIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isNE(mbnsIO.getLife(), covtlnbIO.getLife())
		|| isNE(mbnsIO.getCoverage(), covtlnbIO.getCoverage())) {
			mbnsIO.setStatuz(varcom.endp);
		}
		else {
			sv.optsmode.set("+");
		}
	}

protected void m400CheckMins()
	{
		m400Start();
	}

protected void m400Start()
	{
		minsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		minsIO.setChdrnum(chdrlnbIO.getChdrnum());
		minsIO.setLife(covtlnbIO.getLife());
		minsIO.setCoverage(covtlnbIO.getCoverage());
		minsIO.setRider(covtlnbIO.getRider());
		minsIO.setFormat(formatsInner.minsrec);
		minsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, minsIO);
		if (isNE(minsIO.getStatuz(), varcom.oK)
		&& isNE(minsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(minsIO.getStatuz());
			syserrrec.params.set(minsIO.getParams());
			fatalError600();
		}
		if (isEQ(minsIO.getStatuz(), varcom.oK)) {
			sv.payflag.set("+");
		}
	}

protected void m900CheckRedtrm()
	{
		/*M900-START*/
		mrtaIO=mrtapfDAO.getMrtaRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if (mrtaIO!=null) {
			sv.zsredtrm.set("+");
		}
		else {
			sv.zsredtrm.set(SPACES);
		}
		/*M900-EXIT*/
	}

protected void setupBonus1200()
	{
		para1200();
	}

protected void para1200()
	{
		/* Check if Coverage/Rider is a SUM product. If not SUM product    */
		/* protect field.                                                  */
		/* If SUM product and default exists setup BAPPMETH and protect    */
		/* field. If SUM product and default not setup allow entry.        */
	    itempfList=itempfDAO.getAllItemitem("IT",covtlnbIO.getChdrcoy().toString(), tablesInner.t6005.toString(), covtlnbIO.getCrtable().toString());
		if (itempfList.size()==0) {
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
			return ;
		}
		
		t6005rec.t6005Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		if (isEQ(t6005rec.ind, "1")) {
			sv.bappmeth.set(t6005rec.bappmeth01);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "2")) {
			sv.bappmeth.set(t6005rec.bappmeth02);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "3")) {
			sv.bappmeth.set(t6005rec.bappmeth03);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "4")) {
			sv.bappmeth.set(t6005rec.bappmeth04);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(t6005rec.ind, "5")) {
			sv.bappmeth.set(t6005rec.bappmeth05);
			sv.bappmethOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void riskCessDate1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1500();
				case riskCessTerm1510: 
					riskCessTerm1510();
				case exit1540: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1500()
	{
		if (isEQ(sv.riskCessAge, 0)) {
			goTo(GotoLabel.riskCessTerm1510);
		}
		if (isEQ(t5608rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.riskCessAge, wszzAnbAtCcd));
		}
		if (isEQ(t5608rec.eaage, "E")
		|| isEQ(t5608rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.riskCessAge);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1540);
		}
		sv.riskCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1540);
	}

protected void riskCessTerm1510()
	{
		if (isEQ(sv.riskCessTerm, 0)) {
			sv.rcesdteErr.set(errorsInner.e186);
			return ;
		}
		if (isEQ(t5608rec.eaage, "A")
		|| isEQ(t5608rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.riskCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5608rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.riskCessTerm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.rcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.riskCessDate.set(datcon2rec.intDate2);
	}

protected void premCessDate1550()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para1550();
				case premCessTerm1560: 
					premCessTerm1560();
				case exit1590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1550()
	{
		if (isEQ(sv.premCessAge, 0)) {
			goTo(GotoLabel.premCessTerm1560);
		}
		if (isEQ(t5608rec.eaage, "A")) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			compute(datcon2rec.freqFactor, 0).set(sub(sv.premCessAge, wszzAnbAtCcd));
		}
		if (isEQ(t5608rec.eaage, "E")
		|| isEQ(t5608rec.eaage, SPACES)) {
			datcon2rec.intDate1.set(wszzCltdob);
			datcon2rec.freqFactor.set(sv.premCessAge);
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
			goTo(GotoLabel.exit1590);
		}
		sv.premCessDate.set(datcon2rec.intDate2);
		goTo(GotoLabel.exit1590);
	}

protected void premCessTerm1560()
	{
		if (isEQ(sv.premCessTerm, 0)) {
			return ;
		}
		if (isEQ(t5608rec.eaage, "A")
		|| isEQ(t5608rec.eaage, SPACES)) {
			datcon2rec.freqFactor.set(sv.premCessTerm);
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t5608rec.eaage, "E")) {
			datcon2rec.intDate1.set(wszzCltdob);
			compute(datcon2rec.freqFactor, 0).set(add(sv.premCessTerm, wszzAnbAtCcd));
			if (isNE(chdrlnbIO.getOccdate(), wszzCltdob)) {
				datcon2rec.freqFactor.subtract(1);
			}
		}
		callDatcon21600();
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			sv.pcesdteErr.set(datcon2rec.statuz);
			return ;
		}
		sv.premCessDate.set(datcon2rec.intDate2);
	}

protected void callDatcon21600()
	{
		/*PARA*/
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isEQ(datcon2rec.statuz, varcom.bomb)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadHeading1700()
	{
		try {
			loadScreen1710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loadScreen1710()
	{
		wsaaHeading.set(SPACES);
		/*   Count the number of spaces at the end of the line*/
		/*     (this is assuming there are none at the beginning)*/
		for (wsaaX.set(30); !(isLT(wsaaX, 1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		/* VARYING WSAA-X           FROM 24 BY -1                    */
		/*SUBTRACT WSAA-X             FROM 24 GIVING WSAA-Y.           */
		compute(wsaaY, 0).set(sub(30, wsaaX));
		if (isNE(wsaaY, 0)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		/*   WSAA-X is the size of the heading string*/
		/*   WSAA-Y is the number of spaces in the front*/
		/*   WSAA-Z is the position in the FROM string.*/
		wsaaZ.set(0);
		//wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1, loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1730();
		}
		/*   WSAA-X reused as the position in the TO string.*/
		wsaaX.add(1);
		//wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.crtabdesc.set(wsaaHeading);
		goTo(GotoLabel.exit1790);
	}

protected void moveChar1730()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY, wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void checkDefaults1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					searchTable1810();
				case nextColumn1820: 
					nextColumn1820();
				case termAge1830: 
					termAge1830();
				case moveDefaults1850: 
					moveDefaults1850();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*  - using  the  age  next  birthday  (ANB at RCD) from the
	*       applicable  life  (see above), look up Issue Age on
	*       the AGE and TERM  sections.  If  the  age fits into
	*       a "slot" in  one  of these sections, and the
	*       risk cessation limits  are  the  same,  default and
	*       protect the risk cessation fields. Also do the same
	*       for the premium  cessation  details.  In this case,
	*       also  calculate  the  risk  and  premium  cessation
	*       dates.
	* </pre>
	*/
protected void searchTable1810()
	{
		sub1.set(0);
		wsaaDefaultsInner.wsaaDefaults.set(SPACES);
	}

protected void nextColumn1820()
	{
		sub1.add(1);
		if (isGT(sub1, wsaaMaxOcc)) {
			goTo(GotoLabel.moveDefaults1850);
		}
		/* Skip checking if issue from age and to age are zero.            */
		if (isEQ(t5608rec.ageIssageFrm[sub1.toInt()], 0)
		&& isEQ(t5608rec.ageIssageTo[sub1.toInt()], 0)) {
			goTo(GotoLabel.termAge1830);
		}
		if (!wsaaDefaultsInner.nodefRa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5608rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5608rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.riskCessageFrom[sub1.toInt()], t5608rec.riskCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRa.isTrue()
				&& !wsaaDefaultsInner.defaultRt.isTrue()) {
					wsddRiskCessAge.set(t5608rec.riskCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPa.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5608rec.ageIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5608rec.ageIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.premCessageFrom[sub1.toInt()], t5608rec.premCessageTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPa.isTrue()
				&& !wsaaDefaultsInner.defaultPt.isTrue()) {
					wsddPremCessAge.set(t5608rec.premCessageTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPa.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
	}

protected void termAge1830()
	{
		/* Skip checking if issue from age and to age are zero.            */
		if (isEQ(t5608rec.termIssageFrm[sub1.toInt()], 0)
		&& isEQ(t5608rec.termIssageTo[sub1.toInt()], 0)) {
			goTo(GotoLabel.nextColumn1820);
		}
		if (!wsaaDefaultsInner.nodefRt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5608rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5608rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.riskCesstermFrom[sub1.toInt()], t5608rec.riskCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultRt.isTrue()
				&& !wsaaDefaultsInner.defaultRa.isTrue()) {
					wsddRiskCessTerm.set(t5608rec.riskCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultRt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultRa.set("N");
					wsaaDefaultsInner.wsaaDefaultRt.set("N");
				}
			}
		}
		if (!wsaaDefaultsInner.nodefPt.isTrue()) {
			if (isGTE(wszzAnbAtCcd, t5608rec.termIssageFrm[sub1.toInt()])
			&& isLTE(wszzAnbAtCcd, t5608rec.termIssageTo[sub1.toInt()])
			&& isEQ(t5608rec.premCesstermFrom[sub1.toInt()], t5608rec.premCesstermTo[sub1.toInt()])) {
				if (!wsaaDefaultsInner.defaultPt.isTrue()
				&& !wsaaDefaultsInner.defaultPa.isTrue()) {
					wsddPremCessTerm.set(t5608rec.premCesstermTo[sub1.toInt()]);
					wsaaDefaultsInner.wsaaDefaultPt.set("Y");
				}
				else {
					wsaaDefaultsInner.wsaaDefaultPa.set("N");
					wsaaDefaultsInner.wsaaDefaultPt.set("N");
				}
			}
		}
		goTo(GotoLabel.nextColumn1820);
	}

protected void moveDefaults1850()
	{
		if (wsaaDefaultsInner.defaultRa.isTrue()) {
			sv.riskCessAge.set(wsddRiskCessAge);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultRt.isTrue()) {
			sv.riskCessTerm.set(wsddRiskCessTerm);
			sv.rcessageOut[varcom.pr.toInt()].set("Y");
			sv.rcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPa.isTrue()) {
			sv.premCessAge.set(wsddPremCessAge);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
		if (wsaaDefaultsInner.defaultPt.isTrue()) {
			sv.premCessTerm.set(wsddPremCessTerm);
			sv.pcessageOut[varcom.pr.toInt()].set("Y");
			sv.pcesstrmOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void checkLext1900()
	{
		readLext1910();
	}

protected void readLext1910()
	{
	    lextpf=new Lextpf();
		lextpf.setChdrcoy(covtlnbIO.getChdrcoy().toString());
		lextpf.setChdrnum(covtlnbIO.getChdrnum().toString());
		lextpf.setLife(covtlnbIO.getLife().toString());
		lextpf.setCoverage(covtlnbIO.getCoverage().toString());
		lextpf.setRider(covtlnbIO.getRider().toString());
		lextpf=lextpfDAO.getLextpfRecord(lextpf);
		if (lextpf==null) {
			sv.optextind.set(" ");
		}
		else {
			sv.optextind.set("+");
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		try {
			preStart();
			callScreenIo2010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void preStart()
	{
		/*      SPECIAL EXIT PROCESSING                                    */
		/* Skip this section  if  returning  from  an optional selection   */
		/* (current stack position action flag = '*').                     */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			/*   GO TO 2490-EXIT.                                  <D509CS>*/
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		if (forcedKill.isTrue()) {
			wsspcomn.edterror.set(varcom.oK);
			/*    GO TO 2490-EXIT.                                 <D509CS>*/
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/* Test the POVR file for the existance of a Premium Breakdown     */
		/* record. If one exists then allow the option to display the      */
		/* select window for enquiry....if not then protect.               */
		/* Test here so that the latest POVR is retreived when either      */
		/* returning from a selection or having calculated new values.     */
		readPovr5300();
		if (isEQ(povrIO.getStatuz(), varcom.endp)
		|| isNE(povrIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(povrIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(povrIO.getLife(), covtlnbIO.getLife())
		|| isNE(povrIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(povrIO.getRider(), covtlnbIO.getRider())) {
			sv.pbind.set(SPACES);
			sv.pbindOut[varcom.nd.toInt()].set("Y");
			sv.pbindOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.pbind.set("+");
			sv.pbindOut[varcom.nd.toInt()].set("N");
			sv.pbindOut[varcom.pr.toInt()].set("N");
		}
		if (isNE(tr52drec.txcode, SPACES)) {
			a200CheckCalcTax();
		}
		//ILIFE-1223 STARTS
		//if (isEQ(wsspcomn.flag, "I") || isEQ(sv.taxamt, ZERO)) {
		//Ticket #ILIFE-1330 start by akhan203
		if (isEQ(sv.taxamt, 0)) {						//ILIFE-3258 starts (uncommented the commented portion)
			//Ticket #ILIFE-1330 end
		//ILIFE-1223 ENDS
			sv.taxamtOut[varcom.nd.toInt()].set("Y");
			sv.taxindOut[varcom.nd.toInt()].set("Y");
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			sv.taxindOut[varcom.pr.toInt()].set("Y");
		}													//ILIFE-3258 ends
		//ILIFE-1702 STARTS BY SLAKKALA	
		if(isEQ(wsspcomn.flag, "I")){
				
			sv.taxamtOut[varcom.pr.toInt()].set("Y");
			scrnparams.function.set(varcom.prot);//IBPLIFE-2132
						
		}
		//ILIFE-1702 ENDS 	
		/*
		 * Stamp Duty- start
		 */
		stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
			sv.zstpduty01Out[varcom.nd.toInt()].set("Y");
		}
		/*
		 * Stamp Duty- end
		 */
	}

protected void callScreenIo2010()
	{
		return ;
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2001();
					cont2040();
				case redisplay2480: 
					redisplay2480();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2001()
	{
		/*    CALL 'S5123IO' USING        SCRN-SCREEN-PARAMS               */
		/*                                S5123-DATA-AREA.                 */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If CF11 (KILL) is requested and the current credit is zero or*/
		/* equal to the number of policies in the plan (all or nothing),*/
		/* then skip the  validation.  Otherwise,  highlight  this as an*/
		/* error and then skip the remainder of the validation.*/
		if (isEQ(scrnparams.statuz, "KILL")) {
			if (isEQ(wsaaCredit, 0)
			|| isEQ(wsaaCredit, chdrlnbIO.getPolinc())) {
				/*      GO TO 2490-EXIT                                <D509CS>*/
				/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
				/*          GO TO 2999-EXIT                                        */
				goTo(GotoLabel.exit2090);
			}
			else {
				sv.numappErr.set(errorsInner.g622);
				goTo(GotoLabel.redisplay2480);
			}
		}
		/*VALIDATE*/
		a100CheckLimit();
		/* If  in  enquiry  mode,  skip  all field validation EXCEPT the*/
		/* options/extras indicator.*/
		if (isNE(wsspcomn.flag, "I")) {
			editCoverage2500();
		}
		
	}

protected void cont2040()
	{
		/* If  options/extras already exist, there will be a '+' in this*/
		/* field.  A  request  to access the details is made by entering*/
		/* 'X'.  No  other  values  (other  than  blank) are allowed. If*/
		/* options  and  extras  are  requested,  DO  NOT  CALCULATE THE*/
		/* PREMIUM THIS TIME AROUND.*/
		if (isNE(sv.optextind, " ")
		&& isNE(sv.optextind, "+")
		&& isNE(sv.optextind, "X")) {
			sv.optextindErr.set(errorsInner.g620);
		}
		/* If  reassurance   already exists, there will be a '+' in this   */
		/* field.  A  request  to access the details is made by entering   */
		/* 'X'.  No  other  values  (other  than  blank) are allowed.      */
		if (isNE(sv.ratypind, " ")
		&& isNE(sv.ratypind, "+")
		&& isNE(sv.ratypind, "X")) {
			sv.ratypindErr.set(errorsInner.g620);
		}
		/* Check the premium breakdown indicator.                          */
		if (isNE(sv.pbind, " ")
		&& isNE(sv.pbind, "+")
		&& isNE(sv.pbind, "X")) {
			sv.pbindErr.set(errorsInner.g620);
		}
		/* Check the benefit schedule indicator.                           */
		if (isNE(sv.optsmode, " ")
		&& isNE(sv.optsmode, "+")
		&& isNE(sv.optsmode, "X")) {
			sv.optsmodeErr.set(errorsInner.g620);
		}
		if (isNE(sv.payflag, " ")
		&& isNE(sv.payflag, "+")
		&& isNE(sv.payflag, "X")) {
			sv.payflagErr.set(errorsInner.g620);
		}
		m800ReadMrta();
		/* If everything else is O-K calculate the Installment Premium.*/
		if (isNE(sv.optextind, "X")
		&& isEQ(sv.errorIndicators, SPACES)
		&& isNE(wsspcomn.flag, "I")) {
			calcPremium2700();
		}
		/* Check the Taxcode indicator.                                    */
		if (isNE(sv.taxind, " ")
		&& isNE(sv.taxind, "+")
		&& isNE(sv.taxind, "X")) {
			sv.taxindErr.set(errorsInner.g620);
		}
		if(isEQ(sv.dialdownoptionErr,"E034") && !dialdownFlag)
		{
			sv.dialdownoptionErr.set(SPACES);
		}
		if (exclFlag && isNE(sv.exclind, " ")
				&& isNE(sv.exclind, "+")
				&& isNE(sv.exclind, "X")) {
					sv.exclindErr.set(errorsInner.g620);
				}
		if (isNE(sv.errorIndicators, SPACES)) {
			goTo(GotoLabel.redisplay2480);
		}
		if(stampDutyflag){
			compute(sv.zbinstprem, 2).set(sub(sub(sv.instPrem, sv.zlinstprem),sv.zstpduty01));
		}else{
		compute(sv.zbinstprem, 2).set(sub(sv.instPrem, sv.zlinstprem));
		}
		/* If 'ROLD' was entered,check this is not the first page.*/
		if (isEQ(scrnparams.statuz, varcom.rold)
		&& isEQ(wsaaFirstSeqnbr, covttrmIO.getSeqnbr())) {
			scrnparams.errorCode.set(errorsInner.e027);
			goTo(GotoLabel.redisplay2480);
		}
		validateOccupationOrOccupationClass();	//ICIL-1494
		/* If 'CALC' was entered then re-display the screen.*/
		if (isNE(scrnparams.statuz, varcom.calc)) {
			/*   GO TO 2490-EXIT.                                  <D509CS>*/
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2999-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
	}

protected void redisplay2480()
	{
		wsspcomn.edterror.set("Y");
	}


protected void validateOccupationOrOccupationClass() {
	isFollowUpRequired=false;
	NBPRP056Permission  = FeaConfg.isFeatureExist("2", NBPRP056, appVars, "IT");
	if(NBPRP056Permission) {
		readTA610();
		//String occupation = lifeList.get(0).getOccup().toString();
		if( wsaaOccup != null && !wsaaOccup.trim().isEmpty()) {
			for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
					&& isNE(ta610rec.occclassdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
				if(isEQ(wsaaOccup,ta610rec.occclassdes[wsaaCount.toInt()])){
					isFollowUpRequired = true;
					scrnparams.errorCode.set(errorsInner.rrsu); 
					break;
				}
			}
			
			if(!isFollowUpRequired){
				getOccupationClass2900(wsaaOccup);
				for (wsaaCount.set(1); !(isGT(wsaaCount, 10))
						&& isNE(ta610rec.occcode[wsaaCount.toInt()], SPACES); wsaaCount.add(1)){
					if(isEQ(wsaaOccupationClass,ta610rec.occcode[wsaaCount.toInt()])){
						isFollowUpRequired = true;
						scrnparams.errorCode.set(errorsInner.rrsu); 
						break;
					}
				}	
			}
			
		}
	}
	
}


private void readTA610() {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(tA610);
	itempf.setItemitem(covtlnbIO.getCrtable().toString());
	itempf.setItmfrm(new BigDecimal(wsaaToday.toInt()));
	itempf.setItmto(new BigDecimal(wsaaToday.toInt()));
	ta610List = itempfDAO.findByItemDates(itempf);	//ICIL-1494
	if (ta610List.size()>0 && ta610List.get(0).getGenarea()!=null) {
		ta610rec.tA610Rec.set(StringUtil.rawToString(ta610List.get(0).getGenarea()));
	}	  
}

protected void getOccupationClass2900(String occupation) {
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(t3644);
	   itempf.setItemitem(occupation);
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
	   wsaaOccupationClass.set(t3644rec.occupationClass.toString());
}
	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void editCoverage2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					editFund2500();
					checkSumin2525();
				case checkRcessFields2530: 
					checkRcessFields2530();
					checkPcessFields2535();
					checkAgeTerm2540();
				case ageAnniversary2541: 
					ageAnniversary2541();
				case term2542: 
					term2542();
				case termExact2543: 
					termExact2543();
				case check2544: 
					check2544();
				case checkOccurance2545: 
					checkOccurance2545();
				case checkTermFields2550: 
					checkTermFields2550();
				case checkComplete2555: 
					checkComplete2555();
				case checkMortcls2560: 
					checkMortcls2560();
				case loop2565: 
					loop2565();
				case checkLiencd2570: 
					checkLiencd2570();
				case loop2575: 
					loop2575();
				case checkMore2580: 
					checkMore2580();
				case exit2590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void editFund2500()
	{
		/* If plan processing, and no plicies applicable,*/
		/*   skip the validation as this COVTTRM is to be deleted.*/
		/*IF S5123-NUMAPP             = 0S                     <013>*/
		/*   MOVE L001                TO S5123-NUMAPP-ERR.        <013>*/
		if (plan.isTrue()
		&& isEQ(sv.numapp, 0)) {
			goTo(GotoLabel.exit2590);
		}
		if (isEQ(sv.numapp, 0)) {
			sv.numappErr.set(errorsInner.l001);
		}
		/* Before  the  premium amount is calculated, the screen must be*/
		/* valid.  So  all  editing  is  completed before the premium is*/
		/* calculated.*/
		/*  1) Check  the  sum-assured,  if  applicable, against the*/
		/*       limits (NB. these apply to the plan, so adjust first).*/
		/*    - if only one sum insured is allowed, re-calculate plan*/
		/*      level sum insured (if applicable).*/
		if (isEQ(t5608rec.sumInsMax, t5608rec.sumInsMin)
		&& isNE(t5608rec.sumInsMax, 0)) {
			if (plan.isTrue()) {
				compute(sv.sumin, 1).setRounded((div(mult(t5608rec.sumInsMin, sv.numapp), sv.polinc)));
			}
		}
	}

protected void checkSumin2525()
	{
		if (plan.isTrue()) {
			compute(wsaaSumin, 1).setRounded((div(mult(sv.sumin, sv.polinc), sv.numapp)));
		}
		else {
			wsaaSumin.set(sv.sumin);
		}
		if (isEQ(t5608rec.sumInsMax, t5608rec.sumInsMin)) {
			goTo(GotoLabel.checkRcessFields2530);
		}
		if (isLT(wsaaSumin, t5608rec.sumInsMin)) {
			sv.suminErr.set(errorsInner.e416);
		}
		if (isGT(wsaaSumin, t5608rec.sumInsMax)) {
			sv.suminErr.set(errorsInner.e417);
		}
	}

	/**
	* <pre>
	*  2) Check the consistency of the risk age and term fields
	*       and  premium  age and term fields. Either, risk age
	*       and  premium age must be used or risk term and
	*       premium  term  must  be  used.  They  must  not  be
	*       combined. Note that  these  only need validating if
	*       they were not defaulted.
	*     NOTE: Age and Term fields may now be mixed.                 
	* </pre>
	*/
protected void checkRcessFields2530()
	{
		if (isEQ(sv.select, "J")) {
			wszzAnbAtCcd.set(wsbbAnbAtCcd);
			wszzCltdob.set(wsbbCltdob);
		}
		else {
			wszzAnbAtCcd.set(wsaaAnbAtCcd);
			wszzCltdob.set(wsaaCltdob);
		}
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()) {
			checkDefaults1800();
		}
		if (isGT(sv.riskCessAge, 0)
		&& isGT(sv.riskCessTerm, 0)) {
			sv.rcessageErr.set(errorsInner.f220);
			sv.rcesstrmErr.set(errorsInner.f220);
		}
		if (isNE(t5608rec.eaage, SPACES)
		&& isEQ(sv.riskCessAge, 0)
		&& isEQ(sv.riskCessTerm, 0)) {
			sv.rcessageErr.set(errorsInner.e560);
			sv.rcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkPcessFields2535()
	{
		if (isGT(sv.premCessAge, 0)
		&& isGT(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.f220);
			sv.pcesstrmErr.set(errorsInner.f220);
		}
		if (isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)
		&& isEQ(sv.rcessageErr, SPACES)
		&& isEQ(sv.rcesstrmErr, SPACES)) {
			sv.premCessAge.set(sv.riskCessAge);
			sv.premCessTerm.set(sv.riskCessTerm);
		}
		if (isNE(t5608rec.eaage, SPACES)
		&& isEQ(sv.premCessAge, 0)
		&& isEQ(sv.premCessTerm, 0)) {
			sv.pcessageErr.set(errorsInner.e560);
			sv.pcesstrmErr.set(errorsInner.e560);
		}
	}

protected void checkAgeTerm2540()
	{
		if ((isNE(sv.rcessageErr, SPACES))
		|| (isNE(sv.rcesstrmErr, SPACES))
		|| (isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2560);
		}
		/*    IF S5123-RISK-CESS-AGE      > 0 AND                          */
		/*       S5123-PREM-CESS-AGE      = 0                              */
		/*        MOVE F224               TO S5123-PCESSAGE-ERR            */
		/*        IF NOT DEFAULT-RA                                        */
		/*           MOVE F224            TO S5123-RCESSAGE-ERR.           */
		/*    IF S5123-RISK-CESS-TERM     > 0 AND                          */
		/*       S5123-PREM-CESS-TERM     = 0                              */
		/*        MOVE F225               TO S5123-PCESSTRM-ERR            */
		/*        IF NOT DEFAULT-RT                                        */
		/*           MOVE F225            TO S5123-RCESSTRM-ERR.           */
		if ((isNE(sv.pcessageErr, SPACES))
		|| (isNE(sv.pcesstrmErr, SPACES))
		|| (isNE(sv.rcessageErr, SPACES))
		|| (isNE(sv.rcesstrmErr, SPACES))) {
			goTo(GotoLabel.checkMortcls2560);
		}
		/*  To get this far, everything must be consistant,*/
		/*   now cross check/calculate the risk and premium cessasion*/
		/*   dates. Cross validate these calculated dates against the*/
		/*   edit table (T5608).*/
		if (isNE(t5608rec.eaage, SPACES)
		|| isEQ(sv.riskCessDate, varcom.vrcmMaxDate)) {
			riskCessDate1500();
		}
		else {
			if (isEQ(sv.rcesdteErr, SPACES)) {
				if (isNE(sv.riskCessAge, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.riskCessAge);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.riskCessDate)) {
						/*                MOVE U029    TO S5123-RCESSAGE-ERR            */
						sv.rcessageErr.set(errorsInner.h040);
						sv.rcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.riskCessDate)) {
							/*                   MOVE U029 TO S5123-RCESSAGE-ERR            */
							sv.rcessageErr.set(errorsInner.h040);
							sv.rcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.riskCessTerm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.riskCessTerm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.riskCessDate)) {
							/*                   MOVE U029 TO S5123-RCESSTRM-ERR            */
							sv.rcesstrmErr.set(errorsInner.h040);
							sv.rcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.riskCessDate)) {
								/*                      MOVE U029 TO S5123-RCESSTRM-ERR         */
								sv.rcesstrmErr.set(errorsInner.h040);
								sv.rcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(t5608rec.eaage, SPACES)
		|| isEQ(sv.premCessDate, varcom.vrcmMaxDate)) {
			premCessDate1550();
		}
		else {
			if (isEQ(sv.pcesdteErr, SPACES)) {
				if (isNE(sv.premCessAge, 0)) {
					datcon2rec.intDate1.set(wszzCltdob);
					datcon2rec.freqFactor.set(sv.premCessAge);
					callDatcon21600();
					if (isLT(datcon2rec.intDate2, sv.premCessDate)) {
						/*                MOVE U029    TO S5123-PCESSAGE-ERR            */
						sv.pcessageErr.set(errorsInner.h040);
						sv.pcesdteErr.set(errorsInner.h040);
					}
					else {
						datcon2rec.freqFactor.subtract(1);
						callDatcon21600();
						if (isGTE(datcon2rec.intDate2, sv.premCessDate)) {
							/*                   MOVE U029 TO S5123-PCESSAGE-ERR            */
							sv.pcessageErr.set(errorsInner.h040);
							sv.pcesdteErr.set(errorsInner.h040);
						}
					}
				}
				else {
					if (isNE(sv.premCessTerm, 0)) {
						datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
						datcon2rec.freqFactor.set(sv.premCessTerm);
						callDatcon21600();
						if (isLT(datcon2rec.intDate2, sv.premCessDate)) {
							/*                   MOVE U029 TO S5123-PCESSTRM-ERR            */
							sv.pcesstrmErr.set(errorsInner.h040);
							sv.pcesdteErr.set(errorsInner.h040);
						}
						else {
							datcon2rec.freqFactor.subtract(1);
							callDatcon21600();
							if (isGTE(datcon2rec.intDate2, sv.premCessDate)) {
								/*                      MOVE U029 TO S5123-PCESSTRM-ERR         */
								sv.pcesstrmErr.set(errorsInner.h040);
								sv.pcesdteErr.set(errorsInner.h040);
							}
						}
					}
				}
			}
		}
		if (isNE(sv.rcesdteErr, SPACES)
		|| isNE(sv.pcesdteErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2560);
		}
		if (isEQ(sv.premCessDate, varcom.vrcmMaxDate)) {
			sv.premCessDate.set(sv.riskCessDate);
		}
		if (isGT(sv.premCessDate, sv.riskCessDate)) {
			sv.pcesdteErr.set(errorsInner.e566);
			sv.rcesdteErr.set(errorsInner.e566);
		}
		validateTerm();
		
		if (isNE(sv.rcesdteErr, SPACES)
		|| isNE(sv.pcesdteErr, SPACES)) {
			goTo(GotoLabel.checkMortcls2560);
		}
		//BRD-139:Start
		if(mrtaPermission
		&& isEQ(chdrlnbIO.getCnttype(),"MRT")){  /*ILIFE-3746*/
		  datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
		  datcon3rec.intDate2.set(sv.riskCessDate);
		  datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
			//ILIFE-3703-STARTS
		  if(isGT(mrtaIO.getLoandur(),datcon3rec.freqFactor)) {
		    	scrnparams.errorCode.set(errorsInner.rfxv);//Truncate MRTA
		    	//ILIFE-3703-ENDS
		  } 

		  if(isLT(mrtaIO.getLoandur(),datcon3rec.freqFactor)) {
			  /*ILIFE-3699 starts by slakkala*/
			  scrnparams.errorCode.set(errorsInner.rfxw);
		   	  wsspcomn.edterror.set("Y");//less Trm<pol Trm
		    /*ILIFE-3699 ends by slakkala*/
	      } 
		}
		/*  Calculate cessasion age and term.*/
		/* MOVE WSZZ-CLTDOB            TO DTC3-INT-DATE-1.              */
		/* MOVE S5123-RISK-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-RISK-CESS-AGE.           */
		/* MOVE S5123-PREM-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-AGE.           */
		/* MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-1.              */
		/* MOVE S5123-RISK-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-RISK-CESS-TERM.          */
		/* MOVE S5123-PREM-CESS-DATE   TO DTC3-INT-DATE-2.              */
		/* PERFORM 2600-CALL-DATCON3.                                   */
		/* MOVE DTC3-FREQ-FACTOR       TO WSZZ-PREM-CESS-TERM.          */
		/*  If age is already entered, do not need to re-calculate it      */
		/*  again.                                                         */
		if (isEQ(t5608rec.eaage, "A")) {
			goTo(GotoLabel.ageAnniversary2541);
		}
		if (isEQ(sv.riskCessAge, 0)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge, 0)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
		goTo(GotoLabel.term2542);
	}

protected void validateTerm()
	{
		CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
		List<Covtpf> covtpfList = covtpfDAO.getCovtpfData(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString(), sv.life.toString(), sv.coverage.toString(),"00");
		if (covtpfList != null && !covtpfList.isEmpty()) {
			for (Covtpf covtpf : covtpfList) {
			
					if (isGT(sv.riskCessDate, covtpf.getRcesdte())) {
						sv.rcesdteErr.set(errorsInner.h033);
					}
					if (isGT(sv.premCessDate, covtpf.getPcesdte())) {
						sv.pcesdteErr.set(errorsInner.h044);
					}
			}
		}
}


protected void callDatcon32700(){
	/*PARA*/
	datcon3rec.frequency.set("12");
	callProgram(Datcon3.class, datcon3rec.datcon3Rec);
	if (isNE(datcon3rec.statuz, varcom.oK)) {
		syserrrec.statuz.set(datcon3rec.statuz);
		syserrrec.params.set(datcon3rec.datcon3Rec);
		fatalError600();
	}
	/*EXIT*/
}
protected void ageAnniversary2541()
	{
		if (isEQ(sv.riskCessAge, 0)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessAge.set(datcon3rec.freqFactor);
			wszzRiskCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzRiskCessAge.set(sv.riskCessAge);
		}
		if (isEQ(sv.premCessAge, 0)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessAge.set(datcon3rec.freqFactor);
			wszzPremCessAge.add(wszzAnbAtCcd);
		}
		else {
			wszzPremCessAge.set(sv.premCessAge);
		}
	}

protected void term2542()
	{
		/*  If term is already entered, do not need to re-calculate it     */
		/*  again.                                                         */
		if (isEQ(t5608rec.eaage, "E")
		|| isEQ(t5608rec.eaage, " ")) {
			goTo(GotoLabel.termExact2543);
		}
		if (isEQ(sv.riskCessTerm, 0)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			wszzRiskCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm, 0)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			wszzPremCessTerm.set(datcon3rec.freqFactor);
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
		goTo(GotoLabel.check2544);
	}

protected void termExact2543()
	{
		if (isEQ(sv.riskCessTerm, 0)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.riskCessDate);
			callDatcon32600();
			compute(wszzRiskCessTerm, 5).set(sub(datcon3rec.freqFactor, wszzAnbAtCcd));
		}
		else {
			wszzRiskCessTerm.set(sv.riskCessTerm);
		}
		if (isEQ(sv.premCessTerm, 0)) {
			datcon3rec.intDate1.set(wszzCltdob);
			datcon3rec.intDate2.set(sv.premCessDate);
			callDatcon32600();
			compute(wszzPremCessTerm, 5).set(sub(datcon3rec.freqFactor, wszzAnbAtCcd));
		}
		else {
			wszzPremCessTerm.set(sv.premCessTerm);
		}
	}

protected void check2544()
	{
		/*  Assume the dates are invalid until proved otherwise.*/
		sv.rcessageErr.set(errorsInner.e519);
		sv.pcessageErr.set(errorsInner.e562);
		sv.rcesstrmErr.set(errorsInner.e551);
		sv.pcesstrmErr.set(errorsInner.e563);
		x.set(0);
	}

	/**
	* <pre>
	* Check each possible option.
	* </pre>
	*/
protected void checkOccurance2545()
	{
		x.add(1);
		if (isGT(x, wsaaMaxOcc)) {
			goTo(GotoLabel.checkComplete2555);
		}
		if ((isEQ(t5608rec.ageIssageFrm[x.toInt()], 0)
		&& isEQ(t5608rec.ageIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5608rec.ageIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5608rec.ageIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkTermFields2550);
		}
		if (isGTE(wszzRiskCessAge, t5608rec.riskCessageFrom[x.toInt()])
		&& isLTE(wszzRiskCessAge, t5608rec.riskCessageTo[x.toInt()])) {
			sv.rcessageErr.set(SPACES);
		}
		if (isGTE(wszzPremCessAge, t5608rec.premCessageFrom[x.toInt()])
		&& isLTE(wszzPremCessAge, t5608rec.premCessageTo[x.toInt()])) {
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkTermFields2550()
	{
		if ((isEQ(t5608rec.termIssageFrm[x.toInt()], 0)
		&& isEQ(t5608rec.termIssageTo[x.toInt()], 0))
		|| isLT(wszzAnbAtCcd, t5608rec.termIssageFrm[x.toInt()])
		|| isGT(wszzAnbAtCcd, t5608rec.termIssageTo[x.toInt()])) {
			goTo(GotoLabel.checkOccurance2545);
		}
		if (isGTE(wszzRiskCessTerm, t5608rec.riskCesstermFrom[x.toInt()])
		&& isLTE(wszzRiskCessTerm, t5608rec.riskCesstermTo[x.toInt()])) {
			sv.rcesstrmErr.set(SPACES);
		}
		if (isGTE(wszzPremCessTerm, t5608rec.premCesstermFrom[x.toInt()])
		&& isLTE(wszzPremCessTerm, t5608rec.premCesstermTo[x.toInt()])) {
			sv.pcesstrmErr.set(SPACES);
		}
		goTo(GotoLabel.checkOccurance2545);
	}

protected void checkComplete2555()
	{
		if (isNE(sv.rcesstrmErr, SPACES)
		&& isEQ(sv.riskCessTerm, 0)) {
			sv.rcesdteErr.set(sv.rcesstrmErr);
			sv.rcesstrmErr.set(SPACES);
		}
		if (isNE(sv.pcesstrmErr, SPACES)
		&& isEQ(sv.premCessTerm, 0)) {
			sv.pcesdteErr.set(sv.pcesstrmErr);
			sv.pcesstrmErr.set(SPACES);
		}
		if (isNE(sv.rcessageErr, SPACES)
		&& isEQ(sv.riskCessAge, 0)) {
			sv.rcesdteErr.set(sv.rcessageErr);
			sv.rcessageErr.set(SPACES);
		}
		if (isNE(sv.pcessageErr, SPACES)
		&& isEQ(sv.premCessAge, 0)) {
			sv.pcesdteErr.set(sv.pcessageErr);
			sv.pcessageErr.set(SPACES);
		}
	}

protected void checkMortcls2560()
	{
		/*  3) Mortality-Class,  if the mortality class appears on a*/
		/*       coverage/rider  screen  it  is  a  compulsory field*/
		/*       because it will  be used in calculating the premium*/
		/*       amount. The mortality class entered must one of the*/
		/*       ones in the edit rules table.*/
		if(sv.mortcls.equals(SPACES)) {
			sv.mortclsErr.set(errorsInner.e186);
		}
		x.set(0);
	}

protected void loop2565()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.mortclsErr.set(errorsInner.e420);
			goTo(GotoLabel.checkLiencd2570);
		}
		if (isNE(t5608rec.mortcls[x.toInt()], sv.mortcls)) {
			goTo(GotoLabel.loop2565);
		}
	}

protected void checkLiencd2570()
	{
		if (isEQ(sv.liencd, SPACES)) {
			goTo(GotoLabel.checkMore2580);
		}
		x.set(0);
	}

protected void loop2575()
	{
		x.add(1);
		if (isGT(x, wsaaMaxMort)) {
			sv.liencdErr.set(errorsInner.e531);
			goTo(GotoLabel.checkMore2580);
		}
		if (isEQ(t5608rec.liencd[x.toInt()], SPACES)
		|| isNE(t5608rec.liencd[x.toInt()], sv.liencd)) {
			goTo(GotoLabel.loop2575);
		}
	}

protected void checkMore2580()
	{
		/* Check joint life selection indicator (if applicable).*/
		if (isNE(sv.selectOut[varcom.pr.toInt()], "Y")) {
			if (isNE(sv.select, SPACES)
			&& isNE(sv.select, "J")
			&& isNE(sv.select, "L")) {
				/*       MOVE U028             TO S5123-SELECT-ERR.             */
				sv.selectErr.set(errorsInner.h039);
			}
		}
		if (isEQ(sv.numapp, 0)
		&& isEQ(sv.numavail, sv.polinc)) {
			sv.numappErr.set(errorsInner.l001);
		}
		wsaaWorkCredit.set(wsaaCredit);
		if (nonfirst.isTrue()) {
			wsaaWorkCredit.add(covttrmIO.getNumapp());
		}
		wsaaWorkCredit.subtract(sv.numapp);
		if (isLT(wsaaWorkCredit, 0)
		&& isGT(sv.numapp, sv.numavail)) {
			/*    MOVE U012                TO S5123-NUMAPP-ERR         <021>*/
			sv.numappErr.set(errorsInner.h437);
			wsspcomn.edterror.set("Y");
		}
		/* Check to see if BONUS APPLICATION METHOD is valid for           */
		/* coverage/rider.                                                 */
		if (isNE(sv.bappmeth, SPACES)
		&& isNE(sv.bappmeth, t6005rec.bappmeth01)
		&& isNE(sv.bappmeth, t6005rec.bappmeth02)
		&& isNE(sv.bappmeth, t6005rec.bappmeth03)
		&& isNE(sv.bappmeth, t6005rec.bappmeth04)
		&& isNE(sv.bappmeth, t6005rec.bappmeth05)) {
			sv.bappmethErr.set(errorsInner.d352);
			wsspcomn.edterror.set("Y");
		}
		//ILIFE-3421:Start
		if(premiumflag)
		{
			checkIPPmandatory();
		boolean t5608Flag=false;
		for(int counter=1; counter<t5608rec.prmbasis.length;counter++){
			if(isNE(sv.prmbasis,SPACES)){
				if (isEQ(t5608rec.prmbasis[counter], sv.prmbasis)){
					t5608Flag=true;
					break;
				}
			}
		}
		if(!t5608Flag && isNE(sv.prmbasis,SPACES)){
			sv.prmbasisErr.set("RFV1");
		}
		}
		//ILIFE-3421:End
	}
protected void checkIPPmandatory()
{
	if(AppVars.getInstance().getAppConfig().isVpmsEnable())
	{
		if((isEQ(sv.prmbasis,SPACES)) && prmbasisFlag )
				sv.prmbasisErr.set(errorsInner.e186);
	}
	if((isEQ(sv.tpdtype,SPACES)) && tpdtypeFlag && (tpd1.equals(covtlnbIO.getCrtable().trim())||tps1.equals(covtlnbIO.getCrtable().trim())))
		sv.tpdtypeErr.set(errorsInner.e186);
}
protected void callDatcon32600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void calcPremium2700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para2700();
				case calc2710: 
					calc2710();
				case exit2790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para2700()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2790);
		}
		/* If plan processing, and no plicies applicable,*/
		/*   skip the validation as this COVTTRM is to be deleted.*/
		if (plan.isTrue()
		&& isEQ(sv.numapp, 0)) {
			goTo(GotoLabel.exit2790);
		}
		/* If benefit billed, do not calculate premium*/
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.exit2790);
		}
		/* PREMIUM CALCULATION*/
		/* The  premium amount is  required  on  all  products  and  all*/
		/* validating  must  be  successfully  completed  before  it  is*/
		/* calculated. If there is  no  premium  method defined (i.e the*/
		/* relevant code was blank), the premium amount must be entered.*/
		/* Otherwise, it is optional and always calculated.*/
		/* Note that a premium calculation subroutine may calculate the*/
		/* premium from the sum insured OR the sum insured from the*/
		/* premium.*/
		/* To calculate  it,  call  the  relevant calculation subroutine*/
		/* worked out above passing:*/
		if (isEQ(premReqd, "N")) {
			goTo(GotoLabel.calc2710);
		}
		if (isEQ(sv.instPrem, 0)) {
			/*    MOVE U019                TO S5123-INSTPRM-ERR.            */
			sv.instPremErr.set(errorsInner.g818);
		}
		
		//ILIFE-7845
		if(isEQ(t5675rec.premsubr,premMthd))
		{
			goTo(GotoLabel.calc2710);
		}
		//ILIFE-7845
		goTo(GotoLabel.exit2790);
	}

protected void calc2710()
	{
		premiumrec.function.set("CALC");
		premiumrec.crtable.set(covtlnbIO.getCrtable());
		premiumrec.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		premiumrec.chdrChdrnum.set(sv.chdrnum);
		premiumrec.lifeLife.set(sv.life);
		if(isEQ(sv.lnkgsubrefno,SPACE)|| sv.lnkgsubrefno==null)	
			premiumrec.lnkgSubRefNo.set(SPACE);	
		else{	
			premiumrec.lnkgSubRefNo.set(sv.lnkgsubrefno.toString().trim());	
		}
		//ILIFE-6941 - Start
		if(isEQ(sv.lnkgno,SPACE)) {
			premiumrec.linkcov.set(SPACE);
		}
		else {
			if(sv.lnkgno != null){
				LinkageInfoService linkgService = new LinkageInfoService();		
				FixedLengthStringData linkgCov = new FixedLengthStringData(linkgService.getLinkageInfo(sv.lnkgno.toString()));
				premiumrec.linkcov.set(linkgCov);
			}
		}
		premiumrec.liencd.set(sv.liencd);	//ILIFE-6964
		//ILIFE-6941 - End
		if (isEQ(sv.select, "J")) {
			premiumrec.lifeJlife.set("01");
		}
		else {
			premiumrec.lifeJlife.set("00");
		}
		premiumrec.covrCoverage.set(sv.coverage);
		premiumrec.covrRider.set(sv.rider);
		premiumrec.effectdt.set(chdrlnbIO.getOccdate());
		premiumrec.termdate.set(sv.premCessDate);
		premiumrec.lsex.set(wsaaSex);
		premiumrec.lage.set(wsaaAnbAtCcd);
		premiumrec.jlsex.set(wsbbSex);
		premiumrec.jlage.set(wsbbAnbAtCcd);
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		/*MOVE CPRM-DURATION          TO CPRM-BEN-CESS-TERM.      <026>*/
		premiumrec.cnttype.set(chdrlnbIO.getCnttype());
		datcon3rec.intDate1.set(premiumrec.effectdt);
		datcon3rec.intDate2.set(sv.riskCessDate);
		datcon3rec.frequency.set(freqcpy.yrly);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		premiumrec.riskCessTerm.set(wsaaRiskCessTerm);
		premiumrec.currcode.set(payrpf.getCntcurr());
		/*MOVE CHDRLNB-CNTCURR        TO CPRM-CURRCODE.                */
		/*  (wsaa-sumin already adjusted for plan processing)*/
		premiumrec.sumin.set(wsaaSumin);
		premiumrec.mortcls.set(sv.mortcls);
		premiumrec.tpdtype.set(sv.tpdtype.charAt(5));//ILIFE-7118
		premiumrec.lnkgind.set(sv.lnkgind);
		premiumrec.billfreq.set(payrpf.getBillfreq());
		premiumrec.mop.set(payrpf.getBillchnl());
		/*MOVE CHDRLNB-BILLFREQ       TO CPRM-BILLFREQ.                */
		/*MOVE CHDRLNB-BILLCHNL       TO CPRM-MOP                      */
		premiumrec.ratingdate.set(chdrlnbIO.getOccdate());
		premiumrec.reRateDate.set(chdrlnbIO.getOccdate());
		//premiumrec.calcPrem.set(sv.instPrem); //MIBT273 -code commented
		if (isNE(premiumrec.calcBasPrem, 0)) {
			premiumrec.calcPrem.set(premiumrec.calcBasPrem);
		}
		else{
//			compute(premiumrec.calcPrem,0).set(sub(sv.instPrem,sv.zlinstprem)); //MIBT-273 -sv.instPrem might be set to zero
			premiumrec.calcPrem.set(sv.zbinstprem); //MIBT-273 - sv.zbinstprem should have the base premium
		}
		//premiumrec.calcBasPrem.set(sv.instPrem);//MIBT-273-code commented
				premiumrec.calcBasPrem.set(sv.zbinstprem);//MIBT273	-sv.zbinstprem should have the base premium
				premiumrec.calcLoaPrem.set(0);
		if (plan.isTrue()) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.polinc), sv.numapp)));
		}
		premiumrec.advance.set(SPACES);
		premiumrec.arrears.set(SPACES);
		premiumrec.freqann.set(SPACES);
		premiumrec.withprop.set(SPACES);
		premiumrec.withoprop.set(SPACES);
		premiumrec.ppind.set(SPACES);
		premiumrec.nomlife.set(SPACES);
		premiumrec.guarperd.set(0);
		premiumrec.intanny.set(0);
		premiumrec.capcont.set(0);
		premiumrec.dthpercn.set(0);
		premiumrec.dthperco.set(0);
		premiumrec.language.set(wsspcomn.language);
		premiumrec.commissionPrem.set(ZERO);   //IBPLIFE-5237
		setBMIvalu2800CustomerSpecific();
		if(stampDutyflag) {
			premiumrec.zstpduty01.set(ZERO);
			premiumrec.zstpduty02.set(ZERO);
		}
		/*Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS externalization 
		changes related to TRM calculation] Start
		*/
		/*Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible models Start*/		
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && (er.isCallExternal(t5675rec.premsubr.toString()) || t5675rec.premsubr.toString().trim().equals("PMEX")))) 
		{
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
			if (isEQ(premiumrec.statuz, varcom.bomb)) {	
				syserrrec.statuz.set(premiumrec.statuz);	
				fatalError600();	
			}	
			if (isNE(premiumrec.statuz, varcom.oK)) {	
				if(isEQ(premiumrec.statuz, errorsInner.rrlk)) {	//ILIFE-7614	
					sv.suminErr.set(premiumrec.statuz);	
					goTo(GotoLabel.exit2790);	
				}	
				sv.instPremErr.set(premiumrec.statuz);	
				goTo(GotoLabel.exit2790);	
			}

		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			
			//ILIFE-3421:Start
			/*if(isNE(sv.prmbasis,SPACES)){
				premiumrec.prmbasis.set(sv.prmbasis);
			}
			premiumrec.waitperiod.set(SPACES);
			premiumrec.bentrm.set(SPACES);
			premiumrec.poltyp.set(SPACES);*/
			//ILIFE-3421:End
			
			//ILIFE-7032 - Starts
			if(isEQ(sv.prmbasis,"S")){
				premiumrec.prmbasis.set("Y");
			}else{
				premiumrec.prmbasis.set("");
			}
			//ILIFE-7032 - End
			
			if(isNE(sv.dialdownoption,SPACES)){
				if(sv.dialdownoption.toString().startsWith("0"))
					premiumrec.dialdownoption.set(sv.dialdownoption.substring(1));
				else
					premiumrec.dialdownoption.set(sv.dialdownoption);
			}
			else
				premiumrec.dialdownoption.set("100");
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
			
			Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
			vpxchdrrec.function.set("INIT");
			callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec,vpxchdrrec);
			premiumrec.rstaflag.set(vpxchdrrec.rstaflag);
			Vpxacblrec vpxacblrec=new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec,vpxacblrec.vpxacblRec);
			/*Ticket #ILIFE-2808 Code Promotion for VPMS externalization of LIFE RTA product calculations Start*/
			/* ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product calculations] Start */
			if(t5675rec.premsubr.toString().equals("RPRMRDT") || covtlnbIO.getCrtable().toString().trim().equals("LCP1")){ //ILIFE-8537
				//premiumrec.premMethod.set("PM13");
				hpadRead100();
				vpxlextrec.zszprmcd.set(t5687rec.zszprmcd);
				vpxlextrec.prat.set(mrtaIO.getPrat());
				// ILIFE-5845 start
				compute(vpxlextrec.temptoc, 2).setRounded(mrtaIO.getCoverc());
				vpxlextrec.coverc.set(vpxlextrec.temptoc);
				// ILIFE-5845  end
				compute(vpxlextrec.temptoc, 2).setRounded(sub(premiumrec.duration, mrtaIO.getCoverc()));
				vpxlextrec.trmofcontract.set(vpxlextrec.temptoc);
				vpxlextrec.hpropdte.set(hpadIO.getHpropdte());
				vpxlextrec.premind.set(th615rec.premind);
			}
/*			else
				premiumrec.premMethod.set(t5675rec.premsubr.toString().substring(3));
*/		
		/* ILIFE-3142 End */	
		/*Ticket #ILIFE-2808 - End*/
			premiumrec.cownnum.set(SPACES);
			premiumrec.occdate.set(ZERO);
			covtList= covtpfDAO.searchCovtRecordByCoyNumDescUniquNo(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString());
			if(t5675rec.premsubr.toString().trim().equals("PMEX")){
				premiumrec.setPmexCall.set("Y");
				premiumrec.cownnum.set(chdrlnbIO.getCownnum());
				premiumrec.occdate.set(chdrlnbIO.getOccdate());
			}
			if(stampDutyflag) {
				premiumrec.rstate01.set(stateCode);
			}
			//ILIFE-8502-starts
			premiumrec.commTaxInd.set("Y");
			premiumrec.prevSumIns.set(ZERO);
			premiumrec.inputPrevPrem.set(ZERO);
			clntDao = DAOFactory.getClntpfDAO();
			clnt=clntDao.getClientByClntnum(sv.lifcnum.toString());
			if(null!=clnt && null!=clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()){
				premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
			}else{
				premiumrec.stateAtIncep.set(clntpf.getClntStateCd());
			}
			//ILIFE-8502-end	
			premiumrec.validind.set("2"); //Perform Validation
            callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec,vpxacblrec.vpxacblRec);
            if (isEQ(premiumrec.statuz, varcom.bomb)) {	
    			syserrrec.statuz.set(premiumrec.statuz);	
    			fatalError600();	
    		}	
    		if (isNE(premiumrec.statuz, varcom.oK)) {	
    			if(isEQ(premiumrec.statuz, errorsInner.rrlk)) {	//ILIFE-7614	
    				sv.suminErr.set(premiumrec.statuz);	
    				goTo(GotoLabel.exit2790);	
    			}	
    			sv.instPremErr.set(premiumrec.statuz);	
    			goTo(GotoLabel.exit2790);	
    		}
    		
			//ILIFE-7845
			premiumrec.riskPrem.set(ZERO);
			riskPremflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), RISKPREM_FEATURE_ID, appVars, "IT");
			if (riskPremflag) {
				premiumrec.cnttype.set(chdrlnbIO.getCnttype());
				premiumrec.crtable.set(covtlnbIO.getCrtable());
				if(isEQ(t5675rec.premsubr,premMthd)) {
                    premiumrec.calcPrem.set(sv.instPrem.getbigdata());
					premiumrec.calcTotPrem.set(add(premiumrec.calcPrem, sv.zstpduty01.getbigdata()));
				}else {
				premiumrec.calcTotPrem.set(add(premiumrec.zstpduty01, premiumrec.calcPrem));
				}
				
				if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
				{
					callProgram("RISKPREMIUM", premiumrec.premiumRec);
			}
			}
			//ILIFE-7845
		}
		/*Ticket #IVE-792 - End		*/
		//****Ticket #ILIFE-2005 end
		//Ticket #ILIFE-2022 (commented 2 lines) -- Loaded premium not getting added to Total premium -- start
		//premiumrec.calcPrem.set(premiumrec.calcBasPrem);//added
		//sv.instPrem.set(premiumrec.calcPrem);//added
		//Ticket #ILIFE-2022 (commented 2 lines) -- Loaded premium not getting added to Total premium -- end

		getPrem2050CustomerSpecific();
		/* Adjust premium calculated for plan processing.*/
		if (plan.isTrue()) {
			compute(premiumrec.calcBasPrem, 3).setRounded((div(mult(premiumrec.calcBasPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcLoaPrem, 3).setRounded((div(mult(premiumrec.calcLoaPrem, sv.polinc), sv.numapp)));
			compute(premiumrec.calcPrem, 3).setRounded((div(mult(premiumrec.calcPrem, sv.numapp), sv.polinc)));
		}
		/* Put possibly calculated sum insured back on the screen.*/
		if (plan.isTrue()) {
			compute(sv.sumin, 3).setRounded((div(mult(premiumrec.sumin, sv.numapp), sv.polinc)));
		}
		else {
			sv.sumin.set(premiumrec.sumin);
		}
		if (stampDutyflag) {
			compute(premiumrec.calcPrem, 3).set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
			sv.zstpduty01.set(premiumrec.zstpduty01);
		}
		sv.zbinstprem.set(premiumrec.calcBasPrem);
		sv.zlinstprem.set(premiumrec.calcLoaPrem);
		wsaaCommissionPrem.set(premiumrec.commissionPrem);
		/*BRD-306 START */
		sv.loadper.set(premiumrec.loadper);
		sv.adjustageamt.set(premiumrec.adjustageamt);
		sv.rateadj.set(premiumrec.rateadj);
		sv.fltmort.set(premiumrec.fltmort);
		sv.premadj.set(premiumrec.premadj);
		
		
		/*BRD-306 END */
		/* Having calculated it, the  entered value, if any, is compared*/
		/* with it to check that  it  is within acceptable limits of the*/
		/* automatically calculated figure.  If  it  is  less  than  the*/
		/* amount calculated and  within  tolerance  then  the  manually*/
		/* entered amount is allowed.  If  the entered value exceeds the*/
		/* calculated one, the calculated value is used.*/
		if (isEQ(sv.instPrem, 0)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		/* If premium is set to manual entry, retain the entered amount.   */
		if (isGT(sv.instPrem, 0)
		&& isEQ(wsaaPremind, "Y")) {
			goTo(GotoLabel.exit2790);
		}
		if (isGTE(sv.instPrem, premiumrec.calcPrem)) {
			sv.instPrem.set(premiumrec.calcPrem);
			goTo(GotoLabel.exit2790);
		}
		/* check the tolerance amount against the table entry read above.*/
		compute(wsaaDiff, 2).set(sub(premiumrec.calcPrem, sv.instPrem));
		sv.instPremErr.set(errorsInner.f254);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 11)
		|| isEQ(sv.instPremErr, SPACES)); wsaaSub.add(1)){
			searchForTolerance2740();
		}
		/* If the premium is within the tolerance limits, then we have to  */
		/* adjust the basic premium (the loaded premium will not change).  */
		if (isEQ(sv.instPremErr, SPACES)) {
			if(stampDutyflag){
				compute(sv.zbinstprem, 2).set(sub(sub(sv.instPrem, sv.zlinstprem),sv.zstpduty01));
			}else{
				compute(sv.zbinstprem, 2).set(sub(sv.instPrem, sv.zlinstprem));
		}
		goTo(GotoLabel.exit2790);
	}
}
/*ILIFE-2808 Code Promotion for VPMS externalization of LIFE RTA product calculations Start*/
protected void hpadRead100()
{
	/*HPAD*/
	/* READ OFF HPAD FILE TO RETRIEVE THE PROPOSE DATE (HPAD-PROPDTE)*/
	/* USED TO ACCESS PREMIUM RATE FOR TABLE TH617*/
	
	hpadIO=hpadpfDAO.getHpadData(premiumrec.chdrChdrcoy.toString(), premiumrec.chdrChdrnum.toString());
	/*EXIT*/
}	
/*Ticket #ILIFE-2808 - End*/
	/**
	* <pre>
	***  Calculate tolerance Limit and Check whether it is greater
	***  than the maximum tolerance amount in table T5667.
	* </pre>
	*/
protected void searchForTolerance2740()
	{
		/*IF CHDRLNB-BILLFREQ = T5667-FREQ (WSAA-SUB)                 */
		if (isEQ(payrpf.getBillfreq(), t5667rec.freq[wsaaSub.toInt()])) {
			compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtol[wsaaSub.toInt()])), 100));
			if (isLTE(wsaaDiff, wsaaTol)
			&& isLTE(wsaaDiff, t5667rec.maxAmount[wsaaSub.toInt()])) {
				sv.instPremErr.set(SPACES);
				return ;
			}
			if (isNE(t5667rec.maxamt[wsaaSub.toInt()], 0)) {
				compute(wsaaTol, 3).setRounded(div((mult(premiumrec.calcPrem, t5667rec.prmtoln[wsaaSub.toInt()])), 100));
				if (isLTE(wsaaDiff, wsaaTol)
				&& isLTE(wsaaDiff, t5667rec.maxamt[wsaaSub.toInt()])) {
					sv.instPremErr.set(SPACES);
				}
			}
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			loadWsspFields3010();
			checkCredits3020();
			lextpf3010CustomerSpecific();
			//ILIFE-3421:Start
			if(premiumflag || dialdownFlag){
				insertAndUpdateRcvdpf();
			}
			//IBPLIFE-2132
			if(contnewBFlag){
//				if (isEQ(wsspcomn.flag, "M")) {
//					covppf=covppfDAO.getCovppfCrtable(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(), 
//							covtlnbIO.getCrtable().toString(),covtlnbIO.getCoverage().toString(),covtlnbIO.getRider().toString());
//					if(covppf != null){
//					if (isNE(sv.covrprpse, covppf.getCovrprpse())){
//						covppf.setCovrprpse(sv.covrprpse.toString());
//						if (isEQ(wsaaToday, 0)) {
//					        datcon1rec.function.set(varcom.tday);
//					        Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
//					        wsaaToday.set(datcon1rec.intDate);
//					    }
//						covppf.setCoverage(sv.coverage.toString());
//						covppf.setRider(sv.rider.toString());
//					    covppf.setEffdate(wsaaToday.toInt());
//					    covppf.setDatime(new Timestamp(new Date().getTime()));
//					    covppfDAO.updateCovppfCrtable(covppf);
//					}
//				}
//				}
				if (isEQ(wsspcomn.flag, "I")) {
					covppf=covppfDAO.getCovppfCrtable(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(), covtlnbIO.getCrtable().toString());
				}
				if (isEQ(wsspcomn.flag, "C") || isEQ(wsspcomn.flag, "M") ||  isEQ(wsspcomn.flag, "X")){
					setCovppf();
					Covppf covppftemp=covppfDAO.getCovppfCrtable(covtlnbIO.getChdrcoy().toString(), covtlnbIO.getChdrnum().toString(), 
							covtlnbIO.getCrtable().toString(),covtlnbIO.getCoverage().toString(),covtlnbIO.getRider().toString());
					if(null==covppftemp)
						covppfDAO.insertCovppfRecord(covppf);
					else {
						covppf.setUniqueNumber(covppftemp.getUniqueNumber());
						covppfDAO.updateCovppfCrtable(covppf); 
					}
				}
			}
			//ILIFE-3421:End
			getUnderwriting3030();
			delZcvrRecrds3040CustomerSpecific();
			if (isFollowUpRequired) {
				fluppfAvaList = fluppfDAO.searchFlupRecordByChdrNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
				fupno = fluppfAvaList.size();	//ICIL-1494
				createFollowUps3400();
			}
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

//IBPLIFE-2132
public void setCovppf(){
	covppf = new Covppf();
	covppf.setChdrcoy(covtlnbIO.getChdrcoy().toString());
	covppf.setChdrnum(covtlnbIO.getChdrnum().toString());
	covppf.setChdrpfx(chdrlnbIO.getChdrpfx().toString());
	covppf.setCovrprpse(sv.covrprpse.toString());
	covppf.setDatime(new Timestamp(new Date().getTime()));
	covppf.setEffdate(sv.dateeff.toString().trim().equals("")?wsaaToday.toInt():sv.dateeff.toInt());
	covppf.setCoverage(covtlnbIO.getCoverage().toString());
	covppf.setRider(covtlnbIO.getRider().toString());
	covppf.setJobnm(appVars.getLoggedOnUser());
	covppf.setUsrprf(wsspcomn.username.toString());
	covppf.setTranCode(sv.trcode.toString().trim().equals("")?wsaaBatckey.batcBatctrcde.toString():sv.trcode.toString());
	covppf.setValidflag("1");
	covppf.setCrtable(covtlnbIO.getCrtable().toString());
	
	
}

protected void createFollowUps3400()
{
	try {
		writeFollowUps3430();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void writeFollowUps3430()
{
	for (wsaaCount.set(1); !(isGT(wsaaCount, 5)) && isNE(ta610rec.fupcdes[wsaaCount.toInt()], SPACES); wsaaCount.add(1)) {
		boolean entryFlag = false;
		//ILIFE-9209 STARTS
		if(fluppfAvaList!=null && !fluppfAvaList.isEmpty()) {
			for (Fluppf fluppfdata : fluppfAvaList) {
				if(isEQ(fluppfdata.getFupCde(),ta610rec.fupcdes[wsaaCount.toInt()])) 
					entryFlag=true;
			}
				
		}
		//ILIFE-9209 ENDS
		if(!entryFlag) {		//ILIFE-9209 
		writeFollowUp3500();
		}
	}

fluppfDAO.insertFlupRecord(fluplnbList);
}

protected void writeFollowUp3500()
{
	try {		
		fluppf = new Fluppf();
		lookUpStatus3510();
		lookUpDescription3520();
		writeRecord3530();
	}
	catch (Exception e){
		/* Expected exception for control flow purposes. */
	}
}

protected void lookUpStatus3510() {
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(ta610rec.fupcdes[wsaaCount.toInt()]);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (null == itempf) {
			scrnparams.errorCode.set(errorsInner.e557);
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	}

protected void lookUpDescription3520()
{ 
	descpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());

}

protected void writeRecord3530()
{
	fupno++;
	
	fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
	fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
	setPrecision(fupno, 0);
	fluppf.setFupNo(fupno);
	fluppf.setLife("01");
	fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
	fluppf.setFupDt(wsaaToday.toInt());
	fluppf.setFupCde(ta610rec.fupcdes[wsaaCount.toInt()].toString());
	fluppf.setFupTyp('P');
	fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
	fluppf.setFupRmk(descpf.getLongdesc()); /* IJTI-1479 */
	fluppf.setjLife("00");
	fluppf.setTrdt(varcom.vrcmDate.toInt());
	fluppf.setTrtm(varcom.vrcmTime.toInt());
	fluppf.setUserT(varcom.vrcmUser.toInt());
	fluppf.setEffDate(wsaaToday.toInt());
	fluppf.setCrtDate(wsaaToday.toInt());
	fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
	fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
	fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
	fluplnbList.add(fluppf);
}

protected void loadWsspFields3010()
	{
		/*      SPECIAL EXIT PROCESSING*/
		/* Skip this section  if  returning  from  an optional selection*/
		/* (current stack position action flag = '*').*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			goTo(GotoLabel.exit3490);
		}
		/* Updating occurs with  the Creation, Deletion or Updating of a*/
		/* COVTTRM transaction record.*/
		/* If the 'KILL' function key was pressed or if in enquiry mode,*/
		/* skip the updating.*/
		if (isEQ(scrnparams.statuz, "KILL")
		|| forcedKill.isTrue()) {
			goTo(GotoLabel.exit3490);
		}
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3490);
		}
		/* If premium breakdown selected then exit....                     */
		if (isEQ(sv.pbind, "X")) {
			goTo(GotoLabel.exit3490);
		}
		/* If OPTIONS / EXTRAS selected do not write / update or delete*/
		/* record at this stage.*/
		/* Move the Premium Term to WSSP area to be used in linkage   <016>*/
		/*                                                            <016>*/
		/* IF S5123-OPTEXTIND          = 'X'                            */
		/*    MOVE S5123-PREM-CESS-TERM TO WSSP-BIG-AMT            <016>*/
		/*    GO TO 3490-EXIT.                                          */
		if (isEQ(sv.optextind, "X")) {
			if (isNE(sv.premCessTerm, 0)) {
				wssplife.bigAmt.set(sv.premCessTerm);
				/*       GO TO 3490-EXIT                                <RA9606>*/
				if (isNE(sv.ratypind, "X")) {
					goTo(GotoLabel.exit3490);
				}
			}
			else {
				datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
				datcon3rec.intDate2.set(sv.premCessDate);
				callDatcon33600();
				wsaaFreqFactor.set(datcon3rec.freqFactor);
				wssplife.fupno.set(wsaaFreqFactor);
				/*          GO TO 3490-EXIT                             <RA9606>*/
				if (isNE(sv.ratypind, "X")) {
					goTo(GotoLabel.exit3490);
				}
			}
		}
		/* If TAXIND is selected, do not write / update or delete record   */
		if (isEQ(sv.taxind, "X")) {
			goTo(GotoLabel.exit3490);
		}
	}

protected void checkCredits3020()
	{
		/* Before  updating any  records,  calculate  the  new  "credit"*/
		/* amount.*/
		/*  - add to  the  current  credit  amount,  the  number  of*/
		/*       policies previously  applicable  from  the  COVTTRM*/
		/*       record (zero if  there was no COVTTRM) and subtract*/
		/*       the number applicable entered on the screen.*/
		/*  - a positive  credit means that additional policies must*/
		/*       be added to the plan.*/
		/*  - a negative  credit  means  that  some policies must be*/
		/*       removed from the plan.*/
		if (nonfirst.isTrue()) {
			wsaaCredit.add(covttrmIO.getNumapp());
		}
		wsaaCredit.subtract(sv.numapp);
		/* If the number  applicable  is greater than zero, write/update*/
		/* the COVTTRM record.  If the number applicable is zero, delete*/
		/* the COVTTRM record.*/
		if (isGT(sv.numapp, 0)) {
			if (firsttime.isTrue()) {
				initcovr3500();
				setupcovttrm3550();
				covttrmIO.setFunction(varcom.writr);
				initundc3750();
				undcIO.setFunction(varcom.writr);
				wsaaCtrltime.set("N");
			}
			else {
				setupcovttrm3550();
				/*       MOVE UPDAT            TO COVTTRM-FUNCTION.             */
				covttrmIO.setFunction(varcom.updat);
				undcIO.setFunction(varcom.updat);
			}
		}
		/*IF S5123-NUMAPP             = 0                              */
		/*   IF COVTTRM-NUMAPP = 0                                     */
		/*      GO TO 3490-EXIT                                        */
		/*   ELSE                                                      */
		/*      MOVE COVTLNB-CHDRCOY  TO COVTTRM-CHDRCOY               */
		/*      MOVE COVTLNB-CHDRNUM  TO COVTTRM-CHDRNUM               */
		/*      MOVE COVTLNB-LIFE     TO COVTTRM-LIFE                  */
		/*      MOVE COVTLNB-COVERAGE TO COVTTRM-COVERAGE              */
		/*      MOVE COVTLNB-RIDER    TO COVTTRM-RIDER                 */
		/*      MOVE READH            TO COVTTRM-FUNCTION              */
		/*      CALL 'COVTTRMIO' USING  COVTTRM-PARAMS                 */
		/*      IF COVTTRM-STATUZ     NOT = O-K                        */
		/*         MOVE COVTTRM-PARAMS TO SYSR-PARAMS                  */
		/*         PERFORM 600-FATAL-ERROR                             */
		/*      ELSE                                                   */
		/*         MOVE DELET         TO COVTTRM-FUNCTION              */
		/*         IF WSAA-FIRST-SEQNBR = COVTTRM-SEQNBR               */
		/*            MOVE ZERO      TO WSAA-FIRST-SEQNBR.             */
		if (isLTE(sv.numapp, 0)) {
			covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
			covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
			covttrmIO.setLife(covtlnbIO.getLife());
			covttrmIO.setCoverage(covtlnbIO.getCoverage());
			covttrmIO.setRider(covtlnbIO.getRider());
			covttrmIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, covttrmIO);
			if (isNE(covttrmIO.getStatuz(), varcom.oK)
			&& isNE(covttrmIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(covttrmIO.getParams());
				fatalError600();
			}
			else {
				if (isEQ(covttrmIO.getStatuz(), varcom.mrnf)) {
					goTo(GotoLabel.exit3490);
				}
				else {
					covttrmIO.setFunction(varcom.delet);
					if (isEQ(wsaaFirstSeqnbr, covttrmIO.getSeqnbr())) {
						wsaaFirstSeqnbr.set(0);
					}
				}
			}
		}
		updateCovttrm3700();
		if (isEQ(t5687rec.zsredtrm, "Y")) {
			m800ReadMrta();
			//BRD-139:Start
			if(mrtaPermission
			 ||(isNE(wsaaPremind, "Y"))){
		//	if (isNE(wsaaPremind, "Y")) {
				m300GetMethod();
				rlrdtrmrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
				rlrdtrmrec.chdrnum.set(chdrlnbIO.getChdrnum());
				rlrdtrmrec.life.set(covtlnbIO.getLife());
				rlrdtrmrec.coverage.set(covtlnbIO.getCoverage());
				rlrdtrmrec.rider.set(covtlnbIO.getRider());
				rlrdtrmrec.sumins.set(sv.sumin);
				rlrdtrmrec.effdate.set(covttrmIO.getEffdate());
				rlrdtrmrec.loandur.set(mrtaIO.getLoandur());
				datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
				datcon3rec.intDate2.set(sv.riskCessDate);
				callDatcon32700();				
				rlrdtrmrec.polilcydur.set(datcon3rec.freqFactor);

				if (isEQ(sv.riskCessTerm, 0)) {
					compute(rlrdtrmrec.riskTerm, 0).set(sub(sv.riskCessAge, sv.anbAtCcd));
				}
				else {
					rlrdtrmrec.riskTerm.set(sv.riskCessTerm);
				}
				m500DeleteBenefitSchedule();
				m400CalcBenefitSchedule();
			}
		}
	}

protected void getUnderwriting3030()
	{
		/* If this proposal requires underwriting, determine the Age       */
		/* to use for the Underwriting based on Age and SA.                */
		if (underwritingReqd.isTrue()) {
			calcUndwAge3a00();
		}
		/* Before  updating any  records,  determine the Underwriting      */
		/* Rule for the Component.                                         */
		if (underwritingReqd.isTrue()
		&& isGT(sv.sumin, 0)) {
			getUndwRule3900();
		}
		if (isGT(sv.sumin, 0)
		&& underwritingReqd.isTrue()
		&& isEQ(wsaaT6768Found, "Y")) {
			updateUndc3800();
		}
	}

	/**
	* <pre>
	*    PERFORMED ROUTINES SECTION.   *
	* </pre>
	*/
protected void initcovr3500()
	{
		/*PARA*/
		/* Before creating a  new COVTTRM record initialise the coverage*/
		/* fields.*/
		covttrmIO.setCrtable(covtlnbIO.getCrtable());
		covttrmIO.setPayrseqno(1);
		covttrmIO.setRiskCessAge(0);
		covttrmIO.setSingp(0);
		covttrmIO.setInstprem(0);
		covttrmIO.setZbinstprem(0);
		covttrmIO.setZlinstprem(0);
		covttrmIO.setPremCessAge(0);
		covttrmIO.setRiskCessTerm(0);
		covttrmIO.setPremCessTerm(0);
		covttrmIO.setBenCessAge(0);
		covttrmIO.setBenCessTerm(0);
		covttrmIO.setSumins(0);
		covttrmIO.setRiskCessDate(varcom.vrcmMaxDate);
		covttrmIO.setPremCessDate(varcom.vrcmMaxDate);
		covttrmIO.setBenCessDate(varcom.vrcmMaxDate);
		covttrmIO.setEffdate(varcom.vrcmMaxDate);
		//added for ILIFE-604
		covttrmIO.setPlanSuffix(0);
		/*EXIT*/
	}

protected void setupcovttrm3550()
	{
		para3550();
	}

protected void para3550()
	{
		/* Check for changes in the COVTTRM Record.*/
		wsaaUpdateFlag.set("N");
		if (isNE(sv.riskCessDate, covttrmIO.getRiskCessDate())) {
			covttrmIO.setRiskCessDate(sv.riskCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessDate, covttrmIO.getPremCessDate())) {
			covttrmIO.setPremCessDate(sv.premCessDate);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessAge, covttrmIO.getRiskCessAge())) {
			covttrmIO.setRiskCessAge(sv.riskCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessAge, covttrmIO.getPremCessAge())) {
			covttrmIO.setPremCessAge(sv.premCessAge);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.riskCessTerm, covttrmIO.getRiskCessTerm())) {
			covttrmIO.setRiskCessTerm(sv.riskCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.premCessTerm, covttrmIO.getPremCessTerm())) {
			covttrmIO.setPremCessTerm(sv.premCessTerm);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.sumin, covttrmIO.getSumins())) {
			covttrmIO.setSumins(sv.sumin);
			wsaaUpdateFlag.set("Y");
		}
		/*IF CHDRLNB-BILLFREQ         = '00'                      <001>*/
		if (isEQ(payrpf.getBillfreq(), "00")) {
			if (isNE(sv.instPrem, covttrmIO.getSingp())) {
				covttrmIO.setSingp(sv.instPrem);
				wsaaUpdateFlag.set("Y");
			}
		}
		else {
			if (isNE(sv.instPrem, covttrmIO.getInstprem())) {
				covttrmIO.setInstprem(sv.instPrem);
				wsaaUpdateFlag.set("Y");
			}
		}
		if (isNE(sv.zbinstprem, covttrmIO.getZbinstprem())) {
			covttrmIO.setZbinstprem(sv.zbinstprem);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zlinstprem, covttrmIO.getZlinstprem())) {
			covttrmIO.setZlinstprem(sv.zlinstprem);
			/*BRD-306 START */
			covttrmIO.setLoadper(sv.loadper);
			covttrmIO.setRateadj(sv.rateadj);
			covttrmIO.setFltmort(sv.fltmort);
			covttrmIO.setPremadj(sv.premadj);
			covttrmIO.setAgeadj(sv.adjustageamt);
			/*BRD-306 END */
			wsaaUpdateFlag.set("Y");
		}
		
		if(isNE(wsaaCommissionPrem,ZERO)){
			covttrmIO.setCommPrem(wsaaCommissionPrem);    //IBPLIFE-5237
			wsaaUpdateFlag.set("Y");
		}
		
		if (isNE(sv.mortcls, covttrmIO.getMortcls())) {
			covttrmIO.setMortcls(sv.mortcls);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.liencd, covttrmIO.getLiencd())) {
			covttrmIO.setLiencd(sv.liencd);
			wsaaUpdateFlag.set("Y");
		}
		/*ILIFE-7118-starts*/
		if (isNE(sv.tpdtype, covttrmIO.getTpdtype())) {
			covttrmIO.setTpdtype(sv.tpdtype);
			wsaaUpdateFlag.set("Y");
		}
		/*ILIFE-7118-ends*/
		if ((isEQ(sv.select, "J")
		&& (isEQ(covttrmIO.getJlife(), "00")
		|| isEQ(covttrmIO.getJlife(), "  ")))
		|| ((isEQ(sv.select, " ")
		|| isEQ(sv.select, "L"))
		&& isEQ(covttrmIO.getJlife(), "01"))) {
			wsaaUpdateFlag.set("Y");
			if (isEQ(sv.select, "J")) {
				covttrmIO.setJlife("01");
			}
			else {
				covttrmIO.setJlife("00");
			}
		}
		if (isNE(sv.polinc, covttrmIO.getPolinc())) {
			covttrmIO.setPolinc(sv.polinc);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.numapp, covttrmIO.getNumapp())) {
			covttrmIO.setNumapp(sv.numapp);
			wsaaUpdateFlag.set("Y");
		}
		/*IF CHDRLNB-BILLFREQ         NOT = COVTTRM-BILLFREQ           */
		/*   MOVE CHDRLNB-BILLFREQ    TO COVTTRM-BILLFREQ              */
		if (isNE(payrpf.getBillfreq(), covttrmIO.getBillfreq())) {
			covttrmIO.setBillfreq(payrpf.getBillfreq());
			wsaaUpdateFlag.set("Y");
		}
		/*IF CHDRLNB-BILLCHNL         NOT = COVTTRM-BILLCHNL           */
		/*   MOVE CHDRLNB-BILLCHNL    TO COVTTRM-BILLCHNL              */
		if (isNE(payrpf.getBillchnl(), covttrmIO.getBillchnl())) {
			covttrmIO.setBillchnl(payrpf.getBillchnl());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(chdrlnbIO.getOccdate(), covttrmIO.getEffdate())) {
			covttrmIO.setEffdate(chdrlnbIO.getOccdate());
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaAnbAtCcd, covttrmIO.getAnbccd(1))) {
			covttrmIO.setAnbccd(1, wsaaAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsaaSex, covttrmIO.getSex(1))) {
			covttrmIO.setSex(1, wsaaSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(wsbbAnbAtCcd, covttrmIO.getAnbccd(2))) {
			covttrmIO.setAnbccd(2, wsbbAnbAtCcd);
			wsaaUpdateFlag.set("Y");
		}
		/* IF WSAA-SEX                 NOT = COVTTRM-SEX(2)             */
		if (isNE(wsbbSex, covttrmIO.getSex(2))) {
			covttrmIO.setSex(2, wsbbSex);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.bappmeth, covttrmIO.getBappmeth())) {
			covttrmIO.setBappmeth(sv.bappmeth);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.zstpduty01, covttrmIO.getZstpduty01())&& stampDutyflag) {
			covttrmIO.setZstpduty01(sv.zstpduty01);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(stateCode, covttrmIO.getZclstate())&& stampDutyflag) {
			covttrmIO.setZclstate(stateCode);
			wsaaUpdateFlag.set("Y");
		}
		/*ILIFE-6941 start*/
		if (isNE(sv.lnkgno, covttrmIO.getLnkgno())) {
			covttrmIO.setLnkgno(sv.lnkgno);
			wsaaUpdateFlag.set("Y");
		}
		if (isNE(sv.lnkgsubrefno, covttrmIO.getLnkgsubrefno())) {
			covttrmIO.setLnkgsubrefno(sv.lnkgsubrefno);
			wsaaUpdateFlag.set("Y");
		}
		/*ILIFE-6941 end*/
		
		if (isNE(sv.lnkgind, covttrmIO.getLnkgind())) {
			covttrmIO.setLnkgind(sv.lnkgind);
			wsaaUpdateFlag.set("Y");
		}
		
		covttrmIO.setCntcurr(chdrlnbIO.getCntcurr());
	}

protected void callDatcon33600()
	{
		/*PARA*/
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateCovttrm3700()
	{
		try {
			para3700();
			para3710();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para3700()
	{
		if (isEQ(wsaaUpdateFlag, "N")) {
			goTo(GotoLabel.exit3790);
		}
	}

protected void para3710()
	{
		covttrmIO.setBenCessAge(0);
		covttrmIO.setBenCessTerm(0);
		covttrmIO.setBenCessDate(varcom.vrcmMaxDate);
		covttrmIO.setTermid(varcom.vrcmTermid);
		covttrmIO.setUser(varcom.vrcmUser);
		covttrmIO.setTransactionDate(varcom.vrcmDate);
		covttrmIO.setTransactionTime(varcom.vrcmTime);
		covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
		covttrmIO.setLife(covtlnbIO.getLife());
		covttrmIO.setCoverage(covtlnbIO.getCoverage());
		covttrmIO.setRider(covtlnbIO.getRider());
		covttrmIO.setRiskprem(premiumrec.riskPrem);//ILIFE-7845
		covttrmIO.setFormat(formatsInner.covttrmrec);
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		
		updtpurepremCustomerSpecific();
	}

protected void initundc3750()
	{
		/*PARA*/
		/* Before creating a  new Underwriting Component record            */
		/* initialize the fields.                                          */
		if (isEQ(sv.sumin, 0)) {
			return ;
		}
		undcIO.setChdrnum(0);
		undcIO.setChdrcoy(0);
		undcIO.setLife(0);
		undcIO.setCoverage(0);
		undcIO.setRider(0);
		undcIO.setCurrfrom(0);
		undcIO.setCurrto(varcom.vrcmMaxDate);
		undcIO.setUndwrule(SPACES);
		undcIO.setSpecind(SPACES);
		undcIO.setTranno(1);
		undcIO.setValidflag("0");
		/*EXIT*/
	}

protected void updateUndc3800()
	{
		para3810();
	}

protected void para3810()
	{
		undcIO.setChdrcoy(covttrmIO.getChdrcoy());
		undcIO.setChdrnum(covttrmIO.getChdrnum());
		undcIO.setLife(covttrmIO.getLife());
		undcIO.setJlife(covttrmIO.getJlife());
		undcIO.setCoverage(covttrmIO.getCoverage());
		undcIO.setRider(covttrmIO.getRider());
		undcIO.setCurrfrom(covttrmIO.getEffdate());
		undcIO.setCurrto(varcom.vrcmMaxDate);
		undcIO.setUndwrule(wsaaUndwrule);
		undcIO.setSpecind(SPACES);
		undcIO.setTranno(1);
		undcIO.setValidflag("3");
		undcIO.setFormat(formatsInner.undcrec);
		SmartFileCode.execute(appVars, undcIO);
		if (isNE(undcIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undcIO.getParams());
			syserrrec.statuz.set(undcIO.getStatuz());
			fatalError600();
		}
	}

protected void getUndwRule3900()
	{
		start3910();
	}

	/**
	* <pre>
	* For the given Sum Assured and Age, read the Underwriting        
	* based on Age & SA table T6768.                                  
	* Read T6768 with a key of CRTABLE, CURRENCY and SEX, for the     
	* given coverage.                                                 
	* If the Proposal is a joint-life case, the table will be read    
	* with just CRTABLE and CURRENCY.                                 
	* T6768 holds a matrix of Underwriting rules held in a            
	* two-dimensional table with  Age as the vertical ('y') axis and  
	* SA as the horizontal ('x') axis.                                
	* Locate  the occurrence of Age that is greater than or equal to  
	* the  Age  Next  Birthday.                                       
	* Locate the occurrence of Term that is greater  than  or  equal  
	* to the Term.                                                    
	* Locate the appropriate method.                                  
	* </pre>
	*/
protected void start3910()
	{
		wsaaT6768Crtable.set(covtlnbIO.getCrtable());
		wsaaT6768Curr.set(chdrlnbIO.getCntcurr());
		if (singlif.isTrue()) {
			wsaaT6768Sex.set(wsaaSex);
		}
		else {
			wsaaT6768Sex.set(SPACES);
		}
		/* Read T6768 to determine the underwriting rule based on the age  */
		/* of the life and the sum assured. This is not a straightforward  */
		/* read due to the table having two continuation items.            */
		wsaaItemtabl.set(tablesInner.t6768);
		wsaaItemitem.set(wsaaT6768Key);
		/* Work out the array's Y index based on AGE                       */
		wsaaYa.set(0);
		wsaaZa.set(0);
		wsaaYaFlag.set("N");
		while ( !(yaFound.isTrue()
		|| t6768NotFound.isTrue())) {
			determineYa3e00();
		}
		
		/* If the key does not exist on T6768 there will be no             */
		/* Age/Sum Assured underwriting rule.                              */
		if (t6768NotFound.isTrue()) {
			wsaaUndwrule.set(SPACES);
			return ;
		}
		/* Work out the array's X index based on SUMINS (WSAA-XA).         */
		wsaaXa.set(0);
		wsaaXaFlag.set("N");
		while ( !(xaFound.isTrue())) {
			determineXa3f00();
		}
		
		/* Now we have the correct position(WSAA-XA, WSAA-YA) of the RULE. */
		/* All we have to do now is to work out the corresponding position */
		/* in the copybook by using these co-ordinates.(The copybook is    */
		/* defined as one-dimensional.)                                    */
		wsaaYa.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaYa, 5));
		wsaaIndex.add(wsaaXa);
		wsaaUndwrule.set(t6768rec.undwrule[wsaaIndex.toInt()]);
	}

protected void calcUndwAge3a00()
	{
		start3a10();
	}

protected void start3a10()
	{
		/* If the coverage/rider                                           */
		/* is joint-life case, determine the adjusted                      */
		/* combined age, using T5585.                                      */
		/* If coverage/rider is applicable to Life                         */
		if (isEQ(sv.select, "L")
		|| isEQ(sv.select, " ")) {
			wsaaUndwAge.set(wsaaAnbAtCcd);
			return ;
		}
		/* If coverage/rider is applicable to Joint Life                   */
		if (isEQ(sv.select, "J")) {
			wsaaUndwAge.set(wsbbAnbAtCcd);
			return ;
		}
		/* Must be Joint-life so get combined age                          */
		/* Read T5585 to get Joint Life Age parameters                     */
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
		itempf.setItemtabl(tablesInner.t5585.toString());
		itempf.setItemitem(covttrmIO.getCrtable().toString());
		itempf.setItmfrm(covttrmIO.getEffdate().getbigdata());
		itempf.setItmto(covttrmIO.getEffdate().getbigdata());
		itempfList = itempfDAO.findByItemDates(itempf);
		if (itempfList.size()==0) {
			syserrrec.params.set(covtlnbIO.getCrtable());
			syserrrec.statuz.set(errorsInner.f261);
			fatalError600();
		}
		t5585rec.t5585Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		wsaaSub.set(0);
		/* Adjust the lives                                                */
		wsaaSub00.set(0);
		wsaaSub01.set(0);
		wsaaAge00Adjusted.set(wsaaAnbAtCcd);
		wsaaAge01Adjusted.set(wsbbAnbAtCcd);
		if (isEQ(wsaaSex, t5585rec.sexageadj)) {
			adjustAge003b00();
		}
		if (isEQ(wsbbSex, t5585rec.sexageadj)) {
			adjustAge013c00();
		}
		compute(wsaaAgeDifference, 0).set(sub(wsaaAge00Adjusted, wsaaAge01Adjusted));
		wsaaSub.set(0);
		ageDifferance3d00();
		wsaaUndwAge.set(wsaaAdjustedAge);
	}

protected void adjustAge003b00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust3b10: 
					adjust3b10();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust3b10()
	{
		wsaaSub00.add(1);
		if (isGT(wsaaSub00, 9)) {
			syserrrec.statuz.set(errorsInner.h043);
			return ;
		}
		if (isLTE(wsaaAnbAtCcd, t5585rec.agelimit[wsaaSub00.toInt()])) {
			compute(wsaaAge00Adjusted, 0).set(add(wsaaAnbAtCcd, t5585rec.ageadj[wsaaSub00.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust3b10);
		}
		/*B90-EXIT*/
	}

protected void adjustAge013c00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case adjust3c10: 
					adjust3c10();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void adjust3c10()
	{
		wsaaSub01.add(1);
		if (isGT(wsaaSub01, 9)) {
			syserrrec.statuz.set(errorsInner.h043);
			return ;
		}
		if (isLTE(wsbbAnbAtCcd, t5585rec.agelimit[wsaaSub01.toInt()])) {
			compute(wsaaAge01Adjusted, 0).set(add(wsbbAnbAtCcd, t5585rec.ageadj[wsaaSub01.toInt()]));
		}
		else {
			goTo(GotoLabel.adjust3c10);
		}
		/*C90-EXIT*/
	}

protected void ageDifferance3d00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
				case go3d10: 
					go3d10();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void go3d10()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub, 18)) {
			premiumrec.statuz.set(errorsInner.f262);
			return ;
		}
		if (isGT(wsaaAgeDifference, t5585rec.agedif[wsaaSub.toInt()])) {
			goTo(GotoLabel.go3d10);
		}
		if (isEQ(t5585rec.hghlowage, "H")) {
			if (isGT(wsaaAge00Adjusted, wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge01Adjusted));
			}
		}
		if (isEQ(t5585rec.hghlowage, "L")) {
			if (isLTE(wsaaAge00Adjusted, wsaaAge01Adjusted)) {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge00Adjusted));
			}
			else {
				compute(wsaaAdjustedAge, 0).set(add(t5585rec.addage[wsaaSub.toInt()], wsaaAge01Adjusted));
			}
		}
	}

protected void determineYa3e00()
	{
		init3e10();
	}

protected void init3e10()
	{
		x100ReadItdm();
		if (t6768NotFound.isTrue()) {
			return ;
		}
		for (wsaaZa.set(1); !(isGT(wsaaZa, 10)); wsaaZa.add(1)){
			if (isLTE(wsaaUndwAge, t6768rec.undage[wsaaZa.toInt()])) {
				wsaaYa.set(wsaaZa);
				wsaaZa.set(11);
				yaFound.setTrue();
			}
		}
		if (yaFound.isTrue()) {
			return ;
		}
		/* If the age does not fall into the range specified and an age    */
		/* continuation item exists, read T6768 again using the            */
		/* continuation item as the key.                                   */
		if (isGT(wsaaZa, 10)) {
			if (isNE(t6768rec.agecont, SPACES)) {
				wsaaItemitem.set(t6768rec.agecont);
			}
			else {
				syserrrec.statuz.set(errorsInner.e390);
				fatalError600();
			}
		}
	}

protected void determineXa3f00()
	{
		init3f10();
	}

protected void init3f10()
	{
		for (wsaaZa.set(1); !(isGT(wsaaZa, 5)); wsaaZa.add(1)){
			if (isLTE(sv.sumin, t6768rec.undsa[wsaaZa.toInt()])) {
				wsaaXa.set(wsaaZa);
				wsaaZa.set(6);
				xaFound.setTrue();
			}
		}
		if (xaFound.isTrue()) {
			return ;
		}
		/* If the sum assured does not fall into the range specified and a */
		/* sum assured continuation item exists, read T6768 again using    */
		/* continuation item as the key.                                   */
		if (isGT(wsaaZa, 5)) {
			if (isNE(t6768rec.sacont, SPACES)) {
				wsaaItemitem.set(t6768rec.sacont);
				x100ReadItdm();
				t6768rec.t6768Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
			}
			else {
				syserrrec.statuz.set(errorsInner.e389);
				fatalError600();
			}
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		para4000();
	}

protected void para4000()
	{
		wsspcomn.nextprog.set(wsaaProg);
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		if (forcedKill.isTrue()) {
			wsaaKillFlag.set("N");
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			return ;
		}
		if (isEQ(sv.pbind, "X")) {
			pbindExe5100();
		}
		else {
			if (isEQ(sv.pbind, "?")) {
				pbindRet5200();
			}
			else {
				if (isEQ(sv.optextind, "X")) {
					optionsExe4500();
				}
				else {
					if (isEQ(sv.optextind, "?")) {
						optionsRet4600();
					}
					else {
						if (isEQ(sv.ratypind, "X")) {
							reassuranceExe4200();
						}
						else {
							if (isEQ(sv.ratypind, "?")) {
								reassuranceRet4300();
							}
							else {
								if (isEQ(sv.optsmode, "X")) {
									m100BenefitSchdExe();
								}
								else {
									if (isEQ(sv.optsmode, "?")) {
										m200BenefitSchdRet();
									}
									else {
										if (isEQ(sv.payflag, "X")) {
											m500InstalmentExe();
										}
										else {
											if (isEQ(sv.payflag, "?")) {
												m600InstalmentRet();
											}
											else {
												if (isEQ(sv.zsredtrm, "X")) {
													m700ReducingTerm();
												}
												else {
													if (isEQ(sv.zsredtrm, "?")) {
														m800ReducingTermRet();
													}
													else {
														if (isEQ(sv.taxind, "X")) {
															taxExe5600();
														}
														else {
															if (isEQ(sv.taxind, "?")) {
																taxRet5700();
															}
															else {
																if (isEQ(sv.exclind, "X")) {
																	exclExe6000();
																}
																else {
																	if (isEQ(sv.exclind, "?")) {
																		exclRet6100();
																	}else{
																		if (isEQ(sv.aepaydet, "X") && isEndMat) {
																			antcpEndwPayDet();
																			chdrenq();
																		}else{
																			if (isEQ(sv.aepaydet, "?") && isEndMat) {
																				antcpEndwPayDet1();
																				chdrenq();
																			}
																			else
																			{
																				if (isNE(aepaydet, "+") && isEndMat) {
																					antcpEndwPayDet();
																					chdrenq();
																				}
																			
																		
															
															            else {
																        if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], " ")) {
																	    wayout4700();
																                }
																                else {
																                }	wayin4800();
															            }
																			
																			}
																			
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
protected void exclExe6000(){
	chdrpf = new Chdrpf();
	chdrpf = chdrpfDAO.getCacheObject(chdrpf);
	if (null != chdrpf) {
		if(isNE(chdrlnbIO.getChdrnum(),chdrpf.getChdrnum())) {
			chdrpfDAO.deleteCacheObject(chdrpf);
		}
	}
	covrpf = new Covrpf();
	covrpf = covrpfDAO.getCacheObject(covrpf);
	if (null != covrpf) {
		if(isNE(covtlnbIO.getChdrnum(),covrpf.getChdrnum())) {
			covrpfDAO.deleteCacheObject(covrpf);
		}
	}
    chdrlnbIO.setFunction(varcom.keeps);
	chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
	SmartFileCode.execute(appVars, chdrlnbIO);
	if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(chdrlnbIO.getParams());
		syserrrec.statuz.set(chdrlnbIO.getStatuz());
		fatalError600();
	}
		covtlnbIO.setFunction(varcom.keeps);
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}	
	sv.exclind.set("?");
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
		save4510();
	}
		gensswrec.function.set("G");
		wsspcomn.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		wsspcomn.crtable.set(covtlnbIO.getCrtable().toString());
		
		if(isNE(wsspcomn.flag,"I")){
			tempFlag = wsspcomn.flag.toString();
			wsspcomn.flag.set("X");
			wsspcomn.cmode.set("PS");}
			else
				wsspcomn.cmode.set("IFE");
			gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	
}
protected void exclRet6100(){
	
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
		restore4610();
	}
	checkExcl();
	
	if(isEQ(wsspcomn.flag,"X"))
	{
		wsspcomn.flag.set(tempFlag);
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	wsspcomn.nextprog.set(scrnparams.scrname);
}

protected void antcpEndwPayDet(){
	/*    If the reassurance details have been selected,(value - 'X'), */
	/*    then set an asterisk in the program stack action field to    */
	/*    ensure that control returns here, set the parameters for     */
	/*    generalised secondary switching and save the original        */
	/*    programs from the program stack.                             */
	/*    Set up WSSP-CURRFORM for next programs.              <R96REA>*/
	wsspcomn.currfrom.set(chdrlnbIO.getOccdate());
	sv.aepaydet.set("?");
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
		save4510();
	}
	gensswrec.function.set("H");
	gensww4210();
	compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
	sub2.set(1);
	for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
		load4530();
	}
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
	wsspcomn.programPtr.add(1);
	
}
protected void antcpEndwPayDet1()
{
	/*START*/
	/* On return from this  request, the current stack "action" will   */
	/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
	/* handle the return from options and extras:                      */
	/* Note that to have selected this option in the first place then  */
	/* details must exist.......set the flag for re-selection.         */
	sv.aepaydet.set("+");
	aepaydet ="+";
	/*  - restore the saved programs to the program stack              */
	compute(sub1, 0).set(add(1, wsspcomn.programPtr));
	sub2.set(1);
	for (int loopVar22 = 0; !(loopVar22 == 8); loopVar22 += 1){
		restore4610();
	}
	/*  - blank  out  the  stack  "action"                             */
	wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
	/*       Set  WSSP-NEXTPROG to  the current screen                 */
	/*       name (thus returning to re-display the screen).           */
	wsspcomn.nextprog.set(scrnparams.scrname);
	/*EXIT*/
}



protected void reassuranceExe4200()
	{
		para4200();
	}

protected void para4200()
	{
		/*    If the reassurance details have been selected,(value - 'X'), */
		/*    then set an asterisk in the program stack action field to    */
		/*    ensure that control returns here, set the parameters for     */
		/*    generalised secondary switching and save the original        */
		/*    programs from the program stack.                             */
		/*    Set up WSSP-CURRFORM for next programs.              <R96REA>*/
		wsspcomn.currfrom.set(chdrlnbIO.getOccdate());
		sv.ratypind.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			save4510();
		}
		gensswrec.function.set("B");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 8); loopVar3 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

	/**
	* <pre>
	*   If a value has been placed in the GENS-FUNCTION then call     
	*   the generalised secondary switching module to obtain the      
	*   next 8 programs and load them into the program stack.         
	* </pre>
	*/
protected void gensww4210()
	{
		para4211();
	}

protected void para4211()
	{
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsaaBatckey.batcBatctrcde);
	/*	if(isEQ(gensswrec.function.getData().toString(),"H") && isEQ(wsspcomn.flag,"I"))
		{
			gensswrec.transact.set(T600);
		}*/
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)
		&& isNE(gensswrec.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/* If an entry on T1675 was not found by genswch redisplay the scre*/
		/* with an error and the options and extras indicator*/
		/* with its initial load value*/
		if (isEQ(gensswrec.statuz, varcom.mrnf)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			/*    MOVE V045                TO SCRN-ERROR-CODE          (011)*/
			scrnparams.errorCode.set(errorsInner.h093);
			wsspcomn.nextprog.set(scrnparams.scrname);
			if (lextpf==null) {
				sv.optextind.set(" ");
				return ;
			}
			else {
				sv.optextind.set("+");
				return ;
			}
		}
	}

protected void reassuranceRet4300()
	{
		/*PARA*/
		sv.ratypind.set("+");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar4 = 0; !(loopVar4 == 8); loopVar4 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*EXIT*/
	}

protected void m100BenefitSchdExe()
	{
		m100Start1();
	}

protected void m100Start1()
	{
		sv.optsmode.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar5 = 0; !(loopVar5 == 8); loopVar5 += 1){
			save4510();
		}
		gensswrec.function.set("C");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar6 = 0; !(loopVar6 == 8); loopVar6 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void m200BenefitSchdRet()
	{
		/*M200-START*/
		sv.optsmode.set("+");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar7 = 0; !(loopVar7 == 8); loopVar7 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*M200-EXIT*/
	}

protected void m500InstalmentExe()
	{
		m500Start();
	}

protected void m500Start()
	{
		sv.payflag.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar8 = 0; !(loopVar8 == 8); loopVar8 += 1){
			save4510();
		}
		gensswrec.function.set("D");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar9 = 0; !(loopVar9 == 8); loopVar9 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void m600InstalmentRet()
	{
		/*M600-START*/
		sv.payflag.set("+");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar10 = 0; !(loopVar10 == 8); loopVar10 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*M600-EXIT*/
	}

protected void m700ReducingTerm()
	{
		m700Start();
	}

protected void chdrenq()
{
	chdrenqIO.setChdrcoy(wsspcomn.company);
	chdrenqIO.setChdrnum(chdrlnbIO.getChdrnum());
	chdrenqIO.setCnttype(chdrlnbIO.getCnttype());
	chdrenqIO.setCntcurr(chdrlnbIO.getCntcurr());
	chdrenqIO.setCownnum(chdrlnbIO.getCownnum());
	chdrenqIO.setFunction(varcom.readr);
	chdrenqIO.setFormat(chdrenqrec);
	SmartFileCode.execute(appVars, chdrenqIO);
	if (isNE(chdrenqIO.getStatuz(),varcom.oK)
	&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(chdrenqIO.getParams());
		syserrrec.statuz.set(chdrenqIO.getStatuz());
		fatalError600();
	}
	
	

	
	chdrenqIO.setFunction(varcom.keeps);
	SmartFileCode.execute(appVars, chdrenqIO);
	if (isNE(chdrenqIO.getStatuz(),varcom.oK)
	&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(chdrenqIO.getParams());
		syserrrec.statuz.set(chdrenqIO.getStatuz());
		fatalError600();
	}	


	

	

}



protected void m700Start()
	{
		sv.zsredtrm.set("?");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar11 = 0; !(loopVar11 == 8); loopVar11 += 1){
			save4510();
		}
		gensswrec.function.set("E");
		gensww4210();
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar12 = 0; !(loopVar12 == 8); loopVar12 += 1){
			load4530();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void m800ReducingTermRet()
	{
		/*M800-START*/
		sv.zsredtrm.set("+");
		compute(sub1, 0).set(add(wsspcomn.programPtr, 1));
		sub2.set(1);
		for (int loopVar13 = 0; !(loopVar13 == 8); loopVar13 += 1){
			restore4610();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*M800-EXIT*/
	}

protected void m300GetMethod()
	{
		m300Start();
	}

protected void m300Start()
	{
		
		itempfList=itempfDAO.getAllItemitem("IT",wsspcomn.company.toString(), tablesInner.t6598.toString(), wsaaZsbsmeth.toString());
		if (itempfList.size()==0) {
		t6598rec.t6598Rec.set(SPACES);
		}
		else {
			t6598rec.t6598Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		wsaaSubprog.set(t6598rec.calcprog);
	}

protected void m400CalcBenefitSchedule()
	{
		/*M400-START1*/
		//BRD-139-STARTS by vanusha
		if(!mrtaPermission){
			//ILIFE-7521-starts
			if(benSchdPermission && AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("VPMRLMRTRD")) {
				rlrdtrmrec.cnttype.set(chdrlnbIO.cnttype);
				//rlrdtrmrec.riskTerm.set(sv.riskCessTerm);
				rlrdtrmrec.riskTerm.set(rlrdtrmrec.riskTerm);//ILIFE-8001
				rlrdtrmrec.crtable.set(premiumrec.crtable);
				rlrdtrmrec.lage.set(premiumrec.lage);
				Vpxlextrec vpxlextrec = new Vpxlextrec();
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();			
				vpxlextrec.function.set("INIT");
				vpmcalcrec.function.set("INIT");
				vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
				vpxlextrec.pratNew.set(mrtaIO.getPrat());
				callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec,vpxlextrec);
				vpxlextrec.pratNew.set(mrtaIO.getPrat());
				callProgram(wsaaSubprog, rlrdtrmrec.rlrdtrmRec,vpxlextrec);			
			} //ILIFE-7521
			else{
				callProgram(wsaaSubprog, rlrdtrmrec.rlrdtrmRec);
				if (isEQ(rlrdtrmrec.statuz, varcom.bomb)) {
					syserrrec.statuz.set(rlrdtrmrec.statuz);
					fatalError600();
				}
			}
		}
		else
		{
			datcon1rec.datcon1Rec.set(SPACES);
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
			
			if (isEQ(mrtaIO.getMlresind(),"01A")) {
				compute(NoOfmonths,1).setRounded(mult(rlrdtrmrec.riskTerm, 12));
			}
			if (isEQ(mrtaIO.getMlresind(),"03A")) {
				compute(NoOfmonths,1).setRounded(mult(rlrdtrmrec.riskTerm, 4));
			}
			if (isEQ(mrtaIO.getMlresind(),"06A")) {
				compute(NoOfmonths,1).setRounded(mult(rlrdtrmrec.riskTerm, 2));
			}
			if (isEQ(mrtaIO.getMlresind(),"12A")) {
			NoOfmonths.set(rlrdtrmrec.riskTerm);
			}
			for (wsaaIndex1 = 1 ; !(isGT(wsaaIndex1,NoOfmonths)); wsaaIndex1 ++){
		    
		    try {
				getVPMSInformation();
			} catch (VpmsLoadFailedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    writeMrta();
			    
		/*M400-EXIT1*/
			}
		}
		//BRD-139-ENDS by vanusha
}

//BRD-139-STARTS-vanusha
private void getVPMSInformation() throws VpmsLoadFailedException{
	
	IVpmsBaseSessionFactory sessionFactory = new VpmsJniSessionFactory();
	IVpmsBaseSession session;
	
		session = sessionFactory.create(AppConfig.getVpmsModelDirectory()+"INTLIFE.VPM");
		
		 mrtaprat = mrtaIO.getPrat().toString();
		 mrtaprat = mrtaprat.trim();
		 mrtaLoandur = String.valueOf(mrtaIO.getLoandur());
		 mrtaLoandur = mrtaLoandur.trim();
//		 ILIFE-3709-STARTS
		 IntCalType = mrtaIO.getIntcalType();
		 IntCalType = IntCalType.trim();
//		 ILIFE-3709-ENDS
		session.setAttribute("Input Contract Type", sv.cnttype.toString());
		session.setAttribute("Input Region", " "); 
		session.setAttribute("Input Locale", " "); 
		session.setAttribute("Input TransEffDate", wsaaToday.toString());
		session.setAttribute("Input MTL PLan Cd", "MAS05S"); 
		session.setAttribute("Input Prod Code", "GS1");
		session.setAttribute("Input Coverage Code", covtlnbIO.getCrtable().toString());
		session.setAttribute("Input Loan Duration", mrtaLoandur); /* IJTI-1479 */
		session.setAttribute("Input Interest Rate", mrtaprat); /* IJTI-1479 */
		session.setAttribute("Input Interest Type", IntCalType); //ILIFE-3709
		session.setAttribute("Input Face Amount", sv.sumin.toString());
		session.setAttribute("Input No of Months", Integer.toString(wsaaIndex1));
		
		VpmsComputeResult result = session.computeUsingDefaults("Output Calc MRTA");
		String[] str_array =result.getResult().split("!");
		
		if (str_array != null){
			surrval =str_array[0];
			sumins =str_array[1];
			surrval  = surrval.trim();
			sumins = sumins.trim();
//			 ILIFE-3709-STARTS
//			mbnsIO.setSurrval(array1);
//			mbnsIO.setSumins(array2);
//			 ILIFE-3709-ENDS
			mbnspf = new Mbnspf();
			mbnspf.setSurrval(new BigDecimal(surrval));
			mbnspf.setSumins(new BigDecimal(sumins));
			return;
		}	
}

//BRD-139-ENDS by vanusha
protected void writeMrta()
{
	mbnspf.setChdrcoy(rlrdtrmrec.chdrcoy.toString());
	mbnspf.setChdrnum(rlrdtrmrec.chdrnum.toString());
	mbnspf.setLife(rlrdtrmrec.life.toString());
	mbnspf.setCoverage(rlrdtrmrec.coverage.toString());
	mbnspf.setRider(rlrdtrmrec.rider.toString());
	mbnspf.setYrsinf(wsaaIndex1);
	mbnspf=mbnsDAO.getmbnsRecord(mbnspf.getChdrcoy(),mbnspf.getChdrnum(),mbnspf.getLife(),mbnspf.getCoverage(),mbnspf.getRider(),mbnspf.getYrsinf()); /* IJTI-1479 */
	
	if (mbnspf != null){
		mbnspf = new Mbnspf();
		mbnspf.setChdrcoy(rlrdtrmrec.chdrcoy.toString());
		mbnspf.setChdrnum(rlrdtrmrec.chdrnum.toString());
		mbnspf.setLife(rlrdtrmrec.life.toString());
		mbnspf.setCoverage(rlrdtrmrec.coverage.toString());
		mbnspf.setRider(rlrdtrmrec.rider.toString());
		mbnspf.setYrsinf(wsaaIndex1);
		mbnspf.setSurrval(new BigDecimal(surrval));
		mbnspf.setSumins(new BigDecimal(sumins));
		
		mbnsDAO.updateMbnspfRecord(mbnspf);
	}
	
	else {
		mbnspf = new Mbnspf();
		mbnspf.setChdrcoy(rlrdtrmrec.chdrcoy.toString());
		mbnspf.setChdrnum(rlrdtrmrec.chdrnum.toString());
		mbnspf.setLife(rlrdtrmrec.life.toString());
		mbnspf.setCoverage(rlrdtrmrec.coverage.toString());
		mbnspf.setRider(rlrdtrmrec.rider.toString());
		mbnspf.setYrsinf(wsaaIndex1);
		mbnspf.setSurrval(new BigDecimal(surrval));
		mbnspf.setSumins(new BigDecimal(sumins));
		mbnsDAO.insertMbnspfRecord(mbnspf);
	}
}
protected void m500DeleteBenefitSchedule()
	{
		/*M500-START1*/
		mbnsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		mbnsIO.setChdrnum(chdrlnbIO.getChdrnum());
		mbnsIO.setLife(covtlnbIO.getLife());
		mbnsIO.setCoverage(covtlnbIO.getCoverage());
		mbnsIO.setRider(covtlnbIO.getRider());
		mbnsIO.setYrsinf(0);
		mbnsIO.setFormat(formatsInner.mbnsrec);
		mbnsIO.setFunction(varcom.begn);
		mbnsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		mbnsIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");

		while ( !(isEQ(mbnsIO.getStatuz(), varcom.endp))) {
			m600DeleteRecord();
		}
		
		/*M500-EXIT1*/
	}

protected void m600DeleteRecord()
	{
		m600Start1();
	}

protected void m600Start1()
	{
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)
		&& isNE(mbnsIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getChdrcoy(), mbnsIO.getChdrcoy())
		|| isNE(chdrlnbIO.getChdrnum(), mbnsIO.getChdrnum())
		|| isNE(covtlnbIO.getLife(), mbnsIO.getLife())
		|| isNE(covtlnbIO.getCoverage(), mbnsIO.getCoverage())
		|| isNE(covtlnbIO.getRider(), mbnsIO.getRider())) {
			mbnsIO.setStatuz(varcom.endp);
		}
		if (isEQ(mbnsIO.getStatuz(), varcom.endp)) {
			return ;
		}
		mbnsIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, mbnsIO);
		if (isNE(mbnsIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mbnsIO.getStatuz());
			fatalError600();
		}
		mbnsIO.setFunction(varcom.nextr);
	}

protected void m800ReadMrta()
	{
		m800Start1();
	}

protected void m800Start1()
	{
		
		mrtaIO=mrtapfDAO.getMrtaRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if (mrtaIO==null) {
			return ;
		}
		
		if(!mrtaPermission){
			itempfList = itempfDAO.getAllItemitem("IT", chdrlnbIO.getChdrcoy().toString(), tablesInner.th615.toString(), mrtaIO.getMlresind()); /* IJTI-1479 */
		if (itempfList.size()==0) {
			fatalError600();
		}
		th615rec.th615Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		wsaaPremind.set(th615rec.premind);
		}
	}

protected void optionsExe4500()
	{
		para4500();
	}

protected void para4500()
	{
		/* The  first  thing  to   consider   is   the  handling  of  an*/
		/* options/extras request. If the indicator is 'X', a request to*/
		/* visit options and extras has been made. In this case:*/
		/*  - change the options/extras request indicator to '?',*/
		sv.optextind.set("?");
		/*  - save the next 8 programs from the program stack,*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar14 = 0; !(loopVar14 == 8); loopVar14 += 1){
			save4510();
		}
		/*  - call GENSSWCH with and  action  of 'A' to retrieve the*/
		/*       program switching required,  and  move  them to the*/
		/*       stack,*/
		gensswrec.function.set("A");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar15 = 0; !(loopVar15 == 8); loopVar15 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',*/
		/*  - add one to the program pointer and exit.*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void save4510()
	{
		/*PARA*/
		wsaaSecProg[sub2.toInt()].set(wsspcomn.secProg[sub1.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void load4530()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(gensswrec.progOut[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void optionsRet4600()
	{
		/*PARA*/
		/* On return from this  request, the current stack "action" will*/
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To*/
		/* handle the return from options and extras:*/
		/*  - calculate  the  premium  as  described  above  in  the*/
		/*       'Validation' section, and  check  that it is within*/
		/*       the tolerance limit,*/
		sv.optextind.set("+");
		calcPremium2700();
		/*  - restore the saved programs to the program stack*/
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar16 = 0; !(loopVar16 == 8); loopVar16 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"*/
		/* MOVE ' '                 TO WSSP-SEC-ACTN (WSSP-PROGRAM-PTR).*/
		checkLext1900();
		/*       Set  WSSP-NEXTPROG to  the current screen*/
		/*       name (thus returning to re-display the screen).*/
		/* MOVE SCRN-SCRNAME        TO WSSP-NEXTPROG.                   */
		if (isNE(sv.ratypind, "X")) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		}
		/*EXIT*/
	}

protected void restore4610()
	{
		/*PARA*/
		wsspcomn.secProg[sub1.toInt()].set(wsaaSecProg[sub2.toInt()]);
		sub2.add(1);
		sub1.add(1);
		/*EXIT*/
	}

protected void wayout4700()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4700();
				case cont4710: 
					cont4710();
				case cont4715: 
					cont4715();
				case cont4717: 
					cont4717();
				case cont4720: 
					cont4720();
				case exit4790: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4700()
	{
		/* If control is passed to this  part of the 4000 section on the*/
		/* way  out of the program,  ie.  after  screen  I/O,  then  the*/
		/* current stack position action  flag  will  be  blank.  If the*/
		/* 4000  section  is   being   performed  after  returning  from*/
		/* processing another program  then  the  current stack position*/
		/* action flag will be '*'.*/
		/*    If 'KILL'  has  been  requested,  (CF11),  then  move*/
		/*    spaces to  the current program entry in the program*/
		/*    stack and exit.*/
		/*    IF SCRN-STATUZ              = 'KILL'                         */
		/*       MOVE SPACES           TO WSSP-SEC-PROG (WSSP-PROGRAM-PTR) */
		/*       GO TO 4790-EXIT.                                          */
		if (isEQ(scrnparams.statuz, varcom.rolu)
		|| isEQ(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.cont4710);
		}
		/*    If 'Enter' has been  pressed and "Credit" is non-zero*/
		/*    set the current select  action  field to '*', add 1*/
		/*    to the program pointer and exit.*/
		if (isNE(wsaaCredit, 0)) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		}
		/*    If 'Enter' has been  pressed and "Credit" is zero add*/
		/*    1 to the program pointer and exit.*/
		wsspcomn.programPtr.add(1);
		goTo(GotoLabel.exit4790);
	}

protected void cont4710()
	{
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has been changed set the current*/
		/*    select action field to '*',  add 1 to the program*/
		/*    pointer and exit.*/
		if (isNE(sv.numapp, covttrmIO.getNumapp())) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4790);
		}
		/*    If one of the Roll keys has been pressed and the*/
		/*    Number Applicable has not been changed then loop*/
		/*    round within the program and display the next /*/
		/*    previous screen as requested  (including BEGN).*/
		/*  Save the lase used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covttrmIO.getSeqnbr());
		if (isEQ(sv.numapp, 0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz, varcom.rolu)) {
				covttrmIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4715);
			}
			else {
				covttrmIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, covttrmIO);
				if (isNE(covttrmIO.getStatuz(), varcom.oK)
				&& isNE(covttrmIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(covttrmIO.getParams());
					fatalError600();
				}
			}
		}
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			covttrmIO.setFunction(varcom.nextr);
		}
		else {
			covttrmIO.setFunction(varcom.nextp);
		}
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(), varcom.oK)
		&& isNE(covttrmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		if (isNE(covttrmIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covttrmIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covttrmIO.getLife(), covtlnbIO.getLife())
		|| isNE(covttrmIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covttrmIO.getRider(), covtlnbIO.getRider())) {
			covttrmIO.setStatuz(varcom.endp);
		}
	}

protected void cont4715()
	{
		if (isEQ(covttrmIO.getStatuz(), varcom.endp)) {
			covttrmIO.setNonKey(SPACES);
			covttrmIO.setAnbccd(1, 0);
			covttrmIO.setAnbccd(2, 0);
			covttrmIO.setSingp(0);
			covttrmIO.setInstprem(0);
			covttrmIO.setZbinstprem(0);
			covttrmIO.setZlinstprem(0);
			covttrmIO.setNumapp(0);
			covttrmIO.setPremCessAge(0);
			covttrmIO.setPremCessTerm(0);
			covttrmIO.setPolinc(0);
			covttrmIO.setRiskCessAge(0);
			covttrmIO.setRiskCessTerm(0);
			covttrmIO.setSumins(0);
			covttrmIO.setBenCessAge(0);
			covttrmIO.setBenCessTerm(0);
			wsaaCtrltime.set("Y");
			goTo(GotoLabel.cont4717);
		}
		/* ELSE                                                         */
		wsaaCtrltime.set("N");
		sv.riskCessDate.set(covttrmIO.getRiskCessDate());
		sv.premCessDate.set(covttrmIO.getPremCessDate());
		sv.riskCessAge.set(covttrmIO.getRiskCessAge());
		sv.premCessAge.set(covttrmIO.getPremCessAge());
		sv.riskCessTerm.set(covttrmIO.getRiskCessTerm());
		sv.premCessTerm.set(covttrmIO.getPremCessTerm());
		if (isNE(covttrmIO.getSingp(), 0)) {
			sv.instPrem.set(covttrmIO.getSingp());
			if (isEQ(covttrmIO.getInstprem(), 0)) {
				sv.zbinstprem.set(covttrmIO.getZbinstprem());
				sv.zlinstprem.set(covttrmIO.getZlinstprem());
			}
		}
		else {
			sv.zbinstprem.set(covttrmIO.getZbinstprem());
			sv.zlinstprem.set(covttrmIO.getZlinstprem());
			sv.instPrem.set(covttrmIO.getInstprem());
		}
		/*BRD-306 START */
		sv.loadper.set(covttrmIO.getLoadper());
		sv.rateadj.set(covttrmIO.getRateadj());
		sv.fltmort.set(covttrmIO.getFltmort());
		sv.premadj.set(covttrmIO.getPremadj());
		sv.adjustageamt.set(covttrmIO.getAgeadj());
		/*BRD-306 END */
		if(stampDutyflag){
			if(isNE(covttrmIO.getZstpduty01(),ZERO)){
			sv.zstpduty01.set(covttrmIO.getZstpduty01());
			}
		}
		sv.sumin.set(covttrmIO.getSumins());
		sv.mortcls.set(covttrmIO.getMortcls());
		sv.liencd.set(covttrmIO.getLiencd());
		sv.bappmeth.set(covttrmIO.getBappmeth());
		sv.tpdtype.set(covttrmIO.getTpdtype());//ILIFE-7118
	}

protected void cont4717()
	{
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covttrmIO.getFunction(), varcom.nextp)) {
			wsaaNumavail.add(covttrmIO.getNumapp());
			goTo(GotoLabel.cont4720);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail, 0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covttrmIO.getSeqnbr(), 0);
			covttrmIO.setSeqnbr(add(1, wsaaSaveSeqnbr));
			covttrmIO.setNumapp(wsaaNumavail);
		}
	}

protected void cont4720()
	{
		if (isEQ(covttrmIO.getFunction(), varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr, 0)) {
			wsaaFirstSeqnbr.set(covttrmIO.getSeqnbr());
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covttrmIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(t5608rec.sumInsMax, t5608rec.sumInsMin)
		&& isNE(t5608rec.sumInsMax, 0)
		&& isNE(wsspcomn.flag, "I")) {
			compute(sv.sumin, 1).setRounded((div(mult(t5608rec.sumInsMin, sv.numapp), sv.polinc)));
		}
		if (isNE(sv.sumin, covttrmIO.getSumins())) {
			sv.instPrem.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(0);
			sv.adjustageamt.set(0);
			sv.rateadj.set(0);
			sv.fltmort.set(0);
			sv.premadj.set(0);
			/*BRD-306 END */
			if(stampDutyflag){
				sv.zstpduty01.set(0);
			}
			calcPremium2700();
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
		/* Life selection indicator may only be set up on the first*/
		/* screen*/
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()
		&& isEQ(wsaaFirstSeqnbr, covttrmIO.getSeqnbr())) {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
	}

protected void wayin4800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para4800();
				case rolu4805: 
					rolu4805();
				case cont4810: 
					cont4810();
				case cont4820: 
					cont4820();
				case readCovttrm4830: 
					readCovttrm4830();
				case cont4835: 
					cont4835();
				case cont4837: 
					cont4837();
				case cont4840: 
					cont4840();
				case exit4890: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para4800()
	{
		if (isEQ(scrnparams.statuz, varcom.rolu)
		|| isEQ(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.cont4820);
		}
		if (isEQ(wsaaCredit, 0)) {
			wsspcomn.programPtr.add(1);
			goTo(GotoLabel.exit4890);
		}
		if (isLTE(sv.numapp, sv.numavail)
		&& isLT(wsaaCredit, 0)) {
			goTo(GotoLabel.rolu4805);
		}
		if (isLT(wsaaCredit, 0)) {
			goTo(GotoLabel.cont4810);
		}
	}

	/**
	* <pre>
	*    If 'Enter' has been pressed and 'Credit' has a
	*    positive value then force the program to "roll up".
	* </pre>
	*/
protected void rolu4805()
	{
		scrnparams.statuz.set(varcom.rolu);
		goTo(GotoLabel.cont4820);
	}

protected void cont4810()
	{
		/*   If 'Enter' has been pressed and "Credit" has a negative*/
		/*   value, loop round within the program displaying the*/
		/*   details from the first transaction record, with the*/
		/*   'Number Applicable' highlighted, and give a message*/
		/*   indicating that more policies have been defined than*/
		/*   are on the Contract Header.*/
		covttrmIO.setChdrcoy(covtlnbIO.getChdrcoy());
		covttrmIO.setChdrnum(covtlnbIO.getChdrnum());
		covttrmIO.setLife(covtlnbIO.getLife());
		covttrmIO.setCoverage(covtlnbIO.getCoverage());
		covttrmIO.setRider(covtlnbIO.getRider());
		covttrmIO.setSeqnbr(0);
		/* The Error Message was being moved to the screen and not         */
		/* to the appropriate Screen Field:- S5123-NumApp-Err.             */
		/* This validation has been moved to the 2000 Section.             */
		/*MOVE U012                   TO S5123-NUMAPP-ERR.             */
		covttrmIO.setFunction(varcom.begn);
		goTo(GotoLabel.readCovttrm4830);
	}

protected void cont4820()
	{
		/*    If one of the Roll keys has been pressed then loop*/
		/*    round within the program and display the next or*/
		/*    previous screen as requested (including  BEGN,*/
		/*    available count etc., see above).*/
		/*  Save the lase used sequence number in case we roll off the end*/
		wsaaSaveSeqnbr.set(covttrmIO.getSeqnbr());
		if (isEQ(sv.numapp, 0)) {
			wsaaSaveSeqnbr.subtract(1);
		}
		/*  Cope with case when last record has just been added*/
		if (firsttime.isTrue()) {
			if (isEQ(scrnparams.statuz, varcom.rolu)) {
				covttrmIO.setStatuz(varcom.endp);
				goTo(GotoLabel.cont4835);
			}
			else {
				covttrmIO.setFunction(varcom.begn);
				SmartFileCode.execute(appVars, covttrmIO);
				if (isNE(covttrmIO.getStatuz(), varcom.oK)
				&& isNE(covttrmIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(covttrmIO.getParams());
					fatalError600();
				}
			}
		}
		/* Read next/previous record*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			covttrmIO.setFunction(varcom.nextr);
		}
		else {
			covttrmIO.setFunction(varcom.nextp);
		}
	}

protected void readCovttrm4830()
	{
		SmartFileCode.execute(appVars, covttrmIO);
		if (isNE(covttrmIO.getStatuz(), varcom.oK)
		&& isNE(covttrmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covttrmIO.getParams());
			fatalError600();
		}
		if (isNE(covttrmIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(covttrmIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(covttrmIO.getLife(), covtlnbIO.getLife())
		|| isNE(covttrmIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(covttrmIO.getRider(), covtlnbIO.getRider())) {
			covttrmIO.setStatuz(varcom.endp);
		}
	}

protected void cont4835()
	{
		if (isEQ(covttrmIO.getStatuz(), varcom.endp)) {
			wsaaCtrltime.set("Y");
			covttrmIO.setNonKey(SPACES);
			covttrmIO.setAnbccd(1, 0);
			covttrmIO.setAnbccd(2, 0);
			covttrmIO.setSingp(0);
			covttrmIO.setInstprem(0);
			covttrmIO.setZbinstprem(0);
			covttrmIO.setZlinstprem(0);
			covttrmIO.setNumapp(0);
			covttrmIO.setPremCessAge(0);
			covttrmIO.setPremCessTerm(0);
			covttrmIO.setPolinc(0);
			covttrmIO.setRiskCessAge(0);
			covttrmIO.setRiskCessTerm(0);
			covttrmIO.setSumins(0);
			covttrmIO.setBenCessAge(0);
			covttrmIO.setBenCessTerm(0);
			goTo(GotoLabel.cont4837);
		}
		/* ELSE                                                         */
		wsaaCtrltime.set("N");
		sv.riskCessDate.set(covttrmIO.getRiskCessDate());
		sv.premCessDate.set(covttrmIO.getPremCessDate());
		sv.riskCessAge.set(covttrmIO.getRiskCessAge());
		sv.premCessAge.set(covttrmIO.getPremCessAge());
		sv.riskCessTerm.set(covttrmIO.getRiskCessTerm());
		sv.premCessTerm.set(covttrmIO.getPremCessTerm());
		if (isNE(covttrmIO.getSingp(), 0)) {
			sv.instPrem.set(covttrmIO.getSingp());
			if (isEQ(covttrmIO.getInstprem(), 0)) {
				sv.zbinstprem.set(covttrmIO.getZbinstprem());
				sv.zlinstprem.set(covttrmIO.getZlinstprem());
			}
		}
		else {
			sv.zbinstprem.set(covttrmIO.getZbinstprem());
			sv.zlinstprem.set(covttrmIO.getZlinstprem());
			sv.instPrem.set(covttrmIO.getInstprem());
		}
		/*BRD-306 START */
		sv.loadper.set(covttrmIO.getLoadper());
		sv.rateadj.set(covttrmIO.getRateadj());
		sv.fltmort.set(covttrmIO.getFltmort());
		sv.premadj.set(covttrmIO.getPremadj());
		sv.adjustageamt.set(covttrmIO.getAgeadj());
		/*BRD-306 END */
		if(stampDutyflag){
			if(isNE(covttrmIO.getZstpduty01(),ZERO)){
			sv.zstpduty01.set(covttrmIO.getZstpduty01());
			}
		}
		sv.sumin.set(covttrmIO.getSumins());
		sv.mortcls.set(covttrmIO.getMortcls());
		sv.liencd.set(covttrmIO.getLiencd());
		sv.bappmeth.set(covttrmIO.getBappmeth());
		sv.tpdtype.set(covttrmIO.getTpdtype());//ILIFE-7118
	}

protected void cont4837()
	{
		/*    When displaying the first record again,  set the*/
		/*    number available as the number included in the plan*/
		if (isEQ(covttrmIO.getFunction(), varcom.begn)) {
			wsaaNumavail.set(chdrlnbIO.getPolinc());
			wsaaFirstSeqnbr.set(covttrmIO.getSeqnbr());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying a previous record, calculate the*/
		/*    number available as the current number plus the number*/
		/*    applicable to the record about to be displayed.*/
		if (isEQ(covttrmIO.getFunction(), varcom.nextp)) {
			wsaaNumavail.add(covttrmIO.getNumapp());
			goTo(GotoLabel.cont4840);
		}
		/*    When displaying the next record, or a blank screen to*/
		/*    enter a new record, calculate the number available as*/
		/*    the current number less the number applicable on the*/
		/*    current screen.*/
		/*    If displaying a blank screen for a new record, default*/
		/*    the number applicable to the number available. If this*/
		/*    is less than zero, default it to zero.*/
		wsaaNumavail.subtract(sv.numapp);
		if (isLT(wsaaNumavail, 0)) {
			wsaaNumavail.set(0);
		}
		if (firsttime.isTrue()) {
			setPrecision(covttrmIO.getSeqnbr(), 0);
			covttrmIO.setSeqnbr(add(1, wsaaSaveSeqnbr));
			covttrmIO.setNumapp(wsaaNumavail);
		}
	}

protected void cont4840()
	{
		if (isEQ(covttrmIO.getFunction(), varcom.nextr)
		&& isEQ(wsaaFirstSeqnbr, 0)) {
			wsaaFirstSeqnbr.set(covttrmIO.getSeqnbr());
		}
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.numavail.set(wsaaNumavail);
		sv.numapp.set(covttrmIO.getNumapp());
		/* Re-calculate default SI if applicable*/
		/*  then re-calculate default premium if SI is different*/
		if (isEQ(t5608rec.sumInsMax, t5608rec.sumInsMin)
		&& isNE(t5608rec.sumInsMax, 0)
		&& isNE(wsspcomn.flag, "I")) {
			compute(sv.sumin, 1).setRounded((div(mult(t5608rec.sumInsMin, sv.numapp), sv.polinc)));
		}
		if (isNE(sv.sumin, covttrmIO.getSumins())) {
			sv.instPrem.set(0);
			sv.zbinstprem.set(0);
			sv.zlinstprem.set(0);
			/*BRD-306 START */
			sv.loadper.set(0);
			sv.adjustageamt.set(0);
			sv.rateadj.set(0);
			sv.fltmort.set(0);
			sv.premadj.set(0);
			/*BRD-306 END */
			if(stampDutyflag){
				sv.zstpduty01.set(ZERO);
			}
			calcPremium2700();
		}
		/* Life selection indicator may only be set up on the first*/
		/* screen*/
		if (isNE(t5687rec.jlifePresent, SPACES)
		&& jointlif.isTrue()
		&& isEQ(wsaaFirstSeqnbr, covttrmIO.getSeqnbr())) {
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void pbindExe5100()
	{
		start5110();
	}

protected void start5110()
	{
		/*  - Keep the POVR/CHDR record for Premium Breakdown enquiry.     */
		/*    Ensure we get the latest one.                                */
		readPovr5300();
		if (isEQ(povrIO.getStatuz(), varcom.endp)
		|| isNE(povrIO.getChdrcoy(), covtlnbIO.getChdrcoy())
		|| isNE(povrIO.getChdrnum(), covtlnbIO.getChdrnum())
		|| isNE(povrIO.getLife(), covtlnbIO.getLife())
		|| isNE(povrIO.getCoverage(), covtlnbIO.getCoverage())
		|| isNE(povrIO.getRider(), covtlnbIO.getRider())) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		povrIO.setFunction(varcom.keeps);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.pbind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar17 = 0; !(loopVar17 == 8); loopVar17 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the     */
		/*    next program.                                                */
		gensswrec.function.set("C");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar18 = 0; !(loopVar18 == 8); loopVar18 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void pbindRet5200()
	{
		start5210();
	}

protected void start5210()
	{
		/* Release the POVR records as no longer required.                 */
		povrIO.setFunction(varcom.rlse);
		povrIO.setFormat(formatsInner.povrrec);
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.pbind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar19 = 0; !(loopVar19 == 8); loopVar19 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
	}

protected void readPovr5300()
	{
		start5310();
	}

protected void start5310()
	{
		povrIO.setParams(SPACES);
		povrIO.setChdrcoy(covtlnbIO.getChdrcoy());
		povrIO.setChdrnum(covtlnbIO.getChdrnum());
		povrIO.setLife(covtlnbIO.getLife());
		povrIO.setCoverage(covtlnbIO.getCoverage());
		povrIO.setRider(covtlnbIO.getRider());
		povrIO.setPlanSuffix(0);
		povrIO.setFormat(formatsInner.povrrec);
		povrIO.setFunction(varcom.begn);
		povrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		povrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, povrIO);
		if (isNE(povrIO.getStatuz(), varcom.oK)
		&& isNE(povrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(povrIO.getParams());
			syserrrec.statuz.set(povrIO.getStatuz());
			fatalError600();
		}
	}

protected void taxExe5600()
	{
		start5610();
	}

protected void start5610()
	{
		/*  - Keep the CHDR/COVT record                                    */
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		covtlnbIO.setZbinstprem(sv.zbinstprem);
		covtlnbIO.setInstprem(sv.instPrem);
		covtlnbIO.setFunction(varcom.keeps);
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		/* Use standard GENSWITCH table switching using the next option    */
		/* on table T1675.                                                 */
		/*  - change the request indicator to '?',                         */
		sv.taxind.set("?");
		/*  - save the next 8 programs from the program stack,             */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar20 = 0; !(loopVar20 == 8); loopVar20 += 1){
			save4510();
		}
		/*  - call GENSSWCH with the next table action to retreive the     */
		/*    next program.                                                */
		gensswrec.function.set("F");
		gensww4210();
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar21 = 0; !(loopVar21 == 8); loopVar21 += 1){
			load4530();
		}
		/*  - set the current stack "action" to '*',                       */
		/*  - add one to the program pointer and exit.                     */
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
	}

protected void taxRet5700()
	{
		/*START*/
		/* On return from this  request, the current stack "action" will   */
		/* be '*' and the  options/extras  indicator  will  be  '?'.  To   */
		/* handle the return from options and extras:                      */
		/* Note that to have selected this option in the first place then  */
		/* details must exist.......set the flag for re-selection.         */
		sv.taxind.set("+");
		/*  - restore the saved programs to the program stack              */
		compute(sub1, 0).set(add(1, wsspcomn.programPtr));
		sub2.set(1);
		for (int loopVar22 = 0; !(loopVar22 == 8); loopVar22 += 1){
			restore4610();
		}
		/*  - blank  out  the  stack  "action"                             */
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		/*       Set  WSSP-NEXTPROG to  the current screen                 */
		/*       name (thus returning to re-display the screen).           */
		wsspcomn.nextprog.set(scrnparams.scrname);
		/*EXIT*/
	}

protected void a100CheckLimit()
	{
		a100Ctrl();
	}

protected void a100Ctrl()
	{
		chkrlrec.clntnum.set(sv.lifcnum);
		chkrlrec.company.set(wsspcomn.company);
		chkrlrec.currcode.set(sv.currcd);
		chkrlrec.cnttype.set(chdrlnbIO.getCnttype());
		chkrlrec.crtable.set(covtlnbIO.getCrtable());
		chkrlrec.sumins.set(sv.sumin);
		chkrlrec.life.set(covtlnbIO.getLife());
		chkrlrec.chdrnum.set(chdrlnbIO.getChdrnum());
		chkrlrec.function.set("CHCK");
		callProgram(Chkrlmt.class, chkrlrec.parmRec);
		if (isNE(chkrlrec.statuz, varcom.oK)) {
			if (isEQ(chkrlrec.statuz, errorsInner.e417)) {
				sv.suminErr.set(errorsInner.e417);
			}
			else {
				syserrrec.statuz.set(chkrlrec.statuz);
				syserrrec.params.set(chkrlrec.params);
				syserrrec.iomod.set("CHKRLMT");
				fatalError600();
			}
		}
	}

protected void a200CheckCalcTax()
	{
		try {
			a210Start();
			a250CallTaxSubr();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void a210Start()
	{
		if (isNE(t5687rec.bbmeth, SPACES)) {
			goTo(GotoLabel.a290Exit);
		}
		if (notFirstTaxCalc.isTrue()) {
			a250CallTaxSubr();
		}
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtlnbIO.getCrtable());
		a300ReadTr52e();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			a300ReadTr52e();
		}
		wsaaFirstTaxCalc.set("N");
	}

protected void a250CallTaxSubr()
	{
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
			txcalcrec.life.set(covtlnbIO.getLife());
			txcalcrec.coverage.set(covtlnbIO.getCoverage());
			txcalcrec.rider.set(covtlnbIO.getRider());
			txcalcrec.planSuffix.set(0);
			txcalcrec.crtable.set(covtlnbIO.getCrtable());
			txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
			txcalcrec.register.set(chdrlnbIO.getRegister());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
			wsaaCntCurr.set(chdrlnbIO.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			txcalcrec.batckey.set(wsaaBatckey);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(sv.zbinstprem);
			}
			else {
				txcalcrec.amountIn.set(sv.instPrem);
			}
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrlnbIO.getOccdate());
			txcalcrec.tranno.set(chdrlnbIO.getTranno());
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(0);
			txcalcrec.taxAmt[2].set(0);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], 0)
			|| isGT(txcalcrec.taxAmt[2], 0)) {
				wsaaTaxamt.set(sv.instPrem);
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaTaxamt.add(txcalcrec.taxAmt[2]);
				}
				sv.taxamt.set(wsaaTaxamt);
				sv.taxind.set("+");
				sv.taxamtOut[varcom.nd.toInt()].set("N");
				sv.taxindOut[varcom.nd.toInt()].set("N");
				sv.taxamtOut[varcom.pr.toInt()].set("Y");		//ILIFE-4225
				sv.taxindOut[varcom.pr.toInt()].set("N");
			}
		}
	}

protected void a300ReadTr52e()
	{
		a310Start();
	}

protected void a310Start()
	{
		tr52erec.tr52eRec.set(SPACES);
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(tablesInner.tr52e.toString());
		itempf.setItemitem(wsaaTr52eKey.toString());
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		itempfList = itempfDAO.findByItemDates(itempf);
		if (itempfList.size()==0
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			fatalError600();
		}
		if (itempfList.size()>0) {
			tr52erec.tr52eRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}

protected void x100ReadItdm()
	{
		x100Init();
	}

protected void x100Init()
	{
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(wsaaItemtabl.toString());
		itempf.setItemitem(wsaaItemitem.toString());
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());
		itempfList = itempfDAO.findByItemDates(itempf);
		if (itempfList.size()==0) {
			wsaaT6768Found.set("N");
		}
		else {
			wsaaT6768Found.set("Y");
			t6768rec.t6768Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}
/*
 * Class transformed  from Data Structure WSAA-DEFAULTS--INNER
 */
private static final class WsaaDefaultsInner { 

	private FixedLengthStringData wsaaDefaults = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaDefaultRa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 0);
	private Validator defaultRa = new Validator(wsaaDefaultRa, "Y");
	private Validator nodefRa = new Validator(wsaaDefaultRa, "N");
	private FixedLengthStringData wsaaDefaultPa = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 1);
	private Validator defaultPa = new Validator(wsaaDefaultPa, "Y");
	private Validator nodefPa = new Validator(wsaaDefaultPa, "N");
	private FixedLengthStringData wsaaDefaultRt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 2);
	private Validator defaultRt = new Validator(wsaaDefaultRt, "Y");
	private Validator nodefRt = new Validator(wsaaDefaultRt, "N");
	private FixedLengthStringData wsaaDefaultPt = new FixedLengthStringData(1).isAPartOf(wsaaDefaults, 3);
	private Validator defaultPt = new Validator(wsaaDefaultPt, "Y");
	private Validator nodefPt = new Validator(wsaaDefaultPt, "N");
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e027 = new FixedLengthStringData(4).init("E027");
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e389 = new FixedLengthStringData(4).init("E389");
	private FixedLengthStringData e390 = new FixedLengthStringData(4).init("E390");
	private FixedLengthStringData e416 = new FixedLengthStringData(4).init("E416");
	private FixedLengthStringData e417 = new FixedLengthStringData(4).init("E417");
	private FixedLengthStringData e420 = new FixedLengthStringData(4).init("E420");
	private FixedLengthStringData e519 = new FixedLengthStringData(4).init("E519");
	private FixedLengthStringData e531 = new FixedLengthStringData(4).init("E531");
	private FixedLengthStringData e551 = new FixedLengthStringData(4).init("E551");
	private FixedLengthStringData e560 = new FixedLengthStringData(4).init("E560");
	private FixedLengthStringData e562 = new FixedLengthStringData(4).init("E562");
	private FixedLengthStringData e563 = new FixedLengthStringData(4).init("E563");
	private FixedLengthStringData e566 = new FixedLengthStringData(4).init("E566");
	private FixedLengthStringData f220 = new FixedLengthStringData(4).init("F220");
	private FixedLengthStringData f254 = new FixedLengthStringData(4).init("F254");
	private FixedLengthStringData f261 = new FixedLengthStringData(4).init("F261");
	private FixedLengthStringData f262 = new FixedLengthStringData(4).init("F262");
	private FixedLengthStringData f294 = new FixedLengthStringData(4).init("F294");
	private FixedLengthStringData f335 = new FixedLengthStringData(4).init("F335");
	private FixedLengthStringData g620 = new FixedLengthStringData(4).init("G620");
	private FixedLengthStringData g622 = new FixedLengthStringData(4).init("G622");
	private FixedLengthStringData h043 = new FixedLengthStringData(4).init("H043");
	private FixedLengthStringData h437 = new FixedLengthStringData(4).init("H437");
	private FixedLengthStringData g818 = new FixedLengthStringData(4).init("G818");
	private FixedLengthStringData h039 = new FixedLengthStringData(4).init("H039");
	private FixedLengthStringData h040 = new FixedLengthStringData(4).init("H040");
	private FixedLengthStringData h093 = new FixedLengthStringData(4).init("H093");
	private FixedLengthStringData l001 = new FixedLengthStringData(4).init("L001");
	private FixedLengthStringData d352 = new FixedLengthStringData(4).init("D352");
	private FixedLengthStringData rfxv = new FixedLengthStringData(4).init("RFXV");
	private FixedLengthStringData rfxw = new FixedLengthStringData(4).init("RFXW");
	private FixedLengthStringData rrlk = new FixedLengthStringData(4).init("RRLK");	//ILIFE-7614
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData rrsu = new FixedLengthStringData(4).init("RRSU");
	private FixedLengthStringData h033 = new FixedLengthStringData(4).init("H033"); // Temporary change
	private FixedLengthStringData h044 = new FixedLengthStringData(4).init("H044");	// Temporary change
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5585 = new FixedLengthStringData(5).init("T5585");
	private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
	private FixedLengthStringData t5608 = new FixedLengthStringData(5).init("T5608");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t5675 = new FixedLengthStringData(5).init("T5675");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t6005 = new FixedLengthStringData(5).init("T6005");
	private FixedLengthStringData t6598 = new FixedLengthStringData(5).init("T6598");
	private FixedLengthStringData th615 = new FixedLengthStringData(5).init("TH615");
	private FixedLengthStringData t6768 = new FixedLengthStringData(5).init("T6768");
	private FixedLengthStringData tr675 = new FixedLengthStringData(5).init("TR675");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData covttrmrec = new FixedLengthStringData(10).init("COVTTRMREC");
	private FixedLengthStringData povrrec = new FixedLengthStringData(10).init("POVRREC");
	private FixedLengthStringData mbnsrec = new FixedLengthStringData(10).init("MBNSREC");
	private FixedLengthStringData minsrec = new FixedLengthStringData(10).init("MINSREC");
	private FixedLengthStringData undcrec = new FixedLengthStringData(10).init("UNDCREC");
}
	//ILIFE-3421:Start
	protected void callReadRCVDPF(){
		if(rcvdPFObject == null)
			rcvdPFObject= new Rcvdpf();
		rcvdPFObject.setChdrcoy(covtlnbIO.getChdrcoy().toString());
		rcvdPFObject.setChdrnum(covtlnbIO.getChdrnum().toString());
		rcvdPFObject.setLife(covtlnbIO.getLife().toString());
		rcvdPFObject.setCoverage(covtlnbIO.getCoverage().toString());
		rcvdPFObject.setRider(covtlnbIO.getRider().toString());
		rcvdPFObject.setCrtable(covtlnbIO.getCrtable().toString());
		rcvdPFObject=rcvdDAO.readRcvdpf(rcvdPFObject);
		if(rcvdPFObject == null)
			sv.dialdownoption.set(100);
	}

	protected void insertAndUpdateRcvdpf(){
		boolean rcvdUpdateFlag=false;
		if(rcvdPFObject!=null){
			rcvdPFObject.setChdrcoy(covtlnbIO.getChdrcoy().toString());
			rcvdPFObject.setChdrnum(covtlnbIO.getChdrnum().toString());
			rcvdPFObject.setCoverage(covtlnbIO.getCoverage().toString());
			rcvdPFObject.setCrtable(covtlnbIO.getCrtable().toString());
			rcvdPFObject.setLife(covtlnbIO.getLife().toString());
			rcvdPFObject.setRider(covtlnbIO.getRider().toString());
			if(rcvdPFObject.getPrmbasis()!=null){
			if (isNE(sv.prmbasis, rcvdPFObject.getPrmbasis())) {
				rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
				rcvdUpdateFlag=true;
				}	
			}
			//BRD-NBP-011 starts
			if(rcvdPFObject.getDialdownoption()!=null){
			if (isNE(sv.dialdownoption, rcvdPFObject.getDialdownoption())) {
				rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
				rcvdUpdateFlag=true;
			}
			}
			if(rcvdUpdateFlag){
				rcvdDAO.updateIntoRcvdpf(rcvdPFObject);
			}	

		}else{ 
			rcvdPFObject=new Rcvdpf();
			rcvdPFObject.setChdrcoy(covtlnbIO.getChdrcoy().toString());
			rcvdPFObject.setChdrnum(covtlnbIO.getChdrnum().toString());
			rcvdPFObject.setCoverage(covtlnbIO.getCoverage().toString());
			rcvdPFObject.setCrtable(covtlnbIO.getCrtable().toString());
			rcvdPFObject.setLife(covtlnbIO.getLife().toString());
			rcvdPFObject.setRider(covtlnbIO.getRider().toString());
			if(isNE(sv.prmbasis,SPACES)){
				rcvdPFObject.setPrmbasis(sv.prmbasis.toString());
			}
			//BRD-NBP-011 starts
			if(isNE(sv.dialdownoption,SPACES)){
				rcvdPFObject.setDialdownoption(sv.dialdownoption.toString());
			}
			rcvdDAO.insertRcvdpfRecord((rcvdPFObject));
		}

	}
	//ILIFE-3421:End
	protected void getPrem2050CustomerSpecific(){}
	protected void setBMIvalu2800CustomerSpecific(){}
	protected void delZcvrRecrds3040CustomerSpecific(){}
	protected void lextpf3010CustomerSpecific(){}
	protected void updtpurepremCustomerSpecific(){}
	protected void getPurePrem1011CustomeSpecific(){}
}
