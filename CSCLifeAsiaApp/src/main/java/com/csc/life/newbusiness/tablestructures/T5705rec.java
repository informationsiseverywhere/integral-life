package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:11
 * Description:
 * Copybook name: T5705REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5705rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5705Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData agntsel = new FixedLengthStringData(10).isAPartOf(t5705Rec, 0);
  	public FixedLengthStringData billcurr = new FixedLengthStringData(3).isAPartOf(t5705Rec, 10);
  	public FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(t5705Rec, 13);
  	public FixedLengthStringData campaign = new FixedLengthStringData(6).isAPartOf(t5705Rec, 15);
  	public FixedLengthStringData cntbranch = new FixedLengthStringData(2).isAPartOf(t5705Rec, 21);
  	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(t5705Rec, 23);
  	public ZonedDecimalData incomeSeqNo = new ZonedDecimalData(2, 0).isAPartOf(t5705Rec, 26);
  	public FixedLengthStringData lrepnum = new FixedLengthStringData(20).isAPartOf(t5705Rec, 28);
  	public FixedLengthStringData mop = new FixedLengthStringData(1).isAPartOf(t5705Rec, 48);
  	public FixedLengthStringData mortality = new FixedLengthStringData(1).isAPartOf(t5705Rec, 49);
  	public ZonedDecimalData plansuff = new ZonedDecimalData(2, 0).isAPartOf(t5705Rec, 50);
  	public FixedLengthStringData pursuits = new FixedLengthStringData(12).isAPartOf(t5705Rec, 52);
  	public FixedLengthStringData[] pursuit = FLSArrayPartOfStructure(2, 6, pursuits, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(pursuits, 0, FILLER_REDEFINE);
  	public FixedLengthStringData pursuit01 = new FixedLengthStringData(6).isAPartOf(filler, 0);
  	public FixedLengthStringData pursuit02 = new FixedLengthStringData(6).isAPartOf(filler, 6);
  	public ZonedDecimalData rcdate = new ZonedDecimalData(8, 0).isAPartOf(t5705Rec, 64);
  	public FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(t5705Rec, 72);
  	public FixedLengthStringData reptype = new FixedLengthStringData(2).isAPartOf(t5705Rec, 75);
  	public FixedLengthStringData selection = new FixedLengthStringData(4).isAPartOf(t5705Rec, 77);
  	public FixedLengthStringData smkrqd = new FixedLengthStringData(1).isAPartOf(t5705Rec, 81);
  	public FixedLengthStringData srcebus = new FixedLengthStringData(2).isAPartOf(t5705Rec, 82);
  	public FixedLengthStringData stcal = new FixedLengthStringData(3).isAPartOf(t5705Rec, 84);
  	public FixedLengthStringData stcbl = new FixedLengthStringData(3).isAPartOf(t5705Rec, 87);
  	public FixedLengthStringData stccl = new FixedLengthStringData(3).isAPartOf(t5705Rec, 90);
  	public FixedLengthStringData stcdl = new FixedLengthStringData(3).isAPartOf(t5705Rec, 93);
  	public FixedLengthStringData stcel = new FixedLengthStringData(3).isAPartOf(t5705Rec, 96);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(401).isAPartOf(t5705Rec, 99, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5705Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5705Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}