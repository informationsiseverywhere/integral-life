package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:21
 * Description:
 * Copybook name: COVRCFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrcfikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrcfiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrcfiKey = new FixedLengthStringData(64).isAPartOf(covrcfiFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrcfiChdrcoy = new FixedLengthStringData(1).isAPartOf(covrcfiKey, 0);
  	public FixedLengthStringData covrcfiChdrnum = new FixedLengthStringData(8).isAPartOf(covrcfiKey, 1);
  	public FixedLengthStringData covrcfiLife = new FixedLengthStringData(2).isAPartOf(covrcfiKey, 9);
  	public FixedLengthStringData covrcfiCoverage = new FixedLengthStringData(2).isAPartOf(covrcfiKey, 11);
  	public FixedLengthStringData covrcfiRider = new FixedLengthStringData(2).isAPartOf(covrcfiKey, 13);
  	public PackedDecimalData covrcfiPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrcfiKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrcfiKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrcfiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrcfiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}