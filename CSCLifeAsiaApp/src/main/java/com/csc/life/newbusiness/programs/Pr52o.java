/*
 * File: Pr52o.java
 * Date: December 3, 2013 3:27:32 AM ICT
 * Author: CSC
 *
 * Class transformed from PR52O.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.dataaccess.AgpydocTableDAM;
import com.csc.life.agents.screens.Sr52oScreenVars;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.screens.S5004ScreenVars;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*               PR52O - Policy Despatch Details
* This program will display the policy dispatch details. It will a so
* be used to update the policy acknowledgement date.
*
****************************************************************** ****
* </pre>
*/
public class Pr52o extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR52O");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(3, 0);
		/* WSAA-SEC-PROGS */
	private FixedLengthStringData[] wsaaSecProg = FLSInittedArray(8, 5);
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaTr386Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaTr386Lang = new FixedLengthStringData(1).isAPartOf(wsaaTr386Key, 0);
	private FixedLengthStringData wsaaTr386Pgm = new FixedLengthStringData(5).isAPartOf(wsaaTr386Key, 1);
	private FixedLengthStringData wsaaDesctabl = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaDescitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaDesccoy = new FixedLengthStringData(1);
		/* ERRORS */
	private static final String e032 = "E032";
	private static final String e186 = "E186";
	private static final String f073 = "F073";
	private static final String rlci = "RLCI";
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String th605 = "TH605";
	private static final String tr271 = "TR271";
	private static final String tr386 = "TR386";
		/* FORMATS */
	private static final String cltsrec = "CLTSREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String hpadrec = "HPADREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String agpyagtrec = "AGPYAGTREC";
	private static final String agpydocrec = "AGPYDOCREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
	private AgpydocTableDAM agpydocIO = new AgpydocTableDAM();
	// ILIFE-3396 PERFORMANCE BY OPAL
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	/*ILIFE-3804*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	/*ILIFE-3804*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Th605rec th605rec = new Th605rec();
	private Tr386rec tr386rec = new Tr386rec();
	private Sr52oScreenVars sv = getLScreenVars();

	protected Sr52oScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sr52oScreenVars.class);
	}
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		checkForErrors2080,
		exit2090,
		call3420,
		exit3490
	}

	public Pr52o() {
		super();
		screenVars = sv;
		new ScreenModel("Sr52o", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	protected void largename() {
		/* LGNM-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/* LGNM-EXIT */
	}

protected void plainname()
	{
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();

		} else if (isNE(cltsIO.getGivname(), SPACES)) {
			String firstName =  cltsIO.getGivname().toString();
			String lastName =  cltsIO.getSurname().toString();
			String delimiter = ",";

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			wsspcomn.longconfname.set(fullName);

		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

	protected void payeename() {
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else if (isEQ(cltsIO.getEthorig(), "1")) {
			String firstName =  cltsIO.getGivname().toString();
			String lastName =  cltsIO.getSurname().toString();
			String delimiter = "";
			String salute =  cltsIO.getSalutl().toString();

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			String fullNameWithSalute = stringUtil.includeSalute(salute, fullName);

			wsspcomn.longconfname.set(fullNameWithSalute);
		} else {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable2.addExpression(". ");
			stringVariable2.addExpression(cltsIO.getGivname(), "  ");
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression(cltsIO.getSurname(), "  ");
			stringVariable2.setStringInto(wsspcomn.longconfname);
		}
		/* PAYEE-EXIT */
	}

	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName =  cltsIO.getLgivname().toString();
		String lastName =  cltsIO.getLsurname().toString();
		String delimiter = "";

		// this way we can override StringUtil behaviour in formatName()
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/* CORP-EXIT */
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaBatckey.set(wsspcomn.batchkey);
		sv.dataArea.set(SPACES);
		sv.despdate.set(varcom.vrcmMaxDate);
		sv.packdate.set(varcom.vrcmMaxDate);
		sv.remdte.set(varcom.vrcmMaxDate);
		sv.deemdate.set(varcom.vrcmMaxDate);
		sv.nextActDate.set(varcom.vrcmMaxDate);
		readTr3861100();
		if (isEQ(wsspcomn.flag, "I")) {
			sv.descn.set(tr386rec.progdesc02);
			sv.packdateOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.descn.set(tr386rec.progdesc01);
		}
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		sv.packdate.set(datcon1rec.intDate);
		// ILIFE-3396 PERFORMANCE BY OPAL
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		/*ILIFE-3804 starts*/
		if(null==chdrpf){
			chdrenqIO.setFunction(Varcom.retrv);
			chdrenqIO.setFormat(chdrenqrec);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), Varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrenqIO.getChdrcoy().toString(), chdrenqIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);
				}
			}
		}
		/*ILIFE-3804 ends*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(chdrpf.getChdrcoy());
		chdrlifIO.setChdrnum(chdrpf.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrlifIO.getChdrnum());
		sv.cnttype.set(chdrlifIO.getCnttype());
		sv.cntcurr.set(chdrlifIO.getCntcurr());
		sv.register.set(chdrlifIO.getRegister());
		sv.cownnum.set(chdrlifIO.getCownnum());
		clientDetails1200();
		otherDetails1300();
		getDespatch1400();
		if ((isEQ(sv.packdate, 0)
		|| isEQ(sv.packdate, varcom.vrcmMaxDate))
		&& (isEQ(wsspcomn.flag, "M"))) {
			sv.packdate.set(wsaaToday);
		}
	}

protected void readTr3861100()
	{
		begin1110();
	}

protected void begin1110()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		wsaaTr386Lang.set(wsspcomn.language);
		wsaaTr386Pgm.set(wsaaProg);
		itemIO.setItemitem(wsaaTr386Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
	}

protected void clientDetails1200()
	{
		begin1210();
	}

protected void begin1210()
	{
		cltsIO.setParams(SPACES);
		cltsIO.setClntnum(chdrlifIO.getCownnum());
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			syserrrec.statuz.set(cltsIO.getStatuz());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			wsspcomn.longconfname.set(SPACES);
		}
		else {
			plainname();
		}
		sv.ownername.set(wsspcomn.longconfname);
	}

protected void otherDetails1300()
	{
		begin1310();
	}

protected void begin1310()
	{
		wsaaDesctabl.set(t5688);
		wsaaDescitem.set(chdrlifIO.getCnttype());
		wsaaDesccoy.set(wsspcomn.company);
		readDesc1500();
		sv.ctypedes.set(descIO.getLongdesc());
		wsaaDesctabl.set(t3623);
		wsaaDescitem.set(chdrlifIO.getStatcode());
		wsaaDesccoy.set(wsspcomn.company);
		readDesc1500();
		sv.rstate.set(descIO.getShortdesc());
		wsaaDesctabl.set(t3588);
		wsaaDescitem.set(chdrlifIO.getPstatcode());
		wsaaDesccoy.set(wsspcomn.company);
		readDesc1500();
		sv.pstate.set(descIO.getShortdesc());
	}

protected void getDespatch1400()
	{
		begin1410();
	}

protected void begin1410()
	{
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(chdrlifIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlifIO.getChdrnum());
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		sv.dlvrmode.set(hpadIO.getDlvrmode());
		sv.despdate.set(hpadIO.getDespdate());
		sv.packdate.set(hpadIO.getPackdate());
		sv.remdte.set(hpadIO.getRemdte());
		sv.deemdate.set(hpadIO.getDeemdate());
		sv.nextActDate.set(hpadIO.getNextActDate());
		if (isNE(sv.dlvrmode, SPACES)) {
			wsaaDesctabl.set(tr271);
			wsaaDescitem.set(sv.dlvrmode);
			wsaaDesccoy.set(wsspcomn.fsuco);
			readDesc1500();
			sv.shortds.set(descIO.getShortdesc());
		}
	}

protected void readDesc1500()
	{
		begin1510();
	}

protected void begin1510()
	{
		descIO.setDataKey(SPACES);
		descIO.setStatuz(varcom.oK);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsaaDesccoy);
		descIO.setDesctabl(wsaaDesctabl);
		descIO.setDescitem(wsaaDescitem);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setShortdesc("??????????");
			descIO.setLongdesc("??????????????????????????????");
		}
	}

protected void preScreenEdit()
	{
		preStart();
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(hpadIO.getIncexc(), "E")) {
			sv.packdateOut[varcom.pr.toInt()].set("Y");
			sv.packdateOut[varcom.nd.toInt()].set("Y");
			sv.remdteOut[varcom.nd.toInt()].set("Y");
			sv.deemdateOut[varcom.nd.toInt()].set("Y");
			sv.nxtdteOut[varcom.nd.toInt()].set("Y");
		}
		return ;
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					validate2020();
				case checkForErrors2080:
					checkForErrors2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/* Check if KILL pressed*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/* Check if refresh pressed*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validate2020()
	{
		if (isEQ(sv.packdate, varcom.vrcmMaxDate)) {
			sv.packdateErr.set(e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.packdate, 0)) {
			sv.packdateErr.set(e032);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isGT(sv.packdate, wsaaToday)) {
			sv.packdateErr.set(f073);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isLT(sv.packdate, hpadIO.getHissdte())) {
			sv.packdateErr.set(rlci);
			goTo(GotoLabel.checkForErrors2080);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")
		|| isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		if (isNE(scrnparams.statuz, varcom.kill)) {
			updateChdr3100();
			updateHpad3200();
			checkComm3300();
			createPtrn3500();
		}
		initialize(sftlockrec.sftlockRec);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.entity.set(chdrlifIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

protected void updateChdr3100()
	{
		begin3110();
	}

protected void begin3110()
	{
		chdrlifIO.setChdrcoy(chdrpf.getChdrcoy());
		chdrlifIO.setChdrnum(chdrpf.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
	}

protected void updateHpad3200()
	{
		begin3210();
	}

protected void begin3210()
	{
		hpadIO.setChdrcoy(chdrlifIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlifIO.getChdrnum());
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		hpadIO.setPackdate(sv.packdate);
		hpadIO.setNextActDate(varcom.vrcmMaxDate);
		hpadIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
	}

protected void checkComm3300()
	{
		begin3310();
	}

protected void begin3310()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(wsspcomn.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		if (isNE(th605rec.comind, "Y")) {
			return ;
		}
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(chdrlifIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlifIO.getChdrnum());
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		updateAgpy3400();
	}

protected void updateAgpy3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begin3410();
				case call3420:
					call3420();
					next3480();
				case exit3490:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin3410()
	{
		agpydocIO.setParams(SPACES);
		agpydocIO.setBatccoy(chdrlifIO.getChdrcoy());
		agpydocIO.setRdocnum(chdrlifIO.getChdrnum());
		agpydocIO.setTranno(0);
		agpydocIO.setJrnseq(0);
		agpydocIO.setFormat(agpydocrec);
		agpydocIO.setFunction(varcom.begn);
	}

protected void call3420()
	{
		SmartFileCode.execute(appVars, agpydocIO);
		if (isNE(agpydocIO.getStatuz(), varcom.oK)
		&& isNE(agpydocIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(agpydocIO.getStatuz());
			syserrrec.params.set(agpydocIO.getParams());
			fatalError600();
		}
		if (isEQ(agpydocIO.getStatuz(), varcom.endp)
		|| isNE(agpydocIO.getBatccoy(), chdrlifIO.getChdrcoy())
		|| isNE(agpydocIO.getRdocnum(), chdrlifIO.getChdrnum())) {
			agpydocIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3490);
		}
		if (isEQ(agpydocIO.getEffdate(), 0)
		|| isEQ(agpydocIO.getEffdate(), varcom.vrcmMaxDate)) {
			agpyagtIO.setParams(SPACES);
			agpyagtIO.setRrn(agpydocIO.getRrn());
			agpyagtIO.setFormat(agpyagtrec);
			agpyagtIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				fatalError600();
			}
			agpyagtIO.setEffdate(hpadIO.getPackdate());
			agpyagtIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void next3480()
	{
		agpydocIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call3420);
	}

protected void createPtrn3500()
	{
		begin3510();
	}

protected void begin3510()
	{
		ptrnIO.setDataKey(wsspcomn.batchkey);
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setRecode(SPACES);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setCrtuser(wsspcomn.userid);
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			compute(wsaaSub1, 0).set(add(wsspcomn.programPtr, 1));
			wsaaSub2.set(1);
			for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
				restoreProgram4100();
			}
		}
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void restoreProgram4100()
	{
		/*BEGIN*/
		wsspcomn.secProg[wsaaSub1.toInt()].set(wsaaSecProg[wsaaSub2.toInt()]);
		wsaaSub1.add(1);
		wsaaSub2.add(1);
		/*EXIT*/
	}


	public StringUtil getStringUtil() {
		return stringUtil;
	}

	public void setStringUtil(StringUtil stringUtil) {
		this.stringUtil = stringUtil;
	}

}
