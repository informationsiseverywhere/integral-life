/******************************************************************************
 * File Name 		: Asgnpf.java
 * Author			: nloganathan5
 * Creation Date	: 28 October 2016
 * Project			: Integral Life
 * Description		: The Model Class for ASGNPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.model;

import java.io.Serializable;

public class Asgnpf implements Serializable {

	// Default Serializable UID
	private static final long serialVersionUID = 1L;

	// Constant for database table name
	public static final String TABLE_NAME = "ASGNPF";

	//member variables for columns
	private long uniqueNumber;
	private Character chdrcoy;
	private String chdrnum;
	private String asgnpfx;
	private String asgnnum;
	private String assigname;
	private String reasoncd;
	private Integer commfrom;
	private Integer commto;
	private Integer tranno;
	private Integer seqno;
	private String trancde;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer userT;

	// Constructor
	public Asgnpf ( ) {};

	// Get Methods
	public long getUniqueNumber(){
		return this.uniqueNumber;
	}
	public Character getChdrcoy(){
		return this.chdrcoy;
	}
	public String getChdrnum(){
		return this.chdrnum;
	}
	public String getAsgnpfx(){
		return this.asgnpfx;
	}
	public String getAsgnnum(){
		return this.asgnnum;
	}
	public String getAssigname(){
		return this.assigname;
	}
	public String getReasoncd(){
		return this.reasoncd;
	}
	public Integer getCommfrom(){
		return this.commfrom;
	}
	public Integer getCommto(){
		return this.commto;
	}
	public Integer getTranno(){
		return this.tranno;
	}
	public Integer getSeqno(){
		return this.seqno;
	}
	public String getTrancde(){
		return this.trancde;
	}
	public String getTermid(){
		return this.termid;
	}
	public Integer getTrdt(){
		return this.trdt;
	}
	public Integer getTrtm(){
		return this.trtm;
	}
	public Integer getUserT(){
		return this.userT;
	}

	// Set Methods
	public void setUniqueNumber( long uniqueNumber ){
		 this.uniqueNumber = uniqueNumber;
	}
	public void setChdrcoy( Character chdrcoy ){
		 this.chdrcoy = chdrcoy;
	}
	public void setChdrnum( String chdrnum ){
		 this.chdrnum = chdrnum;
	}
	public void setAsgnpfx( String asgnpfx ){
		 this.asgnpfx = asgnpfx;
	}
	public void setAsgnnum( String asgnnum ){
		 this.asgnnum = asgnnum;
	}
	public void setAssigname( String assigname ){
		 this.assigname = assigname;
	}
	public void setReasoncd( String reasoncd ){
		 this.reasoncd = reasoncd;
	}
	public void setCommfrom( Integer commfrom ){
		 this.commfrom = commfrom;
	}
	public void setCommto( Integer commto ){
		 this.commto = commto;
	}
	public void setTranno( Integer tranno ){
		 this.tranno = tranno;
	}
	public void setSeqno( Integer seqno ){
		 this.seqno = seqno;
	}
	public void setTrancde( String trancde ){
		 this.trancde = trancde;
	}
	public void setTermid( String termid ){
		 this.termid = termid;
	}
	public void setTrdt( Integer trdt ){
		 this.trdt = trdt;
	}
	public void setTrtm( Integer trtm ){
		 this.trtm = trtm;
	}
	public void setUserT( Integer userT ){
		 this.userT = userT;
	}

	// ToString method
	public String toString(){

		StringBuilder output = new StringBuilder();

		output.append("UNIQUE_NUMBER:		");
		output.append(getUniqueNumber());
		output.append("\r\n");
		output.append("CHDRCOY:		");
		output.append(getChdrcoy());
		output.append("\r\n");
		output.append("CHDRNUM:		");
		output.append(getChdrnum());
		output.append("\r\n");
		output.append("ASGNPFX:		");
		output.append(getAsgnpfx());
		output.append("\r\n");
		output.append("ASGNNUM:		");
		output.append(getAsgnnum());
		output.append("\r\n");
		output.append("ASSIGNAME:		");
		output.append(getAssigname());
		output.append("\r\n");
		output.append("REASONCD:		");
		output.append(getReasoncd());
		output.append("\r\n");
		output.append("COMMFROM:		");
		output.append(getCommfrom());
		output.append("\r\n");
		output.append("COMMTO:		");
		output.append(getCommto());
		output.append("\r\n");
		output.append("TRANNO:		");
		output.append(getTranno());
		output.append("\r\n");
		output.append("SEQNO:		");
		output.append(getSeqno());
		output.append("\r\n");
		output.append("TRANCDE:		");
		output.append(getTrancde());
		output.append("\r\n");
		output.append("TERMID:		");
		output.append(getTermid());
		output.append("\r\n");
		output.append("TRDT:		");
		output.append(getTrdt());
		output.append("\r\n");
		output.append("TRTM:		");
		output.append(getTrtm());
		output.append("\r\n");
		output.append("USER_T:		");
		output.append(getUserT());
		output.append("\r\n");

		return output.toString();
	}

}
