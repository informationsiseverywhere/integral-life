/*
 * File: Ph583.java
 * Date: 30 August 2009 1:09:27
 * Author: Quipoz Limited
 * 
 * Class transformed from PH583.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.FlupenqTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.life.newbusiness.screens.Sh583ScreenVars;
import com.csc.life.newbusiness.tablestructures.Th585rec;
import com.csc.life.terminationclaims.dataaccess.FlupclmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* 1000-INITIALIZE
*     Retrieve the FLUP record kept in the calling program.
*     After storing the required information, release the IO Module
*     Set up the HXCLPF maintenance in INQUIRY Mode.
*
* 2000-SCREEN-EDIT
*     Perform editing on the screen depending on the Operation
*     Mode and the Action Code entered.
*
* 3000-UPDATE
*     Perform database updating according to the Operation Mode.
*
* 4000-WHERE-NEXT
*     If ROLU/ROLD, position the record pointer accordingly.
*     If in ADD Mode, set up blank record for another ADD.
*     If in MODIFY Mode, switch back to INQUIRY Mode.
*     Otherwise, next program.
*
*****************************************************************
* </pre>
*/
public class Ph583 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH583");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaInit = "";
	private ZonedDecimalData wsaaCounter = new ZonedDecimalData(1, 0).setUnsigned();
	private String wsaaFunction = "";
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).setUnsigned();
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(2, 0);
	private FixedLengthStringData wsaaFupcode = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaKeyFwd = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaKeyRev = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaKeyStored = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaHxclkey = new FixedLengthStringData(64);

	private FixedLengthStringData wsaaEndItemInd = new FixedLengthStringData(1);
	private Validator wsaaEndItem = new Validator(wsaaEndItemInd, "Y");
	private ZonedDecimalData wsaaIdx = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaItemseq = new FixedLengthStringData(2);
		/* ERRORS */
	private String e005 = "E005";
	private String e026 = "E026";
	private String e027 = "E027";
	private String g382 = "G382";
	private String f720 = "F720";
		/* TABLES */
	private String th585 = "TH585";
		/* FORMATS */
	private String fluplnbrec = "FLUPLNBREC";
	private String flupenqrec = "FLUPENQREC";
	private String flupclmrec = "FLUPCLMREC";
	private String hxclrec = "HXCLREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Follow Ups for Death Claims*/
	private FlupclmTableDAM flupclmIO = new FlupclmTableDAM();
		/*Follow Ups For Inquiry*/
	private FlupenqTableDAM flupenqIO = new FlupenqTableDAM();
		/*Follow-ups - life new business*/
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
		/*Exclusion Clauses Logical File*/
	private HxclTableDAM hxclIO = new HxclTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Th585rec th585rec = new Th585rec();
	private Sh583ScreenVars sv = ScreenProgram.getScreenVars( Sh583ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		retrFlupenq1020, 
		retrFlupclm1025, 
		readHxcl1030, 
		exit1190, 
		preExit, 
		checkForErrors2080, 
		exit2090, 
		exit3090, 
		exit3290, 
		exit3390, 
		exit3490, 
		exit4090, 
		scroll4120, 
		exit4190, 
		scroll4220, 
		rollup4280, 
		exit4290, 
		exit5190
	}

	public Ph583() {
		super();
		screenVars = sv;
		new ScreenModel("Sh583", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
				}
				case retrFlupenq1020: {
					retrFlupenq1020();
				}
				case retrFlupclm1025: {
					retrFlupclm1025();
				}
				case readHxcl1030: {
					readHxcl1030();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		if (wsaaInit.equals("N")) {
			goTo(GotoLabel.readHxcl1030);
		} else {
			wsaaInit = "Y";
		}
		
		fluplnbIO.setFormat(fluplnbrec);
		fluplnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)
		&& isNE(fluplnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		if (isEQ(fluplnbIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.retrFlupenq1020);
		}
		wsaaChdrnum.set(fluplnbIO.getChdrnum());
		wsaaFupno.set(fluplnbIO.getFupno());
		wsaaFupcode.set(fluplnbIO.getFupcode());
		fluplnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		goTo(GotoLabel.readHxcl1030);
	}

protected void retrFlupenq1020()
	{
		flupenqIO.setFormat(flupenqrec);
		flupenqIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, flupenqIO);
		if (isNE(flupenqIO.getStatuz(),varcom.oK)
		&& isNE(flupenqIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(flupenqIO.getParams());
			fatalError600();
		}
		if (isEQ(flupenqIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.retrFlupclm1025);
		}
		wsaaChdrnum.set(flupenqIO.getChdrnum());
		wsaaFupno.set(flupenqIO.getFupno());
		wsaaFupcode.set(flupenqIO.getFupcode());
		flupenqIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, flupenqIO);
		if (isNE(flupenqIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(flupenqIO.getParams());
			fatalError600();
		}
		goTo(GotoLabel.readHxcl1030);
	}

protected void retrFlupclm1025()
	{
		flupclmIO.setFormat(flupclmrec);
		flupclmIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
		wsaaChdrnum.set(flupclmIO.getChdrnum());
		wsaaFupno.set(flupclmIO.getFupno());
		wsaaFupcode.set(flupclmIO.getFupcode());
		flupclmIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, flupclmIO);
		if (isNE(flupclmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(flupclmIO.getParams());
			fatalError600();
		}
	}

protected void readHxcl1030()
	{
		readFirstHxcl1100();
		wsaaFunction = "INQUIRE";
		sv.chdrnum.set(wsaaChdrnum);
		sv.fupcode.set(wsaaFupcode);
		if (isEQ(hxclIO.getStatuz(),varcom.oK)) {
			sv.notemore.set("+");
			formatScreen4900();
		}
		if (isEQ(wsspcomn.flag,"I")) {
			sv.hxcllettypOut[varcom.pr.toInt()].set("Y");
			sv.hxclnote01Out[varcom.pr.toInt()].set("Y");
			sv.hxclnote02Out[varcom.pr.toInt()].set("Y");
			sv.hxclnote03Out[varcom.pr.toInt()].set("Y");
			sv.hxclnote04Out[varcom.pr.toInt()].set("Y");
			sv.hxclnote05Out[varcom.pr.toInt()].set("Y");
			sv.hxclnote06Out[varcom.pr.toInt()].set("Y");
			sv.actionOut[varcom.pr.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void readFirstHxcl1100()
	{
		try {
			readFirstHxclRec1110();
		}
		catch (GOTOException e){
		}
	}

protected void readFirstHxclRec1110()
	{
		hxclIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hxclIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hxclIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "FUPNO");
		hxclIO.setFormat(hxclrec);
		hxclIO.setDataKey(SPACES);
		hxclIO.setChdrcoy(wsspcomn.company);
		hxclIO.setChdrnum(wsaaChdrnum);
		hxclIO.setFupno(wsaaFupno);
		hxclIO.setHxclseqno(ZERO);
		wsaaKeyFwd.set(hxclIO.getDataKey());
		wsaaKeyRev.set(hxclIO.getDataKey());
		SmartFileCode.execute(appVars, hxclIO);
		if (isEQ(hxclIO.getStatuz(),varcom.mrnf)
		|| isEQ(hxclIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1190);
		}
		if (isNE(hxclIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isNE(hxclIO.getChdrcoy(),wsspcomn.company)
		|| isNE(hxclIO.getChdrnum(),wsaaChdrnum)
		|| isNE(hxclIO.getFupno(),wsaaFupno)) {
			hxclIO.setStatuz(varcom.endp);
		}
	}

protected void setUpForInq1200()
	{
		/*INITIALIZE*/
		sv.hxcllettyp.set(hxclIO.getHxcllettyp());
		sv.hxclnotes.set(hxclIO.getHxclnotes());
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsaaFunction,"ADD")
		|| (isEQ(wsaaFunction,"MODIFY")
		&& isNE(sv.notemore,SPACES))) {
			sv.hxclnote01Out[varcom.pr.toInt()].set(SPACES);
			sv.hxcllettypOut[varcom.pr.toInt()].set(SPACES);
			sv.actionOut[varcom.pr.toInt()].set("Y");
			scrnparams.positionCursor.set("HXCLLETTYP");
			
			/* Start of ILIFE-5685 */
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
			/* End of ILIFE-5685 */
		}
		else {
			sv.hxclnote01Out[varcom.pr.toInt()].set("Y");
			sv.hxcllettypOut[varcom.pr.toInt()].set("Y");
			sv.action.set(SPACES);
			scrnparams.positionCursor.set("ACTION");
			if (isEQ(wsaaFunction,"DELETE")
			|| isEQ(wsspcomn.flag,"I")) {
				sv.actionOut[varcom.pr.toInt()].set("Y");
			}
			else {
				sv.actionOut[varcom.pr.toInt()].set(SPACES);
			}
		}
		if (isEQ(wsaaInit,"Y")) {
			wsaaInit = "N";
			scrnparams.function.set(varcom.init);
			goTo(GotoLabel.preExit);
		}
		else {
			//scrnparams.function.set(varcom.norml);
			wsspcomn.sectionno.set("3000");
		}
		
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.rolu)
		|| isEQ(scrnparams.statuz,varcom.rold)) {
			wsaaFunction = "INQUIRE";
			sv.action.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2020()
	{
		if (isNE(sv.action,"A")
		&& isNE(sv.action,"M")
		&& isNE(sv.action,"D")
		&& isNE(sv.action,SPACES)) {
			sv.actionErr.set(e005);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(sv.action,"A")) {
			if (isNE(wsaaFunction,"ADD")) {
				wsaaFunction = "ADD";
				wsspcomn.edterror.set("Y");
				setUpForAdd5200();
				goTo(GotoLabel.exit2090);
			}
			else {
				if (isEQ(sv.hxclnotes,SPACES)
				&& isEQ(sv.hxcllettyp,SPACES)) {
					wsaaFunction = "INQUIRE";
					sv.action.set(SPACES);
					wsspcomn.edterror.set("Y");
					wsaaKeyRev.set(hxclIO.getDataKey());
					wsaaKeyFwd.set(hxclIO.getDataKey());
					if (isEQ(sv.notemore,"+")) {
						formatScreen4900();
					}
					goTo(GotoLabel.exit2090);
				}
			}
		}
		if (isEQ(sv.action,"M")
		&& isNE(wsaaFunction,"MODIFY")) {
			wsaaFunction = "MODIFY";
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.action,"D")
		&& isNE(wsaaFunction,"DELETE")) {
			wsaaFunction = "DELETE";
			if (isNE(sv.notemore,SPACES)) {
				scrnparams.errorCode.set(f720);
			}
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		 
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsaaFunction,"INQUIRE")) {
			scrnparams.statuz.set(varcom.oK);
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaFunction,"ADD")) {
			if (isNE(sv.hxcllettyp,SPACES)) {
				wsaaEndItemInd.set("N");
				for (wsaaIdx.set(0); !(wsaaEndItem.isTrue()); wsaaIdx.add(1)){
					readTh5853400();
					if (!wsaaEndItem.isTrue()) {
						fmtScrn3500();
						addHxclRecord3100();
					}
				}
			}
			else {
				if (isNE(sv.hxclnotes,SPACES)) {
					addHxclRecord3100();
				}
			}
		}
		if (isEQ(wsaaFunction,"MODIFY")) {
			modifyHxclRecord3200();
		}
		if (isEQ(wsaaFunction,"DELETE")
		&& isEQ(scrnparams.statuz,varcom.oK)) {
			deleteHxclRecord3300();
			scrnparams.statuz.set(varcom.rold);
		}
		if (isEQ(sv.action,"A")) {
			setUpForAdd5200();
		}
	}

protected void addHxclRecord3100()
	{
		beginAdd3110();
	}

protected void beginAdd3110()
	{
		getNextSeqno5100();
		hxclIO.setDataArea(SPACES);
		hxclIO.setChdrcoy(wsspcomn.company);
		hxclIO.setChdrnum(wsaaChdrnum);
		hxclIO.setFupno(wsaaFupno);
		hxclIO.setFupcode(wsaaFupcode);
		hxclIO.setHxcllettyp(sv.hxcllettyp);
		hxclIO.setHxclnotes(sv.hxclnotes);
		hxclIO.setUser(varcom.vrcmUser);
		hxclIO.setHxclseqno(wsaaSeqno);
		hxclIO.setFunction(varcom.writr);
		hxclIO.setFormat(hxclrec);
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		sv.notemore.set("+");
	}

protected void modifyHxclRecord3200()
	{
		try {
			beginModify3210();
		}
		catch (GOTOException e){
		}
	}

protected void beginModify3210()
	{
		wsaaFunction = "INQUIRE";
		if (isEQ(sv.notemore,SPACES)) {
			goTo(GotoLabel.exit3290);
		}
		hxclIO.setDataKey(wsaaKeyStored);
		hxclIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hxclIO);
		if (isEQ(hxclIO.getStatuz(),varcom.held)) {
			sv.actionErr.set(g382);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3290);
		}
		if (isNE(hxclIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		hxclIO.setHxclnotes(sv.hxclnotes);
		hxclIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
	}

protected void deleteHxclRecord3300()
	{
		try {
			beginDelete3310();
		}
		catch (GOTOException e){
		}
	}

protected void beginDelete3310()
	{
		wsaaFunction = "INQUIRE";
		if (isEQ(sv.notemore,SPACES)) {
			goTo(GotoLabel.exit3390);
		}
		hxclIO.setDataKey(wsaaKeyStored);
		hxclIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hxclIO);
		if (isEQ(hxclIO.getStatuz(),varcom.held)) {
			sv.actionErr.set(g382);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit3390);
		}
		if (isNE(hxclIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		hxclIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		sv.hxcllettyp.set(SPACES);
		sv.hxclnotes.set(SPACES);
	}

protected void readTh5853400()
	{
		try {
			beginRead3410();
		}
		catch (GOTOException e){
		}
	}

protected void beginRead3410()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(th585);
		itemIO.setItemitem(sv.hxcllettyp);
		if (isEQ(wsaaIdx,ZERO)) {
			wsaaItemseq.set(SPACES);
		}
		else {
			wsaaItemseq.set(wsaaIdx);
		}
		itemIO.setItemseq(wsaaItemseq);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)
		|| isEQ(itemIO.getStatuz(),varcom.endp)) {
			wsaaEndItemInd.set("Y");
			goTo(GotoLabel.exit3490);
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		th585rec.th585Rec.set(itemIO.getGenarea());
		if (isEQ(th585rec.hxclnotes,SPACES)) {
			wsaaEndItemInd.set("Y");
		}
	}

protected void fmtScrn3500()
	{
		/*BEGIN-FMT*/
		sv.hxclnotes.set(th585rec.hxclnotes);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		try {
			checkRoluRold4010();
			//nextProgram4080();
		}
		catch (GOTOException e){
		}
	}

protected void checkRoluRold4010()
	{
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			rollup4100();
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		if (isEQ(scrnparams.statuz,varcom.rold)) {
			rolldown4200();
			wsspcomn.nextprog.set(scrnparams.scrname);
			goTo(GotoLabel.exit4090);
		}
		/*if (isEQ(sv.action,SPACES)) {
			//wsspcomn.nextprog.set(scrnparams.scrname);
			//goTo(GotoLabel.exit4090);
//			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.nextprog.set(new FixedLengthStringData(5).init("Pr589"));
			wsspcomn.programPtr.add(1);
		}*/
		nextProgram4080();
	}

protected void nextProgram4080()
	{
		wsaaInit = "Y";
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
	}

protected void rollup4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					rollingUp4110();
				}
				case scroll4120: {
					scroll4120();
				}
				case exit4190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void rollingUp4110()
	{
		if (isEQ(wsaaKeyFwd,varcom.endp)) {
			scrnparams.errorCode.set(e026);
			goTo(GotoLabel.exit4190);
		}
		if (isEQ(hxclIO.getFunction(),varcom.begn)) {
			hxclIO.setFunction(varcom.nextr);
		}
		else {
			hxclIO.setFunction(varcom.begn);
		}
		wsaaCounter.set(1);
	}

protected void scroll4120()
	{
		hxclIO.setDataKey(wsaaKeyFwd);
		hxclIO.setFormat(hxclrec);
		SmartFileCode.execute(appVars, hxclIO);
		if (isEQ(hxclIO.getStatuz(),varcom.mrnf)
		|| isEQ(hxclIO.getStatuz(),varcom.endp)) {
			scrnparams.errorCode.set(e026);
			wsaaKeyFwd.set(varcom.endp);
			goTo(GotoLabel.exit4190);
		}
		if (isNE(hxclIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isNE(hxclIO.getChdrcoy(),wsspcomn.company)
		|| isNE(hxclIO.getChdrnum(),wsaaChdrnum)
		|| isNE(hxclIO.getFupno(),wsaaFupno)) {
			scrnparams.errorCode.set(e026);
			wsaaKeyFwd.set(varcom.endp);
			goTo(GotoLabel.exit4190);
		}
		if (isEQ(wsaaCounter,2)) {
			wsaaKeyFwd.set(hxclIO.getDataKey());
			goTo(GotoLabel.exit4190);
		}
		wsaaKeyRev.set(hxclIO.getDataKey());
		wsaaCounter.add(1);
		formatScreen4900();
		hxclIO.setFunction(varcom.nextr);
		goTo(GotoLabel.scroll4120);
	}

protected void rolldown4200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					rollingDown4210();
				}
				case scroll4220: {
					scroll4220();
				}
				case rollup4280: {
					rollup4280();
				}
				case exit4290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void rollingDown4210()
	{
		if (isEQ(wsaaKeyRev,varcom.endp)) {
			scrnparams.errorCode.set(e027);
			goTo(GotoLabel.exit4290);
		}
		hxclIO.setFunction(varcom.begn);
	}

protected void scroll4220()
	{
		hxclIO.setDataKey(wsaaKeyRev);
		hxclIO.setFormat(hxclrec);
		SmartFileCode.execute(appVars, hxclIO);
		if (isEQ(hxclIO.getStatuz(),varcom.mrnf)
		|| isEQ(hxclIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.rollup4280);
		}
		if (isNE(hxclIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isNE(hxclIO.getChdrcoy(),wsspcomn.company)
		|| isNE(hxclIO.getChdrnum(),wsaaChdrnum)
		|| isNE(hxclIO.getFupno(),wsaaFupno)) {
			goTo(GotoLabel.rollup4280);
		}
		wsaaHxclkey.set(hxclIO.getDataKey());
		if (isEQ(hxclIO.getFunction(),varcom.begn)) {
			wsaaKeyFwd.set(hxclIO.getDataKey());
			hxclIO.setFunction(varcom.nextp);
			goTo(GotoLabel.scroll4220);
		}
		formatScreen4900();
		wsaaKeyRev.set(hxclIO.getDataKey());
		goTo(GotoLabel.exit4290);
	}

protected void rollup4280()
	{
		if (isEQ(hxclIO.getFunction(),varcom.begn)) {
			hxclIO.setFunction(varcom.endr);
			goTo(GotoLabel.scroll4220);
		}
		wsaaKeyRev.set(varcom.endp);
		scrnparams.errorCode.set(e027);
		if (isEQ(hxclIO.getFunction(),varcom.endr)) {
			sv.notemore.set(SPACES);
			goTo(GotoLabel.exit4290);
		}
		wsaaKeyFwd.set(wsaaHxclkey);
		rollup4100();
		scrnparams.errorCode.set(e027);
	}

protected void formatScreen4900()
	{
		/*LOAD-SCREEN*/
		wsaaKeyStored.set(hxclIO.getDataKey());
		sv.chdrnum.set(hxclIO.getChdrnum());
		sv.fupcode.set(hxclIO.getFupcode());
		sv.hxcllettyp.set(hxclIO.getHxcllettyp());
		sv.hxclnotes.set(hxclIO.getHxclnotes());
		sv.notemore.set("+");
		/*EXIT*/
	}

protected void getNextSeqno5100()
	{
		try {
			readLastSeqno5110();
		}
		catch (GOTOException e){
		}
	}

protected void readLastSeqno5110()
	{
		hxclIO.setDataKey(SPACES);
		hxclIO.setChdrcoy(wsspcomn.company);
		hxclIO.setChdrnum(wsaaChdrnum);
		hxclIO.setFupno(wsaaFupno);
		hxclIO.setHxclseqno(99);
		hxclIO.setFunction(varcom.endr);
		hxclIO.setFormat(hxclrec);
		SmartFileCode.execute(appVars, hxclIO);
		if (isEQ(hxclIO.getStatuz(),varcom.mrnf)
		|| isEQ(hxclIO.getStatuz(),varcom.endp)) {
			wsaaSeqno.set(1);
			goTo(GotoLabel.exit5190);
		}
		if (isNE(hxclIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isNE(hxclIO.getChdrcoy(),wsspcomn.company)
		|| isNE(hxclIO.getChdrnum(),wsaaChdrnum)
		|| isNE(hxclIO.getFupno(),wsaaFupno)) {
			wsaaSeqno.set(1);
			goTo(GotoLabel.exit5190);
		}
		wsaaSeqno.set(hxclIO.getHxclseqno());
		wsaaSeqno.add(1);
	}

protected void setUpForAdd5200()
	{
		/*INIT*/
		wsaaFunction = "ADD";
		sv.hxcllettyp.set(SPACES);
		sv.hxclnotes.set(SPACES);
		/*EXIT*/
	}

protected void setUpForInq5300()
	{
		/*INIT*/
		wsaaFunction = "INQUIRE";
		sv.action.set(SPACES);
		sv.hxclnotes.set(hxclIO.getHxclnotes());
		/*EXIT*/
	}

}
