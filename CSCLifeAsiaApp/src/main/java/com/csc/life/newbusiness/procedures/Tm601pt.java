/*
 * File: Tm601pt.java
 * Date: 30 August 2009 2:38:23
 * Author: Quipoz Limited
 * 
 * Class transformed from TM601PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.Tm601rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TM601.
*
*
*****************************************************************
* </pre>
*/
public class Tm601pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine001, 26, FILLER).init("GL Code for OCC Guidelines                   SM601");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(16);
	private FixedLengthStringData filler7 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 6, FILLER).init("GL Account");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(72);
	private FixedLengthStringData filler9 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 6);
	private FixedLengthStringData filler10 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 32);
	private FixedLengthStringData filler11 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine004, 58);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(72);
	private FixedLengthStringData filler12 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine005, 6);
	private FixedLengthStringData filler13 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine005, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine005, 32);
	private FixedLengthStringData filler14 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine005, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine005, 58);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(72);
	private FixedLengthStringData filler15 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 6);
	private FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 32);
	private FixedLengthStringData filler17 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine006, 58);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(72);
	private FixedLengthStringData filler18 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 6);
	private FixedLengthStringData filler19 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 32);
	private FixedLengthStringData filler20 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine007, 58);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(72);
	private FixedLengthStringData filler21 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine008, 6);
	private FixedLengthStringData filler22 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine008, 32);
	private FixedLengthStringData filler23 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine008, 58);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(72);
	private FixedLengthStringData filler24 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine009, 6);
	private FixedLengthStringData filler25 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine009, 32);
	private FixedLengthStringData filler26 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine009, 58);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(72);
	private FixedLengthStringData filler27 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine010, 6);
	private FixedLengthStringData filler28 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine010, 32);
	private FixedLengthStringData filler29 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine010, 58);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(72);
	private FixedLengthStringData filler30 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine011, 6);
	private FixedLengthStringData filler31 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine011, 32);
	private FixedLengthStringData filler32 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine011, 58);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(72);
	private FixedLengthStringData filler33 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine012, 6);
	private FixedLengthStringData filler34 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine012, 32);
	private FixedLengthStringData filler35 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine012, 58);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(72);
	private FixedLengthStringData filler36 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine013, 6);
	private FixedLengthStringData filler37 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 20, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine013, 32);
	private FixedLengthStringData filler38 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(14).isAPartOf(wsaaPrtLine013, 58);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(28);
	private FixedLengthStringData filler39 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tm601rec tm601rec = new Tm601rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tm601pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tm601rec.tm601Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo011.set(tm601rec.genlcdex03);
		fieldNo014.set(tm601rec.genlcdex04);
		fieldNo008.set(tm601rec.genlcdex02);
		fieldNo005.set(tm601rec.genlcdex01);
		fieldNo017.set(tm601rec.genlcdex05);
		fieldNo020.set(tm601rec.genlcdex06);
		fieldNo029.set(tm601rec.genlcdex09);
		fieldNo032.set(tm601rec.genlcdex10);
		fieldNo023.set(tm601rec.genlcdex07);
		fieldNo026.set(tm601rec.genlcdex08);
		fieldNo006.set(tm601rec.genlcdex11);
		fieldNo009.set(tm601rec.genlcdex12);
		fieldNo012.set(tm601rec.genlcdex13);
		fieldNo015.set(tm601rec.genlcdex14);
		fieldNo018.set(tm601rec.genlcdex15);
		fieldNo021.set(tm601rec.genlcdex16);
		fieldNo024.set(tm601rec.genlcdex17);
		fieldNo027.set(tm601rec.genlcdex18);
		fieldNo030.set(tm601rec.genlcdex19);
		fieldNo033.set(tm601rec.genlcdex20);
		fieldNo007.set(tm601rec.genlcdex21);
		fieldNo010.set(tm601rec.genlcdex22);
		fieldNo013.set(tm601rec.genlcdex23);
		fieldNo016.set(tm601rec.genlcdex24);
		fieldNo019.set(tm601rec.genlcdex25);
		fieldNo022.set(tm601rec.genlcdex26);
		fieldNo025.set(tm601rec.genlcdex27);
		fieldNo028.set(tm601rec.genlcdex28);
		fieldNo031.set(tm601rec.genlcdex29);
		fieldNo034.set(tm601rec.genlcdex30);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
