package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:31
 * Description:
 * Copybook name: COVTTRMKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covttrmkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covttrmFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covttrmKey = new FixedLengthStringData(64).isAPartOf(covttrmFileKey, 0, REDEFINE);
  	public FixedLengthStringData covttrmChdrcoy = new FixedLengthStringData(1).isAPartOf(covttrmKey, 0);
  	public FixedLengthStringData covttrmChdrnum = new FixedLengthStringData(8).isAPartOf(covttrmKey, 1);
  	public FixedLengthStringData covttrmLife = new FixedLengthStringData(2).isAPartOf(covttrmKey, 9);
  	public FixedLengthStringData covttrmCoverage = new FixedLengthStringData(2).isAPartOf(covttrmKey, 11);
  	public FixedLengthStringData covttrmRider = new FixedLengthStringData(2).isAPartOf(covttrmKey, 13);
  	public PackedDecimalData covttrmSeqnbr = new PackedDecimalData(3, 0).isAPartOf(covttrmKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(covttrmKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covttrmFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covttrmFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}