package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.common.DD;
import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR504.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rr504Report extends SMARTReportLayout { 

	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length); //Starts ILIFE-3212
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr5 = new FixedLengthStringData(DD.cltaddr.length); //End ILIFE-3212
	private FixedLengthStringData atranamt = new FixedLengthStringData(17);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData crtabled = new FixedLengthStringData(30);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22);
	private FixedLengthStringData payfrqd = new FixedLengthStringData(10);
	private FixedLengthStringData rcesdt = new FixedLengthStringData(10);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr504Report() {
		super();
	}


	/**
	 * Print the XML for Rr504d01
	 */
	public void printRr504d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		crtabled.setFieldName("crtabled");
		crtabled.setInternal(subString(recordData, 1, 30));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 31, 17));
		singp.setFieldName("singp");
		singp.setInternal(subString(recordData, 48, 17));
		printLayout("Rr504d01",			// Record name
			new BaseData[]{			// Fields:
				crtabled,
				sumins,
				singp
			}
		);

	}

	/**
	 * Print the XML for Rr504d02
	 */
	public void printRr504d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rr504d02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rr504h01
	 */
	public void printRr504h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datetexc.setFieldName("datetexc");
		datetexc.setInternal(subString(recordData, 1, 22));
		clntnm.setFieldName("clntnm");
		clntnm.setInternal(subString(recordData, 23, 37));
		addr1.setFieldName("addr1");
		addr1.setInternal(subString(recordData, 60, DD.cltaddr.length)); //Starts ILIFE-3212
		addr2.setFieldName("addr2");
		addr2.setInternal(subString(recordData, 60+DD.cltaddr.length*1, DD.cltaddr.length));
		addr3.setFieldName("addr3");
		addr3.setInternal(subString(recordData, 60+DD.cltaddr.length*2, DD.cltaddr.length));
		addr4.setFieldName("addr4");
		addr4.setInternal(subString(recordData, 60+DD.cltaddr.length*3, DD.cltaddr.length));
		addr5.setFieldName("addr5");
		addr5.setInternal(subString(recordData, 60+DD.cltaddr.length*4, DD.cltaddr.length));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 60+DD.cltaddr.length*5, 8));
		rcesdt.setFieldName("rcesdt");
		rcesdt.setInternal(subString(recordData, 68+DD.cltaddr.length*5, 10));
		payfrqd.setFieldName("payfrqd");
		payfrqd.setInternal(subString(recordData, 78+DD.cltaddr.length*5, 10));
		cntdesc.setFieldName("cntdesc");
		cntdesc.setInternal(subString(recordData, 88+DD.cltaddr.length*5, 30));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 118+DD.cltaddr.length*5, 3));
		atranamt.setFieldName("atranamt");
		atranamt.setInternal(subString(recordData, 121+DD.cltaddr.length*5, 17)); //End ILIFE-3212
		printLayout("Rr504h01",			// Record name
			new BaseData[]{			// Fields:
				datetexc,
				clntnm,
				addr1,
				addr2,
				addr3,
				addr4,
				addr5,
				chdrnum,
				rcesdt,
				payfrqd,
				cntdesc,
				cntcurr,
				atranamt
			}
		);

		currentPrintLine.set(31);
	}


}
