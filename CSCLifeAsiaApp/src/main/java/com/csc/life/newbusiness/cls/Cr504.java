/*
 * File: Cr504.java
 * Date: 30 August 2009 2:59:18
 * Author: $Id$
 * 
 * Class transformed from CR504.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.life.newbusiness.batchprograms.Br504;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.printing.PrintManager;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Cr504 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData parm = new FixedLengthStringData(299);
	private FixedLengthStringData statuz = new FixedLengthStringData(4);

	public Cr504() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		parm = convertAndSetParam(parm, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			switch (qState) {
			case QS_START: {
				try {
					statuz.set(subString(parm, 1, 4));
				}
				catch (ExtMsgException ex){
					if (ex.messageMatches("CPF0000")
					|| ex.messageMatches("LBE0000")) {
						qState = error;
						break;
					}
					else {
						throw ex;
					}
				}
				new PrintManager().overridePrinterFile("RR504", "RR504", new String[] {"PRTQLTY", "SAVE"}, new Object[] {"*NLQ", "TRUE"});
				callProgram(Br504.class, new Object[] {parm});
			}
			case returnVar: {
				return ;
			}
			case error: {
				appVars.sendMessageToQueue("Unexpected errors occurred", "*");
				statuz.set("BOMB");
				qState = returnVar;
				break;
			}
			default:{
				qState = QS_END;
			}
			}
		}
		
	}
}
