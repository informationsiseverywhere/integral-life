/*
 * File: Th565pt.java
 * Date: 30 August 2009 2:36:11
 * Author: Quipoz Limited
 * 
 * Class transformed from TH565PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH565.
*
*
*****************************************************************
* </pre>
*/
public class Th565pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(31).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(45).isAPartOf(wsaaPrtLine001, 31, FILLER).init("Allowable Transactions Table            SH565");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(36);
	private FixedLengthStringData filler7 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine003, 12, FILLER).init("Base Currency:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine003, 33);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(70);
	private FixedLengthStringData filler9 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine004, 12, FILLER).init("Transaction Codes:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 33);
	private FixedLengthStringData filler11 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine004, 40);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(70);
	private FixedLengthStringData filler12 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 33);
	private FixedLengthStringData filler13 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine005, 40);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(70);
	private FixedLengthStringData filler14 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler15 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine006, 40);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(70);
	private FixedLengthStringData filler16 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler17 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine007, 40);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(70);
	private FixedLengthStringData filler18 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 33);
	private FixedLengthStringData filler19 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine008, 40);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(70);
	private FixedLengthStringData filler20 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 33);
	private FixedLengthStringData filler21 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine009, 40);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(70);
	private FixedLengthStringData filler22 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 33);
	private FixedLengthStringData filler23 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine010, 40);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(70);
	private FixedLengthStringData filler24 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler25 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine011, 40);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(70);
	private FixedLengthStringData filler26 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 33);
	private FixedLengthStringData filler27 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine012, 40);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(70);
	private FixedLengthStringData filler28 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 33);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine013, 40);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(76);
	private FixedLengthStringData filler30 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler31 = new FixedLengthStringData(50).isAPartOf(wsaaPrtLine014, 26, FILLER).init("1st Quarter  2nd Quarter  3rd Quarter  4th Quarter");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(72);
	private FixedLengthStringData filler32 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler33 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine015, 12, FILLER).init("Period From:");
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 31).setPattern("ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine015, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 44).setPattern("ZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine015, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 57).setPattern("ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine015, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 70).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(72);
	private FixedLengthStringData filler37 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler38 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine016, 21, FILLER).init("To:");
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 31).setPattern("ZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine016, 33, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 44).setPattern("ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine016, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 57).setPattern("ZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine016, 59, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 70).setPattern("ZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th565rec th565rec = new Th565rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th565pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th565rec.th565Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(th565rec.currcode);
		fieldNo006.set(th565rec.ztrcde01);
		fieldNo007.set(th565rec.trandesc01);
		fieldNo008.set(th565rec.ztrcde02);
		fieldNo009.set(th565rec.trandesc02);
		fieldNo010.set(th565rec.ztrcde03);
		fieldNo011.set(th565rec.trandesc03);
		fieldNo012.set(th565rec.ztrcde04);
		fieldNo013.set(th565rec.trandesc04);
		fieldNo014.set(th565rec.ztrcde05);
		fieldNo015.set(th565rec.trandesc05);
		fieldNo016.set(th565rec.ztrcde06);
		fieldNo017.set(th565rec.trandesc06);
		fieldNo018.set(th565rec.ztrcde07);
		fieldNo019.set(th565rec.trandesc07);
		fieldNo020.set(th565rec.ztrcde08);
		fieldNo021.set(th565rec.trandesc08);
		fieldNo022.set(th565rec.ztrcde09);
		fieldNo023.set(th565rec.trandesc09);
		fieldNo024.set(th565rec.ztrcde10);
		fieldNo025.set(th565rec.trandesc10);
		fieldNo026.set(th565rec.zmthfrm01);
		fieldNo027.set(th565rec.zmthfrm02);
		fieldNo028.set(th565rec.zmthfrm03);
		fieldNo029.set(th565rec.zmthfrm04);
		fieldNo030.set(th565rec.zmthto01);
		fieldNo031.set(th565rec.zmthto02);
		fieldNo032.set(th565rec.zmthto03);
		fieldNo033.set(th565rec.zmthto04);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
