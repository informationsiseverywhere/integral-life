package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:24
 * Description:
 * Copybook name: TH556REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th556rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th556Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zfooter = new FixedLengthStringData(78).isAPartOf(th556Rec, 0);
  	public FixedLengthStringData ztelno = new FixedLengthStringData(10).isAPartOf(th556Rec, 78);
  	public FixedLengthStringData zunders = new FixedLengthStringData(75).isAPartOf(th556Rec, 88);
  	public FixedLengthStringData[] zunder = FLSArrayPartOfStructure(3, 25, zunders, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(75).isAPartOf(zunders, 0, FILLER_REDEFINE);
  	public FixedLengthStringData zunder01 = new FixedLengthStringData(25).isAPartOf(filler, 0);
  	public FixedLengthStringData zunder02 = new FixedLengthStringData(25).isAPartOf(filler, 25);
  	public FixedLengthStringData zunder03 = new FixedLengthStringData(25).isAPartOf(filler, 50);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(337).isAPartOf(th556Rec, 163, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th556Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th556Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}