package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:36
 * Description:
 * Copybook name: PAYRLIFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Payrlifkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData payrlifFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData payrlifKey = new FixedLengthStringData(64).isAPartOf(payrlifFileKey, 0, REDEFINE);
  	public FixedLengthStringData payrlifChdrcoy = new FixedLengthStringData(1).isAPartOf(payrlifKey, 0);
  	public FixedLengthStringData payrlifChdrnum = new FixedLengthStringData(8).isAPartOf(payrlifKey, 1);
  	public PackedDecimalData payrlifPayrseqno = new PackedDecimalData(1, 0).isAPartOf(payrlifKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(54).isAPartOf(payrlifKey, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(payrlifFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		payrlifFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}