package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.screens.Sr5ahScreenVars;
import com.csc.life.newbusiness.screens.Sr5aiScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.terminationclaims.tablestructures.Tr5agrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.impl.DescDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;

public class Pr5ai extends ScreenProgCS{
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR5AI");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private Sr5aiScreenVars sv = ScreenProgram.getScreenVars( Sr5aiScreenVars.class);
	private Wssplife wssplife = new Wssplife();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	public List<String> listProgdesc = new ArrayList<String>();
	private DescTableDAM descIO = new DescTableDAM();
	private Tr5agrec tr5agrec = new Tr5agrec();
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class);
	private DescDAO descdao =new DescDAOImpl();
	private Exclpf exclpf=null;
	private Sr5ahScreenVars sv1 = ScreenProgram.getScreenVars( Sr5ahScreenVars.class);
	private Datcon2rec datcon2rec = new Datcon2rec();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	//PINNACLE-2855
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Chdrpf chdrobj = null;
	private Chdrpf chdrpf = new Chdrpf();
	private int wsaaTranno=0;
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private int wssaSeqnbr=0;
	private Covrpf covrpf = new Covrpf();
	private Covrpf covr = new Covrpf();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	
	public Pr5ai() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5ah", AppVars.getInstance(), sv1);
		new ScreenModel("Sr5ai", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
					initialise1010();
					}

protected void initialise1010()
	{
    
    /* Read CHDRLNB (RETRV)  in  order to obtain the contract header*/
	/* information.  If  the  number of policies in the plan is zero*/
	/* or  one  then Plan-processing does not apply. If there is any*/
	/* other  numeric  value,  this  value  indicates  the number of*/
	/* policies in the Plan.*/
	covrpf = new Covrpf();
	covrpf = covrpfDAO.getCacheObject(covrpf);
	if (null == covrpf) {
		covtlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		} else {
			covrpf = new Covrpf();
			covrpf.setLife(covtlnbIO.getLife().toString());
			covrpf.setCoverage(covtlnbIO.getCoverage().toString());
			covrpf.setRider(covtlnbIO.getRider().toString());
			}
	}
		chdrpf = new Chdrpf();
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if (null == chdrpf) {
			chdrlnbIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			} else {
				chdrpf = new Chdrpf();
					chdrpf.setTranno(chdrlnbIO.getTranno().toInt());
				}
		}
		if (chdrpf.getTranno() > 1) {
			wsaaTranno = chdrpf.getTranno() + 1;
		} else {
			wsaaTranno = chdrpf.getTranno();
		}
	sv.dataArea.set(SPACES);
	sv.exadtxt.set(" ");
	if (isNE(wsspcomn.flag, "C")){
		sv.excda.set(sv1.excda);
		readPrimaryRecord1020(sv1.excda.toString());
		exclpf=exclpfDAO.readRecordByCode(wsspcomn.chdrChdrnum.toString(), wsspcomn.crtable.toString(), sv.excda.trim(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());
		if(exclpf!=null)
			sv.exadtxt.set(exclpf.getExadtxt());
		
	}
	sv.excltxtOut[varcom.pr.toInt()].set("Y");
	
	
	}

protected void readPrimaryRecord1020(String tableKey)
	{
	    listProgdesc.clear();
		for(int i = 0 ; i <= 2; i++) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			wsaaSeq.set(i);
			itemIO.setItemitem(tableKey);
			if (isEQ(wsaaSeq, ZERO)) {
				itemIO.setItemseq(SPACES);
			}
			else {
				itemIO.setItemseq(wsaaSeq);
			}
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl("TR5AG");
			itemIO.setFormat(itemrec);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();			
			}
			readRecord1031();
		
		}
		StringBuilder strBuilder = new StringBuilder();		
		for (int i=0; i<listProgdesc.size(); i++) {
			strBuilder.append(listProgdesc.get(i).trim());/* IJTI-1523 */
		}
		sv.excltxt.set(strBuilder.toString());
		
		
		}
		
		
	
protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
//		wsaaFirstTime = "N";
		moveToScreen1040();
	}

protected void moveToScreen1040()
	{
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)
		&& isEQ(itemIO.getItemseq(), SPACES)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		
		tr5agrec.tr5agRec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(), SPACES)) {
			generalArea1045();
		}else{
			return;
		}
		
	}

protected void generalArea1045()
	{	
		sv.excltxt.set(tr5agrec.progdescs);
		try {
			listProgdesc.add(sv.excltxt.toString());
		} catch (Exception ex) {
			
		}
		/*CONFIRMATION-FIELDS*/
	}

protected void preScreenEdit()
	{
	if (isEQ(scrnparams.statuz, varcom.kill)) {
		return;
	}                       
		if (isEQ(wsspcomn.flag, "I")||isEQ(wsspcomn.flag, "D")) {
			scrnparams.function.set(varcom.prot);
		}
		
		return ;
		
	}

protected void screenEdit2000()
	{
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(scrnparams.statuz, varcom.calc)) {
		wsspcomn.edterror.set("Y");
		validate();
	}
				}
				

private boolean validate(){
	boolean isValid = true;
	if (isEQ(wsspcomn.flag, "C")||isEQ(wsspcomn.flag, "M")) {
		if(isEQ(sv.excda,SPACES)){
			sv.excdaErr.set("E186");
			sv.excltxt.set(SPACES);
			isValid = false;
			}
		else{
			readPrimaryRecord1020(sv.excda.toString());}
		
		if(isEQ(wsspcomn.flag, "C")||(isEQ(wsspcomn.flag, "M") && isNE(sv.excda,sv1.excda) )){
			Exclpf exclpfrec=null;
			exclpfrec=exclpfDAO.readRecordByCode(wsspcomn.chdrChdrnum.toString(), wsspcomn.crtable.toString(), sv.excda.toString(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());
			if(exclpfrec!=null){
			sv.excdaErr.set("F145");
			isValid = false;}
		}
	}

	
	return isValid;
	
}



protected void update3000()
	{
	if (isEQ(scrnparams.statuz, varcom.kill)) {
		return;
	}
	if (isEQ(wsspcomn.flag, "I")) {
		return;
	}
	boolean isValid = validate();
	
	if(isValid){
	if (isEQ(wsspcomn.flag, "C")) {
		exclpf=new Exclpf();
			exclpf.setChdrcoy(wsspcomn.company.toString());
			exclpf.setChdrnum(wsspcomn.chdrChdrnum.toString());
			exclpf.setCrtable(wsspcomn.crtable.trim());
			exclpf.setExcda(sv.excda.toString().trim());
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			datcon2rec.intDatex1.set(datcon1rec.intDate);
			datcon2rec.frequency.set("12");
			datcon2rec.freqFactor.set(-3);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			wsaaToday.set(datcon1rec.intDate);
			exclpf.setEffdate(wsaaToday.toInt());
			exclpf.setExadxt(sv.exadtxt.toString());
			exclpf.setPrntstat("Pending Print");
			exclpf.setValidflag("3");
			exclpf.setPlnsfx(0);
			exclpf.setTranno(wsaaTranno);
			wssaSeqnbr = exclpfDAO.getMaxSeqnbr(wsspcomn.company.toString(), wsspcomn.chdrChdrnum.toString(),covrpf.getLife(),covrpf.getCoverage(),covrpf.getRider());
			wssaSeqnbr++;
			exclpf.setSeqno(wssaSeqnbr);
			exclpf.setLife(covrpf.getLife());
			exclpf.setCoverage(covrpf.getCoverage());
			exclpf.setRider(covrpf.getRider());
			exclpfDAO.insertRecord(exclpf);		
			
		} else if (isEQ(wsspcomn.flag, "M")) {
			    exclpf.setExadxt(sv.exadtxt.toString());
			    if(exclpf.getPrntstat().trim().equals("Pending Print")){ //PINNACLE-2855
			    	exclpf.setTranno(wsaaTranno);
			    	exclpfDAO.updateRecord(exclpf,sv.excda.toString());
			    }
			    else {
					exclpf.setTranno(wsaaTranno);
					exclpf.setValidflag("2");
					exclpfDAO.updateRecordToInvalidate(exclpf, false);
					exclpf.setExcda(sv.excda.toString());
					datcon1rec.function.set(varcom.tday);
					Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
					wsaaToday.set(datcon1rec.intDate);
					exclpf.setEffdate(wsaaToday.toInt());
					exclpf.setValidflag("1");
					exclpfDAO.insertRecord(exclpf);
			    	
			    }
				sv1.excda.set(sv.excda.toString());
				Descpf desc=null;
				desc=descdao.getdescData("IT", "TR5AG", sv.excda.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
				if(desc!=null)
				sv1.longdesc.set(desc.getLongdesc());
				/*scrnparams.subfileRrn.set(wssplife.subfileRrn);*/
				scrnparams.subfileEnd.set(wssplife.subfileEnd);
				scrnparams.function.set(varcom.supd);
				processScreen("Sr5ah", sv1);
			} 
		else if (isEQ(wsspcomn.flag, "D")){
				if (exclpf.getPrntstat().trim().equals("Pending Print")) {
					exclpfDAO.deleteRecord(exclpf);
				} else {
					exclpf.setTranno(wsaaTranno);
					exclpf.setValidflag("2");
					exclpfDAO.updateRecordToInvalidate(exclpf,true);

				}
			/*scrnparams.subfileRrn.set(wssplife.subfileRrn);*/
			scrnparams.subfileEnd.set(wssplife.subfileEnd);
			scrnparams.function.set(varcom.supd);
			sv1.uniqueNum.set(new Long(0));
			processScreen("Sr5ah", sv1);
		}
	
	}
	else{
		wsspcomn.edterror.set("Y");
	}	
				
	}

protected void whereNext4000()
	{
	wsspcomn.programPtr.add(1);
	scrnparams.function.set("HIDEW");
	}

}
