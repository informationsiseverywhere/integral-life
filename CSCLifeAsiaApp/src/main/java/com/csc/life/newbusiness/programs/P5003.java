/*
 * File: P5003.java
 * Date: 29 August 2009 23:52:38
 * Author: Quipoz Limited
 * 
 * Class transformed from P5003.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;	//ILIFE-5390
import com.csc.fsu.general.dataaccess.model.Payrpf;		//ILIFE-5390
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;	//ILIFE-6891
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.enquiries.procedures.CrtundwrtUtil; //IBPLIFE-680
import com.csc.life.enquiries.procedures.CrtundwrtUtilImpl; //IBPLIFE-680
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.mailroom.dataaccess.dao.JbpmwrkflpfDAO;
import com.csc.life.mailroom.dataaccess.model.Jbpmwrkflpf;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.RlvrpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.UwrspfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Rlvrpf;
import com.csc.life.newbusiness.dataaccess.model.Uwrspf;
import com.csc.life.newbusiness.procedures.LincNBService;
import com.csc.life.newbusiness.screens.S5003ScreenVars;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5673rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.reassurance.dataaccess.CovtcsnTableDAM;
import com.csc.life.reassurance.tablestructures.T5448rec;
import com.csc.life.reassurance.tablestructures.Th618rec;
import com.csc.life.underwriting.dataaccess.dao.UndlpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undlpf;
import com.csc.life.workflow.config.IntegralLifeWorkflowProperties;
import com.csc.smart.procedures.Genssw;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Gensswrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;
/**
 * <pre>
 * Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
 *REMARKS.
 *
 *                     WORK WITH PROPOSAL
 *                     ==================
 *
 *
 * Initialise
 * ----------
 *
 *   Skip  this  section  if  returning from an optional selection
 *   (current stack position action flag = '*').
 *
 *   Clear the subfile ready for loading.
 *
 *   Read  CHDRLNB  (RETRV) in order to obtain the contract header
 *   information.
 *
 *  Read  the  contract definition description from T5688 for the
 *   contract type held on CHDRLNB.
 *
 *   Read  the  contract  definition  details  from  T5688 for the
 *   contract  type  held  on  CHDRLNB. Access the version of this
 *   item for the original commencement date of the risk.
 *
 *   Look up  the  client details of the contract owner (CLTS) and
 *   format the name as a "confirmation name".
 *
 *   This program  displays  the  current  component  parts  of an
 *   existing proposal.  One  record is written to the subfile for
 *   each component part found.
 *
 *   Add a  "contract  header" record with all fields blank except
 *   the   Element-key  (client  code)  and  Element-description
 *   (contract description from above).
 *
 *   For a  contract,  there  will  be  one  or  many  lives (with
 *   optionally a joint life). For each life, there will be one or
 *   many coverages. For each coverage, there will be none, one or
 *   many riders.
 *
 *   Start by reading on record from the life file (LIFELND with a
 *   BEGN) for  the  current  proposal.  Maintain  a  note  of the
 *   next  available  life  number used when reading life records.
 *   This  will be the next number sequentially or a "hole" in the
 *   life number sequence. It will be needed if there is a request
 *   to  add  another  life. Also count the actual number of lives
 *   (not  joint lives) added to the subfile.  This will be needed
 *   during validation.  Add records as follows:
 *
 *        - Life details  -  write  the life record to the subfile
 *             (life-no  from  record,  coverage and rider numbers
 *             blank) with  the  client-no  as the element key and
 *             the "confirmation name" (from CLTS) for the life as
 *             the    description.   Read   the   first   coverage
 *             transaction  (COVTLNB with BEGN) for the life added
 *             to  the  subfile.  Sequentially  read the next life
 *             record (it may be useful).
 *
 *        - Joint life  details  - if the LIFELNB record just read
 *             is for the same "life-no", the joint life no should
 *             not be  zero (if it is, there is a database error.)
 *             This is  a  joint  life associated with the current
 *             life. In  this  case, write another record with the
 *             action  field protected, the life-no " +", coverage
 *             and rider  numbers  blank  and  the element key and
 *             description  as  above. (This record is written for
 *             information  only.) Sequentially read the next life
 *             record (it may be useful).
 *
 *        - Coverage details  -  write  the coverage record to the
 *             subfile  with  the coverage-no from the record, the
 *             life  and  rider numbers blank. (Write the life-no,
 *             coverage-on and rider-no to "hidden" fields for use
 *             if  the  line  is selected.) Put the coverage/rider
 *             code in the element key and look up its description
 *             (T5687)  for the element description. Remember this
 *             coverage  code  for  adding  to  the  rider records
 *             attached  (in  a  hidden field).  Sequentially read
 *             the next coverage/rider transaction.
 *
 *        - Rider details  - if the coverage number is the same as
 *             the one  put  on the coverage subfile record above,
 *             the rider-no will  not be zero (if it is, this is a
 *             database error).  This  is  a  rider  on  the above
 *             coverage.  Write  the  rider  record to the subfile
 *             with the  rider-no  from  the  record, the life and
 *             coverage   numbers   blank.   (Write  the  life-no,
 *             coverage-on and rider-no to "hidden" fields for use
 *             if  the  line  is selected.) Put the coverage/rider
 *             code in the element key and look up its description
 *             (T5687)  for the element description.  Sequentially
 *             read  the  next coverage/rider transaction. If this
 *             is   another   rider   record   for   the   current
 *             transaction, repeat this rider processing. If it is
 *             another  coverage  for  the  same  life, repeat the
 *             processing  from  the coverage section above. If it
 *             is a  coverage  for  another  life,  repeat all the
 *             above processing  for  the  next  life.  If it is a
 *             coverage for  another  proposal  (or the end of the
 *             file) all components have been loaded.
 *
 *   In all cases, load  all pages required in the subfile and set
 *   the subfile more indicator to no.
 *
 * Validation
 * ----------
 *
 *   Skip  this  section  if  returning from an optional selection
 *   (current stack position action flag = '*').
 *
 *   Read   all   modified   subfile   records.  In  enquiry  mode
 *   (WSSP-FLAG  =  'I'), only an action of '1' is allowed against
 *   any line.  Otherwise, validate the selection according to the
 *   record  type  read. (This can be worked out from which fields
 *   are not blank - life-no = life record, coverage-no = coverage
 *   record, rider-no = rider record, none = contract header.):
 *
 *        Contract  header - valid values are '1' and '2'. If '2',
 *             check  that  the maximum number of lives allowed on
 *             this  type  of contract (from T5688 read above) has
 *             not  been  reached already (no of lives accumulated
 *             when the subfile was loaded).
 *             If '1' then clear the available issue field on the
 *             client header record and keep the header record.
 *
 *        Life - valid  values  are '1', '2' and '9'. If '9', skip
 *             all  the  validation  on  any component coverage or
 *             rider for this life (blanking out actions?).
 *             If '1' or '9' then clear the available issue field
 *             on the client header record and keep the header
 *             record.
 *
 *        Coverage  -  valid  values are '1', '2', '3' and '9'. If
 *             '9',   read  the  contract  structure  table  entry
 *             (T5673)   for   the  contract  type,  effective  at
 *             original commencement date.  Check that this is not
 *             a mandatory coverage.  (NB this table item could be
 *             continued  onto  more than one record). If OK, skip
 *             all  the  validation on any rider for this coverage
 *             (blanking out actions?). If '3',  read  the general
 *             coverage/rider  details  table   (T5687)   for  the
 *             coverage code and check that R/I is allowed.
 *
 *        Rider - valid values are '1', '3' and '9'.  If '9', read
 *             the  contract structure table entry (T5673) for the
 *             contract type, effective  at  original commencement
 *             date.  Find the rider details for the coverage code
 *             "hidden" on this record. Check that this  is  not a
 *             mandatory  rider.   (NB this table  item  could  be
 *             continued onto more than one record  and riders for
 *             a coverage can continue onto more  than  one line).
 *             If  '3', read the  general  coverage/rider  details
 *             table (T5687) for the rider code and check that R/I
 *             is allowed.
 *
 * Updating
 * --------
 *
 *   This program performs no updating.
 *
 * Next program
 * ------------
 *
 *   If not returning  from  a  component (stack action is blank),
 *   read the  first  record  from  the  subfile.  If  this is not
 *   selected (action  is  blank),  read  the  next one and so on,
 *   until a selected  record  is found, or the end of the subfile
 *   is reached.
 *
 *   Once the end  of  the  subfile  has been reached, if no items
 *   were  selected at all (stack action still blank) and CALC has
 *   not been requested, retrieve  the next program switching from
 *   secondary switching with an action  of  'E'.  Add  one to the
 *   program stack and exit.  If some items were selected and CALC
 *   has been deAlt with,move a blank to the stack action and exit
 *   (this will cause the  program  to  re-enter initialisation to
 *   re-load the subfile).
 *
 *   Processing  of  what  to  do next is dependant on the type of
 *   record read as follows:
 *
 *        Contract  header  - use generalised secondary switching.
 *             Set   up  the  function  according  to  the  action
 *             entered.   1 = A, 2 = B.  Move  '*'  to  the  stack
 *             action.
 *
 *        Life -  use  generalised  secondary  switching.  Set  up
 *             the function  according  to the action entered. 1 =
 *             F, 2 = G, 9 = H.  Move '*' to the stack action.
 *
 *        Coverage/rider -  for  action  '1', look up the programs
 *             required  to process the generic component (T5671 -
 *             accessed  by  transaction  number concatenated with
 *             coverage/rider  code from the subfile record). Move
 *             these four  programs into the program stack and set
 *             the current stack action to '*'.
 *
 *        Coverage/rider  -  for  actions  '2',  '3'  and  '9',use
 *             generalised  secondary  switching.   Set   up   the
 *             function according to the action entered.  2 = I, 3
 *             = J, 9 = K.  Move '*' to the stack action.
 *
 *   Release any LIFELNB or COVTLNB records stored (RLSE).
 *
 *   For action '2' on the contract header, set up the key details
 *   of  the  life  to  be  processed  (in LIFELNB using the KEEPS
 *   function) by the called programs as follows:
 *
 *             Company - WSSP company
 *             Contract no - from CHDRLNB
 *             Life number - next life number available(worked out
 *                  in 1000 section)
 *
 *   For all life  actions,  set up the key details of the life to
 *   be processed  (in  LIFELNB  using  the KEEPS function) by the
 *   called programs as follows:
 *
 *             Company - WSSP company
 *             Contract no - from CHDRLNB
 *             Life number - from the subfile record
 *
 *   For all coverage/rider actions, set up the key details of the
 *   coverage/rider to  be  processed  (in COVTLNB using the KEEPS
 *   function) by the called programs as follows:
 *
 *             Company - WSSP company
 *             Contract no - from CHDRLNB
 *             Life number - from "hidden" field
 *             Coverage number - from "hidden" field
 *             Rider number - from "hidden" field
 *
 *   For all action, add one to the program pointer and exit.
 *
 *   After finishing the subfile records, if CALC was pressed, use
 *   generalised secondaty switching, with a function of 'C'. Move
 *   '*' to the stack action.
 *
 *****************************************************************
 * </pre>
 */
public class P5003 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5003");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected FixedLengthStringData wsaaStoredLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoredCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoredRider = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStoredCrtable = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaMaxLives = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaNextAvailable = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaActualRead = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaT5673ContItem = new FixedLengthStringData(7).init(SPACES);

	private FixedLengthStringData wsaaUnderwritingReqd = new FixedLengthStringData(1).init("N");
	private Validator underwritingReqd = new Validator(wsaaUnderwritingReqd, "Y");

	private FixedLengthStringData wsaaUndwconfFound = new FixedLengthStringData(1).init("N");
	private Validator undwconfFound = new Validator(wsaaUndwconfFound, "Y");
	private ZonedDecimalData wsbbTableSub = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsbbTableRsub = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsbbCurrLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbCurrCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsbbCalcPressed = new FixedLengthStringData(1).init("N");
	private ZonedDecimalData wsccSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsccSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsccCurrentCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsccError = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaJlifeNum = new FixedLengthStringData(2).isAPartOf(wsaaJlife, 0, REDEFINE);
	private ZonedDecimalData wsaaJlifeN = new ZonedDecimalData(2, 0).isAPartOf(wsaaJlifeNum, 0).setUnsigned();
	/* TABLES */

	private static final String covtcsnrec = "COVTCSNREC";
	private static final String lifelnbrec = "LIFELNBREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
//	private CovtcsnTableDAM covtcsnIO = new CovtcsnTableDAM(); //IBPLIFE-8676
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Batckey wsddBatckey = new Batckey();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Gensswrec gensswrec = new Gensswrec();
	private T5688rec t5688rec = new T5688rec();
	private T5673rec t5673rec = new T5673rec();
	private T5671rec t5671rec = new T5671rec();
	private T5448rec t5448rec = new T5448rec();
	private Th618rec th618rec = new Th618rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5003ScreenVars sv = ScreenProgram.getScreenVars( S5003ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	boolean ispermission = false;
	private static final String t1692 = "T1692";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;
	private Clntpf clntpf;
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Descpf descpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	//ILIFE-5390 started
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Payrpf payrpf = null;
	//ILIFE-5390 ended
	private List<Lifepf> lifeList =new ArrayList<Lifepf>(); 
	protected Lifepf lifepf;
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private boolean wsaaEof;
	private CovtpfDAO covtlnbDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private List<Covtpf> covrlnbList =new ArrayList<Covtpf>(); 
	private List<Covtpf> covrList =new ArrayList<Covtpf>(); 
	private List<Covtpf> covtpfList =new ArrayList<Covtpf>(); //IBPLIFE-8676
	protected boolean eof;
	Iterator<Lifepf> iterator ; 
	Iterator<Covtpf> iteratorCovr ;
	protected Covtpf covtpf;
	protected Covtpf covtlnb;
	protected Covtpf covtpfTemp; //IBPLIFE-8676
	private UndlpfDAO undlpfDAO = getApplicationContext().getBean("undlpfDAO", UndlpfDAO.class);
	protected List<? extends Undlpf> undlpfList;
	Iterator<Covtpf> iteratorCovtlnb ;
	private boolean endOfFile;
	
	private Wssplife wssplife = new Wssplife();
	private Rlvrpf rlvrpf=null;
	private RlvrpfDAO rlvrpfDAO = getApplicationContext().getBean("rlvrpfDAO", RlvrpfDAO.class);
	private static final String ROLLOVER_FEATURE_ID="NBPRP104";
	private boolean rolloverFlag = false;
	/*	ILIFE-6891	*/
	private Datcon3rec datcon3rec = new Datcon3rec();
	private PackedDecimalData wsaaMattrm = new PackedDecimalData(11, 5);	
	private PackedDecimalData wsaaPremtrm = new PackedDecimalData(11, 5);
	/*	ILIFE-6891	*/
	//ILJ-43
	private boolean contDtCalcFlag = false;
	//end
	//ILJ-388 start
	private boolean cntEnqFlag = false;
	private String cntEnqFeature = "CTENQ010";
	//end 
	private boolean lincFlag = false;
	private static final String lincFeature = "NBPRP116";
	private boolean actionSelected = false;
	private static final String tjl47 = "TJL47";
	
	private UwrspfDAO uwrspfDAO = getApplicationContext().getBean("uwrspfDAO", UwrspfDAO.class);
	private boolean actionFlag = false;

	private JbpmwrkflpfDAO jbpmwrkflpfDAO = getApplicationContext().getBean("jbpmwrkflpfDAO",JbpmwrkflpfDAO.class);
	private CrtundwrtUtil crtundwrtUtil = new CrtundwrtUtilImpl(); //IBPLIFE-680

	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {


		exit2e90, 
		exit2g90, 
		exit2h90, 
		exit4799, 
		a400Exit
	}

	public P5003() {
		super();
		screenVars = sv;
		new ScreenModel("S5003", AppVars.getInstance(), sv);
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		//TMLII-268
		try {	/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			processBoMainline(sv, sv.dataArea, parmArray);
			if(isEQ(wsspcomn.sectionno, "1000")){
				/* Here it needs specific error handling, as P5003 screens display 'WARNING: U/wrtg Confirmd' message on screen, */
				/* it is required to bypass this here */
				if(isEQ(scrnparams.errorCode, "J009")){
					scrnparams.errorCode.set(SPACES);
				}
			} else if(isEQ(wsspcomn.sectionno, "2000")
					|| isEQ(wsspcomn.sectionno, "4000")){
				// added to bypass endp state of scrnparams.statuz
				if(isEQ(scrnparams.statuz, varcom.endp)){
					scrnparams.statuz.set(varcom.oK);
				}
			}
			//TMLII-268
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(clntpf.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(clntpf.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntpf.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(clntpf.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(clntpf.getSurname());
		}
		/*PLAIN-EXIT*/
	}

	protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(clntpf.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(clntpf.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	 * <pre>
	 *  END OF CONFNAME **********************************************
	 *      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000()
	{
		//ilj-43
		contDtCalcFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP113", appVars, "IT");	
		if(contDtCalcFlag) {
			sv.contDtCalcScreenflag.set("Y");
		}
		else {
			sv.contDtCalcScreenflag.set("N");
		}
		//end
		wsspcomn.typinj.set(SPACES);
		/* Skip this section if returning from an optional selection,*/
		/* i.e. check the stack action flag equals '*'.*/
		wsddBatckey.set(wsspcomn.batchkey);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsbbCalcPressed.set(SPACES);
		wsaaStoredLife.set(SPACES);
		wsaaStoredCoverage.set(SPACES);
		wsaaStoredRider.set(SPACES);
		wsaaStoredCrtable.set(SPACES);
		wsbbCurrLife.set(SPACES);
		wsbbCurrCoverage.set(SPACES);
		wsbbTableSub.set(ZERO);
		wsaaEof = false;
		eof = false;
		/* Clear the subfile ready for loading.*/
		scrnparams.function.set(varcom.sclr);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		wsaaActualRead.set(ZERO);
		/*    Retrieve contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		//ILJ-388
		cntEnqFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntEnqFeature, appVars, "IT");//ILJ-388
		if(cntEnqFlag) {
			sv.cntEnqScreenflag.set("Y");
		}
		else {
			sv.cntEnqScreenflag.set("N");
		}
		
		if(chdrlnbIO.getCntcurr().toString().equals("JPY")) {
			sv.currencyScreenflag.set("Y");
		}else {
			sv.currencyScreenflag.set("N");
		}
		//ILJ-388 end
		FeaConfg feaConfg=new FeaConfg();
		ispermission=feaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString(), "JCH001",appVars, "IT");
		if(!ispermission){
			sv.cntBranchOut[varcom.nd.toInt()].set("Y");
			sv.cntBranchOut[varcom.pr.toInt()].set("Y");
		}
		/* Read the contract definition description from table*/
		/* T5688 for the contract held on the client header record.*/
		descpf=descDAO.getdescData("IT", "T5688", chdrlnbIO.getCnttype().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descpf.getLongdesc());
		}
		sv.cnttype.set(chdrlnbIO.getCnttype());
		/* Read  the  contract  definition  details  from  T5688 for the*/
		/* contract  type  held  on  CHDRLNB. Access the version of this*/
		/* item for the original commencement date of the risk.*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItemtabl("T5688");
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());

		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if(isNE(it.getItemitem(), chdrlnbIO.getCnttype()))
				{
					scrnparams.errorCode.set(errorsInner.f290);
				}
				t5688rec.t5688Rec.set(StringUtil.rawToString(it.getGenarea()));
				wsaaMaxLives.set(t5688rec.lifemax);
			}
		} 

		getClientDetails1700();
		/*    The following code checks whether underwriting is applicable */
		/*    to this product type. If the Contract Type exists on T6771   */
		/*    (Underwriting Product Rules), then the program will check    */
		/*    if Underwiting has been confirmed or not.                    */
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItemtabl("TR675");
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());

		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if(isNE(it.getItemitem(), chdrlnbIO.getCnttype()))
				{
					wsaaUnderwritingReqd.set("N");
				}

				wsaaUnderwritingReqd.set("Y");
			}
		} 

		/*    Set screen fields*/
		/* Set up title information on the screen.*/
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		sv.cownnum.set(chdrlnbIO.getCownnum());
		/*    MOVE CLTS-CLTTYPE           TO S5003-CNTTYPE.*/
		/* Add a contract header record to the subfile, all the*/
		/* fields are left blank except element key (CNTTYPE)*/
		/* and element description (cnttype description).*/
		sv.elemkey.set(chdrlnbIO.getCnttype());
		if (descpf==null) {
			sv.elemdesc.fill("?");
		}
		else {
			sv.elemdesc.set(descpf.getLongdesc());
		}
		if(ispermission) {
			sv.cntBranch.set(chdrlnbIO.getCntbranch());
			sv.acctmonth.set(chdrlnbIO.getOccdate().toString().substring(4, 6));
			sv.acctyear.set(chdrlnbIO.getOccdate().toString().substring(0, 4));
			/*    Obtain the Branch name from T1692.*/
			descpf=descDAO.getdescData("IT", t1692, chdrlnbIO.getCntbranch().toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
			if (descpf==null) {
				sv.branchdesc.fill("?");
			}
			else {
				sv.branchdesc.set(descpf.getLongdesc());
			}
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

		beginLifeFile1020();
		lincFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), lincFeature, appVars, "IT");
		return;
	}

	/**
	 * <pre>
	 *    Load the complete subfile (in one go !)
	 * Begin reading the life file (LIFELNB) for the current
	 * proposal.
	 * </pre>
	 */
	protected void beginLifeFile1020()
	{

		lifeList = lifepfDAO.getLifeList(wsspcomn.company.toString(),chdrlnbIO.getChdrnum().toString());
		if(lifeList.isEmpty() || lifeList == null){
			wsaaEof = true;
			return;
		}

		iterator =   lifeList.iterator();
		wsaaNextAvailable.set(1);

		if (iterator.hasNext()) {

			lifepf = iterator.next();		
		}

		/* store life no*/
		wsaaStoredLife.set(lifepf.getLife());
		/*    maintain count of next available life no*/
		if (isEQ(wsaaNextAvailable, lifepf.getLife())) {
			wsaaNextAvailable.add(1);
		}
		/* BEGN reading the coverages for the life record read.*/

		covrlnbList = covtlnbDAO.searchCovtRecordByCoyNumDescUniquNo(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());		//ILIFE-5390

		if(covrlnbList.isEmpty() || covrlnbList == null){
			eof = true;
			loadSubfile1030();
			return;
		}
		else {
			/* store coverage no */
			iteratorCovr = covrlnbList.iterator();
			if (iteratorCovr.hasNext()) {

				covtpf = iteratorCovr.next();		
			}
			wsaaStoredCoverage.set(covtpf.getCoverage());
			wsaaStoredRider.set(covtpf.getRider());
		}

		loadSubfile1030();
		return;
	}

	protected void loadSubfile1030()
	{
		while ( !(isEQ(wsaaEof, true))) {
			loadSubfile1100();
		}
		checkUnderwriting1040();
		return;

	}

	protected void checkUnderwriting1040()
	{
		/* Loop thro the Underwriting Lives file UNDL, to check if any     */
		/* lives have had their Underwriting confirmed. Display            */
		/* appropriate warning message.                                    */


		wsaaUndwconfFound.set("N");
		undlpfList = undlpfDAO.checkUndlpf(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());

		und1040CustomerSpecific();

		/* If Underwriting Confirmation was found to be set on at least    */
		/* one of the live/joint lives on the contract, let the user know. */
		if (underwritingReqd.isTrue()) {
			if (undwconfFound.isTrue()) {
				scrnparams.errorCode.set(errorsInner.j009);
			}
			else {
				scrnparams.errorCode.set(errorsInner.j010);
			}
		}
	}
	
	protected void und1040CustomerSpecific(){
		if(undlpfList != null) {
			for(Undlpf undlpf : undlpfList){
				checkUndw1800(undlpf.getUndwflag());
			}

		}
	}

	/**
	 * <pre>
	 *     LOAD THE COMPLETE SUBFILE
	 * </pre>
	 */
	protected void loadSubfile1100()
	{
		/* Move the last record read to the screen and get the*/
		/* description.*/
		sv.life.set(lifepf.getLife());
		sv.elemkey.set(lifepf.getLifcnum());
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifepf.getLifcnum());
		/* Get the confirmation name.*/
		plainname();
		sv.elemdesc.set(wsspcomn.longconfname);
		/*  set hidden fields*/
		sv.hcoverage.set(SPACES);
		sv.hrider.set(SPACES);
		sv.hcrtable.set(SPACES);
		sv.coverage.set(SPACES);
		sv.rider.set(SPACES);
		sv.sumin.set(ZERO);	//ILIFE-6972
		sv.riskCessTerm.set(SPACES);
		sv.premCessTerm.set(SPACES);
		sv.instPrem.set(ZERO);
		sv.hlife.set(lifepf.getLife());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*    maintain count of actual records written*/
		wsaaActualRead.add(1);
		if (isEQ(wsaaNextAvailable, lifepf.getLife())) {
			wsaaNextAvailable.add(1);
		}
		/*    look for joint life.*/

		if(iterator.hasNext())
			lifepf = iterator.next();		
		else
			wsaaEof = true;	

		if (isEQ(wsaaEof, true)) {
			loadComponents1150();
			return;

		}
		/* If this is another main life, skip joint life load*/
		if (isEQ(lifepf.getJlife(), ZERO)) {
			loadComponents1150();
			return;
		}
		/* Move the last record read to the screen and get the*/
		/* description.*/
		sv.life.set(" +");
		sv.hlife.set(lifepf.getLife());
		sv.actionOut[varcom.pr.toInt()].set("Y");
		sv.elemkey.set(lifepf.getLifcnum());
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), lifepf.getLifcnum());
		plainname();
		sv.elemdesc.set(wsspcomn.longconfname);
		/*  move the joint life to the screen*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

		readNextLife1220();
		return;
	}

	/**
	 * <pre>
	 * Read the next life record.
	 * </pre>
	 */
	protected void readNextLife1220()
	{

		if(iterator.hasNext())
			lifepf = iterator.next();

		else
			wsaaEof = true ;

		loadComponents1150();
		return;

	}	
	/**
	 * <pre>
	 * Read the coverage and rider details.
	 * </pre>
	 */
	protected void loadComponents1150()
	{

		while ( !(isNE(wsaaStoredLife, (covtpf!=null?covtpf.getLife():""))//ILIFE-8269
				|| isEQ(eof, true))) {
			loadComponents1200();
		}

		wsaaStoredLife.set(lifepf.getLife());
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     READER THE RELATED COVER DETAILS
	 * </pre>
	 */
	protected void loadComponents1200()
	{
		coverAndRiders1210();
	}

	protected void coverAndRiders1210()
	{
		/* Unprotect the action field if a joint life has been added*/
		/* to the subfile.*/
		sv.actionOut[varcom.pr.toInt()].set(SPACES);
		/* Clear the life and rider fields in the subfile record and*/
		/* set up the record including hidden fields.*/
		if (isEQ(covtpf.getRider(), "  ")
				|| isEQ(covtpf.getRider(), "00")) {
			wsspcomn.cessdte.set(covtpf.getRcesdte());
			sv.coverage.set(covtpf.getCoverage());
			sv.hcoverage.set(covtpf.getCoverage());
			sv.hrider.set("00");
			sv.rider.set(SPACES);
			sv.life.set(SPACES);
			wsaaStoredCrtable.set(covtpf.getCrtable());
			sv.hcrtable.set(covtpf.getCrtable());
		}
		else {
			sv.life.set(SPACES);	
			sv.rider.set(covtpf.getRider());
			sv.hrider.set(covtpf.getRider());
			sv.coverage.set(SPACES);
			sv.hcoverage.set(covtpf.getCoverage());
			sv.hcrtable.set(wsaaStoredCrtable);
		}
		sv.elemkey.set(covtpf.getCrtable());
		getDescription1600();
		sv.hlife.set(covtpf.getLife());
		
		/* Read the PAYR record to get the Billing Details.                */
		payrpf = payrpfDAO.getpayrRecord(wsspcomn.company.toString(),chdrlnbIO.getChdrnum().toString(),1);		//ILIFE-5390
		
		if(ispermission){

			//ILIFE-2001 Start by mranpise
			sv.sumin.set(covtpf.getSumins());
			/*	ILIFE-6891	*/
			if(covtpf.getPcestrm() == 0 && covtpf.getRcestrm() == 0){
				datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
				datcon3rec.intDate2.set(covtpf.getRcesdte());
				callDatcon();
				wsaaMattrm.set(datcon3rec.freqFactor);
				sv.riskCessTerm.set(wsaaMattrm.toString().trim().replace(".", " "));
				datcon3rec.intDate2.set(covtpf.getPcesdte());
				callDatcon();
				wsaaPremtrm.set(datcon3rec.freqFactor);
				sv.premCessTerm.set(wsaaPremtrm.toString().trim().replace(".", " "));
			}
			else{
				sv.riskCessTerm.set(covtpf.getRcestrm());
				sv.premCessTerm.set(covtpf.getPcestrm());
			}
			/*	ILIFE-6891	*/
			//ILIFE-5390 changes started
			
			// ILJ-388 Start
			if (!cntEnqFlag) {
				if (isEQ(payrpf.getBillfreq(), "00")) {
					sv.instPrem.set(covtpf.getSingp()); 
				}
				else{
					sv.instPrem.set(covtpf.getInstprem()); 
				}
			}
			else {
				if (isEQ(payrpf.getBillfreq(), "00")) {
					sv.instPrem.set(covtpf.getSingp().setScale(0, BigDecimal.ROUND_HALF_UP)); 
					}
				else{
					sv.instPrem.set(covtpf.getInstprem().setScale(0, BigDecimal.ROUND_HALF_UP));
				}

			}
			//ILJ-388 end
			//ILIFE-5390 changes ended
			//ILIFE-2001 ends
		}
		/* Add the coverage or rider record to the subfile.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !((isNE(wsaaStoredLife, covtpf.getLife()))
				|| (isNE(wsaaStoredRider, covtpf.getRider()))
				|| (isNE(wsaaStoredCoverage, covtpf.getCoverage()))
				|| isEQ(eof, true))) {
			findNextComponent1300();
		}

		wsaaStoredCoverage.set(covtpf.getCoverage());
		wsaaStoredRider.set(covtpf.getRider());
	}

	protected void findNextComponent1300()
	{
		/*NEXT*/
		/* Read the next coverage/rider record.*/

		if(iteratorCovr.hasNext()){
			covtpf = iteratorCovr.next();
		}
		else{
			eof = true ;
		}
	}
	/**
	 * <pre>
	 *     GET THE DESCRIPTION FOR THE COVERAGE/RIDER
	 * </pre>
	 */
	protected void getDescription1600()
	{
		getDescription1610();
	}

	protected void getDescription1610()
	{
		/* Read the contract definition description from table*/
		/* T5687 for the contract held on the client header record.*/
		descpf=descDAO.getdescData("IT", "T5687", covtpf.getCrtable(), wsspcomn.company.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			sv.elemdesc.set("??????????????????????????????");
		}
		else{
			sv.elemdesc.set(descpf.getLongdesc());
		}

	}

	protected void getClientDetails1700()
	{
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		clntpf=clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
		/* Get the confirmation name.*/
		if ( clntpf==null || isNE(clntpf.getValidflag(), 1)) {
			sv.ownernameErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

	protected void checkUndw1800(String flag)
	{
		/*START*/
		if (isEQ(flag, "Y")) {
			wsaaUndwconfFound.set("Y");
		}
		/*READ-NEXT*/
		/*    Read the next UNDL record.  

		/*EXIT*/
	} 

	/**
	 * <pre>
	 *     RETRIEVE SCREEN FIELDS AND EDIT
	 * </pre>
	 */
	protected void preScreenEdit()
	{
		/*PRE-START*/
		/* Skip this section if returning from an optional selection,      */
		/* i.e. check the stack action flag equals '*'.                    */
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		scrnparams.subfileRrn.set(1);
		return ;
		/*PRE-EXIT*/
	}

	protected void screenEdit2000()
	{
		screenIo2010();
		validateSubfile2020();
	}

	protected void screenIo2010()
	{
		/*    CALL 'S5003IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S5003-DATA-AREA                         */
		/*                         S5003-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/* If the f9 key pressed then set the calc flag for later*/
		/* use.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsbbCalcPressed.set("Y");
		}
		else {
			wsbbCalcPressed.set(SPACES);
		}
	}

	/**
	 * <pre>
	 *  Read the whole subfile for validation
	 *    (Deletions may make previously valid selections irrelevant)
	 * </pre>
	 */
	protected void validateSubfile2020()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
				&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsbbCurrLife.set(SPACES);
		wsbbCurrCoverage.set(SPACES);
		if(lincFlag)
			actionSelected = false;
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}

		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

	/**
	 * <pre>
	 *    Sections performed from the 2000 section above.
	 *          (Screen validation)
	 * </pre>
	 */
	protected void validateSubfile2600()
	{
		/*    Validate subfile fields*/
		/* The action field for a joint live line must be protected.       */
		sv.actionOut[varcom.pr.toInt()].set(" ");
		if (isEQ(sv.life, " +")) {
			sv.actionOut[varcom.pr.toInt()].set("Y");
		}
		/* If deleting a life then all the associate coverages and*/
		/* riders are cleared as no action will affect a deleted*/
		/* coverage/rider.*/
		if (isNE(wsbbCurrLife, SPACES)) {
			if (isEQ(wsbbCurrLife, sv.hlife)) {
				sv.action.set(SPACES);
			}
			else {
				wsbbCurrLife.set(SPACES);
			}
		}
		/* If deleting a coverage then all associated riders*/
		/* riders are cleared as no action will affect a deleted*/
		/* rider.*/
		if (isNE(wsbbCurrCoverage, SPACES)) {
			if (isEQ(wsbbCurrCoverage, sv.hcoverage)) {
				sv.action.set(SPACES);
			}
			else {
				wsbbCurrCoverage.set(SPACES);
			}
		}
		if (isNE(sv.coverage, SPACES)
				|| isNE(sv.rider, SPACES)) {
			validateT54482h00();
		}
		if (isEQ(sv.action, SPACES)) {
			updateErrorIndicators2670();
			return;
		}
		else if(lincFlag && isNE(sv.action, SPACES))
			actionSelected = true;
		/*    Validate action*/
		if (isNE(sv.action, "1")
				&& isNE(sv.action, "2")
				&& isNE(sv.action, "9")
				&& isNE(sv.action, " ")) {
			/*    '3' AND ' '                                              */
			sv.actionErr.set(errorsInner.e005);
			updateErrorIndicators2670();
			return;
		}
		/* If in inquiry mode then allow only an action of 1.*/
		if (isEQ(wsspcomn.flag, "I")
				&& (isNE(sv.action, "1")
						&& isNE(sv.action, SPACES))) {
			/*     MOVE U013               TO S5003-ACTION-ERR               */
			sv.actionErr.set(errorsInner.g503);
			updateErrorIndicators2670();
			return;
		}
		/* Determine which validation is required.*/
		if (isNE(sv.life, SPACES)) {
			validateLife2750();
		}
		else {
			if (isNE(sv.coverage, SPACES)) {
				validateCoverage2800();
			}
			else {
				if (isNE(sv.rider, SPACES)) {
					validateRider2900();
				}
				else {
					validateHeader2700();
				}
			}
		}

		updateErrorIndicators2670();
		return;

	}

	protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

		readNextRecord2680();
		return;
	}

	protected void readNextRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
				&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	 * <pre>
	 *    Sections performed from the 2600 section above.
	 *     VALIDATE THE CONTRACT HEADER ACTION
	 * </pre>
	 */
	protected void validateHeader2700()
	{
		/*VALIDATE-HEADER*/
		if (isNE(sv.action, "1")
				&& isNE(sv.action, "2")) {
			sv.actionErr.set(errorsInner.e005);
		}
		/* If we are adding life records to the header the maximum number*/
		/* of lives must not have been reached (uses the life count*/
		/* created in the initialisation section).*/
		if (isEQ(sv.action, "2")
				&& isGTE(wsaaActualRead, wsaaMaxLives)) {
			sv.actionErr.set(errorsInner.e370);
		}
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     VALIDATE THE LIFE ACTION
	 * </pre>
	 */
	protected void validateLife2750()
	{
		/*VALIDATE-LIFE*/
		if (isNE(sv.action, "1")
				&& isNE(sv.action, "2")
				&& isNE(sv.action, "9")
				&& isNE(sv.action, " ")) {
			sv.actionErr.set(errorsInner.e005);
		}
		/* If deleting a life then all the associate coverages and*/
		/* riders are cleared as no action will affect a deleted*/
		/* coverage/rider.*/
		if (isEQ(sv.action, "9")) {
			wsbbCurrLife.set(sv.life);
		}
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     VALIDATE THE COVERAGE ACTION
	 * </pre>
	 */
	protected void validateCoverage2800()
	{
		validateCoverage2810();
	}

	protected void validateCoverage2810()
	{
		if (isNE(sv.action, "1")
				&& isNE(sv.action, "2")
				&& isNE(sv.action, "9")
				&& isNE(sv.action, " ")) {
			sv.actionErr.set(errorsInner.e005);
			return ;
		}
		/* If a coveage or rider is to be deleted then check to see*/
		/* if it is mandatory - if mandatory then it cannot be removed.*/
		/* If not mandatory then clear the actions on all associate*/
		/* riders.*/
		if (isEQ(sv.action, "9")) {
			getTable2a00();
			if (isEQ(t5673rec.creq[wsbbTableSub.toInt()], "R")) {
				if (isGT(t5673rec.ctmaxcov[wsbbTableSub.toInt()], 1)) {
					checkLimit2g00();
				}
				else {
					/*           MOVE U014          TO S5003-ACTION-ERR              */
					sv.actionErr.set(errorsInner.g815);
					return ;
				}
			}
			else {
				wsbbCurrCoverage.set(sv.coverage);
			}
		}
	}

	/**
	 * <pre>
	 *     VALIDATE THE RIDER ACTION
	 * </pre>
	 */
	protected void validateRider2900()
	{
		/*VALIDATE-RIDER*/
		/*IF S5003-ACTION        NOT = '1' AND '3' AND '9' AND ' '     */
		if (isNE(sv.action, "1")
				&& isNE(sv.action, "9")
				&& isNE(sv.action, " ")) {
			sv.actionErr.set(errorsInner.e005);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2990-EXIT.                                          */
			return ;
		}
		/* If deleting a rider then the rider cannot be mandatory.*/
		if (isEQ(sv.action, "9")) {
			getTable2a00();
			if (isEQ(t5673rec.rreq[wsbbTableRsub.toInt()], "R")) {
				/*        MOVE U014             TO S5003-ACTION-ERR.             */
				sv.actionErr.set(errorsInner.g815);
			}
		}
		/*EXIT1*/
	}

	/**
	 * <pre>
	 *     GET THE TABLE TO REFERENCE WHETHER THE COVERAGE IS
	 *     MANDATORY OR NOT.
	 * </pre>
	 */
	protected void getTable2a00()
	{
		getTable2a10();
		checkError2a80();
	}

	protected void getTable2a10()
	{
		/* Read  the  coverage  definition  details  from  T5673 for the*/
		/* contract  type  held  on  CHDRLNB. Access the version of this*/
		/* item for the original commencement date of the risk.*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(chdrlnbIO.getCnttype().toString());
		itempf.setItemtabl("T5673");
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());

		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if(isNE(it.getItemitem(), chdrlnbIO.getCnttype()))
				{
					sv.actionErr.set(errorsInner.f290);
					return ;
				}
				t5673rec.t5673Rec.set(StringUtil.rawToString(it.getGenarea()));

			}
		} 

		/* Find the coverage in the table by checking the current table*/
		/* and if not found checking subsequent tables if they exist.*/
		wsbbTableSub.set(1);
		while ( !(isEQ(t5673rec.ctable[wsbbTableSub.toInt()], sv.hcrtable)
				|| isNE(sv.actionErr, SPACES))) {
			findCoverage2b00();
		}

		/* Compute the subscript for the first rider and find the rider.*/
		/* Note the position of the coverage (found already above)*/
		/* determines the start position of the associated riders.*/
		if (isNE(sv.rider, SPACES)
				&& isEQ(sv.actionErr, SPACES)) {
			compute(wsbbTableRsub, 0).set(add(mult((sub(wsbbTableSub, 1)), 6), 1));
			while ( !(isEQ(t5673rec.rtable[wsbbTableRsub.toInt()], sv.elemkey)
					|| isNE(sv.actionErr, SPACES))) {
				findRider2e00();
			}

		}
	}

	protected void checkError2a80()
	{
		/* In the case of a table error, set validation to OK so as*/
		/*  not to confuse the performing section.*/
		if (isNE(sv.actionErr, SPACES)) {
			wsbbTableSub.set(1);
			wsbbTableRsub.set(1);
			t5673rec.ctmaxcov[1].set(1);
			t5673rec.rreq[1].set(SPACES);
			t5673rec.creq[1].set(SPACES);
		}
		/*A90-EXIT*/
	}

	/**
	 * <pre>
	 *     SEARCH THE TABLE FOR THE REQUIRED COVERAGE.
	 * </pre>
	 */
	protected void findCoverage2b00()
	{
		/*B10-PROCESS-COVERAGE*/
		/* Actually find the coverage.*/
		for (wsbbTableSub.set(1); !(isGT(wsbbTableSub, 8)
				|| isEQ(t5673rec.ctable[wsbbTableSub.toInt()], sv.hcrtable)); wsbbTableSub.add(1)){
			nothing2c00();
		}
		/* If the coverage has not been found look in the continuation*/
		/* table.*/
		/* IF T5673-CTABLE(WSBB-TABLE-SUB)  NOT = S5003-HCRTABLE        */
		if (isGT(wsbbTableSub, 8)
				|| isNE(t5673rec.ctable[wsbbTableSub.toInt()], sv.hcrtable)) {
			wsbbTableSub.set(1);
			continuationTable2d00();
		}
		/*B90-EXIT*/
	}

	protected void nothing2c00()
	{
		/*C10-ABSOLUTLY-NOTHING*/
		/** Dummy section used to reference the T5673 table.*/
		/*C90-EXIT*/
	}

	protected void continuationTable2d00()
	{
		/* Read  the  coverage  definition  details  from  CONTINUATION*/
		/* TABLE.*/
		/* If last entry on the table is found and the rider looked*/
		/* for has not been found then a system error has occurred.*/
		if (isEQ(t5673rec.gitem, SPACES)) {
			sv.actionErr.set(errorsInner.f290);
			return ;
		}
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(t5673rec.gitem.toString());
		wsaaT5673ContItem.set(t5673rec.gitem);
		itempf.setItemtabl("T5673");
		itempf.setItmfrm(chdrlnbIO.getOccdate().getbigdata());
		itempf.setItmto(chdrlnbIO.getOccdate().getbigdata());

		itempfList = itempfDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {	
				if(isNE(it.getItemitem(), wsaaT5673ContItem))
				{
					scrnparams.errorCode.set(errorsInner.f290);

				}
				t5673rec.t5673Rec.set(StringUtil.rawToString(it.getGenarea()));

			}
		} 

	}

	protected void findRider2e00()
	{
		try {
			processRider2e10();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void processRider2e10()
	{
		/* Search through each line of riders in turn*/
		while ( !(isEQ(wsbbTableRsub, 48)
				|| isEQ(sv.elemkey, t5673rec.rtable[wsbbTableRsub.toInt()])
				|| isNE(sv.actionErr, SPACES))) {
			searchLine2e70();
		}

		/*  If not found on this table entry, try the next one.*/
		if (isNE(sv.elemkey, t5673rec.rtable[wsbbTableRsub.toInt()])) {
			wsbbTableSub.set(1);
			wsbbTableRsub.set(1);
			continuationTable2d00();
		}
		goTo(GotoLabel.exit2e90);

	}

	/**
	 * <pre>
	 *  Check a single line of riders (performed paragraphs).
	 * </pre>
	 */
	protected void searchLine2e70()
	{
		for (int loopVar1 = 0; !(loopVar1 == 6); loopVar1 += 1){
			checkElement2e80();
		}
		if (isNE(sv.elemkey, t5673rec.rtable[wsbbTableRsub.toInt()])) {
			if (isNE(wsbbTableSub, 8)) {
				wsbbTableSub.add(1);
				if (isNE(t5673rec.ctable[wsbbTableSub.toInt()], SPACES)) {
					sv.actionErr.set(errorsInner.f290);
				}
			}
		}
	}

	/**
	 * <pre>
	 ****    ADD 1                    TO WSBB-TABLE-SUB                
	 ****    IF T5673-CTABLE(WSBB-TABLE-SUB)  NOT = SPACE              
	 ****     MOVE F290               TO S5003-ACTION-ERR.             
	 * </pre>
	 */
	protected void checkElement2e80()
	{
		if (isNE(sv.elemkey, t5673rec.rtable[wsbbTableRsub.toInt()])
				&& isNE(wsbbTableRsub, 48)) {
			wsbbTableRsub.add(1);
		}
	}


	protected void checkLimit2g00()
	{
		try {
			setUpKey2g10();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void setUpKey2g10()
	{

		/*  Check first coverage*/
		/*  Set S5003-ACTION-ERR to '?' to signify start of search. When   */
		/*  the desired coverage is found it will be set up to 'G815',     */
		/*  which means we have found one, but it can not de deleted as    */
		/*  it is mandatory. Any subsequent like coverages will set the    */
		/*  S5003-ACTION-ERR field to spaces allowing the deletion of      */
		/*  the coverage. */



		covrList = covtlnbDAO.getCovtlnbRecord(wsspcomn.company.toString(),chdrlnbIO.getChdrnum().toString(),sv.hlife.toString());     

		iteratorCovtlnb = covrList.iterator();

		if(iteratorCovtlnb.hasNext()){
			covtlnb = iteratorCovtlnb.next();
		}

		sv.actionErr.set("?");
		wsccCurrentCoverage.set(covtlnb.getCoverage());

		if (isEQ(covtlnb.getCrtable(), sv.elemkey)) {
			/*     MOVE U014                TO S5003-ACTION-ERR.             */
			sv.actionErr.set(errorsInner.g815);
		}
		while ( !(isEQ(sv.actionErr, SPACES)
				|| isEQ(endOfFile, true))) {
			checkCoverages2g80();
		}

		if (isEQ(sv.actionErr, SPACES)) {
			wsbbCurrCoverage.set(covtlnb.getCoverage());
		}
		goTo(GotoLabel.exit2g90);
	}

	protected void checkCoverages2g80()
	{
		/*  Check for end of current life*/

		if(iteratorCovtlnb.hasNext()){
			covtlnb = iteratorCovtlnb.next();
		}
		else
			endOfFile = true; 

		/*  Check new coverage*/
		if (isNE(covtlnb.getCoverage(), wsccCurrentCoverage)
				&& isEQ(covtlnb.getRider(), "00")
				&& isNE(endOfFile, true)) {
			wsccCurrentCoverage.set(covtlnb.getCoverage());
			if (isEQ(covtlnb.getCrtable(), sv.elemkey)) {
				if (isEQ(sv.actionErr, "?")) {
					/*           MOVE U014          TO S5003-ACTION-ERR              */
					sv.actionErr.set(errorsInner.g815);
				}
				else {
					sv.actionErr.set(SPACES);
				}
			}
		}
	}

	protected void validateT54482h00()
	{
		try {
			checkT54482h10();
			validateTh6182h20();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void checkT54482h10()
	{
		//ILIFE-6587
		String itemitem = sv.cnttype.toString().concat(sv.elemkey.toString().trim().substring(0,4));
		itempfList=itempfDAO.getAllItemitemByDateFrm("IT", chdrlnbIO.getChdrcoy().toString(), "T5448", itemitem,chdrlnbIO.getOccdate().toInt());
		if (itempfList.size() > 0) {
			t5448rec.t5448Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}else{
			sv.actionErr.set(errorsInner.r065);
			goTo(GotoLabel.exit2h90);
		}
	}

	protected void validateTh6182h20()
	{
		itempfList=itempfDAO.getAllItemitem("IT", chdrlnbIO.getChdrcoy().toString(), "TH618", t5448rec.rrsktyp.toString());
		if (itempfList.isEmpty()) {
			sv.actionErr.set(errorsInner.rl30);
			return ;
		}
		th618rec.th618Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		if (isEQ(th618rec.lrkclss, SPACES)) {
			sv.actionErr.set(errorsInner.rl30);
			return ;
		}
	}

	/**
	 * <pre>
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/**  Update database files as required*/
		if(lincFlag && !actionSelected) {
			performLincProcessing();
		}
		/*EXIT*/
	}
	/**
	 * <pre>
	 *     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000()
	{
		nextProgram4010();

	}

	protected void nextProgram4010()
	{
		/* Bypass the intial read of the subfile.*/
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)) {
			bypass4020();
			return;
		}
		/* Re-start the subfile.*/
		wsccError.set(SPACES);
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
				&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		bypass4020();
		Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
		boolean uwFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP123", appVars, smtpfxcpy.item.toString());
		if(uwFlag) {
			saveUwrsRecord();
		}
		return;
	}
	
	private void saveUwrsRecord() {
		
		// not now
		if(actionFlag) {
			actionFlag = false;
			return;
		}
		Map<String, Uwrspf> uwRecCache = (Map<String, Uwrspf>)ThreadLocalStore.get(P5006.UWREC_CACHE);
		if(uwRecCache == null) {
			return;
		}
		List<Uwrspf> uwrspfList = new ArrayList<>();
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
				&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while (isNE(scrnparams.statuz, varcom.endp)) {

			if (isEQ(sv.action, SPACES)) {
				if(uwRecCache.containsKey(sv.elemkey.toString().trim())) {
					uwrspfList.add(uwRecCache.get(sv.elemkey.toString().trim()));
				}
			}else {
				return;
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("S5003", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
					&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
//		List<Uwrspf> uwrspfSavedList = uwrspfDAO.searchUwrspfRecord(
//				chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
//		List<Uwrspf> insertUwrspfList = new ArrayList<>();
//		List<Uwrspf> updateUwrspfList = new ArrayList<>();
//		for(Uwrspf u:uwrspfList) {
//			boolean existFlag = false;
//			for(Uwrspf ui:uwrspfSavedList) {
//				if(u.getCrtable().equals(ui.getCrtable())) {
//					updateUwrspfList.add(u);
//					existFlag = true;
//					break;
//				}
//			}
//			if(!existFlag) {
//				insertUwrspfList.add(u);
//			}
//		}
		uwrspfDAO.insertUwrsData(uwrspfList);
//		uwrspfDAO.updateUwrsData(updateUwrspfList);
		ThreadLocalStore.put(P5006.UWREC_CACHE, null);
	}

	protected void bypass4020()
	{
		/*  Read through the subfile until the next selected line is found*/
		if (isEQ(sv.action, SPACES)) {
			while ( !(isNE(sv.action, SPACES)
					|| isEQ(scrnparams.statuz, varcom.endp))) {
				readSubfile4800();
			}

		}
		/*  Nothing pressed at all, end working with this proposal.*/
		if (isEQ(scrnparams.statuz, varcom.endp)
				&& isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)
				&& isNE(wsbbCalcPressed, "Y")) {
			if(isNE(wsspcomn.sbmaction, "E")) //Do not add or delete UNDRPF
				a100AddUnderwritting();
			gensswrec.function.set("E");
			callGenssw4100();
			//IJTI-400 starts
			if (IntegralLifeWorkflowProperties.isWorkflowEnabled()) {
				wssplife.chdrnum.set(sv.chdrnum);
				wssplife.isUWRequired.set(wsaaUnderwritingReqd);
				wssplife.isUWConfirmed.set(wsaaUndwconfFound);
			}
			//IJTI-400 ends
			//IJTI-1241 Start
			FeaConfg feaConfg=new FeaConfg();
			boolean isWorkflowFeatureEnabled = feaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP110", appVars,"IT");
			if (isEQ(wsspcomn.sbmaction, "A") && IntegralLifeWorkflowProperties.getWorkflowServiceRuntimeURL() != null
					&& (IntegralLifeWorkflowProperties.isWorkflowEnabled() && isNE(sv.chdrnum.toString().trim(), SPACES) && isWorkflowFeatureEnabled)) {
						Jbpmwrkflpf jbpmwrkflpf = jbpmwrkflpfDAO.getLastRecord();
						//condition for Document type 'PS' 
						if(null != jbpmwrkflpf && null != jbpmwrkflpf.getDocttype() && jbpmwrkflpf.getDocttype().trim().equals("PS")){
								jbpmwrkflpfDAO.updateChdrnum(sv.chdrnum.toString().trim(), jbpmwrkflpf.getUnique_number());
						
						}
				}
			//IJTI-1241 End
			
			
			return ;
		}
		/* All requests services,*/
		/*   - no calc*/
		if (isEQ(scrnparams.statuz, varcom.endp)
				&& isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], SPACES)
				&& isNE(wsbbCalcPressed, "Y")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
			scrnparams.errorCode.set(wsccError);
			return ;
		}
		/*   - calc pressed*/
		if (isEQ(scrnparams.statuz, varcom.endp)
				&& isEQ(wsbbCalcPressed, "Y")) {
			wsbbCalcPressed.set("N");
			gensswrec.function.set("C");
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
			callGenssw4100();
			return ;
		}
		/* Header type.*/
		if (isEQ(sv.life, SPACES)
				&& isEQ(sv.coverage, SPACES)
				&& isEQ(sv.rider, SPACES)) {
			headerType4500();
		}
		/* Life type.*/
		if (isNE(sv.life, SPACES)) {
			lifeType4600();
		}
		/* Converage or Rider type.*/
		if (isNE(sv.coverage, SPACES)
				|| isNE(sv.rider, SPACES)) {
			coverRiderType4700();
		}
		/* Blank out action to ensure it is not processed again*/
		sv.action.set(SPACES);
		actionFlag = true;
	}

	/**
	 * <pre>
	 *     SECONDARY SWITCHING
	 * </pre>
	 */
	protected void callGenssw4100()
	{
		callGenssw4110();
	}

	protected void callGenssw4110()
	{
		/* Call gen switch and load the programs into wssp for subsequent*/
		/* execution.*/
		gensswrec.company.set(wsspcomn.company);
		gensswrec.progIn.set(wsaaProg);
		gensswrec.transact.set(wsddBatckey.batcBatctrcde);
		callProgram(Genssw.class, gensswrec.gensswRec);
		if (isNE(gensswrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(gensswrec.statuz);
			fatalError600();
		}
		/*    Load from gensw to wssp*/
		compute(wsccSub1, 0).set(add(1, wsspcomn.programPtr));
		wsccSub2.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 8); loopVar2 += 1){
			gensToWsspProgs4300();
		}
		if (isEQ(gensswrec.function, "E")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(" ");
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		}
		wsspcomn.programPtr.add(1);
	}

	/**
	 * <pre>
	 *     MOVE GENSSW PROGRAM STACK TO WSSP
	 * </pre>
	 */
	protected void gensToWsspProgs4300()
	{
		/*GENS-TO-WSSP-PROGS*/
		/* Move the gen switch programs to wssp.*/
		wsspcomn.secProg[wsccSub1.toInt()].set(gensswrec.progOut[wsccSub2.toInt()]);
		wsccSub1.add(1);
		wsccSub2.add(1);
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     WHEN THE SUBFILE RECORD IS A HEADER TYPE
	 * </pre>
	 */
	protected void headerType4500()
	{
		headerType4510();
	}

	protected void headerType4510()
	{
		/* Release any keeps on the Cover record.*/
		covtlnbIO.setFunction(varcom.rlse);
		covtlnbCall6000();
		/* Release and life record currently kept.*/
		lifelnbIO.setFunction(varcom.rlse);
		lifelnbCall5000();
		/* If the action is select then clear the available issue*/
		/* flag on the header.*/
		if (isEQ(sv.action, "1")) {
			clearAvailableIssue7000();
			gensswrec.function.set("A");
		}
		/* If action is add then set up the life key and keep*/
		/* the record for subsequent use by the next program.*/
		/* Use the next life field created in the initalise section*/
		/* to specify where the life number for the new life to be*/
		/* added.*/
		if (isEQ(sv.action, "2")) {
			lifelnbIO.setDataArea(SPACES);
			lifelnbIO.setChdrcoy(wsspcomn.company);
			lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
			lifelnbIO.setLife(wsaaNextAvailable);
			lifelnbIO.setJlife(ZERO);
			lifelnbIO.setTranno(ZERO);
			lifelnbIO.setCurrfrom(ZERO);
			lifelnbIO.setCurrto(ZERO);
			lifelnbIO.setLifeCommDate(ZERO);
			lifelnbIO.setAnbAtCcd(ZERO);
			lifelnbIO.setCltdob(ZERO);
			lifelnbIO.setUser(ZERO);
			lifelnbIO.setTransactionDate(ZERO);
			lifelnbIO.setTransactionTime(ZERO);
			/* Keep the lifelnb record.*/
			lifelnbIO.setFunction(varcom.keeps);
			lifelnbIO.setFormat(lifelnbrec);
			lifelnbCall5000();
			gensswrec.function.set("B");
		}
		callGenssw4100();
	}

	/**
	 * <pre>
	 *     WHEN THE SUBFILE RECORD IS A LIFE TYPE
	 * </pre>
	 */
	protected void lifeType4600()
	{
		lifeType4610();
	}

	protected void lifeType4610()
	{
		/* Release the Covtlnb record.*/
		covtlnbIO.setFunction(varcom.rlse);
		covtlnbCall6000();
		/* Release the Lifelnb record.*/
		lifelnbIO.setFunction(varcom.rlse);
		lifelnbCall5000();
		/* If action is select then clear the available issue flag*/
		/* in the header record.*/
		if (isEQ(sv.action, "1")) {
			clearAvailableIssue7000();
			gensswrec.function.set("F");
		}
		if (isEQ(sv.action, "2")) {
			gensswrec.function.set("G");
		}
		/* If action is delete then clear the available issue flag*/
		/* in the header record.*/
		if (isEQ(sv.action, "9")) {
			clearAvailableIssue7000();
			gensswrec.function.set("H");
		}
		/* Set up the lifelnb record key and keep - for subsequent*/
		/* programs.*/
		
		/*	START OF ILIFE-6911 Retrieving data based on lifcnum	*/
		lifepf = lifepfDAO.getLifeRecordByLifcNum(wsspcomn.company.toString().trim(), sv.chdrnum.toString().trim(), sv.elemkey.toString().trim());
		if(lifepf == null){
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if(lifepf != null){
			lifelnbIO.setChdrcoy(wsspcomn.company);
			lifelnbIO.setChdrnum(sv.chdrnum);
			lifelnbIO.setValidflag(lifepf.getValidflag());
			lifelnbIO.setStatcode(lifepf.getStatcode());
			lifelnbIO.setTranno(lifepf.getTranno());
			lifelnbIO.setCurrfrom(lifepf.getCurrfrom());
			lifelnbIO.setCurrto(lifepf.getCurrto());
			lifelnbIO.setLife(lifepf.getLife());
			lifelnbIO.setJlife(lifepf.getJlife());
			lifelnbIO.setLifeCommDate(lifepf.getLifeCommDate());
			lifelnbIO.setLifcnum(lifepf.getLifcnum());
			lifelnbIO.setLiferel(lifepf.getLiferel());
			lifelnbIO.setCltdob(lifepf.getCltdob());
			lifelnbIO.setCltsex(lifepf.getCltsex());
			lifelnbIO.setAnbAtCcd(lifepf.getAnbAtCcd());
			lifelnbIO.setAgeadm(lifepf.getAgeadm());
			lifelnbIO.setSelection(lifepf.getSelection());
			lifelnbIO.setSmoking(lifepf.getSmoking());
			lifelnbIO.setOccup(lifepf.getOccup());
			lifelnbIO.setPursuit01(lifepf.getPursuit01());
			lifelnbIO.setPursuit02(lifepf.getPursuit02());
			lifelnbIO.setMaritalState(lifepf.getMaritalState());
			lifelnbIO.setSbstdl(lifepf.getSbstdl());
			lifelnbIO.setTermid(lifepf.getTermid());
			lifelnbIO.setTransactionDate(lifepf.getTransactionDate());
			lifelnbIO.setTransactionTime(lifepf.getTransactionTime());
			lifelnbIO.setUser(lifepf.getUser());
			lifelnbIO.setRelation(lifepf.getRelation());
			lifelnbIO.setFunction(varcom.keeps);
			lifelnbIO.setFormat(lifelnbrec);
			lifelnbCall5000();
		}
		/*	END OF ILIFE-6911	*/
		
		/*lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(sv.chdrnum);
		lifelnbIO.setLife(sv.life);
		lifelnbIO.setJlife("00");*/
		/* Read and store the correct details for the life.*/
		/*lifelnbIO.setFunction(varcom.reads);
		lifelnbCall5000();*/
		callGenssw4100();
	}

	/**
	 * <pre>
	 *     WHEN THE SUBFILE RECORD IS A COVER/RIDER TYPE
	 * </pre>
	 */
	protected void coverRiderType4700()
	{
		coverRiderType4710();
	}

	protected void coverRiderType4710()
	{
		/* Release the lifelnb record.*/
		lifelnbIO.setFunction(varcom.rlse);
		lifelnbCall5000();
		/* Set up the key for the lifelnb record, read and keep for*/
		/* subsequent programs.*/
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(sv.chdrnum);
		lifelnbIO.setLife(sv.hlife);
		lifelnbIO.setJlife("00");
		/* Read and store the correct details for the life.*/
		lifelnbIO.setFunction(varcom.reads);
		lifelnbCall5000();
		/* Release the covtlnb record.*/
		covtlnbIO.setFunction(varcom.rlse);
		covtlnbCall6000();
		/* Set up the key for the covtlnb record and keep this for*/
		/* subsequent programs.*/


		covtlnbIO.setChdrcoy(wsspcomn.company);
		covtlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		covtlnbIO.setLife(sv.hlife);
		covtlnbIO.setCoverage(sv.hcoverage);
		if (isEQ(sv.hrider, SPACES)) {
			sv.hrider.set("00");
		}
		covtlnbIO.setRider(sv.hrider);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
				&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}


		if (isNE(wsspcomn.company, covtlnbIO.getChdrcoy())
				|| isNE(chdrlnbIO.getChdrnum(), covtlnbIO.getChdrnum())
				|| isNE(sv.hlife, covtlnbIO.getLife())
				|| isNE(sv.hcoverage, covtlnbIO.getCoverage())
				|| isNE(sv.hrider, covtlnbIO.getRider())) {
			covtlnbIO.setStatuz(varcom.mrnf);
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		covtlnbIO.setCntcurr(chdrlnbIO.getCntcurr());
		covtlnbIO.setFunction(varcom.keeps);
		covtlnbIO.setFormat("COVTLNBREC");
		covtlnbCall6000();
		//ILIFE-8123-starts 
		rolloverFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), ROLLOVER_FEATURE_ID, appVars, "IT");
        if( rolloverFlag && isEQ(sv.action,9) && isEQ(covtlnbIO.getSingpremtype(),"ROP")) {
    				updateRlvr();
        }
        //ILIFE-8123-ends

		if (isEQ(sv.hrider, "00")) {
			sv.hrider.set(SPACES);
		}
		/* If the action is select then retrieve the next programs*/
		/* to switch to from the table T5671 for the relevent cover/*/
		/* rider.*/
		if (isEQ(sv.action, "1")) {
			lookupT56714791();
			return ;
		}
		if (isEQ(sv.action, "2")) {
			gensswrec.function.set("I");
		}
		/*IF S5003-ACTION = '3'                                        */
		/*   MOVE 'J'                 TO GENS-FUNCTION.                */
		if (isEQ(sv.action, "9")) {
			gensswrec.function.set("K");
		}
		callGenssw4100();
	}

		//ILIFE-8123-starts
		private void updateRlvr() {
			rlvrpf =new Rlvrpf();
			rlvrpf.setChdrcoy(covtlnbIO.getChdrcoy().toString());
			rlvrpf.setChdrnum(sv.chdrnum.toString());
			rlvrpf.setValidFlag("2");
			rlvrpf.setLife(sv.hlife.toString());
			rlvrpf.setCoverage(sv.hcoverage.toString());
			rlvrpfDAO.updateRollovRecord(rlvrpf);
		}
		//ILIFE-8123-ends
	/**
	 * <pre>
	 *     LOOK UP PROGRAMS FROM TABLE T5671
	 * </pre>
	 */
	protected void lookupT56714791()
	{
		try {
			lookupT56714792();
			loadProgsToWssp4794();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void lookupT56714792()
	{
		/* Lookup the programs required to process the cover/rider*/
		/* from table T5671.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsddBatckey.batcBatctrcde);
		stringVariable1.addExpression(sv.elemkey);
		itempfList=itempfDAO.getAllItemitem("IT", wsspcomn.company.toString(), "T5671", stringVariable1.toString());
		if (itempfList.isEmpty()) {
			fatalError600();

		}
		t5671rec.t5671Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
	}

	protected void loadProgsToWssp4794()
	{
		/*    Move the component programs to the WSSP stack.*/
		compute(wsccSub1, 0).set(add(1, wsspcomn.programPtr));
		wsccSub2.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
			t5671ToWsspProgs4796();
		}
		/*    Reset the Action to '*' signifying action desired to*/
		/*    the next program.*/
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		goTo(GotoLabel.exit4799);

	}

	protected void t5671ToWsspProgs4796()
	{
		wsspcomn.secProg[wsccSub1.toInt()].set(t5671rec.pgm[wsccSub2.toInt()]);
		wsccSub1.add(1);
		wsccSub2.add(1);
	}

	protected void readSubfile4800()
	{
		read4802();
	}

	protected void read4802()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5003", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
				&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*  Re-validate the deletion of a whole coverage*/
		/*     (in the case of deleting a coverage on a contract which*/
		/*      may have multiple coverages of the same type, if this*/
		/*      coverage is also mandatory, make sure we do not accidently*/
		/*      delete the last one)*/
		if (isNE(sv.coverage, SPACES)
				&& isEQ(sv.action, "9")) {
			getTable2a00();
			if (isEQ(t5673rec.creq[wsbbTableSub.toInt()], "R")) {
				if (isGT(t5673rec.ctmaxcov[wsbbTableSub.toInt()], 1)) {
					checkLimit2g00();
					if (isNE(sv.actionErr, SPACES)) {
						sv.action.set(SPACES);
						/*              MOVE U037          TO WSCC-ERROR                 */
						wsccError.set(errorsInner.h069);
					}
				}
				else {
					sv.action.set(SPACES);
					/*           MOVE U037             TO WSCC-ERROR                 */
					wsccError.set(errorsInner.h069);
				}
			}
		}
	}

	/**
	 * <pre>
	 *     RLSE OR KEEPS THE LIFELNB RECORD
	 * </pre>
	 */
	protected void lifelnbCall5000()
	{
		/*LIFELNB-CALL*/
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     RLSE OR KEEPS THE COVTLNB RECORD
	 * </pre>
	 */
	protected void covtlnbCall6000()
	{
		/*COVTLNB-CALL*/
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     CLEAR THE AVAILABLE ISSUE FIELD ON THE HEADER
	 * </pre>
	 */
	protected void clearAvailableIssue7000()
	{
		/*  Retrieve the latest version of the contract header*/
		/*   (it may have been changed by a called program)*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Clear the available issue field on the header and keep*/
		/* the client header record.*/
		chdrlnbIO.setAvlisu(SPACES);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

	protected void a100AddUnderwritting()
	{
		/* PERFORM A300-READR-LIFEIO.                                   */
		/* PERFORM A200-DEL-UNDERWRITTING.                     <LFA1062>*/
		//IBPLIFE-8676 - START
//		covtcsnIO.setRecKeyData(SPACES);
//		covtcsnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
//		covtcsnIO.setChdrnum(chdrlnbIO.getChdrnum());
//		covtcsnIO.setFunction(varcom.begn);
//		covtcsnIO.setStatuz(varcom.oK);
//		while ( !(isNE(covtcsnIO.getStatuz(), varcom.oK))) {
//			a200DelUnderwritting();
//		}

//		covtcsnIO.setRecKeyData(SPACES);
//		covtcsnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
//		covtcsnIO.setChdrnum(chdrlnbIO.getChdrnum());
		/* MOVE WSAA-STORED-LIFE       TO COVTCSN-LIFE                  */
		/* MOVE WSAA-STORED-COVERAGE   TO COVTCSN-COVERAGE              */
		/* MOVE WSAA-STORED-RIDER      TO COVTCSN-RIDER                 */
//		covtcsnIO.setFunction(varcom.begn);
//		covtcsnIO.setStatuz(varcom.oK);
//		while ( !(isNE(covtcsnIO.getStatuz(), varcom.oK))) {
//			a400NextrCovtcsnio();
//		}
		covtpfList = covtlnbDAO.searchCovtRecordByCoyNumDescUniquNo(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if(covtpfList != null && !covtpfList.isEmpty()) {
				covtpfTemp = covtpfList.get(covtpfList.size() - 1);
		}
		//IBPLIFE-8676 - END
	}

	protected void a200DelUnderwritting()
	{
		//IBPLIFE-8676 - START
//		covtcsnIO.setFormat(covtcsnrec);
		//		covtcsnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//		covtcsnIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");

//		SmartFileCode.execute(appVars, covtcsnIO);
//		if (isNE(covtcsnIO.getStatuz(), varcom.oK)
//				&& isNE(covtcsnIO.getStatuz(), varcom.endp)) {
//			syserrrec.params.set(covtcsnIO.getParams());
//			syserrrec.statuz.set(covtcsnIO.getStatuz());
//			fatalError600();
//		}
//		if ((isEQ(covtcsnIO.getStatuz(), varcom.endp)
//				|| isNE(chdrlnbIO.getChdrcoy(), covtcsnIO.getChdrcoy())
//				|| isNE(chdrlnbIO.getChdrnum(), covtcsnIO.getChdrnum()))
//				&& isNE(covtcsnIO.getFunction(), varcom.begn)) {
//			covtcsnIO.setStatuz(varcom.endp);
//			return ;
//		}
		//IBPLIFE-8676 - END
		/* IF NOT (COVTCSN-STATUZ      = O-K                    <LA3425>*/
		/* AND CHDRLNB-CHDRCOY         = COVTCSN-CHDRCOY        <LA3425>*/
		/* AND CHDRLNB-CHDRNUM         = COVTCSN-CHDRNUM)       <LA3425>*/
		/*     MOVE ENDP               TO COVTCSN-STATUZ        <LA3425>*/
		/*     GO TO A200-EXIT.                                 <LA3425>*/
//		covtcsnIO.setFunction(varcom.nextr); //IBPLIFE-8676
		initialize(crtundwrec.parmRec);
		/*   PERFORM A500-READ-LIFE.                              <LA3425>*/
		/*   MOVE WSAA-UNDW-CLNTNUM      TO UNDW-CLNTNUM.        <LFA1062>*/
		/*   MOVE LIFELNB-LIFCNUM        TO UNDW-CLNTNUM.         <LA3425>*/
		crtundwrec.coy.set(chdrlnbIO.getChdrcoy());
		crtundwrec.currcode.set(chdrlnbIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrlnbIO.getChdrnum());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		crtundwrec = crtundwrtUtil.process(crtundwrec); //IBPLIFE-680
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
	}

	/**
	 * <pre>
	 *A300-READR-LIFEIO SECTION.                                       
	 *A300-CTRL.                                                       
	 **** MOVE CHDRLNB-CHDRCOY        TO LIFE-CHDRCOY                  
	 **** MOVE CHDRLNB-CHDRNUM        TO LIFE-CHDRNUM                  
	 **** MOVE WSAA-STORED-LIFE       TO LIFE-LIFE.                    
	 **** MOVE                        TO LIFE-JLIFE                    
	 **** MOVE                        TO LIFE-CURRFROM                 
	 **** MOVE READR                  TO LIFE-FUNCTION.                
	 **** MOVE LIFEREC                TO LIFE-FORMAT.                  
	 **** CALL 'LIFEIO'               USING LIFE-PARAMS.               
	 **** IF LIFE-STATUZ              NOT = O-K                        
	 ****    MOVE LIFE-PARAMS         TO SYSR-PARAMS                   
	 ****    MOVE LIFE-STATUZ         TO SYSR-STATUZ                   
	 ****    PERFORM 600-FATAL-ERROR.                                  
	 *A300-EXIT.                                                       
	 **** EXIT.                                                        
	 * </pre>
	 */
	protected void a400NextrCovtcsnio()
	{
		//IBPLIFE-8676 - START
//		covtcsnIO.setFormat(covtcsnrec);
		//		covtcsnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//		covtcsnIO.setFitKeysSearch("CHDRCOY" , "CHDRNUM");
//		SmartFileCode.execute(appVars, covtcsnIO);
//		if (!(isEQ(covtcsnIO.getStatuz(), varcom.oK)
//				&& isEQ(chdrlnbIO.getChdrcoy(), covtcsnIO.getChdrcoy())
//				&& isEQ(chdrlnbIO.getChdrnum(), covtcsnIO.getChdrnum()))) {
//			/* AND WSAA-STORED-LIFE        = COVTCSN-LIFE                   */
//			/* AND WSAA-STORED-COVERAGE    = COVTCSN-COVERAGE               */
//			/* AND WSAA-STORED-RIDER       = COVTCSN-RIDER)                 */
//			covtcsnIO.setStatuz(varcom.endp);
//			return;
//		}
//		covtcsnIO.setFunction(varcom.nextr);
		//IBPLIFE-8676 - END
		initialize(crtundwrec.parmRec);
		wsaaJlifeN.set(ZERO);

		a410NextLife();
		return;
	}

	protected void a410NextLife()
	{
		a500ReadLife();
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		/*   MOVE WSAA-UNDW-CLNTNUM      TO UNDW-CLNTNUM.        <LFA1062>*/
		crtundwrec.clntnum.set(lifelnbIO.getLifcnum());
		//IBPLIFE-8676 - START
//		crtundwrec.coy.set(covtcsnIO.getChdrcoy());
//		crtundwrec.chdrnum.set(covtcsnIO.getChdrnum());
//		crtundwrec.life.set(covtcsnIO.getLife());
//		crtundwrec.crtable.set(covtcsnIO.getCrtable());
		crtundwrec.coy.set(covtpfTemp.getChdrcoy());
		crtundwrec.chdrnum.set(covtpfTemp.getChdrnum());
		crtundwrec.life.set(covtpfTemp.getLife());
		crtundwrec.crtable.set(covtpfTemp.getCrtable());
		//IBPLIFE-8676 - END
		crtundwrec.batctrcde.set(wsddBatckey.batcBatctrcde);
		//IBPLIFE-8676 - START
//		crtundwrec.sumins.set(covtcsnIO.getSumins());
		crtundwrec.sumins.set(covtpfTemp.getSumins());
		//IBPLIFE-8676 - END
		crtundwrec.cnttyp.set(chdrlnbIO.getCnttype());
		crtundwrec.currcode.set(chdrlnbIO.getCntcurr());
		crtundwrec.function.set("ADD");
		crtundwrec = crtundwrtUtil.process(crtundwrec); //IBPLIFE-680
		if (isNE(crtundwrec.status, varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
		wsaaJlifeN.add(1);
		a410NextLife();
		return;
	}

	protected void a500ReadLife()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		//IBPLIFE-8676 - START
//		lifelnbIO.setLife(covtcsnIO.getLife());
		lifelnbIO.setLife(covtpfTemp.getLife());
		//IBPLIFE-8676 - END
		/* MOVE '00'                   TO LIFELNB-JLIFE.        <LA3425>*/
		lifelnbIO.setJlife(wsaaJlife);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
				&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
	}
	
	/*	ILIFE-6891	*/
	protected void callDatcon(){
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
	}
	/*	ILIFE-6891	*/
	
	public void performLincProcessing() {
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return;
		}
		Tjl47rec tjl47rec = new Tjl47rec(); 
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsddBatckey.batcBatctrcde.toString());
		itempf.setItemtabl(tjl47);
		itempf = itempfDAO.getItemRecordByItemkey(itempf);
		if(itempf != null) {
			tjl47rec.tjl47Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			LincNBService lincService = (LincNBService) getApplicationContext().getBean(tjl47rec.lincsubr.toString().toLowerCase());
			lincService.newBusinessProposal(setLincpfHelper(), tjl47rec);
		}
	}
	
	public LincpfHelper setLincpfHelper() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		LincpfHelper lincpfHelper = new LincpfHelper();
		lincpfHelper.setChdrcoy(wsspcomn.company.toString());
		lincpfHelper.setChdrnum(sv.chdrnum.toString());
		lincpfHelper.setOwncoy(wsspcomn.fsuco.toString());
		lincpfHelper.setClntnum(chdrlnbIO.getCownnum().toString());
		lincpfHelper.setCownnum(chdrlnbIO.getCownnum().toString());
		lincpfHelper.setLifcnum(lifepf.getLifcnum());
		lincpfHelper.setLcltdob(lifepf.getCltdob());
		lincpfHelper.setLcltsex(lifepf.getCltsex());
		lincpfHelper.setLzkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setLzkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setLzkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setLzkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setLzkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setLzkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setLzkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setLgivname(clntpf.getGivname());
		lincpfHelper.setLsurname(clntpf.getSurname());
		lincpfHelper.setCnttype(chdrlnbIO.getCnttype().toString());
		lincpfHelper.setOccdate(chdrlnbIO.getOccdate().toInt());	
		lincpfHelper.setTransactionDate(datcon1rec.intDate.toInt());
		lincpfHelper.setTranscd(wsddBatckey.batcBatctrcde.toString());
		lincpfHelper.setLanguage(wsspcomn.language.toString());
		lincpfHelper.setTranno(chdrlnbIO.getTranno().toInt());
		lincpfHelper.setTermId(varcom.vrcmTermid.toString());
		lincpfHelper.setTrdt(varcom.vrcmDate.toInt());
		lincpfHelper.setTrtm(varcom.vrcmTime.toInt());
		lincpfHelper.setUsrprf(varcom.vrcmUser.toString());
		lincpfHelper.setJobnm(wsspcomn.userid.toString());
		lincpfHelper = setClientDetails(lincpfHelper);
		return lincpfHelper;
	}
	
	protected LincpfHelper setClientDetails(LincpfHelper lincpfHelper) {
		clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(lincpfHelper.getOwncoy());
		clntpf.setClntnum(lincpfHelper.getClntnum());
		clntpf = clntpfDAO.selectClient(clntpf);
		lincpfHelper.setZkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setZkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setZkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setZkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setZkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setZkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setZkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setGivname(clntpf.getGivname());
		lincpfHelper.setSurname(clntpf.getSurname());
		lincpfHelper.setCltsex(clntpf.getCltsex());
		lincpfHelper.setCltdob(clntpf.getCltdob());
		lincpfHelper.setClttype(clntpf.getClttype());//check this
		return lincpfHelper;
	}
	
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
		/* ERRORS */
		private FixedLengthStringData e005 = new FixedLengthStringData(4).init("E005");
		private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
		private FixedLengthStringData e370 = new FixedLengthStringData(4).init("E370");
		private FixedLengthStringData f290 = new FixedLengthStringData(4).init("F290");
		private FixedLengthStringData g503 = new FixedLengthStringData(4).init("G503");
		private FixedLengthStringData g815 = new FixedLengthStringData(4).init("G815");
		private FixedLengthStringData h069 = new FixedLengthStringData(4).init("H069");
		private FixedLengthStringData r065 = new FixedLengthStringData(4).init("R065");
		private FixedLengthStringData rl30 = new FixedLengthStringData(4).init("RL30");
		private FixedLengthStringData j009 = new FixedLengthStringData(4).init("J009");
		private FixedLengthStringData j010 = new FixedLengthStringData(4).init("J010");
	}
}
