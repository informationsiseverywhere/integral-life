package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.newbusiness.dataaccess.dao.LincpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Lincpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;

/**
 * @author gsaluja2
 *
 */
public class LincpfDAOImpl extends BaseDAOImpl<Lincpf> implements LincpfDAO{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LincpfDAOImpl.class);
	
	private static final String TABLE = "VM1DTA.LINCPF";
	
	@Override
	public Lincpf readLincpfData(String chdrnum) {
		Lincpf lincpf = null;
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM ").append(TABLE).append(" WHERE CHDRNUM=? ORDER BY UNIQUE_NUMBER ASC");
		LOGGER.info("readLincpfData {}", sql);
		PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        try {
        	ps.setString(1, chdrnum);
        	rs = executeQuery(ps);
            if (rs.next()) {
            	lincpf = new Lincpf(); 
            	lincpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
            	lincpf.setChdrnum(rs.getString("CHDRNUM"));
        		lincpf.setCownnum(rs.getString("COWNNUM"));
        		lincpf.setLifcnum(rs.getString("LIFCNUM"));
        		lincpf.setBatctrcde(rs.getString("BATCTRCDE"));
        		lincpf.setSenddate(rs.getInt("SENDDATE"));
        		lincpf.setRecvdate(rs.getInt("RECVDATE"));
        		lincpf.setRecveror(rs.getString("RECVEROR"));
        		lincpf.setZreclass(rs.getString("ZRECLASS"));
        		lincpf.setZmodtype(rs.getString("ZMODTYPE"));
        		lincpf.setZrsltcde(rs.getString("ZRSLTCDE"));
        		lincpf.setZrsltrsn(rs.getString("ZRSLTRSN"));
        		lincpf.setZerrorno(rs.getString("ZERRORNO"));
        		lincpf.setZnofrecs(rs.getString("ZNOFRECS"));
        		lincpf.setKlinckey(rs.getString("KLINCKEY"));
        		lincpf.setHcltnam(rs.getString("HCLTNAM"));
        		lincpf.setCltsex(rs.getString("CLTSEX"));
        		lincpf.setZclntdob(rs.getInt("ZCLNTDOB"));
        		lincpf.setZeffdate(rs.getInt("ZEFFDATE"));
        		lincpf.setFlag(rs.getString("FLAG"));
        		lincpf.setKglcmpcd(rs.getString("KGLCMPCD"));
        		lincpf.setAddrss(rs.getString("ADDRSS"));
        		lincpf.setZhospbnf(rs.getBigDecimal("ZHOSPBNF"));
        		lincpf.setZsickbnf(rs.getBigDecimal("ZSICKBNF"));
        		lincpf.setZcancbnf(rs.getBigDecimal("ZCANCBNF"));
        		lincpf.setZothsbnf(rs.getBigDecimal("ZOTHSBNF"));
        		lincpf.setZregdate(rs.getInt("ZREGDATE"));
        		lincpf.setZlincdte(rs.getInt("ZLINCDTE"));
        		lincpf.setKjhcltnam(rs.getString("KJHCLTNAM"));
        		lincpf.setZhospopt(rs.getString("ZHOSPOPT"));
        		lincpf.setZrevdate(rs.getInt("ZREVDATE"));
        		lincpf.setZlownnam(rs.getString("ZLOWNNAM"));
        		lincpf.setZcsumins(rs.getBigDecimal("ZCSUMINS"));
        		lincpf.setZasumins(rs.getBigDecimal("ZASUMINS"));
        		lincpf.setZdethopt(rs.getString("ZDETHOPT"));
        		lincpf.setZrqflag(rs.getString("ZRQFLAG"));
        		lincpf.setZrqsetdte(rs.getInt("ZRQSETDTE"));
        		lincpf.setZrqcandte(rs.getInt("ZRQCANDTE"));
        		lincpf.setZepflag(rs.getString("ZEPFLAG"));
        		lincpf.setZcvflag(rs.getString("ZCVFLAG"));
        		lincpf.setZcontsex(rs.getString("ZCONTSEX"));
        		lincpf.setZcontdob(rs.getInt("ZCONTDOB"));
        		lincpf.setZcontaddr(rs.getString("ZCONTADDR"));
        		lincpf.setLifnamkj(rs.getString("LIFNAMKJ"));
        		lincpf.setOwnnamkj(rs.getString("OWNNAMKJ"));
        		lincpf.setWflga(rs.getString("WFLGA"));
        		lincpf.setZerrflg(rs.getString("ZERRFLG"));
        		lincpf.setZoccdate(rs.getInt("ZOCCDATE"));
        		lincpf.setZtrdate(rs.getInt("ZTRDATE"));
        		lincpf.setZadrddte(rs.getInt("ZADRDDTE"));
        		lincpf.setZexdrddte(rs.getInt("ZEXDRDDTE"));
        		lincpf.setZexdrddte(rs.getInt("DTETRMNTD"));
        		lincpf.setUsrprf(rs.getString("USRPRF"));
        		lincpf.setJobnm(rs.getString("JOBNM"));
            }
        }
        catch (SQLException e) {
            LOGGER.error("readLincpfData {}", e);
        } finally {
            close(ps, rs);
        }
		return lincpf;
	}

	@Override
	public void insertLincData(Lincpf lincpf) {
		StringBuilder sql = new StringBuilder();
		sql.append("INSERT INTO ").append(TABLE).append("(CHDRNUM,COWNNUM,LIFCNUM,BATCTRCDE,SENDDATE,RECVDATE,");
		sql.append("RECVEROR,ZRECLASS,ZMODTYPE,ZRSLTCDE,ZRSLTRSN,ZERRORNO,ZNOFRECS,KLINCKEY,HCLTNAM,CLTSEX,");
		sql.append("ZCLNTDOB,ZEFFDATE,FLAG,KGLCMPCD,ADDRSS,ZHOSPBNF,ZSICKBNF,ZCANCBNF,ZOTHSBNF,ZREGDATE,ZLINCDTE,");
		sql.append("KJHCLTNAM,ZHOSPOPT,ZREVDATE,ZLOWNNAM,ZCSUMINS,ZASUMINS,ZDETHOPT,ZRQFLAG,ZRQSETDTE,ZRQCANDTE,");
		sql.append("ZEPFLAG,ZCVFLAG,ZCONTSEX,ZCONTDOB,ZCONTADDR,LIFNAMKJ,OWNNAMKJ,WFLGA,ZERRFLG,ZOCCDATE,ZTRDATE,");
		sql.append("ZADRDDTE,ZEXDRDDTE,DTETRMNTD,USRPRF,JOBNM,DATIME) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,");
		sql.append("?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
		LOGGER.info("insertLincData {}", sql);
		PreparedStatement ps = null;
		try{
			ps = getPrepareStatement(sql.toString());
			ps.setString(1,lincpf.getChdrnum());
			ps.setString(2,lincpf.getCownnum());
			ps.setString(3,lincpf.getLifcnum());
			ps.setString(4,lincpf.getBatctrcde());
			ps.setInt(5,lincpf.getSenddate());
			ps.setInt(6,lincpf.getRecvdate());
			ps.setString(7,lincpf.getRecveror());
			ps.setString(8,lincpf.getZreclass());
			ps.setString(9,lincpf.getZmodtype());
			ps.setString(10,lincpf.getZrsltcde());
			ps.setString(11,lincpf.getZrsltrsn());
			ps.setString(12,lincpf.getZerrorno());
			ps.setString(13,lincpf.getZnofrecs());
			ps.setString(14,lincpf.getKlinckey());
			ps.setString(15,lincpf.getHcltnam());
			ps.setString(16,lincpf.getCltsex());
			ps.setInt(17,lincpf.getZclntdob());
			ps.setInt(18,lincpf.getZeffdate());
			ps.setString(19,lincpf.getFlag());
			ps.setString(20,lincpf.getKglcmpcd());
			ps.setString(21,lincpf.getAddrss());
			ps.setBigDecimal(22,lincpf.getZhospbnf());
			ps.setBigDecimal(23,lincpf.getZsickbnf());
			ps.setBigDecimal(24,lincpf.getZcancbnf());
			ps.setBigDecimal(25,lincpf.getZothsbnf());
			ps.setInt(26, lincpf.getZregdate());
			ps.setInt(27, lincpf.getZlincdte());
			ps.setString(28,lincpf.getKjhcltnam());
			ps.setString(29,lincpf.getZhospopt());
			ps.setInt(30,lincpf.getZrevdate());
			ps.setString(31,lincpf.getZlownnam());
			ps.setBigDecimal(32,lincpf.getZcsumins());
			ps.setBigDecimal(33,lincpf.getZasumins());
			ps.setString(34,lincpf.getZdethopt());
			ps.setString(35,lincpf.getZrqflag());
			ps.setInt(36,lincpf.getZrqsetdte());
			ps.setInt(37,lincpf.getZrqcandte());
			ps.setString(38,lincpf.getZepflag());
			ps.setString(39,lincpf.getZcvflag());
			ps.setString(40,lincpf.getZcontsex());
			ps.setInt(41,lincpf.getZcontdob());
			ps.setString(42,lincpf.getZcontaddr());
			ps.setString(43,lincpf.getLifnamkj());
			ps.setString(44,lincpf.getOwnnamkj());
			ps.setString(45,lincpf.getWflga());
			ps.setString(46,lincpf.getZerrflg());
			ps.setInt(47,lincpf.getZoccdate());
			ps.setInt(48,lincpf.getZtrdate());
			ps.setInt(49,lincpf.getZadrddte());
			ps.setInt(50,lincpf.getZexdrddte());
			ps.setInt(51,lincpf.getDtetrmntd());
			ps.setString(52, lincpf.getUsrprf());
			ps.setString(53, lincpf.getJobnm());
			ps.setTimestamp(54, new Timestamp(System.currentTimeMillis()));
			ps.executeUpdate();
		}
		catch (SQLException e) {
			LOGGER.error("insertLincData()", e);
		} finally {
			close(ps, null);			
		}
	}

	@Override
	public void updateLincData(Lincpf lincpf) {
		StringBuilder sql = new StringBuilder();
		sql.append("UPDATE ").append(TABLE).append(" SET COWNNUM=?,LIFCNUM=?,BATCTRCDE=?,SENDDATE=?,RECVDATE=?,");
		sql.append("RECVEROR=?,ZRECLASS=?,ZMODTYPE=?,ZRSLTCDE=?,ZRSLTRSN=?,ZERRORNO=?,ZNOFRECS=?,KLINCKEY=?,HCLTNAM=?,");
		sql.append("CLTSEX=?,ZCLNTDOB=?,ZEFFDATE=?,FLAG=?,KGLCMPCD=?,ADDRSS=?,ZHOSPBNF=?,ZSICKBNF=?,ZCANCBNF=?,ZOTHSBNF=?,");
		sql.append("ZREGDATE=?,ZLINCDTE=?,KJHCLTNAM=?,ZHOSPOPT=?,ZREVDATE=?,ZLOWNNAM=?,ZCSUMINS=?,ZASUMINS=?,ZDETHOPT=?,");
		sql.append("ZRQFLAG=?,ZRQSETDTE=?,ZRQCANDTE=?,ZEPFLAG=?,ZCVFLAG=?,ZCONTSEX=?,ZCONTDOB=?,ZCONTADDR=?,LIFNAMKJ=?,");
		sql.append("OWNNAMKJ=?,WFLGA=?,ZERRFLG=?,ZOCCDATE=?,ZTRDATE=?,ZADRDDTE=?,ZEXDRDDTE=?,DTETRMNTD=?,");
		sql.append("USRPRF=?,JOBNM=?,DATIME=? WHERE UNIQUE_NUMBER=?");
		LOGGER.info("updateLincData {}", sql);
		PreparedStatement ps = null;
		try{
			ps = getPrepareStatement(sql.toString());
			ps.setString(1,lincpf.getCownnum());
			ps.setString(2,lincpf.getLifcnum());
			ps.setString(3,lincpf.getBatctrcde());
			ps.setInt(4,lincpf.getSenddate());
			ps.setInt(5,lincpf.getRecvdate());
			ps.setString(6,lincpf.getRecveror());
			ps.setString(7,lincpf.getZreclass());
			ps.setString(8,lincpf.getZmodtype());
			ps.setString(9,lincpf.getZrsltcde());
			ps.setString(10,lincpf.getZrsltrsn());
			ps.setString(11,lincpf.getZerrorno());
			ps.setString(12,lincpf.getZnofrecs());
			ps.setString(13,lincpf.getKlinckey());
			ps.setString(14,lincpf.getHcltnam());
			ps.setString(15,lincpf.getCltsex());
			ps.setInt(16,lincpf.getZclntdob());
			ps.setInt(17,lincpf.getZeffdate());
			ps.setString(18,lincpf.getFlag());
			ps.setString(19,lincpf.getKglcmpcd());
			ps.setString(20,lincpf.getAddrss());
			ps.setBigDecimal(21,lincpf.getZhospbnf());
			ps.setBigDecimal(22,lincpf.getZsickbnf());
			ps.setBigDecimal(23,lincpf.getZcancbnf());
			ps.setBigDecimal(24,lincpf.getZothsbnf());
			ps.setInt(25, lincpf.getZregdate());
			ps.setInt(26, lincpf.getZlincdte());
			ps.setString(27,lincpf.getKjhcltnam());
			ps.setString(28,lincpf.getZhospopt());
			ps.setInt(29,lincpf.getZrevdate());
			ps.setString(30,lincpf.getZlownnam());
			ps.setBigDecimal(31,lincpf.getZcsumins());
			ps.setBigDecimal(32,lincpf.getZasumins());
			ps.setString(33,lincpf.getZdethopt());
			ps.setString(34,lincpf.getZrqflag());
			ps.setInt(35,lincpf.getZrqsetdte());
			ps.setInt(36,lincpf.getZrqcandte());
			ps.setString(37,lincpf.getZepflag());
			ps.setString(38,lincpf.getZcvflag());
			ps.setString(39,lincpf.getZcontsex());
			ps.setInt(40,lincpf.getZcontdob());
			ps.setString(41,lincpf.getZcontaddr());
			ps.setString(42,lincpf.getLifnamkj());
			ps.setString(43,lincpf.getOwnnamkj());
			ps.setString(44,lincpf.getWflga());
			ps.setString(45,lincpf.getZerrflg());
			ps.setInt(46,lincpf.getZoccdate());
			ps.setInt(47,lincpf.getZtrdate());
			ps.setInt(48,lincpf.getZadrddte());
			ps.setInt(49,lincpf.getZexdrddte());
			ps.setInt(50,lincpf.getDtetrmntd());
			ps.setString(51, lincpf.getUsrprf());
			ps.setString(52, lincpf.getJobnm());
			ps.setTimestamp(53, new Timestamp(System.currentTimeMillis()));
        	ps.setLong(54, lincpf.getUniqueNumber());
        	ps.executeUpdate();
		}
		catch (SQLException e) {
			LOGGER.error("insertLincData()", e);
		} finally {
			close(ps, null);			
		}
	}
}
