package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:44
 * Description:
 * Copybook name: HPTNMPSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hptnmpskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hptnmpsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hptnmpsKey = new FixedLengthStringData(64).isAPartOf(hptnmpsFileKey, 0, REDEFINE);
  	public FixedLengthStringData hptnmpsChdrcoy = new FixedLengthStringData(1).isAPartOf(hptnmpsKey, 0);
  	public PackedDecimalData hptnmpsPtrneff = new PackedDecimalData(8, 0).isAPartOf(hptnmpsKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(58).isAPartOf(hptnmpsKey, 6, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hptnmpsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hptnmpsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}