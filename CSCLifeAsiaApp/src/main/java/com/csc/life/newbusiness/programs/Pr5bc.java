package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.newbusiness.dataaccess.dao.RlvrpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Rlvrpf;
import com.csc.life.newbusiness.screens.Sr5bcScreenVars;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO; //ILB-486
import com.csc.life.productdefinition.dataaccess.model.Covrpf; //ILB-486
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.DateUtils;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*Function:
*     This screen is used at new business, component add and
* single premium further investment. It will record the
* deductible amount advised by the Employer and Member on
* which contributions tax will be based for a contract
* / coverage by creating a ZCTX record.
*
*     For single premium coverages the user will only be able to
* enter amount (or leave the screen blank). For regular premium
* contracts the user will only be able to enter percentages
* (or leave the screen blank).
*
*      
*
*****************************************************************
* </pre>
*/
public class Pr5bc extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR5BC");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
    private DescTableDAM descIO = new DescTableDAM();
    private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Sr5bcScreenVars sv = ScreenProgram.getScreenVars( Sr5bcScreenVars.class);
	private Rlvrpf rlvrpf=null;
	private RlvrpfDAO rlvrpfDAO = getApplicationContext().getBean("rlvrpfDAO", RlvrpfDAO.class);
	private ItemTableDAM itemIO = new ItemTableDAM();	
	private static final Logger LOGGER = LoggerFactory.getLogger(DateUtils.class);
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);  // ILIFE-8298
	private Covrpf covrpf = new Covrpf(); // ILIFE-8298
	private String life;
	private String coverage;
	private String rider;	
 

	public Pr5bc() {
		super();
		screenVars = sv;
		new ScreenModel("Sr5bc", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
	sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
	scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
	wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

	try {
		super.mainline();
	} catch (COBOLExitProgramException e) {
	// Expected exception for control flow purposes

	}
}
protected void initialise1000(){
	try {
		initialise1010();
	}
	catch (GOTOException e){
	}
}
protected void initialise1010(){
	sv.dataArea.set(SPACES);	
	sv.mbrAccNum.set(SPACES);
	sv.USIFndNum.set(SPACES);
	retrvChdr1500();
	obtainRolloverRecord();
	retrvSingPrem();//ILIFE-7805
	  
}


protected void retrvChdr1500(){
	/*Retrv the chdrlnb record*/	
		
	if(isNE(wsspcomn.chdrChdrnum,SPACES)){
		sv.chdrnum.set(wsspcomn.chdrChdrnum);
		sv.chdrcoy.set(wsspcomn.chdrChdrcoy);	
	}
	if(isEQ(wsspcomn.chdrChdrnum,SPACES)){
		syserrrec.params.set(wsspcomn.chdrChdrnum);
		fatalError600();
	}
}

	protected void obtainRolloverRecord() {
	
		if (isEQ(wsspcomn.tranno.toInt(), 1)) {
			covtlnbIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
			life=covtlnbIO.getLife().toString();
			coverage=covtlnbIO.getCoverage().toString();
			rider=covtlnbIO.getRider().toString();
		} 
		else if (isEQ(wsspcomn.tranno.toInt(), 2)) {
 //ILB-486 start
			covrpf = covrpfDAO.getCacheObject(covrpf);
			if(null==covrpf) {
				covrmjaIO.setFunction(varcom.retrv);
				SmartFileCode.execute(appVars, covrmjaIO);
				if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(covrmjaIO.getParams());
					fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
			}
		}else {
			covrpfDAO.setCacheObject(covrpf);
		}
			/*covrmjaIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}*/
			life=covrpf.getLife();//IJTI-1485
			coverage=covrpf.getCoverage();//IJTI-1485
			rider=covrpf.getRider();//IJTI-1485
 //ILB-486 end
		}
		rlvrpf = rlvrpfDAO.checkRecord(sv.chdrcoy.toString(), sv.chdrnum.toString(), life,coverage);
		if (rlvrpf != null)
			setUpFromRlvrpf();
		else
			sv.startdteDisp.clear();
	}

//ILIFE-7805
protected void retrvSingPrem(){	
	if(isNE(wsspcomn.grossprem,ZERO)){
		sv.totTranAmt.set(wsspcomn.grossprem);	
		sv.totTranAmtOut[varcom.pr.toInt()].set("Y");
	}
}

protected void setUpFromRlvrpf(){
	
	sv.mbrAccNum.set(rlvrpf.getMemaccnum());	
	sv.USIFndNum.set(rlvrpf.getFndusinum());	
	sv.startdte.set(rlvrpf.getSrvstrtdt());
	sv.taxFreeCompAmt.set(rlvrpf.getTaxfreeamt());
	sv.kiwiCompAmt.set(rlvrpf.getKiwisvrcom());
	sv.taxAmt.set(rlvrpf.getTaxedamt());
	sv.nonTaxAmt.set(rlvrpf.getUntaxedamt());
	sv.presAmt.set(rlvrpf.getPrsrvdamt());
	sv.kiwiPresAmt.set(rlvrpf.getKiwiamt());
	sv.unRestrictAmt.set(rlvrpf.getUnrestrict());
	sv.restrictAmt.set(rlvrpf.getRstrict());
	sv.totTranAmt.set(rlvrpf.getTottrfamt());
	sv.moneyInd.set(rlvrpf.getMoneyind());
}

protected void preScreenEdit(){
	try {
		preStart();
	}
	catch (GOTOException e){
	}
}

protected void preStart() {
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(varcom.prot);
	}
}

protected void screenEdit2000(){
	screenIo2010();
	validate2020();	
	checkForErrors2080();
}

protected void screenIo2010(){
	wsspcomn.edterror.set(varcom.oK);
	if (isEQ(scrnparams.statuz, "KILL")) {
		return;
	}
	
}
protected void validate2020(){
	 if(isEQ(wsspcomn.flag,"I")){
		 checkForErrors2080();
	 }
	 /*
	 *    Validate fields
	 */
	 if (isEQ(scrnparams.statuz,varcom.calc)){
		 wsspcomn.edterror.set("Y");
	 }		 
 }

protected void checkForErrors2080(){
	if (isNE(sv.errorIndicators, SPACES)) {
		wsspcomn.edterror.set("Y");
	}
	if (isEQ(scrnparams.statuz,varcom.kill)) {
		return ;
	}
}


/**     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION*/

protected void update3000()
{
	try {
		update3010();  		
	}
	catch (GOTOException e){
		/* Expected exception for control flow purposes. */
	}
}

protected void update3010(){

    /*
    *  Update database files as required / WSSP
    */
	if(isEQ(wsspcomn.flag,"I")){
		return;
	}
	if (isEQ(scrnparams.statuz,varcom.kill)) {
		return ;
	}
	createRollover();
}


protected void createRollover(){
	
	//ILIFE-8123-starts 
	if (isEQ(wsspcomn.tranno.toInt(), 1)) {
			covtlnbIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
			life=covtlnbIO.getLife().toString();
			coverage=covtlnbIO.getCoverage().toString();
			rider=covtlnbIO.getRider().toString();
			wsspcomn.tranno.set(ZERO);
		} else if (isEQ(wsspcomn.tranno.toInt(), 2)) {
 //ILB-486 start
			covrpf = covrpfDAO.getCacheObject(covrpf);
			if(null==covrpf) {
				covrmjaIO.setFunction(varcom.retrv);
				SmartFileCode.execute(appVars, covrmjaIO);
				if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
					syserrrec.params.set(covrmjaIO.getParams());
					fatalError600();
			}
			else {
				covrpf=covrpfDAO.getCovrRecord(covrmjaIO.getChdrcoy().toString(),covrmjaIO.getChdrnum().toString(),covrmjaIO.getLife().toString(),covrmjaIO.getCoverage().toString(),
						covrmjaIO.getRider().toString(),covrmjaIO.getPlanSuffix().toInt(),covrmjaIO.getValidflag().toString());
				if(null==covrpf) {
					fatalError600();
				}
			else {
				covrpfDAO.setCacheObject(covrpf);
				}
			}
		}else {
			covrpfDAO.setCacheObject(covrpf);
		}
			/*covrmjaIO.setFunction("RETRV");
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(covrmjaIO.getParams());
				fatalError600();
			}*/
			life=covrpf.getLife();//IJTI-1485
			coverage=covrpf.getCoverage();//IJTI-1485
			rider=covrpf.getRider();//IJTI-1485
			sv.chdrcoy.set(covrpf.getChdrcoy());
 //ILB-486 end
		}
		rlvrpf = rlvrpfDAO.checkRecord(sv.chdrcoy.toString(), sv.chdrnum.toString(), life,coverage);
		if(rlvrpf != null) {
			if(isNE(sv.mbrAccNum.trim(),"0"))
				rlvrpf.setMemaccnum(sv.mbrAccNum.toString());
			if(isNE(sv.USIFndNum.trim(),"0"))
				rlvrpf.setFndusinum(sv.USIFndNum.toString());
			rlvrpf.setSrvstrtdt(sv.startdte.toInt());
			rlvrpf.setTaxfreeamt(sv.taxFreeCompAmt.getbigdata());
			rlvrpf.setKiwisvrcom(sv.kiwiCompAmt.getbigdata());
			rlvrpf.setTaxedamt(sv.taxAmt.getbigdata());
			rlvrpf.setUntaxedamt(sv.nonTaxAmt.getbigdata());
			rlvrpf.setPrsrvdamt(sv.presAmt.getbigdata());
			rlvrpf.setKiwiamt(sv.kiwiPresAmt.getbigdata());
			rlvrpf.setUnrestrict(sv.unRestrictAmt.getbigdata());
			rlvrpf.setRstrict(sv.restrictAmt.getbigdata());
			rlvrpf.setTottrfamt(sv.totTranAmt.getbigdata());
			rlvrpf.setMoneyind(sv.moneyInd.toString());
			rlvrpfDAO.updateRlvrpf(rlvrpf);
		}else {		 
		if(isNE(sv.mbrAccNum,SPACES) || isNE(sv.USIFndNum,SPACES) || isNE(sv.startdte,varcom.vrcmMaxDate)
		|| isNE(sv.taxFreeCompAmt,ZERO) || isNE(sv.kiwiCompAmt,ZERO) ||isNE(sv.taxAmt,ZERO)
		|| isNE(sv.nonTaxAmt,ZERO) || isNE(sv.presAmt,ZERO) ||isNE(sv.kiwiPresAmt,ZERO)
		|| isNE(sv.unRestrictAmt,ZERO) || isNE(sv.restrictAmt,ZERO) ||isNE(sv.totTranAmt,ZERO) 
		|| isEQ(sv.moneyInd,"Y")) {
			rlvrpf=new Rlvrpf();
			rlvrpf.setLife(life);
			rlvrpf.setCoverage(coverage);
			rlvrpf.setRider(rider);
			rlvrpf.setChdrcoy(sv.chdrcoy.toString());
			rlvrpf.setChdrnum(sv.chdrnum.toString());
			if(isNE(sv.mbrAccNum.trim(),"0"))
				rlvrpf.setMemaccnum(sv.mbrAccNum.toString());
			if(isNE(sv.USIFndNum.trim(),"0"))
				rlvrpf.setFndusinum(sv.USIFndNum.toString());
			rlvrpf.setSrvstrtdt(sv.startdte.toInt());
			rlvrpf.setTaxfreeamt(sv.taxFreeCompAmt.getbigdata());
			rlvrpf.setKiwisvrcom(sv.kiwiCompAmt.getbigdata());
			rlvrpf.setTaxedamt(sv.taxAmt.getbigdata());
			rlvrpf.setUntaxedamt(sv.nonTaxAmt.getbigdata());
			rlvrpf.setPrsrvdamt(sv.presAmt.getbigdata());
			rlvrpf.setKiwiamt(sv.kiwiPresAmt.getbigdata());
			rlvrpf.setUnrestrict(sv.unRestrictAmt.getbigdata());
			rlvrpf.setRstrict(sv.restrictAmt.getbigdata());
			rlvrpf.setTottrfamt(sv.totTranAmt.getbigdata());
			rlvrpf.setMoneyind(sv.moneyInd.toString());
			rlvrpfDAO.insertRecord(rlvrpf);
		}
	}
}

protected void whereNext4000()
{
	nextProgram4010();
}

protected void nextProgram4010()
{
	wsspcomn.programPtr.add(1);
	wsspcomn.nextprog.set(wsaaProg);
}

private static final class FormatsInner {
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}


/*
 * Class transformed  from Data Structure ERRORS_INNER
 */
private static final class ErrorsInner {
		/* ERRORS */
	private FixedLengthStringData e631 = new FixedLengthStringData(4).init("E631");
	private FixedLengthStringData rglm = new FixedLengthStringData(4).init("RGLM");
	private FixedLengthStringData rfxm = new FixedLengthStringData(4).init("RFXM");
	
}

}