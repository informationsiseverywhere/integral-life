package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HratpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:35
 * Class transformed from HRATPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HratpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 533;
	public FixedLengthStringData hratrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hratpfRecord = hratrec;
	
	public FixedLengthStringData itemitem = DD.itemitem.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr01 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr02 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr03 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr04 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr05 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr06 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr07 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr08 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr09 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr10 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData instpr11 = DD.instpr.copy().isAPartOf(hratrec);
	public PackedDecimalData mfacthm = DD.mfacthm.copy().isAPartOf(hratrec);
	public PackedDecimalData mfacthy = DD.mfacthy.copy().isAPartOf(hratrec);
	public PackedDecimalData mfactm = DD.mfactm.copy().isAPartOf(hratrec);
	public PackedDecimalData mfactq = DD.mfactq.copy().isAPartOf(hratrec);
	public PackedDecimalData mfactw = DD.mfactw.copy().isAPartOf(hratrec);
	public PackedDecimalData mfact2w = DD.mfactwb.copy().isAPartOf(hratrec);
	public PackedDecimalData mfact4w = DD.mfactwd.copy().isAPartOf(hratrec);
	public PackedDecimalData mfacty = DD.mfacty.copy().isAPartOf(hratrec);
	public PackedDecimalData premUnit = DD.pmunit.copy().isAPartOf(hratrec);
	public PackedDecimalData unit = DD.unit.copy().isAPartOf(hratrec);
	public FixedLengthStringData disccntmeth = DD.discntmeth.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm01 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm02 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm03 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm04 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm05 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm06 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm07 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm08 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm09 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm10 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm11 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm12 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm13 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm14 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm15 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm16 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm17 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm18 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm19 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm20 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm21 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm22 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm23 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm24 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm25 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm26 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm27 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm28 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm29 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm30 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm31 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm32 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm33 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm34 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm35 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm36 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm37 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm38 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm39 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm40 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm41 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm42 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm43 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm44 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm45 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm46 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm47 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm48 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm49 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm50 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm51 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm52 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm53 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm54 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm55 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm56 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm57 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm58 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm59 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm60 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm61 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm62 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm63 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm64 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm65 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm66 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm67 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm68 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm69 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm70 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm71 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm72 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm73 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm74 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm75 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm76 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm77 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm78 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm79 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm80 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm81 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm82 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm83 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm84 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm85 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm86 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm87 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm88 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm89 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm90 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm91 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm92 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm93 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm94 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm95 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm96 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm97 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm98 = DD.insprm.copy().isAPartOf(hratrec);
	public PackedDecimalData insprm99 = DD.insprm.copy().isAPartOf(hratrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hratrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hratrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hratrec);
	public PackedDecimalData insprem = DD.insprem.copy().isAPartOf(hratrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HratpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HratpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HratpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HratpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HratpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HratpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HratpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HRATPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"ITEMITEM, " +
							"INSTPR01, " +
							"INSTPR02, " +
							"INSTPR03, " +
							"INSTPR04, " +
							"INSTPR05, " +
							"INSTPR06, " +
							"INSTPR07, " +
							"INSTPR08, " +
							"INSTPR09, " +
							"INSTPR10, " +
							"INSTPR11, " +
							"MFACTHM, " +
							"MFACTHY, " +
							"MFACTM, " +
							"MFACTQ, " +
							"MFACTW, " +
							"MFACTWB, " +
							"MFACTWD, " +
							"MFACTY, " +
							"PMUNIT, " +
							"UNIT, " +
							"DISCNTMETH, " +
							"INSPRM01, " +
							"INSPRM02, " +
							"INSPRM03, " +
							"INSPRM04, " +
							"INSPRM05, " +
							"INSPRM06, " +
							"INSPRM07, " +
							"INSPRM08, " +
							"INSPRM09, " +
							"INSPRM10, " +
							"INSPRM11, " +
							"INSPRM12, " +
							"INSPRM13, " +
							"INSPRM14, " +
							"INSPRM15, " +
							"INSPRM16, " +
							"INSPRM17, " +
							"INSPRM18, " +
							"INSPRM19, " +
							"INSPRM20, " +
							"INSPRM21, " +
							"INSPRM22, " +
							"INSPRM23, " +
							"INSPRM24, " +
							"INSPRM25, " +
							"INSPRM26, " +
							"INSPRM27, " +
							"INSPRM28, " +
							"INSPRM29, " +
							"INSPRM30, " +
							"INSPRM31, " +
							"INSPRM32, " +
							"INSPRM33, " +
							"INSPRM34, " +
							"INSPRM35, " +
							"INSPRM36, " +
							"INSPRM37, " +
							"INSPRM38, " +
							"INSPRM39, " +
							"INSPRM40, " +
							"INSPRM41, " +
							"INSPRM42, " +
							"INSPRM43, " +
							"INSPRM44, " +
							"INSPRM45, " +
							"INSPRM46, " +
							"INSPRM47, " +
							"INSPRM48, " +
							"INSPRM49, " +
							"INSPRM50, " +
							"INSPRM51, " +
							"INSPRM52, " +
							"INSPRM53, " +
							"INSPRM54, " +
							"INSPRM55, " +
							"INSPRM56, " +
							"INSPRM57, " +
							"INSPRM58, " +
							"INSPRM59, " +
							"INSPRM60, " +
							"INSPRM61, " +
							"INSPRM62, " +
							"INSPRM63, " +
							"INSPRM64, " +
							"INSPRM65, " +
							"INSPRM66, " +
							"INSPRM67, " +
							"INSPRM68, " +
							"INSPRM69, " +
							"INSPRM70, " +
							"INSPRM71, " +
							"INSPRM72, " +
							"INSPRM73, " +
							"INSPRM74, " +
							"INSPRM75, " +
							"INSPRM76, " +
							"INSPRM77, " +
							"INSPRM78, " +
							"INSPRM79, " +
							"INSPRM80, " +
							"INSPRM81, " +
							"INSPRM82, " +
							"INSPRM83, " +
							"INSPRM84, " +
							"INSPRM85, " +
							"INSPRM86, " +
							"INSPRM87, " +
							"INSPRM88, " +
							"INSPRM89, " +
							"INSPRM90, " +
							"INSPRM91, " +
							"INSPRM92, " +
							"INSPRM93, " +
							"INSPRM94, " +
							"INSPRM95, " +
							"INSPRM96, " +
							"INSPRM97, " +
							"INSPRM98, " +
							"INSPRM99, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"INSPREM, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     itemitem,
                                     instpr01,
                                     instpr02,
                                     instpr03,
                                     instpr04,
                                     instpr05,
                                     instpr06,
                                     instpr07,
                                     instpr08,
                                     instpr09,
                                     instpr10,
                                     instpr11,
                                     mfacthm,
                                     mfacthy,
                                     mfactm,
                                     mfactq,
                                     mfactw,
                                     mfact2w,
                                     mfact4w,
                                     mfacty,
                                     premUnit,
                                     unit,
                                     disccntmeth,
                                     insprm01,
                                     insprm02,
                                     insprm03,
                                     insprm04,
                                     insprm05,
                                     insprm06,
                                     insprm07,
                                     insprm08,
                                     insprm09,
                                     insprm10,
                                     insprm11,
                                     insprm12,
                                     insprm13,
                                     insprm14,
                                     insprm15,
                                     insprm16,
                                     insprm17,
                                     insprm18,
                                     insprm19,
                                     insprm20,
                                     insprm21,
                                     insprm22,
                                     insprm23,
                                     insprm24,
                                     insprm25,
                                     insprm26,
                                     insprm27,
                                     insprm28,
                                     insprm29,
                                     insprm30,
                                     insprm31,
                                     insprm32,
                                     insprm33,
                                     insprm34,
                                     insprm35,
                                     insprm36,
                                     insprm37,
                                     insprm38,
                                     insprm39,
                                     insprm40,
                                     insprm41,
                                     insprm42,
                                     insprm43,
                                     insprm44,
                                     insprm45,
                                     insprm46,
                                     insprm47,
                                     insprm48,
                                     insprm49,
                                     insprm50,
                                     insprm51,
                                     insprm52,
                                     insprm53,
                                     insprm54,
                                     insprm55,
                                     insprm56,
                                     insprm57,
                                     insprm58,
                                     insprm59,
                                     insprm60,
                                     insprm61,
                                     insprm62,
                                     insprm63,
                                     insprm64,
                                     insprm65,
                                     insprm66,
                                     insprm67,
                                     insprm68,
                                     insprm69,
                                     insprm70,
                                     insprm71,
                                     insprm72,
                                     insprm73,
                                     insprm74,
                                     insprm75,
                                     insprm76,
                                     insprm77,
                                     insprm78,
                                     insprm79,
                                     insprm80,
                                     insprm81,
                                     insprm82,
                                     insprm83,
                                     insprm84,
                                     insprm85,
                                     insprm86,
                                     insprm87,
                                     insprm88,
                                     insprm89,
                                     insprm90,
                                     insprm91,
                                     insprm92,
                                     insprm93,
                                     insprm94,
                                     insprm95,
                                     insprm96,
                                     insprm97,
                                     insprm98,
                                     insprm99,
                                     userProfile,
                                     jobName,
                                     datime,
                                     insprem,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		itemitem.clear();
  		instpr01.clear();
  		instpr02.clear();
  		instpr03.clear();
  		instpr04.clear();
  		instpr05.clear();
  		instpr06.clear();
  		instpr07.clear();
  		instpr08.clear();
  		instpr09.clear();
  		instpr10.clear();
  		instpr11.clear();
  		mfacthm.clear();
  		mfacthy.clear();
  		mfactm.clear();
  		mfactq.clear();
  		mfactw.clear();
  		mfact2w.clear();
  		mfact4w.clear();
  		mfacty.clear();
  		premUnit.clear();
  		unit.clear();
  		disccntmeth.clear();
  		insprm01.clear();
  		insprm02.clear();
  		insprm03.clear();
  		insprm04.clear();
  		insprm05.clear();
  		insprm06.clear();
  		insprm07.clear();
  		insprm08.clear();
  		insprm09.clear();
  		insprm10.clear();
  		insprm11.clear();
  		insprm12.clear();
  		insprm13.clear();
  		insprm14.clear();
  		insprm15.clear();
  		insprm16.clear();
  		insprm17.clear();
  		insprm18.clear();
  		insprm19.clear();
  		insprm20.clear();
  		insprm21.clear();
  		insprm22.clear();
  		insprm23.clear();
  		insprm24.clear();
  		insprm25.clear();
  		insprm26.clear();
  		insprm27.clear();
  		insprm28.clear();
  		insprm29.clear();
  		insprm30.clear();
  		insprm31.clear();
  		insprm32.clear();
  		insprm33.clear();
  		insprm34.clear();
  		insprm35.clear();
  		insprm36.clear();
  		insprm37.clear();
  		insprm38.clear();
  		insprm39.clear();
  		insprm40.clear();
  		insprm41.clear();
  		insprm42.clear();
  		insprm43.clear();
  		insprm44.clear();
  		insprm45.clear();
  		insprm46.clear();
  		insprm47.clear();
  		insprm48.clear();
  		insprm49.clear();
  		insprm50.clear();
  		insprm51.clear();
  		insprm52.clear();
  		insprm53.clear();
  		insprm54.clear();
  		insprm55.clear();
  		insprm56.clear();
  		insprm57.clear();
  		insprm58.clear();
  		insprm59.clear();
  		insprm60.clear();
  		insprm61.clear();
  		insprm62.clear();
  		insprm63.clear();
  		insprm64.clear();
  		insprm65.clear();
  		insprm66.clear();
  		insprm67.clear();
  		insprm68.clear();
  		insprm69.clear();
  		insprm70.clear();
  		insprm71.clear();
  		insprm72.clear();
  		insprm73.clear();
  		insprm74.clear();
  		insprm75.clear();
  		insprm76.clear();
  		insprm77.clear();
  		insprm78.clear();
  		insprm79.clear();
  		insprm80.clear();
  		insprm81.clear();
  		insprm82.clear();
  		insprm83.clear();
  		insprm84.clear();
  		insprm85.clear();
  		insprm86.clear();
  		insprm87.clear();
  		insprm88.clear();
  		insprm89.clear();
  		insprm90.clear();
  		insprm91.clear();
  		insprm92.clear();
  		insprm93.clear();
  		insprm94.clear();
  		insprm95.clear();
  		insprm96.clear();
  		insprm97.clear();
  		insprm98.clear();
  		insprm99.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		insprem.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHratrec() {
  		return hratrec;
	}

	public FixedLengthStringData getHratpfRecord() {
  		return hratpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHratrec(what);
	}

	public void setHratrec(Object what) {
  		this.hratrec.set(what);
	}

	public void setHratpfRecord(Object what) {
  		this.hratpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hratrec.getLength());
		result.set(hratrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}