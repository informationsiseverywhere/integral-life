package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:43
 * Description:
 * Copybook name: HPADTRFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hpadtrfkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hpadtrfFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hpadtrfKey = new FixedLengthStringData(256).isAPartOf(hpadtrfFileKey, 0, REDEFINE);
  	public FixedLengthStringData hpadtrfChdrcoy = new FixedLengthStringData(1).isAPartOf(hpadtrfKey, 0);
  	public FixedLengthStringData hpadtrfChdrnum = new FixedLengthStringData(8).isAPartOf(hpadtrfKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(hpadtrfKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hpadtrfFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hpadtrfFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}