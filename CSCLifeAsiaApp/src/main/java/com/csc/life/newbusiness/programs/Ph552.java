/*
 * File: Ph552.java
 * Date: 30 August 2009 1:08:10
 * Author: Quipoz Limited
 * 
 * Class transformed from PH552.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.procedures.Th552pt;
import com.csc.life.newbusiness.screens.Sh552ScreenVars;
import com.csc.life.newbusiness.tablestructures.Th552rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class Ph552 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH552");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "";
	private String wsaaFirstTime = "Y";
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/* ERRORS */
	private static final String e026 = "E026";
	private static final String e027 = "E027";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Th552rec th552rec = new Th552rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sh552ScreenVars sv = getPScreenVars();//ScreenProgram.getScreenVars( Sh552ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		readPrimaryRecord1020, 
		generalArea1045, 
		other1080, 
		other3080, 
		exit3090, 
		continue4020, 
		other4080
	}

	public Ph552() {
		super();
		screenVars = sv;
		new ScreenModel("Sh552", AppVars.getInstance(), sv);
	}
	protected Sh552ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( Sh552ScreenVars.class);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
				case readPrimaryRecord1020: 
					readPrimaryRecord1020();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				case other1080: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		if (isNE(wsaaFirstTime, "Y")) {
			goTo(GotoLabel.readPrimaryRecord1020);
		}
		if (isEQ(itemIO.getItemseq(), SPACES)) {
			wsaaSeq.set(ZERO);
		}
		else {
			wsaaSeq.set(itemIO.getItemseq());
		}
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
	}

protected void readPrimaryRecord1020()
	{
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq, ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq);
		}
		/*READ-RECORD*/
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
		/***** IF WSAA-FIRST-TIME          NOT = 'Y'                        */
		/*****    GO TO 1040-MOVE-TO-SCREEN                                 */
		/***** END-IF.                                                      */
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaFirstTime = "N";
	}


protected void moveToScreen1040()
	{
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)
		&& isEQ(itemIO.getItemseq(), SPACES)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)
		&& isEQ(wsspcomn.flag, "I")) {
			scrnparams.errorCode.set(e026);
			wsaaSeq.subtract(1);
			goTo(GotoLabel.other1080);
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setGenarea(SPACES);
		}
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		
		//htruong25
		moveToScreen1040CustomerSpecific();
		
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		th552rec.th552Rec.set(itemIO.getGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC     **/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.       **/
		if (isNE(itemIO.getGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		th552rec.ageIssageFrm01.set(ZERO);
		th552rec.ageIssageFrm02.set(ZERO);
		th552rec.ageIssageFrm03.set(ZERO);
		th552rec.ageIssageFrm04.set(ZERO);
		th552rec.ageIssageFrm05.set(ZERO);
		th552rec.ageIssageFrm06.set(ZERO);
		th552rec.ageIssageFrm07.set(ZERO);
		th552rec.ageIssageFrm08.set(ZERO);
		th552rec.ageIssageTo01.set(ZERO);
		th552rec.ageIssageTo02.set(ZERO);
		th552rec.ageIssageTo03.set(ZERO);
		th552rec.ageIssageTo04.set(ZERO);
		th552rec.ageIssageTo05.set(ZERO);
		th552rec.ageIssageTo06.set(ZERO);
		th552rec.ageIssageTo07.set(ZERO);
		th552rec.ageIssageTo08.set(ZERO);
		th552rec.zsuminfr01.set(ZERO);
		th552rec.zsuminfr02.set(ZERO);
		th552rec.zsuminfr03.set(ZERO);
		th552rec.zsuminfr04.set(ZERO);
		th552rec.zsuminfr05.set(ZERO);
		th552rec.zsuminfr06.set(ZERO);
		th552rec.zsuminfr07.set(ZERO);
		th552rec.zsuminfr08.set(ZERO);
		th552rec.zsuminto01.set(ZERO);
		th552rec.zsuminto02.set(ZERO);
		th552rec.zsuminto03.set(ZERO);
		th552rec.zsuminto04.set(ZERO);
		th552rec.zsuminto05.set(ZERO);
		th552rec.zsuminto06.set(ZERO);
		th552rec.zsuminto07.set(ZERO);
		th552rec.zsuminto08.set(ZERO);
	}

protected void generalArea1045()
	{
		sv.ageIssageFrms.set(th552rec.ageIssageFrms);
		sv.ageIssageTos.set(th552rec.ageIssageTos);
		sv.currcode.set(th552rec.currcode);
		sv.defFupMeths.set(th552rec.defFupMeths);
		sv.zsuminfrs.set(th552rec.zsuminfrs);
		sv.zsumintos.set(th552rec.zsumintos);
		/*CONFIRMATION-FIELDS*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz, varcom.rold)
		&& isEQ(itemIO.getItemseq(), SPACES)) {
			scrnparams.errorCode.set(e027);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)
		&& isEQ(itemIO.getItemseq(), "99")) {
			scrnparams.errorCode.set(e026);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		if (isEQ(wsaaSeq, ZERO)) {
			itemIO.setItemseq(SPACES);
		}
		else {
			itemIO.setItemseq(wsaaSeq);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(th552rec.th552Rec);
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setFunction(varcom.writr);
			itemIO.setFormat(itemrec);
		}
		else {
			itemIO.setFunction(varcom.rewrt);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.ageIssageFrms, th552rec.ageIssageFrms)) {
			th552rec.ageIssageFrms.set(sv.ageIssageFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ageIssageTos, th552rec.ageIssageTos)) {
			th552rec.ageIssageTos.set(sv.ageIssageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.currcode, th552rec.currcode)) {
			th552rec.currcode.set(sv.currcode);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.defFupMeths, th552rec.defFupMeths)) {
			th552rec.defFupMeths.set(sv.defFupMeths);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zsuminfrs, th552rec.zsuminfrs)) {
			th552rec.zsuminfrs.set(sv.zsuminfrs);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.zsumintos, th552rec.zsumintos)) {
			th552rec.zsumintos.set(sv.zsumintos);
			wsaaUpdateFlag = "Y";
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation4020();
				case continue4020: 
					continue4020();
				case other4080: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation4020()
	{
		if (isNE(scrnparams.statuz, varcom.rolu)
		&& isNE(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.continue4020);
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaSeq.add(1);
		}
		else {
			wsaaSeq.subtract(1);
		}
		goTo(GotoLabel.other4080);
	}

protected void continue4020()
	{
		wsspcomn.programPtr.add(1);
		wsaaSeq.set(ZERO);
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(Th552pt.class, wsaaTablistrec);
		/*EXIT*/
	}

protected void moveToScreen1040CustomerSpecific() {
	
}
public ItemTableDAM getItemIO() {
	return itemIO;
}
public void setItemIO(ItemTableDAM itemIO) {
	this.itemIO = itemIO;
}
}
