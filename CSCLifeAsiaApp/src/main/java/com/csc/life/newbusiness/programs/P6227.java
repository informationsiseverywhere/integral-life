/*
 * File: P6227.java
 * Date: 30 August 2009 0:37:03
 * Author: Quipoz Limited
 * 
 * Class transformed from P6227.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.SoinTableDAM;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.screens.S6227ScreenVars;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* PAYER MAINTENANCE.
*
* Retrieves the contract header details from the contract header
* file (CHDRLNB).    Using RETRV
*
* The payer details i.e. Address are retrieved from the client
* details file (CLTS). If the payer number (payrnum from the
* header) is blank then the owner number is used.
*
* The payer number may be changed, and will be checked
* against the client file.
*
* If 'CALC' is pressed then the screen is redisplayed.
*
* If 'KILL' is pressed then all validation is ignored and the
* PAYER address in the header is left blank - ignored.
*
* Update  the PAYER address in the header record held in the
* buffer using KEEPS.
*
* NOTE: The retrieval of client details and movement of these
* details for both the 1000 and 2000 section is in the
* 1600-RETRIEVAL section.
*
*****************************************************************
* </pre>
*/
public class P6227 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6227");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private static final String e040 = "E040";
	private static final String f782 = "F782";
	private static final String i068 = "I068";
	private static final String t073 = "T073";
		/* TABLES */
	private static final String t6697 = "T6697";
	private static final String clrfrec = "CLRFREC";
	private static final String payrrec = "PAYRREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private SoinTableDAM soinIO = new SoinTableDAM();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6227ScreenVars sv = ScreenProgram.getScreenVars( S6227ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkErrors2200, 
		exit2090
	}

	public P6227() {
		super();
		screenVars = sv;
		new ScreenModel("S6227", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1100();
	}

protected void initialise1100()
	{
		sv.dataArea.set(SPACES);
		sv.incomeSeqNo.set(ZERO);
		/*    Retrieve contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		sv.incomeSeqNo.set(payrIO.getIncomeSeqNo());
		/* If this is not a personal pension contract (ie An entry         */
		/* does not exist on T6697 for this contract type) then            */
		/* non display the source of income field.                         */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6697);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.incseqnoOut[varcom.nd.toInt()].set("Y");
			sv.incseqnoOut[varcom.pr.toInt()].set("Y");
			sv.incomeSeqNo.set(ZERO);
		}
		/* Read the client role file to get the payer number               */
		clrfIO.setForepfx("CH");
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		/*    Retrieve client details using PAYRNUM or COWNNUM if*/
		/*    DESPNUM is blank (from the contract header) as the key.*/
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(clrfIO.getClntnum());
		/*  IF CHDRLNB-PAYRNUM          = SPACES                         */
		/*     MOVE CHDRLNB-COWNNUM     TO CLTS-CLNTNUM                  */
		/*  ELSE                                                         */
		/*     MOVE CHDRLNB-PAYRNUM     TO CLTS-CLNTNUM.                 */
		retrieve1600();
		/*  IF CHDRLNB-PAYRNUM          = SPACES                         */
		/*     MOVE SPACES              TO S6227-PAYRNUM                 */
		/*  ELSE                                                         */
		/*     MOVE CLTS-CLNTNUM        TO S6227-PAYRNUM.                */
		sv.payrnum.set(cltsIO.getClntnum());
		if (isEQ(wsspcomn.flag, "I")) {
			sv.payrnumOut[varcom.pr.toInt()].set("Y");
		}
	}

	/**
	* <pre>
	*      RETRIEVE CLIENT DETAILS AND SET SCREEN
	* </pre>
	*/
protected void retrieve1600()
	{
		retrieve1700();
	}

protected void retrieve1700()
	{
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*    Set screen fields*/
		sv.payorname.set(SPACES);
		sv.cltaddr01.set(SPACES);
		sv.cltaddr02.set(SPACES);
		sv.cltaddr03.set(SPACES);
		sv.cltaddr04.set(SPACES);
		sv.cltaddr05.set(SPACES);
		sv.cltpcode.set(SPACES);
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			sv.payrnumErr.set(e040);
			return ;
		}
		/* Is the client dead ?*/
		if (isLTE(cltsIO.getCltdod(), chdrlnbIO.getOccdate())) {
			sv.payrnumErr.set(f782);
		}
		/* Get the formatted confirmation name.*/
		plainname();
		sv.payorname.set(wsspcomn.longconfname);
		sv.cltaddr01.set(cltsIO.getCltaddr01());
		sv.cltaddr02.set(cltsIO.getCltaddr02());
		sv.cltaddr03.set(cltsIO.getCltaddr03());
		sv.cltaddr04.set(cltsIO.getCltaddr04());
		sv.cltaddr05.set(cltsIO.getCltaddr05());
		sv.cltpcode.set(cltsIO.getCltpcode());
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2100();
				case checkErrors2200: 
					checkErrors2200();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S6227IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S6227-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*    If F11 pressed bypass validation.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT.                                          */
			goTo(GotoLabel.exit2090);
		}
		/*    If F9 pressed then set a srceen error so the screen*/
		/*    is re-displayed.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*    If enquiry mode no validation required                       */
		if (isEQ(wsspcomn.flag, "I")) {
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*       GO TO 2900-EXIT                                           */
			goTo(GotoLabel.exit2090);
		}
	}

protected void validate2100()
	{
		/*    Validate fields*/
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.payrnum);
		if (isEQ(sv.payrnum, SPACES)) {
			sv.payrnumErr.set(t073);
			goTo(GotoLabel.checkErrors2200);
		}
		/* IF S6227-PAYRNUM = SPACES                                    */
		/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
		/*     GO TO 2900-EXIT.                                    <001>*/
		/*     GO TO 2090-EXIT.                                    <001>*/
		/*MOVE CHDRLNB-COWNNUM     TO CLTS-CLNTNUM                  */
		/*                            S6227-PAYRNUM.                */
		/* IF S6227-PAYRNUM            = CHDRLNB-COWNNUM                */
		/*    MOVE F464                TO S6227-PAYRNUM-ERR.            */
		retrieve1600();
		/* If the Income Source field is entered a record must             */
		/* exist on SOIN for FSU company/ Payer Client No./ Income         */
		/* Sequence No.                                                    */
		if (isNE(sv.incomeSeqNo, ZERO)) {
			soinIO.setDataKey(SPACES);
			soinIO.setClntcoy(wsspcomn.fsuco);
			soinIO.setClntnum(sv.payrnum);
			soinIO.setIncomeSeqNo(sv.incomeSeqNo);
			soinIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, soinIO);
			if (isNE(soinIO.getStatuz(), varcom.oK)
			&& isNE(soinIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(soinIO.getParams());
				fatalError600();
			}
			if (isEQ(soinIO.getStatuz(), varcom.mrnf)) {
				sv.incseqnoErr.set(i068);
			}
		}
	}

protected void checkErrors2200()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3100();
	}

protected void updateDatabase3100()
	{
		/*  Update database files as required / WSSP*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*    If F11 pressed bypass keep of contract header detail.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		if (isNE(sv.payrnum, clrfIO.getClntnum())) {
			payrIO.setMandref(SPACES);
		}
		payrIO.setIncomeSeqNo(sv.incomeSeqNo);
		payrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Keep the PAYR record.                                           */
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* If the payer number is changed delete the old client            */
		/* role record and create a new one.                               */
		if (isNE(sv.payrnum, clrfIO.getClntnum())) {
			cltrelnrec.cltrelnRec.set(SPACES);
			cltrelnrec.clrrrole.set("PY");
			cltrelnrec.forepfx.set("CH");
			cltrelnrec.forecoy.set(chdrlnbIO.getChdrcoy());
			wsaaChdrnum.set(payrIO.getChdrnum());
			wsaaPayrseqno.set(payrIO.getPayrseqno());
			cltrelnrec.forenum.set(wsaaForenum);
			cltrelnrec.clntpfx.set("CN");
			cltrelnrec.clntcoy.set(wsspcomn.fsuco);
			cltrelnrec.clntnum.set(clrfIO.getClntnum());
			cltrelnrec.function.set("DEL  ");
			callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
			if (isNE(cltrelnrec.statuz, varcom.oK)) {
				syserrrec.syserrStatuz.set(cltrelnrec.statuz);
				fatalError600();
			}
			cltrelnrec.cltrelnRec.set(SPACES);
			cltrelnrec.clrrrole.set("PY");
			cltrelnrec.forepfx.set("CH");
			cltrelnrec.forecoy.set(payrIO.getChdrcoy());
			wsaaChdrnum.set(payrIO.getChdrnum());
			wsaaPayrseqno.set(payrIO.getPayrseqno());
			cltrelnrec.forenum.set(wsaaForenum);
			cltrelnrec.clntpfx.set("CN");
			cltrelnrec.clntcoy.set(wsspcomn.fsuco);
			cltrelnrec.clntnum.set(sv.payrnum);
			cltrelnrec.function.set("ADD  ");
			callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
			if (isNE(cltrelnrec.statuz, varcom.oK)) {
				syserrrec.syserrStatuz.set(cltrelnrec.statuz);
				fatalError600();
			}
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		scrnparams.function.set("HIDEW");
		/*EXIT*/
	}
}
