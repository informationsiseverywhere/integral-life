package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:03
 * Description:
 * Copybook name: LEXTAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lextafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lextafiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lextafiKey = new FixedLengthStringData(64).isAPartOf(lextafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData lextafiChdrcoy = new FixedLengthStringData(1).isAPartOf(lextafiKey, 0);
  	public FixedLengthStringData lextafiChdrnum = new FixedLengthStringData(8).isAPartOf(lextafiKey, 1);
  	public FixedLengthStringData lextafiLife = new FixedLengthStringData(2).isAPartOf(lextafiKey, 9);
  	public FixedLengthStringData lextafiCoverage = new FixedLengthStringData(2).isAPartOf(lextafiKey, 11);
  	public FixedLengthStringData lextafiRider = new FixedLengthStringData(2).isAPartOf(lextafiKey, 13);
  	public PackedDecimalData lextafiSeqnbr = new PackedDecimalData(3, 0).isAPartOf(lextafiKey, 15);
  	public PackedDecimalData lextafiTranno = new PackedDecimalData(5, 0).isAPartOf(lextafiKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(lextafiKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lextafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lextafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}