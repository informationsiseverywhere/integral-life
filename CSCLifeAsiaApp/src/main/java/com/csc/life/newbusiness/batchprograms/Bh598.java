/*
 * File: Bh598.java
 * Date: 29 August 2009 21:37:11
 * Author: Quipoz Limited
 * 
 * Class transformed from BH598.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HchrmpsTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.HptnmpsTableDAM;
import com.csc.life.newbusiness.dataaccess.HttapfTableDAM;
import com.csc.life.newbusiness.tablestructures.Th596rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*    Monthly New Business Turnarounds Splitter Extract
*    =================================================
*
*    This program is aimed for extract all the relevant
*    information required to produce the Monthly Totalling New
*    Business Turnarounds report. The extraction is mainly
*    based on PTRN, CHDR, FLUPLNB files and also TH596
*    as selection criteria. The extracted information are then
*    written to temporary files, HTTAxx.
*
*
*****************************************************************
* </pre>
*/
public class Bh598 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private HttapfTableDAM httapf = new HttapfTableDAM();
	private DiskFileDAM htta01 = new DiskFileDAM("HTTA01");
	private DiskFileDAM htta02 = new DiskFileDAM("HTTA02");
	private DiskFileDAM htta03 = new DiskFileDAM("HTTA03");
	private DiskFileDAM htta04 = new DiskFileDAM("HTTA04");
	private DiskFileDAM htta05 = new DiskFileDAM("HTTA05");
	private DiskFileDAM htta06 = new DiskFileDAM("HTTA06");
	private DiskFileDAM htta07 = new DiskFileDAM("HTTA07");
	private DiskFileDAM htta08 = new DiskFileDAM("HTTA08");
	private DiskFileDAM htta09 = new DiskFileDAM("HTTA09");
	private DiskFileDAM htta10 = new DiskFileDAM("HTTA10");
	private DiskFileDAM htta11 = new DiskFileDAM("HTTA11");
	private DiskFileDAM htta12 = new DiskFileDAM("HTTA12");
	private DiskFileDAM htta13 = new DiskFileDAM("HTTA13");
	private DiskFileDAM htta14 = new DiskFileDAM("HTTA14");
	private DiskFileDAM htta15 = new DiskFileDAM("HTTA15");
	private DiskFileDAM htta16 = new DiskFileDAM("HTTA16");
	private DiskFileDAM htta17 = new DiskFileDAM("HTTA17");
	private DiskFileDAM htta18 = new DiskFileDAM("HTTA18");
	private DiskFileDAM htta19 = new DiskFileDAM("HTTA19");
	private DiskFileDAM htta20 = new DiskFileDAM("HTTA20");
	private FixedLengthStringData htta01Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta02Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta03Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta04Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta05Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta06Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta07Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta08Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta09Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta10Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta11Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta12Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta13Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta14Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta15Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta16Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta17Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta18Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta19Rec = new FixedLengthStringData(27);
	private FixedLengthStringData htta20Rec = new FixedLengthStringData(27);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH598");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaPrintFlag = new FixedLengthStringData(1);
	private Validator printReady = new Validator(wsaaPrintFlag, "Y");
	private Validator printNotReady = new Validator(wsaaPrintFlag, "N");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaHttaFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHttaFn, 0, FILLER).init("HTTA");
	private FixedLengthStringData wsaaHttaRunid = new FixedLengthStringData(2).isAPartOf(wsaaHttaFn, 4);
	private ZonedDecimalData wsaaHttaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHttaFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTrcodeFound = new FixedLengthStringData(1);
	private Validator trcodeFound = new Validator(wsaaTrcodeFound, "Y");

	private FixedLengthStringData wsaaStatcodeFound = new FixedLengthStringData(1);
	private Validator statcodeFound = new Validator(wsaaStatcodeFound, "Y");

	private FixedLengthStringData wsaaFupcodeFound = new FixedLengthStringData(1);
	private Validator fupcodeFound = new Validator(wsaaFupcodeFound, "Y");

	private FixedLengthStringData wsaaMppCertMatch = new FixedLengthStringData(1);
	private Validator mppCertMatch = new Validator(wsaaMppCertMatch, "Y");

	private FixedLengthStringData wsaaCnttypeMatch = new FixedLengthStringData(1);
	private Validator cnttypeMatch = new Validator(wsaaCnttypeMatch, "Y");

	private FixedLengthStringData wsaaFirstTimeFup = new FixedLengthStringData(1);
	private Validator firstTimeFup = new Validator(wsaaFirstTimeFup, "Y");
	private ZonedDecimalData wsaaW = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaRiskStatusComp = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaFupcodeComp = new FixedLengthStringData(3);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTh596Sub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaMthStart = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaMthEnd = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaPendForOtherRow = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNoFupRow = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsbbEffectiveDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaEffectiveDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaEffYr = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0).setUnsigned();
	private ZonedDecimalData wsaaEffMth = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4).setUnsigned();
	private ZonedDecimalData wsaaEffDay = new ZonedDecimalData(2, 0).isAPartOf(filler3, 6).setUnsigned();

	private FixedLengthStringData wsaaTh596 = new FixedLengthStringData(12760);
	private FixedLengthStringData[] wsaaStoreTh596 = FLSArrayPartOfStructure(40, 319, wsaaTh596, 0);
	private FixedLengthStringData[] wsaaRowId = FLSDArrayPartOfArrayStructure(2, wsaaStoreTh596, 0);
	private FixedLengthStringData[] wsaaTh596Hcondate = FLSDArrayPartOfArrayStructure(1, wsaaStoreTh596, 2);
	private FixedLengthStringData[][] wsaaTh596Trcode = FLSDArrayPartOfArrayStructure(28, 4, wsaaStoreTh596, 3);
	private FixedLengthStringData[][] wsaaTh596CnRiskStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaStoreTh596, 115);
	private FixedLengthStringData[][] wsaaTh596Fupcode = FLSDArrayPartOfArrayStructure(60, 3, wsaaStoreTh596, 139);
	private int wsaaTh596TrcodeNo = 28;
	private int wsaaTh596StatcodeNo = 12;
	private int wsaaTh596FupcodeNo = 60;

	private FixedLengthStringData wsaaFupcodeArray = new FixedLengthStringData(180);
	private FixedLengthStringData[] wsaaFupcodePr = FLSArrayPartOfStructure(60, 3, wsaaFupcodeArray, 0);
	private FixedLengthStringData wsaaFupcode = new FixedLengthStringData(3);
		/* ERRORS */
	private String ivrm = "IVRM";
	private String hpadrec = "HPADREC";
	private String itemrec = "ITEMREC";
	private String hchrmpsrec = "HCHRMPSREC";
	private String hptnmpsrec = "HPTNMPSREC";
	private String ptrnrevrec = "PTRNREVREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String fluplnbrec = "FLUPLNBREC";
		/* TABLES */
	private String th596 = "TH596";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Follow-ups - life new business*/
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
		/*Contract Details By Status*/
	private HchrmpsTableDAM hchrmpsIO = new HchrmpsTableDAM();
		/*Contract Additional Details*/
	private HpadTableDAM hpadIO = new HpadTableDAM();
		/*Policy Transaction By Effective Date*/
	private HptnmpsTableDAM hptnmpsIO = new HptnmpsTableDAM();
		/*TEMP MTHLY NB TURNAROUND EXTRACT FILE*/
	private HttapfTableDAM httapfData = new HttapfTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*TEMP MTHLY NB TURNAROUND EXTRACT FILE*/
	private HttapfTableDAM payrpfData = new HttapfTableDAM();
		/*Policy transaction history for reversals*/
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private Th596rec th596rec = new Th596rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next1280, 
		exit1290, 
		exit3090, 
		z110Start, 
		z180Next, 
		z190Exit, 
		z310Start, 
		z380Next, 
		z390Exit, 
		y280Next, 
		y290Exit, 
		w280Next, 
		w290Exit
	}

	public Bh598() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		iy.set(1);
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		wsbbEffectiveDate.set(bsscIO.getEffectiveDate());
		wsaaEffDay.set(1);
		wsaaMthStart.set(wsaaEffectiveDate);
		if (isEQ(wsaaEffMth,12)) {
			wsaaEffYr.add(1);
			wsaaEffMth.set(1);
		}
		else {
			wsaaEffMth.add(1);
		}
		datcon1rec.intDate.set(wsaaEffectiveDate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		datcon2rec.intDate1.set(datcon1rec.intDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaMthEnd.set(datcon2rec.intDate2);
		wsaaHttaRunid.set(bprdIO.getSystemParam04());
		wsaaHttaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		hptnmpsIO.setParams(SPACES);
		hptnmpsIO.setChdrcoy(bsprIO.getCompany());
		hptnmpsIO.setPtrneff(wsaaMthEnd);
		hptnmpsIO.setTranno(99999);
		hptnmpsIO.setFormat(hptnmpsrec);
		hptnmpsIO.setFunction(varcom.begn);
		hchrmpsIO.setParams(SPACES);
		hchrmpsIO.setChdrcoy(bsprIO.getCompany());
		hchrmpsIO.setFormat(hchrmpsrec);
		hchrmpsIO.setFunction(varcom.begn);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th596);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			readTh5961200();
		}
		
		y400SpecialCases();
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaHttaFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set("CLRTMPF");
			fatalError600();
		}
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(HTTA");
		stringVariable1.append(iz.toString());
		stringVariable1.append(") TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaHttaFn.toString());
		stringVariable1.append(") MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		stringVariable1.append(" SEQONLY(*YES 1000)");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz,1)) {
			htta01.openOutput();
		}
		if (isEQ(iz,2)) {
			htta02.openOutput();
		}
		if (isEQ(iz,3)) {
			htta03.openOutput();
		}
		if (isEQ(iz,4)) {
			htta04.openOutput();
		}
		if (isEQ(iz,5)) {
			htta05.openOutput();
		}
		if (isEQ(iz,6)) {
			htta06.openOutput();
		}
		if (isEQ(iz,7)) {
			htta07.openOutput();
		}
		if (isEQ(iz,8)) {
			htta08.openOutput();
		}
		if (isEQ(iz,9)) {
			htta09.openOutput();
		}
		if (isEQ(iz,10)) {
			htta10.openOutput();
		}
		if (isEQ(iz,11)) {
			htta11.openOutput();
		}
		if (isEQ(iz,12)) {
			htta12.openOutput();
		}
		if (isEQ(iz,13)) {
			htta13.openOutput();
		}
		if (isEQ(iz,14)) {
			htta14.openOutput();
		}
		if (isEQ(iz,15)) {
			htta15.openOutput();
		}
		if (isEQ(iz,16)) {
			htta16.openOutput();
		}
		if (isEQ(iz,17)) {
			htta17.openOutput();
		}
		if (isEQ(iz,18)) {
			htta18.openOutput();
		}
		if (isEQ(iz,19)) {
			htta19.openOutput();
		}
		if (isEQ(iz,20)) {
			htta20.openOutput();
		}
	}

protected void readTh5961200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1210();
				}
				case next1280: {
					next1280();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1210()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),th596)) {
			itemIO.setStatuz(varcom.endp);
			wsaaTh596Sub.set(wsaaSub);
			goTo(GotoLabel.exit1290);
		}
		if (isNE(subString(itemIO.getItemitem(), 1, 5),wsaaProg)) {
			goTo(GotoLabel.next1280);
		}
		th596rec.th596Rec.set(itemIO.getGenarea());
		wsaaSub.add(1);
		wsaaRowId[wsaaSub.toInt()].set(subString(itemIO.getItemitem(), 6, 2));
		wsaaTh596Hcondate[wsaaSub.toInt()].set(th596rec.hcondate);
		wsaaX.set(0);
		for (wsaaY.set(1); !(isGT(wsaaY,wsaaTh596TrcodeNo)); wsaaY.add(1)){
			if (isNE(th596rec.trcode[wsaaY.toInt()],SPACES)) {
				wsaaX.add(1);
				wsaaTh596Trcode[wsaaSub.toInt()][wsaaX.toInt()].set(th596rec.trcode[wsaaY.toInt()]);
			}
		}
		wsaaX.set(0);
		for (wsaaY.set(1); !(isGT(wsaaY,wsaaTh596StatcodeNo)); wsaaY.add(1)){
			if (isNE(th596rec.cnRiskStat[wsaaY.toInt()],SPACES)) {
				wsaaX.add(1);
				wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaX.toInt()].set(th596rec.cnRiskStat[wsaaY.toInt()]);
			}
		}
		wsaaX.set(0);
		for (wsaaY.set(1); !(isGT(wsaaY,wsaaTh596FupcodeNo)); wsaaY.add(1)){
			if (isNE(th596rec.fupcdes[wsaaY.toInt()],SPACES)) {
				wsaaX.add(1);
				wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()].set(th596rec.fupcdes[wsaaY.toInt()]);
			}
		}
	}

protected void next1280()
	{
		itemIO.setFunction(varcom.nextr);
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		printReady.setTrue();
		if (isEQ(hptnmpsIO.getStatuz(),varcom.endp)) {
			z300ProcessHchrmps();
		}
		else {
			z100ProcessHptnmps();
		}
		if (isEQ(hptnmpsIO.getStatuz(),varcom.endp)
		&& isEQ(hchrmpsIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
		}
		else {
			wsspEdterror.set(varcom.oK);
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			update3010();
		}
		catch (GOTOException e){
		}
	}

protected void update3010()
	{
		if (printNotReady.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaPrevChdrnum,SPACES)) {
			wsaaPrevChdrnum.set(httapfData.chdrnum);
		}
		if (isNE(httapfData.chdrnum,wsaaPrevChdrnum)) {
			iy.add(1);
			wsaaPrevChdrnum.set(httapfData.chdrnum);
		}
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(wsaaPrevChdrnum,SPACES)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(iy,1)) {
			htta01.write(httapfData);
		}
		if (isEQ(iy,2)) {
			htta02.write(httapfData);
		}
		if (isEQ(iy,3)) {
			htta03.write(httapfData);
		}
		if (isEQ(iy,4)) {
			htta04.write(httapfData);
		}
		if (isEQ(iy,5)) {
			htta05.write(httapfData);
		}
		if (isEQ(iy,6)) {
			htta06.write(httapfData);
		}
		if (isEQ(iy,7)) {
			htta07.write(httapfData);
		}
		if (isEQ(iy,8)) {
			htta08.write(httapfData);
		}
		if (isEQ(iy,9)) {
			htta09.write(httapfData);
		}
		if (isEQ(iy,10)) {
			htta10.write(httapfData);
		}
		if (isEQ(iy,11)) {
			htta11.write(httapfData);
		}
		if (isEQ(iy,12)) {
			htta12.write(httapfData);
		}
		if (isEQ(iy,13)) {
			htta13.write(httapfData);
		}
		if (isEQ(iy,14)) {
			htta14.write(httapfData);
		}
		if (isEQ(iy,15)) {
			htta15.write(httapfData);
		}
		if (isEQ(iy,16)) {
			htta16.write(httapfData);
		}
		if (isEQ(iy,17)) {
			htta17.write(httapfData);
		}
		if (isEQ(iy,18)) {
			htta18.write(httapfData);
		}
		if (isEQ(iy,19)) {
			htta19.write(httapfData);
		}
		if (isEQ(iy,20)) {
			htta20.write(httapfData);
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			htta01.close();
		}
		if (isEQ(iz,2)) {
			htta02.close();
		}
		if (isEQ(iz,3)) {
			htta03.close();
		}
		if (isEQ(iz,4)) {
			htta04.close();
		}
		if (isEQ(iz,5)) {
			htta05.close();
		}
		if (isEQ(iz,6)) {
			htta06.close();
		}
		if (isEQ(iz,7)) {
			htta07.close();
		}
		if (isEQ(iz,8)) {
			htta08.close();
		}
		if (isEQ(iz,9)) {
			htta09.close();
		}
		if (isEQ(iz,10)) {
			htta10.close();
		}
		if (isEQ(iz,11)) {
			htta11.close();
		}
		if (isEQ(iz,12)) {
			htta12.close();
		}
		if (isEQ(iz,13)) {
			htta13.close();
		}
		if (isEQ(iz,14)) {
			htta14.close();
		}
		if (isEQ(iz,15)) {
			htta15.close();
		}
		if (isEQ(iz,16)) {
			htta16.close();
		}
		if (isEQ(iz,17)) {
			htta17.close();
		}
		if (isEQ(iz,18)) {
			htta18.close();
		}
		if (isEQ(iz,19)) {
			htta19.close();
		}
		if (isEQ(iz,20)) {
			htta20.close();
		}
	}

protected void z100ProcessHptnmps()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case z110Start: {
					z110Start();
				}
				case z180Next: {
					z180Next();
				}
				case z190Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void z110Start()
	{
		SmartFileCode.execute(appVars, hptnmpsIO);
		if (isNE(hptnmpsIO.getStatuz(),varcom.oK)
		&& isNE(hptnmpsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hptnmpsIO.getParams());
			fatalError600();
		}
		if (isEQ(hptnmpsIO.getStatuz(),varcom.endp)
		|| isNE(hptnmpsIO.getChdrcoy(),bsprIO.getCompany())
		|| isLT(hptnmpsIO.getPtrneff(),wsaaMthStart)) {
			printNotReady.setTrue();
			hptnmpsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.z190Exit);
		}
		hptnmpsIO.setFunction(varcom.nextr);
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(hptnmpsIO.getChdrcoy());
		chdrlifIO.setChdrnum(hptnmpsIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		datcon3rec.freqFactor.set(0);
		wsaaTrcodeFound.set(SPACES);
		firstTimeFup.setTrue();
		for (wsaaSub.set(1); !(trcodeFound.isTrue()
		|| (isGT(wsaaSub,wsaaTh596Sub))); wsaaSub.add(1)){
			for (wsaaY.set(1); !(trcodeFound.isTrue()
			|| (isGT(wsaaY,wsaaTh596TrcodeNo))
			|| (isEQ(wsaaTh596Trcode[wsaaSub.toInt()][wsaaY.toInt()],SPACES))); wsaaY.add(1)){
				if (isEQ(hptnmpsIO.getBatctrcde(),wsaaTh596Trcode[wsaaSub.toInt()][wsaaY.toInt()])) {
					if (isEQ(wsaaRowId[wsaaSub.toInt()],"L")
					&& isEQ(chdrlifIO.getValidflag(),"1")) {
						goTo(GotoLabel.z180Next);
					}
					if (isEQ(wsaaTh596Hcondate[wsaaSub.toInt()],"Y")) {
						if (firstTimeFup.isTrue()) {
							datcon3rec.datcon3Rec.set(SPACES);
							datcon3rec.intDate2.set(hptnmpsIO.getPtrneff());
							ptrnrevIO.setParams(SPACES);
							ptrnrevIO.setChdrcoy(hptnmpsIO.getChdrcoy());
							ptrnrevIO.setChdrnum(hptnmpsIO.getChdrnum());
							z200Duration();
						}
						z400FupChk();
					}
					else {
						trcodeFound.setTrue();
					}
				}
			}
		}
		compute(wsaaSub, 0).set(sub(wsaaSub,1));
		if (isEQ(wsaaTrcodeFound,SPACES)) {
			goTo(GotoLabel.z180Next);
		}
		httapfData.chdrcoy.set(chdrlifIO.getChdrcoy());
		httapfData.chdrnum.set(chdrlifIO.getChdrnum());
		httapfData.cntbranch.set(chdrlifIO.getCntbranch());
		httapfData.agntnum.set(chdrlifIO.getAgntnum());
		httapfData.cnttype.set(chdrlifIO.getCnttype());
		httapfData.hrow.set(wsaaRowId[wsaaSub.toInt()]);
		httapfData.daysvrnce.set(datcon3rec.freqFactor);
		goTo(GotoLabel.z190Exit);
	}

protected void z180Next()
	{
		goTo(GotoLabel.z110Start);
	}

protected void z200Duration()
	{
		z210Start();
	}

protected void z210Start()
	{
		ptrnrevIO.setTranno(1);
		ptrnrevIO.setFormat(ptrnrevrec);
		ptrnrevIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		datcon3rec.freqFactor.set(0);
		datcon3rec.intDate1.set(ptrnrevIO.getPtrneff());
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isEQ(datcon3rec.freqFactor,0)) {
			datcon3rec.freqFactor.set(1);
		}
		w100FindFup();
		wsaaFirstTimeFup.set("N");
	}

protected void z300ProcessHchrmps()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case z310Start: {
					z310Start();
				}
				case z380Next: {
					z380Next();
				}
				case z390Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void z310Start()
	{
		SmartFileCode.execute(appVars, hchrmpsIO);
		if (isNE(hchrmpsIO.getStatuz(),varcom.oK)
		&& isNE(hchrmpsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hchrmpsIO.getParams());
			fatalError600();
		}
		if (isEQ(hchrmpsIO.getStatuz(),varcom.endp)
		|| isNE(hchrmpsIO.getChdrcoy(),bsprIO.getCompany())) {
			printNotReady.setTrue();
			hchrmpsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.z390Exit);
		}
		hchrmpsIO.setFunction(varcom.nextr);
		ptrnrevIO.setParams(SPACES);
		ptrnrevIO.setChdrcoy(hchrmpsIO.getChdrcoy());
		ptrnrevIO.setChdrnum(hchrmpsIO.getChdrnum());
		ptrnrevIO.setTranno(1);
		ptrnrevIO.setFormat(ptrnrevrec);
		ptrnrevIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(),varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(ptrnrevIO.getStatuz());
			syserrrec.params.set(ptrnrevIO.getParams());
			fatalError600();
		}
		if (isEQ(ptrnrevIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.z380Next);
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.freqFactor.set(0);
		datcon3rec.intDate1.set(ptrnrevIO.getPtrneff());
		datcon3rec.intDate2.set(wsbbEffectiveDate);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isEQ(datcon3rec.freqFactor,0)) {
			datcon3rec.freqFactor.set(1);
		}
		wsaaStatcodeFound.set(SPACES);
		wsaaFupcodeFound.set(SPACES);
		firstTimeFup.setTrue();
		for (wsaaSub.set(1); !((isGT(wsaaSub,wsaaTh596Sub))
		|| (statcodeFound.isTrue()
		&& fupcodeFound.isTrue())); wsaaSub.add(1)){
			for (wsaaY.set(1); !((isGT(wsaaY,wsaaTh596StatcodeNo))
			|| (isEQ(wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()],SPACES))
			|| statcodeFound.isTrue()); wsaaY.add(1)){
				if (isEQ(hchrmpsIO.getStatcode(),wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()])) {
					statcodeFound.setTrue();
				}
			}
			if (statcodeFound.isTrue()) {
				if (firstTimeFup.isTrue()) {
					y100FindFlup();
					wsaaFirstTimeFup.set("N");
					if (isEQ(wsaaFupcode,SPACES)) {
						hpadIO.setParams(SPACES);
						hpadIO.setChdrcoy(hchrmpsIO.getChdrcoy());
						hpadIO.setChdrnum(hchrmpsIO.getChdrnum());
						hpadIO.setFormat(hpadrec);
						hpadIO.setFunction(varcom.readr);
						SmartFileCode.execute(appVars, hpadIO);
						if (isNE(hpadIO.getStatuz(),varcom.oK)
						&& isNE(hpadIO.getStatuz(),varcom.mrnf)) {
							syserrrec.statuz.set(hpadIO.getStatuz());
							syserrrec.params.set(hpadIO.getParams());
							fatalError600();
						}
						if (isEQ(hpadIO.getHuwdcdte(),varcom.vrcmMaxDate)) {
							fupcodeFound.setTrue();
						}
					}
					else {
						wsaaFupcodeFound.set(SPACES);
					}
				}
				if (isEQ(wsaaFupcodeFound,SPACES)) {
					y300FlupCompare();
				}
			}
		}
		compute(wsaaSub, 0).set(sub(wsaaSub,1));
		if (isEQ(wsaaStatcodeFound,SPACES)
		|| isEQ(wsaaFupcodeFound,SPACES)) {
			goTo(GotoLabel.z380Next);
		}
		httapfData.chdrcoy.set(hchrmpsIO.getChdrcoy());
		httapfData.chdrnum.set(hchrmpsIO.getChdrnum());
		httapfData.cntbranch.set(hchrmpsIO.getCntbranch());
		httapfData.agntnum.set(hchrmpsIO.getAgntnum());
		httapfData.cnttype.set(hchrmpsIO.getCnttype());
		httapfData.hrow.set(wsaaRowId[wsaaSub.toInt()]);
		httapfData.daysvrnce.set(datcon3rec.freqFactor);
		goTo(GotoLabel.z390Exit);
	}

protected void z380Next()
	{
		goTo(GotoLabel.z310Start);
	}

protected void z400FupChk()
	{
		/*Z410-CHK*/
		if (isEQ(wsaaFupcode,SPACES)
		&& isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][1],SPACES)) {
			trcodeFound.setTrue();
		}
		if (isNE(wsaaFupcode,SPACES)
		&& isNE(wsaaTh596Fupcode[wsaaSub.toInt()][1],SPACES)) {
			trcodeFound.setTrue();
		}
		/*Z490-EXIT*/
	}

protected void y100FindFlup()
	{
		/*Y110-FLUP*/
		initialize(wsaaFupcodeArray);
		wsaaZ.set(0);
		wsaaFupcode.set(SPACES);
		fluplnbIO.setParams(SPACES);
		fluplnbIO.setChdrcoy(hchrmpsIO.getChdrcoy());
		fluplnbIO.setChdrnum(hchrmpsIO.getChdrnum());
		fluplnbIO.setFupno(ZERO);
		fluplnbIO.setFormat(fluplnbrec);
		fluplnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluplnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluplnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(fluplnbIO.getStatuz(),varcom.endp))) {
			y200LoopAroundFlups();
		}
		
		/*Y190-EXIT*/
	}

protected void y200LoopAroundFlups()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					y210Loop();
				}
				case y280Next: {
					y280Next();
				}
				case y290Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void y210Loop()
	{
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)
		&& isNE(fluplnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		if (isNE(fluplnbIO.getChdrcoy(),hchrmpsIO.getChdrcoy())
		|| isNE(fluplnbIO.getChdrnum(),hchrmpsIO.getChdrnum())
		|| isEQ(fluplnbIO.getStatuz(),varcom.endp)) {
			fluplnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.y290Exit);
		}
		if (isEQ(fluplnbIO.getFupstat(),"R")
		|| isEQ(fluplnbIO.getFupstat(),"W")) {
			goTo(GotoLabel.y280Next);
		}
		wsaaZ.add(1);
		wsaaFupcodePr[wsaaZ.toInt()].set(fluplnbIO.getFupcode());
		wsaaFupcode.set(fluplnbIO.getFupcode());
	}

protected void y280Next()
	{
		fluplnbIO.setFunction(varcom.nextr);
	}

protected void y300FlupCompare()
	{
		/*Y310-FLUP*/
		wsaaFupcodeFound.set(SPACES);
		for (wsaaW.set(1); !((isGT(wsaaW,wsaaZ))
		|| fupcodeFound.isTrue()); wsaaW.add(1)){
			for (wsaaX.set(1); !((isGT(wsaaX,wsaaTh596FupcodeNo))
			|| (isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()],SPACES))
			|| fupcodeFound.isTrue()); wsaaX.add(1)){
				if (isEQ(wsaaFupcodePr[wsaaW.toInt()],wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()])) {
					fupcodeFound.setTrue();
				}
			}
		}
		if (isEQ(wsaaFupcodeFound,SPACES)) {
			if (isEQ(wsaaSub,wsaaTh596Sub)) {
				fupcodeFound.setTrue();
			}
			else {
				wsaaStatcodeFound.set(SPACES);
			}
		}
		/*Y390-EXIT*/
	}

protected void y400SpecialCases()
	{
		y410Sp();
	}

protected void y410Sp()
	{
		wsaaStatcodeFound.set(SPACES);
		wsaaNoFupRow.set(0);
		wsaaPendForOtherRow.set(0);
		wsaaRiskStatusComp.set(bprdIO.getSystemParam01());
		wsaaFupcodeComp.set(bprdIO.getSystemParam03());
		for (wsaaSub.set(1); !(isGT(wsaaSub,wsaaTh596Sub)
		|| (isGT(wsaaNoFupRow,0)
		&& isGT(wsaaPendForOtherRow,0))); wsaaSub.add(1)){
			for (wsaaY.set(1); !(isGT(wsaaY,wsaaTh596StatcodeNo)
			|| isEQ(wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()],SPACES)
			|| statcodeFound.isTrue()); wsaaY.add(1)){
				if (isEQ(wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()],wsaaRiskStatusComp)) {
					statcodeFound.setTrue();
				}
			}
			if (statcodeFound.isTrue()) {
				if (isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][1],SPACES)) {
					wsaaNoFupRow.set(wsaaSub);
				}
				else {
					for (wsaaX.set(1); !(isGT(wsaaX,wsaaTh596FupcodeNo)
					|| isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()],SPACES)
					|| isGT(wsaaPendForOtherRow,0)); wsaaX.add(1)){
						if (isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()],wsaaFupcodeComp)) {
							wsaaPendForOtherRow.set(wsaaSub);
						}
					}
					if (isEQ(wsaaPendForOtherRow,0)) {
						wsaaStatcodeFound.set(SPACES);
					}
				}
			}
		}
	}

protected void w100FindFup()
	{
		/*W110-FLUP*/
		wsaaFupcode.set(SPACES);
		fluplnbIO.setParams(SPACES);
		fluplnbIO.setChdrcoy(chdrlifIO.getChdrcoy());
		fluplnbIO.setChdrnum(chdrlifIO.getChdrnum());
		fluplnbIO.setFupno(ZERO);
		fluplnbIO.setFormat(fluplnbrec);
		fluplnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluplnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluplnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(fluplnbIO.getStatuz(),varcom.endp))) {
			w200LoopAroundFlups();
		}
		
		/*W190-EXIT*/
	}

protected void w200LoopAroundFlups()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					w210Loop();
				}
				case w280Next: {
					w280Next();
				}
				case w290Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void w210Loop()
	{
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)
		&& isNE(fluplnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		if (isNE(fluplnbIO.getChdrcoy(),chdrlifIO.getChdrcoy())
		|| isNE(fluplnbIO.getChdrnum(),chdrlifIO.getChdrnum())
		|| isNE(wsaaFupcode,SPACES)
		|| isEQ(fluplnbIO.getStatuz(),varcom.endp)) {
			fluplnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.w290Exit);
		}
		if (isEQ(fluplnbIO.getFupstat(),"R")
		|| isEQ(fluplnbIO.getFupstat(),"W")) {
			goTo(GotoLabel.w280Next);
		}
		wsaaFupcode.set(fluplnbIO.getFupcode());
	}

protected void w280Next()
	{
		fluplnbIO.setFunction(varcom.nextr);
	}
}
