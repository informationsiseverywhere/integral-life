package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5705
 * @version 1.0 generated on 30/08/09 06:50
 * @author Quipoz
 */
public class S5705ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(639);
	public FixedLengthStringData dataFields = new FixedLengthStringData(159).isAPartOf(dataArea, 0);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,13);
	public FixedLengthStringData campaign = DD.campaign.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,26);
	public ZonedDecimalData incomeSeqNo = DD.incseqno.copyToZonedDecimal().isAPartOf(dataFields,27);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,29);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields,37);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields,45);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData lrepnum = DD.lrepnum.copy().isAPartOf(dataFields,83);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,103);
	public FixedLengthStringData mortality = DD.mortality.copy().isAPartOf(dataFields,104);
	public ZonedDecimalData plansuff = DD.plansuff.copyToZonedDecimal().isAPartOf(dataFields,105);
	public FixedLengthStringData pursuits = new FixedLengthStringData(12).isAPartOf(dataFields, 107);
	public FixedLengthStringData[] pursuit = FLSArrayPartOfStructure(2, 6, pursuits, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(12).isAPartOf(pursuits, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01 = DD.pursuit.copy().isAPartOf(filler,0);
	public FixedLengthStringData pursuit02 = DD.pursuit.copy().isAPartOf(filler,6);
	public ZonedDecimalData rcdate = DD.rcdate.copyToZonedDecimal().isAPartOf(dataFields,119);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,127);
	public FixedLengthStringData reptype = DD.reptype.copy().isAPartOf(dataFields,130);
	public FixedLengthStringData selection = DD.selection.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData smkrqd = DD.smkrqd.copy().isAPartOf(dataFields,136);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(dataFields,137);
	public FixedLengthStringData stcal = DD.stcal.copy().isAPartOf(dataFields,139);
	public FixedLengthStringData stcbl = DD.stcbl.copy().isAPartOf(dataFields,142);
	public FixedLengthStringData stccl = DD.stccl.copy().isAPartOf(dataFields,145);
	public FixedLengthStringData stcdl = DD.stcdl.copy().isAPartOf(dataFields,148);
	public FixedLengthStringData stcel = DD.stcel.copy().isAPartOf(dataFields,151);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,154);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(120).isAPartOf(dataArea, 159);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData campaignErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData incseqnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lrepnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mortalityErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData plansuffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData pursuitsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] pursuitErr = FLSArrayPartOfStructure(2, 4, pursuitsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(pursuitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData pursuit02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData rcdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData reptypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData selectionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData smkrqdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData srcebusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData stcalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData stcblErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData stcclErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData stcdlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData stcelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(360).isAPartOf(dataArea, 279);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] campaignOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] incseqnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lrepnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mortalityOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] plansuffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData pursuitsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] pursuitOut = FLSArrayPartOfStructure(2, 12, pursuitsOut, 0);
	public FixedLengthStringData[][] pursuitO = FLSDArrayPartOfArrayStructure(12, 1, pursuitOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(24).isAPartOf(pursuitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pursuit01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] pursuit02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] rcdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] reptypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] selectionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] smkrqdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] srcebusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] stcalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] stcblOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] stcclOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] stcdlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] stcelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);
	public FixedLengthStringData rcdateDisp = new FixedLengthStringData(10);

	public LongData S5705screenWritten = new LongData(0);
	public LongData S5705protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5705ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(cntcurrOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billcurrOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(registerOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntbranchOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(srcebusOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(campaignOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreqOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mopOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcdateOut,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntselOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reptypeOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcalOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcblOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcclOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcdlOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcelOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectionOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pursuit01Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pursuit02Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, itmfrm, itmto, cntcurr, billcurr, register, cntbranch, srcebus, incomeSeqNo, plansuff, campaign, billfreq, mop, rcdate, agntsel, reptype, lrepnum, stcal, stcbl, stccl, stcdl, stcel, selection, smkrqd, mortality, pursuit01, pursuit02};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut, cntcurrOut, billcurrOut, registerOut, cntbranchOut, srcebusOut, incseqnoOut, plansuffOut, campaignOut, billfreqOut, mopOut, rcdateOut, agntselOut, reptypeOut, lrepnumOut, stcalOut, stcblOut, stcclOut, stcdlOut, stcelOut, selectionOut, smkrqdOut, mortalityOut, pursuit01Out, pursuit02Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr, cntcurrErr, billcurrErr, registerErr, cntbranchErr, srcebusErr, incseqnoErr, plansuffErr, campaignErr, billfreqErr, mopErr, rcdateErr, agntselErr, reptypeErr, lrepnumErr, stcalErr, stcblErr, stcclErr, stcdlErr, stcelErr, selectionErr, smkrqdErr, mortalityErr, pursuit01Err, pursuit02Err};
		screenDateFields = new BaseData[] {itmfrm, itmto, rcdate};
		screenDateErrFields = new BaseData[] {itmfrmErr, itmtoErr, rcdateErr};
		screenDateDispFields = new BaseData[] {itmfrmDisp, itmtoDisp, rcdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5705screen.class;
		protectRecord = S5705protect.class;
	}

}
