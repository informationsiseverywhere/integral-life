package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr5aiscreen extends ScreenRecord{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}
	public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sr5aiScreenVars sv = (Sr5aiScreenVars) pv;
			clearInds(av, pfInds);
			write(lrec, sv.Sr5aiscreenWritten, null, av, null, ind2, ind3);
		}

		public static void read(Indicator ind2, Indicator ind3) {}

		public static void clearClassString(VarModel pv) {
			Sr5aiScreenVars screenVars = (Sr5aiScreenVars)pv;
			screenVars.screenRow.setClassString("");
			screenVars.screenColumn.setClassString("");
			screenVars.excda.setClassString("");
			screenVars.exadtxt.setClassString("");
			screenVars.excltxt.setClassString("");
				}

	/**
	 * Clear all the variables in Sr5aiscreen
	 */
		public static void clear(VarModel pv) {
			Sr5aiScreenVars screenVars = (Sr5aiScreenVars) pv;
			screenVars.screenRow.clear();
			screenVars.screenColumn.clear();
			screenVars.excda.clear();
			screenVars.exadtxt.clear();
			screenVars.excltxt.clear();
				}


}
