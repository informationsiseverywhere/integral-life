package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:45
 * Description:
 * Copybook name: HRATKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hratkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hratFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hratKey = new FixedLengthStringData(256).isAPartOf(hratFileKey, 0, REDEFINE);
  	public FixedLengthStringData hratItemitem = new FixedLengthStringData(8).isAPartOf(hratKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(248).isAPartOf(hratKey, 8, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hratFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hratFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}