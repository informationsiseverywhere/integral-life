package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:36
 * Description:
 * Copybook name: PAYRREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Payrrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData payrrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData payrrevKey = new FixedLengthStringData(64).isAPartOf(payrrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData payrrevChdrcoy = new FixedLengthStringData(1).isAPartOf(payrrevKey, 0);
  	public FixedLengthStringData payrrevChdrnum = new FixedLengthStringData(8).isAPartOf(payrrevKey, 1);
  	public PackedDecimalData payrrevPayrseqno = new PackedDecimalData(1, 0).isAPartOf(payrrevKey, 9);
  	public PackedDecimalData payrrevTranno = new PackedDecimalData(5, 0).isAPartOf(payrrevKey, 10);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(payrrevKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(payrrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		payrrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}