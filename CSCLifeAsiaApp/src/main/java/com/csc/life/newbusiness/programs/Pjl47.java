package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.life.newbusiness.screens.Sjl47ScreenVars;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pjl47 extends ScreenProgCS {
	private static final Logger LOGGER = LoggerFactory.getLogger(Pjl47.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pjl47");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private final ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private Tjl47rec tjl47rec = new Tjl47rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl47ScreenVars sv = ScreenProgram.getScreenVars( Sjl47ScreenVars.class);
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Itmdkey wsaaItmdkey = new Itmdkey();
	private Itempf itempf = new Itempf();
	Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Descpf descpf = new Descpf();
	
	public Pjl47() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl47", AppVars.getInstance(), sv);
	}
	
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			LOGGER.info("mainline:", e);
		}
	}

	@Override
	protected void initialise1000() {		
		initialise1010();
	}

	protected void initialise1010() {
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		wsaaItmdkey.set(wsspsmart.itmdkey);
		descpf=descDAO.getdescData("IT", wsaaItemkey.itemItemtabl.toString(), 
				wsaaItemkey.itemItemitem.toString(), wsaaItemkey.itemItemcoy.toString(), 
				wsspcomn.language.toString());
		if (descpf==null) {
			fatalError600();
		}
		sv.longdesc.set(descpf.getLongdesc());
			itempf = itempfDAO.findItemByItdm(wsaaItemkey.itemItemcoy.toString(), 
					wsaaItemkey.itemItemtabl.toString(),wsaaItemkey.itemItemitem.toString());
			if (itempf == null ) {
			fatalError600();
		}
		
		tjl47rec.tjl47Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isNE(tjl47rec.tjl47Rec, SPACES)) {
			sv.lincsubr.set(tjl47rec.lincsubr);
			sv.rectype01.set(tjl47rec.rectype01);
			sv.rectype02.set(tjl47rec.rectype02);
			sv.rectype03.set(tjl47rec.rectype03);
			sv.chgtype01.set(tjl47rec.chgtype01.toUpperCase());
			sv.chgtype02.set(tjl47rec.chgtype02.toUpperCase());
			sv.chgtype03.set(tjl47rec.chgtype03.toUpperCase());
		}
	}

	@Override
	protected void preScreenEdit() {
		preStart();
	}

	protected void preStart() {
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(Varcom.prot);
		}
		return;
	}

	@Override
	protected void screenEdit2000() {
		screenIo2010();
		exit2090();
	}

	protected void screenIo2010() {
		wsspcomn.edterror.set(Varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			exit2090();
		}
		/*OTHER*/
		if(isEQ(sv.lincsubr, SPACES)) {
			sv.lincsubrErr.set("E186");
		}
	}

	protected void exit2090() {
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	@Override
	protected void update3000()	{
		preparation3010();
		updatePrimaryRecord3050();
		updateRecord3055();
	}

	protected void preparation3010() {
		if (isEQ(wsspcomn.flag,"I")) {
			return;
		}
	}

	protected void updatePrimaryRecord3050() {
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itempf.setTranid(varcom.vrcmCompTranid.trim());
	}

	protected void updateRecord3055() {
		checkChanges();
		itempf.setGenarea(tjl47rec.tjl47Rec.toString().getBytes());
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemtabl(wsaaItemkey.itemItemtabl.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaItemkey.itemItemitem.toString());
		itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
				.getGENAREAJString(tjl47rec.tjl47Rec.toString().getBytes(), tjl47rec));
		itempfDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM");
		}
	
	protected void checkChanges() {
		if (isNE(sv.lincsubr,tjl47rec.lincsubr)) {
			tjl47rec.lincsubr.set(sv.lincsubr);
		}
		if (isNE(sv.rectype01,tjl47rec.rectype01)) {
			tjl47rec.rectype01.set(sv.rectype01);
		}
		if (isNE(sv.rectype02,tjl47rec.rectype02)) {
			tjl47rec.rectype02.set(sv.rectype02);
		}
		if (isNE(sv.rectype03,tjl47rec.rectype03)) {
			tjl47rec.rectype03.set(sv.rectype03);
		}
		if (isNE(sv.chgtype01.toUpperCase(),tjl47rec.chgtype01.toUpperCase())) {
			tjl47rec.chgtype01.set(sv.chgtype01.toUpperCase());
		}
		if (isNE(sv.chgtype02.toUpperCase(),tjl47rec.chgtype02.toUpperCase())) {
			tjl47rec.chgtype02.set(sv.chgtype02.toUpperCase());
		}
		if (isNE(sv.chgtype03.toUpperCase(),tjl47rec.chgtype03.toUpperCase())) {
			tjl47rec.chgtype03.set(sv.chgtype03.toUpperCase());
		}
	}

	@Override
	protected void whereNext4000() {
		/*NEXT-PROGRAM*/
		wsspcomn.nextprog.set(wsaaProg);
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
