package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * @author gsaluja2
 *
 */
public class Sjl67screen extends ScreenRecord {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 12, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sjl67ScreenVars sv = (Sjl67ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl67screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sjl67ScreenVars screenVars = (Sjl67ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.resultcd01.setClassString("");
		screenVars.resultcd02.setClassString("");
		screenVars.resultcd03.setClassString("");
		screenVars.resultcd04.setClassString("");
		screenVars.resultcd05.setClassString("");
		screenVars.resultcd06.setClassString("");
		screenVars.resultcd07.setClassString("");
		screenVars.resultcd08.setClassString("");
		screenVars.resultcd09.setClassString("");
		screenVars.rcveror01.setClassString("");
		screenVars.rcveror02.setClassString("");
		screenVars.rcveror03.setClassString("");
		screenVars.rcveror04.setClassString("");
		screenVars.rcveror05.setClassString("");
		screenVars.rcveror06.setClassString("");
		screenVars.rcveror07.setClassString("");
		screenVars.rcveror08.setClassString("");
		screenVars.rcveror09.setClassString("");
	}

/**
 * Clear all the variables in Sjl67screen
 */
	public static void clear(VarModel pv) {
		Sjl67ScreenVars screenVars = (Sjl67ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.resultcd01.clear();
		screenVars.resultcd02.clear();
		screenVars.resultcd03.clear();
		screenVars.resultcd04.clear();
		screenVars.resultcd05.clear();
		screenVars.resultcd06.clear();
		screenVars.resultcd07.clear();
		screenVars.resultcd08.clear();
		screenVars.resultcd09.clear();
		screenVars.rcveror01.clear();
		screenVars.rcveror02.clear();
		screenVars.rcveror03.clear();
		screenVars.rcveror04.clear();
		screenVars.rcveror05.clear();
		screenVars.rcveror06.clear();
		screenVars.rcveror07.clear();
		screenVars.rcveror08.clear();
		screenVars.rcveror09.clear();
	}
}
