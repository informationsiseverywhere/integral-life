package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:04
 * Description:
 * Copybook name: NBPISSUEI
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Nbpissuei extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(66);
  	public FixedLengthStringData messageHeader = new FixedLengthStringData(30).isAPartOf(rec, 0);
  	public FixedLengthStringData msgid = new FixedLengthStringData(10).isAPartOf(messageHeader, 0);
  	public ZonedDecimalData msglng = new ZonedDecimalData(5, 0).isAPartOf(messageHeader, 10).setUnsigned();
  	public ZonedDecimalData msgcnt = new ZonedDecimalData(5, 0).isAPartOf(messageHeader, 15).setUnsigned();
  	public FixedLengthStringData filler = new FixedLengthStringData(10).isAPartOf(messageHeader, 20, FILLER);
  	public FixedLengthStringData messageData = new FixedLengthStringData(36).isAPartOf(rec, 30);
  	public FixedLengthStringData bgenS5073 = new FixedLengthStringData(10).isAPartOf(messageData, 0);
  	public FixedLengthStringData bgenS5073Chdrsel = new FixedLengthStringData(10).isAPartOf(bgenS5073, 0);
  	public FixedLengthStringData bgenDatime = new FixedLengthStringData(26).isAPartOf(messageData, 10);
  	public FixedLengthStringData bgenChdrDatime = new FixedLengthStringData(26).isAPartOf(bgenDatime, 0);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}