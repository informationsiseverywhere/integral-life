package com.csc.life.newbusiness.dataaccess.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;
import java.math.BigDecimal;


public class Rlvrpf implements Serializable{
	
 	private long uniqueNumber;
	private String chdrcoy;
 	private String chdrnum;
 	private String life;
	private String coverage;
	private String rider;
 	private String memaccnum;
 	private String fndusinum;
 	private int srvstrtdt;
 	private BigDecimal taxfreeamt;
 	private BigDecimal kiwisvrcom;
 	private BigDecimal taxedamt;
	private BigDecimal untaxedamt;
 	private BigDecimal prsrvdamt;
 	private BigDecimal  kiwiamt;
 	private BigDecimal  unrestrict;
 	private BigDecimal  rstrict;
 	private BigDecimal  tottrfamt;
 	private String moneyind;
 	private String validFlag;
  	private int  usrprf;
 	private String jobnm;
 	private Date datime;
 	
 	public Rlvrpf() {
		chdrcoy = "";
		chdrnum = "";
		life="";
		coverage="";
		rider="";
		memaccnum = "";
		fndusinum = "";
		srvstrtdt = 0;
		taxfreeamt = BigDecimal.ZERO;
		kiwisvrcom = BigDecimal.ZERO;
		taxedamt = BigDecimal.ZERO;
		untaxedamt = BigDecimal.ZERO;
		prsrvdamt = BigDecimal.ZERO;
		kiwiamt = BigDecimal.ZERO;
		unrestrict = BigDecimal.ZERO;
		rstrict = BigDecimal.ZERO;
		tottrfamt = BigDecimal.ZERO;
		moneyind = "";
		validFlag = "";
		usrprf = 0;
		jobnm = "";
	}
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	//ILIFE-8123-starts 
	public String getLife() {
        return life;
    }
    public void setLife(String life) {
        this.life = life;
    }
    
    public String getCoverage() {
        return coverage;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public String getRider() {
        return rider;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
  //ILIFE-8123-ends
	public String getMemaccnum() {
		return memaccnum;
	}
	public void setMemaccnum(String memaccnum) {
		this.memaccnum = memaccnum;
	}
	public String getFndusinum() {
		return fndusinum;
	}
	public void setFndusinum(String fndusinum) {
		this.fndusinum = fndusinum;
	}
	public int getSrvstrtdt() {
		return srvstrtdt;
	}
	public void setSrvstrtdt(int srvstrtdt) {
		this.srvstrtdt = srvstrtdt;
	}
	public BigDecimal getTaxfreeamt() {
		return taxfreeamt;
	}
	public void setTaxfreeamt(BigDecimal taxfreeamt) {
		this.taxfreeamt = taxfreeamt;
	}
	public BigDecimal getKiwisvrcom() {
		return kiwisvrcom;
	}
	public void setKiwisvrcom(BigDecimal kiwisvrcom) {
		this.kiwisvrcom = kiwisvrcom;
	}
	public BigDecimal getTaxedamt() {
		return taxedamt;
	}
	public void setTaxedamt(BigDecimal taxedamt) {
		this.taxedamt = taxedamt;
	}
	public BigDecimal getUntaxedamt() {
		return untaxedamt;
	}
	public void setUntaxedamt(BigDecimal untaxedamt) {
		this.untaxedamt = untaxedamt;
	}
	public BigDecimal getPrsrvdamt() {
		return prsrvdamt;
	}
	public void setPrsrvdamt(BigDecimal prsrvdamt) {
		this.prsrvdamt = prsrvdamt;
	}
	public BigDecimal getKiwiamt() {
		return kiwiamt;
	}
	public void setKiwiamt(BigDecimal kiwiamt) {
		this.kiwiamt = kiwiamt;
	}
	public BigDecimal getUnrestrict() {
		return unrestrict;
	}
	public void setUnrestrict(BigDecimal unrestrict) {
		this.unrestrict = unrestrict;
	}
	public BigDecimal getRstrict() {
		return rstrict;
	}
	public void setRstrict(BigDecimal rstrict) {
		this.rstrict = rstrict;
	}
	public BigDecimal getTottrfamt() {
		return tottrfamt;
	}
	public void setTottrfamt(BigDecimal tottrfamt) {
		this.tottrfamt = tottrfamt;
	}
	public String getMoneyind() {
		return moneyind;
	}
	public void setMoneyind(String moneyind) {
		this.moneyind = moneyind;
	}
	
	public String getValidFlag() {
		return validFlag;
	}
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}
	public int getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(int usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
 	
}
