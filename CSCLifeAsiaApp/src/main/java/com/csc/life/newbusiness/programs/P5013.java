/*
 * File: P5013.java
 * Date: 29 August 2009 23:55:08
 * Author: Quipoz Limited
 *
 * Class transformed from P5013.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.procedures.UwQuestionnaireUtil;
import com.csc.life.contractservicing.procedures.UwoccupationUtil;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.screens.S5013ScreenVars;
import com.csc.life.newbusiness.tablestructures.T5657rec;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;
import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* This   screen/program   P5013   is   used   to   capture  the
* Special   Terms   (Options/Extras)  details  for  traditional
* generic components.
*
* Initialise
* ----------
*
* Retrieve  the  COVTTRM  record (RETRV) in order to access the
* Coverage/Rider  details,  as  these were previously read they
* should still be in storage.
*
* The COVTTRM key will be:-
*
*  Company, Contract-no, Life, Coverage-no, Rider-no.
*
* Look  up  the  description  of the coverage rider (DESC) from
* T5687.
*
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*  - read  the life details using LIFELNB (life number from
*       COVTTRM, joint life number '00').  Look up the name
*       from the  client  details  (CLTS)  and  format as a
*       "confirmation name".
*
*  - read the joint life details using LIFELNB (life number
*       from COVTTRM,  joint  life number '01').  If found,
*       look up the name from the client details (CLTS) and
*       format as a "confirmation name".
*
* Read  the  general  coverage/rider details from T5687 for the
* the coverage type held on COVTLNB. Access the version for the
* original commencement date of the risk.
*
* To determine which life the non-standard terms apply to decide
* whether or not it is  a Single or Joint-life case (if it is a
* joint  life  case,  the  joint  life record was found above).
* Then:
*
*  - if both lives are present and the Join-life indicator is
*       'N',  or if there is only a single life,  non-dispLAy
*       the life selection option.
*
*
* OPTIONS/EXTRAS DETAILS
*
* The  subfile  is  a  fixed  size  of 8 records. Initialise it
* first,  and then randomly access the required subfile records
* to  load  any  existing  details.  The  details are loaded as
* follows:
*
*  - read  each  LEXT  record (keyed: company, contract-no,
*       life-no, coverage, rider, sequence).
*
*  - if present,  retrieve  the  subfile  record  with  the
*       relative record  number of the record read.  Output
*       all   existing   values,   looking-up   the   short
*       description of the reason code from T5651. Write an
*       indicator to  a  hidden  field to identify that the
*       record  already  exists on the database. Output the
*       life  indicator  as follows; blank if joint life no
*       is blank, 'L' if '00', 'J' if '01'.
*
* Validation
* ----------
*
* If enquiry  mode  (the  WSSP-FLAG  =  'I'), protect all input
* capable fields.
*
* If enquiry mode or 'KILL' is requested, skip all validation.
*
* Edit each line  modified in the subfile according to the Help
* information.
*
* If  all the different rating options are left blank, retrieve
* the  default  from T5657 (key cover/rider-code, reason-code).
* Update  the  subfile  records  with these in case it is to be
* re-displayed.
*
* If 'CALC' was entered then re-display the screen.
*
* Updating
* --------
*
* If the WSSP-FLAG = 'I' (enquiry mode)  or the 'KILL' function
* key was pressed, then skip the updating.
*
* For each  subfile  record, update the options and extras file
* as follows:
*
*  - if the  record   was  loaded  from  the  database  (as
*       identified by  the  "hidden"  field above), and all
*       fields are now blank, delete the record.
*
*  - if the record  was  present  and  the  details are not
*       blank, read, update and re-write the record.
*
*  - if the record was not present, write a new one.
*
* The fields must be initialised as follows:
*
*       Valid flag - '3'
*       Transaction no - from CDHRLNB
*       Current from - from CDHRLNB
*       Current to - from CDHRLNB
*       Sub-standard indicator from T5657 (keyed as above)
*       Joint life - blank if 'Life flag' is blank, '00' if
*            life flag = L, '01' if life flag = J.
*
* Next Program
* ------------
*
*  - add one to the program pointer and exit.
*
*****************************************************************
* </pre>
*/
public class P5013 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5013");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaLifeSex = new FixedLengthStringData(1);

	protected FixedLengthStringData wsaaT5657key = new FixedLengthStringData(6);
	protected FixedLengthStringData wsaaT5657key1 = new FixedLengthStringData(4).isAPartOf(wsaaT5657key, 0).init(SPACES);
	protected FixedLengthStringData wsaaT5657key2 = new FixedLengthStringData(2).isAPartOf(wsaaT5657key, 4).init(SPACES);

	private FixedLengthStringData wsaaTh549Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTh549Crtable = new FixedLengthStringData(4).isAPartOf(wsaaTh549Key, 0);
	private ZonedDecimalData wsaaTh549Zmortpct = new ZonedDecimalData(3, 0).isAPartOf(wsaaTh549Key, 4).setUnsigned();
	private FixedLengthStringData wsaaTh549Zsexmort = new FixedLengthStringData(1).isAPartOf(wsaaTh549Key, 7);
	private ZonedDecimalData wsaaPremCessTerm = new ZonedDecimalData(2, 0);
	private ZonedDecimalData wsaaPremAgeTerm = new ZonedDecimalData(2, 0).setUnsigned();

	protected FixedLengthStringData wsaaEntryFound = new FixedLengthStringData(1);
	private Validator noEntryFound = new Validator(wsaaEntryFound, "N");
		/* ERRORS */
	private static final String e492 = "E492";
	private static final String f246 = "F246";
	private static final String g709 = "G709";
	protected static final String h039 = "H039";
	private static final String t040 = "T040";
	private static final String f239 = "F239";
	private static final String f307 = "F307";
	private static final String d032 = "D032";
	private static final String hl27 = "HL27";
		/* TABLES */
	protected static final String t5651 = "T5651";
	protected static final String t5657 = "T5657";
	private static final String t5687 = "T5687";
	private static final String th549 = "TH549";
	private static final String cltsrec = "CLTSREC";
	protected static final String lextrec = "LEXTREC";
	protected static final String itemrec = "ITEMREC";
	protected static final String descrec = "DESCREC";
	private String uwlvrec = "UWLVREC";
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	protected LextTableDAM lextIO = new LextTableDAM();
	protected LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	protected T5657rec t5657rec = new T5657rec();
	protected T5687rec t5687rec = new T5687rec();
	private Wssplife wssplife = new Wssplife();
	private S5013ScreenVars sv = getPScreenVars();
	private boolean uwFlag = false;
	private UwQuestionnaireUtil uwQuestionnaireUtil = getApplicationContext().getBean("uwQuestionnaireUtil", UwQuestionnaireUtil.class);
	private UwoccupationUtil uwoccupationUtil = getApplicationContext().getBean("uwoccupationUtil", UwoccupationUtil.class);
	private UWQuestionnaireRec uwQuestionnaireRec = null;
	private UWQuestionnaireRec uwQuestionnaireRecReturn =null;
	private UWOccupationRec uwOccupationRec = null;
	public static final String UWQUESTIONNAIRECACHE= "UWQUESTIONNAIRE";
	private boolean vpmsUpdateFlag = false; 
	private List<String> questions = null;
	private UndqpfDAO undqpfDAO = getApplicationContext().getBean("undqpfDAO", UndqpfDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	int index = 0;
	private static final String ACCEPTED = "Accepted";
	
 /**
 * Contains all possible labels used by goTo action.
 */
protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		lext1015,
		loadExistingLext1030,
		exit2490,
		lookUpDesc2505,
		updateErrorIndicators2520,
		updateLext3520,
		nextLine3580
	}

	public P5013() {
		super();
		screenVars = sv;
		new ScreenModel("S5013", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected S5013ScreenVars getPScreenVars()
{
	return ScreenProgram.getScreenVars(S5013ScreenVars.class);
}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ; 
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					initialise1010();
				case lext1015:
					lext1015();
				case loadExistingLext1030:
					loadExistingLext1030();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		sv.agerate.set(ZERO);
		sv.seqnbr.set(ZERO);
		sv.extCessTerm.set(ZERO);
		sv.insprm.set(ZERO);
		/*BRD-306 START */
		sv.premadj.set(ZERO);
		/*BRD-306 END */
		sv.znadjperc.set(ZERO);
		sv.zmortpct.set(ZERO);
		sv.oppc.set(ZERO);
		wsaaEntryFound.set("N");
		scrnparams.function.set(varcom.sclr);
		processScreen("S5013", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*    Retrieve the COVTTRM record (RETRV) in order to access the*/
		/*    coverage/Rider details, as these were previously read they*/
		/*    could still be in storage.*/
		covtlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(covtlnbIO.getChdrnum());
		sv.coverage.set(covtlnbIO.getCoverage());
		sv.life.set(covtlnbIO.getLife());
		sv.crtable.set(covtlnbIO.getCrtable());
		sv.rider.set(covtlnbIO.getRider());
		/*    Look up the description of the coverage rider (DESC) T5687.*/
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covtlnbIO.getCrtable());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.crtabdesc.set(descIO.getLongdesc());
		}
		else {
			sv.crtabdesc.fill("?");
		}
		/* Read  the  general  coverage/rider details from T5687 for the*/
		/* the coverage type held on COVTLNB. Access the version for the*/
		/* original commencement date of the risk.*/
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covtlnbIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(wsspcomn.company,itdmIO.getItemcoy())
		|| isNE(t5687,itdmIO.getItemtabl())
		|| isNE(covtlnbIO.getCrtable(),itdmIO.getItemitem())) {
			itdmIO.setStatuz(varcom.endp);
		}
		if (isNE(itdmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		/*    Life Assured and Joint Life details.*/
		/*    To obtain the life assured and joint-life details (if any) do*/
		/*    the following;-*/
		/*  - read  the life details using LIFELNB (life number from*/
		/*       COVTLNB, joint life number '00').  Look up the name*/
		/*       from the  client  details  (CLTS)  and  format as a*/
		/*       "confirmation name".*/
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		uwFlag = FeaConfg.isFeatureExist("2", "NBPRP123",appVars, "IT");
		if(uwFlag) {
			sv.uwFlag = 1;
			uwlvIO.setDataKey(SPACES);
			uwlvIO.setUserid(wsspcomn.userid);
			uwlvIO.setCompany(wsspcomn.company);
			uwlvIO.setFunction(varcom.readr);
			uwlvIO.setFormat(uwlvrec);
			SmartFileCode.execute(appVars, uwlvIO);
			if (isNE(uwlvIO.getStatuz(),varcom.oK)
					&& isNE(uwlvIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(uwlvIO.getParams());
				fatalError600();
			}
			if (isEQ(uwlvIO.getStatuz(),varcom.oK)) {
				if(isEQ(uwlvIO.getSkipautouw(),"Y")){
					sv.skipautouw = 1;
				}else{
					sv.skipautouw = 2;
				}
			}else{
				sv.skipautouw = 2;
			}
			if (isEQ(wsspcomn.flag,"C")) {
				sv.pageFlag = 1;
			}else{
				sv.pageFlag = 2;
			}
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		}else {
			sv.uwFlag = 2;
		}
		
		sv.occup01.set(lifelnbIO.getOccup());
		sv.pursuit01.set(lifelnbIO.getPursuit01());
		sv.pursuit02.set(lifelnbIO.getPursuit02());
		sv.smoking01.set(lifelnbIO.getSmoking());
		/*    Read CLTS record for Life and format name.*/
		cltsIO.setClntpfx("CN");
		/* MOVE LIFELNB-CHDRCOY        TO CLTS-CLNTCOY.                 */
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		wsaaLifeSex.set(cltsIO.getCltsex());
		plainname();
		sv.linsname.set(wsspcomn.longconfname);
		/*  - read the joint life details using LIFELNB (life number*/
		/*       from COVTLNB,  joint  life number '01').  If found,*/
		/*       look up the name from the client details (CLTS) and*/
		/*       format as a "confirmation name".*/
		lifelnbIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lifelnbIO.setChdrnum(covtlnbIO.getChdrnum());
		lifelnbIO.setLife(covtlnbIO.getLife());
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			sv.jlifcnum.set(SPACES);
			sv.jlinsname.set(SPACES);
			sv.occup02.set(SPACES);
			sv.pursuit03.set(SPACES);
			sv.pursuit04.set(SPACES);
			sv.smoking02.set(SPACES);
			goTo(GotoLabel.lext1015);
		}
		sv.occup02.set(lifelnbIO.getOccup());
		sv.pursuit03.set(lifelnbIO.getPursuit01());
		sv.pursuit04.set(lifelnbIO.getPursuit02());
		sv.smoking02.set(lifelnbIO.getSmoking());
		/*    Read CLTS record for Life and format name.*/
		cltsIO.setClntpfx("CN");
		/* MOVE LIFELNB-CHDRCOY        TO CLTS-CLNTCOY.                 */
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFunction(varcom.readr);
		cltsIO.setFormat(cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		sv.jlifcnum.set(lifelnbIO.getLifcnum());
		plainname();
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void lext1015()
	{
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)
		|| isEQ(t5687rec.jlifePresent,"N")) {
			sv.selectOut[varcom.nd.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			addBlankLine1020();
		}
		goTo(GotoLabel.loadExistingLext1030);
	}

protected void addBlankLine1020()
	{
		processScreen("S5013", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void loadExistingLext1030()
	{
  		lextIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lextIO.setChdrnum(covtlnbIO.getChdrnum());
		lextIO.setLife(covtlnbIO.getLife());
		lextIO.setCoverage(covtlnbIO.getCoverage());
		lextIO.setRider(covtlnbIO.getRider());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		index = 1;
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp)
		|| isNE(lextIO.getChdrcoy(),covtlnbIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(),covtlnbIO.getChdrnum())
		|| isNE(lextIO.getLife(),covtlnbIO.getLife())
		|| isNE(lextIO.getCoverage(),covtlnbIO.getCoverage())
		|| isNE(lextIO.getRider(),covtlnbIO.getRider()))) {
			loadSubfile1500();
		}
		loadExistingLext1031();
		if (isEQ(wsspcomn.flag,"I")) {
			sv.opcdaOut[varcom.pr.toInt()].set("Y");
		}
	}
protected void getUndqList(){
	questions = (List<String>)ThreadLocalStore.get(UWQUESTIONNAIRECACHE);
	if(questions == null){
		List<Undqpf> undqList = undqpfDAO.searchUnqpf(lifelnbIO.getChdrcoy().toString().trim(), lifelnbIO.getChdrnum().toString().trim(), 
				lifelnbIO.getLife().toString().trim(), "00");
		questions = new ArrayList<String>();
		for(Undqpf u : undqList){
			if("Y".equals(u.getAnswer().trim())){
				questions.add(u.getQuestidf());
			}
		}
	}
}
protected void loadExistingLext1031(){
	if(uwFlag && isEQ(wsspcomn.flag,"C")){
		List<String> autoTermsList = (List)ThreadLocalStore.get(P5006.AUTO_TERMS_LIST);
		if(autoTermsList != null && autoTermsList.contains(covtlnbIO.getCrtable().toString().trim())) {
			return;
		} else {
			getUndqList();
			for (String question : questions) {
				uwQuestionnaireRec = new UWQuestionnaireRec();
				uwQuestionnaireRec.setTransEffdate(datcon1rec.intDate.getbigdata());
				uwQuestionnaireRec.setContractType(chdrlnbIO.getCnttype().toString());
				uwQuestionnaireRecReturn = uwQuestionnaireUtil.getUWMessage(uwQuestionnaireRec, wsspcomn.language.toString(), covtlnbIO.getChdrcoy().toString(),
						question, covtlnbIO.crtable.toString(),"");
				loadExistingLext1032(index);
			}
				loadDefaultOccupation(index);
		}
	}
}
protected void loadExistingLext1032(int index){
	if(!uwQuestionnaireRecReturn.getOutputUWDec().isEmpty() && !uwQuestionnaireRecReturn.getOutputUWDec().contains(ACCEPTED)
			&& !uwQuestionnaireRecReturn.getOutputUWDec().contains("0")){
		for(int i=0;i<uwQuestionnaireRecReturn.getOutputUWDec().size();i++){
			scrnparams.subfileRrn.set(index);
			scrnparams.function.set(varcom.sread);
			processScreen("S5013", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			loadExistingLext1033(i);
			scrnparams.function.set(varcom.supd);
			processScreen("S5013", sv);
			if (isNE(scrnparams.statuz,varcom.oK)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			index+=1;
		}
	}
}
protected void loadExistingLext1033(int i){
	sv.agerate.set(uwQuestionnaireRecReturn.getOutputSplTermAge().get(i));
	sv.extCessTerm.set(ZERO);
	sv.insprm.set(uwQuestionnaireRecReturn.getOutputSplTermRateAdj().get(i));
	sv.premadj.set(ZERO);
	sv.oppc.set(uwQuestionnaireRecReturn.getOutputSplTermLoadPer().get(i));
	sv.opcda.set(uwQuestionnaireRecReturn.getOutputSplTermCode().get(i));
	sv.seqnbr.set(index);
	sv.reasind.set("1");
	sv.znadjperc.set(uwQuestionnaireRecReturn.getOutputSplTermSAPer().get(i));
	sv.zmortpct.set(uwQuestionnaireRecReturn.getOutputMortPerc().get(i));
	sv.uwoverwrite.set("Y");
	getT5651Desc(lifelnbIO.getChdrcoy().toString(),uwQuestionnaireRecReturn.getOutputSplTermCode().get(i));
	if (isEQ(lifelnbIO.getJlife(),"00")) {
		sv.select.set("L");
	}
	else {
		if (isEQ(lifelnbIO.getJlife(),"01")) {
			sv.select.set("J");
		}
		else {
			sv.select.set(SPACES);
		}
	}
	wsaaEntryFound.set("Y");
}
protected void getT5651Desc(String company,String descItem){
	descIO.setDataArea(SPACES);
	descIO.setDescpfx("IT");
	descIO.setDesccoy(company);
	descIO.setDesctabl(t5651);
	descIO.setDescitem(descItem);
	descIO.setLanguage(wsspcomn.language);
	descIO.setFormat(descrec);
	descIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(),varcom.oK)
	&& isNE(descIO.getStatuz(),varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	if (isEQ(descIO.getStatuz(),varcom.oK)) {
		sv.shortdesc.set(descIO.getShortdesc());
	}
	else {
		sv.shortdesc.fill("?");
	}
}
private void loadDefaultOccupation(int index) {
	
	scrnparams.subfileRrn.set(index);
	scrnparams.function.set(varcom.sread);
	processScreen("S5013", sv);
	if (isNE(scrnparams.statuz,varcom.oK)) {
		syserrrec.statuz.set(scrnparams.statuz);
		fatalError600();
	}
	uwOccupationRec = new UWOccupationRec();
	uwOccupationRec.transEffdate.set(datcon1rec.intDate.getbigdata());
	uwOccupationRec.occupCode.set(cltsIO.getOccpcode());
	uwOccupationRec.indusType.set(cltsIO.getStatcode());
	uwOccupationRec = uwoccupationUtil.getUWOccupationRec(uwOccupationRec, chdrlnbIO.getChdrcoy().toString(),
			wsspcomn.fsuco.toString(), chdrlnbIO.getCnttype().toString(), sv.crtable.toString().trim());
	if (uwOccupationRec != null && isNE(uwOccupationRec.outputUWDec, ACCEPTED)
			&& isNE(uwOccupationRec.outputUWDec, SPACE)
			&& isNE(uwOccupationRec.outputUWDec, "0")) {
		scrnparams.function.set(varcom.sadd);
		sv.seqnbr.set(index);
		sv.insprm.set(uwOccupationRec.outputSplTermRateAdj);
		sv.agerate.set(uwOccupationRec.outputSplTermAge);
		sv.oppc.set(uwOccupationRec.outputSplTermLoadPer);
		sv.zmortpct.set(uwOccupationRec.outputMortPerc);
		sv.znadjperc.set(uwOccupationRec.outputSplTermSAPer);
		sv.opcda.set(uwOccupationRec.outputSplTermCode);
		sv.reasind.set("1");
		sv.uwoverwrite.set("Y");
		sv.extCessTerm.set(ZERO);
		sv.premadj.set(ZERO);
		sv.agerateOut[varcom.pr.toInt()].set("Y");
		sv.ecestrmOut[varcom.pr.toInt()].set("Y");
		sv.insprmOut[varcom.pr.toInt()].set("Y");
		// sv.opcdaOut[varcom.pr.toInt()].set("Y");
		sv.oppcOut[varcom.pr.toInt()].set("Y");
		sv.reasindOut[varcom.pr.toInt()].set("Y");
		sv.selectOut[varcom.pr.toInt()].set("Y");
		sv.seqnbrOut[varcom.pr.toInt()].set("Y");
		sv.shortdescOut[varcom.pr.toInt()].set("Y");
		sv.zmortpctOut[varcom.pr.toInt()].set("Y");
		sv.znadjpercOut[varcom.pr.toInt()].set("Y");
		sv.premadjOut[varcom.pr.toInt()].set("Y");

		scrnparams.function.set(varcom.supd);
		processScreen("S5013", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}
}
protected void loadSubfile1500()
	{
		para1500();
	}

protected void para1500()
	{
		if(uwFlag){
			scrnparams.subfileRrn.set(index);
		}else{
			scrnparams.subfileRrn.set(lextIO.getSeqnbr());
		}
		scrnparams.function.set(varcom.sread);
		processScreen("S5013", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		sv.agerate.set(lextIO.getAgerate());
		sv.extCessTerm.set(lextIO.getExtCessTerm());
		sv.insprm.set(lextIO.getInsprm());
		/*BRD-306 START */
		sv.premadj.set(lextIO.getPremadj());
		/*BRD-306 END */
		sv.opcda.set(lextIO.getOpcda());
		sv.oppc.set(lextIO.getOppc());
		sv.seqnbr.set(lextIO.getSeqnbr());
		sv.reasind.set(lextIO.getReasind());
		sv.znadjperc.set(lextIO.getZnadjperc());
		sv.zmortpct.set(lextIO.getZmortpct());
		if(uwFlag){
			sv.uwoverwrite.set(lextIO.getUwoverwrite());
		}
//		descIO.setDataArea(SPACES);
//		descIO.setDescpfx("IT");
//		descIO.setDesccoy(lextIO.getChdrcoy());
//		descIO.setDesctabl(t5651);
//		descIO.setDescitem(lextIO.getOpcda());
//		descIO.setLanguage(wsspcomn.language);
//		descIO.setFormat(descrec);
//		descIO.setFunction(varcom.readr);
//		SmartFileCode.execute(appVars, descIO);
//		if (isNE(descIO.getStatuz(),varcom.oK)
//		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
//			syserrrec.params.set(descIO.getParams());
//			fatalError600();
//		}
//		if (isEQ(descIO.getStatuz(),varcom.oK)) {
//			sv.shortdesc.set(descIO.getShortdesc());
//		}
//		else {
//			sv.shortdesc.fill("?");
//		}
		getT5651Desc(lextIO.getChdrcoy().toString(),lextIO.getOpcda().toString().trim());
		if (isEQ(lextIO.getJlife(),"00")) {
			sv.select.set("L");
		}
		else {
			if (isEQ(lextIO.getJlife(),"01")) {
				sv.select.set("J");
			}
			else {
				sv.select.set(SPACES);
			}
		}
		wsaaEntryFound.set("Y");
		scrnparams.function.set(varcom.supd);
		processScreen("S5013", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		lextIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
		index += 1;
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,"KILL")
		|| isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2490);
		}
		if (isEQ(scrnparams.statuz,"CALC")) {
			wsspcomn.edterror.set("Y");
		}
		
	}

protected void validateScreen2010()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5013", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2500();
		}

		if (noEntryFound.isTrue()
		&& isNE(sv.znadjperc,ZERO)) {
			sv.znadjpercErr.set(e492);
			wsspcomn.edterror.set("Y");
		}
		/*BRD-306 START */
		if (noEntryFound.isTrue()
		&& isNE(sv.premadj,ZERO)) {
			sv.premadjErr.set(e492);
			wsspcomn.edterror.set("Y");
		}
		/*BRD-306 END */
	}

protected void validateSubfile2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					opcda2501();
					opcda2500();
				case lookUpDesc2505:
					lookUpDesc2505();
					default2510();
				case updateErrorIndicators2520:
					updateErrorIndicators2520();
					readNextModifiedRecord2530();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
protected void opcda2502(){
	uwQuestionnaireRec = new UWQuestionnaireRec();
	uwQuestionnaireRec.setTransEffdate(datcon1rec.intDate.getbigdata());
	uwQuestionnaireRec.setContractType(chdrlnbIO.getCnttype().toString());
	uwQuestionnaireRecReturn = uwQuestionnaireUtil.getUWMessage(uwQuestionnaireRec, wsspcomn.language.toString(), covtlnbIO.getChdrcoy().toString(),
			"", covtlnbIO.crtable.toString(),sv.opcda.toString().trim());
	if(!uwQuestionnaireRecReturn.getOutputUWDec().isEmpty()
			&& !uwQuestionnaireRecReturn.getOutputUWDec().contains(ACCEPTED)
			&& !uwQuestionnaireRecReturn.getOutputUWDec().contains("0")){
		sv.agerate.set(uwQuestionnaireRecReturn.getOutputSplTermAge().get(0));
		sv.insprm.set(uwQuestionnaireRecReturn.getOutputSplTermRateAdj().get(0));
		sv.oppc.set(uwQuestionnaireRecReturn.getOutputSplTermLoadPer().get(0));
		sv.opcda.set(uwQuestionnaireRecReturn.getOutputSplTermCode().get(0));
		sv.znadjperc.set(uwQuestionnaireRecReturn.getOutputSplTermSAPer().get(0));
		sv.zmortpct.set(uwQuestionnaireRecReturn.getOutputMortPerc().get(0));
		sv.extCessTerm.set(ZERO);
		sv.premadj.set(ZERO);
		sv.reasind.set("1");
	}
}
protected void opcda2501(){
	if(uwFlag && isEQ(sv.uwoverwrite,"Y")){
		if(sv.opcda.toString().startsWith("L") ||sv.opcda.toString().startsWith("R")||sv.opcda.toString().startsWith("M")){
			opcda2502();
		}else if(sv.opcda.toString().startsWith("A") || sv.opcda.toString().startsWith("B")){
			opcda2503();
		}
	}
}
protected void opcda2503(){
	uwOccupationRec = new UWOccupationRec();
	uwOccupationRec.transEffdate.set(datcon1rec.intDate.getbigdata());
	uwOccupationRec.occupCode.set(cltsIO.getOccpcode());
	uwOccupationRec.indusType.set(cltsIO.getStatcode());
	uwOccupationRec = uwoccupationUtil.getUWOccupationRec(uwOccupationRec, chdrlnbIO.getChdrcoy().toString(),
			wsspcomn.fsuco.toString(), chdrlnbIO.getCnttype().toString(), sv.crtable.toString().trim());
	if (uwOccupationRec != null && isNE(uwOccupationRec.outputUWDec, ACCEPTED)
			&& isNE(uwOccupationRec.outputUWDec, SPACE)
			&& isNE(uwOccupationRec.outputUWDec, "0")) {
		sv.agerate.set(uwOccupationRec.outputSplTermAge);
		sv.insprm.set(uwOccupationRec.outputSplTermRateAdj);
		sv.oppc.set(uwOccupationRec.outputSplTermLoadPer);
		sv.opcda.set(uwOccupationRec.outputSplTermCode);
		sv.znadjperc.set(uwOccupationRec.outputSplTermSAPer);
		sv.zmortpct.set(uwOccupationRec.outputMortPerc);
		sv.extCessTerm.set(ZERO);
		sv.premadj.set(ZERO);
		sv.reasind.set("1");
	}
}
protected void opcda2500()
	{
		/*    Edit each line  modified in the subfile according to the Help*/
		/*    information.*/
		if (isEQ(sv.opcda,SPACES)) {
			sv.shortdesc.set(SPACES);
		}
		if (isEQ(sv.agerate,ZERO)
		&& isEQ(sv.extCessTerm,ZERO)
		&& isEQ(sv.insprm,ZERO)
		&& isEQ(sv.oppc,ZERO)
		&& isEQ(sv.opcda,SPACES)
		&& isEQ(sv.select,SPACES)
		&& isEQ(sv.zmortpct,0)
		/*BRD-306 START */
		&& isEQ(sv.premadj,ZERO)
		) {
			wsaaEntryFound.set("N");
			goTo(GotoLabel.updateErrorIndicators2520);
		}
		/*BRD-306 END */
		wsaaEntryFound.set("Y");
		wsaaPremCessTerm.set(wssplife.bigAmt);
		if (isEQ(sv.extCessTerm,ZERO)) {
			if (isNE(wsaaPremCessTerm,ZERO)) {
				sv.extCessTerm.set(wsaaPremCessTerm);
			}
			else {
				sv.extCessTerm.set(wssplife.fupno);
			}
		}
		if (isNE(wsaaPremCessTerm,ZERO)) {
			if (isGT(sv.extCessTerm,wsaaPremCessTerm)) {
				sv.ecestrmErr.set(t040);
			}
		}
		else {
			wsaaPremAgeTerm.set(wssplife.fupno);
			if (isGT(sv.extCessTerm,wsaaPremAgeTerm)) {
				sv.ecestrmErr.set(t040);
			}
		}
		
		validation2110CustomerSpecific();
		if (isEQ(sv.reasind,SPACES)) {
			sv.reasind.set("1");
		}
		if (isNE(sv.reasind,"1")
		&& isNE(sv.reasind,"2")
		&& isNE(sv.reasind,"3")) {
			sv.reasindErr.set(g709);
			wsspcomn.edterror.set("Y");
		}
		if (isNE(sv.zmortpct,ZERO)) {
			checkTh5492600();
		}
		if (isNE(sv.opcda,SPACES)) {
			goTo(GotoLabel.lookUpDesc2505);
		}
		if (isNE(sv.agerate,ZERO)) {
			sv.agerateErr.set(e492);
		}
		if (isNE(sv.insprm,ZERO)) {
			sv.insprmErr.set(e492);
		}
		/*BRD-306 START */
		if (isNE(sv.premadj,ZERO)) {
			sv.premadjErr.set(e492);
		}
		/*BRD-306 END */
		if (isNE(sv.oppc,ZERO)) {
			sv.oppcErr.set(e492);
		}
		if (isNE(sv.extCessTerm,ZERO)) {
			sv.ecestrmErr.set(e492);
		}
		if (isNE(sv.select,ZERO)) {
			sv.selectErr.set(e492);
		}
		if (isNE(sv.zmortpct,ZERO)) {
			sv.selectErr.set(e492);
		}
		goTo(GotoLabel.updateErrorIndicators2520);
	}

	protected void validation2110CustomerSpecific() {
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf) || isEQ(t5687rec.jlifePresent, "N")) {
			if (isNE(sv.select, SPACES)) {
				sv.select.set(SPACES);
			}
		} else {
			if (isNE(sv.select, "J") && isNE(sv.select, "L")) {
				sv.selectErr.set(h039);
			}
		}
	}
protected void lookUpDesc2505()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5651);
		descIO.setDescitem(sv.opcda);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.shortdesc.set(descIO.getShortdesc());
		}
		else {
			sv.shortdesc.fill("?");
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemtabl(t5651);
		itemIO.setItemitem(sv.opcda);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.opcdaErr.set(f239);
			goTo(GotoLabel.updateErrorIndicators2520);
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemtabl(t5657);
		wsaaT5657key1.set(sv.crtable);
		wsaaT5657key2.set(sv.opcda);
		itemIO.setItemitem(wsaaT5657key);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.opcdaErr.set(f246);
			goTo(GotoLabel.updateErrorIndicators2520);
		}
		t5657rec.t5657Rec.set(itemIO.getGenarea());
	}

protected void default2510()
	{
		
		default2511CustomerSpecific();
		/** ILIFE-926 **/
		/*if (isNE(t5657rec.tabtype,SPACES)) {*/
		/** ILIFE-926 **/
		
		if (isNE(t5657rec.tabtype,"N")) {
			goTo(GotoLabel.updateErrorIndicators2520);
		}
		if (isEQ(sv.agerate,ZERO)
				&& isEQ(sv.insprm,ZERO)
				&& isEQ(sv.oppc,ZERO)) {
			if (isEQ(t5657rec.t5657Rec,SPACES)) {
				sv.agerate.set(ZERO);
				sv.insprm.set(ZERO);
				sv.oppc.set(ZERO);
			}
			else {
				sv.agerate.set(t5657rec.agerate);
				sv.insprm.set(t5657rec.insprm);
				sv.oppc.set(t5657rec.oppc);
			}
		}
		if (isNE(sv.opcda,SPACES) && !uwFlag) {
			if (isEQ(sv.agerate,ZERO)
					&& isEQ(sv.insprm,ZERO)
					&& isEQ(sv.oppc,ZERO)
					&& isEQ(sv.zmortpct,ZERO)
					/*BRD-306 START */
					&& isEQ(sv.premadj,ZERO)) {
				sv.agerateErr.set(f307);
				sv.insprmErr.set(f307);
				sv.oppcErr.set(f307);
				sv.zmortpctErr.set(f307);
				sv.premadjErr.set(f307);
			}
			/*BRD-306 END */
		}
		
	}

protected void  default2511CustomerSpecific(){
	/** ILIFE-926 **/
	/*if (isNE(t5657rec.tabtype,SPACES)) {*/
	/** ILIFE-926 **/
	if (isNE(t5657rec.tabtype,"N")) {
		if (isNE(sv.agerate,ZERO)) {
			sv.agerateErr.set(d032);
			sv.opcdaErr.set(d032);
		}
		if (isNE(sv.oppc,ZERO)) {
			sv.oppcErr.set(d032);
			sv.opcdaErr.set(d032);
		}
		if (isNE(sv.insprm,ZERO)) {
			sv.insprmErr.set(d032);
			sv.opcdaErr.set(d032);
		}
	}
}
protected void updateErrorIndicators2520()
	{
		if (isNE(sv.errorSubfile,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S5013", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2530()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("S5013", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void checkTh5492600()
	{
			th5492601();
		}

protected void th5492601()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covtlnbIO.getChdrcoy());
		itdmIO.setItemtabl(th549);
		wsaaTh549Crtable.set(covtlnbIO.getCrtable());
		wsaaTh549Zmortpct.set(sv.zmortpct);
		wsaaTh549Zsexmort.set(wsaaLifeSex);
		itdmIO.setItemitem(wsaaTh549Key);
		itdmIO.setItmfrm(covtlnbIO.getEffdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(wsaaTh549Key,itdmIO.getItemitem())
		|| isNE(covtlnbIO.getChdrcoy(),itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(),th549)
		|| isEQ(itdmIO.getStatuz(), "ENDP")) {
			sv.zmortpctErr.set(hl27);
			wsspcomn.edterror.set("Y");
			return ;
		}
	}

protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,"KILL")
		|| isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5013", sv);

		lextIO.setParams(SPACES);

		lextIO.setChdrcoy(covtlnbIO.getChdrcoy());
		lextIO.setChdrnum(covtlnbIO.getChdrnum());
		lextIO.setLife(covtlnbIO.getLife());
		lextIO.setCoverage(covtlnbIO.getCoverage());
		lextIO.setRider(covtlnbIO.getRider());
		lextIO.setZnadjperc(sv.znadjperc);
		lextIO.setFormat(lextrec);

		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			srdn3500();
		}

	}

protected void srdn3500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					para3510();
				case updateLext3520:
					updateLext3520();
				case nextLine3580:
					nextLine3580();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para3510()
	{
		if (isEQ(sv.seqnbr,ZERO)
		&& isEQ(sv.opcda,SPACES)) {
			goTo(GotoLabel.nextLine3580);
		}

		if (isNE(sv.seqnbr,ZERO)
		&& isEQ(sv.opcda,SPACES)) {
			lextIO.setSeqnbr(sv.seqnbr);
			lextIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, lextIO);
			if (isNE(lextIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(lextIO.getParams());
				fatalError600();
			}
			else {
				lextIO.setFunction(varcom.delet);
				goTo(GotoLabel.updateLext3520);
			}
		} 
		//MIBT-54 STARTS
		else{
//			if(uwFlag){
//				lextIO.setSeqnbr(sv.seqnbr);
//			}else{
				lextIO.setSeqnbr(scrnparams.subfileRrn);
//			}
			lextIO.setFunction(varcom.readh);
			SmartFileCode.execute(appVars, lextIO);
		}
		//MIBT-54 ENDS
		if(uwFlag){
//			lextIO.setSeqnbr(sv.seqnbr);
			lextIO.setUwoverwrite(sv.uwoverwrite);
		}
//		else{
			lextIO.setSeqnbr(scrnparams.subfileRrn);
//		}
		lextIO.setValidflag("1");
		lextIO.setCurrfrom(chdrlnbIO.getOccdate());
		lextIO.setCurrto(varcom.vrcmMaxDate);
		lextIO.setOpcda(sv.opcda);
		lextIO.setOppc(sv.oppc);
		lextIO.setInsprm(sv.insprm);
		/*BRD-306 START */
		lextIO.setPremadj(sv.premadj);
		/*BRD-306 END */
		lextIO.setAgerate(sv.agerate);
		lextIO.setReasind(sv.reasind);
		lextIO.setZmortpct(sv.zmortpct);
		if (isEQ(sv.select,"L")) {
			lextIO.setJlife("00");
		}
		else {
			if (isEQ(sv.select,"J")) {
				lextIO.setJlife("01");
			}
			else {
				lextIO.setJlife("00");
			}
		}
		lextIO.setTranno(chdrlnbIO.getTranno());
		lextIO.setExtCommDate(chdrlnbIO.getOccdate());
		lextIO.setExtCessDate(varcom.vrcmMaxDate);
		lextIO.setExtCessTerm(sv.extCessTerm);
		if (isNE(sv.extCessTerm,0)) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon2rec.freqFactor.set(sv.extCessTerm);
			datcon2rec.frequency.set("01");
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			else {
				lextIO.setExtCessDate(datcon2rec.intDate2);
			}
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(covtlnbIO.getChdrcoy());
		itemIO.setItemtabl(t5657);
		wsaaT5657key1.set(sv.crtable);
		wsaaT5657key2.set(sv.opcda);
		itemIO.setItemitem(wsaaT5657key);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5657rec.t5657Rec.set(itemIO.getGenarea());
		lextIO.setSbstdl(t5657rec.sbstdl);
		if(uwFlag){
			srdn3501();
		}
		lextIO.setFunction(varcom.updat);
	}
protected void srdn3501(){
	lextIO.setUwoverwrite(sv.uwoverwrite);
	if(sv.skipautouw == 1 && isNE(wsspcomn.flag,"C")){
		if(isEQ(sv.uwoverwrite,"Y") && isNE(sv.uwoverwrite,SPACE)){
			if(sv.opcda.toString().startsWith("L") ||sv.opcda.toString().startsWith("R")||sv.opcda.toString().startsWith("M")){
				srdn3502();
			}
			if(sv.opcda.toString().startsWith("A") ||sv.opcda.toString().startsWith("B")){
				srdn3503();
			}
		}
	}
}
protected void srdn3502(){
	uwQuestionnaireRec = new UWQuestionnaireRec();
	uwQuestionnaireRec.setTransEffdate(datcon1rec.intDate.getbigdata());
	uwQuestionnaireRec.setContractType(chdrlnbIO.getCnttype().toString());
	uwQuestionnaireRecReturn = uwQuestionnaireUtil.getUWMessage(uwQuestionnaireRec,"E", 
			chdrlnbIO.getChdrcoy().toString().trim(),"", sv.crtable.toString().trim(),sv.opcda.toString().trim());
	if(uwQuestionnaireRecReturn != null && !uwQuestionnaireRecReturn.getOutputUWDec().isEmpty() 
			&& !uwQuestionnaireRecReturn.getOutputUWDec().contains(ACCEPTED)
			&& !uwQuestionnaireRecReturn.getOutputUWDec().contains("0")){
		lextIO.setInsprm(uwQuestionnaireRecReturn.getOutputSplTermRateAdj().get(0));
		lextIO.setAgerate(uwQuestionnaireRecReturn.getOutputSplTermAge().get(0));
		lextIO.setOppc(uwQuestionnaireRecReturn.getOutputSplTermLoadPer().get(0));
		lextIO.setZmortpct(uwQuestionnaireRecReturn.getOutputMortPerc().get(0));
		lextIO.setZnadjperc(uwQuestionnaireRecReturn.getOutputSplTermSAPer().get(0));
		vpmsUpdateFlag = true;
	}
}
protected void srdn3503(){
	uwOccupationRec = new UWOccupationRec();
	uwOccupationRec.transEffdate.set(datcon1rec.intDate.getbigdata());
	uwOccupationRec.occupCode.set(cltsIO.getOccpcode());
	uwOccupationRec.indusType.set(cltsIO.getStatcode());
	uwOccupationRec = uwoccupationUtil.getUWOccupationRec(uwOccupationRec, chdrlnbIO.getChdrcoy().toString(), wsspcomn.fsuco.toString(),
			chdrlnbIO.getCnttype().toString(),sv.crtable.toString().trim());
	if(uwOccupationRec != null && isNE(uwOccupationRec.outputUWDec,SPACE) 
			&& isNE(uwOccupationRec.outputUWDec, "0")
			&& isNE(uwOccupationRec.outputUWDec,ACCEPTED) ){
		lextIO.setInsprm(uwOccupationRec.outputSplTermRateAdj);
		lextIO.setAgerate(uwOccupationRec.outputSplTermAge);
		lextIO.setOppc(uwOccupationRec.outputSplTermLoadPer);
		lextIO.setZmortpct(uwOccupationRec.outputMortPerc);
		lextIO.setZnadjperc(uwOccupationRec.outputSplTermSAPer);
		vpmsUpdateFlag = true;
	}
}
protected void updateLext3520()
	{
		lextIO.setTermid(varcom.vrcmTermid);
		lextIO.setTransactionDate(varcom.vrcmDate);
		lextIO.setTransactionTime(varcom.vrcmTime);
		lextIO.setUser(varcom.vrcmUser);
		if(!uwFlag || !vpmsUpdateFlag){
			lextIO.setZnadjperc(sv.znadjperc);
		}
		/*BRD-306 START */
		lextIO.setPremadj(sv.premadj);
		/*BRD-306 END */
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lextIO.getParams());
			fatalError600();
		}
	}

protected void nextLine3580()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("S5013", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
