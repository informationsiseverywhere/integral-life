package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PcddpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:01
 * Class transformed from PCDDPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PcddpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 99;
	public FixedLengthStringData pcddrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData pcddpfRecord = pcddrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(pcddrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(pcddrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(pcddrec);
	public PackedDecimalData splitBcomm = DD.splitc.copy().isAPartOf(pcddrec);
	public PackedDecimalData splitBpts = DD.splitb.copy().isAPartOf(pcddrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(pcddrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(pcddrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(pcddrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(pcddrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(pcddrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(pcddrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(pcddrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(pcddrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(pcddrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(pcddrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(pcddrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public PcddpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for PcddpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public PcddpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for PcddpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public PcddpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for PcddpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public PcddpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("PCDDPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"AGNTNUM, " +
							"SPLITC, " +
							"SPLITB, " +
							"VALIDFLAG, " +
							"TRANNO, " +
							"CURRFROM, " +
							"CURRTO, " +
							"USER_T, " +
							"TERMID, " +
							"TRTM, " +
							"TRDT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     agntnum,
                                     splitBcomm,
                                     splitBpts,
                                     validflag,
                                     tranno,
                                     currfrom,
                                     currto,
                                     user,
                                     termid,
                                     transactionTime,
                                     transactionDate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		agntnum.clear();
  		splitBcomm.clear();
  		splitBpts.clear();
  		validflag.clear();
  		tranno.clear();
  		currfrom.clear();
  		currto.clear();
  		user.clear();
  		termid.clear();
  		transactionTime.clear();
  		transactionDate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getPcddrec() {
  		return pcddrec;
	}

	public FixedLengthStringData getPcddpfRecord() {
  		return pcddpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setPcddrec(what);
	}

	public void setPcddrec(Object what) {
  		this.pcddrec.set(what);
	}

	public void setPcddpfRecord(Object what) {
  		this.pcddpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(pcddrec.getLength());
		result.set(pcddrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}