/*
 * File: P5005.java
 * Date: 29 August 2009 23:53:29
 * Author: Quipoz Limited
 * 
 * Class transformed from P5005.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.ClprTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.AnsopfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Ansopf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.tablestructures.T3584rec;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.tablestructures.T2240rec;
import com.csc.fsu.general.tablestructures.T3644rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.dao.ZctxpfDAO;
import com.csc.life.agents.dataaccess.model.Zctxpf;
import com.csc.life.contractservicing.procedures.UwQuestionnaireUtil;
import com.csc.life.contractservicing.procedures.UwoccupationUtil;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LfcllnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.life.newbusiness.procedures.Antisocl;
import com.csc.life.newbusiness.recordstructures.Antisoclkey;
import com.csc.life.newbusiness.screens.S5005ScreenVars;
import com.csc.life.newbusiness.tablestructures.NBProposalRec;
import com.csc.life.newbusiness.tablestructures.T5657rec;
import com.csc.life.productdefinition.dataaccess.MliasecTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;
import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.terminationclaims.dataaccess.LifematTableDAM;
import com.csc.life.underwriting.dataaccess.UndlTableDAM;
import com.csc.life.underwriting.dataaccess.dao.UndlpfDAO;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.life.underwriting.tablestructures.T6769rec;
import com.csc.life.underwriting.tablestructures.T6771rec;
import com.csc.life.underwriting.tablestructures.T6772rec;
import com.csc.life.underwriting.tablestructures.Tr675rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;

/**
 * <pre>
 * Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
 *REMARKS.
 *        Life and Joint Life Assured Details
 *
 *Initialise
 *----------
 *
 *   Retrieve the contract  header  (CHDRLNB) and the life details
 *   to be maintained  (LIFELNB).  Release  the LIFELNB I/O module
 *   for use within this program.
 *
 *   Read the contract  definition  table  entry  (T5688)  for the
 *   contract type  (form CHDRLNB), effective at original contract
 *   commencement date.
 *
 *   If the maximum number of joint lives allowed from T5688 is 0,
 *   non-display the 'Roll keys' text literal and disable the roll
 *   keys. (Use the joint life no field to do this)
 *
 *   If the first (or only) life  is being displayed,  non-dispaly
 *   relationship fields and  literals. These are only needed when
 *   displaying joint life details.
 *
 *   If life details  exist  (effective  date  not zero), move all
 *   current details to  the  screen.  Look  up the client details
 *   (CLTS) and format the name as 'confirmation'.
 *   Default the client number for the first life to the contract
 *   owner.
 *
 *
 * Validation
 * ----------
 *
 *   If in enquiry  mode  (WSSP-FLAG  =  'I'), protect the screen.
 *   Then skip all validation.
 *
 *   If changed (CHG in  DDS)  and  not  blank,  check  the client
 *   against CLTS. Set  up  confirmation  name  again from details
 *   read.
 *
 *   If the client is a company, display an error message.
 *    - personal clients only can be added as life insured's.
 *
 *   Note - for an optional  joint  life (min no allowed = 0), and
 *   joint life currently  being maintained, the joint life can be
 *   deleted by having all  its  details blanked out. Thus in this
 *   case, if the client  number  is  blank, ensure that all other
 *   fields are also blank (or zero).
 *
 *   Validate all fields according to the help except:
 *             Sex
 *             ANB at RCD
 *             Date of birth
 *             Occupation
 *
 *   The relationship field  can  only  be checked as mandatory if
 *   validating a joint life.
 *
 *   If CALC pressed (and the client number is valid),
 *
 *        - overwrite  the   following  from  the  client  details
 *             previously read:
 *
 *             Sex
 *             ANB at RCD  - note *
 *             Date of birth
 *             Occupation
 *
 *        * only overwrite if  not  mandatory (ANB at RCD required
 *             indicator from T5688 no 'R')
 *
 *   Otherwise CALC was not pressed,
 *
 *        - validate the roll key, if pressed:
 *             ROLD only valid on joint life version of screen,
 *             ROLU only valid on main life version of screen.
 *
 *        - validate the remaining fields:
 *             Sex - note *
 *             ANB at RCD - calculate if not 'required'
 *             Date of birth - note *
 *             Occupation - default if not entered
 *
 *        * cannot be different  from  the  client  details if the
 *             life is assured  on  any  other  contract. To check
 *             this, read the  client  roles  file  (CLRR) for the
 *             client number  and  role of life assured (CLRF-LIFE
 *             from CLNTRLSREC).  If  there  are any for contracts
 *             other than the  current one, this means sex and DoB
 *             must be the  same  as  already  held  on the client
 *             record.
 *
 * Updating
 * --------
 *
 *   If in enquiry mode, skip the updating.
 *
 *   If the sex of age  next  birthday  (ANB at RCD) on the screen
 *
 *   Maintain the client roles file (CLRR) as follows:
 *
 *        - if the client number on  the screen is not the same as
 *             the client number  on  the  life record, delete the
 *             life role for  the  client  number on the life file
 *             and add a new  one  for  the  client  number on the
 *             screen.
 *
 *        - if it is a new life, add a new client role record.
 *
 *        - if it is a  joint  life being deleted, delete the life
 *             role for the client on file.
 *
 *   For  new  life   (or  joint  life)  records,  initialise  the
 *   following fields:
 *             Transaction number from CHDRLNB
 *             Current from date from CHDRLNB
 *             Current to date from CHDRLNB
 *             Valid flag to '3'
 *
 *   Write/update the required life/joint life.
 *
 *   Note - the first life  has  a  joint life number of 0 and the
 *   joint life has a joint line number of 1.
 *
 *   If the age or  sex  of  the  life  details are different from
 *   those held on  the  client  record,  update the client record
 *   with the life versions (CLTS).
 *
 * Next
 * ----
 *
 *   If a roll  key  was pressed, process the other life. Read the
 *   joint life record  (which may or may not exist) if 'ROLU' was
 *   pressed and the first life record if 'ROLD' was pressed. Look
 *   up the client details for the confirmation name.
 *
 *   IF <enter>  pressed  on  the  first life screen and the joint
 *   life is mandatory  (min  on  T5688  = 1), check to see that a
 *   joint life has  been  set  up (READR LIFELNB). If it has not,
 *   force a  "roll  up"  by  blanking  out the screen details and
 *   re-presenting it.
 *
 *   NOTE -  when  displaying  joint  life details, show the joint
 *   life  number  and  relationship  fields.  When displaying the
 *   ordinary life screen, do not show them (i.e. ND and PR).
 *
 *****************************************************************
 * </pre>
 */
public class P5005 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5005");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
//	private String wsaaUpdateFlag = "N";

	private FixedLengthStringData wsaa1stTimeIn = new FixedLengthStringData(1).init("Y");
	private Validator n1stTimeIn = new Validator(wsaa1stTimeIn, "Y");
	protected String wsaaUpdateClts = "N";

	protected FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	protected FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0).init(SPACES);
	protected FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaForenum, 8).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(20).isAPartOf(wsaaForenum, 10, FILLER).init(SPACES);
	private String wsaaDeltJlife = "N";
	protected ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).init(ZERO);
	private String wsaaRollIndc = "N";
	protected String wsaaNewLife = "N";
	private FixedLengthStringData wsaaClient = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaLifeselMrnf = new FixedLengthStringData(1).init(SPACES);
	private String wsaaOptBlankscr = "N";
	private FixedLengthStringData wsaaDuprCheck = new FixedLengthStringData(10).init(SPACES);
//	private FixedLengthStringData wsaaJlduprCheck = new FixedLengthStringData(10).init(SPACES);
	protected FixedLengthStringData wsaaAgeadm = new FixedLengthStringData(1).init(SPACES);

	private FixedLengthStringData wsaaIfDuplicateCl = new FixedLengthStringData(1).init("N");
	private Validator wsaaDuplicateCl = new Validator(wsaaIfDuplicateCl, "Y");
	private FixedLengthStringData wsaaOptind = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaT2240Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT2240Lang = new FixedLengthStringData(1).isAPartOf(wsaaT2240Key, 0);
	private FixedLengthStringData wsaaT2240Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT2240Key, 1);

	private FixedLengthStringData wsaaT6769Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6769Lang = new FixedLengthStringData(1).isAPartOf(wsaaT6769Key, 0);
	private FixedLengthStringData wsaaT6769Bmibasis = new FixedLengthStringData(5).isAPartOf(wsaaT6769Key, 1);
	protected FixedLengthStringData wsaaBmirule = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaHxh = new PackedDecimalData(16, 5);
	protected PackedDecimalData wsaaBmi = new PackedDecimalData(16, 5);
	/* 88 Levels for Underwriting.                                     */
	protected FixedLengthStringData wsaaUnderwritingReqd = new FixedLengthStringData(1).init("N");
	protected Validator underwritingReqd = new Validator(wsaaUnderwritingReqd, "Y");
	private Validator underwritingNotReqd = new Validator(wsaaUnderwritingReqd, "N");

	private FixedLengthStringData wsaaBmiReqd = new FixedLengthStringData(1).init("N");
	protected Validator bmiReqd = new Validator(wsaaBmiReqd, "Y");

	private FixedLengthStringData wsaaBmiValid = new FixedLengthStringData(1).init("N");
	protected Validator bmiNotValid = new Validator(wsaaBmiValid, "N");

	private FixedLengthStringData wsaaOptQuest = new FixedLengthStringData(1).init("N");
	private Validator optQuestFound = new Validator(wsaaOptQuest, "Y");

	private FixedLengthStringData wsaaOptDoct = new FixedLengthStringData(1).init("N");
	private Validator optDoctFound = new Validator(wsaaOptDoct, "Y");

	private FixedLengthStringData wsaaOptMlia = new FixedLengthStringData(1).init("N");
	private Validator optMliaFound = new Validator(wsaaOptMlia, "Y");

	private FixedLengthStringData wsaaMandQuestionsFlag = new FixedLengthStringData(1).init("N");
	private Validator mandQuesExists = new Validator(wsaaMandQuestionsFlag, "Y");

//	private FixedLengthStringData wsaaFoundCheckBox = new FixedLengthStringData(1).init("N");
//	private Validator foundCheckBox = new Validator(wsaaFoundCheckBox, "Y");
	private FixedLengthStringData wsaaQuestset = new FixedLengthStringData(8);
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClprTableDAM clprIO = new ClprTableDAM();
	protected CltsTableDAM cltsIO = new CltsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private LfcllnbTableDAM lfcllnbIO = new LfcllnbTableDAM();
	protected LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private LifematTableDAM lifematIO = new LifematTableDAM();
	private MliasecTableDAM mliasecIO = new MliasecTableDAM();
	private UndlTableDAM undlIO = new UndlTableDAM();
	private UndlpfDAO undlpfDAO = getApplicationContext().getBean("undlpfDAO", UndlpfDAO.class);
//	private UndqTableDAM undqIO = new UndqTableDAM();
	private UndqpfDAO undqpfDAO = getApplicationContext().getBean("undqpfDAO", UndqpfDAO.class);
	private T2240rec t2240rec = new T2240rec();
	protected Cltrelnrec cltrelnrec = new Cltrelnrec();
	protected Optswchrec optswchrec = new Optswchrec();
	protected Agecalcrec agecalcrec = new Agecalcrec();
	private T5688rec t5688rec = new T5688rec();
	private T6769rec t6769rec = new T6769rec();
	private T6772rec t6772rec = new T6772rec();
//	private Tr675rec tr675rec = new Tr675rec();
	protected Tr675rec tr675rec = getTr675rec();

	public Tr675rec getTr675rec() {
		return new Tr675rec();
	}
	private T6771rec t6771rec = new T6771rec();
	private Wssplife wssplife = new Wssplife();
	//private S5005ScreenVars sv = ScreenProgram.getScreenVars( S5005ScreenVars.class);
	private S5005ScreenVars sv =   getLScreenVars();
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private Zctxpf zctxpf=null;
	private ZctxpfDAO zctxpfDAO = getApplicationContext().getBean("zctxpfDAO", ZctxpfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected List<Itempf> itempfList;
	private Itempf itempf = null;
/*	private Itempf itempf1 = null;*/
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
	private List<Clrrpf> clrrpfList;
	private Clrrpf clrrpf = null;
	boolean wsaaEof ;
	Iterator<Clrrpf> iterator ;
	private boolean benesequence = false;
	private boolean riskamtFlag = false;
	private static final String M = "M";
	private static final String C = "C";
	//ICIL-160 deal with the occupation class for Chinalocalization
	protected boolean chinaOccupPermission;
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clntpf clntpf = null;
	private String wsaaoccuCode = "";
	private String wsaalifesel = "";
	private boolean checkChdrFlag = false;
	private T3644rec t3644rec = new T3644rec();
	private T3584rec t3584rec = new T3584rec();
	private T5661rec t5661rec = new T5661rec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
	private Fluppf fluppf;
	private Descpf fupDescpf;
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
    private FluppfDAO fluppfDAO = getApplicationContext().getBean("fluppfDAO", FluppfDAO.class);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData fupno = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData fupOldno = new ZonedDecimalData(2, 0).setUnsigned();
//	private List<Fluppf> fluppfList = new ArrayList<Fluppf>();
	private List<String> followList = new ArrayList<String>();
	private boolean firstFlag = false;
	private boolean entryFlag = false;
	private List<String> t5661List = new ArrayList<String>();	
	private static final String wssaT3584 ="T3584";
	private Map<String, List<Itempf>> t5661Map = null;
	boolean isFollowUpRequired  = false;	//ICIL-1494
	//ILJ-62
	private boolean NBPRP115permission = false;
	private boolean batchEntryperm = false;
	private static final String NBPRP119 ="NBPRP119";
	private NBProposalRec nbproposalRec = new NBProposalRec();
	private ExternalisedRules er = new ExternalisedRules();
	

	private Antisoclkey antisoclkey = new Antisoclkey();
		
	private String lifeAssured="ZLF";
	private AnsopfDAO ansopfDAO = getApplicationContext().getBean("AnsopfDAO", AnsopfDAO.class);
//	private Ansopf ansopf = null;
	private List<Ansopf> ansopflist = new ArrayList<Ansopf>();
	private static final String t5661 = "T5661";
	private static final String T3644 = "T3644";
	private static final String T6772 = "T6772";
	private FixedLengthStringData name = new FixedLengthStringData(240);
	
	private boolean NBPRP117permission = false;
	private boolean uwFlag = false;
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	protected LextTableDAM lextIO = new LextTableDAM();
	private CovtpfDAO covtpfDAO= getApplicationContext().getBean("covtpfDAO",CovtpfDAO.class);
	private UwoccupationUtil uwoccupationUtil = getApplicationContext().getBean("uwoccupationUtil", UwoccupationUtil.class);
	private UWOccupationRec uwOccupationRec = null;
	protected FixedLengthStringData wsaaT5657key = new FixedLengthStringData(6);
	protected FixedLengthStringData wsaaT5657key1 = new FixedLengthStringData(4).isAPartOf(wsaaT5657key, 0).init(SPACES);
	protected FixedLengthStringData wsaaT5657key2 = new FixedLengthStringData(2).isAPartOf(wsaaT5657key, 4).init(SPACES);
	protected T5657rec t5657rec = new T5657rec();
	public static final String UWQUESTIONNAIRECACHE= "UWQUESTIONNAIRE";
	private Clntkey wsaaClntkey = new Clntkey();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {

	
		exit2090, 
		exit2290, 
		exit2490
	}

	public P5005() {
		super();
		screenVars = sv;
		new ScreenModel("S5005", AppVars.getInstance(), sv);
	}

	
	protected S5005ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5005ScreenVars.class);
	}
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			//TMLII-268
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			//TMLII-268
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}


	protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

	protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

	protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

	protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	 * <pre>
	 *  END OF CONFNAME **********************************************
	 *      INITIALISE FIELDS FOR SHOWING ON SCREEN
	 * </pre>
	 */
	protected void initialise1000()
	{
		initialise1010();
		initOccuClass1020();
	}

	protected void initialise1010()
	{
		wsspcomn.typinj.set(SPACES);
		wsspcomn.clntkey.set(SPACES);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			/*       MOVE '3000'              TO WSSP-SECTIONNO        <LA1143>*/
			//goTo(GotoLabel.exit1090);
			return;
		}
		wsaaLifeselMrnf.set(SPACES);
		wsaaOptind.set(SPACES);
		wssplife.trandesc.set(SPACES);
		sv.dataArea.set(SPACES);
		sv.dob.set(varcom.vrcmMaxDate);
		sv.anbage.set(ZERO);
		sv.height.set(ZERO);
		sv.weight.set(ZERO);
		sv.bmi.set(ZERO);
		setFieldsCustomerSpecific();
		sv.dummyOut[varcom.nd.toInt()].set("Y");
		wsaaNewLife = "N";
		wsaaUnderwritingReqd.set("N");
		/*    PERFORM 1200-OPTSWCH-INIT.                           <V4L011>*/
		/*  Retrieve contract header details*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Read T2240 for age definition.                                  */
		wsaaEof = false;

		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.fsuco.toString());
		itempf.setItemtabl(tablesInner.t2240.toString());
		wsaaT2240Key.set(SPACES);
		wsaaT2240Lang.set(wsspcomn.language);
		wsaaT2240Cnttype.set(chdrlnbIO.getCnttype());
		itempf.setItemitem(wsaaT2240Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);


		if (null == itempf) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.fsuco.toString());
			itempf.setItemtabl(tablesInner.t2240.toString());
			/*     MOVE '***'              TO ITEM-ITEMITEM         <AGEBTH>*/
			wsaaT2240Key.set(SPACES);
			wsaaT2240Lang.set(wsspcomn.language);
			wsaaT2240Cnttype.set("***");
			itempf.setItemitem(wsaaT2240Key.toString());
			itemIO.setFunction(varcom.readr);
			itempf = itempfDAO. getItempfRecord(itempf);
		}
		t2240rec.t2240Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isNE(t2240rec.agecode01, SPACES)) {
			sv.zagelit.set(t2240rec.zagelit01);
		}
		else {
			if (isNE(t2240rec.agecode02, SPACES)) {
				sv.zagelit.set(t2240rec.zagelit02);
			}
			else {
				if (isNE(t2240rec.agecode03, SPACES)) {
					sv.zagelit.set(t2240rec.zagelit03);
				}
				/* MTL002 */
				else {
					if (isNE(t2240rec.agecode04, SPACES)) {
						sv.zagelit.set(t2240rec.zagelit04);
					}
				}
				/* MTL002 */		
			}
		}
		sv.zagelitOut[varcom.hi.toInt()].set("N");
		/*  Retrieve life details*/
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)  && isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {	//ILIFE-6911
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*  Release the LIFELNBIO module for use within the system*/
		lifelnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*
		 * fwang3 ICIL-4
		 */
		benesequence = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP087", appVars, "IT"); // fwang3 ICIL-4
		//ILJ-62
		NBPRP115permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP115", appVars, "IT"); 
		NBPRP117permission = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP117", appVars, "IT"); 
		batchEntryperm = FeaConfg.isFeatureExist(wsspcomn.company.toString(), NBPRP119, appVars, "IT"); 
		if (benesequence) {
			if (isEQ(wsspcomn.flag, "I")) {
				sv.relationwithowner.set(lifelnbIO.getRelation());
			} else if (isEQ(wsspcomn.flag, "M")) {
				if (isEQ(lifelnbIO.getRelation(), SPACES)) {
					sv.relationwithowner.set("SELF"); // default is self
				} else {
					sv.relationwithowner.set(lifelnbIO.getRelation());
				}
			} else if (isEQ(wsspcomn.flag, "C") && (lifelnbIO.getRelation()==null || isEQ(lifelnbIO.getRelation(), SPACES))) {//ICIL-357 when create and no review 
				sv.relationwithowner.set("SELF"); // default is self
			} else {
				sv.relationwithowner.set(lifelnbIO.getRelation());
			}
		}
		
		if (NBPRP115permission) {
			if (isEQ(wsspcomn.flag, "I")) {
				sv.relationwithowner.set(lifelnbIO.getRelation());
			} else if (isEQ(wsspcomn.flag, "M")) {
				if (isNE(lifelnbIO.getRelation(), SPACES)) {
					sv.relationwithowner.set(lifelnbIO.getRelation());
				}
			} else if (isEQ(wsspcomn.flag, "C") && (lifelnbIO.getRelation()!=null || isNE(lifelnbIO.getRelation(), SPACES))) {//ICIL-357 when create and no review 
				sv.relationwithowner.set(lifelnbIO.getRelation());
			}
		}
	
		
		//ICIL-160
		chinaOccupPermission = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "NBPRP056", appVars, "IT");
		if(chinaOccupPermission) {
			sv.occupationClassOut[varcom.nd.toInt()].set("Y");
		}	
		riskamtFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "UNWRT001", appVars, "IT");
		if(!riskamtFlag){
			sv.optind04Out[varcom.nd.toInt()].set("Y");
		}
		
		if(riskamtFlag){
			if (wsspcomn.flag.equals(C)){
				sv.optind04Out[varcom.nd.toInt()].set("Y");
				}
			if (wsspcomn.flag.equals(M)){
				sv.optind04Out[varcom.nd.toInt()].set("N");
				}
			}
		
		uwFlag = FeaConfg.isFeatureExist("2", "NBPRP123",appVars, "IT");
		if(uwFlag) {
			sv.uwFlag = 1;
			sv.setIndustryOccuMap(getIndustryOccuMap());
		}else {
			sv.uwFlag = 2;
		}
		
		/*  READ CONTRACT DEFINITION TABLE*/


		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),tablesInner.t5688.toString(),chdrlnbIO.getCnttype().toString(),chdrlnbIO.getOccdate().toInt()); 

		if (itempfList.size() <1 || null == itempfList) {
			scrnparams.errorCode.set(errorsInner.e268);
		}
		else {
			t5688rec.t5688Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
		wsaaMandQuestionsFlag.set("N");
		wsaaBmiReqd.set("N");
		/*  READ UNDERWRITING PRODUCT RULES                                */
		/*    The following code checks whether underwriting is applicable */
		/*    to this product type. If the Contract Type exists on TR675   */
		/*    (Underwriting Product Rules), then the user may switch to    */
		/*    the Underwriting Questionnaire screen.                       */


		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),tablesInner.tr675.toString(),chdrlnbIO.getCnttype().toString(),chdrlnbIO.getOccdate().toInt()); 

		if (itempfList.size() <1 || null == itempfList)
		{
			/* turn OFF the option switching for Questions and disable         */
			/* Height, Weight and BMI fields.                                  */
			sv.optdsc02Out[varcom.nd.toInt()].set("Y");
			sv.optdsc02Out[varcom.pr.toInt()].set("Y");
			sv.optind02Out[varcom.nd.toInt()].set("Y");
			sv.optind02Out[varcom.pr.toInt()].set("Y");
			sv.heightOut[varcom.nd.toInt()].set("Y");
			sv.heightOut[varcom.pr.toInt()].set("Y");
			sv.weightOut[varcom.nd.toInt()].set("Y");
			sv.weightOut[varcom.pr.toInt()].set("Y");
			sv.bmiOut[varcom.nd.toInt()].set("Y");
			sv.bmiOut[varcom.pr.toInt()].set("Y");
			//goTo(GotoLabel.next1070);
			next1070();
			return;
		}
		else {
			wsaaUnderwritingReqd.set("Y");
			setRecDataCustomerSpecific();			
		}
		wsaaQuestset.set(SPACES);
		/* If Underwriting is required on the product and the BMI Basis    */
		/* exists on TR675, read T6769 (Underwriting based on BMI) to get  */
		/* the BMI Factor for the given BMI Basis.                         */
		/* If the BMI Basis = spaces, BMI is not mandatory,                */
		/* hence Height and Weight are not mandatory.                      */
		if (underwritingReqd.isTrue()) {
			if (isNE(tr675rec.bmibasis, SPACES)) {
				wsaaBmiReqd.set("Y");
				readT67691300();
			}
		}
		else {
			wsaaBmiReqd.set("N");
		}
		
		next1070();
		t5661Map = itemDAO.loadSmartTable("IT", wsspcomn.company.toString(), t5661);
		return;
	}
	private Map<String, Map<String, String>> getIndustryOccuMap() {
		
		String fsucoStr = wsspcomn.fsuco.toString();
		List<Itempf> t3628List = itempfDAO.getAllitems("IT", fsucoStr, "T3628");
		List<Itempf> t3644List = itempfDAO.getAllitems("IT", fsucoStr, T3644);
		Map<String,Descpf> descMap = descDAO.getItems("IT", fsucoStr, T3644, "E");
		Map<String, Map<String, String>> result = new HashMap<>();
		if(t3628List != null && !t3628List.isEmpty()) {
			for(Itempf t3628:t3628List) {
				Map<String, String> t3644Map = new TreeMap<>();
				result.put(t3628.getItemitem().trim(), t3644Map);
			}
			if(t3644List !=null && !t3644List.isEmpty()) {
//				for(Itempf t3644:t3644List) {
//					T3644rec t3644rec = new T3644rec();
//					t3644rec.t3644Rec.set(StringUtil.rawToString(t3644.getGenarea()));
//					String industryCode = t3644rec.occupationClass.toString().trim();
//					if(result.containsKey(industryCode)) {
//						result.get(industryCode).put(t3644.getItemitem().trim(), descMap.get(t3644.getItemitem().trim()).getLongdesc().replace("'", "\\'"));
//					}
//				}
				result = getT3644Map(t3644List,descMap,result);
			}
		}
		return result;
	}
	private Map<String, Map<String, String>> getT3644Map(List<Itempf> t3644List,Map<String,Descpf> descMap,Map<String, Map<String, String>> result){
		for(Itempf t3644:t3644List) {
			T3644rec t3644rec = new T3644rec();
			t3644rec.t3644Rec.set(StringUtil.rawToString(t3644.getGenarea()));
			String industryCode = t3644rec.occupationClass.toString().trim();
			if(result.containsKey(industryCode)) {
				result.get(industryCode).put(t3644.getItemitem().trim(), descMap.get(t3644.getItemitem().trim()).getLongdesc().replace("'", "\\'"));
			}
		}
		return result;
	}
	protected void  setRecDataCustomerSpecific(){
		
		tr675rec.tr675Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		
	}

	protected void next1070()
	{
		optswchInit1200();
		if (underwritingNotReqd.isTrue()) {
			sv.heightOut[varcom.nd.toInt()].set("Y");
			sv.heightOut[varcom.pr.toInt()].set("Y");
			sv.weightOut[varcom.nd.toInt()].set("Y");
			sv.weightOut[varcom.pr.toInt()].set("Y");
			sv.bmiOut[varcom.nd.toInt()].set("Y");
			sv.bmiOut[varcom.pr.toInt()].set("Y");
		}
		/*  If joint life not applicable, non-display fields.*/
		if (isEQ(t5688rec.jlifemax, ZERO)) {
			sv.rollitOut[varcom.nd.toInt()].set("Y");
			sv.jlifeOut[varcom.nd.toInt()].set("Y");
		}
		nonDispfieldsCustomerSpecific();
		/*  Display standard fields*/
		sv.chdrnum.set(lifelnbIO.getChdrnum());
		sv.life.set(lifelnbIO.getLife());
		sv.jlife.set(lifelnbIO.getJlife());
		/*  If new life, just show above field*/
		/*  IF FIRST LIFE DEFAULT TO THE CONTRACT OWNER*/
		if (isEQ(lifelnbIO.getLifcnum(), SPACES)) {
			if (isEQ(lifelnbIO.getLife(), "01")) {
				wsaaNewLife = "Y";
				sv.lifesel.set(chdrlnbIO.getCownnum());
				//goTo(GotoLabel.continue1080);
				continue1080();
				return;
			}
			else {
				wsaaNewLife = "Y";
				//goTo(GotoLabel.exit1090);
				return;
			}
		}
		continue1080();
		return;	
	}

	
	protected void nonDispfieldsCustomerSpecific()
	{
		/*  If main life, non-display relationship field.*/
		if (isEQ(lifelnbIO.getJlife(), ZERO)) {
			sv.relationOut[varcom.pr.toInt()].set("Y"); 
			sv.relationOut[varcom.nd.toInt()].set("Y"); 
		}
		
	}
	/**
	 * <pre>
	 *  Look up existing client details
	 * </pre>
	 */
	protected void continue1080()
	{
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		if (isEQ(wsaaNewLife, "Y")) {
			cltsIO.setClntnum(sv.lifesel);
		}
		else {
			cltsIO.setClntnum(lifelnbIO.getLifcnum());
		}
		wsaaClient.set(cltsIO.getClntnum());
		cltsio1900();
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		if (isEQ(cltsIO.getClttype(), "C")) {
			sv.lifesel.set(SPACES);
			sv.lifename.set(SPACES);
		}
		if (isEQ(cltsIO.getClttype(), "P")) {
			z100CheckMliasec();
		}
		readClpr1100();
		sv.clrskind.set(clprIO.getClrskind());
		if (isEQ(wsaaNewLife, "Y")) {
			sv.occup.set(cltsIO.getOccpcode());
			getT3644();
			return ;
		}
		/*  THIS IS MODIFY OR INQUIRE, SO SHOW ALL EXISTING VALUES.*/
		sv.lifesel.set(lifelnbIO.getLifcnum());
		/* MOVE CLTS-CLTSEX            TO S5005-SEX.                    */
		/* MOVE CLTS-CLTDOB            TO S5005-DOB.                    */
		sv.sex.set(lifelnbIO.getCltsex());
		sv.dob.set(lifelnbIO.getCltdob());
		sv.anbage.set(lifelnbIO.getAnbAtCcd());
		sv.selection.set(lifelnbIO.getSelection());
		sv.relation.set(lifelnbIO.getLiferel());
		/* MOVE LIFELNB-AGEADM         TO S5005-AGEADM.                 */
		wsaaAgeadm.set(lifelnbIO.getAgeadm());
		/* if AGEADM not spaces display 'Age admitted'                     */
		/* IF WSAA-AGEADM = SPACES                              <LA3291>*/
		/*     MOVE 'Y'                TO S5005-DUMMY-OUT (ND)  <LA3291>*/
		/* ELSE                                                 <LA3291>*/
		/*     MOVE SPACES             TO S5005-DUMMY-OUT (ND). <LA3291>*/
		if (isEQ(wsaaAgeadm, "Y")) {
			sv.dummyOut[varcom.nd.toInt()].set(SPACES);
		}
		else {
			sv.dummyOut[varcom.nd.toInt()].set("Y");
		}
		sv.smoking.set(lifelnbIO.getSmoking());
		/* MOVE CLTS-OCCPCODE          TO S5005-OCCUP.                  */
		
		sv.occup.set(lifelnbIO.getOccup());
		getT3644();
		sv.pursuit01.set(lifelnbIO.getPursuit01());
		sv.pursuit02.set(lifelnbIO.getPursuit02());
		/* Read the Underwriting Life file UNDL, to get the Height,        */
		/* Weight and BMI for the given Life.                              */
		sv.height.set(ZERO);
		sv.weight.set(ZERO);
		sv.bmi.set(ZERO);
		if (underwritingReqd.isTrue()) {
			undlIO.setParams(SPACES);
			undlIO.setChdrcoy(lifelnbIO.getChdrcoy());
			undlIO.setChdrnum(lifelnbIO.getChdrnum());
			undlIO.setLife(lifelnbIO.getLife());
			undlIO.setJlife(lifelnbIO.getJlife());
			undlIO.setFormat(formatsInner.undlrec);
			undlIO.setFunction(varcom.readr);
			callUndlio6000();
			if (isEQ(undlIO.getStatuz(), varcom.oK)) {
				sv.height.set(undlIO.getHeight());
				sv.weight.set(undlIO.getWeight());
				sv.bmi.set(undlIO.getBmi());
			}
		}
	}

	protected void readClpr1100()
	{
		begin1110();
	}

	protected void begin1110()
	{
		clprIO.setParams(SPACES);
		clprIO.setClntpfx(cltsIO.getClntpfx());
		clprIO.setClntcoy(cltsIO.getClntcoy());
		clprIO.setClntnum(cltsIO.getClntnum());
		clprIO.setFunction(varcom.readr);
		clprIO.setFormat(formatsInner.clprrec);
		SmartFileCode.execute(appVars, clprIO);
		if (isNE(clprIO.getStatuz(), varcom.oK)
				&& isNE(clprIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(clprIO.getStatuz());
			syserrrec.params.set(clprIO.getParams());
			fatalError600();
		}
		if (isEQ(clprIO.getStatuz(), varcom.mrnf)) {
			clprIO.setClrskind(SPACES);
		}
	}

	protected void optswchInit1200()
	{
		call1210();
	}

	protected void call1210()
	{
		/* Perform a KEEPS on the LIFELNB, as it has previously been       */
		/* released, and is needed in the Existence check s/routine.       */
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		/* Call OPTSWCH to retrieve check box text description             */
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			optswchrec.optsItemCompany.set("0");
			syserrrec.function.set("INIT");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		/*    MOVE OPTS-DSC(1)              TO S5005-OPTDSC.       <V4L011>*/
		/*    MOVE OPTS-IND(1)              TO S5005-OPTIND.       <V4L011>*/
		/*    MOVE OPTS-DSC(2)              TO S5005-OPTDSC-02.            */
		/*    MOVE OPTS-IND(2)              TO S5005-OPTIND-02.            */
		sv.optdsc01.set(optswchrec.optsDsc[1]);
		sv.optind01.set(optswchrec.optsInd[1]);
		sv.optdsc02.set(optswchrec.optsDsc[2]);
		sv.optind02.set(optswchrec.optsInd[2]);
		sv.optdsc03.set(optswchrec.optsDsc[3]);
		sv.optind03.set(optswchrec.optsInd[3]);
		if(riskamtFlag){
		sv.optdsc04.set(optswchrec.optsDsc[4]);
		sv.optind04.set(optswchrec.optsInd[4]);
		}
		if (isEQ(optswchrec.optsDsc[1], SPACES)) {
			sv.optind01Out[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(optswchrec.optsDsc[2], SPACES)) {
			sv.optind02Out[varcom.nd.toInt()].set("Y");
		}
		if (isEQ(optswchrec.optsDsc[3], SPACES)) {
			sv.optind03Out[varcom.nd.toInt()].set("Y");
		}
		if(riskamtFlag){
		if (isEQ(optswchrec.optsDsc[4], SPACES)) {
				if (wsspcomn.flag.equals(C)){
					sv.optind04Out[varcom.nd.toInt()].set("Y");
				}
				if (wsspcomn.flag.equals(M)){
					sv.optind04Out[varcom.nd.toInt()].set("N");
				}
			}
		}
		if (underwritingNotReqd.isTrue()) {
			sv.optind02Out[varcom.nd.toInt()].set("Y");
			sv.optind02Out[varcom.pr.toInt()].set("Y");
			sv.optdsc03Out[varcom.nd.toInt()].set("Y");
			sv.optind03Out[varcom.pr.toInt()].set("Y");
		if(riskamtFlag){
			if (wsspcomn.flag.equals(C)){
					sv.optdsc04Out[varcom.nd.toInt()].set("Y");
					sv.optind04Out[varcom.pr.toInt()].set("Y");
			}
			if (wsspcomn.flag.equals(M)){
				sv.optdsc04Out[varcom.nd.toInt()].set("N");
				sv.optind04Out[varcom.pr.toInt()].set("N");
				}
			}
		}
		/* Perform a RLSE  on the LIFELNB, as it was used in the           */
		/* existence check s/routine, and needs releasing for later use    */
		/* in this program.                                                */
		lifelnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}

	}

	protected void readT67691300()
	{

		//smalchi2 for ILIFE-810 STARTS
		wsaaT6769Lang.set(wsspcomn.language);
		wsaaT6769Bmibasis.set(tr675rec.bmibasis);

		itempfList = itempfDAO.getItdmByFrmdate(wsspcomn.company.toString(),tablesInner.t6769.toString(),wsaaT6769Key.toString(),chdrlnbIO.getOccdate().toInt()); 

		if (itempfList.size() <1 || null == itempfList){
			scrnparams.errorCode.set(errorsInner.e031);
		}
		//ENDS
		else {
			t6769rec.t6769Rec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
		}
	}

	/**
	 * <pre>
	 *    Sections performed from the 1000 section above.
	 * </pre>
	 */
	protected void cltsio1900()
	{
		/*CLTSIO*/
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
				&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			cltsIO.setParams(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			/*     MOVE E339               TO S5005-LIFESEL-ERR             */
			sv.lifeselErr.set(errorsInner.h143);
		}
		else {
			/*    IF CLTS-CLTDOD           NOT > CHDRLNB-OCCDATE            */
			if (isLT(cltsIO.getCltdod(), chdrlnbIO.getOccdate())) {
				sv.lifeselErr.set(errorsInner.f782);
			}
		}
	}
	
	//ICIL-160
	protected void initOccuClass1020() {
		if(chinaOccupPermission && isNE(sv.lifesel, SPACES) && isNE(sv.occup, SPACES)) {
			getOccupationClass2900();
			wsaalifesel = sv.lifesel.toString();
            wsaaoccuCode = sv.occup.trim();
		}
	}
	protected void getOccuClass2055() {
		//1. the occupation is changed, need not to load the client message.
		//2. the occupation is not changed, need to load the client message when click fresh button
		if(isNE(wsaaoccuCode, sv.occup.trim()) && !checkChdrFlag) {
			checkChdrFlag = true;
		}
		if(chinaOccupPermission && isNE(sv.lifesel, SPACES) && (isNE(sv.lifesel, wsaalifesel)|| !checkChdrFlag)) {
		 wsaalifesel = sv.lifesel.toString();
		 checkChdrFlag = true;
		 clntpf = new Clntpf();
		 clntpf.setClntcoy(wsspcomn.fsuco.toString());
		 clntpf.setClntpfx(fsupfxcpy.clnt.toString());
		 clntpf.setClntnum( sv.lifesel.trim());//ICIL-225
		 clntpf = clntpfDAO.getCltsRecordByKey(clntpf);
		 if(clntpf !=null) {
		   sv.occup.set(clntpf.getOccpcode());
		   sv.occupationClass.set(clntpf.getOccclass());
		   wsaaoccuCode = sv.occup.trim();//ICIL-227
		  }
		}
		
	}

	/**
	 * <pre>
	 *     RETRIEVE SCREEN FIELDS AND EDIT
	 * </pre>
	 */
	protected void preScreenEdit()
	{
		preStart();
	}

	protected void preStart()
	{
		/*    If entering on enquiry set the function to protect the       */
		/*    screen fields.                                               */
		if (isEQ(wsspcomn.flag, "I")) {
			/*        MOVE PROT               TO SCRN-FUNCTION.         <S9503>*/
			sv.chdrnumOut[varcom.pr.toInt()].set("Y");
			sv.lifeOut[varcom.pr.toInt()].set("Y");
			sv.jlifeOut[varcom.pr.toInt()].set("Y");
			sv.lifeselOut[varcom.pr.toInt()].set("Y");
			sv.sexOut[varcom.pr.toInt()].set("Y");
			sv.dobOut[varcom.pr.toInt()].set("Y");
			sv.anbageOut[varcom.pr.toInt()].set("Y");
			sv.selectionOut[varcom.pr.toInt()].set("Y");
			sv.occupOut[varcom.pr.toInt()].set("Y");
			sv.smokingOut[varcom.pr.toInt()].set("Y");
			sv.pursuit01Out[varcom.pr.toInt()].set("Y");
			sv.pursuit02Out[varcom.pr.toInt()].set("Y");
			sv.heightOut[varcom.pr.toInt()].set("Y");
			sv.weightOut[varcom.pr.toInt()].set("Y");
			sv.bmiOut[varcom.pr.toInt()].set("Y");
			sv.relationOut[varcom.pr.toInt()].set("Y");
			sv.occupationClassOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set("Y");
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (underwritingReqd.isTrue()) {
			if (isEQ(optswchrec.optsType[2], "X")) {
				sv.optdsc02.set(optswchrec.optsDsc[2]);
				sv.optind02.set(optswchrec.optsInd[2]);
			}
			if (isEQ(optswchrec.optsType[3], "X")) {
				sv.optdsc03.set(optswchrec.optsDsc[3]);
				sv.optind03.set(optswchrec.optsInd[3]);
			}
			if(riskamtFlag){
			if (isEQ(optswchrec.optsType[4], "X")) {
				sv.optdsc04.set(optswchrec.optsDsc[4]);
				sv.optind04.set(optswchrec.optsInd[4]);
			}
			}
			if (isEQ(optswchrec.optsInd[2], SPACES)
					&& mandQuesExists.isTrue()) {
				sv.optind02.set("X");
			}
		}
		if (benesequence || NBPRP115permission) {// fwang3
			if (isEQ(wsspcomn.flag, "I")) {
				sv.relationwithownerOut[varcom.pr.toInt()].set("Y");
			}
		} else {
			sv.relationwithownerOut[varcom.nd.toInt()].set("Y");
		}
		return ;
	}

	protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validate2020();
			checkForErrors2080();
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	 * <pre>
	 *2010-VALIDATE-START.                                             
	 * </pre>
	 */
	protected void screenIo2010()
	{
		/*    CALL 'S5005IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5005-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		/*    IF ENTERING ON ENQUIRY SET THE FUNCTION TO PROTECT THE       */
		/*    SCREEN FIELDS.                                               */
		wsspcomn.clntkey.set(SPACES);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
			
		}
		if (isEQ(wsspcomn.flag, "I")) {
			sv.sexOut[varcom.pr.toInt()].set("Y");
			sv.relationOut[varcom.pr.toInt()].set("Y");
			sv.lifeselOut[varcom.pr.toInt()].set("Y");
		}
		wsspcomn.edterror.set(varcom.oK);
		/*    Catering for F11.                                    <V4L011>*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		
		}
		/*    If entering on enquiry, then check scrolling for scrolling.*/
		checkScrolling2200();
		if (isEQ(wsspcomn.flag, "I")) {
			/*       PERFORM 2100-OPTSWCH-CHCK                         <V4L011>*/
			/*       PERFORM 2200-CHECK-SCROLLING                              */
			goTo(GotoLabel.exit2090);
			
		}
		
		getOccuClass2055();
	}

	protected void validate2020()
	{
		/*    Validate SECTION.*/
		validateAll2100();
		/*    If Client = spaces AND Roll-Up pressed, allow roll-up.       */
		if (isEQ(sv.lifesel, SPACES)
				&& isEQ(scrnparams.statuz, varcom.rold)) {
			goTo(GotoLabel.exit2090);
			
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			/*       MOVE 'Y'                 TO WSSP-EDTERROR.                */
			wsspcomn.edterror.set("Y");
			if (bmiReqd.isTrue()
					&& isNE(sv.height, ZERO)
					&& isNE(sv.weight, ZERO)) {
				calculateBmi2900();
			}
		}
		if (isEQ(scrnparams.statuz, varcom.rold)
				|| isEQ(scrnparams.statuz, varcom.rolu)) {
			checkScrolling2200();
		}
		/*       GO TO 2190-EXIT.*/
		if(uwFlag) {
			if (isNE(sv.industry, SPACES) && isEQ(sv.occup, SPACES)) {
				sv.occupErr.set("H366");
			}
			if (isEQ(sv.industry, SPACES) && isNE(sv.occup, SPACES)) {
				sv.industryErr.set("H366");
			}
		}
		optswchChck2100();
	}

	protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}else {//ICIL-227
			if(chinaOccupPermission) {			
				if(sv.occup !=null && !sv.occup.trim().equals("")) {
					getOccupationClass2900();
				}else {
					sv.occupationClass.set(SPACES);
				}
				if(isNE(sv.occup.trim(), wsaaoccuCode)) {	
					if(wsspcomn.language.equals("S")) {
						appVars.addMessage("\\u804C\\u4E1A\\u5DF2\\u7ECF\\u66F4\\u65B0\\u5728\\u5BA2\\u6237\\u7EF4\\u62A4\\u9875\\u9762");
					}else {
					    appVars.addMessage("Occ updated at client maintain");
					}
				}
			}
		}
	}

	protected void validateAll2100()
	{
		validateStart2110();
	}

	protected void validateStart2110()
	{
		/* Retrieve client details when the client has been selected       */
		/* via Client Scroll (ie Function key F4 used).                    */
        
		if (isNE(wsspcomn.clntkey, SPACES)) {
			wsaaClntkey.clntFileKey.set(wsspcomn.clntkey);
			sv.lifesel.set(wsaaClntkey.clntClntnum);
			wsspcomn.clntkey.set(SPACES);
			sv.lifeselOut[varcom.chg.toInt()].set("Y");
		}
		/* If adding a Life & no Client has been input, redisplay          */
		/* screen with message.                                            */
		/*    IF S5005-LIFESEL              = SPACE                <CAS1.0>*/
		/*       MOVE E186                 TO S5005-LIFESEL-ERR    <CAS1.0>*/
		/*       GO TO 2190-EXIT                                   <CAS1.0>*/
		/*    END-IF.                                              <CAS1.0>*/
		/* If the client has changed, set the indicator to ensure          */
		/* an attempt is made to read the new client.                      */
		if (isNE(sv.lifesel, wsaaClient)) {
			wsaaClient.set(sv.lifesel);
			sv.lifeselOut[varcom.chg.toInt()].set("Y");
		}
		else {
			sv.lifeselOut[varcom.chg.toInt()].set("N");
		}
		/* The date of birth field on screen S5005 is optional.*/
		/* If the date of birth is entered on the screen and it is*/
		/* different from what is on the client file (CLTS-CLTDOB) -*/
		/* an error message is produced until the correct date of*/
		/* birth has been entered.*/
		/* Before checking the CLTDOB, make sure a client exists.          */
		if (isEQ(sv.lifeselOut[varcom.chg.toInt()], "Y")) {
			findClient2300();
			if (isEQ(wsaaLifeselMrnf, "Y")) {
				/*        MOVE E339           TO S5005-LIFESEL-ERR      <RA9606>*/
				sv.lifeselErr.set(errorsInner.h143);
				//goTo(GotoLabel.exit2190);
				return;
			}
		}
		if (isNE(cltsIO.getCltdob(), ZERO)
				&& isNE(cltsIO.getCltdob(), varcom.vrcmMaxDate)) {
			if ((isNE(sv.dob, cltsIO.getCltdob()))
					&& (isNE(sv.dob, SPACES))
					&& (isNE(sv.dob, varcom.vrcmMaxDate))) {
				/*         MOVE U020                TO S5005-DOB-ERR             */
				sv.dobErr.set(errorsInner.g819);
			}
		}
		/*        GO TO 2190-EXIT.                                 <009>*/
		/* To verify whether Joint Life is required                        */
		checkMinJlife2400();
		/* If error E598 exists (Joint Life required)                      */
		/* set screen error                                                */
		if (isEQ(scrnparams.errorCode, errorsInner.e598)) {
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit2190);
			return;
		}
		/* If T5688 Joint Life is required AND Client has not been         */
		/* entered AND Page up is pressed - error.                         */
		if (isEQ(sv.lifesel, SPACES)
				&& isGT(t5688rec.jlifemin, 0)
				&& isEQ(scrnparams.statuz, varcom.rold)) {
			sv.lifename.set(SPACES); //ILIFE-4369
			sv.lifeselErr.set(errorsInner.e186);
			//goTo(GotoLabel.exit2190);
			return;
		}
		/*    If Client = spaces AND Roll-Up pressed, allow roll-up.       */
		if (isEQ(sv.lifesel, SPACES)
				&& isEQ(scrnparams.statuz, varcom.rold)) {
			wsaaDeltJlife = "Y";
			//goTo(GotoLabel.exit2190);
			return;
		}
		/* Client number must be entered if ENTER is pressed               */
		if (isEQ(sv.lifesel, SPACES)) {
			sv.lifename.set(SPACES); //ILIFE-4369
			sv.lifeselErr.set(errorsInner.e186);
			//goTo(GotoLabel.exit2190);
			return;
		}
		
		chckVal01CustomerSpecific();
		if (isNE(sv.lifeselOut[varcom.chg.toInt()], "Y")) {
			findClient2300();
			if ((isNE(cltsIO.getCltdob(), ZERO)
					&& isNE(cltsIO.getCltdob(), varcom.vrcmMaxDate))
					&& isNE(cltsIO.getCltdob(), sv.dob)
					&& isEQ(cltsIO.getStatuz(), varcom.oK)
					&& !n1stTimeIn.isTrue()) {
				/*   Check if life already has a contract.*/
				readLfcllnb2125();
				if (isEQ(sv.dob, ZERO)
						|| isEQ(sv.dob, varcom.vrcmMaxDate)
						&& isNE(t5688rec.anbrqd, "R")) {
					sv.dob.set(cltsIO.getCltdob());
				}
				else {
					if (isNE(lfcllnbIO.getStatuz(), varcom.mrnf)) {
						if (isNE(cltsIO.getCltdob(), sv.dob)) {
							sv.dobErr.set(errorsInner.h367);
						}
					}
					else {
						if (isNE(t5688rec.anbrqd, "R")) {
							wsaa1stTimeIn.set("N");
							sv.dob.set(cltsIO.getCltdob());
						}
					}
				}
			}
			else {
				chkValCustomerSpecific();
				
			}
		}
		wsaa1stTimeIn.set("N");
		cltsCheck2120();
		/*
		 * fwang3 ICIL-4
		 */
		if (benesequence) {
			if (isEQ(sv.relationwithowner, SPACE)) {
				sv.relationwithownerErr.set(errorsInner.e186);
			} else {// if owner and assured is same and the relation not 'Self'
				if (isEQ(chdrlnbIO.getCownnum(), sv.lifesel) && isNE(sv.relationwithowner, "SELF")) {
					sv.relationwithownerErr.set("RRBN"); 
				}
			}
			if (isNE(sv.relationwithowner, SPACE)) {
				getWarningMsg();
			}
		}
		
		if(NBPRP115permission) {
			
			nbproposalRec = new NBProposalRec();
			nbproposalRec.setCnttype("");	
			nbproposalRec.setCrtable("");
			nbproposalRec.setNoField(1);
			nbproposalRec.setScreeId("S5005");
			nbproposalRec.getFieldName().add("Relationship with Owner");
			nbproposalRec.getFieldValue().add(sv.relationwithowner.toString());
			
			if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("NBPROPOSALVALD")))
			{
				callProgram("NBPROPOSALVALD",nbproposalRec);
				for(int i = 0; i < nbproposalRec.getOutputFieldName().size() ; i++) {
					if(nbproposalRec.getOutputFieldName().get(i).equals("Relationship with Owner")) {
						if(isNE(nbproposalRec.getOuputErrorCode().get(i),"") && isEQ(sv.relationwithownerErr, SPACES)) {
							wsspcomn.edterror.set("Y");
							sv.relationwithownerErr.set(nbproposalRec.getOuputErrorCode().get(i));	//e186
						}
					}
				}
			}
		}
	}
	protected void getWarningMsg(){
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.fsuco.toString());
		itempf.setItemtabl(wssaT3584);
		itempf.setItemitem(sv.relationwithowner.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if(itempf != null) {
			  t3584rec.t3584Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
			  if(isNE(t3584rec.insurown,SPACES) && isEQ(t3584rec.insurown,"Y")){
				  isFollowUpRequired = true;	//ICIL-1494
			   scrnparams.errorCode.set("RRSV");
			  }	 
	   }
		else{
			t3584rec.t3584Rec.set(SPACES);
		}
		
	}
	protected void readt6772(){
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(T6772);
		itempf.setItemitem(wsaaBmirule.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if(itempf != null) {
			  t6772rec.t6772Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
	   }
		else{
			t6772rec.t6772Rec.set(SPACES);
		}
		for(int ix = 1 ; ix < t6772rec.fupcdes.length ; ix++) { // Working for length equals 6
			if(isNE(t6772rec.fupcdes[ix], SPACE)){
				followList.add(t6772rec.fupcdes[ix].toString());
	
			}
		}
		writeFollowUps3410();
	}
	
	protected void chckVal01CustomerSpecific(){
		/*    Validate client number (if changed)*/
		if (isEQ(sv.lifesel, SPACES)
				&& isEQ(sv.jlife, "00")
				&& isNE(sv.life, "00")) {
			sv.lifename.set(SPACES); //ILIFE-4369
			sv.lifeselErr.set(errorsInner.e186);
			//goTo(GotoLabel.exit2190);
			return;
		}
		else {
			/*    IF S5005-LIFESEL         NOT = SPACES                     */
			/*    AND (S5005-LIFESEL-OUT (CHG) = 'Y'                        */
			/*    OR WSAA-LIFESEL-MRNF     = 'Y')                           */
			/*       PERFORM 2300-FIND-CLIENT                               */
			/*       IF  CLTS-CLTDOB NOT = ZEROS AND NOT = VRCM-MAX-DATE    */
			/*           MOVE CLTS-CLTDOB   TO S5005-DOB                    */
			/*       ELSE                                                   */
			
			if (isNE(cltsIO.getOccpcode(), SPACES) && !chinaOccupPermission && ! uwFlag) {//ICIL-160 In this page, the occupation could be changed
				sv.occup.set(cltsIO.getOccpcode());
			}
		}
	}
	
	protected void chkValCustomerSpecific(){

		if (isNE(cltsIO.getOccpcode(), SPACES) && isEQ(cltsIO.getStatuz(), varcom.oK) && !chinaOccupPermission &&  ! uwFlag) {
			sv.occup.set(cltsIO.getOccpcode());
		}
	}
   protected void getOccupationClass2900() {
	   itempf = new Itempf();
	   itempf.setItempfx("IT");
	   itempf.setItemcoy(wsspcomn.fsuco.toString());
	   itempf.setItemtabl(T3644);
	   itempf.setItemitem(sv.occup.toString());
	   itempf = itempfDAO.getItempfRecord(itempf);
	   if(itempf != null) {
		   t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
		   sv.occupationClass.set(t3644rec.occupationClass);
	   }
   }

	protected void cltsCheck2120()
	{
		//MIBT-143
		/*Clear the values of sv.optind01 and wsaaOptind
	 when cltsIO.getSecuityno() is empty.
	 OR
	 If the client do not have ID then clear the sv.optind01 and wsaaOptind */
		if (isEQ(cltsIO.getSecuityno(), SPACES)) {
			//sv.optind01.set(SPACES);
			wsaaOptind.set(SPACES);
		}
		/*    If Client not found then*/
		/*       If an optional joint life then set flag to skip update*/
		/*          and exit. i.e. ignore request.*/
		/*       Else set 3000- skip flag off as this is a mandatory*/
		/*          joint life.*/
		/* 2400- section is now performed before this IF statement         */
		/* as otherwise WSAA-DELT-JLIFE would never be true (which         */
		/* is why the details were never being deleted.)                   */
		/* This Checking is now moved to earlier part of the section since */
		/* it would be bypassed when the client number is blank.           */
		/* PERFORM 2400-CHECK-MIN-JLIFE                         <A07408>*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			if (isEQ(t5688rec.jlifemin, ZERO)
					&& isNE(sv.jlife, "00")
					&& isEQ(sv.lifesel, SPACES)
					&& isNE(wsaaDeltJlife, "Y")) {
				sv.lifeselErr.set(SPACES);
				wsaaOptBlankscr = "Y";
				//goTo(GotoLabel.exit2190);
				return;
			}
			else {
				wsaaOptBlankscr = "N";
				/*          MOVE LIFELNB-LIFCNUM  TO S5005-LIFESEL*/
				/*          MOVE SPACES           TO S5005-LIFESEL-ERR*/
				if (isEQ(sv.lifesel, SPACES)) {
					sv.lifeselErr.set(SPACES);
				}
				//goTo(GotoLabel.exit2190);
				return;
			}
		}
		else {
			wsaaOptBlankscr = "N";
		}
		if (isNE(cltsIO.getClttype(), "P")) {
			sv.lifeselErr.set(errorsInner.g844);
			//goTo(GotoLabel.exit2190);
			return;
		}
		else {
			if (isNE(cltsIO.getCltind(), "C")) {
				/*MOVE H032             TO S5005-LIFESEL-ERR             */
				sv.lifeselErr.set(errorsInner.g499);
				//goTo(GotoLabel.exit2190);
				return;
			}
		}
		/*       ELSE*/
		/*          MOVE SPACES           TO S5005-LIFESEL-ERR.*/
		if (isEQ(cltsIO.getSecuityno(), SPACES)
				&& isNE(sv.optinds, SPACES)
				&& isNE(sv.optind01, SPACES)) {
			sv.optind01.set(SPACES);
			sv.optind01Err.set(errorsInner.rl34);
		}
		readLfcllnb2125();
		return;

	}


	protected void readLfcllnb2125()
	{
		lfcllnbIO.setParams(SPACES);
		lfcllnbIO.setFunction(varcom.readr);
		lfcllnbIO.setChdrcoy(wsspcomn.fsuco);
		lfcllnbIO.setLifcnum(sv.lifesel);
		SmartFileCode.execute(appVars, lfcllnbIO);

		jointLife2130();
		return;
	}

	protected void jointLife2130()
	{
		/*    Check Joint life exists.*/
		/*    If the delete of an optional Joint life is requested, exit*/
		/*    Else error as delete is not permitted for mandatory joint*/
		/*       life.*/
		checkMinJlife2400();
		if (isEQ(wsaaDeltJlife, "Y")) {
			//goTo(GotoLabel.exit2190);
			return;
		}
		/*  IF SCRN-ERROR-CODE          = U031                           */
		/*  OR S5005-LIFESEL-ERR        = U043                           */
		/*  OR S5005-LIFESEL-ERR        = U044                           */
		if (isEQ(scrnparams.errorCode, errorsInner.h042)
				|| isEQ(sv.lifeselErr, errorsInner.h075)
				|| isEQ(sv.lifeselErr, errorsInner.h076)) {
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit2190);
			return;
		}

		screenValidate2140();
		return;
	}

	protected void screenValidate2140()
	{
		/*    Validate all screen fields for correctness.*/
		validateFields2500();
		if (isEQ(sv.sex, SPACES)) {
			sv.sex.set(cltsIO.getCltsex());
		}
		if (((isEQ(sv.anbage, ZERO)
				|| isEQ(sv.anbage, SPACES))
				&& isEQ(t5688rec.anbrqd, "R"))) {
			sv.anbageErr.set(errorsInner.e186);
			wsspcomn.edterror.set("Y");
			//goTo(GotoLabel.exit2190);
			return;
		}
		/*    Due to windowing the Client must be found if no change in*/
		/*       life number.*/
		/*    Confused? well this is due to the fact that windowing does*/
		/*       not set the change (chg) flag. So we must check just in*/
		/*       case.*/
		if (isNE(sv.lifeselOut[varcom.chg.toInt()], "Y")) {
			findClient2300();
		}

		clientValidate2150();
		return;
	}

	protected void clientValidate2150()
	{
		/*    If client not found then exit*/
		/*    else default the set client details to the screen.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			//goTo(GotoLabel.exit2190);
			return;
		}
		else {
			if (isNE(sv.lifesel, SPACES)
					&& (isNE(sv.lifeselErr, errorsInner.h075)
							|| isNE(sv.lifeselErr, errorsInner.h076))) {
				screenDefault2170();
			}
		}
		/*    Check Age next birthday.*/
		if (((isEQ(sv.anbage, ZERO)
				|| isEQ(sv.anbage, SPACES))
				&& isEQ(t5688rec.anbrqd, "R"))) {
			sv.anbageErr.set(errorsInner.e186);
		}

		clientRoleVal2160();
		return;
	}

	protected void clientRoleVal2160()
	{
		/*    Find the Life's Client Role if it exists.*/
		wsaaIfDuplicateCl.set("N");
		checkRoles2700();
		/*    Make sure that it is not a duplicate client on the           */
		/*    same contract.                                               */
		if (wsaaDuplicateCl.isTrue()) {
			sv.lifeselErr.set(errorsInner.r003);
			//goTo(GotoLabel.exit2190);
			return;
		}
		wsaaForenum.set(clrrpf.getForenum());
		if (isEQ(wsaaChdrnum, lifelnbIO.getChdrnum())) {
			//goTo(GotoLabel.exit2190);
			return;
		}
		else {
			screenVsClnt2180();
		}
		//goTo(GotoLabel.exit2190);
		return;
	}

	protected void screenDefault2170()
	{
		/*    Move Client details to the screen provided there is a life*/
		/*    to which the defaults can be assigned.*/
		/* IF S5005-AGEADM             NOT = SPACES                     */
		/*    PERFORM 2180-SCREEN-VS-CLNT.                              */
		if (isEQ(sv.sex, SPACES)) {
			sv.sex.set(cltsIO.getCltsex());
		}
		chkVal02CustomerSpecific();
		if (isNE(cltsIO.getCltdob(), varcom.vrcmMaxDate)
				&& isEQ(sv.dobErr, SPACES)
				&& isEQ(sv.dob, ZERO)
				|| isEQ(sv.dobErr, varcom.vrcmMaxDate)
				&& isNE(t5688rec.anbrqd, "R")) {
			sv.dob.set(cltsIO.getCltdob());
		}
		/*    ELSE*/
		/*       IF  S5005-DOB            = VRCM-MAX-DATE*/
		/*           MOVE VRCM-MAX-DATE       TO S5005-DOB.*/
		if (isNE(sv.dob, varcom.vrcmMaxDate)
				&& isNE(sv.dob, ZERO)) {
			if (isGT(sv.dob, chdrlnbIO.getOccdate())) {
				sv.dobErr.set(errorsInner.rla7);
			}
		}
		/*    MOVE SPACES                 TO S5005-OCCUP-ERR               */
		/* MOVE SPACES                 TO S5005-ANBAGE-ERR.     <RA9606>*/
		/*    MOVE ZERO                   TO S5005-ANBAGE.*/
		/* IF S5005-DOB                NOT = VRCM-MAX-DATE              */
		calculateAnb2600();
		if (isEQ(sv.jlife, "00")) {
			wsaaDuprCheck.set(sv.lifesel);
		}
		/*  Get the Question Set from TR675                                */
		if (underwritingReqd.isTrue()
				&& isNE(optswchrec.optsInd[2], "+")) {
			if (isEQ(sv.sex, "M")) {
				if (isLTE(sv.anbage, tr675rec.age01)) {
					wsaaQuestset.set(tr675rec.questset01);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age01)
						&& isLTE(sv.anbage, tr675rec.age02)) {
					wsaaQuestset.set(tr675rec.questset03);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age02)
						&& isLTE(sv.anbage, tr675rec.age03)) {
					wsaaQuestset.set(tr675rec.questset05);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age03)
						&& isLTE(sv.anbage, tr675rec.age04)) {
					wsaaQuestset.set(tr675rec.questset07);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age04)
						&& isLTE(sv.anbage, tr675rec.age05)) {
					wsaaQuestset.set(tr675rec.questset09);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age05)
						&& isLTE(sv.anbage, tr675rec.age06)) {
					wsaaQuestset.set(tr675rec.questset11);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
			}
			else {
				if (isLTE(sv.anbage, tr675rec.age01)) {
					wsaaQuestset.set(tr675rec.questset02);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age01)
						&& isLTE(sv.anbage, tr675rec.age02)) {
					wsaaQuestset.set(tr675rec.questset04);
					//(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age02)
						&& isLTE(sv.anbage, tr675rec.age03)) {
					wsaaQuestset.set(tr675rec.questset06);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age03)
						&& isLTE(sv.anbage, tr675rec.age04)) {
					wsaaQuestset.set(tr675rec.questset08);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age04)
						&& isLTE(sv.anbage, tr675rec.age05)) {
					wsaaQuestset.set(tr675rec.questset10);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				if (isGT(sv.anbage, tr675rec.age05)
						&& isLTE(sv.anbage, tr675rec.age06)) {
					wsaaQuestset.set(tr675rec.questset12);
					//goTo(GotoLabel.readT67712171);
					readT67712171();
					return;
				}
				//goTo(GotoLabel.screenVsClnt2180);
				screenVsClnt2180();
				return;
			}
		}
		else {
			//goTo(GotoLabel.screenVsClnt2180);
			screenVsClnt2180();
			return;
		}
	}

	protected void chkVal02CustomerSpecific(){
		if (isEQ(sv.occup, SPACES) && !uwFlag) {
			sv.occup.set(cltsIO.getOccpcode());
//			if (uwFlag && isNE(sv.occup, SPACES)) {
//				itempf = new Itempf();
//				itempf.setItempfx("IT");
//				itempf.setItemcoy(wsspcomn.fsuco.toString());
//				itempf.setItemtabl(T3644);
//				itempf.setItemitem(sv.occup.toString());
//				itempf = itempfDAO.getItempfRecord(itempf);
//				if(itempf != null) {
//					t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
//				}
//				sv.industry.set(t3644rec.occupationClass);
//			}
		}
	}
	protected void readT67712171()
	{
		/* If Underwriting is required on the product and the BMI Basis    */
		/* exists on TR675, read T6769 (Underwriting based on BMI) to get  */
		/* the BMI Factor for the given BMI Basis.                         */
/*		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(tablesInner.t6771);
		itdmIO.setItemitem(wsaaQuestset);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
				&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(), wsspcomn.company)
				|| isNE(itdmIO.getItemtabl(), tablesInner.t6771)
				|| isNE(itdmIO.getItemitem(), wsaaQuestset)
				|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			NEXT_SENTENCE
		}
		else {
			t6771rec.t6771Rec.set(itdmIO.getGenarea());
		}*/
		List<Itempf> items = itemDAO.getItdmByFrmdate(wsspcomn.company.toString(), tablesInner.t6771.toString(), wsaaQuestset.toString(), chdrlnbIO.getOccdate().toInt());
		if (items.isEmpty()) {
			fatalError600();
		}
		t6771rec.t6771Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		
		/* Are there mandatory underwriting questions for the product.     */
		if (isNE(wsspcomn.flag, "I")) {
			if (isEQ(itdmIO.getStatuz(), varcom.oK)) {
				for (wsaaSub.set(1); !(isGT(wsaaSub, 40)); wsaaSub.add(1)){
					if (isEQ(t6771rec.questst[wsaaSub.toInt()], "M")) {
						mandQuesExists.setTrue();
					}
				}
			}
		}
		if(!(batchEntryperm && !mandQuesExists.isTrue())){
			if (underwritingReqd.isTrue()) {
				if (isNE(optswchrec.optsType[2], "+")) {
					sv.optind02.set("X");
				}
			}
		}
		
		screenVsClnt2180();
		return;
	}

	protected void screenVsClnt2180()
	{
		/*    Screen Fields validated against the clients details.*/
		if (isNE(sv.sex, cltsIO.getCltsex())
				&& isNE(cltsIO.getCltsex(), SPACES)
				&& isNE(sv.sex, SPACES)) {
			/*     MOVE U021                TO S5005-SEX-ERR                 */
			sv.sexErr.set(errorsInner.g983);
			sv.sex.set(SPACES);
		}
		/* This piece of code has been moved to the start of the 2000*/
		/* section to ensure that the error message is displayed more*/
		/* than once*/
		/*IF S5005-DOB                NOT = CLTS-CLTDOB                */
		/*AND CLTS-CLTDOB             NOT = VRCM-MAX-DATE              */
		/*AND S5005-DOB               NOT = SPACES                     */
		/*MOVE U020                TO S5005-DOB-ERR.                */
		/*    IF S5005-OPTIND             =     SPACES             <V4L011>*/
		/*       MOVE WSAA-OPTIND         TO    S5005-OPTIND.      <V4L011>*/
		if (isEQ(sv.optind01, SPACES)) {
			sv.optind01.set(wsaaOptind);
		}
	}

	protected void z100CheckMliasec()
	{
		z100Start();
	}

	protected void z100Start()
	{
		if (isEQ(cltsIO.getSecuityno(), SPACES)) {
			return ;
		}
		mliasecIO.setSecurityno(cltsIO.getSecuityno());
		mliasecIO.setFunction(varcom.readr);
		mliasecIO.setFormat(formatsInner.mliasecrec);
		SmartFileCode.execute(appVars, mliasecIO);
		if (isNE(mliasecIO.getStatuz(), varcom.oK)
				&& isNE(mliasecIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mliasecIO.getStatuz());
			syserrrec.params.set(mliasecIO.getParams());
			fatalError600();
		}
		if (isEQ(mliasecIO.getStatuz(), varcom.oK)
				&& isEQ(sv.optind01, SPACES)) {
			sv.optind01.set("+");
			wsaaOptind.set("+");
		}
	}

	protected void optswchChck2100()
	{
		call2110();
	}

	protected void call2110()
	{
		optswchrec.optsFunction.set("CHCK");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		optswchrec.optsSelCode.set("CBOX");
		/*    MOVE S5005-OPTIND             TO OPTS-IND(1).        <V4L011>*/
		/*    MOVE S5005-OPTIND-02          TO OPTS-IND(2).                */
		optswchrec.optsInd[1].set(sv.optind01);
		call2111CustomerSpecific();
		if (underwritingReqd.isTrue()) {
			optswchrec.optsInd[2].set(sv.optind02);
			optswchrec.optsInd[3].set(sv.optind03);
		}
		if(riskamtFlag){
		optswchrec.optsInd[4].set(sv.optind04);
		}
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		/* If the check has detected an error in OPTSWCH a non o-k         */
		/* status is returned. Unfortunately, it doesn't tell you          */
		/* which indicator was wrong! Rather than hard code a check        */
		/* for 'X' the status is moved to both indicators.                 */
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			/*       MOVE OPTS-STATUZ      TO S5005-OPTIND-ERR         <V4L011>*/
			sv.optindErr[1].set(optswchrec.optsStatuz);
			if (underwritingReqd.isTrue()) {
				sv.optindErr[2].set(optswchrec.optsStatuz);
				sv.optindErr[3].set(optswchrec.optsStatuz);
				sv.optindErr[4].set(optswchrec.optsStatuz);
			}
		}
		call2112CustomerSpecific();
	}
	
		
	protected void checkScrolling2200()
	{
		try {
			checkScrolling2210();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void checkScrolling2210()
	{
		wsaaRollIndc = " ";
		if (isEQ(scrnparams.statuz, varcom.rold)) {
			rold2230();
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			rolu2240();
		}
		goTo(GotoLabel.exit2290);
	}

	protected void rold2230()
	{
		if (isEQ(sv.jlife, "00")) {
			scrnparams.errorCode.set(errorsInner.f250);
			wsspcomn.edterror.set("Y");
		}
		else {
			wsaaRollIndc = "D";
		}
	}

	protected void rolu2240()
	{
		if (isEQ(sv.jlife, "01")) {
			scrnparams.errorCode.set(errorsInner.f250);
			wsspcomn.edterror.set("Y");
		}
		else {
			wsaaRollIndc = "U";
		}
	}

	protected void findClient2300()
	{
		checkClient2310();
	}

	protected void checkClient2310()
	{
		cltsIO.setStatuz(varcom.oK);
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.lifesel);
		cltsio1900();
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
			sv.lifename.set(SPACES);
			wsaaLifeselMrnf.set("Y");
			sv.clrskind.set(SPACES);
		}
		else {
			wsaaLifeselMrnf.set("N");
			plainname();
			/*       MOVE WSSP-LONGCONFNAME   TO S5005-LIFENAME.               */
			sv.lifename.set(wsspcomn.longconfname);
			readClpr1100();
			sv.clrskind.set(clprIO.getClrskind());
		}
	}

	protected void checkMinJlife2400()
	{
		try {
			checkMinJlife2410();
			checkFields2420();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	protected void checkMinJlife2410()
	{
		wsaaDeltJlife = "N";
		wsaaOptBlankscr = "N";
		if (isEQ(wsaaDuprCheck, sv.lifesel)
				&& isNE(sv.jlife, "00")) {
			/*     MOVE U043                TO S5005-LIFESEL-ERR             */
			sv.lifeselErr.set(errorsInner.h075);
			goTo(GotoLabel.exit2490);
		}
		getJointLife2800();
		/* IF WSAA-JLDUPR-CHECK        =  S5005-LIFESEL                 */
		if (isEQ(lifematIO.getStatuz(), varcom.oK)
				&& isEQ(lifematIO.getLifcnum(), sv.lifesel)
				&& isEQ(sv.jlife, "00")) {
			/*     MOVE U044                TO S5005-LIFESEL-ERR             */
			sv.lifeselErr.set(errorsInner.h076);
			goTo(GotoLabel.exit2490);
		}
		if (isNE(sv.jlife, "00")) {
			if (isEQ(sv.lifesel, SPACES)) {
				if (isGT(t5688rec.jlifemin, ZERO)) {
					/*           MOVE U031 TO SCRN-ERROR-CODE                        */
					/*           MOVE H042 TO SCRN-ERROR-CODE                <A07408>*/
					scrnparams.errorCode.set(errorsInner.e598);
					goTo(GotoLabel.exit2490);
				}
				else {
					/*          MOVE 'Y'              TO WSAA-DELT-JLIFE.              */
					if (isEQ(lifematIO.getStatuz(), varcom.oK)) {
						wsaaDeltJlife = "Y";
					}
				}
			}
		}
		if (isEQ(t5688rec.jlifemin, ZERO)
				&& isNE(sv.jlife, "00")
				&& isEQ(sv.lifesel, SPACES)) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.exit2490);
		}
	}

	protected void checkFields2420()
	{
		if (isNE(sv.sex, SPACES)) {
			sv.sexErr.set(errorsInner.e374);
		}
		if (isNE(sv.dob, varcom.vrcmMaxDate)) {
			sv.dobErr.set(errorsInner.e374);
		}
		/* IF S5005-AGEADM             NOT = SPACE                      */
		/*    MOVE E374                TO S5005-AGEADM-ERR.             */
		if (isNE(sv.anbage, ZERO)) {
			sv.anbageErr.set(errorsInner.e374);
		}
		if (isNE(sv.selection, SPACES)) {
			sv.selectionErr.set(errorsInner.e374);
		}
		if (isNE(sv.smoking, SPACES)) {
			sv.smokingErr.set(errorsInner.e374);
		}
		if (isNE(sv.occup, SPACES)) {
			sv.occupErr.set(errorsInner.e374);
		}
		if (isNE(sv.pursuit01, SPACES)) {
			sv.pursuit01Err.set(errorsInner.e374);
		}
		if (isNE(sv.pursuit02, SPACES)) {
			sv.pursuit02Err.set(errorsInner.e374);
		}
		if (isNE(sv.relation, SPACES)) {
			sv.relationErr.set(errorsInner.e374);
		}
		checkFieldsCustomerSpecific();
		/*    MOVE 'Y'                    TO WSAA-DELT-JLIFE.              */
		if (isGT(t5688rec.jlifemin, 0)) {
			/*      MOVE U031 TO SCRN-ERROR-CODE.                            */
			scrnparams.errorCode.set(errorsInner.h042);
		}
	}
	private void getT3644(){
		if(uwFlag){
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.fsuco.toString());
			itempf.setItemtabl(T3644);
			itempf.setItemitem(sv.occup.toString());
			itempf = itempfDAO.getItempfRecord(itempf);
			if(itempf != null) {
				t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
				sv.industry.set(t3644rec.occupationClass);
			}
		}
	}
	protected void checkFieldsCustomerSpecific(){
		if (isNE(sv.relation, SPACES)) {
			sv.relationErr.set(errorsInner.e374);
		}
	}
	
	protected void validateFields2500()
	{
		validateFields2510();
	}

	protected void validateFields2510()
	{
		/* if SOE & CLTDOB exist display 'Age admitted'                    */
		if (isEQ(cltsIO.getSoe(), SPACES)
				|| isEQ(cltsIO.getCltdob(), varcom.vrcmMaxDate)
				|| isEQ(cltsIO.getCltdob(), ZERO)) {
			sv.dummyOut[varcom.nd.toInt()].set("Y");
			/*     MOVE SPACES           TO WSAA-AGEADM             <LA3291>*/
			wsaaAgeadm.set("N");
		}
		else {
			sv.dummyOut[varcom.nd.toInt()].set(SPACES);
			/*     IF WSAA-AGEADM = SPACES                          <LA3291>*/
			/*         MOVE 'X'          TO WSAA-AGEADM             <LA3291>*/
			if (isEQ(wsaaAgeadm, "N")
					|| isEQ(wsaaAgeadm, SPACES)) {
				wsaaAgeadm.set("Y");
			}
		}
		/* IF S5005-AGEADM             = SPACES                         */
		/*     MOVE E186               TO S5005-AGEADM-ERR.             */
		/* IF S5005-AGEADM             NOT  = 'Y'                       */
		/* AND S5005-AGEADM            NOT  = 'N'                       */
		/*    MOVE E315                TO S5005-AGEADM-ERR.             */
		if (isEQ(sv.dob, varcom.vrcmMaxDate)) {
			if (isEQ(cltsIO.getCltdob(), varcom.vrcmMaxDate)) {
				sv.dobErr.set(errorsInner.e186);
			}
			else {
				if (isNE(t5688rec.anbrqd, "R")) {
					sv.dob.set(cltsIO.getCltdob());
				}
			}
		}
		if (isEQ(sv.selection, SPACES)) {
			sv.selectionErr.set(errorsInner.e186);
		}
		if (isEQ(sv.occup, SPACES)
				&& isEQ(t5688rec.occrqd, "R")) {
			if (isNE(cltsIO.getOccpcode(), SPACES)) {
				sv.occup.set(cltsIO.getOccpcode());
				getT3644();
			}
			else {
				sv.occupErr.set(errorsInner.e186);
			}
		}
		if (isEQ(sv.smoking, SPACES)
				&& isEQ(t5688rec.smkrqd, "R")) {
			sv.smokingErr.set(errorsInner.e186);
		}
		/*    CHECK RELATIONSHIP  (FOR JOINT LIFE ONLY)*/
		if (isNE(sv.jlife, "00")) {
			if (isEQ(sv.relation, SPACES)) {
				sv.relationErr.set(errorsInner.f658);
			}
		}
		/* check age next birthday.*/
		if (isNE(sv.anbage, ZERO)) {
			wsaaAnb.set(sv.anbage);
			calculateAnb2600();
			if (isNE(sv.anbage, wsaaAnb)) {
				sv.anbage.set(wsaaAnb);
				/*        MOVE U024 TO S5005-ANBAGE-ERR.                         */
				sv.anbageErr.set(errorsInner.h015);
			}
		}
		if (isNE(sv.anbage, ZERO)
				&& isNE(sv.anbage, SPACES)
				&& isEQ(t5688rec.anbrqd, "R")
				&& isNE(sv.anbage, agecalcrec.agerating)) {
			sv.anbageErr.set(errorsInner.e530);
			wsspcomn.edterror.set("Y");
		}
		/* If Height & Weight entered and BMI Required for                 */
		/* Underwriting, calculate BMI.                                    */
		if (bmiReqd.isTrue()) {
			if (isEQ(sv.height, ZERO)) {
				sv.heightErr.set(errorsInner.e186);
				sv.bmi.set(ZERO);
			}
			if (isEQ(sv.weight, ZERO)) {
				sv.weightErr.set(errorsInner.e186);
				sv.bmi.set(ZERO);
			}
		}
		if (bmiReqd.isTrue()
				&& isNE(sv.height, ZERO)
				&& isNE(sv.weight, ZERO)) {
			calculateBmi2900();
		}
		}

	protected void calculateAnb2600()
	{
		calculateAnb2610();
	}

	protected void calculateAnb2610()
	{
		/*    MOVE S5005-DOB              TO DTC3-INT-DATE-1.              */
		/*    MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-2.              */
		/*    MOVE '01'                   TO DTC3-FREQUENCY.               */
		/*    CALL 'DATCON3' USING DTC3-DATCON3-REC.                       */
		/*    IF DTC3-STATUZ              = O-K                            */
		/* Round up the age.                                               */
		/*       ADD .99999 DTC3-FREQ-FACTOR GIVING S5005-ANBAGE.          */
		/*      MOVE DTC3-STATUZ        TO LIFD-ANBAGE-ERR *commented pre*/
		/* New routine to calculate Age next/nearest/last birthday         */
		/* IF S5005-DOB                = VRCM-MAX-DATE          <RA9606>*/
		/*     GO TO 2690-EXIT.                                 <RA9606>*/
		/* Do not calculate the Age next birthday when there is no date    */
		/* of birth to Calculate it with.                                  */
		if (isEQ(sv.dob, varcom.vrcmMaxDate)
				&& (isEQ(cltsIO.getCltdob(), ZERO)
						|| isEQ(cltsIO.getCltdob(), varcom.vrcmMaxDate))) {
			return ;
		}
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.language.set(wsspcomn.language);
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		/* MOVE S5005-DOB              TO AGEC-INT-DATE-1.      <RA9606>*/
		if ((isNE(cltsIO.getCltdob(), varcom.vrcmMaxDate)
				&& isNE(cltsIO.getCltdob(), ZERO))
				&& isEQ(t5688rec.anbrqd, "R")) {
			agecalcrec.intDate1.set(cltsIO.getCltdob());
		}
		else {
			agecalcrec.intDate1.set(sv.dob);
		}
		
		agecalcrec.intDate2.set(chdrlnbIO.getOccdate());
		agecalcrec.company.set(wsspcomn.fsuco);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isEQ(agecalcrec.statuz, "IVFD")) {
			sv.dobErr.set(errorsInner.f401);
			return ;
		}
		if (isNE(agecalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(agecalcrec.statuz);
			fatalError600();
		}
		/* MOVE AGEC-AGERATING         TO S5005-ANBAGE.         <RA9606>*/
		if (isNE(t5688rec.anbrqd, "R")) {
			sv.anbage.set(agecalcrec.agerating);
		}
	}

	protected void checkRoles2700()
	{
					checkRoles2710();
	}

	protected void checkRoles2710()
	{
		//clrrIO.setParams(SPACES);
		//clrrIO.setClntpfx("CN");
		//clrrIO.setClntcoy(wsspcomn.fsuco);
		//clrrIO.setClntnum(sv.lifesel);
		//clrrIO.setClrrrole("LF");
		//clrrIO.setFunction(varcom.begn);
		//clrrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//clrrIO.setFitKeysSearch("CLNTNUM" , "CLRRROLE");
		//clrrIO.setFormat(formatsInner.clrrrec);
	clrrpf = new Clrrpf();
	clrrpf.setForepfx("");
	clrrpf.setForecoy("");
	clrrpf.setForenum("");
	clrrpfList = clrrpfDAO.findClrrClntnum(wsspcomn.fsuco.toString(),sv.lifesel.toString().trim(),"LF");	//ILIFE-6882 trim added with lifesel
	if(null == clrrpfList || clrrpfList.size()<1)
		wsaaEof = true;
	iterator =   clrrpfList.iterator();
		readNext2720();
		return;
	}

	protected void readNext2720()
	{
		//clrrIO.setForepfx(SPACES);
		//clrrIO.setForecoy(SPACES);
		//clrrIO.setForenum(SPACES);
	//	SmartFileCode.execute(appVars, clrrIO);
	//	if (isNE(clrrIO.getStatuz(), varcom.oK)
	//	&& isNE(clrrIO.getStatuz(), varcom.endp)) {
	//		syserrrec.params.set(clrrIO.getParams());
	//		fatalError600();
	//	}
		/*  IF S5005-LIFESEL      NOT  = CLRR-CLNTNUM OR        <A06640>*/
		/*     CLRR-CLRRROLE      NOT  = 'LF'                   <A06640>*/
		/*     MOVE ENDP               TO CLRR-STATUZ           <A06640>*/
		/*     GO TO 2790-EXIT.                                 <A07464>*/
		/*  END-IF.                                             <A06640>*/
		/* Check for a change of key.                                      */
	    if(iterator.hasNext())
			clrrpf = iterator.next();
       else
	   wsaaEof = true;


		if (wsaaEof) {
			clrrpf.setForenum(lifelnbIO.getChdrnum().toString());
			//goTo(GotoLabel.exit2790);
			return;
		}
		/* If CLRR record read 'U'sed to be valid, read the next CLRR rec. */
		if (isNE(clrrpf.getUsed2b(), SPACES)) {
			//goTo(GotoLabel.nextRecord2730);
			readNext2720();
			return;
		}
		if (isEQ(sv.lifesel, clrrpf.getClntnum())) {
			if (isEQ(chdrlnbIO.getChdrnum(), subString(clrrpf.getForenum(), 1, 8))
			&& isNE(sv.life, subString(clrrpf.getForenum(), 9, 2))) {
				wsaaIfDuplicateCl.set("Y");
				/*        MOVE ENDP               TO CLRR-STATUZ.                  */
				//clrrIO.setStatuz(varcom.endp);
				wsaaEof = true;
				//goTo(GotoLabel.exit2790);
				return;
			}
		}
		readNext2720();
		return;
	}

	/**
	 * <pre>
	 ****  Moved this check to after status not = OK/not = ENDP        
	 ****  to prevent looping.                                         
	 ****  IF CLRR-STATUZ             = ENDP                           
	 ****     MOVE LIFELNB-CHDRNUM    TO CLRR-FORENUM                  
	 ****     GO TO 2790-EXIT.                                         
	 ****  MOVE CLRR-FORENUM          TO WSAA-FORENUM.                 
	 ****  IF WSAA-CHDRNUM            NOT = LIFELNB-CHDRNUM            
	 ****     GO TO 2790-EXIT.                                         
	 * </pre>
	 */


	protected void getJointLife2800()
	{
		readLife2810();
	}

	protected void readLife2810()
	{
		lifematIO.setDataArea(SPACES);
		lifematIO.setChdrcoy(wsspcomn.company);
		lifematIO.setChdrnum(sv.chdrnum);
		lifematIO.setLife(sv.life);
		lifematIO.setJlife("01");
		lifematIO.setFunction(varcom.readr);
		lifematIO.setFormat(formatsInner.lifematrec);
		SmartFileCode.execute(appVars, lifematIO);
		if (isNE(lifematIO.getStatuz(), varcom.oK)
				&& isNE(lifematIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(lifematIO.getParams());
			syserrrec.statuz.set(lifematIO.getStatuz());
			fatalError600();
		}
	}

	protected void calculateBmi2900()
	{
		start2910();
	}

	/**
	 * <pre>
	 * Calculate the BMI for the Life, using the Height, Weight        
	 * and BMI Factor.                                                 
	 * </pre>
	 */
	protected void start2910()
	{
		compute(wsaaHxh, 5).set((mult(sv.height, sv.height)));
		compute(wsaaBmi, 5).set((mult((div(sv.weight, wsaaHxh)), t6769rec.bmifact)));
		/* Check that BMI is valid against T6769 (Underwriting             */
		/* based on BMI).                                                  */
		wsaaBmiValid.set("N");
		for (wsaaSub.set(1); !(isGT(wsaaSub, 10)
				|| isEQ(wsaaBmiValid, "Y")); wsaaSub.add(1)){
			if (isLTE(wsaaBmi, t6769rec.bmi[wsaaSub.toInt()])) {
				wsaaBmiValid.set("Y");
				wsaaBmirule.set(t6769rec.undwrule[wsaaSub.toInt()]);
			}
		}
		readt6772();
		start2911CustomerSpecific();
	}
	protected void start2911CustomerSpecific()
	{
	if (bmiNotValid.isTrue()) {
		sv.heightErr.set(errorsInner.e590);
		sv.weightErr.set(errorsInner.e590);
		wsspcomn.edterror.set("Y");
	}
	else {
		sv.bmi.set(wsaaBmi);
	}
	}
	/**
	 * <pre>
	 *     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	 * </pre>
	 */
	protected void update3000()
	{
		updateDatabase3010();
	}

	protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		/*    Catering for F11.                                            */
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		if (isEQ(wsaaOptBlankscr, "Y")) {
			return ;
		}
		/*    If Client = spaces AND Roll-Up pressed, allow roll-up.       */
		wsaaUpdateClts = "N";
		chkScreenChngesCustomerSpecific();
		maintainRoles3200();
		checkInitialValues3300();
		checkChanges3400();
		updates3500();
		if (isFollowUpRequired && isNE(sv.relationwithowner, SPACE) && firstFlag == false) {	//ICIL-1494
			firstFlag = true;
			for(int ix = 1 ; ix < t3584rec.fupcdes.length ; ix++) { // Working for length equals 6
				if(isNE(t3584rec.fupcdes[ix], SPACE)){
					followList.add(t3584rec.fupcdes[ix].toString());
		
				}
			
			}
			writeFollowUps3410();
			
		}
		
		//ILJ
		
		if(NBPRP117permission)
		  {
				
				datcon1rec.function.set(varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				if (isNE(datcon1rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon1rec.datcon1Rec);
					syserrrec.statuz.set(datcon1rec.statuz);
					fatalError600();
				}
				wsaaToday.set(datcon1rec.intDate);
				wsaaT5661Lang.set(wsspcomn.language);
				wsaaT5661Fupcode.set(lifeAssured.trim());
		
				readT5661();
				
				fupDescpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
				
				clntpf = new Clntpf();
				clntpf.setClntpfx("CN");
				clntpf.setClntcoy(wsspcomn.fsuco.toString());
				clntpf.setClntnum(sv.lifesel.toString());
				clntpf = clntpfDAO.getCltsRecordByKey(clntpf); 
				
				initialize(antisoclkey.antisocialKey);
				antisoclkey.kjSName.set(clntpf.getLsurname());
				antisoclkey.kjGName.set(clntpf.getLgivname());
				antisoclkey.clntype.set(clntpf.getClttype());

				
				callProgram(Antisocl.class, antisoclkey.antisocialKey);
				
				List<Fluppf> fluppfobj = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
				
				if (isNE(antisoclkey.statuz, varcom.oK)) {
					
					
					fupOldno.set(0);
					fupno.set(0);
					
					if(fluppfobj.size()>0)
					{
						
						int i=0;
				    	for(Fluppf fluppf: fluppfobj)
					    {
					    	if(isEQ(fluppf.getFupCde().trim(),lifeAssured.trim()))	
					     	{
					    		i++;
					    		fluppfDAO.deleteRecordbyCode(fluppf);
								List<Fluppf> fluppfobj1 = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
								if(fluppfobj1.size()>=1)
								{
								  fupOldno.set(fluppfobj1.get(fluppfobj1.size()-1).getFupNo());
								  fupno.add(fupOldno);
								}
								else
								{
									fupno.add(fupOldno);
								}
					    		updateFluppf();
					    		break;
						    }
					    }
					    	
					    	if(i==0)
					    	{
					    		fupOldno.set(fluppfobj.get(fluppfobj.size()-1).getFupNo());
								fupno.add(fupOldno);
								updateFluppf();
					    	}
				    	
					}
					else
					{
						fupno.add(fupOldno);
						updateFluppf();
					}
					
					
			  }
				
				else
				{
					if(fluppfobj.size()>0)
					{
						for(Fluppf fluppf: fluppfobj)
					    {
					    	if(isEQ(fluppf.getFupCde().trim(),lifeAssured.trim()))	
					     	{
					    		fluppfDAO.deleteRecordbyCode(fluppf);
					     	}
					    }
					}
				}
		  }
		if(uwFlag && (isEQ(wsspcomn.flag,"M") || isEQ(wsspcomn.flag,"C"))){
			preUpdateLext();
		}
	}
	private void preUpdateLext(){
		if(isNE(sv.optind01, "X") && isNE(sv.optind02, "X")&&isNE(sv.optind03, "X")
				&&isNE(sv.optind04, "X")){
			if(isEQ(wsaaUpdateClts, "Y")){
				updateLext();
			}
		}
	}
	private void updateLext(){
		lextpfDAO.deleteByAutoRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(),"O");
		List<Covtpf> covtpfList=covtpfDAO.searchCovtRecordByCoyNum(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		int index = lextpfDAO.getMaxSeqnbr(chdrlnbIO.getChdrcoy().toString(),chdrlnbIO.getChdrnum().toString());
		for(Covtpf covtpf : covtpfList){
			if(isNE(sv.industry,SPACE) || isNE(sv.occup,SPACE)){
				updateLextByOccp(covtpf,index+1);
				index += 1;
			}
		}
	}
	private void updateLextByOccp(Covtpf covtpf,int index){
		uwOccupationRec = new UWOccupationRec();
		uwOccupationRec.transEffdate.set(datcon1rec.intDate.getbigdata());
		uwOccupationRec.occupCode.set(sv.occup);
		uwOccupationRec.indusType.set(sv.industry);
		uwOccupationRec = uwoccupationUtil.getUWOccupationRec(uwOccupationRec, chdrlnbIO.getChdrcoy().toString(),
				wsspcomn.fsuco.toString(), chdrlnbIO.getCnttype().toString(), covtpf.getCrtable().toString().trim());
		if (uwOccupationRec != null && isNE(uwOccupationRec.outputUWDec, "Accepted")
				&& isNE(uwOccupationRec.outputUWDec, SPACE)
				&& isNE(uwOccupationRec.outputUWDec, "0")) {
			lextIO.setParams(SPACES);
			lextIO.setChdrcoy(covtpf.getChdrcoy());
			lextIO.setChdrnum(covtpf.getChdrnum());
			lextIO.setLife(covtpf.getLife());
			lextIO.setCoverage(covtpf.getCoverage());
			lextIO.setRider(covtpf.getRider());
			lextIO.setFormat("LEXTREC");
			lextIO.setSeqnbr(index);
			lextIO.setValidflag("1");
			lextIO.setCurrfrom(chdrlnbIO.getOccdate());
			lextIO.setCurrto(varcom.vrcmMaxDate);
			lextIO.setUwoverwrite("Y");
			lextIO.setAgerate(uwOccupationRec.outputSplTermAge);
			lextIO.setInsprm(uwOccupationRec.outputSplTermRateAdj);
			lextIO.setOppc(uwOccupationRec.outputSplTermLoadPer);
			lextIO.setOpcda(uwOccupationRec.outputSplTermCode);
			lextIO.setZnadjperc(uwOccupationRec.outputSplTermSAPer);
			lextIO.setZmortpct(uwOccupationRec.outputMortPerc);
			lextIO.setExtCessTerm(SPACE);
			lextIO.setPremadj(SPACE);
			lextIO.setReasind(SPACE);
			lextIO.setJlife("00");
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(covtpf.getChdrcoy());
			itemIO.setItemtabl("T5657");
			wsaaT5657key1.set(covtpf.getCrtable());
			wsaaT5657key2.set(uwOccupationRec.outputSplTermCode);
			itemIO.setItemitem(wsaaT5657key);
			itemIO.setFunction(varcom.readr);
			itemIO.setFormat(itemrec);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			t5657rec.t5657Rec.set(itemIO.getGenarea());
			lextIO.setSbstdl(t5657rec.sbstdl);
			lextIO.setTermid(varcom.vrcmTermid);
			lextIO.setTransactionDate(varcom.vrcmDate);
			lextIO.setTransactionTime(varcom.vrcmTime);
			lextIO.setUser(varcom.vrcmUser);
			lextIO.setFunction(varcom.updat);
			SmartFileCode.execute(appVars, lextIO);
			if (isNE(lextIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(lextIO.getParams());
				fatalError600();
			}
		}
	}
	protected void readT5661()
	{
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);

		if (itempf == null) {
			syserrrec.params.set("IT".concat(wsspcomn.company.toString()).concat(t5661).concat(wsaaT5661Key.toString()));
			fatalError600();
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));

	}
	
	protected void updateFluppf()
	{
		name.set(clntpf.getLsurname().trim().replace(".", "").replace("-", "").concat(clntpf.getLgivname().trim().replace(".", "").replace("-", "")));
		ansopflist = ansopfDAO.getRecord(name.toString().trim());
		
		if(ansopflist.size()>0)
		{
			for(Ansopf ansopf : ansopflist)
			 {
				
				fupno.add(1);
				fluppf = new Fluppf();
				
				fluppf.setChdrcoy(wsspcomn.company.charat(0));
				fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
				fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
				fluppf.setFupTyp(clntpf.getClttype().charAt(0));
				fluppf.setLife("01");
				fluppf.setjLife("");
				fluppf.setFupNo(fupno.toInt());
				fluppf.setFupCde(lifeAssured.trim());
				fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
				fluppf.setFupDt(wsaaToday.toInt());
				fluppf.setFupRmk(fupDescpf.getLongdesc().trim() + ansopf.getAnsonum() +"/"+sv.lifesel);
				fluppf.setClamNum("");
				fluppf.setUserT(varcom.vrcmUser.toInt());
				fluppf.setTermId(varcom.vrcmTermid.toString());
				fluppf.setTrdt(varcom.vrcmDate.toInt());
				fluppf.setTrtm(varcom.vrcmTime.toInt());
				fluppf.setzAutoInd(' ');
				fluppf.setUsrPrf(varcom.vrcmUser.toString());
				fluppf.setJobNm(wsspcomn.userid.toString());
				fluppf.setEffDate(wsaaToday.toInt());
				fluppf.setCrtUser(varcom.vrcmUser.toString());
				fluppf.setCrtDate(wsaaToday.toInt());
				fluppf.setLstUpUser(varcom.vrcmUser.toString());
				fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
				fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
				fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
				fluppfDAO.insertFlupRecord(fluppf);
						 
			}
		}
	}
	
	protected void writeFollowUps3410()
	{
		 //to be set
		List<Fluppf> fluppfobj = fluppfDAO.searchFlupRecordByChdrNum(wsspcomn.company.toString(), chdrlnbIO.getChdrnum().toString());
		fupOldno.set(fluppfobj.size());
		if(followList!=null && !followList.isEmpty()){
			for(String followup : followList ){
				wsaaT5661Lang.set(wsspcomn.language);
				wsaaT5661Fupcode.set(followup);
				if (!t5661Map.containsKey(wsaaT5661Key)) {
		            syserrrec.params.set("t5661:" + wsaaT5661Key);
		            fatalError600();
		        }
		        Itempf item5661 = t5661Map.get(wsaaT5661Key).get(0);
		        t5661rec.t5661Rec.set(StringUtil.rawToString(item5661.getGenarea()));
		        for(int iy = 1 ; iy <= 10; iy++){
					if(isNE(t5661rec.fuposs[iy], SPACE))
						t5661List.add(t5661rec.fuposs[iy].toString());	//ICIL-1494
				}
				entryFlag=false;
				if(followup.trim().length() > 0 && fluppfobj != null && !fluppfobj.isEmpty()){
					//if(isLT(fupOldno,fluppfobj.size()))
					//if(isGT(fupOldno,ZERO))
						//fupOldno.add(1);
					for(Fluppf followObj : fluppfobj ){					
						if(isEQ(followObj.getFupCde(),followup.trim())) {
							entryFlag=true;
							if(t5661List.contains(followObj.getFupSts()))
							{
								fupno.set(fupOldno);
								writeFollowUp3500(followup);
								break;
							}
						}
					}
					
				}	
				if(!entryFlag){
					if(followup.trim().length() > 0)
					{
						fupno.set(fupOldno);
						writeFollowUp3500(followup);
						fupOldno.set(fupno);
					}		
				}
			}
			/*if(fluppfList != null && !fluppfList.isEmpty())
				fluppfDAO.insertFlupRecord(fluppfList);*/
		}
	}

	protected void writeFollowUp3500(String followup)
	{
		try {
			fluppf = new Fluppf();
			lookUpStatus3510(followup);
			writeRecord3530(followup);
		}
		catch (Exception e){
			/* Expected exception for control flow purposes. */
		}
	}
	protected void lookUpStatus3510(String followup)
	{

		
		/*itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(t5661);
		itempf.setItemitem(wsaaT5661Key.toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if (null == itempf) {
			return;
		}
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));*/
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
		fupDescpf=descDAO.getdescData("IT", t5661, wsaaT5661Key.toString(), wsspcomn.company.toString(), wsspcomn.language.toString());
	}


private void writeRecord3530(String followup) 
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		fupno.add(1);
		fluppf.setChdrcoy(wsspcomn.company.toString().charAt(0));
		fluppf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		fluppf.setFupNo(fupno.toInt());
		fluppf.setLife("01");
		fluppf.setTranNo(chdrlnbIO.getTranno().toInt());
		fluppf.setFupDt(wsaaToday.toInt());
		fluppf.setFupCde(followup.trim());
		fluppf.setFupTyp('P');
		fluppf.setFupRmk(fupDescpf.getLongdesc());/* IJTI-1523 */
		fluppf.setjLife("00");
		fluppf.setTrdt(varcom.vrcmDate.toInt());
		fluppf.setTrtm(varcom.vrcmTime.toInt());
		fluppf.setUserT(varcom.vrcmUser.toInt());
		fluppf.setEffDate(wsaaToday.toInt());
		fluppf.setCrtUser(varcom.vrcmUser.toString());
		fluppf.setCrtDate(wsaaToday.toInt());
		fluppf.setLstUpUser(varcom.vrcmUser.toString());
		fluppf.setzLstUpDt(varcom.vrcmMaxDate.toInt());
		fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
		fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
		fluppfDAO.insertFlupRecord(fluppf);
	}

	/**
	 * <pre>
	 *    Sections performed from the 3000 section above.
	 * </pre>
	 */
	protected void maintainRoles3200()
	{
					maintainRoles3220();
	
	}

	protected void maintainRoles3220()
	{
		if (isEQ(sv.lifesel, lifelnbIO.getLifcnum())) {
			//goTo(GotoLabel.exit3200);
			return;
		}
		if (isEQ(lifelnbIO.getLifcnum(), SPACES)) {
			//goTo(GotoLabel.addRole3240);
			addRole3240();
			return;
		}
		if (isEQ(wsaaNewLife, "Y")) {
			wsaaNewLife = "N";
			//goTo(GotoLabel.addRole3240);
			addRole3240();
			return;
		}
		deltRole3230();
		return;
	}

	/**
	 * <pre>
	 *  DELETE PREVIOUS OWNER FROM THE CLIENT ROLE FILE
	 *  ALSO DELETE IF A JOINT LIFE IS BEING DELETED
	 *  Do not Physically delete record, but change 'USED-TO-BE'       
	 *  status to 'U'.                                                 
	 * </pre>
	 */
	protected void deltRole3230()
	{
		wsaaForenum.set(SPACES);
		cltrelnrec.data.set(SPACES);
		/*MOVE 'DEL  '                TO CLRN-FUNCTION.                */
		cltrelnrec.function.set("REM  ");
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(lifelnbIO.getLifcnum());
		cltrelnrec.clrrrole.set("LF");
		cltrelnrec.forepfx.set(chdrlnbIO.getChdrpfx());
		cltrelnrec.forecoy.set(chdrlnbIO.getChdrcoy());
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(chdrlnbIO.getChdrnum());
		wsaaLife.set(lifelnbIO.getLife());
		cltrelnrec.forenum.set(wsaaForenum);
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		/*    IF CLRR-STATUZ              = MRNF*/
		/*       GO TO 3235-CONTINUE.*/
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			fatalError600();
		}
		continue3235();
		return;
	}

	protected void continue3235()
	{
		if (isEQ(wsaaDeltJlife, "Y")) {
			//goTo(GotoLabel.exit3200);
			return;
		}
		addRole3240();
		return;
	}

	protected void addRole3240()
	{
		wsaaForenum.set(SPACES);
		cltrelnrec.data.set(SPACES);
		cltrelnrec.clrrrole.set("LF");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(wsspcomn.company);
		wsaaForenum.set(SPACES);
		wsaaChdrnum.set(lifelnbIO.getChdrnum());
		wsaaLife.set(lifelnbIO.getLife());
		cltrelnrec.forenum.set(wsaaForenum);
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(sv.lifesel);
		cltrelnrec.function.set("ADD  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(cltrelnrec.statuz);
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			fatalError600();
		}
	}

	protected void checkInitialValues3300()
	{
		para3301();
		continue3320();
	}

	protected void para3301()
	{
		/*    IF THE LIFELNB DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		/*      SET UP THE KEY*/
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(sv.chdrnum);
		lifelnbIO.setLife(sv.life);
		lifelnbIO.setJlife(sv.jlife);
		lifelnbIO.setTranno(chdrlnbIO.getTranno());
		lifelnbIO.setCurrfrom(chdrlnbIO.getCurrfrom());
		lifelnbIO.setCurrto(chdrlnbIO.getCurrto());
		lifelnbIO.setValidflag("3");
		lifelnbIO.setLifeCommDate(chdrlnbIO.getOccdate());
		lifelnbIO.setTransactionDate(varcom.vrcmDate);
		lifelnbIO.setTransactionTime(varcom.vrcmTime);
		lifelnbIO.setUser(varcom.vrcmUser);
	}

	protected void continue3320()
	{
		lifelnbIO.setLifcnum(sv.lifesel);
		lifelnbIO.setCltsex(sv.sex);
		lifelnbIO.setCltdob(sv.dob);
		if (isEQ(sv.dob, SPACES)
				|| isEQ(sv.dob, varcom.vrcmMaxDate)) {
			lifelnbIO.setCltdob(cltsIO.getCltdob());
		}
		lifelnbIO.setAnbAtCcd(sv.anbage);
		lifelnbIO.setSelection(sv.selection);
		lifelnbIO.setLiferel(sv.relation);
		/* MOVE S5005-AGEADM           TO LIFELNB-AGEADM.               */
		lifelnbIO.setAgeadm(wsaaAgeadm);
		lifelnbIO.setSmoking(sv.smoking);
		if (isNE(sv.occup, SPACES)) {
			if (isNE(sv.occup, lifelnbIO.getOccup())) {
				if (isEQ(lifelnbIO.getOccup(), SPACES)) {
					lifelnbIO.setOccup(sv.occup);
				}
			}
		}
		lifelnbIO.setPursuit01(sv.pursuit01);
		lifelnbIO.setPursuit02(sv.pursuit02);
	}

	protected void checkChanges3400()
	{
		para3401();
	}

	protected void para3401()
	{
		if (isNE(sv.sex, cltsIO.getCltsex())) {
			cltsIO.setCltsex(sv.sex);
			wsaaUpdateClts = "Y";
		}
		if (isNE(sv.occup, cltsIO.getOccpcode())
				&& isNE(sv.occup, SPACES)) {
			if (isEQ(cltsIO.getOccpcode(), SPACES)) {
				cltsIO.setOccpcode(sv.occup);
				wsaaUpdateClts = "Y";
			}
			if(chinaOccupPermission) {
				cltsIO.setOccpcode(sv.occup);
				cltsIO.setOccclass(sv.occupationClass);
				wsaaUpdateClts = "Y";
			}
		}
		if (isNE(sv.occup, cltsIO.getOccpcode()) && uwFlag) {
			cltsIO.setOccpcode(sv.occup);
			wsaaUpdateClts = "Y";
		}
		if (isNE(sv.dob, cltsIO.getCltdob())) {
			if (isEQ(cltsIO.getCltdob(), ZERO)
					|| isEQ(cltsIO.getCltdob(), varcom.vrcmMaxDate)) {
				cltsIO.setCltdob(sv.dob);
				wsaaUpdateClts = "Y";
			}
		}
	}

	protected void updates3500()
	{
		updates3520();
	}

	protected void updates3520()
	{
		if (benesequence || NBPRP115permission) {//fwang3 ICIL-4
			lifelnbIO.setRelation(sv.relationwithowner);
		}
		if(chinaOccupPermission){
			lifelnbIO.setOccup(sv.occup);
		}
		if (isEQ(wsaaDeltJlife, "Y")) {
/*			lifelnbIO.setDataKey(SPACES);
			         MOVE WSSP-FSUCO         TO LIFELNB-CHDRCOY
			lifelnbIO.setChdrcoy(wsspcomn.company);
			lifelnbIO.setChdrnum(sv.chdrnum);
			lifelnbIO.setLife(sv.life);
			lifelnbIO.setJlife(sv.jlife);
			lifelnbIO.setFunction(varcom.readh);
			callLifelnbio5000();
			if (isNE(lifelnbIO.getStatuz(), varcom.oK)
					&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(lifelnbIO.getParams());
				fatalError600();
			}*/
			int rows = lifepfDAO.deleteLifeRecord(wsspcomn.company.toString(), sv.chdrnum.toString(), sv.life.toString(), sv.jlife.toString());
			//if (isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			if (rows > 0) {
				//lifelnbIO.setFunction(varcom.delet);
//				callLifelnbio5000();
				deleteQuestions7000();
				deleteUndLife8000();
			}
			return ;
		}
		lifelnbIO.setFunction(varcom.rlse);
		callLifelnbio5000();
		/* CALL FOR UPDAT*/
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.updat);
		callLifelnbio5000();
		if (isEQ(wsaaUpdateClts, "Y")) {
			cltsIO.setFunction(varcom.updat);
			if(uwFlag){
				cltsIO.setOccpcode(sv.occup);
				cltsIO.setStatcode(sv.industry);
			}
			callCltsio3900();
		}
		/* Update the Underwriting Life File UNDL.                         */
		if (underwritingReqd.isTrue()) {
			undlIO.setParams(SPACES);
			undlIO.setChdrcoy(wsspcomn.company);
			undlIO.setChdrnum(sv.chdrnum);
			undlIO.setLife(sv.life);
			undlIO.setJlife(sv.jlife);
			undlIO.setFunction(varcom.readh);
			callUndlio6000();
			if (isEQ(undlIO.getStatuz(), varcom.mrnf)) {
				undlIO.setParams(SPACES);
				undlIO.setClntnum(1, SPACES);
				undlIO.setClntnum(2, SPACES);
				undlIO.setClntnum(3, SPACES);
				undlIO.setClntnum(4, SPACES);
				undlIO.setClntnum(5, SPACES);
				undlIO.setChdrcoy(wsspcomn.company);
				undlIO.setChdrnum(sv.chdrnum);
				undlIO.setLife(sv.life);
				undlIO.setJlife(sv.jlife);
				undlIO.setHeight(sv.height);
				undlIO.setWeight(sv.weight);
				undlIO.setBmi(wsaaBmi);
				undlIO.setCurrfrom(lifelnbIO.getCurrfrom());
				undlIO.setCurrto(lifelnbIO.getCurrto());
				undlIO.setTranno(lifelnbIO.getTranno());
				undlIO.setBmibasis(tr675rec.bmibasis);
				undlIO.setBmirule(wsaaBmirule);
				undlIO.setValidflag("3");
				undlIO.setUndwflag("N");
				undlIO.setFormat(formatsInner.undlrec);
				undlIO.setFunction(varcom.writr);
				callUndlio6000();
			}
			else {
				undlIO.setHeight(sv.height);
				undlIO.setWeight(sv.weight);
				undlIO.setBmi(wsaaBmi);
				undlIO.setBmirule(wsaaBmirule);
				if (isEQ(undlIO.getUndwflag(), SPACES)) {
					undlIO.setUndwflag("N");
				}
				undlIO.setFunction(varcom.updat);
				callUndlio6000();
			}
		}
		zctxpf=zctxpfDAO.readRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if(zctxpf!=null && isNE(sv.lifesel,zctxpf.getClntnum())){
			zctxpf.setClntnum(sv.lifesel.toString());
			boolean check=zctxpfDAO.updateClient(zctxpf);}
	}

	protected void callCltsio3900()
	{
		/*PARA*/
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(sv.lifesel);
		cltsIO.setFormat(formatsInner.cltsrec);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.dbparams.set(cltsIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	 * <pre>
	 *     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	 * </pre>
	 */
	protected void whereNext4000()
	{
		nextProgram4100();
	}

	protected void nextProgram4100()
	{
		/*    IF   WSSP-SEC-ACTN (WSSP-PROGRAM-PTR) = '*'          <V4L011>*/
		/*         MOVE RETRV             TO LIFELNB-FUNCTION      <V4L011>*/
		/*         PERFORM 5000-CALL-LIFELNBIO                     <V4L011>*/
		/*         MOVE RLSE              TO LIFELNB-FUNCTION      <V4L011>*/
		/*         PERFORM 5000-CALL-LIFELNBIO                     <V4L011>*/
		/*         PERFORM 4800-OPTSWCH-CALL                       <V4L011>*/
		/*         GO TO 4090-EXIT.                                <V4L011>*/
		/*    Catering for F11.                                            */
		appVars.clearMessages();
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			//goTo(GotoLabel.exit4090);
			return;
		}
		/*  ROLL UP TO DISPLAY JOINT LIFE*/
		wsspcomn.nextprog.set(wsaaProg);
		scrnparams.positionCursor.set("LIFESEL");
		/* If either option switch is found....                            */
		wsaaOptQuest.set("N");
		wsaaOptMlia.set("N");
		checkBoxes4500();
		if (optQuestFound.isTrue()
				|| optMliaFound.isTrue()) {
			lifelnbIO.setFunction(varcom.keeps);
			lifelnbIO.setFormat(formatsInner.lifelnbrec);
			callLifelnbio5000();
		}
		/* call OPTSWCH, so that the Program Stack                         */
		/* gets set/reset correctly.                                       */
		if (isEQ(sv.lifesel, SPACES)
				&& isEQ(wsaaRollIndc, "D")) {
			//goTo(GotoLabel.skip4110);
			skip4110();
			return;
		}
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
				&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			syserrrec.function.set("STCK");
			syserrrec.dbioStatuz.set(optswchrec.optsStatuz);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			syserrrec.iomod.set("OPTSWCH");
			fatalError600();
		}
		if (isEQ(optswchrec.optsStatuz, varcom.endp)) {
			wsspcomn.nextprog.set(scrnparams.scrname);
			lifelnbIO.setFunction(varcom.rlse);
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(lifelnbIO.getParams());
				syserrrec.statuz.set(lifelnbIO.getStatuz());
				fatalError600();
			}
			//goTo(GotoLabel.exit4090);
			return;
		}

		skip4110();
		return;
	}

	protected void skip4110()
	{
		/*  ROLL UP TO DISPLAY JOINT LIFE                                  */
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.positionCursor.set("OPTIND01");
		}
		else {
			scrnparams.positionCursor.set("LIFESEL");
		}
		/*    MOVE 'LIFESEL'              TO SCRN-POSITION-CURSOR.         */
		if (optQuestFound.isTrue()
				|| optMliaFound.isTrue()) {
			//goTo(GotoLabel.nextProgram4300);
			nextProgram4300();
			return;

		}
		if (isEQ(wsaaRollIndc, "U")) {
			if (isEQ(t5688rec.jlifemin, 0)
					&& isEQ(t5688rec.jlifemax, 0)) {
				//goTo(GotoLabel.nextProgram4300);
				nextProgram4300();
				return;
			}
			else {
				sv.outputIndicators.set(SPACES);
				//goTo(GotoLabel.clearScreen4150);
				clearScreen4150();
				return;
			}
		}
		/*  ROLL DOWN TO DISPLAY FIRST LIFE*/
		if (isEQ(wsaaRollIndc, "D")) {
			lifelnbIO.setJlife("00");
			lifelnbIO.setFunction("READR");
			callLifelnbio5000();
			sv.relationOut[varcom.pr.toInt()].set("Y");
			sv.relationOut[varcom.nd.toInt()].set("Y");
			sv.dummyOut[varcom.nd.toInt()].set("Y");
			//goTo(GotoLabel.redisplay4200);
			redisplay4200();
			return;
		}
		/* If Option Switch is set, then do this before allowing           */
		/* page up or down                                                 */
		if (optDoctFound.isTrue()
				|| optQuestFound.isTrue()) {
			//goTo(GotoLabel.nextProgram4300);
			nextProgram4300();
			return;
		}
		/*  CHECK IF A MANDATORY JOINT LIFE HAS BEEN SET UP*/
		/*    IF NOT FORCE A REDISPLAY*/
		if (((isEQ(scrnparams.statuz, varcom.oK))
				&& (isGT(t5688rec.jlifemin, 0)))) {
			//goTo(GotoLabel.clearScreen4150);
			clearScreen4150();
			return;

		}
		else {
			//goTo(GotoLabel.nextProgram4300);
			nextProgram4300();
			return;
		}

	}

	protected void clearScreen4150()
	{
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction("READR");
		callLifelnbio5000();
		if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
			sv.dataArea.set(SPACES);
			sv.chdrnum.set(lifelnbIO.getChdrnum());
			sv.life.set(lifelnbIO.getLife());
			lifelnbIO.setLifcnum(SPACES);
			sv.jlife.set(lifelnbIO.getJlife());
			sv.anbage.set(ZERO);
			sv.dob.set(varcom.vrcmMaxDate);
			sv.dummyOut[varcom.nd.toInt()].set("Y");
			/*       MOVE SPACES           TO WSAA-AGEADM           <LA3291>*/
			wsaaAgeadm.set("N");
			sv.weight.set(ZERO);
			sv.height.set(ZERO);
			sv.bmi.set(ZERO);
			setFieldsCustomerSpecific();
			sv.optind01.set(SPACES);
			sv.optind02.set(SPACES);
			sv.optind03.set(SPACES);
			sv.optind04.set(SPACES);
			optswchInit1200();
			if (underwritingNotReqd.isTrue()) {
				sv.heightOut[varcom.nd.toInt()].set("Y");
				sv.heightOut[varcom.pr.toInt()].set("Y");
				sv.weightOut[varcom.nd.toInt()].set("Y");
				sv.weightOut[varcom.pr.toInt()].set("Y");
				sv.bmiOut[varcom.nd.toInt()].set("Y");
				sv.bmiOut[varcom.pr.toInt()].set("Y");
			}
			//goTo(GotoLabel.goBack4250);
			goBack4250();
			return;
		}
		if (isEQ(wsaaRollIndc, "U")) {
			//goTo(GotoLabel.redisplay4200);
			redisplay4200();
			return;
		}
		else {
		//	goTo(GotoLabel.nextProgram4300);
			nextProgram4300();
			return;
		}
	}

	protected void redisplay4200()
	{
		/*    IF WSAA-ROLL-INDC           NOT = 'N'*/
		/*       MOVE 'N'                 TO WSAA-ROLL-INDC.*/
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsio1900();
		plainname();
		sv.lifename.set(wsspcomn.longconfname);
		sv.sex.set(cltsIO.getCltsex());
		sv.dob.set(cltsIO.getCltdob());
		sv.jlife.set(lifelnbIO.getJlife());
		sv.lifesel.set(lifelnbIO.getLifcnum());
		sv.anbage.set(lifelnbIO.getAnbAtCcd());
		sv.selection.set(lifelnbIO.getSelection());
		sv.relation.set(lifelnbIO.getLiferel());
		/* MOVE LIFELNB-AGEADM         TO S5005-AGEADM.                 */
		wsaaAgeadm.set(lifelnbIO.getAgeadm());
		/* IF WSAA-AGEADM = SPACES                              <LA3291>*/
		if (isEQ(wsaaAgeadm, "N")) {
			sv.dummyOut[varcom.nd.toInt()].set("Y");
		}
		else {
			sv.dummyOut[varcom.nd.toInt()].set(SPACES);
		}
		sv.smoking.set(lifelnbIO.getSmoking());
		displayOccpCustomerSpecific();
		sv.pursuit01.set(lifelnbIO.getPursuit01());
		sv.pursuit02.set(lifelnbIO.getPursuit02());
		if (underwritingReqd.isTrue()) {
			lifelnbIO.setFunction(varcom.keeps);
			callLifelnbio5000();
			optswchInit1200();
			/* Get the Underwriting details for the Life, if there             */
			/* are any.                                                        */
			undlIO.setParams(SPACES);
			undlIO.setChdrcoy(lifelnbIO.getChdrcoy());
			undlIO.setChdrnum(lifelnbIO.getChdrnum());
			undlIO.setLife(lifelnbIO.getLife());
			undlIO.setJlife(lifelnbIO.getJlife());
			undlIO.setFormat(formatsInner.undlrec);
			undlIO.setFunction(varcom.readr);
			callUndlio6000();
			if (isEQ(undlIO.getStatuz(), varcom.mrnf)) {
				//goTo(GotoLabel.goBack4250);
				goBack4250();
				return;
			}
			sv.weight.set(undlIO.getWeight());
			sv.height.set(undlIO.getHeight());
			sv.bmi.set(undlIO.getBmi());
		}
		else {
			sv.weightOut[varcom.nd.toInt()].set("Y");
			sv.weightOut[varcom.pr.toInt()].set("Y");
			sv.heightOut[varcom.nd.toInt()].set("Y");
			sv.heightOut[varcom.pr.toInt()].set("Y");
			sv.bmiOut[varcom.nd.toInt()].set("Y");
			sv.bmiOut[varcom.pr.toInt()].set("Y");
			sv.optind02Out[varcom.nd.toInt()].set("Y");
			sv.optind02Out[varcom.pr.toInt()].set("Y");
			sv.optind03Out[varcom.nd.toInt()].set("Y");
			sv.optind03Out[varcom.pr.toInt()].set("Y");
			if(riskamtFlag){
			wsspcomn.clntkey.set(sv.lifesel);
			if (wsspcomn.flag.equals(C)){
					sv.optind04Out[varcom.nd.toInt()].set("Y");
					sv.optind04Out[varcom.pr.toInt()].set("Y");
			}
			if (wsspcomn.flag.equals(M)){
				
				sv.optind04Out[varcom.nd.toInt()].set("N");
				sv.optind04Out[varcom.pr.toInt()].set("N");
				}
			}
		}

	}
	protected void displayOccpCustomerSpecific(){
		sv.occup.set(cltsIO.getOccpcode()); 
		if(uwFlag){
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(wsspcomn.fsuco.toString());
			itempf.setItemtabl(T3644);
			itempf.setItemitem(sv.occup.toString());
			itempf = itempfDAO.getItempfRecord(itempf);
			if(itempf != null) {
				t3644rec.t3644Rec.set(StringUtil.rawToString(itempf.getGenarea()));  
				sv.industry.set(t3644rec.occupationClass);
			}
		}
	}

	protected void goBack4250()
	{
		if (isEQ(wsaaRollIndc, "U")) {
			if (isEQ(wsspcomn.secActn[2], "*")) {
				wsspcomn.secActn[2].set(SPACES);
			}
		}
		if (isNE(wsaaRollIndc, "N")) {
			wsaaRollIndc = "N";
		}
		wsspcomn.nextprog.set(scrnparams.scrname);
		//goTo(GotoLabel.exit4090);
		return;
	}

	protected void nextProgram4300()
	{
		lifelnbIO.setDataArea(SPACES);
		lifelnbIO.setFunction(varcom.readr);
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(sv.chdrnum);
		lifelnbIO.setLife(sv.life);
		/*    MOVE '00'                   TO LIFELNB-JLIFE.                */
		if (isEQ(sv.lifesel, SPACES)) {
			lifelnbIO.setJlife("00");
		}
		else {
			lifelnbIO.setJlife(sv.jlife);
		}
		callLifelnbio5000();
		/*    MOVE KEEPS                  TO LIFELNB-FUNCTION.             */
		/*    MOVE LIFELNBREC             TO LIFELNB-FORMAT.               */
		/*    PERFORM 5000-CALL-LIFELNBIO.                                 */
		/* ADD 1                       TO WSSP-PROGRAM-PTR.             */
		if (isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			lifelnbIO.setFunction(varcom.keeps);
			lifelnbIO.setFormat(formatsInner.lifelnbrec);
			callLifelnbio5000();
			wsspcomn.programPtr.add(1);
		}
		else {
			wsspcomn.programPtr.add(-1);
		}
	}

	protected void checkBoxes4500()
	{
		start4510();
	}

	/**
	 * <pre>
	 * Each check box is processed one at a time.                      
	 * </pre>
	 */
	protected void start4510()
	{
		/* If there are no errors on the screen and a client number        */
		/* does not exist, there is no need for Questions or Doctors       */
		/* to be input.                                                    */
		if (isEQ(wsspcomn.edterror, "****")
				&& isEQ(sv.lifesel, SPACES)) {
			sv.optind02.set(SPACES);
			return ;
		}
		if (isEQ(sv.optind01, "X")) {
			optswchrec.optsInd[1].set("X");
			sv.optind[1].set(SPACES);
			wsaaOptMlia.set("Y");
		}
		if (isEQ(sv.optind02, "X")) {
			optswchrec.optsInd[2].set("X");
			sv.optind[2].set(SPACES);
			wsaaOptQuest.set("Y");
		}
		if (isEQ(sv.optind03, "X")) {
			optswchrec.optsInd[3].set("X");
			sv.optind[3].set(SPACES);
		}
		if(riskamtFlag){
			wsspcomn.clntkey.set(sv.lifesel);	
		if (isEQ(sv.optind04, "X")) {
			optswchrec.optsInd[4].set("X");
			sv.optind[4].set(SPACES);
		}
		}
	}

	/**
	 * <pre>
	 *4800-OPTSWCH-CALL SECTION.                               <V4L011>
	 ***************************                               <V4L011>
	 *4800-CALL.                                               <V4L011>
	 *                                                         <V4L011>
	 *    MOVE WSAA-PROG                TO WSSP-NEXTPROG.      <V4L011>
	 *                                                         <V4L011>
	 * Call 'OPTSWCH' with a function of 'STCK' to determine   <V4L011>
	 * which screen to display next.                           <V4L011>
	 *                                                         <V4L011>
	 *    MOVE WSSP-COMPANY             TO OPTS-ITEM-COMPANY.  <V4L011>
	 *    MOVE 'STCK'                   TO OPTS-FUNCTION.      <V4L011>
	 *    CALL 'OPTSWCH'             USING OPTSWCH-REC         <V4L011>
	 *                                     WSSP-SEC-PROGS      <V4L011>
	 *                                     WSSP-SEC-ACTNS      <V4L011>
	 *                                     WSSP-PROGRAM-PTR    <V4L011>
	 *                                     WSSP-FLAG.          <V4L011>
	 *                                                         <V4L011>
	 *    IF (OPTS-STATUZ            NOT = O-K) AND            <V4L011>
	 *       (OPTS-STATUZ            NOT = ENDP)               <V4L011>
	 *         MOVE WSSP-COMPANY        TO OPTS-ITEM-COMPANY   <V4L011>
	 *         MOVE 'STCK'              TO SYSR-FUNCTION       <V4L011>
	 *                                     SYSR-STATUZ         <V4L011>
	 *         MOVE 'OPTSWCH'           TO SYSR-IOMOD          <V4L011>
	 *         PERFORM 600-FATAL-ERROR                         <V4L011>
	 *    END-IF.                                              <V4L011>
	 *                                                         <V4L011>
	 **** MOVE OPTS-IND(1)              TO S5005-OPTIND.       <LA1143>
	 *    MOVE OPTS-IND(2)              TO S5005-OPTIND-02.            
	 *    MOVE WSAA-OPTIND              TO S5005-OPTIND.       <LA1143>
	 *                                                         <V4L011>
	 **** IF OPTS-STATUZ                 = O-K                 <LA1143>
	 ****    ADD  1                     TO WSSP-PROGRAM-PTR    <LA1143>
	 **** ELSE                                                 <LA1143>
	 ****    MOVE SCRN-SCRNAME          TO WSSP-NEXTPROG       <LA1143>
	 **** END-IF.                                              <LA1143>
	 *    IF OPTS-STATUZ                 = ENDP                <LA1143>
	 *       MOVE SCRN-SCRNAME          TO WSSP-NEXTPROG       <LA1143>
	 *    ELSE                                                 <LA1143>
	 *       ADD  1                     TO WSSP-PROGRAM-PTR    <LA1143>
	 *    END-IF.                                              <V4L011>
	 *                                                         <V4L011>
	 *4890-EXIT.                                               <V4L011>
	 *     EXIT.                                               <V4L011>
	 * </pre>
	 */
	protected void callLifelnbio5000()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
				&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.dbparams.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	protected void callUndlio6000()
	{
		/*PARA*/
		SmartFileCode.execute(appVars, undlIO);
		if (isNE(undlIO.getStatuz(), varcom.oK)
				&& isNE(undlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.dbparams.set(undlIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

	protected void deleteQuestions7000()
	{
		init7010();
	}

	protected void init7010()
	{
/*		undqIO.setDataKey(SPACES);
		undqIO.setChdrcoy(wsspcomn.company);
		undqIO.setChdrnum(sv.chdrnum);
		undqIO.setLife(sv.life);
		undqIO.setJlife(sv.jlife);
		undqIO.setFormat(formatsInner.undqrec);
		undqIO.setFunction(varcom.begn);
		undqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		undqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "JLIFE");
		SmartFileCode.execute(appVars, undqIO);
		if (isNE(undqIO.getStatuz(), varcom.oK)
				&& isNE(undqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(undqIO.getParams());
			fatalError600();
		}
		if (isNE(wsspcomn.company, undqIO.getChdrcoy())
				|| isNE(sv.chdrnum, undqIO.getChdrnum())
				|| isNE(sv.life, undqIO.getLife())
				|| isNE(sv.jlife, undqIO.getJlife())) {
			undqIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(undqIO.getStatuz(), varcom.endp))) {
			undqIO.setFunction(varcom.deltd);
			SmartFileCode.execute(appVars, undqIO);
			if (isNE(undqIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(undqIO.getParams());
				fatalError600();
			}
			undqIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, undqIO);
			if (isNE(undqIO.getStatuz(), varcom.oK)
					&& isNE(undqIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(undqIO.getParams());
				fatalError600();
			}
			if (isNE(wsspcomn.company, undqIO.getChdrcoy())
					|| isNE(sv.chdrnum, undqIO.getChdrnum())
					|| isNE(sv.life, undqIO.getLife())
					|| isNE(sv.jlife, undqIO.getJlife())) {
				undqIO.setStatuz(varcom.endp);
			}
		}*/
		undqpfDAO.deleteUndqRecord(wsspcomn.company.toString(), sv.chdrnum.toString(), sv.life.toString(),
				sv.jlife.toString());
		sv.lifeselErr.set(SPACES);
	}

	protected void deleteUndLife8000()
	{
		init8010();
	}

	protected void init8010()
	{
/*		undlIO.setParams(SPACES);
		undlIO.setChdrcoy(wsspcomn.company);
		undlIO.setChdrnum(sv.chdrnum);
		undlIO.setLife(sv.life);
		undlIO.setJlife(sv.jlife);
		undlIO.setFormat(formatsInner.undlrec);
		undlIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, undlIO);
		if (isEQ(undlIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		if (isNE(undlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undlIO.getParams());
			fatalError600();
		}
		undlIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, undlIO);
		if (isNE(undlIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(undlIO.getParams());
			fatalError600();
		}*/
		undlpfDAO.deleteUndlRecord(wsspcomn.company.toString(), sv.chdrnum.toString(), sv.life.toString(), sv.jlife.toString());
	}
	/*
	 * Class transformed  from Data Structure ERRORS--INNER
	 */
	private static final class ErrorsInner { 
		/* ERRORS */
		private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
		private FixedLengthStringData e268 = new FixedLengthStringData(4).init("E268");
		private FixedLengthStringData f250 = new FixedLengthStringData(4).init("F250");
		private FixedLengthStringData h143 = new FixedLengthStringData(4).init("H143");
		private FixedLengthStringData e374 = new FixedLengthStringData(4).init("E374");
		private FixedLengthStringData e530 = new FixedLengthStringData(4).init("E530");
		private FixedLengthStringData g844 = new FixedLengthStringData(4).init("G844");
		private FixedLengthStringData g819 = new FixedLengthStringData(4).init("G819");
		private FixedLengthStringData g983 = new FixedLengthStringData(4).init("G983");
		private FixedLengthStringData h015 = new FixedLengthStringData(4).init("H015");
		private FixedLengthStringData f658 = new FixedLengthStringData(4).init("F658");
		private FixedLengthStringData f782 = new FixedLengthStringData(4).init("F782");
		private FixedLengthStringData h367 = new FixedLengthStringData(4).init("H367");
		private FixedLengthStringData h042 = new FixedLengthStringData(4).init("H042");
		private FixedLengthStringData g499 = new FixedLengthStringData(4).init("G499");
		private FixedLengthStringData h075 = new FixedLengthStringData(4).init("H075");
		private FixedLengthStringData h076 = new FixedLengthStringData(4).init("H076");
		private FixedLengthStringData f401 = new FixedLengthStringData(4).init("F401");
		private FixedLengthStringData r003 = new FixedLengthStringData(4).init("R003");
		private FixedLengthStringData rl34 = new FixedLengthStringData(4).init("RL34");
		private FixedLengthStringData rla7 = new FixedLengthStringData(4).init("RLA7");
		private FixedLengthStringData e031 = new FixedLengthStringData(4).init("E031");
		private FixedLengthStringData e590 = new FixedLengthStringData(4).init("E590");
		private FixedLengthStringData e598 = new FixedLengthStringData(4).init("E598");
		private FixedLengthStringData rreg = new FixedLengthStringData(4).init("RREG");//ICIL-160
	}
	/*
	 * Class transformed  from Data Structure TABLES--INNER
	 */
	private static final class TablesInner { 
		private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
		private FixedLengthStringData t2240 = new FixedLengthStringData(5).init("T2240");
		private FixedLengthStringData t6769 = new FixedLengthStringData(5).init("T6769");
		private FixedLengthStringData t6771 = new FixedLengthStringData(5).init("T6771");
		private FixedLengthStringData tr675 = new FixedLengthStringData(5).init("TR675");
	}
	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner { 
		/* FORMATS */
		private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
		private FixedLengthStringData mliasecrec = new FixedLengthStringData(10).init("MLIASECREC");
		private FixedLengthStringData clrrrec = new FixedLengthStringData(10).init("CLRRREC");
		private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
		private FixedLengthStringData clprrec = new FixedLengthStringData(10).init("CLPRREC");
		private FixedLengthStringData lifematrec = new FixedLengthStringData(10).init("LIFEMATREC");
		private FixedLengthStringData undlrec = new FixedLengthStringData(10).init("UNDLREC");
		private FixedLengthStringData undqrec = new FixedLengthStringData(10).init("UNDQREC");
	}
	
	public LifelnbTableDAM getlifelnbIO() {
		return lifelnbIO;
	}

	public void setCltsIO(LifelnbTableDAM lifelnbIO) {
		this.lifelnbIO = lifelnbIO;
	}
	
	public PackedDecimalData getWsaaBmi() {
		return wsaaBmi;
	}


	public void setWsaaBmi(PackedDecimalData wsaaBmi) {
		this.wsaaBmi = wsaaBmi;
	}

	public CltsTableDAM getCltsIO() {
		return cltsIO;
	}


	public void setCltsIO(CltsTableDAM cltsIO) {
		this.cltsIO = cltsIO;
	}

	
	public T5688rec getT5688rec() {
		return t5688rec;
	}


	public void setT5688rec(T5688rec t5688rec) {
		this.t5688rec = t5688rec;
	}

	
	public UndlTableDAM getUndlIO() {
		return undlIO;
	}


	public void setUndlIO(UndlTableDAM undlIO) {
		this.undlIO = undlIO;
	}
	
	public LifelnbTableDAM getLifelnbIO() {
		return lifelnbIO;
	}


	public void setLifelnbIO(LifelnbTableDAM lifelnbIO) {
		this.lifelnbIO = lifelnbIO;
	}


	protected void setFieldsCustomerSpecific() { }
	protected void call2111CustomerSpecific(){ }
	protected void call2112CustomerSpecific(){ }
	protected void chkScreenChngesCustomerSpecific(){ }


}
