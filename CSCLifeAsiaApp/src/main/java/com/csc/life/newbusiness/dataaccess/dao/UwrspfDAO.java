package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Uwrspf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UwrspfDAO extends BaseDAO<Uwrspf> {
	public void insertUwrsData(List<Uwrspf> uwrspfList);
	public void deleteUwrsData(Uwrspf uwrspf);
	public List<Uwrspf> searchUwrspfRecord(String chdrcoy, String chdrnum);
	public void updateUwrsData(List<Uwrspf> uwrspfList);
}