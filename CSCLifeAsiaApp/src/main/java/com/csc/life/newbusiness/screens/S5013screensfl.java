package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5013screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 8;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {8, 9, 4, 5, 6, 7, 1, 2, 3, 10, 50}; 

	public static RecInfo lrec = new RecInfo();
	public static String UWOVERWRITE = "uwoverwrite";
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {15, 22, 2, 75}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5013ScreenVars sv = (S5013ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s5013screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s5013screensfl, 
			sv.S5013screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S5013ScreenVars sv = (S5013ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s5013screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S5013ScreenVars sv = (S5013ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s5013screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S5013screensflWritten.gt(0))
		{
			sv.s5013screensfl.setCurrentIndex(0);
			sv.S5013screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S5013ScreenVars sv = (S5013ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s5013screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5013ScreenVars screenVars = (S5013ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqnbr.setFieldName("seqnbr");
				screenVars.opcda.setFieldName("opcda");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.agerate.setFieldName("agerate");
				screenVars.oppc.setFieldName("oppc");
				screenVars.insprm.setFieldName("insprm");
				screenVars.extCessTerm.setFieldName("extCessTerm");
				screenVars.select.setFieldName("select");
				screenVars.reasind.setFieldName("reasind");
				screenVars.znadjperc.setFieldName("znadjperc");
				screenVars.zmortpct.setFieldName("zmortpct");
				screenVars.premadj.setFieldName("premadj");
				screenVars.uwoverwrite.setFieldName(UWOVERWRITE);
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.seqnbr.set(dm.getField("seqnbr"));
			screenVars.opcda.set(dm.getField("opcda"));
			screenVars.shortdesc.set(dm.getField("shortdesc"));
			screenVars.agerate.set(dm.getField("agerate"));
			screenVars.oppc.set(dm.getField("oppc"));
			screenVars.insprm.set(dm.getField("insprm"));
			screenVars.extCessTerm.set(dm.getField("extCessTerm"));
			screenVars.select.set(dm.getField("select"));
			screenVars.reasind.set(dm.getField("reasind"));
			screenVars.znadjperc.set(dm.getField("znadjperc"));
			screenVars.zmortpct.set(dm.getField("zmortpct"));
			screenVars.premadj.set(dm.getField("premadj"));
			screenVars.uwoverwrite.set(dm.getField(UWOVERWRITE));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S5013ScreenVars screenVars = (S5013ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.seqnbr.setFieldName("seqnbr");
				screenVars.opcda.setFieldName("opcda");
				screenVars.shortdesc.setFieldName("shortdesc");
				screenVars.agerate.setFieldName("agerate");
				screenVars.oppc.setFieldName("oppc");
				screenVars.insprm.setFieldName("insprm");
				screenVars.extCessTerm.setFieldName("extCessTerm");
				screenVars.select.setFieldName("select");
				screenVars.reasind.setFieldName("reasind");
				screenVars.znadjperc.setFieldName("znadjperc");
				screenVars.zmortpct.setFieldName("zmortpct");
				screenVars.premadj.setFieldName("premadj");
				screenVars.uwoverwrite.setFieldName(UWOVERWRITE);
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("seqnbr").set(screenVars.seqnbr);
			dm.getField("opcda").set(screenVars.opcda);
			dm.getField("shortdesc").set(screenVars.shortdesc);
			dm.getField("agerate").set(screenVars.agerate);
			dm.getField("oppc").set(screenVars.oppc);
			dm.getField("insprm").set(screenVars.insprm);
			dm.getField("extCessTerm").set(screenVars.extCessTerm);
			dm.getField("select").set(screenVars.select);
			dm.getField("reasind").set(screenVars.reasind);
			dm.getField("znadjperc").set(screenVars.znadjperc);
			dm.getField("zmortpct").set(screenVars.zmortpct);
			dm.getField("premadj").set(screenVars.premadj);
			dm.getField(UWOVERWRITE).set(screenVars.uwoverwrite);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S5013screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S5013ScreenVars screenVars = (S5013ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.seqnbr.clearFormatting();
		screenVars.opcda.clearFormatting();
		screenVars.shortdesc.clearFormatting();
		screenVars.agerate.clearFormatting();
		screenVars.oppc.clearFormatting();
		screenVars.insprm.clearFormatting();
		screenVars.extCessTerm.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.reasind.clearFormatting();
		screenVars.znadjperc.clearFormatting();
		screenVars.zmortpct.clearFormatting();
		screenVars.premadj.clearFormatting();
		screenVars.uwoverwrite.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S5013ScreenVars screenVars = (S5013ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.seqnbr.setClassString("");
		screenVars.opcda.setClassString("");
		screenVars.shortdesc.setClassString("");
		screenVars.agerate.setClassString("");
		screenVars.oppc.setClassString("");
		screenVars.insprm.setClassString("");
		screenVars.extCessTerm.setClassString("");
		screenVars.select.setClassString("");
		screenVars.reasind.setClassString("");
		screenVars.znadjperc.setClassString("");
		screenVars.zmortpct.setClassString("");
		screenVars.premadj.setClassString("");
		screenVars.uwoverwrite.setClassString("");
	}

/**
 * Clear all the variables in S5013screensfl
 */
	public static void clear(VarModel pv) {
		S5013ScreenVars screenVars = (S5013ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.seqnbr.clear();
		screenVars.opcda.clear();
		screenVars.shortdesc.clear();
		screenVars.agerate.clear();
		screenVars.oppc.clear();
		screenVars.insprm.clear();
		screenVars.extCessTerm.clear();
		screenVars.select.clear();
		screenVars.reasind.clear();
		screenVars.znadjperc.clear();
		screenVars.zmortpct.clear();
		screenVars.premadj.clear();
		screenVars.uwoverwrite.clear();
	}
}
