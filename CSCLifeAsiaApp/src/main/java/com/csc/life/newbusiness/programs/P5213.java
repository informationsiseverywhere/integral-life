/*
 * File: P5213.java
 * Date: 30 August 2009 0:18:31
 * Author: Quipoz Limited
 * 
 * Class transformed from P5213.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.RtrnsacTableDAM;
import com.csc.life.general.procedures.Agecalc;  //ICIL-93
import com.csc.life.general.recordstructures.Agecalcrec; //ICIL-93
import com.csc.life.interestbearing.dataaccess.dao.HitspfDAO;
import com.csc.life.interestbearing.dataaccess.model.Hitspf;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.screens.S5213ScreenVars;
import com.csc.life.newbusiness.tablestructures.T6799rec;
import com.csc.life.productdefinition.dataaccess.dao.HitrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.UtrspfDAO;
import com.csc.life.productdefinition.dataaccess.model.Hitrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Utrspf;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Td5gfrec;   //ICIL-93
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BaseScreenData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*        ALTER/CANCEL FROM INCEPTION SUBMENU.
*        ------------------------------------
*
* Overview.
* ---------
*
*  This program is used to validate a Contract that has been
* selected for being Cancelled/Altered from Inception. It also
* provides the user with the opportunity to go to Contract
* Enquiry if the user is unsure about which contract to select
* for Cancelling/Altering from Inception.
*  Once the program has passed all the specified validation
* criteria, then this program will pass control to the
* Cancel/Alter from Inception confirmation screen, S5214.
*
* Validation.
* -----------
*
*  The UTRN file is checked to make sure there are no unprocessed
* UTRNs pending for the contract we are trying to work with.
*  Read T6661 using current transaction code to see what the
* forward transaction code is set to - this will tell us what
* PTRN record to stop at when we are looping through them to
* see whether they are reversible or not.
*  Check Contract Header status against entry in T5679.
*  Check all Coverage record statuses against entry in T5679.
*  Check each transaction performed on the Contract since issue
* by reading through all the PTRN records (using PTRNREV
* logical file) and checking the transaction code in the
* PTRNREV record against its entry in T6661, the Reversals
* table, to see whether the transaction is reversible through
* 'Full Contract Reversal' or not.
*  If a transaction is found which fails the above test, then
* the user is informed of this and must then go and reverse
* the transaction through the Reversals (Windback) submenu
* before trying the CFI/AFI again.
*  Stop checking PTRNs if the Trancode on the PTRN matches the
* one read from T6661 OR if the Tranno on the PTRN is 1.
* - The tranno 1 signifies that the Contract was issued
*   through Fast Track New Business rather than the normal
*   2-step Proposal-then-Issue of ordinary New Business
*   processing.
*  If CFI/AFI selected, then read T6799 to make sure CFI/AFI
* transaction codes are in table.
*  Check that the business date is within the initial period
* of the contract as defined in T5688 - i.e. is the business
* date within the allowable CFI/AFI period?
*
* Updating.
* ---------
*
*  If the user has selected the 'Contract Enquiry' option, and
* the Contract exists, then do a 'KEEPS' on the CHDRENQ logical
* file record so that it can be passed along to the Contract
* Enquiry program, P6353.
*  Else AFI/CFI has been selected so do a 'KEEPS' on the
* CHDRLNB logical file to pass to the CFI/AFI program P5214.
*
* Tables Used.
* ------------
*
* T5679 - Status requirements for transactions
* T5688 - Contract Structure
* T6661 - Transaction Reversal Parameters
* T6799 - AFI/CFI Subroutines
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* If there are outstanding unprocessed HITR records, output
* an error message : HL08 'Unprocessed HITR pending'.
*
* Allow AFI/CFI to proceed if there are unprocessed UTRNs or
* HITRs as long as there are no summary records (i.e. UTRSs or
* HITSs) since this means Unit Deal has not yet been run for
* the policy so all unprocessed records can be safely deleted
* (this will be done by ZRULRVISS).
*
*
*****************************************************************
* </pre>
*/
public class P5213 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5213");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaChdrOk = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaStopAtTrancode = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsaaChdrDetails = new FixedLengthStringData(17);
	private FixedLengthStringData wsaaChdrnumStore = new FixedLengthStringData(8).isAPartOf(wsaaChdrDetails, 0);
	private FixedLengthStringData wsaaChdrcoyStore = new FixedLengthStringData(1).isAPartOf(wsaaChdrDetails, 8);
	private FixedLengthStringData wsaaChdrBranchStore = new FixedLengthStringData(2).isAPartOf(wsaaChdrDetails, 9);
	private FixedLengthStringData wsaaChdrStatuzStore = new FixedLengthStringData(4).isAPartOf(wsaaChdrDetails, 11);
	private FixedLengthStringData wsaaChdrStatStore = new FixedLengthStringData(2).isAPartOf(wsaaChdrDetails, 15);

	private FixedLengthStringData wsaaFreelookBasis = new FixedLengthStringData(1);
	private Validator rcdBasis = new Validator(wsaaFreelookBasis, "1");
	private Validator issueDateBasis = new Validator(wsaaFreelookBasis, "2");
	private Validator maxMoneyUwBasis = new Validator(wsaaFreelookBasis, "3");
	private Validator ackDeemBasis = new Validator(wsaaFreelookBasis, "4");
	private Validator ackDeemIssBasis = new Validator(wsaaFreelookBasis, "5");
	
	private FixedLengthStringData wsaaFreelookBasisChn = new FixedLengthStringData(1);
	private Validator issueDateChnBasis = new Validator(wsaaFreelookBasisChn, "1");
	private Validator ackBasis = new Validator(wsaaFreelookBasisChn, "2");
	private Validator deemedBasis = new Validator(wsaaFreelookBasisChn, "3");
	private Validator minAckDeemedBasis = new Validator(wsaaFreelookBasisChn, "4");
	
	private ZonedDecimalData wsaaFreelookDate = new ZonedDecimalData(8, 0).setUnsigned();
	private static final String wsaaProposalProg = "P5004";
	
		/* TABLES */
	private static final String t5645 = "T5645";
	private static final String t5679 = "T5679";
	private static final String t5688 = "T5688";
	private static final String t6661 = "T6661";
	private static final String t6799 = "T6799";
	private static final String th605 = "TH605";
	private static final String td5gf = "TD5GF";  // ICIL-93
	
	
	
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
//	private HitrrnlTableDAM hitrrnlIO = new HitrrnlTableDAM();
//	private HitsTableDAM hitsIO = new HitsTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
//	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private RtrnsacTableDAM rtrnsacIO = new RtrnsacTableDAM();
//	private UtrnextTableDAM utrnextIO = new UtrnextTableDAM();
//	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Batckey wsaaBatchkey = new Batckey();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6661rec t6661rec = new T6661rec();
	private T6799rec t6799rec = new T6799rec();
	private Th605rec th605rec = new Th605rec();
	private Td5gfrec td5gfrec = new Td5gfrec();      // ICIL-93
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private S5213ScreenVars sv = ScreenProgram.getScreenVars( S5213ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private UtrspfDAO utrspfDAO = getApplicationContext().getBean("utrspfDAO", UtrspfDAO.class);
	private HitrpfDAO hitrpfDAO = getApplicationContext().getBean("hitrpfDAO", HitrpfDAO.class);
	private HitspfDAO hitspfDAO = getApplicationContext().getBean("hitspfDAO", HitspfDAO.class);
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private Agecalcrec agecalcrec = new Agecalcrec();  //ICIL-93
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).setUnsigned(); //ICIL-93
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO",LifepfDAO.class);
	private Lifepf lifepf = new Lifepf();
	
	
	boolean ctcnl002Permission = false; // ICIL-93
	
	private Map<String, List<Itempf>> t6661Map = null;
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO= getApplicationContext().getBean("chdrpfDAO",ChdrpfDAO.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		callUtrn2893, 
		a120CallHitr, 
		batching3080, 
		exit3090
	}

	public P5213() {
		super();
		screenVars = sv;
		new ScreenModel("S5213", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
	    ctcnl002Permission  = FeaConfg.isFeatureExist("2", "CTCNL002", appVars, "IT");     // ICIL-93
		sv.dataArea.set(SPACES);
		wsaaChdrOk.set(SPACES);
		wsaaStopAtTrancode.set(SPACES);
		wsaaSub.set(ZERO);
		sv.action.set(wsspcomn.sbmaction);
		
		sv.effdateDisp.set(SPACES);
		
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(errorsInner.e070);
		}
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		readTh6051100();
	}

protected void readTh6051100()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(th605);
		itemIO.setItemitem(wsspcomn.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		
	}
	
	

protected void preScreenEdit()
	{
		/*PRE-START*/
	if (ctcnl002Permission==false){
		sv.effdateOut[varcom.nd.toInt()].set("Y");
		sv.effdateDisp.setEnabled(BaseScreenData.DISABLED);
	}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*START*/
		wsspcomn.edterror.set(varcom.oK);
		//JOptionPane.showMessageDialog(null, sv.effdate, "alert", JOptionPane.ERROR_MESSAGE); 
		
		/*VALIDATE*/
		validateAction2100();
		if(ctcnl002Permission){
			if(isEQ(sv.effdate,varcom.vrcmMaxDate)){
				this.wsspcomn.occdate.set(wsaaToday);
			}else{
				this.wsspcomn.occdate.set(sv.effdate);
				if(isGT(sv.effdate, wsaaToday)){
					sv.effdateErr.set(errorsInner.f073);
				}
			}
		
		}

		if (isEQ(sv.actionErr,SPACES)) {
			if (isEQ(scrnparams.statuz,"BACH")) {
				verifyBatchControl2900();
			}
			else {
				validateKeys2200();
			}
		}
	
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		if(start2100()){
			checkSanctions2120();
		}
		}

protected boolean start2100()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			return false;
		}
		return true;
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		/*    Initialise Working Storage                                   */
		wsaaChdrDetails.set(SPACES);
		/*    Validate fields*/
		if (isNE(subprogrec.key1,"Y")) {
			return ;
		}
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(sv.chdrsel);
		chdrlnbIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel,SPACES)) {
			SmartFileCode.execute(appVars, chdrlnbIO);
		}
		else {
			chdrlnbIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.e544);
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				return ;
			}
		}
		wsaaChdrnumStore.set(chdrlnbIO.getChdrnum());
		wsaaChdrcoyStore.set(chdrlnbIO.getChdrcoy());
		if (isEQ(sv.chdrselErr,SPACES)
		&& isEQ(sv.action,"B")) {
			checkContractDetails2500();
			return ;
		}
		if(isLT(sv.effdate,chdrlnbIO.ccdate )){
			sv.effdateErr.set(errorsInner.t044);
			return ;
		}
		
		if (isEQ(sv.chdrselErr,SPACES)
		&& (isEQ(sv.action,"A")
		|| isEQ(sv.action, "C")
		|| isEQ(sv.action, "D"))
		&& isNE(wsspcomn.branch,chdrlnbIO.getCntbranch())) {
			sv.chdrselErr.set(errorsInner.e455);
		}
		if ((isEQ(subprogrec.key1,"Y"))
		&& (isEQ(sv.chdrselErr,SPACES))) {
			checkUdel2891();
			a100CheckHitr();
		}
		checkT6661Entry2300();
		if (isEQ(sv.actionErr,SPACES)) {
			checkT6799Entry2400();
		}
		if (isEQ(sv.chdrselErr,SPACES)
		&& isEQ(sv.actionErr,SPACES)) {
			checkContractDetails2500();
		}
	}

protected void checkT6661Entry2300()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setItemitem(subprogrec.transcd);
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.actionErr.set(errorsInner.f014);
			return ;
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		wsaaStopAtTrancode.set(t6661rec.trcode);
	}

protected void checkT6799Entry2400()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6799);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setItemitem(subprogrec.transcd);
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.actionErr.set(errorsInner.f031);
			return ;
		}
		t6799rec.t6799Rec.set(itemIO.getGenarea());
	}


protected void checkContractDetails2500()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(subprogrec.transcd);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
 		chdrStatus2600();
		if (isEQ(wsaaChdrOk,"N")) {
			sv.chdrselErr.set(errorsInner.e767);
			return ;
		}
		if (isEQ(sv.action,"B")) {
			return ;
		}
		
		String coy = wsspcomn.company.toString();
		t6661Map = itemDAO.loadSmartTable("IT", coy, "T6661");
		List<Ptrnpf> ptrnrevList = ptrnpfDAO.searchPtrnrevData(coy, chdrlnbIO.getChdrnum().toString());
		if(ptrnrevList == null || ptrnrevList.isEmpty()){
			sv.chdrselErr.set(errorsInner.f018);
		}else{
			processPtrns2700(ptrnrevList);
		}
		
		/* Check Contract being CFI/AFI'd is within the allowable*/
		/*  CFI/AFI period as defined on T5688.*/
		if (isEQ(sv.action, "D")) {
		
	//ICIL-93
			if(ctcnl002Permission){
				freelookCheckCHN();
			}else{ 
				a200CheckFreelook();
			}
	
		}
		else {
		checkDates2800();
	}
	}

protected void chdrStatus2600()
	{
		/*START*/
		wsaaSub.set(ZERO);
		wsaaChdrOk.set("N");
		while ( !(isEQ(wsaaSub,12))) {
			wsaaSub.add(1);
			if (isEQ(chdrlnbIO.getStatcode(),t5679rec.cnRiskStat[wsaaSub.toInt()])) {
				wsaaSub.set(12);
				wsaaChdrOk.set("Y");
			}
		}
		
		/*EXIT*/
	}

protected void processPtrns2700(List<Ptrnpf> ptrnrevList)
	{
		boolean foundFlag = false;
		for (int i = 0; i < ptrnrevList.size(); i++) {
			Ptrnpf p = ptrnrevList.get(i);
			if (isEQ(p.getBatctrcde(), wsaaStopAtTrancode) || p.getTranno() == 1) {
				foundFlag = true;
				break;
		}
			if (t6661Map != null && t6661Map.containsKey(p.getBatctrcde())) {
				List<Itempf> itempfList = t6661Map.get(p.getBatctrcde());
				Itempf item = itempfList.get(0);
				t6661rec.t6661Rec.set(StringUtil.rawToString(item.getGenarea()));
				if (isNE(t6661rec.contRevFlag, "Y") && isNE(t6661rec.contRevFlag, "N")) {
					sv.chdrselErr.set(errorsInner.f015);
					break;
		}
			} else {
			sv.chdrselErr.set(errorsInner.f014);
				break;

		}
		}
		if (!foundFlag) {
			sv.chdrselErr.set(errorsInner.f018);
		}
	}

protected void checkDates2800()
	{
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setPayrseqno(ZERO);
		payrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(),varcom.oK)
		&& isNE(payrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		if (isNE(payrIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(payrIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isEQ(payrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(errorsInner.e792);
			fatalError600();
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5688);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			itemIO.setItemitem(chdrlnbIO.getCnttype());
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			sv.chdrselErr.set(errorsInner.e308);
			return ;
		}
		
		t5688rec.t5688Rec.set(itemIO.getGenarea());
		datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
		
		//===========juge===========
		if(ctcnl002Permission){
			if(isEQ(sv.effdate,varcom.vrcmMaxDate)){
				datcon3rec.intDate2.set(wsaaToday);
			}else{
				datcon3rec.intDate2.set(sv.effdate);
			}
			datcon3rec.frequency.set("12");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
			if (isGT(datcon3rec.freqFactor,t5688rec.cfiLim)) {
				sv.effdateErr.set(errorsInner.f325);
			}
			
		}else{
			datcon3rec.intDate2.set(wsaaToday);
			datcon3rec.frequency.set("12");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
			if (isGT(datcon3rec.freqFactor,t5688rec.cfiLim)) {
				sv.chdrselErr.set(errorsInner.f325);
			}
		}
		
		
	}


protected void checkUdel2891()
	{
		List<Utrnpf> utrnextList = utrnpfDAO.searchUtrnextRecord(wsaaChdrcoyStore.toString(), wsaaChdrnumStore.toString());
		List<Utrspf> utrsList = utrspfDAO.searchUtrsRecord(wsaaChdrcoyStore.toString(), wsaaChdrnumStore.toString());
		if (utrnextList != null && !utrnextList.isEmpty()) {
			for (Utrnpf utrnextIO : utrnextList) {
				if (utrsList != null && !utrsList.isEmpty()) {
					for (Utrspf utrsIO : utrsList) {
						if (utrsIO.getChdrcoy().equals(utrnextIO.getChdrcoy())
								&& utrsIO.getChdrnum().equals(utrnextIO.getChdrnum())
								&& utrsIO.getLife().equals(utrnextIO.getLife())
								&& utrsIO.getCoverage().equals(utrnextIO.getCoverage())
								&& utrsIO.getRider().equals(utrnextIO.getRider())
								&& utrsIO.getPlanSuffix() == utrnextIO.getPlanSuffix()
								&& utrsIO.getUnitVirtualFund().equals(utrnextIO.getUnitVirtualFund())
								&& utrsIO.getUnitType().equals(utrnextIO.getUnitType())) {
							sv.chdrselErr.set(errorsInner.i030);
							return;
				}
			}
			}
		}
	}
	}

protected void a100CheckHitr()
	{
		String chdrnum = wsaaChdrnumStore.toString();
		List<Hitrpf> hitrrnlList = hitrpfDAO.searchHitrrnlRecord(wsspcomn.company.toString(), chdrnum);
		List<String> chdrnumList = new ArrayList<String>();
		chdrnumList.add(chdrnum);
		Map<String, List<Hitspf>> hitsMap = hitspfDAO.searchHitsRecordByChdrcoy(wsaaChdrcoyStore.toString(),
				chdrnumList);
		if (hitrrnlList != null && !hitrrnlList.isEmpty()) {
			for (Hitrpf hitrrnlIO : hitrrnlList) {
				if (hitsMap != null && hitsMap.containsKey(chdrnum)) {
					for (Hitspf hitsIO : hitsMap.get(chdrnum)) {
						if (hitsIO.getChdrcoy().equals(hitrrnlIO.getChdrcoy())
								&& hitsIO.getChdrnum().equals(hitrrnlIO.getChdrnum())
								&& hitsIO.getLife().equals(hitrrnlIO.getLife())
								&& hitsIO.getCoverage().equals(hitrrnlIO.getCoverage())
								&& hitsIO.getRider().equals(hitrrnlIO.getRider())
								&& hitsIO.getPlanSuffix() == hitrrnlIO.getPlanSuffix()
								&& hitsIO.getZintbfnd().equals(hitrrnlIO.getZintbfnd())) {
							sv.chdrselErr.set(errorsInner.hl08);
							return;
				}
				}
			}
			}
		}
	}
	
protected void freelookCheckCHN()  //ICIL-93	
	{
		
		/* Validate the cooling off period for freelook cancellation       */
		/* based on the rule defined in TD5GF.                             */
		getCoolOff();
		wsaaFreelookBasisChn.set(td5gfrec.ratebas);       
		if (!(issueDateChnBasis.isTrue()
		|| ackBasis.isTrue()
		|| deemedBasis.isTrue()		
		|| minAckDeemedBasis.isTrue())) {
			sv.chdrselErr.set(errorsInner.rlcb);
			return ;
		}
		a300ReadHpad();
		/* issue date*/
		if (issueDateChnBasis.isTrue()) {
			wsaaFreelookDate.set(hpadIO.getHissdte());
		}
		
		/* ack */
		if (ackBasis.isTrue()) {
			if (isNE(hpadIO.getPackdate(), varcom.vrcmMaxDate)
					&& isNE(hpadIO.getPackdate(), ZERO)) {
						wsaaFreelookDate.set(hpadIO.getPackdate());
					}
					else {
						sv.chdrselErr.set(errorsInner.rlaj);
						return ;
					}
		}
		
		/* deemed */
		if (deemedBasis.isTrue()) {
			if (isNE(hpadIO.getDeemdate(), varcom.vrcmMaxDate)
					&& isNE(hpadIO.getDeemdate(), ZERO)) {
						wsaaFreelookDate.set(hpadIO.getDeemdate());
					}
					else {
						sv.chdrselErr.set(errorsInner.rlaj);
						return ;
					}
		}
		
		/* min- ack or deemed */
		if (minAckDeemedBasis.isTrue()) {
			if (isGT(hpadIO.getDeemdate(), hpadIO.getPackdate())) {
				if (isNE(hpadIO.getPackdate(), varcom.vrcmMaxDate)
						&& isNE(hpadIO.getPackdate(), ZERO)) {
							wsaaFreelookDate.set(hpadIO.getPackdate());
						}
			}
			else {
				if (isNE(hpadIO.getDeemdate(), varcom.vrcmMaxDate)
						&& isNE(hpadIO.getDeemdate(), ZERO)) {
							wsaaFreelookDate.set(hpadIO.getDeemdate());
						}
						else {
							sv.chdrselErr.set(errorsInner.rlaj);
							return ;
						}
			}

		}

		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wsaaFreelookDate);
		//datcon3rec.intDate2.set(wsaaToday);
		//=========change========
		if(ctcnl002Permission){
			if(isEQ(sv.effdate,varcom.vrcmMaxDate)){
				datcon3rec.intDate2.set(wsaaToday);
			}else{
				datcon3rec.intDate2.set(sv.effdate);
			}
			datcon3rec.frequency.set("DY");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
		
			if (isGT(datcon3rec.freqFactor, th605rec.tdayno)) {
				sv.effdateErr.set(errorsInner.rlca);
			}
		}else{
			datcon3rec.intDate2.set(wsaaToday);
			datcon3rec.frequency.set("DY");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
		
			if (isGT(datcon3rec.freqFactor, th605rec.tdayno)) {
				sv.chdrselErr.set(errorsInner.rlca);
			}
		}
		
		
	
	}   
	

protected void readTd5gf1100()      // ICIL-93
{
	itempf = new Itempf();
	itempf.setItempfx(smtpfxcpy.item.toString());
	itempf.setItemcoy(wsspcomn.company.toString());
	itempf.setItemtabl(td5gf);/* IJTI-1523 */
	itempf.setItemitem(chdrlnbIO.getCnttype().toString() + chdrlnbIO.getSrcebus().toString());
	itempf = itempfDAO.getItempfRecord(itempf);
	
	
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf);/* IJTI-1523 */
		itempf.setItemitem(chdrlnbIO.getCnttype().toString() + "**");
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	
	
	if (itempf == null) {
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemtabl(td5gf);/* IJTI-1523 */
		itempf.setItemitem("*****");
		itempf = itempfDAO.getItempfRecord(itempf);	
	}
	
	if (itempf != null) {
		td5gfrec.td5gfRec.set(StringUtil.rawToString(itempf.getGenarea()));
	}
	else {
		td5gfrec.td5gfRec.set(SPACES);
	}
	
}
	
protected void getCoolOff()    // ICIL-93
{
	lifepf = lifepfDAO.getLifeRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(), "01", "00");
	if (lifepf== null ) {
		syserrrec.params.set(lifepf);
		fatalError600();
	}
	
	initialize(agecalcrec.agecalcRec);
	agecalcrec.function.set("CALCP");
	agecalcrec.language.set(wsspcomn.language);
	agecalcrec.intDate1.set(lifepf.getCltdob());
	agecalcrec.intDate2.set(wsaaToday);
	//agecalcrec.intDate2.set(sv.effdate);
	
	agecalcrec.company.set(wsspcomn.fsuco);
	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isNE(agecalcrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		fatalError600();
	}
	wsaaAnb.set(agecalcrec.agerating);
	readTd5gf1100();

		if (isEQ(lifepf.getCltsex(),td5gfrec.gender01)  && isGTE(wsaaAnb,td5gfrec.frmage01) && isLTE(wsaaAnb,td5gfrec.toage01)) {
			td5gfrec.cooloff.set(td5gfrec.coolingoff1);
			th605rec.tdayno.set(td5gfrec.coolingoff);
			
		}
		else {
			if (isEQ(lifepf.getCltsex(),td5gfrec.gender02) && isGTE(wsaaAnb,td5gfrec.frmage02) && isLTE(wsaaAnb,td5gfrec.toage02)) {
				td5gfrec.cooloff.set(td5gfrec.coolingoff2);
				 th605rec.tdayno.set(td5gfrec.coolingoff);
			}
			else {
				if (isEQ(lifepf.getCltsex(),td5gfrec.gender03) && isGTE(wsaaAnb,td5gfrec.frmage03) && isLTE(wsaaAnb,td5gfrec.toage03)) {
					td5gfrec.cooloff.set(td5gfrec.coolingoff3);
					th605rec.tdayno.set(td5gfrec.coolingoff);
				}
				
				else {
					if (isEQ(lifepf.getCltsex(),td5gfrec.gender04) && isGTE(wsaaAnb,td5gfrec.frmage04) && isLTE(wsaaAnb,td5gfrec.toage04)) {
						td5gfrec.cooloff.set(td5gfrec.coolingoff4);
						th605rec.tdayno.set(td5gfrec.coolingoff);
					}
				}
						
			}
		}
}
		

protected void a200CheckFreelook()
	{
		/* Validate the cooling off period for freelook cancellation       */
		/* based on the rule defined in TH605.                             */
		wsaaFreelookBasis.set(th605rec.ratebas);
		if (!(rcdBasis.isTrue()
		|| issueDateBasis.isTrue()
		|| ackDeemBasis.isTrue()
		|| ackDeemIssBasis.isTrue()
		|| maxMoneyUwBasis.isTrue())) {
			sv.chdrselErr.set(errorsInner.rlcb);
			return ;
		}
		if (rcdBasis.isTrue()) {
			wsaaFreelookDate.set(chdrlnbIO.getOccdate());
		}
		if (issueDateBasis.isTrue()) {
			a300ReadHpad();
			wsaaFreelookDate.set(hpadIO.getHissdte());
		}
		if (maxMoneyUwBasis.isTrue()) {
			a300ReadHpad();
			a400GetReceiptDate();
			if (isNE(sv.chdrselErr, SPACES)) {
				return ;
			}
			if (isGT(rtrnsacIO.getEffdate(), hpadIO.getHuwdcdte())) {
				wsaaFreelookDate.set(rtrnsacIO.getEffdate());
			}
			else {
				wsaaFreelookDate.set(hpadIO.getHuwdcdte());
			}
		}
		if (ackDeemBasis.isTrue()) {
			a300ReadHpad();
			if (isNE(hpadIO.getPackdate(), varcom.vrcmMaxDate)
			&& isNE(hpadIO.getPackdate(), ZERO)) {
				wsaaFreelookDate.set(hpadIO.getPackdate());
			}
			else {
				if (isNE(hpadIO.getDeemdate(), varcom.vrcmMaxDate)
				&& isNE(hpadIO.getDeemdate(), ZERO)) {
					wsaaFreelookDate.set(hpadIO.getDeemdate());
				}
				else {
					sv.chdrselErr.set(errorsInner.rlaj);
					return ;
				}
			}
		}
		if (ackDeemIssBasis.isTrue()) {
			a300ReadHpad();
			if (isNE(hpadIO.getPackdate(), varcom.vrcmMaxDate)
			&& isNE(hpadIO.getPackdate(), ZERO)) {
				wsaaFreelookDate.set(hpadIO.getPackdate());
			}
			else {
				if (isNE(hpadIO.getDeemdate(), varcom.vrcmMaxDate)
				&& isNE(hpadIO.getDeemdate(), ZERO)) {
					wsaaFreelookDate.set(hpadIO.getDeemdate());
				}
				else {
					wsaaFreelookDate.set(hpadIO.getHissdte());
				}
			}
		}
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(wsaaFreelookDate);
		datcon3rec.intDate2.set(wsaaToday);
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		if (isGT(datcon3rec.freqFactor, th605rec.tdayno)) {
			sv.chdrselErr.set(errorsInner.rlca);
		}
	}

protected void a300ReadHpad()
	{
		/*A300-BEGIN*/
		hpadIO.setDataArea(SPACES);
		hpadIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlnbIO.getChdrnum());
		hpadIO.setFormat(formatsInner.hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		/*A300-EXIT*/
	}

protected void a400GetReceiptDate()
	{
		/*A400-BEGIN*/
		a500ReadT5645();
		a600GetFirstSuspDate();
		/*A400-EXIT*/
	}

protected void a500ReadT5645()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProposalProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}


protected void a600GetFirstSuspDate()
	{
		rtrnsacIO.setDataArea(SPACES);
		rtrnsacIO.setRldgcoy(chdrlnbIO.getChdrcoy());
		rtrnsacIO.setSacscode(t5645rec.sacscode[2]);
		rtrnsacIO.setRldgacct(chdrlnbIO.getChdrnum());
		rtrnsacIO.setSacstyp(t5645rec.sacstype[2]);
		rtrnsacIO.setOrigccy(SPACES);
		rtrnsacIO.setFormat(formatsInner.rtrnrec);
		rtrnsacIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, rtrnsacIO);
		if (isNE(rtrnsacIO.getStatuz(), varcom.oK)
		&& isNE(rtrnsacIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnsacIO.getParams());
			syserrrec.statuz.set(rtrnsacIO.getStatuz());
			fatalError600();
		}
		if (isEQ(rtrnsacIO.getStatuz(), varcom.endp)
		|| isNE(rtrnsacIO.getRldgcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(rtrnsacIO.getSacscode(), t5645rec.sacscode[2])
		|| isNE(rtrnsacIO.getRldgacct(), chdrlnbIO.getChdrnum())
		|| isNE(rtrnsacIO.getSacstyp(), t5645rec.sacstype[2])
		|| isNE(rtrnsacIO.getRdocpfx(), "CA")) {
			sv.chdrselErr.set(errorsInner.e678);
		}
	}

protected void verifyBatchControl2900()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(errorsInner.e073);
			/*  The exit paragraph name will be standardised to 2090 -EXIT.    */
			/*        GO TO 2990-EXIT.                                         */
			return ;
		}
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}


protected void update3000()
	{
		if(start3000()){
					batching3080();
				}
				}

protected boolean start3000()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.action,"B")) {
			chdrenqIO.setParams(SPACES);
			chdrenqIO.setChdrnum(chdrlnbIO.getChdrnum());
			chdrenqIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			chdrenqIO.setFunction(varcom.readr);
			chdrenqIO.setFormat(formatsInner.chdrenqrec);
			chdrpf = chdrpfDAO.getChdrpf(wsspcomn.company.toString().trim(), sv.chdrsel.toString().trim()); //ILIFE-6816
			chdrpfDAO.setCacheObject(chdrpf); //ILIFE-6816
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				fatalError600();
			}
			chdrenqIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				fatalError600();
			}
			return true;
		}
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(errorsInner.f910);
			wsspcomn.edterror.set("Y");
			return false;
		}
		
		
		return true;
	}


	

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*START*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e073 = new FixedLengthStringData(4).init("E073");
	private FixedLengthStringData e070 = new FixedLengthStringData(4).init("E070");
	private FixedLengthStringData e308 = new FixedLengthStringData(4).init("E308");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData e544 = new FixedLengthStringData(4).init("E544");
	private FixedLengthStringData e678 = new FixedLengthStringData(4).init("E678");
	private FixedLengthStringData e767 = new FixedLengthStringData(4).init("E767");
	private FixedLengthStringData e792 = new FixedLengthStringData(4).init("E792");
	private FixedLengthStringData f014 = new FixedLengthStringData(4).init("F014");
	private FixedLengthStringData f015 = new FixedLengthStringData(4).init("F015");
	private FixedLengthStringData f018 = new FixedLengthStringData(4).init("F018");
	private FixedLengthStringData f031 = new FixedLengthStringData(4).init("F031");
	private FixedLengthStringData f325 = new FixedLengthStringData(4).init("F325");
	private FixedLengthStringData f910 = new FixedLengthStringData(4).init("F910");
	private FixedLengthStringData i030 = new FixedLengthStringData(4).init("I030");
	private FixedLengthStringData hl08 = new FixedLengthStringData(4).init("HL08");
	private FixedLengthStringData rlca = new FixedLengthStringData(4).init("RLCA");
	private FixedLengthStringData rlcb = new FixedLengthStringData(4).init("RLCB");
	private FixedLengthStringData rlaj = new FixedLengthStringData(4).init("RLAJ");
	private FixedLengthStringData f073 = new FixedLengthStringData(4).init("F073");
	private FixedLengthStringData t044 = new FixedLengthStringData(4).init("T044");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC   ");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC   ");
	private FixedLengthStringData rtrnrec = new FixedLengthStringData(10).init("RTRNREC   ");
}
}
