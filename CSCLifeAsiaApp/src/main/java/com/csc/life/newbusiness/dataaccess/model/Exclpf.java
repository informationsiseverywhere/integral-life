package com.csc.life.newbusiness.dataaccess.model;

import java.io.Serializable;
import java.util.Date;

public class Exclpf implements Serializable{
	
	private long unique_number;
	private String chdrcoy;
	private String chdrnum;
	private String crtable;
	private int seqno;
	private String excda;
	private int effdate;
	private int subseqnbr;
	private String exadtxt;
	private String prntstat;
	private String usrprf;	
	private String jobnm;	
	private Date datime;
	private String validflag; //PINNACLE-2855
	private Integer tranno; //PINNACLE-2855
	private String life;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	

	
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCrtable() {
		return crtable;
	}
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	public int getSeqno() {
		return seqno;
	}
	public void setSeqno(int seqno) {
		this.seqno = seqno;
	}
	public String getExcda() {
		return excda;
	}
	public void setExcda(String excda) {
		this.excda = excda;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public int getSubseqnbr() {
		return subseqnbr;
	}
	public void setSubseqnbr(int subseqnbr) {
		this.subseqnbr = subseqnbr;
	}
	public String getExadtxt() {
		return exadtxt;
	}
	public void setExadxt(String exadtxt) {
		this.exadtxt = exadtxt;
	}
	public String getPrntstat() {
		return prntstat;
	}
	public void setPrntstat(String prntstat) {
		this.prntstat = prntstat;
	}
	public String getUsrprf() {
		return usrprf;
	}
	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}
	public String getJobnm() {
		return jobnm;
	}
	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public String getLife() {
		return life;
	}
	public void setLife(String life) {
		this.life = life;
	}
	public String getCoverage() {
		return coverage;
	}
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	public String getRider() {
		return rider;
	}
	public void setRider(String rider) {
		this.rider = rider;
	}
	public Integer getPlnsfx() {
		return plnsfx;
	}
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	
	
}
