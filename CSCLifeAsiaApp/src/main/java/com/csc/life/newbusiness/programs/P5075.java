/*
 * File: P5075.java
 * Date: 30 August 2009 0:03:24
 * Author: Quipoz Limited
 *
 * Class transformed from P5075.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.procedures.Uwlmtchk;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.enquiries.recordstructures.Uwlmtrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LfcllnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ResnTableDAM;
import com.csc.life.newbusiness.screens.S5075ScreenVars;
import com.csc.life.productdefinition.dataaccess.MliaafiTableDAM;
import com.csc.life.productdefinition.dataaccess.MliachdTableDAM;
import com.csc.life.productdefinition.procedures.Rlliadb;
import com.csc.life.productdefinition.recordstructures.Rlliadbrec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5500rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T5689rec;
import com.csc.life.reassurance.dataaccess.CovtcsnTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*        Withdraw/Decline/Reinstate Contract
*
* Initialise
* ----------
*
*
* The details of the contract being worked on will be stored in
* the CHDRLNB I/O module.  Retrieve the details.
*
* Look up the following always:
*       Contract type from T5688,
*       Contract status from T3623.
*       Servicing branch from T1692 (default from sign-on branch)
*
* Look up the  contract  definition  details from T5688. If the
* minimum  number of  policies  in  the  plan  is  blank,  plan
* processing is not  applicable  to the contract. In this case,
* non-display and protect the prompt for the number of policies
* in the plan.  Otherwise,  if the number of policies is blank,
* move in the default value from the table.
*
*
*  Look up the following:
*       Premium status from T5075, (***NO, FROM T3588!!!)         <003>
*       The owner's client (CLTS) details.  Call FMTCLTN to
*            format the client name for confirmation.
*       Count the number of life (LIFELNB) records attached
*            to the proposal.  For each life and joint life
*            record read, check to see if any are a life on
*            another proposal (use LFCLLNB),
*       The agent (AGNT)  to  get  the  client  number, and
*            hence the  client (agent's) name. Call FMTCLTN
*            to format the client name for confirmation.
*
*
* Validation
* ----------
*
* All information entered is optional, so no validation is
* required.
*
* Updating
* --------
*
*
* Update the Contract header details as follows:
*
*   Add one to the contract transaction number
*   Contract status to status on T5679
*   Contract status to today
*   Contract status transaction no to contract transaction no.
*   Transaction Number last used to contract transaction no.
*   Use contract number to read MLIACHD, if the indicator is
*   'Y' then update the record in MLIACHD else delete the
*   contract record in MLIACHD (for reversal transacton).
*   If the reason code or narrative was entered, write a resn
*   record with the information entered .
*
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*
*
*****************************************************************
* </pre>
*/
public class P5075 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5075");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t3623 = "T3623";
	private static final String t5679 = "T5679";
	protected static final String t5688 = "T5688";
	private static final String t5689 = "T5689";
	private static final String t5500 = "T5500";
	private static final String t3588 = "T3588";
	private static final String tr384 = "TR384";
	protected PackedDecimalData wsaaX = new PackedDecimalData(3, 0).init(0);
	protected PackedDecimalData wsaaY = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaZ = new PackedDecimalData(3, 0).init(0);
	protected ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaKill = new FixedLengthStringData(1).init(SPACES);
	private Validator firstTime = new Validator(wsaaKill, "N");

	private FixedLengthStringData wserOwnersel = new FixedLengthStringData(1);
	private Validator wserOwnerselInvalid = new Validator(wserOwnersel, "Y");

	private FixedLengthStringData wserAgntsel = new FixedLengthStringData(1);
	private Validator wserAgntselInvalid = new Validator(wserAgntsel, "Y");

	private FixedLengthStringData wserCntbranch = new FixedLengthStringData(1);
	private Validator wserCntbranchInvalid = new Validator(wserCntbranch, "Y");

	private FixedLengthStringData wserBilling = new FixedLengthStringData(1);
	private Validator wserBillingInvalid = new Validator(wserBilling, "Y");
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaJlifeNum = new FixedLengthStringData(2).isAPartOf(wsaaJlife, 0, REDEFINE);
	private ZonedDecimalData wsaaJlifeN = new ZonedDecimalData(2, 0).isAPartOf(wsaaJlifeNum, 0).setUnsigned();
		/* WSAA-END-HEX */
	private PackedDecimalData wsaaHex20 = new PackedDecimalData(3, 0).init(200).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaHex20, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaEndUnderline = new FixedLengthStringData(1).isAPartOf(filler1, 0);
		/* WSAA-START-HEX */
	private PackedDecimalData wsaaHex26 = new PackedDecimalData(3, 0).init(260).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(2).isAPartOf(wsaaHex26, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaStartUnderline = new FixedLengthStringData(1).isAPartOf(filler3, 0);

	private FixedLengthStringData wsaaHeading = new FixedLengthStringData(26);
	private FixedLengthStringData[] wsaaHeadingChar = FLSArrayPartOfStructure(26, 1, wsaaHeading, 0);

	protected FixedLengthStringData wsaaHedline = new FixedLengthStringData(24);
	private FixedLengthStringData[] wsaaHead = FLSArrayPartOfStructure(24, 1, wsaaHedline, 0);
//MIBT-345
	private FixedLengthStringData wsaaErrorlineSs = new FixedLengthStringData(80);
	private FixedLengthStringData wsaaLeveldss = new FixedLengthStringData(6).isAPartOf(wsaaErrorlineSs, 0);
	private FixedLengthStringData wsaaLevelss = new FixedLengthStringData(3).isAPartOf(wsaaErrorlineSs, 6);
	private FixedLengthStringData wsaaErrss1 = new FixedLengthStringData(16).isAPartOf(wsaaErrorlineSs, 9);
	private ZonedDecimalData wsaaTrsaSs = new ZonedDecimalData(15, 2).isAPartOf(wsaaErrorlineSs, 25).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData wsaaErrss2 = new FixedLengthStringData(15).isAPartOf(wsaaErrorlineSs, 45);
	private ZonedDecimalData wsaaUwlmtSs = new ZonedDecimalData(15, 2).isAPartOf(wsaaErrorlineSs, 60).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");

	private FixedLengthStringData wsaaErrorlineS = new FixedLengthStringData(80);
	private FixedLengthStringData wsaaLevelds = new FixedLengthStringData(6).isAPartOf(wsaaErrorlineS, 0);
	private FixedLengthStringData wsaaLevels = new FixedLengthStringData(3).isAPartOf(wsaaErrorlineS, 6);
	private FixedLengthStringData wsaaErrs1 = new FixedLengthStringData(16).isAPartOf(wsaaErrorlineS, 9);
	private ZonedDecimalData wsaaTrsaS = new ZonedDecimalData(15, 2).isAPartOf(wsaaErrorlineS, 25).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private FixedLengthStringData wsaaErrs2 = new FixedLengthStringData(15).isAPartOf(wsaaErrorlineS, 45);
	private ZonedDecimalData wsaaUwlmtS = new ZonedDecimalData(15, 2).isAPartOf(wsaaErrorlineS, 60).setPattern("Z,ZZZ,ZZZ,ZZZ,ZZ9.99");
	private ZonedDecimalData wsaaRcd = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private FixedLengthStringData wsddBillfreq = new FixedLengthStringData(2);
	private FixedLengthStringData wsddMop = new FixedLengthStringData(1);
		/*Joind FSU and Life agent headers - new b*/
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage/Rider Transaction Details*/
	private CovtcsnTableDAM covtcsnIO = new CovtcsnTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
		/*Follow-ups - life new business*/
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LfcllnbTableDAM lfcllnbIO = new LfcllnbTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		/*Substd & Hosp by IC/Contract No./Action*/
	private MliaafiTableDAM mliaafiIO = new MliaafiTableDAM();
		/*Substandard & Hospitalisatn by Contr No.*/
	private MliachdTableDAM mliachdIO = new MliachdTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
		/*Reason Details  Logical File*/
	private ResnTableDAM resnIO = new ResnTableDAM();
	protected Batckey wsaaBatckey = new Batckey();
	private T5500rec t5500rec = new T5500rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T5689rec t5689rec = new T5689rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Batcuprec batcuprec = new Batcuprec();
	protected Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Uwlmtrec uwlmtrec = new Uwlmtrec();
	protected Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Rlliadbrec rlliadbrec = new Rlliadbrec();
	private Crtundwrec crtundwrec = new Crtundwrec();
	protected Wssplife wssplife = new Wssplife();
	//private S5075ScreenVars sv = ScreenProgram.getScreenVars( S5075ScreenVars.class);
	private S5075ScreenVars sv =   getLScreenVars();

	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1190,
		exit1690,
		nextLife3826,
		exit3829,
		m200Exit
	}

	public P5075() {
		super();
		screenVars = sv;
		new ScreenModel("S5075", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		blankOutFields1020();
	}

protected void initialise1010()
	{
		/* First time in only, read accounting rules for transaction*/
		/*                     look up the singed-on branch description*/
		/*                     get todays date*/
		/*    IF WSAA-BATCKEY NOT = SPACES*/
		/*       GO TO 1020-BLANK-OUT-FIELDS.*/
		/* Set up the batch key fields.*/
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		/*    MOVE WSSP-BATCHKEY          TO WSAA-BATCKEY.*/
		/*    MOVE SPACES                 TO ITEM-DATA-KEY.*/
		/*    MOVE 'IT'                   TO ITEM-ITEMPFX.*/
		/*    MOVE WSSP-COMPANY           TO ITEM-ITEMCOY.*/
		/*    MOVE T5645                  TO ITEM-ITEMTABL.*/
		/*    MOVE WSKY-BATC-BATCTRCDE    TO ITEM-ITEMITEM.*/
		/*    MOVE 'READR'                TO ITEM-FUNCTION.*/
		/*    CALL 'ITEMIO' USING ITEM-PARAMS.*/
		/*    IF ITEM-STATUZ              NOT = O-K*/
		/*                            AND NOT = MRNF*/
		/*        MOVE ITEM-STATUZ        TO SYSR-STATUZ*/
		/*        PERFORM 600-FATAL-ERROR.*/
		/*    MOVE ITEM-GENAREA TO T5645-T5645-REC.*/
		/*1890-EXIT.*/
		/*    EXIT.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}

protected void blankOutFields1020()
	{	
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		sv.billcd.set(varcom.vrcmMaxDate);
		sv.lassured.set(ZERO);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.polinc.set(ZERO);
		sv.batctrcde.set(wsaaBatckey.batcBatctrcde);
		wserOwnersel.set("Y");
		wserAgntsel.set("Y");
		wserCntbranch.set("Y");
		wserBilling.set("Y");
		/* Retrieve contract fields from I/O module*/
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		if (isNE(chdrlnbIO.getOccdate(),ZERO)) {
			sv.occdate.set(chdrlnbIO.getOccdate());
		}
		t5688rec.polmin.set(ZERO);
		/* get servicing branch description*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(chdrlnbIO.getCntbranch());
		sv.cntbranch.set(chdrlnbIO.getCntbranch());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.sbrdesc.fill("?");
		}
		else {
			sv.sbrdesc.set(descIO.getLongdesc());
		}
		/* Retrieve contract type from T5688*/
		/*       validation rules from T5689*/
		readContractTables1600();
		if (isNE(sv.occdateErr,SPACES)) {
			wserBilling.set("Y");
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			wsaaHedline.set(descIO.getLongdesc());
			sv.ctypedes.set(descIO.getLongdesc());/*ILIFE-7467*/
		}
		else {
			wsaaHedline.fill("?");
			sv.ctypedes.fill("?");/*ILIFE-7467*/
		}
		loadHeading1100();
		/*    Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrlnbIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.statdsc.set(descIO.getShortdesc());
		}
		else {
			sv.statdsc.fill("?");
		}
		/*   Check to see if plan processing is applicable.*/
		if (isEQ(t5688rec.polmin,ZERO)) {
			sv.polincOut[varcom.nd.toInt()].set("Y");
			sv.polincOut[varcom.pr.toInt()].set("Y");
		}
		else {
			if (isEQ(chdrlnbIO.getPolinc(),ZERO)) {
				sv.polinc.set(t5688rec.poldef);
			}
		}
		wserOwnersel.set(SPACES);
		wserAgntsel.set(SPACES);
		wserCntbranch.set(SPACES);
		wserBilling.set(SPACES);
		/*  Load all fields from the Contract Header to the Screen*/
		sv.polinc.set(chdrlnbIO.getPolinc());
		sv.ownersel.set(chdrlnbIO.getCownnum());
		sv.occdate.set(chdrlnbIO.getOccdate());
		sv.billfreq.set(chdrlnbIO.getBillfreq());
		sv.mop.set(chdrlnbIO.getBillchnl());
		sv.billcd.set(chdrlnbIO.getBillcd());
		sv.cntcurr.set(chdrlnbIO.getCntcurr());
		sv.register.set(chdrlnbIO.getRegister());
		sv.srcebus.set(chdrlnbIO.getSrcebus());
		sv.reptype.set(chdrlnbIO.getReptype());
		sv.lrepnum.set(chdrlnbIO.getRepnum());
		sv.cntbranch.set(chdrlnbIO.getCntbranch());
		sv.agntsel.set(chdrlnbIO.getAgntnum());
		sv.campaign.set(chdrlnbIO.getCampaign());
		sv.stcal.set(chdrlnbIO.getChdrstcda());
		sv.stcbl.set(chdrlnbIO.getChdrstcdb());
		sv.stccl.set(chdrlnbIO.getChdrstcdc());
		sv.stcdl.set(chdrlnbIO.getChdrstcdd());
		sv.stcel.set(chdrlnbIO.getChdrstcde());
		/*  Look up premium status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrlnbIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.oK)) {
			sv.premStatDesc.set(descIO.getShortdesc());
		}
		else {
			sv.premStatDesc.fill("?");
		}
		/*   Look up all other descriptions*/
		formatClientName1700();
		if (isNE(sv.ownerselErr,SPACES)) {
			wserOwnersel.set("Y");
		}
		if (isNE(sv.cntbranchErr,SPACES)) {
			wserCntbranch.set("Y");
		}
		formatAgentName1900();
		if (isNE(sv.agntselErr,SPACES)) {
			wserAgntsel.set("Y");
		}
		/*   Count no of lives assured.*/
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, lifelnbIO);
		if ((isNE(lifelnbIO.getStatuz(),varcom.oK))
		&& (isNE(lifelnbIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		sv.conprosal.set("N");
		while ( !((isNE(lifelnbIO.getChdrcoy(),wsspcomn.company))
		|| (isNE(lifelnbIO.getChdrnum(),chdrlnbIO.getChdrnum()))
		|| (isEQ(lifelnbIO.getStatuz(),varcom.endp)))) {
			countLives1300();
		}
		saveValues1615();
	}

protected void loadHeading1100()
	{
		try {
			loadScreen1110();
		}
		catch (GOTOException e){
		}
	}

protected void loadScreen1110()
	{
		wsaaHeading.set(SPACES);
		for (wsaaX.set(24); !(isLT(wsaaX,1)
		|| isNE(wsaaHead[wsaaX.toInt()], SPACES)); wsaaX.add(-1))
{
			/* No processing required. */
		}
		compute(wsaaY, 0).set(sub(24,wsaaX));
		if (isNE(wsaaY,ZERO)) {
			wsaaY.divide(2);
		}
		wsaaY.add(1);
		wsaaZ.set(ZERO);
		//wsaaHeadingChar[wsaaY.toInt()].set(wsaaStartUnderline);
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(wsaaX);
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			moveChar1130();
		}
		wsaaX.add(1);
		//wsaaHeadingChar[wsaaX.toInt()].set(wsaaEndUnderline);
		sv.heading.set(wsaaHeading);
		goTo(GotoLabel.exit1190);
	}

protected void moveChar1130()
	{
		wsaaZ.add(1);
		compute(wsaaX, 0).set(add(wsaaY,wsaaZ));
		wsaaHeadingChar[wsaaX.toInt()].set(wsaaHead[wsaaZ.toInt()]);
	}

protected void countLives1300()
	{
					count1300();
					readNextLife1380();
				}

protected void count1300()
	{
		sv.lassured.add(1);
		if (isEQ(sv.conprosal,"Y")) {
			return ;
		}
		lfcllnbIO.setDataKey(SPACES);
		lfcllnbIO.setChdrcoy(wsspcomn.company);
		lfcllnbIO.setLifcnum(lifelnbIO.getLifcnum());
		lfcllnbIO.setFunction(varcom.begn);
		lfcllnbIO.setStatuz(varcom.oK);
		/* MOVE READR                  TO LFCLLNB-FUNCTION.             */
		/* CALL 'LFCLLNBIO'  USING LFCLLNB-PARAMS.                      */
		/* IF LFCLLNB-STATUZ           NOT = O-K                        */
		/*                         AND NOT = MRNF                       */
		/*     MOVE LFCLLNB-PARAMS     TO SYSR-PARAMS                   */
		/*     PERFORM 600-FATAL-ERROR.                                 */
		/* IF LFCLLNB-STATUZ = O-K                                      */
		/*     MOVE 'Y'                TO S5075-CONPROSAL.              */
		do{
			SmartFileCode.execute(appVars, lfcllnbIO);
			if (isNE(lfcllnbIO.getStatuz(),varcom.oK)
			&& isNE(lfcllnbIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(lfcllnbIO.getParams());
				fatalError600();
			}
			if (isNE(lfcllnbIO.getChdrcoy(),wsspcomn.company)
			|| isNE(lfcllnbIO.getLifcnum(),lifelnbIO.getLifcnum())) {
				lfcllnbIO.setStatuz(varcom.endp);
			}
			if (isEQ(lfcllnbIO.getStatuz(),varcom.oK)
			&& isNE(lfcllnbIO.getChdrnum(),lifelnbIO.getChdrnum())) {
				sv.conprosal.set("Y");
				lfcllnbIO.setStatuz(varcom.endp);
			}
			else {
				lfcllnbIO.setFunction(varcom.nextr);
			}
		}
		while ( !(isEQ(lfcllnbIO.getStatuz(),varcom.endp)));
	}

protected void readNextLife1380()
	{
		lifelnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if ((isNE(lifelnbIO.getStatuz(),varcom.oK))
		&& (isNE(lifelnbIO.getStatuz(),varcom.endp))) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readContractTables1600()
	{
		try {
			readContractDefinition1610();
			readBillingValidation1650();
		}
		catch (GOTOException e){
		}
	}

protected void readContractDefinition1610()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5688)
		|| isNE(itdmIO.getItemitem(),chdrlnbIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.occdateErr.set(errorsInner.f290);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
	}

protected void readBillingValidation1650()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItemtabl(t5689);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(sv.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5689)
		|| isNE(itdmIO.getItemitem(),chdrlnbIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.occdateErr.set(errorsInner.e268);
			goTo(GotoLabel.exit1690);
		}
		else {
			t5689rec.t5689Rec.set(itdmIO.getGenarea());
		}
		/*  Check for single MoP and/or frequency*/
		wsddBillfreq.set(SPACES);
		wsddMop.set(SPACES);
		wsaaX.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 30); loopVar2 += 1){
			checkTable1660();
		}
		if (isNE(wsddBillfreq,"??")) {
			sv.billfreq.set(wsddBillfreq);
			sv.billfreqOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.billfreqOut[varcom.pr.toInt()].set(SPACES);
		}
		if (isNE(wsddMop,"?")) {
			sv.mop.set(wsddMop);
			sv.mopOut[varcom.pr.toInt()].set("Y");
		}
		else {
			sv.mopOut[varcom.pr.toInt()].set(SPACES);
		}
		goTo(GotoLabel.exit1690);
	}

protected void checkTable1660()
	{
		if (isNE(t5689rec.billfreq[wsaaX.toInt()],SPACES)
		&& isNE(wsddBillfreq,"??")) {
			if (isEQ(wsddBillfreq,SPACES)) {
				wsddBillfreq.set(t5689rec.billfreq[wsaaX.toInt()]);
			}
			else {
				if (isNE(t5689rec.billfreq[wsaaX.toInt()],wsddBillfreq)) {
					wsddBillfreq.set("??");
				}
			}
		}
		if (isNE(t5689rec.mop[wsaaX.toInt()],SPACES)
		&& isNE(wsddMop,"?")) {
			if (isEQ(wsddMop,SPACES)) {
				wsddMop.set(t5689rec.mop[wsaaX.toInt()]);
			}
			else {
				if (isNE(t5689rec.mop[wsaaX.toInt()],wsddMop)) {
					wsddMop.set("?");
				}
			}
		}
		wsaaX.add(1);
	}

protected void formatClientName1700()
	{
			readClientRecord1710();
		}

protected void readClientRecord1710()
	{
		if (isEQ(sv.ownersel,SPACES)) {
			sv.ownerselErr.set(errorsInner.e186);
			return ;
		}
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.ownersel);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)
		|| isNE(cltsIO.getValidflag(),1)) {
			sv.ownerselErr.set(errorsInner.e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void formatAgentName1900()
	{
			readAgent1910();
		}

protected void readAgent1910()
	{
		if (isEQ(sv.agntsel,SPACES)) {
			sv.agntselErr.set(errorsInner.e186);
			return ;
		}
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntsel);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		sv.agentname.set(SPACES);
		if (isEQ(aglflnbIO.getStatuz(),varcom.mrnf)) {
			sv.agntselErr.set(errorsInner.e305);
			return ;
		}
		if (isNE(sv.occdate,ZERO)
		&& isNE(sv.occdate,varcom.vrcmMaxDate)) {
			if (isLT(aglflnbIO.getDtetrm(),sv.occdate)
			|| isLT(aglflnbIO.getDteexp(),sv.occdate)
			|| isGT(aglflnbIO.getDteapp(),sv.occdate)) {
				sv.agntselErr.set(errorsInner.e475);
			}
		}
		if (isNE(aglflnbIO.getAgntbr(),sv.cntbranch)) {
			sv.agntselErr.set(errorsInner.e455);
		}
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(aglflnbIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'S5075IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5075-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag,"M")
		|| isEQ(wsspcomn.flag,"E")
		|| isEQ(wsspcomn.flag,"G")) {
			checkAuthority2100();
		}
		/* IF  S5075-REASONCD          = SPACES                 <A06859>*/
		/*     IF S5075-RESNDESC       = SPACES                 <A06859>*/
		/*          MOVE '?'           TO S5075-RESNDESC        <A06859>*/
		/*     END-IF                                           <A06859>*/
		/*     GO TO 2090-EXIT                                  <A06859>*/
		/* END-IF.                                              <A06859>*/
		/*                                                      <A06859>*/
		/* IF  S5075-RESNDESC      NOT = SPACES                 <A06859>*/
		/*       GO TO 2090-EXIT                                <A06859>*/
		/* END-IF.                                              <A06859>*/
		/* IF SCRN-STATUZ              = CALC                           */
		/*    MOVE 'Y'                 TO WSSP-EDTERROR.                */
		/* Only read T5500 for the description if the user has not already */
		/* provided one.                                                   */
			if (isEQ(sv.resndesc,SPACES)) {
			readT5500Desc2200();
		}
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void checkAuthority2100()
	{
		start2110();
	}

protected void start2110()
	{
		uwlmtrec.function.set("CHCK");
		uwlmtrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		uwlmtrec.chdrnum.set(chdrlnbIO.getChdrnum());
		uwlmtrec.cnttyp.set(chdrlnbIO.getCnttype());
		uwlmtrec.cownpfx.set(chdrlnbIO.getCownpfx());
		uwlmtrec.lifcnum.set(sv.ownersel);
		uwlmtrec.language.set(wsspcomn.language);
		uwlmtrec.fsuco.set(wsspcomn.fsuco);
		uwlmtrec.userid.set(wsspcomn.userid);
		callProgram(Uwlmtchk.class, uwlmtrec.rec);
		if (isNE(uwlmtrec.statuz,varcom.oK)) {
			if (isEQ(uwlmtrec.statuz,"SS")) {
				wsaaLevelss.set(uwlmtrec.uwlevel);
				wsaaLeveldss.set("Level:");
				wsaaErrss1.set(". Sub Stnd TRSA Amt ");
				wsaaTrsaSs.set(uwlmtrec.sumins);
				wsaaErrss2.set(" > Sub Stnd UW Lmt ");
				wsaaUwlmtSs.set(uwlmtrec.undwrlim);
				scrnparams.errorline.set(wsaaErrorlineSs);
			}
			else {
				wsaaLevels.set(uwlmtrec.uwlevel);
				wsaaLevelds.set("Level:");
				wsaaErrs1.set(". Stnd TRSA Amt ");
				wsaaTrsaS.set(uwlmtrec.sumins);
				wsaaErrs2.set(" > Stnd UW Lmt ");
				wsaaUwlmtS.set(uwlmtrec.undwrlim);
				scrnparams.errorline.set(wsaaErrorlineS);
			}
			wsspcomn.edterror.set("Y");
		}
	}

protected void readT5500Desc2200()
	{
		read2210();
	}

protected void read2210()
	{
		initialize(descIO.getRecKeyData());
		initialize(descIO.getRecNonKeyData());
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5500);
		descIO.setDescitem(sv.reasoncd);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.resndesc.set(descIO.getLongdesc());
		}
		else {
			sv.resndesc.set(SPACES);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		/*  Update contract header fields as follows*/
		/*  contract status date to today's date*/
		chdrlnbIO.setStatdate(wsaaToday);
		/*  read T5679 for contract status*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			scrnparams.errorCode.set(errorsInner.f321);
			return ;
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		chdrlnbIO.setStatcode(t5679rec.setCnRiskStat);
		chdrlnbIO.setPstatcode(t5679rec.setCnPremStat);
		/*  contract status transaction no to contract tran no*/
		chdrlnbIO.setStattran(chdrlnbIO.getTranno());
		chdrlnbIO.setPstattran(chdrlnbIO.getTranno());
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		chdrlnbIO.setFunction("WRITS");
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.flag,"G")) {
			m100ReadMliachd();
		}
		else {
			checkSubstd3500();
			if (isEQ(t5500rec.mind,"Y")) {
				rlliadbrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
				rlliadbrec.chdrnum.set(chdrlnbIO.getChdrnum());
				rlliadbrec.fsucoy.set(wsspcomn.fsuco);
				rlliadbrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
				rlliadbrec.language.set(wsspcomn.language);
				callProgram(Rlliadb.class, rlliadbrec.rlliaRec);
				if (isNE(rlliadbrec.statuz,varcom.oK)) {
					syserrrec.params.set(rlliadbrec.rlliaRec);
					syserrrec.statuz.set(rlliadbrec.statuz);
					fatalError600();
				}
			}
		}
		/* Write a PTRN record.*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlnbIO.getChdrnum());
		ptrnIO.setTranno(chdrlnbIO.getTranno());
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setCrtuser(wsspcomn.userid);  //IJS-523
		ptrnIO.setValidflag(1);//ILIFE-7929
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
		/* Update the batch header using BATCUP.*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(wsaaBatckey.batcKey);
		batcuprec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(ZERO);
		batcuprec.sub.set(ZERO);
		batcuprec.bcnt.set(ZERO);
		batcuprec.bval.set(ZERO);
		batcuprec.ascnt.set(ZERO);
		batcuprec.statuz.set(ZERO);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			fatalError600();
		}
		/* Create a RESN record if reason code or narrative was entered*/
		if (isNE(sv.resndesc,SPACES)
		|| isNE(sv.reasoncd,SPACES)) {
			resnIO.setDataKey(SPACES);
			resnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			resnIO.setChdrnum(chdrlnbIO.getChdrnum());
			resnIO.setTranno(chdrlnbIO.getTranno());
			resnIO.setReasoncd(sv.reasoncd);
			resnIO.setResndesc(sv.resndesc);
			resnIO.setTrancde(wsaaBatckey.batcBatctrcde);
		resnIO.setTransactionDate(varcom.vrcmDate);
		resnIO.setTransactionTime(varcom.vrcmTime);
		resnIO.setUser(varcom.vrcmUser);
			resnIO.setFormat(formatsInner.resnrec);
		resnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, resnIO);
		if (isNE(resnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(resnIO.getParams());
			fatalError600();
		}
		}
		/* If not inquiry mode, remove all UNDRPF records.              */
		if (isNE(wsspcomn.flag,"I")) {
			deleteUndrpf3700();
		}
		if (isEQ(wsspcomn.flag,"M")
		|| isEQ(wsspcomn.flag,"E")) {
			writeLetter6000();
		}
		/* Release the soft lock on the contract.*/
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		/* If reversing Withdraw/Decline/Postpone, generate UNDRPF rec  */
		/*    IF WSSP-FLAG                = 'E'                    <V71L11>*/
		if (isEQ(wsspcomn.flag,"G")) {
			createUndrpf3800();
		}
		lockTheContract();
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void checkSubstd3500()
	{
		checkSubstdFlag3510();
	}

protected void checkSubstdFlag3510()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5500);
		itemIO.setItemitem(sv.reasoncd);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),"MRNF")) {
			sv.reasoncdErr.set(errorsInner.e268);
		}
		else {
			t5500rec.t5500Rec.set(itemIO.getGenarea());
		}
	}

protected void deleteUndrpf3700()
	{
		delete3710();
	}

protected void delete3710()
	{
		initialize(crtundwrec.parmRec);
		crtundwrec.coy.set(chdrlnbIO.getChdrcoy());
		crtundwrec.currcode.set(chdrlnbIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrlnbIO.getChdrnum());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
	}

protected void createUndrpf3800()
	{
		/*CREATE*/
		covtcsnIO.setRecKeyData(SPACES);
		covtcsnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covtcsnIO.setChdrnum(chdrlnbIO.getChdrnum());
		covtcsnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtcsnIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtcsnIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covtcsnIO.setFormat(formatsInner.covtcsnrec);
		covtcsnIO.setStatuz(varcom.oK);
		while ( !(isNE(covtcsnIO.getStatuz(),varcom.oK))) {
			createUndrCovt3820();
		}

		/*EXIT*/
	}

protected void createUndrCovt3820()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
		try {
				switch (nextMethod) {
				case DEFAULT:
			create3825();
				case nextLife3826:
			nextLife3826();
				case exit3829:
				}
				break;
		}
		catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
		}
	}
	}

protected void create3825()
	{
		SmartFileCode.execute(appVars, covtcsnIO);
		if (isEQ(covtcsnIO.getStatuz(),varcom.endp)
		|| isNE(chdrlnbIO.getChdrcoy(),covtcsnIO.getChdrcoy())
		|| isNE(chdrlnbIO.getChdrnum(),covtcsnIO.getChdrnum())) {
			covtcsnIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3829);
		}
		covtcsnIO.setFunction(varcom.nextr);
		initialize(crtundwrec.parmRec);
		wsaaJlifeN.set(ZERO);
	}

protected void nextLife3826()
	{
		m300ReadLife();
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		crtundwrec.clntnum.set(lifelnbIO.getLifcnum());
		crtundwrec.coy.set(covtcsnIO.getChdrcoy());
		crtundwrec.chdrnum.set(covtcsnIO.getChdrnum());
		crtundwrec.life.set(covtcsnIO.getLife());
		crtundwrec.crtable.set(covtcsnIO.getCrtable());
		crtundwrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		crtundwrec.sumins.set(covtcsnIO.getSumins());
		crtundwrec.cnttyp.set(chdrlnbIO.getCnttype());
		crtundwrec.currcode.set(chdrlnbIO.getCntcurr());
		crtundwrec.function.set("ADD");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			fatalError600();
		}
		wsaaJlifeN.add(1);
		goTo(GotoLabel.nextLife3826);
	}

protected void m100ReadMliachd()
	{
		/*M100-START*/
		mliachdIO.setStatuz(varcom.oK);
		mliachdIO.setMlentity(chdrlnbIO.getChdrnum());
		mliachdIO.setFormat(formatsInner.mliachdrec);
		mliachdIO.setFunction(varcom.begnh);
		while ( !(isEQ(mliachdIO.getStatuz(),varcom.endp))) {
			m200ReadMliachdio();
		}

		/*M100-EXIT*/
	}

protected void m200ReadMliachdio()
	{
		try {
			m200Start();
			m200Nextr();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void m200Start()
	{
		SmartFileCode.execute(appVars, mliachdIO);
		if (isNE(mliachdIO.getStatuz(),varcom.oK)
		&& isNE(mliachdIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mliachdIO.getStatuz());
			syserrrec.params.set(mliachdIO.getParams());
			fatalError600();
		}
		if (isNE(chdrlnbIO.getChdrnum(),mliachdIO.getMlentity())) {
			mliachdIO.setStatuz(varcom.endp);
		}
		if (isEQ(mliachdIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.m200Exit);
		}
		if (isEQ(mliachdIO.getMind(),"Y")) {
			mliaafiIO.setMlcoycde(mliachdIO.getMlcoycde());
			mliaafiIO.setMloldic(mliachdIO.getMloldic());
			mliaafiIO.setMlothic(mliachdIO.getMlothic());
			mliaafiIO.setClntnaml(mliachdIO.getClntnaml());
			mliaafiIO.setDob(mliachdIO.getDob());
			mliaafiIO.setRskflg(mliachdIO.getRskflg());
			mliaafiIO.setHpropdte(mliachdIO.getHpropdte());
			mliaafiIO.setMlhsperd(mliachdIO.getMlhsperd());
			mliaafiIO.setMlmedcde01(mliachdIO.getMlmedcde01());
			mliaafiIO.setMlmedcde02(mliachdIO.getMlmedcde02());
			mliaafiIO.setMlmedcde03(mliachdIO.getMlmedcde03());
			mliaafiIO.setIndc("Y");
			mliaafiIO.setMind("N");
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
			mliaafiIO.setEffdate(wsaaToday);
			mliaafiIO.setSecurityno(mliachdIO.getSecurityno());
			mliaafiIO.setMlentity(chdrlnbIO.getChdrnum());
			mliaafiIO.setActn("3");
			mliaafiIO.setFormat(formatsInner.mliaafirec);
			mliaafiIO.setFunction(varcom.updat);
			SmartFileCode.execute(appVars, mliaafiIO);
			if (isNE(mliaafiIO.getStatuz(),varcom.oK)
			&& isNE(mliaafiIO.getStatuz(),varcom.mrnf)) {
				syserrrec.statuz.set(mliaafiIO.getStatuz());
				syserrrec.params.set(mliaafiIO.getParams());
				fatalError600();
			}
		}
		else {
			mliachdIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, mliachdIO);
			if (isNE(mliachdIO.getStatuz(),varcom.oK)
			&& isNE(mliachdIO.getStatuz(),varcom.mrnf)) {
				syserrrec.statuz.set(mliachdIO.getStatuz());
				syserrrec.params.set(mliachdIO.getParams());
				fatalError600();
			}
		}
	}

protected void m200Nextr()
	{
		mliachdIO.setFunction(varcom.nextr);
	}

protected void m300ReadLife()
	{
		m300Read();
	}

protected void m300Read()
	{
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setChdrcoy(wsspcomn.company);
		lifelnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		lifelnbIO.setLife(covtcsnIO.getLife());
		lifelnbIO.setJlife(wsaaJlife);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void writeLetter6000()
	{
			readT66346010();
		}

protected void readT66346010()
	{
		/*  Get the Letter type from T6634.                               <*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		/* MOVE T6634                      TO ITEM-ITEMTABL.    <PCPPRT>*/
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction cod       <*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlnbIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		/* IF  ITEM-STATUZ             NOT = O-K                <FUPLET>*/
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("***", SPACES);
			stringVariable2.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
			stringVariable2.setStringInto(itemIO.getItemitem());
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)
			&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
				syserrrec.statuz.set(itemIO.getStatuz());
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
				itemIO.setGenarea(SPACES);
			}
		}
		/* MOVE ITEM-GENAREA               TO T6634-T6634-REC.  <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF  T6634-LETTER-TYPE           = SPACES             <PCPPRT>*/
		if (isEQ(tr384rec.letterType,SPACES)) {
			return ;
		}
		/*  Write LETC via LETRQST and update the seqno by 1              <*/
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlnbIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.clntcoy.set(chdrlnbIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlnbIO.getCownnum());
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.rdocpfx.set("CH");
		letrqstrec.rdoccoy.set(chdrlnbIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlnbIO.getChdrnum());
		letrqstrec.rdocnum.set(chdrlnbIO.getChdrnum());
		letrqstrec.tranno.set(chdrlnbIO.getTranno());
		letrqstrec.branch.set(wsspcomn.branch);
		letrqstrec.despnum.set(chdrlnbIO.getDespnum());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);
		letrqstrec.otherKeys.set(wsaaBatckey.batcBatctrcde);
		StringUtil stringVariable3 = new StringUtil();
		stringVariable3.addExpression(chdrlnbIO.getChdrnum(), SPACES);
		stringVariable3.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable3.addExpression(wsspcomn.language, SPACES);
		stringVariable3.setStringInto(letrqstrec.otherKeys);
		letrqstrec.function.set("ADD");
		/* IF  T6634-LETTER-TYPE       NOT = SPACES             <PCPPRT>*/
		if (isNE(tr384rec.letterType,SPACES)) {
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz,varcom.oK)) {
				syserrrec.params.set(letrqstrec.params);
				syserrrec.statuz.set(letrqstrec.statuz);
				fatalError600();
			}
		}
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner {
	private FixedLengthStringData e186 = new FixedLengthStringData(4).init("E186");
	private FixedLengthStringData e268 = new FixedLengthStringData(4).init("E268");
	private FixedLengthStringData e304 = new FixedLengthStringData(4).init("E304");
	private FixedLengthStringData e305 = new FixedLengthStringData(4).init("E305");
	private FixedLengthStringData e455 = new FixedLengthStringData(4).init("E455");
	private FixedLengthStringData e475 = new FixedLengthStringData(4).init("E475");
	private FixedLengthStringData f290 = new FixedLengthStringData(4).init("F290");
	private FixedLengthStringData f321 = new FixedLengthStringData(4).init("F321");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData resnrec = new FixedLengthStringData(10).init("RESNREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData mliachdrec = new FixedLengthStringData(10).init("MLIACHDREC");
	private FixedLengthStringData mliaafirec = new FixedLengthStringData(10).init("MLIAAFIREC");
	private FixedLengthStringData covtcsnrec = new FixedLengthStringData(10).init("COVTCSNREC");
	
}
protected void lockTheContract() {
	// TODO Auto-generated method stub
	
}


public void setWsaaProg(FixedLengthStringData wsaaProg) {
	this.wsaaProg = wsaaProg;
}
protected void saveValues1615()
{
	
}
protected S5075ScreenVars getLScreenVars() {
	return ScreenProgram.getScreenVars(S5075ScreenVars.class);
}


}
