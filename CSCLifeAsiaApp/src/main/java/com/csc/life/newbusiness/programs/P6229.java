/*
 * File: P6229.java
 * Date: 30 August 2009 0:37:15
 * Author: Quipoz Limited
 *
 * Class transformed from P6229.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.SlckpfDAO;
import com.csc.smart400framework.dataaccess.dao.impl.SlckpfDAOImpl;
import com.csc.smart400framework.dataaccess.model.Slckpf;
import com.csc.smart400framework.parent.Mainf;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*
*              TIDY UP AND COMPLETE OUTSTANDING UPDATING
*              =========================================
*
*   This  is  the  mainline program called when a normal "end" of
*   proposal maintenance is requested. It purpose is to generally
*   tidy up and complete the outstanding updating.
*
*   No  screen  is  required, thus there will be no processing in
*   either  the  1000 or 2000 sections. All processing will be in
*   the 3000 section.
*
* Updating
* --------
*
*   This program may be run in one of two modes:
*
*        A) The  contract  proposal is being set up for the first
*             time,
*
*        B) A existing proposal is being modified.
*
*   To determine which version is  to  be  run, RETRV the current
*   contract header (CHDRLNB) being  worked  on.  Then attempt to
*   READR this record. If there is  no  record  found,  a new one
*   must be set up -  version A. Otherwise, the contract proposal
*   already exists - version B.
*
*   If version A:
*
*        1) write a PTRN record initialising fields as follows:
*             - contract key from contract header,
*             - transaction number from  contract header (will be
*                  1),
*             - record code blank,
*             - transaction  effective   date   to   TDAY   (from
*                  DATCON1),
*             - batch key information from WSSP.
*
*        2) write a client role record for the contract owner:
*             - client prefix - 'CN'
*             - client company - FSU company from WSSP,
*             - client number - from contract header (owner),
*             - role - 'OW',
*             - foreign key prefix - 'CH',
*             - foreign key company - sign-on company from WSSP,
*             - foreign key number - contract number.
*
*   If version B:
*
*        1) The details held in the  CHDRLNB linkage area will be
*             for the old version  of  the contract header. Store
*             the client number of  the original owner in working
*             storage  and  retrieve   the  new  contract  header
*             details again. If the old and new owner numbers are
*             the same, do nothing.  Otherwise, update the client
*             roles file by deleting the  owner  role for the old
*             client and adding an owner  role  for  the new one.
*             Fields as above.
*
*   For both versions:
*
*        1) store the record stamping details (user, transaction,
*             date, time) with the KEEPS function (CHDRLNB).
*
*        2) write  the stored contract  header  to  the  database
*             (call CHDRLNB with WRITS function).
*
*        3) update the batch header  by  calling  BATCUP  with  a
*             function of WRITS and the following parameters:
*             - transaction count = 1,
*             - all other amounts zero,
*             - batch key from WSSP.
*
*        4) release the soft lock  on  the contract (call SFTLOCK
*             with function UNLK).
*
*****************************************************************
* </pre>
*/
public class P6229 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6229");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaChdrlnbCownnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaFunction = new FixedLengthStringData(1).init(SPACES);
		/* FORMATS */
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String ptrnrec = "PTRNREC";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Batcuprec batcuprec1 = new Batcuprec();
	private Sftlockrec sftlockrec2 = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Batckey wsaaBatckey = new Batckey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private SlckpfDAO slckpfDAO = new SlckpfDAOImpl();
	public P6229() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		/*INITIALISE*/
		/** NOT USED.*/
		/*EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required / WSSP*/
		/* Set up the batch key fields.*/
		wsaaBatckey.batcKey.set(wsspcomn.batchkey);
		/* Retrieve contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Now read the contract header information.*/
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if ((isNE(chdrlnbIO.getStatuz(), varcom.oK))
		&& (isNE(chdrlnbIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Determine which version to run.*/
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			chdrlnbIO.setFunction(varcom.retrv);
			chdrlnbIO.setFormat(chdrlnbrec);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
			else {
				newContract3200();
			}
		}
		else {
			modifyContract3300();
		}
		/* Store the record stamping details.*/
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Write the contract header.*/
		chdrlnbIO.setFunction(varcom.writs);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if ((isNE(chdrlnbIO.getStatuz(), varcom.oK))
		&& (isNE(chdrlnbIO.getStatuz(), varcom.dupr))) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		
		insertDataCustomerSpecific();
		
		/* Update the batch header using BATCUP.*/
		batcuprec1.batcupRec.set(SPACES);
		batcuprec1.batcpfx.set(wsaaBatckey.batcBatcpfx);
		batcuprec1.batccoy.set(wsaaBatckey.batcBatccoy);
		batcuprec1.batcbrn.set(wsaaBatckey.batcBatcbrn);
		batcuprec1.batcactyr.set(wsaaBatckey.batcBatcactyr);
		batcuprec1.batcactmn.set(wsaaBatckey.batcBatcactmn);
		batcuprec1.batctrcde.set(wsaaBatckey.batcBatctrcde);
		batcuprec1.batcbatch.set(wsaaBatckey.batcBatcbatch);
		batcuprec1.trancnt.set(1);
		batcuprec1.etreqcnt.set(ZERO);
		batcuprec1.sub.set(ZERO);
		batcuprec1.bcnt.set(ZERO);
		batcuprec1.bval.set(ZERO);
		batcuprec1.ascnt.set(ZERO);
		batcuprec1.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec1.batcupRec);
		if (isNE(batcuprec1.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec1.batcupRec);
			fatalError600();
		}
		callBldenrl3500();
		/* Release the soft lock on the contract.*/
		Slckpf slckpf=new Slckpf();
		slckpf.setEnttyp("CH");
		slckpf.setCompany(wsspcomn.company.toString());
		slckpf.setEntity(chdrlnbIO.getChdrnum().toString());
		
		Slckpf result = slckpfDAO.readHSlckData(slckpf);
		if (result != null) {
			sftlockrec2.sftlockRec.set(SPACES);
			sftlockrec2.company.set(wsspcomn.company);
			sftlockrec2.entity.set(chdrlnbIO.getChdrnum());
			sftlockrec2.enttyp.set("CH");
			sftlockrec2.user.set(varcom.vrcmUser);
			sftlockrec2.transaction.set(wsaaBatckey.batcBatctrcde);
			sftlockrec2.statuz.set(SPACES);
			sftlockrec2.function.set("UNLK");
			callProgram(Sftlock.class, sftlockrec2.sftlockRec);
			if (isNE(sftlockrec2.statuz, varcom.oK)) {
				syserrrec.params.set(sftlockrec2.sftlockRec);
				syserrrec.statuz.set(sftlockrec2.statuz);
				fatalError600();
			}
		}
	}

	/**
	* <pre>
	*     CONTRACT PROPOSAL BEING SET UP FOR THE FIRST TIME
	* </pre>
	*/
protected void newContract3200()
	{
		newContract3210();
	}

protected void newContract3210()
	{
		/* Write a PTRN record.*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(wsaaBatckey.batcBatccoy);
		ptrnIO.setBatcbrn(wsaaBatckey.batcBatcbrn);
		ptrnIO.setBatcactyr(wsaaBatckey.batcBatcactyr);
		ptrnIO.setBatcactmn(wsaaBatckey.batcBatcactmn);
		ptrnIO.setBatctrcde(wsaaBatckey.batcBatctrcde);
		ptrnIO.setBatcbatch(wsaaBatckey.batcBatcbatch);
		ptrnIO.setChdrpfx(chdrlnbIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlnbIO.getChdrnum());
		ptrnIO.setTranno(chdrlnbIO.getTranno());
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		/*MOVE VRCM-TERM              TO PTRN-TERMID.                  */
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setCrtuser(wsspcomn.userid);  //PINNACLE-2931
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* MOVE DTC1-INT-DATE          TO PTRN-PTRNEFF.                 */
		/*  MOVE CHDRLNB-OCCDATE        TO PTRN-PTRNEFF.         <LA2113>*/
		ptrnIO.setPtrneff(datcon1rec.intDate);
		ptrnIO.setDatesub(datcon1rec.intDate);
		ptrnIO.setValidflag(1);//ILIFE-7929
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			fatalError600();
		}
		/* Write a client role record.*/
		cltrelnrec.data.set(SPACES);
		cltrelnrec.function.set("ADD  ");
		clrrCall3400();
	}

	/**
	* <pre>
	*     CONTRACT PROPOSAL BEING SET UP FOR THE FIRST TIME
	* </pre>
	*/
protected void modifyContract3300()
	{
		modifyContract3310();
	}

protected void modifyContract3310()
	{
		wsaaChdrlnbCownnum.set(chdrlnbIO.getCownnum());
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* If the old and new are the same then skip client role*/
		/* creation.*/
		/*IF CHDRLNB-COWNNUM          NOT = WSAA-CHDRLNB-COWNNUM       */
		if (isEQ(chdrlnbIO.getCownnum(), wsaaChdrlnbCownnum)) {
			return ;
		}
		/* Delete the client role record.*/
		cltrelnrec.data.set(SPACES);
		cltrelnrec.function.set("DEL  ");
		clrrCall3400();
		/* Write a new client role record.                                 */
		cltrelnrec.data.set(SPACES);
		cltrelnrec.function.set("ADD  ");
		clrrCall3400();
	}

	/**
	* <pre>
	*     CLIENT ROLE RECORD IO MODULE CALL
	* </pre>
	*/
protected void clrrCall3400()
	{
		clrrCall3410();
		actualCall3420();
	}

protected void clrrCall3410()
	{
		/* If the client role record is to be deleted then by pass*/
		/* setting up the key as this has already been done when*/
		/* the read hold occurred.*/
		/*    IF CLRR-FUNCTION            = DELET*/
		/*       GO TO 3420-ACTUAL-CALL.*/
		/* Create the key for accessing the client role record.*/
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsspcomn.fsuco);
		cltrelnrec.clntnum.set(chdrlnbIO.getCownnum());
		/* WSAA-CHDRLNB-COWNNUM contains the old owner.                    */
		if (isEQ(cltrelnrec.function, "DEL  ")) {
			cltrelnrec.clntnum.set(wsaaChdrlnbCownnum);
		}
		cltrelnrec.clrrrole.set("OW");
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(wsspcomn.company);
		cltrelnrec.forenum.set(chdrlnbIO.getChdrnum());
	}

protected void actualCall3420()
	{
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callBldenrl3500()
	{
		/*START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrlnbIO.getChdrpfx());
		bldenrlrec.company.set(chdrlnbIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrlnbIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
protected void insertDataCustomerSpecific(){}

}
