package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;


import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface NlgtpfDAO extends BaseDAO<Nlgtpf> {
	
	public List<Nlgtpf> readNlgtpf(String chdrcoy, String chdrnum);
	public Nlgtpf getNlgtRecord(String chdrcoy,String chdrnum);
	public void insertNlgtpf(Nlgtpf nlgtpf);
	public Nlgtpf getNlgRecord(String chdrcoy,String chdrnum,String validflag);
	public Nlgtpf getnlgtran(String chdrcoy,String chdrnum,String tranno);
	public Nlgtpf getnlgmaxtran(String chdrcoy,String chdrnum);
}
