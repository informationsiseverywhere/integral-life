package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN Date: 20 December 2019 Author: vdivisala
 */
@SuppressWarnings("unchecked")
public class Sjl05screen extends ScreenRecord {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] { 4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21 };
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] { 1, 22, 3, 79 });
	}

	/**
	 * Writes a record to the screen.
	 * 
	 * @param errorInd
	 *            - will be set on if an error occurs
	 * @param noRecordFoundInd
	 *            - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3) {
		Sjl05ScreenVars sv = (Sjl05ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sjl05screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
	}

	public static void clearClassString(VarModel pv) {
		Sjl05ScreenVars screenVars = (Sjl05ScreenVars) pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.billfreq01.setClassString("");
		screenVars.billfreq02.setClassString("");
		screenVars.billfreq03.setClassString("");
		screenVars.billfreq04.setClassString("");
		screenVars.billfreq05.setClassString("");
		screenVars.billfreq06.setClassString("");
		screenVars.billfreq07.setClassString("");
		screenVars.billfreq08.setClassString("");
		screenVars.billfreq09.setClassString("");
		screenVars.billfreq10.setClassString("");
		
		screenVars.mop01.setClassString("");
		screenVars.mop02.setClassString("");
		screenVars.mop03.setClassString("");
		screenVars.mop04.setClassString("");
		screenVars.mop05.setClassString("");
		screenVars.mop06.setClassString("");
		screenVars.mop07.setClassString("");
		screenVars.mop08.setClassString("");
		screenVars.mop09.setClassString("");
		screenVars.mop10.setClassString("");
		
		screenVars.fwcondt01.setClassString("");
		screenVars.fwcondt02.setClassString("");
		screenVars.fwcondt03.setClassString("");
		screenVars.fwcondt04.setClassString("");
		screenVars.fwcondt05.setClassString("");
		screenVars.fwcondt06.setClassString("");
		screenVars.fwcondt07.setClassString("");
		screenVars.fwcondt08.setClassString("");
		screenVars.fwcondt09.setClassString("");
		screenVars.fwcondt10.setClassString("");
		
		screenVars.concommflg01.setClassString("");
		screenVars.concommflg02.setClassString("");
		screenVars.concommflg03.setClassString("");
		screenVars.concommflg04.setClassString("");
		screenVars.concommflg05.setClassString("");
		screenVars.concommflg06.setClassString("");
		screenVars.concommflg07.setClassString("");
		screenVars.concommflg08.setClassString("");
		screenVars.concommflg09.setClassString("");
		screenVars.concommflg10.setClassString("");
	}

	/**
	 * Clear all the variables in Sjl05screen
	 */
	public static void clear(VarModel pv) {
		Sjl05ScreenVars screenVars = (Sjl05ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.billfreq01.clear();
		screenVars.billfreq02.clear();
		screenVars.billfreq03.clear();
		screenVars.billfreq04.clear();
		screenVars.billfreq05.clear();
		screenVars.billfreq06.clear();
		screenVars.billfreq07.clear();
		screenVars.billfreq08.clear();
		screenVars.billfreq09.clear();
		screenVars.billfreq10.clear();
		
		screenVars.mop01.clear();
		screenVars.mop02.clear();
		screenVars.mop03.clear();
		screenVars.mop04.clear();
		screenVars.mop05.clear();
		screenVars.mop06.clear();
		screenVars.mop07.clear();
		screenVars.mop08.clear();
		screenVars.mop09.clear();
		screenVars.mop10.clear();
		
		screenVars.fwcondt01.clear();
		screenVars.fwcondt02.clear();
		screenVars.fwcondt03.clear();
		screenVars.fwcondt04.clear();
		screenVars.fwcondt05.clear();
		screenVars.fwcondt06.clear();
		screenVars.fwcondt07.clear();
		screenVars.fwcondt08.clear();
		screenVars.fwcondt09.clear();
		screenVars.fwcondt10.clear();
		
		screenVars.concommflg01.clear();
		screenVars.concommflg02.clear();
		screenVars.concommflg03.clear();
		screenVars.concommflg04.clear();
		screenVars.concommflg05.clear();
		screenVars.concommflg06.clear();
		screenVars.concommflg07.clear();
		screenVars.concommflg08.clear();
		screenVars.concommflg09.clear();
		screenVars.concommflg10.clear();
	}
}