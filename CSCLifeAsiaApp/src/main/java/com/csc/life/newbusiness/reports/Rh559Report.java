package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH559.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh559Report extends SMARTReportLayout { 

	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData clntsname = new FixedLengthStringData(30);
	private FixedLengthStringData cntbranch = new FixedLengthStringData(2);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private ZonedDecimalData crlimit = new ZonedDecimalData(17, 2);//ILIFE-2536
	private FixedLengthStringData datetexc = new FixedLengthStringData(22);
	private FixedLengthStringData descr = new FixedLengthStringData(30);
	private FixedLengthStringData entity = new FixedLengthStringData(16);
	private ZonedDecimalData origross = new ZonedDecimalData(17, 2);//ILIFE-2536
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totprem = new ZonedDecimalData(17, 2);//ILIFE-2536
	private FixedLengthStringData zxidno = new FixedLengthStringData(10);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh559Report() {
		super();
	}


	/**
	 * Print the XML for Rh559b00
	 */
	public void printRh559b00(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh559b00",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh559d01
	 */
	public void printRh559d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(30).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		entity.setFieldName("entity");
		entity.setInternal(subString(recordData, 1, 16));
		clntsname.setFieldName("clntsname");
		clntsname.setInternal(subString(recordData, 17, 30));
		zxidno.setFieldName("zxidno");
		zxidno.setInternal(subString(recordData, 47, 10));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 57, 3));
		crlimit.setFieldName("crlimit");
		crlimit.setInternal(subString(recordData, 60, 17));//ILIFE-2536
		origross.setFieldName("origross");
		origross.setInternal(subString(recordData, 77, 17));//ILIFE-2536
		totprem.setFieldName("totprem");
		totprem.setInternal(subString(recordData, 94, 17));//ILIFE-2536
		descr.setFieldName("descr");
		descr.setInternal(subString(recordData, 111, 30));//ILIFE-2536
		if(indicArea.charAt(30).equals("1")){
		printLayout("Rh559d01",			// Record name
			new BaseData[]{			// Fields:
				entity,
				clntsname,
				zxidno,
				cntcurr,
				crlimit,
				origross,
				totprem,
				descr
			}
			, new Object[] {			// indicators
				new Object[]{"ind30", indicArea.charAt(30)}
			}
		);
		}else{
			printLayout("Rh559d01",			// Record name
					new BaseData[]{			// Fields:
						entity,
						clntsname,
						zxidno,
						cntcurr,
						descr
					}
					, new Object[] {			// indicators
						new Object[]{"ind30", indicArea.charAt(30)}
					}
				);
		}

	}

	/**
	 * Print the XML for Rh559e01
	 */
	public void printRh559e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(5);

		printLayout("Rh559e01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh559h01
	 */
	public void printRh559h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		datetexc.setFieldName("datetexc");
		datetexc.setInternal(subString(recordData, 11, 22));
		time.setFieldName("time");
		time.set(getTime());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 33, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 34, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		cntbranch.setFieldName("cntbranch");
		cntbranch.setInternal(subString(recordData, 64, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 66, 30));
		printLayout("Rh559h01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				datetexc,
				time,
				company,
				companynm,
				pagnbr,
				cntbranch,
				branchnm
			}
		);

		currentPrintLine.set(9);
	}


}
