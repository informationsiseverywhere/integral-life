package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:26
 * Description:
 * Copybook name: TH575REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th575rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th575Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData scrtitles = new FixedLengthStringData(34).isAPartOf(th575Rec, 0);
  	public FixedLengthStringData[] scrtitle = FLSArrayPartOfStructure(2, 17, scrtitles, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(34).isAPartOf(scrtitles, 0, FILLER_REDEFINE);
  	public FixedLengthStringData scrtitle01 = new FixedLengthStringData(17).isAPartOf(filler, 0);
  	public FixedLengthStringData scrtitle02 = new FixedLengthStringData(17).isAPartOf(filler, 17);
  	public FixedLengthStringData ttdesc = new FixedLengthStringData(17).isAPartOf(th575Rec, 34);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(449).isAPartOf(th575Rec, 51, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th575Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th575Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}