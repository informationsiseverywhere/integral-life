package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HxclpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:36
 * Class transformed from HXCLPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HxclpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 524;
	public FixedLengthStringData hxclrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hxclpfRecord = hxclrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hxclrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hxclrec);
	public PackedDecimalData fupno = DD.fupno.copy().isAPartOf(hxclrec);
	public PackedDecimalData hxclseqno = DD.hxclseqno.copy().isAPartOf(hxclrec);
	public FixedLengthStringData hxclnote01 = DD.hxclnote.copy().isAPartOf(hxclrec);
	public FixedLengthStringData hxclnote02 = DD.hxclnote.copy().isAPartOf(hxclrec);
	public FixedLengthStringData hxclnote03 = DD.hxclnote.copy().isAPartOf(hxclrec);
	public FixedLengthStringData hxclnote04 = DD.hxclnote.copy().isAPartOf(hxclrec);
	public FixedLengthStringData hxclnote05 = DD.hxclnote.copy().isAPartOf(hxclrec);
	public FixedLengthStringData hxclnote06 = DD.hxclnote.copy().isAPartOf(hxclrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hxclrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hxclrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hxclrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(hxclrec);
	public FixedLengthStringData fupcode = DD.fupcde.copy().isAPartOf(hxclrec);
	public FixedLengthStringData hxcllettyp = DD.hxcllettyp.copy().isAPartOf(hxclrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HxclpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HxclpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HxclpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HxclpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HxclpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HxclpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HxclpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HXCLPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"FUPNO, " +
							"HXCLSEQNO, " +
							"HXCLNOTE01, " +
							"HXCLNOTE02, " +
							"HXCLNOTE03, " +
							"HXCLNOTE04, " +
							"HXCLNOTE05, " +
							"HXCLNOTE06, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"USER_T, " +
							"FUPCDE, " +
							"HXCLLETTYP, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     fupno,
                                     hxclseqno,
                                     hxclnote01,
                                     hxclnote02,
                                     hxclnote03,
                                     hxclnote04,
                                     hxclnote05,
                                     hxclnote06,
                                     userProfile,
                                     jobName,
                                     datime,
                                     user,
                                     fupcode,
                                     hxcllettyp,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		fupno.clear();
  		hxclseqno.clear();
  		hxclnote01.clear();
  		hxclnote02.clear();
  		hxclnote03.clear();
  		hxclnote04.clear();
  		hxclnote05.clear();
  		hxclnote06.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		user.clear();
  		fupcode.clear();
  		hxcllettyp.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHxclrec() {
  		return hxclrec;
	}

	public FixedLengthStringData getHxclpfRecord() {
  		return hxclpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHxclrec(what);
	}

	public void setHxclrec(Object what) {
  		this.hxclrec.set(what);
	}

	public void setHxclpfRecord(Object what) {
  		this.hxclpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hxclrec.getLength());
		result.set(hxclrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}