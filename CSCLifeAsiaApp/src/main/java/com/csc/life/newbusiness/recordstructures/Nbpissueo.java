package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:04
 * Description:
 * Copybook name: NBPISSUEO
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Nbpissueo extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rec = new FixedLengthStringData(937);
  	public FixedLengthStringData messageHeader = new FixedLengthStringData(30).isAPartOf(rec, 0);
  	public FixedLengthStringData msgid = new FixedLengthStringData(10).isAPartOf(messageHeader, 0);
  	public ZonedDecimalData msglng = new ZonedDecimalData(5, 0).isAPartOf(messageHeader, 10).setUnsigned();
  	public ZonedDecimalData msgcnt = new ZonedDecimalData(5, 0).isAPartOf(messageHeader, 15).setUnsigned();
  	public FixedLengthStringData filler = new FixedLengthStringData(10).isAPartOf(messageHeader, 20, FILLER);
  	public FixedLengthStringData messageData = new FixedLengthStringData(907).isAPartOf(rec, 30);
  	public FixedLengthStringData bgenS6378 = new FixedLengthStringData(907).isAPartOf(messageData, 0);
  	public FixedLengthStringData[] bgenS6378Sfl = FLSArrayPartOfStructure(30, 28, bgenS6378, 0);
  	public FixedLengthStringData[] bgenS6378Erordsc = FLSDArrayPartOfArrayStructure(24, bgenS6378Sfl, 0);
  	public FixedLengthStringData[] bgenS6378Crtable = FLSDArrayPartOfArrayStructure(4, bgenS6378Sfl, 24);
  	public ZonedDecimalData bgenPrppreissOccdate = new ZonedDecimalData(8, 0).isAPartOf(bgenS6378, 840).setUnsigned();
  	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(bgenPrppreissOccdate, 0, FILLER_REDEFINE);
  	public ZonedDecimalData bgenPrppreissOccdateCcyy = new ZonedDecimalData(4, 0).isAPartOf(filler1, 0).setUnsigned();
  	public ZonedDecimalData bgenPrppreissOccdateMm = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4).setUnsigned();
  	public ZonedDecimalData bgenPrppreissOccdateDd = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6).setUnsigned();
  	public ZonedDecimalData bgenPrppreissBillcd = new ZonedDecimalData(8, 0).isAPartOf(bgenS6378, 848).setUnsigned();
  	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(bgenPrppreissBillcd, 0, FILLER_REDEFINE);
  	public ZonedDecimalData bgenPrppreissBillcdCcyy = new ZonedDecimalData(4, 0).isAPartOf(filler2, 0).setUnsigned();
  	public ZonedDecimalData bgenPrppreissBillcdMm = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4).setUnsigned();
  	public ZonedDecimalData bgenPrppreissBillcdDd = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6).setUnsigned();
  	public FixedLengthStringData bgenPrppreissBillfreq = new FixedLengthStringData(2).isAPartOf(bgenS6378, 856);
  	public FixedLengthStringData bgenPrppreissBillchnl = new FixedLengthStringData(2).isAPartOf(bgenS6378, 858);
  	public FixedLengthStringData bgenPrppreissAgentname = new FixedLengthStringData(47).isAPartOf(bgenS6378, 860);


	public void initialize() {
		COBOLFunctions.initialize(rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}