package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6226screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static int maxRecords = 6;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {11, 12, 21, 22, 26, 27, 36, 37}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {5, 10, 2, 71}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6226ScreenVars sv = (S6226ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6226screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6226screensfl, 
			sv.S6226screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6226ScreenVars sv = (S6226ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6226screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6226ScreenVars sv = (S6226ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6226screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6226screensflWritten.gt(0))
		{
			sv.s6226screensfl.setCurrentIndex(0);
			sv.S6226screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6226ScreenVars sv = (S6226ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6226screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6226ScreenVars screenVars = (S6226ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.hasgnnum.setFieldName("hasgnnum");
				screenVars.hseqno.setFieldName("hseqno");
				screenVars.asgnsel.setFieldName("asgnsel");
				screenVars.assigneeName.setFieldName("assigneeName");
				screenVars.reasoncd.setFieldName("reasoncd");
				screenVars.commfromDisp.setFieldName("commfromDisp");
				screenVars.commtoDisp.setFieldName("commtoDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.updteflag.set(dm.getField("updteflag"));
			screenVars.hasgnnum.set(dm.getField("hasgnnum"));
			screenVars.hseqno.set(dm.getField("hseqno"));
			screenVars.asgnsel.set(dm.getField("asgnsel"));
			screenVars.assigneeName.set(dm.getField("assigneeName"));
			screenVars.reasoncd.set(dm.getField("reasoncd"));
			screenVars.commfromDisp.set(dm.getField("commfromDisp"));
			screenVars.commtoDisp.set(dm.getField("commtoDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6226ScreenVars screenVars = (S6226ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.updteflag.setFieldName("updteflag");
				screenVars.hasgnnum.setFieldName("hasgnnum");
				screenVars.hseqno.setFieldName("hseqno");
				screenVars.asgnsel.setFieldName("asgnsel");
				screenVars.assigneeName.setFieldName("assigneeName");
				screenVars.reasoncd.setFieldName("reasoncd");
				screenVars.commfromDisp.setFieldName("commfromDisp");
				screenVars.commtoDisp.setFieldName("commtoDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("updteflag").set(screenVars.updteflag);
			dm.getField("hasgnnum").set(screenVars.hasgnnum);
			dm.getField("hseqno").set(screenVars.hseqno);
			dm.getField("asgnsel").set(screenVars.asgnsel);
			dm.getField("assigneeName").set(screenVars.assigneeName);
			dm.getField("reasoncd").set(screenVars.reasoncd);
			dm.getField("commfromDisp").set(screenVars.commfromDisp);
			dm.getField("commtoDisp").set(screenVars.commtoDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6226screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6226ScreenVars screenVars = (S6226ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.updteflag.clearFormatting();
		screenVars.hasgnnum.clearFormatting();
		screenVars.hseqno.clearFormatting();
		screenVars.asgnsel.clearFormatting();
		screenVars.assigneeName.clearFormatting();
		screenVars.reasoncd.clearFormatting();
		screenVars.commfromDisp.clearFormatting();
		screenVars.commtoDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6226ScreenVars screenVars = (S6226ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.updteflag.setClassString("");
		screenVars.hasgnnum.setClassString("");
		screenVars.hseqno.setClassString("");
		screenVars.asgnsel.setClassString("");
		screenVars.assigneeName.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.commfromDisp.setClassString("");
		screenVars.commtoDisp.setClassString("");
	}

/**
 * Clear all the variables in S6226screensfl
 */
	public static void clear(VarModel pv) {
		S6226ScreenVars screenVars = (S6226ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.updteflag.clear();
		screenVars.hasgnnum.clear();
		screenVars.hseqno.clear();
		screenVars.asgnsel.clear();
		screenVars.assigneeName.clear();
		screenVars.reasoncd.clear();
		screenVars.commfromDisp.clear();
		screenVars.commfrom.clear();
		screenVars.commtoDisp.clear();
		screenVars.commto.clear();
	}
}
