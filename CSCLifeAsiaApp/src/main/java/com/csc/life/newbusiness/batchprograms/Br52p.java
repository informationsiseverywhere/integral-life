/*
 * File: Br52p.java
 * Date: December 3, 2013 2:09:48 AM ICT
 * Author: CSC
 * 
 * Class transformed from BR52P.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;//ILIFE-8804
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
//ILIFE-8804 start
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
//ILIFE-8804 end
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.dataaccess.AgpydocTableDAM;
//ILIFE-8804 start
import com.csc.life.newbusiness.dataaccess.dao.AckdpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.dao.impl.AckdpfDAOImpl;
import com.csc.life.newbusiness.dataaccess.model.Ackdpf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
//ILIFE-8804 end
import com.csc.life.newbusiness.tablestructures.Tr52qrec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.procedures.SftlockUtil; //ILIFE-8804
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.SftlockRecBean;//ILIFE-8804
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;//ILIFE-8804
import com.csc.smart400framework.dataaccess.SmartFileCode;
//ILIFE-8804 start
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
//ILIFE-8804 end
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB) COMMIT(*NONE) <Do Not D lete>
*      *
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*           BR52P - Policy Acknowledgement Letters
*
*   This new SQLCBL program is created to extract policies with
*   outstanding acknowledgement slip. This job will generate the
*   acknowledgement reminder letter or deemed received letter base 
*   on the policy despatch rules defined in TR52Q. This process wi l
*   exclude contracts with delivery modes that have been set under
*   the excluded delivery modes in TR52Q.
*
*   Initialise
*     - Load the policy schedule details from TR52Q.
*     - Get valid contract status from T5679.
*     - Get company defaults details from TH605.
*
*   Read
*     - Declare HPADPF1 Cursor
*
*   Perform     Until End of File
*
*      Edit
*       - Skip policy that has already had acknowledgement date
*         or deemed received date
*       - Get the policy details by reading CHDRLNB.
*       - Get the policy dispatch details from 2600 section.
*       - Calculate the overdue days for the acknowledgement slip
*       - Get the current overdue type
*       - Softlock the policy
*
*      Update
*       - Generate the letter for the current overdue type
*       - Update policy acknowledgement details HPAD
*       - REWRT CHDRLNB record
* </pre>
*/
public class Br52p extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlhpadpf1rs = null;
	private java.sql.PreparedStatement sqlhpadpf1ps = null;
	private java.sql.Connection sqlhpadpf1conn = null;
	private String sqlhpadpf1 = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR52P");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFound = new FixedLengthStringData(1);
	private Validator found = new Validator(wsaaFound, "Y");
	private Validator notFound = new Validator(wsaaFound, "N");
	//private static final int wsaaTr52qSize = 100;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaTr52qSize = 1000;

		/* WSAA-TR52Q-ARRAY */
	private FixedLengthStringData[] wsaaTr52qRec = FLSInittedArray (1000, 57);
	private FixedLengthStringData[] wsaaTr52qCnttype = FLSDArrayPartOfArrayStructure(3, wsaaTr52qRec, 0);
	private FixedLengthStringData[] wsaaTr52qDetails = FLSDArrayPartOfArrayStructure(54, wsaaTr52qRec, 3);
	private FixedLengthStringData[][] wsaaTr52qDdocVal = FLSDArrayPartOfArrayStructure(2, 11, wsaaTr52qDetails, 0);
	private ZonedDecimalData[][] wsaaTr52qDaexpy = ZDArrayPartOfArrayStructure(3, 0, wsaaTr52qDdocVal, 0);
	private FixedLengthStringData[][] wsaaTr52qHlettype = FLSDArrayPartOfArrayStructure(8, wsaaTr52qDdocVal, 3);
	private FixedLengthStringData[][] wsaaTr52qDlvrmode = FLSDArrayPartOfArrayStructure(8, 4, wsaaTr52qDetails, 22);
	private ZonedDecimalData wsaaEffDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaExclude = new FixedLengthStringData(1).init("E");
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String th605 = "TH605";
	private static final String tr52q = "TR52Q";
		/* ERRORS */
	private static final String e101 = "E101";
	private static final String h791 = "H791";
	private static final String stnf = "STNF";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private int contot01 = 0;
	private int contot03 = 0;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler3, 5);

		/* SQL-HPADPF */
	private FixedLengthStringData sqlHpadrec = new FixedLengthStringData(33);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
	private AgpydocTableDAM agpydocIO = new AgpydocTableDAM();
//	private BscdTableDAM bscdIO = new BscdTableDAM();

	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private P6671par p6671par = new P6671par();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Th605rec th605rec = new Th605rec();
	private Tr52qrec tr52qrec = new Tr52qrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaVariableInner wsaaVariableInner = new WsaaVariableInner();
	//ILIFE-8804 start
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Itempf itempf = null;
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Chdrpf chdrpf = new Chdrpf();
	private Ptrnpf ptrnpf = new Ptrnpf();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private SftlockUtil sftlockUtil = new SftlockUtil();
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
	private Hpadpf hpadpf = new Hpadpf();
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).setUnsigned();
	private AckdpfDAO ackdpfDAO = new AckdpfDAOImpl();
	private FixedLengthStringData wsaaAckdFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaAckdFn, 0, FILLER).init("ACKD");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaAckdFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaAckdFn, 6).setUnsigned();
	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private int batchID =0;
    private int batchExtractSize;
    private Iterator<Ackdpf> iteratorList;
    private Ackdpf ackdpf; 
    private T5679rec t5679rec = new T5679rec();
    //ILIFE-8804 end
    private List<Chdrpf> chdrpfUpdateList = new ArrayList<>();
    private List<Ptrnpf> ptrnpfInsertList = new ArrayList<>();
    private List<Hpadpf> hpadpfUpdateList = new ArrayList<>();
    private Map<String,Chdrpf> chdrpfMap = null;    
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextTr52q1150, 
		exit1190, 
		endOfFile2080, 
		exit2090, 
		call3420, 
		exit3490
	}

	public Br52p() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		readChunkRecord(); //ILIFE-8804
	}

protected void initialise1010()
	{
	contot01 = 0;
	contot03 = 0;
		wsspEdterror.set(varcom.oK);
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)) {
			wsaaVariableInner.wsaaChdrnumFrm.set(LOVALUE);
		}
		else {
			wsaaVariableInner.wsaaChdrnumFrm.set(p6671par.chdrnum);
		}
		if (isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaVariableInner.wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaVariableInner.wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffDate.set(bsscIO.getEffectiveDate());
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
 		//ILIFE-8804 start
		wsaaToday.set(datcon1rec.intDate);
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
 		//ILIFE-8804 end
		wsaaVariableInner.wsaaAllSub.set(0);
		wsaaVariableInner.wsaaTr52qCnt.set(0);
		/* Load the policy schedule details from TR52Q into array*/
		readTr52q1100();
		/* Get valid contract status*/	
		readT56791300();
		/* Get company defaults details*/
		readTh6051400();
	}
 //ILIFE-8804 start
private void readChunkRecord() {
    	List<Ackdpf> pfList = this.ackdpfDAO.searchAckdpfRecord(wsaaAckdFn.toString(), wsaaThread.toString(),
				batchExtractSize, batchID);
        iteratorList = pfList.iterator();
 //ILIFE-8804 end
        
        if(!pfList.isEmpty())
        {
        	List<String> chdrnumList = new ArrayList<>();
            for(Ackdpf ackdpf: pfList)
            {
            	chdrnumList.add(ackdpf.getChdrnum());
            }
        	chdrpfMap = chdrpfDAO.getLatestChdrnoFromChdrnumChdrcoy(bsprIO.getCompany().toString() , chdrnumList);          
        }
        contot01 += pfList.size();
	}

protected void readTr52q1100()
	{
 //ILIFE-8804 start
		wsaaVariableInner.ix.set(1);
		List<Itempf> itempfList= itemDAO.getItemByPfxAndCoyAndTabl(smtpfxcpy.item.toString(), bsprIO.getCompany().toString(), tr52q) ;  
		for(Itempf item: itempfList) {
			if (isGT(wsaaVariableInner.ix, wsaaTr52qSize)) {
				syserrrec.statuz.set(h791);
				syserrrec.params.set(tr52q);
				fatalError600();
			}
			tr52qrec.tr52qRec.set(StringUtil.rawToString(itempfList.get(0).getGenarea()));
			wsaaTr52qCnttype[wsaaVariableInner.ix.toInt()].set(item.getItemitem().trim());
			if (isEQ(item.getItemitem().trim(), "***")) {
				wsaaVariableInner.wsaaAllSub.set(wsaaVariableInner.ix);
			}
			wsaaVariableInner.wsaaTr52qCnt.add(1);
			for (wsaaVariableInner.iw.set(1); !(isGT(wsaaVariableInner.iw, 2)); wsaaVariableInner.iw.add(1)){
				wsaaTr52qDaexpy[wsaaVariableInner.ix.toInt()][wsaaVariableInner.iw.toInt()].set(tr52qrec.daexpy[wsaaVariableInner.iw.toInt()]);
				wsaaTr52qHlettype[wsaaVariableInner.ix.toInt()][wsaaVariableInner.iw.toInt()].set(tr52qrec.letterType[wsaaVariableInner.iw.toInt()]);
			}
			wsaaVariableInner.iv.set(0);
			for (wsaaVariableInner.iw.set(2); !(isGT(wsaaVariableInner.iw, 9)); wsaaVariableInner.iw.add(1)){
				wsaaVariableInner.iv.add(1);
				wsaaTr52qDlvrmode[wsaaVariableInner.ix.toInt()][wsaaVariableInner.iv.toInt()].set(tr52qrec.dlvrmode[wsaaVariableInner.iw.toInt()]);
			}
			wsaaVariableInner.ix.add(1);
 //ILIFE-8804 end
		}
	}

protected void readT56791300()	
{	
	itempf = new Itempf();
	itempf.setItempfx(smtpfxcpy.item.toString());
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl(t5679);
	itempf.setItemitem(bprdIO.getAuthCode().toString());
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	if (itempf == null) {
		syserrrec.params.set(smtpfxcpy.item.toString().concat(bsprIO.getCompany().toString()).concat(t5679).concat(bprdIO.getAuthCode().toString()));
		fatalError600();
	}
	t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
}	



protected void readTh6051400()
	{
 //ILIFE-8804 start
		itempf = new Itempf();
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(bsprIO.getCompany().toString());
		itempf.setItemtabl(th605);
		itempf.setItemitem(bsprIO.getCompany().toString());
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf == null) {
			syserrrec.statuz.set(varcom.mrnf);
			syserrrec.params.set(smtpfxcpy.item.toString().concat(bsprIO.getCompany().toString()).concat(th605).concat(bsprIO.getCompany().toString()));
			fatalError600();
		}
		th605rec.th605Rec.set(StringUtil.rawToString(itempf.getGenarea()));
 //ILIFE-8804 end
	}

protected void readFile2000()
	{
 //ILIFE-8804 start
		if (!iteratorList.hasNext()) {
			batchID++;
			readChunkRecord();
			if (!iteratorList.hasNext()) {
				wsspEdterror.set(Varcom.endp);
				return;
			}
			
		}
		 this.ackdpf = iteratorList.next();
 //ILIFE-8804 end
	}


protected void edit2500()
	{
		wsspEdterror.set(varcom.oK);
		/* Skip the policy that has already had acknowledgement date or*/
		/* deemed received date*/
 //ILIFE-8804 start
		if (isNE(ackdpf.getPackdate(), varcom.vrcmMaxDate) 
		&& isNE(ackdpf.getPackdate(), ZERO)
		|| isNE(ackdpf.getDeemdate(), varcom.vrcmMaxDate) 
		&& isNE(ackdpf.getDeemdate(), ZERO)) {
			wsspEdterror.set(SPACES);
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			return ;
		}
		/* Get the policy details*/		
		chdrpf = chdrpfMap.get(ackdpf.getChdrnum());
				
		if(chdrpf==null) {
			syserrrec.statuz.set(varcom.mrnf);
			syserrrec.params.set(ackdpf.getChdrcoy().concat(ackdpf.getChdrnum()));
			fatalError600();
		}
		
		/* Check whether the contract risk status is valid*/	
		wsaaFound.set("N");	
		for (wsaaVariableInner.iw.set(1); !(isGT(wsaaVariableInner.iw, 12)	
		|| found.isTrue()); wsaaVariableInner.iw.add(1)){	
			if (isEQ(chdrpf.getStatcode(), t5679rec.cnRiskStat[wsaaVariableInner.iw.toInt()])) {	
				wsaaFound.set("Y");	
			}	
		}	
		if (notFound.isTrue()) {	
			wsspEdterror.set(SPACES); //MIBT-372 {Developer to code this line only} 	
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);	
			callContot001();	
			return ;	
		}	
		/* Check whether the contract prem status is valid*/	
		wsaaFound.set("N");	
		for (wsaaVariableInner.iw.set(1); !(isGT(wsaaVariableInner.iw, 12)	
		|| found.isTrue()); wsaaVariableInner.iw.add(1)){	
			if (isEQ(chdrpf.getPstcde(), t5679rec.cnPremStat[wsaaVariableInner.iw.toInt()])) {	
				wsaaFound.set("Y");	
			}	
		}	
		if (notFound.isTrue()) {	
			wsspEdterror.set(SPACES); //MIBT-372 {Developer to code this line only} 	
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();	
			return ;	
		}
		
		/* Get the policy dispatch details*/
		getTr52qItem2600();
		/* Check whether delivery mode is excluded or not*/
		wsaaFound.set("N");
		for (wsaaVariableInner.iw.set(1); !(isGT(wsaaVariableInner.iw, 8) 
		|| found.isTrue()); wsaaVariableInner.iw.add(1)) {
			if (isEQ(wsaaTr52qDlvrmode[wsaaVariableInner.wsaaSub.toInt()][wsaaVariableInner.iw.toInt()], ackdpf.getDlvrmode())) {
				wsaaFound.set("Y");
			}
		}
		if (found.isTrue()) {
			wsspEdterror.set(SPACES);
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			return;
		}
		/* Calculate the overdue days for the acknowledgement slip */
		datcon3rec.intDate1.set(ackdpf.getDespdate());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			syserrrec.statuz.set(datcon3rec.statuz);
			syserrrec.params.set(datcon3rec.datcon3Rec);
			fatalError600();
		}
		wsaaVariableInner.wsaaOverdueDays.set(datcon3rec.freqFactor);
		wsaaVariableInner.wsaaDaySub.set(ZERO);
		wsaaVariableInner.wsaaLetterType.set(SPACES);
		/* Get the current overdue type */
		for (wsaaVariableInner.iy.set(
		2); !(isEQ(wsaaVariableInner.iy, 0) 
		|| isGT(wsaaVariableInner.wsaaDaySub, ZERO)); wsaaVariableInner.iy.add(-1)) {
			if (isGT(wsaaTr52qDaexpy[wsaaVariableInner.wsaaSub.toInt()][wsaaVariableInner.iy.toInt()], ZERO)
					&& isLTE(wsaaTr52qDaexpy[wsaaVariableInner.wsaaSub.toInt()][wsaaVariableInner.iy.toInt()],wsaaVariableInner.wsaaOverdueDays)) {
				wsaaVariableInner.wsaaLetterType.set(wsaaTr52qHlettype[wsaaVariableInner.wsaaSub.toInt()][wsaaVariableInner.iy.toInt()]);
				wsaaVariableInner.wsaaDaySub.set(wsaaVariableInner.iy);
			}
		}
		/* If there is no letter type defined for the current overdue, */
		/* then skip this record */
		if (isEQ(wsaaVariableInner.wsaaLetterType, SPACES)) {
			wsspEdterror.set(SPACES);
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			return ;
		}
		softlockContract2700();
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			conlogrec.error.set(e101);
			conlogrec.params.set(sftlockrec.sftlockRec);
			callConlog003();
			wsspEdterror.set(SPACES);
			contotrec.totno.set(ct04);
			contotrec.totval.set(1);
			callContot001();
		}
	}

protected void getTr52qItem2600()
	{
		begin2610();
	}

protected void begin2610()
	{
		wsaaVariableInner.wsaaSub.set(0);
		wsaaVariableInner.wsaaCntSub.set(0);
		/* Search through the array for correct TR52Q item*/
		for (wsaaVariableInner.ix.set(1); !(isGT(wsaaVariableInner.ix, wsaaVariableInner.wsaaTr52qCnt)
		|| isGT(wsaaVariableInner.wsaaCntSub, ZERO)); wsaaVariableInner.ix.add(1)){
			if (isEQ(chdrpf.getCnttype(), wsaaTr52qCnttype[wsaaVariableInner.ix.toInt()])) {
				wsaaVariableInner.wsaaCntSub.set(wsaaVariableInner.ix);
				wsaaVariableInner.ix.set(500);
			}
		}
		/* If item not found and there is no generic item, then display*/
		/* error message*/
		if (isEQ(wsaaVariableInner.wsaaCntSub, 0)
		&& isEQ(wsaaVariableInner.wsaaAllSub, 0)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(tr52q); //ILIFE-8804
			stringVariable1.addExpression(chdrpf.getCnttype()); //ILIFE-8804
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(stnf);
			fatalError600();
		}
		/* Return the item TR52Q subscript into WSAA-SUB*/
		if (isGT(wsaaVariableInner.wsaaCntSub, ZERO)) {
			wsaaVariableInner.wsaaSub.set(wsaaVariableInner.wsaaCntSub);
		}
		else {
			wsaaVariableInner.wsaaSub.set(wsaaVariableInner.wsaaAllSub);
		}
	}

protected void softlockContract2700()
	{
 //ILIFE-8804 start
		sftlockRecBean.setFunction("LOCK");
		sftlockRecBean.setCompany(bsprIO.getCompany().toString());
		sftlockRecBean.setEnttyp(chdrpf.getChdrpfx());
		sftlockRecBean.setEntity(ackdpf.getChdrnum());
		sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
		sftlockRecBean.setUser("99999");
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				fatalError600();
 //ILIFE-8804 end
		}
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
 //ILIFE-8804 start
		/* Generate the letter for the current overdue type*/
		letrqstrec.requestCompany.set(ackdpf.getChdrcoy());
		letrqstrec.letterType.set(wsaaVariableInner.wsaaLetterType);
		letrqstrec.clntcoy.set(chdrpf.getCowncoy());
		letrqstrec.clntnum.set(chdrpf.getCownnum());
		letrqstrec.letterRequestDate.set(bsscIO.getEffectiveDate());
		letrqstrec.rdocpfx.set(chdrpf.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrpf.getChdrcoy());
		letrqstrec.rdocnum.set(chdrpf.getChdrnum());
		compute(letrqstrec.tranno, 0).set(add(chdrpf.getTranno(), 1));
		letrqstrec.chdrcoy.set(chdrpf.getChdrcoy());
		letrqstrec.chdrnum.set(chdrpf.getChdrnum());
		letrqstrec.despnum.set(chdrpf.getDespnum());
		letrqstrec.branch.set(chdrpf.getCntbranch());
		letrqstrec.otherKeys.set(SPACES);
		letrqstrec.trcde.set(bprdIO.getAuthCode());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			syserrrec.params.set(letrqstrec.params);
			syserrrec.statuz.set(letrqstrec.statuz);
			fatalError600();
		}
		/* Update policy acknowledgement details*/
		updateHpad3200();		
		setPrecision(chdrpf.getTranno(), 0);
		chdrpf.setTranno(add(chdrpf.getTranno(),1).toInt());
		chdrpfUpdateList.add(chdrpf);
		
 //ILIFE-8804 end
		/* Create policy trransaction history*/
		createPtrn3500();
		contot03++;
		/* Unlock the policy*/
		unlockContract3600();
	}

protected void updateHpad3200()
	{
		begin3210();
	}

protected void begin3210()
	{
 //ILIFE-8804 start
		hpadpf= new Hpadpf();
		hpadpf.setChdrnum(ackdpf.getChdrnum());
		hpadpf.setChdrcoy(ackdpf.getChdrcoy());
		hpadpf.setDeemdate(ackdpf.getDeemdate());
		hpadpf.setUniqueNumber(ackdpf.getHpaduqn());
		/* If letter is generated for 'deemed received'(WSAA-DAY-SUB=2)*/
		if (isEQ(wsaaVariableInner.wsaaDaySub, 2)) {
			hpadpf.setNxtdte(varcom.vrcmMaxDate.toInt());
			hpadpf.setDeemdate(bsscIO.getEffectiveDate().toInt());
			checkComm3300();
		}
		else {
			hpadpf.setRemdte(bsscIO.getEffectiveDate().toInt());
			/* Get the next overdue days*/
			wsaaVariableInner.wsaaDaySub.add(1);
			/* Calculate the next activity date*/
			datcon2rec.frequency.set("DY");
			datcon2rec.intDate1.set(ackdpf.getDespdate());
			datcon2rec.freqFactor.set(wsaaTr52qDaexpy[wsaaVariableInner.wsaaSub.toInt()][wsaaVariableInner.wsaaDaySub.toInt()]);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			hpadpf.setNxtdte(datcon2rec.intDate2.toInt());
		}
		hpadpfUpdateList.add(hpadpf);		
 //ILIFE-8804 end 
	}

protected void checkComm3300()
	{
		/*BEGIN*/
		/* Check whether need to update agency postings records of*/
		/* the policy or not*/
		if (isNE(th605rec.comind, "Y")) {
			return ;
		}
		updateAgpy3400();
		/*EXIT*/
	}

protected void updateAgpy3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin3410();
				case call3420: 
					call3420();
					next3480();
				case exit3490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin3410()
	{
		agpydocIO.setParams(SPACES);
		agpydocIO.setBatccoy(hpadpf.getChdrcoy()); //ILIFE-8804
		agpydocIO.setRdocnum(hpadpf.getChdrnum()); //ILIFE-8804
		agpydocIO.setTranno(0);
		agpydocIO.setJrnseq(0);
		agpydocIO.setFormat(formatsInner.agpydocrec);
		agpydocIO.setFunction(varcom.begn);
	}

protected void call3420()
	{
		SmartFileCode.execute(appVars, agpydocIO);
		if (isNE(agpydocIO.getStatuz(), varcom.oK)
		&& isNE(agpydocIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(agpydocIO.getStatuz());
			syserrrec.params.set(agpydocIO.getParams());
			fatalError600();
		}
		if (isEQ(agpydocIO.getStatuz(), varcom.endp)
		|| isNE(agpydocIO.getBatccoy(), hpadpf.getChdrcoy())
		|| isNE(agpydocIO.getRdocnum(), hpadpf.getChdrnum())) {
			agpydocIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3490);
		}
		if (isEQ(agpydocIO.getEffdate(), 0)
		|| isEQ(agpydocIO.getEffdate(), varcom.vrcmMaxDate)) {
			agpyagtIO.setParams(SPACES);
			agpyagtIO.setRrn(agpydocIO.getRrn());
			agpyagtIO.setFormat(formatsInner.agpyagtrec);
			agpyagtIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				fatalError600();
			}
			agpyagtIO.setEffdate(hpadpf.getDeemdate()); //ILIFE-8804
			agpyagtIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void next3480()
	{
		agpydocIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call3420);
	}

protected void createPtrn3500()
	{
		begin3510();
	}

protected void begin3510()
	{
 //ILIFE-8804 start
		ptrnpf = new Ptrnpf();
		ptrnpf.setBatcpfx("BA");
		ptrnpf.setBatccoy(batcdorrec.company.toString());
		ptrnpf.setBatcbrn(batcdorrec.branch.toString());
		ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
		ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
		ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
		ptrnpf.setBatcbatch(batcdorrec.batch.toString());
		ptrnpf.setChdrpfx(chdrpf.getChdrpfx());
		ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrpf.getChdrnum());
		ptrnpf.setRecode(SPACES.stringValue());
		ptrnpf.setTranno(chdrpf.getTranno());
		/* MOVE WSAA-TODAY             TO PTRN-PTRNEFF.                 */
		ptrnpf.setPtrneff(bsscIO.getEffectiveDate().toInt());
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setUserT(subString(bsscIO.getDatimeInit(), 21, 6).toInt());
		/* MOVE WSAA-TODAY             TO PTRN-DATESUB.                 */
		ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
		ptrnpf.setCrtuser(bsscIO.getUserName().toInternal());
		ptrnpf.setValidflag("1");
		ptrnpf.setTermid(varcom.vrcmTermid.toString());
		ptrnpf.setTrdt(Integer.valueOf(getCobolDate()));
		ptrnpf.setTrtm(Integer.valueOf(getCobolTime()));
		ptrnpfInsertList.add(ptrnpf);		
 //ILIFE-8804 end
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT1*/
		if(chdrpfUpdateList != null && !chdrpfUpdateList.isEmpty())
		{
			chdrpfDAO.updateChdrTrannoByUniqueNo(chdrpfUpdateList);
			chdrpfUpdateList.clear();
		}
		if(hpadpfUpdateList != null && !hpadpfUpdateList.isEmpty())
		{
			hpadpfDAO.updateRecordsByUniqueNumber(hpadpfUpdateList);
			hpadpfUpdateList.clear();
		}
		if(ptrnpfInsertList != null && !ptrnpfInsertList.isEmpty())
		{
			ptrnpfDAO.insertPtrnPF(ptrnpfInsertList);
			ptrnpfInsertList.clear();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(contot01);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(contot03);
		callContot001();
	
	}

protected void unlockContract3600()
	{
 //ILIFE-8804 start
		sftlockRecBean.setFunction("UNLK");
		sftlockRecBean.setCompany(bsprIO.getCompany().toString());
		sftlockRecBean.setEnttyp(chdrpf.getChdrpfx());
		sftlockRecBean.setEntity(ackdpf.getChdrnum());
		sftlockRecBean.setTransaction(bprdIO.getAuthCode().toString());
		sftlockRecBean.setUser("999999");
		sftlockRecBean.setStatuz(SPACES.stringValue());
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				fatalError600();
 //ILIFE-8804 end
		}
	}


protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT1*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-VARIABLE--INNER
 */
private static final class WsaaVariableInner { 
		/* WSAA-VARIABLE */
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0);
	private ZonedDecimalData iy = new ZonedDecimalData(3, 0);
	private ZonedDecimalData iw = new ZonedDecimalData(3, 0);
	private ZonedDecimalData iv = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaTr52qCnt = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaCntSub = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaAllSub = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaDaySub = new ZonedDecimalData(2, 0);
	private PackedDecimalData wsaaOverdueDays = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaLetterType = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumFrm = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData agpyagtrec = new FixedLengthStringData(10).init("AGPYAGTREC");
	private FixedLengthStringData agpydocrec = new FixedLengthStringData(10).init("AGPYDOCREC");
}
}