/*
 * File: T5608pt.java
 * Date: 30 August 2009 2:23:57
 * Author: Quipoz Limited
 * 
 * Class transformed from T5608PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5608.
*
*
*****************************************************************
* </pre>
*/
public class T5608pt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5608rec t5608rec = new T5608rec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5608pt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5608rec.t5608Rec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		generalCopyLinesInner.fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		generalCopyLinesInner.fieldNo006.set(datcon1rec.extDate);
		generalCopyLinesInner.fieldNo007.set(t5608rec.sumInsMin);
		generalCopyLinesInner.fieldNo008.set(t5608rec.sumInsMax);
		generalCopyLinesInner.fieldNo009.set(t5608rec.ageIssageFrm01);
		generalCopyLinesInner.fieldNo010.set(t5608rec.ageIssageTo01);
		generalCopyLinesInner.fieldNo011.set(t5608rec.ageIssageFrm02);
		generalCopyLinesInner.fieldNo012.set(t5608rec.ageIssageTo02);
		generalCopyLinesInner.fieldNo013.set(t5608rec.ageIssageFrm03);
		generalCopyLinesInner.fieldNo014.set(t5608rec.ageIssageTo03);
		generalCopyLinesInner.fieldNo015.set(t5608rec.ageIssageFrm04);
		generalCopyLinesInner.fieldNo016.set(t5608rec.ageIssageTo04);
		generalCopyLinesInner.fieldNo017.set(t5608rec.ageIssageFrm05);
		generalCopyLinesInner.fieldNo018.set(t5608rec.ageIssageTo05);
		generalCopyLinesInner.fieldNo019.set(t5608rec.ageIssageFrm06);
		generalCopyLinesInner.fieldNo020.set(t5608rec.ageIssageTo06);
		generalCopyLinesInner.fieldNo021.set(t5608rec.ageIssageFrm07);
		generalCopyLinesInner.fieldNo022.set(t5608rec.ageIssageTo07);
		generalCopyLinesInner.fieldNo023.set(t5608rec.ageIssageFrm08);
		generalCopyLinesInner.fieldNo024.set(t5608rec.ageIssageTo08);
		generalCopyLinesInner.fieldNo025.set(t5608rec.riskCessageFrom01);
		generalCopyLinesInner.fieldNo026.set(t5608rec.riskCessageTo01);
		generalCopyLinesInner.fieldNo027.set(t5608rec.riskCessageFrom02);
		generalCopyLinesInner.fieldNo028.set(t5608rec.riskCessageTo02);
		generalCopyLinesInner.fieldNo029.set(t5608rec.riskCessageFrom03);
		generalCopyLinesInner.fieldNo030.set(t5608rec.riskCessageTo03);
		generalCopyLinesInner.fieldNo031.set(t5608rec.riskCessageFrom04);
		generalCopyLinesInner.fieldNo032.set(t5608rec.riskCessageTo04);
		generalCopyLinesInner.fieldNo033.set(t5608rec.riskCessageFrom05);
		generalCopyLinesInner.fieldNo034.set(t5608rec.riskCessageTo05);
		generalCopyLinesInner.fieldNo035.set(t5608rec.riskCessageFrom06);
		generalCopyLinesInner.fieldNo036.set(t5608rec.riskCessageTo06);
		generalCopyLinesInner.fieldNo037.set(t5608rec.riskCessageFrom07);
		generalCopyLinesInner.fieldNo038.set(t5608rec.riskCessageTo07);
		generalCopyLinesInner.fieldNo039.set(t5608rec.riskCessageFrom08);
		generalCopyLinesInner.fieldNo040.set(t5608rec.riskCessageTo08);
		generalCopyLinesInner.fieldNo041.set(t5608rec.premCessageFrom01);
		generalCopyLinesInner.fieldNo042.set(t5608rec.premCessageTo01);
		generalCopyLinesInner.fieldNo043.set(t5608rec.premCessageFrom02);
		generalCopyLinesInner.fieldNo044.set(t5608rec.premCessageTo02);
		generalCopyLinesInner.fieldNo045.set(t5608rec.premCessageFrom03);
		generalCopyLinesInner.fieldNo046.set(t5608rec.premCessageTo03);
		generalCopyLinesInner.fieldNo047.set(t5608rec.premCessageFrom04);
		generalCopyLinesInner.fieldNo048.set(t5608rec.premCessageTo04);
		generalCopyLinesInner.fieldNo049.set(t5608rec.premCessageFrom05);
		generalCopyLinesInner.fieldNo050.set(t5608rec.premCessageTo05);
		generalCopyLinesInner.fieldNo051.set(t5608rec.premCessageFrom06);
		generalCopyLinesInner.fieldNo052.set(t5608rec.premCessageTo06);
		generalCopyLinesInner.fieldNo053.set(t5608rec.premCessageFrom07);
		generalCopyLinesInner.fieldNo054.set(t5608rec.premCessageTo07);
		generalCopyLinesInner.fieldNo055.set(t5608rec.premCessageFrom08);
		generalCopyLinesInner.fieldNo056.set(t5608rec.premCessageTo08);
		generalCopyLinesInner.fieldNo057.set(t5608rec.eaage);
		generalCopyLinesInner.fieldNo058.set(t5608rec.termIssageFrm01);
		generalCopyLinesInner.fieldNo059.set(t5608rec.termIssageTo01);
		generalCopyLinesInner.fieldNo060.set(t5608rec.termIssageFrm02);
		generalCopyLinesInner.fieldNo061.set(t5608rec.termIssageTo02);
		generalCopyLinesInner.fieldNo062.set(t5608rec.termIssageFrm03);
		generalCopyLinesInner.fieldNo063.set(t5608rec.termIssageTo03);
		generalCopyLinesInner.fieldNo064.set(t5608rec.termIssageFrm04);
		generalCopyLinesInner.fieldNo065.set(t5608rec.termIssageTo04);
		generalCopyLinesInner.fieldNo066.set(t5608rec.termIssageFrm05);
		generalCopyLinesInner.fieldNo067.set(t5608rec.termIssageTo05);
		generalCopyLinesInner.fieldNo068.set(t5608rec.termIssageFrm06);
		generalCopyLinesInner.fieldNo069.set(t5608rec.termIssageTo06);
		generalCopyLinesInner.fieldNo070.set(t5608rec.termIssageFrm07);
		generalCopyLinesInner.fieldNo071.set(t5608rec.termIssageTo07);
		generalCopyLinesInner.fieldNo072.set(t5608rec.termIssageFrm08);
		generalCopyLinesInner.fieldNo073.set(t5608rec.termIssageTo08);
		generalCopyLinesInner.fieldNo074.set(t5608rec.riskCesstermFrom01);
		generalCopyLinesInner.fieldNo075.set(t5608rec.riskCesstermTo01);
		generalCopyLinesInner.fieldNo076.set(t5608rec.riskCesstermFrom02);
		generalCopyLinesInner.fieldNo077.set(t5608rec.riskCesstermTo02);
		generalCopyLinesInner.fieldNo078.set(t5608rec.riskCesstermFrom03);
		generalCopyLinesInner.fieldNo079.set(t5608rec.riskCesstermTo03);
		generalCopyLinesInner.fieldNo080.set(t5608rec.riskCesstermFrom04);
		generalCopyLinesInner.fieldNo081.set(t5608rec.riskCesstermTo04);
		generalCopyLinesInner.fieldNo082.set(t5608rec.riskCesstermFrom05);
		generalCopyLinesInner.fieldNo083.set(t5608rec.riskCesstermTo05);
		generalCopyLinesInner.fieldNo084.set(t5608rec.riskCesstermFrom06);
		generalCopyLinesInner.fieldNo085.set(t5608rec.riskCesstermTo06);
		generalCopyLinesInner.fieldNo086.set(t5608rec.riskCesstermFrom07);
		generalCopyLinesInner.fieldNo087.set(t5608rec.riskCesstermTo07);
		generalCopyLinesInner.fieldNo088.set(t5608rec.riskCesstermFrom08);
		generalCopyLinesInner.fieldNo089.set(t5608rec.riskCesstermTo08);
		generalCopyLinesInner.fieldNo090.set(t5608rec.premCesstermFrom01);
		generalCopyLinesInner.fieldNo091.set(t5608rec.premCesstermTo01);
		generalCopyLinesInner.fieldNo092.set(t5608rec.premCesstermFrom02);
		generalCopyLinesInner.fieldNo093.set(t5608rec.premCesstermTo02);
		generalCopyLinesInner.fieldNo094.set(t5608rec.premCesstermFrom03);
		generalCopyLinesInner.fieldNo095.set(t5608rec.premCesstermTo03);
		generalCopyLinesInner.fieldNo096.set(t5608rec.premCesstermFrom04);
		generalCopyLinesInner.fieldNo097.set(t5608rec.premCesstermTo04);
		generalCopyLinesInner.fieldNo098.set(t5608rec.premCesstermFrom05);
		generalCopyLinesInner.fieldNo099.set(t5608rec.premCesstermTo05);
		generalCopyLinesInner.fieldNo100.set(t5608rec.premCesstermFrom06);
		generalCopyLinesInner.fieldNo101.set(t5608rec.premCesstermTo06);
		generalCopyLinesInner.fieldNo102.set(t5608rec.premCesstermFrom07);
		generalCopyLinesInner.fieldNo103.set(t5608rec.premCesstermTo07);
		generalCopyLinesInner.fieldNo104.set(t5608rec.premCesstermFrom08);
		generalCopyLinesInner.fieldNo105.set(t5608rec.premCesstermTo08);
		generalCopyLinesInner.fieldNo107.set(t5608rec.mortcls01);
		generalCopyLinesInner.fieldNo108.set(t5608rec.mortcls02);
		generalCopyLinesInner.fieldNo109.set(t5608rec.mortcls03);
		generalCopyLinesInner.fieldNo110.set(t5608rec.mortcls04);
		generalCopyLinesInner.fieldNo111.set(t5608rec.mortcls05);
		generalCopyLinesInner.fieldNo112.set(t5608rec.mortcls06);
		generalCopyLinesInner.fieldNo113.set(t5608rec.liencd01);
		generalCopyLinesInner.fieldNo114.set(t5608rec.liencd02);
		generalCopyLinesInner.fieldNo115.set(t5608rec.liencd03);
		generalCopyLinesInner.fieldNo116.set(t5608rec.liencd04);
		generalCopyLinesInner.fieldNo117.set(t5608rec.liencd05);
		generalCopyLinesInner.fieldNo118.set(t5608rec.liencd06);
		generalCopyLinesInner.fieldNo106.set(t5608rec.specind);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Term Based Edit Rules                    S5608");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 11, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 21);
	private FixedLengthStringData filler5 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 26, FILLER).init("   Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(46);
	private FixedLengthStringData filler7 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 19);
	private FixedLengthStringData filler8 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine003, 29, FILLER).init("  To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 36);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(59);
	private FixedLengthStringData filler9 = new FixedLengthStringData(19).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Sum insured min:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine004, 19).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine004, 34, FILLER).init("    Max:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine004, 44).setPattern("ZZZZZZZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(79);
	private FixedLengthStringData filler11 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler12 = new FixedLengthStringData(61).isAPartOf(wsaaPrtLine005, 18, FILLER).init("F---T   F---TF---T   F---T   F---T   F---T   F---T   F---T");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(80);
	private FixedLengthStringData filler13 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Issue Age:");
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 17).setPattern("ZZZ");
	private FixedLengthStringData filler14 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 21).setPattern("ZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 25).setPattern("ZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 29).setPattern("ZZZ");
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 33).setPattern("ZZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 37).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 41).setPattern("ZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 45).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 49).setPattern("ZZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 53).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 57).setPattern("ZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 61).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 65).setPattern("ZZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 69).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 73).setPattern("ZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine006, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(80);
	private FixedLengthStringData filler29 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine007, 0, FILLER).init(" Cessation Age:");
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 17).setPattern("ZZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 21).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 25).setPattern("ZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 29).setPattern("ZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 33).setPattern("ZZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 37).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 41).setPattern("ZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 45).setPattern("ZZZ");
	private FixedLengthStringData filler37 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 49).setPattern("ZZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 53).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 57).setPattern("ZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 61).setPattern("ZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 65).setPattern("ZZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 69).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 73).setPattern("ZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(80);
	private FixedLengthStringData filler45 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" Prem Cess Age:");
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 17).setPattern("ZZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 21).setPattern("ZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 25).setPattern("ZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 29).setPattern("ZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 33).setPattern("ZZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 37).setPattern("ZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 41).setPattern("ZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 45).setPattern("ZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 49).setPattern("ZZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 53).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 57).setPattern("ZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 61).setPattern("ZZZ");
	private FixedLengthStringData filler57 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 65).setPattern("ZZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 69).setPattern("ZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 73).setPattern("ZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(44);
	private FixedLengthStringData filler61 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine009, 0, FILLER).init(" Cess date, Anniversary or Exact:");
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 35);
	private FixedLengthStringData filler62 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 36, FILLER).init("   (A/E)");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(80);
	private FixedLengthStringData filler63 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" Issue Age:");
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 17).setPattern("ZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 21).setPattern("ZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 25).setPattern("ZZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 29).setPattern("ZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 33).setPattern("ZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 37).setPattern("ZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 41).setPattern("ZZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 45).setPattern("ZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 49).setPattern("ZZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 53).setPattern("ZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 57).setPattern("ZZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 61).setPattern("ZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 65).setPattern("ZZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 69).setPattern("ZZZ");
	private FixedLengthStringData filler77 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 73).setPattern("ZZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(80);
	private FixedLengthStringData filler79 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" Coverage Term:");
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 17).setPattern("ZZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 21).setPattern("ZZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 25).setPattern("ZZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 29).setPattern("ZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 33).setPattern("ZZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 37).setPattern("ZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 41).setPattern("ZZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 45).setPattern("ZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 49).setPattern("ZZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 53).setPattern("ZZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 57).setPattern("ZZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 61).setPattern("ZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 65).setPattern("ZZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 69).setPattern("ZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 73).setPattern("ZZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(80);
	private FixedLengthStringData filler95 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" Prem Cess Term:");
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 17).setPattern("ZZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 21).setPattern("ZZZ");
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 24, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 25).setPattern("ZZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 28, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 29).setPattern("ZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 33).setPattern("ZZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 37).setPattern("ZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 40, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 41).setPattern("ZZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 45).setPattern("ZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 48, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 49).setPattern("ZZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 52, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 53).setPattern("ZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 56, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 57).setPattern("ZZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 61).setPattern("ZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 65).setPattern("ZZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 68, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 69).setPattern("ZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 72, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 73).setPattern("ZZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 76, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 77).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(18);
	private FixedLengthStringData filler111 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" Special Terms:");
	private FixedLengthStringData fieldNo106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 17);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(47);
	private FixedLengthStringData filler112 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" Mortality Class:");
	private FixedLengthStringData fieldNo107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 21);
	private FixedLengthStringData filler113 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 22, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 26);
	private FixedLengthStringData filler114 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 31);
	private FixedLengthStringData filler115 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 36);
	private FixedLengthStringData filler116 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 41);
	private FixedLengthStringData filler117 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 42, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo112 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 46);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(48);
	private FixedLengthStringData filler118 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" Lien Codes:");
	private FixedLengthStringData fieldNo113 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 21);
	private FixedLengthStringData filler119 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 23, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo114 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 26);
	private FixedLengthStringData filler120 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 28, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo115 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 31);
	private FixedLengthStringData filler121 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 33, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo116 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 36);
	private FixedLengthStringData filler122 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 38, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo117 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 41);
	private FixedLengthStringData filler123 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo118 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 46);
}
}
