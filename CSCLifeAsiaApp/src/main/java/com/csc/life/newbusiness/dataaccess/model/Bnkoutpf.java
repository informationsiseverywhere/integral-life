
package com.csc.life.newbusiness.dataaccess.model;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BNKOUTPF", schema = "VM1DTA")
public class Bnkoutpf implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "UNIQUE_NUMBER")
	private long uniqueNumber;
	private String chdrnum;
	private String agntnum;
	private String bnkout;
	private String bnkoutname;
	private String bnksm;
	private String bnksmname;
	private String bnktel;
	private String bnktelname;
	private String usrprf;
	private String jobnm;
	private Timestamp datime;

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	
	public String getAgntnum() {
		return agntnum;
	}

	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}

	public long getUniqueNumber() {
		return uniqueNumber;
	}

	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}

	public String getBnkout() {
		return bnkout;
	}

	public void setBnkout(String bnkout) {
		this.bnkout = bnkout;
	}

	public String getBnkoutname() {
		return bnkoutname;
	}

	public void setBnkoutname(String bnkoutname) {
		this.bnkoutname = bnkoutname;
	}

	public String getBnksm() {
		return bnksm;
	}

	public void setBnksm(String bnksm) {
		this.bnksm = bnksm;
	}

	public String getBnksmname() {
		return bnksmname;
	}

	public void setBnksmname(String bnksmname) {
		this.bnksmname = bnksmname;
	}

	public String getBnktel() {
		return bnktel;
	}

	public void setBnktel(String bnktel) {
		this.bnktel = bnktel;
	}

	public String getBnktelname() {
		return bnktelname;
	}

	public void setBnktelname(String bnktelname) {
		this.bnktelname = bnktelname;
	}

	public String getUsrprf() {
		return usrprf;
	}

	public void setUsrprf(String usrprf) {
		this.usrprf = usrprf;
	}

	public String getJobnm() {
		return jobnm;
	}

	public void setJobnm(String jobnm) {
		this.jobnm = jobnm;
	}

	public Timestamp getDatime() {
		return datime;
	}

	public void setDatime(Timestamp datime) {
		this.datime = datime;
	}
	

}
