/*
 * File: P5105.java
 * Date: 30 August 2009 0:07:42
 * Author: Quipoz Limited
 * 
 * Class transformed from P5105.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainf;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* P5105 - Fast Track Initial Switching Program.
* ---------------------------------------------
* This program is invoked from New Contract proposal for
* Fast Track Issue. This program reads T5671 with 5105 + product
* type + * to get the programs to call.
*
*****************************************************************
* </pre>
*/
public class P5105 extends Mainf {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5105");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsccSecProgram = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsccSecProg = FLSArrayPartOfStructure(8, 5, wsccSecProgram, 0);

	private FixedLengthStringData wsaaProgname = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaPrognum = new FixedLengthStringData(4).isAPartOf(wsaaProgname, 1);

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Prognum = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0);
	private FixedLengthStringData wsaaT5671Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5671Key, 4);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaT5671Key, 7, FILLER).init("*");
		/* FORMATS */
	private String chdrlnbrec = "CHDRLNBREC";
		/* TABLES */
	private String t5671 = "T5671";
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5671rec t5671rec = new T5671rec();
	private Batckey wsaaBatckey = new Batckey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextProgram4080
	}

	public P5105() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1005();
		readTableT56711015();
	}

protected void initialise1005()
	{
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaProgname.set(wsaaProg);
		/*RETRIEVE-CHDRLNB*/
		chdrlnbIO.setFunction(varcom.retrv);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void readTableT56711015()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		wsaaT5671Prognum.set(wsaaPrognum);
		wsaaT5671Cnttype.set(chdrlnbIO.getCnttype());
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		else {
			t5671rec.t5671Rec.set(itemIO.getGenarea());
		}
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*EXIT*/
	}

protected void whereNext4000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					nextProgram4010();
				}
				case nextProgram4080: {
					nextProgram4080();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.nextProgram4080);
		}
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],SPACES)) {
			wsaaX.set(wsspcomn.programPtr);
		}
		wsaaY.set(1);
		for (int loopVar1 = 0; !(loopVar1 == 8); loopVar1 += 1){
			saveProgStack4100();
		}
		/*LOAD-PROGRAM-STACK*/
		wsaaX.set(wsspcomn.programPtr);
		wsaaY.set(ZERO);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			loadProgramStack4200();
		}
	}

protected void nextProgram4080()
	{
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void saveProgStack4100()
	{
		/*SAVE*/
		wsaaX.add(1);
		wsaaY.add(1);
		/*EXIT*/
	}

protected void loadProgramStack4200()
	{
		/*LOAD*/
		wsaaX.add(1);
		wsaaY.add(1);
		wsspcomn.secProg[wsaaX.toInt()].set(t5671rec.pgm[wsaaY.toInt()]);
		/*EXIT*/
	}
}
