/*
 * File: Tradiss.java
 * Date: 30 August 2009 2:44:53
 * Author: Quipoz Limited
 * 
 * Class transformed from TRADISS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.cashdividends.dataaccess.CovttrdTableDAM;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.tablestructures.Th502rec;
import com.csc.life.newbusiness.dataaccess.CovrtrbTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*   TRADITIONAL LIFE NEW BUSINESS BONUS INFORMATION UPDATE
*
*   This subroutine is used by the New Business transaction to
*   update Bonus Information fields on the coverage file for
*   Traditional type components.
*
*   It sets fields to indicate :
*
*   - whether a contract attracts reversionary bonuses or not.
*
*   - whether the bonus is to be added at the Policy Anniversary
*     date or at the Company Bonus Declaration date.
*
*   - the Anniversary Processing date.
*
*   - the Bonus Declaration Date.
*
*   The subroutine is processed through the switching table T5671
*   similar to UNLISS in Unit Linked component cases.
*
*   The coverage file is accessed through the COVRTRB logical.
*
*   The subroutine is called from within the AT module for each
*   Traditional Component found on a proposal being issued.
*
*   Processing (Linkage ISUALLREC Copybook).
*   ----------
*
*   Perform READH-COVRTRB.
*   Perform Update-fields.
*   Perform REWRT-COVRTRB.
*
*   - Read T6640 using component as the key to get Bonus
*     Indicator and Bonus Method.
*
*   - Check for valid Bonus Indicator.
*
*   - If a valid Bonus Indicator other than spaces was entered,
*     check that a Bonus Method was entered also.
*
*   - Set up the Bonus Indicator on the COVR with 'P' or 'C'
*     accordingly.
*
*   - Trigger dates are set up :
*
*     (i) Anniversary Processing Date
*     (ii) Unit Statement Date (Bonus Declaration Date)
*
*   The Anniversary processing date is initially set to all 9's
*   and then the following sections are executed:-
*
*   Anniversary Processing Date - this is always performed.
*      Set the following fields for a call to DATCON2:
*                DTC2-INT-DATE-1   -  CCDATE
*                DTC2-FREQUENCY    -  '01'
*                DTC2-FREQ-FACTOR  -  1
*
*      Call DATCON2 and use the returned date as the anniversary
*      processing date.
*
*   Unit-Statement-Date - this is always performed.
*
*   The COVRTRB-UNIT-STATEMENT-DATE (CBUNST) is used as the date
*   which stores the last bonus declaration date. This field was
*   not previously used on the COVR, rather the CHDR stored  the
*   Unit Statement Date. Though the field name refers to the Unit
*   Link side of Life/400, utilising this already existing and so
*   far unused field avoided having to recompile all the logicals
*   that use CBUNST.
*
*   Storing this date on the COVR enables 'Catch Ups' to be done
*   by determining when the previous bonus was paid. The CBUNST
*   field is initially written in TRADISS only and is set to be
*   the Contract Commencement date
*
*   Initialising the field in this manner makes it possible to
*   establish which components have never had a bonus paid.
*
*  Tables Accessed and their keys :-
*  ------------------------------
*
*  T6640 - Key = Coverage/Rider Code - CRTABLE (From COVRTRB)
*
*                  LIFE ASIA V2.0 ENHANCEMENTS.
*                  ----------------------------
*
* Inclusion of cash dividend Processing.                              *
*****************************************************************
* </pre>
*/
public class Tradiss extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaSubr = "TRADISS";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* ERRORS */
	private String g586 = "G586";
	private String g588 = "G588";
	private String hl49 = "HL49";
	private String covrtrbrec = "COVRTRBREC";
	private String covttrdrec = "COVTTRDREC";
	private String hcsdrec = "HCSDREC";
	private String itemrec = "ITEMREC";
	private String t6640 = "T6640";
	private String th502 = "TH502";
		/*COVR layout for trad. reversionary bonus*/
	private CovrtrbTableDAM covrtrbIO = new CovrtrbTableDAM();
		/*COVT - Traditional Business*/
	private CovttrdTableDAM covttrdIO = new CovttrdTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Cash Dividend Details Logical*/
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private Isuallrec isuallrec = new Isuallrec();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T6640rec t6640rec = new T6640rec();
	private Th502rec th502rec = new Th502rec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit4690, 
		seExit9090, 
		dbExit9190
	}

	public Tradiss() {
		super();
	}

public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline1000()
	{
		/*START*/
		initialise2000();
		readCovrtrbRecord3000();
		updateFields4000();
		rewriteCovrtrbRecord5000();
		exitProgram();
	}

protected void initialise2000()
	{
		/*START*/
		syserrrec.subrname.set(wsaaSubr);
		/*EXIT*/
	}

protected void readCovrtrbRecord3000()
	{
		start3000();
	}

protected void start3000()
	{
		covrtrbIO.setChdrcoy(isuallrec.company);
		covrtrbIO.setChdrnum(isuallrec.chdrnum);
		covrtrbIO.setLife(isuallrec.life);
		covrtrbIO.setCoverage(isuallrec.coverage);
		covrtrbIO.setRider(isuallrec.rider);
		covrtrbIO.setPlanSuffix(isuallrec.planSuffix);
		covrtrbIO.setFunction(varcom.readh);
		covrtrbIO.setFormat(covrtrbrec);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			dbError9100();
		}
	}

protected void updateFields4000()
	{
		/*START*/
		readT66404100();
		setBonusIndicator4200();
		annivProcessDate4300();
		bonusDeclareDate4400();
		hcsdDetails4600();
		/*EXIT*/
	}

protected void readT66404100()
	{
		start4100();
	}

protected void start4100()
	{
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covrtrbIO.getCrtable());
		itdmIO.setItmfrm(covrtrbIO.getCrrcd());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError9100();
		}
		if (isNE(itdmIO.getItemcoy(),isuallrec.company)
		|| isNE(itdmIO.getItemtabl(),t6640)
		|| isNE(itdmIO.getItemitem(),covrtrbIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covrtrbIO.getCrtable());
			syserrrec.statuz.set(g588);
			dbError9100();
		}
		else {
			t6640rec.t6640Rec.set(itdmIO.getGenarea());
		}
	}

protected void setBonusIndicator4200()
	{
		bonusIndicator4210();
		bonusMethod4220();
	}

protected void bonusIndicator4210()
	{
		if (isNE(t6640rec.zbondivalc,SPACES)) {
			readTh5024500();
		}
		else {
			th502rec.th502Rec.set(SPACES);
		}
	}

protected void bonusMethod4220()
	{
		if (isNE(t6640rec.zbondivalc,SPACES)) {
			//if (isEQ(th502rec.zrevbonalc,"X")) {
			if (isEQ(th502rec.zrevbonalc,"Y")) {//ILIFE-6921 :Impact of ILIFE-4085 added this condition
				if (isEQ(t6640rec.revBonusMeth,SPACES)) {
					syserrrec.statuz.set(g586);
					systemError9000();
				}
			}
			
			//if(isEQ(th502rec.zcshdivalc,"X"))
			if (isEQ(th502rec.zcshdivalc,"Y")) {//ILIFE-6921 :Impact of ILIFE-4085 added this condition
				if (isEQ(t6640rec.zcshdivmth,SPACES)) {
					syserrrec.statuz.set(g586);
					systemError9000();
				}
			}
		}
		/*SET-BONUS-INDICATOR*/
		covrtrbIO.setBonusInd(t6640rec.zbondivalc);
		/*EXIT*/
	}

protected void annivProcessDate4300()
	{
		/*START*/
		covrtrbIO.setAnnivProcDate(99999999);
		datcon2rec.intDate1.set(covrtrbIO.getCrrcd());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			systemError9000();
		}
		covrtrbIO.setAnnivProcDate(datcon2rec.intDate2);
		/*EXIT*/
	}

protected void bonusDeclareDate4400()
	{
		/*START*/
		if (isEQ(th502rec.zconannalc,"X")) {
			covrtrbIO.setUnitStatementDate(covrtrbIO.getCrrcd());
		}
		else {
			covrtrbIO.setUnitStatementDate(99999999);
		}
		/*EXIT*/
	}

protected void readTh5024500()
	{
		start4510();
	}

protected void start4510()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(th502);
		itemIO.setItemitem(t6640rec.zbondivalc);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError9100();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hl49);
			itemIO.setStatuz(hl49);
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
		th502rec.th502Rec.set(itemIO.getGenarea());
		if (isEQ(th502rec.zrevbonalc,SPACES)
		&& isEQ(th502rec.zcshdivalc,SPACES)) {
			syserrrec.statuz.set(hl49);
			itemIO.setStatuz(hl49);
			syserrrec.params.set(itemIO.getParams());
			dbError9100();
		}
	}

protected void hcsdDetails4600()
	{
		try {
			hcsdDet4600();
		}
		catch (GOTOException e){
		}
	}

protected void hcsdDet4600()
	{
	 //if(isNE(th502rec.zcshdivalc,"X")){
	   if(isNE(th502rec.zcshdivalc,"Y")) {//ILIFE-6921 :Impact of ILIFE-4085 added this condition
			goTo(GotoLabel.exit4690);
		}
		covttrdIO.setDataKey(SPACES);
		covttrdIO.setChdrcoy(isuallrec.company);
		covttrdIO.setChdrnum(isuallrec.chdrnum);
		covttrdIO.setLife(isuallrec.life);
		covttrdIO.setCoverage(isuallrec.coverage);
		covttrdIO.setRider(isuallrec.rider);
		covttrdIO.setPlanSuffix(isuallrec.planSuffix);
		covttrdIO.setFormat(covttrdrec);
		covttrdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covttrdIO);
		if (isNE(covttrdIO.getStatuz(),varcom.oK)
		&& isNE(covttrdIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(covttrdIO.getStatuz());
			syserrrec.params.set(covttrdIO.getParams());
			dbError9100();
		}
		if (isEQ(covttrdIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit4690);
		}
		hcsdIO.setDataArea(SPACES);
		hcsdIO.setChdrcoy(isuallrec.company);
		hcsdIO.setChdrnum(isuallrec.chdrnum);
		hcsdIO.setLife(isuallrec.life);
		hcsdIO.setCoverage(isuallrec.coverage);
		hcsdIO.setRider(isuallrec.rider);
		hcsdIO.setPlanSuffix(isuallrec.planSuffix);
		hcsdIO.setValidflag("1");
		hcsdIO.setTranno(covrtrbIO.getTranno());
		hcsdIO.setEffdate(covrtrbIO.getCrrcd());
		hcsdIO.setZdivopt(covttrdIO.getZdivopt());
		hcsdIO.setZcshdivmth(t6640rec.zcshdivmth);
		hcsdIO.setPaycoy(covttrdIO.getPaycoy());
		hcsdIO.setPayclt(covttrdIO.getPayclt());
		hcsdIO.setPaymth(covttrdIO.getPaymth());
		hcsdIO.setFacthous(covttrdIO.getFacthous());
		hcsdIO.setBankkey(covttrdIO.getBankkey());
		hcsdIO.setBankacckey(covttrdIO.getBankacckey());
		hcsdIO.setPaycurr(covttrdIO.getPaycurr());
		hcsdIO.setFormat(hcsdrec);
		hcsdIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(hcsdIO.getParams());
			syserrrec.statuz.set(hcsdIO.getStatuz());
			dbError9100();
		}
	}

protected void rewriteCovrtrbRecord5000()
	{
		start5000();
	}

protected void start5000()
	{
		covrtrbIO.setChdrcoy(isuallrec.company);
		covrtrbIO.setChdrnum(isuallrec.chdrnum);
		covrtrbIO.setLife(isuallrec.life);
		covrtrbIO.setCoverage(isuallrec.coverage);
		covrtrbIO.setRider(isuallrec.rider);
		covrtrbIO.setPlanSuffix(isuallrec.planSuffix);
		covrtrbIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrtrbIO);
		if (isNE(covrtrbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrtrbIO.getParams());
			syserrrec.statuz.set(covrtrbIO.getStatuz());
			dbError9100();
		}
	}

protected void systemError9000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9000();
				}
				case seExit9090: {
					seExit9090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9000()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.seExit9090);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit9090()
	{
		isuallrec.statuz.set(varcom.bomb);
		exitProgram();
	}

protected void dbError9100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start9100();
				}
				case dbExit9190: {
					dbExit9190();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start9100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			goTo(GotoLabel.dbExit9190);
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit9190()
	{
		isuallrec.statuz.set(varcom.bomb);
		exitProgram();
	}
}
