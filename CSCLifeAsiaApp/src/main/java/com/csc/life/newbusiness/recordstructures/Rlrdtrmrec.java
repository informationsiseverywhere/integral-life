package com.csc.life.newbusiness.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:10:06
 * Description:
 * Copybook name: RLRDTRMREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Rlrdtrmrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData rlrdtrmRec = new FixedLengthStringData(63);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(2).isAPartOf(rlrdtrmRec, 0);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rlrdtrmRec, 2);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(rlrdtrmRec, 10);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(rlrdtrmRec, 12);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(rlrdtrmRec, 14);
  	public ZonedDecimalData riskTerm = new ZonedDecimalData(2, 0).isAPartOf(rlrdtrmRec, 16).setUnsigned();
  	public PackedDecimalData sumins = new PackedDecimalData(17, 2).isAPartOf(rlrdtrmRec, 18).setUnsigned();
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(rlrdtrmRec, 27);
  	/*BRD-139 starts*/
  	public FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rlrdtrmRec, 31); 
  	public FixedLengthStringData mtlplcd = new FixedLengthStringData(8).isAPartOf(rlrdtrmRec, 34);
  	public FixedLengthStringData loandur = new FixedLengthStringData(3).isAPartOf(rlrdtrmRec, 42);
  	public FixedLengthStringData polilcydur = new FixedLengthStringData(3).isAPartOf(rlrdtrmRec, 45);
  	public FixedLengthStringData effdate = new FixedLengthStringData(8).isAPartOf(rlrdtrmRec, 48);
  	public PackedDecimalData lage = new PackedDecimalData(3, 0).isAPartOf(rlrdtrmRec, 56);//ILIFE-7521
  	/*BRD-139 ends*/
	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rlrdtrmRec, 59);//ILIFE-7521
	public List<BigDecimal> reducedSumins = new ArrayList<>();//ILIFE-7521



	public void initialize() {
		COBOLFunctions.initialize(rlrdtrmRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		rlrdtrmRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}