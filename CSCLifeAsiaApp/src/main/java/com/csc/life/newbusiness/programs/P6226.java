/*
 * File: P6226.java
 * Date: 30 August 2009 0:36:56
 * Author: Quipoz Limited
 * 
 * Class transformed from P6226.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.life.newbusiness.dataaccess.AsgnlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.screens.S6226ScreenVars;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*               CONTRACT ASSIGNMENT  - NEW BUSINESS
*                    P6226 - Contract Assignee
* Overview :
*     An assignee is a Company/Bank/Individual who is related
* to the contract by some declaration of interest. They are
* therefore entitled to any information related to the
* Contract entitling them to Schedule Copies etc. An example
* may be a Loan of money by the bank to pay for the life
* insurance or perhaps a trustee.
*
*     This program contains a multiple input subfile which
* can hold any amount of assignees dated on a from and to
* basis. Assignees can be Deleted/Modified/Added, all
* assignees are attatched to the client by client roles and a
* detail line is contained against the contract for each
* assignee (ASGNPF).
*
*  1000-Initialise -
* Load the Subfile for all attached assignees.
*
*     Subfile - Load the First page into the subfile, by
* reading through the Assignee File
* ASGN and a function of BEGN. We must however signify the
* first page by holding a first page flag in working storage,
* this will be used when ROLU or ROLD is in action. The BEGN
* will be on the CHDRNUM and we will
* perform a LOOP section 6 times for each line of the subfile.
*
*     Each loop through will load 1 assignee until we have a
* Subfile Screen full. This may mean outputing blank lines.
*
*     Firstly read the assignee file storing the key for the
* first record loaded into the page as this will be used for
* Pageing Down and Up. If the File key changes then set the
* subfile record to blanks and a flag to signify that there
* are no more assignees to load. On a successful read we will
* attempt to find the assignees name by reading the CLTS file
* with the assignee number, again if found format the name. We
* can now set up the subfile record with the assignee details.
* Note that we will store the assignee number in a hidden
* field, to check for modifications. Finally load the next
* available sequence number to a hidden sequence number. This
* will control the order in which record were created on the
* file. Each time a successful record is read we will check
* its sequence number against a working storage field to find
* the highest number in order to assign new records correctly.
* One last point is a hidden field called UPDTEFLAG which is
* set with an 'N' if an assignee record is loaded to the
* subfile and a ' ' if a blank line is inserted. This flag
* will determine what action is to be taken in the record when
* The Assignee file is updated. The 2000 section will check
* for any changes and set the flag appropriately, see the
* 2000-section for more detail.
*
*     Whether we have a blank record or an assignee to load
* we will perform a subfile add on the screen (SADD) which
* will place the detail line on the screen.
*
*     The above will be performed until we have loaded that
* first initial screen.
*
*  2000-Validation -
*  ~~~~~~~~~~~~~~~~  Call the screen to be displayed, then
* check for screen status equal to KILL or CALC. If KILL then
* exit, but if CALC move 'Y' to WSSP-EDTERROR to cause the
* screen to be called at the start of the 2000-section again.
* Next we will check for a request to ROLD, this must cause an
* error if the First page flag set in the Subfile load is
* equal to 'Y', signifying we are at the start of the file. We
* must also flag the fact that ROLD  or ROLU was requested for
* use in the 4000-section where any such request will be
* handled.- This is because we must update the file before
* loading the next/previous page otherwise we would lose all
* the information entered.
*
*     The main part of the validation section is to action
* any changes made to the subfile records, hence we will
* validate all subfile records until the end of the screen is
* reached. Each record is validated as follows :
*
*     - Perform a sequential read next change to find
*       the any altered records (SRNCH).
*
*     - If the Subfile record is blank and the hidden
*       update flag is 'N' then we will reset the flag
*       to 'D' as this record was previously loaded from
*       the ASGN file and is now to be deleted.
*
*     - If the fields are blank and the update flag is
*       blank move an 'X' to the update flag to signify
*       the record was blank on the subfile load and is
*       still blank now.
*
*     - If the assignee number is not blank then read
*       the CLTS file for the assignees name. this is
*       prevelant on an option of CALC after altering
*       the assignees number.
*
*     - Validate that the date TO is not prior to the
*       date FROM.
*
*     - If we have got this far and the update flag is
*       still 'N' or ' ' we can reset the flag to 'U'
*       for Update and 'A' for add. 'N' means we loaded
*       a record from the ASGN file initially and ' '
*       means we have just added details to an initial
*       blank subfile line.
*
*     Lastly we must Update the subfile record with a
* function of SUPD to pass the newly changed update flags and
* screen changes into force.
*
* 3000-Update -
* ~~~~~~~~~~~
* SCRN-STATUZ = KILL then exit, otherwise update the assignee
* file for each record in the subfile i.e. 6 times.
* For each record :   Call the screen with an SREAD to read
*                     the record next check to see if the
*                     record has been updated/added/deleted.
*
*                     - No updates i.e.flag = 'N' or ' '
*                       exit the section.
*
*                     - Set up the Key details of the
*                       ASGNLNB file for the following
*                       updates.
*
*                     - If the record is to be deleted
*                       then READH the ASGN record then
*                       DELET the held record and finally
*                       delete the client Role as above
*                       with a READH followed by a DELET.
*                       To read the CLRR record, use the
*                       following key details.
*
*                            CLNTPFX = 'CN'
*                            CLNTCOY = WSSP-FSUCO
*                            CLNTNUM = Hidden assignee No
*                            CLRRROLE= 'NE'
*                            FOREPFX = 'CH'
*                            FORECOY = CHDRCOY
*                            FORENUM = CHDRNUM
*
*                     - If the record is to be added then
*                       add 1 to the stored seqno and
*                       move it to the file, move all
*                       details from the screen record to
*                       the file and WRITR the rec.
*                       We must also add a Client Role
*                       record with peramaters as above,
*                       but the CLNTNUM field will
*                       contain the screen assignee
*                       number.
*
*                     - If we have got this far then we
*                       must be updating the record.
*                       Simply read the record and hold
*                       it READH and move the screen
*                       details to the file and rewrite
*                       the record REWRT.
*
* 4000-Next -
* ~~~~~~~~~
*     This section will handle either a  ROLU, ROLD or EXIT
* request.
*
* ROLU :  Set the first page flag off as we are loading the
* next page of data. set the file function to read the next
* record then clear the Subfile for the load. We must now load
* the screen with another 6 records by performing the same
* process as in the 1000-section see above.
*
* ROLD :  If Rolldown requested move the key of the first
* record in the last subfile page to the key of the file and
* BEGIN in order to position the file correctly. We will read
* backwards through the file in order to find the previous 6
* records. To do this however we must read seven record
* backwards in order to determine if we are at the first
* record of the file, as this will mean we must set flags for
* validation. If at any time during the NEXTP reads we hit the
* beginning of the file and have not yet filled the subfile,
* We must load forwards as in the above 1000-section subfile
* load. Note that we can never load the subfile from the
* bottom up as the IO  RRN cannot be manipulated. We must find
* our file position and load forward on a clean subfile.
*
*      FILES USED     - ASGNLNBSKM
*                       CLTSSKM
*                       CLRRSKM
*                       CHDRLNBSKM
*
*****************************************************************
* </pre>
*/
public class P6226 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6226");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/*----First page is set when at the start of the ASGN file.*/
	private String wsaaFirstPage = "";
		/*----RRN used to read back up the ASGN file one screens worth.*/
	private PackedDecimalData wsaaRrn = new PackedDecimalData(5, 0);
		/*----SEQNO used to load the file in order of entry.*/
	private PackedDecimalData wsaaSeqno = new PackedDecimalData(3, 0);

		/*----The following work area is to hold the ASGN key of the
		----first subfile record for ROLLDOWN processing.*/
	private FixedLengthStringData wsaaFirstAsgnKey = new FixedLengthStringData(256);
	private FixedLengthStringData wsaaAsgnChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaFirstAsgnKey, 0);
	private FixedLengthStringData wsaaAsgnChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstAsgnKey, 1);
	private PackedDecimalData wsaaAsgnSeqno = new PackedDecimalData(3, 0).isAPartOf(wsaaFirstAsgnKey, 9);
	private FixedLengthStringData wsaaAsgnAsgnnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstAsgnKey, 11).init(SPACES);
	private FixedLengthStringData filler = new FixedLengthStringData(237).isAPartOf(wsaaFirstAsgnKey, 19, FILLER).init(SPACES);

		/* WSAA-ROLL-FLGS */
	private FixedLengthStringData wsaaRolu = new FixedLengthStringData(1);
	private Validator rollup = new Validator(wsaaRolu, "Y");

	private FixedLengthStringData wsaaRold = new FixedLengthStringData(1);
	private Validator rolldown = new Validator(wsaaRold, "Y");

		/*----Flag to signify the store of the first ASGN record loaded
		----at subfile RRN 1. Used for ROLD request.*/
	private FixedLengthStringData wsaaFirstSubfileRec = new FixedLengthStringData(1);
	private Validator firstSubfileRec = new Validator(wsaaFirstSubfileRec, "Y");

		/*----Flag to signify no more records to load from ASGNLNB.*/
	private FixedLengthStringData wsaaNoMoreAssignees = new FixedLengthStringData(1);
	private Validator asgnlnbEof = new Validator(wsaaNoMoreAssignees, "Y");
	private String wsaaUpdated = "N";
		/* ERRORS */
	private static final String e017 = "E017";
	private static final String f498 = "F498";
	private static final String f691 = "F691";
	private static final String f782 = "F782";
	private static final String h359 = "H359";
	private static final String h366 = "H366";
	private static final String e032 = "E032";
	private static final String rgig = "RGIG";
		/* FORMATS */
	private static final String asgnlnbrec = "ASGNLNBREC";
	private static final String clrrrec = "CLRRREC";
	private static final String covtlnbrec = "COVTLNBREC";
	private AsgnlnbTableDAM asgnlnbIO = new AsgnlnbTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S6226ScreenVars sv = ScreenProgram.getScreenVars( S6226ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		exit2190, 
		exit2290, 
		exit3190, 
		exit5090
	}

	public P6226() {
		super();
		screenVars = sv;
		new ScreenModel("S6226", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("S6226", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsspcomn.flag, "I")) {
			sv.asgnselOut[varcom.pr.toInt()].set("Y");
			sv.reasoncdOut[varcom.pr.toInt()].set("Y");
			sv.commfromOut[varcom.pr.toInt()].set("Y");
			sv.commtoOut[varcom.pr.toInt()].set("Y");
		}
		/*    Retrieve contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Load the first page into the subfile.*/
		wsaaNoMoreAssignees.set("N");
		wsaaFirstSubfileRec.set("Y");
		wsaaSeqno.set(ZERO);
		/* Read the Assignee file with a logical view of ASGNLNB.*/
		/* This file will contain all the Assignees against a contract*/
		/*    the Key will be Company, Contract number, and Sequence*/
		/*    number which is used to load the file in the order in*/
		/*    which it was entered.*/
		/* 5000-section is a commom area used to load the 6 lines of the*/
		/*    subfile. It will be used within the 4000-section for ROLD*/
		/*    and ROLU requests.*/
		asgnlnbIO.setDataArea(SPACES);
		asgnlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		asgnlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		asgnlnbIO.setSeqno(ZERO);
		asgnlnbIO.setFunction(varcom.begn);
		for (int loopVar1 = 0; !(loopVar1 == 6); loopVar1 += 1){
			loadSubfile5000();
		}
		/* Set the screen to signify more records can be loaded. Note that*/
		/*    this may NOT really be the case, but we must trick the*/
		/*    the system to enable us to add records.*/
		/* Set the screen to signify it is the first page of data. We can*/
		/*    then Validate against a ROLD request when there are no recs*/
		/*    to load.*/
		scrnparams.subfileMore.set("Y");
		wsaaFirstPage = "Y";
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateScreen2010();
			checkForErrors2030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S6226IO' USING SCRN-SCREEN-PARAMS                      */
		/*                         S6226-DATA-AREA                         */
		/*                         S6226-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validateScreen2010()
	{
		/* If terminating processing go to exit.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/* If roll down requested and display shows first page then error.*/
		if (isEQ(scrnparams.statuz, varcom.rold)
		&& isEQ(wsaaFirstPage, "Y")) {
			scrnparams.errorCode.set(f498);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Set flag to signify ROLD requested.*/
		if (isEQ(scrnparams.statuz, varcom.rold)) {
			wsaaRold.set("Y");
		}
		/* If roll up requested and no more assignees to be displayed then*/
		/*    error.*/
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaRolu.set("Y");
		}
	}

protected void checkForErrors2030()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE-SUBFILE*/
		/* Validate all enteries in the subfile.*/
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2100();
		}
		
	}

protected void validateSubfile2100()
	{
		try {
			readNextModifiedRecord2110();
			updateErrorIndicators2130();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readNextModifiedRecord2110()
	{
		/* Read down the subfile to find the next modified record.*/
		scrnparams.function.set(varcom.srnch);
		processScreen("S6226", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
		/*FIELD-VALIDATIONS*/
		/* Validate changes to any of the subfile fields.*/
		fieldValidations2200();
	}

protected void updateErrorIndicators2130()
	{
		/* SUPD is performed on each record NOT just when errors occur,*/
		/*   it is used to update the S6226-updteflag for processing in*/
		/*   the 3000 section.*/
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("S6226", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void fieldValidations2200()
	{
		try {
			delete2210();
			replace2215();
			blank2220();
			validate2240();
			updateOrAdd2270();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void delete2210()
	{
		/* Skip validation for blank subfile record. */
		if (isEQ(sv.asgnsel, SPACES)
		&& isEQ(sv.assigneeName, SPACES)
		&& isEQ(sv.reasoncd, SPACES)
		&& isEQ(sv.commfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.commto, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.exit2290);
		}
	
		/* Check if fields have been blanked out i.e. deleted.*/
		if (isEQ(sv.asgnsel, SPACES)
		&& isEQ(sv.reasoncd, SPACES)
		&& isEQ(sv.commfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.commto, varcom.vrcmMaxDate)
		&& isEQ(sv.updteflag, "N")) {
			sv.updteflag.set("D");
			wsaaUpdated = "Y";
			goTo(GotoLabel.exit2290);
		}
	}

protected void replace2215()
	{
		/* Check if the Assignee Number has changed.                       */
		if (isNE(sv.asgnsel, sv.hasgnnum)
		&& isNE(sv.hasgnnum, SPACES)) {
			sv.updteflag.set("R");
			wsaaUpdated = "Y";
			cltsIO.setDataArea(SPACES);
			cltsIO.setClntnum(sv.asgnsel);
			x100AssigneeName();
			goTo(GotoLabel.exit2290);
		}
	}

protected void blank2220()
	{
		/* Fields have been blanked out for a new record*/
		if (isEQ(sv.asgnsel, SPACES)
		&& isEQ(sv.reasoncd, SPACES)
		&& isEQ(sv.commfrom, varcom.vrcmMaxDate)
		&& isEQ(sv.commto, varcom.vrcmMaxDate)
		&& isEQ(sv.updteflag, " ")) {
			sv.updteflag.set("X");
			wsaaUpdated = "Y";
			goTo(GotoLabel.exit2290);
		}
	}

protected void validate2240()
	{
		/* ILIFE-5081 Removed unnecessary validations*/
		/* Read main coverage details.                                     */
		covtlnbIO.setDataKey(SPACES);
		covtlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covtlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		covtlnbIO.setLife("01");
		covtlnbIO.setCoverage("01");
		covtlnbIO.setRider("00");
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFormat(covtlnbrec);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
		&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())) {
			covtlnbIO.setStatuz(varcom.endp);
		}

		/* Read the Assignee name if the assignee number is not spaces.*/
		if (isNE(sv.asgnsel, SPACES)) {
			cltsIO.setDataArea(SPACES);
			cltsIO.setClntnum(sv.asgnsel);
			x100AssigneeName();
		}

		/* The Assignment To date should be defaulted to the Risk          */
		/* Cessation date.                                                 */
		if (isEQ(sv.commto, varcom.vrcmMaxDate)) {
			if (isNE(sv.asgnsel, SPACES)
			|| isNE(sv.reasoncd, SPACES)
			|| isNE(sv.commfrom, varcom.vrcmMaxDate)) {
				sv.commto.set(covtlnbIO.getRiskCessDate());
			}
		}
		else {
			if (isGT(sv.commto, covtlnbIO.getRiskCessDate()) && covtlnbIO.getRiskCessDate().gt(0)) { //ILIFE-5796
				sv.commtoErr.set(e032);
			}
		}
		/* If the Commence from date is after the Commence to date then*/
		/*    error.*/
		if (isGT(sv.commfrom, sv.commto)) {
			sv.commtoErr.set(e017);
		}
		/* If the Commence from date is before the Contract Commencement   */
		/* date then error.                                                */
		if (isLT(sv.commfrom, chdrlnbIO.getOccdate())) {
			sv.commfromErr.set(h359);
		}
		/* If the reason code is blank, then throw an error.               */
		if (isEQ(sv.reasoncd, SPACES)
				&& (isNE(sv.asgnsel, SPACES)
					|| isNE(sv.commfrom, varcom.vrcmMaxDate)
					|| isNE(sv.commto, varcom.vrcmMaxDate))) {
					sv.reasoncdErr.set(h366);
				}

	}

protected void updateOrAdd2270()
	{
		/* The subfile record has been changed so update the hidden*/
		/*    flag. If it is 'N' then a record exists already so update*/
		/*    else if the flag is ' ' then we are adding a new record.*/
		if (isEQ(sv.updteflag, "N")) {
			/* ILIFE-5081 code changes started*/
			if (isNE(sv.hasgnnum, sv.asgnsel)) {
				sv.updteflag.set("D");
			}
			else {
				sv.updteflag.set("U");
			}
		}
		if (isEQ(sv.updteflag, SPACES)
				&& isNE(sv.asgnsel, SPACES)
				&& isNE(sv.reasoncd, SPACES)
				&& isNE(sv.commfrom, varcom.vrcmMaxDate)) {
					sv.updteflag.set("A");
				}
		/* ILIFE-5081 code changes end*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/* If termination of processing then go to exit, no updates reqd.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/* Perform updates for all subfile records up to a maximum of*/
		/*    one page.*/
		for (wsaaRrn.set(1); !(isGT(wsaaRrn, 6)); wsaaRrn.add(1)){
			updateAssignees3100();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DO THE FOLLOW UP FILE UPDATING
	* </pre>
	*/
protected void updateAssignees3100()
	{
		try {
			readSubfile3110();
			setAsgnKey3130();
			deleteAssignee3140();
			replaceAssignee3145();
			addAssignee3150();
			updateAssignee3160();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void readSubfile3110()
	{
		/* Get the subfile record using the relative record number.*/
		scrnparams.subfileRrn.set(wsaaRrn);
		scrnparams.function.set(varcom.sread);
		processScreen("S6226", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*NO-UPDATES*/
		/* The subfile record has not been changed.*/
		if (isEQ(sv.updteflag, SPACES)
		|| isEQ(sv.updteflag, "N")) {
			/* GO TO 3190-EXIT                                           */
			/*ELSE                                                         */
			/*  MOVE 'Y'                 TO WSAA-UPDATED.                 */
			/*  GO TO 3190-EXIT.                                          */
			goTo(GotoLabel.exit3190);
		}
	}

	/**
	* <pre>
	***     GO TO 3190-EXIT.                                          
	* </pre>
	*/
protected void setAsgnKey3130()
	{
		/* Set up key for updating or writing an Assignee.*/
		asgnlnbIO.setDataArea(SPACES);
		asgnlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		asgnlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		asgnlnbIO.setAsgnnum(sv.hasgnnum);
		asgnlnbIO.setSeqno(sv.hseqno);
	}

protected void deleteAssignee3140()
	{
		/* If the update is a delete.*/
		if (isEQ(sv.updteflag, "D")) {
			asgnlnbIO.setFunction(varcom.readh);
			asgnlnbFile3300();
			asgnlnbIO.setFormat(asgnlnbrec);
			asgnlnbIO.setFunction(varcom.delet);
			asgnlnbFile3300();
			deleteClientRole3400();
			/* ILIFE-5081 code changes started*/
			if (isEQ(sv.asgnsel, SPACES)) {
				goTo(GotoLabel.exit3190);
			}
			else {
				sv.updteflag.set("A");
			}
			/* ILIFE-5081 code changes end*/
		}
	}

protected void replaceAssignee3145()
	{
		/* If the update is a replace.                                     */
		if (isEQ(sv.updteflag, "R")) {
			asgnlnbIO.setAsgnnum(sv.hasgnnum);
			asgnlnbIO.setFunction(varcom.readh);
			asgnlnbFile3300();
			asgnlnbIO.setFormat(asgnlnbrec);
			asgnlnbIO.setFunction(varcom.delet);
			asgnlnbFile3300();
			deleteClientRole3400();
			addAssignee3200();
			goTo(GotoLabel.exit3190);
		}
	}

protected void addAssignee3150()
	{
		/* If the update is an addition.*/
		if (isEQ(sv.updteflag, "A")) {
			addAssignee3200();
			goTo(GotoLabel.exit3190);
		}
	}

protected void updateAssignee3160()
	{
		/* If the update is an update.*/
		if (isEQ(sv.updteflag, "U")) {
			asgnlnbIO.setFunction(varcom.readh);
			asgnlnbFile3300();
		}
		if (isEQ(sv.updteflag, "X")) {
			return ;
		}
		asgnlnbIO.setAsgnnum(sv.asgnsel);
		asgnlnbIO.setReasoncd(sv.reasoncd);
		asgnlnbIO.setCommfrom(sv.commfrom);
		asgnlnbIO.setCommto(sv.commto);
		/*MOVE VRCM-TERM              TO ASGNLNB-TERMID.               */
		asgnlnbIO.setTermid(varcom.vrcmTermid);
		asgnlnbIO.setUser(varcom.vrcmUser);
		asgnlnbIO.setTransactionDate(varcom.vrcmDate);
		asgnlnbIO.setTransactionTime(varcom.vrcmTime);
		asgnlnbIO.setFormat(asgnlnbrec);
		asgnlnbIO.setFunction(varcom.rewrt);
		asgnlnbFile3300();
	}

protected void addAssignee3200()
	{
		asgnlnbFile3210();
		addRole3220();
	}

protected void asgnlnbFile3210()
	{
		/* Set up all fields for the creation of an Assignee record.*/
		asgnlnbIO.setAsgnnum(sv.asgnsel);
		asgnlnbIO.setReasoncd(sv.reasoncd);
		asgnlnbIO.setCommfrom(sv.commfrom);
		asgnlnbIO.setCommto(sv.commto);
		asgnlnbIO.setAsgnpfx("CN");
		asgnlnbIO.setTranno(chdrlnbIO.getTranno());
		wsaaSeqno.add(1);
		asgnlnbIO.setSeqno(wsaaSeqno);
		/*MOVE VRCM-TERM              TO ASGNLNB-TERMID.               */
		asgnlnbIO.setTermid(varcom.vrcmTermid);
		asgnlnbIO.setUser(varcom.vrcmUser);
		asgnlnbIO.setTransactionDate(varcom.vrcmDate);
		asgnlnbIO.setTransactionTime(varcom.vrcmTime);
		asgnlnbIO.setFormat(asgnlnbrec);
		asgnlnbIO.setFunction(varcom.writr);
		asgnlnbFile3300();
	}

protected void addRole3220()
	{
		/* Create a new Client Role for the Assignee.*/
		clrrIO.setParams(SPACES);
		clrrIO.setClrrrole("NE");
		clrrIO.setForepfx("CH");
		clrrIO.setForecoy(chdrlnbIO.getChdrcoy());
		clrrIO.setForenum(chdrlnbIO.getChdrnum());
		clrrIO.setClntpfx("CN");
		clrrIO.setClntcoy(wsspcomn.fsuco);
		clrrIO.setClntnum(sv.asgnsel);
		clrrIO.setFormat(clrrrec);
		clrrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)
		&& isNE(clrrIO.getStatuz(), varcom.dupr)) {
			syserrrec.statuz.set(clrrIO.getStatuz());
			fatalError600();
		}
	}

protected void asgnlnbFile3300()
	{
		/*ASGNLNB-FILE*/
		/* Call the Assignee file.*/
		SmartFileCode.execute(appVars, asgnlnbIO);
		if (isNE(asgnlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(asgnlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void deleteClientRole3400()
	{
		readRole3410();
		deleteRole3420();
	}

protected void readRole3410()
	{
		/* Read Client Role record prior to delete*/
		clrrIO.setParams(SPACES);
		clrrIO.setFunction(varcom.readh);
		clrrIO.setClntpfx("CN");
		clrrIO.setClntcoy(wsspcomn.fsuco);
		clrrIO.setClntnum(sv.hasgnnum);
		clrrIO.setClrrrole("NE");
		clrrIO.setForepfx("CH");
		clrrIO.setForecoy(chdrlnbIO.getChdrcoy());
		clrrIO.setForenum(chdrlnbIO.getChdrnum());
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrrIO.getStatuz());
			fatalError600();
		}
	}

protected void deleteRole3420()
	{
		/* Delete previous ASGN from the Client role file.*/
		clrrIO.setFormat(clrrrec);
		clrrIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrrIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		nextProgram4010();
	}

protected void nextProgram4010()
	{
		/* If roll up requested set first page flag off and the First*/
		/*   subfile record flag ON, as we are about to load a new page.*/
		/* Move NEXTR to read the next GN record. Clear the subfile for*/
		/*   loading and read in the next 6 lines.*/
		if (rollup.isTrue()) {
			wsaaFirstPage = "N";
			wsaaFirstSubfileRec.set("Y");
			asgnlnbIO.setFunction(varcom.nextr);
			clearSubfile4200();
			if (isEQ(wsspcomn.flag, "I")) {
				sv.asgnselOut[varcom.pr.toInt()].set("Y");
				sv.reasoncdOut[varcom.pr.toInt()].set("Y");
				sv.commfromOut[varcom.pr.toInt()].set("Y");
				sv.commtoOut[varcom.pr.toInt()].set("Y");
			}
			for (int loopVar2 = 0; !(loopVar2 == 6); loopVar2 += 1){
				loadSubfile5000();
			}
		}
		/* If roll down requested move the key of the first record in the*/
		/*   last subfile page to the key of the file and BEGIN in order*/
		/*   to position the file correctly.*/
		/* We will read backwards through the file in order to find the*/
		/*   previous 6 records.*/
		if (rolldown.isTrue()) {
			asgnlnbIO.setParams(SPACES);
			asgnlnbIO.setDataKey(wsaaFirstAsgnKey);
			asgnlnbIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, asgnlnbIO);
			if (isNE(asgnlnbIO.getStatuz(), varcom.oK)
			&& isNE(asgnlnbIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(asgnlnbIO.getParams());
				fatalError600();
			}
			else {
				/* Set the count to position itself 7 records back. This will*/
				/*   determine if we are loading the first page again. i.e.*/
				/*   If the end of the file is hit on the seventh NEXTP then we*/
				/*   are at the start of the file.*/
				wsaaRrn.set(7);
				while ( !(isEQ(asgnlnbIO.getStatuz(), varcom.endp)
				|| isNE(asgnlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
				|| isNE(asgnlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
				|| isEQ(wsaaRrn, 0))) {
					roldSubfileLoad4100();
				}
				
			}
		}
		/* If we have requested a page backwards (ROLD) then we perform*/
		/*   the load of the previous 6 records. Note that The position*/
		/*   of the file will either be 7 records back with a function*/
		/*   of NEXTP or positioned at the start of the file with a BEGN.*/
		if (rolldown.isTrue()
		&& isEQ(wsaaRrn, 0)) {
			wsaaFirstSubfileRec.set("Y");
			clearSubfile4200();
			if (isEQ(wsspcomn.flag, "I")) {
				sv.asgnselOut[varcom.pr.toInt()].set("Y");
				sv.reasoncdOut[varcom.pr.toInt()].set("Y");
				sv.commfromOut[varcom.pr.toInt()].set("Y");
				sv.commtoOut[varcom.pr.toInt()].set("Y");
			}
			for (int loopVar3 = 0; !(loopVar3 == 6); loopVar3 += 1){
				loadSubfile5000();
			}
		}
		/* Clear the Roll up and Roll down flags for future use.*/
		/* Redisplay the screen.*/
		if (rollup.isTrue()
		|| rolldown.isTrue()) {
			wsaaRolu.set("N");
			wsaaRold.set("N");
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			/* Perform updates for this transaction.*/
			/* Exit to the next program in the stack.*/
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
			scrnparams.function.set("HIDEW");
		}
	}

protected void roldSubfileLoad4100()
	{
		roldSubfileLoad4110();
	}

protected void roldSubfileLoad4110()
	{
		/* Read the previous Assignee records until a key change or the*/
		/*   section has been performed 7 times.*/
		asgnlnbIO.setFunction(varcom.nextp);
		SmartFileCode.execute(appVars, asgnlnbIO);
		if (isNE(asgnlnbIO.getStatuz(), varcom.oK)
		&& isNE(asgnlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(asgnlnbIO.getParams());
			fatalError600();
		}
		/* If key changes then we will have to clear the subfile and start*/
		/*   again but at the start of the file this time with a BEGN.*/
		/* This means we are now loading the first page of the whole*/
		/*   subfile. 5000-section will load each line for us.*/
		if (isNE(asgnlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(asgnlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(asgnlnbIO.getStatuz(), varcom.endp)) {
			asgnlnbIO.setParams(SPACES);
			clearSubfile4200();
			asgnlnbIO.setDataKey(wsaaFirstAsgnKey);
			asgnlnbIO.setFunction(varcom.begn);
			wsaaFirstPage = "Y";
			wsaaRrn.set(0);
			return ;
		}
		/* We will move The ASGN key to store on each record as it will*/
		/*    be required if the end of file is reached.*/
		/* Decrease the count until we are 7 records back up the file.*/
		/* Assumeing we can count back 7 records, we will move a function*/
		/*    of NEXTR to read the next record forward in the 5000-section*/
		/* Remember we read 7 records backwards to find out if we will hit*/
		/*    the beginning of the file.*/
		wsaaFirstAsgnKey.set(asgnlnbIO.getDataKey());
		wsaaRrn.subtract(1);
		asgnlnbIO.setFunction(varcom.nextr);
	}

protected void clearSubfile4200()
	{
		/*CLEAR-SUBFILE*/
		/* Initialise the Screen subfile.*/
		sv.subfileArea.set(SPACES);
		sv.commfrom.set(varcom.vrcmMaxDate);
		sv.commto.set(varcom.vrcmMaxDate);
		sv.hseqno.set(ZERO);
		scrnparams.function.set(varcom.sclr);
		processScreen("S6226", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     Load subfile
	* </pre>
	*/
protected void loadSubfile5000()
	{
		try {
			assigneeRead5010();
			endOfFile5020();
			asgnToScreen5030();
			addSubfileRecord5040();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void assigneeRead5010()
	{
		/* Read the ASGN file for all Assignees associated with the*/
		/*    Contract. This is performed 6 times once for each line of*/
		/*    the subfile.*/
		SmartFileCode.execute(appVars, asgnlnbIO);
		if (isNE(asgnlnbIO.getStatuz(), varcom.oK)
		&& isNE(asgnlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(asgnlnbIO.getParams());
			fatalError600();
		}
		/* On the first record store the key for use in ROLD request.*/
		if (firstSubfileRec.isTrue()) {
			wsaaFirstSubfileRec.set("N");
			if (isNE(asgnlnbIO.getAsgnnum(), SPACES)) {
				wsaaFirstAsgnKey.set(asgnlnbIO.getRecKeyData());
			}
		}
		asgnlnbIO.setFunction(varcom.nextr);
	}

protected void endOfFile5020()
	{
		/* On change of key clear that subfile record and set flag to*/
		/*    signify no more Assignee records.*/
		if (isNE(asgnlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(asgnlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(asgnlnbIO.getStatuz(), varcom.endp)) {
			wsaaNoMoreAssignees.set("Y");
			sv.subfileFields.set(SPACES);
			sv.commfrom.set(varcom.vrcmMaxDate);
			sv.commto.set(varcom.vrcmMaxDate);
			sv.hseqno.set(ZERO);
			addSubfileRecord5040();
			goTo(GotoLabel.exit5090);
		}
	}

protected void asgnToScreen5030()
	{
		/* Read the Assignee name and format it if the Assignee number is*/
		/*    not blank.*/
		if (isNE(asgnlnbIO.getAsgnnum(), SPACES)) {
			cltsIO.setDataArea(SPACES);
			cltsIO.setClntnum(asgnlnbIO.getAsgnnum());
			x100AssigneeName();
		}
		/* Set up the Screen subfile fields.*/
		sv.asgnsel.set(asgnlnbIO.getAsgnnum());
		sv.hasgnnum.set(asgnlnbIO.getAsgnnum());
		sv.reasoncd.set(asgnlnbIO.getReasoncd());
		sv.commfrom.set(asgnlnbIO.getCommfrom());
		sv.commto.set(asgnlnbIO.getCommto());
		sv.hseqno.set(asgnlnbIO.getSeqno());
		if (isGT(asgnlnbIO.getSeqno(), wsaaSeqno)) {
			wsaaSeqno.set(asgnlnbIO.getSeqno());
		}
		sv.updteflag.set("N");
	}

protected void addSubfileRecord5040()
	{
		/* Add the record to the Subfile.*/
		scrnparams.function.set(varcom.sadd);
		processScreen("S6226", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void x100AssigneeName()
	{
		x110LoadScreen();
	}

protected void x110LoadScreen()
	{
		/* Read the clients file for the Assignee name.*/
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.assigneeName.set(SPACES);
			sv.asgnselErr.set(rgig);
		}
		else {
			plainname();
			/*       Check Death Date for assignee < Orig Comm Date            */
			if (isNE(cltsIO.getCltdod(), varcom.vrcmMaxDate)
			&& isGT(chdrlnbIO.getOccdate(), cltsIO.getCltdod())) {
				sv.asgnselErr.set(f782);
			}
			sv.assigneeName.set(wsspcomn.longconfname);
		}
	}
}
