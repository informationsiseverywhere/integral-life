package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SM600
 * @version 1.0 generated on 30/08/09 07:07
 * @author Quipoz
 */
public class Sm600ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(314);
	public FixedLengthStringData dataFields = new FixedLengthStringData(138).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData genlcdex = DD.genlcdex.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData genlcdey = DD.genlcdey.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData genldesc = DD.genldesc.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,67);
	public ZonedDecimalData occpct = DD.occpct.copyToZonedDecimal().isAPartOf(dataFields,97);
	public ZonedDecimalData statfrom = DD.statfrom.copyToZonedDecimal().isAPartOf(dataFields,102);
	public ZonedDecimalData statto = DD.statto.copyToZonedDecimal().isAPartOf(dataFields,117);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData ynflag = DD.ynflag.copy().isAPartOf(dataFields,137);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 138);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData genlcdexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData genlcdeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData genldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData occpctErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData statfromErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData stattoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ynflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 182);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] genlcdexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] genlcdeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] genldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] occpctOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] statfromOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] stattoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ynflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sm600screenWritten = new LongData(0);
	public LongData Sm600protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sm600ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, genldesc, occpct, ynflag, statfrom, statto, genlcdex, genlcdey};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, genldescOut, occpctOut, ynflagOut, statfromOut, stattoOut, genlcdexOut, genlcdeyOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, genldescErr, occpctErr, ynflagErr, statfromErr, stattoErr, genlcdexErr, genlcdeyErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sm600screen.class;
		protectRecord = Sm600protect.class;
	}

}
