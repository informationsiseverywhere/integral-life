package com.csc.life.newbusiness.dataaccess.dao;

import com.csc.life.newbusiness.dataaccess.model.Slncpf;

/**
 * @author gsaluja2
 *
 */
public interface SlncpfDAO {
	public void insertSlncData(Slncpf slncpf);
	public void updateSlncData(Slncpf slncpf);
	public Slncpf readSlncpfData(String chdrnum);
}
