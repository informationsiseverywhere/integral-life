package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sm601screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sm601ScreenVars sv = (Sm601ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sm601screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sm601ScreenVars screenVars = (Sm601ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.genlcdex03.setClassString("");
		screenVars.genlcdex04.setClassString("");
		screenVars.genlcdex02.setClassString("");
		screenVars.genlcdex01.setClassString("");
		screenVars.genlcdex05.setClassString("");
		screenVars.genlcdex06.setClassString("");
		screenVars.genlcdex09.setClassString("");
		screenVars.genlcdex10.setClassString("");
		screenVars.genlcdex07.setClassString("");
		screenVars.genlcdex08.setClassString("");
		screenVars.genlcdex11.setClassString("");
		screenVars.genlcdex12.setClassString("");
		screenVars.genlcdex13.setClassString("");
		screenVars.genlcdex14.setClassString("");
		screenVars.genlcdex15.setClassString("");
		screenVars.genlcdex16.setClassString("");
		screenVars.genlcdex17.setClassString("");
		screenVars.genlcdex18.setClassString("");
		screenVars.genlcdex19.setClassString("");
		screenVars.genlcdex20.setClassString("");
		screenVars.genlcdex21.setClassString("");
		screenVars.genlcdex22.setClassString("");
		screenVars.genlcdex23.setClassString("");
		screenVars.genlcdex24.setClassString("");
		screenVars.genlcdex25.setClassString("");
		screenVars.genlcdex26.setClassString("");
		screenVars.genlcdex27.setClassString("");
		screenVars.genlcdex28.setClassString("");
		screenVars.genlcdex29.setClassString("");
		screenVars.genlcdex30.setClassString("");
	}

/**
 * Clear all the variables in Sm601screen
 */
	public static void clear(VarModel pv) {
		Sm601ScreenVars screenVars = (Sm601ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.genlcdex03.clear();
		screenVars.genlcdex04.clear();
		screenVars.genlcdex02.clear();
		screenVars.genlcdex01.clear();
		screenVars.genlcdex05.clear();
		screenVars.genlcdex06.clear();
		screenVars.genlcdex09.clear();
		screenVars.genlcdex10.clear();
		screenVars.genlcdex07.clear();
		screenVars.genlcdex08.clear();
		screenVars.genlcdex11.clear();
		screenVars.genlcdex12.clear();
		screenVars.genlcdex13.clear();
		screenVars.genlcdex14.clear();
		screenVars.genlcdex15.clear();
		screenVars.genlcdex16.clear();
		screenVars.genlcdex17.clear();
		screenVars.genlcdex18.clear();
		screenVars.genlcdex19.clear();
		screenVars.genlcdex20.clear();
		screenVars.genlcdex21.clear();
		screenVars.genlcdex22.clear();
		screenVars.genlcdex23.clear();
		screenVars.genlcdex24.clear();
		screenVars.genlcdex25.clear();
		screenVars.genlcdex26.clear();
		screenVars.genlcdex27.clear();
		screenVars.genlcdex28.clear();
		screenVars.genlcdex29.clear();
		screenVars.genlcdex30.clear();
	}
}
