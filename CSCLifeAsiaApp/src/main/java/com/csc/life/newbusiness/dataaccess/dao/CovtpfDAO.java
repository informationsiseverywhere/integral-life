package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;
import java.util.Map;

import com.csc.life.newbusiness.dataaccess.model.Covtpf;

import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface CovtpfDAO extends BaseDAO<Covtpf> {
	//Ilife-3310 by liwei
	public Covtpf getCovtridData(String chdrcoy,String chdrnum,String life,String coverage,String crtable);
	public List<Covtpf> getCovtpfData(String chdrcoy,String chdrnum,String life,String coverage,String rider);
	public List<Covtpf> getCovtpfData(String chdrcoy,String chdrnum,String life,String coverage,String rider, int planSuffix);
	public List<Covtpf> getCovtcovData(String chdrcoy,String chdrnum,String life,String crtable);
	/* ILIFE-3395 STARTS */
	public List<Covtpf> getCovtCovDetails(Covtpf covtpfData);
	// Introduced for ILIFE-3439
	public List<Covtpf> searchCovtRecordByCoyNum(String chdrcoy, String chdrNum);
	public List<Covtpf> searchCovtRecordByCoyNumDescUniquNo(String chdrcoy, String chdrNum);
	public List<Covtpf> getCovtlnbRecord(String chdrcoy, String chdrnum, String life) ;
	public Covtpf getCovtlnbData(String chdrcoy,String chdrnum,String life,String coverage,String rider);
	//ILIFE-6809
	public List<Covtpf> getCovtridData(String chdrcoy, String chdrnum, String life,String coverage);
	public int bulkUpdateCovtData(Map<String, Covtpf> covtUpdateMap);
	public List<Covtpf> getSingpremtype(String coy, String chdrnum); /*ILIFE-8098 */
	public void deleteCovtpfRecord(Covtpf covtpf); //ILIFE-8709
	public List<Covtpf> getCovtlnbRecord(String chdrcoy, String chdrnum,String life, String jlife);
	public int updateCovtData(Covtpf covtpf);
	public int insertCovtData(Covtpf covtpf);
	public int updateCovtpf(Covtpf covtpf);
	public Map<String, List<Covtpf>> searchCovt(String coy, List<String> chdrnumList); //ILB-947
}
