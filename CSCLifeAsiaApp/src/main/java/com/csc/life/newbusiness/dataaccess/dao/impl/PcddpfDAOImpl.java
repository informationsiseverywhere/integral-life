package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.PcddpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class PcddpfDAOImpl extends BaseDAOImpl<Pcddpf> implements PcddpfDAO{

	private static final Logger LOGGER = LoggerFactory.getLogger(PcddpfDAOImpl.class);

	public List<Pcddpf> searchPcddRecordByChdrNum(String chdrcoy, String chdrNum) throws SQLRuntimeException{
		
		StringBuilder sqlPcddSelect = new StringBuilder();
		
		sqlPcddSelect.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, AGNTNUM, SPLITC, SPLITB, VALIDFLAG, ");
		sqlPcddSelect.append("TRANNO, CURRFROM, CURRTO, USER_T, TERMID, TRTM, TRDT ");
		sqlPcddSelect.append("FROM PCDDPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlPcddSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, AGNTNUM ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psPcddSelect = getPrepareStatement(sqlPcddSelect.toString());
		ResultSet sqlPcddRs = null;
		List<Pcddpf> pcddSearchResult = new ArrayList<Pcddpf>();
		try {
			
			psPcddSelect.setString(1, chdrcoy);
			psPcddSelect.setString(2, chdrNum);
			
			sqlPcddRs = executeQuery(psPcddSelect);

			while (sqlPcddRs.next()) {
				
				Pcddpf pcddpf = new Pcddpf();
				
				pcddpf.setUniqueNumber(sqlPcddRs.getInt("UNIQUE_NUMBER"));
				pcddpf.setChdrcoy(sqlPcddRs.getString("CHDRCOY").charAt(0));
				pcddpf.setChdrnum(sqlPcddRs.getString("CHDRNUM"));
				pcddpf.setAgntNum(sqlPcddRs.getString("AGNTNUM"));
				pcddpf.setSplitC(sqlPcddRs.getBigDecimal("SPLITC"));
				pcddpf.setSplitB(sqlPcddRs.getBigDecimal("SPLITB"));
				pcddpf.setValidFlag(sqlPcddRs.getString("VALIDFLAG").charAt(0));
				pcddpf.setTranNo(sqlPcddRs.getInt("TRANNO"));
				pcddpf.setCurrFrom(sqlPcddRs.getInt("CURRFROM"));
				pcddpf.setCurrTo(sqlPcddRs.getInt("CURRTO"));
				pcddpf.setUserT(sqlPcddRs.getInt("USER_T"));
				pcddpf.setTermId(sqlPcddRs.getString("TERMID"));
				pcddpf.setTrtM(sqlPcddRs.getInt("TRTM"));
				pcddpf.setTrtD(sqlPcddRs.getInt("TRDT"));
				
				pcddSearchResult.add(pcddpf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchPcddRecordByChdrNum()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psPcddSelect, sqlPcddRs);
		}
		return pcddSearchResult;
	}
	
	  public Map<String, List<Pcddpf>> searchPcddRecordByChdrnum(List<String> chdrnumList){
	    	
			StringBuilder sqlSelect1 = new StringBuilder();
			sqlSelect1.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,AGNTNUM,SPLITC,SPLITB,VALIDFLAG,TRANNO,CURRFROM,CURRTO,TERMID,TRTM,TRDT ");
		    sqlSelect1.append("FROM PCDDPF WHERE VALIDFLAG = '1' AND ");
		    sqlSelect1.append(getSqlInStr("CHDRNUM", chdrnumList));
		    sqlSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, AGNTNUM ASC, UNIQUE_NUMBER DESC ");

		    PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
		    ResultSet sqlrs = null;
		    Map<String, List<Pcddpf>> resultMap = new HashMap<>();
		    
		    try {
		    	sqlrs = executeQuery(psSelect);
		
		        while (sqlrs.next()) {
					Pcddpf pcddpf = new Pcddpf();
					pcddpf.setChdrcoy(sqlrs.getString("chdrcoy").charAt(0));
					pcddpf.setChdrnum(sqlrs.getString("chdrnum"));
					pcddpf.setAgntNum(sqlrs.getString("agntnum"));
					pcddpf.setSplitC(sqlrs.getBigDecimal("splitc"));
					pcddpf.setSplitB(sqlrs.getBigDecimal("splitb"));
					pcddpf.setValidFlag(sqlrs.getString("validflag").charAt(0));
					pcddpf.setTranNo(sqlrs.getInt("tranno"));
					pcddpf.setCurrFrom(sqlrs.getInt("currfrom"));
					pcddpf.setCurrTo(sqlrs.getInt("currto"));
					pcddpf.setTermId(sqlrs.getString("termid"));
					pcddpf.setTrtM(sqlrs.getInt("trtm"));
					pcddpf.setTrtD(sqlrs.getInt("trdt"));
					pcddpf.setUniqueNumber(sqlrs.getLong("Unique_number"));
					
					if (resultMap.containsKey(pcddpf.getChdrnum())) {
						resultMap.get(pcddpf.getChdrnum()).add(pcddpf);
					} else {
						List<Pcddpf> pcddpfList = new ArrayList<>();
						pcddpfList.add(pcddpf);
						resultMap.put(pcddpf.getChdrnum(), pcddpfList);
					}
		        }
		    } catch (SQLException e) {
		        LOGGER.error("searchPcddRecordByChdrnum()", e);//IJTI-1561
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(psSelect, sqlrs);
		    }
			return resultMap;
		}
}
