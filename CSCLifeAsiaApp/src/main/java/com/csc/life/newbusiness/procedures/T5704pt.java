/*
 * File: T5704pt.java
 * Date: 30 August 2009 2:26:42
 * Author: Quipoz Limited
 * 
 * Class transformed from T5704PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.T5704rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T5704.
*
*
*
***********************************************************************
* </pre>
*/
public class T5704pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Fast Track Component Rules               S5704");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(72);
	private FixedLengthStringData filler9 = new FixedLengthStringData(18).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Min. Base Unit:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(11, 2).isAPartOf(wsaaPrtLine004, 18).setPattern("ZZZZZZZZZ.ZZ");
	private FixedLengthStringData filler10 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 30, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine004, 38, FILLER).init("Min. Base Increment:");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(11, 2).isAPartOf(wsaaPrtLine004, 60).setPattern("ZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(46);
	private FixedLengthStringData filler12 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler13 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine005, 5, FILLER).init("Component Type                Sum Assured");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(57);
	private FixedLengthStringData filler14 = new FixedLengthStringData(57).isAPartOf(wsaaPrtLine006, 0, FILLER).init("    Coverage   Rider    % of Base     Based     Base Unit");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(66);
	private FixedLengthStringData filler15 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 6);
	private FixedLengthStringData filler16 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine007, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 15);
	private FixedLengthStringData filler17 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine007, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 40);
	private FixedLengthStringData filler19 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine007, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(66);
	private FixedLengthStringData filler20 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 6);
	private FixedLengthStringData filler21 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine008, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 15);
	private FixedLengthStringData filler22 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine008, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 40);
	private FixedLengthStringData filler24 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine008, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(66);
	private FixedLengthStringData filler25 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 6);
	private FixedLengthStringData filler26 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 15);
	private FixedLengthStringData filler27 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine009, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 40);
	private FixedLengthStringData filler29 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine009, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(66);
	private FixedLengthStringData filler30 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 6);
	private FixedLengthStringData filler31 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 15);
	private FixedLengthStringData filler32 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine010, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 40);
	private FixedLengthStringData filler34 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine010, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(66);
	private FixedLengthStringData filler35 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 6);
	private FixedLengthStringData filler36 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine011, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 15);
	private FixedLengthStringData filler37 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine011, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 40);
	private FixedLengthStringData filler39 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine011, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(66);
	private FixedLengthStringData filler40 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 6);
	private FixedLengthStringData filler41 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine012, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 15);
	private FixedLengthStringData filler42 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine012, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 40);
	private FixedLengthStringData filler44 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine012, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(66);
	private FixedLengthStringData filler45 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 6);
	private FixedLengthStringData filler46 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine013, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 15);
	private FixedLengthStringData filler47 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine013, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 40);
	private FixedLengthStringData filler49 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine013, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(66);
	private FixedLengthStringData filler50 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 6);
	private FixedLengthStringData filler51 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine014, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 15);
	private FixedLengthStringData filler52 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine014, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine014, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 40);
	private FixedLengthStringData filler54 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine014, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(66);
	private FixedLengthStringData filler55 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 6);
	private FixedLengthStringData filler56 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine015, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 15);
	private FixedLengthStringData filler57 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine015, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine015, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 40);
	private FixedLengthStringData filler59 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine015, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine015, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(66);
	private FixedLengthStringData filler60 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 6);
	private FixedLengthStringData filler61 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine016, 10, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine016, 15);
	private FixedLengthStringData filler62 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine016, 19, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(6, 2).isAPartOf(wsaaPrtLine016, 25).setPattern("ZZZZ.ZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 32, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 40);
	private FixedLengthStringData filler64 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine016, 41, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(17, 2).isAPartOf(wsaaPrtLine016, 48).setPattern("ZZZZZZZZZZZZZZZ.ZZ");

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(76);
	private FixedLengthStringData filler65 = new FixedLengthStringData(68).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" F1=Help   F3=Exit   F12=Cancel                  Continuation Item:");
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine017, 68);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5704rec t5704rec = new T5704rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T5704pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t5704rec.t5704Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo013.set(t5704rec.unitpkg01);
		fieldNo012.set(t5704rec.sinsbsd01);
		fieldNo007.set(t5704rec.mbaseunt);
		fieldNo008.set(t5704rec.mbaseinc);
		fieldNo009.set(t5704rec.crtable01);
		fieldNo010.set(t5704rec.rtable01);
		fieldNo011.set(t5704rec.unitpct01);
		fieldNo014.set(t5704rec.crtable02);
		fieldNo015.set(t5704rec.rtable02);
		fieldNo016.set(t5704rec.unitpct02);
		fieldNo019.set(t5704rec.crtable03);
		fieldNo020.set(t5704rec.rtable03);
		fieldNo021.set(t5704rec.unitpct03);
		fieldNo024.set(t5704rec.crtable04);
		fieldNo025.set(t5704rec.rtable04);
		fieldNo026.set(t5704rec.unitpct04);
		fieldNo029.set(t5704rec.crtable05);
		fieldNo030.set(t5704rec.rtable05);
		fieldNo031.set(t5704rec.unitpct05);
		fieldNo034.set(t5704rec.crtable06);
		fieldNo035.set(t5704rec.rtable06);
		fieldNo036.set(t5704rec.unitpct06);
		fieldNo039.set(t5704rec.crtable07);
		fieldNo040.set(t5704rec.rtable07);
		fieldNo041.set(t5704rec.unitpct07);
		fieldNo044.set(t5704rec.crtable08);
		fieldNo045.set(t5704rec.rtable08);
		fieldNo046.set(t5704rec.unitpct08);
		fieldNo049.set(t5704rec.crtable09);
		fieldNo050.set(t5704rec.rtable09);
		fieldNo051.set(t5704rec.unitpct09);
		fieldNo054.set(t5704rec.crtable10);
		fieldNo055.set(t5704rec.rtable10);
		fieldNo056.set(t5704rec.unitpct10);
		fieldNo059.set(t5704rec.contitem);
		fieldNo017.set(t5704rec.sinsbsd02);
		fieldNo022.set(t5704rec.sinsbsd03);
		fieldNo027.set(t5704rec.sinsbsd04);
		fieldNo032.set(t5704rec.sinsbsd05);
		fieldNo037.set(t5704rec.sinsbsd06);
		fieldNo042.set(t5704rec.sinsbsd07);
		fieldNo047.set(t5704rec.sinsbsd08);
		fieldNo052.set(t5704rec.sinsbsd09);
		fieldNo057.set(t5704rec.sinsbsd10);
		fieldNo018.set(t5704rec.unitpkg02);
		fieldNo023.set(t5704rec.unitpkg03);
		fieldNo028.set(t5704rec.unitpkg04);
		fieldNo033.set(t5704rec.unitpkg05);
		fieldNo038.set(t5704rec.unitpkg06);
		fieldNo043.set(t5704rec.unitpkg07);
		fieldNo048.set(t5704rec.unitpkg08);
		fieldNo053.set(t5704rec.unitpkg09);
		fieldNo058.set(t5704rec.unitpkg10);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
