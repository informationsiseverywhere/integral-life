package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:46
 * Description:
 * Copybook name: HXCLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hxclkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hxclFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hxclKey = new FixedLengthStringData(64).isAPartOf(hxclFileKey, 0, REDEFINE);
  	public FixedLengthStringData hxclChdrcoy = new FixedLengthStringData(1).isAPartOf(hxclKey, 0);
  	public FixedLengthStringData hxclChdrnum = new FixedLengthStringData(8).isAPartOf(hxclKey, 1);
  	public PackedDecimalData hxclFupno = new PackedDecimalData(2, 0).isAPartOf(hxclKey, 9);
  	public PackedDecimalData hxclHxclseqno = new PackedDecimalData(2, 0).isAPartOf(hxclKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(51).isAPartOf(hxclKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hxclFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hxclFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}