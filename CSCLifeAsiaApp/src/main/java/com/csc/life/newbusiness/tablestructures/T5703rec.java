package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:08
 * Description:
 * Copybook name: T5703REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5703rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5703Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData virtFundSplitMethod = new FixedLengthStringData(4).isAPartOf(t5703Rec, 0);
  	public ZonedDecimalData premCessDate = new ZonedDecimalData(8, 0).isAPartOf(t5703Rec, 4);
  	public ZonedDecimalData premCessAge = new ZonedDecimalData(3, 0).isAPartOf(t5703Rec, 12);
  	public ZonedDecimalData premCessTerm = new ZonedDecimalData(3, 0).isAPartOf(t5703Rec, 15);
  	public ZonedDecimalData riskCessDate = new ZonedDecimalData(8, 0).isAPartOf(t5703Rec, 18);
  	public ZonedDecimalData riskCessAge = new ZonedDecimalData(3, 0).isAPartOf(t5703Rec, 26);
  	public ZonedDecimalData riskCessTerm = new ZonedDecimalData(3, 0).isAPartOf(t5703Rec, 29);
  	public FixedLengthStringData taxind = new FixedLengthStringData(1).isAPartOf(t5703Rec, 32);
  	public FixedLengthStringData filler = new FixedLengthStringData(467).isAPartOf(t5703Rec, 33, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5703Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5703Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}