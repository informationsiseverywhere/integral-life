package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class Sr626screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {22, 17, 4, 23, 5, 18, 24, 15, 16, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 7;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {4, 5, 15, 14, 1, 2, 11, 12}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {15, 20, 2, 77}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr626ScreenVars sv = (Sr626ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr626screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr626screensfl, 
			sv.Sr626screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr626ScreenVars sv = (Sr626ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr626screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr626ScreenVars sv = (Sr626ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr626screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr626screensflWritten.gt(0))
		{
			sv.sr626screensfl.setCurrentIndex(0);
			sv.Sr626screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr626ScreenVars sv = (Sr626ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr626screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr626ScreenVars screenVars = (Sr626ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hseqno.setFieldName("hseqno");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.textone.setFieldName("textone");
				screenVars.reasoncd.setFieldName("reasoncd");
				screenVars.datefrmDisp.setFieldName("datefrmDisp");
				screenVars.datetoDisp.setFieldName("datetoDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.hseqno.set(dm.getField("hseqno"));
			screenVars.clntnum.set(dm.getField("clntnum"));
			screenVars.textone.set(dm.getField("textone"));
			screenVars.reasoncd.set(dm.getField("reasoncd"));
			screenVars.datefrmDisp.set(dm.getField("datefrmDisp"));
			screenVars.datetoDisp.set(dm.getField("datetoDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr626ScreenVars screenVars = (Sr626ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.hseqno.setFieldName("hseqno");
				screenVars.clntnum.setFieldName("clntnum");
				screenVars.textone.setFieldName("textone");
				screenVars.reasoncd.setFieldName("reasoncd");
				screenVars.datefrmDisp.setFieldName("datefrmDisp");
				screenVars.datetoDisp.setFieldName("datetoDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("hseqno").set(screenVars.hseqno);
			dm.getField("clntnum").set(screenVars.clntnum);
			dm.getField("textone").set(screenVars.textone);
			dm.getField("reasoncd").set(screenVars.reasoncd);
			dm.getField("datefrmDisp").set(screenVars.datefrmDisp);
			dm.getField("datetoDisp").set(screenVars.datetoDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr626screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr626ScreenVars screenVars = (Sr626ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.hseqno.clearFormatting();
		screenVars.clntnum.clearFormatting();
		screenVars.textone.clearFormatting();
		screenVars.reasoncd.clearFormatting();
		screenVars.datefrmDisp.clearFormatting();
		screenVars.datetoDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr626ScreenVars screenVars = (Sr626ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.hseqno.setClassString("");
		screenVars.clntnum.setClassString("");
		screenVars.textone.setClassString("");
		screenVars.reasoncd.setClassString("");
		screenVars.datefrmDisp.setClassString("");
		screenVars.datetoDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr626screensfl
 */
	public static void clear(VarModel pv) {
		Sr626ScreenVars screenVars = (Sr626ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.hseqno.clear();
		screenVars.clntnum.clear();
		screenVars.textone.clear();
		screenVars.reasoncd.clear();
		screenVars.datefrmDisp.clear();
		screenVars.datefrm.clear();
		screenVars.datetoDisp.clear();
		screenVars.dateto.clear();
	}
}
