package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:07
 * Description:
 * Copybook name: LIFEKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifekey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifeFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData lifeKey = new FixedLengthStringData(64).isAPartOf(lifeFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifeChdrcoy = new FixedLengthStringData(1).isAPartOf(lifeKey, 0);
  	public FixedLengthStringData lifeChdrnum = new FixedLengthStringData(8).isAPartOf(lifeKey, 1);
  	public FixedLengthStringData lifeLife = new FixedLengthStringData(2).isAPartOf(lifeKey, 9);
  	public FixedLengthStringData lifeJlife = new FixedLengthStringData(2).isAPartOf(lifeKey, 11);
  	public PackedDecimalData lifeCurrfrom = new PackedDecimalData(8, 0).isAPartOf(lifeKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(lifeKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifeFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifeFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}