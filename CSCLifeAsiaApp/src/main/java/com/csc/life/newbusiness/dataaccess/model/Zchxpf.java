package com.csc.life.newbusiness.dataaccess.model;


public class Zchxpf {
	
	private String chdrpfx; 
	private String chdrcoy; 
	private String chdrnum; 
	private String statcode;
	private String pstcde;
	public String getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(String chdrpfx) {
		this.chdrpfx = chdrpfx;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getStatcode() {
		return statcode;
	}
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	public String getPstcde() {
		return pstcde;
	}
	public void setPstcde(String pstcde) {
		this.pstcde = pstcde;
	}
	
	

}
