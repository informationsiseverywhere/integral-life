package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:06
 * Description:
 * Copybook name: LIFEAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifeafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifeafiFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifeafiKey = new FixedLengthStringData(256).isAPartOf(lifeafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifeafiChdrcoy = new FixedLengthStringData(1).isAPartOf(lifeafiKey, 0);
  	public FixedLengthStringData lifeafiChdrnum = new FixedLengthStringData(8).isAPartOf(lifeafiKey, 1);
  	public FixedLengthStringData lifeafiLife = new FixedLengthStringData(2).isAPartOf(lifeafiKey, 9);
  	public FixedLengthStringData lifeafiJlife = new FixedLengthStringData(2).isAPartOf(lifeafiKey, 11);
  	public PackedDecimalData lifeafiTranno = new PackedDecimalData(5, 0).isAPartOf(lifeafiKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(lifeafiKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifeafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifeafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}