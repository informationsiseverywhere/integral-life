package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5074screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
//	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5074ScreenVars sv = (S5074ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.S5074screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5074ScreenVars screenVars = (S5074ScreenVars)pv;
		/*screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.ctypedes.setClassString("");
		screenVars.statdsc.setClassString("");
		screenVars.premStatDesc.setClassString("");
		screenVars.ownersel.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.lassured.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.conprosal.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.billcdDisp.setClassString("");
		screenVars.polinc.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.billcurr.setClassString("");
		screenVars.register.setClassString("");
		screenVars.srcebus.setClassString("");
		screenVars.reptype.setClassString("");
		screenVars.lrepnum.setClassString("");
		screenVars.cntbranch.setClassString("");
		screenVars.sbrdesc.setClassString("");
		screenVars.agntsel.setClassString("");
		screenVars.agentname.setClassString("");
		screenVars.campaign.setClassString("");
		screenVars.stcal.setClassString("");
		screenVars.stcbl.setClassString("");
		screenVars.stccl.setClassString("");
		screenVars.stcdl.setClassString("");
		screenVars.stcel.setClassString("");
		screenVars.hcurrcd.setClassString("");
		screenVars.instPrem.setClassString("");
		screenVars.cntsusp.setClassString("");
		screenVars.cntfee.setClassString("");
		screenVars.tolerance.setClassString("");
		screenVars.totlprm.setClassString("");
		screenVars.totamnt.setClassString("");
		screenVars.prmdepst.setClassString("");
		screenVars.taxamt.setClassString("");*/
		ScreenRecord.setClassStringFormatting(pv);	
	
	}

/**
 * Clear all the variables in S5074screen
 */
	public static void clear(VarModel pv) {
		S5074ScreenVars screenVars = (S5074ScreenVars) pv;
		/*screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.ctypedes.clear();
		screenVars.statdsc.clear();
		screenVars.premStatDesc.clear();
		screenVars.ownersel.clear();
		screenVars.ownername.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.lassured.clear();
		screenVars.billfreq.clear();
		screenVars.conprosal.clear();
		screenVars.mop.clear();
		screenVars.billcdDisp.clear();
		screenVars.billcd.clear();
		screenVars.polinc.clear();
		screenVars.cntcurr.clear();
		screenVars.billcurr.clear();
		screenVars.register.clear();
		screenVars.srcebus.clear();
		screenVars.reptype.clear();
		screenVars.lrepnum.clear();
		screenVars.cntbranch.clear();
		screenVars.sbrdesc.clear();
		screenVars.agntsel.clear();
		screenVars.agentname.clear();
		screenVars.campaign.clear();
		screenVars.stcal.clear();
		screenVars.stcbl.clear();
		screenVars.stccl.clear();
		screenVars.stcdl.clear();
		screenVars.stcel.clear();
		screenVars.hcurrcd.clear();
		screenVars.instPrem.clear();
		screenVars.cntsusp.clear();
		screenVars.cntfee.clear();
		screenVars.tolerance.clear();
		screenVars.totlprm.clear();
		screenVars.totamnt.clear();
		screenVars.prmdepst.clear();
		screenVars.taxamt.clear();*/

		ScreenRecord.clear(pv);
	}
}
