package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:36
 * Description:
 * Copybook name: PAYRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Payrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData payrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData payrKey = new FixedLengthStringData(64).isAPartOf(payrFileKey, 0, REDEFINE);
  	public FixedLengthStringData payrChdrcoy = new FixedLengthStringData(1).isAPartOf(payrKey, 0);
  	public FixedLengthStringData payrChdrnum = new FixedLengthStringData(8).isAPartOf(payrKey, 1);
  	public PackedDecimalData payrPayrseqno = new PackedDecimalData(1, 0).isAPartOf(payrKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(54).isAPartOf(payrKey, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(payrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		payrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}