package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:37
 * Description:
 * Copybook name: TH609REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th609rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th609Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData fupcdes = new FixedLengthStringData(4).isAPartOf(th609Rec, 0);
  	public FixedLengthStringData indic = new FixedLengthStringData(1).isAPartOf(th609Rec, 4);
  	public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(th609Rec, 5);
  	public ZonedDecimalData prcnt = new ZonedDecimalData(5, 2).isAPartOf(th609Rec, 6);
  	public FixedLengthStringData taxind = new FixedLengthStringData(1).isAPartOf(th609Rec, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(488).isAPartOf(th609Rec, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th609Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th609Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}