
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.ArrayList;
import java.util.List;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import com.csc.fsu.clients.dataaccess.dao.AnsopfDAO;
import com.csc.fsu.clients.dataaccess.model.Ansopf;
import com.csc.life.newbusiness.recordstructures.Antisoclkey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;


public class Antisocl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private static final String wsaaSubrname = "ANTISOCL";
	
	private FixedLengthStringData surname = new FixedLengthStringData(120);
	private FixedLengthStringData givename = new FixedLengthStringData(120);
	private FixedLengthStringData name = new FixedLengthStringData(240);
	
	
		/* ERRORS */

	private static final String jl41 = "JL41";
	private static final String jl42 = "JL42";
	private static final String jl62 = "JL62";
	private static final String jl63 = "JL63";

	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Antisoclkey antisoclkey = new Antisoclkey();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private AnsopfDAO ansopfDAO = getApplicationContext().getBean("AnsopfDAO", AnsopfDAO.class);
	private Ansopf ansopf = null;
	private List<Ansopf> ansopflist = new ArrayList<Ansopf>();
	
/**
 * Contains all possible labels used by goTo action.
 */
	
	public Antisocl() {
		super();
	}

	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
public void mainline(Object... parmArray)
	{	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 1);
		antisoclkey.antisocialKey = convertAndSetParam(antisoclkey.antisocialKey, parmArray, 0);
		try {
			main1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void main1000()
	{
		start1000();
	
		exit090();
	}

protected void start1000()
	{	
		antisoclkey.statuz.set("****");
		surname.set(antisoclkey.kjSName.toString().trim().replace(".", "").replace("-", ""));
		givename.set(antisoclkey.kjGName.toString().trim().replace(".", "").replace("-", ""));
		
		name.set(surname.concat(givename).trim());
		
		if(isNE(name, SPACES))
		{
			
				ansopflist = ansopfDAO.getRecord(name.toString());
			
				if(ansopflist.size()>0){
					
					if(ansopflist.size()>0 && ansopflist.size()<2){
						if(isEQ(antisoclkey.clntype,"P"))
						{
							antisoclkey.statuz.set(jl41);
							return ;	
						}
						else if(isEQ(antisoclkey.clntype,"C"))
						{
							antisoclkey.statuz.set(jl42);
							return ;
						}
					}
					else if(ansopflist.size()>1)
					{
						if(isEQ(antisoclkey.clntype,"P"))
						{
							antisoclkey.statuz.set(jl62);
							return ;	
						}
						else if(isEQ(antisoclkey.clntype,"C"))
						{
							antisoclkey.statuz.set(jl63);
							return ;
						}
					}
				}
	
			else
			{
				antisoclkey.statuz.set("****");
				return ;
			}
		}
			
		
	}


protected void exit090()
{
	exitProgram();
}

protected void fatalError600()
	{
	/*FATAL*/
	syserrrec.subrname.set(wsaaSubrname);
	if (isNE(syserrrec.statuz, SPACES)
	|| isNE(syserrrec.syserrStatuz, SPACES)) {
		syserrrec.syserrType.set("1");
	}
	else {
		syserrrec.syserrType.set("2");
	}
	callProgram(Syserr.class, syserrrec.syserrRec);
	/*EXIT-PROGRAM*/
	exitProgram();
	}

}


