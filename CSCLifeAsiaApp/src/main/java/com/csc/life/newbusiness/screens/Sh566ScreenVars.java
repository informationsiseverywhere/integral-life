package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH566
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh566ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(143);
	public FixedLengthStringData dataFields = new FixedLengthStringData(47).isAPartOf(dataArea, 0);
	public ZonedDecimalData cnt = DD.cnt.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData ztranind = DD.ztranind.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 47);
	public FixedLengthStringData cntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ztranindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 71);
	public FixedLengthStringData[] cntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ztranindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh566screenWritten = new LongData(0);
	public LongData Sh566protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh566ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, ztranind, cnt};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, ztranindOut, cntOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, ztranindErr, cntErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh566screen.class;
		protectRecord = Sh566protect.class;
	}

}
