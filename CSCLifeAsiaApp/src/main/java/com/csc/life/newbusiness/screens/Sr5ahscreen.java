package com.csc.life.newbusiness.screens;


	import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
	import com.quipoz.COBOLFramework.util.COBOLAppVars;
	import com.quipoz.framework.datatype.Indicator;
	import com.quipoz.framework.util.QPUtilities;
	import com.quipoz.framework.util.VarModel;

	public class Sr5ahscreen extends ScreenRecord { 

		public static final String ROUTINE = QPUtilities.getThisClass();
		public static final boolean overlay = false;
		public static final int[] pfInds = new int[] {1, 2, 3, 4, 13, 15, 16, 17, 18, 21, 22, 23, 24}; 
		public static RecInfo lrec = new RecInfo();
		static {
			lrec.recordName = ROUTINE + "Written";
			COBOLAppVars.recordSizes.put(ROUTINE, new int[] {20, 23, 2, 52}); 
		}

	/**
	 * Writes a record to the screen.
	 * @param errorInd - will be set on if an error occurs
	 * @param noRecordFoundInd - will be set on if no changed record is found
	 */
		public static void write(COBOLAppVars av, VarModel pv,
			Indicator ind2, Indicator ind3) {
			Sr5ahScreenVars sv = (Sr5ahScreenVars) pv;
			clearInds(av, pfInds);
			write(lrec, sv.Sr5ahscreenWritten, null, av, null, ind2, ind3);
		}

		public static void read(Indicator ind2, Indicator ind3) {}

		public static void clearClassString(VarModel pv) {
			Sr5ahScreenVars screenVars = (Sr5ahScreenVars)pv;
		
		}

	/**
	 * Clear all the variables in Sr5ahscreen
	 */
		public static void clear(VarModel pv) {
			Sr5ahScreenVars screenVars = (Sr5ahScreenVars) pv;
			
			
		}

}
