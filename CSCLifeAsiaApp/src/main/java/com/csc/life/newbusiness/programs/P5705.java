/*
 * File: P5705.java
 * Date: 30 August 2009 0:35:04
 * Author: Quipoz Limited
 * 
 * Class transformed from P5705.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.procedures.T5705pt;
import com.csc.life.newbusiness.screens.S5705ScreenVars;
import com.csc.life.newbusiness.tablestructures.T5705rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5705 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5705");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5705rec t5705rec = new T5705rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5705ScreenVars sv = ScreenProgram.getScreenVars( S5705ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		preExit, 
		exit2090, 
		other3080, 
		exit3090
	}

	public P5705() {
		super();
		screenVars = sv;
		new ScreenModel("S5705", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				}
				case generalArea1045: {
					generalArea1045();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itemIO.setDataKey(wsspsmart.itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setDescpfx(itemIO.getItempfx());
		descIO.setDesccoy(itemIO.getItemcoy());
		descIO.setDesctabl(itemIO.getItemtabl());
		descIO.setDescitem(itemIO.getItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itemIO.getItemcoy());
		sv.tabl.set(itemIO.getItemtabl());
		sv.item.set(itemIO.getItemitem());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5705rec.t5705Rec.set(itemIO.getGenarea());
		if (isNE(itemIO.getGenarea(),SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5705rec.incomeSeqNo.set(ZERO);
		t5705rec.plansuff.set(ZERO);
		t5705rec.rcdate.set(varcom.maxdate);
	}

protected void generalArea1045()
	{
		sv.agntsel.set(t5705rec.agntsel);
		sv.billcurr.set(t5705rec.billcurr);
		sv.billfreq.set(t5705rec.billfreq);
		sv.campaign.set(t5705rec.campaign);
		sv.cntbranch.set(t5705rec.cntbranch);
		sv.cntcurr.set(t5705rec.cntcurr);
		sv.incomeSeqNo.set(t5705rec.incomeSeqNo);
		if (isEQ(itemIO.getItmfrm(),0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itemIO.getItmfrm());
		}
		if (isEQ(itemIO.getItmto(),0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itemIO.getItmto());
		}
		sv.lrepnum.set(t5705rec.lrepnum);
		sv.mop.set(t5705rec.mop);
		sv.mortality.set(t5705rec.mortality);
		sv.plansuff.set(t5705rec.plansuff);
		sv.pursuits.set(t5705rec.pursuits);
		sv.rcdate.set(t5705rec.rcdate);
		sv.register.set(t5705rec.register);
		sv.reptype.set(t5705rec.reptype);
		sv.selection.set(t5705rec.selection);
		sv.smkrqd.set(t5705rec.smkrqd);
		sv.srcebus.set(t5705rec.srcebus);
		sv.stcal.set(t5705rec.stcal);
		sv.stcbl.set(t5705rec.stcbl);
		sv.stccl.set(t5705rec.stccl);
		sv.stcdl.set(t5705rec.stcdl);
		sv.stcel.set(t5705rec.stcel);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			scrnparams.function.set(varcom.prot);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
				}
				case exit2090: {
					exit2090();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				}
				case other3080: {
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag,"C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itemIO.setFunction(varcom.readh);
		itemIO.setDataKey(wsspsmart.itemkey);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itemIO.setTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itemIO.setTableprog(wsaaProg);
		itemIO.setGenarea(t5705rec.t5705Rec);
		itemIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.agntsel,t5705rec.agntsel)) {
			t5705rec.agntsel.set(sv.agntsel);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.billcurr,t5705rec.billcurr)) {
			t5705rec.billcurr.set(sv.billcurr);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.billfreq,t5705rec.billfreq)) {
			t5705rec.billfreq.set(sv.billfreq);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.campaign,t5705rec.campaign)) {
			t5705rec.campaign.set(sv.campaign);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.cntbranch,t5705rec.cntbranch)) {
			t5705rec.cntbranch.set(sv.cntbranch);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.cntcurr,t5705rec.cntcurr)) {
			t5705rec.cntcurr.set(sv.cntcurr);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.incomeSeqNo,t5705rec.incomeSeqNo)) {
			t5705rec.incomeSeqNo.set(sv.incomeSeqNo);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm,itemIO.getItmfrm())) {
			itemIO.setItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto,itemIO.getItmto())) {
			itemIO.setItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.lrepnum,t5705rec.lrepnum)) {
			t5705rec.lrepnum.set(sv.lrepnum);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.mop,t5705rec.mop)) {
			t5705rec.mop.set(sv.mop);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.mortality,t5705rec.mortality)) {
			t5705rec.mortality.set(sv.mortality);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.plansuff,t5705rec.plansuff)) {
			t5705rec.plansuff.set(sv.plansuff);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.pursuits,t5705rec.pursuits)) {
			t5705rec.pursuits.set(sv.pursuits);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.rcdate,t5705rec.rcdate)) {
			t5705rec.rcdate.set(sv.rcdate);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.register,t5705rec.register)) {
			t5705rec.register.set(sv.register);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.reptype,t5705rec.reptype)) {
			t5705rec.reptype.set(sv.reptype);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.selection,t5705rec.selection)) {
			t5705rec.selection.set(sv.selection);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.smkrqd,t5705rec.smkrqd)) {
			t5705rec.smkrqd.set(sv.smkrqd);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.srcebus,t5705rec.srcebus)) {
			t5705rec.srcebus.set(sv.srcebus);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.stcal,t5705rec.stcal)) {
			t5705rec.stcal.set(sv.stcal);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.stcbl,t5705rec.stcbl)) {
			t5705rec.stcbl.set(sv.stcbl);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.stccl,t5705rec.stccl)) {
			t5705rec.stccl.set(sv.stccl);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.stcdl,t5705rec.stcdl)) {
			t5705rec.stcdl.set(sv.stcdl);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.stcel,t5705rec.stcel)) {
			t5705rec.stcel.set(sv.stcel);
			wsaaUpdateFlag = "Y";
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5705pt.class, wsaaTablistrec);
		/*EXIT*/
	}
}
