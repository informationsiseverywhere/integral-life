/*
 * File: Pm510.java
 * Date: 30 August 2009 1:13:45
 * Author: Quipoz Limited
 * 
 * Class transformed from PM510.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.newbusiness.recordstructures.Pm510par;
import com.csc.life.newbusiness.screens.Sm510ScreenVars;
import com.csc.smart.dataaccess.BppdTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.tablestructures.T1698rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This is a parameter prompt program that captures the accounting
*  month and year during which the contracts issued be picked up
*  for the report. As the contracts go by issurance date, the
*  accounting month and year will be converted to a date range based
*  on the setting in T1698 and will be shown on the screen. The
*  contract's first date will be compared against the date range
*  for selection.
*
***********************************************************************
* </pre>
*/
public class Pm510 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PM510");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaFound = "";
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaMonthTo = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaYearTo = new ZonedDecimalData(4, 0).setUnsigned();
	private String e012 = "E012";
		/* TABLES */
	private String t1698 = "T1698";
		/* FORMATS */
	private String bscdrec = "BSCDREC";
	private String bsscrec = "BSSCREC";
	private String buparec = "BUPAREC";
	private String bppdrec = "BPPDREC";
	private String itemrec = "ITEMREC";
		/*Parameter Prompt Definition - Multi-thre*/
	private BppdTableDAM bppdIO = new BppdTableDAM();
		/*Schedule Definition - Multi Thread Batch*/
	private BscdTableDAM bscdIO = new BscdTableDAM();
		/*Schedule File - Multi-Thread Batch*/
	private BsscTableDAM bsscIO = new BsscTableDAM();
		/*User Parameter Area - Multi-Thread Batch*/
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Pm510par pm510par = new Pm510par();
	private T1698rec t1698rec = new T1698rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sm510ScreenVars sv = ScreenProgram.getScreenVars( Sm510ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		loop2100, 
		loop2200
	}

	public Pm510() {
		super();
		screenVars = sv;
		new ScreenModel("Sm510", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		bsscIO.setFormat(bsscrec);
		bsscIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bsscIO);
		if (isNE(bsscIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bsscIO.getParams());
			fatalError600();
		}
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		bppdIO.setFormat(bppdrec);
		bppdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bppdIO);
		if (isNE(bppdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bppdIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.datefrm.set(varcom.maxdate);
		sv.dateto.set(varcom.maxdate);
		sv.mnth.set(ZERO);
		sv.year.set(ZERO);
		sv.scheduleName.set(bsscIO.getScheduleName());
		sv.scheduleNumber.set(bsscIO.getScheduleNumber());
		sv.effdate.set(bsscIO.getEffectiveDate());
		sv.acctmonth.set(bsscIO.getAcctMonth());
		sv.acctyear.set(bsscIO.getAcctYear());
		sv.jobq.set(bscdIO.getJobq());
		sv.bcompany.set(bppdIO.getCompany());
		sv.bbranch.set(bsscIO.getInitBranch());
		sv.mnth.set(bsscIO.getAcctMonth());
		sv.year.set(bsscIO.getAcctYear());
		getEffectiveFrom2100();
		getEffectiveTo2200();
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set(SPACES);
		}
		/*VALIDATE*/
		getEffectiveFrom2100();
		getEffectiveTo2200();
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void getEffectiveFrom2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2100();
				}
				case loop2100: {
					loop2100();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2100()
	{
		wsaaFound = "N";
		wsaaCompany.set(bppdIO.getCompany());
	}

protected void loop2100()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bppdIO.getCompany());
		itemIO.setItemtabl(t1698);
		itemIO.setItemitem(wsaaCompany);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1698rec.t1698Rec.set(itemIO.getGenarea());
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,13)
		|| isEQ(wsaaFound,"Y")); wsaaIndex.add(1)){
			if (isEQ(t1698rec.acctyr[wsaaIndex.toInt()],sv.year)
			&& isEQ(t1698rec.acctmnth[wsaaIndex.toInt()],sv.mnth)) {
				wsaaFound = "Y";
			}
		}
		if (isNE(wsaaFound,"Y")) {
			if (isNE(t1698rec.contitem,SPACES)) {
				wsaaCompany.set(t1698rec.contitem);
				goTo(GotoLabel.loop2100);
			}
			else {
				sv.mnthErr.set(e012);
			}
		}
		wsaaIndex.add(-1);
		sv.datefrm.set(t1698rec.frmdate[wsaaIndex.toInt()]);
	}

protected void getEffectiveTo2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2200();
				}
				case loop2200: {
					loop2200();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2200()
	{
		wsaaFound = "N";
		wsaaYearTo.set(sv.year);
		compute(wsaaMonthTo, 0).set(add(sv.mnth,1));
		if (isGT(wsaaMonthTo,12)) {
			wsaaYearTo.add(1);
			wsaaMonthTo.set(1);
		}
		wsaaCompany.set(bppdIO.getCompany());
	}

protected void loop2200()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bppdIO.getCompany());
		itemIO.setItemtabl(t1698);
		itemIO.setItemitem(wsaaCompany);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t1698rec.t1698Rec.set(itemIO.getGenarea());
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,13)
		|| isEQ(wsaaFound,"Y")); wsaaIndex.add(1)){
			if (isEQ(t1698rec.acctyr[wsaaIndex.toInt()],wsaaYearTo)
			&& isEQ(t1698rec.acctmnth[wsaaIndex.toInt()],wsaaMonthTo)) {
				wsaaFound = "Y";
			}
		}
		if (isNE(wsaaFound,"Y")) {
			if (isNE(t1698rec.contitem,SPACES)) {
				wsaaCompany.set(t1698rec.contitem);
				goTo(GotoLabel.loop2200);
			}
			else {
				sv.mnthErr.set(e012);
			}
		}
		wsaaIndex.add(-1);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(t1698rec.frmdate[wsaaIndex.toInt()]);
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}
		sv.dateto.set(datcon2rec.intDate2);
	}

protected void update3000()
	{
		/*LOAD-FIELDS*/
		moveParameters3100();
		writeParamRecord3200();
		/*EXIT*/
	}

protected void moveParameters3100()
	{
		/*MOVE-TO-PARM-RECORD*/
		pm510par.datefrm.set(sv.datefrm);
		pm510par.dateto.set(sv.dateto);
		pm510par.mnth.set(sv.mnth);
		pm510par.year.set(sv.year);
		/*EXIT*/
	}

protected void writeParamRecord3200()
	{
		writBupa3210();
	}

protected void writBupa3210()
	{
		bupaIO.setParams(SPACES);
		bupaIO.setScheduleName(sv.scheduleName);
		bupaIO.setScheduleNumber(sv.scheduleNumber);
		bupaIO.setEffectiveDate(sv.effdate);
		bupaIO.setAcctMonth(sv.acctmonth);
		bupaIO.setAcctYear(sv.acctyear);
		bupaIO.setCompany(sv.bcompany);
		bupaIO.setBranch(sv.bbranch);
		bupaIO.setParmarea(pm510par.parmRecord);
		bupaIO.setParmPromptProg(bppdIO.getParmPromptProg());
		bupaIO.setFormat(buparec);
		bupaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bupaIO);
		if (isNE(bupaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bupaIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
