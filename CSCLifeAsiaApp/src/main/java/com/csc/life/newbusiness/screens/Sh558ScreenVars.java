package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH558
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh558ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(174);
	public FixedLengthStringData dataFields = new FixedLengthStringData(94).isAPartOf(dataArea, 0);
	public FixedLengthStringData adsc = DD.adsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,51);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,89);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(20).isAPartOf(dataArea, 94);
	public FixedLengthStringData adscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 114);
	public FixedLengthStringData[] adscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh558screenWritten = new LongData(0);
	public LongData Sh558protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh558ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, adsc};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, adscOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, adscErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh558screen.class;
		protectRecord = Sh558protect.class;
	}

}
