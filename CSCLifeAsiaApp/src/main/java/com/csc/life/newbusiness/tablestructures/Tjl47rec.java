package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * @author gsaluja2
 *
 */
public class Tjl47rec extends ExternalData {
	private static final long serialVersionUID = 1L;
	public FixedLengthStringData tjl47Rec = new FixedLengthStringData(16);
  	public FixedLengthStringData lincsubr = new FixedLengthStringData(7).isAPartOf(tjl47Rec, 0);
  	public FixedLengthStringData rectypes = new FixedLengthStringData(6).isAPartOf(tjl47Rec, 7);
  	public FixedLengthStringData[] rectype = FLSArrayPartOfStructure(3, 2, rectypes, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(rectypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rectype01 = new FixedLengthStringData(2).isAPartOf(filler1, 0);
  	public FixedLengthStringData rectype02 = new FixedLengthStringData(2).isAPartOf(filler1, 2);
  	public FixedLengthStringData rectype03 = new FixedLengthStringData(2).isAPartOf(filler1, 4);
  	public FixedLengthStringData chgtypes = new FixedLengthStringData(3).isAPartOf(tjl47Rec, 13);
  	public FixedLengthStringData[] chgtype = FLSArrayPartOfStructure(3, 1, chgtypes, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(3).isAPartOf(chgtypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData chgtype01 = new FixedLengthStringData(1).isAPartOf(filler2, 0);
  	public FixedLengthStringData chgtype02 = new FixedLengthStringData(1).isAPartOf(filler2, 1);
  	public FixedLengthStringData chgtype03 = new FixedLengthStringData(1).isAPartOf(filler2, 2);

	public void initialize() {
		COBOLFunctions.initialize(tjl47Rec);
	}	

	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			tjl47Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
}
