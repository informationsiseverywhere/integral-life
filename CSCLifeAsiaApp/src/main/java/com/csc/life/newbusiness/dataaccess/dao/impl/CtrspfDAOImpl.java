/******************************************************************************
 * File Name 		: CtrspfDAOImpl.java
 * Author			: nloganathan5
 * Creation Date	: 28 October 2016
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for CTRSPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.CtrspfDAO;
import com.csc.life.newbusiness.dataaccess.model.Ctrspf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class CtrspfDAOImpl extends BaseDAOImpl<Ctrspf> implements CtrspfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CtrspfDAOImpl.class);

	public List<Ctrspf> searchCtrspfRecord(String chdrcoy, String chdrnum) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, SEQNO, CLNTNUM, DATEFRM, ");
		sqlSelect.append("DATETO, REASONCD, USRPRF, JOBNM, DATIME ");
		sqlSelect.append("FROM CTRSPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, SEQNO ASC, UNIQUE_NUMBER DESC");


		PreparedStatement psCtrspfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlCtrspfRs = null;
		List<Ctrspf> outputList = new ArrayList<Ctrspf>();

		try {

			psCtrspfSelect.setString(1, chdrcoy);
			psCtrspfSelect.setString(2, chdrnum);

			sqlCtrspfRs = executeQuery(psCtrspfSelect);

			while (sqlCtrspfRs.next()) {

				Ctrspf ctrspf = new Ctrspf();

				ctrspf.setUniqueNumber(sqlCtrspfRs.getInt("UNIQUE_NUMBER"));
				ctrspf.setChdrcoy(getCharFromString(sqlCtrspfRs,"CHDRCOY"));
				ctrspf.setChdrnum(sqlCtrspfRs.getString("CHDRNUM"));
				ctrspf.setSeqno(sqlCtrspfRs.getInt("SEQNO"));
				ctrspf.setClntnum(sqlCtrspfRs.getString("CLNTNUM"));
				ctrspf.setDatefrm(sqlCtrspfRs.getInt("DATEFRM"));
				ctrspf.setDateto(sqlCtrspfRs.getInt("DATETO"));
				ctrspf.setReasoncd(sqlCtrspfRs.getString("REASONCD"));
				ctrspf.setUsrprf(sqlCtrspfRs.getString("USRPRF"));
				ctrspf.setJobnm(sqlCtrspfRs.getString("JOBNM"));
				ctrspf.setDatime(sqlCtrspfRs.getDate("DATIME"));

				outputList.add(ctrspf);
			}

		} catch (SQLException e) {
				LOGGER.error("searchCtrspfRecord()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(psCtrspfSelect, sqlCtrspfRs);
		}

		return outputList;
	}
	
	private Character getCharFromString(ResultSet sqlpCovt1Rs , String columnName) throws SQLException{
		
		Character characterOutput = null;
		
		String stringOutput = sqlpCovt1Rs.getString(columnName);
		
		if(null != stringOutput){
			characterOutput = stringOutput.charAt(0);
		}
		
		return characterOutput;
	}
}
