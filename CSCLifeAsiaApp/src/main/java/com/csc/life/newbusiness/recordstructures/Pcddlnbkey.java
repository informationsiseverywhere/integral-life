package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:36
 * Description:
 * Copybook name: PCDDLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Pcddlnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData pcddlnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData pcddlnbKey = new FixedLengthStringData(64).isAPartOf(pcddlnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData pcddlnbChdrcoy = new FixedLengthStringData(1).isAPartOf(pcddlnbKey, 0);
  	public FixedLengthStringData pcddlnbChdrnum = new FixedLengthStringData(8).isAPartOf(pcddlnbKey, 1);
  	public FixedLengthStringData pcddlnbAgntnum = new FixedLengthStringData(8).isAPartOf(pcddlnbKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(pcddlnbKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(pcddlnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		pcddlnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}