package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:25
 * Description:
 * Copybook name: TH565REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th565rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th565Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(th565Rec, 0);
  	public FixedLengthStringData trandescs = new FixedLengthStringData(300).isAPartOf(th565Rec, 3);
  	public FixedLengthStringData[] trandesc = FLSArrayPartOfStructure(10, 30, trandescs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(300).isAPartOf(trandescs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData trandesc01 = new FixedLengthStringData(30).isAPartOf(filler, 0);
  	public FixedLengthStringData trandesc02 = new FixedLengthStringData(30).isAPartOf(filler, 30);
  	public FixedLengthStringData trandesc03 = new FixedLengthStringData(30).isAPartOf(filler, 60);
  	public FixedLengthStringData trandesc04 = new FixedLengthStringData(30).isAPartOf(filler, 90);
  	public FixedLengthStringData trandesc05 = new FixedLengthStringData(30).isAPartOf(filler, 120);
  	public FixedLengthStringData trandesc06 = new FixedLengthStringData(30).isAPartOf(filler, 150);
  	public FixedLengthStringData trandesc07 = new FixedLengthStringData(30).isAPartOf(filler, 180);
  	public FixedLengthStringData trandesc08 = new FixedLengthStringData(30).isAPartOf(filler, 210);
  	public FixedLengthStringData trandesc09 = new FixedLengthStringData(30).isAPartOf(filler, 240);
  	public FixedLengthStringData trandesc10 = new FixedLengthStringData(30).isAPartOf(filler, 270);
  	public FixedLengthStringData zmthfrms = new FixedLengthStringData(8).isAPartOf(th565Rec, 303);
  	public ZonedDecimalData[] zmthfrm = ZDArrayPartOfStructure(4, 2, 0, zmthfrms, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(zmthfrms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zmthfrm01 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData zmthfrm02 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
  	public ZonedDecimalData zmthfrm03 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
  	public ZonedDecimalData zmthfrm04 = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
  	public FixedLengthStringData zmthtos = new FixedLengthStringData(8).isAPartOf(th565Rec, 311);
  	public ZonedDecimalData[] zmthto = ZDArrayPartOfStructure(4, 2, 0, zmthtos, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(zmthtos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData zmthto01 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData zmthto02 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2);
  	public ZonedDecimalData zmthto03 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
  	public ZonedDecimalData zmthto04 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6);
  	public FixedLengthStringData ztrcdes = new FixedLengthStringData(40).isAPartOf(th565Rec, 319);
  	public FixedLengthStringData[] ztrcde = FLSArrayPartOfStructure(10, 4, ztrcdes, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(ztrcdes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ztrcde01 = new FixedLengthStringData(4).isAPartOf(filler3, 0);
  	public FixedLengthStringData ztrcde02 = new FixedLengthStringData(4).isAPartOf(filler3, 4);
  	public FixedLengthStringData ztrcde03 = new FixedLengthStringData(4).isAPartOf(filler3, 8);
  	public FixedLengthStringData ztrcde04 = new FixedLengthStringData(4).isAPartOf(filler3, 12);
  	public FixedLengthStringData ztrcde05 = new FixedLengthStringData(4).isAPartOf(filler3, 16);
  	public FixedLengthStringData ztrcde06 = new FixedLengthStringData(4).isAPartOf(filler3, 20);
  	public FixedLengthStringData ztrcde07 = new FixedLengthStringData(4).isAPartOf(filler3, 24);
  	public FixedLengthStringData ztrcde08 = new FixedLengthStringData(4).isAPartOf(filler3, 28);
  	public FixedLengthStringData ztrcde09 = new FixedLengthStringData(4).isAPartOf(filler3, 32);
  	public FixedLengthStringData ztrcde10 = new FixedLengthStringData(4).isAPartOf(filler3, 36);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(141).isAPartOf(th565Rec, 359, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th565Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th565Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}