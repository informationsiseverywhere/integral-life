package com.csc.life.newbusiness.programs;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.screens.S3615ScreenVars;
import com.csc.life.newbusiness.tablestructures.T3615rec;
import com.csc.life.terminationclaims.programs.Pjl51;
import com.csc.smart.recordstructures.Desckey;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Itmdkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
****************************************************************** ****
* </pre>
*/
public class P3615 extends ScreenProgCS {

	private static final Logger LOGGER = LoggerFactory.getLogger(P3615.class);
	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P3615");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private Wsspsmart wsspsmart = new Wsspsmart();
	
	private S3615ScreenVars sv = ScreenProgram.getScreenVars( S3615ScreenVars.class);
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);
	private final ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);	
	private Desckey wsaaDesckey = new Desckey();
	private Itemkey wsaaItemkey = new Itemkey();
	private Itmdkey wsaaItmdkey = new Itmdkey();
	private Itempf itempf = new Itempf();
	private T3615rec t3615rec = new T3615rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private boolean nbprp124Permission;

	public P3615() {
		super();
		screenVars = sv;
		new ScreenModel("S3615", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
@Override
public void mainline(Object... parmArray)
{
	sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
	scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
	wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
	wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
	try {
		super.mainline();
	}
	catch (COBOLExitProgramException e) {
		LOGGER.info("mainline {}", e);
	}
}

	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
@Override
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		nbprp124Permission  = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP124", appVars, "IT");
		sv.dataArea.set(SPACES);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaDesckey.descDescpfx.set(wsaaItemkey.itemItempfx);
		wsaaDesckey.descDesccoy.set(wsaaItemkey.itemItemcoy);
		wsaaDesckey.descDesctabl.set(wsaaItemkey.itemItemtabl);
		wsaaDesckey.descDescitem.set(wsaaItemkey.itemItemitem);
		wsaaDesckey.descItemseq.set(wsaaItemkey.itemItemseq);
		wsaaDesckey.descLanguage.set(wsspcomn.language);
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		wsaaItmdkey.set(wsspsmart.itmdkey);
		Descpf descpf = descDAO.getdescData("IT", wsaaItemkey.itemItemtabl.toString(),
				wsaaItemkey.itemItemitem.toString(), wsaaItemkey.itemItemcoy.toString(), wsspcomn.language.toString());
		if (descpf==null) {
			fatalError600();
		}else {
			sv.longdesc.set(descpf.getLongdesc());
		}

		itempf = itempfDAO.findItemByItdm(wsaaItemkey.itemItemcoy.toString(), 
				wsaaItemkey.itemItemtabl.toString(),wsaaItemkey.itemItemitem.toString());

		if (itempf == null ) {
			fatalError600();
		}else {
			t3615rec.t3615Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}

		if(!nbprp124Permission) {
			sv.onepcashlessOut[Varcom.pr.toInt()].set("Y");
		}
		if (isNE(t3615rec.t3615Rec, SPACES)) {
			generalArea1045();
		}
	}


protected void generalArea1045()
	{
		sv.onepcashless.set(t3615rec.onepcashless);
	}

protected void preScreenEdit()
{
		preStart();
}

protected void preStart()
{
	if (isEQ(wsspcomn.flag,"I")) {
		scrnparams.function.set(Varcom.prot);
	}
}
@Override
protected void screenEdit2000()
{
	screenIo2010();
	exit2090();
			
}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(Varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
@Override
protected void update3000()
	{
		updatePrimaryRecord3050();
		updateRecord3055();
	}


protected void updatePrimaryRecord3050()
	{
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itempf.setTranid(varcom.vrcmCompTranid.trim());
	}

protected void updateRecord3055()
	{

		wsaaUpdateFlag = "N";
		checkChanges3100();
		if (isNE(wsaaUpdateFlag,"Y")) {
			return;
		}
		itempf.setGenarea(t3615rec.t3615Rec.toString().getBytes());
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemtabl(wsaaItemkey.itemItemtabl.toString());
		itempf.setItemcoy(wsspcomn.company.toString());
		itempf.setItemitem(wsaaItemkey.itemItemitem.toString());
		itempfDAO.updateByKey(itempf, "ITEMPFX, ITEMCOY, ITEMTABL, ITEMITEM");

	}

protected void checkChanges3100()
	{
		if (isNE(sv.onepcashless, t3615rec.onepcashless)) {
			t3615rec.onepcashless.set(sv.onepcashless);
			wsaaUpdateFlag = "Y";
		}
	}

@Override
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

}
