package com.csc.life.newbusiness.dataaccess.dao;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface FluppfDAO extends BaseDAO<Fluppf>{

	public List<Fluppf> searchFlupRecordByChdrNum(String chdrcoy, String chdrNum) throws SQLRuntimeException;
	public Fluppf readFupsts(String chdrcoy, String chdrNum) throws SQLRuntimeException;
	public LinkedHashSet<String> getFollowUps(String clntnum,String clntcoy);
	public void insertFlupRecord(Fluppf fluppf);
	public void deleteFluppfRecord(List<Fluppf> fluprevIOList);
	public void deleteRecordbyCode(Fluppf fluppf);
	public List<Fluppf> searchFlupRecord(String chdrcoy, String chdrNum,String Clamnum) throws SQLRuntimeException;
	public Map<String, List<Fluppf>> searchFlupRecord(String chdrcoy, List<String> chdrList);
	//ILIFE-6296 by wli31
	public boolean checkFluppfRecordByChdrnum(String chdrcoy, String chdrNum) throws SQLRuntimeException;
	public void insertFlupRecord(List<Fluppf> fluplnbList);
	//ILIFE-6590 wli31
	public List<Fluppf> getFluplnbRecordForPerformance(String chdrcoy, String chdrnum);
	public List<Fluppf> getFluplnbRecordsForLife(String chdrcoy, String chdrnum, String life);
	public List<Fluppf> getFluppfRecordByChdrnumAndTyp(Fluppf fluppf);
	public void deleteRecord(String chdrNum, String fupcode, String beneficiary);
	boolean isCheckExistFluppf(Fluppf fluppf);
}

