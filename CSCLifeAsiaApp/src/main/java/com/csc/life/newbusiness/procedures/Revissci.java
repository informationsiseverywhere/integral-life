/*
 * File: Revissci.java
 * Date: 30 August 2009 2:09:14
 * Author: Quipoz Limited
 * 
 * Class transformed from REVISSCI.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList; //BRD-411
import java.util.Iterator;
import java.util.List; //BRD-411
import com.csc.fsu.general.dataaccess.ArcmTableDAM;
import com.csc.fsu.general.dataaccess.BextrevTableDAM;
import com.csc.fsu.general.dataaccess.dao.AcmvpfDAO;
import com.csc.fsu.general.dataaccess.dao.ZfmcpfDAO;
import com.csc.fsu.general.dataaccess.model.Acmvpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.dataaccess.AgpydocTableDAM;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZctnrevTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.dataaccess.ZptnrevTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.archiving.procedures.Acmvrevcp;
import com.csc.life.contractservicing.dataaccess.AcmvrevTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmrevTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrrevTableDAM;
import com.csc.life.contractservicing.dataaccess.FluprevTableDAM;
import com.csc.life.contractservicing.dataaccess.RtrnrevTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Greversrec;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.Fpcolf1TableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrcfiTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrcfiTableDAM;
import com.csc.life.newbusiness.dataaccess.LifecfiTableDAM;
import com.csc.life.newbusiness.dataaccess.LinscfiTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.ZswrpfDAO; //BRD-411
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Zswrpf; //BRD-411
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*                CANCEL FROM INCEPTION
*                ---------------------
*
* Overview.
* ---------
*
* This subroutine will perform all the processing necessary
*  to Cancel a Contract from Inception. This routine will be
*  called from the Full Contract Reversal AT module, REVGENAT,
*  and perform the reversal of all of the necessary transactions
*  which occurred during the Issue. It will logically delete all
*  unwanted records, write a new transaction history record, and
*  update the Status of the Contract and all associated coverage
*  records.
*
*
* Processing.
* -----------
*
* Update CHDR Contract Header details:
*
*  Read the CHDR file using the CHDRCFI logical view with a key
*  of Company and Contract number from the Linkage
*
*     Set status to whatever is set on T5679 entry
*     Update Transaction number
*     Set Paid-to date to Contract Commencement Date
*     Set Billing Renewal date to Contract Commencement Date
*     Set Transaction Date, Time, User and Terminal-ID
*     Set the 'Available for Issue' flag to 'N'
*
* Read T5729 to check if contract is flexible premium based.
* Reverse Accounting movements ( ACMVs ):
*
*  Read the Account movements file (ACMVPF) using the logical
*  view ACMVREV with a key of contract number/transaction number
*  from linkage.
*
*  For each record found:
*   Write a new record with the following...
*   - Set the accounting month/year with the batch
*   - accounting month/year from linkage.
*   - Set the tranno with the NEW tranno from linkage.
*   - Set the batch key with new batch key from linkage.
*   - Multiply the original currency amount by -1.
*   - Multiply the accounting currency amount by -1.
*   - Set the transaction reference with the NEW
*   - transaction code from linkage.
*   - Set write the transaction description with the long
*   - description of the new trans code from T1688.
*   - Set the effective date with todays date.
*   - Write the new record to the database by calling the
*     subroutine 'LIFACMV'.
*
* Update Agent commission records ( AGCMs ):
*
* Read the Agents commission file (AGCMPF) with the logical
*  view AGCMREV using the contract number as the key.
*
*  For each record found carry out the following processing:-
*     If the effective date of the record is less than or equal
*     to the new paid to date (effective date 1 from linkage)
*     and the paid to date of the record is greater than the
*     new paid to date:-
*          Set up the details required in the COMLINKREC copybook
*          and call the relevant reversal subroutine. This is
*          obtained from T5644 using the method from the record
*          for this type of commission as found earlier.
*
*          On return from the subroutine move the relevant
*          earned/paid amounts to working storage variables.
*          Use the AGCMRVS logical to update the AGCM record
*          with the values from the subroutine currently held
*          in the working storage variables.
*
* Update the FPRM records (Flexible Premiums only):
*
*  Read and hold records with same contract and transaction
*  number as in linkage.
*  Rewrite them with validflag = '2'.
*
* Delete BEXT Billing Extract records:
*
*  Read the BEXT file using the BEXTREV logical view with a key
*  of Company and Contract number from the Linkage and the
*  Billed-to date set to MAX-DATE
*
*     For each record read which matches on Company & Contract
*      number, DELETe record.
*
* Update the FPCO records (Flexible Premiums only):
*
*  Read and hold records with same contract and transaction
*  number as in linkage.
*  Rewrite them with validflag = '2'.
*
* Update BNFY Beneficiary records:
*
*  Read the BNFY file using the BNFYLNB logical view with a key
*  of Company and Contract number from the Linkage.
*
*     For each record read which matches on Company & Contract
*      number, update VALIDFLAG from '1' to '2'.
*
* Update COVR Coverage records:
*
*  Read the COVR file using the COVRCFI logical view with a key
*  of Company and Contract number from the Linkage.
*
*  For each record read which matches on Company & Contract
*   number,
*
*     1) Reversal of Unit-Linked/Traditional components:
*
*            Loop through all the Coverages/Riders associated
*            with the Contract we are processing and call any
*            Generic processing subroutines that are specified
*            in T5671
*
*     2) Update of all COVR records:
*            Set statii to whatever is set on T5679 entry
*            Update Transaction number
*            Zeroise the Anniversary processing date
*            Zeroise the Rereate date
*            Zeroise the Benefit billing date
*            Set Transaction Date, Time, User and Terminal-ID
*            The CRDEBT field on the COVR field is not zeroised,
*             since the contract has been CFI'd and can no
*             longer be used anyway.
*            The BTDATE (billed-to date) is left as was at the
*             time of the CFI  - the reason is that at proposal
*             stage, the user can change the billed-to date to
*             be anything and as we don't know what it was at
*             issue time, there is no way we can reset it.
*
* Delete FLUP Follow-up records:
*
*  Read the FLUP file using the FLUPREV logical view with a key
*  of Company and Contract number from the Linkage and the
*  Transaction number set to zeros
*
*     For each record read which matches on Company & Contract
*      number, DELETe record.
*
* Update PAYR Contract Payer record:
*
*  Read the PAYR file using the PAYR logical view with a key
*  of Company and Contract number from the Linkage and the
*  Payer Sequence number starting at zeros
*
*   For each record read which matches on Company & Contract
*   number,
*        Set Paid-to date to Contract Commencement date
*        Set Billing Renewal date to Contract Commencement date
*        Update Transaction number
*        Set Transaction Date, Time, User and Terminal-ID
*
* Update LEXT Life Options/Extras records:
*
*  No Changes required - these records will not be changed
*   in any way. They will be left as at time of CFI.
*
* Update LIFE Life details records:
*
*  Read the LIFE file using the LIFECFI logical view with a key
*  of Company and Contract number from the Linkage.
*
*   For each record read which matches on Company & Contract
*   number,
*        Set statii to whatever is set on T5679 entry
*        Update Transaction number
*        Set Transaction Date, Time, User and Terminal-ID
*
* Delete LINS Life Installemt records:
*
*  Read the LINS file using the LINSCFI logical view with a key
*  of Company and Contract number from the Linkage.
*
*     For each record read which matches on Company & Contract
*      number, DELETe record.
*
* Update PCDD Proposed Commission/Bonus points split records:
*
*  No Changes required - these records will not be changed
*   in any way. They will be left as at time of CFI.
*
* Update RACT Reassurance records:
*
*  No Changes required - these records will not be changed
*   in any way. They will be left as at time of CFI.
*
* Reverse Accounting Movement ( RTRN ):
*
* Read the file (RTRNPF) using the logical view RTRNREV with
* a key of contract number/transaction number from linkage.
*
* For each record found:
*   Write a new record with the following...
*   - Set the accounting month/year with the batch
*   - accounting month/year from linkage.
*   - Set the tranno with the NEW tranno from linkage.
*   - Set the batch key with new batch key from linkage.
*   - Multiply the original currency amount by -1.
*   - Multiply the accounting currency amount by -1.
*   - Set the transaction reference with the NEW
*   - transaction code from linkage.
*   - Set the transaction description with the long
*   - description of the new trans code from T1688.
*   - Set the effective date with todays date.
*   - Write the new record to the database by calling the
*     subroutine 'LIFRTRN'.
*
*
* Tables used.
* ------------
*
* T1688 - Transaction Codes
* T5644 - Commission Releaee methods
* T5679 - Valid Transaction Statii
* T5729 - Flexible Premium products
*
*****************************************************************
* </pre>
*/
public class Revissci extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "REVISSCI";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaUpdateAgcmFlag = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaCommType = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaTransDesc = new FixedLengthStringData(30).init(SPACES);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(1, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaBillingFrequency = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillingFrequency, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaActyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaPeriod, 0).setUnsigned();
	private ZonedDecimalData wsaaActmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaPeriod, 4).setUnsigned();

	private FixedLengthStringData wsbbPeriod = new FixedLengthStringData(6);
	private ZonedDecimalData wsbbActyr = new ZonedDecimalData(4, 0).isAPartOf(wsbbPeriod, 0).setUnsigned();
	private ZonedDecimalData wsbbActmn = new ZonedDecimalData(2, 0).isAPartOf(wsbbPeriod, 4).setUnsigned();
	private String wsaaIoCall = "";
		/* WSAA-STORE-VALUES */
	private PackedDecimalData[] wsaaPayamnt = PDInittedArray(3, 13, 2);
	private PackedDecimalData[] wsaaErndamt = PDInittedArray(3, 13, 2);

	private FixedLengthStringData wsaaFlexiblePremium = new FixedLengthStringData(1).init(SPACES);
	private Validator flexiblePremium = new Validator(wsaaFlexiblePremium, "Y");
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).init(SPACES);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(ZERO);
	private FixedLengthStringData wsaaT5687Crtable = new FixedLengthStringData(4).init(SPACES);
	private PackedDecimalData wsaaT5687Crrcd = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaT6661Exist = new FixedLengthStringData(1);
	private Validator t6661Found = new Validator(wsaaT6661Exist, "Y");
	private Validator t6661Nfnd = new Validator(wsaaT6661Exist, "N");

	private FixedLengthStringData wsaaT5671Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5671Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5671Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Key, 4).init(SPACES);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
		/* ERRORS */
	private static final String e792 = "E792";
	private static final String f294 = "F294";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5644 = "T5644";
	private static final String t5679 = "T5679";
	private static final String t5671 = "T5671";
	private static final String t5687 = "T5687";
	private static final String t5729 = "T5729";
	private static final String t6661 = "T6661";
	private static final String th605 = "TH605";
	private AcmvrevTableDAM acmvrevIO = new AcmvrevTableDAM();//ILB-492
	private Acmvpf acmvpf = new Acmvpf();
	private List<Acmvpf> acmvpfList1 = null;
	private List<Acmvpf> acmvpfList2 = null;
	private AcmvpfDAO acmvpfDAO = getApplicationContext().getBean("acmvpfDAO", AcmvpfDAO.class);//ILB-492
	
	private AgcmrevTableDAM agcmrevIO = new AgcmrevTableDAM();
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
	private AgpydocTableDAM agpydocIO = new AgpydocTableDAM();
	private ArcmTableDAM arcmIO = new ArcmTableDAM();
	private BextrevTableDAM bextrevIO = new BextrevTableDAM();
	private ChdrcfiTableDAM chdrcfiIO = new ChdrcfiTableDAM();
	private CovrcfiTableDAM covrcfiIO = new CovrcfiTableDAM();
	private CovrrevTableDAM covrrevIO = new CovrrevTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FluprevTableDAM fluprevIO = new FluprevTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private Fpcolf1TableDAM fpcolf1IO = new Fpcolf1TableDAM();
	private FprmTableDAM fprmIO = new FprmTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifecfiTableDAM lifecfiIO = new LifecfiTableDAM();
	private LinscfiTableDAM linscfiIO = new LinscfiTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private RtrnrevTableDAM rtrnrevIO = new RtrnrevTableDAM();
	private TaxdrevTableDAM taxdrevIO = new TaxdrevTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZctnrevTableDAM zctnrevIO = new ZctnrevTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private ZptnrevTableDAM zptnrevIO = new ZptnrevTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Greversrec greversrec = new Greversrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5644rec t5644rec = new T5644rec();
	private T5679rec t5679rec = new T5679rec();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
	private Th605rec th605rec = new Th605rec();
	protected Reverserec reverserec = new Reverserec();
	private FormatsInner formatsInner = new FormatsInner();
	private ZswrpfDAO  zswrDAO =  getApplicationContext().getBean("ZswrpfDAO", ZswrpfDAO.class);//BRD-411
	private static final String  FMC_FEATURE_ID = "BTPRO026";	//IBPLIFE-1433
	private boolean fmcOnFlag = false;	//IBPLIFE-1433
	private ZfmcpfDAO zfmcpfDAO = getApplicationContext().getBean("zfmcpfDAO", ZfmcpfDAO.class); //IBPLIFE-1433
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		call3320, 
		exit3390, 
		continue4100, 
		readNextAcmv4180, 
		pass5350, 
		exit5390, 
		call15020, 
		exit15090, 
		call16020, 
		exit16090
	}

	public Revissci() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		reverserec.reverseRec = convertAndSetParam(reverserec.reverseRec, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1090();
	}

protected void start1000()
	{
		syserrrec.subrname.set(wsaaSubr);
		reverserec.statuz.set(varcom.oK);
		initialise2000();
		processChdr3000();
		processPayr10000();
		processAcmv4000();
		processAcmvOptical4010();
		processFpco4300();
		processFprm4500();
		processAgcm5000();
		processBext6000();
		/* PERFORM 7000-PROCESS-BNFY.                                   */
		processCovr8000();
		processFlup9000();
		/* PERFORM 10000-PROCESS-PAYR.                                  */
		processLife11000();
		processLins12000();
		processTaxd12200();
		processRtrn13000();
		processZafrN200();// BRD-411
		//IBPLIFE-1433 Starts
		fmcOnFlag = FeaConfg.isFeatureExist(reverserec.company.toString(), FMC_FEATURE_ID, appVars, "IT");
		if (fmcOnFlag) {
			processzfmcN100();
		}
		//IBPLIFE-1433 End
		processZdivCustSpecific();
		/* Read table TH605 to see if Bonus Workbench Extraction used      */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(th605);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(reverserec.company);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError99000();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
		if (isNE(th605rec.bonusInd, "Y")) {
			return ;
		}
		/* Bonus Workbench  *                                              */
		processZptn15000();
		processZctn16000();
	}
//BRD-411
protected void processZafrN200() { 

	List<Zswrpf> zswrpfiList=new ArrayList<>();
    Zswrpf zw=new Zswrpf();
    zw.setChdrpfx("CH");
    zw.setChdrcoy(reverserec.company.toString());
    zw.setChdrnum(reverserec.chdrnum.toString()); 
    zw.setValidflag("1" );
    zswrpfiList=zswrDAO.getZswrpfData( zw.getChdrnum(),zw.getChdrcoy(),  zw.getChdrpfx(),  zw.getValidflag());/* IJTI-1523 */       

     if (zswrpfiList.size()>0 )
    {
           for (Zswrpf p : zswrpfiList) 
            {                    
        	   zswrDeletesN300();
           }
    }
}

protected void zswrDeletesN300()
{
	startN310();
}

protected void startN310()
{
	
	
		    List<Zswrpf> zswrpfiList=new ArrayList<>();
		    Zswrpf zw=new Zswrpf();
		    zw.setChdrpfx("CH");
		    zw.setChdrcoy(reverserec.company.toString());
		    zw.setChdrnum(reverserec.chdrnum.toString()); 
		    zw.setValidflag("1" );
		    boolean m=zswrDAO.deleteZswrpf( zw.getChdrnum(),zw.getChdrcoy(),  zw.getChdrpfx());/* IJTI-1523 */

			
}//BRD-411

protected void exit1090()
	{
		exitProgram();
	}

protected void initialise2000()
	{
		start2000();
	}

protected void start2000()
	{
		wsaaItem.set(SPACES);
		wsaaTransDesc.set(SPACES);
		wsaaUpdateAgcmFlag.set(SPACES);
		wsaaBatckey.set(reverserec.batchkey);
		wsaaCommType.set(ZERO);
		wsaaBillfreq.set(ZERO);
		wsaaSub.set(ZERO);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)){
			wsaaPayamnt[wsaaSub.toInt()].set(ZERO);
			wsaaErndamt[wsaaSub.toInt()].set(ZERO);
		}
		wsaaSub.set(ZERO);
		wsaaLife.set(SPACES);
		wsaaCoverage.set(SPACES);
		wsaaRider.set(SPACES);
		wsaaPlanSuffix.set(ZERO);
		/* get todays date*/
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			systemError99000();
		}
		wsaaToday.set(datcon1rec.intDate);
		/* get transaction description for putting on ACMV later on.*/
		getDescription2100();
		/* read T5679 to get valid transaction status codes*/
		readT56792200();
	}

protected void getDescription2100()
	{
		start2100();
	}

protected void start2100()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(reverserec.company);
		descIO.setLanguage(reverserec.language);
		descIO.setDescitem(reverserec.batctrcde);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaTransDesc.fill("?");
		}
		else {
			wsaaTransDesc.set(descIO.getLongdesc());
		}
	}

protected void readT56792200()
	{
		start2200();
	}

protected void start2200()
	{
		/* Read table T5679 to get transaction statii*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5679);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(reverserec.batctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void processChdr3000()
	{
		start3000();
	}

protected void start3000()
	{
		/* Set CHDR record to be CFI'd.*/
		chdrcfiIO.setParams(SPACES);
		chdrcfiIO.setChdrcoy(reverserec.company);
		chdrcfiIO.setChdrnum(reverserec.chdrnum);
		chdrcfiIO.setFormat(formatsInner.chdrcfirec);
		chdrcfiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrcfiIO);
		if (isNE(chdrcfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrcfiIO.getParams());
			syserrrec.statuz.set(chdrcfiIO.getStatuz());
			databaseError99500();
		}
		/* Update fields on CHDR record and rewrite.*/
		if (isNE(t5679rec.setCnRiskStat, SPACES)) {
			chdrcfiIO.setStatcode(t5679rec.setCnRiskStat);
		}
		if (isEQ(chdrcfiIO.getBillfreq(), "00")) {
			if (isNE(t5679rec.setSngpCnStat, SPACES)) {
				chdrcfiIO.setPstatcode(t5679rec.setSngpCnStat);
			}
		}
		else {
			if (isNE(t5679rec.setCnPremStat, SPACES)) {
				chdrcfiIO.setPstatcode(t5679rec.setCnPremStat);
			}
		}
		/*  Check if contract is flexible premium.                 <D9604>*/
		readT57293100();
		chdrcfiIO.setTranno(reverserec.newTranno);
		chdrcfiIO.setAvlisu("N");
		chdrcfiIO.setStatdate(reverserec.effdate1);
		chdrcfiIO.setStattran(reverserec.newTranno);
		chdrcfiIO.setPstatdate(reverserec.effdate1);
		chdrcfiIO.setPstattran(reverserec.newTranno);
		chdrcfiIO.setBtdate(chdrcfiIO.getCcdate());
		chdrcfiIO.setPtdate(chdrcfiIO.getCcdate());
		chdrcfiIO.setBillcd(chdrcfiIO.getCcdate());
		varcom.vrcmCompTermid.set(reverserec.termid);
		varcom.vrcmUser.set(reverserec.user);
		varcom.vrcmDate.set(reverserec.transDate);
		varcom.vrcmTime.set(reverserec.transTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrcfiIO.setTranid(varcom.vrcmCompTranid);
		chdrcfiIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrcfiIO);
		if (isNE(chdrcfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrcfiIO.getParams());
			syserrrec.statuz.set(chdrcfiIO.getStatuz());
			databaseError99500();
		}
		updateAgpy3300();
	}

	/**
	* <pre>
	**********************************************************<D9604>
	* </pre>
	*/
protected void readT57293100()
	{
		start3110();
	}

	/**
	* <pre>
	**********************************************************<D9604>
	* </pre>
	*/
protected void start3110()
	{
		wsaaFlexiblePremium.set("N");
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5729);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemitem(chdrcfiIO.getCnttype());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			wsaaFlexiblePremium.set("Y");
		}
	}

protected void updateAgpy3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin3310();
				case call3320: 
					call3320();
					next3380();
				case exit3390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin3310()
	{
		agpydocIO.setParams(SPACES);
		agpydocIO.setBatccoy(reverserec.company);
		agpydocIO.setRdocnum(reverserec.chdrnum);
		agpydocIO.setTranno(0);
		agpydocIO.setJrnseq(0);
		agpydocIO.setFormat(formatsInner.agpydocrec);
		agpydocIO.setFunction(varcom.begn);
	}

protected void call3320()
	{
		SmartFileCode.execute(appVars, agpydocIO);
		if (isNE(agpydocIO.getStatuz(), varcom.oK)
		&& isNE(agpydocIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(agpydocIO.getStatuz());
			syserrrec.params.set(agpydocIO.getParams());
			databaseError99500();
		}
		if (isEQ(agpydocIO.getStatuz(), varcom.endp)
		|| isNE(agpydocIO.getBatccoy(), reverserec.company)
		|| isNE(agpydocIO.getRdocnum(), reverserec.chdrnum)) {
			agpydocIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3390);
		}
		if (isEQ(agpydocIO.getEffdate(), 0)
		|| isEQ(agpydocIO.getEffdate(), varcom.vrcmMaxDate)) {
			agpyagtIO.setParams(SPACES);
			agpyagtIO.setRrn(agpydocIO.getRrn());
			agpyagtIO.setFormat(formatsInner.agpyagtrec);
			agpyagtIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				databaseError99500();
			}
			agpyagtIO.setEffdate(wsaaToday);
			agpyagtIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				databaseError99500();
			}
		}
	}

protected void next3380()
	{
		agpydocIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call3320);
	}

protected void processAcmv4000()
	{
		start4000();
	}

protected void start4000()
	{//ILB-492
		/*acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		wsaaIoCall = "IO";
		acmvrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		acmvrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		SmartFileCode.execute(appVars, acmvrevIO);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
	
	*/	
		acmvpf.setRldgcoy(reverserec.company.toString());
		acmvpf.setRdocnum(reverserec.chdrnum.toString());
		acmvpf.setTranno(reverserec.tranno.toInt());
		wsaaIoCall = "IO";
		acmvpfList1=acmvpfDAO.findAcmvRecords(acmvpf);;
		if(acmvpfList1==null){
			databaseError99500();
		}
//		Iterator<Acmvpf> acmvpfItr= acmvpfList1.iterator();
//		while ( acmvpfItr.hasNext()) {
//			reverseAcmvs4100();
//		}
		
		for(Acmvpf acmvpfItr : acmvpfList1) {
			acmvpf=acmvpfItr;
			reverseAcmvs4100();
		} 
	
	}

protected void processAcmvOptical4010()
	{
		start4010();
	}

protected void start4010()
	{
		arcmIO.setParams(SPACES);
		arcmIO.setFileName("ACMV");
		arcmIO.setFunction(varcom.readr);
		arcmIO.setFormat(formatsInner.arcmrec);
		SmartFileCode.execute(appVars, arcmIO);
		if (isNE(arcmIO.getStatuz(), varcom.oK)
		&& isNE(arcmIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(arcmIO.getParams());
			syserrrec.statuz.set(arcmIO.getStatuz());
			databaseError99500();
		}
		/* This code unused in Integral java */
		
		/* If the PTRN being processed has an Accounting Period Greater    */
		/* than the last period Archived then there is no need to          */
		/* attempt to read the Optical Device.                             */
		if (isEQ(arcmIO.getStatuz(), varcom.mrnf)) {
			wsbbPeriod.set(ZERO);
		}
		else {
			wsbbActyr.set(arcmIO.getAcctyr());
			wsbbActmn.set(arcmIO.getAcctmnth());
		}
		wsaaActyr.set(reverserec.ptrnBatcactyr);
		wsaaActmn.set(reverserec.ptrnBatcactmn);
		if (isGT(wsaaPeriod, wsbbPeriod)) {
			return ;
		}
		acmvrevIO.setParams(SPACES);
		acmvrevIO.setRldgcoy(reverserec.company);
		acmvrevIO.setRdocnum(reverserec.chdrnum);
		acmvrevIO.setTranno(reverserec.tranno);
		acmvrevIO.setBatctrcde(reverserec.oldBatctrcde);
		acmvrevIO.setBatcactyr(reverserec.ptrnBatcactyr);
		acmvrevIO.setBatcactmn(reverserec.ptrnBatcactmn);
		wsaaIoCall = "CP";
		acmvrevIO.setFunction(varcom.begn);
		acmvrevIO.setFormat(formatsInner.acmvrevrec);
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(acmvrevIO.getStatuz(), varcom.endp))) {
			reverseAcmvs4100();
		}
		
		/* If the next policy is found then we must call the COLD API      */
		/* again with a function of CLOSE.                                 */
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())) {
			acmvrevIO.setFunction("CLOSE");
			FixedLengthStringData groupTEMP2 = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP2);
			acmvrevIO.setParams(groupTEMP2);
			if (isNE(acmvrevIO.getStatuz(), varcom.oK)
			&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(acmvrevIO.getParams());
				databaseError99500();
			}
		}
	}

protected void reverseAcmvs4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start4100();
				case continue4100: 
					continue4100();
				case readNextAcmv4180: 
					readNextAcmv4180();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start4100()
	{
		/* check If ACMV just read has same trancode as transaction*/
		/*  we are trying to reverse*/
		itemIO.setParams(SPACES);
		wsaaT6661Exist.set("N");
		itemIO.setItempfx(smtpfxcpy.item);
		//itemIO.setItemcoy(acmvrevIO.getRldgcoy());
		itemIO.setItemcoy(acmvpf.getRldgcoy());//ILB-492
		itemIO.setItemtabl(t6661);
		//itemIO.setItemitem(acmvrevIO.getBatctrcde());
		itemIO.setItemitem(acmvpf.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT6661Exist.set("N");
		}
		else {
			if (isEQ(itemIO.getStatuz(), varcom.oK)) {
				wsaaT6661Exist.set("Y");
			}
		}
		if (t6661Nfnd.isTrue()) {
			goTo(GotoLabel.continue4100);
		}
		//if (isNE(acmvrevIO.getBatctrcde(), reverserec.oldBatctrcde)) {
		if (isNE(acmvpf.getBatctrcde(), reverserec.oldBatctrcde)) {//ILB-492
			goTo(GotoLabel.readNextAcmv4180);
		}
	}

	/**
	* <pre>
	* post equal & opposite amount to ACMV just read
	* </pre>
	*/
protected void continue4100()
	{//ILB-492
		lifacmvrec.lifacmvRec.set(SPACES);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.frcdate.set(ZERO);
		lifacmvrec.transactionDate.set(ZERO);
		lifacmvrec.transactionTime.set(ZERO);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(reverserec.batchkey);
		//lifacmvrec.rdocnum.set(acmvrevIO.getRdocnum());
		lifacmvrec.rdocnum.set(acmvpf.getRdocnum());
		lifacmvrec.tranno.set(reverserec.newTranno);
		lifacmvrec.sacscode.set(acmvpf.getSacscode());
		lifacmvrec.sacstyp.set(acmvpf.getSacstyp());
		lifacmvrec.glcode.set(acmvpf.getGlcode());
		lifacmvrec.glsign.set(acmvpf.getGlsign());
		lifacmvrec.jrnseq.set(acmvpf.getJrnseq());
		lifacmvrec.rldgcoy.set(acmvpf.getRldgcoy());
		lifacmvrec.genlcoy.set(acmvpf.getGenlcoy());
		lifacmvrec.rldgacct.set(acmvpf.getRldgacct());
		lifacmvrec.origcurr.set(acmvpf.getOrigcurr());
		compute(lifacmvrec.acctamt, 2).set(mult(acmvpf.getAcctamt(), -1));
		compute(lifacmvrec.origamt, 2).set(mult(acmvpf.getOrigamt(), -1));
		lifacmvrec.genlcur.set(acmvpf.getGenlcur());
		lifacmvrec.crate.set(acmvpf.getCrate());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.tranref.set(acmvpf.getTranref());
		lifacmvrec.trandesc.set(wsaaTransDesc);
		/* MOVE DTC1-INT-DATE          TO LIFA-EFFDATE.                 */
		lifacmvrec.effdate.set(acmvpf.getEffdate());
		/* MOVE ACMVREV-FRCDATE        TO LIFA-FRCDATE.                 */
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.termid.set(reverserec.termid);
		lifacmvrec.user.set(reverserec.user);
		lifacmvrec.transactionTime.set(reverserec.transTime);
		lifacmvrec.transactionDate.set(reverserec.transDate);
		if (t6661Nfnd.isTrue()) {
			lifacmvrec.suprflag.set("Y");
		}
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			systemError99000();
		}
	}

protected void readNextAcmv4180()
	{//ILB 492
		/*  Read the next ACMV record.
		acmvrevIO.setFunction(varcom.nextr);
		  CALL 'ACMVREVIO'            USING ACMVREV-PARAMS.            
		if (isEQ(wsaaIoCall, "CP")) {
			FixedLengthStringData groupTEMP = acmvrevIO.getParams();
			callProgram(Acmvrevcp.class, groupTEMP);
			acmvrevIO.setParams(groupTEMP);
		}
		else {
			SmartFileCode.execute(appVars, acmvrevIO);
		}
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
		&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(acmvrevIO.getParams());
			syserrrec.statuz.set(acmvrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
		|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
		|| isNE(reverserec.tranno, acmvrevIO.getTranno())
		|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
			acmvrevIO.setStatuz(varcom.endp);
		}*/
	
	//acmvrevIO.setFunction(varcom.nextr);            
	if (isEQ(wsaaIoCall, "CP")) {
		FixedLengthStringData groupTEMP = acmvrevIO.getParams();
		callProgram(Acmvrevcp.class, groupTEMP);
		acmvrevIO.setParams(groupTEMP);
		
		if (isNE(acmvrevIO.getStatuz(), varcom.oK)
				&& isNE(acmvrevIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(acmvrevIO.getParams());
					syserrrec.statuz.set(acmvrevIO.getStatuz());
					databaseError99500();
				}
		if (isNE(reverserec.company, acmvrevIO.getRldgcoy())
			|| isNE(reverserec.chdrnum, acmvrevIO.getRdocnum())
			|| isNE(reverserec.tranno, acmvrevIO.getTranno())
			|| isEQ(acmvrevIO.getStatuz(), varcom.endp)) {
					acmvrevIO.setStatuz(varcom.endp);
				}
	}
	/*else {
		
		acmvpfList2 = acmvpfDAO.getNextAcmvpfRecord4180(acmvpf);
		
	}
	
	if(null==acmvpfList2) {
		databaseError99500();
	}*/
	
	
		/*EXIT*/
	}

protected void processFpco4300()
	{
		/*START*/
		if (!flexiblePremium.isTrue()) {
			return ;
		}
		fpcoIO.setParams(SPACES);
		fpcoIO.setChdrcoy(reverserec.company);
		fpcoIO.setChdrnum(reverserec.chdrnum);
		fpcoIO.setPlanSuffix(ZERO);
		fpcoIO.setTargfrom(ZERO);
		fpcoIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fpcoIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fpcoIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(fpcoIO.getStatuz(), varcom.endp))) {
			rewrtFpco4400();
		}
		
		/*EXIT*/
	}

protected void rewrtFpco4400()
	{
		start4401();
	}

	/**
	* <pre>
	**************************                                <D9604>
	* </pre>
	*/
protected void start4401()
	{
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)
		&& isNE(fpcoIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(fpcoIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check FPCO record read is associated with contract      <D9604>*/
		if (isNE(reverserec.company, fpcoIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, fpcoIO.getChdrnum())) {
			fpcoIO.setStatuz(varcom.endp);
			return ;
		}
		/* check FPCO record read is not the one just written      <D9604>*/
		if (isEQ(fpcoIO.getTranno(), reverserec.newTranno)) {
			fpcoIO.setFunction(varcom.nextr);
			return ;
		}
		fpcoIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			databaseError99500();
		}
		fpcoIO.setValidflag("2");
		fpcoIO.setCurrto(reverserec.effdate1);
		fpcoIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			databaseError99500();
		}
		/*                                                         <D9604>*/
		/* WRITE A NEW VALIDFLAG 1 FPCO WITH THE ACTIVE IND SET TO <D9604>*/
		/* 'N' AND ONLY A TARGET PREMIUM IN THE PREMIUM DETAILS.   <D9604>*/
		fpcolf1IO.setParams(SPACES);
		fpcolf1IO.setChdrcoy(fpcoIO.getChdrcoy());
		fpcolf1IO.setChdrnum(fpcoIO.getChdrnum());
		fpcolf1IO.setValidflag("1");
		fpcolf1IO.setCurrto(varcom.vrcmMaxDate);
		fpcolf1IO.setCurrfrom(reverserec.effdate1);
		fpcolf1IO.setEffdate(reverserec.effdate1);
		fpcolf1IO.setTargfrom(fpcoIO.getTargfrom());
		fpcolf1IO.setTargto(fpcoIO.getTargto());
		fpcolf1IO.setLife(fpcoIO.getLife());
		fpcolf1IO.setCoverage(fpcoIO.getCoverage());
		fpcolf1IO.setRider(fpcoIO.getRider());
		fpcolf1IO.setPlanSuffix(fpcoIO.getPlanSuffix());
		fpcolf1IO.setActiveInd("N");
		fpcolf1IO.setAnnProcessInd("N");
		fpcolf1IO.setTargetPremium(fpcoIO.getTargetPremium());
		fpcolf1IO.setPremRecPer(ZERO);
		fpcolf1IO.setBilledInPeriod(ZERO);
		fpcolf1IO.setOverdueMin(ZERO);
		fpcolf1IO.setMinOverduePer(ZERO);
		fpcolf1IO.setTranno(reverserec.newTranno);
		fpcolf1IO.setAnnivProcDate(fpcoIO.getAnnivProcDate());
		fpcolf1IO.setFunction(varcom.writr);
		fpcolf1IO.setFormat(formatsInner.fpcolf1rec);
		SmartFileCode.execute(appVars, fpcolf1IO);
		if (isNE(fpcolf1IO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fpcolf1IO.getParams());
			syserrrec.statuz.set(fpcolf1IO.getStatuz());
			databaseError99500();
		}
		fpcoIO.setFunction(varcom.nextr);
	}

protected void processFprm4500()
	{
		start4510();
	}

	/**
	* <pre>
	***************************                               <D9604>
	* </pre>
	*/
protected void start4510()
	{
		if (!flexiblePremium.isTrue()) {
			return ;
		}
		fprmIO.setParams(SPACES);
		fprmIO.setChdrcoy(reverserec.company);
		fprmIO.setChdrnum(reverserec.chdrnum);
		fprmIO.setPayrseqno(1);
		fprmIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			databaseError99500();
		}
		fprmIO.setValidflag("2");
		fprmIO.setCurrto(reverserec.effdate1);
		fprmIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			databaseError99500();
		}
		fprmIO.setCurrfrom(reverserec.effdate1);
		fprmIO.setCurrto(reverserec.effdate1);
		fprmIO.setValidflag("1");
		fprmIO.setTotalRecd(0);
		fprmIO.setTotalBilled(0);
		fprmIO.setMinPrmReqd(0);
		fprmIO.setTranno(reverserec.newTranno);
		fprmIO.setFunction(varcom.writr);
		fprmIO.setFormat(formatsInner.fprmrec);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			databaseError99500();
		}
	}

protected void processAgcm5000()
	{
		start5000();
	}

protected void start5000()
	{
		agcmrevIO.setParams(SPACES);
		agcmrevIO.setChdrnum(reverserec.chdrnum);
		agcmrevIO.setChdrcoy(reverserec.company);
		agcmrevIO.setPlanSuffix(ZERO);
		/* MOVE BEGN                   TO AGCMREV-FUNCTION.             */
		/* MOVE BEGNH                  TO AGCMREV-FUNCTION.     <LA3993>*/
		agcmrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agcmrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agcmrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, agcmrevIO);
		if (isNE(agcmrevIO.getStatuz(), varcom.oK)
		&& isNE(agcmrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrevIO.getParams());
			syserrrec.statuz.set(agcmrevIO.getStatuz());
			systemError99000();
		}
		if (isNE(reverserec.chdrnum, agcmrevIO.getChdrnum())
		|| isNE(reverserec.company, agcmrevIO.getChdrcoy())
		|| isEQ(agcmrevIO.getStatuz(), varcom.endp)) {
			agcmrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(agcmrevIO.getStatuz(), varcom.endp))) {
			reverseAgcms5100();
		}
		
	}

protected void reverseAgcms5100()
	{
		start5100();
		readNextAgcm5180();
	}

protected void start5100()
	{
		/* Check tranno on AGCMREV record just read is OK for us to*/
		/*  reverse - else read Next AGCMREV record*/
		if (isNE(reverserec.tranno, agcmrevIO.getTranno())) {
			return ;
		}
		/* Need to get Coverage/Rider name ( CRTABLE ) to pass to the*/
		/*  Commission subroutines, so read COVR file.*/
		getCrtable5200();
		wsaaT5687Crtable.set(covrrevIO.getCrtable());
		wsaaT5687Crrcd.set(covrrevIO.getCrrcd());
		readT568714000();
		/* We use the Commission Reversal subroutines to do the processing*/
		/*  for us.*/
		/* the loop enables us to check for all commission types....*/
		/*        WSAA-COMM-TYPE = 1 ..... Initial commission*/
		/*        WSAA-COMM-TYPE = 2 ..... Renewal commission*/
		/*        WSAA-COMM-TYPE = 3 ..... Servicing commission*/
		comlinkrec.currto.set(ZERO);
		comlinkrec.targetPrem.set(ZERO);
		comlinkrec.seqno.set(ZERO);
		if (flexiblePremium.isTrue()) {
			readFpco5600();
		}
		for (wsaaCommType.set(1); !(isGT(wsaaCommType, 3)); wsaaCommType.add(1)){
			callSubroutine5300();
		}
		/* decide whether AGCM record needs amending or not.*/
		if (isNE(wsaaUpdateAgcmFlag, SPACES)) {
			rewriteAgcm5500();
		}
	}

protected void readNextAgcm5180()
	{
		agcmrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmrevIO);
		if (isNE(agcmrevIO.getStatuz(), varcom.oK)
		&& isNE(agcmrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(agcmrevIO.getParams());
			syserrrec.statuz.set(agcmrevIO.getStatuz());
			systemError99000();
		}
		if (isNE(reverserec.chdrnum, agcmrevIO.getChdrnum())
		|| isNE(reverserec.company, agcmrevIO.getChdrcoy())
		|| isEQ(agcmrevIO.getStatuz(), varcom.endp)) {
			agcmrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void getCrtable5200()
	{
		start5200();
	}

protected void start5200()
	{
		covrrevIO.setParams(SPACES);
		covrrevIO.setChdrcoy(agcmrevIO.getChdrcoy());
		covrrevIO.setChdrnum(agcmrevIO.getChdrnum());
		covrrevIO.setLife(agcmrevIO.getLife());
		covrrevIO.setCoverage(agcmrevIO.getCoverage());
		covrrevIO.setRider(agcmrevIO.getRider());
		covrrevIO.setPlanSuffix(agcmrevIO.getPlanSuffix());
		covrrevIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrrevIO);
		if (isNE(covrrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrrevIO.getParams());
			syserrrec.statuz.set(covrrevIO.getStatuz());
			systemError99000();
		}
	}

protected void callSubroutine5300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start5300();
				case pass5350: 
					pass5350();
				case exit5390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5300()
	{
		/* zeroise local array variables first*/
		wsaaPayamnt[wsaaCommType.toInt()].set(ZERO);
		wsaaErndamt[wsaaCommType.toInt()].set(ZERO);
		wsaaItem.set(SPACES);
		/* check for initial commission*/
		if (isEQ(wsaaCommType, 1)) {
			if (isEQ(agcmrevIO.getBascpy(), SPACES)) {
				goTo(GotoLabel.exit5390);
			}
			else {
				wsaaItem.set(agcmrevIO.getBascpy());
			}
		}
		/* check for renewal commission*/
		if (isEQ(wsaaCommType, 2)) {
			if (isEQ(agcmrevIO.getRnwcpy(), SPACES)) {
				goTo(GotoLabel.exit5390);
			}
			else {
				wsaaItem.set(agcmrevIO.getRnwcpy());
			}
		}
		/* check for servicing commission*/
		if (isEQ(wsaaCommType, 3)) {
			if (isEQ(agcmrevIO.getSrvcpy(), SPACES)) {
				goTo(GotoLabel.exit5390);
			}
			else {
				wsaaItem.set(agcmrevIO.getSrvcpy());
			}
		}
		/* Now read T5644, the commission release methods table to find*/
		/*  the commission reversal subroutine we are going to call.*/
		readT56445400();
		/* check there is a commission reversal S/R to call*/
		if (isEQ(t5644rec.subrev, SPACES)) {
			goTo(GotoLabel.exit5390);
		}
		/* set up linkage for the commission reversal subroutine*/
		comlinkrec.method.set(wsaaItem);
		comlinkrec.function.set(SPACES);
		comlinkrec.statuz.set(SPACES);
		comlinkrec.agent.set(agcmrevIO.getAgntnum());
		comlinkrec.chdrcoy.set(reverserec.company);
		comlinkrec.chdrnum.set(reverserec.chdrnum);
		comlinkrec.language.set(reverserec.language);
		comlinkrec.life.set(agcmrevIO.getLife());
		comlinkrec.coverage.set(agcmrevIO.getCoverage());
		comlinkrec.rider.set(agcmrevIO.getRider());
		comlinkrec.planSuffix.set(agcmrevIO.getPlanSuffix());
		comlinkrec.seqno.set(agcmrevIO.getSeqno());
		comlinkrec.crtable.set(covrrevIO.getCrtable());
		comlinkrec.jlife.set(covrrevIO.getJlife());
		comlinkrec.agentClass.set(agcmrevIO.getAgentClass());
		comlinkrec.effdate.set(agcmrevIO.getEfdate());
		/* MOVE CHDRCFI-BILLFREQ       TO CLNK-BILLFREQ.                */
		/* MOVE CHDRCFI-BILLFREQ       TO WSAA-BILLING-FREQUENCY.       */
		if (isEQ(t5687rec.singlePremInd, "Y")) {
			comlinkrec.billfreq.set("00");
			wsaaBillingFrequency.set("01");
		}
		else {
			comlinkrec.billfreq.set(payrIO.getBillfreq());
			wsaaBillingFrequency.set(payrIO.getBillfreq());
		}
		comlinkrec.annprem.set(agcmrevIO.getAnnprem());
		compute(comlinkrec.instprem, 2).set(div(agcmrevIO.getAnnprem(), wsaaBillfreq));
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.ptdate.set(ZERO);
		if (flexiblePremium.isTrue()) {
			comlinkrec.icommtot.set(agcmrevIO.getInitcom());
			comlinkrec.instprem.set(ZERO);
			goTo(GotoLabel.pass5350);
		}
		if (isEQ(wsaaCommType, 1)) {
			comlinkrec.icommtot.set(agcmrevIO.getInitcom());
			comlinkrec.icommpd.set(agcmrevIO.getCompay());
			comlinkrec.icommernd.set(agcmrevIO.getComern());
		}
	}

protected void pass5350()
	{
		callProgram(t5644rec.subrev, comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, varcom.oK)) {
			syserrrec.params.set(comlinkrec.clnkallRec);
			syserrrec.statuz.set(comlinkrec.statuz);
			systemError99000();
		}
		zrdecplrec.amountIn.set(comlinkrec.payamnt);
		a000CallRounding();
		comlinkrec.payamnt.set(zrdecplrec.amountOut);
		zrdecplrec.amountIn.set(comlinkrec.erndamt);
		a000CallRounding();
		comlinkrec.erndamt.set(zrdecplrec.amountOut);
		/* store values in array for AGCM update later on*/
		wsaaPayamnt[wsaaCommType.toInt()].set(comlinkrec.payamnt);
		wsaaErndamt[wsaaCommType.toInt()].set(comlinkrec.erndamt);
		/* set update required flag*/
		wsaaUpdateAgcmFlag.set("Y");
	}

protected void readT56445400()
	{
		start5400();
	}

protected void start5400()
	{
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItemtabl(t5644);
		itemIO.setItempfx("IT");
		itemIO.setItemitem(wsaaItem);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			systemError99000();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*5500-REWRITE-AGCM SECTION.                                       
	*5500-START.                                                      
	* Use the AGCMRVS logical view to read, lock & update the         
	* AGCM record with the amended commission values, using the       
	* values from the current AGCMREV logical to provide the key      
	* to reading the AGCMRVS record.                                  
	**** MOVE SPACES                 TO AGCMRVS-PARAMS.               
	**** MOVE AGCMREV-CHDRCOY        TO AGCMRVS-CHDRCOY.              
	**** MOVE AGCMREV-CHDRNUM        TO AGCMRVS-CHDRNUM.              
	**** MOVE AGCMREV-TRANNO         TO AGCMRVS-TRANNO.               
	**** MOVE READH                  TO AGCMRVS-FUNCTION.             
	**** MOVE AGCMRVSREC             TO AGCMRVS-FORMAT.               
	**** CALL 'AGCMRVSIO'            USING AGCMRVS-PARAMS.            
	**** IF AGCMRVS-STATUZ           NOT = O-K                        
	****     MOVE AGCMRVS-PARAMS     TO SYSR-PARAMS                   
	****     MOVE AGCMRVS-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	* update commission paid & earned fields                          
	**** IF AGCMREV-BASCPY           NOT = SPACES                     
	****     MOVE WSAA-PAYAMNT (1)   TO AGCMRVS-COMPAY                
	****     MOVE WSAA-ERNDAMT (1)   TO AGCMRVS-COMERN                
	**** END-IF.                                                      
	**** IF AGCMREV-RNWCPY           NOT = SPACES                     
	****     MOVE WSAA-PAYAMNT (2)   TO AGCMRVS-RNLCDUE               
	****     MOVE WSAA-ERNDAMT (2)   TO AGCMRVS-RNLCEARN              
	**** END-IF.                                                      
	**** IF AGCMREV-SRVCPY           NOT = SPACES                     
	****     MOVE WSAA-PAYAMNT (3)   TO AGCMRVS-SCMDUE                
	****     MOVE WSAA-ERNDAMT (3)   TO AGCMRVS-SCMEARN               
	**** END-IF.                                                      
	**** MOVE REWRT                  TO AGCMRVS-FUNCTION.             
	**** MOVE AGCMRVSREC             TO AGCMRVS-FORMAT.               
	**** CALL 'AGCMRVSIO'            USING AGCMRVS-PARAMS.            
	**** IF AGCMRVS-STATUZ           NOT = O-K                        
	****     MOVE AGCMRVS-PARAMS     TO SYSR-PARAMS                   
	****     MOVE AGCMRVS-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99000-SYSTEM-ERROR                               
	**** END-IF.                                                      
	**** MOVE SPACES                 TO WSAA-UPDATE-AGCM-FLAG.        
	*5590-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void rewriteAgcm5500()
	{
		start5500();
	}

protected void start5500()
	{
		/* As part of AQR fix 4859 <003> the orginal 5500-Rewrite Section  */
		/* has been commented out and replaced by a revised 5500-Rewrite   */
		/* section which has had the read on the AGCMRVS file removed as   */
		/* this rewrite can be done using another file AGCMREV. The        */
		/* AGCMREV logical file is updated with the revised commission     */
		/* values.                                                         */
		/* update commission paid & earned fields                          */
		if (isNE(agcmrevIO.getBascpy(), SPACES)) {
			agcmrevIO.setCompay(wsaaPayamnt[1]);
			agcmrevIO.setComern(wsaaErndamt[1]);
		}
		if (isNE(agcmrevIO.getRnwcpy(), SPACES)) {
			agcmrevIO.setRnlcdue(wsaaPayamnt[2]);
			agcmrevIO.setRnlcearn(wsaaErndamt[2]);
		}
		if (isNE(agcmrevIO.getSrvcpy(), SPACES)) {
			agcmrevIO.setScmdue(wsaaPayamnt[3]);
			agcmrevIO.setScmearn(wsaaErndamt[3]);
		}
		/*                                                         <D9604>*/
		/* donot put negative values on the AGCM                   <D9604> */
		/*                                                         <D9604> */
		if (isLT(agcmrevIO.getCompay(), ZERO)) {
			agcmrevIO.setCompay(ZERO);
		}
		if (isLT(agcmrevIO.getComern(), ZERO)) {
			agcmrevIO.setComern(ZERO);
		}
		if (isLT(agcmrevIO.getRnlcdue(), ZERO)) {
			agcmrevIO.setRnlcdue(ZERO);
		}
		if (isLT(agcmrevIO.getRnlcearn(), ZERO)) {
			agcmrevIO.setRnlcearn(ZERO);
		}
		if (isLT(agcmrevIO.getScmdue(), ZERO)) {
			agcmrevIO.setScmdue(ZERO);
		}
		if (isLT(agcmrevIO.getScmearn(), ZERO)) {
			agcmrevIO.setScmearn(ZERO);
		}
		agcmrevIO.setPtdate(chdrcfiIO.getCcdate());
		/* MOVE REWRT                  TO AGCMREV-FUNCTION.     <LA3993>*/
		agcmrevIO.setFunction(varcom.writd);
		agcmrevIO.setFormat(formatsInner.agcmrevrec);
		SmartFileCode.execute(appVars, agcmrevIO);
		if (isNE(agcmrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(agcmrevIO.getParams());
			syserrrec.statuz.set(agcmrevIO.getStatuz());
			systemError99000();
		}
		wsaaUpdateAgcmFlag.set(SPACES);
	}

protected void readFpco5600()
	{
		start5610();
	}

	/**
	* <pre>
	**************************                                <D9604>
	* </pre>
	*/
protected void start5610()
	{
		fpcolf1IO.setParams(SPACES);
		fpcolf1IO.setChdrcoy(agcmrevIO.getChdrcoy());
		fpcolf1IO.setChdrnum(agcmrevIO.getChdrnum());
		fpcolf1IO.setCoverage(agcmrevIO.getCoverage());
		fpcolf1IO.setPlanSuffix(agcmrevIO.getPlanSuffix());
		fpcolf1IO.setActiveInd(SPACES);
		fpcolf1IO.setTargfrom(ZERO);
		fpcolf1IO.setFunction(varcom.begn);
		fpcolf1IO.setFormat(formatsInner.fpcolf1rec);
		SmartFileCode.execute(appVars, fpcolf1IO);
		if (isNE(fpcolf1IO.getStatuz(), varcom.oK)
		&& isNE(fpcolf1IO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fpcolf1IO.getParams());
			syserrrec.statuz.set(fpcolf1IO.getStatuz());
			systemError99000();
		}
		if (isEQ(fpcolf1IO.getStatuz(), varcom.endp)) {
			return ;
		}
		/*                                                         <D9604>*/
		comlinkrec.targetPrem.set(fpcolf1IO.getTargetPremium());
		comlinkrec.currto.set(fpcolf1IO.getTargto());
	}

protected void processBext6000()
	{
		/*START*/
		/* Delete any Billing Extract (BEXT) records associated with*/
		/*  the contract we are CFIing.*/
		bextrevIO.setParams(SPACES);
		bextrevIO.setFormat(formatsInner.bextrevrec);
		bextrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bextrevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bextrevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		bextrevIO.setChdrcoy(reverserec.company);
		bextrevIO.setChdrnum(reverserec.chdrnum);
		bextrevIO.setBtdate(varcom.vrcmMaxDate);
		while ( !(isEQ(bextrevIO.getStatuz(), varcom.endp))) {
			bextDeletes6100();
		}
		
		/*EXIT*/
	}

protected void bextDeletes6100()
	{
		start6100();
	}

protected void start6100()
	{
		SmartFileCode.execute(appVars, bextrevIO);
		if (isNE(bextrevIO.getStatuz(), varcom.oK)
		&& isNE(bextrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(bextrevIO.getParams());
			syserrrec.statuz.set(bextrevIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(bextrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check BEXT record read is associated with contract*/
		/* - If not, Release record*/
		if (isNE(reverserec.company, bextrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, bextrevIO.getChdrnum())) {
			bextrevIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so BEXT exists for contract, so we will now Read*/
		/*  Lock the record and then delete it.*/
		bextrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, bextrevIO);
		if (isNE(bextrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bextrevIO.getParams());
			syserrrec.statuz.set(bextrevIO.getStatuz());
			databaseError99500();
		}
		bextrevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, bextrevIO);
		if (isNE(bextrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(bextrevIO.getParams());
			syserrrec.statuz.set(bextrevIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record*/
		bextrevIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	* Update of BNFY records no longer needed, so sections removed    
	*7000-PROCESS-BNFY SECTION.                                       
	*7000-START.                                                      
	* Set any Beneficiary records that have validflag set to '1'
	*  and are associated with this contract to have the
	*  validflag set to '2'.
	**** MOVE SPACES                 TO BNFYLNB-PARAMS.               
	**** MOVE BNFYLNBREC             TO BNFYLNB-FORMAT.               
	**** MOVE BEGNH                  TO BNFYLNB-FUNCTION.             
	**** MOVE BEGN                   TO BNFYLNB-FUNCTION.     <LA4556>
	**** MOVE REVE-COMPANY           TO BNFYLNB-CHDRCOY.              
	**** MOVE REVE-CHDRNUM           TO BNFYLNB-CHDRNUM.              
	**** PERFORM 7100-BNFY-AMENDS    UNTIL BNFYLNB-STATUZ = ENDP.     
	*7090-EXIT.                                                       
	**** EXIT.                                                        
	*7100-BNFY-AMENDS SECTION.                                        
	*7100-START.                                                      
	**** CALL 'BNFYLNBIO'            USING BNFYLNB-PARAMS.            
	**** IF BNFYLNB-STATUZ           NOT = O-K                        
	****    AND BNFYLNB-STATUZ       NOT = ENDP                       
	****     MOVE BNFYLNB-PARAMS     TO SYSR-PARAMS                   
	****     MOVE BNFYLNB-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99500-DATABASE-ERROR                             
	**** END-IF.                                                      
	**** IF BNFYLNB-STATUZ           = ENDP                           
	****     GO TO 7190-EXIT                                          
	**** END-IF.                                                      
	* check BNFY record read is associated with contract
	* - If not, Release record
	* RLSE is not needed, as BEGNH is replaced by BEGN, record is     
	* not held.                                                       
	****                                                      <LA4556>
	**** IF REVE-COMPANY             NOT = BNFYLNB-CHDRCOY            
	****    OR REVE-CHDRNUM          NOT = BNFYLNB-CHDRNUM            
	****     MOVE RLSE               TO BNFYLNB-FUNCTION              
	****     CALL 'BNFYLNBIO'        USING BNFYLNB-PARAMS             
	****     IF BNFYLNB-STATUZ       NOT = O-K                        
	****         MOVE BNFYLNB-PARAMS TO SYSR-PARAMS                   
	****         MOVE BNFYLNB-STATUZ TO SYSR-STATUZ                   
	****         PERFORM 99500-DATABASE-ERROR                         
	****     END-IF                                                   
	****     MOVE ENDP               TO BNFYLNB-STATUZ                
	****     GO TO 7190-EXIT                                          
	**** END-IF.                                                      
	* Get here, so BNFY exists for contract, so we will now amend
	**** MOVE REVE-NEW-TRANNO        TO BNFYLNB-TRANNO.               
	**** MOVE '2'                    TO BNFYLNB-VALIDFLAG.            
	**** MOVE REWRT                  TO BNFYLNB-FUNCTION.             
	**** MOVE WRITD                  TO BNFYLNB-FUNCTION.     <LA4556>
	**** CALL 'BNFYLNBIO'            USING BNFYLNB-PARAMS.            
	**** IF BNFYLNB-STATUZ           NOT = O-K                        
	****     MOVE BNFYLNB-PARAMS     TO SYSR-PARAMS                   
	****     MOVE BNFYLNB-STATUZ     TO SYSR-STATUZ                   
	****     PERFORM 99500-DATABASE-ERROR                             
	**** END-IF.                                                      
	* Now get next record
	**** MOVE NEXTR                  TO BNFYLNB-FUNCTION.             
	*7190-EXIT.                                                       
	**** EXIT.                                                        
	* </pre>
	*/
protected void processCovr8000()
	{
		/*START*/
		/* Set COVR coverage/rider records to be CFI'd.*/
		covrcfiIO.setParams(SPACES);
		covrcfiIO.setFormat(formatsInner.covrcfirec);
		/* MOVE BEGNH                  TO COVRCFI-FUNCTION.             */
		covrcfiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrcfiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrcfiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrcfiIO.setChdrcoy(reverserec.company);
		covrcfiIO.setChdrnum(reverserec.chdrnum);
		covrcfiIO.setLife(SPACES);
		covrcfiIO.setCoverage(SPACES);
		covrcfiIO.setRider(SPACES);
		covrcfiIO.setPlanSuffix(ZERO);
		while ( !(isEQ(covrcfiIO.getStatuz(), varcom.endp))) {
			covrAmends8100();
		}
		
		/*EXIT*/
	}

protected void covrAmends8100()
	{
		start8100();
	}

protected void start8100()
	{
		SmartFileCode.execute(appVars, covrcfiIO);
		if (isNE(covrcfiIO.getStatuz(), varcom.oK)
		&& isNE(covrcfiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrcfiIO.getParams());
			syserrrec.statuz.set(covrcfiIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(covrcfiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check COVR record read is associated with contract*/
		/* - If not, Release record*/
		/* RLSE is not needed, as BEGNH is replaced by BEGN, record is     */
		/* not held.                                                       */
		if (isNE(reverserec.company, covrcfiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, covrcfiIO.getChdrnum())) {
			/*     MOVE RLSE               TO COVRCFI-FUNCTION              */
			/*     CALL 'COVRCFIIO'        USING COVRCFI-PARAMS             */
			/*     IF COVRCFI-STATUZ       NOT = O-K                        */
			/*         MOVE COVRCFI-PARAMS TO SYSR-PARAMS                   */
			/*         MOVE COVRCFI-STATUZ TO SYSR-STATUZ                   */
			/*         PERFORM 99500-DATABASE-ERROR                         */
			/*     END-IF                                                   */
			covrcfiIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so COVR exists for contract*/
		/* check whether Life/Coverage/Rider have changed - if so, we*/
		/*  need to call any generic Subroutines that are on T5671*/
		if (isNE(covrcfiIO.getLife(), wsaaLife)
		|| isNE(covrcfiIO.getCoverage(), wsaaCoverage)
		|| isNE(covrcfiIO.getRider(), wsaaRider)
		|| isNE(covrcfiIO.getPlanSuffix(), wsaaPlanSuffix)) {
			genericSubr8200();
			wsaaLife.set(covrcfiIO.getLife());
			wsaaCoverage.set(covrcfiIO.getCoverage());
			wsaaRider.set(covrcfiIO.getRider());
			wsaaPlanSuffix.set(covrcfiIO.getPlanSuffix());
		}
		/* Now update COVR record*/
		covrcfiIO.setTranno(chdrcfiIO.getTranno());
		covrcfiIO.setAnnivProcDate(ZERO);
		covrcfiIO.setRerateDate(ZERO);
		covrcfiIO.setBenBillDate(ZERO);
		covrcfiIO.setCoverageDebt(ZERO);
		wsaaT5687Crtable.set(covrcfiIO.getCrtable());
		wsaaT5687Crrcd.set(covrcfiIO.getCrrcd());
		readT568714000();
		if (isEQ(covrcfiIO.getRider(), "00")
		|| isEQ(covrcfiIO.getRider(), SPACES)) {
			if (isNE(t5679rec.setCovRiskStat, SPACES)) {
				covrcfiIO.setStatcode(t5679rec.setCovRiskStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat, SPACES)) {
				covrcfiIO.setStatcode(t5679rec.setRidRiskStat);
			}
		}
		/* IF CHDRCFI-BILLFREQ         NOT = '00'                       */
		if (isNE(payrIO.getBillfreq(), "00")
		&& isNE(t5687rec.singlePremInd, "Y")) {
			if (isEQ(covrcfiIO.getRider(), "00")
			|| isEQ(covrcfiIO.getRider(), SPACES)) {
				if (isNE(t5679rec.setCovPremStat, SPACES)) {
					covrcfiIO.setPstatcode(t5679rec.setCovPremStat);
				}
			}
			else {
				if (isNE(t5679rec.setRidPremStat, SPACES)) {
					covrcfiIO.setPstatcode(t5679rec.setRidPremStat);
				}
			}
		}
		else {
			if (isEQ(covrcfiIO.getRider(), "00")
			|| isEQ(covrcfiIO.getRider(), SPACES)) {
				if (isNE(t5679rec.setSngpCovStat, SPACES)) {
					covrcfiIO.setPstatcode(t5679rec.setSngpCovStat);
				}
			}
			else {
				if (isNE(t5679rec.setSngpRidStat, SPACES)) {
					covrcfiIO.setPstatcode(t5679rec.setSngpRidStat);
				}
			}
		}
		covrcfiIO.setTermid(reverserec.termid);
		covrcfiIO.setUser(reverserec.user);
		covrcfiIO.setTransactionDate(reverserec.transDate);
		covrcfiIO.setTransactionTime(reverserec.transTime);
		/* MOVE REWRT                  TO COVRCFI-FUNCTION.             */
		covrcfiIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, covrcfiIO);
		if (isNE(covrcfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(covrcfiIO.getParams());
			syserrrec.statuz.set(covrcfiIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record*/
		covrcfiIO.setFunction(varcom.nextr);
	}

protected void genericSubr8200()
	{
		start8200();
	}

protected void start8200()
	{
		/* Read T5671 to see if any Generic processing is required for*/
		/*  the coverage/rider we are processing*/
		itemIO.setParams(SPACES);
		itemIO.setItemcoy(reverserec.company);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5671);
		wsaaT5671Crtable.set(covrcfiIO.getCrtable());
		wsaaT5671Batctrcde.set(reverserec.oldBatctrcde);
		itemIO.setItemitem(wsaaT5671Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			t5671rec.t5671Rec.set(SPACES);
			return ;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		/* Set up linkage area for calls to any of the S/routines on*/
		/*  T5671*/
		greversrec.reverseRec.set(SPACES);
		greversrec.chdrcoy.set(covrcfiIO.getChdrcoy());
		greversrec.chdrnum.set(covrcfiIO.getChdrnum());
		greversrec.tranno.set(reverserec.tranno);
		greversrec.planSuffix.set(covrcfiIO.getPlanSuffix());
		greversrec.life.set(covrcfiIO.getLife());
		greversrec.coverage.set(covrcfiIO.getCoverage());
		greversrec.rider.set(covrcfiIO.getRider());
		greversrec.crtable.set(covrcfiIO.getCrtable());
		greversrec.newTranno.set(reverserec.newTranno);
		greversrec.batckey.set(reverserec.batchkey);
		greversrec.effdate.set(ZERO);
		greversrec.termid.set(reverserec.termid);
		greversrec.user.set(reverserec.user);
		greversrec.transDate.set(reverserec.transDate);
		greversrec.transTime.set(reverserec.transTime);
		greversrec.contractAmount.set(ZERO);
		wsaaSub.set(ZERO);
		greversrec.language.set(reverserec.language);
		/* Now call all the S/Routines specified in the T5671 entry*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)){
			callTrevsubs8300();
		}
	}

protected void callTrevsubs8300()
	{
		/*START*/
		greversrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.trevsub[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.trevsub[wsaaSub.toInt()], greversrec.reverseRec);
			if (isNE(greversrec.statuz, varcom.oK)) {
				syserrrec.params.set(greversrec.reverseRec);
				syserrrec.statuz.set(greversrec.statuz);
				systemError99000();
			}
		}
		/*EXIT*/
	}

protected void processFlup9000()
	{
		/*START*/
		/* Delete any Follow-up ( FLUP ) records associated with*/
		/*  the contract we are CFIing.*/
		fluprevIO.setParams(SPACES);
		fluprevIO.setFormat(formatsInner.fluprevrec);
		fluprevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluprevIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluprevIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		fluprevIO.setChdrcoy(reverserec.company);
		fluprevIO.setChdrnum(reverserec.chdrnum);
		fluprevIO.setTranno(ZERO);
		while ( !(isEQ(fluprevIO.getStatuz(), varcom.endp))) {
			flupDeletes9100();
		}
		
		/*EXIT*/
	}

protected void flupDeletes9100()
	{
		start9100();
	}

protected void start9100()
	{
		SmartFileCode.execute(appVars, fluprevIO);
		if (isNE(fluprevIO.getStatuz(), varcom.oK)
		&& isNE(fluprevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fluprevIO.getParams());
			syserrrec.statuz.set(fluprevIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(fluprevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check FLUP record read is associated with contract*/
		/* - If not, Release record*/
		if (isNE(reverserec.company, fluprevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, fluprevIO.getChdrnum())) {
			fluprevIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so FLUP exists for contract, so we will now Read*/
		/*  lock the record and then delete it.*/
		fluprevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fluprevIO);
		if (isNE(fluprevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprevIO.getParams());
			syserrrec.statuz.set(fluprevIO.getStatuz());
			databaseError99500();
		}
		fluprevIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fluprevIO);
		if (isNE(fluprevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(fluprevIO.getParams());
			syserrrec.statuz.set(fluprevIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record*/
		fluprevIO.setFunction(varcom.nextr);
	}

protected void processPayr10000()
	{
		start10000();
	}

protected void start10000()
	{
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(reverserec.company);
		payrIO.setChdrnum(reverserec.chdrnum);
		payrIO.setPayrseqno(ZERO);
		payrIO.setFormat(formatsInner.payrrec);
		/* MOVE BEGNH                  TO PAYR-FUNCTION.                */
		payrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrIO);
		/* There should be 1 PAYR associated with the contract*/
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, payrIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, payrIO.getChdrnum())) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(e792);
			databaseError99500();
		}
		/* Update fields on PAYR record and rewrite.*/
		payrIO.setTranno(reverserec.newTranno);
		payrIO.setBtdate(chdrcfiIO.getCcdate());
		payrIO.setPtdate(chdrcfiIO.getCcdate());
		payrIO.setBillcd(chdrcfiIO.getCcdate());
		payrIO.setEffdate(chdrcfiIO.getCcdate());
		payrIO.setNextdate(chdrcfiIO.getCcdate());
		payrIO.setPstatcode(chdrcfiIO.getPstatcode());
		payrIO.setTermid(reverserec.termid);
		payrIO.setUser(reverserec.user);
		payrIO.setTransactionDate(reverserec.transDate);
		payrIO.setTransactionTime(reverserec.transTime);
		/* MOVE REWRT                  TO PAYR-FUNCTION.                */
		payrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			databaseError99500();
		}
	}

protected void processLife11000()
	{
		/*START*/
		/* Set any LIFE records that have validflag set to '1'*/
		/*  and are associated with this contract to have the*/
		/*  tranno updated, statii codes set & V/flag left as '1'.*/
		lifecfiIO.setParams(SPACES);
		lifecfiIO.setFormat(formatsInner.lifecfirec);
		/* MOVE BEGNH                  TO LIFECFI-FUNCTION.             */
		lifecfiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifecfiIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifecfiIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		lifecfiIO.setChdrcoy(reverserec.company);
		lifecfiIO.setChdrnum(reverserec.chdrnum);
		while ( !(isEQ(lifecfiIO.getStatuz(), varcom.endp))) {
			lifeAmends11100();
		}
		
		/*EXIT*/
	}

protected void lifeAmends11100()
	{
		start11100();
	}

protected void start11100()
	{
		SmartFileCode.execute(appVars, lifecfiIO);
		if (isNE(lifecfiIO.getStatuz(), varcom.oK)
		&& isNE(lifecfiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifecfiIO.getParams());
			syserrrec.statuz.set(lifecfiIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(lifecfiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check LIFE record read is associated with contract*/
		/* - If not, Release record*/
		/* RLSE is not needed, as BEGNH is replaced by BEGN, record is     */
		/* not held.                                                       */
		if (isNE(reverserec.company, lifecfiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, lifecfiIO.getChdrnum())) {
			/*     MOVE RLSE               TO LIFECFI-FUNCTION              */
			/*     CALL 'LIFECFIIO'        USING LIFECFI-PARAMS             */
			/*     IF LIFECFI-STATUZ       NOT = O-K                        */
			/*         MOVE LIFECFI-PARAMS TO SYSR-PARAMS                   */
			/*         MOVE LIFECFI-STATUZ TO SYSR-STATUZ                   */
			/*         PERFORM 99500-DATABASE-ERROR                         */
			/*     END-IF                                                   */
			lifecfiIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so LIFE exists for contract, so we will now amend*/
		lifecfiIO.setTranno(reverserec.newTranno);
		lifecfiIO.setTermid(reverserec.termid);
		lifecfiIO.setUser(reverserec.user);
		lifecfiIO.setTransactionDate(reverserec.transDate);
		lifecfiIO.setTransactionTime(reverserec.transTime);
		/* MOVE REWRT                  TO LIFECFI-FUNCTION.             */
		lifecfiIO.setFunction(varcom.writd);
		if (isEQ(lifecfiIO.getJlife(), "00")
		|| isEQ(lifecfiIO.getJlife(), SPACES)) {
			if (isNE(t5679rec.setLifeStat, SPACES)) {
				lifecfiIO.setStatcode(t5679rec.setLifeStat);
			}
		}
		else {
			if (isNE(t5679rec.setJlifeStat, SPACES)) {
				lifecfiIO.setStatcode(t5679rec.setJlifeStat);
			}
		}
		SmartFileCode.execute(appVars, lifecfiIO);
		if (isNE(lifecfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifecfiIO.getParams());
			syserrrec.statuz.set(lifecfiIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record*/
		lifecfiIO.setFunction(varcom.nextr);
	}

protected void processLins12000()
	{
		/*START*/
		/* Delete any Life Installment ( LINS ) records associated with*/
		/*  the contract we are CFIing.*/
		linscfiIO.setParams(SPACES);
		linscfiIO.setFormat(formatsInner.linscfirec);
		linscfiIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		linscfiIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		linscfiIO.setChdrcoy(reverserec.company);
		linscfiIO.setChdrnum(reverserec.chdrnum);
		while ( !(isEQ(linscfiIO.getStatuz(), varcom.endp))) {
			linsDeletes12100();
		}
		
		/*EXIT*/
	}

protected void linsDeletes12100()
	{
		start12100();
	}

protected void start12100()
	{
		SmartFileCode.execute(appVars, linscfiIO);
		if (isNE(linscfiIO.getStatuz(), varcom.oK)
		&& isNE(linscfiIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(linscfiIO.getParams());
			syserrrec.statuz.set(linscfiIO.getStatuz());
			databaseError99500();
		}
		if (isEQ(linscfiIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* check LINS record read is associated with contract*/
		/* - If not, Release record*/
		if (isNE(reverserec.company, linscfiIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, linscfiIO.getChdrnum())) {
			linscfiIO.setStatuz(varcom.endp);
			return ;
		}
		/* Get here, so LINS exists for contract, so we will now read*/
		/*  lock the record and then delete it*/
		linscfiIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, linscfiIO);
		if (isNE(linscfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(linscfiIO.getParams());
			syserrrec.statuz.set(linscfiIO.getStatuz());
			databaseError99500();
		}
		linscfiIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, linscfiIO);
		if (isNE(linscfiIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(linscfiIO.getParams());
			syserrrec.statuz.set(linscfiIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record*/
		linscfiIO.setFunction(varcom.nextr);
	}

protected void processTaxd12200()
	{
		/*START*/
		/* Delete all TAXDREV records under the same contract              */
		taxdrevIO.setParams(SPACES);
		taxdrevIO.setFormat(formatsInner.taxdrevrec);
		/* MOVE BEGNH                  TO TAXDREV-FUNCTION.     <LA4758>*/
		taxdrevIO.setFunction(varcom.begn);
		taxdrevIO.setChdrcoy(reverserec.company);
		taxdrevIO.setChdrnum(reverserec.chdrnum);
		taxdrevIO.setEffdate(ZERO);
		while ( !(isEQ(taxdrevIO.getStatuz(), varcom.endp))) {
			taxdDeletes12300();
		}
		
		/*EXIT*/
	}

protected void taxdDeletes12300()
	{
		start12310();
	}

protected void start12310()
	{
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)
		&& isNE(taxdrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.company, taxdrevIO.getChdrcoy())
		|| isNE(reverserec.chdrnum, taxdrevIO.getChdrnum())) {
			taxdrevIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(taxdrevIO.getStatuz(), varcom.endp)) {
			return ;
		}
		/* If TAXDREV exists for contract, issue DELETE                    */
		/* MOVE DELET                  TO TAXDREV-FUNCTION.             */
		taxdrevIO.setFunction(varcom.deltd);
		SmartFileCode.execute(appVars, taxdrevIO);
		if (isNE(taxdrevIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(taxdrevIO.getParams());
			syserrrec.statuz.set(taxdrevIO.getStatuz());
			databaseError99500();
		}
		/* Now get next record                                             */
		taxdrevIO.setFunction(varcom.nextr);
	}

protected void processRtrn13000()
	{
		start13000();
	}

protected void start13000()
	{
		rtrnrevIO.setParams(SPACES);
		rtrnrevIO.setRldgcoy(reverserec.company);
		rtrnrevIO.setRdocnum(reverserec.chdrnum);
		rtrnrevIO.setTranno(reverserec.tranno);
		rtrnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		rtrnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(rtrnrevIO.getStatuz(), varcom.endp))) {
			reverseRtrns13100();
		}
		
	}

protected void reverseRtrns13100()
	{
		start13100();
		nextRtrnrev13180();
	}

protected void start13100()
	{
		/* check if transaction codes match before processing RTRN*/
		if (isNE(reverserec.oldBatctrcde, rtrnrevIO.getBatctrcde())) {
			return ;
		}
		/*     Write a new record to the RTRN file*/
		/*     by calling the subroutine LIFRTRN.*/
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(reverserec.batchkey);
		lifrtrnrec.rdocnum.set(chdrcfiIO.getChdrnum());
		lifrtrnrec.rldgacct.set(chdrcfiIO.getChdrnum());
		lifrtrnrec.tranno.set(reverserec.newTranno);
		lifrtrnrec.jrnseq.set(rtrnrevIO.getTranseq());
		/* MOVE CHDRCFI-CHDRCOY        TO LIFR-RLDGCOY.                 */
		lifrtrnrec.rldgcoy.set(rtrnrevIO.getRldgcoy());
		lifrtrnrec.sacscode.set(rtrnrevIO.getSacscode());
		lifrtrnrec.sacstyp.set(rtrnrevIO.getSacstyp());
		lifrtrnrec.trandesc.set(wsaaTransDesc);
		lifrtrnrec.crate.set(rtrnrevIO.getCrate());
		lifrtrnrec.transactionTime.set(rtrnrevIO.getTrandate());
		lifrtrnrec.transactionDate.set(rtrnrevIO.getTrantime());
		lifrtrnrec.user.set(reverserec.user);
		lifrtrnrec.termid.set(reverserec.termid);
		compute(lifrtrnrec.acctamt, 2).set(mult(rtrnrevIO.getAcctamt(), -1));
		compute(lifrtrnrec.origamt, 2).set(mult(rtrnrevIO.getOrigamt(), -1));
		lifrtrnrec.genlcur.set(rtrnrevIO.getGenlcur());
		lifrtrnrec.origcurr.set(rtrnrevIO.getOrigccy());
		lifrtrnrec.genlcoy.set(rtrnrevIO.getGenlcoy());
		lifrtrnrec.glcode.set(rtrnrevIO.getGlcode());
		lifrtrnrec.glsign.set(rtrnrevIO.getGlsign());
		lifrtrnrec.postyear.set(rtrnrevIO.getPostyear());
		lifrtrnrec.postmonth.set(rtrnrevIO.getPostmonth());
		/* MOVE DTC1-INT-DATE          TO LIFR-EFFDATE.                 */
		lifrtrnrec.effdate.set(rtrnrevIO.getEffdate());
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.contot.set(1);
		lifrtrnrec.substituteCode[1].set(chdrcfiIO.getCnttype());
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			syserrrec.statuz.set(lifrtrnrec.statuz);
			systemError99000();
		}
	}

protected void nextRtrnrev13180()
	{
		rtrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, rtrnrevIO);
		if (isNE(rtrnrevIO.getStatuz(), varcom.oK)
		&& isNE(rtrnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rtrnrevIO.getParams());
			syserrrec.statuz.set(rtrnrevIO.getStatuz());
			databaseError99500();
		}
		if (isNE(reverserec.chdrnum, rtrnrevIO.getRdocnum())
		|| isNE(reverserec.company, rtrnrevIO.getRldgcoy())
		|| isNE(reverserec.tranno, rtrnrevIO.getTranno())) {
			rtrnrevIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void readT568714000()
	{
		para14000();
	}

protected void para14000()
	{
		/*    Read Table T5687 to obtain frequency alteration basis        */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(reverserec.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(wsaaT5687Crtable);
		itdmIO.setItmfrm(wsaaT5687Crrcd);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			systemError99000();
		}
		if ((isNE(itdmIO.getItemcoy(), reverserec.company))
		|| (isNE(itdmIO.getItemtabl(), t5687))
		|| (isNE(itdmIO.getItemitem(), wsaaT5687Crtable))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(f294);
			systemError99000();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void processZptn15000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc15010();
				case call15020: 
					call15020();
					writ15030();
					nextr15080();
				case exit15090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc15010()
	{
		zptnrevIO.setDataArea(SPACES);
		zptnrevIO.setChdrcoy(reverserec.company);
		zptnrevIO.setChdrnum(reverserec.chdrnum);
		zptnrevIO.setTranno(reverserec.tranno);
		zptnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		zptnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zptnrevIO.setFormat(formatsInner.zptnrevrec);
	}

protected void call15020()
	{
		SmartFileCode.execute(appVars, zptnrevIO);
		if (isNE(zptnrevIO.getStatuz(),varcom.oK)
		&& isNE(zptnrevIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zptnrevIO.getStatuz());
			syserrrec.params.set(zptnrevIO.getParams());
			databaseError99500();
		}
		if (isEQ(zptnrevIO.getStatuz(),varcom.endp)
		|| isNE(zptnrevIO.getChdrcoy(),reverserec.company)
		|| isNE(zptnrevIO.getChdrnum(),reverserec.chdrnum)
		|| isNE(zptnrevIO.getTranno(),reverserec.tranno)) {
			zptnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit15090);
		}
	}

protected void writ15030()
	{
		zptnIO.setDataArea(SPACES);
		zptnIO.setChdrcoy(zptnrevIO.getChdrcoy());
		zptnIO.setChdrnum(zptnrevIO.getChdrnum());
		zptnIO.setLife(zptnrevIO.getLife());
		zptnIO.setCoverage(zptnrevIO.getCoverage());
		zptnIO.setRider(zptnrevIO.getRider());
		zptnIO.setTranno(reverserec.newTranno);
		setPrecision(zptnIO.getOrigamt(), 2);
		zptnIO.setOrigamt(mult(zptnrevIO.getOrigamt(),-1));
		zptnIO.setTransCode(reverserec.batctrcde);
		zptnIO.setEffdate(zptnrevIO.getEffdate());
		zptnIO.setBillcd(zptnrevIO.getBillcd());
		zptnIO.setInstfrom(zptnrevIO.getInstfrom());
		zptnIO.setInstto(zptnrevIO.getInstto());
		zptnIO.setTrandate(datcon1rec.intDate);
		zptnIO.setZprflg(zptnrevIO.getZprflg());
		zptnIO.setFormat(formatsInner.zptnrec);
		zptnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zptnIO.getStatuz());
			syserrrec.params.set(zptnIO.getParams());
			databaseError99500();
		}
	}

protected void nextr15080()
	{
		zptnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call15020);
	}

protected void processZctn16000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					proc16010();
				case call16020: 
					call16020();
					writ16030();
					nextr16080();
				case exit16090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void proc16010()
	{
		zctnrevIO.setDataArea(SPACES);
		zctnrevIO.setChdrcoy(reverserec.company);
		zctnrevIO.setChdrnum(reverserec.chdrnum);
		zctnrevIO.setTranno(reverserec.tranno);
		zctnrevIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		zctnrevIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		zctnrevIO.setFormat(formatsInner.zctnrevrec);
	}

protected void call16020()
	{
		SmartFileCode.execute(appVars, zctnrevIO);
		if (isNE(zctnrevIO.getStatuz(), varcom.oK)
		&& isNE(zctnrevIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(zctnrevIO.getStatuz());
			syserrrec.params.set(zctnrevIO.getParams());
			databaseError99500();
		}
		if (isEQ(zctnrevIO.getStatuz(), varcom.endp)
		|| isNE(zctnrevIO.getChdrcoy(), reverserec.company)
		|| isNE(zctnrevIO.getChdrnum(), reverserec.chdrnum)
		|| isNE(zctnrevIO.getTranno(), reverserec.tranno)) {
			zctnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit16090);
		}
	}

protected void writ16030()
	{
		zctnIO.setDataArea(SPACES);
		zctnIO.setAgntcoy(zctnrevIO.getAgntcoy());
		zctnIO.setAgntnum(zctnrevIO.getAgntnum());
		zctnIO.setChdrcoy(zctnrevIO.getChdrcoy());
		zctnIO.setChdrnum(zctnrevIO.getChdrnum());
		zctnIO.setLife(zctnrevIO.getLife());
		zctnIO.setCoverage(zctnrevIO.getCoverage());
		zctnIO.setRider(zctnrevIO.getRider());
		zctnIO.setTranno(reverserec.newTranno);
		setPrecision(zctnIO.getCommAmt(), 2);
		zctnIO.setCommAmt(mult(zctnrevIO.getCommAmt(), -1));
		setPrecision(zctnIO.getPremium(), 2);
		zctnIO.setPremium(mult(zctnrevIO.getPremium(), -1));
		zctnIO.setSplitBcomm(zctnrevIO.getSplitBcomm());
		zctnIO.setTransCode(reverserec.batctrcde);
		zctnIO.setEffdate(zctnrevIO.getEffdate());
		zctnIO.setTrandate(datcon1rec.intDate);
		zctnIO.setZprflg(zctnrevIO.getZprflg());
		zctnIO.setFormat(formatsInner.zctnrec);
		zctnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, zctnIO);
		if (isNE(zctnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zctnIO.getStatuz());
			syserrrec.params.set(zctnIO.getParams());
			databaseError99500();
		}
	}

protected void nextr16080()
	{
		zctnrevIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call16020);
	}

protected void systemError99000()
	{
		start99000();
		exit99490();
	}

protected void start99000()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99490()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void databaseError99500()
	{
		start99500();
		exit99990();
	}

protected void start99500()
	{
		if ((isEQ(syserrrec.statuz, varcom.bomb))) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit99990()
	{
		reverserec.statuz.set(varcom.bomb);
		exit1090();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(reverserec.company);
		zrdecplrec.statuz.set(varcom.oK);
	//	zrdecplrec.currency.set(acmvrevIO.getOrigcurr());
		zrdecplrec.currency.set(acmvpf.getOrigcurr());//ILB-492	
		zrdecplrec.batctrcde.set(reverserec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError99000();
		}
		/*A900-EXIT*/
	}
	protected void processzfmcN100() {
		zfmcpfDAO.updateZfmcValidflag(reverserec.company.toString(), reverserec.chdrnum.toString());	//IBPLIFE-1433
	}
	
	protected void processZdivCustSpecific()
    {
	
    }
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
	private FixedLengthStringData agcmrevrec = new FixedLengthStringData(10).init("AGCMREVREC");
	private FixedLengthStringData agpyagtrec = new FixedLengthStringData(10).init("AGPYAGTREC");
	private FixedLengthStringData agpydocrec = new FixedLengthStringData(10).init("AGPYDOCREC");
	private FixedLengthStringData bextrevrec = new FixedLengthStringData(10).init("BEXTREVREC");
	private FixedLengthStringData chdrcfirec = new FixedLengthStringData(10).init("CHDRCFIREC");
	private FixedLengthStringData covrcfirec = new FixedLengthStringData(10).init("COVRCFIREC");
	private FixedLengthStringData fluprevrec = new FixedLengthStringData(10).init("FLUPREVREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC   ");
	private FixedLengthStringData lifecfirec = new FixedLengthStringData(10).init("LIFECFIREC");
	private FixedLengthStringData linscfirec = new FixedLengthStringData(10).init("LINSCFIREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData arcmrec = new FixedLengthStringData(10).init("ARCMREC");
	private FixedLengthStringData fprmrec = new FixedLengthStringData(10).init("FPRMREC");
	private FixedLengthStringData fpcolf1rec = new FixedLengthStringData(10).init("FPCOLF1REC");
	private FixedLengthStringData zptnrec = new FixedLengthStringData(10).init("ZPTNREC");
	private FixedLengthStringData zctnrec = new FixedLengthStringData(10).init("ZCTNREC");
	private FixedLengthStringData zptnrevrec = new FixedLengthStringData(10).init("ZPTNREVREC");
	private FixedLengthStringData zctnrevrec = new FixedLengthStringData(10).init("ZCTNREVREC");
	private FixedLengthStringData taxdrevrec = new FixedLengthStringData(10).init("TAXDREVREC");
}
}
