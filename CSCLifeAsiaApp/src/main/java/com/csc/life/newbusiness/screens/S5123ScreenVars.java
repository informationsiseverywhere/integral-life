package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5123
 * @version 1.0 generated on 30/08/09 06:35
 * @author Quipoz
 */
public class S5123ScreenVars extends SmartVarModel { 

	/*public FixedLengthStringData dataArea = new FixedLengthStringData(1394);*/
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	/*public FixedLengthStringData dataFields = new FixedLengthStringData(546).isAPartOf(dataArea, 0);*/
	public ZonedDecimalData anbAtCcd = DD.anbccd.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(dataFields,3);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(dataFields,15);
	public FixedLengthStringData crtabdesc = DD.crtabdesc.copy().isAPartOf(dataFields,17);
	public FixedLengthStringData currcd = DD.currcd.copy().isAPartOf(dataFields,47);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,50);
	public FixedLengthStringData jlifcnum = DD.jlifcnum.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData jlinsname = DD.jlinsname.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData lifcnum = DD.lifcnum.copy().isAPartOf(dataFields,124);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,132);
	public FixedLengthStringData linsname = DD.linsname.copy().isAPartOf(dataFields,134);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,181);
	public ZonedDecimalData numapp = DD.numapp.copyToZonedDecimal().isAPartOf(dataFields,182);
	public ZonedDecimalData numavail = DD.numavail.copyToZonedDecimal().isAPartOf(dataFields,186);
	public FixedLengthStringData optextind = DD.optextind.copy().isAPartOf(dataFields,190);
	public FixedLengthStringData optsmode = DD.optsmode.copy().isAPartOf(dataFields,191);
	public FixedLengthStringData payflag = DD.payflag.copy().isAPartOf(dataFields,192);
	public FixedLengthStringData pbind = DD.pbind.copy().isAPartOf(dataFields,193);
	public ZonedDecimalData premCessDate = DD.pcesdte.copyToZonedDecimal().isAPartOf(dataFields,194);
	public ZonedDecimalData premCessAge = DD.pcessage.copyToZonedDecimal().isAPartOf(dataFields,202);
	public ZonedDecimalData premCessTerm = DD.pcesstrm.copyToZonedDecimal().isAPartOf(dataFields,205);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,208);
	public FixedLengthStringData ratypind = DD.ratypind.copy().isAPartOf(dataFields,212);
	public ZonedDecimalData riskCessDate = DD.rcesdte.copyToZonedDecimal().isAPartOf(dataFields,213);
	public ZonedDecimalData riskCessAge = DD.rcessage.copyToZonedDecimal().isAPartOf(dataFields,221);
	public ZonedDecimalData riskCessTerm = DD.rcesstrm.copyToZonedDecimal().isAPartOf(dataFields,224);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(dataFields,227);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,229);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,230);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,231);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(dataFields,233);
	public ZonedDecimalData sumin = DD.sumin.copyToZonedDecimal().isAPartOf(dataFields,237);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,252);
	public FixedLengthStringData taxind = DD.taxind.copy().isAPartOf(dataFields,269);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,270);
	public ZonedDecimalData zbinstprem = DD.zbinstprem.copyToZonedDecimal().isAPartOf(dataFields,283);
	public ZonedDecimalData zlinstprem = DD.zlinstprem.copyToZonedDecimal().isAPartOf(dataFields,300);
	public FixedLengthStringData zsredtrm = DD.zsredtrm.copy().isAPartOf(dataFields,317);
	/*BRD-306 START */
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields, 318);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields, 321);
	public ZonedDecimalData loadper = DD.loadper.copyToZonedDecimal().isAPartOf(dataFields, 351);
	public ZonedDecimalData rateadj = DD.rateadj.copyToZonedDecimal().isAPartOf(dataFields, 368);
	public ZonedDecimalData fltmort = DD.fltmort.copyToZonedDecimal().isAPartOf(dataFields, 385);
	public ZonedDecimalData premadj = DD.premadj.copyToZonedDecimal().isAPartOf(dataFields, 402);
	public ZonedDecimalData adjustageamt = DD.adjustageamt.copyToZonedDecimal().isAPartOf(dataFields, 419);
	/*BRD-306 END */
	public FixedLengthStringData prmbasis = DD.prmbasis.copy().isAPartOf(dataFields,436);//ILIFE-3421
	public FixedLengthStringData dialdownoption = DD.dialdownoption.copy().isAPartOf(dataFields,437);//BRD-NBP-011
	public FixedLengthStringData exclind = DD.optextind.copy().isAPartOf(dataFields,440);
	public FixedLengthStringData lnkgno = DD.lnkgno.copy().isAPartOf(dataFields,441);//ILIFE-6941
	public FixedLengthStringData lnkgsubrefno = DD.lnkgsubrefno.copy().isAPartOf(dataFields,491);//ILIFE-6941
	public FixedLengthStringData tpdtype= DD.tpdtype.copy().isAPartOf(dataFields, 541);//ILIFE-7118
	
	public FixedLengthStringData lnkgind = DD.jpjlnkind.copy().isAPartOf(dataFields, 546);
	public ZonedDecimalData zstpduty01 = DD.zstpduty.copyToZonedDecimal().isAPartOf(dataFields,547);
	public FixedLengthStringData aepaydet = DD.aepaydet.copy().isAPartOf(dataFields,564);
	//IBPLIFE-2132
	public FixedLengthStringData trcode = DD.trcode.copy().isAPartOf(dataFields,565);
	public FixedLengthStringData trcdedesc = DD.trandesc.copy().isAPartOf(dataFields,569);
	public FixedLengthStringData dateeff = DD.dateeff.copy().isAPartOf(dataFields,599);
	public FixedLengthStringData covrprpse = DD.covrprpse.copy().isAPartOf(dataFields,607);
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	/*public FixedLengthStringData errorIndicators = new FixedLengthStringData(212).isAPartOf(dataArea, 546);*/
	public FixedLengthStringData anbccdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bappmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData coverageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData crtabdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData currcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData instPremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData jlinsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData liencdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifcnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData linsnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData numappErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData numavailErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData optextindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData optsmodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData payflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData pbindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData pcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData pcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData pcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData ratypindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData rcesdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData rcessageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData rcesstrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData riderErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData stssectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData suminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData taxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData zbinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData zlinstpremErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData zsredtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	/*BRD-306 START */
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData loadperErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData rateadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData fltmortErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData premadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData adjustageamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData prmbasisErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);//ILIFE-3421
	public FixedLengthStringData dialdownoptionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);//BRD-NBP-011
	public FixedLengthStringData exclindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData lnkgnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);//ILIFE-6941
	public FixedLengthStringData lnkgsubrefnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);//ILIFE-6941
	public FixedLengthStringData tpdtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208); //ILIFE-7118
	public FixedLengthStringData lnkgindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	/*BRD-306 END */
	public FixedLengthStringData zstpduty01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);
	public FixedLengthStringData aepaydetErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);
	//IBPLIFE-2132
	public FixedLengthStringData trcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);
	public FixedLengthStringData trcdedescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 228);
	public FixedLengthStringData dateeffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);
	public FixedLengthStringData covrprpseErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);
	
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	/*public FixedLengthStringData outputIndicators = new FixedLengthStringData(636).isAPartOf(dataArea, 758);*/
	public FixedLengthStringData[] anbccdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bappmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] coverageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] crtabdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] currcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] instPremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] jlinsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] liencdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifcnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] linsnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] numappOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] numavailOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] optextindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] optsmodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] payflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] pbindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] pcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] pcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] pcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] ratypindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] rcesdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] rcessageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] rcesstrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] riderOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] stssectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] suminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] taxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] zbinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] zlinstpremOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] zsredtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	
	/*BRD-306 START */
	public FixedLengthStringData[]cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[]ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[]loadperOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[]rateadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[]fltmortOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[]premadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[]adjustageamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);

	/*BRD-306 END */
	public FixedLengthStringData[]prmbasisOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);//ILIFE-3421
	public FixedLengthStringData[]dialdownoptionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);//BRD-NBP-011
	public FixedLengthStringData[] exclindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] lnkgnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);//ILIFE-6941
	public FixedLengthStringData[]  lnkgsubrefnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);//ILIFE-6941
	public FixedLengthStringData[]  tpdtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);//ILIFE-7118
	
	public FixedLengthStringData[]  lnkgindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[]zstpduty01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);
	public FixedLengthStringData[] aepaydetOut  = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);
	public FixedLengthStringData[] riskCessAgeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);//ILJ-43
	//public FixedLengthStringData[] riskCessDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);//ILJ-43
	//IBPLIFE-2132
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[] trcdedescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);
	public FixedLengthStringData[] dateeffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);
	public FixedLengthStringData[] covrprpseOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);
	
		
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData premCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData riskCessDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData dateeffDisp = new FixedLengthStringData(10);//IBPLIFE-2132
	
	public LongData S5123screenWritten = new LongData(0);
	public LongData S5123protectWritten = new LongData(0);
	public FixedLengthStringData contnewBScreenflag = new FixedLengthStringData(1); 

	
	public static int[] screenSflPfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 

	public boolean hasSubfile() {
		return false;
	}


	public S5123ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(zagelitOut,new String[] {null, null, "12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(suminOut,new String[] {"14","02","-14","01",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcessageOut,new String[] {"15","03","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesstrmOut,new String[] {"16","04","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(rcesdteOut,new String[] {"29","28","-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcessageOut,new String[] {"17","41","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesstrmOut,new String[] {"18","25","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pcesdteOut,new String[] {"30","28","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"19","06","-19","05",null, null, null, null, null, null, null, null});
		fieldIndMap.put(liencdOut,new String[] {"20","08","-20","07",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bappmethOut,new String[] {"36","37","-36","37",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optextindOut,new String[] {"21","09","-21","09",null, null, null, null, null, null, null, null});
		fieldIndMap.put(instPremOut,new String[] {"22","31","-22","66",null, null, null, null, null, null, null, null});
		fieldIndMap.put(ratypindOut,new String[] {"23","40","-01","40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(pbindOut,new String[] {"33","34","-33","34",null, null, null, null, null, null, null, null});
		fieldIndMap.put(polincOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"24","26","-23","27",null, null, null, null, null, null, null, null});
		fieldIndMap.put(numavailOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(numappOut,new String[] {"25","11","-24","10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zlinstpremOut,new String[] {null, null, null, "32",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optsmodeOut,new String[] {"35","36","-35","36",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payflagOut,new String[] {"38","36","-38","36",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zsredtrmOut,new String[] {"42","39","-42","39",null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxindOut,new String[] {"43","44","-43","43",null, null, null, null, null, null, null, null});
		//ILIFE-1223 STARTS
		fieldIndMap.put(taxamtOut,new String[] {null, "46", null, "45",null, null, null, null, null, null, null, null});
		//ILIFE-1223 ENDS
		/*BRD-306 START */
		fieldIndMap.put(cnttypeOut,new String[] {null, null, null, "47", null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctypedesOut,new String[] {null, null, null, "48", null, null, null, null, null, null, null, null});
		fieldIndMap.put(loadperOut,new String[] {null, null, null, "49", null, null, null, null, null, null, null, null});
		fieldIndMap.put(rateadjOut,new String[] {null, null, null, "50", null, null, null, null, null, null, null, null});
		fieldIndMap.put(fltmortOut,new String[] {null, null, null, "51", null, null, null, null, null, null, null, null});
		fieldIndMap.put(premadjOut,new String[] {null, null, null, "52", null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjustageamtOut,new String[] {null, null, null, "53", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zbinstpremOut,new String[] {null, null, null, "54", null, null, null, null, null, null, null, null});
		/*BRD-306 END */
		fieldIndMap.put(zstpduty01Out,new String[] {null, null, null, "79", null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmbasisOut,new String[] {"55", "56", null, "57", null, null, null, null, null, null, null, null});////ILIFE-3421
		fieldIndMap.put(dialdownoptionOut,new String[] {"72", "70", "-72", "71", null, null, null, null, null, null, null, null});//BRD-NBP-011
		fieldIndMap.put(exclindOut,new String[] {null, null, null, "73", null, null, null, null, null, null, null, null});
		fieldIndMap.put(lnkgnoOut,new String[] {null, "60", null, "58", null, null, null, null, null, null, null, null}); //ILIFE-6941
		fieldIndMap.put(lnkgsubrefnoOut,new String[] {null,"61", null, "59", null, null, null, null, null, null, null, null});//ILIFE-6941
		fieldIndMap.put(tpdtypeOut,new String[] {"78",null, null, "77", null, null, null, null, null, null, null, null}); //ILIFE-7118
		fieldIndMap.put(lnkgindOut,new String[] {null,"63", null, "62", null, null, null, null, null, null, null, null});
		fieldIndMap.put(aepaydetOut,new String[] {"64","65", "-64", "67", null, null, null, null, null, null, null, null});
		fieldIndMap.put(riskCessAgeOut,new String[] {null, null, null, "123", null, null, null, null, null, null, null, null});//ILJ-43
		//fieldIndMap.put(riskCessDateOut,new String[] {null, null, null, "124", null, null, null, null, null, null, null, null});//ILJ-43
		
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields =getscreenDateDispFields();
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5123screen.class;
		protectRecord = S5123protect.class;
	}
	
	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	public int getDataAreaSize()
	{
		return 1679;
	}
	public int getDataFieldsSize()
	{
		return 707;
	}
	public int getErrorIndicatorSize()
	{
		return 240;
	}
	public int getOutputFieldSize()
	{
		return 732;
	}
	public BaseData[] getscreenFields(){
		return new BaseData[] {zbinstprem, crtabdesc, chdrnum, life, coverage, rider, lifcnum, linsname, zagelit, anbAtCcd, statFund, statSect, statSubsect, jlifcnum, jlinsname, sumin, currcd, riskCessAge, riskCessTerm, riskCessDate, premCessAge, premCessTerm, premCessDate, mortcls, liencd, bappmeth, optextind, instPrem, ratypind, pbind, polinc, select, numavail, numapp, zlinstprem, optsmode, payflag, zsredtrm, taxind, taxamt, loadper, rateadj, fltmort, premadj, cnttype, ctypedes,adjustageamt, prmbasis, dialdownoption,exclind,lnkgno,lnkgsubrefno,tpdtype,lnkgind,zstpduty01,aepaydet,trcode,trcdedesc,dateeff,covrprpse};
	}
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {zbinstpremOut, crtabdescOut, chdrnumOut, lifeOut, coverageOut, riderOut, lifcnumOut, linsnameOut, zagelitOut, anbccdOut, stfundOut, stsectOut, stssectOut, jlifcnumOut, jlinsnameOut, suminOut, currcdOut, rcessageOut, rcesstrmOut, rcesdteOut, pcessageOut, pcesstrmOut, pcesdteOut, mortclsOut, liencdOut, bappmethOut, optextindOut, instPremOut, ratypindOut, pbindOut, polincOut, selectOut, numavailOut, numappOut, zlinstpremOut, optsmodeOut, payflagOut, zsredtrmOut, taxindOut, taxamtOut,loadperOut, rateadjOut, rateadjOut, premadjOut, cnttypeOut , ctypedesOut,adjustageamtOut, prmbasisOut, dialdownoptionOut,exclindOut,lnkgnoOut,lnkgsubrefnoOut,tpdtypeOut,lnkgindOut,zstpduty01Out,aepaydetOut,riskCessAgeOut,trcodeOut,trcdedescOut,dateeffOut,covrprpseOut};
	}
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {zbinstpremErr, crtabdescErr, chdrnumErr, lifeErr, coverageErr, riderErr, lifcnumErr, linsnameErr, zagelitErr, anbccdErr, stfundErr, stsectErr, stssectErr, jlifcnumErr, jlinsnameErr, suminErr, currcdErr, rcessageErr, rcesstrmErr, rcesdteErr, pcessageErr, pcesstrmErr, pcesdteErr, mortclsErr, liencdErr, bappmethErr, optextindErr, instPremErr, ratypindErr, pbindErr, polincErr, selectErr, numavailErr, numappErr, zlinstpremErr, optsmodeErr, payflagErr, zsredtrmErr, taxindErr, taxamtErr,loadperErr, rateadjErr, fltmortErr, premadjErr, cnttypeErr, ctypedesErr,adjustageamtErr, prmbasisErr, dialdownoptionErr,exclindErr,lnkgnoErr,lnkgsubrefnoErr,tpdtypeErr,lnkgindErr,zstpduty01Err,aepaydetErr,trcodeErr,trcdedescErr,dateeffErr,covrprpseErr};
	}
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {riskCessDate, premCessDate, dateeff};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {riskCessDateDisp, premCessDateDisp, dateeffDisp};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {rcesdteErr, pcesdteErr, dateeffErr};
	}

}

