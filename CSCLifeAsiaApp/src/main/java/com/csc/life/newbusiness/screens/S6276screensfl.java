package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6276screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {17, 4, 22, 18, 5, 23, 15, 24, 16, 7, 1, 2, 3, 12, 21}; 
	public static int maxRecords = 18;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {1, 2}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {5, 21, 5, 74}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6276ScreenVars sv = (S6276ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.s6276screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.s6276screensfl, 
			sv.S6276screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		S6276ScreenVars sv = (S6276ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.s6276screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		S6276ScreenVars sv = (S6276ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.s6276screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.S6276screensflWritten.gt(0))
		{
			sv.s6276screensfl.setCurrentIndex(0);
			sv.S6276screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		S6276ScreenVars sv = (S6276ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.s6276screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6276ScreenVars screenVars = (S6276ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.bankkey.setFieldName("bankkey");
				screenVars.bankacckey.setFieldName("bankacckey");
				screenVars.mandref.setFieldName("mandref");
				screenVars.mandstat.setFieldName("mandstat");
				screenVars.select.setFieldName("select");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.effdateDisp.set(dm.getField("effdateDisp"));
			screenVars.bankkey.set(dm.getField("bankkey"));
			screenVars.bankacckey.set(dm.getField("bankacckey"));
			screenVars.mandref.set(dm.getField("mandref"));
			screenVars.mandstat.set(dm.getField("mandstat"));
			screenVars.select.set(dm.getField("select"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			S6276ScreenVars screenVars = (S6276ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.effdateDisp.setFieldName("effdateDisp");
				screenVars.bankkey.setFieldName("bankkey");
				screenVars.bankacckey.setFieldName("bankacckey");
				screenVars.mandref.setFieldName("mandref");
				screenVars.mandstat.setFieldName("mandstat");
				screenVars.select.setFieldName("select");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("effdateDisp").set(screenVars.effdateDisp);
			dm.getField("bankkey").set(screenVars.bankkey);
			dm.getField("bankacckey").set(screenVars.bankacckey);
			dm.getField("mandref").set(screenVars.mandref);
			dm.getField("mandstat").set(screenVars.mandstat);
			dm.getField("select").set(screenVars.select);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		S6276screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		S6276ScreenVars screenVars = (S6276ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.effdateDisp.clearFormatting();
		screenVars.bankkey.clearFormatting();
		screenVars.bankacckey.clearFormatting();
		screenVars.mandref.clearFormatting();
		screenVars.mandstat.clearFormatting();
		screenVars.select.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		S6276ScreenVars screenVars = (S6276ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.mandref.setClassString("");
		screenVars.mandstat.setClassString("");
		screenVars.select.setClassString("");
	}

/**
 * Clear all the variables in S6276screensfl
 */
	public static void clear(VarModel pv) {
		S6276ScreenVars screenVars = (S6276ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.bankkey.clear();
		screenVars.bankacckey.clear();
		screenVars.mandref.clear();
		screenVars.mandstat.clear();
		screenVars.select.clear();
	}
}
