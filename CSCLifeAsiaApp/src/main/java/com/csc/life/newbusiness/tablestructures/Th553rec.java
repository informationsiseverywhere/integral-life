package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:24
 * Description:
 * Copybook name: TH553REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th553rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th553Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData scrate = new ZonedDecimalData(12, 7).isAPartOf(th553Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(488).isAPartOf(th553Rec, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th553Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th553Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}