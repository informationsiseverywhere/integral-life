package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5075
 * @version 1.0 generated on 30/08/09 06:33
 * @author Quipoz
 */
public class S5075ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(dataFields,57);
	public ZonedDecimalData billcd = DD.billcd.copyToZonedDecimal().isAPartOf(dataFields,61);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,69);
	public FixedLengthStringData campaign = DD.campaign.copy().isAPartOf(dataFields,71);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,77);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,85);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,87);
	public FixedLengthStringData conprosal = DD.conprosal.copy().isAPartOf(dataFields,90);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,91);
	public FixedLengthStringData heading = DD.heading.copy().isAPartOf(dataFields,121);
	public ZonedDecimalData lassured = DD.lassured.copyToZonedDecimal().isAPartOf(dataFields,151);
	public FixedLengthStringData lrepnum = DD.lrepnum.copy().isAPartOf(dataFields,153);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,173);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,174);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,182);
	public FixedLengthStringData ownersel = DD.ownersel.copy().isAPartOf(dataFields,229);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,239);
	public FixedLengthStringData premStatDesc = DD.pstatdsc.copy().isAPartOf(dataFields,243);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(dataFields,253);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,257);
	public FixedLengthStringData reptype = DD.reptype.copy().isAPartOf(dataFields,260);
	public FixedLengthStringData resndesc = DD.resndesc.copy().isAPartOf(dataFields,262);
	public FixedLengthStringData sbrdesc = DD.sbrdesc.copy().isAPartOf(dataFields,312);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(dataFields,352);
	public FixedLengthStringData statdsc = DD.statdsc.copy().isAPartOf(dataFields,354);
	public FixedLengthStringData stcal = DD.stcal.copy().isAPartOf(dataFields,364);
	public FixedLengthStringData stcbl = DD.stcbl.copy().isAPartOf(dataFields,367);
	public FixedLengthStringData stccl = DD.stccl.copy().isAPartOf(dataFields,370);
	public FixedLengthStringData stcdl = DD.stcdl.copy().isAPartOf(dataFields,373);
	public FixedLengthStringData stcel = DD.stcel.copy().isAPartOf(dataFields,376);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData batctrcdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData billcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData campaignErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData conprosalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData headingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData lassuredErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lrepnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ownerselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData pstatdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData reptypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData resndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData sbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData srcebusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData statdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData stcalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData stcblErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData stcclErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData stcdlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData stcelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] batctrcdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] billcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] campaignOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] conprosalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] headingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] lassuredOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lrepnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ownerselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] pstatdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] reptypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] resndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] sbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] srcebusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] statdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] stcalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] stcblOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] stcclOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] stcdlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] stcelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);

	public LongData S5075screenWritten = new LongData(0);
	public LongData S5075protectWritten = new LongData(0);

	public static int[] screenSflPfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 20, 21, 22, 23, 24,38,33};

	public boolean hasSubfile() {
		return false;
	}


	public S5075ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(polincOut,new String[] {null, null, null, "48",null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5075screen.class;
		protectRecord = S5075protect.class;
	}
	
	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {occdate,billcd};
	}
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {occdateDisp,billcdDisp};//SGS001
	}
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {occdateErr,billcdErr};
	}
	
	
	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	
	public int getDataAreaSize() {
		return 891;
	}
	public int getDataFieldsSize(){
		return 379;
	}
	public int getErrorIndicatorSize(){
		return 128 ; 
	}
	public int getOutputFieldSize(){
		return 384; 
	}
	
	public BaseData[] getscreenFields(){
		return new BaseData[] {heading, chdrnum, ctypedes, statdsc, premStatDesc, ownersel, ownername, occdate, lassured, billfreq, conprosal, mop, billcd, polinc, cntcurr, register, srcebus, reptype, lrepnum, cntbranch, sbrdesc, agntsel, agentname, campaign, stcal, stcbl, stccl, stcdl, stcel, reasoncd, resndesc, batctrcde};
	}
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][]  {headingOut, chdrnumOut, ctypedesOut, statdscOut, pstatdscOut, ownerselOut, ownernameOut, occdateOut, lassuredOut, billfreqOut, conprosalOut, mopOut, billcdOut, polincOut, cntcurrOut, registerOut, srcebusOut, reptypeOut, lrepnumOut, cntbranchOut, sbrdescOut, agntselOut, agentnameOut, campaignOut, stcalOut, stcblOut, stcclOut, stcdlOut, stcelOut, reasoncdOut, resndescOut, batctrcdeOut};
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[]  {headingErr, chdrnumErr, ctypedesErr, statdscErr, pstatdscErr, ownerselErr, ownernameErr, occdateErr, lassuredErr, billfreqErr, conprosalErr, mopErr, billcdErr, polincErr, cntcurrErr, registerErr, srcebusErr, reptypeErr, lrepnumErr, cntbranchErr, sbrdescErr, agntselErr, agentnameErr, campaignErr, stcalErr, stcblErr, stcclErr, stcdlErr, stcelErr, reasoncdErr, resndescErr, batctrcdeErr};
		
	}
}
