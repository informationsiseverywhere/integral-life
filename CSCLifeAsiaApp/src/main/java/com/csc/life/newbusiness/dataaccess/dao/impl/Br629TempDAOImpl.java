package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.Br629TempDAO;
import com.csc.life.newbusiness.dataaccess.model.Br629DTO;
import com.csc.life.regularprocessing.dataaccess.model.B5018DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br629TempDAOImpl extends BaseDAOImpl<Br629DTO> implements Br629TempDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Br629TempDAOImpl.class);
    
	private final static String TABLE_NAME = "BR629DATA"; //ILB-475
	
	private void deleteTempData() {
		String sqlStr = "DELETE FROM " + TABLE_NAME;
		PreparedStatement ps = getPrepareStatement(sqlStr);
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("deleteTempData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	private void insertTempData(String tableName, String clntpfx, String clntcoy) {
        StringBuilder sqlStr = new StringBuilder("INSERT INTO ");
        sqlStr.append(TABLE_NAME);
        sqlStr.append(" (CHDRPFX, CHDRCOY, CHDRNUM, COWNPFX, COWNCOY, COWNNUM, CNTTYPE, OCCDATE, TRANNO, STATCODE, PSTCDE, ZSUFCDTE, LSURNAME, LGIVNAME, LSURNAME2, LGIVNAME2, CLTDOB, LIFCNUM ");
        sqlStr.append(")");
        sqlStr.append(" SELECT ");
        sqlStr.append("CH.CHDRPFX, CH.CHDRCOY, CH.CHDRNUM, CH.COWNPFX, CH.COWNCOY, CH.COWNNUM, CH.CNTTYPE, CH.OCCDATE, CH.TRANNO, CH.STATCODE, CH.PSTCDE, HP.ZSUFCDTE, CL1.LSURNAME, CL1.LGIVNAME, CL2.LSURNAME, CL2.LGIVNAME, CL2.CLTDOB, LI.LIFCNUM");
        sqlStr.append(" FROM ");
        sqlStr.append(tableName);
        sqlStr.append(" HP");
        sqlStr.append(" JOIN CHDRPF CH ON CH.CHDRCOY = HP.CHDRCOY AND CH.CHDRNUM = HP.CHDRNUM AND CH.SERVUNIT = 'LP' AND CH.VALIDFLAG = '1'"); 
        sqlStr.append(" LEFT JOIN CLNTPF CL1 ON CL1.CLNTPFX = ? AND CL1.CLNTCOY = ? AND CL1.CLNTNUM = CH.COWNNUM AND CL1.VALIDFLAG = '1'"); 
        sqlStr.append(" LEFT JOIN LIFEPF LI ON LI.CHDRCOY = HP.CHDRCOY AND LI.CHDRNUM = HP.CHDRNUM AND LI.LIFE = '01' AND LI.JLIFE = '00' AND LI.VALIDFLAG = '1'"); 
        sqlStr.append(" LEFT JOIN CLNTPF CL2 ON CL2.CLNTPFX = ? AND CL2.CLNTCOY = ? AND CL2.CLNTNUM = LI.LIFCNUM AND CL2.VALIDFLAG = '1'"); 
        sqlStr.append(" WHERE CH.OCCDATE < = HP.ZSUFCDTE");
		PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		try {
			ps.setString(1, clntpfx);
			ps.setString(2, clntcoy);
			ps.setString(3, clntpfx);
			ps.setString(4, clntcoy);
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertTempData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

	}
	
	@Override
	public void buildTempData(String tableName, String clntpfx, String clntcoy) {
		deleteTempData();
		insertTempData(tableName, clntpfx, clntcoy);
	}

	@Override
	public List<Br629DTO> findTempDataResult(int batchExtractSize, int batchID) {
        StringBuilder sqlStr = new StringBuilder();
        sqlStr.append("SELECT * FROM (");
        sqlStr.append("SELECT FLOOR((ROW_NUMBER() OVER (ORDER BY TE.CHDRPFX, TE.CHDRCOY, TE.CHDRNUM, TE.COWNPFX, TE.COWNCOY, TE.COWNNUM)-1)/?) ROWNM, TE.* from BR629DATA TE)cc where cc.rownm=? ");  //ILB-475

        PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		List<Br629DTO> dtoList = new LinkedList<Br629DTO>();
		ResultSet rs = null;
        try {
        	ps.setInt(1, batchExtractSize);
        	ps.setInt(2, batchID);

        	rs = ps.executeQuery();

            while (rs.next()) {
            	Br629DTO dto = new Br629DTO();
          	
            	dto.setChdrpfx(rs.getString("CHDRPFX"));
            	dto.setChdrcoy(rs.getString("CHDRCOY"));
            	dto.setChdrnum(rs.getString("CHDRNUM"));
            	dto.setCownpfx(rs.getString("COWNPFX"));
            	dto.setCowncoy(rs.getString("COWNCOY"));
            	dto.setCownnum(rs.getString("COWNNUM"));
            	dto.setCnttype(rs.getString("CNTTYPE"));
            	dto.setTranno(rs.getInt("TRANNO"));
            	dto.setOccdate(rs.getInt("OCCDATE"));
            	dto.setStatcode(rs.getString("STATCODE"));
            	dto.setPstcde(rs.getString("PSTCDE"));
            	dto.setZsufcdte(rs.getInt("ZSUFCDTE"));
            	dto.setCl1lsurname(rs.getString("LSURNAME"));
            	dto.setCl1lgivname(rs.getString("LGIVNAME"));
            	dto.setCl2lsurname(rs.getString("LSURNAME2"));
            	dto.setCl2lgivname(rs.getString("LGIVNAME2"));
            	dto.setCl2cltdob(rs.getString("CLTDOB"));
            	dto.setLifcnum(rs.getString("LIFCNUM"));
                
                dtoList.add(dto);
            }

        } catch (SQLException e) {
            LOGGER.error("findTempDataResult()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return dtoList;

	}

}