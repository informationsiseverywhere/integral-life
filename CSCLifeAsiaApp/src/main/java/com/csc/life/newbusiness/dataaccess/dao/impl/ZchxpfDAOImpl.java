package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.ZchxpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Zchxpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZchxpfDAOImpl extends BaseDAOImpl<Zchxpf> implements ZchxpfDAO {

private static final Logger LOGGER = LoggerFactory.getLogger(ZchxpfDAOImpl.class);
	
	public List<Zchxpf> findResults(String tableId, String memName, int batchExtractSize, int batchID) {
		StringBuilder sql = new StringBuilder("SELECT * FROM (");
        sql.append("SELECT ROW_NUMBER()OVER(ORDER BY TE.UNIQUE_NUMBER DESC) ROWNM, TE.* FROM ");
        sql.append(tableId);
        sql.append(" TE");
        sql.append(" WHERE MEMBER_NAME = ? )cc WHERE cc.ROWNM > ? AND cc.ROWNM <= ?");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Zchxpf pf = null;
		List<Zchxpf> list = new ArrayList<>();
		int startIdx = batchID * batchExtractSize;
		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, memName);
			ps.setInt(2, startIdx);
			ps.setInt(3, startIdx+batchExtractSize);
			rs = ps.executeQuery();
			while (rs.next()) {
				pf = new Zchxpf();
				pf.setChdrcoy(rs.getString("CHDRCOY"));
				pf.setChdrnum(rs.getString("CHDRNUM"));
				pf.setChdrpfx(rs.getString("CHDRPFX"));
				pf.setPstcde(rs.getString("PSTCDE"));
				pf.setStatcode(rs.getString("STATCODE"));
				list.add(pf);
			}
		} catch (SQLException e) {
			LOGGER.error("findResults()".concat(e.getMessage()));
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return list;
	}

}
