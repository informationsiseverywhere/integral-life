package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * @version 1.0 generated on 30/08/09 05:49
 * @author Quipoz
 */
public class Sr590screensfl extends Subfile { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {17, 4, 22, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21, 20}; 
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] {30, 42, 41, 32, 40, 31, 36, 33, 34, 39, 38, 35, 38}; 

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {11, 15, 2, 61}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr590ScreenVars sv = (Sr590ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sr590screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sr590screensfl, 
			sv.Sr590screensflWritten , ind2, ind3, maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv,
		Indicator ind2) {
		Sr590ScreenVars sv = (Sr590ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sr590screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3, DecimalData sflIndex) {
		Sr590ScreenVars sv = (Sr590ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sr590screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sr590screensflWritten.gt(0))
		{
			sv.sr590screensfl.setCurrentIndex(0);
			sv.Sr590screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		Sr590ScreenVars sv = (Sr590ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sr590screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}
	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn()) av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv,
		BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr590ScreenVars screenVars = (Sr590ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.fupno.setFieldName("fupno");
				screenVars.uprflag.setFieldName("uprflag");
				screenVars.zitem.setFieldName("zitem");
				screenVars.indic.setFieldName("indic");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.language.setFieldName("language");
				screenVars.fupcdes.setFieldName("fupcdes");
				screenVars.lifeno.setFieldName("lifeno");
				screenVars.jlife.setFieldName("jlife");
				screenVars.fupstat.setFieldName("fupstat");
				screenVars.fupremdtDisp.setFieldName("fupremdtDisp");
				screenVars.crtdateDisp.setFieldName("crtdateDisp");
				screenVars.fupremk.setFieldName("fupremk");
				screenVars.actn.setFieldName("actn");
				screenVars.select.setFieldName("select");
				screenVars.fuptype.setFieldName("fuptype");
				screenVars.fuprcvdDisp.setFieldName("fuprcvdDisp");
				screenVars.exprdateDisp.setFieldName("exprdateDisp");
			}
			screenVars.screenIndicArea.set(dm.getField("screenIndicArea"));
			screenVars.fupno.set(dm.getField("fupno"));
			screenVars.uprflag.set(dm.getField("uprflag"));
			screenVars.zitem.set(dm.getField("zitem"));
			screenVars.indic.set(dm.getField("indic"));
			screenVars.crtuser.set(dm.getField("crtuser"));
			screenVars.language.set(dm.getField("language"));
			screenVars.fupcdes.set(dm.getField("fupcdes"));
			screenVars.lifeno.set(dm.getField("lifeno"));
			screenVars.jlife.set(dm.getField("jlife"));
			screenVars.fupstat.set(dm.getField("fupstat"));
			screenVars.fupremdtDisp.set(dm.getField("fupremdtDisp"));
			screenVars.crtdateDisp.set(dm.getField("crtdateDisp"));
			screenVars.fupremk.set(dm.getField("fupremk"));
			screenVars.actn.set(dm.getField("actn"));
			screenVars.select.set(dm.getField("select"));
			screenVars.fuptype.set(dm.getField("fuptype"));
			screenVars.fuprcvdDisp.set(dm.getField("fuprcvdDisp"));
			screenVars.exprdateDisp.set(dm.getField("exprdateDisp"));
		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av,
		 VarModel pv) {
		if (dm != null) {
			Sr590ScreenVars screenVars = (Sr590ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName("screenIndicArea");
				screenVars.fupno.setFieldName("fupno");
				screenVars.uprflag.setFieldName("uprflag");
				screenVars.zitem.setFieldName("zitem");
				screenVars.indic.setFieldName("indic");
				screenVars.crtuser.setFieldName("crtuser");
				screenVars.language.setFieldName("language");
				screenVars.fupcdes.setFieldName("fupcdes");
				screenVars.lifeno.setFieldName("lifeno");
				screenVars.jlife.setFieldName("jlife");
				screenVars.fupstat.setFieldName("fupstat");
				screenVars.fupremdtDisp.setFieldName("fupremdtDisp");
				screenVars.crtdateDisp.setFieldName("crtdateDisp");
				screenVars.fupremk.setFieldName("fupremk");
				screenVars.actn.setFieldName("actn");
				screenVars.select.setFieldName("select");
				screenVars.fuptype.setFieldName("fuptype");
				screenVars.fuprcvdDisp.setFieldName("fuprcvdDisp");
				screenVars.exprdateDisp.setFieldName("exprdateDisp");
			}
			dm.getField("screenIndicArea").set(screenVars.screenIndicArea);
			dm.getField("fupno").set(screenVars.fupno);
			dm.getField("uprflag").set(screenVars.uprflag);
			dm.getField("zitem").set(screenVars.zitem);
			dm.getField("indic").set(screenVars.indic);
			dm.getField("crtuser").set(screenVars.crtuser);
			dm.getField("language").set(screenVars.language);
			dm.getField("fupcdes").set(screenVars.fupcdes);
			dm.getField("lifeno").set(screenVars.lifeno);
			dm.getField("jlife").set(screenVars.jlife);
			dm.getField("fupstat").set(screenVars.fupstat);
			dm.getField("fupremdtDisp").set(screenVars.fupremdtDisp);
			dm.getField("crtdateDisp").set(screenVars.crtdateDisp);
			dm.getField("fupremk").set(screenVars.fupremk);
			dm.getField("actn").set(screenVars.actn);
			dm.getField("select").set(screenVars.select);
			dm.getField("fuptype").set(screenVars.fuptype);
			dm.getField("fuprcvdDisp").set(screenVars.fuprcvdDisp);
			dm.getField("exprdateDisp").set(screenVars.exprdateDisp);
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sr590screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sr590ScreenVars screenVars = (Sr590ScreenVars)pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.fupno.clearFormatting();
		screenVars.uprflag.clearFormatting();
		screenVars.zitem.clearFormatting();
		screenVars.indic.clearFormatting();
		screenVars.crtuser.clearFormatting();
		screenVars.language.clearFormatting();
		screenVars.fupcdes.clearFormatting();
		screenVars.lifeno.clearFormatting();
		screenVars.jlife.clearFormatting();
		screenVars.fupstat.clearFormatting();
		screenVars.fupremdtDisp.clearFormatting();
		screenVars.crtdateDisp.clearFormatting();
		screenVars.fupremk.clearFormatting();
		screenVars.actn.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.fuptype.clearFormatting();
		screenVars.fuprcvdDisp.clearFormatting();
		screenVars.exprdateDisp.clearFormatting();
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sr590ScreenVars screenVars = (Sr590ScreenVars)pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.fupno.setClassString("");
		screenVars.uprflag.setClassString("");
		screenVars.zitem.setClassString("");
		screenVars.indic.setClassString("");
		screenVars.crtuser.setClassString("");
		screenVars.language.setClassString("");
		screenVars.fupcdes.setClassString("");
		screenVars.lifeno.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.fupstat.setClassString("");
		screenVars.fupremdtDisp.setClassString("");
		screenVars.crtdateDisp.setClassString("");
		screenVars.fupremk.setClassString("");
		screenVars.actn.setClassString("");
		screenVars.select.setClassString("");
		screenVars.fuptype.setClassString("");
		screenVars.fuprcvdDisp.setClassString("");
		screenVars.exprdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr590screensfl
 */
	public static void clear(VarModel pv) {
		Sr590ScreenVars screenVars = (Sr590ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.fupno.clear();
		screenVars.uprflag.clear();
		screenVars.zitem.clear();
		screenVars.indic.clear();
		screenVars.crtuser.clear();
		screenVars.language.clear();
		screenVars.fupcdes.clear();
		screenVars.lifeno.clear();
		screenVars.jlife.clear();
		screenVars.fupstat.clear();
		screenVars.fupremdtDisp.clear();
		screenVars.fupremdt.clear();
		screenVars.crtdateDisp.clear();
		screenVars.crtdate.clear();
		screenVars.fupremk.clear();
		screenVars.actn.clear();
		screenVars.select.clear();
		screenVars.fuptype.clear();
		screenVars.fuprcvdDisp.clear();
		screenVars.fuprcvd.clear();
		screenVars.exprdateDisp.clear();
		screenVars.exprdate.clear();
	}
}
