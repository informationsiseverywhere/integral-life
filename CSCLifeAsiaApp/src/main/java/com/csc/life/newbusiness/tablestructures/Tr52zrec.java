package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:27
 * Description:
 * Copybook name: TR52ZREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr52zrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr52zRec = new FixedLengthStringData(500);
  	public FixedLengthStringData bnytypes = new FixedLengthStringData(20).isAPartOf(tr52zRec, 0);
  	public FixedLengthStringData[] bnytype = FLSArrayPartOfStructure(10, 2, bnytypes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(bnytypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData bnytype01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData bnytype02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData bnytype03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData bnytype04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData bnytype05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData bnytype06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData bnytype07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData bnytype08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData bnytype09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData bnytype10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData cltrelns = new FixedLengthStringData(200).isAPartOf(tr52zRec, 20);
  	public FixedLengthStringData[] cltreln = FLSArrayPartOfStructure(50, 4, cltrelns, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(200).isAPartOf(cltrelns, 0, FILLER_REDEFINE);
  	public FixedLengthStringData cltreln01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData cltreln02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData cltreln03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData cltreln04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData cltreln05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData cltreln06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData cltreln07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData cltreln08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData cltreln09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData cltreln10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData cltreln11 = new FixedLengthStringData(4).isAPartOf(filler1, 40);
  	public FixedLengthStringData cltreln12 = new FixedLengthStringData(4).isAPartOf(filler1, 44);
  	public FixedLengthStringData cltreln13 = new FixedLengthStringData(4).isAPartOf(filler1, 48);
  	public FixedLengthStringData cltreln14 = new FixedLengthStringData(4).isAPartOf(filler1, 52);
  	public FixedLengthStringData cltreln15 = new FixedLengthStringData(4).isAPartOf(filler1, 56);
  	public FixedLengthStringData cltreln16 = new FixedLengthStringData(4).isAPartOf(filler1, 60);
  	public FixedLengthStringData cltreln17 = new FixedLengthStringData(4).isAPartOf(filler1, 64);
  	public FixedLengthStringData cltreln18 = new FixedLengthStringData(4).isAPartOf(filler1, 68);
  	public FixedLengthStringData cltreln19 = new FixedLengthStringData(4).isAPartOf(filler1, 72);
  	public FixedLengthStringData cltreln20 = new FixedLengthStringData(4).isAPartOf(filler1, 76);
  	public FixedLengthStringData cltreln21 = new FixedLengthStringData(4).isAPartOf(filler1, 80);
  	public FixedLengthStringData cltreln22 = new FixedLengthStringData(4).isAPartOf(filler1, 84);
  	public FixedLengthStringData cltreln23 = new FixedLengthStringData(4).isAPartOf(filler1, 88);
  	public FixedLengthStringData cltreln24 = new FixedLengthStringData(4).isAPartOf(filler1, 92);
  	public FixedLengthStringData cltreln25 = new FixedLengthStringData(4).isAPartOf(filler1, 96);
  	public FixedLengthStringData cltreln26 = new FixedLengthStringData(4).isAPartOf(filler1, 100);
  	public FixedLengthStringData cltreln27 = new FixedLengthStringData(4).isAPartOf(filler1, 104);
  	public FixedLengthStringData cltreln28 = new FixedLengthStringData(4).isAPartOf(filler1, 108);
  	public FixedLengthStringData cltreln29 = new FixedLengthStringData(4).isAPartOf(filler1, 112);
  	public FixedLengthStringData cltreln30 = new FixedLengthStringData(4).isAPartOf(filler1, 116);
  	public FixedLengthStringData cltreln31 = new FixedLengthStringData(4).isAPartOf(filler1, 120);
  	public FixedLengthStringData cltreln32 = new FixedLengthStringData(4).isAPartOf(filler1, 124);
  	public FixedLengthStringData cltreln33 = new FixedLengthStringData(4).isAPartOf(filler1, 128);
  	public FixedLengthStringData cltreln34 = new FixedLengthStringData(4).isAPartOf(filler1, 132);
  	public FixedLengthStringData cltreln35 = new FixedLengthStringData(4).isAPartOf(filler1, 136);
  	public FixedLengthStringData cltreln36 = new FixedLengthStringData(4).isAPartOf(filler1, 140);
  	public FixedLengthStringData cltreln37 = new FixedLengthStringData(4).isAPartOf(filler1, 144);
  	public FixedLengthStringData cltreln38 = new FixedLengthStringData(4).isAPartOf(filler1, 148);
  	public FixedLengthStringData cltreln39 = new FixedLengthStringData(4).isAPartOf(filler1, 152);
  	public FixedLengthStringData cltreln40 = new FixedLengthStringData(4).isAPartOf(filler1, 156);
  	public FixedLengthStringData cltreln41 = new FixedLengthStringData(4).isAPartOf(filler1, 160);
  	public FixedLengthStringData cltreln42 = new FixedLengthStringData(4).isAPartOf(filler1, 164);
  	public FixedLengthStringData cltreln43 = new FixedLengthStringData(4).isAPartOf(filler1, 168);
  	public FixedLengthStringData cltreln44 = new FixedLengthStringData(4).isAPartOf(filler1, 172);
  	public FixedLengthStringData cltreln45 = new FixedLengthStringData(4).isAPartOf(filler1, 176);
  	public FixedLengthStringData cltreln46 = new FixedLengthStringData(4).isAPartOf(filler1, 180);
  	public FixedLengthStringData cltreln47 = new FixedLengthStringData(4).isAPartOf(filler1, 184);
  	public FixedLengthStringData cltreln48 = new FixedLengthStringData(4).isAPartOf(filler1, 188);
  	public FixedLengthStringData cltreln49 = new FixedLengthStringData(4).isAPartOf(filler1, 192);
  	public FixedLengthStringData cltreln50 = new FixedLengthStringData(4).isAPartOf(filler1, 196);
  	public FixedLengthStringData gitem = new FixedLengthStringData(8).isAPartOf(tr52zRec, 220);
  	public FixedLengthStringData manopts = new FixedLengthStringData(10).isAPartOf(tr52zRec, 228);
  	public FixedLengthStringData[] manopt = FLSArrayPartOfStructure(10, 1, manopts, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(10).isAPartOf(manopts, 0, FILLER_REDEFINE);
  	public FixedLengthStringData manopt01 = new FixedLengthStringData(1).isAPartOf(filler2, 0);
  	public FixedLengthStringData manopt02 = new FixedLengthStringData(1).isAPartOf(filler2, 1);
  	public FixedLengthStringData manopt03 = new FixedLengthStringData(1).isAPartOf(filler2, 2);
  	public FixedLengthStringData manopt04 = new FixedLengthStringData(1).isAPartOf(filler2, 3);
  	public FixedLengthStringData manopt05 = new FixedLengthStringData(1).isAPartOf(filler2, 4);
  	public FixedLengthStringData manopt06 = new FixedLengthStringData(1).isAPartOf(filler2, 5);
  	public FixedLengthStringData manopt07 = new FixedLengthStringData(1).isAPartOf(filler2, 6);
  	public FixedLengthStringData manopt08 = new FixedLengthStringData(1).isAPartOf(filler2, 7);
  	public FixedLengthStringData manopt09 = new FixedLengthStringData(1).isAPartOf(filler2, 8);
  	public FixedLengthStringData manopt10 = new FixedLengthStringData(1).isAPartOf(filler2, 9);
  	public FixedLengthStringData revcflgs = new FixedLengthStringData(50).isAPartOf(tr52zRec, 238);
  	public FixedLengthStringData[] revcflg = FLSArrayPartOfStructure(50, 1, revcflgs, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(50).isAPartOf(revcflgs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData revcflg01 = new FixedLengthStringData(1).isAPartOf(filler3, 0);
  	public FixedLengthStringData revcflg02 = new FixedLengthStringData(1).isAPartOf(filler3, 1);
  	public FixedLengthStringData revcflg03 = new FixedLengthStringData(1).isAPartOf(filler3, 2);
  	public FixedLengthStringData revcflg04 = new FixedLengthStringData(1).isAPartOf(filler3, 3);
  	public FixedLengthStringData revcflg05 = new FixedLengthStringData(1).isAPartOf(filler3, 4);
  	public FixedLengthStringData revcflg06 = new FixedLengthStringData(1).isAPartOf(filler3, 5);
  	public FixedLengthStringData revcflg07 = new FixedLengthStringData(1).isAPartOf(filler3, 6);
  	public FixedLengthStringData revcflg08 = new FixedLengthStringData(1).isAPartOf(filler3, 7);
  	public FixedLengthStringData revcflg09 = new FixedLengthStringData(1).isAPartOf(filler3, 8);
  	public FixedLengthStringData revcflg10 = new FixedLengthStringData(1).isAPartOf(filler3, 9);
  	public FixedLengthStringData revcflg11 = new FixedLengthStringData(1).isAPartOf(filler3, 10);
  	public FixedLengthStringData revcflg12 = new FixedLengthStringData(1).isAPartOf(filler3, 11);
  	public FixedLengthStringData revcflg13 = new FixedLengthStringData(1).isAPartOf(filler3, 12);
  	public FixedLengthStringData revcflg14 = new FixedLengthStringData(1).isAPartOf(filler3, 13);
  	public FixedLengthStringData revcflg15 = new FixedLengthStringData(1).isAPartOf(filler3, 14);
  	public FixedLengthStringData revcflg16 = new FixedLengthStringData(1).isAPartOf(filler3, 15);
  	public FixedLengthStringData revcflg17 = new FixedLengthStringData(1).isAPartOf(filler3, 16);
  	public FixedLengthStringData revcflg18 = new FixedLengthStringData(1).isAPartOf(filler3, 17);
  	public FixedLengthStringData revcflg19 = new FixedLengthStringData(1).isAPartOf(filler3, 18);
  	public FixedLengthStringData revcflg20 = new FixedLengthStringData(1).isAPartOf(filler3, 19);
  	public FixedLengthStringData revcflg21 = new FixedLengthStringData(1).isAPartOf(filler3, 20);
  	public FixedLengthStringData revcflg22 = new FixedLengthStringData(1).isAPartOf(filler3, 21);
  	public FixedLengthStringData revcflg23 = new FixedLengthStringData(1).isAPartOf(filler3, 22);
  	public FixedLengthStringData revcflg24 = new FixedLengthStringData(1).isAPartOf(filler3, 23);
  	public FixedLengthStringData revcflg25 = new FixedLengthStringData(1).isAPartOf(filler3, 24);
  	public FixedLengthStringData revcflg26 = new FixedLengthStringData(1).isAPartOf(filler3, 25);
  	public FixedLengthStringData revcflg27 = new FixedLengthStringData(1).isAPartOf(filler3, 26);
  	public FixedLengthStringData revcflg28 = new FixedLengthStringData(1).isAPartOf(filler3, 27);
  	public FixedLengthStringData revcflg29 = new FixedLengthStringData(1).isAPartOf(filler3, 28);
  	public FixedLengthStringData revcflg30 = new FixedLengthStringData(1).isAPartOf(filler3, 29);
  	public FixedLengthStringData revcflg31 = new FixedLengthStringData(1).isAPartOf(filler3, 30);
  	public FixedLengthStringData revcflg32 = new FixedLengthStringData(1).isAPartOf(filler3, 31);
  	public FixedLengthStringData revcflg33 = new FixedLengthStringData(1).isAPartOf(filler3, 32);
  	public FixedLengthStringData revcflg34 = new FixedLengthStringData(1).isAPartOf(filler3, 33);
  	public FixedLengthStringData revcflg35 = new FixedLengthStringData(1).isAPartOf(filler3, 34);
  	public FixedLengthStringData revcflg36 = new FixedLengthStringData(1).isAPartOf(filler3, 35);
  	public FixedLengthStringData revcflg37 = new FixedLengthStringData(1).isAPartOf(filler3, 36);
  	public FixedLengthStringData revcflg38 = new FixedLengthStringData(1).isAPartOf(filler3, 37);
  	public FixedLengthStringData revcflg39 = new FixedLengthStringData(1).isAPartOf(filler3, 38);
  	public FixedLengthStringData revcflg40 = new FixedLengthStringData(1).isAPartOf(filler3, 39);
  	public FixedLengthStringData revcflg41 = new FixedLengthStringData(1).isAPartOf(filler3, 40);
  	public FixedLengthStringData revcflg42 = new FixedLengthStringData(1).isAPartOf(filler3, 41);
  	public FixedLengthStringData revcflg43 = new FixedLengthStringData(1).isAPartOf(filler3, 42);
  	public FixedLengthStringData revcflg44 = new FixedLengthStringData(1).isAPartOf(filler3, 43);
  	public FixedLengthStringData revcflg45 = new FixedLengthStringData(1).isAPartOf(filler3, 44);
  	public FixedLengthStringData revcflg46 = new FixedLengthStringData(1).isAPartOf(filler3, 45);
  	public FixedLengthStringData revcflg47 = new FixedLengthStringData(1).isAPartOf(filler3, 46);
  	public FixedLengthStringData revcflg48 = new FixedLengthStringData(1).isAPartOf(filler3, 47);
  	public FixedLengthStringData revcflg49 = new FixedLengthStringData(1).isAPartOf(filler3, 48);
  	public FixedLengthStringData revcflg50 = new FixedLengthStringData(1).isAPartOf(filler3, 49);
  	public FixedLengthStringData selfinds = new FixedLengthStringData(10).isAPartOf(tr52zRec, 288);
  	public FixedLengthStringData[] selfind = FLSArrayPartOfStructure(10, 1, selfinds, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(selfinds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData selfind01 = new FixedLengthStringData(1).isAPartOf(filler4, 0);
  	public FixedLengthStringData selfind02 = new FixedLengthStringData(1).isAPartOf(filler4, 1);
  	public FixedLengthStringData selfind03 = new FixedLengthStringData(1).isAPartOf(filler4, 2);
  	public FixedLengthStringData selfind04 = new FixedLengthStringData(1).isAPartOf(filler4, 3);
  	public FixedLengthStringData selfind05 = new FixedLengthStringData(1).isAPartOf(filler4, 4);
  	public FixedLengthStringData selfind06 = new FixedLengthStringData(1).isAPartOf(filler4, 5);
  	public FixedLengthStringData selfind07 = new FixedLengthStringData(1).isAPartOf(filler4, 6);
  	public FixedLengthStringData selfind08 = new FixedLengthStringData(1).isAPartOf(filler4, 7);
  	public FixedLengthStringData selfind09 = new FixedLengthStringData(1).isAPartOf(filler4, 8);
  	public FixedLengthStringData selfind10 = new FixedLengthStringData(1).isAPartOf(filler4, 9);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(202).isAPartOf(tr52zRec, 298, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr52zRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr52zRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}