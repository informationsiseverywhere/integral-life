package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Sr57gScreenVars extends SmartVarModel { 

	/** TSD 296 Modification Start*/
	//public FixedLengthStringData dataArea = new FixedLengthStringData(441);
	public FixedLengthStringData dataArea = new FixedLengthStringData(526);
	//public FixedLengthStringData dataFields = new FixedLengthStringData(233).isAPartOf(dataArea, 0);
	public FixedLengthStringData dataFields = new FixedLengthStringData(270).isAPartOf(dataArea, 0);
	/*TSD 296 Modification End*/
	public FixedLengthStringData bankaccdsc = DD.bankaccdsc.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData bankdesc = DD.bankdesc.copy().isAPartOf(dataFields,50);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(dataFields,80);
	public ZonedDecimalData billcd = DD.billcd.copyToZonedDecimal().isAPartOf(dataFields,90);
	public FixedLengthStringData branchdesc = DD.branchdesc.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,128);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(dataFields,131);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,133);
	public FixedLengthStringData mandref = DD.dcmandref.copy().isAPartOf(dataFields,163); //ILIFE-2472
	public FixedLengthStringData numsel = DD.numsel.copy().isAPartOf(dataFields,168);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,178);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,225);
	/* TSD 296 Modification Start*/
	public FixedLengthStringData mrbnk = DD.mrbnk.copy().isAPartOf(dataFields, 233);
	public FixedLengthStringData bnkbrn = DD.bnkbrn.copy().isAPartOf(dataFields, 234);
	public FixedLengthStringData bnkbrndesc = DD.branchdesc.copy().isAPartOf(dataFields, 240);
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(52).isAPartOf(dataArea, 233);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 270);
	/*TSD 296 Modification End*/
	public FixedLengthStringData bankaccdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bankacckeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData bankdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bankkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData branchdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData facthousErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData mandrefErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData numselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	/*TSD 296 Modification Start*/
	public FixedLengthStringData mrbnkErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData bnkbrnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData bnkbrndescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 285);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 334);
	/*TSD 296 Modification End*/
	public FixedLengthStringData[] bankaccdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bankacckeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] bankdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bankkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] branchdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] facthousOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] mandrefOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] numselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	/*TSD 296 Modification Start*/
	public FixedLengthStringData[] mrbnkOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] bnkbrnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] bnkbrndescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	/*TSD 296 Modification End*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billcdDisp = new FixedLengthStringData(10);

	public LongData Sr57gscreenWritten = new LongData(0);
	public LongData Sr57gwindowWritten = new LongData(0);
	public LongData Sr57gprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sr57gScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(mandrefOut,new String[] {"01","50","-01",null, null, null, null, null, null, null, null, null});
		/*IFSU-852 changes starts*/
		fieldIndMap.put(mrbnkOut,new String[] {"02","51","-02","52", null, null, null, null, null, null, null, null});
		/*IFSU-852 changes end*/
		screenFields = new BaseData[] {currcode, numsel, billcd, payrnum, payorname, mandref, facthous, longdesc, bankkey, bankdesc, branchdesc, bankacckey, bankaccdsc, mrbnk, bnkbrn, bnkbrndesc};
		screenOutFields = new BaseData[][] {currcodeOut, numselOut, billcdOut, payrnumOut, payornameOut, mandrefOut, facthousOut, longdescOut, bankkeyOut, bankdescOut, branchdescOut, bankacckeyOut, bankaccdscOut, mrbnkOut, bnkbrnOut, bnkbrndescOut};
		screenErrFields = new BaseData[] {currcodeErr, numselErr, billcdErr, payrnumErr, payornameErr, mandrefErr, facthousErr, longdescErr, bankkeyErr, bankdescErr, branchdescErr, bankacckeyErr, bankaccdscErr, mrbnkErr, bnkbrnErr, bnkbrndescErr};
		screenDateFields = new BaseData[] {billcd};
		screenDateErrFields = new BaseData[] {billcdErr};
		screenDateDispFields = new BaseData[] {billcdDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr57gscreen.class;
		protectRecord = Sr57gprotect.class;
	}

}
