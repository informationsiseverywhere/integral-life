/*
 * File: Br59b.java
 * Date: 23 November 2013 09:47:13
 * Author: CSC Limited
 *  
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import java.util.Iterator;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.life.productdefinition.dataaccess.model.Zafapf;
import com.csc.life.productdefinition.dataaccess.dao.ZafapfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.ZafapfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Zswrpf;
import com.csc.life.productdefinition.dataaccess.dao.ZswrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.ZswrpfDAOImpl;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Tr58yrec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswdTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UswhTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnaloTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.procedures.Ufprice;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This batch program process the Juvenile extra details files, changes salutation of 
*   policy owner once he/she reaches age 15.
*   
***************************************************************************************
* </pre>
*/
public class Br59b extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR59B");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private T5540rec t5540rec = new T5540rec();
	private T5679rec t5679rec = new T5679rec();
	private T5544rec t5544rec = new T5544rec();
	private Tr58yrec tr58yrec = new Tr58yrec();
	
	private String cltsrec = "CLTSREC";
	private String zswrdterec = "ZSWRDTEREC";	
	private String zswrposrec = "ZSWPOSREC";
	private String utrnalorec = "UTRNALOREC";


		/* TABLES */
	//private String tt001 = "TT001";
		/* ERRORS */
	private String ivrm = "IVRM";
	/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	
	private int wsasInd = 0;
	

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");
	private FixedLengthStringData wsaaT5544Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaUsmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 0);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 4);
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();	
	private UtrnaloTableDAM utrnaloIO = new UtrnaloTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private Ufpricerec ufpricerec = new Ufpricerec();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private UswdTableDAM uswdIO = new UswdTableDAM();
	private UswhTableDAM uswhIO = new UswhTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private T6647rec t6647rec = new T6647rec();
	private T5645rec t5645rec = new T5645rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private List<Zafapf> zafapfList = new ArrayList<Zafapf>();
	private FormatsInner formatsInner = new FormatsInner();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private FixedLengthStringData  wsaaConUtrnalo = new FixedLengthStringData(4);
	private FixedLengthStringData  wssaTran = new FixedLengthStringData(4);
	private String wsaaSubr = "BR59B";
	private String flag = "";
	private Agecalcrec agecalcrec = new Agecalcrec();
	private P6671par p6671par = new P6671par();
	int pos;int age = 0;
	private ZswrpfDAO  zswrDAO =  getApplicationContext().getBean("ZswrpfDAO", ZswrpfDAO.class);
	private ZafapfDAO  zafaDAO =  getApplicationContext().getBean("ZafapfDAO", ZafapfDAO.class);
	List<Zswrpf> zswrpfiList=new ArrayList<>();
	Zswrpf zswrpf=new Zswrpf();
	Zswrpf zswr=new Zswrpf();
	private Iterator<Zswrpf> iteratorList;
	int i;
	private int intBatchID;
	private int intBatchExtractSize;	
	private int intBatchStep;
	private String strCompany;
	private String strEffDate;
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1209, 
		exit1309, 
		exit1409, 
		exit1509, 
		exit2090, 
		exit2590, 
		exit3090, 
		nextHpua3180, 
		x1090Exit
	}
	private boolean noRecordFound;
	public Br59b() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}
 
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
	p6671par.parmRecord.set(bupaIO.getParmarea());
	strCompany = bsprIO.getCompany().toString();
	strEffDate = bsscIO.getEffectiveDate().toString();	
	intBatchID = 0;
	intBatchStep = 0;
	noRecordFound = false;
	zswrpfiList = new ArrayList<Zswrpf>();

	if (bprdIO.systemParam01.isNumeric())
		if (bprdIO.systemParam01.toInt() > 0)
			intBatchExtractSize = bprdIO.systemParam01.toInt();
		else
			intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();
	else
		intBatchExtractSize = bprdIO.cyclesPerCommit.toInt();		
	
	int extractRows = zswrDAO.populateZrstpfTemp(strCompany, intBatchExtractSize, p6671par.chdrnum.toString(), p6671par.chdrnum1.toString());
	if (extractRows > 0) {
		performDataChunk();	
		
		if (!zswrpfiList.isEmpty()){
			initialise1010();
			 
		}			
	}
	else{
		 
		noRecordFound = true;
		return;
	}
		
	}

protected void performDataChunk(){
	 
	intBatchID = intBatchStep;		
	if (zswrpfiList.size() > 0) zswrpfiList.clear();
	zswrpfiList = zswrDAO.loadDataByBatch(intBatchID);
	if (zswrpfiList.isEmpty())	wsspEdterror.set(varcom.endp);
	else iteratorList = zswrpfiList.iterator();	
}
protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
	 	datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl("T6647");
		itemIO.setItemitem("BT51***"); //Illustration only this B670 has to be transaction code form object register)
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		
			
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			t6647rec.t6647Rec.set(" ");
		}
		else {
			t6647rec.t6647Rec.set(itemIO.getGenarea());
		}
		/* Read T5679 for valid statii.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl("T5679");
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());	
		
		
		wsspEdterror.set(varcom.oK);
	} 
	
protected void populateZswrpf()
{
	zswr.setChdrpfx("CH");
	zswr.setChdrcoy(bsprIO.getCompany().toString());
	zswr.setChdrnum(p6671par.chdrnum.toString()); 
	zswr.setValidflag("1" );
	 
	zswrpfiList=zswrDAO.getAllZswrpfData( zswr.getChdrnum(),zswr.getChdrcoy(),  zswr.getChdrpfx(),  zswr.getValidflag(), p6671par.chdrnum1.toString() );/* IJTI-1523 */
	
	

}

protected void readFile2000()
	{
		try {
			readFile2010();
		}
		catch (GOTOException e){
		}
	} 

protected void readFile2010()
	{
	
	

	if (!zswrpfiList.isEmpty()){	
		if (!iteratorList.hasNext()) {
			intBatchStep++;	
			performDataChunk();
			if (!zswrpfiList.isEmpty()){
				zswrpf = new Zswrpf();
				zswrpf = iteratorList.next();
				 		
			}
		}else {		
			zswrpf = new Zswrpf();
			zswrpf = iteratorList.next();
			 
		}	
	}else wsspEdterror.set(varcom.endp);

		
	 
  
/*	try
	{
		if (!zswrpfiList.isEmpty())
		{	
			if (!iteratorList.hasNext()) 
			{	
				performDataChunk();
				if (!zswrpfiList.isEmpty())
				{
						zswrpf = new Zswrpf();
						zswrpf = iteratorList.next();
				}
				else
				        wsspEdterror.set(varcom.endp);
				 
			}
			else 
			{		
				zswrpf = new Zswrpf();
				zswrpf = iteratorList.next();
			}	
		}
		else {
			wsspEdterror.set(varcom.endp);
		
		}
	}
	catch (Exception ex)
	{
		wsspEdterror.set(varcom.endp);
	}
	 */
    
}

protected void edit2500( )
	{
		try {
			edit2510( );
			 
		}
		catch (GOTOException e){
		}
	}

protected void edit2510( )
	{
		if(flag.equals("N")){
			wsspEdterror.set(SPACES);
		//	zswrdteIO.setFunction(varcom.nextr);
			return;
		}
		
		if(isEQ(zswrpf.getZafropt().trim(),"NO")){/* IJTI-1523 */
			wsspEdterror.set(SPACES);
		//	zswrdteIO.setFunction(varcom.nextr);
			return;
		}
		
		validateChdr2600( );
		if (!validContract.isTrue()) {
			/*  Log CHDRs with invalid statii*/
			contotrec.totno.set(controlTotalsInner.ct02);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			//zswrdteIO.setFunction(varcom.nextr);
			return ;
		}
		wsaaConUtrnalo.set(SPACES);
		checkUtrns6900( );
		if (isEQ(wsaaConUtrnalo, "Y")) {
			/*  Log CHDRs with invalid statii*/
			contotrec.totno.set(controlTotalsInner.ct05);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			//zswrdteIO.setFunction(varcom.nextr);
			return ;
		}
		determineAFR4100( );
		wsspEdterror.set(varcom.oK);
	}


protected void validateChdr2600( )
	{
	       chdrmjaIO.setDataKey(SPACES);
	       chdrmjaIO.setChdrcoy(zswrpf.getChdrcoy());
	       chdrmjaIO.setChdrnum(zswrpf.getChdrnum());
	       chdrmjaIO.setFunction(varcom.readr);
	       SmartFileCode.execute(appVars, chdrmjaIO);
	       if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
		   syserrrec.params.set(chdrmjaIO.getParams());
		   syserrrec.statuz.set(syserrrec.dbioStatuz);
		   fatalError600();
	        }
	       wsaaValidChdr.set("Y");
		/*START*/
		/* Validate Contract status against T5679.*/
		wsaaValidChdr.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)
			|| validContract.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()], chdrmjaIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)
				|| validContract.isTrue()); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()], chdrmjaIO.getPstatcode())) {
						wsaaValidChdr.set("Y");
					}
				}
			}
		} 
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update3010();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}	}

protected void update3010()
	{

 if(isLTE(zswrpf.getZnextdte(),bsscIO.getEffectiveDate()))
 {
		//iter and check date
		wsaaTran.set("N");
		determineTran5200();
		if(isEQ(wsaaTran, "N")) {
  		   wsaaTranno.set(0);
  		 UpdtChdrmja5300();
   		   writePtrn5400();		
		   rewriteZswrdte6600();
           writeZafa6800();
		  // zswrdteIO.setFunction(varcom.nextr);
 		   contotrec.totno.set(controlTotalsInner.ct03);
		   contotrec.totval.set(1);
		   callContot001();
		   goTo(GotoLabel.exit3090);	
		} 

		wsaaTranno.set(0);
		UpdtChdrmja5300();
		updateAFR5500();
		writePtrn5400();		
		rewriteZswrdte6600();
         writeZafa6800();
		//zswrdteIO.setFunction(varcom.nextr);
		contotrec.totno.set(controlTotalsInner.ct04);
		contotrec.totval.set(1);
		callContot001();
	}
	}

	
protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}


protected void determineAFR4100( ) {
	
  initializeTempArray4200();
  readT55404300( );
  //readTT120();
  readUlnkIO();
  calculateFundValue4400();
  calculateRBal4900();	  

}

protected void initializeTempArray4200() {
	wsaaContFValue.set(0);
	wsaaRebalAmt.set(0);

	for (wsaaInd=1; wsaaInd <= 10; wsaaInd++) {
		wsaaVrtFnd[wsaaInd].set(" ");
		wsaaFundAmnt[wsaaInd].set(0);
		wsaaDfndAmnt[wsaaInd].set(0);
		wsaaBuySell[wsaaInd].set(" ");
		wsaaBSAmnt[wsaaInd].set(0);
		wsaaDeviate[wsaaInd].set(0);
		wsaaWtAvg[wsaaInd].set(0);
		wsaaOrigFS[wsaaInd].set(0);
		wsbbLife[wsaaInd].set(" ");
		wsbbCoverage[wsaaInd].set(" ");	
		wsbbRider[wsaaInd].set(" ");
		wsbbPlanSuffix[wsaaInd].set(" ");	
		wsbbUnitType[wsaaInd].set(" ");
		wsccPrice[wsaaInd].set(0);
	}
}


protected void readT55404300() {

	itdmIO.setDataKey(SPACES);
	itdmIO.setItemcoy(zswrpf.getChdrcoy());
	itdmIO.setItemtabl("T5540");
	itdmIO.setItmfrm(bsscIO.getEffectiveDate());
	itdmIO.setItemitem(zswrpf.getCrtable());
	itdmIO.setFormat(formatsInner.itemrec);
	itdmIO.setFunction(varcom.begn);
	//performance improvement --  Niharika Modi 
	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
 
	SmartFileCode.execute(appVars, itdmIO);
	if ((isNE(itdmIO.getStatuz(), varcom.endp))
	&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
	syserrrec.statuz.set(itdmIO.getStatuz());
		syserrrec.params.set(itdmIO.getParams());
		fatalError600();
	}
	if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
		itdmIO.setGenarea(SPACES);
	}
	if (isNE(itdmIO.getItemcoy(), zswrpf.getChdrcoy())
	|| (isNE(itdmIO.getItemtabl(), "T5540"))
	|| (isNE(itdmIO.getItemitem(), zswrpf.getCrtable()))) {
		itdmIO.setGenarea(SPACES);
	}
	t5540rec.t5540Rec.set(itdmIO.getGenarea());
}		

protected void readTT120(){
	itdmIO.setDataKey(SPACES);
	itdmIO.setItemcoy(zswrpf.getChdrcoy());
	itdmIO.setItemtabl("TT120");
	itdmIO.setItmfrm(bsscIO.getEffectiveDate());
	itdmIO.setItemitem(zswrpf.getZafritem());
	itdmIO.setFormat(formatsInner.itemrec);
	itdmIO.setFunction(varcom.begn);
	//performance improvement --  Niharika Modi 
	itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
	itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
 
	SmartFileCode.execute(appVars, itdmIO);
	if ((isNE(itdmIO.getStatuz(), varcom.endp))
	&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
		syserrrec.statuz.set(itdmIO.getStatuz());
		syserrrec.params.set(itdmIO.getParams());
		fatalError600();
	}
	if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
		itdmIO.setGenarea(SPACES);
	}
	if (isNE(itdmIO.getItemcoy(), zswrpf.getChdrcoy())
	|| (isNE(itdmIO.getItemtabl(), "TR58Y"))
	|| (isNE(itdmIO.getItemitem(), zswrpf.getZafritem()))) {
		itdmIO.setGenarea(SPACES);
	}
	tr58yrec.tr58yrec.set(itdmIO.getGenarea());
}

protected void calculateFundValue4400() {
	wsaaInd = 0;
	wsaaPrice.set(0);
	utrsIO.setParams(SPACES);
	utrsIO.setChdrcoy(zswrpf.getChdrcoy());
	utrsIO.setChdrnum(zswrpf.getChdrnum());
	utrsIO.setLife(" ");
	utrsIO.setCoverage(" ");
	utrsIO.setRider(" ");
	utrsIO.setUnitVirtualFund(" ");
	utrsIO.setUnitType(" ");
	utrsIO.setPlanSuffix(0);
	utrsIO.setFunction(varcom.begn);
	
	SmartFileCode.execute(appVars, utrsIO);
	
	if (isNE(utrsIO.getStatuz(), varcom.oK) && 
		(isNE(utrsIO.getStatuz(),varcom.endp))) {
		syserrrec.params.set(covrmjaIO.getParams());
		syserrrec.statuz.set(covrmjaIO.getStatuz());
		fatalError600();
	}
	wsaaInd = 1;
	while (isNE(utrsIO.getStatuz(), varcom.endp))
	 {
	      if (isNE(utrsIO.getChdrcoy(),chdrmjaIO.getChdrcoy()) ||
		  isNE(utrsIO.getChdrnum(),chdrmjaIO.getChdrnum()) ||
		  isEQ(utrsIO.getCoverage(), "02")) {
	    	break;
	      }else{
      	       checkPrice4500();
	      }
         }	
} 
private void readUlnkIO()
{
	ulnkIO.setDataKey(SPACES);
	ulnkIO.setCoverage("01");
	ulnkIO.setRider("00");
	ulnkIO.setChdrcoy(chdrmjaIO.getChdrcoy());
	ulnkIO.setChdrnum(chdrmjaIO.getChdrnum());
	ulnkIO.setLife("01");
	ulnkIO.setTranno(ZERO);
	ulnkIO.setFunction(varcom.begn);
	SmartFileCode.execute(appVars, ulnkIO);
	if(isNE(ulnkIO.getStatuz(), varcom.oK) &&
	 isNE(ulnkIO.statuz,varcom.endp)) {
		syserrrec.statuz.set(ulnkIO.statuz);
		fatalError600();
	}
	
}
protected void checkPrice4500() {

	boolean fundFlg = false;
	int k =1;
	if(isEQ(ulnkIO.getChdrnum(),chdrmjaIO.getChdrnum())){
		while(isNE(ulnkIO.getUalfnd(k),SPACES) && k<=10){
			if(isEQ(ulnkIO.getUalfnd(k),utrsIO.getUnitVirtualFund())){
				fundFlg = true;
				break;
			}
			k++;
		}
		if(!fundFlg){
			getNextUtrsFile4800();
			return;
		}
	}
	wsaaPrice.set(0);
	getFundPrice4600();
	accumulateFv4700();
	
}

protected void getFundPrice4600() {

    ufpricerec.function.set("DEALP");
	ufpricerec.mode.set("BATCH");
	ufpricerec.company.set(utrsIO.getChdrcoy());
	ufpricerec.unitVirtualFund.set(utrsIO.getUnitVirtualFund());
	ufpricerec.unitType.set(utrsIO.getUnitType());
	ufpricerec.effdate.set(bsscIO.getEffectiveDate());
	ufpricerec.nowDeferInd.set(t6647rec.aloind);
	callProgram(Ufprice.class, ufpricerec.ufpriceRec);
	if (isNE(ufpricerec.statuz,varcom.oK)
	&& isNE(ufpricerec.statuz,varcom.endp)) {
		syserrrec.params.set(ufpricerec.ufpriceRec);
		syserrrec.statuz.set(ufpricerec.statuz);
		fatalError600();
	}
	
	wsaaUfprPriceDate.set(ufpricerec.priceDate);
	wsaaUfprBarePrice.set(ufpricerec.barePrice);
	if (isEQ(t6647rec.bidoffer, 'B'))
	{
		wsaaPrice.set(ufpricerec.bidPrice);
	}
	else 
		{
			wsaaPrice.set(ufpricerec.offerPrice);
		}
}


protected void accumulateFv4700() {
	
	wsaaCurrentUnitFBal.set(0);
	wsaaCurrentDUnitFBal.set(0);
	compute(wsaaCurrentUnitFBal, 2).setRounded(mult(wsaaPrice,utrsIO.getCurrentUnitBal()));
	compute(wsaaCurrentDUnitFBal, 2).setRounded(mult(wsaaPrice,utrsIO.getCurrentDunitBal()));

	if (isEQ(utrsIO.getCoverage(), "01")) {
		wsaaVrtFnd[wsaaInd].set(utrsIO.getUnitVirtualFund());
		wsaaFundAmnt[wsaaInd].set(wsaaCurrentUnitFBal);
		wsaaDfndAmnt[wsaaInd].set(wsaaCurrentDUnitFBal);
		wsbbLife[wsaaInd].set(utrsIO.getLife());
		wsbbCoverage[wsaaInd].set(utrsIO.getCoverage());	
		wsbbRider[wsaaInd].set(utrsIO.getRider());
		wsbbPlanSuffix[wsaaInd].set(utrsIO.getPlanSuffix());
		wsbbUnitType[wsaaInd].set(utrsIO.getUnitType());
		compute(wsaaContFValue, 2).set(add(wsaaContFValue,wsaaCurrentUnitFBal));
		wsccPrice[wsaaInd].set(wsaaPrice);
		wsaaInd = wsaaInd + 1;
	}
 	getNextUtrsFile4800();		
}
 
protected void getNextUtrsFile4800() {
	
    utrsIO.setFunction(varcom.nextr);
    SmartFileCode.execute(appVars, utrsIO);
    if (isNE(utrsIO.getStatuz(), varcom.oK) && 
	    (isNE(utrsIO.getStatuz(),varcom.endp))) {
		syserrrec.params.set(covrmjaIO.getParams());
		syserrrec.statuz.set(covrmjaIO.getStatuz());
		fatalError600();
    } 

}
	
protected void	calculateRBal4900() {

	wtAvg5000();	
	calcRbal5100();
}

protected void wtAvg5000() {
	//age = clientAge();
	for (wsaaInd=1; wsaaInd <= 10; wsaaInd++) {
		wsaaTemp1.set(0);wsaaTemp2.set(0);
		wsaaTemp3.set(0);wsaaTemp4.set(0);

		compute(wsaaTemp1, 2).setRounded(div(mult(wsaaFundAmnt[wsaaInd], 100), wsaaContFValue));
		wsaaWtAvg[wsaaInd].set(wsaaTemp1);  //Get Weighted Average Fund split 
		int j = 1;
		for(j=1; j<=10;j++){
			
			if(zswrpf.getUalfnd01()!=null && (zswrpf.getUalfnd01().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc01());
				break;
			}else if(zswrpf.getUalfnd02()!=null && (zswrpf.getUalfnd02().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc02());
				break;
			}else if(zswrpf.getUalfnd03()!=null && (zswrpf.getUalfnd03().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc03());
				break;
			}else if(zswrpf.getUalfnd04()!=null && (zswrpf.getUalfnd04().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc04());
				break;
			}else if(zswrpf.getUalfnd05()!=null && (zswrpf.getUalfnd05().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc05());
				break;
			}else if(zswrpf.getUalfnd06()!=null && (zswrpf.getUalfnd06().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc06());
				break;
			}else if(zswrpf.getUalfnd07()!=null && (zswrpf.getUalfnd07().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc07());
				break;
			}else if(zswrpf.getUalfnd08()!=null && (zswrpf.getUalfnd08().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc08());
				break;
			}else if(zswrpf.getUalfnd09()!=null && (zswrpf.getUalfnd09().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc09());
				break;
			}else if(zswrpf.getUalfnd10()!=null && (zswrpf.getUalfnd10().trim().equals(wsaaVrtFnd[wsaaInd].toString().trim()))){
				wsaaOrigFS[wsaaInd].set(zswrpf.getUalprc10());
				break;
			}
			
		}
		
		
                compute(wsaaTemp2, 2).set(sub(wsaaOrigFS[wsaaInd], wsaaWtAvg[wsaaInd])); 	
		if (isLT(wsaaTemp2,0)) {
                   wsaaBuySell[wsaaInd].set("S");
                   }
		else {
		   wsaaBuySell[wsaaInd].set("B");
		}
		wsaaTemp4.set(wsaaTemp2);
		if(isLT(wsaaTemp4,0))
				{ 
			compute(wsaaTemp4,2).set(mult(wsaaTemp4,-1));
					}
		wsaaDeviate[wsaaInd].set(wsaaTemp4);
		/*if(isNE(tt120rec.tt120Rec,SPACES) ){
			pos = checkFundpriority(wsaaVrtFnd[wsaaInd].toString());
			if(pos>0){
				compute(wsaaTemp3, 2).set(tt120rec.unitPremPercent[pos]);
			}
			else{
				compute(wsaaTemp3, 2).set(t5540rec.zvariance);
			}
				
		}else*/
		compute(wsaaTemp3, 2).set(t5540rec.zvariance.toString().trim());
		
		if(isEQ(wsaaTemp4,ZERO)){
			 wsaaBuySell[wsaaInd].set(" ");
		}
 		if(wsaaTemp4.toInt()>=wsaaTemp3.toInt()){
 			rebalanceFlag = true;
		} /*else
		{	
		
			wsaaBuySell[wsaaInd].set(" ");
		}*/
	}	
}		


protected int checkFundpriority(String fund){
	
	for(int i =0 ; i < 9; i++){
		if(tr58yrec.ageIssageFrm[i+1].toInt()<=age && tr58yrec.ageIssageTo[i+1].toInt()>=age){
			for (int j = 1; j<=5; j++){
				if(tr58yrec.unitVirtualFund[(i*5)+j].toString().equals(fund)){
					return (i*5)+j;
				}
			}
		}
	}
	return 0;
}

protected int clientAge(){

	lifelnbIO.setChdrcoy(chdrmjaIO.getChdrcoy());
	lifelnbIO.setChdrnum(chdrmjaIO.getChdrnum());
	lifelnbIO.setJlife("00");
	lifelnbIO.setLife("01");
	lifelnbIO.setFunction(varcom.readr);
	lifelnbIO.setFormat(formatsInner.lifelnbrec);
	SmartFileCode.execute(appVars, lifelnbIO);
    if (isNE(lifelnbIO.getStatuz(), varcom.oK) && 
	    (isNE(lifelnbIO.getStatuz(),varcom.mrnf))) {
		syserrrec.params.set(covrmjaIO.getParams());
		syserrrec.statuz.set(covrmjaIO.getStatuz());
		fatalError600();
    } 

	
	initialize(agecalcrec.agecalcRec);
	agecalcrec.function.set("CALCP");
	agecalcrec.language.set("E");
	agecalcrec.cnttype.set(chdrmjaIO.getCnttype());
	agecalcrec.intDate1.set(lifelnbIO.getCltdob());
	
	agecalcrec.intDate2.set(datcon1rec.intDate);
	agecalcrec.company.set("9");

	callProgram(Agecalc.class, agecalcrec.agecalcRec);
	if (isEQ(agecalcrec.statuz, "IVFD")) {
		return 0;
	}
	if (isNE(agecalcrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(agecalcrec.statuz);
		fatalError600();
	}
	return agecalcrec.agerating.toInt();
}
protected void calcRbal5100() {

	for (wsaaInd=1; wsaaInd <= 10; wsaaInd++) {
		wsaaTemp6.set(0);
		if(isNE(wsaaBuySell[wsaaInd], " ")){
		    /*compute(wsaaTemp2, 2).set(mult(div(wsaaDeviate[wsaaInd], wsaaContFValue),100));
		    wsaaBSAmnt[wsaaInd].set(wsaaTemp2);
		    wsccNofDunits[wsaaInd].setRounded(div(wsaaBSAmnt[wsaaInd],wsccPrice[wsaaInd]));	*/
		    
		    compute(wsaaTemp6, 2).set(div(mult(wsaaContFValue,wsaaOrigFS[wsaaInd]),100));
		    compute(wsaaTemp6,2).setRounded(sub(wsaaTemp6,wsaaFundAmnt[wsaaInd]));
		    wsaaBSAmnt[wsaaInd].setRounded(wsaaTemp6);
		    /*if(isLT(wsaaBSAmnt[wsaaInd],0))
			{ 
		    	compute(wsaaBSAmnt[wsaaInd],2).set(mult(wsaaBSAmnt[wsaaInd],-1));
			}*/
		    fundAmt.setRounded(div(wsaaBSAmnt[wsaaInd],wsccPrice[wsaaInd]));
		    wsccNofDunits[wsaaInd].set(fundAmt);	
		}
	}
}

protected void	determineTran5200() {
	/*wsaaB.set(" "); wsaaS.set(" ");

	for (wsaaInd=1; wsaaInd <= 10; wsaaInd++) {
	     if (isNE(wsaaBuySell[wsaaInd], " ")) {
		if (isEQ(wsaaBuySell[wsaaInd],"B")) {
			wsaaB.set("Y");
		}
	        if (isEQ(wsaaBuySell[wsaaInd],"S")) {
		    wsaaS.set("Y");
		}
	     }
	}
	if (isEQ(wsaaB ,"Y") && (isEQ(wsaaS, "Y"))) {
		wsaaTran.set("Y");
	}*/
	if(rebalanceFlag){
		wsaaTran.set("Y");
	}
}

	private FixedLengthStringData wsaaErrorStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaErrorFormat = new FixedLengthStringData(16);
	private PackedDecimalData wsaaTotFee = new PackedDecimalData(18, 3).init(0);
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaTaxAmt = new PackedDecimalData(18, 3).init(0);
	private FixedLengthStringData wsaaTime = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaHhmmss = new ZonedDecimalData(6, 0).isAPartOf(wsaaTime, 0).setUnsigned();

		/* WSAA-GENERAL-FIELDS */
	private FixedLengthStringData wsaaEndTrans = new FixedLengthStringData(1);
	private Validator endOfTransaction = new Validator(wsaaEndTrans, "Y");

	private FixedLengthStringData wsaaNotReady = new FixedLengthStringData(1);
	private Validator noNeedToPost = new Validator(wsaaNotReady, "Y");

	private FixedLengthStringData wsaa1stTime = new FixedLengthStringData(1);
	private Validator n1stTimeThru = new Validator(wsaa1stTime, "Y");
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30);

	private FixedLengthStringData wsaaPostingTable = new FixedLengthStringData(210);
	private FixedLengthStringData[] wsaaPostAmount = FLSArrayPartOfStructure(10, 21, wsaaPostingTable, 0);
	private FixedLengthStringData[] wsaaPostCurr = FLSDArrayPartOfArrayStructure(3, wsaaPostAmount, 0);
	private PackedDecimalData[] wsaaPostActualAmt = PDArrayPartOfArrayStructure(17, 2, wsaaPostAmount, 3);
	private PackedDecimalData[] wsaaPostTotFee = PDArrayPartOfArrayStructure(17, 2, wsaaPostAmount, 12);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
	private ZonedDecimalData wsaaSvltSub = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();
	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();
	private FixedLengthStringData wsaaEffdateX = new FixedLengthStringData(8).init(" ");
	private FixedLengthStringData wsaaNowDeferInd = new FixedLengthStringData(1).init(" ");
	private FixedLengthStringData wsaaB = new FixedLengthStringData(1).init(" ");
	private FixedLengthStringData wsaaS = new FixedLengthStringData(1).init(" ");
	private FixedLengthStringData wsaaTran = new FixedLengthStringData(1).init(" ");
	private FixedLengthStringData wsaaTrnCode = new FixedLengthStringData(7).init(" ");
	private FixedLengthStringData wsaaTcode = new FixedLengthStringData(4).isAPartOf(wsaaTrnCode,0);
	private FixedLengthStringData wsaaTplan = new FixedLengthStringData(3).isAPartOf(wsaaTrnCode,4);
	private PackedDecimalData wsaaProcSeqNo = new PackedDecimalData(3,0).init(0);
	private FixedLengthStringData wsaaBidOffer = new FixedLengthStringData(1).init(" ");
	private FixedLengthStringData wsaaBasInd = new FixedLengthStringData(1).init(" ");
	private FixedLengthStringData wsaaLylProc = new FixedLengthStringData(1).init(" ");
	private PackedDecimalData wsaaDenom = new PackedDecimalData(3).init(100);
	private PackedDecimalData wsaaFundCounter = new PackedDecimalData(3).init(0);
	private PackedDecimalData wsaaContractAmount = new PackedDecimalData(13,2).init(0);
	private PackedDecimalData wsbbContractAmount = new PackedDecimalData(13,2).init(0);
	private FixedLengthStringData wsaaSubAcct = new FixedLengthStringData(4).init("RTRG");
	private FixedLengthStringData wsaaTrigMod = new FixedLengthStringData(10).init("ZPREMFEE");
	private PackedDecimalData wsaaPrice = new PackedDecimalData(9,5).init(0);
	private FixedLengthStringData wsaaLadmRec = new FixedLengthStringData(760);
	private FixedLengthStringData[] wsaaLadmAmount = FLSArrayPartOfStructure(10, 76, wsaaLadmRec, 0);
	private FixedLengthStringData[] wsaaVrtFnd = FLSDArrayPartOfArrayStructure(4, wsaaLadmAmount, 0);
	private PackedDecimalData[] wsaaFundAmnt = PDArrayPartOfArrayStructure(17, 2, wsaaLadmAmount, 4);
	private PackedDecimalData[] wsaaDfndAmnt = PDArrayPartOfArrayStructure(17, 2, wsaaLadmAmount, 13);
	private PackedDecimalData[] wsccNofDunits = PDArrayPartOfArrayStructure(17, 5, wsaaLadmAmount, 13);
	
	private FixedLengthStringData[] wsaaBuySell = FLSDArrayPartOfArrayStructure(1, wsaaLadmAmount, 22);
	private PackedDecimalData[] wsaaBSAmnt = PDArrayPartOfArrayStructure(17, 2, wsaaLadmAmount, 23);
	private PackedDecimalData[] wsaaDeviate = PDArrayPartOfArrayStructure(17, 2, wsaaLadmAmount, 32);
	private PackedDecimalData[] wsaaWtAvg = PDArrayPartOfArrayStructure(17, 2, wsaaLadmAmount, 41);
	private PackedDecimalData[] wsaaOrigFS = PDArrayPartOfArrayStructure(17, 2, wsaaLadmAmount, 50);
	private FixedLengthStringData[] wsbbLife = FLSDArrayPartOfArrayStructure(2, wsaaLadmAmount, 59);
	private FixedLengthStringData[] wsbbCoverage = FLSDArrayPartOfArrayStructure(2, wsaaLadmAmount, 61);
	private FixedLengthStringData[] wsbbRider = FLSDArrayPartOfArrayStructure(2, wsaaLadmAmount, 63);
	private FixedLengthStringData[] wsbbPlanSuffix = FLSDArrayPartOfArrayStructure(2, wsaaLadmAmount, 65);
	private FixedLengthStringData[] wsbbUnitType = FLSDArrayPartOfArrayStructure(1, wsaaLadmAmount, 67);
	private PackedDecimalData[] wsccPrice = PDArrayPartOfArrayStructure(9,5, wsaaLadmAmount, 68);

	private PackedDecimalData wsaaCurrentUnitFBal = new PackedDecimalData(13,2).init(0);
	private PackedDecimalData wsaaCurrentDUnitFBal = new PackedDecimalData(13,2).init(0);
	private PackedDecimalData wsaaCurrentUnitBal = new PackedDecimalData(11,5).init(0);
	private PackedDecimalData wsaaCurrentDUnitBal = new PackedDecimalData(11,5).init(0);

	private FixedLengthStringData wsaaConUtrn = new FixedLengthStringData(1).init(" ");

	private FixedLengthStringData wsaaVrtF = new FixedLengthStringData(4).init(" ");
	private PackedDecimalData wsaaContFValue = new PackedDecimalData(13,4).init(0);
	private PackedDecimalData wsbbPrcnt = new PackedDecimalData(5,2).init(0);
	private PackedDecimalData wsbbBSAmnt = new PackedDecimalData(17,4).init(0);
	private PackedDecimalData wsaaRebalAmt = new PackedDecimalData(13,2).init(0);
	private ZonedDecimalData wsaaUfprPriceDate = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaUfprBarePrice = new PackedDecimalData(11,5).init(0);
	private int wsaaInd = 0;
	private int wsbbInd = 0;
	private PackedDecimalData wsaaUtrnFundAmnt = new PackedDecimalData(15,2).init(0);
	private PackedDecimalData wsaaTemp1 = new PackedDecimalData(05,2).init(0);
	private PackedDecimalData wsaaTemp2 = new PackedDecimalData(13,2).init(0);
	
	private PackedDecimalData wsaaTemp3 = new PackedDecimalData(11,5).init(0);
	private PackedDecimalData wsaaTemp4 = new PackedDecimalData(13,2).init(0);
	private PackedDecimalData wsaaTemp5 = new PackedDecimalData(13,2).init(0);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5).init(0);
	private PackedDecimalData wsaaTemp6 = new PackedDecimalData(13,4).init(0);
	private PackedDecimalData mtlPercentage = new PackedDecimalData(5,2).init(0);
	private PackedDecimalData fundAmt = new PackedDecimalData(10,4).init(0);
	
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private boolean rebalanceFlag = false;
protected void UpdtChdrmja5300()

{
    chdrmjaIO.setDataKey(SPACES);
    chdrmjaIO.setChdrcoy(zswrpf.getChdrcoy());
    chdrmjaIO.setChdrnum(zswrpf.getChdrnum());
    chdrmjaIO.setFunction(varcom.readh);
    SmartFileCode.execute(appVars, chdrmjaIO);
    if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
	 syserrrec.params.set(chdrmjaIO.getParams());
	 syserrrec.statuz.set(syserrrec.dbioStatuz);
	 fatalError600();
    }

    chdrmjaIO.setValidflag("2");
    chdrmjaIO.setFunction(varcom.rewrt);
    SmartFileCode.execute(appVars, chdrmjaIO);
    if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
	 syserrrec.params.set(chdrmjaIO.getParams());
	 syserrrec.statuz.set(syserrrec.dbioStatuz);
	 fatalError600();
    }

    chdrmjaIO.setValidflag("1");
    wsaaTranno.set(chdrmjaIO.getTranno());
    compute(wsaaTranno).set(add(wsaaTranno,1));
    chdrmjaIO.setTranno(wsaaTranno);

    chdrmjaIO.setFunction(varcom.writr);
    SmartFileCode.execute(appVars, chdrmjaIO);
    if (isNE(chdrmjaIO.getStatuz(),varcom.oK)) {
	 syserrrec.params.set(chdrmjaIO.getParams());
	 syserrrec.statuz.set(syserrrec.dbioStatuz);
	 fatalError600();
    }	
}

protected void writePtrn5400()

	{
		ptrnIO.setParams(SPACES);
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
	    ptrnIO.setChdrpfx(chdrmjaIO.getChdrpfx());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(chdrmjaIO.getTranno());	
		ptrnIO.setPtrneff(zswrpf.getZnextdte());
		ptrnIO.setUser(0);
		//ptrnIO.setPtrneff(bsscIO.getEffectiveDate());
		ptrnIO.setTransactionDate(datcon1rec.intDate);
	        ptrnIO.setTransactionTime(varcom.vrcmTime);
                ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(0);
		ptrnIO.setBatcpfx(batcdorrec.prefix);
		ptrnIO.setBatccoy(batcdorrec.company);
		ptrnIO.setBatcbrn(batcdorrec.branch);
		ptrnIO.setBatcactyr(batcdorrec.actyear);
		ptrnIO.setBatcactmn(batcdorrec.actmonth);
		ptrnIO.setBatctrcde(batcdorrec.trcde);
		ptrnIO.setBatcbatch(batcdorrec.batch);
		//ptrnIO.setPrtflg(" ");	
		ptrnIO.setValidflag("1");	
		ptrnIO.setDatesub(bsscIO.getEffectiveDate());
		ptrnIO.setCrtuser(bsscIO.getUserName().toString());   //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);

		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			fatalError600();
		}
	}

protected void updateAFR5500() {

	writeUswh8600();
	
	uswdIO.setParams(SPACES);
	uswdIO.setChdrcoy(chdrmjaIO.getChdrcoy());
	uswdIO.setChdrnum(chdrmjaIO.getChdrnum());
	uswdIO.setPlanSuffix(wsbbPlanSuffix[1]);
	uswdIO.setLife(wsbbLife[1]);
	//uswdIO.setLife(" ");
	uswdIO.setCoverage(wsbbCoverage[1]);
	uswdIO.setRider(wsbbRider[1]);
	uswdIO.setTranno(chdrmjaIO.getTranno());
	uswdIO.setPercentAmountInd("U");

	for (wsaaInd=1; wsaaInd < 21; wsaaInd++) {

		uswdIO.setSrcfund(wsaaInd, SPACES);
		uswdIO.setScfndtyp(wsaaInd, SPACES);
		uswdIO.setScfndcur(wsaaInd, SPACES);
		uswdIO.setScprcamt(wsaaInd, ZERO);
		uswdIO.setScestval(wsaaInd, ZERO);
		uswdIO.setScactval(wsaaInd, ZERO);
	}

	for (wsaaInd=1; wsaaInd < 10; wsaaInd++) {
		uswdIO.setTgtfund(wsaaInd, SPACES);
		uswdIO.setTgtprcnt(wsaaInd, ZERO);
		uswdIO.setTgfndcur(wsaaInd, SPACES);
		uswdIO.setTgfndtyp(wsaaInd, SPACES);	
	}

//Source Fund Write UTRN entry and update USWD source fund details

	wsbbInd = 1;
	for (wsaaInd=1; wsaaInd <= 10; wsaaInd++) {
		if (isEQ(wsaaBuySell[wsaaInd] ,"S")) {
			writeUswdSourceF6400();
			writeUtrnrec6000();			;
		}
	}

//Target Fund Write USWD Target fund details
	wsbbBSAmnt.set(0);
	for (wsaaInd=1; wsaaInd <= 10; wsaaInd++) {
		if (isEQ(wsaaBuySell[wsaaInd] ,"B")) {
			compute(wsbbBSAmnt,2).set(add(wsbbBSAmnt,wsaaBSAmnt[wsaaInd]));
		}
	}

	wsbbInd = 1;
	for (wsaaInd=1; wsaaInd <= 10; wsaaInd++) {
		if (isEQ(wsaaBuySell[wsaaInd] ,"B")) {
			writeUswdTargetF6500();
		}
	}
	uswdIO.setEffdate(bsscIO.getEffectiveDate());
	uswdIO.setOverrideFee(SPACES); // Overrider Switch fee, this can be Y, but leave it as N can relook when testing.
	uswdIO.setFormat(formatsInner.uswdrec);
	uswdIO.setFunction(varcom.writr);
	SmartFileCode.execute(appVars, uswdIO);
	if (isNE(uswdIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(uswdIO.getParams());
		fatalError600();
	}

	
}

protected void writeUswh8600() {

	uswhIO.setDataArea(SPACES);
	uswhIO.setChdrcoy(chdrmjaIO.getChdrcoy());
	uswhIO.setChdrnum(chdrmjaIO.getChdrnum());
	uswhIO.setTranno(chdrmjaIO.getTranno());
	uswhIO.setSwitchRule(t6647rec.swmeth);
	uswhIO.setFormat(formatsInner.uswhrec);
	uswhIO.setFunction(varcom.writr);
	SmartFileCode.execute(appVars, uswhIO);
	if (isNE(uswhIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(uswhIO.getParams());
		fatalError600();
	}
}


protected void writeUtrnrec6000() {
    
	utrnIO.setParams(SPACES);
	utrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
	utrnIO.setChdrnum(chdrmjaIO.getChdrnum());
	utrnIO.setLife(wsbbLife[wsaaInd]);
	utrnIO.setCoverage(wsbbCoverage[wsaaInd]);
	utrnIO.setRider(wsbbRider[wsaaInd]);
	utrnIO.setPlanSuffix(wsbbPlanSuffix[wsaaInd]);
	utrnIO.unitVirtualFund.set(wsaaVrtFnd[wsaaInd]);
	utrnIO.unitType.set(wsbbUnitType[wsaaInd]);
	utrnIO.setTranno(chdrmjaIO.getTranno());
	utrnIO.setTermid(SPACES);
	utrnIO.transactionDate.set(datcon1rec.intDate);
	utrnIO.transactionTime.set(getCobolTime());
	utrnIO.user.set(0); 
	utrnIO.batccoy.set(batcdorrec.company);
	utrnIO.batcbrn.set(batcdorrec.branch);
	utrnIO.batcactyr.set(batcdorrec.actyear);
	utrnIO.batcactmn.set(batcdorrec.actmonth);
	utrnIO.batctrcde.set(batcdorrec.trcde);
	utrnIO.batcbatch.set(batcdorrec.batch);
	utrnIO.jobnoPrice.set(0);
	utrnIO.fundRate.set(0);
	utrnIO.unitSubAccount.set("ACCUM");
	utrnIO.strpdate.set(0);
	readTableT54446300();  //AFR
	utrnIO.nowDeferInd.set(t5544rec.nowDeferInd); // AFR  
        utrnIO.nofUnits.set(0);
	utrnIO.nofDunits.set(wsccNofDunits[wsaaInd]);
	//compute(utrnIO.nofDunits,0).setRounded(mult(utrnIO.nofDunits,-1));
	utrnIO.moniesDate.set(bsscIO.getEffectiveDate());  	
	utrnIO.priceDateUsed.set(0);
	utrnIO.priceUsed.set(0);
	utrnIO.unitBarePrice.set(0); 
	utrnIO.crtable.set(zswrpf.getCrtable());
	utrnIO.cntcurr.set(chdrmjaIO.getCntcurr());        
	utrnIO.feedbackInd.set(" ");
	utrnIO.covdbtind.set(" ");
	utrnIO.inciNum.set(0);
	utrnIO.inciPerd01.set(0);
	utrnIO.inciPerd02.set(0);
	utrnIO.inciprm01.set(0);
	utrnIO.inciprm02.set(0);
	utrnIO.ustmno.set(0);
        utrnIO.contractAmount.set(0);	   
	utrnIO.fundCurrency.set(chdrmjaIO.cntcurr);
	utrnIO.fundAmount.set(0);
	readTableT56456200();
	utrnIO.sacscode.set(t5645rec.sacscode02);
	utrnIO.sacstyp.set(t5645rec.sacstype02);
	utrnIO.genlcde.set(t5645rec.glmap02);
	utrnIO.contractType.set(chdrmjaIO.getCnttype());
	utrnIO.triggerModule.set("FUNDSWCH");
	utrnIO.triggerKey.set(SPACES);
	utrnIO.procSeqNo.set(t6647rec.procSeqNo);  
	utrnIO.svp.set(1);
	utrnIO.discountFactor.set(1);
	utrnIO.surrenderPercent.set(0);
	readFromCovr6100();
	utrnIO.crComDate.set(covrmjaIO.getCrrcd());
	utrnIO.canInitUnitInd.set("N");
	utrnIO.switchIndicator.set("Y");
	//utrnIO.setTrandate(datcon1rec.intDate);//CR-262 
	//utrnIO.setRepflg(SPACES);//CR-262
	utrnIO.function.set(varcom.writr);
	utrnIO.format.set(formatsInner.utrnrec);
	SmartFileCode.execute(appVars, utrnIO);
	if (isNE(utrnIO.getStatuz(), varcom.oK)) {
		syserrrec.params.set(utrnIO.getParams());
		syserrrec.statuz.set(utrnIO.getStatuz());
		fatalError600();
	}	

}


protected void readFromCovr6100() {

	covrmjaIO.setParams(SPACES);
	covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
	covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
	covrmjaIO.setLife(wsbbLife[wsaaInd]);
	covrmjaIO.setCoverage(wsbbCoverage[wsaaInd]);
	covrmjaIO.setRider(wsbbRider[wsaaInd]);
	covrmjaIO.setPlanSuffix(wsbbPlanSuffix[wsaaInd]);
        covrmjaIO.setFunction(varcom.readr);
	SmartFileCode.execute(appVars, covrmjaIO);
	if (isNE(covrmjaIO.getStatuz(), varcom.oK) 
				&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {	
		syserrrec.params.set(covrmjaIO.getParams());
		syserrrec.statuz.set(covrmjaIO.getStatuz());
		fatalError600();
	}
}


protected void readTableT56456200()
 {
	itemIO.setParams(SPACES);
	itemIO.setItemcoy(chdrmjaIO.getChdrcoy());
	itemIO.setItemtabl("T5645");
	itemIO.setItempfx("IT");
	itemIO.setItemitem(wsaaSubr);
	itemIO.setFunction("READR");
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),"****")) {
		syserrrec.params.set(itemIO.getParams());
		syserrrec.statuz.set(syserrrec.dbioStatuz);
		fatalError600();
	}
	t5645rec.t5645Rec.set(itemIO.getGenarea());
 }

protected void readTableT54446300() {

		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setFunction(varcom.begn);

		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
 
		itdmIO.setItemcoy(chdrmjaIO.getChdrcoy());
		itdmIO.setItemtabl("T5544");
		wsaaUsmeth.set(t6647rec.swmeth);
		wsaaCurrcode.set(chdrmjaIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5544Key);

		itdmIO.setItmfrm(bsscIO.getEffectiveDate());
		SmartFileCode.execute(appVars, itdmIO);

		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}

		if (isNE(itdmIO.getItemcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), "T5544")
		|| isNE(itdmIO.getItemitem(), wsaaT5544Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT5544Key);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set("g033");
			fatalError600();
		}
		t5544rec.t5544Rec.set(itdmIO.getGenarea());
	}
		

protected void writeUswdSourceF6400() {

		uswdIO.setSrcfund(wsbbInd , wsaaVrtFnd[wsaaInd]);
		uswdIO.setScfndtyp(wsbbInd, wsbbUnitType[wsaaInd]);
		uswdIO.setScfndcur(wsbbInd, chdrmjaIO.getCntcurr());
		uswdIO.setScprcamt(wsbbInd, mult(wsccNofDunits[wsaaInd],-1), true);
		uswdIO.setScestval(wsbbInd, wsaaFundAmnt[wsaaInd]);
		uswdIO.setScactval(wsbbInd, ZERO);
		wsbbInd = wsbbInd +1;

		
}

protected void	writeUswdTargetF6500() {

		uswdIO.setTgtfund(wsbbInd, wsaaVrtFnd[wsaaInd]);
		wsbbPrcnt.set(0);
		//compute(wsbbPrcnt,2).setRounded(mult(div(wsaaBSAmnt[wsaaInd], wsbbBSAmnt),100));
		compute(wsbbPrcnt,2).setRounded(div(mult(wsaaBSAmnt[wsaaInd], 100),wsbbBSAmnt));
		mtlPercentage.set(add(mtlPercentage,wsbbPrcnt));
		if(isGT(mtlPercentage,99)){
			wsbbPrcnt.set(add(wsbbPrcnt,sub(100,mtlPercentage)));  
		}	
		uswdIO.setTgtprcnt(wsbbInd, wsbbPrcnt);
		uswdIO.setTgfndcur(wsbbInd, chdrmjaIO.getCntcurr());
		uswdIO.setTgfndtyp(wsbbInd, SPACES);	
		wsbbInd = wsbbInd +1;
}

protected void	rewriteZswrdte6600() {

		/*zswrdteIO.setValidflag("2");
                zswrdteIO.setFunction(varcom.writd);
  		SmartFileCode.execute(appVars, zswrdteIO);
		if (isNE(zswrdteIO.getStatuz(),varcom.oK)
		&& isNE(zswrdteIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(zswrdteIO.getStatuz());
			syserrrec.params.set(zswrdteIO.getParams());
			fatalError600();
		}*/
		writeZswrpos6700();
}

protected void writeZswrpos6700() 
{
	zswrDAO.deleteZswrpf(zswrpf.getChdrnum(), zswrpf.getChdrcoy(), "CH");/* IJTI-1523 */
	
	List<Zswrpf> zswrpfiList=new ArrayList<>();
	Zswrpf zw=new Zswrpf();
	
	zw.setChdrpfx("CH");
	zw.setChdrcoy(zswrpf.getChdrcoy());
	zw.setChdrnum(zswrpf.getChdrnum());
	zw.setCnntype(zswrpf.getCnntype()); //
	zw.setValidflag("1");
	zw.setCrtable(zswrpf.getCrtable());
	zw.setZafropt(zswrpf.getZafropt());
	zw.setZchkopt(zswrpf.getZchkopt());
	zw.setZregdte(zswrpf.getZregdte());
	zw.setAge(zswrpf.getAge());
	zw.setTranno(chdrmjaIO.getTranno().toInt());
	zw.setZafrfreq(zswrpf.getZafrfreq());
	zw.setZlastdte(zswrpf.getZnextdte());
	zw.setZwdwlrsn(zswrpf.getZwdwlrsn());
	zw.setZwdlddte(varcom.vrcmMaxDate.toInt());
	initialize(datcon4rec.datcon4Rec);
	datcon4rec.freqFactor.set(1);
	datcon4rec.frequency.set(zswrpf.getZafrfreq());
	datcon4rec.intDate1.set(zswrpf.getZnextdte());
	callProgram(Datcon4.class, datcon4rec.datcon4Rec);
	if (isNE(datcon4rec.statuz,varcom.oK)) {
	    syserrrec.statuz.set(datcon4rec.statuz);
	    syserrrec.params.set(datcon4rec.datcon4Rec);
	    fatalError600();
	}		
	zw.setZnextdte(datcon4rec.intDate2.toInt());
	zw.setUalfnd01(zswrpf.getUalfnd01());
	zw.setUalfnd02(zswrpf.getUalfnd02());
	zw.setUalfnd03(zswrpf.getUalfnd03());
	zw.setUalfnd04(zswrpf.getUalfnd04());
	zw.setUalfnd05(zswrpf.getUalfnd05());
	zw.setUalfnd06(zswrpf.getUalfnd06());
	zw.setUalfnd07(zswrpf.getUalfnd07());
	zw.setUalfnd08(zswrpf.getUalfnd08());
	zw.setUalfnd09(zswrpf.getUalfnd09());
	zw.setUalfnd10(zswrpf.getUalfnd10());

	zw.setUalprc01(zswrpf.getUalprc01());
	zw.setUalprc02(zswrpf.getUalprc02());
	zw.setUalprc03(zswrpf.getUalprc03());
	zw.setUalprc04(zswrpf.getUalprc04());
	zw.setUalprc05(zswrpf.getUalprc05());
	zw.setUalprc06(zswrpf.getUalprc06());
	zw.setUalprc07(zswrpf.getUalprc07());
	zw.setUalprc08(zswrpf.getUalprc08());
	zw.setUalprc09(zswrpf.getUalprc09());
	zw.setUalprc10(zswrpf.getUalprc10());	
	zw.setZlstupdate(zswrpf.getZlstupdate());
	zw.setZlstupdusr(zswrpf.getZlstupdusr());
	zw.setZafritem(zswrpf.getZafritem());
	zw.setZenable("E");
	zswrpfiList.add(zw);
	zswrDAO.insertZswrpfListRecord(zswrpfiList);
	if (zswrpfiList.size()<1){
		// syserrrec.statuz.set(zswrposIO.getStatuz());
	   //syserrrec.params.set(zswrposIO.getParams());
	   //fatalError600();
	}

}

protected void writeZafa6800() {
	
	  FixedLengthStringData ualprc01=new FixedLengthStringData(17);
	  FixedLengthStringData ualprc02=new FixedLengthStringData(17);
	  FixedLengthStringData ualprc03=new FixedLengthStringData(17);
	  FixedLengthStringData ualprc04=new FixedLengthStringData(17);
	  FixedLengthStringData ualprc05=new FixedLengthStringData(17);
	  FixedLengthStringData ualprc06=new FixedLengthStringData(17);
	  FixedLengthStringData ualprc07=new FixedLengthStringData(17);
	  FixedLengthStringData ualprc08=new FixedLengthStringData(17);
	  FixedLengthStringData ualprc09=new FixedLengthStringData(17);
	  FixedLengthStringData ualprc10=new FixedLengthStringData(17);
	  
	  FixedLengthStringData ualFundAmnt01=new FixedLengthStringData(17);
	  FixedLengthStringData ualFundAmnt02=new FixedLengthStringData(17);
	  FixedLengthStringData ualFundAmnt03=new FixedLengthStringData(17);
	  FixedLengthStringData ualFundAmnt04=new FixedLengthStringData(17);
	  FixedLengthStringData ualFundAmnt05=new FixedLengthStringData(17);
	  FixedLengthStringData ualFundAmnt06=new FixedLengthStringData(17);
	  FixedLengthStringData ualFundAmnt07=new FixedLengthStringData(17);
	  FixedLengthStringData ualFundAmnt08=new FixedLengthStringData(17);
	  FixedLengthStringData ualFundAmnt09=new FixedLengthStringData(17);
	  FixedLengthStringData ualFundAmnt10=new FixedLengthStringData(17);
	  
	  FixedLengthStringData Zwtavga01=new FixedLengthStringData(17);
	  FixedLengthStringData Zwtavga02=new FixedLengthStringData(17);
	  FixedLengthStringData Zwtavga03=new FixedLengthStringData(17);
	  FixedLengthStringData Zwtavga04=new FixedLengthStringData(17);
	  FixedLengthStringData Zwtavga05=new FixedLengthStringData(17);
	  FixedLengthStringData Zwtavga06=new FixedLengthStringData(17);
	  FixedLengthStringData Zwtavga07=new FixedLengthStringData(17);
	  FixedLengthStringData Zwtavga08=new FixedLengthStringData(17);
	  FixedLengthStringData Zwtavga09=new FixedLengthStringData(17);
	  FixedLengthStringData Zwtavga10=new FixedLengthStringData(17);
	  
	  
	  FixedLengthStringData Zdevita01=new FixedLengthStringData(17);
	  FixedLengthStringData Zdevita02=new FixedLengthStringData(17);
	  FixedLengthStringData Zdevita03=new FixedLengthStringData(17);
	  FixedLengthStringData Zdevita04=new FixedLengthStringData(17);
	  FixedLengthStringData Zdevita05=new FixedLengthStringData(17);
	  FixedLengthStringData Zdevita06=new FixedLengthStringData(17);
	  FixedLengthStringData Zdevita07=new FixedLengthStringData(17);
	  FixedLengthStringData Zdevita08=new FixedLengthStringData(17);
	  FixedLengthStringData Zdevita09=new FixedLengthStringData(17);
	  FixedLengthStringData Zdevita10=new FixedLengthStringData(17);
	  
	  
	  FixedLengthStringData Zbuysell01=new FixedLengthStringData(17);
	  FixedLengthStringData Zbuysell02=new FixedLengthStringData(17);
	  FixedLengthStringData Zbuysell03=new FixedLengthStringData(17);
	  FixedLengthStringData Zbuysell04=new FixedLengthStringData(17);
	  FixedLengthStringData Zbuysell05=new FixedLengthStringData(17);
	  FixedLengthStringData Zbuysell06=new FixedLengthStringData(17);
	  FixedLengthStringData Zbuysell07=new FixedLengthStringData(17);
	  FixedLengthStringData Zbuysell08=new FixedLengthStringData(17);
	  FixedLengthStringData Zbuysell09=new FixedLengthStringData(17);
	  FixedLengthStringData Zbuysell10=new FixedLengthStringData(17);
	  
	  FixedLengthStringData Zbsamnt01=new FixedLengthStringData(17);
	  FixedLengthStringData Zbsamnt02=new FixedLengthStringData(17);
	  FixedLengthStringData Zbsamnt03=new FixedLengthStringData(17);
	  FixedLengthStringData Zbsamnt04=new FixedLengthStringData(17);
	  FixedLengthStringData Zbsamnt05=new FixedLengthStringData(17);
	  FixedLengthStringData Zbsamnt06=new FixedLengthStringData(17);
	  FixedLengthStringData Zbsamnt07=new FixedLengthStringData(17);
	  FixedLengthStringData Zbsamnt08=new FixedLengthStringData(17);
	  FixedLengthStringData Zbsamnt09=new FixedLengthStringData(17);
	  FixedLengthStringData Zbsamnt10=new FixedLengthStringData(17);
	  
	  
	Zafapf zafaIO=new Zafapf();	
	zafaIO.setChdrpfx(zswrpf.getChdrpfx());
	zafaIO.setChdrcoy(zswrpf.getChdrcoy());
	zafaIO.setChdrnum(zswrpf.getChdrnum());
	zafaIO.setCnttype(zswrpf.getCnntype());
	zafaIO.setValidflag(zswrpf.getValidflag());
	zafaIO.setCrtable(zswrpf.getCrtable());
	zafaIO.setZafropt(zswrpf.getZafropt());
	zafaIO.setZchkopt(zswrpf.getZchkopt());
	zafaIO.setZregdte(zswrpf.getZregdte());
	zafaIO.setAge(zswrpf.getAge());
	zafaIO.setTranno(zswrpf.getTranno());
	zafaIO.setZafrfreq(zswrpf.getZafrfreq());
	zafaIO.setVrtfund(zswrpf.getVrtfund());
	Double lhundred=100.00;
	//for (wsaaInd=1; wsaaInd <= 10; wsaaInd++) {
	  zafaIO.setUalfnd01(wsaaVrtFnd[1].toString());
	  ualprc01.set(wsaaOrigFS[1]);
	  zafaIO.setUalprc01(ualprc01.toLong()/lhundred);
	  ualFundAmnt01.set(wsaaFundAmnt[1]);
	  zafaIO.setFundamnt01(ualFundAmnt01.toLong()/lhundred);
	  Zwtavga01.set(wsaaWtAvg[1]);
	  zafaIO.setZwtavga01(Zwtavga01.toLong()/lhundred);
	  Zdevita01.set(wsaaDeviate[1]);
	  zafaIO.setZdevita01(Zdevita01.toLong()/lhundred);
	  zafaIO.setZbuysell01(wsaaBuySell[1].toString());
	  
	  if(isLT(wsaaBSAmnt[1],ZERO))
	    Zbsamnt01.set(mult(wsaaBSAmnt[1],-1));
	  else
		Zbsamnt01.set(wsaaBSAmnt[1]);
	  
	  zafaIO.setZbsamnt01(Zbsamnt01.toLong()/lhundred);	  
	  
	  zafaIO.setUalfnd02(wsaaVrtFnd[2].toString());
	  ualprc02.set(wsaaOrigFS[2]);
	  zafaIO.setUalprc02(ualprc02.toLong()/lhundred);
	  ualFundAmnt02.set(wsaaFundAmnt[2]);
	  zafaIO.setFundamnt02(ualFundAmnt02.toLong()/lhundred);
	  Zwtavga02.set(wsaaWtAvg[2]);
	  zafaIO.setZwtavga02(Zwtavga02.toLong()/lhundred);
	  Zdevita02.set(wsaaDeviate[2]);
	  zafaIO.setZdevita02(Zdevita02.toLong()/lhundred);
	  zafaIO.setZbuysell02(wsaaBuySell[2].toString());

	  if(isLT(wsaaBSAmnt[2],ZERO))
		Zbsamnt02.set(mult(wsaaBSAmnt[2],-1));
	  else
		Zbsamnt02.set(wsaaBSAmnt[2]);
	  
	  zafaIO.setZbsamnt02(Zbsamnt02.toLong()/lhundred);
	  
	  
	  zafaIO.setUalfnd03(wsaaVrtFnd[3].toString());
	  ualprc03.set(wsaaOrigFS[3]);
	  zafaIO.setUalprc03(ualprc03.toLong()/lhundred);
	  ualFundAmnt03.set(wsaaFundAmnt[3]);
	  zafaIO.setFundamnt03(ualFundAmnt03.toLong()/lhundred);
	  Zwtavga03.set(wsaaWtAvg[3]);
	  zafaIO.setZwtavga03(Zwtavga03.toLong()/lhundred);
	  Zdevita03.set(wsaaDeviate[3]);
	  zafaIO.setZdevita03(Zdevita03.toLong()/lhundred);
	  zafaIO.setZbuysell03(wsaaBuySell[3].toString());
	   
	  if(isLT(wsaaBSAmnt[3],ZERO))
			Zbsamnt03.set(mult(wsaaBSAmnt[3],-1));
	  else
			Zbsamnt03.set(wsaaBSAmnt[3]);
	  
	  zafaIO.setZbsamnt03(Zbsamnt03.toLong()/lhundred);
	   
	  zafaIO.setUalfnd04(wsaaVrtFnd[4].toString());
	  ualprc04.set(wsaaOrigFS[4]);
	  zafaIO.setUalprc04(ualprc04.toLong()/lhundred);
	  ualFundAmnt04.set(wsaaFundAmnt[4]);
	  zafaIO.setFundamnt04(ualFundAmnt04.toLong()/lhundred);
	  Zwtavga04.set(wsaaWtAvg[4]);
	  zafaIO.setZwtavga04(Zwtavga04.toLong()/lhundred);
	  Zdevita04.set(wsaaDeviate[4]);
	  zafaIO.setZdevita04(Zdevita04.toLong()/lhundred);
	  zafaIO.setZbuysell04(wsaaBuySell[4].toString());
	  if(isLT(wsaaBSAmnt[4],ZERO))
			Zbsamnt04.set(mult(wsaaBSAmnt[4],-1));
	  else
			Zbsamnt04.set(wsaaBSAmnt[4]);
	  
	  zafaIO.setZbsamnt04(Zbsamnt04.toLong()/lhundred);
	  
	  zafaIO.setUalfnd05(wsaaVrtFnd[5].toString());
	  ualprc05.set(wsaaOrigFS[5]);
	  zafaIO.setUalprc05(ualprc05.toLong()/lhundred);
	  ualFundAmnt05.set(wsaaFundAmnt[5]);
	  zafaIO.setFundamnt05(ualFundAmnt05.toLong()/lhundred);
	  Zwtavga05.set(wsaaWtAvg[5]);
	  zafaIO.setZwtavga05(Zwtavga05.toLong()/lhundred);
	  Zdevita05.set(wsaaDeviate[5]);
	  zafaIO.setZdevita05(Zdevita05.toLong()/lhundred);
	  zafaIO.setZbuysell05(wsaaBuySell[5].toString());
	  if(isLT(wsaaBSAmnt[5],ZERO))
			Zbsamnt05.set(mult(wsaaBSAmnt[5],-1));
	  else
			Zbsamnt05.set(wsaaBSAmnt[5]);
	  zafaIO.setZbsamnt05(Zbsamnt05.toLong()/lhundred);
	  
	  zafaIO.setUalfnd06(wsaaVrtFnd[6].toString());
	  ualprc06.set(wsaaOrigFS[6]);
	  zafaIO.setUalprc06(ualprc06.toLong()/lhundred);
	  ualFundAmnt06.set(wsaaFundAmnt[6]);
	  zafaIO.setFundamnt06(ualFundAmnt06.toLong()/lhundred);
	  Zwtavga06.set(wsaaWtAvg[6]);
	  zafaIO.setZwtavga06(Zwtavga06.toLong()/lhundred);
	  Zdevita06.set(wsaaDeviate[6]);
	  zafaIO.setZdevita06(Zdevita06.toLong()/lhundred);
	  zafaIO.setZbuysell06(wsaaBuySell[6].toString());
	  if(isLT(wsaaBSAmnt[6],ZERO))
			Zbsamnt06.set(mult(wsaaBSAmnt[6],-1));
	  else
			Zbsamnt06.set(wsaaBSAmnt[6]);
	  
	  zafaIO.setZbsamnt06(Zbsamnt06.toLong()/lhundred);
	  
	  
	  zafaIO.setUalfnd07(wsaaVrtFnd[7].toString());
	  ualprc07.set(wsaaOrigFS[7]);
	  zafaIO.setUalprc07(ualprc07.toLong()/lhundred);
	  ualFundAmnt07.set(wsaaFundAmnt[7]);
	  zafaIO.setFundamnt07(ualFundAmnt07.toLong()/lhundred);
	  Zwtavga07.set(wsaaWtAvg[7]);
	  zafaIO.setZwtavga07(Zwtavga07.toLong()/lhundred);
	  Zdevita07.set(wsaaDeviate[7]);
	  zafaIO.setZdevita07(Zdevita07.toLong()/lhundred);
	  zafaIO.setZbuysell07(wsaaBuySell[7].toString());
	 
	  if(isLT(wsaaBSAmnt[7],ZERO))
			Zbsamnt07.set(mult(wsaaBSAmnt[7],-1));
	  else
			Zbsamnt07.set(wsaaBSAmnt[7]);
	  
	  zafaIO.setZbsamnt07(Zbsamnt07.toLong()/lhundred);
	  
	  
	  
	  zafaIO.setUalfnd08(wsaaVrtFnd[8].toString());
	  ualprc08.set(wsaaOrigFS[8]);
	  zafaIO.setUalprc08(ualprc08.toLong()/lhundred);
	  ualFundAmnt08.set(wsaaFundAmnt[8]);
	  zafaIO.setFundamnt08(ualFundAmnt08.toLong()/lhundred);
	  Zwtavga08.set(wsaaWtAvg[8]);
	  zafaIO.setZwtavga08(Zwtavga08.toLong()/lhundred);
	  Zdevita08.set(wsaaDeviate[8]);
	  zafaIO.setZdevita08(Zdevita08.toLong()/lhundred);
	  zafaIO.setZbuysell08(wsaaBuySell[8].toString());
	 
	  if(isLT(wsaaBSAmnt[8],ZERO))
			Zbsamnt08.set(mult(wsaaBSAmnt[8],-1));
	  else
			Zbsamnt08.set(wsaaBSAmnt[8]);
	  
	  zafaIO.setZbsamnt08(Zbsamnt08.toLong()/lhundred);
	  
	  
	  zafaIO.setUalfnd09(wsaaVrtFnd[9].toString());
	  ualprc09.set(wsaaOrigFS[9]);
	  zafaIO.setUalprc09(ualprc09.toLong()/lhundred);
	  ualFundAmnt09.set(wsaaFundAmnt[9]);
	  zafaIO.setFundamnt09(ualFundAmnt09.toLong()/lhundred);
	  Zwtavga09.set(wsaaWtAvg[9]);
	  zafaIO.setZwtavga09(Zwtavga09.toLong()/lhundred);
	  Zdevita09.set(wsaaDeviate[9]);
	  zafaIO.setZdevita09(Zdevita09.toLong()/lhundred);
	  zafaIO.setZbuysell09(wsaaBuySell[9].toString());
	  
	  if(isLT(wsaaBSAmnt[9],ZERO))
			Zbsamnt09.set(mult(wsaaBSAmnt[9],-1));
	  else
			Zbsamnt09.set(wsaaBSAmnt[9]);
	  
	  zafaIO.setZbsamnt09(Zbsamnt09.toLong()/lhundred);
	  
	  
	  zafaIO.setUalfnd10(wsaaVrtFnd[10].toString());
	  ualprc10.set(wsaaOrigFS[10]);
	  zafaIO.setUalprc10(ualprc10.toLong()/lhundred);
	  ualFundAmnt10.set(wsaaFundAmnt[10]);
	  zafaIO.setFundamnt10(ualFundAmnt10.toLong()/lhundred);
	  Zwtavga10.set(wsaaWtAvg[10]);
	  zafaIO.setZwtavga10(Zwtavga10.toLong()/lhundred);
	  Zdevita10.set(wsaaDeviate[10]);
	  zafaIO.setZdevita10(Zdevita10.toLong()/lhundred);
	  zafaIO.setZbuysell10(wsaaBuySell[10].toString());
	  
	  if(isLT(wsaaBSAmnt[10],ZERO))
			Zbsamnt10.set(mult(wsaaBSAmnt[10],-1));
	  else
			Zbsamnt10.set(wsaaBSAmnt[10]);
	  
	  zafaIO.setZbsamnt10(Zbsamnt10.toLong()/lhundred);
	  
	//}

	zafaIO.setZwdwlrsn(zswrpf.getZwdwlrsn());
	zafaIO.setZlastdte(zswrpf.getZlastdte());
	zafaIO.setZnextdte(zswrpf.getZnextdte());
	zafaIO.setZwdlddte(zswrpf.getZwdlddte());
	if (isEQ(wsaaTran,"Y")) {
		zafaIO.setLinedetail("AFR Transaction Successfull");
	}
	else {
		zafaIO.setLinedetail("AFR Not Done");
	}
	zafaIO.setZlstupdte(bsscIO.getEffectiveDate().toInt());
	zafaIO.setZafritem(zswrpf.getZafritem());
    //    zafaIO.setFunction(varcom.writr);
    //    zafaIO.setFormat(formatsInner.zafarec);
	zafapfList.add(zafaIO);
	//SmartFileCode.execute(appVars, zafaIO);
	zafaDAO.insertZafapfRecord(zafapfList);
	if (zafapfList.size()<1) {
	  // syserrrec.statuz.set(zafaIO.getStatuz());
	  // syserrrec.params.set(zafaIO.getParams());
	  // fatalError600();
	}
}		

protected void checkUtrns6900( )
	{
		/* check if outstanding UTRNS exist.  If they do exist, disallow   */
		/* fund switch                                                     */
		utrnaloIO.setRecKeyData(SPACES);
		utrnaloIO.setRecNonKeyData(SPACES);
		utrnaloIO.setChdrcoy(zswrpf.getChdrcoy());
		utrnaloIO.setChdrnum(zswrpf.getChdrnum());
		utrnaloIO.setFunction(varcom.begn);
		//start life performance atiwari23
		utrnaloIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		utrnaloIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		//end;
		SmartFileCode.execute(appVars, utrnaloIO);
		if (isNE(utrnaloIO.getStatuz(),varcom.oK)
		&& isNE(utrnaloIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(utrnaloIO.getStatuz());
			syserrrec.params.set(utrnaloIO.getParams());
			fatalError600();
		}
		/* Where there are unprocessed UTRNS on a contract, regardless of  */
		/* whether they are for a single policy where the contract is a    */
		/* multiple policy contract, display the error message and do not  */
		/* allow the switch to take place.                                 */
		if (isNE(utrnaloIO.getChdrcoy(),bsprIO.getCompany())
		|| isNE(utrnaloIO.getChdrnum(),chdrmjaIO.getChdrnum())
		|| isEQ(utrnaloIO.getStatuz(),varcom.endp)) {
			utrnaloIO.setStatuz(varcom.endp);
		}
		else {
			wsaaConUtrnalo.set("Y");
		}
	}
private static final class ControlTotalsInner { 
	/* CONTROL-TOTALS */
private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
}
private static final class FormatsInner { 
	/* FORMATS */
private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
private FixedLengthStringData uswdrec = new FixedLengthStringData(10).init("USWDREC");
private FixedLengthStringData uswhrec = new FixedLengthStringData(10).init("USWHREC");
private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
private FixedLengthStringData zswrrec = new FixedLengthStringData(10).init("ZSWRREC");
private FixedLengthStringData zafarec = new FixedLengthStringData(10).init("ZSWRREC");
private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
}
}
