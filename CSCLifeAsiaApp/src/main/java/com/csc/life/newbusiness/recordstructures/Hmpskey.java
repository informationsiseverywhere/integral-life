package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:42
 * Description:
 * Copybook name: HMPSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hmpskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hmpsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hmpsKey = new FixedLengthStringData(64).isAPartOf(hmpsFileKey, 0, REDEFINE);
  	public FixedLengthStringData hmpsChdrcoy = new FixedLengthStringData(1).isAPartOf(hmpsKey, 0);
  	public FixedLengthStringData hmpsCntbranch = new FixedLengthStringData(2).isAPartOf(hmpsKey, 1);
  	public FixedLengthStringData hmpsCnttype = new FixedLengthStringData(3).isAPartOf(hmpsKey, 3);
  	public PackedDecimalData hmpsYear = new PackedDecimalData(4, 0).isAPartOf(hmpsKey, 6);
  	public PackedDecimalData hmpsMnth = new PackedDecimalData(2, 0).isAPartOf(hmpsKey, 9);
  	public FixedLengthStringData hmpsCntcurr = new FixedLengthStringData(3).isAPartOf(hmpsKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(hmpsKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hmpsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hmpsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}