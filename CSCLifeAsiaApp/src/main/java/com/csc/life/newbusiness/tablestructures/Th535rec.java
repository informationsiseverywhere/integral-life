package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:00
 * Description:
 * Copybook name: TH535REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th535rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th535Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData optionChange = new FixedLengthStringData(10).isAPartOf(th535Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(490).isAPartOf(th535Rec, 10, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th535Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th535Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}