package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:18
 * Description:
 * Copybook name: ZLIFELCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zlifelckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zlifelcFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zlifelcKey = new FixedLengthStringData(256).isAPartOf(zlifelcFileKey, 0, REDEFINE);
  	public FixedLengthStringData zlifelcChdrcoy = new FixedLengthStringData(1).isAPartOf(zlifelcKey, 0);
  	public FixedLengthStringData zlifelcLifcnum = new FixedLengthStringData(8).isAPartOf(zlifelcKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(zlifelcKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zlifelcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zlifelcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}