package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:13
 * Description:
 * Copybook name: CHDRCFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrcfikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrcfiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrcfiKey = new FixedLengthStringData(64).isAPartOf(chdrcfiFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrcfiChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrcfiKey, 0);
  	public FixedLengthStringData chdrcfiChdrnum = new FixedLengthStringData(8).isAPartOf(chdrcfiKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(chdrcfiKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrcfiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrcfiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}