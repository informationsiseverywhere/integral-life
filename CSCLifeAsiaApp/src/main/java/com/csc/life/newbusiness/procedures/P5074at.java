/*
 * File: P5074at.java
 * Date: 30 August 2009 0:02:34
 * Author: Quipoz Limited
 *
 * Class transformed from P5074AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Properties;//IBPLIFE-1357

import org.slf4j.Logger; //ILIFE-8709
import org.slf4j.LoggerFactory; //ILIFE-8709
import org.springframework.util.ObjectUtils;

import com.csc.diary.procedures.Dryproces;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.accounting.dataaccess.dao.AcblpfDAO; //ILIFE-8709
import com.csc.fsu.accounting.dataaccess.dao.RtrnpfDAO; //ILIFE-8709
import com.csc.fsu.accounting.dataaccess.model.Acblpf; //ILIFE-8709
import com.csc.fsu.clients.dataaccess.dao.ClexpfDAO;//IBPLIFE-1357
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO; //ILIFE-8709
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO; //ILIFE-8709
import com.csc.fsu.clients.dataaccess.model.Clexpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf; //ILIFE-8709
import com.csc.fsu.clients.dataaccess.model.Clrfpf; //ILIFE-8709
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO; //ILIFE-8709
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO; //ILIFE-8709
import com.csc.fsu.general.dataaccess.dao.ZfmcpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf; //ILIFE-8709
import com.csc.fsu.general.dataaccess.model.Ptrnpf; //ILIFE-8709
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.ZrdecplcPojo; //ILIFE-8709
import com.csc.fsu.general.procedures.ZrdecplcUtils; //ILIFE-8709
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.general.tablestructures.Tr29srec;//IBPLIFE-1357
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AgcmTableDAM;
import com.csc.life.agents.dataaccess.AgpyagtTableDAM;
import com.csc.life.agents.dataaccess.AgpydocTableDAM;
import com.csc.life.agents.dataaccess.ZrapTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.T5647rec;
import com.csc.life.agents.tablestructures.Tr695rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.dataaccess.dao.RskppfDAO;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO; //ILIFE-8709
import com.csc.life.contractservicing.dataaccess.model.Rskppf;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf; //ILIFE-8709
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.enquiries.procedures.CrtundwrtUtil; //IBPLIFE-680
import com.csc.life.enquiries.procedures.CrtundwrtUtilImpl; //IBPLIFE-680
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.InctTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.PcddlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO; //ILIFE-8709
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO; //ILIFE-8709
import com.csc.life.newbusiness.dataaccess.dao.UnltpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Covtpf; //ILIFE-8709
import com.csc.life.newbusiness.dataaccess.model.Exclpf;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf; //ILIFE-8709
import com.csc.life.newbusiness.dataaccess.model.Unltpf;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.newbusiness.tablestructures.T3615rec;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;
import com.csc.life.newbusiness.tablestructures.Tr52qrec;
import com.csc.life.newbusiness.tablestructures.Tr627rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO; //ILIFE-8709
import com.csc.life.productdefinition.dataaccess.dao.ExclpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO; //ILIFE-8709
import com.csc.life.productdefinition.dataaccess.dao.ZswrpfDAO;
//ILIFE-8709 end
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Zswrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.procedures.Rlliadb;
import com.csc.life.productdefinition.procedures.RlpdlonPojo; //ILIFE-8709
import com.csc.life.productdefinition.procedures.RlpdlonUtils; //ILIFE-8709
import com.csc.life.productdefinition.procedures.Vpuubbl;
import com.csc.life.productdefinition.procedures.Vpxubbl;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Rlliadbrec;
import com.csc.life.productdefinition.recordstructures.Stdtallrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpmfmtrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5667rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5676rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.productdefinition.tablestructures.Tr59xrec;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.procedures.Actvres;
import com.csc.life.reassurance.recordstructures.Actvresrec;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO; //ILIFE-8709
//ILIFE-8709 start
import com.csc.life.regularprocessing.dataaccess.dao.ZctnpfDAO;
import com.csc.life.regularprocessing.dataaccess.dao.ZptnpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.regularprocessing.dataaccess.model.Zctnpf;
import com.csc.life.regularprocessing.dataaccess.model.Zptnpf;
import com.csc.life.regularprocessing.recordstructures.MgmFeeRec;
import com.csc.life.regularprocessing.recordstructures.Ubblallpar;
import com.csc.life.regularprocessing.tablestructures.T5534rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.recordstructures.Vpxubblrec;
import com.csc.life.underwriting.dataaccess.dao.UndcpfDAO;
import com.csc.life.underwriting.dataaccess.dao.UndlpfDAO;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undcpf;
import com.csc.life.underwriting.dataaccess.model.Undlpf;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.SftlockUtil;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sendmlrec;//IBPLIFE-1357
import com.csc.smart.recordstructures.SftlockRecBean;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.model.Rtrnpf;
import com.csc.smart400framework.dataaccess.model.Zfmcpf;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.TwilloSMSProvider;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.mail.SendMail;


/**
* <pre>
*
*REMARKS.
*                AT MODULE FOR LIFE ISSUE
*
* This AT  module  converts  the  proposal  into  an  in  force
* contract.  The contract number is passed in the AT parameters
* area  as  the  "primary  key". The total amount of cash to be
* applied to  the  contract from contract suspense is passed as
* the "transaction area".
*
*PREPARATION
*
* Read the contract header.
*
* Call DATCON1 and store today's date (TDAY).
*
* Read the Transaction Status Codes table entry (T5679) for the
* current transaction code. The record status codes and premium
* status codes are required for all record updates.
*
*CONTRACT HEADER LEVEL
*
* The  roles for all  the  contract  level  client  fields  are
* checked  and  updated  if required.  Role records will not
* exist, so for each role on the client header write a role
* record.
* Check the following roles:
*
*           Dispatch Address, if  blank  on  CHDR,  set up role
*                using Owner,
*           Payer, if blank on CHDR, set up role using Owner,
*           Assignee,  if not blank write a role record for
*                      assignee.
*
* Write the beneficiary roles as follows:
*
*      Read the beneficiary file (BNFYLNB) for contract company
*           and contract number, writing a role (CLRR) for each
*           record read.
*
* Check  the  commission details (PCDDLNB) for the contract. If
* there  are no records set up, write one with all details from
* the contract header except:
*           - agent number from servicing agent on the contract
*                header,
*           - percentage commission split = 100%,
*           - percentage bonus split = 100%.
*
*LIVES ASSURED
*
* Read all lives (LIFELNB)  for  the contract. Update each life
* as follows:
*           Valid flag to '1'
*           Transaction number from contract header
*           Status code to life status from T5679 read above
*
*COMPONENTS - CALCULATE SUMMARISED POLICIES
*
* If plan processing is  required  (no-policies  in  plan > 1),
* read all component transaction  records  for  the coverage to
* work out the number of components to be maintained in summary
* format. For each  coverage/rider,  one  or  more  transaction
* records will have been  written. Each transaction will have a
* "no-policies-applicable"    field.    Store    the   SMALLEST
* no-policies-applicable out of  the FIRST transaction for each
* coverage/rider (i.e.  ignore  all  transaction records, other
* than the first one.)
*
* This smallest number is the  number  of policies to be stored
* in the single coverage/rider  records  written  with  a  plan
* suffix  of zero. (Non-plan  policies  will  have  their  only
* policy coverage/rider records written  with  a plan suffix of
* zero.)  All  other  policies   within   the  plan  will  have
* individual coverage/rider  records written with non-zero plan
* suffixes. If this number is  1,  there  will  be  no  records
* written in summary format. If  this is the same as the number
* of  policies in the plan,  only  a  summary  record  will  be
* written.
*
* Before  they  are  forgotten, store this "smallest number" on
* the  contract  header  as  the no-summarised-policies and the
* next-available-suffix  and  re-write the record.  (The record
* will be actually  updated  again  later,  but it is needed in
* this state by called subroutines).
*
*COMPONENTS - CREATE COVERAGE/RIDER INFORMATION
*
* Coverage/rider  records are  written  and  processed  in  one
* standard way. This way  applies  to both a summary/individual
* policy components and  to  variation  policy  components. The
* generation of component information  is driven by reading the
* coverage/rider  transaction  (COVTLNB)  records  sequentially
* (again).  Once each record is finished with, delete it.
*
* The following cases are possible when creating coverage/rider
* information (COVRLNB):
*
*      A) Plan processing is  not  applicable  or  only summary
*           records  are  required.  Write  one  coverage/rider
*           record for each  transaction  record  read  with  a
*           suffix of zero.
*
*      B) Plan processing is applicable, but no summary records
*           are to be  written.  For  each  transaction record,
*           divide the amounts  (e.g.  sum insured, premium and
*           lump sum) by no. of policies applicable. Write  one
*           coverage/rider record  for  each policy. Start with
*           suffix number 1 and add one until a transaction for
*           a  different   component   is   read  (coverage-no,
*           rider-no different).
*
*      C) Plan  processing  is   applicable  and  some  of  the
*           policies  are  to  be  summarised.  For  the  first
*           transaction for a  component  (a set of transaction
*           records with the  same  coverage  and  rider number
*           apply  to the one  component),  if  the  number  of
*           policies which apply  to the transaction is greater
*           than  the   "SMALLEST"   number  calculated  above,
*           generate  a  summary  record  for  this  "SMALLEST"
*           no. of policies. Apportion the premium, sum assured
*           and lump sum by dividing them  by  no. of  policies
*           applicable to  transaction  and  multiplying by the
*           number to be summarised.  Write  this record with a
*           plan suffix of zero. Then write one record for each
*           policy remaining  unaccounted  for  on the existing
*           transaction record, and  for  each  policy  on  any
*           other transaction  records  (for  this  component).
*           Calculate  the  first   suffix  as  the  number  of
*           policies summarised plus  one.  Then  add  one each
*           time as in method  (B).  If  the number of policies
*           applicable  is  the  same   as  the  number  to  be
*           summarised, write a summary coverage/rider for this
*           transaction and individual policy ones as above for
*           the subsequent transactions.
*
* Before the coverage records are written calculate the BENEFIT
* BILLING.
*      1) If the  benefit  billing  method (on T5687) is blank,
*           accumulate  total  premium  amount  for  accounting
*           later.   Accumulate   single  premium  and  regular
*           premium  separately.  Single  and  Regular  premium
*           ammounts are held separately on COVT/COVR. Both may
*           be present on one  COVT/COVR if the  component edit
*           rules allow. If it is a regular premium, it must be
*           adjusted  for  the  period  which is actually being
*           paid for. This is done by calling DATCON3 with  the
*           contract  commencement  date, billing  commencement
*           date and frequency. Multiply  the  COVRLNB  premium
*           amount by the factor returned to  give  the  actual
*           premium amount to be accounted for. Accumulate both
*           the adjusted and non-adjusted amounts.
*
*      2) If the  benefit  billing method is not blank, look-up
*           the benefit billing subroutine (T5534) and call it.
*           Linkage as follows:
*           - Company
*           - Contract number
*           - Life number
*           - Coverage number
*           - Rider number
*           - Plan suffix
*           - Contract type
*           - Contract currency
*           - Batch key
*           - Client type
*           - Client currency
*           - Status
*
*      (The copybook UBBLALLPAR contains the above fields.)
*
* Access the general  coverage  rider details (T5687) effective
* at the risk commencement  date  for  the coverage/rider code.
* Initialise coverage/rider records as follows:
*           All fields from COVTLNB (NB amounts)
*           Current from and current to from contract header
*           Transaction number from contract header
*           Valid flag to '1'
*           Status code to coverage/rider status from T5679
*           Premium status  code  to coverage/rider status from
*                T5679 (regular premium if billing frequency on
*                contract header is not '00' and single premium
*                indicator  on  T5687  is  not  'Y',  otherwise
*                single premium)
*           Coverage/rider  commencement  date from "effective"
*                date
*           Statistical and reporting codes from T5687
*           Update the optional extras records (LEXT), and set
*           the benefit billing date,rerate and CPI dates.
*           All other numerics and dates to zero
*
* The COVR-CPI-DATE is calculated as follows:-
*
* The INCTPF option field, updated during  proposal. This will
* be  driven  from  Risk  Commencement  Date using a Frequency
* Factor stored on  T6658.  T6658 is keyed  on the Anniversary
* Method from T5687.
*
* A  check  on  'Minimum Term to Cessation'  and 'Maximum Age'
* must  be  made on T6658. If the contract does  not pass  the
* test for Minimum Term or Maximum Age then set  COVR-CPI-DATE
* to HI-DATE.
*
* If a LEXT record exists AND T6654 'Add to Existing Component'
* is equal to spaces then set COVR-CPI-DATE to HI-DATE.
*
* Delete INCTPF record.
*
* If this is a flexible premium contract & the installment premium
* not = 0 then write a FPCO record with information such as
* target premium, minimum overdue, maximum overdue.
*
* After each COVRLNB record is written:
* If the benefit billing method is not blank, look-up
*          the benefit billing subroutine (T5534) and call it.
*          Linkage as follows :
*          - Company
*          - Contract number
*          - Life number
*          - Coverage number
*          - Rider number
*          - Plan suffix
*          - Contract type
*          - Contract currency
*          - Batch key
*          - Client type
*          - Client currency
*          - Status
*
* AFTER EACH COVRLNB record is written:
*
*      3) If the stamp duty method is not blank, call the stamp
*           duty   calculation   subroutine   (held  on  T5676)
*           obtained  for  the method on T5687.  Accumulate the
*           amount for accounting later. Linkage as follows:
*           - Company
*           - Contract number
*           - Life number
*           - Coverage number
*           - Rider number
*           - Policy suffix
*           - Stamp duty amount due
*           - Effective date (contract commencement date)
*           - contract currency code
*           - Status
*
*      (The copybook STDTALLREC contains the above fields.)
*
*      4) Look  up   the   coverage/rider   generic   component
*           processing   programs    (T5671   -   accessed   by
*           transaction number concatenated with coverage/rider
*           code). Call  each non-blank subroutine, passing the
*           standard linkage:
*           - Company
*           - Contract number
*           - Life number
*           - Coverage number
*           - Rider number
*           - Policy suffix
*           - Premium amount paid/applicable (as calculated for
*                accumulation  in  (1)  above,  single  premium
*                amount/adjusted regular amount)
*           - Batch key
*           - Status
*
*      (The copybook ISUALLREC contains the above fields.)
*
*      For each component, it will call ACTVRES to activate the
*      reassurance cessions (RACD) for the component and then
*      update the reassurance history. ACTVRES will call REXPUPD
*      to update the amount retained by the company, reassured
*      by treaty and reassured facultatively.
*
*      5) COMMISSION  CALCULATION  -  The  following  steps are
*           required  for  each  commission  agent  (read  from
*           PCDDLNB):
*           Work through 5a) to 5f) using the Regular Premium
*           amount, if non-zero. T5687 now holds Commission methods
*           for Regular, Single and Top Ups, so use those applicable
*           for Regular first.
*           Next trundle through 5a) to 5c) if the Single Premium
*           amount is non-zero using the appropriate method from
*           T5687 for Single premiums.
*
*      5a) If  the  initial commission calculation method (from
*           T5687  looked-up  above)  is not blank, look up the
*           subroutine  required  (T5647).  For each commission
*           split  record  set  up  for the contract (PCDDLNB),
*           call the commission calculation subroutine passing:
*           - Company
*           - Contract number
*           - Life number
*           - Coverage number
*           - Rider number
*           - Plan suffix
*           - Agent number (from PCDDLNB)
*           - Joint life number
*           - Coverage/rider code (CRTABLE)
*           - Commission method code
*           - Proportion  of  annualised premium (calculated as
*                COVRLNB  premium  amount * payment frequency *
*                split%)
*           - Premium amount paid/applicable (as calculated for
*                accumulation in (1) above)
*           - Total initial commission = 0
*           - Paid initial commission = 0
*           - Earned initial commission = 0
*           - Payment amount = 0
*           - Earned amount = 0
*           - Effective date = coverage/rider effective date
*           - Status
*
*      (The copybook COMLINKREC contains the above fields.)
*
*      5b) If  the initial commission payment method from T5687
*           above is not blank, look up the subroutine required
*           from  T5644.  For  each  commission agent (as above
*           from PCDDLNB), if the commission payment method was
*           not blank,  call  it with the standard linkage area
*           as passed back from the last subroutine. Otherwise,
*           if the  method was blank, look up the agent details
*           (AGLFLNB) and use the initial payment method stored
*           there in the  same  way as if it was entered on the
*           table.
*
*      5c) Accumulate  the  commission  amounts  for accounting
*           later.  Keep  one set of totals per agent (there is
*           a  maximum  of  10  commission  agents allowed on a
*           contract), per subsidiary account type:
*           - Initial commission due
*           - Initial commission earned
*           - Initial commission paid in advance
*
*      5d) If  the  servicing  commission  payment  method from
*           T5687  above  is  not  blank  (otherwise  check the
*           agent details as above for the method), look up the
*           subroutine required from T5644 and call it with the
*           standard  linkage  area  re-initialised  as in (5a)
*           above,  and  accumulate  the  amounts  returned for
*           servicing    commission   accounting   later   (per
*           agent/sub-a/c):
*           - Servicing commission due
*           - Servicing commission earned
*
*      5e) For renewal commission payment method, use the method
*          defined in the agent details (AGNT). If this method is
*          set to spaces, then use the method defined on table
*          T5687.  Look up the subroutine required from T5644
*          and call it with the standard linkage area
*          re-initialised as in (5A) above and accumulated  for
*          the amounts returned for renewal commission accounting
*          later. ( Per agent/sub-a/c):
*           - Renewal commission due
*           - Renewal commission earned
*
*      5f) For  each  commission agent (as above from PCDDLNB),
*           write an AGCM record as follows:
*           - Current from, current  to and transaction no from
*                contract header
*           - Valid flag to '1'
*           - "Key" details from coverage/rider record
*           - Annualised premium as calculated above
*           - Initial  commission amounts (total, paid, earned)
*                as returned from steps (5a/b) above
*           - Effective date as above
*
*
*      6) Keep  track  of  the  oldest  premium  cessation date
*           written.  These dates will define the contract risk
*           cessation and premium cessation dates.
*
* REASSURANCE
*
* For each Coverage/Rider written, if there are any associated
*   Reassurance records, ( found by using the RACTLNB logical ),
*   then Read-lock, update Tranno field, set validflag from
*   '3' to '1' (ie. In Force) and Rewrite each one of them.
*
*CONTRACT HEADER
*
* Look-up the contract  definition  details  (T5688)  using the
* contract type code, effective  at  risk commencement date. If
* the  contract fee method is not blank, look-up the subroutine
* required from T5674 and call it with the following linkage:
*           - Company
*           - Contract fee
*           - Status
*
*      (The copybook MGFEELREC contains the above fields.)
*
* Update the contract header itself as follows:
*
*           Valid flag to '1'
*           Contract  commencement  date  to  original contract
*                commencement date
*           Contract status to status on T5679
*           Contract status date to today
*           Contract   status   transaction   no   to  contract
*                transaction number
*           Premium status code  to  contract status from T5679
*                (regular  premium   if  billing  frequency  on
*                contract header  is  not  00, otherwise single
*                premium)
*           Premium status date to today
*           Premium   status   transaction   no   to   contract
*                transaction number
*           Transaction    number   last   used   to   contract
*                transaction number
*           If the  total  of  the single premiums and adjusted
*                regular  premiums  (ie  there was some sort of
*                payment made):
*           - current   instalment   from   date   to  original
*                commencement date
*           - current    instalment    to   date   to   billing
*                commencement date
*           - current total instalment amounts 01-06;  01=total
*                single  premium plus adjusted regular premiums
*                accumulated  above,  02=contract  fee  amount;
*                03=tolerance  amount  calculated as 01+02 less
*                the  amount passed in the AT transaction area;
*                04, 05=0;  06=total(01 to 05)
*           If the billing frequency is 00:
*           - paid to date, billed to date to premium cessation
*                date worked out above
*           If the billing frequency is not 00:
*           - paid   to  date,  billed  to  date  from  billing
*                commencement date
*           - instalment  billing  commencement date to billing
*                commencement date
*           - instalment  billing  to date to premium cessation
*                date worked out above
*           - billing  day  to day part of billing commencement
*                date
*           - billing   month   to   month   part   of  billing
*                commencement date
*           - instalment   amounts   01-06;   01=total  regular
*                premiums  accumulated  above,  02=contract fee
*                amount; 03, 04, 05=0; 06=total(01 to 05)
*           - The stdg. inst to date (SINSTTO) is no longer a
*                Life requirement so set to Max-date.
*
*CONTRACT ACCOUNTING
*
* The total premium and  stamp  duty  amounts  will  have  been
* accumulated.  The  contract  fee  amount  will also have been
* worked out.
*
* Read the transaction accounting rules from T5645. The meaning
* of each rule is as follows:
*           01 - Contract suspense
*           02 - Single Premium
*           03 - Regular Premium
*           04 - Contract fee
*           05 - Tolerance write off
*           06 - Stamp duty
*           07 - Stamp duty payable
*           08 - Initial commission due
*           09 - Initial commission earned
*           10 - Initial commission paid in advance
*           11 - Servicing commission due
*           12 - Servicing commission earned
*           13 - Renewal commission due
*           14 - Renewal commission earned
*********** 15 - Reinsurance ??????????????
*********** 16 - Reinsurance ??????????????
*           15 - Override commission due
* Seq 01... 01 - Override commission earned
*
* Read  the  description  of  this table entry as well. This is
* used   as   a  narrative  description  on  all  the  postings
* generated.
*
* If  the amount passed in the AT transaction area is not zero,
* call LIFRTRN ("cash" posting routine) to post to the contract
* suspense  account.  The posting required is defined in "line"
* 01 on the  T5645  entry.  Set up and pass the linkage area as
* follows:
*           Function - PSTW
*           Batch key - from AT linkage
*           Document number - contract number
*           Sequence  number  -  add 1 to the previous sequence
*                number each time (originally 0)
*           Sub-account  code  and  type,  GL  map, GL sign and
*                control  total  number - from applicable T5645
*                entry
*           Company codes (sub-ledger and GL) - batch company
*           Subsidiary account - contract number
*           Original currency code - contract currency
*           Original  currency  amount  -  as  passed in the AT
*                transaction area
*           Accounting currency code - blank (it will be looked
*                up by the subroutine)
*           Accounting  currency  amount  -  zero  (it  will be
*                calculated by the subroutine)
*           Exchange  rate - zero (it will be calculated by the
*                subroutine)
*           Transaction reference - contract transaction number
*           Transaction description  -  from  transaction  code
*                description
*           Posting  month  and  year  -  blank  (they  will be
*                defaulted)
*           Effective date - contract commencement date
*           Reconciliation amount - zero
*           Reconciled date - Max-Date
*           Transaction ID - from AT linkage
*           Substitution code 1 - contract type
*
* Call LIFACMV  ("non-cash"  posting routine) six times to post
* to the premiums  (02  and 03), fees (04), tolerance write off
* (05), stamp duty (06) and stamp duty payable (07).  (Skip the
* call if the  appropriate amount is zero.) Set up and pass the
* linkage area as follows:
*           Function - PSTW
*           Batch key - from AT linkage
*           Document number - contract number
*           Sequence  number  -  add 1 to the previous sequence
*                number each time
*           Sub-account  code  and  type,  GL  map, GL sign and
*                control  total  number - from applicable T5645
*                entry
*           Company codes (sub-ledger and GL) - batch company
*           Subsidiary account - contract number
*           Original currency code - contract currency
*           Original  currency amount - 02=total single premium
*                accumulated   from   each   component   above,
*                03=total  adjusted regular premium accumulated
*                from  each  component  above,  04=contract fee
*                returned    from    the    subroutine   above,
*                05=02+03+04-01,  06  and  07=total  stamp duty
*                accumulated from each component above
*           Accounting currency code - blank (it will be looked
*                up by the subroutine)
*           Accounting  currency  amount  -  zero  (it  will be
*                calculated by the subroutine)
*           Exchange  rate - zero (it will be calculated by the
*                subroutine)
*           Transaction reference - contract transaction number
*           Transaction description  -  from  transaction  code
*                description
*           Posting  month  and  year  -  blank  (they  will be
*                defaulted)
*           Effective date - contract commencement date
*           Reconciliation amount - zero
*           Reconciled date - Max-Date
*           Transaction ID - from AT linkage
*           Substitution code 1 - contract type
*
*COMMISSION ACCOUNTING
*
* Call  LIFACMV  ("non-cash" posting routine) up to seven times
* for  each  commission  agent.  These  amounts  will have been
* accumulated  during the commission calculations done earlier.
* These  amounts  are  (just  in case you haven't worked it out
* yet):
*           08 - Initial commission due
*           09 - Initial commission earned
*           10 - Initial commission paid in advance
*           11 - Servicing commission due
*           12 - Servicing commission earned
*           13 - Renewal commission due
*           14 - Renewal commission earned
*           15 - Override commission due
* Seq 01... 01 - Override commission earned
*
* Skip the call  if  the appropriate amount is zero. Set up and
* pass the linkage area as follows:
*           Function - PSTW
*           Batch key - from AT linkage
*           Document number - contract number
*           Sequence  number  -  add 1 to the previous sequence
*                number each time
*           Sub-account  code  and  type,  GL  map, GL sign and
*                control  total  number - from applicable T5645
*                entry
*           Company codes (sub-ledger and GL) - batch company
*           Subsidiary account - commission agent number
*           Original currency code - contract currency
*           Original   currency   amount  -  applicable  amount
*                accumulated for the "line" being posted
*           Accounting currency code - blank (it will be looked
*                up by the subroutine)
*           Accounting  currency  amount  -  zero  (it  will be
*                calculated by the subroutine)
*           Exchange  rate - zero (it will be calculated by the
*                subroutine)
*           Transaction reference - contract transaction number
*           Transaction description  -  from  transaction  code
*                description
*           Posting  month  and  year  -  blank  (they  will be
*                defaulted)
*           Effective date - contract commencement date
*           Reconciliation amount - zero
*           Reconciled date - Max-Date
*           Transaction ID - from AT linkage
*           Substitution code 1 - contract type
*
*GENERAL HOUSE-KEEPING
*
* 1) Write transaction PTRN record:
*           - contract key from contract header,
*           - transaction number from contract header,
*           - transaction effective date to today,
*           - batch key information from AT linkage.
*
* 2)  update the batch header by calling BATCUP with a function
* of WRITS and the following parameters:
*           - transaction count = 1,
*           - all other amounts zero,
*           - batch key from WSSP.
*
* 3)  Call  policy  schedule  printing routine? (table driven?)
* This  will  be  added  when  the  plan  schedule  routine  is
* specified (shortly).
*
* 4) Release contract soft lock.
*
* A000-STATISTICS SECTION.
* ~~~~~~~~~~~~~~~~~~~~~~~
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* LIFSTTR
* ~~~~~~~
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written for it.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are COVRSTS and COVRSTA.
* COVRSTS allows only validflag 1 records, and COVRSTA
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various ATs
* which affect contracts/coverages. It is designed to
* track a policy through its life and report on any
* changes to it. The statistical records produced form
* the basis for the Agent Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as its item name the transaction code and
* the contract status of a contract, e.g. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'IF'.
* On T6629, there are two question fields, which state
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum assured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, eg. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, eg AB, will be moved to
* the STTR record. The same principal applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas, PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to COVRSTA records, and
* current categories refer to COVRSTS records. AGSMSTS
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the AGCMSTS, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record, there could be several current
* categories, therefore the corresponding number of STTRs
* will be written. Also, if the premium has increased or
* decreased, a number of STTRs will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTRs is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTRs
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out up to and including the TRANNOR number.
*
******************Enhancements for Life Asia 1.0****************
*
* Enhancement 1
*
* This enhancement to update the issue dates maintained on
* Contract Additional Detail file HPAD.
*
* =============
* Enhancement 2
*
* Referring to enhancement in P6378 where on issuing contracts,
* suspense available in different currencies is checked to settle
* the premium payable.  The enhancement here is to display
* suspense and tolerance limits in collection currency rather
* than always in contract currency.
*
******************Enhancements for Life Asia 2.0****************
*
*  Update the new basic and loaded premium fields when writing        *
*  the new COVR record.                                               *
*
*  Before calling the commission calculation routines, check          *
*  the basic commission indicator on T5687. If it is set,             *
*  use the basic premium, as opposed to the gross premium,            *
*  to calculate commission.                                           *
*                                                                     *
*****************************************************
* </pre>
*/

public class P5074at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5074AT");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaPrimaryChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(209);
	//private PackedDecimalData wsaaTotamnt = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 0);
	protected PackedDecimalData wsaaTotamnt = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 0);
	//private PackedDecimalData wsaaTotamnt1 = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 9);
	protected PackedDecimalData wsaaTotamnt1 = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 9);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 18);
	//private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 19);
	protected PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 19);
	//private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 23);
	//private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 27);
	//private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 31);
	protected PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 23);
	protected PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 27);
	protected FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 31);
	
	private FixedLengthStringData filler1 = new FixedLengthStringData(174).isAPartOf(wsaaTransactionRec, 35, FILLER).init(SPACES);
	private PackedDecimalData wsaaNumapp = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	//private String wsaaSingPrmInd = "N";
	protected String wsaaSingPrmInd = "N";
	protected String wsaaSuspInd = "";
	//private PackedDecimalData wsaaMgfee = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaMgfee = new PackedDecimalData(17, 2);
	//private PackedDecimalData wsaaMgfeeRegAmt = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaMgfeeRegAmt = new PackedDecimalData(17, 2);
	//private PackedDecimalData wsaaSingpFee = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaSingpFee = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaNoOfRecs = new PackedDecimalData(3, 0).setUnsigned();
	//private PackedDecimalData wsaaAgentSub = new PackedDecimalData(3, 0).setUnsigned();
	protected PackedDecimalData wsaaAgentSub = new PackedDecimalData(3, 0).setUnsigned();
	//private PackedDecimalData wsaaStampDutyAcc = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaStampDutyAcc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0).setUnsigned();
	protected PackedDecimalData wsaaSub1 = new PackedDecimalData(3, 0).setUnsigned();
	//private PackedDecimalData wsaaOldCessDate = new PackedDecimalData(8, 0);
	protected PackedDecimalData wsaaOldCessDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaLextDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaNoSummaryRec = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaAgcmCedagent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaSaveAgntnum = new FixedLengthStringData(8);
	//private PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	protected PackedDecimalData wsaaJrnseq = new PackedDecimalData(3, 0);
	//private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	protected FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
	private PackedDecimalData wsaaAmountIn = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaLetokeys = new FixedLengthStringData(35);
	private FixedLengthStringData wsaaOkeyLanguage = new FixedLengthStringData(1).isAPartOf(wsaaLetokeys, 0);
	private ZonedDecimalData wsaaOkeyTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaLetokeys, 1).setUnsigned();
	//ILIFE-1971-Starts
	private FixedLengthStringData wsaaTotTaxCalculated = new FixedLengthStringData(13).isAPartOf(wsaaLetokeys, 6);
	//private PackedDecimalData wsaaTotTax = new PackedDecimalData(13, 2);
	protected PackedDecimalData wsaaTotTax = new PackedDecimalData(13, 2);
	//ILIFE-1971-ENDS
		/* WSAA-OVERRIDE-AGENTS */
	private FixedLengthStringData[] wsaaOverrideAgent = FLSInittedArray (10, 340);
	private FixedLengthStringData[][] wsaaOverrideAgentsComm = FLSDArrayPartOfArrayStructure(10, 34, wsaaOverrideAgent, 0);
	private FixedLengthStringData[][] wsaaOverrideAgntnum = FLSDArrayPartOfArrayStructure(8, wsaaOverrideAgentsComm, 0);
	private PackedDecimalData[][] wsaaOverrideComm = PDArrayPartOfArrayStructure(17, 2, wsaaOverrideAgentsComm, 8);
	private FixedLengthStringData[][] wsaaCedagent = FLSDArrayPartOfArrayStructure(8, wsaaOverrideAgentsComm, 17);
	private PackedDecimalData[][] wsaaOvrdCommPaid = PDArrayPartOfArrayStructure(17, 2, wsaaOverrideAgentsComm, 25);

	private FixedLengthStringData wsaaCompkey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCompkeyTranno = new FixedLengthStringData(4).isAPartOf(wsaaCompkey, 0);
	private FixedLengthStringData wsaaCompkeyCrtable = new FixedLengthStringData(4).isAPartOf(wsaaCompkey, 4);
	private ZonedDecimalData wsaaC = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaL = new ZonedDecimalData(2, 0).setUnsigned();

		/* WSAA-COVERAGE-PREMIUMS */
	private FixedLengthStringData[] wsaaLifeLevel = FLSInittedArray (10, 3366);
	private FixedLengthStringData[][] wsaaCoverageLevel = FLSDArrayPartOfArrayStructure(99, 34, wsaaLifeLevel, 0);
	private ZonedDecimalData[][] wsaaCovtInstprem = ZDArrayPartOfArrayStructure(17, 2, wsaaCoverageLevel, 0);
	private ZonedDecimalData[][] wsaaCovtSingp = ZDArrayPartOfArrayStructure(17, 2, wsaaCoverageLevel, 17);
	private ZonedDecimalData wsaaCoverageNum = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaLifeNum = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaClnkAnnprem = new PackedDecimalData(18, 4).setUnsigned();
	private FixedLengthStringData wsaaPremiumFlag = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaStoredOvcpc = new PackedDecimalData(5, 2);
	private FixedLengthStringData wsaaStoredAgentClass = new FixedLengthStringData(4);

		/* WSAA-T5645 */
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (45, 21);
	//private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	protected ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	//private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	protected FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	//private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	protected FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	//private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	protected FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	//private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
	protected FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
	private PackedDecimalData wsaaCntSuspense = new PackedDecimalData(17, 2);
	//private PackedDecimalData wsaaInstpremTot = new PackedDecimalData(13, 2);
	protected PackedDecimalData wsaaInstpremTot = new PackedDecimalData(13, 2);
	//private PackedDecimalData wsaaInstpremTotXge = new PackedDecimalData(13, 2);
	protected PackedDecimalData wsaaInstpremTotXge = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPayrSuspense = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotSingp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotRegpAdj = new PackedDecimalData(17, 2);
	//private PackedDecimalData wsaaTotTolerance = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTotTolerance = new PackedDecimalData(17, 2);
	//private ZonedDecimalData wsaaFeeFreq = new ZonedDecimalData(11, 5);
	protected ZonedDecimalData wsaaFeeFreq = new ZonedDecimalData(11, 5);
	protected PackedDecimalData wsaaAmntDue = new PackedDecimalData(17, 2);
	protected ZonedDecimalData wsaaToleranceApp = new ZonedDecimalData(4, 2);
	protected ZonedDecimalData wsaaToleranceApp2 = new ZonedDecimalData(4, 2);
	//private PackedDecimalData wsaaTolerance = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTolerance = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaTolerance2 = new PackedDecimalData(17, 2);
	protected ZonedDecimalData wsaaAmountLimit = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaAmountLimit2 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).setUnsigned();
	//private ZonedDecimalData wsaaFeeAdj = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaFeeAdj = new ZonedDecimalData(17, 2);
	private FixedLengthStringData wsaaMainCoverage = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBasicCommMeth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBascpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSrvcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRnwcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBasscmth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBasscpy = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaTr695Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr695Coverage = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 0);
	private FixedLengthStringData wsaaTr695Rider = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 4);

	protected FixedLengthStringData wsaaT5667Key = new FixedLengthStringData(8);
	protected FixedLengthStringData wsaaT5667Trancd = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 0);
	protected FixedLengthStringData wsaaT5667Curr = new FixedLengthStringData(4).isAPartOf(wsaaT5667Key, 4);

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT7508Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Cnttype = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 4);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaPayrkey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrkey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrkey, 8);
		/* WSAA-COMPONENT-TOTALS */
	//private PackedDecimalData wsaaCompSingPrem = new PackedDecimalData(17, 2);
	protected PackedDecimalData wsaaCompSingPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompRegPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompErnIcomm = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompAdvIcomm = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompErnScomm = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompErnRcomm = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompErnOcomm = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompAdvOcomm = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompTaxRelief = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);
		/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator compLevelAcc = new Validator(wsaaAcctLevel, "Y");

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");
	private ZonedDecimalData wsaaFlexPremFq = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaMinTrmToCess = new ZonedDecimalData(3, 0);
	private FixedLengthStringData wsaaSpecTermsExist = new FixedLengthStringData(1);
	//private ZonedDecimalData wsbbSub = new ZonedDecimalData(2, 0).setUnsigned();
	protected ZonedDecimalData wsbbSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaEarliestRerateDate = new PackedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaCpiValid = new FixedLengthStringData(1);
	private Validator cpiValid = new Validator(wsaaCpiValid, "Y");

	private FixedLengthStringData wsaaWopMatch = new FixedLengthStringData(1);
	private Validator wopMatch = new Validator(wsaaWopMatch, "Y");
	private Validator wopNotMatch = new Validator(wsaaWopMatch, "N");

	private FixedLengthStringData wsaaCrtableMatch = new FixedLengthStringData(1).init("N");
	private Validator crtableMatch = new Validator(wsaaCrtableMatch, "Y");
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
		/*01  WSAA-LETOKEYS.                                       <PCPPRT>*/
	private PackedDecimalData wsaaComtotKept = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCompayKept = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaComernKept = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrtimes = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaMinOverdue = new PackedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaT5729Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCompayKept2 = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPrmdepst = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaL1Clntnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaL2Clntnum = new FixedLengthStringData(8).init(SPACES);
	private String wsaaWaiveCont = "";

	private FixedLengthStringData wsaaZrwvflgs = new FixedLengthStringData(3);
	private FixedLengthStringData[] wsaaZrwvflg = FLSArrayPartOfStructure(3, 1, wsaaZrwvflgs, 0);
	private FixedLengthStringData wsaaTr517Rec = new FixedLengthStringData(250);
	private ZonedDecimalData wsaaDobPlusTr627 = new ZonedDecimalData(8, 0).setUnsigned();
	private ZonedDecimalData wsaaZsufcdte = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaLifcnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaBillingDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaBillingDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillDateMnth = new ZonedDecimalData(2, 0).isAPartOf(filler4, 4).setUnsigned();
	private ZonedDecimalData wsaaBillDateDay = new ZonedDecimalData(2, 0).isAPartOf(filler4, 6).setUnsigned();
	private ZonedDecimalData wsaaAnnivExtractDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaAnnivExtractDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaAnnivExtractMnth = new ZonedDecimalData(2, 0).isAPartOf(filler5, 4).setUnsigned();
	private ZonedDecimalData wsaaAnnivExtractDay = new ZonedDecimalData(2, 0).isAPartOf(filler5, 6).setUnsigned();
	private static final int wsaaEarliestRcesdte = 0;
	private static final int wsaaEarliestCrrcd = 0;
	private PackedDecimalData wsaaRegPremFirst = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegPremRenewal = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZctnAnnprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaZctnInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaRegIntmDate = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaBillfq9 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaExcessInstNum = new ZonedDecimalData(5, 0).setUnsigned();
	//private ZonedDecimalData wsaaPremTax01 = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaPremTax01 = new ZonedDecimalData(17, 2);
	//private ZonedDecimalData wsaaPremTax02 = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaPremTax02 = new ZonedDecimalData(17, 2);
	//private ZonedDecimalData wsaaTaxBaseAmt = new ZonedDecimalData(17, 2);
	protected ZonedDecimalData wsaaTaxBaseAmt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOccdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(wsaaOccdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaOccMm = new ZonedDecimalData(2, 0).isAPartOf(filler7, 4).setUnsigned();
	private ZonedDecimalData wsaaOccDd = new ZonedDecimalData(2, 0).isAPartOf(filler7, 6).setUnsigned();

	private FixedLengthStringData wsaaFound = new FixedLengthStringData(1);
	private Validator found = new Validator(wsaaFound, "Y");
	private Validator notFound = new Validator(wsaaFound, "N");
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private static final String f294 = "F294";
	private static final String e308 = "E308";
	private static final String i035 = "I035";
	private static final String h036 = "H036";
	private static final String g437 = "G437";
	private static final String tr29s = "TR29S"; //IBPLIFE-1357
	//private AcblenqTableDAM acblenqIO = new AcblenqTableDAM();
	private AgcmTableDAM agcmIO = new AgcmTableDAM();
		/*Joind FSU and Life agent headers - new b*/
	private AgpyagtTableDAM agpyagtIO = new AgpyagtTableDAM();
	private AgpydocTableDAM agpydocIO = new AgpydocTableDAM();
	//private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client Roles by Foreign Key*/
		/*Client role and relationships logical fi*/
	//private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	protected CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
		/*COVR layout for trad. reversionary bonus*/
		/*Coverage/rider transactions - new busine*/
	//private DescTableDAM descIO = new DescTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
		/*Flexible Premiums Coverage Logical*/
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
		/*Flexible Premium Logical File*/
	private FprmTableDAM fprmIO = new FprmTableDAM();
		/*Contract Additional Details*/
		/*Temporary Increase Trigger File*/
	private InctTableDAM inctIO = new InctTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	//private ItemTableDAM itemIO = new ItemTableDAM();
	protected ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	//private PayrTableDAM payrIO = new PayrTableDAM();
	//private PcddlnbTableDAM pcddlnbIO = new PcddlnbTableDAM();
	protected PcddlnbTableDAM pcddlnbIO = new PcddlnbTableDAM();
	//private TaxdTableDAM taxdIO = new TaxdTableDAM();

	private ZrapTableDAM zrapIO = new ZrapTableDAM();
	//private Batckey wsaaBatckey = new Batckey();
	protected Batckey wsaaBatckey = new Batckey();
	private Crtundwrec crtundwrec = new Crtundwrec();
	//private Varcom varcom = new Varcom();
	protected Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	//private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	protected Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	//private Lifacmvrec lifacmvrec = new Lifacmvrec();
	protected Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Prasrec prasrec = new Prasrec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Actvresrec actvresrec = new Actvresrec();
	//private Syserrrec syserrrec = new Syserrrec();
	protected Syserrrec syserrrec = new Syserrrec();
	//private T5679rec t5679rec = new T5679rec();
	protected T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T5534rec t5534rec = new T5534rec();
	private T5676rec t5676rec = new T5676rec();
	private T5671rec t5671rec = new T5671rec();
	private T5647rec t5647rec = new T5647rec();
	private T5644rec t5644rec = new T5644rec();
	//private T5688rec t5688rec = new T5688rec();
	protected T5688rec t5688rec = new T5688rec();
	private Tr517rec tr517rec = new Tr517rec();
	//private T5674rec t5674rec = new T5674rec();
	protected T5674rec t5674rec = new T5674rec();
	//private T5645rec t5645rec = new T5645rec();
	protected T5645rec t5645rec = new T5645rec();
	//private T6687rec t6687rec = new T6687rec();
	protected T6687rec t6687rec = new T6687rec();
	protected T5667rec t5667rec = new T5667rec();
	private T3695rec t3695rec = new T3695rec();
	private T6658rec t6658rec = new T6658rec();
	private Tr384rec tr384rec = new Tr384rec();
	private T5729rec t5729rec = new T5729rec();
	private Tr695rec tr695rec = new Tr695rec();
	private T7508rec t7508rec = new T7508rec();
	//private Th605rec th605rec = new Th605rec();
	protected Th605rec th605rec = new Th605rec();
	private Tr627rec tr627rec = new Tr627rec();
	//private Tr52drec tr52drec = new Tr52drec();
	protected Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Tr52qrec tr52qrec = new Tr52qrec();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Ubblallpar ubblallpar = new Ubblallpar();
	private Stdtallrec stdtallrec = new Stdtallrec();
	private Isuallrec isuallrec = new Isuallrec();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Batcuprec batcuprec = new Batcuprec();
	//private Mgfeelrec mgfeelrec = new Mgfeelrec();
	protected Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Rlliadbrec rlliadbrec = new Rlliadbrec();
	//private Rlpdlonrec rlpdlonrec = new Rlpdlonrec();
	//private Zorlnkrec zorlnkrec = new Zorlnkrec();
	protected Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	//private Zrdecplrec zrdecplrec = new Zrdecplrec();
	//private Atmodrec atmodrec = new Atmodrec();
	protected Atmodrec atmodrec = new Atmodrec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	//private FormatsInner formatsInner = new FormatsInner();
	protected FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaAgentsInner wsaaAgentsInner = new WsaaAgentsInner();
	//private WsaaBillingInformationInner wsaaBillingInformationInner = new WsaaBillingInformationInner();
	protected WsaaBillingInformationInner wsaaBillingInformationInner = new WsaaBillingInformationInner();
	private ExternalisedRules er = new ExternalisedRules();
	private ZswrpfDAO  zswrDAO =  getApplicationContext().getBean("ZswrpfDAO", ZswrpfDAO.class);
	private boolean stampDutyflag = false;
	private ExclpfDAO exclpfDAO = getApplicationContext().getBean("exclpfDAO",ExclpfDAO.class); 
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();//NLG
	private MgmFeeRec mgmFeeRec = new MgmFeeRec();//ALS-4706
	private static final String  MGMFEE_ID="CSULK003"; //ALS-4706
	private boolean mgmFeeFlag=false;
	private PackedDecimalData wsaaCompErnInitCommGst = new PackedDecimalData(17, 2);	
	private PackedDecimalData wsaaCompErnRnwlCommGst = new PackedDecimalData(17, 2);	
	private boolean gstOnCommFlag = false;
	private ZonedDecimalData wsaaCounter = new ZonedDecimalData(2, 0).setUnsigned();
	private boolean riskPremflag = false;
	private static final String  RISKPREM_FEATURE_ID="NBPRP094";
	private List<Rskppf> insertRiskPremList = new ArrayList<>();
	private RskppfDAO rskppfDAO =  getApplicationContext().getBean("rskppfDAO", RskppfDAO.class);
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
	private Premiumrec premiumrec = new Premiumrec();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	List<Itempf> itempf;
	private Tr59xrec tr59xrec = new Tr59xrec();
	private Map<String, List<Itempf>> t5567Map;
	private ItemDAO itemDao = getApplicationContext().getBean("itemDao",ItemDAO.class);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaItemitem, 3);
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	//private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private T5567rec t5567rec = new T5567rec();
	private ZonedDecimalData wsaapolFee = new ZonedDecimalData(8, 2);
	Chdrpf chdrpf = null;
	//ILIFE-8919 Starts
	private static final String SYS_COY = "0";
	//ILIFE-8919 End
	private boolean isDMSFlgOn = false;	//ILIFE-8880
	private static final String DMS_FEATURE = "SAFCF001";	//ILIFE-8880
	//ILIFE-8709 start
	private List<? extends Undlpf> undlpfList = new ArrayList<>();
	private UndlpfDAO undlpfDAO = getApplicationContext().getBean("undlpfDAO", UndlpfDAO.class);
	private UndcpfDAO undcpfDAO = getApplicationContext().getBean("undcpfDAO", UndcpfDAO.class);
	private UndqpfDAO undqpfDAO = getApplicationContext().getBean("undqpfDAO", UndqpfDAO.class);
	private List<Covrpf> covrpfList = new ArrayList<Covrpf>();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	protected Covrpf covrtrb = new Covrpf();
	private Ptrnpf ptrnpf = new Ptrnpf();
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Batckey wsaaBatcKey = new Batckey();
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private Taxdpf taxdpf = new Taxdpf();
	private TaxdpfDAO taxdpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private List<Taxdpf> taxdInsertList = new ArrayList<Taxdpf>();
	private static final Logger LOGGER = LoggerFactory.getLogger(P5074at.class);
	private ZctnpfDAO zctnpfDAO =  getApplicationContext().getBean("zctnpfDAO", ZctnpfDAO.class);
	private Zctnpf zctnpf = new Zctnpf(); 
	private List<Zctnpf> insertZctnpfList = new ArrayList<>();
	protected Payrpf payrpf=new Payrpf();
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private List<Payrpf> payrpfList = new ArrayList<>();
	private List<Payrpf> updatePayrpfList =  new ArrayList<>();
	private RlpdlonUtils rlpdlonUtils = getApplicationContext().getBean("rlpdlonUtils", RlpdlonUtils.class);
	private RlpdlonPojo rlpdlonPojo = new RlpdlonPojo();
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
	private Hpadpf hpadpf = new Hpadpf();
	private Clntpf clts = new Clntpf();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private Clrfpf clrfpf = new Clrfpf();
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private Lifepf lifepf = new Lifepf();
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private List<Lifepf> lifepfList = new ArrayList<>();
	private List<Lifepf> updateLifepflist =  new ArrayList<>();
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO",AglfpfDAO.class);
	private Aglfpf aglfpf  = new Aglfpf();
	private RtrnpfDAO rtrnsacDao = getApplicationContext().getBean("rtrnpfDAO",RtrnpfDAO.class);
	private Rtrnpf rtrnsac = new Rtrnpf();
	private Acblpf acblpf = new Acblpf();
	private AcblpfDAO acblDao = getApplicationContext().getBean("acblpfDAO",AcblpfDAO.class);
	private Zptnpf zptnIO = new Zptnpf();
	private List<Zptnpf> insertZptnpfList = new ArrayList<Zptnpf>();
	private ZptnpfDAO zptnpfDAO = getApplicationContext().getBean("zptnpfDAO", ZptnpfDAO.class);
	private SftlockRecBean sftlockRecBean = new SftlockRecBean();
	private SftlockUtil sftlockUtil = new SftlockUtil();
	private List<Covtpf> covtpfList;
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private Covtpf covtlnb = new Covtpf();
	private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
	private Racdpf racdpf = new Racdpf();
	private List<Racdpf> racdpfList;
	int racdpUpdate = 0;
	private boolean lincFlag = false;
	 //ILIFE-8709 end
	//IBPLIFE-1357 starts
	private BaseDAO dao = getApplicationContext().getBean("baseDAO", BaseDAOImpl.class);
	private ClexpfDAO clexpfDAO = getApplicationContext().getBean("clexpfDAO", ClexpfDAO.class);
	private Clexpf clexpf = null;
	private FixedLengthStringData wsaaTo = new FixedLengthStringData(50);
    private FixedLengthStringData phoneNumberTo = new FixedLengthStringData(16);
	private Sendmlrec sendmlrec = new Sendmlrec();
	private Properties properties = new Properties();
	private static final String TEMPLATE_PROPERTIES = "templateEmailSMS_issue.properties";
	private FixedLengthStringData wsaaAlertv1 = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaAlertv2 = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaAlertv3 = new FixedLengthStringData(60);
	private Tr29srec tr29srec = new Tr29srec();
	//IBPLIFE-1357 ends
	private static final String  FMC_FEATURE_ID="BTPRO026";	//IBPLIFE-1379
	private boolean fmcOnFlag=false;	//IBPLIFE-1379
	private ZfmcpfDAO  zfmcpfDAO =  getApplicationContext().getBean("zfmcpfDAO", ZfmcpfDAO.class); //IBPLIFE-1376
	private Zfmcpf zfmcpf=new Zfmcpf(); //IBPLIFE-1376
	private UnltpfDAO unltpfDAO = getApplicationContext().getBean("unltpfDAO", UnltpfDAO.class); //IBPLIFE-1376
	private CrtundwrtUtil crtundwrtUtil = new CrtundwrtUtilImpl(); //IBPLIFE-680
	private boolean unitLinked = false; 
	private boolean onePflag = false; 
	private static final String  ONE_P_CASHLESS="NBPRP124";
	private T3615rec t3615rec = new T3615rec();
	private boolean stampDutyFlagcomp;
	protected PackedDecimalData wsaaStampDutyComp = new PackedDecimalData(17, 2);
	private Exclpf exclpf=null;
/**
 * Contains all possible labels used by goTo action.
 */
	//private enum GotoLabel implements GOTOInterface {
protected enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1590,
		skipDespatch2111,
		exit2119,
		exit232c,
		readNextLext2323,
		setBenBillDate2323,
		oxit232d,
		skipBonusWorkbench2333,
		premTax233d,
		exit233a,
		exit233c,
		n5b233d,
		n5c233d,
		n5cSkip233d,
		n5e233d,
		n5eSkip233d,
		n5f233d,
		singlePremium233d,
		n6c233d,
		n6cSkip233d,
		readNext2332,
		exit233d,
		aglf233e,
		writeAgcm233e,
		checkNext233e,
		exit233e,
		exit233g,
		singpFee2352,
		adjustPremium2352,
		exit235a,
		nonCashPostings2555,
		incrementSub2559,
		call2b00,
		next2b00,
		exit2b00,
		a230Exit,
		a480Next,
		a490Exit
	}

	public P5074at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		/*MAINLINE*/
		initialise1000();
		process2000();
		a000Statistics();
		a500AddUnderwritting();
		//AFR
		d100Zswr();
		//AFR		
		dryProcessing8000();
		rlseSoftlock2840();
		/*EXIT*/
		msgSending(); //IBPLIFE-1357 
		mainCustomerSpecific();
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void mainCustomerSpecific(){
	
}
protected void xxxxFatalError()
	{
					xxxxFatalErrors();
					xxx1ErrorProg();
				}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxx1ErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		//ILIFE-1971-Starts
		wsaaTotTax.set(ZERO);
		//ILIFE-1971-Ends
		wsaaSingPrmInd = "N";
		wsaaMgfee.set(ZERO);
		wsaaMgfeeRegAmt.set(ZERO);
		wsaaSingpFee.set(ZERO);
		wsaaComtotKept.set(ZERO);
		wsaaCompayKept.set(ZERO);
		wsaaComernKept.set(ZERO);
		wsaaOvrtimes.set(ZERO);
		wsaaCompayKept2.set(ZERO);
		comlinkrec.language.set(atmodrec.language);
		wsaaDobPlusTr627.set(varcom.vrcmMaxDate);
		wsaaZsufcdte.set(varcom.vrcmMaxDate);
		wsaaLifcnum.set(SPACES);
		wsaaAmountIn.set(ZERO);
		wsaaCompSingPrem.set(ZERO);
		wsaaCompRegPrem.set(ZERO);
		readContractHeader1100();
		readTableT57291150();
		readTableTh6051180();
		/* Read table TR52D using CHDRLNB-REGISTER as key                  */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItemcoy(atmodrec.company);
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setItempfx("IT");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
		}
		isDMSFlgOn = FeaConfg.isFeatureExist(SYS_COY, DMS_FEATURE, appVars, "IT"); //ILIFE-8880
		gstOnCommFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "CTISS007", appVars, "IT"); //ILIFE-8392
		fmcOnFlag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), FMC_FEATURE_ID, appVars, "IT"); //IBPLIFE-1379
		onePflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString(), ONE_P_CASHLESS, appVars, "IT");
		stampDutyFlagcomp = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString(), "CTISS013", appVars, "IT");
		if(onePflag) {
			readOnePCashless();
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
		callDatcon11200();
		readStatusCodes1300();
		calcSuspense1400();
		clearFields1500();
 //ILIFE-8709 start
	//	initialize(rlpdlonPojo);
		rlpdlonPojo.setFunction(varcom.info.toString());
		rlpdlonPojo.setPrmdepst(BigDecimal.ZERO);
		wsaaPrmdepst.set(ZERO);
		r200UpdateApa();
		wsaaPrmdepst.set(rlpdlonPojo.getPrmdepst());
		if (isGT(rlpdlonPojo.getPrmdepst(),ZERO)) {
			wsaaSuspInd = "Y";
 //ILIFE-8709 end
		}
		/*    Store the contract agent number, to be used later, if        */
		/*    the agent cedes commision to other agents.                   */
		
		readFeechrge1600();
		wsaaAgcmCedagent.set(chdrlnbIO.getAgntnum());
 //ILIFE-8709 start
		payrpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		payrpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		payrpf.setPayrseqno(1);
		payrpf.setValidflag("1");	
		payrpfList = payrpfDAO.readPayrData(payrpf);
		if (payrpfList == null || payrpfList.isEmpty()) {
			syserrrec.params.set(chdrlnbIO.getChdrnum());
			xxxxFatalError();
			return;
		}
		for(Payrpf payrpf : payrpfList){

				loadPayerDetails1700(payrpf);
		}
		
 //ILIFE-8709 end
		lincFlag = FeaConfg.isFeatureExist(atmodrec.company.toString(), "NBPRP116", appVars, "IT");
	}

	/**
	* <pre>
	* Read the contract head.                                     *
	* </pre>
	*/
protected void readFeechrge1600(){
	
}
protected void readContractHeader1100()
	{
		readContractHeader1110();
	}

protected void readContractHeader1110()
	{
		wsaaPrimaryKey.set(atmodrec.primaryKey);
		chdrlnbIO.setDataArea(SPACES);
		chdrlnbIO.setChdrcoy(atmodrec.company);
		chdrlnbIO.setChdrnum(wsaaPrimaryChdrnum);
		chdrlnbIO.setFunction(varcom.reads);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			xxxxFatalError();
		}
		/* Read HPAD file for Proposal Details                             */
 //ILIFE-8709 start		
		hpadpf=hpadpfDAO.getHpadData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		if(hpadpf==null) {
			syserrrec.params.set(chdrlnbIO.getChdrcoy().concat(chdrlnbIO.getChdrnum()));
			xxxxFatalError();
		}
 //ILIFE-8709 end
	}

protected void readTableT57291150()
	{
			start1151();
		}

protected void start1151()
	{
		notFlexiblePremiumContract.setTrue();
		/*  Read T5729.                                            <D9604>*/
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5729);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
//		itdmIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrlnbIO.getChdrcoy(), SPACES);
			stringVariable1.addExpression(tablesInner.t5729, SPACES);
			stringVariable1.addExpression(chdrlnbIO.getCnttype(), SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5729)
		|| isNE(itdmIO.getItemitem(),chdrlnbIO.getCnttype())
		|| isGT(itdmIO.getItmfrm(),chdrlnbIO.getOccdate())
		|| isLT(itdmIO.getItmto(),chdrlnbIO.getOccdate())) {
			return ;
		}
		t5729rec.t5729Rec.set(itdmIO.getGenarea());
		flexiblePremiumContract.setTrue();
	}

protected void readTableTh6051180()
	{
		start1181();
	}

protected void start1181()
	{
		/*  Read TH605 to see if bonus workbench extraction used.          */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(atmodrec.company);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		/*    IF ITEM-STATUZ           NOT = O-K AND MRNF          <V70L01>*/
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

	protected void readOnePCashless() {
		Itempf itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(chdrlnbIO.getChdrcoy().toString());
		itempf.setItemtabl(tablesInner.t3615.toString());
		itempf.setItemitem(chdrlnbIO.getSrcebus().toString());
		itempf.setValidflag("1");
		itempf = itemDAO.getItemRecordByItemkey(itempf);			
		if (itempf != null) {
			t3615rec.t3615Rec.set(StringUtil.rawToString(itempf.getGenarea()));			
		}
	}

	/**
	* <pre>
	* Call DATCON1 subroutine.                                    *
	* </pre>
	*/
protected void callDatcon11200()
	{
		/*CALL-DATCON1*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	* Read the status code table.                                 *
	* </pre>
	*/
protected void readStatusCodes1300()
	{
		readStatusCodes1310();
	}

protected void readStatusCodes1310()
	{
		wsaaBatckey.set(atmodrec.batchKey);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	* Clear all the accumulators.                                 *
	* </pre>
	*/
protected void calcSuspense1400()
	{
		calcSuspense1410();
	}

protected void calcSuspense1410()
	{
		/*  Read financial accounting rules*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("P5074");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*    Read the table T3695 for the field sign of the 01 posting.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t3695);
		itemIO.setItemitem(t5645rec.sacstype01);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		/*    Read  the sub account balance for  the contract and apply*/
		/*    the sign for the sub account type.*/
		/*    Sub Account search enhanced to look for Suspense payment     */
		/*    in Contract Currency, Billing Currency or any currency.      */
		/* MOVE SPACES                 TO ACBL-DATA-AREA.               */
		/* MOVE ZEROS                  TO ACBL-RLDGACCT.                */
		/* MOVE ATMD-COMPANY           TO ACBL-RLDGCOY.                 */
		/* MOVE CHDRLNB-CHDRNUM        TO ACBL-RLDGACCT.                */
		/* MOVE CHDRLNB-CNTCURR        TO ACBL-ORIGCURR.                */
		/* MOVE T5645-SACSCODE-01      TO ACBL-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-01      TO ACBL-SACSTYP.                 */
		/* MOVE READR                  TO ACBL-FUNCTION.                */
		/* CALL 'ACBLIO'               USING ACBL-PARAMS.               */
		/* IF  (ACBL-STATUZ         NOT = O-K)                          */
		/* AND (ACBL-STATUZ         NOT = MRNF)                         */
		/*    MOVE ACBL-PARAMS      TO SYSR-PARAMS                      */
		/*    PERFORM XXXX-FATAL-ERROR.                                 */
		/* IF ACBL-STATUZ               = MRNF                          */
		/*    MOVE ZERO                 TO WSAA-CNT-SUSPENSE            */
		/* ELSE                                                         */
		/*    IF T3695-SIGN             = '-'                           */
		/*       MULTIPLY ACBL-SACSCURBAL BY -1                         */
		/*                              GIVING WSAA-CNT-SUSPENSE        */
		/*    ELSE                                                      */
		/*        MOVE ACBL-SACSCURBAL  TO WSAA-CNT-SUSPENSE.           */
		wsaaCntSuspense.set(ZERO);
		wsaaSuspInd = "N";
 //ILIFE-8709 start
		acblpf.setRldgacct(chdrlnbIO.getChdrnum().toString());
		acblpf.setRldgcoy(atmodrec.company.toString());
		acblpf.setSacscode(t5645rec.sacscode01.toString());
		acblpf.setSacstyp(t5645rec.sacstype01.toString());
		for (wsaaSub.set(1); !(isGT(wsaaSub,3)
				|| isEQ(wsaaSuspInd,"Y")); wsaaSub.add(1)){
					checkSuspense1400a();
				}
 //ILIFE-8709 end
		
		
	}

protected void checkSuspense1400a()
	{
		locateSuspense1410a();
	}

protected void locateSuspense1410a()
	{
		/*    Read the Suspense file to see if any money has been          */
		/*    received for this contract. The search order for Suspense    */
		/*    details is Contract Currency; Billing Currency; Any          */
		/*    Currency. If Suspense is found set appropriate values.       */
 //ILIFE-8709 start	
		if (isEQ(wsaaSub,1)) {
			acblpf.setOrigcurr(chdrlnbIO.getCntcurr().toString());
		}
		else {
			if (isEQ(wsaaSub,2)) {
				acblpf.setOrigcurr(chdrlnbIO.getBillcurr().toString());
			}
			else {
				acblpf.setOrigcurr(SPACES.stringValue());
			}
		}
		List<Acblpf> acbl;
		if(onePflag) {
			 acbl = acblDao.searchAcblenqRecord(chdrlnbIO.chdrcoy.toString(), chdrlnbIO.getChdrnum().toString());
			 if(!(acbl.isEmpty())){
				acblpf=acbl.get(0);
				if(acblpf.getSacscurbal().floatValue()!=0) {
					wsaaSuspInd = "Y";
					if (isEQ(t3695rec.sign,"-")) {
						compute(wsaaCntSuspense, 2).set(mult(acblpf.getSacscurbal(),-1));
					}
					else {
						wsaaCntSuspense.set(acblpf.getSacscurbal());
					}
				}
			}
		}
		else {
			acbl = acblDao.selectAcblData(acblpf);
			if(!(acbl.isEmpty())){
				acblpf=acbl.get(0);
				if(acblpf.getSacscurbal().floatValue()!=0) {
					wsaaSuspInd = "Y";
					if (isEQ(t3695rec.sign,"-")) {
						compute(wsaaCntSuspense, 2).set(mult(acblpf.getSacscurbal(),-1));
					}
					else {
						wsaaCntSuspense.set(acblpf.getSacscurbal());
					}
				}
			}
		}
 //ILIFE-8709 end
	}

	/**
	* <pre>
	* Clear all the accumulators.                                 *
	* </pre>
	*/
protected void clearFields1500()
	{
		try {
			clearFields1510();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void clearFields1510()
	{
		wsaaStampDutyAcc.set(0);
		wsaaStampDutyComp.set(0);
		wsaaTotSingp.set(0);
		wsaaTotRegpAdj.set(0);
		wsaaTotTolerance.set(0);
		wsbbSub.set(1);
		prasrec.taxrelamt.set(0);
		wsaaInstpremTot.set(ZERO);
		wsaaInstpremTotXge.set(ZERO);
		while ( !(isGT(wsbbSub,9))) {
			wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBillcd[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRegPremAcc[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaSingPremAcc[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaFeTax[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaTaxrelamt[wsbbSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(SPACES);
			wsbbSub.add(1);
		}

		wsaaNoSummaryRec.set(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
			clearCommTable1520();
		}
		wsaaSub.set(0);
		wsaaTransactionRec.set(atmodrec.transArea);
		goTo(GotoLabel.exit1590);
	}

protected void clearCommTable1520()
	{
		wsaaAgentsInner.wsaaCommDue[wsaaSub.toInt()].set(0);
		wsaaAgentsInner.wsaaCommEarn[wsaaSub.toInt()].set(0);
		wsaaAgentsInner.wsaaCommPaid[wsaaSub.toInt()].set(0);
		wsaaAgentsInner.wsaaServDue[wsaaSub.toInt()].set(0);
		wsaaAgentsInner.wsaaServEarn[wsaaSub.toInt()].set(0);
		wsaaAgentsInner.wsaaRenlDue[wsaaSub.toInt()].set(0);
		wsaaAgentsInner.wsaaAnnprem[wsaaSub.toInt()].set(0);
		wsaaAgentsInner.wsaaSingp[wsaaSub.toInt()].set(0);
		wsaaAgentsInner.wsaaRenlEarn[wsaaSub.toInt()].set(0);
		wsaaAgentsInner.wsaaTcomDue[wsaaSub.toInt()].set(0);
		for (wsaaAgentSub.set(1); !(isGT(wsaaAgentSub,10)); wsaaAgentSub.add(1)){
			clearOverride1550();
		}
	}

protected void clearOverride1550()
	{
		wsaaOverrideAgntnum[wsaaSub.toInt()][wsaaAgentSub.toInt()].set(SPACES);
		wsaaOverrideComm[wsaaSub.toInt()][wsaaAgentSub.toInt()].set(ZERO);
		wsaaOvrdCommPaid[wsaaSub.toInt()][wsaaAgentSub.toInt()].set(ZERO);
	}

	/**
	* <pre>
	*1600-CHECK-FREQ-DATES SECTION.
	*    Code for checking half-yearly dates to allocate         <067>
	*    frequency factors is no longer needed, as DATCON3       <067>
	*    now correctly calculates these.                         <067>
	*                                                            <067>
	*    we are only interested in monthly or half yearly
	*    payment frequencies
	**** IF CHDRLNB-BILLFREQ  = '12'
	*****IF WSAA-BILLFREQ(WSBB-SUB)  = '12'
	*****    GO TO 1630-CHECK-MONTHLY-DATES.
	**** IF CHDRLNB-BILLFREQ NOT = '02'
	*****IF WSAA-BILLFREQ(WSBB-SUB) NOT = '02'
	*    IF WSAA-BILLFREQ(WSBB-SUB) NOT = '12'                   <067>
	*        GO TO 1690-EXIT.
	*****IF ((WSAA-PREM-MTH1    = 02) AND
	*****    (WSAA-PREM-DAY1    > 27) AND
	*****    (WSAA-PREM-DAY1    < 30)) AND
	*****   ((WSAA-PREM-DAY2    = 31) AND
	*****    (WSAA-PREM-MTH2    = 08))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****IF ((WSAA-PREM-MTH1    = 03) AND
	*****    (WSAA-PREM-DAY1    = 30)) AND
	*****   ((WSAA-PREM-DAY2    = 30) AND
	*****    (WSAA-PREM-MTH2    = 09))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****IF ((WSAA-PREM-MTH1    = 04) AND
	*****    (WSAA-PREM-DAY1    = 30)) AND
	*****   ((WSAA-PREM-DAY2    = 31) AND
	*****    (WSAA-PREM-MTH2    = 10))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****IF ((WSAA-PREM-MTH1    = 05) AND
	*****    (WSAA-PREM-DAY1    = 30)) AND
	*****   ((WSAA-PREM-DAY2    = 30) AND
	*****    (WSAA-PREM-MTH2    = 11))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****IF ((WSAA-PREM-MTH1    = 06) AND
	*****    (WSAA-PREM-DAY1    = 30)) AND
	*****   ((WSAA-PREM-DAY2    = 31) AND
	*****    (WSAA-PREM-MTH2    = 12))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****IF ((WSAA-PREM-MTH1    = 08) AND
	*****    (WSAA-PREM-DAY1    = 30)) AND
	*****   ((WSAA-PREM-DAY2    = 28) AND
	*****    (WSAA-PREM-MTH2    = 02))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****IF ((WSAA-PREM-MTH1    = 09) AND
	*****    (WSAA-PREM-DAY1    = 30)) AND
	*****   ((WSAA-PREM-DAY2    = 31) AND
	*****    (WSAA-PREM-MTH2    = 03))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****IF ((WSAA-PREM-MTH1    = 10) AND
	*****    (WSAA-PREM-DAY1    = 30)) AND
	*****   ((WSAA-PREM-DAY2    = 30) AND
	*****    (WSAA-PREM-MTH2    = 04))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****IF ((WSAA-PREM-MTH1    = 11) AND
	*****    (WSAA-PREM-DAY1    = 30)) AND
	*****   ((WSAA-PREM-DAY2    = 31) AND
	*****    (WSAA-PREM-MTH2    = 05))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****IF ((WSAA-PREM-MTH1    = 12) AND
	*****    (WSAA-PREM-DAY1    = 30)) AND
	*****   ((WSAA-PREM-DAY2    = 30) AND
	*****    (WSAA-PREM-MTH2    = 06))
	*****    MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*****GO TO 1690-EXIT.
	*1630-CHECK-MONTHLY-DATES.
	*    IF ((WSAA-PREM-MTH1    = 01) AND
	*        (WSAA-PREM-DAY1    > 27)  AND
	*        (WSAA-PREM-DAY1    < 31)) AND
	*       ((WSAA-PREM-DAY2    = 28) AND
	*        (WSAA-PREM-MTH2    = 02))
	*        MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*    IF ((WSAA-PREM-MTH1    = 03) AND
	*        (WSAA-PREM-DAY1    = 30)) AND
	*       ((WSAA-PREM-DAY2    = 30) AND
	*        (WSAA-PREM-MTH2    = 04))
	*        MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*    IF ((WSAA-PREM-MTH1    = 05) AND
	*        (WSAA-PREM-DAY1    = 30)) AND
	*       ((WSAA-PREM-DAY2    = 30) AND
	*        (WSAA-PREM-MTH2    = 06))
	*        MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*    IF ((WSAA-PREM-MTH1    = 08) AND
	*        (WSAA-PREM-DAY1    = 30)) AND
	*       ((WSAA-PREM-DAY2    = 30) AND
	*        (WSAA-PREM-MTH2    = 09))
	*        MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*    IF ((WSAA-PREM-MTH1    = 10) AND
	*        (WSAA-PREM-DAY1    = 30)) AND
	*       ((WSAA-PREM-DAY2    = 30) AND
	*        (WSAA-PREM-MTH2    = 11))
	*        MOVE 1             TO WSAA-FREQ-FACTOR(WSBB-SUB).
	*1690-EXIT.
	*     EXIT.
	* </pre>
	*/
protected void loadPayerDetails1700(Payrpf payrpf)
	{
		loadPayerDetails1710(payrpf);
	}

protected void loadPayerDetails1710(Payrpf payrpf)
	{
		/* Read the client role file to get the payer number.*/
 //ILIFE-8709 start
		clrfpf = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrlnbIO.getChdrpfx().toString(), payrpf.getChdrcoy(),payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1)), "PY");
		if (clrfpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(payrpf.getChdrcoy().concat(payrpf.getChdrnum().substring(0, 8).concat(String.valueOf(payrpf.getPayrseqno()).substring(0, 1))));
			xxxxFatalError();
		}
		
		/* Load the working storage table using the payer sequence*/
		/* number as the subscript.*/
		wsbbSub.set(payrpf.getPayrseqno());
		wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()].set(payrpf.getIncomeSeqNo());
		wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()].set(payrpf.getBillfreq());
		wsaaBillingInformationInner.wsaaBillcd[wsbbSub.toInt()].set(payrpf.getBillcd());
		wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].set(payrpf.getBtdate());
		wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()].set(clrfpf.getClntnum());
		wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()].set(clrfpf.getClntcoy());
		wsaaBillingInformationInner.wsaaBillday[wsbbSub.toInt()].set(payrpf.getBillday());
		wsaaBillingInformationInner.wsaaBillmonth[wsbbSub.toInt()].set(payrpf.getBillmonth());
		/* Work out how many instalments are to be paid at issue.*/
		if (isEQ(payrpf.getBillfreq(),"00")) {
			wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()].set(1);
		}
		if (isNE(payrpf.getBillfreq(),"00")
		&& isNE(payrpf.getBtdate(),chdrlnbIO.getOccdate())) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(payrpf.getBtdate());
			datcon3rec.frequency.set(payrpf.getBillfreq());
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				syserrrec.statuz.set(datcon3rec.statuz);
				xxxxFatalError();
			}
			wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()].set(datcon3rec.freqFactor);
			/* Store the first payer's frequency factor to adjust*/
			/* the contract fee later.*/
			if (isEQ(payrpf.getPayrseqno(),1)) {
				wsaaFeeFreq.set(datcon3rec.freqFactor);
			}
			/** if we are dealing with unusual dates, set up a predetermined*/
			/**  factor(for details of dates refer to sdf 2095)*/
			/**       MOVE CHDRLNB-OCCDATE     TO WSAA-PREM-DATE1               */
			/**       MOVE PAYR-BILLCD         TO WSAA-PREM-DATE2               */
			/**       PERFORM 1600-CHECK-FREQ-DATES                             */
		}
 //ILIFE-8709 end
	}

	/**
	* <pre>
	* Do everything.                                              *
	* </pre>
	*/
protected void process2000()
	{
		process2010();
		accountingRules2020();
	}

protected void process2010()
	{
		/* read t5688*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
		|| isNE(itdmIO.getItemitem(),chdrlnbIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(chdrlnbIO.getCnttype());
			syserrrec.statuz.set(e308);
			xxxxFatalError();
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		wsaaAcctLevel.set(t5688rec.comlvlacc);
	}

protected void accountingRules2020()
	{
		/*    Read second sequence of T5645 first to determine override*/
		/*    commission details and tax relief details.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("P5074");
		itemIO.setItemseq("02");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Set up array containing 3rd page of T5645 entries*/
		wsaaCount.set(30);
		wsaaCounter.set(ZERO);
		while ( !(isEQ(wsaaCount,45))) {
			wsaaCount.add(1);
			wsaaCounter.add(1);			
			wsaaT5645Cnttot[wsaaCount.toInt()].set(t5645rec.cnttot[wsaaCounter.toInt()]);
			wsaaT5645Sacscode[wsaaCount.toInt()].set(t5645rec.sacscode[wsaaCounter.toInt()]);
			wsaaT5645Sacstype[wsaaCount.toInt()].set(t5645rec.sacstype[wsaaCounter.toInt()]);
			wsaaT5645Glmap[wsaaCount.toInt()].set(t5645rec.glmap[wsaaCounter.toInt()]);
			wsaaT5645Sign[wsaaCount.toInt()].set(t5645rec.sign[wsaaCounter.toInt()]);
		}
		wsaaCount.set(15);
		wsaaCounter.set(ZERO);
		while ( !(isEQ(wsaaCount,30))) {
			wsaaCount.add(1);
			wsaaCounter.add(1);			
			wsaaT5645Cnttot[wsaaCount.toInt()].set(t5645rec.cnttot[wsaaCounter.toInt()]);
			wsaaT5645Sacscode[wsaaCount.toInt()].set(t5645rec.sacscode[wsaaCounter.toInt()]);
			wsaaT5645Sacstype[wsaaCount.toInt()].set(t5645rec.sacstype[wsaaCounter.toInt()]);
			wsaaT5645Glmap[wsaaCount.toInt()].set(t5645rec.glmap[wsaaCounter.toInt()]);
			wsaaT5645Sign[wsaaCount.toInt()].set(t5645rec.sign[wsaaCounter.toInt()]);
		}
		/*    Read first sequence of T5645 first to determine override*/
		/*    commission details and tax relief details.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("P5074");
		itemIO.setItemseq("01");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Set up array containing 2nd page of T5645 entries*/
		wsaaCount.set(ZERO);
		while ( !(isEQ(wsaaCount,15))) {
			wsaaCount.add(1);
			wsaaT5645Cnttot[wsaaCount.toInt()].set(t5645rec.cnttot[wsaaCount.toInt()]);
			wsaaT5645Sacscode[wsaaCount.toInt()].set(t5645rec.sacscode[wsaaCount.toInt()]);
			wsaaT5645Sacstype[wsaaCount.toInt()].set(t5645rec.sacstype[wsaaCount.toInt()]);
			wsaaT5645Glmap[wsaaCount.toInt()].set(t5645rec.glmap[wsaaCount.toInt()]);
			wsaaT5645Sign[wsaaCount.toInt()].set(t5645rec.sign[wsaaCount.toInt()]);
		}

		/*    Read other sequence of T5645.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem("P5074");
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*    Read T5645 descriptions.*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrlnbIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("P5074");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		/*   If a description is not found in the signon language then*/
		/*   English is used as a default.*/
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			/*    MOVE 'E'                 TO DESC-LANGUAGE                 */
			descIO.setLanguage(atmodrec.language);
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				syserrrec.statuz.set(descIO.getStatuz());
				xxxxFatalError();
			}
		}
		contractHeaderLevel2100();
		livesAssured2200();
		if(lincFlag)
			performLincProcessing();
		components2300();
		underwritingUndl2x00();
		underwritingUndc2y00();
		underwritingUndq2z00();
		inctIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		inctIO.setChdrnum(chdrlnbIO.getChdrnum());
		inctIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, inctIO);
		if (isNE(inctIO.getStatuz(),varcom.oK)
		&& isNE(inctIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(inctIO.getParams());
			syserrrec.statuz.set(inctIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(inctIO.getStatuz(),varcom.oK)) {
			inctIO.setFunction(varcom.delet);
			SmartFileCode.execute(appVars, inctIO);
			if (isNE(inctIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(inctIO.getParams());
				syserrrec.statuz.set(inctIO.getStatuz());
				xxxxFatalError();
			}
		}
 //ILIFE-8709 start
		payrpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		payrpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		payrpf.setPayrseqno(1);
		payrpf.setValidflag("1");
		payrpfList = payrpfDAO.readPayrData(payrpf);
		if (payrpfList == null || payrpfList.isEmpty()) {
			syserrrec.params.set(chdrlnbIO.getChdrcoy().concat(chdrlnbIO.getChdrnum()));
			xxxxFatalError();
		}
		for(Payrpf payrpf : payrpfList){

			if(!(isNE(payrpf.getChdrcoy(), chdrlnbIO.getChdrcoy())
					|| isNE(payrpf.getChdrnum(), chdrlnbIO.getChdrnum()))){

				updatePayer2350(payrpf);
			}
			else{
				break;
			}

		getTolerance2360(payrpf);
		procChild6000();
		contractHeaderRev2400(payrpf);
		contractAccounting2500();
		wsbbSub.set(1);
		while ( !(isGT(wsbbSub,9)
		|| isEQ(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()], SPACES))) {
			payerAccounting2550(payrpf);
		}
		wsaaAgentSub.set(1);
		commissionAccounting2600();
		commissionHold2a00();
		//ILIFE-3997-STARTS
		
		//ILIFE-3997-ENDS
		/* PERFORM 2700-REINSURANCE-ACCOUNTING.                         */
		postfeechrge();
		housekeeping2800();
		if (isEQ(chdrlnbIO.getNlgflg(),'Y')){
			CreateNlgt();
		}
		writeLetter5000();
	}
	}
 //ILIFE-8709 end

	/**
	* <pre>
	* Contract header level processing.                           *
	* </pre>
	*/
protected void postfeechrge(){
	
}
protected void contractHeaderLevel2100()
	{
		/*CONTRACT-HEADER-LEVEL*/
		contractHeaderRoles2110();
		/* PERFORM 2120-BENEFICIARY-ROLES.                              */
		commissionDetails2130();
		/*EXIT*/
	}

	/**
	* <pre>
	* Update the role records.                                    *
	* </pre>
	*/
protected void contractHeaderRoles2110()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
		try {
				switch (nextMethod) {
				case DEFAULT:
			contractHeaderRoles2111();
				case skipDespatch2111:
					skipDespatch2111();
				case exit2119:
				}
				break;
		}
		catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void contractHeaderRoles2111()
	{
		/* DISPATCH*/
		/* If the despatch number is blank then use owner.*/
		if (isEQ(chdrlnbIO.getDespnum(),SPACES)) {
			cltrelnrec.clntnum.set(chdrlnbIO.getCownnum());
		}
		else {
			/*    MOVE CHDRLNB-DESPNUM     TO CLRN-CLNTNUM.                 */
			goTo(GotoLabel.skipDespatch2111);
		}
		cltrelnrec.clrrrole.set("DA");
		writeClientRole2113();
	}

protected void skipDespatch2111()
	{
		/* ASSIGNEE*/
		/* If assignee is blank then delete any existing role record.*/
		if (isNE(chdrlnbIO.getAsgnnum(),SPACES)) {
			cltrelnrec.clntnum.set(chdrlnbIO.getAsgnnum());
			cltrelnrec.clrrrole.set("NE");
			writeClientRole2113();
		}
		goTo(GotoLabel.exit2119);
	}

	/**
	* <pre>
	* Write role record.
	* </pre>
	*/
protected void writeClientRole2113()
	{
		cltrelnrec.clntpfx.set("CN");
		cltrelnrec.clntcoy.set(wsaaFsuCoy);
		cltrelnrec.forepfx.set("CH");
		cltrelnrec.forecoy.set(atmodrec.company);
		cltrelnrec.forenum.set(chdrlnbIO.getChdrnum());
		cltrelnrec.function.set("ADD  ");
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
//		MIBT-356
//		if (isNE(cltrelnrec.statuz,varcom.oK)) {
//			syserrrec.params.set(cltrelnrec.cltrelnRec);
//			syserrrec.statuz.set(cltrelnrec.statuz);
//			xxxxFatalError();
//		}
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrlnbIO.getChdrpfx());
		bldenrlrec.company.set(chdrlnbIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrlnbIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Update the benificiary role records.                        *
	*2120-BENEFICIARY-ROLES SECTION.
	*2121-BENEFICIARY-ROLES.
	*    MOVE SPACES                 TO BNFYLNB-DATA-AREA.
	*    MOVE ATMD-COMPANY           TO BNFYLNB-CHDRCOY.
	*    MOVE CHDRLNB-CHDRNUM        TO BNFYLNB-CHDRNUM.
	*    MOVE BEGN                   TO BNFYLNB-FUNCTION.
	*    CALL 'BNFYLNBIO'            USING BNFYLNB-PARAMS.
	*    IF BNFYLNB-STATUZ           NOT = O-K
	*                                AND NOT = ENDP
	*       MOVE BNFYLNB-PARAMS      TO SYSR-PARAMS
	*       MOVE BNFYLNB-STATUZ      TO SYSR-STATUZ
	*       PERFORM XXXX-FATAL-ERROR.
	*    IF BNFYLNB-STATUZ           = ENDP
	*    OR BNFYLNB-CHDRCOY          NOT = ATMD-COMPANY
	*    OR BNFYLNB-CHDRNUM          NOT = CHDRLNB-CHDRNUM
	*       GO TO 2129-EXIT.
	*    MOVE 'CN'                   TO CLRN-CLNTPFX.
	*    MOVE WSAA-FSU-COY           TO CLRN-CLNTCOY.
	*    MOVE 'CH'                   TO CLRN-FOREPFX.
	*    MOVE ATMD-COMPANY           TO CLRN-FORECOY.
	*    MOVE CHDRLNB-CHDRNUM        TO CLRN-FORENUM.
	*2122-WRITE-BENEFICIARY-ROLE.
	*    MOVE BNFYLNB-BNYCLT         TO CLRN-CLNTNUM.
	*    MOVE 'BN'                   TO CLRN-CLRRROLE.
	*    MOVE 'ADD  '                TO CLRN-FUNCTION.
	*    CALL 'CLTRELN'              USING CLRN-CLTRELN-REC.
	*    IF CLRN-STATUZ              NOT = O-K
	*       MOVE CLRN-CLTRELN-REC    TO SYSR-PARAMS
	*       MOVE CLRN-STATUZ         TO SYSR-STATUZ
	*       PERFORM XXXX-FATAL-ERROR.
	*    MOVE NEXTR                  TO BNFYLNB-FUNCTION.
	*    CALL 'BNFYLNBIO'            USING BNFYLNB-PARAMS.
	*    IF BNFYLNB-STATUZ           NOT = O-K
	*                                AND NOT = ENDP
	*       MOVE BNFYLNB-PARAMS      TO SYSR-PARAMS
	*       MOVE BNFYLNB-STATUZ      TO SYSR-STATUZ
	*       PERFORM XXXX-FATAL-ERROR.
	*    IF BNFYLNB-CHDRCOY          NOT = ATMD-COMPANY
	*    OR BNFYLNB-CHDRNUM          NOT = CHDRLNB-CHDRNUM
	*       MOVE ENDP                TO BNFYLNB-STATUZ.
	*2123-PERFORM-WRITE-READN.
	*    PERFORM 2122-WRITE-BENEFICIARY-ROLE
	*                                UNTIL BNFYLNB-STATUZ = ENDP.
	*2129-EXIT.
	*    EXIT.
	* Write a commission details record if none exists.           *
	* </pre>
	*/
protected void commissionDetails2130()
	{
			commissionDetails2131();
		}

protected void commissionDetails2131()
	{
		pcddlnbIO.setChdrcoy(atmodrec.company);
		pcddlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		pcddlnbIO.setAgntnum(SPACES);
		pcddlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcddlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcddlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, pcddlnbIO);
		if (isNE(pcddlnbIO.getStatuz(),varcom.oK)
		&& isNE(pcddlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcddlnbIO.getParams());
			syserrrec.statuz.set(pcddlnbIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(pcddlnbIO.getChdrcoy(),atmodrec.company)
		&& isEQ(pcddlnbIO.getChdrnum(),chdrlnbIO.getChdrnum())
		&& isNE(pcddlnbIO.getStatuz(),varcom.endp)) {
			return ;
		}
		
		pcddlnbIO.setChdrcoy(atmodrec.company);
		pcddlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		pcddlnbIO.setAgntnum(chdrlnbIO.getAgntnum());
		pcddlnbIO.setTranno(chdrlnbIO.getTranno());
		pcddlnbIO.setCurrfrom(chdrlnbIO.getOccdate());
		pcddlnbIO.setCurrto(chdrlnbIO.getCurrto());
		pcddlnbIO.setUser(wsaaUser);
		pcddlnbIO.setTermid(wsaaTermid);
		pcddlnbIO.setTransactionTime(wsaaTransactionTime);
		pcddlnbIO.setTransactionDate(wsaaTransactionDate);
		pcddlnbIO.setSplitBcomm(100);
		pcddlnbIO.setSplitBpts(100);
		pcddlnbIO.setValidflag("1");
		pcddlnbIO.setFormat(formatsInner.pcddlnbrec);
		pcddlnbIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, pcddlnbIO);
		if (isNE(pcddlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(pcddlnbIO.getParams());
			syserrrec.statuz.set(pcddlnbIO.getStatuz());
			xxxxFatalError();
		}
		}

	/**
	* <pre>
	* Update all the life records.                                *
	* </pre>
	*/
protected void livesAssured2200()
	{
		livesAssured2201();
	}

protected void livesAssured2201()
	{
		/* Read first life.*/
 //ILIFE-8709 start
		lifepfList = lifepfDAO.getLifeList(atmodrec.company.toString(), chdrlnbIO.getChdrnum().toString());
		
		wsaaLifcnum.set(lifepfList.get(0).getLifcnum());
		for (Lifepf life : lifepfList) {
			if (isEQ(life.getValidflag(), "2")) {
				continue;
			}
			updateLife2210(life);
		}
 //ILIFE-8709 end
	}

	/**
	* <pre>
	* Actually update the life record.                            *
	* </pre>
	*/
protected void updateLife2210(Lifepf life)
	{
		updateLife2211(life);
	}

protected void updateLife2211(Lifepf life)
	{
 //ILIFE-8709 start
		life.setValidflag("1");
		life.setTranno(chdrlnbIO.getTranno().toInt());
		life.setStatcode(t5679rec.setLifeStat.toString());
		if (isNE(life.getJlife(),SPACES)
		&& isNE(life.getJlife(),"00")) {
			life.setStatcode(t5679rec.setJlifeStat.toString());
		}
		life.setUser(wsaaUser.toInt());
		life.setTransactionDate(wsaaTransactionDate.toInt());
		life.setTransactionTime(wsaaTransactionTime.toInt());
		life.setTermid(wsaaTermid.toString());
		updateLifepflist.add(life);
		lifepfDAO.updateLifeRecord(updateLifepflist);
 //ILIFE-8709 end
	}

	/**
	* <pre>
	* Create cover and rider information.                         *
	* </pre>
	*/
protected void components2300()
	{
		components2301();
		noPlanProcessing2302();
	}

protected void components2301()
	{
		/*    PTDATE moved in early as it is used in commission processing*/
		if (isEQ(chdrlnbIO.getBillfreq(),"00")) {
			chdrlnbIO.setPtdate(wsaaOldCessDate);
		}
		else {
			chdrlnbIO.setPtdate(chdrlnbIO.getBtdate());
		}
		/* Find the smallest number applicable.*/
		wsaaNumapp.set(1);
		calcSummarisedPolicies2310();
		chdrlnbIO.setPolsum(wsaaNumapp);
		chdrlnbIO.setNxtsfx(wsaaNumapp);
		if (isEQ(wsaaNumapp,1)) {
			chdrlnbIO.setPolsum(ZERO);
		}
		varcom.vrcmCompTermid.set(wsaaTermid);
		varcom.vrcmUser.set(wsaaUser);
		varcom.vrcmDate.set(wsaaTransactionDate);
		varcom.vrcmTime.set(wsaaTransactionTime);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrlnbIO.setTranid(varcom.vrcmCompTranid);
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			xxxxFatalError();
		}
		chdrlnbIO.setFunction(varcom.writs);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	* Create the cover and rider records.
	* </pre>
	*/
protected void noPlanProcessing2302()
	{
		createRiderCoverInfo2320();
		/* To detect for WOP and update the re-rate and CPI dates          */
		/* to it if applicable.                                            */
		a100WopDates();
		/*EXIT*/
	}

	/**
	* <pre>
	* Calculate the sumarised policies.                           *
	* </pre>
	*/
protected void calcSummarisedPolicies2310()
	{
			calcSummarisedPolicies2311();
		}

protected void calcSummarisedPolicies2311()
	{
		/* Initialise coverage premium table.*/
		wsaaC.set(1);
		wsaaL.set(1);
		while ( !((isGT(wsaaL,10)))) {
			initialiseCovrPrem3000();
		}

		/* Start the COVT file.*/
		wsaaNumapp.set(ZERO);
 //ILIFE-8709 start		
		covtpfList = covtpfDAO.searchCovtRecordByCoyNumDescUniquNo(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		wsaaCoverage.set(covtlnb.getCoverage());
		wsaaRider.set(covtlnb.getRider());
		wsaaNumapp.set(covtlnb.getNumapp());
		if (isEQ(covtlnb.getRider(),"00")
		|| isEQ(covtlnb.getRider(), "  ")) {
			wsaaCoverageNum.set(covtlnb.getCoverage());
			wsaaLifeNum.set(covtlnb.getLife());
			wsaaCovtInstprem[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()].add(covtlnb.getInstprem().doubleValue());
			wsaaCovtSingp[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()].add(covtlnb.getSingp().doubleValue());
		}
		if(!(covtpfList.isEmpty())){
			for (Covtpf covtlnbpf:covtpfList){
				covtlnb=covtlnbpf;
				readCoverRider231a();
			}
		}

		
		calculateFeeTotal();    //MLIL# 765

	}
	
	//MLIL# 765
protected void calculateFeeTotal()
	{
	
	}

	/**
	* <pre>
	* Read the COVT file, when the cover changes check the number *
	* applicable against the least store number applicable,       *
	* storing the least.                                          *
	* </pre>
	*/
 //ILIFE-8709 start
protected void readCoverRider231a()
	{
		if (isNE(covtlnb.getCoverage(),wsaaCoverage)
			|| isNE(covtlnb.getRider(),wsaaRider)) {
				wsaaCoverage.set(covtlnb.getCoverage());
				wsaaRider.set(covtlnb.getRider());
				if (isLT(covtlnb.getNumapp(),wsaaNumapp)) {
					wsaaNumapp.set(covtlnb.getNumapp());
				}
			}
		if (isEQ(covtlnb.getRider(),"00")
		|| isEQ(covtlnb.getRider(), "  ")) {
			wsaaCoverageNum.set(covtlnb.getCoverage());
			wsaaLifeNum.set(covtlnb.getLife());
			wsaaCovtInstprem[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()].add(covtlnb.getInstprem().doubleValue());
			wsaaCovtSingp[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()].add(covtlnb.getSingp().doubleValue());
		}
	}
 //ILIFE-8709 end
	/**
	* <pre>
	* Actually create the COVR records depending on criteria.     *
	* </pre>
	*/
protected void createRiderCoverInfo2320()
	{
			createRiderCoverInfo2321();
		}

protected void createRiderCoverInfo2321()
	{
		/* Begin the COVT file again.*/
	 //ILIFE-8709 start
		covtpfList = covtpfDAO.searchCovtRecordByCoyNumDescUniquNo(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		Covtpf covtpf=covtpfList.get(0);
		if (isNE(covtpf.getSingp(),0)) {
			wsaaSingPrmInd = "Y";
		}
		wsaaOldCessDate.set(covtpf.getPcesdte());
		/* Clear the COVR record for subsequent writing..*/
		covrlnbIO.setNonKey(SPACES);
		covrlnbIO.setSumins(0);
		covrlnbIO.setVarSumInsured(0);
		covrlnbIO.setDeferPerdAmt(0);
		covrlnbIO.setTotMthlyBenefit(0);
		covrlnbIO.setSingp(0);
		covrlnbIO.setInstprem(0);
		covrlnbIO.setZbinstprem(0);
		covrlnbIO.setcommPrem(0);     //IBPLIFE-5237
		covrlnbIO.setZlinstprem(0);
		/*BRD-306 START */	
		covrlnbIO.setLoadper(0);
		covrlnbIO.setRateadj(0);
		covrlnbIO.setAgeadj(0);
		covrlnbIO.setFltmort(0);
		covrlnbIO.setPremadj(0);
		/*BRD-306 END */
		//BRD-NBP-012-STARTS
		covrlnbIO.setGmib(SPACES);
		covrlnbIO.setGmdb(SPACES);
		covrlnbIO.setGmwb(SPACES);
		//BRD-NBP-012-ENDS
		stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if (stampDutyflag) {
			covrlnbIO.setZstpduty01(ZERO);
			covrlnbIO.setZclstate(SPACES);
		}
		covrlnbIO.setStatSumins(0);
		covrlnbIO.setAnbAtCcd(0);
		covrlnbIO.setPayrseqno(0);
		covrlnbIO.setCrrcd(0);
		/* No plan processing is applicable for every coverage or rider*/
		/* so just write a COVR record for each COVT record.*/
		if (isEQ(wsaaNumapp,chdrlnbIO.getPolinc())
		|| isLTE(chdrlnbIO.getPolinc(),1)) {
			for(Covtpf covtlnbpf : covtpfList){
				covtlnb=covtlnbpf;
				aPlanNotApplicable232a();
			}

			return ;
		}
		/* No summary records are to be written. Split out all covers*/
		/* and rider records starting with a suffix number of 1.*/
		if (isEQ(wsaaNumapp,1)
		&& isGT(chdrlnbIO.getPolinc(),1)) {
			wsaaCoverage.set(covtpf.getCoverage());
			wsaaRider.set(covtpf.getRider());
			wsaaNoSummaryRec.set("Y");
			wsaaPlanSuffix.set(1);
			
			for(Covtpf covtlnbpf : covtpfList){
				covtlnb=covtlnbpf;
				bPlanApplicable232b();
			}

			return ;
		}
		/* Otherwise plan processing is applicable.*/
		
		for(Covtpf covtlnbpf : covtpfList){
			covtlnb=covtlnbpf;
			cPlanAndSummaries232c();
		}
 //ILIFE-8709 end
	}
 //ILIFE-8709 start
protected void aPlanNotApplicable232a()
	{
		/*A-PLAN-NOT-APPLICABLE*/
		wsaaPlanSuffix.set(0);
		covrlnbIO.setSumins(covtlnb.getSumins());
		covrlnbIO.setSingp(covtlnb.getSingp());
		covrlnbIO.setInstprem(covtlnb.getInstprem());
		covrlnbIO.setZbinstprem(covtlnb.getZbinstprem());
		covrlnbIO.setZlinstprem(covtlnb.getZlinstprem());	
		covrlnbIO.setcommPrem(covtlnb.getCommPrem());   //IBPLIFE-5237
		writeCoverRiders232d();
		m800UpdateMlia();
		deletAndReadCovt232e();
		/*A-EXIT*/
	}

protected void bPlanApplicable232b()
	{
		bPlanApplicable2321();
	}

protected void bPlanApplicable2321()
	{
		/* Divide sum insured and premium by the number applicable.*/
		setPrecision(covrlnbIO.getSumins(), 3);
		covrlnbIO.setSumins(div(covtlnb.getSumins(),covtlnb.getNumapp()), true);
		zrdecplcPojo.setAmountIn(covrlnbIO.getSumins().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setSumins(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		/* Initialise SINGP and INSTPREM first, since they may contain*/
		/* values from the last policy.(Essential for non-identical*/
		/* policies)*/
		covrlnbIO.setSingp(0);
		covrlnbIO.setZbinstprem(0);
		covrlnbIO.setZlinstprem(0);
		covrlnbIO.setInstprem(0);
		covrlnbIO.setcommPrem(0);  //IBPLIFE-5237
		
		if (isNE(covtlnb.getSingp(),0)) {
			setPrecision(covrlnbIO.getSingp(), 3);
			covrlnbIO.setSingp(div(covtlnb.getSingp(),covtlnb.getNumapp()), true);
		}
		zrdecplcPojo.setAmountIn(covrlnbIO.getSingp().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setSingp(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		if (isNE(covtlnb.getInstprem(),0)) {
			setPrecision(covrlnbIO.getInstprem(), 3);
			covrlnbIO.setInstprem(div(covtlnb.getInstprem(),covtlnb.getNumapp()), true);
		}
		zrdecplcPojo.setAmountIn(covrlnbIO.getInstprem().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setInstprem(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		if (isNE(covtlnb.getZbinstprem(),0)) {
			setPrecision(covrlnbIO.getZbinstprem(), 3);
			covrlnbIO.setZbinstprem(div(covtlnb.getZbinstprem(),covtlnb.getNumapp()), true);
		}
		zrdecplcPojo.setAmountIn(covrlnbIO.getZbinstprem().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setZbinstprem(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		if (isNE(covtlnb.getZlinstprem(),0)) {
			setPrecision(covrlnbIO.getZlinstprem(), 3);
			covrlnbIO.setZlinstprem(div(covtlnb.getZlinstprem(),covtlnb.getNumapp()), true);
		}
		zrdecplcPojo.setAmountIn(covrlnbIO.getZlinstprem().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setZlinstprem(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		
		/*IBPLIFE- 5237 starts */
		if (isNE(covtlnb.getCommPrem(),0)) {
			setPrecision(covrlnbIO.getCommPrem(), 3);
			covrlnbIO.setcommPrem(div(covtlnb.getCommPrem(),covtlnb.getNumapp()), true);
		}
		zrdecplcPojo.setAmountIn(covrlnbIO.getCommPrem().getbigdata()); 
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		covrlnbIO.setcommPrem(zrdecplcPojo.getAmountOut()); 
		/*IBPLIFE- 5237 ends */
		
		
		
		/* Start with a plan suffix of 1 (and adding 1 each time a*/
		/* record is written.) and write a cover/rider record for*/
		/* each policy.*/
		PackedDecimalData loopEndVar1 = new PackedDecimalData(7, 0);
		loopEndVar1.set(covtlnb.getNumapp());
		for (int loopVar1 = 0; !(isEQ(loopVar1,loopEndVar1.toInt())); loopVar1 += 1){
			writeCoverRiders232d();
		}
		deletAndReadCovt232e();
		/* Reset the plan suffix number when a different component*/
		/* is read.*/
		if (isNE(covtlnb.getCoverage(),wsaaCoverage)
		|| isNE(covtlnb.getRider(),wsaaRider)) {
			wsaaCoverage.set(covtlnb.getCoverage());
			wsaaRider.set(covtlnb.getRider());
			wsaaNoSummaryRec.set("Y");
			wsaaPlanSuffix.set(1);
		}
	}

protected void cPlanAndSummaries232c()
	{
		try {
			cPlanAndSummaries2321();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void cPlanAndSummaries2321()
	{
		/* Write a summary record.*/
		wsaaPlanSuffix.set(0);
		setPrecision(covrlnbIO.getSumins(), 3);
		covrlnbIO.setSumins(mult((div(covtlnb.getSumins(),covtlnb.getNumapp())),wsaaNumapp), true);
		zrdecplcPojo.setAmountIn(covrlnbIO.getSumins().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setSumins(zrdecplcPojo.getAmountOut());
		/* Initialise SINGP and INSTPREM first, since they may contain*/
		/* values from the last policy.(Essential for non-identical*/
		/* policies)*/
		covrlnbIO.setSingp(0);
		covrlnbIO.setZbinstprem(0);
		covrlnbIO.setZlinstprem(0);
		covrlnbIO.setInstprem(0);
		covrlnbIO.setcommPrem(0);    //IBPLIFE-5237
		
		
		if (isNE(covtlnb.getSingp(),0)) {
			setPrecision(covrlnbIO.getSingp(), 3);
			covrlnbIO.setSingp(mult((div(covtlnb.getSingp(),covtlnb.getNumapp())),wsaaNumapp), true);
		}
		zrdecplcPojo.setAmountIn(covrlnbIO.getSingp().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setSingp(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		if (isNE(covtlnb.getInstprem(),0)) {
			setPrecision(covrlnbIO.getInstprem(), 3);
			covrlnbIO.setInstprem(mult((div(covtlnb.getInstprem(),covtlnb.getNumapp())),wsaaNumapp), true);
		}
		zrdecplcPojo.setAmountIn(covrlnbIO.getInstprem().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setInstprem(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		if (isNE(covtlnb.getZbinstprem(),0)) {
			setPrecision(covrlnbIO.getZbinstprem(), 3);
			covrlnbIO.setZbinstprem(mult((div(covtlnb.getZbinstprem(),covtlnb.getNumapp())),wsaaNumapp), true);
		}
		zrdecplcPojo.setAmountIn(covrlnbIO.getZbinstprem().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setZbinstprem(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		if (isNE(covtlnb.getZlinstprem(),0)) {
			setPrecision(covrlnbIO.getZlinstprem(), 3);
			covrlnbIO.setZlinstprem(mult((div(covtlnb.getZlinstprem(),covtlnb.getNumapp())),wsaaNumapp), true);
		}
		zrdecplcPojo.setAmountIn(covrlnbIO.getZlinstprem().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		covrlnbIO.setZlinstprem(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		
		/* Write COVR records with a number applicable fraction of*/
		/* the sum insured and premium and write the COVR records*/
		/* until the number applicable is reached.*/
		writeCoverRiders232d();
		compute(wsaaNoOfRecs, 0).set(sub(covtlnb.getNumapp(),wsaaNumapp));
		compute(wsaaPlanSuffix, 0).set(add(wsaaNumapp,1));
		if (isNE(wsaaNoOfRecs,0)) {
			PackedDecimalData loopEndVar2 = new PackedDecimalData(7, 0);
			loopEndVar2.set(wsaaNoOfRecs);
			for (int loopVar2 = 0; !(isEQ(loopVar2,loopEndVar2.toInt())); loopVar2 += 1){
				writeCovrs2322();
			}
		}
		wsaaCoverage.set(covtlnb.getCoverage());
		wsaaRider.set(covtlnb.getRider());
		deletAndReadCovt232e();
		/* Read the next record.*/
		if(covtlnb!=null){
			nextCoverRider2323();
		}

		/* By pass paragraphs below - they are called from above.*/
		goTo(GotoLabel.exit232c);
	}

protected void writeCovrs2322()
	{
		setPrecision(covrlnbIO.getSumins(), 3);
		covrlnbIO.setSumins((div(covtlnb.getSumins(),covtlnb.getNumapp())), true);
		/* Initialise SINGP and INSTPREM first, since they may contain*/
		/* values from the last policy.(Essential for non-identical*/
		/* policies)*/
		covrlnbIO.setSingp(0);
		covrlnbIO.setZbinstprem(0);
		covrlnbIO.setZlinstprem(0);
		covrlnbIO.setInstprem(0);
		covrlnbIO.setcommPrem(0);
		
		
		if (isNE(covtlnb.getSingp(),0)) {
			setPrecision(covrlnbIO.getSingp(), 3);
			covrlnbIO.setSingp((div(covtlnb.getSingp(),covtlnb.getNumapp())), true);
		}
		if (isNE(covtlnb.getInstprem(),0)) {
			setPrecision(covrlnbIO.getInstprem(), 3);
			covrlnbIO.setInstprem((div(covtlnb.getInstprem(),covtlnb.getNumapp())), true);
		}
		if (isNE(covtlnb.getZbinstprem(),0)) {
			setPrecision(covrlnbIO.getZbinstprem(), 3);
			covrlnbIO.setZbinstprem((div(covtlnb.getZbinstprem(),covtlnb.getNumapp())), true);
		}
		if (isNE(covtlnb.getZlinstprem(),0)) {
			setPrecision(covrlnbIO.getZlinstprem(), 3);
			covrlnbIO.setZlinstprem((div(covtlnb.getZlinstprem(),covtlnb.getNumapp())), true);
		}
		if (isNE(covtlnb.getCommPrem(),0)) {
			setPrecision(covrlnbIO.getCommPrem(), 3);
			covrlnbIO.setcommPrem((div(covtlnb.getCommPrem(),covtlnb.getNumapp())), true);
		}
		
		
		writeCoverRiders232d();
	}

protected void nextCoverRider2323()
{
 PackedDecimalData loopEndVar3 = new PackedDecimalData(7, 0);
	loopEndVar3.set(covtlnb.getNumapp());
	for (int loopVar3 = 0; !(isEQ(loopVar3,loopEndVar3.toInt())); loopVar3 += 1){
		writeCovrs2322();
	}
	deletAndReadCovt232e();
}

protected void writeCoverRiders232d()
	{
		writeCoverRiders2321();
	}

protected void writeCoverRiders2321()
	{
		/* Access the table to initialise the COVR fields.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covtlnb.getCrtable());
		itdmIO.setItmfrm(covtlnb.getEffdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),atmodrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t5687)
		|| isNE(itdmIO.getItemitem(),covtlnb.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			//ILAE-63 START by dpuhawan
			t5687rec.t5687Rec.set(SPACES);
			//ILAE-63 END;			
			syserrrec.params.set(covtlnb.getCrtable());
			syserrrec.statuz.set(f294);
			xxxxFatalError();
		}
		else {
			/*     MOVE ITDM-GENAREA        TO T5687-T5687-REC.              */
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
			if(isDMSFlgOn) {	//ILIFE-8880
				wsaaBasicCommMeth.set(SPACES);
			} else {
				wsaaBasicCommMeth.set(t5687rec.basicCommMeth);
			}
			wsaaBascpy.set(t5687rec.bascpy);
			wsaaSrvcpy.set(t5687rec.srvcpy);
			wsaaRnwcpy.set(t5687rec.rnwcpy);
			wsaaBasscmth.set(t5687rec.basscmth);
			wsaaBasscpy.set(t5687rec.basscpy);
		}
		covrlnbIO.setPlanSuffix(wsaaPlanSuffix);
		covrlnbIO.setCurrfrom(chdrlnbIO.getOccdate());
		covrlnbIO.setCurrto(chdrlnbIO.getCurrto());
		covrlnbIO.setTranno(chdrlnbIO.getTranno());
		covrlnbIO.setCrrcd(covtlnb.getEffdate());
		covrlnbIO.setValidflag("1");
		if (isEQ(covtlnb.getRider(),"00")
		|| isEQ(covtlnb.getRider(),SPACES)) {
			wsaaMainCoverage.set(covtlnb.getCrtable());
			covrlnbIO.setStatcode(t5679rec.setCovRiskStat);
		}
		else {
			wsaaTr695Coverage.set(wsaaMainCoverage);
			wsaaTr695Rider.set(covtlnb.getCrtable());
			itdmIO.setDataArea(SPACES);
			itdmIO.setItemcoy(atmodrec.company);
			itdmIO.setItemtabl(tablesInner.tr695);
			itdmIO.setItemitem(wsaaTr695Key);
			itdmIO.setItmfrm(covtlnb.getEffdate());
			itdmIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(),varcom.oK)
			&& isNE(itdmIO.getStatuz(),varcom.endp)) {
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(itdmIO.getStatuz());
				xxxxFatalError();
			}
			if (isNE(itdmIO.getItemcoy(),atmodrec.company)
			|| isNE(itdmIO.getItemtabl(), tablesInner.tr695)
			|| isNE(itdmIO.getItemitem(),wsaaTr695Key)
			|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
				itdmIO.setStatuz(varcom.endp);
			}
			if (isEQ(itdmIO.getStatuz(),varcom.endp)) {
				wsaaTr695Rider.set("****");
				itdmIO.setDataArea(SPACES);
				itdmIO.setItemcoy(atmodrec.company);
				itdmIO.setItemtabl(tablesInner.tr695);
				itdmIO.setItemitem(wsaaTr695Key);
				itdmIO.setItmfrm(covtlnb.getEffdate());
				itdmIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
				SmartFileCode.execute(appVars, itdmIO);
				if (isNE(itdmIO.getStatuz(),varcom.oK)
				&& isNE(itdmIO.getStatuz(),varcom.endp)) {
					syserrrec.params.set(itdmIO.getParams());
					syserrrec.statuz.set(itdmIO.getStatuz());
					xxxxFatalError();
				}
				if (isNE(itdmIO.getItemcoy(),atmodrec.company)
				|| isNE(itdmIO.getItemtabl(), tablesInner.tr695)
				|| isNE(itdmIO.getItemitem(),wsaaTr695Key)
				|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
					/*CONTINUE_STMT*/
				}
				else {
					tr695rec.tr695Rec.set(itdmIO.getGenarea());
					if(isDMSFlgOn) {	//ILIFE-9039
						wsaaBasicCommMeth.set(SPACES);
					} else {
						wsaaBasicCommMeth.set(tr695rec.basicCommMeth);
					}	
					wsaaBascpy.set(tr695rec.bascpy);
					wsaaSrvcpy.set(tr695rec.srvcpy);
					wsaaRnwcpy.set(tr695rec.rnwcpy);
					wsaaBasscmth.set(tr695rec.basscmth);
					wsaaBasscpy.set(tr695rec.basscpy);
				}
			}
			else {
				tr695rec.tr695Rec.set(itdmIO.getGenarea());
				if(isDMSFlgOn) {	//ILIFE-9039
						wsaaBasicCommMeth.set(SPACES);
					} else {
						wsaaBasicCommMeth.set(tr695rec.basicCommMeth);
					}	
				wsaaBascpy.set(tr695rec.bascpy);
				wsaaSrvcpy.set(tr695rec.srvcpy);
				wsaaRnwcpy.set(tr695rec.rnwcpy);
				wsaaBasscmth.set(tr695rec.basscmth);
				wsaaBasscpy.set(tr695rec.basscpy);
			}
			/*      MOVE T5679-SET-RID-RISK-STAT TO COVRLNB-STATCODE.        */
			covrlnbIO.setStatcode(t5679rec.setRidRiskStat);
		}
		if (isNE(chdrlnbIO.getBillfreq(),"00")
		&& isNE(t5687rec.singlePremInd,"Y")) {
			if (isEQ(covtlnb.getRider(),SPACES)
			|| isEQ(covtlnb.getRider(),"00")) {
				covrlnbIO.setPstatcode(t5679rec.setCovPremStat);
			}
			else {
				covrlnbIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		else {
			if (isEQ(covtlnb.getRider(),SPACES)
			|| isEQ(covtlnb.getRider(),"00")) {
				covrlnbIO.setPstatcode(t5679rec.setSngpCovStat);
			}
			else {
				covrlnbIO.setPstatcode(t5679rec.setSngpRidStat);
			}
		}
		/*        MOVE T5679-SET-SNGP-COV-STAT                          */
		/*                             TO COVRLNB-PSTATCODE.            */
		covrlnbIO.setReptcds(t5687rec.reptcds);
		covrlnbIO.setStatFund(t5687rec.statFund);
		covrlnbIO.setStatSect(t5687rec.statSect);
		covrlnbIO.setStatSubsect(t5687rec.statSubSect);
		/* Move relevent COVT fields and clear non-used numerics*/
		/* and dates.*/
		covrlnbIO.setChdrcoy(covtlnb.getChdrcoy());
		covrlnbIO.setChdrnum(covtlnb.getChdrnum());
		covrlnbIO.setLife(covtlnb.getLife());
		covrlnbIO.setCoverage(covtlnb.getCoverage());
		covrlnbIO.setRider(covtlnb.getRider());
		covrlnbIO.setCrInstamt01(0);
		covrlnbIO.setCrInstamt02(0);
		covrlnbIO.setCrInstamt03(0);
		covrlnbIO.setCrInstamt04(0);
		covrlnbIO.setCrInstamt05(0);
		covrlnbIO.setEstMatValue01(0);
		covrlnbIO.setEstMatValue02(0);
		covrlnbIO.setEstMatDate01(0);
		covrlnbIO.setEstMatDate02(0);
		covrlnbIO.setEstMatInt01(0);
		covrlnbIO.setEstMatInt02(0);
		covrlnbIO.setPremCurrency(chdrlnbIO.getCntcurr());
		covrlnbIO.setJlife(covtlnb.getJlife());
		if (isEQ(covtlnb.getJlife(),SPACES)
		|| isEQ(covtlnb.getJlife(),"00")) {
			covrlnbIO.setAnbAtCcd(covtlnb.getAnbccd01());
			covrlnbIO.setSex(covtlnb.getSex01());
		}
		else {
			covrlnbIO.setAnbAtCcd(covtlnb.getAnbccd02());
			covrlnbIO.setSex(covtlnb.getSex02());
		}
		covrlnbIO.setCrtable(covtlnb.getCrtable());
		covrlnbIO.setRiskCessDate(covtlnb.getRcesdte());
		covrlnbIO.setPremCessDate(covtlnb.getPcesdte());
		covrlnbIO.setBenCessDate(covtlnb.getBcesdte());
		covrlnbIO.setNextActDate(0);
		covrlnbIO.setRiskCessAge(covtlnb.getRcesage());
		covrlnbIO.setPremCessAge(covtlnb.getPcesage());
		covrlnbIO.setBenCessAge(covtlnb.getBcesage());
		/*MOVE 0                      TO COVRLNB-BEN-CESS-AGE.         */
		covrlnbIO.setRiskCessTerm(covtlnb.getRcestrm());
		covrlnbIO.setPremCessTerm(covtlnb.getPcestrm());
		covrlnbIO.setBenCessTerm(covtlnb.getBcestrm());
		/*MOVE 0                      TO COVRLNB-BEN-CESS-TERM.        */
		covrlnbIO.setVarSumInsured(0);
		covrlnbIO.setMortcls(covtlnb.getMortcls());
		covrlnbIO.setLiencd(covtlnb.getLiencd());
		covrlnbIO.setBappmeth(covtlnb.getBappmeth());
		covrlnbIO.setDeferPerdAmt(0);
		covrlnbIO.setTotMthlyBenefit(0);
		covrlnbIO.setCoverageDebt(0);
		covrlnbIO.setStatSumins(0);
		covrlnbIO.setRtrnyrs(0);
		covrlnbIO.setPremCessAgeMth(0);
		covrlnbIO.setPremCessAgeDay(0);
		covrlnbIO.setPremCessTermMth(0);
		covrlnbIO.setPremCessTermDay(0);
		covrlnbIO.setRiskCessAgeMth(0);
		covrlnbIO.setRiskCessAgeDay(0);
		covrlnbIO.setRiskCessTermMth(0);
		covrlnbIO.setRiskCessTermDay(0);
		covrlnbIO.setTransactionDate(wsaaTransactionDate);
		covrlnbIO.setTransactionTime(wsaaTransactionTime);
		covrlnbIO.setUser(wsaaUser);
		covrlnbIO.setTermid(wsaaTermid);
		covrlnbIO.setReserveUnitsInd(covtlnb.getRsunin());
		covrlnbIO.setReserveUnitsDate(covtlnb.getRundte());
		covrlnbIO.setPayrseqno(covtlnb.getPayrseqno());
		covrlnbIO.setTpdtype(covtlnb.getTpdtype());//ILIFE-7118
		/* Before the coverage record is written compute the rerate,*/
		/* rereate from and benefit billing dates.*/
		computeDates232d();
		if(fmcOnFlag && isNE(t5687rec.bbmeth,SPACES))
			d100WriteZfmc();
		/* Compute the Benefit Billing.*/
		benefitBilling233a();
		/* If this is a flexible premium contract then we need to w<D9604>*/
		/* an FPCO record at coverage level.                       <D9604>*/
		if (flexiblePremiumContract.isTrue()
		&& isNE(covrlnbIO.getInstprem(),0)) {
			writeFpco7000();
		}
		/*BRD-306 START */	
		covrlnbIO.setLoadper(covtlnb.getLoadper());
		covrlnbIO.setRateadj(covtlnb.getRateadj());
		covrlnbIO.setAgeadj(covtlnb.getAgeadj());
		covrlnbIO.setFltmort(covtlnb.getFltmort());
		covrlnbIO.setPremadj(covtlnb.getPremadj());
		//BRD-NBP-012-STARTS
		covrlnbIO.setGmib(covtlnb.getGmib());
		covrlnbIO.setGmdb(covtlnb.getGmdb());
		covrlnbIO.setGmwb(covtlnb.getGmwb());
		/*ILIFE-6941 start*/
		covrlnbIO.setLnkgno(covtlnb.getLnkgno());
		covrlnbIO.setLnkgsubrefno(covtlnb.getLnkgsubrefno());
		/*ILIFE-6941 end*/
		covrlnbIO.setLnkgind(covtlnb.getLnkgind());
		covrlnbIO.setSingpremtype(covtlnb.getSingpremtype());//ILIFE-7805
		/*BRD-306 END */
		stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if (stampDutyflag) {
			covrlnbIO.setZstpduty01(covtlnb.getZstpduty01());
			covrlnbIO.setZclstate(covtlnb.getZclstate());
		}
		covrlnbIO.setRiskprem(covtlnb.getRiskprem()); //ILIFE-7845
		/* Write the covr record.*/
		covrlnbIO.setFormat(formatsInner.covrlnbrec);
		covrlnbIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covrlnbIO.getParams());
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			xxxxFatalError();
		}
		
		updtpurepremCustomerSpecific();
		riskPremflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), RISKPREM_FEATURE_ID, appVars, "IT");
		if (riskPremflag) {
			calculateRiskPrem();
		}
		/* After each COVR record is written do benefit billing, stamp*/
		/* duty, extra component processing and commission calculations.*/
		/* Call the benefit billing routine (NOW).*/
		if (isNE(t5534rec.subprog,SPACES) && !fmcOnFlag) {	//IBPLIFE-1379
			/*IVE-869 LIFE RUL Benefit Billing Calculation Rework on Contract Issue P5074AT - Integration with latest PA compatible models*/
			//callProgram(t5534rec.subprog, ubblallpar.ubblallRec);
			if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(t5534rec.subprog.toString()) && er.isExternalized(chdrlnbIO.cnttype.toString(), ubblallpar.crtable.toString())))
			{
				callProgramX(t5534rec.subprog, ubblallpar.ubblallRec); //ILIFE-7368
			}
			else
			{
				Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
				Vpxubblrec vpxubblrec = new Vpxubblrec();
				Vpmfmtrec vpmfmtrec = new Vpmfmtrec();
				vpmcalcrec.linkageArea.set(ubblallpar.ubblallRec);		
				//ubblallpar.sumins.format(ubblallpar.sumins.toString());
				vpxubblrec.function.set("INIT");
				callProgram(Vpxubbl.class, vpmcalcrec.vpmcalcRec,vpxubblrec);	
				ubblallpar.ubblallRec.set(vpmcalcrec.linkageArea);
				vpmfmtrec.initialize();
				vpmfmtrec.date01.set(ubblallpar.effdate);
				vpmfmtrec.previousSum.set(ZERO);
				vpmfmtrec.state.set(covrlnbIO.zclstate);
				vpmfmtrec.riskComDate.set(covrlnbIO.currfrom);
				mgmFeeFlag = FeaConfg.isFeatureExist(covtlnb.getChdrcoy().trim(),MGMFEE_ID, appVars, "IT");
				if(mgmFeeFlag){
					callProgram(t5534rec.subprog, vpmfmtrec, mgmFeeRec, vpxubblrec);
				}else{
					callProgram(t5534rec.subprog, vpmfmtrec,ubblallpar.ubblallRec,vpxubblrec); 
				}
				callProgram(Vpuubbl.class, vpmcalcrec.vpmcalcRec, vpmfmtrec);
				
				if(isEQ(ubblallpar.statuz,SPACES))
					ubblallpar.statuz.set(Varcom.oK);
			}
			/*IVE-795 RUL Product - Benefit Billing Calculation end*/
			
			t5534rec.t5534Rec.set(SPACES);
			if (isNE(ubblallpar.statuz,varcom.oK)) {
				syserrrec.params.set(ubblallpar.ubblallRec);
				syserrrec.statuz.set(ubblallpar.statuz);
				xxxxFatalError();
			}
		}
		subroutine2330();
		wsaaPlanSuffix.add(1);
	}

protected void calculateRiskPrem() {
	premiumrec.cnttype.set(chdrlnbIO.getCnttype());
	premiumrec.crtable.set(covtlnb.getCrtable());
	premiumrec.calcTotPrem.set(covtlnb.getInstprem());
	if((AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal("RISKPREMIUM")))
	{
		callProgram("RISKPREMIUM", premiumrec.premiumRec);
	}
	Rskppf rskppf = new Rskppf();
	rskppf = calculateDate(rskppf);
	readTr59x();
	if(isNE(tr59xrec.polfee, SPACES) && isNE(wsaapolFee, ZERO) && isNE("D", chdrlnbIO.getBillchnl())) {
		rskppf.setPolfee(wsaapolFee.getbigdata());
	}else {
		rskppf.setPolfee(BigDecimal.ZERO);
	}
	rskppf.setChdrcoy(covtlnb.getChdrcoy());
	rskppf.setChdrnum(covtlnb.getChdrnum());
	rskppf.setLife(covtlnb.getLife());
	rskppf.setCoverage(covtlnb.getCoverage());
	rskppf.setRider(covtlnb.getRider());
	rskppf.setCnttype(chdrlnbIO.getCnttype().toString());
	rskppf.setCrtable(covtlnb.getCrtable());
	rskppf.setInstprem(covtlnb.getInstprem());
	if(isEQ("D", chdrlnbIO.getBillchnl())) {
		rskppf.setRiskprem(BigDecimal.ZERO);
	}else {
		rskppf.setRiskprem(premiumrec.riskPrem.getbigdata());
	}
	insertRiskPremList.add(rskppf);

	if (insertRiskPremList != null && !insertRiskPremList.isEmpty()) rskppfDAO.insertRecords(insertRiskPremList);
}

protected void readTr59x() {
	List<Itempf> items = itemDAO.getAllItemitem("IT", covtlnb.getChdrcoy(), "TR59X", chdrlnbIO.getCnttype().toString());
	if(!ObjectUtils.isEmpty(items)){
		tr59xrec.tr59xrec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		readT5567();
	}else{
		tr59xrec.tr59xrec.set(SPACES);
	}
	
}

protected void readT5567() {
	chdrpf = chdrpfDAO.getchdrRecordData(covtlnb.getChdrcoy(),covtlnb.getChdrnum());
	t5567Map = itemDao.loadSmartTable("IT", covtlnb.getChdrcoy(), "T5567");
	wsaaCnttype.set(chdrlnbIO.getCnttype().toString());
	wsaaCntcurr.set(chdrpf.getCntcurr());
	if(t5567Map.containsKey(wsaaItemitem.toString())) {
		List<Itempf> t5567List = t5567Map.get(wsaaItemitem.toString());
        for(Itempf t5567Item: t5567List) {
             	t5567rec.t5567Rec.set(StringUtil.rawToString(t5567Item.getGenarea()));           
        }
	}else {
		t5567rec.t5567Rec.set(SPACES);
	}
	
	for (wsaaSub2.set(1); !(isGT(wsaaSub2,10)); wsaaSub2.add(1)){
		if (isEQ(t5567rec.billfreq[wsaaSub2.toInt()],chdrpf.getBillfreq())) {
			wsaapolFee.set(t5567rec.cntfee[wsaaSub2.toInt()]);
			wsaaSub2.set(11);
		}else {
			wsaapolFee.set(ZERO);
		}
	}
	
}

protected Rskppf calculateDate(Rskppf rskppf) {
	Calendar proposalDate = Calendar.getInstance();
    Calendar taxFromDate = Calendar.getInstance();
    Calendar taxToDate = Calendar.getInstance();
	try {
		proposalDate.setTime(dateFormat.parse(String.valueOf(datcon1rec.intDate.toInt())));
	    int month=proposalDate.get(Calendar.MONTH)+1;
	    if(month>=7) {
	    	taxFromDate.set(proposalDate.get(Calendar.YEAR), 6, 01);
	    	taxToDate.set(proposalDate.get(Calendar.YEAR)+1, 5, 30);
	    }
	    else { 
	    	taxFromDate.set(proposalDate.get(Calendar.YEAR)-1, 6, 01);
	    	taxToDate.set(proposalDate.get(Calendar.YEAR), 5, 30);
	    }
	    rskppf.setDatefrm(Integer.parseInt(dateFormat.format(taxFromDate.getTime())));
	    rskppf.setDateto(Integer.parseInt(dateFormat.format(taxToDate.getTime())));
	    rskppf.setEffDate(covtlnb.getEffdate());
	}
	catch (ParseException e) {		
		e.printStackTrace();
	}
	return rskppf;
}

protected void computeDates232d()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					optionalExtras2321();
				case readNextLext2323:
					readNextLext2323();
				case setBenBillDate2323:
					setBenBillDate2323();
					rerateDates2324();
					cpiDates2325();
					calcDate2327();
				case oxit232d:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void optionalExtras2321()
	{
		wsaaSpecTermsExist.set(SPACES);
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lextIO.setChdrnum(chdrlnbIO.getChdrnum());
		lextIO.setLife(covrlnbIO.getLife());
		lextIO.setCoverage(covrlnbIO.getCoverage());
		lextIO.setRider(covrlnbIO.getRider());
		lextIO.setSeqnbr(0);
		lextIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lextIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lextIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(lextIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isNE(lextIO.getLife(),covrlnbIO.getLife())
		|| isNE(lextIO.getCoverage(),covrlnbIO.getCoverage())
		|| isNE(lextIO.getRider(),covrlnbIO.getRider())) {
			lextIO.setStatuz(varcom.endp);
		}
		if (isEQ(lextIO.getStatuz(),varcom.endp)) {
			wsaaSpecTermsExist.set("N");
		}
		wsaaLextDate.set(varcom.vrcmMaxDate);
		while ( !(isEQ(lextIO.getStatuz(),varcom.endp)
		|| isNE(lextIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(lextIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isNE(lextIO.getCoverage(),covrlnbIO.getCoverage())
		|| isNE(lextIO.getRider(),covrlnbIO.getRider()))) {
			optionalExtraProcessingThru2322();
		}

		goTo(GotoLabel.setBenBillDate2323);
	}

protected void optionalExtraProcessing2322()
	{
		if (isEQ(lextIO.getExtCessTerm(),0)) {
			goTo(GotoLabel.readNextLext2323);
		}
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon2rec.freqFactor.set(lextIO.getExtCessTerm());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		if (isEQ(datcon2rec.intDate2,lextIO.getExtCessDate())) {
			goTo(GotoLabel.readNextLext2323);
		}
		lextIO.setExtCessDate(datcon2rec.intDate2);
		lextIO.setTermid(wsaaTermid);
		lextIO.setTransactionDate(wsaaTransactionDate);
		lextIO.setTransactionTime(wsaaTransactionTime);
		lextIO.setUser(wsaaUser);
		lextIO.setFormat(formatsInner.lextrec);
		lextIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void readNextLext2323()
	{
		if (isLT(lextIO.getExtCessDate(),wsaaLextDate)) {
			wsaaLextDate.set(lextIO.getExtCessDate());
		}
		lextIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lextIO);
		if (isNE(lextIO.getStatuz(),varcom.oK)
		&& isNE(lextIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lextIO.getParams());
			syserrrec.statuz.set(lextIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void setBenBillDate2323()
	{
		if (isEQ(t5687rec.bbmeth,SPACES)) {
			/*   MOVE VRCM-MAX-DATE        TO COVRLNB-BEN-CESS-DATE        */
			covrlnbIO.setBenBillDate(0);
		}
		else {
			/*   MOVE COVTLNB-PREM-CESS-DATE TO COVRLNB-BEN-CESS-DATE      */
			wsaaOccdate.set(chdrlnbIO.getOccdate());
			covrlnbIO.setBenBillDate(chdrlnbIO.getOccdate());
		}
	}

protected void rerateDates2324()
	{
		/*  Calculate the re-rate date. Add the re-rate frequency from  */
		/*  T5687 to the Contract Commencement date. If there is no     */
		/*  frequency specified then the re-rate date is the premium    */
		/*  cessation date for this component.                          */
		if (isNE(t5687rec.rtrnwfreq,0)) {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(t5687rec.rtrnwfreq);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				xxxxFatalError();
			}
			else {
				covrlnbIO.setRerateDate(datcon2rec.intDate2);
			}
		}
		else {
			if (isEQ(chdrlnbIO.getBillfreq(),"00")
			&& isNE(t5687rec.zsredtrm,"Y")) {
				covrlnbIO.setRerateDate(covrlnbIO.getRiskCessDate());
			}
			else {
				covrlnbIO.setRerateDate(covrlnbIO.getPremCessDate());
			}
		}
		/*  If a Special Term (LEXT) record expires before the re-rate  */
		/*  date then set the re-rate date to that instead.             */
		if (isLT(wsaaLextDate,covrlnbIO.getRerateDate())) {
			covrlnbIO.setRerateDate(wsaaLextDate);
		}
		/* If the rerate date is greater than the premium cessation date   */
		/* then set the re-rate date to the premium cessation date.        */
		if (isLT(covrlnbIO.getPremCessDate(), covrlnbIO.getRerateDate())) {
			covrlnbIO.setRerateDate(covrlnbIO.getPremCessDate());
		}
		/*  Set the date to use for the rates when re-rating.           */
		/*  If a minimum guarantee period is specified on T5687 then    */
		/*  use this to calculate the date that this runs to by         */
		/*  adding it to the Contract Commencement date.                */
		/*  If there is no minimum guarantee period then the rates      */
		/*  from the re-rate date will be used.                         */
		if (isEQ(t5687rec.premGuarPeriod,0)) {
			covrlnbIO.setRerateFromDate(covrlnbIO.getRerateDate());
		}
		else {
			datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(t5687rec.premGuarPeriod);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				xxxxFatalError();
			}
			/*  Check whether the re-rate date is within the minimum        */
			/*  guarantee period. If it is then use the rates from the      */
			/*  Contract Commencement date. If not use the rates from the   */
			/*  re-rate date.                                               */
			if (isLT(datcon2rec.intDate2,covrlnbIO.getRerateDate())) {
				covrlnbIO.setRerateFromDate(covrlnbIO.getRerateDate());
			}
			else {
				/* Use the calculated Rerate-From-Date rather than                 */
				/* Contract Commencement Date.                                     */
				/*         MOVE CHDRLNB-OCCDATE TO COVRLNB-RERATE-FROM-DATE<080>*/
				covrlnbIO.setRerateFromDate(datcon2rec.intDate2);
			}
		}

		/*1. create two columns in RACDPf table (RRTDAT(getRerateDate),RRTFRM (setRerateFromDate  ))*/
	/*	2. Here read all RACD records whose validflag is 3 and update these  two new fields with date from above covrlnbio
		racdpfList = racdpfDAO.searchRacdmjaResult(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(),covrlnbIO.getLife().toString(),covrlnbIO.getCoverage().toString(),covrlnbIO.getRider().toString(),covrlnbIO.getPlanSuffix().toString());
*/
		 racdpUpdate=racdpfDAO.updateRacd(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(),covrlnbIO.getLife().toString(),covrlnbIO.getCoverage().toString(),covrlnbIO.getRider().toString(),covrlnbIO.getPlanSuffix().toString(),covrlnbIO.getRerateDate().toInt(),covrlnbIO.getRerateFromDate().toInt(),"3");

			
		
	}

	/**
	* <pre>
	*2324-RERATE-DATES.
	*    MOVE CHDRLNB-OCCDATE        TO COVRLNB-RERATE-FROM-DATE.
	*    MOVE WSAA-LEXT-DATE         TO WSAA-RE-RATE-DATE.
	*    IF COVRLNB-PREM-CESS-DATE   <  WSAA-RE-RATE-DATE
	*       MOVE COVRLNB-PREM-CESS-DATE TO WSAA-RE-RATE-DATE.
	*    IF T5687-RTRNWFREQ     NOT  = 0
	*       MOVE CHDRLNB-OCCDATE     TO DTC2-INT-DATE-1
	*       MOVE '01'                TO DTC2-FREQUENCY
	*       MOVE T5687-RTRNWFREQ     TO DTC2-FREQ-FACTOR
	*       CALL 'DATCON2'           USING DTC2-DATCON2-REC
	*       IF DTC2-STATUZ           NOT = O-K
	*          MOVE DTC2-DATCON2-REC TO SYSR-PARAMS
	*          MOVE DTC2-STATUZ      TO SYSR-STATUZ
	*          PERFORM XXXX-FATAL-ERROR
	*       ELSE
	*          IF DTC2-INT-DATE-2    < WSAA-RE-RATE-DATE
	*             MOVE DTC2-INT-DATE-2 TO WSAA-RE-RATE-DATE,
	*                                  COVRLNB-RERATE-FROM-DATE.
	*    MOVE WSAA-RE-RATE-DATE      TO COVRLNB-RERATE-DATE.
	* </pre>
	*/
protected void cpiDates2325()
	{
		covrlnbIO.setCpiDate(99999999);
		inctIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		inctIO.setChdrnum(chdrlnbIO.getChdrnum());
		inctIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, inctIO);
		if (isNE(inctIO.getStatuz(),varcom.oK)
		&& isNE(inctIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(inctIO.getParams());
			syserrrec.statuz.set(inctIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(inctIO.getStatuz(),varcom.mrnf)
		|| isEQ(t5687rec.anniversaryMethod,SPACES)) {
			goTo(GotoLabel.oxit232d);
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItmfrm(covrlnbIO.getCrrcd());
		itdmIO.setItemtabl(tablesInner.t6658);
		itdmIO.setItemitem(t5687rec.anniversaryMethod);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isNE(itdmIO.getItemcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6658)
		|| isNE(itdmIO.getItemitem(),t5687rec.anniversaryMethod)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
			itdmIO.setItemtabl(tablesInner.t6658);
			itdmIO.setItmfrm(covrlnbIO.getCrrcd());
			itdmIO.setItemitem(t5687rec.anniversaryMethod);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(h036);
			xxxxFatalError();
		}
		t6658rec.t6658Rec.set(itdmIO.getGenarea());
		if (isEQ(wsaaSpecTermsExist,SPACES)
		&& isEQ(t6658rec.addexist,SPACES)) {
			goTo(GotoLabel.oxit232d);
		}
		if (isEQ(t6658rec.premsubr,SPACES)
		|| isEQ(t6658rec.billfreq,"00")) {
			goTo(GotoLabel.oxit232d);
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(covrlnbIO.getCrrcd());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(t6658rec.billfreq);
	}

protected void calcDate2327()
	{
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		covrlnbIO.setCpiDate(datcon2rec.intDate2);
		datcon3rec.intDate1.set(covrlnbIO.getCrrcd());
		datcon3rec.intDate2.set(covrlnbIO.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			xxxxFatalError();
		}
		datcon3rec.freqFactor.add(0.99999);
		wsaaMinTrmToCess.set(datcon3rec.freqFactor);
		/*    IF  T6658-MAX-AGE        NOT > 0                     <V73L03>*/
		/*        MOVE 99                 TO T6658-MAX-AGE.        <V73L03>*/
		/*                                                         <V73L03>*/
		/*    IF  T6658-MINCTRM            > WSAA-MIN-TRM-TO-CESS  <V73L03>*/
		/*    OR  T6658-MAX-AGE            < COVRLNB-ANB-AT-CCD    <V73L03>*/
		/*        MOVE 99999999               TO COVRLNB-CPI-DATE. <V73L03>*/
		if (isLTE(t6658rec.agemax, 0)) {
			/*        MOVE 99                 TO T6658-AGEMAX.         <V73L03>*/
			t6658rec.agemax.set(999);
		}
		if (isGT(t6658rec.minctrm,wsaaMinTrmToCess)
		|| isLT(t6658rec.agemax, covrlnbIO.getAnbAtCcd())) {
			covrlnbIO.setCpiDate(99999999);
		}
	}

protected void optionalExtraProcessingThru2322()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					optionalExtraProcessing2322();
				case readNextLext2323:
					readNextLext2323();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void deletAndReadCovt232e()
	{
		Covtpf covtlnbpf = new Covtpf(covtlnb);
		covtpfDAO.deleteCovtpfRecord(covtlnbpf);
		
		/* Keep the oldest cessation date.*/
		if (isLT(covtlnbpf.getPcesdte(),wsaaOldCessDate)) {
			wsaaOldCessDate.set(covtlnbpf.getPcesdte());
		}
		}



	/**
	* <pre>
	* Performed after each COVR is written.
	* </pre>
	*/
protected void subroutine2330()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					subroutine2331();
				case skipBonusWorkbench2333:
					skipBonusWorkbench2333();
					postSinglePremium233d();					
					postRegularPremium233d();
					postStampDuty233d();
				case premTax233d:
					premTax233d();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void subroutine2331()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrlnbIO.getChdrcoy());
		descIO.setDesctabl(tablesInner.t5645);
		descIO.setLanguage(atmodrec.language);
		descIO.setDescitem("P5074");
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			xxxxFatalError();
		}
		/*   If a description is not found in the signon language then*/
		/*   English is used as a default.*/
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLanguage("E");
			descIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, descIO);
			if (isNE(descIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				syserrrec.statuz.set(descIO.getStatuz());
				xxxxFatalError();
			}
		}
		/*                                                         <V4L001>*/
		lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
		stampDutyflag = FeaConfg.isFeatureExist(chdrlnbIO.getChdrcoy().toString().trim(), "NBPROP01", appVars, "IT");
		if(!stampDutyflag){
		stampDuty233b();
		}else{
			wsaaStampDutyAcc.add(covrlnbIO.getZstpduty01());
		}
		wsaaStampDutyComp.set(covrlnbIO.getZstpduty01());
		componentProcessing233c();
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.skipBonusWorkbench2333);
		}
		/* Bonus Workbench *                                               */
		/* Single Premium                                                  */
		if (isNE(covrlnbIO.getSingp(),0)) {
 //ILIFE-8709 start
			zptnIO = new Zptnpf();
			zptnIO.setChdrcoy(covrlnbIO.getChdrcoy().toString());
			zptnIO.setChdrnum(covrlnbIO.getChdrnum().toString());
			zptnIO.setLife(covrlnbIO.getLife().toString());
			zptnIO.setCoverage(covrlnbIO.getCoverage().toString());
			zptnIO.setRider(covrlnbIO.getRider().toString());
			zptnIO.setTranno(covrlnbIO.getTranno().toShort());
			zptnIO.setOrigamt(covrlnbIO.getSingp().getbigdata());
			zptnIO.setTransCode(wsaaBatckey.batcBatctrcde.toString());
			zptnIO.setEffdate(covrlnbIO.getCrrcd().toInt());
			zptnIO.setBillcd(covrlnbIO.getCrrcd().toInt());
			zptnIO.setInstfrom(covrlnbIO.getCrrcd().toInt());
			zptnIO.setInstto(covrlnbIO.getCrrcd().toInt());
			zptnIO.setTrandate(datcon1rec.intDate.toInt());
			zptnIO.setZprflg("S");
			insertZptnpfList.add(zptnIO);
			zptnpfDAO.insertZptnpf(insertZptnpfList);
 //ILIFE-8709 end
		}
		/* Regular Premium                                                 */
		/*  MOVE PAYR-BILLFREQ            TO WSAA-BILLFQ-9.      <LA5139>*/
		wsaaBillfq9.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
		if (isGT(wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()], wsaaBillfq9)) {
			compute(wsaaExcessInstNum, 0).set(sub(wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()], wsaaBillfq9));
			compute(wsaaRegPremFirst, 2).set(mult(covrlnbIO.getInstprem(),wsaaBillfq9));
			compute(wsaaRegPremRenewal, 2).set(mult(covrlnbIO.getInstprem(),wsaaExcessInstNum));
			initialize(datcon4rec.datcon4Rec);
			datcon4rec.freqFactor.set(1);
			datcon4rec.frequency.set("01");
			datcon4rec.intDate1.set(chdrlnbIO.getOccdate());
			/*     MOVE PAYR-BILLDAY        TO DTC4-BILLDAY          <LA5139>*/
			/*     MOVE PAYR-BILLMONTH      TO DTC4-BILLMONTH        <LA5139>*/
			datcon4rec.billday.set(wsaaBillingInformationInner.wsaaBillday[wsbbSub.toInt()]);
			datcon4rec.billmonth.set(wsaaBillingInformationInner.wsaaBillmonth[wsbbSub.toInt()]);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon4rec.statuz);
				syserrrec.params.set(datcon4rec.datcon4Rec);
				xxxxFatalError();
			}
			wsaaRegIntmDate.set(datcon4rec.intDate2);
		}
		else {
			compute(wsaaRegPremFirst, 2).set(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
			wsaaRegPremRenewal.set(ZERO);
			/*     MOVE PAYR-BTDATE         TO WSAA-REG-INTM-DATE    <LA5139>*/
			wsaaRegIntmDate.set(wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()]);
		}
		/* Regular Premium - First Year                                    */
		if (isNE(wsaaRegPremFirst,0)) {
 //ILIFE-8709 start
			zptnIO = new Zptnpf();
			zptnIO.setChdrcoy(covrlnbIO.getChdrcoy().toString());
			zptnIO.setChdrnum(covrlnbIO.getChdrnum().toString());
			zptnIO.setLife(covrlnbIO.getLife().toString());
			zptnIO.setCoverage(covrlnbIO.getCoverage().toString());
			zptnIO.setRider(covrlnbIO.getRider().toString());
			zptnIO.setTranno(covrlnbIO.getTranno().toInt());
			zptnIO.setOrigamt(wsaaRegPremFirst.getbigdata());
			zptnIO.setTransCode(wsaaBatckey.batcBatctrcde.toString());
			zptnIO.setEffdate(chdrlnbIO.getOccdate().toInt());
			zptnIO.setBillcd(chdrlnbIO.getOccdate().toInt());
			zptnIO.setInstfrom(chdrlnbIO.getOccdate().toInt());
			zptnIO.setInstto(wsaaRegIntmDate.toInt());
			zptnIO.setTrandate(datcon1rec.intDate.toInt());
			zptnIO.setZprflg("I");
			insertZptnpfList.add(zptnIO);
			zptnpfDAO.insertZptnpf(insertZptnpfList);
 //ILIFE-8709 end 
		}
		/* Regular Premium - Renewal Year                                  */
		if (isNE(wsaaRegPremRenewal,0)) {
 //ILIFE-8709 start
			zptnIO.setChdrcoy(covrlnbIO.getChdrcoy().toString());
			zptnIO.setChdrnum(covrlnbIO.getChdrnum().toString());
			zptnIO.setLife(covrlnbIO.getLife().toString());
			zptnIO.setCoverage(covrlnbIO.getCoverage().toString());
			zptnIO.setRider(covrlnbIO.getRider().toString());
			zptnIO.setTranno(covrlnbIO.getTranno().toInt());
			zptnIO.setOrigamt(wsaaRegPremRenewal.getbigdata());
			zptnIO.setTransCode(wsaaBatckey.batcBatctrcde.toString());
			zptnIO.setBillcd(chdrlnbIO.getOccdate().toInt());
			zptnIO.setEffdate(wsaaRegIntmDate.toInt());
			zptnIO.setInstfrom(wsaaRegIntmDate.toInt());
			/*     MOVE PAYR-BTDATE         TO ZPTN-INSTTO           <LA5139>*/
			zptnIO.setInstto(wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].toInt());
			zptnIO.setTrandate(datcon1rec.intDate.toInt());
			zptnIO.setZprflg("R");
			insertZptnpfList.add(zptnIO);
			zptnpfDAO.insertZptnpf(insertZptnpfList);
 //ILIFE-8709 end
		}
	}

protected void skipBonusWorkbench2333()
	{
		/*                                                      <R96REA>*/
		/*Introducing Reassurance RACT record processing         <R96REA>*/
		/*                                                      <R96REA>*/
		/*  Activate Reassurance Cessions (RACD records) if require        */
		checkForReassurance4000();
		/* We need the component premiums to do the tax posting            */
		/* move the skiping of the component posting further down this     */
		/* section.                                                        */
		/* IF NOT COMP-LEVEL-ACC                                        */
		/*     GO TO 2339-BEGIN-PCCD.                                   */
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(atmodrec.batchKey);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(chdrlnbIO.getOccdate());
		setlifeacmvCustomerSpecific();
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		wsaaJrnseq.set(0);
		wsaaRldgChdrnum.set(covrlnbIO.getChdrnum());
		wsaaRldgLife.set(covrlnbIO.getLife());
		wsaaRldgCoverage.set(covrlnbIO.getCoverage());
		wsaaRldgRider.set(covrlnbIO.getRider());
		wsaaPlan.set(covrlnbIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		/* If component level account applicable,*/
		/* do the premium postings for this component.*/
		/* Store premiums.*/
		wsaaCompSingPrem.set(covrlnbIO.getSingp());
		wsaaCompRegPrem.set(covrlnbIO.getInstprem());
		/* Adjust Regular Premium (WSBB-SUB will still be set as the*/
		/* payor for this component).*/
		compute(wsaaCompRegPrem, 2).set(mult(wsaaCompRegPrem, wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
		lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
		/* skip the posting of accounts at this level if we have a         */
		/* contract level posting.                                         */
		/* redirect to post the premium tax before checking PCDD           */
		if (!compLevelAcc.isTrue()) {
			goTo(GotoLabel.premTax233d);
		}
	}

protected void postSinglePremium233d()
	{
		if (isNE(wsaaCompSingPrem,0)) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[4]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[4]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[4]);
			lifacmvrec.glsign.set(wsaaT5645Sign[4]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[4]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			lifacmvrec.origamt.set(wsaaCompSingPrem);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		 }
			
	}

protected void postStampDuty233d() {
	
	if(stampDutyFlagcomp && isNE(wsaaStampDutyComp,0) && (isNE(wsaaCompSingPrem, 0) || isNE(wsaaCompRegPrem,0))) {
		
		lifacmvrec.sacscode.set(t5645rec.sacscode06);
		lifacmvrec.sacstyp.set(t5645rec.sacstype06);
		lifacmvrec.glcode.set(t5645rec.glmap06);
		lifacmvrec.glsign.set(t5645rec.sign06);
		lifacmvrec.contot.set(t5645rec.cnttot06);
		lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(wsaaStampDutyComp);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		lifacmvCheck2950();
		lifacmvrec.sacscode.set(t5645rec.sacscode07);
		lifacmvrec.sacstyp.set(t5645rec.sacstype07);
		lifacmvrec.glcode.set(t5645rec.glmap07);
		lifacmvrec.glsign.set(t5645rec.sign07);
		lifacmvrec.contot.set(t5645rec.cnttot07);
		lifacmvrec.origamt.set(wsaaStampDutyComp);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		lifacmvCheck2950();
 }
}

protected void postRegularPremium233d()
	{
		if (isNE(wsaaCompRegPrem,0)) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[5]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[5]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[5]);
			lifacmvrec.glsign.set(wsaaT5645Sign[5]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[5]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			lifacmvrec.origamt.set(wsaaCompRegPrem);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
	}

protected void premTax233d()
	{
		if (isNE(covrlnbIO.getSingp(), ZERO)
		|| isNE(covrlnbIO.getInstprem(), ZERO)) {
			if (isNE(tr52drec.txcode, SPACES)) {
				premTax233g();
			}
		}
		/*BEGIN-PCCD*/
		/* Begin PCDDLNB file.*/
		pcddlnbIO.setParams(SPACES);
		pcddlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		pcddlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		pcddlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		pcddlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		pcddlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		wsaaAgentSub.set(1);
		while ( !(isNE(pcddlnbIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(pcddlnbIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isEQ(pcddlnbIO.getStatuz(),varcom.endp))) {
			commissionAgent233d();
		}

		/*EXIT*/
	}

protected void benefitBilling233a()
	{
		try {
			benefitBilling2331();
			callSubroutine2334();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void benefitBilling2331()
	{
		/* No need to check the frequency of the contract as we now have*/
		/* a separate field for Single and Regular Prems. The correct one*/
		/* will have been filled in at New Business!*/
		/*    Accumulate the Premiums.*/
		wsbbSub.set(covrlnbIO.getPayrseqno());
		wsaaBillingInformationInner.wsaaSingPremAcc[wsbbSub.toInt()].add(covrlnbIO.getSingp());
		wsaaBillingInformationInner.wsaaRegPremAcc[wsbbSub.toInt()].add(covrlnbIO.getInstprem());
		/* If the benefit billing method is blank there will be no*/
		/* benefit billing subroutine to call so we've finished!*/
		if (isEQ(t5687rec.bbmeth,SPACES)) {
			goTo(GotoLabel.exit233a);
		}
	}

protected void callSubroutine2334()
	{
		/* Fetch relevent billing subroutine.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5534);
		itemIO.setItemitem(t5687rec.bbmeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5534rec.t5534Rec.set(itemIO.getGenarea());
		if (isEQ(t5534rec.subprog,SPACES)) {
			return ;
		}
		ubblallpar.ubblallRec.set(SPACES);
		ubblallpar.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		ubblallpar.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		ubblallpar.lifeLife.set(covrlnbIO.getLife());
		ubblallpar.lifeJlife.set(covrlnbIO.getJlife());
		ubblallpar.covrCoverage.set(covrlnbIO.getCoverage());
		ubblallpar.covrRider.set(covrlnbIO.getRider());
		ubblallpar.planSuffix.set(covrlnbIO.getPlanSuffix());
		ubblallpar.billfreq.set(t5534rec.unitFreq);
		ubblallpar.cntcurr.set(chdrlnbIO.getCntcurr());
		ubblallpar.cnttype.set(chdrlnbIO.getCnttype());
		ubblallpar.tranno.set(chdrlnbIO.getTranno());
		ubblallpar.effdate.set(covrlnbIO.getCrrcd());
		ubblallpar.premMeth.set(t5534rec.premmeth);
		ubblallpar.jlifePremMeth.set(t5534rec.jlPremMeth);
		ubblallpar.sumins.set(covrlnbIO.getSumins());
		ubblallpar.premCessDate.set(covrlnbIO.getPremCessDate());
		ubblallpar.crtable.set(covrlnbIO.getCrtable());
		ubblallpar.billchnl.set(covtlnb.getBillchnl());
		ubblallpar.mortcls.set(covtlnb.getMortcls());
		ubblallpar.svMethod.set(t5534rec.svMethod);
		ubblallpar.language.set(atmodrec.language);
		ubblallpar.user.set(wsaaUser);
		ubblallpar.batccoy.set(wsaaBatckey.batcBatccoy);
		ubblallpar.batcbrn.set(wsaaBatckey.batcBatcbrn);
		ubblallpar.batcactyr.set(wsaaBatckey.batcBatcactyr);
		ubblallpar.batcactmn.set(wsaaBatckey.batcBatcactmn);
		ubblallpar.batctrcde.set(wsaaBatckey.batcBatctrcde);
		ubblallpar.batch.set(wsaaBatckey.batcBatcbatch);
		ubblallpar.tranno.set(chdrlnbIO.getTranno());
		ubblallpar.adfeemth.set(t5534rec.adfeemth);
		ubblallpar.function.set("ISSUE");
		ubblallpar.polsum.set(chdrlnbIO.getPolsum());
		ubblallpar.ptdate.set(chdrlnbIO.getPtdate());
		ubblallpar.polinc.set(chdrlnbIO.getPolinc());
		ubblallpar.singp.set(ZERO);
		ubblallpar.occdate.set(chdrlnbIO.getOccdate());
		ubblallpar.chdrRegister.set(chdrlnbIO.getRegister());
		//ALS-4706
		mgmFeeFlag = FeaConfg.isFeatureExist(covtlnb.getChdrcoy().trim(),MGMFEE_ID, appVars, "IT");
		if(mgmFeeFlag){
		initMgmFeeRec();
		}
		/* Get the next benefit billing date.*/
		/*    MOVE SPACE                  TO DTC2-DATCON2-REC.             */
		/*    MOVE T5534-UNIT-FREQ        TO DTC2-FREQUENCY.               */
		/*    MOVE 1                      TO DTC2-FREQ-FACTOR.             */
		/*    MOVE COVRLNB-CRRCD          TO DTC2-INT-DATE-1.              */
		/*    MOVE 0                      TO DTC2-INT-DATE-2.              */
		/*    CALL 'DATCON2'              USING DTC2-DATCON2-REC.          */
		/*    IF DTC2-STATUZ              NOT = O-K                        */
		/*       MOVE DTC2-DATCON2-REC    TO SYSR-PARAMS                   */
		/*       MOVE DTC2-STATUZ         TO SYSR-STATUZ                   */
		/*       PERFORM XXXX-FATAL-ERROR.                                 */
		/*    MOVE DTC2-INT-DATE-2        TO COVRLNB-BEN-BILL-DATE.        */
		if(!fmcOnFlag && !unitLinked){
			initialize(datcon4rec.datcon4Rec);
			datcon4rec.frequency.set(t5534rec.unitFreq);
			datcon4rec.freqFactor.set(1);
			datcon4rec.intDate1.set(covrlnbIO.getCrrcd());
			datcon4rec.intDate2.set(0);
			datcon4rec.billdayNum.set(wsaaOccDd);
			datcon4rec.billmonthNum.set(wsaaOccMm);
			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
			if (isNE(datcon4rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon4rec.datcon4Rec);
				syserrrec.statuz.set(datcon4rec.statuz);
				xxxxFatalError();
			}
			covrlnbIO.setBenBillDate(datcon4rec.intDate2);
		}
		else{
			covrlnbIO.setBenBillDate(covrlnbIO.getCrrcd());
		}
	}

protected void stampDuty233b()
	{
			stampDuty2331();
		}

protected void stampDuty2331()
	{
		/* If no stamp duty then exit otherwise call the stamp duty*/
		/* calculation subroutine.*/
		if (isEQ(t5687rec.stampDutyMeth,SPACES)) {
			return ;
		}
		/* Fetch relevent stamp duty subroutine.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5676);
		itemIO.setItemitem(t5687rec.stampDutyMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5676rec.t5676Rec.set(itemIO.getGenarea());
		if (isEQ(t5676rec.subprog,SPACES)) {
			return ;
		}
		stdtallrec.company.set(chdrlnbIO.getChdrcoy());
		stdtallrec.chdrnum.set(chdrlnbIO.getChdrnum());
		stdtallrec.life.set(covrlnbIO.getLife());
		stdtallrec.coverage.set(covrlnbIO.getCoverage());
		stdtallrec.rider.set(covrlnbIO.getRider());
		stdtallrec.plnsfx.set(covrlnbIO.getPlanSuffix());
		stdtallrec.cntcurr.set(chdrlnbIO.getCntcurr());
		stdtallrec.effdate.set(covrlnbIO.getCrrcd());
		stdtallrec.stampDuty.set(0);
		callProgram(t5676rec.subprog, stdtallrec.stdt001Rec);
		if (isNE(stdtallrec.statuz,varcom.oK)) {
			syserrrec.params.set(stdtallrec.stdt001Rec);
			syserrrec.statuz.set(stdtallrec.statuz);
			xxxxFatalError();
		}
		zrdecplcPojo.setAmountIn(stdtallrec.stampDuty.getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		wsaaStampDutyAcc.add(zrdecplcPojo.getAmountOut().doubleValue()); //ILIFE-8709
	}

	/**
	* <pre>
	* Lookup the generic component processing programs (T5671)
	* and call each non blank subroutine.
	* </pre>
	*/
protected void componentProcessing233c()
	{
		try {
			componentProcessingPara233c();
			subCall2332();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void componentProcessingPara233c()
	{
		/*2331-COMPONENT-PROCESSING.                                       */
		wsaaCompkeyTranno.set(wsaaBatckey.batcBatctrcde);
		wsaaCompkeyCrtable.set(covrlnbIO.getCrtable());
		/* Fetch coverage/rider generic component processing progs.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setItemitem(wsaaCompkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit233c);
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		isuallrec.company.set(chdrlnbIO.getChdrcoy());
		isuallrec.chdrnum.set(chdrlnbIO.getChdrnum());
		isuallrec.life.set(covrlnbIO.getLife());
		wsaaLifeNum.set(covrlnbIO.getLife());
		isuallrec.coverage.set(covrlnbIO.getCoverage());
		wsaaCoverageNum.set(covrlnbIO.getCoverage());
		isuallrec.rider.set(covrlnbIO.getRider());
		isuallrec.planSuffix.set(covrlnbIO.getPlanSuffix());
		wsbbSub.set(covrlnbIO.getPayrseqno());
		isuallrec.freqFactor.set(wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]);
		isuallrec.batchkey.set(atmodrec.batchKey);
		isuallrec.transactionDate.set(wsaaTransactionDate);
		isuallrec.transactionTime.set(wsaaTransactionTime);
		isuallrec.user.set(wsaaUser);
		isuallrec.termid.set(wsaaTermid);
		isuallrec.convertUnlt.set(wsaaNoSummaryRec);
		isuallrec.covrInstprem.set(wsaaCovtInstprem[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()]);
		isuallrec.covrSingp.set(wsaaCovtSingp[wsaaLifeNum.toInt()][wsaaCoverageNum.toInt()]);
		isuallrec.effdate.set(covrlnbIO.getCrrcd());
		/*    This new field, ISUA-NEW-TRANNO, is NOT used for issue.      */
		/*     just initialise to ZEROS. It is only used by the            */
		/*     subroutine GRVULCHG when trying to Reverse INCI records     */
		/*     affected by a Component Modify.                             */
		isuallrec.newTranno.set(ZERO);
		isuallrec.function.set(SPACES);
		isuallrec.oldcovr.set(SPACES);
		isuallrec.oldrider.set(SPACES);
		/*  Pass the Language field to the Issue Subroutine.            */
		isuallrec.language.set(atmodrec.language);
		isuallrec.runDate.set(covrlnbIO.getCrrcd());
		for (wsaaSub.set(1); !(isGT(wsaaSub,3)); wsaaSub.add(1)){
			subCall2332();
		}
	}

	/**
	* <pre>
	* Do the subroutine calls.
	* </pre>
	*/
protected void subCall2332()
	{
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			xxxxFatalError();
		}
		isuallrec.statuz.set(varcom.oK);
		if (isNE(t5671rec.subprog[wsaaSub.toInt()],SPACES)) {
			callProgram(t5671rec.subprog[wsaaSub.toInt()], isuallrec.isuallRec);
			if (isNE(isuallrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(isuallrec.statuz);
				syserrrec.params.set(isuallrec.isuallRec);
				xxxxFatalError();
			}
		}
		if (isNE(isuallrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(isuallrec.statuz);
			syserrrec.params.set(isuallrec.isuallRec);
			xxxxFatalError();
		}
		wsaaNoSummaryRec.set(SPACES);
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void commissionAgent233d()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					commissionAgent2331();
					readAgentFile2331();
					initialiseLinkage2332();
					regularPremium233d();
					n5a233d();
				case n5b233d:
					n5b233d();
				case n5c233d:
					n5c233d();
				case n5cSkip233d:
					n5cSkip233d();
					n5d233d();
				case n5e233d:
					n5e233d();
				case n5eSkip233d:
					n5eSkip233d();
				case n5f233d:
					n5f233d();
				case singlePremium233d:
					singlePremium233d();
					n6a233d();
					n6b233d();
				case n6c233d:
					n6c233d();
				case n6cSkip233d:
					n6cSkip233d();
					n6d233d();
				case readNext2332:
					readNext2332();
				case exit233d:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void commissionAgent2331()
	{
		/* Zeroise values for this coverage/agent combination.*/
		wsaaCompSingPrem.set(ZERO);
		wsaaCompRegPrem.set(ZERO);
		wsaaCompErnIcomm.set(ZERO);
		wsaaCompAdvIcomm.set(ZERO);
		wsaaCompErnScomm.set(ZERO);
		wsaaCompErnRcomm.set(ZERO);
		wsaaCompErnOcomm.set(ZERO);
		wsaaCompAdvOcomm.set(ZERO);
		wsaaComtotKept.set(ZERO);
		wsaaCompayKept.set(ZERO);
		wsaaComernKept.set(ZERO);
		wsaaOvrtimes.set(ZERO);
		if(gstOnCommFlag) {
			wsaaCompErnInitCommGst.set(ZERO);			
			wsaaCompErnRnwlCommGst.set(ZERO);			
		}
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(atmodrec.batchKey);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(chdrlnbIO.getOccdate());
		setlifeacmvCustomerSpecific();
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		wsaaJrnseq.set(0);
		wsaaRldgChdrnum.set(covrlnbIO.getChdrnum());
		wsaaRldgLife.set(covrlnbIO.getLife());
		wsaaRldgCoverage.set(covrlnbIO.getCoverage());
		wsaaRldgRider.set(covrlnbIO.getRider());
		wsaaPlan.set(covrlnbIO.getPlanSuffix());
		wsaaRldgPlanSuffix.set(wsaaPlansuff);
		lifacmvrec.rldgacct.set(wsaaRldgacct);
		SmartFileCode.execute(appVars, pcddlnbIO);
		if (isNE(pcddlnbIO.getStatuz(),varcom.oK)
		&& isNE(pcddlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(pcddlnbIO.getParams());
			syserrrec.statuz.set(pcddlnbIO.getStatuz());
			xxxxFatalError();
		}
		if (isNE(pcddlnbIO.getChdrcoy(),chdrlnbIO.getChdrcoy())
		|| isNE(pcddlnbIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isEQ(pcddlnbIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit233d);
		}
	}

protected void readAgentFile2331()
	{
		/* Read agent file.*/
 //ILIFE-8709 start
		aglfpf = aglfpfDAO.searchAglflnb(pcddlnbIO.getChdrcoy().toString(), pcddlnbIO.getAgntnum().toString());
		if (aglfpf == null) {
			aglfpf = new Aglfpf();
			aglfpf.setOvcpc(BigDecimal.ZERO);
		}
		wsaaStoredAgentClass.set(aglfpf.getAgentClass());
		 //ILIFE-8709 end
	}

protected void initialiseLinkage2332()
	{
		comlinkrec.chdrcoy.set(pcddlnbIO.getChdrcoy());
		comlinkrec.chdrnum.set(pcddlnbIO.getChdrnum());
		comlinkrec.life.set(covrlnbIO.getLife());
		comlinkrec.coverage.set(covrlnbIO.getCoverage());
		comlinkrec.effdate.set(chdrlnbIO.getOccdate());
		comlinkrec.currto.set(covrlnbIO.getCurrto());
		comlinkrec.rider.set(covrlnbIO.getRider());
		comlinkrec.planSuffix.set(covrlnbIO.getPlanSuffix());
		comlinkrec.agent.set(pcddlnbIO.getAgntnum());
		comlinkrec.jlife.set(covrlnbIO.getJlife());
		comlinkrec.crtable.set(covrlnbIO.getCrtable());
		comlinkrec.agentClass.set(wsaaStoredAgentClass);
		comlinkrec.icommtot.set(0);
		comlinkrec.icommpd.set(0);
		comlinkrec.icommernd.set(0);
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		comlinkrec.instprem.set(0);
		comlinkrec.ptdate.set(0);
		comlinkrec.annprem.set(0);
		comlinkrec.targetPrem.set(0);
		wsaaClnkAnnprem.set(0);
		comlinkrec.commprem.set(0);
		comlinkrec.billfreq.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
		comlinkrec.seqno.set(1);
		wsaaAgentsInner.wsaaAgntnum[wsaaAgentSub.toInt()].set(pcddlnbIO.getAgntnum());
		if(gstOnCommFlag) {
			comlinkrec.gstAmount.set(0);
		}
	}

protected void regularPremium233d()
	{
		/* Need to process both Regular and Single Premium commission*/
		/* using different methods from T5687. They may, in some cases,*/
		/* both appear on the same coverage!!!*/
		/* We will deal with the Regular bit first.*/
		if (isEQ(covrlnbIO.getInstprem(),0)) {
			goTo(GotoLabel.singlePremium233d);
		}
		wsbbSub.set(covrlnbIO.getPayrseqno());
		wsaaBillfreq9.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
		/*    COMPUTE CLNK-ANNPREM ROUNDED                                 */
		if (isNE(t5687rec.zrrcombas,SPACES)) {
			compute(wsaaClnkAnnprem, 5).setRounded(mult(mult(covrlnbIO.getZbinstprem(),wsaaBillfreq9),(div(pcddlnbIO.getSplitBcomm(),100))));
			if (isNE(th605rec.bonusInd,"Y")) {
				compute(wsaaZctnAnnprem, 3).setRounded(mult(covrlnbIO.getZbinstprem(),wsaaBillfreq9));
			}
		}
		else {
			if (isNE(th605rec.bonusInd,"Y")) {
				compute(wsaaZctnAnnprem, 3).setRounded(mult(covrlnbIO.getInstprem(),wsaaBillfreq9));
			}
			compute(wsaaClnkAnnprem, 5).setRounded(mult(mult(covrlnbIO.getInstprem(),wsaaBillfreq9),(div(pcddlnbIO.getSplitBcomm(),100))));
		}
		/* The following Compute statement is used to round the contents   */
		/* of WSAA-CLNK-ANNPREM and move them into CLNK-ANNPREM.           */
		compute(comlinkrec.annprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
		
		
		if(isNE(covrlnbIO.getCommPrem(),ZERO)){
			comlinkrec.commprem.set(covrlnbIO.getCommPrem());  //IBPLIFE-5237
		}
		
		if (isNE(t5687rec.zrrcombas,SPACES)) {
			compute(comlinkrec.instprem, 3).setRounded(mult(mult(covrlnbIO.getZbinstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]), (div(pcddlnbIO.getSplitBcomm(), 100))));
			if (isNE(th605rec.bonusInd,"Y")) {
				compute(wsaaZctnInstprem, 3).setRounded(mult(covrlnbIO.getZbinstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
			}
		}
		else {
			if (isNE(th605rec.bonusInd,"Y")) {
				compute(wsaaZctnInstprem, 3).setRounded(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
			}
			compute(comlinkrec.instprem, 3).setRounded(mult(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]), (div(pcddlnbIO.getSplitBcomm(), 100))));
		}
		/*  IF T5687-BASIC-COMM-METH    = SPACES                         */
		/*     GO TO 233D-5B.                                            */
		if (isEQ(wsaaBasicCommMeth,SPACES)) {
			goTo(GotoLabel.n5b233d);
		}
	}

protected void n5a233d()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5647);
		/*  MOVE T5687-BASIC-COMM-METH  TO ITEM-ITEMITEM,                */
		/*                                 AGCM-BASIC-COMM-METH.         */
		itemIO.setItemitem(wsaaBasicCommMeth);
		agcmIO.setBasicCommMeth(wsaaBasicCommMeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5647rec.t5647Rec.set(itemIO.getGenarea());
		/*  MOVE T5687-BASIC-COMM-METH  TO CLNK-METHOD.                  */
		comlinkrec.method.set(wsaaBasicCommMeth);
		if (isNE(t5647rec.commsubr,SPACES)) {
			callProgram(t5647rec.commsubr, comlinkrec.clnkallRec);
			/*    IF CLNK-STATUZ         NOT  = O-K                         */
			if (isNE(comlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				/*       MOVE CLNK-STATUZ         TO SYSR-STATUZ                */
				syserrrec.statuz.set(comlinkrec.statuz);
				xxxxFatalError();
			}
		}
	}

protected void n5b233d()
	{
		/*  IF  T5687-BASCPY            = SPACES                         */
		/*  AND AGLFLNB-BCMTAB          = SPACES                         */
		/*      GO TO 233D-5C.                                           */
		if (isEQ(wsaaBascpy,SPACES)
		&& isEQ(aglfpf.getBcmtab(),SPACES)) {
			goTo(GotoLabel.n5c233d);
		}
		/*                                                         <D9604>*/
		/* Dont use AGLF if Flexible Premium contract *            <D9604>*/
		/*  IF T5687-BASCPY             = SPACES                 <V42004>*/
		/*     AND FLEXIBLE-PREMIUM-CONTRACT                     <V42004>*/
		/*     GO TO 233D-5C.                                    <V42004>*/
		if (isEQ(wsaaBascpy,SPACES)
		&& flexiblePremiumContract.isTrue()) {
			goTo(GotoLabel.n5c233d);
		}
		/*                                                         <D9604>*/
		/* Set up target and period for Flexible Prem              <D9604>*/
		/* Contracts                                               <D9604>*/
		/*                                                         <D9604>*/
		compute(comlinkrec.targetPrem, 3).setRounded(mult(covrlnbIO.getInstprem(),wsaaBillfreq9));
		comlinkrec.currto.set(chdrlnbIO.getOccdate());
		if (flexiblePremiumContract.isTrue()) {
			comlinkrec.instprem.set(0);
		}
		/*                                                         <D9604>*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5644);
		/*  IF T5687-BASCPY             = SPACES                         */
		if (isEQ(wsaaBascpy,SPACES)) {
			itemIO.setItemitem(aglfpf.getBcmtab()); //ILIFE-8709
			comlinkrec.method.set(aglfpf.getBcmtab()); //ILIFE-8709
			agcmIO.setBascpy(aglfpf.getBcmtab()); //ILIFE-8709
		}
		else {
			/*     MOVE T5687-BASCPY     TO ITEM-ITEMITEM                    */
			/*                              AGCM-BASCPY                      */
			/*                              CLNK-METHOD                      */
			itemIO.setItemitem(wsaaBascpy);
			agcmIO.setBascpy(wsaaBascpy);
			comlinkrec.method.set(wsaaBascpy);
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isNE(t5644rec.compysubr,SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			/*    IF CLNK-STATUZ          NOT = O-K                         */
			if (isNE(comlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				/*       MOVE CLNK-STATUZ         TO SYSR-STATUZ                */
				syserrrec.statuz.set(comlinkrec.statuz);
				xxxxFatalError();
			}
		}
 //ILIFE-8709 start
		zrdecplcPojo.setAmountIn(comlinkrec.payamnt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.payamnt.set(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setAmountIn(comlinkrec.icommtot.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.icommtot.set(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setAmountIn(comlinkrec.erndamt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.erndamt.set(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
	}

	/**
	* <pre>
	* Accumulate the commission amounts.
	* </pre>
	*/
protected void n5c233d()
	{
		wsaaAgentsInner.wsaaCommDue[wsaaAgentSub.toInt()].add(comlinkrec.payamnt);
		wsaaAgentsInner.wsaaAnnprem[wsaaAgentSub.toInt()].set(comlinkrec.annprem);
		compute(wsaaAgentsInner.wsaaCommPaid[wsaaAgentSub.toInt()], 3).setRounded(add(wsaaAgentsInner.wsaaCommPaid[wsaaAgentSub.toInt()], (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
		wsaaAgentsInner.wsaaCommEarn[wsaaAgentSub.toInt()].add(comlinkrec.erndamt);
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.n5cSkip233d);
		}
		/* Bonus Workbench *                                               */
		if (isNE(comlinkrec.payamnt,ZERO)) {
 //ILIFE-8709 start
			zctnpf = new Zctnpf();
			zctnpf.setAgntcoy(pcddlnbIO.getChdrcoy().toString());
			zctnpf.setAgntnum(pcddlnbIO.getAgntnum().toString());
			zctnpf.setCommAmt(comlinkrec.payamnt.getbigdata());
			if (isGT(comlinkrec.instprem,comlinkrec.annprem)) {
				zctnpf.setPremium(wsaaZctnAnnprem.getbigdata());
			}
			else {
				zctnpf.setPremium(wsaaZctnInstprem.getbigdata());
			}
			zctnpf.setSplitBcomm(pcddlnbIO.getSplitBcomm().getbigdata());
			zctnpf.setZprflg("I");
			zctnpf.setChdrcoy(comlinkrec.chdrcoy.toString());
			zctnpf.setChdrnum(comlinkrec.chdrnum.toString());
			zctnpf.setLife(comlinkrec.life.toString());
			zctnpf.setCoverage(comlinkrec.coverage.toString());
			zctnpf.setRider(comlinkrec.rider.toString());
			zctnpf.setTranno(covrlnbIO.getTranno().toInt());
			zctnpf.setTransCode(wsaaBatckey.batcBatctrcde.toString());
			zctnpf.setEffdate(comlinkrec.effdate.toInt());
			zctnpf.setTrandate(datcon1rec.intDate.toInt());
			insertZctnpfList.add(zctnpf);
			zctnpfDAO.insertZctnpf(insertZctnpfList);
 //ILIFE-8709 end
		}
	}

protected void n5cSkip233d()
	{
		/* Set values on AGCM.*/
		agcmIO.setInitcom(comlinkrec.icommtot);
		agcmIO.setCompay(comlinkrec.erndamt);// ILIFE-8392
		agcmIO.setComern(comlinkrec.erndamt);
		/* Accumulate component level initial commission values for*/
		/* later posting.*/
		if (compLevelAcc.isTrue()) {
			wsaaCompErnIcomm.add(comlinkrec.erndamt);
			comlinkrec.initcommpay.set(comlinkrec.erndamt); // ILIFE-8392
			
			if(gstOnCommFlag) {
				compute(wsaaCompErnInitCommGst, 3).setRounded(add(wsaaCompErnInitCommGst, comlinkrec.gstAmount));
				agcmIO.setInitCommGst(wsaaCompErnInitCommGst);
				compute(wsaaCompAdvIcomm, 3).setRounded(add(wsaaCompAdvIcomm,(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt))));//ILIFE-7965	
			}
			else{
				compute(wsaaCompAdvIcomm, 3).setRounded(add(wsaaCompAdvIcomm,(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt))));//ILIFE-7965	
			}
		}
	}

	/**
	* <pre>
	* Servicing commission.
	* </pre>
	*/
protected void n5d233d()
	{
		agcmIO.setScmearn(0);
		agcmIO.setScmdue(0);
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5644);
		/*  IF T5687-SRVCPY             = SPACES                         */
		if (isEQ(wsaaSrvcpy,SPACES)) {
			itemIO.setItemitem(aglfpf.getScmtab()); //ILIFE-8709
			agcmIO.setSrvcpy(aglfpf.getScmtab()); //ILIFE-8709
		}
		else {
			/*     MOVE T5687-SRVCPY        TO ITEM-ITEMITEM                 */
			itemIO.setItemitem(wsaaSrvcpy);
			agcmIO.setSrvcpy(wsaaSrvcpy);
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			goTo(GotoLabel.n5e233d);
		}
		/*  No servicing commission for flexible premiums at issue <D9604>*/
		if (flexiblePremiumContract.isTrue()) {
			goTo(GotoLabel.n5e233d);
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.n5e233d);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		initialiseLinkage2332();
		wsbbSub.set(covrlnbIO.getPayrseqno());
		wsaaBillfreq9.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
		/*    COMPUTE CLNK-ANNPREM ROUNDED                                 */
		if (isNE(t5687rec.zrrcombas,SPACES)) {
			compute(wsaaClnkAnnprem, 5).setRounded(mult(mult(covrlnbIO.getZbinstprem(),wsaaBillfreq9),(div(pcddlnbIO.getSplitBcomm(),100))));
		}
		else {
			compute(wsaaClnkAnnprem, 5).setRounded(mult(mult(covrlnbIO.getInstprem(),wsaaBillfreq9),(div(pcddlnbIO.getSplitBcomm(),100))));
		}
		/* The following Compute statement is used to round the contents   */
		/* of WSAA-CLNK-ANNPREM and move them into CLNK-ANNPREM.           */
		compute(comlinkrec.annprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
		if (isNE(t5687rec.zrrcombas,SPACES)) {
			compute(comlinkrec.instprem, 3).setRounded(mult(mult(covrlnbIO.getZbinstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]), (div(pcddlnbIO.getSplitBcomm(), 100))));
		}
		else {
			compute(comlinkrec.instprem, 3).setRounded(mult(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]), (div(pcddlnbIO.getSplitBcomm(), 100))));
		}
		
		if(isNE(covrlnbIO.getCommPrem(),ZERO)){
			comlinkrec.commprem.set(covrlnbIO.getCommPrem());  //IBPLIFE-5237
		}
		if (isNE(aglfpf.getScmtab(),SPACES)) {
			comlinkrec.method.set(aglfpf.getScmtab());
		}
		else {
			/*     MOVE T5687-SRVCPY        TO CLNK-METHOD.                  */
			comlinkrec.method.set(wsaaSrvcpy);
		}
		if (isNE(t5644rec.compysubr,SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			/*    IF CLNK-STATUZ          NOT = O-K                         */
			if (isNE(comlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				/*       MOVE CLNK-STATUZ         TO SYSR-STATUZ                */
				syserrrec.statuz.set(comlinkrec.statuz);
				xxxxFatalError();
			}
		}
 //ILIFE-8709 start
		zrdecplcPojo.setAmountIn(comlinkrec.payamnt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.payamnt.set(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setAmountIn(comlinkrec.icommtot.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.icommtot.set(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setAmountIn(comlinkrec.erndamt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.erndamt.set(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
		wsaaAgentsInner.wsaaServDue[wsaaAgentSub.toInt()].add(comlinkrec.payamnt);
		wsaaAgentsInner.wsaaServEarn[wsaaAgentSub.toInt()].add(comlinkrec.erndamt);
		/* Accumulate component level servicing commission values for*/
		/* later posting.*/
		if (compLevelAcc.isTrue()) {
			wsaaCompErnScomm.add(comlinkrec.erndamt);
		}
		/* Set up AGCM values.*/
		agcmIO.setScmdue(comlinkrec.payamnt);
		agcmIO.setScmearn(comlinkrec.erndamt);
	}

	/**
	* <pre>
	* Renewal commission.
	* </pre>
	*/
protected void n5e233d()
	{
		initialiseLinkage2332();
		wsbbSub.set(covrlnbIO.getPayrseqno());
		wsaaBillfreq9.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
		/*    COMPUTE CLNK-ANNPREM ROUNDED                                 */
		if (isNE(t5687rec.zrrcombas,SPACES)) {
			compute(wsaaClnkAnnprem, 5).setRounded(mult(mult(covrlnbIO.getZbinstprem(),wsaaBillfreq9),(div(pcddlnbIO.getSplitBcomm(),100))));
			if (isNE(th605rec.bonusInd,"Y")) {
				compute(wsaaZctnAnnprem, 3).setRounded(mult(covrlnbIO.getZbinstprem(),wsaaBillfreq9));
			}
		}
		else {
			if (isNE(th605rec.bonusInd,"Y")) {
				compute(wsaaZctnAnnprem, 3).setRounded(mult(covrlnbIO.getInstprem(),wsaaBillfreq9));
			}
			compute(wsaaClnkAnnprem, 5).setRounded(mult(mult(covrlnbIO.getInstprem(),wsaaBillfreq9),(div(pcddlnbIO.getSplitBcomm(),100))));
		}
		/* The following Compute statement is used to round the contents   */
		/* of WSAA-CLNK-ANNPREM and move them into CLNK-ANNPREM.           */
		compute(comlinkrec.annprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
		if (isNE(t5687rec.zrrcombas,SPACES)) {
			compute(comlinkrec.instprem, 3).setRounded(mult(mult(covrlnbIO.getZbinstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]), (div(pcddlnbIO.getSplitBcomm(), 100))));
			if (isNE(th605rec.bonusInd,"Y")) {
				compute(wsaaZctnInstprem, 3).setRounded(mult(covrlnbIO.getZbinstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
			}
		}
		else {
			if (isNE(th605rec.bonusInd,"Y")) {
				compute(wsaaZctnInstprem, 3).setRounded(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
			}
			compute(comlinkrec.instprem, 3).setRounded(mult(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]), (div(pcddlnbIO.getSplitBcomm(), 100))));
		}
		
		if(isNE(covrlnbIO.getCommPrem(),ZERO)){
			comlinkrec.commprem.set(covrlnbIO.getCommPrem());  //IBPLIFE-5237
		}
		
		agcmIO.setRnlcdue(0);
		agcmIO.setRnlcearn(0);
		/* Set up target and period for Flexible Premium           <D9604>*/
		/* contracts                                               <D9604>*/
		if (isNE(covrlnbIO.getInstprem(),ZERO)) {
			compute(comlinkrec.targetPrem, 3).setRounded(mult(covrlnbIO.getInstprem(),wsaaBillfreq9));
		}
		comlinkrec.currto.set(chdrlnbIO.getOccdate());
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5644);
		/*                                                         <D9604>*/
		/* Dont use AGLF if Flexible Premium contract *            <D9604>*/
		/*                                                         <D9604>*/
		/*  IF T5687-RNWCPY             = SPACES                 <V42004>*/
		if (isEQ(wsaaRnwcpy,SPACES)
		&& flexiblePremiumContract.isTrue()) {
			goTo(GotoLabel.n5f233d);
		}
		/*                                                         <D9604>*/
		/* USE T5687 ENTRY IF PRESENT                              <D9604> */
		/*    IF AGLFLNB-RCMTAB  NOT      = SPACES                 <D9604>*/
		/*  IF T5687-RNWCPY             = SPACES                 <V42004>*/
		if (isEQ(wsaaRnwcpy,SPACES)) {
			itemIO.setItemitem(aglfpf.getRcmtab()); //ILIFE-8709
			comlinkrec.method.set(aglfpf.getRcmtab()); //ILIFE-8709
			agcmIO.setRnwcpy(aglfpf.getRcmtab()); //ILIFE-8709
		}
		else {
			/*     MOVE T5687-RNWCPY        TO ITEM-ITEMITEM                 */
			itemIO.setItemitem(wsaaRnwcpy);
			agcmIO.setRnwcpy(wsaaRnwcpy);
			comlinkrec.method.set(wsaaRnwcpy);
		}
		if (isEQ(itemIO.getItemitem(),SPACES)) {
			goTo(GotoLabel.n5f233d);
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.n5f233d);
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isNE(t5644rec.compysubr,SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			/*    IF CLNK-STATUZ          NOT = O-K                         */
			if (isNE(comlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				/*       MOVE CLNK-STATUZ         TO SYSR-STATUZ                */
				syserrrec.statuz.set(comlinkrec.statuz);
				xxxxFatalError();
			}
		}
		else {
			goTo(GotoLabel.n5f233d);
		}
 //ILIFE-8709 start
		zrdecplcPojo.setAmountIn(comlinkrec.payamnt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.payamnt.set(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setAmountIn(comlinkrec.erndamt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.erndamt.set(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
		wsaaAgentsInner.wsaaRenlDue[wsaaAgentSub.toInt()].add(comlinkrec.payamnt);
		wsaaAgentsInner.wsaaRenlEarn[wsaaAgentSub.toInt()].add(comlinkrec.erndamt);
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.n5eSkip233d);
		}
		/* Bonus Workbench *                                               */
		if (isNE(comlinkrec.payamnt,ZERO)) {
 //ILIFE-8709 start
			zctnpf = new Zctnpf();
			zctnpf.setAgntcoy(pcddlnbIO.getChdrcoy().toString());
			zctnpf.setAgntnum(pcddlnbIO.getAgntnum().toString());
			zctnpf.setCommAmt(comlinkrec.payamnt.getbigdata());
			setPrecision(zctnpf.getPremium(), 3);
			zctnpf.setPremium(sub(wsaaZctnInstprem,wsaaZctnAnnprem).getbigdata());
			zctnpf.setSplitBcomm(pcddlnbIO.getSplitBcomm().getbigdata());
			zctnpf.setZprflg("R");
			zctnpf.setChdrcoy(comlinkrec.chdrcoy.toString());
			zctnpf.setChdrnum(comlinkrec.chdrnum.toString());
			zctnpf.setLife(comlinkrec.life.toString());
			zctnpf.setCoverage(comlinkrec.coverage.toString());
			zctnpf.setRider(comlinkrec.rider.toString());
			zctnpf.setTranno(covrlnbIO.getTranno().toInt());
			zctnpf.setTransCode(wsaaBatckey.batcBatctrcde.toString());
			zctnpf.setEffdate(wsaaRegIntmDate.toInt());
			zctnpf.setTrandate(datcon1rec.intDate.toInt());
			insertZctnpfList.add(zctnpf);
			zctnpfDAO.insertZctnpf(insertZctnpfList);
 //ILIFE-8709 end
		}
	}

protected void n5eSkip233d()
	{
		/* Set up AGCM values.*/
		agcmIO.setRnlcdue(comlinkrec.payamnt);
		agcmIO.setRnlcearn(comlinkrec.erndamt);
		/* Accumulate component level renewal commission values for*/
		/* later posting.*/
		if (compLevelAcc.isTrue()) {
			wsaaCompErnRcomm.add(comlinkrec.erndamt);
		}
		if(gstOnCommFlag) {
			compute(wsaaCompErnRnwlCommGst, 3).setRounded(add(wsaaCompErnRnwlCommGst, comlinkrec.gstAmount));			
			agcmIO.setRnwlCommGst(wsaaCompErnRnwlCommGst);
		}
	}

	/**
	* <pre>
	* Write AGCM record.
	* </pre>
	*/
protected void n5f233d()
	{
		/*    MOVE CLNK-ANNPREM           TO AGCM-ANNPREM.                 */
		/* The following 2 Compute statement are used to round the         */
		/* contents of WSAA-CLNK-ANNPREM and move them into CLNK-ANNPREM   */
		/* and AGCM-ANNPREM.                                               */
		compute(comlinkrec.annprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
		setPrecision(agcmIO.getAnnprem(), 5);
		agcmIO.setAnnprem(mult(wsaaClnkAnnprem,1), true);
		/* If all commission values are zero do not write an AGCM.*/
		if (isEQ(agcmIO.getAnnprem(),0)
		&& isEQ(agcmIO.getInitcom(),0)
		&& isEQ(agcmIO.getCompay(),0)
		&& isEQ(agcmIO.getComern(),0)
		&& isEQ(agcmIO.getScmdue(),0)
		&& isEQ(agcmIO.getScmearn(),0)
		&& isEQ(agcmIO.getRnlcdue(),0)
		&& isEQ(agcmIO.getRnlcearn(),0)) {
			goTo(GotoLabel.singlePremium233d);
		}
		agcmIO.setTermid(wsaaTermid);
		agcmIO.setTransactionDate(wsaaTransactionDate);
		agcmIO.setTransactionTime(wsaaTransactionTime);
		agcmIO.setUser(wsaaUser);
		agcmIO.setChdrcoy(covrlnbIO.getChdrcoy());
		agcmIO.setChdrnum(covrlnbIO.getChdrnum());
		agcmIO.setAgntnum(aglfpf.getAgntnum()); //ILIFE-8709
		wsaaSaveAgntnum.set(aglfpf.getAgntnum()); //ILIFE-8709
		agcmIO.setAgentClass(aglfpf.getAgentClass()); //ILIFE-8709
		agcmIO.setLife(covrlnbIO.getLife());
		agcmIO.setCoverage(covrlnbIO.getCoverage());
		agcmIO.setRider(covrlnbIO.getRider());
		agcmIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		agcmIO.setTranno(chdrlnbIO.getTranno());
		agcmIO.setEfdate(chdrlnbIO.getOccdate());
		agcmIO.setCurrfrom(covrlnbIO.getCurrfrom());
		agcmIO.setCurrto(covrlnbIO.getCurrto());
		agcmIO.setOvrdcat("B");
		agcmIO.setCedagent(SPACES);
		agcmIO.setValidflag("1");
		agcmIO.setPtdate(chdrlnbIO.getPtdate());
		agcmIO.setSeqno(1);
		agcmIO.setFormat(formatsInner.agcmrec);
		agcmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			xxxxFatalError();
		}
		wsaaPremiumFlag.set("R");
		wsaaSub.set(ZERO);
		wsaaCompayKept2.set(agcmIO.getCompay());
		wsaaOvrtimes.set(ZERO);
		zrapIO.setFunction(varcom.begn);
		zrapIO.setStatuz(varcom.oK);
		while ( !(isEQ(aglfpf.getReportag(),SPACES)
		|| isEQ(aglfpf.getOvcpc(),ZERO))) {
			overriderCommissions233e();
		}

		/* PERFORM 233E-OVERRIDER-COMMISSIONS                           */
		/*                             UNTIL ZRAP-STATUZ  = ENDP.       */
		if (isEQ(covrlnbIO.getSingp(),0)
		|| isEQ(wsaaBasscmth,SPACES)) {
			goTo(GotoLabel.readNext2332);
		}
		/*    Get original agent details back again*/
		readAgentFile2331();
	}

protected void singlePremium233d()
	{
		/* Now for the single premium bit (if any)!*/
		if (isEQ(covrlnbIO.getSingp(),0)) {
			goTo(GotoLabel.readNext2332);
		}
		initialiseLinkage2332();
		comlinkrec.billfreq.set("00");
		/*    COMPUTE CLNK-ANNPREM ROUNDED                                 */
		if (isNE(t5687rec.zrrcombas,SPACES)
		&& isEQ(covrlnbIO.getInstprem(),0)) {
			compute(wsaaClnkAnnprem, 5).setRounded(mult(covrlnbIO.getZbinstprem(),(div(pcddlnbIO.getSplitBcomm(),100))));
		}
		else {
			compute(wsaaClnkAnnprem, 5).setRounded(mult(covrlnbIO.getSingp(),(div(pcddlnbIO.getSplitBcomm(),100))));
		}
		/*    MOVE CLNK-ANNPREM           TO CLNK-INSTPREM.                */
		/* The following Compute statements are used to round the contents */
		/* of WSAA-CLNK-ANNPREM and move them into CLNK-ANNPREM and        */
		/* CLNK-INSTPREM.                                                  */
		compute(comlinkrec.annprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
		compute(comlinkrec.instprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
		/*  IF T5687-BASSCMTH           = SPACES                         */
		if (isEQ(wsaaBasscmth,SPACES)) {
			goTo(GotoLabel.readNext2332);
		}
	}

protected void n6a233d()
	{
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5647);
		/*  MOVE T5687-BASSCMTH         TO ITEM-ITEMITEM,                */
		itemIO.setItemitem(wsaaBasscmth);
		agcmIO.setBasicCommMeth(wsaaBasscmth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5647rec.t5647Rec.set(itemIO.getGenarea());
		/*  MOVE T5687-BASSCMTH         TO CLNK-METHOD.                  */
		comlinkrec.method.set(wsaaBasscmth);
		if (isNE(t5647rec.commsubr,SPACES)) {
			callProgram(t5647rec.commsubr, comlinkrec.clnkallRec);
			/*    IF CLNK-STATUZ          NOT = O-K                         */
			if (isNE(comlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				/*       MOVE CLNK-STATUZ         TO SYSR-STATUZ                */
				syserrrec.statuz.set(comlinkrec.statuz);
				xxxxFatalError();
			}
		}
 //ILIFE-8709 start
		zrdecplcPojo.setAmountIn(comlinkrec.erndamt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.erndamt.set(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setAmountIn(comlinkrec.payamnt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.payamnt.set(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
	}

protected void n6b233d()
	{
		/*  IF  T5687-BASSCPY           = SPACES                         */
		if (isEQ(wsaaBasscpy,SPACES)
		&& isEQ(aglfpf.getBcmtab(),SPACES)
		|| isEQ(covrlnbIO.getSingp(),0)) {
			goTo(GotoLabel.n6c233d);
		}
		/* Dont use AGLF if Flexible Premium contract *                    */
		/*  IF T5687-BASSCPY            = SPACES                 <V42004>*/
		if (isEQ(wsaaBasscpy,SPACES)
		&& flexiblePremiumContract.isTrue()) {
			agcmIO.setBascpy(SPACES);
			goTo(GotoLabel.n6c233d);
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5644);
		/*  IF T5687-BASSCPY            = SPACES                         */
		if (isEQ(wsaaBasscpy,SPACES)) {
			itemIO.setItemitem(aglfpf.getBcmtab()); //ILIFE-8709
			comlinkrec.method.set(aglfpf.getBcmtab()); //ILIFE-8709
			agcmIO.setBascpy(aglfpf.getBcmtab()); //ILIFE-8709
		}
		else {
			/*     MOVE T5687-BASSCPY       TO ITEM-ITEMITEM                 */
			itemIO.setItemitem(wsaaBasscpy);
			agcmIO.setBascpy(wsaaBasscpy);
			comlinkrec.method.set(wsaaBasscpy);
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isNE(t5644rec.compysubr,SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			/*    IF CLNK-STATUZ          NOT = O-K                         */
			if (isNE(comlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				/*       MOVE CLNK-STATUZ         TO SYSR-STATUZ                */
				syserrrec.statuz.set(comlinkrec.statuz);
				xxxxFatalError();
			}
		}
 //ILIFE-8709 start
		zrdecplcPojo.setAmountIn(comlinkrec.erndamt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.erndamt.set(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setAmountIn(comlinkrec.payamnt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		comlinkrec.payamnt.set(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
	}

	/**
	* <pre>
	* Accumulate the commission amounts.
	* </pre>
	*/
protected void n6c233d()
	{
		/* to split the accumulation of single premium commsion.*/
		/*    ADD CLNK-PAYAMNT      TO WSAA-COMM-DUE(WSAA-AGENT-SUB).      */
		wsaaAgentsInner.wsaaTcomDue[wsaaAgentSub.toInt()].add(comlinkrec.payamnt);
		wsaaAgentsInner.wsaaSingp[wsaaAgentSub.toInt()].set(comlinkrec.annprem);
		compute(wsaaAgentsInner.wsaaCommPaid[wsaaAgentSub.toInt()], 3).setRounded(add(wsaaAgentsInner.wsaaCommPaid[wsaaAgentSub.toInt()], (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
		wsaaAgentsInner.wsaaCommEarn[wsaaAgentSub.toInt()].add(comlinkrec.erndamt);
		if (isNE(th605rec.bonusInd,"Y")) {
			goTo(GotoLabel.n6cSkip233d);
		}
		/* Bonus Workbench *                                               */
		if (isNE(comlinkrec.payamnt,ZERO)) {
 //ILIFE-8709 start
			zctnpf = new Zctnpf();
			zctnpf.setAgntcoy(pcddlnbIO.getChdrcoy().toString());
			zctnpf.setAgntnum(pcddlnbIO.getAgntnum().toString());
			zctnpf.setCommAmt(comlinkrec.payamnt.getbigdata());
			zctnpf.setPremium(covrlnbIO.getSingp().getbigdata());
			zctnpf.setSplitBcomm(pcddlnbIO.getSplitBcomm().getbigdata());
			zctnpf.setZprflg("S");
			zctnpf.setChdrcoy(comlinkrec.chdrcoy.toString());
			zctnpf.setChdrnum(comlinkrec.chdrnum.toString());
			zctnpf.setLife(comlinkrec.life.toString());
			zctnpf.setCoverage(comlinkrec.coverage.toString());
			zctnpf.setRider(comlinkrec.rider.toString());
			zctnpf.setTranno(covrlnbIO.getTranno().toInt());
			zctnpf.setTransCode(wsaaBatckey.batcBatctrcde.toString());
			zctnpf.setEffdate(wsaaRegIntmDate.toInt());
			zctnpf.setTrandate(datcon1rec.intDate.toInt());
			insertZctnpfList.add(zctnpf);
			zctnpfDAO.insertZctnpf(insertZctnpfList);
 //ILIFE-8709 end
		}
	}

protected void n6cSkip233d()
	{
		/* Accumulate component level initial commission values for*/
		/* later posting.*/
		if (compLevelAcc.isTrue()) {
			wsaaCompErnIcomm.add(comlinkrec.erndamt);
			comlinkrec.initcommpay.set(comlinkrec.erndamt); // ILIFE-8392
			if(gstOnCommFlag) {
				compute(wsaaCompErnInitCommGst, 3).setRounded(add(wsaaCompErnInitCommGst, comlinkrec.gstAmount));
				agcmIO.setInitCommGst(wsaaCompErnInitCommGst);
				compute(wsaaCompAdvIcomm, 3).setRounded(add(wsaaCompAdvIcomm,(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt))));//ILIFE-7965	
			}
			else {
				compute(wsaaCompAdvIcomm, 3).setRounded(add(wsaaCompAdvIcomm,(sub(add(comlinkrec.erndamt, comlinkrec.gstAmount),comlinkrec.payamnt))));//ILIFE-7965	
			}
		}
		
		/* Set values on AGCM.*/
		agcmIO.setInitcom(comlinkrec.icommtot);
		agcmIO.setCompay(comlinkrec.payamnt);
		agcmIO.setComern(comlinkrec.erndamt);
		agcmIO.setScmearn(0);
		agcmIO.setScmdue(0);
		agcmIO.setSrvcpy(SPACES);
		agcmIO.setRnlcdue(0);
		agcmIO.setRnlcearn(0);
		agcmIO.setRnwcpy(SPACES);
	}

	/**
	* <pre>
	* Write AGCM record.
	* </pre>
	*/
protected void n6d233d()
	{
		/*    MOVE CLNK-ANNPREM           TO AGCM-ANNPREM.                 */
		/* The following Compute statements are used to round the contents */
		/* of WSAA-CLNK-ANNPREM and move them into CLNK-ANNPREM and        */
		/* AGCM-ANNPREM.                                                   */
		compute(comlinkrec.annprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
		setPrecision(agcmIO.getAnnprem(), 5);
		agcmIO.setAnnprem(mult(wsaaClnkAnnprem,1), true);
		/* If all commission values are zero do not write an AGCM.*/
		if (isEQ(agcmIO.getAnnprem(),0)
		&& isEQ(agcmIO.getInitcom(),0)
		&& isEQ(agcmIO.getCompay(),0)
		&& isEQ(agcmIO.getComern(),0)
		&& isEQ(agcmIO.getScmdue(),0)
		&& isEQ(agcmIO.getScmearn(),0)
		&& isEQ(agcmIO.getRnlcdue(),0)
		&& isEQ(agcmIO.getRnlcearn(),0)) {
			goTo(GotoLabel.readNext2332);
		}
		agcmIO.setTermid(wsaaTermid);
		agcmIO.setTransactionDate(wsaaTransactionDate);
		agcmIO.setTransactionTime(wsaaTransactionTime);
		agcmIO.setUser(wsaaUser);
		agcmIO.setChdrcoy(covrlnbIO.getChdrcoy());
		agcmIO.setChdrnum(covrlnbIO.getChdrnum());
		agcmIO.setAgntnum(aglfpf.getAgntnum()); //ILIFE-8709
		wsaaSaveAgntnum.set(aglfpf.getAgntnum()); //ILIFE-8709
		agcmIO.setAgentClass(wsaaStoredAgentClass);
		agcmIO.setLife(covrlnbIO.getLife());
		agcmIO.setCoverage(covrlnbIO.getCoverage());
		agcmIO.setRider(covrlnbIO.getRider());
		agcmIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		agcmIO.setTranno(chdrlnbIO.getTranno());
		agcmIO.setEfdate(chdrlnbIO.getOccdate());
		agcmIO.setCurrfrom(covrlnbIO.getCurrfrom());
		agcmIO.setCurrto(covrlnbIO.getCurrto());
		agcmIO.setCedagent(SPACES);
		agcmIO.setValidflag("1");
		agcmIO.setOvrdcat("B");
		agcmIO.setCedagent(SPACES);
		/* MOVE CHDRLNB-PTDATE         TO AGCM-PTDATE.                  */
		agcmIO.setPtdate(ZERO);
		agcmIO.setSeqno(1);
		agcmIO.setFormat(formatsInner.agcmrec);
		agcmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			xxxxFatalError();
		}
		wsaaPremiumFlag.set("S");
		wsaaCompayKept2.set(agcmIO.getCompay());
		wsaaSub.set(ZERO);
		zrapIO.setFunction(varcom.begn);
		wsaaOvrtimes.set(ZERO);
		while ( !(isEQ(aglfpf.getReportag(),SPACES)
		|| isEQ(aglfpf.getOvcpc(),ZERO))) {
			overriderCommissions233e();
		}

	}

	/**
	* <pre>
	**** PERFORM 233E-OVERRIDER-COMMISSIONS
	****                             UNTIL ZRAP-STATUZ  = ENDP.
	* </pre>
	*/
protected void readNext2332()
	{
		componentPostings233f();
		pcddlnbIO.setFunction(varcom.nextr);
		wsaaAgentSub.add(1);
		if (isGT(wsaaAgentSub,10)) {
			syserrrec.statuz.set(i035);
			xxxxFatalError();
		}
	}

protected void overriderCommissions233e()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					overriderCommissions2331();
				case aglf233e:
					aglf233e();
					n5b233e();
				case writeAgcm233e:
					writeAgcm233e();
					overrideCommEarned233e();
					overrideCommAdvanced233e();
					sumOverrideComm233e();
				case checkNext233e:
					checkNext233e();
				case exit233e:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void overriderCommissions2331()
	{
		wsaaStoredOvcpc.set(aglfpf.getOvcpc()); //ILIFE-8709
		/* Call Basic and initial commission subroutines.*/
		comlinkrec.chdrcoy.set(pcddlnbIO.getChdrcoy());
		comlinkrec.chdrnum.set(pcddlnbIO.getChdrnum());
		comlinkrec.life.set(covrlnbIO.getLife());
		comlinkrec.coverage.set(covrlnbIO.getCoverage());
		comlinkrec.effdate.set(chdrlnbIO.getOccdate());
		comlinkrec.rider.set(covrlnbIO.getRider());
		comlinkrec.planSuffix.set(covrlnbIO.getPlanSuffix());
		comlinkrec.agent.set(aglfpf.getAgntnum()); //ILIFE-8709
		/* MOVE 'LINJO'                TO CLNK-FUNCTION.                */
		comlinkrec.jlife.set(covrlnbIO.getJlife());
		comlinkrec.crtable.set(covrlnbIO.getCrtable());
		comlinkrec.agentClass.set(wsaaStoredAgentClass);
		comlinkrec.icommtot.set(0);
		comlinkrec.icommpd.set(0);
		comlinkrec.icommernd.set(0);
		comlinkrec.payamnt.set(0);
		comlinkrec.erndamt.set(0);
		comlinkrec.instprem.set(0);
		comlinkrec.annprem.set(0);
		comlinkrec.targetPrem.set(0);
		comlinkrec.payamnt.set(wsaaCompayKept2);
		comlinkrec.zorcode.set(aglfpf.getZrorcode()); //ILIFE-8709
		comlinkrec.efdate.set(aglfpf.getEffdate()); //ILIFE-8709
		wsaaClnkAnnprem.set(0);
		comlinkrec.seqno.set(1);
		wsbbSub.set(covrlnbIO.getPayrseqno());
		wsaaBillfreq9.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
		if (isEQ(wsaaPremiumFlag,"S")) {
			/*        COMPUTE CLNK-ANNPREM ROUNDED                             */
			if (isNE(t5687rec.zrrcombas,SPACES)
			&& isEQ(covrlnbIO.getInstprem(),0)) {
				compute(wsaaClnkAnnprem, 5).setRounded(mult(covrlnbIO.getZbinstprem(),(div(pcddlnbIO.getSplitBcomm(),100))));
			}
			else {
				compute(wsaaClnkAnnprem, 5).setRounded(mult(covrlnbIO.getSingp(),(div(pcddlnbIO.getSplitBcomm(),100))));
			}
			/*        MOVE CLNK-ANNPREM TO CLNK-INSTPREM                       */
			/*                                                   2     <V70L01>*/
			/* The following Compute statements are used to round the contents */
			/* of WSAA-CLNK-ANNPREM and move them into CLNK-ANNPREM and        */
			/* CLNK-INSTPREM.                                                  */
			compute(comlinkrec.annprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
			compute(comlinkrec.instprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
			comlinkrec.billfreq.set("00");
		}
		else {
			if (isEQ(wsaaPremiumFlag,"R")) {
				/*        COMPUTE CLNK-ANNPREM ROUNDED                             */
				if (isNE(t5687rec.zrrcombas,SPACES)) {
					compute(wsaaClnkAnnprem, 5).setRounded(mult(mult(covrlnbIO.getZbinstprem(),wsaaBillfreq9),(div(pcddlnbIO.getSplitBcomm(),100))));
				}
				else {
					compute(wsaaClnkAnnprem, 5).setRounded(mult(mult(covrlnbIO.getInstprem(),wsaaBillfreq9),(div(pcddlnbIO.getSplitBcomm(),100))));
				}
				/* The following Compute statement is used to round the contents   */
				/* of WSAA-CLNK-ANNPREM and move them into CLNK-ANNPREM.           */
				compute(comlinkrec.annprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
			}
		}
		if (isNE(t5687rec.zrrcombas,SPACES)) {
			compute(comlinkrec.instprem, 3).setRounded(mult(mult(covrlnbIO.getZbinstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]), (div(pcddlnbIO.getSplitBcomm(), 100))));
		}
		else {
			compute(comlinkrec.instprem, 3).setRounded(mult(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]), (div(pcddlnbIO.getSplitBcomm(), 100))));
		}
		/* Set up target and Period for Flexible Premium contracts.<D9604>*/
		if (isEQ(wsaaPremiumFlag,"R")) {
			compute(comlinkrec.targetPrem, 3).setRounded(mult(covrlnbIO.getInstprem(),wsaaBillfreq9));
		}
		comlinkrec.currto.set(chdrlnbIO.getOccdate());
		if (isEQ(wsaaPremiumFlag,"S")) {
			/*     IF T5687-BASSCMTH        = SPACES                         */
			if (isEQ(wsaaBasscmth,SPACES)) {
				goTo(GotoLabel.aglf233e);
			}
		}
		else {
			/*     IF T5687-BASIC-COMM-METH = SPACES                         */
			/*        GO TO 233E-AGLF                                        */
			/*     END-IF                                                    */
			if (isEQ(wsaaBasicCommMeth,SPACES)) {
				goTo(GotoLabel.aglf233e);
			}
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5647);
		if (isEQ(wsaaPremiumFlag,"S")) {
			/*     MOVE T5687-BASSCMTH      TO ITEM-ITEMITEM,                */
			itemIO.setItemitem(wsaaBasscmth);
			agcmIO.setBasicCommMeth(wsaaBasscmth);
		}
		else {
			/*     MOVE T5687-BASIC-COMM-METH                                */
			/*                              TO ITEM-ITEMITEM,                */
			/*                                 AGCM-BASIC-COMM-METH.         */
			itemIO.setItemitem(wsaaBasicCommMeth);
			agcmIO.setBasicCommMeth(wsaaBasicCommMeth);
		}
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5647rec.t5647Rec.set(itemIO.getGenarea());
		if (isEQ(wsaaPremiumFlag,"S")) {
			/*     MOVE T5687-BASSCMTH      TO CLNK-METHOD                   */
			comlinkrec.method.set(wsaaBasscmth);
		}
		else {
			/*     MOVE T5687-BASIC-COMM-METH                                */
			/*                              TO CLNK-METHOD.                  */
			comlinkrec.method.set(wsaaBasicCommMeth);
		}
		if (isNE(t5647rec.commsubr,SPACES)) {
			callProgram(t5647rec.commsubr, comlinkrec.clnkallRec);
			/*    IF CLNK-STATUZ           NOT = O-K                        */
			if (isNE(comlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				/*       MOVE CLNK-STATUZ         TO SYSR-STATUZ                */
				syserrrec.statuz.set(comlinkrec.statuz);
				xxxxFatalError();
			}
		}
	}

protected void aglf233e()
	{
		/*MOVE AGCM-AGNTNUM           TO AGCM-CEDAGENT.                */
		/* MOVE AGLFLNB-REPORTAG       TO AGCM-AGNTNUM.                 */
		agcmIO.setAgntnum(zrapIO.getReportag());
 //ILIFE-8709 start
		aglfpf = aglfpfDAO.searchAglflnb(pcddlnbIO.getChdrcoy().toString(), aglfpf.getReportag());
		if (aglfpf == null) {
			aglfpf = new Aglfpf();
			aglfpf.setReportag(SPACES.stringValue());
			aglfpf.setOvcpc(BigDecimal.ZERO);
			goTo(GotoLabel.exit233e);
		}
		 //ILIFE-8709 end
	}

protected void n5b233e()
	{
		/*                                                         <D9604>*/
		/* Dont use AGLF if Flexible Premium contract *            <D9604>*/
		/*                                                         <D9604>*/
		if (isEQ(wsaaPremiumFlag,"R")
		&& isEQ(wsaaBascpy,SPACES)
		&& isEQ(aglfpf.getBcmtab(),SPACES)
		&& flexiblePremiumContract.isTrue()) {
			goTo(GotoLabel.writeAgcm233e);
		}
		if (isEQ(wsaaPremiumFlag,"S")) {
			/*     IF T5687-BASSCPY         = SPACES                 <V42004>*/
			if (isEQ(wsaaBasscpy,SPACES)
			&& isEQ(aglfpf.getBcmtab(),SPACES)) {
				goTo(GotoLabel.writeAgcm233e);
			}
		}
		if (isEQ(wsaaPremiumFlag,"R")) {
			/*     IF T5687-BASCPY          = SPACES                 <V42004>*/
			if (isEQ(wsaaBascpy,SPACES)
			&& isEQ(aglfpf.getBcmtab(),SPACES)) {
				goTo(GotoLabel.writeAgcm233e);
			}
		}
		if (isEQ(wsaaPremiumFlag,"R")
		&& flexiblePremiumContract.isTrue()) {
			comlinkrec.instprem.set(0);
		}
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5644);
		if (isEQ(wsaaPremiumFlag,"S")) {
			/*     IF T5687-BASSCPY         = SPACES                 <V42004>*/
			if (isEQ(wsaaBasscpy,SPACES)) {
				/*    IF T5687-ZRORPMSP        = SPACES                         */
				itemIO.setItemitem(aglfpf.getBcmtab());
				comlinkrec.method.set(aglfpf.getBcmtab());
				agcmIO.setBascpy(aglfpf.getBcmtab());
			}
			else {
				/*        MOVE T5687-BASSCPY    TO ITEM-ITEMITEM         <V42004>*/
				/*       MOVE T5687-ZRORPMSP   TO ITEM-ITEMITEM                 */
				itemIO.setItemitem(wsaaBasscpy);
				comlinkrec.method.set(wsaaBasscpy);
				agcmIO.setBascpy(wsaaBasscpy);
				comlinkrec.zcomcode.set(wsaaBasscpy);
			}
		}
		else {
			/*     IF T5687-BASCPY          = SPACES                 <V42004>*/
			/*    IF T5687-ZRORPMRG        = SPACES                         */
			if (isEQ(wsaaBascpy,SPACES)) {
				itemIO.setItemitem(aglfpf.getBcmtab());
				comlinkrec.method.set(aglfpf.getBcmtab());
				agcmIO.setBascpy(aglfpf.getBcmtab());
				comlinkrec.zcomcode.set(aglfpf.getBcmtab());
			}
			else {
				/*        MOVE T5687-BASCPY     TO ITEM-ITEMITEM         <V42004>*/
				/*       MOVE T5687-ZRORPMRG   TO ITEM-ITEMITEM                 */
				itemIO.setItemitem(wsaaBascpy);
				agcmIO.setBascpy(wsaaBascpy);
				comlinkrec.zcomcode.set(wsaaBascpy);
				comlinkrec.method.set(wsaaBascpy);
			}
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		if (isNE(t5644rec.compysubr,SPACES)) {
			callProgram(t5644rec.compysubr, comlinkrec.clnkallRec);
			/*    IF CLNK-STATUZ         NOT  = O-K                         */
			if (isNE(comlinkrec.statuz,varcom.oK)) {
				syserrrec.params.set(comlinkrec.clnkallRec);
				/*       MOVE CLNK-STATUZ         TO SYSR-STATUZ                */
				syserrrec.statuz.set(comlinkrec.statuz);
				xxxxFatalError();
			}
		}
 //ILIFE-8709 start
		zrdecplcPojo.setAmountIn(comlinkrec.erndamt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		agcmIO.setComern(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setAmountIn(comlinkrec.payamnt.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		agcmIO.setCompay(zrdecplcPojo.getAmountOut());
		zrdecplcPojo.setAmountIn(comlinkrec.icommtot.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		agcmIO.setInitcom(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
	}

	/**
	* <pre>
	**** MOVE CLNK-PAYAMNT           TO AGCM-COMPAY.          <V76F06>
	**** MOVE CLNK-ERNDAMT           TO AGCM-COMERN.          <V76F06>
	**** MOVE CLNK-ICOMMTOT          TO AGCM-INITCOM.         <V76F06>
	* </pre>
	*/
protected void writeAgcm233e()
	{
		/* Add 1 to the no. of times this section has been looped.         */
		wsaaOvrtimes.add(1);
		/* Compute the overrider commissions.*/
		/* COMPUTE                                                      */
		/*   AGCM-INITCOM ROUNDED                                       */
		/*                = CLNK-ICOMMTOT * (WSAA-STORED-OVCPC / 100).  */
		/* COMPUTE                                                      */
		/*   AGCM-COMPAY ROUNDED                                        */
		/*                = CLNK-PAYAMNT * (WSAA-STORED-OVCPC / 100).   */
		/* COMPUTE                                                      */
		/*   AGCM-COMERN ROUNDED                                        */
		/*                = CLNK-ERNDAMT * (WSAA-STORED-OVCPC / 100).   */
		/* If this is the first time the overrider section is being        */
		/* performed for a particular policy, compute o/r commission       */
		/* using the initial commission as a basis for calculations.       */
		/* Otherwise use the stored values (with suffix 'KEPT') to         */
		/* allow for the possibility of there being more than one          */
		/* o/r level.                                                      */
		if (isLT(wsaaOvrtimes,2)) {
			setPrecision(agcmIO.getInitcom(), 3);
			agcmIO.setInitcom(mult(comlinkrec.icommtot,(div(wsaaStoredOvcpc,100))), true);
 //ILIFE-8709 start
			zrdecplcPojo.setAmountIn(agcmIO.getInitcom().getbigdata());
			zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
			callRounding9000();
			agcmIO.setInitcom(zrdecplcPojo.getAmountOut());
			wsaaComtotKept.set(agcmIO.getInitcom());
			setPrecision(agcmIO.getCompay(), 3);
			agcmIO.setCompay(mult(comlinkrec.payamnt,(div(wsaaStoredOvcpc,100))), true);
			zrdecplcPojo.setAmountIn(agcmIO.getCompay().getbigdata());
			zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
			callRounding9000();
			agcmIO.setCompay(zrdecplcPojo.getAmountOut());
			wsaaCompayKept.set(agcmIO.getCompay());
			setPrecision(agcmIO.getComern(), 3);
			agcmIO.setComern(mult(comlinkrec.erndamt,(div(wsaaStoredOvcpc,100))), true);
			zrdecplcPojo.setAmountIn(agcmIO.getComern().getbigdata());
			zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
			callRounding9000();
			agcmIO.setComern(zrdecplcPojo.getAmountOut());
			wsaaComernKept.set(agcmIO.getComern());
		}
		else {
			setPrecision(agcmIO.getInitcom(), 3);
			agcmIO.setInitcom(mult(wsaaComtotKept,(div(wsaaStoredOvcpc,100))), true);
			zrdecplcPojo.setAmountIn(agcmIO.getInitcom().getbigdata());
			zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
			callRounding9000();
			agcmIO.setInitcom(zrdecplcPojo.getAmountOut());
			wsaaComtotKept.set(agcmIO.getInitcom());
			setPrecision(agcmIO.getCompay(), 3);
			agcmIO.setCompay(mult(wsaaCompayKept,(div(wsaaStoredOvcpc,100))), true);
			zrdecplcPojo.setAmountIn(agcmIO.getCompay().getbigdata());
			zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
			callRounding9000();
			agcmIO.setCompay(zrdecplcPojo.getAmountOut());
			wsaaCompayKept.set(agcmIO.getCompay());
			setPrecision(agcmIO.getComern(), 3);
			agcmIO.setComern(mult(wsaaComernKept,(div(wsaaStoredOvcpc,100))), true);
			zrdecplcPojo.setAmountIn(agcmIO.getComern().getbigdata());
			zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
			callRounding9000();
			agcmIO.setComern(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
			wsaaComernKept.set(agcmIO.getComern());
		}
		/* Store override commission values.*/
		if (compLevelAcc.isTrue()) {
			wsaaCompErnOcomm.set(agcmIO.getComern());
			compute(wsaaCompAdvOcomm, 3).setRounded(sub(agcmIO.getCompay(),agcmIO.getComern()));
		}
		/* Write the AGCM record.*/
		agcmIO.setScmdue(0);
		agcmIO.setScmearn(0);
		agcmIO.setRnlcdue(0);
		agcmIO.setRnlcearn(0);
		agcmIO.setSrvcpy(SPACES);
		agcmIO.setRnwcpy(SPACES);
		/*    MOVE CLNK-ANNPREM           TO AGCM-ANNPREM.                 */
		/* The following Compute statements are used to round the contents */
		/* of WSAA-CLNK-ANNPREM and move them into CLNK-ANNPREM and        */
		/* AGCM-ANNPREM.                                                   */
		compute(comlinkrec.annprem, 5).setRounded(mult(wsaaClnkAnnprem,1));
		setPrecision(agcmIO.getAnnprem(), 5);
		agcmIO.setAnnprem(mult(wsaaClnkAnnprem,1), true);
		/* If all commission values are zero do not write an AGCM.*/
		if (isEQ(agcmIO.getAnnprem(),0)
		&& isEQ(agcmIO.getInitcom(),0)
		&& isEQ(agcmIO.getCompay(),0)
		&& isEQ(agcmIO.getComern(),0)
		&& isEQ(agcmIO.getScmdue(),0)
		&& isEQ(agcmIO.getScmearn(),0)
		&& isEQ(agcmIO.getRnlcdue(),0)
		&& isEQ(agcmIO.getRnlcearn(),0)) {
			goTo(GotoLabel.checkNext233e);
		}
		agcmIO.setTermid(wsaaTermid);
		agcmIO.setTransactionDate(wsaaTransactionDate);
		agcmIO.setTransactionTime(wsaaTransactionTime);
		agcmIO.setUser(wsaaUser);
		agcmIO.setChdrcoy(covrlnbIO.getChdrcoy());
		agcmIO.setChdrnum(covrlnbIO.getChdrnum());
		/*MOVE AGLFLNB-AGNTNUM        TO AGCM-AGNTNUM,                 */
		agcmIO.setAgntnum(aglfpf.getAgntnum()); //ILIFE-8709
		/*                               WSAA-AGCM-AGNTNUM.            */
		/*MOVE WSAA-STORED-AGENT-CLASS TO AGCM-AGENT-CLASS.            */
		agcmIO.setAgentClass(aglfpf.getAgentClass()); //ILIFE-8709
		agcmIO.setLife(covrlnbIO.getLife());
		agcmIO.setCoverage(covrlnbIO.getCoverage());
		agcmIO.setRider(covrlnbIO.getRider());
		agcmIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		agcmIO.setTranno(chdrlnbIO.getTranno());
		agcmIO.setEfdate(chdrlnbIO.getOccdate());
		agcmIO.setCurrfrom(covrlnbIO.getCurrfrom());
		agcmIO.setCurrto(covrlnbIO.getCurrto());
		agcmIO.setOvrdcat("O");
		agcmIO.setValidflag("1");
		/*MOVE SPACES                 TO AGCM-CEDAGENT.                */
		if (isEQ(wsaaAgcmCedagent,agcmIO.getAgntnum())) {
			agcmIO.setCedagent(SPACES);
		}
		else {
			agcmIO.setCedagent(wsaaAgcmCedagent);
		}
		/* MOVE CHDRLNB-PTDATE         TO AGCM-PTDATE.                  */
		if (isEQ(wsaaPremiumFlag,"S")) {
			agcmIO.setPtdate(ZERO);
		}
		else {
			agcmIO.setPtdate(chdrlnbIO.getPtdate());
		}
		agcmIO.setSeqno(1);
		agcmIO.setFormat(formatsInner.agcmrec);
		agcmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmIO);
		if (isNE(agcmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agcmIO.getParams());
			syserrrec.statuz.set(agcmIO.getStatuz());
			xxxxFatalError();
		}
		wsaaSub.add(1);
		if (isGT(wsaaSub,10)) {
			syserrrec.statuz.set(i035);
			xxxxFatalError();
		}
		wsaaOverrideAgntnum[wsaaAgentSub.toInt()][wsaaSub.toInt()].set(aglfpf.getAgntnum()); //ILIFE-8709
		compute(wsaaOvrdCommPaid[wsaaAgentSub.toInt()][wsaaSub.toInt()], 3).setRounded(add(wsaaOvrdCommPaid[wsaaAgentSub.toInt()][wsaaSub.toInt()],(sub(agcmIO.getCompay(),agcmIO.getComern()))));
		wsaaOverrideComm[wsaaAgentSub.toInt()][wsaaSub.toInt()].add(agcmIO.getCompay());
		if (!compLevelAcc.isTrue()) {
			goTo(GotoLabel.checkNext233e);
		}
		/* Do override commission ACMVs for this overriding agent for*/
		/* this agent for this component (WSAA-AGENT-SUB and WSAA-SUB*/
		/* should already be correctly set).*/
		lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
	}

protected void overrideCommEarned233e()
	{
		if (isNE(wsaaCompErnOcomm,ZERO)) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[10]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[10]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[10]);
			lifacmvrec.glsign.set(wsaaT5645Sign[10]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[10]);
			lifacmvrec.origamt.set(wsaaCompErnOcomm);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
	}

protected void overrideCommAdvanced233e()
	{
		if (isNE(wsaaCompAdvOcomm,ZERO)) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[11]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[11]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[11]);
			lifacmvrec.glsign.set(wsaaT5645Sign[11]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[11]);
			lifacmvrec.origamt.set(wsaaCompAdvOcomm);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
	}

protected void sumOverrideComm233e()
	{
		if (isNE(wsaaCompAdvOcomm,ZERO)
		|| isNE(wsaaCompErnOcomm,ZERO)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode15);
			lifacmvrec.sacstyp.set(t5645rec.sacstype15);
			lifacmvrec.glcode.set(t5645rec.glmap15);
			lifacmvrec.glsign.set(t5645rec.sign15);
			lifacmvrec.contot.set(t5645rec.cnttot15);
			lifacmvrec.rldgacct.set(aglfpf.getAgntnum()); //ILIFE-8709
			lifacmvrec.origamt.set(wsaaCompAdvOcomm);
			lifacmvrec.origamt.add(wsaaCompErnOcomm);
			lifacmvrec.tranref.set(wsaaRldgacct);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
			lifacmvrec.rldgacct.set(wsaaRldgacct);
		}
	}

protected void checkNext233e()
	{
		/* Read the next reports to if it exists..*/
		if (isEQ(aglfpf.getReportag(),SPACES)) {
			aglfpf.setOvcpc(BigDecimal.ZERO);
			/******   GO TO 233E-EXIT.                                          */
		}
	}

protected void componentPostings233f()
	{
		postInitEarnedComm2334();
		postInitAdvanceComm2335();
		postServEarnedComm2336();
		postRenewalEarnedComm2337();
		getTaxRelfSubroutine2338a();
		checkRtrnFile2338b();
		calcTaxRelief2338c();
	}

protected void postInitEarnedComm2334()
	{
		if (isNE(wsaaCompErnIcomm,0)) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[6]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[6]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[6]);
			lifacmvrec.glsign.set(wsaaT5645Sign[6]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[6]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			lifacmvrec.origamt.set(wsaaCompErnIcomm);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
			if(gstOnCommFlag && isNE(wsaaCompErnInitCommGst,0)) {
				gstPostings(wsaaCompErnInitCommGst.getbigdata(), wsaaSaveAgntnum.toString(), 17);
			}
		}
	}

protected void postInitAdvanceComm2335()
	{
		if (isNE(wsaaCompAdvIcomm,0)) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[7]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[7]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[7]);
			lifacmvrec.glsign.set(wsaaT5645Sign[7]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[7]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			lifacmvrec.origamt.set(wsaaCompAdvIcomm);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
		if (isNE(wsaaCompAdvIcomm,ZERO)
		|| isNE(wsaaCompErnIcomm,ZERO)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
			/* IF THIS IS A Single Premium Commission, posting TO 'SG'         */
			if (isEQ(wsaaSingPrmInd,"Y")) {
				lifacmvrec.sacscode.set(wsaaT5645Sacscode[15]);
				lifacmvrec.sacstyp.set(wsaaT5645Sacstype[15]);
				lifacmvrec.glcode.set(wsaaT5645Glmap[15]);
				lifacmvrec.glsign.set(wsaaT5645Sign[15]);
				lifacmvrec.contot.set(wsaaT5645Cnttot[15]);
			}
			lifacmvrec.rldgacct.set(wsaaSaveAgntnum);
			if(!gstOnCommFlag) {
				lifacmvrec.origamt.set(wsaaCompAdvIcomm);
			}
			else {
				/*lifacmvrec.origamt.set(mult(wsaaCompErnInitCommGst, -1));
				lifacmvrec.origamt.add(wsaaCompErnIcomm);*/
				lifacmvrec.origamt.set(comlinkrec.initcommpay);//ILIFE-7965	// ILIFE-8392
			}
			lifacmvrec.origamt.add(wsaaCompErnIcomm);
			wsaaRldgChdrnum.set(covrlnbIO.getChdrnum());
			wsaaRldgLife.set(covrlnbIO.getLife());
			wsaaRldgCoverage.set(covrlnbIO.getCoverage());
			wsaaRldgRider.set(covrlnbIO.getRider());
			wsaaPlan.set(covrlnbIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmIO.getAnnprem());
				b100CallZorcompy();
			}
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			if(gstOnCommFlag && isNE(zorlnkrec.gstAmount,0) && null!=wsaaOverrideAgntnum[wsaaAgentSub.toInt()][wsaaSub.toInt()]) {
				gstPostings(zorlnkrec.gstAmount.getbigdata(), wsaaOverrideAgntnum[wsaaAgentSub.toInt()][wsaaSub.toInt()].toString(), 18); // ILIFE-8468
			}
		}
	}

protected void postServEarnedComm2336()
	{
		if (isNE(wsaaCompErnScomm,0)) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[8]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[8]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[8]);
			lifacmvrec.glsign.set(wsaaT5645Sign[8]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[8]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			lifacmvrec.origamt.set(wsaaCompErnScomm);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
	}

protected void postRenewalEarnedComm2337()
	{
		if (isNE(wsaaCompErnRcomm,0)) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[9]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[9]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[9]);
			lifacmvrec.glsign.set(wsaaT5645Sign[9]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[9]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			lifacmvrec.origamt.set(wsaaCompErnRcomm);
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
			if(gstOnCommFlag && isNE(wsaaCompErnRnwlCommGst,0)) {
				gstPostings(wsaaCompErnRnwlCommGst.getbigdata(),wsaaSaveAgntnum.toString(),19);
			}
		}
	}

protected void getTaxRelfSubroutine2338a()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t6687);
		itemIO.setItemitem(t5688rec.taxrelmth);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t6687rec.t6687Rec.set(itemIO.getGenarea());
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
	}

protected void checkRtrnFile2338b()
	{
		/* Read the RTRN file to see if a cash receipt has been*/
		/* created for this contract.*/
 //ILIFE-8709 start	
		rtrnsac = rtrnsacDao.getRtrnsacRecords(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(), isEQ(wsaaSuspInd,"Y")?acblpf.getOrigcurr():SPACES.toString(), 
				t5645rec.sacscode01.toString(), t5645rec.sacstype01.toString());
		if(rtrnsac == null){
			rtrnsac = new Rtrnpf();
			rtrnsac.setEffdate(Integer.parseInt(varcom.vrcmMaxDate.toString()));
		}
		
 //ILIFE-8709 end
	}

protected void calcTaxRelief2338c()
	{
		/* If the tax relief method is not spaces calculate the tax*/
		/* relief amount and deduct it from the premium.*/
		if (isNE(t5688rec.taxrelmth,SPACES)) {
			wsbbSub.set(covrlnbIO.getPayrseqno());
			prasrec.clntnum.set(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()]);
			prasrec.clntcoy.set(wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()]);
			prasrec.incomeSeqNo.set(wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()]);
			prasrec.cnttype.set(chdrlnbIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			/* Use the due date unless a receipt exists with a date later*/
			/* then the due date.*/
			if (isEQ(rtrnsac.getEffdate(),varcom.vrcmMaxDate)) {
				prasrec.effdate.set(chdrlnbIO.getOccdate());
			}
			else {
				if (isGT(chdrlnbIO.getOccdate(),rtrnsac.getEffdate())) {
					prasrec.effdate.set(chdrlnbIO.getOccdate());
				}
				else {
					prasrec.effdate.set(rtrnsac.getEffdate());
				}
			}
			prasrec.company.set(chdrlnbIO.getChdrcoy());
			compute(prasrec.grossprem, 2).set(add(wsaaCompSingPrem,wsaaCompRegPrem));
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				xxxxFatalError();
			}
			zrdecplcPojo.setAmountIn(prasrec.taxrelamt.getbigdata()); //ILIFE-8709
			zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
			callRounding9000();
			/*****     MOVE PRAS-TAXRELAMT         TO WSAA-COMP-TAX-RELIEF      */
		}
	}

protected void premTax233g()
	{
		try {
			start233g();
			createTaxd233g();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start233g()
	{
		wsaaPremTax01.set(0);
		wsaaPremTax02.set(0);
		wsaaTaxBaseAmt.set(0);
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covrlnbIO.getCrtable());
		readTr52e233h();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			readTr52e233h();
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e233h();
		}
		/* Call TR52D tax subroutine                                       */
		if (isNE(tr52erec.taxind01, "Y")) {
			goTo(GotoLabel.exit233g);
		}
		if (isNE(covrlnbIO.getSingp(), ZERO)) {
			txcalcrec.function.set("CPST");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
			txcalcrec.life.set(covrlnbIO.getLife());
			txcalcrec.coverage.set(covrlnbIO.getCoverage());
			txcalcrec.rider.set(covrlnbIO.getRider());
			txcalcrec.planSuffix.set(covrlnbIO.getPlanSuffix());
			txcalcrec.crtable.set(covrlnbIO.getCrtable());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			txcalcrec.tranno.set(chdrlnbIO.getTranno());
			txcalcrec.language.set(atmodrec.language);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
			wsaaCntCurr.set(chdrlnbIO.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			txcalcrec.cntTaxInd.set(SPACES);
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrlnbIO.getOccdate());
			txcalcrec.jrnseq.set(wsaaJrnseq);
			txcalcrec.amountIn.set(wsaaCompSingPrem);
			txcalcrec.batckey.set(atmodrec.batchKey);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				xxxxFatalError();
			}
			if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
				/*NEXT_SENTENCE*/
			}
			else {
				wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[1]);
			}
			if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
				/*NEXT_SENTENCE*/
			}
			else {
				wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[2]);
			}
			wsaaPremTax01.add(txcalcrec.taxAmt[1]);
			wsaaPremTax02.add(txcalcrec.taxAmt[2]);
			wsaaTaxBaseAmt.add(txcalcrec.amountIn);
			wsaaJrnseq.set(txcalcrec.jrnseq);
		}
		if (isNE(covrlnbIO.getInstprem(), ZERO)) {
			txcalcrec.function.set("CPST");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
			txcalcrec.life.set(covrlnbIO.getLife());
			txcalcrec.coverage.set(covrlnbIO.getCoverage());
			txcalcrec.rider.set(covrlnbIO.getRider());
			txcalcrec.planSuffix.set(covrlnbIO.getPlanSuffix());
			txcalcrec.crtable.set(covrlnbIO.getCrtable());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
			wsaaCntCurr.set(chdrlnbIO.getCntcurr());
			txcalcrec.tranno.set(chdrlnbIO.getTranno());
			txcalcrec.language.set(atmodrec.language);
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			txcalcrec.transType.set("PREM");
			txcalcrec.effdate.set(chdrlnbIO.getOccdate());
			txcalcrec.jrnseq.set(wsaaJrnseq);
			txcalcrec.batckey.set(atmodrec.batchKey);
			wsaaAmountIn.set(ZERO);
			if (isEQ(tr52erec.zbastyp, "Y")) {
				compute(wsaaAmountIn, 2).set(mult(covrlnbIO.getZbinstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
			}
			else {
				compute(wsaaAmountIn, 2).set(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
			}
			txcalcrec.amountIn.set(wsaaAmountIn);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				xxxxFatalError();
			}
			if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
				/*NEXT_SENTENCE*/
			}
			else {
				wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[1]);
			}
			if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
				/*NEXT_SENTENCE*/
			}
			else {
				wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[2]);
			}
			wsaaPremTax01.add(txcalcrec.taxAmt[1]);
			wsaaPremTax02.add(txcalcrec.taxAmt[2]);
			wsaaTaxBaseAmt.add(txcalcrec.amountIn);
			wsaaJrnseq.set(txcalcrec.jrnseq);
		}
	}

protected void createTaxd233g()
	{
		if (isEQ(wsaaPremTax01, 0)
		&& isEQ(wsaaPremTax02, 0)) {
			return ;
		}
 //ILIFE-8709 start
		taxdpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		taxdpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		taxdpf.setLife(covrlnbIO.getLife().toString());
		taxdpf.setCoverage(covrlnbIO.getCoverage().toString());
		taxdpf.setRider(covrlnbIO.getRider().toString());
		taxdpf.setPlansfx(0);
		taxdpf.setEffdate(txcalcrec.effdate.toInt());
		taxdpf.setInstfrom(txcalcrec.effdate.toInt());
		taxdpf.setBillcd(txcalcrec.effdate.toInt());
		if (isNE(wsaaCompSingPrem, ZERO)) {
			taxdpf.setInstto(varcom.vrcmMaxDate.toInt());
		}
		else {
			taxdpf.setInstto(wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()].toInt());
		}
		wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		int i = 1;
		taxdpf.setTranref(wsaaTranref.toString());
		taxdpf.setTranno(chdrlnbIO.getTranno().toInt());
		taxdpf.setTrantype("PREM");
		taxdpf.setBaseamt(wsaaTaxBaseAmt.getbigdata());
		taxdpf.setTaxamt01(wsaaPremTax01.getbigdata());
		taxdpf.setTaxamt02(wsaaPremTax02.getbigdata());
		taxdpf.setTaxamt03(BigDecimal.ZERO);
		taxdpf.setTxabsind01(txcalcrec.taxAbsorb[i].toString());
		taxdpf.setTxtype01(txcalcrec.taxType[i].toString());
		++i;
		taxdpf.setTxabsind02(txcalcrec.taxAbsorb[i].toString());
		taxdpf.setTxtype02(txcalcrec.taxType[i].toString());
		taxdpf.setTxabsind03(SPACES.toString());
		taxdpf.setTxtype03(SPACES.toString());
		taxdpf.setPostflg("P");
		taxdInsertList.add(taxdpf);
		taxdpfDAO.insertTaxdPF(taxdInsertList);
		taxdInsertList.clear();
		
		//ILFE-1971-Starts
	//	wsaaTotTax=add(wsaaTotTax,taxdIO.getTaxamt01(),taxdIO.getTaxamt02(),taxdIO.getTaxamt03());
		wsaaTotTax=add(wsaaTotTax,taxdpf.getTaxamt01(),taxdpf.getTaxamt02(),taxdpf.getTaxamt03());
		//ILIFE-1971-Ends
 //ILIFE-8709 end
	}

protected void readTr52e233h()
	{
		start233h();
	}

protected void start233h()
	{
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(atmodrec.company);
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (((isNE(itdmIO.getItemcoy(), atmodrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
		|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp)))
		&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			syserrrec.params.set(wsaaTr52eKey);
			syserrrec.statuz.set(itdmIO.getStatuz());
			xxxxFatalError();
		}
		if (((isEQ(itdmIO.getItemcoy(), atmodrec.company))
		&& (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
		&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey))
		&& (isNE(itdmIO.getStatuz(), varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

protected void updatePayer2350(Payrpf payrpf)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updatePayer2351(payrpf);
				case singpFee2352:
					singpFee2352();
				case adjustPremium2352:
					adjustPremium2352();
					rewritePayer2353(payrpf);
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updatePayer2351(Payrpf payrpf)
	{
		wsbbSub.set(payrpf.getPayrseqno());
		/* Look up the tax relief subroutine on T6687.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t6687);
		itemIO.setItemitem(t5688rec.taxrelmth);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			t6687rec.t6687Rec.set(itemIO.getGenarea());
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
		if (isEQ(t5688rec.feemeth,SPACES)) {
			mgfeelrec.mgfee.set(0);
			goTo(GotoLabel.adjustPremium2352);
		}
		/* Calculate the contract fee*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		/*  Set up linkage area and call subroutine.*/
		mgfeelrec.company.set(chdrlnbIO.getChdrcoy());
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set(wsaaBillingInformationInner.wsaaBillfreq[1]);
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,varcom.oK)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			xxxxFatalError();
		}
 //ILIFE-8709 start
		zrdecplcPojo.setAmountIn(mgfeelrec.mgfee.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		wsaaMgfeeRegAmt.set(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
		/* MOVE MGFL-MGFEE             TO WSAA-MGFEE-REG-AMT.           */
		if (isEQ(chdrlnbIO.getBillcd(),chdrlnbIO.getOccdate())) {
			goTo(GotoLabel.singpFee2352);
		}
		wsaaMgfee.set(mgfeelrec.mgfee);
	}

protected void singpFee2352()
	{
		if (isEQ(wsaaSingPrmInd,"N")) {
			goTo(GotoLabel.adjustPremium2352);
		}
		mgfeelrec.billfreq.set("00");
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,varcom.oK)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			syserrrec.statuz.set(mgfeelrec.statuz);
			xxxxFatalError();
		}
 //ILIFE-8709 start
		zrdecplcPojo.setAmountIn(mgfeelrec.mgfee.getbigdata());
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
		callRounding9000();
		wsaaSingpFee.set(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
	}

	/**
	* <pre>
	**** MOVE MGFL-MGFEE             TO WSAA-SINGP-FEE.
	* </pre>
	*/
protected void adjustPremium2352()
	{
		/* Adjust the regular premium*/
		compute(wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()], 0).set(mult(wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()], wsaaBillingInformationInner.wsaaRegPremAcc[wsbbSub.toInt()]));
		/* Calculate the amount needed to issue this contract*/
		compute(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()], 0).set(add(wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()], wsaaBillingInformationInner.wsaaSingPremAcc[wsbbSub.toInt()]));
		/* Read the RTRN file to see if a cash receipt has been*/
		/* created for this contract.*/
 //ILIFE-8709 start	
		rtrnsac = rtrnsacDao.getRtrnsacRecords(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(), isEQ(wsaaSuspInd,"Y")?acblpf.getOrigcurr():SPACES.toString(), 
				t5645rec.sacscode01.toString(), t5645rec.sacstype01.toString());
		if(rtrnsac == null){
			rtrnsac = new Rtrnpf();//IJTI-320
			rtrnsac.setEffdate(Integer.parseInt(varcom.vrcmMaxDate.toString()));
		}
		
 //ILIFE-8709 end
		/* If the tax relief method is not spaces calculate the tax*/
		/* relief amount and deduct it from the premium.*/
		if (isNE(t5688rec.taxrelmth,SPACES)) {
			prasrec.clntnum.set(wsaaBillingInformationInner.wsaaClntnum[wsbbSub.toInt()]);
			prasrec.clntcoy.set(wsaaBillingInformationInner.wsaaClntcoy[wsbbSub.toInt()]);
			prasrec.incomeSeqNo.set(wsaaBillingInformationInner.wsaaIncomeSeqNo[wsbbSub.toInt()]);
			prasrec.cnttype.set(chdrlnbIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			/* Use the due date unless a receipt exists with a date later*/
			/* then the due date.*/
			if (isEQ(rtrnsac.getEffdate(),varcom.vrcmMaxDate)) {
				prasrec.effdate.set(chdrlnbIO.getOccdate());
			}
			else {
				if (isGT(chdrlnbIO.getOccdate(),rtrnsac.getEffdate())) {
					prasrec.effdate.set(chdrlnbIO.getOccdate());
				}
				else {
					prasrec.effdate.set(rtrnsac.getEffdate());
				}
			}
			prasrec.company.set(chdrlnbIO.getChdrcoy());
			prasrec.grossprem.set(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()]);
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz,varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				xxxxFatalError();
			}
 //ILIFE-8709 start
			zrdecplcPojo.setAmountIn(prasrec.taxrelamt.getbigdata());
			zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString());
			callRounding9000();
			prasrec.taxrelamt.set(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
			wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()].subtract(prasrec.taxrelamt);
			wsaaBillingInformationInner.wsaaTaxrelamt[wsbbSub.toInt()].set(prasrec.taxrelamt);
		}
		/* Add the contract fee to the instalment premium for*/
		/* payer No. 1.*/
		if (isEQ(wsbbSub,1)) {
			/*if (isGT(wsaaFeeFreq,ZERO)) {
				compute(wsaaFeeAdj, 5).set(mult(wsaaMgfee,wsaaFeeFreq));
			}
			else {
				wsaaFeeAdj.set(wsaaMgfee);
			}
			if (isNE(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()], ZERO)) {
				compute(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()], 2).add(add(wsaaFeeAdj, wsaaSingpFee));
			}
			if (isNE(tr52drec.txcode, SPACES)) {
				cntfeeTax235a();
			}*/
			adjstPrmCustomerSpecific();
		}
		/* THE PAYR-SUSPENSE WILL BE CALCULATED IN PHASE B*/
		/* FOR THE MOMENT INITIALISE PAYER-SUSPENSE TO ZERO.*/
		wsaaPayrSuspense.set(0);
		/* Calculate the amount of tolerance needed to issue*/
		/* this contract.*/
		/* The tolerance is the difference between the amount due*/
		/* and the amount in suspense.*/
		/* If the Suspense amount is not in the Contract currency,         */
		/* convert the amount due to the Suspense currency before          */
		/* determining if tolerance is needed. Retain amounts due          */
		/* for GL posting purposes.                                        */
		wsaaBillingInformationInner.wsaaTolrUsed[wsbbSub.toInt()].set(0);
		/* MOVE WSAA-INSTPREM(WSBB-SUB)   TO WSAA-AMNT-DUE.             */
		if (isEQ(wsaaSuspInd,"N")) {
			wsaaAmntDue.set(ZERO);
		}
		else {
			if (isEQ(chdrlnbIO.getCntcurr(),acblpf.getOrigcurr())) {
				wsaaAmntDue.set(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()]);
				/* add the taxes to the amount due                              */
				compute(wsaaAmntDue, 2).set(add(add(add(wsaaAmntDue, wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()]), wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()]), wsaaBillingInformationInner.wsaaFeTax[wsbbSub.toInt()]));
			}
			else {
				wsaaInstpremTot.add(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()]);
				compute(wsaaInstpremTot, 2).add(add(wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()], add(wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()], wsaaBillingInformationInner.wsaaFeTax[wsbbSub.toInt()])));
				conlinkrec.clnk002Rec.set(SPACES);
				conlinkrec.amountOut.set(ZERO);
				conlinkrec.rateUsed.set(ZERO);
				conlinkrec.cashdate.set(hpadpf.getHprrcvdt()); //ILIFE-8709
				conlinkrec.currIn.set(chdrlnbIO.getCntcurr());
				conlinkrec.currOut.set(acblpf.getOrigcurr()); //ILIFE-8709
				conlinkrec.amountIn.set(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()]);
				/* add the taxes to the amount due                              */
				compute(conlinkrec.amountIn, 2).set(add(add(add(conlinkrec.amountIn, wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()]), wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()]), wsaaBillingInformationInner.wsaaFeTax[wsbbSub.toInt()]));
				conlinkrec.company.set(chdrlnbIO.getChdrcoy());
				conlinkrec.function.set("SURR");
				callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
				if (isNE(conlinkrec.statuz,varcom.oK)) {
					syserrrec.params.set(conlinkrec.clnk002Rec);
					syserrrec.statuz.set(conlinkrec.statuz);
					xxxxFatalError();
				}
				if (isNE(conlinkrec.amountOut, 0)) {
 //ILIFE-8709 start
					zrdecplcPojo.setCurrency(conlinkrec.currOut.toString());
					zrdecplcPojo.setAmountIn(conlinkrec.amountOut.getbigdata());
					zrdecplcPojo.setCurrency(acblpf.getOrigcurr());
					callRounding9000();
					conlinkrec.amountOut.set(zrdecplcPojo.getAmountOut());
 //ILIFE-8709 end
				}
				wsaaInstpremTotXge.add(conlinkrec.amountOut);
				wsaaAmntDue.set(conlinkrec.amountOut);
			}
		}
		if (isGT(wsaaAmntDue,wsaaPayrSuspense)) {
			wsaaAmntDue.subtract(wsaaPayrSuspense);
			/*       IF WSAA-AMNT-DUE > WSAA-CNT-SUSPENSE                      */
			/*          COMPUTE WSAA-TOLR-USED(WSBB-SUB) = WSAA-AMNT-DUE -     */
			/*                                   WSAA-CNT-SUSPENSE             */
			/*       END-IF                                                    */
			/*       SUBTRACT WSAA-TOLR-USED(WSBB-SUB) FROM WSAA-AMNT-DUE      */
			/*       COMPUTE WSAA-CNT-SUSPENSE = WSAA-CNT-SUSPENSE -           */
			/*                                   WSAA-AMNT-DUE                 */
			if (isGT(wsaaAmntDue,wsaaCntSuspense)) {
				if ((setPrecision(wsaaAmntDue, 2)
				&& isGT(wsaaAmntDue,add(wsaaCntSuspense,wsaaPrmdepst)))) {
					compute(wsaaBillingInformationInner.wsaaTolrUsed[wsbbSub.toInt()], 2).set(sub(sub(wsaaAmntDue, wsaaCntSuspense), wsaaPrmdepst));
					adjstPrmCustomerSpecific2353();
					wsaaCntSuspense.set(ZERO);
					wsaaPrmdepst.set(ZERO);
				}
				else {
					compute(wsaaPrmdepst, 2).set(sub(wsaaPrmdepst,(sub(wsaaAmntDue,wsaaCntSuspense))));
					wsaaCntSuspense.set(ZERO);
				}
			}
			else {
				compute(wsaaCntSuspense, 2).set(sub(wsaaCntSuspense,wsaaAmntDue));
			}
		}
		wsaaTotTolerance.add(wsaaBillingInformationInner.wsaaTolrUsed[wsbbSub.toInt()]);
		/* Keep a total of single and regular premiums for this*/
		/* contract.*/
		wsaaTotRegpAdj.add(wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()]);
		wsaaTotSingp.add(wsaaBillingInformationInner.wsaaSingPremAcc[wsbbSub.toInt()]);
	}

protected void adjstPrmCustomerSpecific(){
	
	if (isGT(wsaaFeeFreq,ZERO)) {
		compute(wsaaFeeAdj, 5).set(mult(wsaaMgfee,wsaaFeeFreq));
	}
	else {
		wsaaFeeAdj.set(wsaaMgfee);
	}
	if (isNE(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()], ZERO)) {
		compute(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()], 2).add(add(wsaaFeeAdj, wsaaSingpFee));
	}
	if (isNE(tr52drec.txcode, SPACES)) {
		cntfeeTax235a();
	}
}

protected void adjstPrmCustomerSpecific2353(){
	wsaaAmntDue.subtract(wsaaBillingInformationInner.wsaaTolrUsed[wsbbSub.toInt()]);
}

protected void rewritePayer2353(Payrpf payrpf)
	{
 //ILIFE-8709 start
		/*  Rewrite the PAYR record.*/
		Payrpf updatepayrpfdata = new Payrpf(payrpf);			
		if (isEQ(updatepayrpfdata.getBillfreq(),"00")) {
			updatepayrpfdata.setPtdate(wsaaOldCessDate.toInt());
			updatepayrpfdata.setBtdate(wsaaOldCessDate.toInt());
		}
		if (isNE(updatepayrpfdata.getBillfreq(),"00")) {
			updatepayrpfdata.setSinstamt01(wsaaBillingInformationInner.wsaaRegPremAcc[wsbbSub.toInt()].getbigdata());
			if (isEQ(updatepayrpfdata.getPayrseqno(),1)) {
				updatepayrpfdata.setSinstamt02(wsaaMgfeeRegAmt.getbigdata());
			}
			setPrecision(updatepayrpfdata.getSinstamt06(), 2);
			updatepayrpfdata.setSinstamt06(add(updatepayrpfdata.getSinstamt01(),updatepayrpfdata.getSinstamt02()).getbigdata());
			updatepayrpfdata.setPtdate(updatepayrpfdata.getBtdate());
		}
		if (isEQ(updatepayrpfdata.getBillfreq(),"00")) {
			/* OR  T5687-SINGLE-PREM-IND   = 'Y'                            */
			updatepayrpfdata.setPstatcode(t5679rec.setSngpCnStat.toString());
		}
		else {
			updatepayrpfdata.setPstatcode(t5679rec.setCnPremStat.toString());
		}
		updatepayrpfdata.setTranno(chdrlnbIO.getTranno().toInt());
		updatePayrpfList.add(updatepayrpfdata);
		payrpfDAO.updatePayr(updatePayrpfList);
 //ILIFE-8709 end
	}

protected void cntfeeTax235a()
	{
		try {
			start235a();
			createTaxd235a();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start235a()
	{
		/* Read table TR52E                                                */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		readTr52e233h();
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			readTr52e233h();
		}
		/* Call TR52D tax subroutine                                       */
		if (isNE(tr52erec.taxind02, "Y")) {
			goTo(GotoLabel.exit235a);
		}
		txcalcrec.function.set("CPST");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.crtable.set(covrlnbIO.getCrtable());
		txcalcrec.cntTaxInd.set("Y");
		txcalcrec.planSuffix.set(0);
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		txcalcrec.tranno.set(chdrlnbIO.getTranno());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.effdate.set(chdrlnbIO.getOccdate());
		wsaaAmountIn.set(ZERO);
		compute(wsaaAmountIn, 3).setRounded(add(wsaaSingpFee, wsaaFeeAdj));
		txcalcrec.amountIn.set(wsaaAmountIn);
		txcalcrec.transType.set("CNTF");
		txcalcrec.batckey.set(atmodrec.batchKey);
		txcalcrec.jrnseq.set(wsaaJrnseq);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(txcalcrec.statuz);
			syserrrec.params.set(txcalcrec.linkRec);
			xxxxFatalError();
		}
		if (isEQ(txcalcrec.taxAbsorb[1], "Y")) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaBillingInformationInner.wsaaFeTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[1]);
		}
		if (isEQ(txcalcrec.taxAbsorb[2], "Y")) {
			/*NEXT_SENTENCE*/
		}
		else {
			wsaaBillingInformationInner.wsaaFeTax[wsbbSub.toInt()].add(txcalcrec.taxAmt[2]);
		}
		wsaaJrnseq.set(txcalcrec.jrnseq);
	}

protected void createTaxd235a()
	{
		if (isEQ(txcalcrec.taxAmt[1], 0)
		&& isEQ(txcalcrec.taxAmt[2], 0)) {
			return ;
		}
 //ILIFE-8709 start
		
		taxdpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		taxdpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		taxdpf.setTrantype(txcalcrec.transType.toString());
		taxdpf.setLife(SPACES.toString());
		taxdpf.setCoverage(SPACES.toString());
		taxdpf.setRider(SPACES.toString());
		taxdpf.setPlansfx(0);
		taxdpf.setEffdate(txcalcrec.effdate.toInt());
		taxdpf.setInstfrom(txcalcrec.effdate.toInt());
		taxdpf.setBillcd(txcalcrec.effdate.toInt());
		taxdpf.setInstto(payrpf.getBtdate());
		taxdpf.setTranno(chdrlnbIO.getTranno().toInt());
		taxdpf.setBaseamt(txcalcrec.amountIn.getbigdata());
		wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaTranref);
		int i=1;
		taxdpf.setTranref(wsaaTranref.toString());
		taxdpf.setTaxamt01(txcalcrec.taxAmt[i].getbigdata());
		taxdpf.setTxabsind01(txcalcrec.taxAbsorb[i].toString());
		taxdpf.setTxtype01(txcalcrec.taxType[i].toString());
		++i;
		taxdpf.setTaxamt02(txcalcrec.taxAmt[i].getbigdata());
		taxdpf.setTxabsind02(txcalcrec.taxAbsorb[i].toString());
		taxdpf.setTxtype02(txcalcrec.taxType[i].toString());
		taxdpf.setTaxamt03(BigDecimal.ZERO);
		taxdpf.setTxabsind03(SPACES.toString());
		taxdpf.setTxtype03(SPACES.toString());
		taxdpf.setPostflg("P");
		taxdInsertList.add(taxdpf);
		taxdpfDAO.insertTaxdPF(taxdInsertList);
		taxdInsertList.clear();
		//ILFE-1971-Starts
	//	wsaaTotTax=add(wsaaTotTax,taxdIO.getTaxamt01(),taxdIO.getTaxamt02(),taxdIO.getTaxamt03());
		wsaaTotTax=add(wsaaTotTax,taxdpf.getTaxamt01(),taxdpf.getTaxamt02(),taxdpf.getTaxamt03());
		//ILIFE-1971-Ends
 //ILIFE-8709 end
	}

protected void getTolerance2360(Payrpf payrpf)
	{
		readT56672361(payrpf);
	}

protected void readT56672361(Payrpf payrpf)
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t5667);
		wsaaT5667Trancd.set(wsaaBatckey.batcBatctrcde);
		if (isEQ(wsaaSuspInd,"Y")) {
			wsaaT5667Curr.set(acblpf.getOrigcurr()); //ILIFE-8709
		}
		else {
			wsaaT5667Curr.set(SPACES);
		}
		itemIO.setItemitem(wsaaT5667Key);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			wsaaToleranceApp.set(ZERO);
			wsaaToleranceApp2.set(ZERO);
			wsaaAmountLimit.set(ZERO);
			wsaaAmountLimit2.set(ZERO);
		}
		else {
			t5667rec.t5667Rec.set(itemIO.getGenarea());
			wsaaSub1.set(1);
			while ( !(isGT(wsaaSub1,11))) {
				if (isEQ(t5667rec.freq[wsaaSub1.toInt()],payrpf.getBillfreq())) {
					wsaaToleranceApp.set(t5667rec.prmtol[wsaaSub1.toInt()]);
					wsaaToleranceApp2.set(t5667rec.prmtoln[wsaaSub1.toInt()]);
					wsaaAmountLimit.set(t5667rec.maxAmount[wsaaSub1.toInt()]);
					wsaaAmountLimit2.set(t5667rec.maxamt[wsaaSub1.toInt()]);
					wsaaSub1.set(11);
				}
				wsaaSub1.add(1);
			}

		}
		compute(wsaaTolerance, 2).set(div((mult(wsaaAmntDue,wsaaToleranceApp)),100));
		if (isGT(wsaaTolerance,wsaaAmountLimit)) {
			wsaaTolerance.set(wsaaAmountLimit);
		}
		compute(wsaaTolerance2, 2).set(div((mult(wsaaAmntDue,wsaaToleranceApp2)),100));
		if (isGT(wsaaTolerance2,wsaaAmountLimit2)) {
			wsaaTolerance2.set(wsaaAmountLimit2);
		}
	}

protected void contractHeaderRev2400(Payrpf payrpf)
	{
		updateHeader2492();
		updateLastPrem2493(payrpf);//ILIFE-7505 //ILIFE-8709
	}

	/**
	* <pre>
	*2410-CONTRACT-HEADER-REV.
	* </pre>
	*/
protected void updateHeader2492()
	{
		chdrlnbIO.setValidflag("1");
		chdrlnbIO.setCcdate(chdrlnbIO.getOccdate());
		chdrlnbIO.setStatcode(t5679rec.setCnRiskStat);
		chdrlnbIO.setStatdate(datcon1rec.intDate);
		chdrlnbIO.setStattran(chdrlnbIO.getTranno());
		if(onePflag && isEQ(chdrlnbIO.getBillchnl(),'D') && isNE(t3615rec.onepcashless,"Y") && isEQ(chdrlnbIO.getRcopt(),"Y")) {
			chdrlnbIO.setPstatcode("NR");	//IBPLIFE-3145
		} else 
		if (isEQ(chdrlnbIO.getBillfreq(),"00")) {
			/* OR  T5687-SINGLE-PREM-IND   = 'Y'                            */
			chdrlnbIO.setPstatcode(t5679rec.setSngpCnStat);
		}
		else {
			chdrlnbIO.setPstatcode(t5679rec.setCnPremStat);
		}
		chdrlnbIO.setPstatdate(datcon1rec.intDate);
		chdrlnbIO.setPstattran(chdrlnbIO.getTranno());
		chdrlnbIO.setTranlused(chdrlnbIO.getTranno());
		/* Should update instalment totals for single premium or regular*/
		/* premium ot both.*/
		/* Total fee = fee * no. of instalment paid.*/
		/* Tolerance = amount paid - premium due - fee due.*/
		if (isNE(wsaaTotSingp,ZERO)
		|| isNE(wsaaTotRegpAdj,ZERO)) {
			chdrlnbIO.setInstfrom(chdrlnbIO.getOccdate());
			setPrecision(chdrlnbIO.getInsttot01(), 2);
			chdrlnbIO.setInsttot01(sub((add(wsaaTotSingp,wsaaTotRegpAdj)),wsaaTotTolerance));
		}
		setPrecision(chdrlnbIO.getInsttot02(), 2);
		chdrlnbIO.setInsttot02(add(wsaaFeeAdj,wsaaSingpFee));
		chdrlnbIO.setInsttot03(wsaaTotTolerance);
		chdrlnbIO.setInsttot04(0);
		chdrlnbIO.setInsttot05(0);
		setPrecision(chdrlnbIO.getInsttot06(), 2);
		chdrlnbIO.setInsttot06(add(chdrlnbIO.getInsttot03(),(add(chdrlnbIO.getInsttot01(),chdrlnbIO.getInsttot02()))));
		if (isEQ(chdrlnbIO.getBillfreq(),"00")) {
			chdrlnbIO.setPtdate(wsaaOldCessDate);
			chdrlnbIO.setBtdate(wsaaOldCessDate);
		}
		else {
			chdrlnbIO.setSinstfrom(chdrlnbIO.getBillcd());
			chdrlnbIO.setSinstto(varcom.vrcmMaxDate);
			chdrlnbIO.setSinstamt01(wsaaBillingInformationInner.wsaaRegPremAcc[1]);
			updateHdrCustomerSpecific();
			/*chdrlnbIO.setSinstamt02(wsaaMgfeeRegAmt);
			chdrlnbIO.setSinstamt03(0);
			chdrlnbIO.setSinstamt04(0);
			chdrlnbIO.setSinstamt05(0);
			chdrlnbIO.setSinstamt06(0);
			setPrecision(chdrlnbIO.getSinstamt06(), 2);
			chdrlnbIO.setSinstamt06(add(chdrlnbIO.getSinstamt06(),add(chdrlnbIO.getSinstamt01(),chdrlnbIO.getSinstamt02())));*/
		}
		updtSintamntsCustomerSpecific2493();
		/* Tolerance should not be applied to every instalment.*/
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			xxxxFatalError();
		}
		chdrlnbIO.setFunction(varcom.writs);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			xxxxFatalError();
		}
		/* Get the policy dispatch details                                 */
		readTr52q2900();
		/* Update the HPAD record to show contract now issued              */
 //ILIFE-8709 start
		hpadpf = hpadpfDAO.getHpadData(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		
		hpadpf.setHissdte(datcon1rec.intDate.toInt());
		if (isEQ(hpadpf.getHoissdte(),varcom.vrcmMaxDate)) {
			hpadpf.setHoissdte(datcon1rec.intDate.toInt());
		}
		hpadpf.setZsufcdte(wsaaZsufcdte.toInt());
		if (isEQ(hpadpf.getZsufcdte(),varcom.vrcmMaxDate)) {
			hpadpf.setProcflg("Y");
		}
		else {
			hpadpf.setProcflg("N");
		}
		hpadpf.setValidflag("1");
		updDespatchDetails2950(hpadpf);
		hpadpfDAO.updateRecord(hpadpf);
 //ILIFE-8709 end
	}

protected void updateHdrCustomerSpecific(){
	
	chdrlnbIO.setSinstamt02(wsaaMgfeeRegAmt);
	chdrlnbIO.setSinstamt03(0);
	chdrlnbIO.setSinstamt04(0);
	chdrlnbIO.setSinstamt05(0);
	chdrlnbIO.setSinstamt06(0);
	setPrecision(chdrlnbIO.getSinstamt06(), 2);
	chdrlnbIO.setSinstamt06(add(chdrlnbIO.getSinstamt06(),add(chdrlnbIO.getSinstamt01(),chdrlnbIO.getSinstamt02())));
}

//ILIFE-7505 starts
protected void updateLastPrem2493(Payrpf payrpf){
	Chdrpf chdrObj =null;
	chdrObj=chdrpfDAO.getchdrRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
	
		if (isEQ(payrpf.getBillfreq(),"00")) {
		//Update last premium record for Single Premium Policies
		chdrpfDAO.updateLastPrem(wsaaAmntDue.getbigdata(),chdrObj.getUniqueNumber());
	}
}
//ILIFE-7505 ends

protected void contractAccounting2500()
	{
		nonCashPostings2520();
	}

	/**
	* <pre>
	*2510-CONTRACT-ACCOUNTING.
	* </pre>
	*/
protected void nonCashPostings2520()
	{
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(atmodrec.batchKey);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.frcdate.set(0);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.origamt.set(wsaaTotamnt);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(chdrlnbIO.getOccdate());
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		if (isNE(chdrlnbIO.getInsttot02(),0)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode04);
			lifacmvrec.sacstyp.set(t5645rec.sacstype04);
			lifacmvrec.glcode.set(t5645rec.glmap04);
			lifacmvrec.glsign.set(t5645rec.sign04);
			lifacmvrec.contot.set(t5645rec.cnttot04);
			/* Can not guarantee the first payment is a full instalment.*/
			lifacmvrec.origamt.set(chdrlnbIO.getInsttot02());
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
		/* Post the tolerance at contract level*/
		/* Post using the Suspense currency, which may differ from         */
		/* the Contract currency.                                          */
		lifacmvrec.origamt.set(wsaaTotTolerance);
		if (isNE(lifacmvrec.origamt, 0)) {
			lifacmvrec.origcurr.set(acblpf.getOrigcurr());
			if (isLTE(lifacmvrec.origamt, wsaaTolerance)) {
				lifacmvrec.sacscode.set(t5645rec.sacscode05);
				lifacmvrec.sacstyp.set(t5645rec.sacstype05);
				lifacmvrec.glcode.set(t5645rec.glmap05);
				lifacmvrec.glsign.set(t5645rec.sign05);
				lifacmvrec.contot.set(t5645rec.cnttot05);
			}
			else {
				lifacmvrec.sacscode.set(wsaaT5645Sacscode[14]);
				lifacmvrec.sacstyp.set(wsaaT5645Sacstype[14]);
				lifacmvrec.glcode.set(wsaaT5645Glmap[14]);
				lifacmvrec.glsign.set(wsaaT5645Sign[14]);
				lifacmvrec.contot.set(wsaaT5645Cnttot[14]);
				lifacmvrec.rldgacct.set(chdrlnbIO.getAgntnum());
			}
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
			lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
			/*       PERFORM 2950-LIFACMV-CHECK.                               */
			lifacmvCheck2950();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(lifacmvrec.origamt);
				b100CallZorcompy();
			}
		}
		if(!stampDutyFlagcomp) {

		if (isNE(wsaaStampDutyAcc,0) && isNE(wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()], 0)) {
			lifacmvrec.origamt.set(wsaaStampDutyAcc);
			lifacmvrec.sacscode.set(t5645rec.sacscode06);
			lifacmvrec.sacstyp.set(t5645rec.sacstype06);
			lifacmvrec.glcode.set(t5645rec.glmap06);
			lifacmvrec.glsign.set(t5645rec.sign06);
			lifacmvrec.contot.set(t5645rec.cnttot06);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
			lifacmvrec.sacscode.set(t5645rec.sacscode07);
			lifacmvrec.sacstyp.set(t5645rec.sacstype07);
			lifacmvrec.glcode.set(t5645rec.glmap07);
			lifacmvrec.glsign.set(t5645rec.sign07);
			lifacmvrec.contot.set(t5645rec.cnttot07);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
		}
		/* If the Contract currency differs from the Suspense currency     */
		/* post the amount due in both the contract & suspense currencies. */
		/* But only if an initial premium is required, ie..                */
		/* BTDATE > CCDATE.                                                */
		if ((isNE(chdrlnbIO.getCntcurr(),acblpf.getOrigcurr()))
		&& (isGT(wsaaInstpremTotXge,ZERO))) {
			lifacmvrec.origcurr.set(acblpf.getOrigcurr());
			lifacmvrec.origamt.set(wsaaInstpremTotXge);
			lifacmvrec.contot.set(wsaaT5645Cnttot[12]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[12]);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[12]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[12]);
			lifacmvrec.glsign.set(wsaaT5645Sign[12]);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
			lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
			lifacmvrec.origamt.set(wsaaInstpremTot);
			lifacmvrec.contot.set(wsaaT5645Cnttot[13]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[13]);
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[13]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[13]);
			lifacmvrec.glsign.set(wsaaT5645Sign[13]);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
		/*                                                         <V4L001>*/
		/* This paragraph is required before the PAYER-ACCOUTING so that   */
		/* suspense will have enough money for the amount due. If there    */
		/* has extract suspense for the amount due then NEXT SENTENCE. If  */
		/* Adv Prem Deposit amount (APA) is required to pay the amount due */
		/* then move DELT to withdraw money from APA and put into suspense.*/
		/* Otherwise, take the extra money from suspense and try to post it*/
		/* into APA.                                               <V4L001>*/
		/*                                                         <V4L001>*/
 //ILIFE-8709 start
		
		if (isEQ(wsaaTotamnt1,ZERO)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isGT(wsaaTotamnt1,ZERO)) {
				rlpdlonPojo.setFunction(varcom.delt.toString());
				rlpdlonPojo.setPrmdepst(wsaaTotamnt1.getbigdata());
			}
			else {
				rlpdlonPojo.setFunction(varcom.insr.toString());
				compute(wsaaTotamnt1, 2).set(mult(wsaaTotamnt1,(-1)));
				rlpdlonPojo.setPrmdepst(wsaaTotamnt1.getbigdata());
 //ILIFE-8709 end
			}
			r200UpdateApa();
		}
	}

protected void payerAccounting2550(Payrpf payrpf)
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					payerAccounting2551();
				case nonCashPostings2555:
					nonCashPostings2555();
				case incrementSub2559:
					incrementSub2559(payrpf); //ILIFE-8709
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void payerAccounting2551()
	{
		/* Deduct the amount needed to issue this contract,*/
		/* less the tolerance applied from the suspense A/C.*/
		lifrtrnrec.origamt.set(ZERO);
		if (isEQ(chdrlnbIO.getOccdate(), wsaaBillingInformationInner.wsaaBtdate[wsbbSub.toInt()])
		&& isNE(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()], "00")
		&& isEQ(wsaaBillingInformationInner.wsaaSingPremAcc[wsbbSub.toInt()], ZERO)) {
			goTo(GotoLabel.incrementSub2559);
		}
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.batckey.set(atmodrec.batchKey);
		lifrtrnrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifrtrnrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifrtrnrec.tranno.set(chdrlnbIO.getTranno());
		lifrtrnrec.jrnseq.set(ZERO);
		lifrtrnrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifrtrnrec.sacscode.set(t5645rec.sacscode01);
		/* MOVE CHDRLNB-CNTCURR        TO LIFR-ORIGCURR.                */
		lifrtrnrec.origcurr.set(acblpf.getOrigcurr()); //ILIFE-8709
		if(onePflag) {
			lifrtrnrec.sacstyp.set(wsaaT5645Sacstype[32].toString());
			lifrtrnrec.glcode.set(wsaaT5645Glmap[32].toString());
			lifrtrnrec.glsign.set(wsaaT5645Sign[32].toString());
			lifrtrnrec.contot.set(wsaaT5645Cnttot[32].toString());
		}
		else {
			lifrtrnrec.sacstyp.set(t5645rec.sacstype01);
			lifrtrnrec.glcode.set(t5645rec.glmap01);
			lifrtrnrec.glsign.set(t5645rec.sign01);
			lifrtrnrec.contot.set(t5645rec.cnttot01);
		}
		lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
		lifrtrnrec.trandesc.set(descIO.getLongdesc());
		lifrtrnrec.crate.set(0);
		lifrtrnrec.acctamt.set(0);
		lifrtrnrec.rcamt.set(0);
		lifrtrnrec.user.set(wsaaUser);
		lifrtrnrec.termid.set(wsaaTermid);
		/* COMPUTE LIFR-ORIGAMT        =  WSAA-INSTPREM(WSBB-SUB) -     */
		/*                                WSAA-TOLR-USED(WSBB-SUB).     */
		if (isEQ(chdrlnbIO.getCntcurr(),acblpf.getOrigcurr())) {
			compute(lifrtrnrec.origamt, 2).set(add(add(add(sub(wsaaBillingInformationInner.wsaaInstprem[wsbbSub.toInt()], wsaaBillingInformationInner.wsaaTolrUsed[wsbbSub.toInt()]), wsaaBillingInformationInner.wsaaRpTax[wsbbSub.toInt()]), wsaaBillingInformationInner.wsaaSpTax[wsbbSub.toInt()]), wsaaBillingInformationInner.wsaaFeTax[wsbbSub.toInt()]));
		}
		else {
			compute(lifrtrnrec.origamt, 2).set(sub(wsaaInstpremTotXge, wsaaTotTolerance));
		}
		lifrtrnrec.genlcur.set(SPACES);
		lifrtrnrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifrtrnrec.postyear.set(SPACES);
		lifrtrnrec.postmonth.set(SPACES);
		lifrtrnrec.effdate.set(chdrlnbIO.getOccdate());
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		if (isEQ(lifrtrnrec.origamt, 0)) {
			goTo(GotoLabel.nonCashPostings2555);
		}
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifrtrnrec.lifrtrnRec);
			syserrrec.statuz.set(lifrtrnrec.statuz);
			xxxxFatalError();
		}
	}

protected void nonCashPostings2555()
	{
		if (compLevelAcc.isTrue()) {
			goTo(GotoLabel.incrementSub2559);
		}
		/* Post the single premium.*/
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(atmodrec.batchKey);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		lifacmvrec.jrnseq.set(ZERO);
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.contot.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.frcdate.set(0);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(chdrlnbIO.getOccdate());
		setlifeacmvCustomerSpecific();
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		if (isNE(wsaaBillingInformationInner.wsaaSingPremAcc[wsbbSub.toInt()], 0)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode02);
			lifacmvrec.sacstyp.set(t5645rec.sacstype02);
			lifacmvrec.glcode.set(t5645rec.glmap02);
			lifacmvrec.glsign.set(t5645rec.sign02);
			lifacmvrec.contot.set(t5645rec.cnttot02);
			lifacmvrec.origamt.set(wsaaBillingInformationInner.wsaaSingPremAcc[wsbbSub.toInt()]);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
		/* Post regular premium.*/
		if (isNE(wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()], 0)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode03);
			lifacmvrec.sacstyp.set(t5645rec.sacstype03);
			lifacmvrec.glcode.set(t5645rec.glmap03);
			lifacmvrec.glsign.set(t5645rec.sign03);
			lifacmvrec.contot.set(t5645rec.cnttot03);
			lifacmvrec.origamt.set(wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()]);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
	}

	/**
	* <pre>
	* Post the tolerance needed to issue this contract.
	* Post the tax relief amount
	* </pre>
	*/
protected void incrementSub2559(Payrpf payrpf)
	{
		/*    MOVE WSAA-INREVNUM(WSBB-SUB)    TO LIFA-RLDGACCT.         */
		lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifacmvrec.origamt.set(wsaaBillingInformationInner.wsaaTaxrelamt[wsbbSub.toInt()]);
		if (isNE(lifacmvrec.origamt, 0)) {
			lifacmvrec.sacscode.set(wsaaT5645Sacscode[3]);
			lifacmvrec.sacstyp.set(wsaaT5645Sacstype[3]);
			lifacmvrec.glcode.set(wsaaT5645Glmap[3]);
			lifacmvrec.glsign.set(wsaaT5645Sign[3]);
			lifacmvrec.contot.set(wsaaT5645Cnttot[3]);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
		}
		/* If this is a flexible premium contract then write       <D9604>*/
		/* an FPRM record at payer level                           <D9604>*/
		if (flexiblePremiumContract.isTrue()) {
			writeFprmRec6500(payrpf); //ILIFE-8709
		}
		wsbbSub.add(1);
	}

protected void commissionAccounting2600()
	{
		commissionAccounting2610();
		doPostings2630();
		postEachAgent2640();
	}

protected void commissionAccounting2610()
	{
		lifacmvrec.function.set("PSTW");
		lifacmvrec.batckey.set(atmodrec.batchKey);
		lifacmvrec.transactionTime.set(wsaaTransactionTime);
		lifacmvrec.transactionDate.set(wsaaTransactionDate);
		lifacmvrec.termid.set(wsaaTermid);
		lifacmvrec.user.set(wsaaUser);
		lifacmvrec.rdocnum.set(chdrlnbIO.getChdrnum());
		lifacmvrec.rldgcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.origcurr.set(chdrlnbIO.getCntcurr());
		lifacmvrec.tranno.set(chdrlnbIO.getTranno());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.origamt.set(wsaaTotamnt);
		lifacmvrec.genlcur.set(SPACES);
		lifacmvrec.genlcoy.set(chdrlnbIO.getChdrcoy());
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.effdate.set(chdrlnbIO.getOccdate());
		setlifeacmvCustomerSpecific();
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.substituteCode[1].set(chdrlnbIO.getCnttype());
		wsaaJrnseq.set(0);
	}

protected void doPostings2630()
	{
		/*    IF WSAA-COMM-DUE(WSAA-AGENT-SUB) NOT = 0                     */
		if (!compLevelAcc.isTrue()
		&& isNE(wsaaAgentsInner.wsaaCommDue[wsaaAgentSub.toInt()], 0)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode08);
			lifacmvrec.sacstyp.set(t5645rec.sacstype08);
			lifacmvrec.glcode.set(t5645rec.glmap08);
			lifacmvrec.glsign.set(t5645rec.sign08);
			lifacmvrec.contot.set(t5645rec.cnttot08);
			lifacmvrec.rldgacct.set(wsaaAgentsInner.wsaaAgntnum[wsaaAgentSub.toInt()]);
			lifacmvrec.origamt.set(wsaaAgentsInner.wsaaCommDue[wsaaAgentSub.toInt()]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			/*       PERFORM 2950-LIFACMV-CHECK.                               */
			lifacmvCheck2950();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(wsaaAgentsInner.wsaaAnnprem[wsaaAgentSub.toInt()]);
				b100CallZorcompy();
			}
		}
		if (!compLevelAcc.isTrue()) {
			if (isNE(wsaaAgentsInner.wsaaCommEarn[wsaaAgentSub.toInt()], 0)) {
				lifacmvrec.sacscode.set(t5645rec.sacscode09);
				lifacmvrec.sacstyp.set(t5645rec.sacstype09);
				lifacmvrec.glcode.set(t5645rec.glmap09);
				lifacmvrec.glsign.set(t5645rec.sign09);
				lifacmvrec.contot.set(t5645rec.cnttot09);
				lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
				lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
				lifacmvrec.origamt.set(wsaaAgentsInner.wsaaCommEarn[wsaaAgentSub.toInt()]);
				wsaaJrnseq.add(1);
				lifacmvrec.jrnseq.set(wsaaJrnseq);
				callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
				lifacmvCheck2950();
			}
		}
		if (!compLevelAcc.isTrue()) {
			if (isNE(wsaaAgentsInner.wsaaCommPaid[wsaaAgentSub.toInt()], 0)) {
				lifacmvrec.sacscode.set(t5645rec.sacscode10);
				lifacmvrec.sacstyp.set(t5645rec.sacstype10);
				lifacmvrec.glcode.set(t5645rec.glmap10);
				lifacmvrec.glsign.set(t5645rec.sign10);
				lifacmvrec.contot.set(t5645rec.cnttot10);
				lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
				lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
				lifacmvrec.origamt.set(wsaaAgentsInner.wsaaCommPaid[wsaaAgentSub.toInt()]);
				wsaaJrnseq.add(1);
				lifacmvrec.jrnseq.set(wsaaJrnseq);
				callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
				lifacmvCheck2950();
			}
		}
		if (isNE(wsaaAgentsInner.wsaaServDue[wsaaAgentSub.toInt()], 0)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode11);
			lifacmvrec.sacstyp.set(t5645rec.sacstype11);
			lifacmvrec.glcode.set(t5645rec.glmap11);
			lifacmvrec.glsign.set(t5645rec.sign11);
			lifacmvrec.contot.set(t5645rec.cnttot11);
			lifacmvrec.rldgacct.set(wsaaAgentsInner.wsaaAgntnum[wsaaAgentSub.toInt()]);
			lifacmvrec.origamt.set(wsaaAgentsInner.wsaaServDue[wsaaAgentSub.toInt()]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			/*       PERFORM 2950-LIFACMV-CHECK.                               */
			lifacmvCheck2950();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(wsaaAgentsInner.wsaaAnnprem[wsaaAgentSub.toInt()]);
				b100CallZorcompy();
			}
		}
		if (!compLevelAcc.isTrue()) {
			if (isNE(wsaaAgentsInner.wsaaServEarn[wsaaAgentSub.toInt()], 0)) {
				lifacmvrec.sacscode.set(t5645rec.sacscode12);
				lifacmvrec.sacstyp.set(t5645rec.sacstype12);
				lifacmvrec.glcode.set(t5645rec.glmap12);
				lifacmvrec.glsign.set(t5645rec.sign12);
				lifacmvrec.contot.set(t5645rec.cnttot12);
				lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
				lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
				lifacmvrec.origamt.set(wsaaAgentsInner.wsaaServEarn[wsaaAgentSub.toInt()]);
				wsaaJrnseq.add(1);
				lifacmvrec.jrnseq.set(wsaaJrnseq);
				callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
				lifacmvCheck2950();
			}
		}
		if (isNE(wsaaAgentsInner.wsaaRenlDue[wsaaAgentSub.toInt()], 0)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode13);
			lifacmvrec.sacstyp.set(t5645rec.sacstype13);
			lifacmvrec.glcode.set(t5645rec.glmap13);
			lifacmvrec.glsign.set(t5645rec.sign13);
			lifacmvrec.contot.set(t5645rec.cnttot13);
			lifacmvrec.rldgacct.set(wsaaAgentsInner.wsaaAgntnum[wsaaAgentSub.toInt()]);
			lifacmvrec.origamt.set(wsaaAgentsInner.wsaaRenlDue[wsaaAgentSub.toInt()]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			wsaaJrnseq.add(1);
			wsaaRldgChdrnum.set(covrlnbIO.getChdrnum());
			wsaaRldgLife.set(covrlnbIO.getLife());
			wsaaRldgCoverage.set(covrlnbIO.getCoverage());
			wsaaRldgRider.set(covrlnbIO.getRider());
			wsaaPlan.set(covrlnbIO.getPlanSuffix());
			wsaaRldgPlanSuffix.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			/*       PERFORM 2950-LIFACMV-CHECK.                               */
			lifacmvCheck2950();
			/* Override Commission                                             */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(wsaaAgentsInner.wsaaAnnprem[wsaaAgentSub.toInt()]);
				b100CallZorcompy();
			}
		}
		if (!compLevelAcc.isTrue()) {
			if (isNE(wsaaAgentsInner.wsaaRenlEarn[wsaaAgentSub.toInt()], 0)) {
				lifacmvrec.sacscode.set(t5645rec.sacscode14);
				lifacmvrec.sacstyp.set(t5645rec.sacstype14);
				lifacmvrec.glcode.set(t5645rec.glmap14);
				lifacmvrec.glsign.set(t5645rec.sign14);
				lifacmvrec.contot.set(t5645rec.cnttot14);
				lifacmvrec.rldgacct.set(chdrlnbIO.getChdrnum());
				lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
				lifacmvrec.origamt.set(wsaaAgentsInner.wsaaRenlEarn[wsaaAgentSub.toInt()]);
				wsaaJrnseq.add(1);
				lifacmvrec.jrnseq.set(wsaaJrnseq);
				callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
				lifacmvCheck2950();
			}
		}
		/* Single Premium Commission posting                            */
		if (!compLevelAcc.isTrue()) {
			if (isNE(wsaaAgentsInner.wsaaTcomDue[wsaaAgentSub.toInt()], 0)) {
				lifacmvrec.sacscode.set(wsaaT5645Sacscode[15]);
				lifacmvrec.sacstyp.set(wsaaT5645Sacstype[15]);
				lifacmvrec.glcode.set(wsaaT5645Glmap[15]);
				lifacmvrec.glsign.set(wsaaT5645Sign[15]);
				lifacmvrec.contot.set(wsaaT5645Cnttot[15]);
				lifacmvrec.rldgacct.set(wsaaAgentsInner.wsaaAgntnum[wsaaAgentSub.toInt()]);
				lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
				lifacmvrec.origamt.set(wsaaAgentsInner.wsaaTcomDue[wsaaAgentSub.toInt()]);
				wsaaJrnseq.add(1);
				lifacmvrec.jrnseq.set(wsaaJrnseq);
				callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
				/*           PERFORM 2950-LIFACMV-CHECK.                   <V6L000>*/
				lifacmvCheck2950();
				/* Override Commission                                             */
				if (isEQ(th605rec.indic, "Y")) {
					comlinkrec.annprem.set(wsaaAgentsInner.wsaaSingp[wsaaAgentSub.toInt()]);
					b100CallZorcompy();
			}
		}
		}
		/* OVERIDING COMMISSION TO BE ADDED.*/
		if (!compLevelAcc.isTrue()) {
			for (wsaaSub.set(1); !(isGT(wsaaSub,10)); wsaaSub.add(1)){
				overrideCommission2691();
			}
		}
	}

protected void postEachAgent2640()
	{
		for (wsaaAgentSub.set(2); !(isGT(wsaaAgentSub,10)); wsaaAgentSub.add(1)){
			doPostings2630();
		}
		/*EXIT*/
	}

protected void overrideCommission2691()
	{
		start2692();
	}

protected void start2692()
	{
		/*    Has now been added...... as follows...*/
		if (isNE(wsaaOverrideComm[wsaaAgentSub.toInt()][wsaaSub.toInt()],ZERO)) {
			lifacmvrec.sacscode.set(t5645rec.sacscode15);
			lifacmvrec.sacstyp.set(t5645rec.sacstype15);
			lifacmvrec.glcode.set(t5645rec.glmap15);
			lifacmvrec.glsign.set(t5645rec.sign15);
			lifacmvrec.contot.set(t5645rec.cnttot15);
			lifacmvrec.rldgacct.set(wsaaOverrideAgntnum[wsaaAgentSub.toInt()][wsaaSub.toInt()]);
			lifacmvrec.origamt.set(wsaaOverrideComm[wsaaAgentSub.toInt()][wsaaSub.toInt()]);
			lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
			wsaaJrnseq.add(1);
			lifacmvrec.jrnseq.set(wsaaJrnseq);
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
			lifacmvCheck2950();
			if (!compLevelAcc.isTrue()) {
				lifacmvrec.sacscode.set(wsaaT5645Sacscode[1]);
				lifacmvrec.sacstyp.set(wsaaT5645Sacstype[1]);
				lifacmvrec.glcode.set(wsaaT5645Glmap[1]);
				lifacmvrec.glsign.set(wsaaT5645Sign[1]);
				lifacmvrec.contot.set(wsaaT5645Cnttot[1]);
				lifacmvrec.rldgacct.set(covrlnbIO.getChdrnum());
				/*        MOVE WSAA-OVERRIDE-COMM(WSAA-AGENT-SUB WSAA-SUB)      */
				/*                                      TO LIFA-ORIGAMT         */
				compute(lifacmvrec.origamt, 2).set(sub(wsaaOverrideComm[wsaaAgentSub.toInt()][wsaaSub.toInt()], wsaaOvrdCommPaid[wsaaAgentSub.toInt()][wsaaSub.toInt()]));
				/*        MOVE COVRLNB-CHDRNUM        TO LIFA-TRANREF           */
				/*        ADD  +1                     TO WSAA-JRNSEQ            */
				/*        MOVE WSAA-JRNSEQ            TO LIFA-JRNSEQ            */
				/*        CALL 'LIFACMV'           USING LIFA-LIFACMV-REC       */
				/*        PERFORM 2950-LIFACMV-CHECK.                           */
				if (isNE(lifacmvrec.origamt, ZERO)) {
					lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
					wsaaJrnseq.add(1);
					lifacmvrec.jrnseq.set(wsaaJrnseq);
					callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
					lifacmvCheck2950();
				}
			}
		}
		if (!compLevelAcc.isTrue()) {
			if (isNE(wsaaOvrdCommPaid[wsaaAgentSub.toInt()][wsaaSub.toInt()],ZERO)) {
				lifacmvrec.sacscode.set(wsaaT5645Sacscode[2]);
				lifacmvrec.sacstyp.set(wsaaT5645Sacstype[2]);
				lifacmvrec.glcode.set(wsaaT5645Glmap[2]);
				lifacmvrec.glsign.set(wsaaT5645Sign[2]);
				lifacmvrec.contot.set(wsaaT5645Cnttot[2]);
				lifacmvrec.rldgacct.set(covrlnbIO.getChdrnum());
				lifacmvrec.origamt.set(wsaaOvrdCommPaid[wsaaAgentSub.toInt()][wsaaSub.toInt()]);
				lifacmvrec.tranref.set(covrlnbIO.getChdrnum());
				wsaaJrnseq.add(1);
				lifacmvrec.jrnseq.set(wsaaJrnseq);
				callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
				lifacmvCheck2950();
			}
		}
	}

	/**
	* <pre>
	*2700-REINSURANCE-ACCOUNTING SECTION.
	*2710-REINSURANCE-ACCOUNTING.
	* To be added out when Steve Hathaway has sorted it out..
	*2790-EXIT.
	*    EXIT.
	* </pre>
	*/
protected void housekeeping2800()
	{
		/*HOUSEKEEPING*/
		writePtrn2810();
		updateBatchHeader2820();
		/*EXIT*/
	}

protected void m800UpdateMlia()
	{
		m800Start();
	}

protected void m800Start()
	{
		rlliadbrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		rlliadbrec.chdrnum.set(chdrlnbIO.getChdrnum());
		rlliadbrec.fsucoy.set(wsaaFsuCoy);
 //ILIFE-8709 start
		wsaaBatcKey.set(atmodrec.batchKey);
		rlliadbrec.batctrcde.set(ptrnpf.getBatctrcde());
 //ILIFE-8709 end
		rlliadbrec.language.set(atmodrec.language);
		callProgram(Rlliadb.class, rlliadbrec.rlliaRec);
		if (isNE(rlliadbrec.statuz,varcom.oK)) {
			syserrrec.params.set(rlliadbrec.rlliaRec);
			syserrrec.statuz.set(rlliadbrec.statuz);
			xxxxFatalError();
		}
	}

protected void writePtrn2810()
	{
		writePtrn2811();
	}

protected void writePtrn2811()
	{
 //ILIFE-8709 start	
		ptrnpf = new Ptrnpf();
		wsaaBatcKey.set(atmodrec.batchKey);
		ptrnpf.setTermid(wsaaTermid.toString());
		ptrnpf.setTrdt(wsaaTransactionDate.toInt());
		ptrnpf.setTrtm(wsaaTransactionTime.toInt());
		ptrnpf.setUserT(wsaaUser.toInt());
		ptrnpf.setBatcpfx(wsaaBatcKey.batcBatcpfx.toString());
		ptrnpf.setBatccoy(wsaaBatcKey.batcBatccoy.toString());
		ptrnpf.setBatcbrn(wsaaBatcKey.batcBatcbrn.toString());
		ptrnpf.setBatcactyr(wsaaBatcKey.batcBatcactyr.toInt());
		ptrnpf.setBatctrcde(wsaaBatcKey.batcBatctrcde.toString());
		ptrnpf.setBatcactmn(wsaaBatcKey.batcBatcactmn.toInt());
		ptrnpf.setBatcbatch(wsaaBatcKey.batcBatcbatch.toString());
		ptrnpf.setTranno(chdrlnbIO.getTranno().toInt());
		/*  MOVE CHDRLNB-OCCDATE        TO PTRN-PTRNEFF.                 */
		ptrnpf.setPtrneff(datcon1rec.intDate.toInt());
		ptrnpf.setDatesub(datcon1rec.intDate.toInt());
		ptrnpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		ptrnpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		ptrnpf.setValidflag("1");//ILIFE-7929
		List<Ptrnpf> ptrnList = new ArrayList();
		ptrnList.add(ptrnpf);
		boolean result = ptrnpfDAO.insertPtrnPF(ptrnList);
		if (!result) {
			syserrrec.params.set(ptrnpf.getChdrcoy().concat(ptrnpf.getChdrnum()));
			xxxxFatalError();
		}
 //ILIFE-8709 end
	}

protected void updateBatchHeader2820()
	{
		/*UPDATE-BATCH-HEADER*/
		batcuprec.batcupRec.set(SPACES);
		batcuprec.batchkey.set(atmodrec.batchKey);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.function.set(varcom.writs);
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz,varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*2830-POLICY-ROUTINE SECTION.
	*2831-POLICY-ROUTINE.
	* To be added.
	*2839-EXIT.
	*    EXIT.
	* </pre>
	*/
protected void rlseSoftlock2840()
	{
		rlseSoftlock2841();
	}

protected void rlseSoftlock2841()
	{
		/* Release the soft lock on the contract.*/
 //ILIFE-8709 start
		sftlockRecBean.setFunction("UNLK");
		sftlockRecBean.setCompany(atmodrec.company.toString());
		sftlockRecBean.setEnttyp("CH");
		sftlockRecBean.setEntity(chdrlnbIO.getChdrnum().toString());
		sftlockRecBean.setTransaction(atmodrec.batchKey.toString());
		sftlockRecBean.setStatuz(SPACES.stringValue());
		sftlockUtil.process(sftlockRecBean, appVars);
		if (!varcom.oK.toString().equals(sftlockRecBean.getStatuz())) {
				syserrrec.statuz.set(sftlockRecBean.getStatuz());
				xxxxFatalError();
 //ILIFE-8709 end
		}
	}

protected void readTr52q2900()
	{
		begin2900();
	}

protected void begin2900()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52q);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
			itemIO.setItemtabl(tablesInner.tr52q);
			itemIO.setItemitem("***");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				xxxxFatalError();
			}
			else {
				tr52qrec.tr52qRec.set(itemIO.getGenarea());
			}
		}
		else {
			tr52qrec.tr52qRec.set(itemIO.getGenarea());
		}
	}

protected void updDespatchDetails2950(Hpadpf hpadpf)
	{
		begin2950(hpadpf);
	}

protected void begin2950(Hpadpf hpadpf)
	{
 //ILIFE-8709 start
		hpadpf.setDespdate(hpadpf.getHissdte());
		hpadpf.setPackdate(varcom.vrcmMaxDate.toInt());
		hpadpf.setRemdte(varcom.vrcmMaxDate.toInt());
		hpadpf.setDeemdate(varcom.vrcmMaxDate.toInt());
		hpadpf.setNxtdte(varcom.vrcmMaxDate.toInt());
		if (isEQ(hpadpf.getDlvrmode(), SPACES)) {
			hpadpf.setDlvrmode(tr52qrec.dlvrmode01.toString());
		}
		/* Check whether the delivery mode is excluded from acknowledgement*/
		/* process                                                         */
		wsaaFound.set("N");
		for (ix.set(2); !(isGT(ix, 9)
		|| found.isTrue()); ix.add(1)){
			if (isEQ(tr52qrec.dlvrmode[ix.toInt()], hpadpf.getDlvrmode())) {
				wsaaFound.set("Y");
			}
		}
		if (found.isTrue()) {
			hpadpf.setIncexc("E");
		}
		else {
			hpadpf.setIncexc("I");
			
			datcon2rec.frequency.set("DY");
			datcon2rec.intDate1.set(hpadpf.getDespdate());
			datcon2rec.freqFactor.set(tr52qrec.daexpy01);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				xxxxFatalError();
			}
			hpadpf.setNxtdte(datcon2rec.intDate2.toInt());
 //ILIFE-8709 end
		}
	}

protected void lifacmvCheck2950()
	{
		/*CHECK*/
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			syserrrec.params.set(lifacmvrec.lifacmvRec);
			syserrrec.statuz.set(lifacmvrec.statuz);
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void commissionHold2a00()
	{
		/*A00-BEGIN*/
		if (isEQ(th605rec.comind, "Y")
		&& isEQ(hpadpf.getIncexc(), "I")) {
			updateAgpy2b00();
		}
		/*A00-EXIT*/
	}

protected void updateAgpy2b00()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begin2b00();
				case call2b00:
					call2b00();
				case next2b00:
					next2b00();
				case exit2b00:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin2b00()
	{
		agpydocIO.setParams(SPACES);
		agpydocIO.setBatccoy(hpadpf.getChdrcoy()); //ILIFE-8709
		agpydocIO.setRdocnum(hpadpf.getChdrnum()); //ILIFE-8709
		agpydocIO.setTranno(0);
		agpydocIO.setJrnseq(0);
		agpydocIO.setFormat(formatsInner.agpydocrec);
		agpydocIO.setFunction(varcom.begn);
	}

protected void call2b00()
	{
		SmartFileCode.execute(appVars, agpydocIO);
		if (isNE(agpydocIO.getStatuz(), varcom.oK)
		&& isNE(agpydocIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(agpydocIO.getStatuz());
			syserrrec.params.set(agpydocIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(agpydocIO.getStatuz(), varcom.endp)
		|| isNE(agpydocIO.getBatccoy(), hpadpf.getChdrcoy())
		|| isNE(agpydocIO.getRdocnum(), hpadpf.getChdrnum())) {
			agpydocIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2b00);
		}
		if (isNE(agpydocIO.getTranno(), chdrlnbIO.getTranno())) {
			goTo(GotoLabel.next2b00);
		}
		if (isNE(agpydocIO.getEffdate(), varcom.vrcmMaxDate)) {
			agpyagtIO.setParams(SPACES);
			agpyagtIO.setRrn(agpydocIO.getRrn());
			agpyagtIO.setFormat(formatsInner.agpyagtrec);
			agpyagtIO.setFunction(varcom.readd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				xxxxFatalError();
			}
			agpyagtIO.setEffdate(varcom.vrcmMaxDate);
			agpyagtIO.setFunction(varcom.writd);
			SmartFileCode.execute(appVars, agpyagtIO);
			if (isNE(agpyagtIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(agpyagtIO.getParams());
				syserrrec.statuz.set(agpyagtIO.getStatuz());
				xxxxFatalError();
			}
		}
	}

protected void next2b00()
	{
		agpydocIO.setFunction(varcom.nextr);
		goTo(GotoLabel.call2b00);
	}

protected void underwritingUndl2x00()
	{
		/*X00-INIT*/
		/* Read first Underwriting Life record                             */
 //ILIFE-8709 start	
		undlpfList = undlpfDAO.checkUndlpf(atmodrec.company.toString(), chdrlnbIO.getChdrnum().toString());
		if(undlpfList != null) {
			for(Undlpf undlpf : undlpfList){
				undlpf.setValidflag("1");
				undlpf.setTranno(chdrlnbIO.getTranno().toInt());//IBPLIFE-8671
				undlpfDAO.updateUndlTranno(undlpfList);
			}
		}
	
		
 //ILIFE-8709 end
		/*X09-EXIT*/
	}

protected void underwritingUndc2y00()
	{
		/*Y00-INIT*/
		/* Read first Underwriting Component                               */
		List <Undcpf> undcpfList;
		undcpfList = undcpfDAO.searchUndcpfRecord(atmodrec.company.toString(),chdrlnbIO.getChdrnum().toString());
		
		if(undcpfList != null) {
			for(Undcpf undcpf : undcpfList){
				undcpf.setValidflag("1".charAt(0));
				undcpf.setTranno(chdrlnbIO.getTranno().toInt());
				undcpfDAO.updateUndcTranno(undcpfList);
			}
		}
		
		/*Y09-EXIT*/
	}


protected void underwritingUndq2z00()
	{
		/*Z00-INIT*/
		/* Read first Underwriting Question                                */
		List <Undqpf> undqpfList;
		undqpfList = undqpfDAO.searchUndqpfRecord(atmodrec.company.toString(),chdrlnbIO.getChdrnum().toString());
		
		if(undqpfList != null) {
			for(Undqpf undqpf : undqpfList){
				undqpf.setValidflag("1");
				undqpf.setTranno(chdrlnbIO.getTranno().toInt());
				undqpfDAO.updateUndqTranno(undqpfList);
			}
		}
		
		/*Z09-EXIT*/
	}


 //ILIFE-8709 end
protected void initialiseCovrPrem3000()
	{
		/*PARA*/
		wsaaCovtInstprem[wsaaL.toInt()][wsaaC.toInt()].set(0);
		wsaaCovtSingp[wsaaL.toInt()][wsaaC.toInt()].set(0);
		wsaaC.add(1);
		if (isGT(wsaaC,99)) {
			wsaaL.add(1);
			wsaaC.set(1);
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     EXIT.
	*4000-CHECK-FOR-REASSURANCE SECTION.                      <R96REA>
	*4000-START.                                              <R96REA>
	****                                                      <R96REA>
	**** MOVE SPACES                 TO RACTLNB-PARAMS.       <R96REA>
	**** MOVE CHDRLNB-CHDRCOY        TO RACTLNB-CHDRCOY.      <R96REA>
	**** MOVE CHDRLNB-CHDRNUM        TO RACTLNB-CHDRNUM.      <R96REA>
	**** MOVE COVRLNB-LIFE           TO RACTLNB-LIFE.         <R96REA>
	**** MOVE COVRLNB-COVERAGE       TO RACTLNB-COVERAGE.     <R96REA>
	**** MOVE COVRLNB-RIDER          TO RACTLNB-RIDER.        <R96REA>
	**** MOVE BEGN                   TO RACTLNB-FUNCTION.     <R96REA>
	**** MOVE RACTLNBREC             TO RACTLNB-FORMAT.       <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTLNBIO'            USING RACTLNB-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           NOT = O-K                <R96REA>
	****    AND RACTLNB-STATUZ       NOT = ENDP               <R96REA>
	****     MOVE RACTLNB-PARAMS     TO SYSR-PARAMS           <R96REA>
	****     MOVE RACTLNB-STATUZ     TO SYSR-STATUZ           <R96REA>
	****     PERFORM XXXX-FATAL-ERROR                         <R96REA>
	**** END-IF.                                              <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-CHDRCOY          NOT = CHDRLNB-CHDRCOY    <R96REA>
	****    OR RACTLNB-CHDRNUM       NOT = CHDRLNB-CHDRNUM    <R96REA>
	****    OR RACTLNB-LIFE          NOT = COVRLNB-LIFE       <R96REA>
	****    OR RACTLNB-COVERAGE      NOT = COVRLNB-COVERAGE   <R96REA>
	****    OR RACTLNB-RIDER         NOT = COVRLNB-RIDER      <R96REA>
	****    OR RACTLNB-STATUZ        = ENDP                   <R96REA>
	****     MOVE ENDP               TO RACTLNB-STATUZ        <R96REA>
	**** END-IF.                                              <R96REA>
	****                                                      <R96REA>
	**** PERFORM 4100-PROCESS-RACTS  UNTIL RACTLNB-STATUZ = EN<R96REA>
	*4090-EXIT.                                               <R96REA>
	**** EXIT.                                                <R96REA>
	*4100-PROCESS-RACTS SECTION.                              <R96REA>
	*4110-START.                                              <R96REA>
	* Lock RACT record prior to updating validflag & tranno
	**** MOVE READH                  TO RACTLNB-FUNCTION.     <R96REA>
	**** MOVE RACTLNBREC             TO RACTLNB-FORMAT.       <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTLNBIO'            USING RACTLNB-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           NOT = O-K                <R96REA>
	****     MOVE RACTLNB-PARAMS     TO SYSR-PARAMS           <R96REA>
	****     MOVE RACTLNB-STATUZ     TO SYSR-STATUZ           <R96REA>
	****     PERFORM XXXX-FATAL-ERROR                         <R96REA>
	**** END-IF.                                              <R96REA>
	* Update validflag & tranno & REWRT RACT record
	**** MOVE '1'                    TO RACTLNB-VALIDFLAG.    <R96REA>
	**** MOVE CHDRLNB-TRANNO         TO RACTLNB-TRANNO.       <R96REA>
	**** MOVE VRCM-DATE              TO RACTLNB-TRANSACTION-DA<R96REA>
	**** MOVE VRCM-TIME              TO RACTLNB-TRANSACTION-TI<R96REA>
	**** MOVE VRCM-USER              TO RACTLNB-USER.         <R96REA>
	**** MOVE VRCM-TERMID            TO RACTLNB-TERMID.       <R96REA>
	****                                                      <R96REA>
	**** MOVE REWRT                  TO RACTLNB-FUNCTION.     <R96REA>
	**** MOVE RACTLNBREC             TO RACTLNB-FORMAT.       <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTLNBIO'            USING RACTLNB-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           NOT = O-K                <R96REA>
	****     MOVE RACTLNB-PARAMS     TO SYSR-PARAMS           <R96REA>
	****     MOVE RACTLNB-STATUZ     TO SYSR-STATUZ           <R96REA>
	****     PERFORM XXXX-FATAL-ERROR                         <R96REA>
	**** END-IF.                                              <R96REA>
	* Look for next RACT record associated with this component
	**** MOVE NEXTR                  TO RACTLNB-FUNCTION.     <R96REA>
	**** MOVE RACTLNBREC             TO RACTLNB-FORMAT.       <R96REA>
	****                                                      <R96REA>
	**** CALL 'RACTLNBIO'            USING RACTLNB-PARAMS.    <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-STATUZ           NOT = O-K                <R96REA>
	****    AND RACTLNB-STATUZ       NOT = ENDP               <R96REA>
	****     MOVE RACTLNB-PARAMS     TO SYSR-PARAMS           <R96REA>
	****     MOVE RACTLNB-STATUZ     TO SYSR-STATUZ           <R96REA>
	****     PERFORM XXXX-FATAL-ERROR                         <R96REA>
	**** END-IF.                                              <R96REA>
	****                                                      <R96REA>
	**** IF RACTLNB-CHDRCOY          NOT = CHDRLNB-CHDRCOY    <R96REA>
	****    OR RACTLNB-CHDRNUM       NOT = CHDRLNB-CHDRNUM    <R96REA>
	****    OR RACTLNB-LIFE          NOT = COVRLNB-LIFE       <R96REA>
	****    OR RACTLNB-COVERAGE      NOT = COVRLNB-COVERAGE   <R96REA>
	****    OR RACTLNB-RIDER         NOT = COVRLNB-RIDER      <R96REA>
	****    OR RACTLNB-STATUZ        = ENDP                   <R96REA>
	****     MOVE ENDP               TO RACTLNB-STATUZ        <R96REA>
	**** END-IF.                                              <R96REA>
	*4190-EXIT.                                               <R96REA>
	**** EXIT.                                                <R96REA>
	* </pre>
	*/
protected void checkForReassurance4000()
	{
		check4010();
	}

protected void check4010()
	{
		if (isNE(covrlnbIO.getLife(),wsaaLife)) {
			readLife4100();
			wsaaLife.set(covrlnbIO.getLife());
		}
		actvresrec.actvresRec.set(SPACES);
		actvresrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		actvresrec.chdrnum.set(chdrlnbIO.getChdrnum());
		actvresrec.cnttype.set(chdrlnbIO.getCnttype());
		actvresrec.currency.set(chdrlnbIO.getCntcurr());
		actvresrec.tranno.set(chdrlnbIO.getTranno());
		actvresrec.life.set(covtlnb.getLife());
		actvresrec.coverage.set(covtlnb.getCoverage());
		actvresrec.rider.set(covtlnb.getRider());
		actvresrec.planSuffix.set(ZERO);
		actvresrec.crtable.set(covtlnb.getCrtable());
		actvresrec.effdate.set(covtlnb.getEffdate());
		actvresrec.clntcoy.set(wsaaFsuCoy);
		actvresrec.l1Clntnum.set(wsaaL1Clntnum);
		actvresrec.jlife.set(covtlnb.getJlife());
		if (isNE(wsaaL2Clntnum,SPACES)) {
			actvresrec.l2Clntnum.set(wsaaL2Clntnum);
		}
		actvresrec.oldSumins.set(ZERO);
		actvresrec.newSumins.set(covtlnb.getSumins());
		actvresrec.crrcd.set(covtlnb.getEffdate());
		actvresrec.language.set(atmodrec.language);
		actvresrec.function.set("ACT8");
		actvresrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Actvres.class, actvresrec.actvresRec);
		if (isNE(actvresrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(actvresrec.statuz);
			syserrrec.params.set(actvresrec.actvresRec);
			xxxxFatalError();
		}
	}

protected void readLife4100()
	{
		life4110();
	}

	/**
	* <pre>
	************************                                  <R96REA>
	* </pre>
	*/
protected void life4110()
	{
 //ILIFE-8709 start
		lifepf.setChdrcoy(atmodrec.company.toString());
		lifepf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		lifepf.setLife(covrlnbIO.getLife().toString());
		lifepf.setJlife("00");
		lifepf = lifepfDAO.getLifepf(lifepf);
		if(lifepf==null){
			syserrrec.params.set(chdrlnbIO.getChdrnum());
			xxxxFatalError();
		}
		
		wsaaL1Clntnum.set(lifepf.getLifcnum());
		
		lifepfList = lifepfDAO.getLifeData(atmodrec.company.toString(),chdrlnbIO.getChdrnum().toString(), covrlnbIO.getLife().toString(), "01");
		if (!(lifepfList.isEmpty())){
			wsaaL2Clntnum.set(lifepfList.get(0).getLifcnum());
		}
 //ILIFE-8709 end
		else {
			wsaaL2Clntnum.set(SPACES);
		}
	}

	protected void updateExcl() {
		List<Exclpf> exclpflist = exclpfDAO.getRecordByContract(chdrlnbIO.getChdrnum().toString());
		for (int i = 0; i < exclpflist.size(); i++) {
			Exclpf exclpf = exclpflist.get(i);
			if(isEQ(exclpf.getValidflag(),"3")) { //PINNACLE-2855
			exclpf.setTranno(chdrlnbIO.getTranno().toInt());
			exclpfDAO.updateStatus(exclpf);
			}
		}
	
	}
	
	protected void writeLetter5000() {
		readTr3845010();
		updateExcl();
	}

	/**
	* <pre>
	*5010-READ-T6634.                                         <PCPPRT>
	* </pre>
	*/
protected void readTr3845010()
	{
		/*  Get the Letter type from T6634.                                */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlnbIO.getChdrcoy());
		/* MOVE T6634                      TO ITEM-ITEMTABL.    <PCPPRT>*/
		itemIO.setItemtabl(tablesInner.tr384);
		/*  Build key to T6634 from contract type & transaction code.      */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrlnbIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(g437);
			xxxxFatalError();
		}
		/* if not found, read again using generic key.                     */
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("***", SPACES);
			stringVariable2.addExpression(wsaaBatckey.batcBatctrcde, SPACES);
			stringVariable2.setStringInto(itemIO.getItemitem());
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(g437);
				xxxxFatalError();
			}
		}
		/* MOVE ITEM-GENAREA               TO T6634-T6634-REC.  <PCPPRT>*/
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF  T6634-LETTER-TYPE           = SPACES             <PCPPRT>*/
		if (isEQ(tr384rec.letterType,SPACES)) {
			return ;
		}
		/*  Write LETC via LETRQST.                                        */
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrlnbIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.  <PCPPRT>*/
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(chdrlnbIO.getOccdate());
		letrqstrec.clntcoy.set(chdrlnbIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlnbIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrlnbIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlnbIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlnbIO.getChdrnum());
		letrqstrec.tranno.set(chdrlnbIO.getTranno());
		letrqstrec.otherKeys.set(SPACES);
		/* MOVE SPACES                 TO WSAA-LETOKEYS.        <PCPRT> */
		/* MOVE CHDRLNB-CHDRNUM        TO WSAA-LT-CHDRNUM.      <PCPRT> */
		/* MOVE ATMD-LANGUAGE          TO WSAA-LT-LANGUAGE.     <PCPRT> */
		/* MOVE WSAA-LETOKEYS          TO LETRQST-OTHER-KEYS.   <PCPRT> */
		letrqstrec.otherKeys.set(atmodrec.language);
		letrqstrec.rdocpfx.set(chdrlnbIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlnbIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlnbIO.getChdrnum());
		letrqstrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlnbIO.getChdrnum());
		letrqstrec.branch.set(chdrlnbIO.getCntbranch());
		letrqstrec.trcde.set(wsaaBatckey.batcBatctrcde);//IBPTE-1453
		wsaaLetokeys.set(SPACES);
		wsaaOkeyLanguage.set(atmodrec.language);
		wsaaOkeyTranno.set(chdrlnbIO.getTranno());
		//ILIFE-1971
		wsaaTotTaxCalculated.set(wsaaTotTax.toString());
		letrqstrec.otherKeys.set(wsaaLetokeys);
		letrqstrec.function.set("ADD");
		/* IF  T6634-LETTER-TYPE       NOT = SPACES             <PCPPRT>*/
		if (isNE(tr384rec.letterType,SPACES)) {
			/*     CALL 'LETRQST' USING LETRQST-PARAMS              <N003>  */
			/*     CALL 'HLETRQS' USING LETRQST-PARAMS              <PCPRT  */
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz,varcom.oK)) {
				syserrrec.params.set(letrqstrec.params);
				syserrrec.statuz.set(letrqstrec.statuz);
				xxxxFatalError();
			}
		}
	}
//ILIFE-3997-STARTS
protected void CreateNlgt(){
	
		initialize(nlgcalcrec.nlgcalcRec);
			nlgcalcrec.function.set(SPACES);
			nlgcalcrec.fsuco.set(wsaaFsuCoy);
			nlgcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
			nlgcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
			nlgcalcrec.tranno.set(ptrnpf.getTranno()); //ILIFE-8709
			nlgcalcrec.effdate.set(datcon1rec.intDate);
			nlgcalcrec.occdate.set(chdrlnbIO.getOccdate());
			nlgcalcrec.ptdate.set(chdrlnbIO.getPtdate());
			nlgcalcrec.billfreq.set(chdrlnbIO.getBillfreq());
			nlgcalcrec.cnttype.set(chdrlnbIO.getCnttype());
			nlgcalcrec.language.set(atmodrec.language);
			nlgcalcrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
			nlgcalcrec.inputAmt.set(ZERO);
			nlgcalcrec.frmdate.set(chdrlnbIO.getOccdate());
			nlgcalcrec.todate.set(varcom.vrcmMaxDate);
			nlgcalcrec.nlgBalance.set(ZERO);
			nlgcalcrec.totTopup.set(ZERO);
			nlgcalcrec.totWdrAmt.set(ZERO);
			nlgcalcrec.ovduePrem.set(ZERO);
			nlgcalcrec.unpaidPrem.set(ZERO);
			nlgcalcrec.transMode.set("N");
			nlgcalcrec.status.set(Varcom.oK);
	callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
	if (isNE(nlgcalcrec.status,varcom.oK)) {
		syserrrec.statuz.set(nlgcalcrec.status);
		syserrrec.params.set(nlgcalcrec.nlgcalcRec);
		xxxxFatalError();
	}
	
}
//ILIFE-3997-ENDS
protected void procChild6000()
	{
			para6010();
		}

protected void para6010()
	{
		if (isEQ(wsaaLifcnum,chdrlnbIO.getCownnum())) {
			return ;
		}
		/*  Read TR627 to get the attained age                             */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.tr627);
		itemIO.setItemitem(chdrlnbIO.getCnttype());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			return ;
		}
		tr627rec.tr627Rec.set(itemIO.getGenarea());
		/*  Read Life record to get life number, use life number to get    */
		/*  client date of birth                                           */
 //ILIFE-8709 start		
		lifepf.setChdrcoy(atmodrec.company.toString());
		lifepf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		lifepf.setLife("01");
		lifepf.setJlife("00");
		lifepf = lifepfDAO.getLifepf(lifepf);
		if(lifepf==null){
			syserrrec.params.set(chdrlnbIO.getChdrnum());
			xxxxFatalError();
		}
		clts.setClntpfx("CN");
		clts.setClntcoy(wsaaFsuCoy.toString());
		clts.setClntnum(lifepf.getLifcnum());
		clts = clntpfDAO.selectActiveClient(clts);
		if(clts==null) {
			syserrrec.params.set(lifepf.getLifcnum());
			xxxxFatalError();
		}
		 //ILIFE-8709 end
		
		/*   Get Sufficient Date by adding the Date of Birth and TR627     */
		/*   Self-sufficient Age                                           */
		/*   Use this Sufficient Date if TR627-EAAGE is Exact              */
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(clts.getCltdob()); //ILIFE-8709
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(tr627rec.zsufcage);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.subrname.set("DATCON2");
			xxxxFatalError();
		}
		wsaaDobPlusTr627.set(datcon2rec.intDate2);
 //ILIFE-8709 end
		if (isEQ(tr627rec.eaage,"E")) {
			wsaaZsufcdte.set(wsaaDobPlusTr627);
			return ;
		}
		/*  If TR627-EAAGE is Anniversary, then have to compare the        */
		/*  above Sufficient Date with policy anniversary date             */
		/*  Take the Anniversary Date if lesser than the above Sufficient  */
		/*  Date                                                           */
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon2rec.frequency.set("01");
		compute(datcon2rec.freqFactor, 0).set(sub(tr627rec.zsufcage,lifepf.getAnbAtCcd())); //ILIFE-8709
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.subrname.set("DATCON2");
			xxxxFatalError();
		}
		wsaaZsufcdte.set(datcon2rec.intDate2);
		if (isGT(wsaaDobPlusTr627,datcon2rec.intDate2)) {
			datcon2rec.intDate1.set(datcon2rec.intDate2);
			datcon2rec.frequency.set("01");
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.subrname.set("DATCON2");
				xxxxFatalError();
			}
			wsaaZsufcdte.set(datcon2rec.intDate2);
 //ILIFE-8709 start		
		}
 //ILIFE-8709 end
	}

protected void writeFprmRec6500(Payrpf payrpf)
	{
		start6510(payrpf);
	}

protected void start6510(Payrpf payrpf)
	{
		fprmIO.setDataArea(SPACES);
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub,6)
		|| isEQ(t5729rec.frqcy[wsaaT5729Sub.toInt()],chdrlnbIO.getBillfreq())); wsaaT5729Sub.add(1))
{
			/*CONTINUE_STMT*/
		}
		wsaaMinOverdue.set(ZERO);
		if (isEQ(wsaaT5729Sub,1)){
			wsaaMinOverdue.set(t5729rec.overdueMina01);
		}
		else if (isEQ(wsaaT5729Sub,2)){
			wsaaMinOverdue.set(t5729rec.overdueMinb01);
		}
		else if (isEQ(wsaaT5729Sub,3)){
			wsaaMinOverdue.set(t5729rec.overdueMinc01);
		}
		else if (isEQ(wsaaT5729Sub,4)){
			wsaaMinOverdue.set(t5729rec.overdueMind01);
		}
		else if (isEQ(wsaaT5729Sub,5)){
			wsaaMinOverdue.set(t5729rec.overdueMine01);
		}
		else if (isEQ(wsaaT5729Sub,6)){
			wsaaMinOverdue.set(t5729rec.overdueMinf01);
		}
		else{
			syserrrec.params.set(tablesInner.t5729);
			xxxxFatalError();
		}
		setPrecision(fprmIO.getMinPrmReqd(), 1);
		fprmIO.setMinPrmReqd(mult(div(wsaaMinOverdue, 100), wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()]), true);
		zrdecplcPojo.setAmountIn(fprmIO.getMinPrmReqd().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		fprmIO.setMinPrmReqd(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		fprmIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		fprmIO.setChdrnum(chdrlnbIO.getChdrnum());
		fprmIO.setPayrseqno(payrpf.getPayrseqno()); //ILIFE-8709
		fprmIO.setValidflag("1");
		fprmIO.setCurrfrom(chdrlnbIO.getOccdate());
		fprmIO.setCurrto(varcom.vrcmMaxDate);
		fprmIO.setTotalRecd(wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()]);
		fprmIO.setTotalBilled(wsaaBillingInformationInner.wsaaRegPremAdj[wsbbSub.toInt()]);
		fprmIO.setTranno(chdrlnbIO.getTranno());
		fprmIO.setUserProfile(chdrlnbIO.getUserProfile());
		fprmIO.setJobName(chdrlnbIO.getJobName());
		fprmIO.setDatime(chdrlnbIO.getDatime());
		fprmIO.setFormat(formatsInner.fprmrec);
		fprmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fprmIO.getParams());
			syserrrec.statuz.set(fprmIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void writeFpco7000()
	{
		start7010();
	}

protected void start7010()
	{
		fpcoIO.setChdrcoy(SPACES);
		fpcoIO.setChdrnum(SPACES);
		fpcoIO.setLife(SPACES);
		fpcoIO.setCoverage(SPACES);
		fpcoIO.setRider(SPACES);
		fpcoIO.setActiveInd(SPACES);
		fpcoIO.setPlanSuffix(ZERO);
		fpcoIO.setCurrfrom(ZERO);
		fpcoIO.setTargfrom(ZERO);
		fpcoIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		fpcoIO.setChdrnum(chdrlnbIO.getChdrnum());
		fpcoIO.setLife(covrlnbIO.getLife());
		fpcoIO.setJlife(covrlnbIO.getJlife());
		fpcoIO.setCoverage(covrlnbIO.getCoverage());
		fpcoIO.setRider(covrlnbIO.getRider());
		fpcoIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		fpcoIO.setValidflag("1");
		fpcoIO.setCurrfrom(chdrlnbIO.getOccdate());
		fpcoIO.setTargfrom(chdrlnbIO.getOccdate());
		fpcoIO.setEffdate(chdrlnbIO.getOccdate());
		fpcoIO.setCurrto(varcom.vrcmMaxDate);
		/*                                                         <D9604>*/
		/* Calculate TARGTO as OCCDATE + 1 year                    <D9604> */
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			xxxxFatalError();
		}
		fpcoIO.setTargto(datcon2rec.intDate2);
		wsaaAnnivExtractDate.set(fpcoIO.getTargto());
		wsaaBillingDate.set(wsaaBillingInformationInner.wsaaBillcd[wsbbSub.toInt()]);
		if ((isLT(wsaaBillDateMnth,wsaaAnnivExtractMnth))
		|| (isLT(wsaaBillDateDay,wsaaAnnivExtractDay)
		&& isLTE(wsaaBillDateMnth,wsaaAnnivExtractMnth))) {
			wsaaAnnivExtractDay.set(wsaaBillDateDay);
			wsaaAnnivExtractMnth.set(wsaaBillDateMnth);
		}
		fpcoIO.setAnnivProcDate(wsaaAnnivExtractDate);
		fpcoIO.setActiveInd("Y");
		wsaaFlexPremFq.set(wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()]);
		setPrecision(fpcoIO.getTargetPremium(), 2);
		fpcoIO.setTargetPremium(mult(covrlnbIO.getInstprem(),wsaaFlexPremFq));
		if (isEQ(wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()], ZERO)) {
			fpcoIO.setPremRecPer(ZERO);
			fpcoIO.setBilledInPeriod(ZERO);
		}
		else {
			setPrecision(fpcoIO.getPremRecPer(), 2);
			fpcoIO.setPremRecPer(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
			setPrecision(fpcoIO.getBilledInPeriod(), 2);
			fpcoIO.setBilledInPeriod(mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]));
		}
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub,6)
		|| isEQ(t5729rec.frqcy[wsaaT5729Sub.toInt()], wsaaBillingInformationInner.wsaaBillfreq[wsbbSub.toInt()])); wsaaT5729Sub.add(1))
{
			/*CONTINUE_STMT*/
		}
		wsaaMinOverdue.set(ZERO);
		if (isEQ(wsaaT5729Sub,1)){
			wsaaMinOverdue.set(t5729rec.overdueMina01);
		}
		else if (isEQ(wsaaT5729Sub,2)){
			wsaaMinOverdue.set(t5729rec.overdueMinb01);
		}
		else if (isEQ(wsaaT5729Sub,3)){
			wsaaMinOverdue.set(t5729rec.overdueMinc01);
		}
		else if (isEQ(wsaaT5729Sub,4)){
			wsaaMinOverdue.set(t5729rec.overdueMind01);
		}
		else if (isEQ(wsaaT5729Sub,5)){
			wsaaMinOverdue.set(t5729rec.overdueMine01);
		}
		else if (isEQ(wsaaT5729Sub,6)){
			wsaaMinOverdue.set(t5729rec.overdueMinf01);
		}
		else{
			syserrrec.params.set(tablesInner.t5729);
			xxxxFatalError();
		}
		fpcoIO.setMinOverduePer(wsaaMinOverdue);
		setPrecision(fpcoIO.getOverdueMin(), 3);
		fpcoIO.setOverdueMin(mult(div(wsaaMinOverdue, 100), (mult(covrlnbIO.getInstprem(), wsaaBillingInformationInner.wsaaFreqFactor[wsbbSub.toInt()]))), true);
		zrdecplcPojo.setAmountIn(fpcoIO.getOverdueMin().getbigdata()); //ILIFE-8709
		zrdecplcPojo.setCurrency(chdrlnbIO.getCntcurr().toString()); //ILIFE-8709
		callRounding9000();
		fpcoIO.setOverdueMin(zrdecplcPojo.getAmountOut()); //ILIFE-8709
		fpcoIO.setAnnProcessInd(SPACES);
		fpcoIO.setTranno(chdrlnbIO.getTranno());
		fpcoIO.setFormat(formatsInner.fpcorec);
		fpcoIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(fpcoIO.getParams());
			syserrrec.statuz.set(fpcoIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void dryProcessing8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* This section will determine if the DIARY system is present      */
		/* If so, the appropriate parameters are filled and the            */
		/* diary processor is called.                                      */
		wsaaT7508Cnttype.set(chdrlnbIO.getCnttype());
		wsaaT7508Batctrcde.set(wsaaBatckey.batcBatctrcde);
		readT75088100();
		/* If item not found try other types of contract.                  */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT7508Cnttype.set("***");
			readT75088100();
		}
		/* If item not found no Diary Update Processing is Required.       */
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			return ;
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypRunDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypCompany.set(wsaaBatckey.batcBatccoy);
		drypDryprcRecInner.drypBranch.set(wsaaBatckey.batcBatcbrn);
		drypDryprcRecInner.drypLanguage.set(atmodrec.language);
		drypDryprcRecInner.drypBatchKey.set(wsaaBatckey.batcKey);
		drypDryprcRecInner.drypEntityType.set(t7508rec.dryenttp01);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypEntity.set(wsaaPrimaryChdrnum);
		drypDryprcRecInner.drypEffectiveDate.set(datcon1rec.intDate);
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypFsuCompany.set(wsaaFsuCoy);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		drypDryprcRecInner.drypAplsupto.set(ZERO);
		drypDryprcRecInner.drypStmdte.set(chdrlnbIO.getStatementDate());
		/* Fill the parameters.                                            */
		drypDryprcRecInner.drypTranno.set(chdrlnbIO.getTranno());
		drypDryprcRecInner.drypBillchnl.set(chdrlnbIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlnbIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlnbIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlnbIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlnbIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlnbIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlnbIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlnbIO.getCnttype());
		drypDryprcRecInner.drypCpiDate.set(covrlnbIO.getCpiDate());
		drypDryprcRecInner.drypBbldate.set(covrlnbIO.getBenBillDate());
		drypDryprcRecInner.drypOccdate.set(chdrlnbIO.getOccdate());
		drypDryprcRecInner.drypCertdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypStmdte.set(chdrlnbIO.getStatementDate());
		drypDryprcRecInner.drypRcesdte.set(wsaaEarliestRcesdte);
		drypDryprcRecInner.drypCbunst.set(wsaaEarliestCrrcd);
		if (flexiblePremiumContract.isTrue()) {
			drypDryprcRecInner.drypTargto.set(fpcoIO.getTargto());
			drypDryprcRecInner.drypCpiDate.set(fpcoIO.getTargto());
		}
		else {
			drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		}
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			xxxxFatalError();
		}
	}

protected void readT75088100()
	{
		start8110();
	}

protected void start8110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(atmodrec.company);
		itemIO.setItemtabl(tablesInner.t7508);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			xxxxFatalError();
		}
	}

protected void callRounding9000()
	{
		/*CALL*/
 //ILIFE-8709 start
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setStatuz(Varcom.oK.toString());
		zrdecplcPojo.setCompany(atmodrec.company.toString());
		zrdecplcPojo.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(zrdecplcPojo.getStatuz());
			syserrrec.params.set(zrdecplcPojo.toString());
			xxxxFatalError();
		}
 //ILIFE-8709 end
		/*EXIT*/
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(wsaaBatckey.batcBatccoy);
		lifsttrrec.batcbrn.set(wsaaBatckey.batcBatcbrn);
		lifsttrrec.batcactyr.set(wsaaBatckey.batcBatcactyr);
		lifsttrrec.batcactmn.set(wsaaBatckey.batcBatcactmn);
		lifsttrrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		lifsttrrec.batcbatch.set(wsaaBatckey.batcBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlnbIO.getChdrnum());
		lifsttrrec.tranno.set(covrlnbIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz,varcom.oK)) {
			syserrrec.params.set(lifsttrrec.lifsttrRec);
			syserrrec.statuz.set(lifsttrrec.statuz);
			xxxxFatalError();
		}
	}

protected void a100WopDates()
	{
		/*A110-WOP*/
 //ILIFE-8709 start	
		covrpfList = covrpfDAO.getcovrtrbRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString(), "01", "01", "00");
		if(!(covrpfList.isEmpty())){
			for (Covrpf covrtrb:covrpfList){
				a210Covrtrb(covrtrb);
			}
		}
 //ILIFE-8709 end

		/*A190-EXIT*/
	}

protected void a210Covrtrb(Covrpf covrtrb)
	{
		a300ReadTr517();
		wsaaCpiValid.set("N");
		if (wopMatch.isTrue()) {
			wsaaTr517Rec.set(tr517rec.tr517Rec);
			if (isGT(covrtrb.getCpiDate(),covrtrb.getRerateDate())) {
				wsaaEarliestRerateDate.set(covrtrb.getRerateDate());
			}
			else {
				wsaaEarliestRerateDate.set(covrtrb.getCpiDate());
			}
			/* Determine if the WOP is life insured specific.                  */
			if (isNE(tr517rec.zrwvflg02,"Y")) {
				covrlnbIO.setParams(SPACES);
				covrlnbIO.setChdrcoy(covrtrb.getChdrcoy());
				covrlnbIO.setChdrnum(covrtrb.getChdrnum());
				covrlnbIO.setLife(covrtrb.getLife());
				covrlnbIO.setPlanSuffix(0);
				covrlnbIO.setFormat(formatsInner.covrlnbrec);
				covrlnbIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
				while ( !(isNE(covrlnbIO.getChdrcoy(),covrtrb.getChdrcoy())
				|| isNE(covrlnbIO.getChdrnum(),covrtrb.getChdrnum())
				|| isNE(covrlnbIO.getLife(),covrtrb.getLife())
				|| isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
					a400SetWopDate();
				}

				if (isNE(wsaaEarliestRerateDate,0)) {
					a240RewriteCovrtrb();
				}
			}
			else {
				covrlnbIO.setParams(SPACES);
				covrlnbIO.setChdrcoy(covrtrb.getChdrcoy());
				covrlnbIO.setChdrnum(covrtrb.getChdrnum());
				covrlnbIO.setPlanSuffix(0);
				covrlnbIO.setFormat(formatsInner.covrlnbrec);
				covrlnbIO.setFunction(varcom.begn);
				//performance improvement -- Anjali
				covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
				covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
				while ( !(isNE(covrlnbIO.getChdrcoy(),covrtrb.getChdrcoy())
				|| isNE(covrlnbIO.getChdrnum(),covrtrb.getChdrnum())
				|| isEQ(covrlnbIO.getStatuz(),varcom.endp))) {
					a400SetWopDate();
				}

				if (isNE(wsaaEarliestRerateDate,0)) {
					a240RewriteCovrtrb();
				}
			}
		}
	}

/*protected void a220Next()
	{
		covrtrbIO.setFunction(varcom.nextr);
	}
*/
protected void a240RewriteCovrtrb()
	{
		/*A250-REWRITE*/
		covrtrb.setRerateDate(wsaaEarliestRerateDate.toInt());
		if (cpiValid.isTrue()) {
			covrtrb.setCpiDate(wsaaEarliestRerateDate.toInt());
		}
		covrpfDAO.updateCovrpfRecord(covrtrb);
	}


 //ILIFE-8709 end
protected void a300ReadTr517()
	{
			a310Read();
		}

protected void a310Read()
	{
		/* Read TR517 for Wavier of Premium Component.                     */
		wopNotMatch.setTrue();
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrtrb.getChdrcoy()); //ILIFE-8709
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(covrtrb.getCrtable()); //ILIFE-8709
		itdmIO.setItmfrm(covrtrb.getCrrcd()); //ILIFE-8709
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itdmIO.getStatuz(),varcom.endp)
		|| isNE(itdmIO.getItemcoy(),covrtrb.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(subString(itdmIO.getItemitem(), 1, 4),covrtrb.getCrtable())) {
			return ;
		}
		if (isEQ(itdmIO.getStatuz(),varcom.oK)) {
			wopMatch.setTrue();
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
		}
	}

protected void a400SetWopDate()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a410SetWop();
					a420Check();
				case a480Next:
					a480Next();
				case a490Exit:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410SetWop()
	{
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(),varcom.oK)
		&& isNE(covrlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			xxxxFatalError();
		}
		if (isNE(tr517rec.zrwvflg02,"Y")) {
			if (isNE(covrlnbIO.getChdrcoy(),covrtrb.getChdrcoy())
			|| isNE(covrlnbIO.getChdrnum(),covrtrb.getChdrnum())
			|| isNE(covrlnbIO.getLife(),covrtrb.getLife())
			|| isEQ(covrlnbIO.getStatuz(),varcom.endp)) {
				covrlnbIO.setStatuz(varcom.endp);
				goTo(GotoLabel.a490Exit);
			}
		}
		else {
			if (isNE(covrlnbIO.getChdrcoy(),covrtrb.getChdrcoy())
			|| isNE(covrlnbIO.getChdrnum(),covrtrb.getChdrnum())
			|| isEQ(covrlnbIO.getStatuz(),varcom.endp)) {
				covrlnbIO.setStatuz(varcom.endp);
				goTo(GotoLabel.a490Exit);
			}
		}
		if (isNE(covrlnbIO.getValidflag(),"1")) {
			goTo(GotoLabel.a480Next);
		}
		wsaaCrtableMatch.set("N");
		tr517rec.tr517Rec.set(wsaaTr517Rec);
	}

protected void a420Check()
	{
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,50)
		|| crtableMatch.isTrue()); wsaaCnt.add(1)){
			if (isEQ(covrlnbIO.getCrtable(),tr517rec.ctable[wsaaCnt.toInt()])) {
				crtableMatch.setTrue();
			}
		}
		if (!crtableMatch.isTrue()
		&& isNE(tr517rec.contitem,SPACES)) {
			a30cReadTr517();
			if (isEQ(wsaaWaiveCont,"Y")) {
				a420Check();
				return ;
			}
		}
		if (crtableMatch.isTrue()
		&& (isEQ(tr517rec.zrwvflg02,"Y")
		|| isEQ(covrlnbIO.getLife(),covrtrb.getLife()))) {
			if (isLT(covrlnbIO.getRerateDate(),wsaaEarliestRerateDate)
			&& isGT(covrlnbIO.getRerateDate(),0)) {
				wsaaEarliestRerateDate.set(covrlnbIO.getRerateDate());
			}
			if (isNE(covrlnbIO.getCpiDate(),varcom.vrcmMaxDate)) {
				cpiValid.setTrue();
			}
			if (isLT(covrlnbIO.getCpiDate(),wsaaEarliestRerateDate)
			&& isGT(covrlnbIO.getCpiDate(),0)) {
				wsaaEarliestRerateDate.set(covrlnbIO.getCpiDate());
			}
		}
	}

protected void a480Next()
	{
		covrlnbIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	*                                                         <V4L001>
	* </pre>
	*/
protected void r200UpdateApa()
	{
		r210Linkup();
	}

	/**
	* <pre>
	*************************                                 <V4L001>
	* </pre>
	*/
protected void r210Linkup()
	{
 //ILIFE-8709 start
		rlpdlonPojo.setPstw(("PSTW"));
		rlpdlonPojo.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		rlpdlonPojo.setChdrnum(chdrlnbIO.getChdrnum().toString());
		rlpdlonPojo.setEffdate(chdrlnbIO.getOccdate().toInt());
		rlpdlonPojo.setCurrency(acblpf.getOrigcurr());
		rlpdlonPojo.setTranno(chdrlnbIO.getTranno().toInt());
		rlpdlonPojo.setTranseq(wsaaJrnseq.toInt());
		rlpdlonPojo.setLongdesc(descIO.getLongdesc().toString());
		rlpdlonPojo.setLanguage(atmodrec.language.toString());
		rlpdlonPojo.setBatchkey(atmodrec.batchKey.toString());
		rlpdlonPojo.setAuthCode(rlpdlonPojo.getTrcde());
		rlpdlonPojo.setTime(wsaaTransactionTime.toInt());
		rlpdlonPojo.setDate_var(wsaaTransactionDate.toInt());
		rlpdlonPojo.setUser(wsaaUser.toInt());
		rlpdlonPojo.setTermid(wsaaTermid.toString());
		rlpdlonUtils.callRlpdlon(rlpdlonPojo);
		if (isNE(rlpdlonPojo.getStatuz(), varcom.oK)) {
		    syserrrec.statuz.set(rlpdlonPojo.getStatuz());
		    syserrrec.params.set(rlpdlonPojo);
		    xxxxFatalError();
	        }
		wsaaJrnseq.set(rlpdlonPojo.getTranseq());
 //ILIFE-8709 end
	}

protected void a30cReadTr517()
	{
			a31cRead();
		}

protected void a31cRead()
	{
		/* Read TR517 for Wavier of Premium Component.                     */
		wsaaWaiveCont = "N";
		wsaaZrwvflgs.set(tr517rec.zrwvflgs);
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrtrb.getChdrcoy()); //ILIFE-8709
		itdmIO.setItemtabl(tablesInner.tr517);
		itdmIO.setItemitem(tr517rec.contitem);
		itdmIO.setItmfrm(covrtrb.getCrrcd()); //ILIFE-8709
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		|| isNE(itdmIO.getItemcoy(),covrtrb.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tablesInner.tr517)
		|| isNE(itdmIO.getItemitem(),tr517rec.contitem)) {
			return ;
		}
		wsaaWaiveCont = "Y";
		tr517rec.tr517Rec.set(itdmIO.getGenarea());
		tr517rec.zrwvflgs.set(wsaaZrwvflgs);
	}

protected void a500AddUnderwritting()
	{
		a500Ctrl();
	}

protected void a500Ctrl()
	{
		/* PERFORM A700-READR-LIFEIO.                                   */
		/* PERFORM A600-DEL-UNDERWRITTING.                     <LFA1062>*/
	 //ILIFE-8709 start
		covrpfList = covrpfDAO.getCovrsurByComAndNum(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		for (Covrpf covrtrb : covrpfList) {
			a600DelUnderwritting(covrtrb);
		}
		for (Covrpf covrtrb : covrpfList) {
			a800NextrCovrtrbio(covrtrb);
		}
		 //ILIFE-8709 end
	}

protected void a600DelUnderwritting(Covrpf covrtrb)
	{
			a600Ctrl(covrtrb);
		}

protected void a600Ctrl(Covrpf covrtrb)
	{
		initialize(crtundwrec.parmRec);
		a900ReadLife();
		/* MOVE WSAA-L1-CLNTNUM        TO UNDW-CLNTNUM.        <LFA1062>*/
		crtundwrec.clntnum.set(lifepf.getLifcnum()); //ILIFE-8709
		crtundwrec.coy.set(chdrlnbIO.getChdrcoy());
		crtundwrec.currcode.set(chdrlnbIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrlnbIO.getChdrnum());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		crtundwrec = crtundwrtUtil.process(crtundwrec); //IBPLIFE-680
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			xxxxFatalError();
		}
	}

	/**
	* <pre>
	*A700-READR-LIFEIO SECTION.
	*A700-CTRL.
	**** MOVE CHDRLNB-CHDRCOY        TO LIFE-CHDRCOY
	**** MOVE CHDRLNB-CHDRNUM        TO LIFE-CHDRNUM
	**** MOVE WSAA-LIFE              TO LIFE-LIFE.
	**** MOVE                        TO LIFE-JLIFE
	**** MOVE                        TO LIFE-CURRFROM
	**** MOVE READR                  TO LIFE-FUNCTION.
	**** MOVE LIFEREC                TO LIFE-FORMAT.
	**** CALL 'LIFEIO'               USING LIFE-PARAMS.
	**** IF LIFE-STATUZ              NOT = O-K
	****    MOVE LIFE-PARAMS         TO SYSR-PARAMS
	****    MOVE LIFE-STATUZ         TO SYSR-STATUZ
	****    PERFORM XXXX-FATAL-ERROR.
	*A700-EXIT.
	**** EXIT.
	* </pre>
	*/
 //ILIFE-8709 start
protected void a800NextrCovrtrbio(Covrpf covrtrb)
	{
			a800Ctrl(covrtrb);
		}

protected void a800Ctrl(Covrpf covrtrb)
	{
	
		initialize(crtundwrec.parmRec);
		a900ReadLife();
		/*  MOVE WSAA-L1-CLNTNUM        TO UNDW-CLNTNUM.        <LFA1062>*/
		crtundwrec.clntnum.set(lifepf.getLifcnum());
		crtundwrec.coy.set(covrtrb.getChdrcoy());
		crtundwrec.chdrnum.set(covrtrb.getChdrnum());
		crtundwrec.life.set(covrtrb.getLife());
		crtundwrec.crtable.set(covrtrb.getCrtable());
		crtundwrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		crtundwrec.sumins.set(covrtrb.getSumins());
		crtundwrec.cnttyp.set(chdrlnbIO.getCnttype());
		crtundwrec.currcode.set(chdrlnbIO.getCntcurr());
		crtundwrec.function.set("ADD");
		crtundwrec = crtundwrtUtil.process(crtundwrec); //IBPLIFE-680
		if (isNE(crtundwrec.status,varcom.oK)) {
			syserrrec.params.set(crtundwrec.parmRec);
			syserrrec.statuz.set(crtundwrec.status);
			syserrrec.iomod.set("CRTUNDWRT");
			xxxxFatalError();
		}
	}

protected void a900ReadLife()
	{
		a900Ctrl();
	}

protected void a900Ctrl()
	{
		lifepf.setChdrcoy(atmodrec.company.toString());
		lifepf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		lifepf.setLife(covrlnbIO.getLife().toString());
		lifepf.setJlife("00");
		lifepf = lifepfDAO.getLifepf(lifepf);
		
 //ILIFE-8709 end
	}

protected void b100CallZorcompy()
	{
		b110Start();
	}

protected void b110Start()
	{
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.effdate.set(chdrlnbIO.getOccdate());
		if (isEQ(chdrlnbIO.getBillfreq(), "00")) {
			zorlnkrec.ptdate.set(wsaaOldCessDate);
		}
		else {
			zorlnkrec.ptdate.set(chdrlnbIO.getBtdate());
		}
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(chdrlnbIO.getTranno());
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[1]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zorlnkrec.statuz);
			syserrrec.params.set(zorlnkrec.zorlnkRec);
			xxxxFatalError();
		}
	}

protected void d100Zswr() {
	//AFR
	
		FixedLengthStringData zafropt1=new FixedLengthStringData(15);
		FixedLengthStringData zafroptitem=new FixedLengthStringData(15);
		FixedLengthStringData zafrequency=new FixedLengthStringData(15);
		int znextdte=0;
		List<Zswrpf> zswrpfiList=new ArrayList<>();
		Zswrpf zw=new Zswrpf();
		zw.setChdrpfx(chdrlnbIO.getChdrpfx().toString());
		zw.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		zw.setChdrnum(chdrlnbIO.getChdrnum().toString()); 
		zw.setValidflag("3" );
		zswrpfiList=zswrDAO.getZswrpfData( zw.getChdrnum(),zw.getChdrcoy(),  zw.getChdrpfx(),  zw.getValidflag());/* IJTI-1523 */	
		
		if (zswrpfiList.size()>0 )
		{
			
			for (Zswrpf p : zswrpfiList) 
			 {             	 
				zafropt1.set(p.getZafropt());
				zafroptitem.set(p.getZafritem());
				zafrequency.set(p.getZafrfreq());
				znextdte=p.getZnextdte();
			 }
			
				zw.setChdrpfx(chdrlnbIO.getChdrpfx().toString());
				zw.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
				zw.setChdrnum(chdrlnbIO.getChdrnum().toString()); 
		
				zw.setValidflag("1");
	    		if (isNE(zafropt1, "NO") || isNE(zafroptitem,"NO")) 
	    		{
	    			zw.setZregdte(datcon1rec.intDate.toInt());    		
	    			zw.setTranno(chdrlnbIO.getTranno().toInt());
	    			initialize(datcon4rec.datcon4Rec);
	    			datcon4rec.freqFactor.set(1);
	    			datcon4rec.frequency.set(zafrequency);
	    			datcon4rec.intDate1.set(chdrlnbIO.getOccdate());
	    			callProgram(Datcon4.class, datcon4rec.datcon4Rec);
	    			if (isNE(datcon4rec.statuz, varcom.oK)) {
	    				syserrrec.statuz.set(datcon4rec.statuz);
	    				syserrrec.params.set(datcon4rec.datcon4Rec);
	    				xxxxFatalError();
	    			}
	    			zw.setZnextdte(datcon4rec.intDate2.toInt());    		
	    			zw.setZenable("E");
	    			zswrDAO.updateZswrpfvalidflag(zw);
	    		} else {
	    			zw.setZregdte(varcom.vrcmMaxDate.toInt());    		
	    			zw.setTranno(chdrlnbIO.getTranno().toInt());
	    			zw.setZnextdte(znextdte);    		
	    			zw.setZenable("E");
	    			zswrDAO.updateZswrpfvalidflag(zw);
	    		}
		}
    }
//ALS-4706-start
private void initMgmFeeRec(){
	
	List<String> val=new ArrayList<String>();
	val.add("0");
	mgmFeeRec.setFunction("ISSUE");
	mgmFeeRec.setChdrChdrcoy(chdrlnbIO.getChdrcoy().toString());
	mgmFeeRec.setChdrChdrnum(chdrlnbIO.getChdrnum().toString());
	mgmFeeRec.setLifeLife(covrlnbIO.getLife().toString());
	mgmFeeRec.setLifeJlife(covrlnbIO.getJlife().toString());
	mgmFeeRec.setCovrCoverage(covrlnbIO.getCoverage().toString());
	mgmFeeRec.setCovrRider(covrlnbIO.getRider().toString());
	mgmFeeRec.setPlanSuffix(covrlnbIO.getPlanSuffix().toInt());
	mgmFeeRec.setBillfreq(t5534rec.unitFreq.toString());
	mgmFeeRec.setCntcurr(chdrlnbIO.getCntcurr().toString());
	mgmFeeRec.setEffdate(covrlnbIO.getCrrcd().toInt());
	mgmFeeRec.setPremMeth(t5534rec.premmeth.toString());
	mgmFeeRec.setJlifePremMeth(t5534rec.jlPremMeth.toString());
	mgmFeeRec.setSumins(covrlnbIO.getSumins().getbigdata());
	mgmFeeRec.setPremCessDate(covrlnbIO.getPremCessDate().toInt());
	mgmFeeRec.setCrtable(covrlnbIO.getCrtable().toString());
	mgmFeeRec.setMortcls(covtlnb.getMortcls());
	mgmFeeRec.setSvMethod(t5534rec.svMethod.toString());
	mgmFeeRec.setAdfeemth(t5534rec.adfeemth.toString());
	mgmFeeRec.setBatccoy(wsaaBatckey.batcBatccoy.toString());
	mgmFeeRec.setBatcbrn(wsaaBatckey.batcBatcbrn.toString());
	mgmFeeRec.setBatcactmn(wsaaBatckey.batcBatcactmn.toInt());
	mgmFeeRec.setBatcactyr(wsaaBatckey.batcBatcactyr.toInt());
	mgmFeeRec.setBatctrcde(wsaaBatckey.batcBatctrcde.toString());
	mgmFeeRec.setBatch(wsaaBatckey.batcBatcbatch.toString());
	mgmFeeRec.setBillchnl(covtlnb.getBillchnl());
	mgmFeeRec.setCnttype(chdrlnbIO.getCnttype().toString());
	mgmFeeRec.setTranno(chdrlnbIO.getTranno().toInt());
	mgmFeeRec.setPolsum(chdrlnbIO.getPolsum().toInt());
	mgmFeeRec.setPtdate(chdrlnbIO.getPtdate().toInt());
	mgmFeeRec.setOccdate(chdrlnbIO.getOccdate().toInt());
	mgmFeeRec.setPolinc(chdrlnbIO.getPolinc().toInt());
	mgmFeeRec.setSingp(BigDecimal.ZERO);
	mgmFeeRec.setChdrRegister(chdrlnbIO.getRegister().toString());
	mgmFeeRec.setLanguage(atmodrec.language.toString());
	mgmFeeRec.setUser(wsaaUser.toInt());
	mgmFeeRec.setFirstPrem(covtlnb.getZbinstprem());
	mgmFeeRec.setCurUnitBal(val);
	mgmFeeRec.setUnitBidPrice(val);
	mgmFeeRec.setNrFunds(0);
	mgmFeeRec.setPolcurr(0);
	mgmFeeRec.setBenfunc("0");
} //ALS-4706-ends

	protected void gstPostings(BigDecimal origamt, String agntNum, int gstSub) {
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[gstSub]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[gstSub]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[gstSub]);
		lifacmvrec.glsign.set(wsaaT5645Sign[gstSub]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[gstSub]);
		lifacmvrec.tranref.set(agntNum);
		lifacmvrec.rldgacct.set(agntNum);
		lifacmvrec.origamt.set(origamt);
		wsaaJrnseq.add(1);
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		lifacmvCheck2950();
	}
	
	protected void performLincProcessing() {
		Tjl47rec tjl47rec = new Tjl47rec();
		Itempf item = new Itempf();
		item.setItempfx("IT");
		item.setItemcoy(atmodrec.company.toString());
		item.setItemitem(wsaaBatckey.batcBatctrcde.toString());
		item.setItemtabl("TJL47");
		item = itemDAO.getItemRecordByItemkey(item);
		if(item != null) {
			tjl47rec.tjl47Rec.set(StringUtil.rawToString(item.getGenarea()));
			LincNBService lincService = (LincNBService) getApplicationContext().getBean(tjl47rec.lincsubr.toString().toLowerCase());
			lincService.newBusinessIssuance(setLincpfHelper(), tjl47rec);
		}
	}
	
	protected LincpfHelper setLincpfHelper() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		LincpfHelper lincpfHelper = new LincpfHelper();
		lincpfHelper.setChdrcoy(atmodrec.company.toString());
		lincpfHelper.setChdrnum(wsaaPrimaryChdrnum.toString());
		lincpfHelper.setOwncoy(wsaaFsuCoy.toString().trim());
		lincpfHelper.setClntnum(chdrlnbIO.getCownnum().toString());
		lincpfHelper.setCownnum(chdrlnbIO.getCownnum().toString());
		lincpfHelper.setCnttype(chdrlnbIO.getCnttype().toString().trim());
		lincpfHelper.setOccdate(chdrlnbIO.getOccdate().toInt());
		lincpfHelper.setTransactionDate(datcon1rec.intDate.toInt());
		lincpfHelper.setTranscd(wsaaBatckey.batcBatctrcde.toString());
		lincpfHelper.setLanguage(atmodrec.language.toString());
		lincpfHelper.setTranno(chdrlnbIO.getTranno().toInt());
		lincpfHelper.setTermId(varcom.vrcmTermid.toString());
		lincpfHelper.setTrdt(varcom.vrcmDate.toInt());
		lincpfHelper.setTrtm(varcom.vrcmTime.toInt());
		lincpfHelper.setUsrprf(varcom.vrcmUser.toString());
		lincpfHelper.setJobnm(chdrlnbIO.getJobName().toString());
		lincpfHelper = setLifeDetails(lincpfHelper);
		lincpfHelper = setClientDetails(lincpfHelper);
		return lincpfHelper;
	}
	
	protected LincpfHelper setLifeDetails(LincpfHelper lincpfHelper) {
		Clntpf clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(lincpfHelper.getOwncoy());
		clntpf.setClntnum(wsaaLifcnum.toString().trim());
		clntpf = clntpfDAO.selectClient(clntpf);
		lincpfHelper.setLifcnum(wsaaLifcnum.toString().trim());
		lincpfHelper.setLcltdob(clntpf.getCltdob());
		lincpfHelper.setLcltsex(clntpf.getCltsex());//IJTI-1793
		lincpfHelper.setLzkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setLzkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setLzkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setLzkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setLzkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setLzkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setLzkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setLgivname(clntpf.getGivname());
		lincpfHelper.setLsurname(clntpf.getSurname());
		return lincpfHelper;
	}
	
	protected LincpfHelper setClientDetails(LincpfHelper lincpfHelper) {
		Clntpf clntpf = new Clntpf();
		clntpf.setClntpfx("CN");
		clntpf.setClntcoy(lincpfHelper.getOwncoy());
		clntpf.setClntnum(lincpfHelper.getClntnum());
		clntpf = clntpfDAO.selectClient(clntpf);
		lincpfHelper.setZkanasnm(clntpf.getZkanasnm());
		lincpfHelper.setZkanagnm(clntpf.getZkanagnm());
		lincpfHelper.setZkanaddr01(clntpf.getZkanaddr01());
		lincpfHelper.setZkanaddr02(clntpf.getZkanaddr02());
		lincpfHelper.setZkanaddr03(clntpf.getZkanaddr03());
		lincpfHelper.setZkanaddr04(clntpf.getZkanaddr04());
		lincpfHelper.setZkanaddr05(clntpf.getZkanaddr05());
		lincpfHelper.setGivname(clntpf.getGivname());
		lincpfHelper.setSurname(clntpf.getSurname());
		lincpfHelper.setCltsex(clntpf.getCltsex());
		lincpfHelper.setCltdob(clntpf.getCltdob());
		lincpfHelper.setClttype(clntpf.getClttype());
		return lincpfHelper;
	}
/*
 * Class transformed  from Data Structure WSAA-AGENTS--INNER
 */
private static final class WsaaAgentsInner {

		/* WSAA-AGENTS */
	private FixedLengthStringData[] wsaaAgentsCommissions = FLSInittedArray (10, 101);
	private FixedLengthStringData[] wsaaAgntnum = FLSDArrayPartOfArrayStructure(8, wsaaAgentsCommissions, 0);
	private PackedDecimalData[] wsaaSplitBcomm = PDArrayPartOfArrayStructure(5, 2, wsaaAgentsCommissions, 8);
	private PackedDecimalData[] wsaaCommDue = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 11);
	private PackedDecimalData[] wsaaCommEarn = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 20);
	private PackedDecimalData[] wsaaCommPaid = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 29);
	private PackedDecimalData[] wsaaServDue = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 38);
	private PackedDecimalData[] wsaaServEarn = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 47);
	private PackedDecimalData[] wsaaRenlDue = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 56);
	private PackedDecimalData[] wsaaRenlEarn = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 65);
	private PackedDecimalData[] wsaaTcomDue = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 74);
	private PackedDecimalData[] wsaaAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 83);
	private PackedDecimalData[] wsaaSingp = PDArrayPartOfArrayStructure(17, 2, wsaaAgentsCommissions, 92);
}
/*
 * Class transformed  from Data Structure WSAA-BILLING-INFORMATION--INNER
 */
//private static final class WsaaBillingInformationInner {
protected static final class WsaaBillingInformationInner {

		/* WSAA-BILLING-INFORMATION */
	private FixedLengthStringData[] wsaaBillingDetails = FLSInittedArray (9, 132);
	private ZonedDecimalData[] wsaaIncomeSeqNo = ZDArrayPartOfArrayStructure(2, 0, wsaaBillingDetails, 0, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaBillfreq = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 2);
	private FixedLengthStringData[] wsaaBillchnl = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 4);
	private PackedDecimalData[] wsaaBillcd = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 6);
	private PackedDecimalData[] wsaaBtdate = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 11);
	private FixedLengthStringData[] wsaaBillcurr = FLSDArrayPartOfArrayStructure(3, wsaaBillingDetails, 16);
	private FixedLengthStringData[] wsaaClntcoy = FLSDArrayPartOfArrayStructure(1, wsaaBillingDetails, 19);
	private FixedLengthStringData[] wsaaClntnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 20);
	private PackedDecimalData[] wsaaRegPremAcc = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 28);
	
	private PackedDecimalData[] wsaaSingPremAcc = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 37);
	
	private PackedDecimalData[] wsaaRegPremAdj = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 46);
	private PackedDecimalData[] wsaaInstprem = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 55);
	
	private PackedDecimalData[] wsaaSpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 64);
	
	private PackedDecimalData[] wsaaRpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 73);	
	private PackedDecimalData[] wsaaFeTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 82);
	
	private ZonedDecimalData[] wsaaFreqFactor = ZDArrayPartOfArrayStructure(11, 5, wsaaBillingDetails, 91);
	private PackedDecimalData[] wsaaTaxrelamt = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 102);
	private FixedLengthStringData[] wsaaInrevnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 111);
	private PackedDecimalData[] wsaaTolrUsed = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 119);
	
	private FixedLengthStringData[] wsaaBillday = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 128);
	private FixedLengthStringData[] wsaaBillmonth = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 130);
	
	public PackedDecimalData[] getWsaaSpTax() {
		return wsaaSpTax;
	}
	public void setWsaaSpTax(PackedDecimalData[] wsaaSpTax) {
		this.wsaaSpTax = wsaaSpTax;
	}
	public PackedDecimalData[] getWsaaRpTax() {
		return wsaaRpTax;
	}
	public void setWsaaRpTax(PackedDecimalData[] wsaaRpTax) {
		this.wsaaRpTax = wsaaRpTax;
	}
	public PackedDecimalData[] getWsaaBtdate() {
		return wsaaBtdate;
	}
	public void setWsaaBtdate(PackedDecimalData[] wsaaBtdate) {
		this.wsaaBtdate = wsaaBtdate;
	}
	public FixedLengthStringData[] getWsaaBillfreq() {
		return wsaaBillfreq;
	}
	public void setWsaaBillfreq(FixedLengthStringData[] wsaaBillfreq) {
		this.wsaaBillfreq = wsaaBillfreq;
	}
	public PackedDecimalData[] getWsaaInstprem() {
		return wsaaInstprem;
	}
	public void setWsaaInstprem(PackedDecimalData[] wsaaInstprem) {
		this.wsaaInstprem = wsaaInstprem;
	}
	public PackedDecimalData[] getWsaaRegPremAcc() {
		return wsaaRegPremAcc;
	}
	public void setWsaaRegPremAcc(PackedDecimalData[] wsaaRegPremAcc) {
		this.wsaaRegPremAcc = wsaaRegPremAcc;
	}
	public PackedDecimalData[] getWsaaSingPremAcc() {
		return wsaaSingPremAcc;
	}
	public void setWsaaSingPremAcc(PackedDecimalData[] wsaaSingPremAcc) {
		this.wsaaSingPremAcc = wsaaSingPremAcc;
	}
	public PackedDecimalData[] getWsaaTolrUsed() {
		return wsaaTolrUsed;
	}
	public void setWsaaTolrUsed(PackedDecimalData[] wsaaTolrUsed) {
		this.wsaaTolrUsed = wsaaTolrUsed;
	}
	public PackedDecimalData[] getWsaaFeTax() {
		return wsaaFeTax;
	}
	public void setWsaaFeTax(PackedDecimalData[] wsaaFeTax) {
		this.wsaaFeTax = wsaaFeTax;
	}
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner {
		/* TABLES */
	private FixedLengthStringData t5679 = new FixedLengthStringData(5).init("T5679");
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5534 = new FixedLengthStringData(5).init("T5534");
	private FixedLengthStringData t5676 = new FixedLengthStringData(5).init("T5676");
	private FixedLengthStringData t5671 = new FixedLengthStringData(5).init("T5671");
	private FixedLengthStringData t5647 = new FixedLengthStringData(5).init("T5647");
	private FixedLengthStringData t5644 = new FixedLengthStringData(5).init("T5644");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
	private FixedLengthStringData t5674 = new FixedLengthStringData(5).init("T5674");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t6687 = new FixedLengthStringData(5).init("T6687");
	private FixedLengthStringData t5667 = new FixedLengthStringData(5).init("T5667");
	private FixedLengthStringData t3695 = new FixedLengthStringData(5).init("T3695");
	private FixedLengthStringData t6658 = new FixedLengthStringData(5).init("T6658");
	private FixedLengthStringData tr384 = new FixedLengthStringData(5).init("TR384");
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private FixedLengthStringData tr517 = new FixedLengthStringData(5).init("TR517");
	private FixedLengthStringData tr695 = new FixedLengthStringData(5).init("TR695");
	private FixedLengthStringData t7508 = new FixedLengthStringData(5).init("T7508");
	private FixedLengthStringData th605 = new FixedLengthStringData(5).init("TH605");
	private FixedLengthStringData tr627 = new FixedLengthStringData(5).init("TR627");
	private FixedLengthStringData tr52d = new FixedLengthStringData(5).init("TR52D");
	private FixedLengthStringData tr52e = new FixedLengthStringData(5).init("TR52E");
	private FixedLengthStringData tr52q = new FixedLengthStringData(5).init("TR52Q");
	private FixedLengthStringData t3615 = new FixedLengthStringData(5).init("T3615");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
//private static final class FormatsInner {
protected static final class FormatsInner {
		/* FORMATS */
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData pcddlnbrec = new FixedLengthStringData(10).init("PCDDLNBREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
	private FixedLengthStringData covrtrbrec = new FixedLengthStringData(10).init("COVRTRBREC");
	private FixedLengthStringData agcmrec = new FixedLengthStringData(10).init("AGCMREC");
	private FixedLengthStringData lextrec = new FixedLengthStringData(10).init("LEXTREC");
	private FixedLengthStringData agpyagtrec = new FixedLengthStringData(10).init("AGPYAGTREC");
	private FixedLengthStringData agpydocrec = new FixedLengthStringData(10).init("AGPYDOCREC");
	private FixedLengthStringData fprmrec = new FixedLengthStringData(10).init("FPRMREC");
	private FixedLengthStringData fpcorec = new FixedLengthStringData(10).init("FPCOREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	
	private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
	public FixedLengthStringData getTaxdrec() {
		return taxdrec;
	}
	public void setTaxdrec(FixedLengthStringData taxdrec) {
		this.taxdrec = taxdrec;
	}
	public FixedLengthStringData getItemrec() {
		return itemrec;
	}
	public void setItemrec(FixedLengthStringData itemrec) {
		this.itemrec = itemrec;
	}
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypBbldate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 41);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypCertdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 56);
	private PackedDecimalData drypRcesdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 61);
	private PackedDecimalData drypCbunst = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 66);
}


protected void updtpurepremCustomerSpecific(){   }


protected void setlifeacmvCustomerSpecific(){
	
}

protected void updtSintamntsCustomerSpecific2493(){
	
}

//IBPLIFE-1357 starts
protected void msgSending(){
	getAlertDetails();
	Itempf itempf = itemDAO.findItemByItem("9",tr29s,chdrlnbIO.getChdrcoy().toString());
	if(itempf!=null) {
		tr29srec.tr29sRec.set(StringUtil.rawToString(itempf.getGenarea()));
	}else {
		return;
	}

	if((null != clexpf &&  null != clexpf.getRinternet()) && (isEQ(tr29srec.moneyFlag, "2") || isEQ(tr29srec.moneyFlag, "4"))
			&& isNE(clexpf.getRinternet(), SPACES)) {/*-   Check if email address was entered*/
		sendEmail1800();
	}
	if((null != clexpf &&  null != clexpf.getRmblphone()) && (isEQ(tr29srec.moneyFlag, "3") || isEQ(tr29srec.moneyFlag, "4"))
			&& isNE(clexpf.getRmblphone(), SPACES)) {/*-   Check if mobile number was entered*/
			sendSms1801();
		}
}

protected void getAlertDetails()
{
     com.csc.fsu.underwriting.dataaccess.dao.model.Chdrpf chdrpfObj = new com.csc.fsu.underwriting.dataaccess.dao.model.Chdrpf();
	chdrpfObj.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
	chdrpfObj.setChdrnum(chdrlnbIO.getChdrnum().toString());
	List<com.csc.fsu.underwriting.dataaccess.dao.model.Chdrpf> list = dao.findByKey(chdrpfObj);
	if(!list.isEmpty()) {
		chdrpfObj = list.get(0);
		wsaaAlertv2.set(chdrpfObj.getChdrnum().trim());
	}
	Clntpf clntpf = new Clntpf();
	List<Clntpf> clientList = clntpfDAO.queryReadr(chdrpfObj.getCownpfx(),chdrpfObj.getCowncoy(),chdrpfObj.getCownnum());
	if(!clientList.isEmpty()) {
		clntpf = clientList.get(0);
		wsaaAlertv1.set(clntpf.getGivname().trim());
	}
	wsaaAlertv3.set(datcon1rec.extDate);
	clexpf = new Clexpf();
	clexpf= clexpfDAO.getClexpf(clntpf.getClntnum());
	if(clexpf != null) {
		wsaaTo.set(clexpf.getRinternet());
		phoneNumberTo.set(clexpf.getRmblphone());
	}
}

protected void sendEmail1800()
{
	/*-   Set up the parameters for the SENDMAILCL program.*/
	String emailContent = prepareContent();
	sendmlrec.etext.set(emailContent);
	sendmlrec.statuz.set(varcom.oK);
	sendmlrec.to.set(wsaaTo);
	sendmlrec.esubject.set("Email SMS");
	SendMail.send(sendmlrec.to, sendmlrec.esubject, sendmlrec.etext, sendmlrec.statuz);
	if (isNE(sendmlrec.statuz, varcom.oK)) {
		syserrrec.statuz.set(sendmlrec.statuz);
		syserrrec.params.set("Email");
		xxxxFatalError();
	}
}

protected void sendSms1801()
{
	/*-   Set up the parameters for the SENDMAILCL program.*/
	String smsContent = prepareContent().replaceAll("\n","").replaceAll("\t","");
	final FixedLengthStringData smsStatus = varcom.oK;
	try{
		TwilloSMSProvider.sendSMS(phoneNumberTo.toString().trim(), smsContent, smsStatus);
	}
	catch(Exception e)
	{
		LOGGER.error("The number is unverified. Trial accounts cannot send messages to unverified numbers",e);
	}
}

protected String prepareContent() {
ClassLoader p5074at = P5074at.class.getClassLoader();
InputStream is = p5074at.getResourceAsStream(TEMPLATE_PROPERTIES);
try {
	properties.load(is);
	StringBuilder string = new StringBuilder();
	string.append(properties.getProperty("t642.template")+" "+wsaaAlertv1.trim()+","+"\n\n");
	string.append(properties.getProperty("t642.template1")+" "+wsaaAlertv2.trim()+" ");
	string.append(properties.getProperty("t642.template2")+" "+wsaaAlertv3.trim()+"."+" ");
	string.append(properties.getProperty("t642.template3")+" ");
	string.append(properties.getProperty("t642.template4")+"\n\n");
	string.append(properties.getProperty("t642.template5")+" "+"\n\n");
	string.append(properties.getProperty("t642.template6")+"\n");
	string.append(properties.getProperty("t642.template7"));

	return string.toString();
} catch (IOException e) {
	throw new IllegalArgumentException("Unable to load " + TEMPLATE_PROPERTIES);
}
}
//IBPLIFE-1357 ends
protected void d100WriteZfmc() 
{
	// Get VPMS value read if the plan is eligible for FMC Charge
	// Read above table pass chdrlnbIO.getCnttype to check if the plan is
	// set up. If not found or FMC applicable is N
	// Return from this method.
	// Then Read for coverage
	// If No return from the method

	Unltpf unltpf = new Unltpf();
	unltpf.setChdrcoy(atmodrec.company.toString());
	unltpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
	List<Unltpf> unltPfList = unltpfDAO.getFundUnltpf(unltpf);
	List<String> fundList = new ArrayList<>();
	if (unltPfList != null && !unltPfList.isEmpty()) {
			unitLinked = true;
			Unltpf tempUnlt = unltPfList.get(0);
			fundList.add(tempUnlt.getUalfnd01());
			fundList.add(tempUnlt.getUalfnd02());
			fundList.add(tempUnlt.getUalfnd03());
			fundList.add(tempUnlt.getUalfnd04());
			fundList.add(tempUnlt.getUalfnd05());
			fundList.add(tempUnlt.getUalfnd06());
			fundList.add(tempUnlt.getUalfnd07());
			fundList.add(tempUnlt.getUalfnd08());
			fundList.add(tempUnlt.getUalfnd09());
			fundList.add(tempUnlt.getUalfnd10());
	}
	else{
		unitLinked = false;
		return;
	}
		zfmcpf = new Zfmcpf();
		zfmcpf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		zfmcpf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		zfmcpf.setLife(covrlnbIO.getLife().toString());
		zfmcpf.setJlife(covrlnbIO.getJlife().toString());
		zfmcpf.setCoverage(covrlnbIO.getCoverage().toString());
		zfmcpf.setRider(covrlnbIO.getRider().toString());
		zfmcpf.setPlnsfx(Integer.valueOf(covrlnbIO.getPlanSuffix().toInt()));
		zfmcpf.setPrmcur(covrlnbIO.premCurrency.toString());
		zfmcpf.setSumins(new BigDecimal(covrlnbIO.getSumins().toString()));
		zfmcpf.setPcesdte(Integer.valueOf(covrlnbIO.premCessDate.toInt()));
		zfmcpf.setMortcls(covrlnbIO.getMortcls().toString());
		zfmcpf.setCrtable(covrlnbIO.getCrtable().toString());
		zfmcpf.setValidflag("1");
		zfmcpf.setTranno(Integer.valueOf(chdrlnbIO.getTranno().toInt()));
		zfmcpf.setZnofdue(1);
		initialize(datcon4rec.datcon4Rec);
		zfmcpf.setZfmcdat(Integer.valueOf(chdrlnbIO.getOccdate().toInt()));
		zfmcpfDAO.insertZfmcPF(zfmcpf);
	}
}
