/*
 * File: Cnvth596.java
 * Date: 29 August 2009 22:42:02
 * Author: Quipoz Limited
 * 
 * Class transformed from CNVTH596.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.tablestructures.Th596rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table TH596, field has changed from FUPCODE
*   to FUPCDES.
*
*
***********************************************************************
* </pre>
*/
public class Cnvth596 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVTH596");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String th596 = "TH596";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData th596OldRec = new FixedLengthStringData(500);
	private FixedLengthStringData th596OldCnRiskStats = new FixedLengthStringData(24).isAPartOf(th596OldRec, 0);
	private FixedLengthStringData[] th596OldCnRiskStat = FLSArrayPartOfStructure(12, 2, th596OldCnRiskStats, 0);
	private FixedLengthStringData th596OldFupcodes = new FixedLengthStringData(180).isAPartOf(th596OldRec, 24);
	private FixedLengthStringData[] th596OldFupcode = FLSArrayPartOfStructure(60, 3, th596OldFupcodes, 0);
	private FixedLengthStringData th596OldHcondate = new FixedLengthStringData(1).isAPartOf(th596OldRec, 204);
	private FixedLengthStringData th596OldTrcodes = new FixedLengthStringData(112).isAPartOf(th596OldRec, 205);
	private FixedLengthStringData[] th596OldTrcode = FLSArrayPartOfStructure(28, 4, th596OldTrcodes, 0);
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Th596rec th596rec = new Th596rec();
	private Varcom varcom = new Varcom();

	public Cnvth596() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
			start1000();
			exit1000();
		}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(th596);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMTABL");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT TH596 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(),th596)) {
			getAppVars().addDiagnostic("Table TH596 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof,"Y")
		|| isNE(itemIO.getStatuz(),varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT TH596 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
			update2000();
			rewriteTh5962080();
		}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		th596OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		th596rec.hcondate.set(th596OldHcondate);
		for (ix.set(1); !(isGT(ix,60)); ix.add(1)){
			populateItem3000();
		}
		for (ix.set(1); !(isGT(ix,12)); ix.add(1)){
			th596rec.cnRiskStat[ix.toInt()].set(th596OldCnRiskStat[ix.toInt()]);
		}
		for (ix.set(1); !(isGT(ix,12)); ix.add(1)){
			th596rec.trcode[ix.toInt()].set(th596OldTrcode[ix.toInt()]);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(th596rec.th596Rec);
	}

protected void rewriteTh5962080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(),th596)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for TH596");
		}
	}

protected void populateItem3000()
	{
		/*POPULATE*/
		th596rec.fupcdes[ix.toInt()].set(th596OldFupcode[ix.toInt()]);
		/*EXIT*/
	}
}
