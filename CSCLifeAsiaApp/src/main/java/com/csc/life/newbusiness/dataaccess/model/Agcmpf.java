package com.csc.life.newbusiness.dataaccess.model;

import java.math.BigDecimal;

public class Agcmpf {
    private long uniqueNumber; 
	private String chdrcoy;
	private String chdrnum;
	private String agntnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private int planSuffix;
	private int tranno;
	private int efdate;
	private BigDecimal annprem;
	private String basicCommMeth;
	private BigDecimal initcom;
	private String bascpy;
	private BigDecimal compay;
	private BigDecimal comern;
	private String srvcpy;
	private BigDecimal scmdue;
	private BigDecimal scmearn;
	private String rnwcpy;
	private BigDecimal rnlcdue;
	private BigDecimal rnlcearn;
	private String agentClass;
	private String termid;
	private int transactionDate;
	private int transactionTime;
	private int user;
	private String crtable;
	private int currfrom;
	private int currto;
	private String validflag;
	private int seqno;
	private int ptdate;
	private String cedagent;
	private String ovrdcat;
	private String dormantFlag;
	private String userProfile;
	private String jobName;
	private String datime;
	private BigDecimal rnwlCommGst;
	private BigDecimal initCommGst;
	
	public Agcmpf() {
	}
    public Agcmpf(Agcmpf agcmpf) {
		super();
		this.uniqueNumber = agcmpf.uniqueNumber;
		this.chdrcoy = agcmpf.chdrcoy;
		this.chdrnum = agcmpf.chdrnum;
		this.agntnum = agcmpf.agntnum;
		this.life = agcmpf.life;
		this.jlife = agcmpf.jlife;
		this.coverage = agcmpf.coverage;
		this.rider = agcmpf.rider;
		this.planSuffix = agcmpf.planSuffix;
		this.tranno = agcmpf.tranno;
		this.efdate = agcmpf.efdate;
		this.annprem = agcmpf.annprem;
		this.basicCommMeth = agcmpf.basicCommMeth;
		this.initcom = agcmpf.initcom;
		this.bascpy = agcmpf.bascpy;
		this.compay = agcmpf.compay;
		this.comern = agcmpf.comern;
		this.srvcpy = agcmpf.srvcpy;
		this.scmdue = agcmpf.scmdue;
		this.scmearn = agcmpf.scmearn;
		this.rnwcpy = agcmpf.rnwcpy;
		this.rnlcdue = agcmpf.rnlcdue;
		this.rnlcearn = agcmpf.rnlcearn;
		this.agentClass = agcmpf.agentClass;
		this.termid = agcmpf.termid;
		this.transactionDate = agcmpf.transactionDate;
		this.transactionTime = agcmpf.transactionTime;
		this.user = agcmpf.user;
		this.crtable = agcmpf.crtable;
		this.currfrom = agcmpf.currfrom;
		this.currto = agcmpf.currto;
		this.validflag = agcmpf.validflag;
		this.seqno = agcmpf.seqno;
		this.ptdate = agcmpf.ptdate;
		this.cedagent = agcmpf.cedagent;
		this.ovrdcat = agcmpf.ovrdcat;
		this.dormantFlag = agcmpf.dormantFlag;
		this.userProfile = agcmpf.userProfile;
		this.jobName = agcmpf.jobName;
		this.datime = agcmpf.datime;
		this.rnwlCommGst = agcmpf.rnwlCommGst;//ILIFE-7965
		this.initCommGst = agcmpf.initCommGst;//ILIFE-7965
	}
	public long getUniqueNumber() {
        return uniqueNumber;
    }
    public void setUniqueNumber(long uniqueNumber) {
        this.uniqueNumber = uniqueNumber;
    }
    public String getChdrcoy() {
        return chdrcoy;
    }
    public String getChdrnum() {
        return chdrnum;
    }
    public String getAgntnum() {
        return agntnum;
    }
    public String getLife() {
        return life;
    }
    public String getJlife() {
        return jlife;
    }
    public String getCoverage() {
        return coverage;
    }
    public String getRider() {
        return rider;
    }
    public int getPlanSuffix() {
        return planSuffix;
    }
    public int getTranno() {
        return tranno;
    }
    public int getEfdate() {
        return efdate;
    }
    public BigDecimal getAnnprem() {
        return annprem;
    }
    public String getBasicCommMeth() {
        return basicCommMeth;
    }
    public BigDecimal getInitcom() {
        return initcom;
    }
    public String getBascpy() {
        return bascpy;
    }
    public BigDecimal getCompay() {
        return compay;
    }
    public BigDecimal getComern() {
        return comern;
    }
    public String getSrvcpy() {
        return srvcpy;
    }
    public BigDecimal getScmdue() {
        return scmdue;
    }
    public BigDecimal getScmearn() {
        return scmearn;
    }
    public String getRnwcpy() {
        return rnwcpy;
    }
    public BigDecimal getRnlcdue() {
        return rnlcdue;
    }
    public BigDecimal getRnlcearn() {
        return rnlcearn;
    }
    public String getAgentClass() {
        return agentClass;
    }
    public String getTermid() {
        return termid;
    }
    public int getTransactionDate() {
        return transactionDate;
    }
    public int getTransactionTime() {
        return transactionTime;
    }
    public int getUser() {
        return user;
    }
    public String getCrtable() {
        return crtable;
    }
    public int getCurrfrom() {
        return currfrom;
    }
    public int getCurrto() {
        return currto;
    }
    public String getValidflag() {
        return validflag;
    }
    public int getSeqno() {
        return seqno;
    }
    public int getPtdate() {
        return ptdate;
    }
    public String getCedagent() {
        return cedagent;
    }
    public String getOvrdcat() {
        return ovrdcat;
    }
    public String getDormantFlag() {
        return dormantFlag;
    }
    public String getUserProfile() {
        return userProfile;
    }
    public String getJobName() {
        return jobName;
    }
    public String getDatime() {
        return datime;
    }
    public void setChdrcoy(String chdrcoy) {
        this.chdrcoy = chdrcoy;
    }
    public void setChdrnum(String chdrnum) {
        this.chdrnum = chdrnum;
    }
    public void setAgntnum(String agntnum) {
        this.agntnum = agntnum;
    }
    public void setLife(String life) {
        this.life = life;
    }
    public void setJlife(String jlife) {
        this.jlife = jlife;
    }
    public void setCoverage(String coverage) {
        this.coverage = coverage;
    }
    public void setRider(String rider) {
        this.rider = rider;
    }
    public void setPlanSuffix(int planSuffix) {
        this.planSuffix = planSuffix;
    }
    public void setTranno(int tranno) {
        this.tranno = tranno;
    }
    public void setEfdate(int efdate) {
        this.efdate = efdate;
    }
    public void setAnnprem(BigDecimal annprem) {
        this.annprem = annprem;
    }
    public void setBasicCommMeth(String basicCommMeth) {
        this.basicCommMeth = basicCommMeth;
    }
    public void setInitcom(BigDecimal initcom) {
        this.initcom = initcom;
    }
    public void setBascpy(String bascpy) {
        this.bascpy = bascpy;
    }
    public void setCompay(BigDecimal compay) {
        this.compay = compay;
    }
    public void setComern(BigDecimal comern) {
        this.comern = comern;
    }
    public void setSrvcpy(String srvcpy) {
        this.srvcpy = srvcpy;
    }
    public void setScmdue(BigDecimal scmdue) {
        this.scmdue = scmdue;
    }
    public void setScmearn(BigDecimal scmearn) {
        this.scmearn = scmearn;
    }
    public void setRnwcpy(String rnwcpy) {
        this.rnwcpy = rnwcpy;
    }
    public void setRnlcdue(BigDecimal rnlcdue) {
        this.rnlcdue = rnlcdue;
    }
    public void setRnlcearn(BigDecimal rnlcearn) {
        this.rnlcearn = rnlcearn;
    }
    public void setAgentClass(String agentClass) {
        this.agentClass = agentClass;
    }
    public void setTermid(String termid) {
        this.termid = termid;
    }
    public void setTransactionDate(int transactionDate) {
        this.transactionDate = transactionDate;
    }
    public void setTransactionTime(int transactionTime) {
        this.transactionTime = transactionTime;
    }
    public void setUser(int user) {
        this.user = user;
    }
    public void setCrtable(String crtable) {
        this.crtable = crtable;
    }
    public void setCurrfrom(int currfrom) {
        this.currfrom = currfrom;
    }
    public void setCurrto(int currto) {
        this.currto = currto;
    }
    public void setValidflag(String validflag) {
        this.validflag = validflag;
    }
    public void setSeqno(int seqno) {
        this.seqno = seqno;
    }
    public void setPtdate(int ptdate) {
        this.ptdate = ptdate;
    }
    public void setCedagent(String cedagent) {
        this.cedagent = cedagent;
    }
    public void setOvrdcat(String ovrdcat) {
        this.ovrdcat = ovrdcat;
    }
    public void setDormantFlag(String dormantFlag) {
        this.dormantFlag = dormantFlag;
    }
    public void setUserProfile(String userProfile) {
        this.userProfile = userProfile;
    }
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }
    public void setDatime(String datime) {
        this.datime = datime;
    }
    /*ILIFE-7965 : Starts*/
	public BigDecimal getRnwlCommGst() {
		return rnwlCommGst;
	}
	public void setRnwlCommGst(BigDecimal rnwlCommGst) {
		this.rnwlCommGst = rnwlCommGst;
	}
	public BigDecimal getInitCommGst() {
		return initCommGst;
	}
	public void setInitCommGst(BigDecimal initCommGst) {
		this.initCommGst = initCommGst;
	}
	/*ILIFE-7965 : Ends*/
	
}