/*
 * File: P5006.java
 * Date: 29 August 2009 23:53:53
 * Author: Quipoz Limited
 * 
 * Class transformed from P5006.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.procedures.UwQuestionnaireUtil;
import com.csc.life.contractservicing.procedures.UwoccupationUtil;
import com.csc.life.enquiries.dataaccess.UwlvTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtcovTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtridTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.dataaccess.model.Uwrspf;
import com.csc.life.newbusiness.screens.S5006ScreenVars;
import com.csc.life.newbusiness.tablestructures.T5657rec;
import com.csc.life.productdefinition.recordstructures.UWOccupationRec;
import com.csc.life.productdefinition.recordstructures.UWQuestionnaireRec;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5673rec;
import com.csc.life.underwriting.dataaccess.dao.UndqpfDAO;
import com.csc.life.underwriting.dataaccess.model.Undqpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.ThreadLocalStore;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                       COMPONENT SELECTION
*                       ===================
* Initialise
* ----------
*
* Skip  this  section  if  returning from an optional selection
* (current stack position action flag = '*').
*
* Clear the subfile ready for loading.
*
* Read  CHDRLNB  (RETRV) in order to obtain the contract header
* information  and  read  LIFELNB  (RETRV) for the life assured
* details.
*
* Read  the  contract definition description from T5688 for the
* contract type held on CHDRLNB.
* LIFE ASSURED AND JOINT LIFE DETAILS
*
* To obtain the life assured and joint-life details (if any) do
* the following;-
*
*       - using the life  detaiLs  passed (retrieved above from
*         LIFELNB, joint life number '00').  Look  up the  name
*         from  the  client  details  (CLTS)  and  format as  a
*         "confirmation name".
*
*       - read the joint life details using LIFELNB (joint life
*         number '01').  If  found, look up  the name  from the
*         client details (CLTS) and  format as  a "confirmation
*         name".
*
* This  program displays component parts for either a life or a
* coverage.  To determine which version is required, attempt to
* retrieve the current coverage transaction (from COVTLNB).  If
* there  is no COVTLNB record to retrieve (MRNF return status),
* coverages  and  riders  for the entire contract are required.
* Otherwise, just riders for the current coverage are required.
*
* Read  the  contract  structure  table  (T5673),  accessed  by
* contract  type  (from  CHDRLNB) and effective on the original
* inception  date  of  the contract. When processing this table
* item,  it  can  continue  onto  additional  (unlimited) table
* items.  These  should  be  read in turn during the processing
* described below.  The  original  inception  date must also be
* used to provide the correct version of each item read.
*
* THE CASE OF ALL COVERAGES AND RIDERS
*
* Taking one coverage at  a  time, one record is written to the
* subfile for the  coverage,  with one for each rider attaching
* to it. The coverage record is written with a blank rider code
* and the rider with a blank coverage code. In the table, there
* is  space for six riders against each coverage. It there is a
* requirement for more than six riders on a coverage, they will
* be  continued  on  the next line.  In this case, the coverage
* will  be  left  blank  to  indicate  a continuation.  NB, the
* coverage  on the last line of a table item could be continued
* onto  another  item.  Look  up  the  long  description of the
* coverage/rider   from  the  general  coverage  details  table
* (T5687) for each subfile record written.
*
* It is possible that the proposal has already been set up when
* a user  requests  this  screen.  Read  the  current  coverage
* transaction   records   for   the  coverage  being  processed
* (COVTCOV,   keyed   company,   contract-no,   coverage  type,
* coverage-no; select  rider-no  = 0). Accumulate the number of
* DIFFERENT coverages  of  the same type already stored. (There
* could be multiple  records  with  the  same  key.  Ignore the
* duplicates.) Whenever  a  transaction is read for a different
* coverage, keep  the  number  of the highest transaction read.
* This is needed  to  calculate  the next coverage number to be
* allocated. (The  coverage  number  needed  is the highest one
* used by any coverage  type.) If there is already at least one
* coverage transaction, put  a  '+'  in  the  select field.  If
* there is already the maximum allowable (as defined in T5673),
* protect the select field so that no more can be requested. If
* the coverage is  mandatory  (this is indicated by an "R", for
* required, in  the  coverage required indicator) and there are
* no transactions for  the  coverage on file, "select" the line
* by moving "X" in to the select field and protect it.
*
* When adding  rider details to  subfile, they may be mandatory
* (indicated in a similar way to mandatory coverages above). In
* this case, "select" the  line  by moving "X" in to the select
* field and protect  it.  -  note  that rider selection will be
* ignored if the coverage is not selected.
*
* Details of all  coverages  and  riders  for the contract type
* should be loaded in this case.
*
* THE CASE OF ALL RIDERS FOR ONE COVERAGE ONLY
*
* In this case, a COVTLNB record will have been retrieved. This
* will hold details of  the  coverage to which riders are to be
* added. Write a coverage  record to the subfile, with a '+' in
* the selection field,  and with this field protected. (Look up
* the description as above.)
*
* Search through  the  contract structure table entries (T5673)
* for the applicable  coverage  code. This will give the riders
* attachable to this  coverage  in this case. Write a record to
* the subfile for each applicable rider.
*
* It is possible that  the riders have already been set up when
* a  user   requests   this  screen.  Read  the  current  rider
* transaction record  for  the  rider being processed (COVTRID,
* keyed  company, contract-no, coverage-no, rider type;  select
* rider-no *ne 0).  If  there  is already a record, this is the
* maximum allowable, so  put  a  '+'  in  the  select field and
* protect it so that no more can be requested.  If the rider is
* mandatory (this is  indicated by an "R", for required, in the
* rider required indicator) and there is no transaction for the
* rider on file, "select"  the  line  by  moving  "X" in to the
* select field and protect it.
*
* Whenever a transaction  is  read  for a different rider, keep
* the number of  the  highest transaction read.  This is needed
* to calculate the next rider number to be allocated.
*
* In all cases, load  all pages required in the subfile and set
* the subfile more indicator to no.
*
* Validation
* ----------
*
* Skip  this  section  if  returning from an optional selection
* (current stack position action flag = '*').
*
* Read  all   modified  subfile  records.  The  only  allowable
* selections are  '+',  'X'  and  blank.  Ignore inconsistences
* between riders being selected for coverages which have not.
*
* Updating
* --------
*
* This program performs no updating.
*
* Next program
* ------------
*
* If returning from  a  selection (action action is '*'), check
* that  if  the  current  component  was  mandatory,  that  his
* actually been entered.  (Use  a  hidden  field in the subfile
* record when loading in  the 1000 section to indicate if it is
* a required  section?)  Read the current coverage/ transaction
* record again (COVTLNB,  the  key  was  set up by this program
* previously.) If  there  is  no  transaction for the mandatory
* component, KEEPS the  current  key  again and exit.
*
*
*
*
*
* If not returning  from  a  component (stack action is blank),
* save the next four  programs currently on the stack. Read the
* first record from  the  subfile. If this is not selected ('X'
* in select field), read  the  next  one  and  so  on,  until a
* selected record is  found,  or  the  end  of  the  subfile is
* reached.
*
* For the case  when  all  coverages  and riders are displayed,
* keep track of whether  a  coverage  is  selected or not. If a
* coverage is not  selected,  skip  all its riders even if they
* are selected.  If  a  coverage  is  selected, re-set the next
* rider sequence number to zero.
*
* For the case  when  only  the  riders  for  one  coverage are
* displayed, the coverage cannot be selected. Process the rider
* selected even though the coverage has not.
*
* Once the end  of  the  subfile  has been reached, restore the
* previously saved  four  programs, blank out the stack action,
* add one to the pointer and exit.
*
* If a subfile  record  has been selected, look up the programs
* required to  be  processed  from  the coverage/rider programs
* table (T5671  -  accessed  by transaction number concatenated
* with coverage/rider code from the subfile record). Move these
* four programs into  the  program  stack  and  set the current
* stack action to '*'  (so  that the system will return to this
* program to process the next one).
*
* Set up the  key details of the coverage/rider to be processed
* (in COVTLNB using  the KEEPS function) by the called programs
* as follows:
*
*       Company - WSSP company
*       Contract no - from CHDRLNB
*       Life number - from LIFELNB
*       Coverage number -  from COVTLNB if read in the 1000
*            section  (i.e.  adding  to  coverage case), or
*            sequentially from the number calculated in the
*            1000  section for each coverage selected (i.e.
*            adding to life case)
*       Rider number -  '00'  if  this  is  a  coverage, or
*            sequentially from the number calculated above
*
* Add one to  the  program  pointer  and  exit  to  process the
* required generic component.
*
******************Enhancements for Life Asia 1.0****************
*
* In order to allow components to be selected on a specific life,
* a new indicatore has been introduced to T5673. This indicator
* specifies whether the component can be selected for just the
* primary life, or just the subsequent lives or for all lives.
* This module uses the indicator T5673-ZRLIFIND to determine
* which component may be selected. The processing is as follows :
*
* - For each component found on T5673, check the T5673-ZRLIFIND :
*
*   - If we are processing a primary life and the T5673-ZRLIFIND
*     contains an 'S' for that component, then don't set up the
*     component details for display.
*
*   - If we are processing subsequent lives and T5673-ZRLIFIND
*     contains an 'P' for that component, then don't set up the
*     component details for display.
*****************************************************************
* </pre>
*/
public class P5006 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5006");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	protected PackedDecimalData wsaaSub1 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub2 = new PackedDecimalData(2, 0).init(1);
	private PackedDecimalData wsaaSub4 = new PackedDecimalData(2, 0).init(ZERO);
	private PackedDecimalData wsaaSub5 = new PackedDecimalData(2, 0).init(ZERO);
	private PackedDecimalData wsaaCcount = new PackedDecimalData(2, 0).init(ZERO);
	private String wsaaNoSelect = "N";
	private String wsaaLifeorcov = "N";
	private PackedDecimalData wsaaSubTest = new PackedDecimalData(2, 0).init(ZERO);
	private PackedDecimalData wsaaRemain = new PackedDecimalData(2, 0).init(ZERO);

	private FixedLengthStringData wsaaRiderRider = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaRider = new ZonedDecimalData(2, 0).isAPartOf(wsaaRiderRider, 0);

	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaRiderRider, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaRiderR = new FixedLengthStringData(2).isAPartOf(filler, 0);

	private FixedLengthStringData wsaaCoverCover = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaCoverage = new ZonedDecimalData(2, 0).isAPartOf(wsaaCoverCover, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(wsaaCoverCover, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaCoverageR = new FixedLengthStringData(2).isAPartOf(filler1, 0);

	private FixedLengthStringData wsaaNextCovridno = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaNextCovno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 0);
	private FixedLengthStringData wsaaNextRidno = new FixedLengthStringData(2).isAPartOf(wsaaNextCovridno, 2);

	private FixedLengthStringData filler2 = new FixedLengthStringData(4).isAPartOf(wsaaNextCovridno, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaNextCovnoR = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0).setUnsigned();
	private ZonedDecimalData wsaaNextRidnoR = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2).setUnsigned();

	private FixedLengthStringData wsaaLineType = new FixedLengthStringData(1);
	private Validator wsaaLcover = new Validator(wsaaLineType, "1");
	private Validator wsaaCcover = new Validator(wsaaLineType, "2");

	private FixedLengthStringData wsaaCoverFlag = new FixedLengthStringData(1);
	private Validator wsaaCover = new Validator(wsaaCoverFlag, "Y");

	private FixedLengthStringData wsaaExitFlag = new FixedLengthStringData(1);
	private Validator wsaaExit = new Validator(wsaaExitFlag, "Y");

	private FixedLengthStringData wsaaSelectFlag = new FixedLengthStringData(1);
	private Validator wsaaSelection = new Validator(wsaaSelectFlag, "Y");

	private FixedLengthStringData wsaaImmexitFlag = new FixedLengthStringData(1);
	private Validator wsaaImmExit = new Validator(wsaaImmexitFlag, "Y");

	private FixedLengthStringData wsaaProgramSave = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaSecProg = FLSArrayPartOfStructure(4, 5, wsaaProgramSave, 0);

	private FixedLengthStringData wsaaConcatName = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrancode = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaConcatName, 4);
	private FixedLengthStringData wsaaLifeKey = new FixedLengthStringData(64);
	private String wsaaSkipCover = "";

	private FixedLengthStringData wsaaKey = new FixedLengthStringData(13);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaKey, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaKey, 1);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaKey, 9);
	private FixedLengthStringData wsaaJlife = new FixedLengthStringData(2).isAPartOf(wsaaKey, 11);
		/* ERRORS */
	private String e005 = "E005";
	private String h999 = "H999";
	private String erep = "EREP";
		/* TABLES */
	private String t5671 = "T5671";
	private String t5673 = "T5673";
	private String covtlnbrec = "COVTLNBREC";
	private String lifelnbrec = "LIFELNBREC";
		/*Contract header - life new business*/
	protected ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*New business transactions - coverages on*/
	private CovtcovTableDAM covtcovIO = new CovtcovTableDAM();
		/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
		/*New business transactions - riders only*/
	private CovtridTableDAM covtridIO = new CovtridTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life and joint life details - new busine*/
	protected LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private T5671rec t5671rec = new T5671rec();
	protected T5673rec t5673rec = new T5673rec();
	private Batckey wsaaBatckey = new Batckey();
	private Wssplife wssplife = new Wssplife();
	protected S5006ScreenVars sv = ScreenProgram.getScreenVars( S5006ScreenVars.class);
	int count=0;
	private UwlvTableDAM uwlvIO = new UwlvTableDAM();
	private UwoccupationUtil uwoccupationUtil = getApplicationContext().getBean("uwoccupationUtil", UwoccupationUtil.class);
	private boolean uwFlag = false;
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private UWOccupationRec uwOccupationRec = new UWOccupationRec();
	private UwQuestionnaireUtil uwQuestionnaireUtil = getApplicationContext().getBean("uwQuestionnaireUtil", UwQuestionnaireUtil.class);
	private UWQuestionnaireRec uwQuestionnaireRec = new UWQuestionnaireRec();
	private List<String> questions = null;
	public static final String UWQUESTIONNAIRECACHE= "UWQUESTIONNAIRE";
	public static final String UWREC_CACHE = "UWREC_CACHE";
	public static final String AUTO_TERMS_LIST = "AUTO_TERMS_LIST";
	public static final String COMUW_RESULT_CACHE = "COMUW_RESULT_CACHE";
	private int seqnbr = 1;
	private UndqpfDAO undqpfDAO = getApplicationContext().getBean("undqpfDAO", UndqpfDAO.class);
	
	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		exit1090, 
		exit1190, 
		subfileLoad1210, 
		riderCheck1440, 
		exit1490, 
		exit1890, 
		preExit, 
		selectCheck2060, 
		continue2080, 
		exit2090, 
		exit3090, 
		exit4090, 
		exit4190, 
		exit4290, 
		exit4490, 
		exit4590, 
		exit4690
	}

	public P5006() {
		super();
		screenVars = sv;
		new ScreenModel("S5006", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void initialise1000()
	{
		try {
			skipOnFlag1010();
			initialiseSubfile1020();
			retreiveHeader1030();
			readContractLongdesc1040();
			retreiveLife1050();
			coverRiderTable1070();
			// IBPLIFE-1490
			uwFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP123", appVars, smtpfxcpy.item.toString());
			if(uwFlag) {
				sv.uwFlag = 1;
				initialiseUWparameter();
			}else {
				sv.uwFlag = 2;
			}
			retrieveCurrCoverage1080();
		}
		catch (GOTOException e){
		}
	}

protected void skipOnFlag1010()
	{
		scrnparams.subfileRrn.set(1);
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsaaBatckey.set(wsspcomn.batchkey);
	}

protected void initialiseSubfile1020()
	{
		wsaaSub1.set(1);
		wsaaSub2.set(1);
		wsaaSub4.set(1);
		wsaaSub5.set(1);
		wsaaLineType.set(ZERO);
		scrnparams.subfileMore.set("Y");
		wsaaCoverFlag.set("N");
		wsaaExitFlag.set("N");
		wsaaSelectFlag.set("N");
		wsaaNoSelect = "N";
		wsaaImmexitFlag.set("N");
		wsaaProgramSave.set(SPACES);
		wsaaConcatName.set(SPACES);
		wsaaNextCovno.set("00");
		wsaaNextRidno.set("00");
		wsaaSub1.set(1);
		wsaaSub2.set(1);
		wsaaCcount.set(ZERO);
		wsaaCoverage.set(ZERO);
		wsaaRider.set(ZERO);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		scrnparams.function.set(varcom.sclr);
		processScreen("S5006", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void retreiveHeader1030()
	{
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void readContractLongdesc1040()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("T5688");
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		sv.ctypedes.set(descIO.getLongdesc());
		checkLife1800();
	}

protected void retreiveLife1050()
	{
		lifelnbIO.setFunction(varcom.retrv);
		lifeDetails1100();
		/*LIFE-OR-JLIFE*/
		lifelnbIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, lifelnbIO);
		
		questions = (List<String>)ThreadLocalStore.get(UWQUESTIONNAIRECACHE);
		if(questions == null){
			List<Undqpf> undqList = undqpfDAO.searchUnqpf(lifelnbIO.getChdrcoy().toString().trim(), lifelnbIO.getChdrnum().toString().trim(), 
					lifelnbIO.getLife().toString().trim(), lifelnbIO.getJlife().toString().trim());
			questions = new ArrayList<String>();
			for(Undqpf u : undqList){
				if("Y".equals(u.getAnswer().trim())){
					questions.add(u.getQuestidf());
				}
			}
		}
		
		lifelnbIO.setJlife("01");
		lifelnbIO.setFunction(varcom.readr);
		lifeDetails1100();
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemtabl(t5673);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
	}

protected void coverRiderTable1070()
	{
		itdmIO.setItemcoy(wsspcomn.company);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),wsspcomn.company)
		|| isNE(itdmIO.getItemtabl(),t5673)
		|| (isNE(itdmIO.getItemitem(),chdrlnbIO.getCnttype())
		&& isNE(itdmIO.getItemitem(),t5673rec.gitem))
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			sv.chdrnumErr.set(h999);
			itdmIO.setStatuz(varcom.endp);
		}
		else {
			t5673rec.t5673Rec.set(itdmIO.getGenarea());
		}
	}

protected void retrieveCurrCoverage1080()
	{
		covtlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.oK)) {
			wsaaLineType.set("2");
		}
		else {
			wsaaLineType.set("1");
		}

		while ( !(isEQ(scrnparams.subfileMore,"N"))) {
			loadSubfile1200();
		}
		
	}

private void initialiseUWparameter() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		uwOccupationRec = new UWOccupationRec();
		uwOccupationRec.indusType.set(cltsIO.getStatcode());
		uwOccupationRec.occupCode.set(cltsIO.getOccpcode());
		uwOccupationRec.transEffdate.set(datcon1rec.intDate);
		uwlvIO.setParams(SPACES);
		uwlvIO.setCompany(wsspcomn.company);
		uwlvIO.setUserid(wsspcomn.userid);
		uwlvIO.setFormat("UWLVREC");
		uwlvIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, uwlvIO);
		
		uwQuestionnaireRec = new UWQuestionnaireRec();
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		uwQuestionnaireRec.setTransEffdate(datcon1rec.intDate.getbigdata());
		uwQuestionnaireRec.setContractType(chdrlnbIO.getCnttype().toString());
		sv.mainCovrDisFlag = -1;
		
		ThreadLocalStore.put(COMUW_RESULT_CACHE, new HashMap<>());
		ThreadLocalStore.put(AUTO_TERMS_LIST, null);
}

	/**
	 * 
	 * @param crtable
	 * @return 0:accept, 1:Decline & user has authority, 2:Decline & user doesn't has authority
	 */
	private int checkComponent(String crtable) {
		String company = wsspcomn.company.toString();
		String fsuco = wsspcomn.fsuco.toString();
		String cnttype = chdrlnbIO.getCnttype().toString();
		
		if (crtable != null && !crtable.isEmpty()) {
			int result = checkOccupation(crtable, company, fsuco, cnttype);
			int flag = checkQuestion(crtable, company);
			if (result == 2 || result == 1 || flag == 1 || flag == 2) {
				return getUWIntResult(result, flag, crtable);
			}
		}
		return 0;
	}
	private int getUWIntResult(int result, int flag, String crtable) {
		if (!"Y".equals(uwlvIO.getUwdecision().toString().trim()) && (result == 2 || flag == 2)) {
			// DISABLED
			sv.select.set(SPACE);
			sv.selectOut[varcom.pr.toInt()].set("Y");
			return 2;
		}
		cacheResult(crtable);
		// do not pop if disable
		if(isEQ(sv.selectOut[varcom.pr.toInt()], "Y")) {
			return 0;
		}
		return 1;
	}
	private void cacheResult(String crtable) {
		Map<String, List<String>> comuwResult = (Map<String, List<String>>)ThreadLocalStore.get(COMUW_RESULT_CACHE);
		if(comuwResult == null) {
			comuwResult = new HashMap<>();
			ThreadLocalStore.put(COMUW_RESULT_CACHE, comuwResult);
		}
		String chdrnum = chdrlnbIO.getChdrnum().toString();
		if(comuwResult.containsKey(chdrnum)){
			comuwResult.get(chdrnum).add(crtable);
		}else {
			List<String> comList = new ArrayList<>();
			comList.add(crtable);
			comuwResult.put(chdrnum, comList);
		}
	}
	private int checkOccupation(String crtable, String company, String fsuco, String cnttype) {
		int result = uwoccupationUtil.getUWResult(uwOccupationRec, company, fsuco, cnttype, crtable);
		if (sv.getUwResultMap() != null) {
			sv.getUwResultMap().put(crtable, result);
		}
		return result;
	}

	private int checkQuestion(String crtable, String company) {
		int flag = 0;
		if (questions != null) {
			for (String question : questions) {
				UWQuestionnaireRec uwQuestionnaireRecReturn = uwQuestionnaireUtil.getUWMessage(uwQuestionnaireRec,
						wsspcomn.language.toString(), company, question, crtable, "");
				if (!uwQuestionnaireRecReturn.getOutputUWDec().isEmpty()
						&& uwQuestionnaireRecReturn.getOutputUWDec().contains("Decline")) {
					sv.getUwQuestionResultMap().put(crtable, 2);
					flag = 2;
					break;
				}
				if (!uwQuestionnaireRecReturn.getOutputUWDec().isEmpty()
						&& uwQuestionnaireRecReturn.getOutputUWDec().contains("Conditionally Accepted")) {
					flag = 1;
					sv.getUwQuestionResultMap().put(crtable, 1);
					break;
				}
				sv.getUwQuestionResultMap().put(crtable, 0);
			}
		}
		return flag;
	}
protected void lifeDetails1100()
	{
		try {
			readLife1110();
			retreiveClientDetails1120();
			readLifeJlife1140();
		}
		catch (GOTOException e){
		}
	}

protected void readLife1110()
	{
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)
		|| isEQ(lifelnbIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1190);
		}
	}

protected void retreiveClientDetails1120()
	{
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setClntpfx("CN");
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/*CLIENT-NAME-FORMAT*/
		plainname();
	}

protected void readLifeJlife1140()
	{
		if (isEQ(lifelnbIO.getJlife(),"00")
		|| isEQ(lifelnbIO.getJlife(),SPACES)) {
			lifeToScreen1150();
		}
		else {
			jlifeToScreen1160();
		}
		goTo(GotoLabel.exit1190);
	}

protected void lifeToScreen1150()
	{
		sv.life.set(lifelnbIO.getLife());
		sv.lifcnum.set(lifelnbIO.getLifcnum());
		sv.linsname.set(wsspcomn.longconfname);
		sv.jlife.set(SPACES);
	}

protected void jlifeToScreen1160()
	{
		sv.jlife.set(lifelnbIO.getJlife());
		sv.jlifcnum.set(lifelnbIO.getLifcnum());
		sv.jlinsname.set(wsspcomn.longconfname);
	}

protected void loadSubfile1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case subfileLoad1210: {
					subfileLoad1210();
				}
				case riderCheck1440: {
					riderCheck1440();
				}
				case exit1490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void subfileLoad1210()
	{
		wsaaExitFlag.set("N");
		if (isEQ(covtlnbIO.getStatuz(),varcom.oK)) {
			wsaaLifeorcov = "C";
			coverageTrans1410();
		}
		else {
			wsaaLifeorcov = "L";
			lifeTrans1310();
		}
		if ((isEQ(wsaaLifeorcov,"C")
		&& (isGT(wsaaSub1,8)
		|| isGT(wsaaSub2,48)))
		|| (isEQ(wsaaLifeorcov,"L")
		&& (isGT(wsaaSub1,8)
		&& isGT(wsaaSub2,48)))) {
			if (isNE(t5673rec.gitem,SPACES)) {
				itdmIO.setItemitem(t5673rec.gitem);
				wsaaSub1.set(1);
				wsaaSub2.set(1);
				scrnparams.subfileMore.set("Y");
				coverRiderTable1070();
				if (isEQ(itdmIO.getStatuz(),varcom.oK)
				|| isEQ(itdmIO.getStatuz(),"FULL")) {
					goTo(GotoLabel.subfileLoad1210);
				}
				else {
					scrnparams.subfileMore.set("N");
				}
			}
			else {
				scrnparams.subfileMore.set("N");
			}
		}
		goTo(GotoLabel.exit1490);
	}

protected void lifeTrans1310()
	{
		if (wsaaLcover.isTrue()) {
			checkCover1330();
			wsaaLineType.set("3");
		}
		else {
			for (int loopVar1 = 0; !(loopVar1 == 6); loopVar1 += 1){
				riderFile1360();
			}
			wsaaLineType.set("1");
		}
		if (isLTE(wsaaSub1,8)
		&& isLTE(wsaaSub2,48)) {
			endSubfile1320();
		}
	}

protected void endSubfile1320()
	{
		if (isEQ(t5673rec.ctable[wsaaSub1.toInt()],SPACES)
		&& isEQ(t5673rec.rtable[wsaaSub2.toInt()],SPACES)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			scrnparams.subfileMore.set("Y");
		}
	}

protected void checkCover1330()
	{
		wsaaExitFlag.set("N");
		wsaaNoSelect = "N";
		if (isNE(t5673rec.ctable[wsaaSub1.toInt()],SPACES)) {
			wsaaCcount.set(ZERO);
			covtcovIO.setDataKey(SPACES);
			covtcovIO.setChdrcoy(chdrlnbIO.getChdrcoy());
			covtcovIO.setChdrnum(chdrlnbIO.getChdrnum());
			covtcovIO.setLife(lifelnbIO.getLife());
			covtcovIO.setCrtable(t5673rec.ctable[wsaaSub1.toInt()]);
			covtcovIO.setFunction("BEGN");
			while ( !(wsaaExit.isTrue())) {
				checkCovtcov1340();
			}
			
			coverValidation1350();
			if (isEQ(wsaaSkipCover,"N")) {
				coverToScreen1600();
			}
		}
		wsaaSub1.add(1);
	}

protected void checkCovtcov1340()
	{
		SmartFileCode.execute(appVars, covtcovIO);
		if (isNE(covtcovIO.getStatuz(),varcom.oK)
		&& isNE(covtcovIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtcovIO.getParams());
			fatalError600();
		}
		if (isNE(covtcovIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isNE(covtcovIO.getLife(),lifelnbIO.getLife())
		|| isEQ(covtcovIO.getStatuz(),varcom.endp)) {
			wsaaExitFlag.set("Y");
			covtcovIO.setCoverage("00");
		}
		else {
			if (isEQ(covtcovIO.getCrtable(),t5673rec.ctable[wsaaSub1.toInt()])) {
				wsaaCcount.add(1);
			}
		}
		if (isGT(covtcovIO.getCoverage(),wsaaNextCovno)) {
			wsaaNextCovno.set(covtcovIO.getCoverage());
		}
		covtcovIO.setFunction(varcom.nextr);
	}

protected void coverValidation1350()
	{
		if (isGT(wsaaCcount,ZERO)) {
			sv.select.set("+");
		}
		if (isEQ(t5673rec.ctmaxcov[wsaaSub1.toInt()],wsaaCcount)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
			wsaaNoSelect = "Y";
		}
		if (isEQ(t5673rec.creq[wsaaSub1.toInt()],"R")
		&& isLT(wsaaCcount,t5673rec.ctmaxcov[wsaaSub1.toInt()])
		&& isNE(sv.select,"+")) {
			sv.select.set("X");
/*			ILIFE-1097 Start,field should not be disabled when count is less than t5673rec 
			as in this case coverage can be added */ 
//			sv.selectOut[varcom.pr.toInt()].set("Y");
			//ILIFE-1097 End
			sv.hrequired.set("Y");
		}
		wsaaSkipCover = "N";
		if (isEQ(t5673rec.zrlifind[wsaaSub1.toInt()],"P")
		&& isNE(lifelnbIO.getLife(),"01")) {
			wsaaSkipCover = "Y";
		}
		if (isEQ(t5673rec.zrlifind[wsaaSub1.toInt()],"S")
		&& isEQ(lifelnbIO.getLife(),"01")) {
			wsaaSkipCover = "Y";
		}
	}

protected void riderFile1360()
	{
		sv.ctable.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		rider1360CustomerSpecific();
		if (isNE(t5673rec.rtable[wsaaSub2.toInt()],SPACES)) {
			if (isEQ(wsaaSkipCover,"N")) {
				riderToScreen1700();
			}
		}
		wsaaSub2.add(1);
	}

protected void rider1360CustomerSpecific(){
	
}

protected void coverageTrans1410()
	{
		if (wsaaCcover.isTrue()) {
			//wsaaSub1.set(1);
			while ( !(wsaaExit.isTrue())) {
				findCover1420();
			}
			wsaaSub1.add(1);
		}
		else {
			for (int loopVar1 = 0; !(loopVar1 == 6); loopVar1 += 1){
				riderInfo1430();
			}
		}
	}

protected void findCover1420()
	{
		wsaaCoverageR.set(covtlnbIO.getCoverage());
		wsaaNextRidno.set(ZERO);
		if (isEQ(wsaaNoSelect,"Y")) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		wsaaSkipCover = "N";
		if (isEQ(t5673rec.zrlifind[wsaaSub1.toInt()],"P")
		&& isNE(lifelnbIO.getLife(),"01")) {
			wsaaSkipCover = "Y";
		}
		if (isEQ(t5673rec.zrlifind[wsaaSub1.toInt()],"S")
		&& isEQ(lifelnbIO.getLife(),"01")) {
			wsaaSkipCover = "Y";
		}
		if (isEQ(covtlnbIO.getCrtable(),t5673rec.ctable[wsaaSub1.toInt()])) {
			sv.select.set("+");
			wsaaExitFlag.set("Y");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			if(isEQ(wsaaSkipCover, "N")) {
				coverToScreen1600();
			}
			wsaaLineType.set("3");
		}
		else {
			if (isLTE(wsaaSub1,8)) {
				wsaaSub1.add(1);
			}
			wsaaSub2.add(6);
		}
		if (isGT(wsaaSub1,8)) {
			wsaaExitFlag.set("Y");
		}
	}

protected void riderInfo1430()
	{
		sv.ctable.set(SPACES);
		sv.hrequired.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.rtable.set(SPACES);
		sv.select.set(SPACES);
		sv.selectOut[varcom.pr.toInt()].set(SPACES);
		compute(wsaaSubTest, 0).setDivide(wsaaSub2, (6));
		wsaaRemain.setRemainder(wsaaSubTest);
		wsaaSubTest.add(1);
		if (isEQ(wsaaRemain,1)
		&& isGTE(wsaaSubTest,wsaaSub1)) {
			if (isNE(t5673rec.ctable[wsaaSubTest.toInt()],SPACES)) {
				scrnparams.subfileMore.set("N");
				wsaaLineType.set("2");
				goTo(GotoLabel.riderCheck1440);
			}
		}
		if (isEQ(t5673rec.rtable[wsaaSub2.toInt()],SPACES)) {
			//scrnparams.subfileMore.set("N");
			wsaaLineType.set("2");
		}
		else if(isEQ(wsaaSkipCover, "N")) {
			scrnparams.subfileMore.set("Y");
			riderCheck1440();
			riderToScreen1700();
		}
		if (isEQ(wsaaSub2,48)) {
			scrnparams.subfileMore.set("N");
		}
		else {
			wsaaSub2.add(1);
		}
	}

protected void riderCheck1440()
	{
		covtridIO.setDataKey(SPACES);
		covtridIO.setChdrcoy(wsspcomn.company);
		covtridIO.setChdrnum(chdrlnbIO.getChdrnum());
		covtridIO.setLife(lifelnbIO.getLife());
		covtridIO.setCoverage(covtlnbIO.getCoverage());
		covtridIO.setCrtable(t5673rec.rtable[wsaaSub2.toInt()]);
		covtridIO.setFunction("READR");
		SmartFileCode.execute(appVars, covtridIO);
		if (isNE(covtridIO.getStatuz(),varcom.oK)
		&& isNE(covtridIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(covtridIO.getParams());
			fatalError600();
		}
		if (isEQ(covtridIO.getStatuz(),varcom.oK)) {
			sv.select.set("+");
			sv.selectOut[varcom.pr.toInt()].set("Y");
			if (isGT(covtridIO.getRider(),wsaaNextRidno)) {
				wsaaNextRidno.set(covtridIO.getRider());
			}
		}
	}

protected void coverToScreen1600()
	{
		coverToLine1610();
	}

protected void coverToLine1610()
	{
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("T5687");
		descIO.setDescitem(t5673rec.ctable[wsaaSub1.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.ctable.set(t5673rec.ctable[wsaaSub1.toInt()]);
		// IBPLIFE-1490
		if(uwFlag) {
			sv.mainCovrDisFlag = checkComponent(sv.ctable.toString());
		}
		cover1610CustomerSpecific();
		sv.rtable.set(SPACES);
		sv.longdesc.set(descIO.getLongdesc());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5006", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void cover1610CustomerSpecific(){
	
}

protected void riderToScreen1700()
	{
		riderToLine1710();
	}

protected void riderToLine1710()
	{
		sv.rtable.set(SPACES);
		if (isEQ(t5673rec.rreq[wsaaSub2.toInt()],"R")) {
			if (isNE(sv.select,"+")) {
				sv.select.set("X");
				sv.selectOut[varcom.pr.toInt()].set("Y");
				sv.hrequired.set("Y");
			}
		}
		sv.ctable.set(SPACES);
		sv.rtable.set(t5673rec.rtable[wsaaSub2.toInt()]);
		if(uwFlag) {
			if(sv.mainCovrDisFlag == 2) {
				sv.select.set(SPACE);
				sv.selectOut[varcom.pr.toInt()].set("Y");
			}else {
				checkComponent(sv.rtable.toString());// sv.uwResultMap
			}
		}
		rider1710CustomerSpecific1();
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl("T5687");
		descIO.setDescitem(t5673rec.rtable[wsaaSub2.toInt()]);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setStatuz(varcom.oK);
			descIO.setLongdesc("??????????????????????????????");
		}
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.longdesc.set(descIO.getLongdesc());
		scrnparams.function.set(varcom.sadd);
		processScreen("S5006", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		rider1710CustomerSpecific2();
	}

protected void rider1710CustomerSpecific1(){
	
}
protected void rider1710CustomerSpecific2(){
	
}

protected void checkLife1800()
	{
		try {
			readLife1810();
		}
		catch (GOTOException e){
		}
	}

protected void readLife1810()
	{
		lifelnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)
		&& isNE(lifelnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isEQ(lifelnbIO.getStatuz(),varcom.mrnf)
		|| isEQ(lifelnbIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1890);
		}
		if (isEQ(lifelnbIO.getJlife(),"00")) {
			goTo(GotoLabel.exit1890);
		}
		wsaaChdrcoy.set(lifelnbIO.getChdrcoy());
		wsaaChdrnum.set(lifelnbIO.getChdrnum());
		wsaaLife.set(lifelnbIO.getLife());
		wsaaJlife.set("00");
		lifelnbIO.setParams(SPACES);
		lifelnbIO.setDataKey(wsaaKey);
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			fatalError600();
		}
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		scrnparams.subfileRrn.set(1);
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validateSubfile2050();
				}
				case selectCheck2060: {
					selectCheck2060();
				}
				case continue2080: {
					continue2080();
				}
				case exit2090: {
					exit2090CustomerSpecific();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void exit2090CustomerSpecific(){
	
}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*ROLL-UP*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			wsspcomn.edterror.set(varcom.oK);
		}
		if (isEQ(scrnparams.statuz,varcom.rolu)
		|| isEQ(scrnparams.statuz,varcom.rold)) {
			goTo(GotoLabel.exit2090);
		}
	}

protected void validateSubfile2050()
	{
		scrnparams.function.set(varcom.sstrt);
		processScreen("S5006", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void selectCheck2060()
	{
	
	if (isEQ(sv.select,"X")) {
		count++;
	}
		if (isEQ(sv.select,SPACES)
		|| isEQ(sv.select,"+")
		|| isEQ(sv.select,"X")) {
			goTo(GotoLabel.continue2080);
		}
		sv.selectErr.set(e005);
		sv.selectOut[varcom.pr.toInt()].set("N");
		wsspcomn.edterror.set("Y");
		scrnparams.function.set(varcom.supd);
		processScreen("S5006", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void continue2080()
	{
	
		scrnparams.function.set(varcom.srdn);
		processScreen("S5006", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isNE(scrnparams.statuz,varcom.endp)) {
			goTo(GotoLabel.selectCheck2060);
			
		}
		
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			if (count==0 && !uwFlag) {
				sv.selectErr.set(erep);
				sv.selectOut[varcom.pr.toInt()].set("N");
				wsspcomn.edterror.set("Y");
				scrnparams.function.set(varcom.supd);
				processScreen("S5006", sv);
				if (isNE(scrnparams.statuz,varcom.oK)) {
					syserrrec.statuz.set(scrnparams.statuz);
					fatalError600();
				}
					}
		}
		uwCheck();
	}
private void uwCheck() {
	if(uwFlag) {
		if (isEQ(sv.uworflag,"Y")) {
			if (isEQ(sv.uworreason,SPACE)) {
				sv.selectErr.set("RPJM");
				sv.selectOut[varcom.pr.toInt()].set("N");
				wsspcomn.edterror.set("Y");
			}
		}
	}
}
protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		goTo(GotoLabel.exit3090);
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			initSelection4100();
		}
		else {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			scrnparams.function.set(varcom.srdn);
			if (isEQ(sv.hrequired,"Y")) {
				verifyComponent4200();
			}
		}
		if (wsaaImmExit.isTrue()) {
			goTo(GotoLabel.exit4090);
		}
		verifyNextSelect4300();
		if (wsaaImmExit.isTrue()) {
			goTo(GotoLabel.exit4090);
		}
		coverRiderProgs4500();
		readProgramTables4600();
	}

protected void initSelection4100()
	{
		try {
			saveNextProgs4110();
		}
		catch (GOTOException e){
		}
	}

protected void saveNextProgs4110()
	{
		compute(wsaaSub4, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub5.set(1);
		for (int loopVar2 = 0; !(loopVar2 == 4); loopVar2 += 1){
			loop14120();
		}
		scrnparams.function.set(varcom.sstrt);
		goTo(GotoLabel.exit4190);
	}

protected void loop14120()
	{
		wsaaSecProg[wsaaSub5.toInt()].set(wsspcomn.secProg[wsaaSub4.toInt()]);
		wsaaSub4.add(1);
		wsaaSub5.add(1);
	}

protected void verifyComponent4200()
	{
		try {
			verifyReqTrans4210();
		}
		catch (GOTOException e){
		}
	}

protected void verifyReqTrans4210()
	{
		wsaaExitFlag.set("N");
		wsaaImmexitFlag.set("N");
		covtlnbIO.setChdrcoy(wsspcomn.company);
		covtlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		covtlnbIO.setLife(lifelnbIO.getLife());
		covtlnbIO.setCoverage(wsaaCoverageR);
		covtlnbIO.setRider(wsaaRiderR);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
//		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		while ( !(wsaaExit.isTrue())) {
			verifyTrans4220();
		}
		
		if (isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			wsspcomn.msgarea.set(" MANDATORY TRANSACTION - PLEASE COMPLETE.");
			wsaaImmexitFlag.set("Y");
			readProgramTables4600();
		}
		wsaaExitFlag.set("N");
		goTo(GotoLabel.exit4290);
	}

protected void verifyTrans4220()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(),wsspcomn.company)
		|| isNE(covtlnbIO.getChdrnum(),chdrlnbIO.getChdrnum())
		|| isNE(covtlnbIO.getLife(),lifelnbIO.getLife())
		|| isNE(covtlnbIO.getCoverage(),wsaaCoverageR)
		|| isNE(covtlnbIO.getRider(),wsaaRiderR)
		|| isNE(covtlnbIO.getSeqnbr(),ZERO)
		|| isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			wsaaExitFlag.set("Y");
		}
		if (isEQ(covtlnbIO.getStatuz(),varcom.oK)) {
			wsaaExitFlag.set("Y");
		}
		covtlnbIO.setFunction(varcom.nextr);
	}

protected void verifyNextSelect4300()
	{
		/*VERIFY*/
		wsaaSelectFlag.set("N");
		wsaaImmexitFlag.set("N");
		while ( !(wsaaSelection.isTrue()
		|| wsaaImmExit.isTrue())) {
			next4400();
		}
		
		/*EXIT*/
	}

protected void next4400()
	{
		try {
			nextRec4410();
			ifSubfileEndp4420();
		}
		catch (GOTOException e){
		}
	}

protected void nextRec4410()
	{
		processScreen("S5006", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void ifSubfileEndp4420()
	{
		if (isEQ(scrnparams.statuz,varcom.endp)) {
			reloadProgsWssp4430();
			goTo(GotoLabel.exit4490);
		}
		if (isNE(sv.ctable,SPACES)) {
			wsaaCoverFlag.set("N");
		}
		if (isEQ(sv.select,"X")) {
			if (isEQ(wsaaLifeorcov,"L")) {
				if (isNE(sv.ctable,SPACES)) {
					wsaaCoverFlag.set("Y");
					wsaaSelectFlag.set("Y");
				}
				else {
					if (wsaaCover.isTrue()) {
						wsaaSelectFlag.set("Y");
					}
					else {
						wsaaSelectFlag.set("N");
					}
				}
			}
			else {
				wsaaCoverFlag.set("Y");
				wsaaSelectFlag.set("Y");
			}
		}
		else {
			wsaaSelectFlag.set("N");
		}
		scrnparams.function.set(varcom.srdn);
		goTo(GotoLabel.exit4490);
	}

protected void reloadProgsWssp4430()
	{
		wsaaImmexitFlag.set("Y");
		compute(wsaaSub4, 0).set(add(1,wsspcomn.programPtr));
		wsaaSub5.set(1);
		for (int loopVar3 = 0; !(loopVar3 == 4); loopVar3 += 1){
			loop34440();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
		wsspcomn.programPtr.add(1);
		goTo(GotoLabel.exit4490);
	}

protected void loop34440()
	{
		wsspcomn.secProg[wsaaSub4.toInt()].set(wsaaSecProg[wsaaSub5.toInt()]);
		wsaaSub4.add(1);
		wsaaSub5.add(1);
	}

protected void coverRiderProgs4500()
	{
		try {
			initCovtlnb4510();
			coverOrRider4520();
		}
		catch (GOTOException e){
		}
	}

protected void initCovtlnb4510()
	{
		covtlnbIO.setDataArea(SPACES);
		covtlnbIO.setPayrseqno(1);
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setAnbccd(1, ZERO);
		covtlnbIO.setAnbccd(2, ZERO);
		covtlnbIO.setRiskCessAge(ZERO);
		covtlnbIO.setPremCessAge(ZERO);
		covtlnbIO.setBenCessAge(ZERO);
		covtlnbIO.setRiskCessTerm(ZERO);
		covtlnbIO.setPremCessTerm(ZERO);
		covtlnbIO.setBenCessTerm(ZERO);
		covtlnbIO.setSumins(ZERO);
		covtlnbIO.setPolinc(ZERO);
		covtlnbIO.setNumapp(ZERO);
		covtlnbIO.setInstprem(ZERO);
		covtlnbIO.setSingp(ZERO);
		covtlnbIO.setTransactionDate(varcom.vrcmDate);
		covtlnbIO.setTransactionTime(varcom.vrcmTime);
		covtlnbIO.setUser(varcom.vrcmUser);
		covtlnbIO.setRiskCessDate(varcom.vrcmMaxDate);
		covtlnbIO.setRiskCessDate(varcom.vrcmMaxDate);
		covtlnbIO.setPremCessDate(varcom.vrcmMaxDate);
		covtlnbIO.setBenCessDate(varcom.vrcmMaxDate);
		covtlnbIO.setReserveUnitsDate(varcom.vrcmMaxDate);
		covtlnbIO.setEffdate(varcom.vrcmMaxDate);
	}

protected void coverOrRider4520()
	{
		if (isNE(sv.ctable,SPACES)) {
			coverNextProg4530();
		}
		if (isNE(sv.rtable,SPACES)) {
			riderNextProg4540();
		}
		keepsCovtlnb4550();
		if(uwFlag) {
			saveUwrspfRecord();
			saveAutoTerms();
		}
		goTo(GotoLabel.exit4590);
	}

private void saveUwrspfRecord() {
	if(isEQ(sv.uworflag, "Y")) {
		Uwrspf uwrspf = new Uwrspf();
		uwrspf.setChdrcoy(chdrlnbIO.getChdrcoy().toString());
		uwrspf.setChdrnum(chdrlnbIO.getChdrnum().toString());
		uwrspf.setCrtable(wsaaCrtable.toString());
		uwrspf.setReason(sv.uworreason.toString());
		uwrspf.setTrancde(wsaaTrancode.toString());
		uwrspf.setTranno(chdrlnbIO.getTranno().toInt());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		uwrspf.setTrdt(datcon1rec.intDate.toInt());
		uwrspf.setTrtm(varcom.vrcmTime.toInt());
		uwrspf.setUser(varcom.vrcmUser.toInt());
		uwrspf.setTermid(varcom.vrcmTermid.toString());
		//uwrspfDAO.insertUwrsData(uwrspf);
		Map<String, Uwrspf> uwRecCache = (Map<String, Uwrspf>)ThreadLocalStore.get(UWREC_CACHE);
		if(uwRecCache == null) {
			uwRecCache = new HashMap<>();
			ThreadLocalStore.put(UWREC_CACHE, uwRecCache);
		}
		uwRecCache.put(wsaaCrtable.toString(), uwrspf);
	}
}
private void saveAutoTerms() {
	checkQuestionTerm();
	checkOccupationTerm();
}

	private void checkQuestionTerm() {
		List<Undqpf> undqList = undqpfDAO.searchUnqpf(lifelnbIO.getChdrcoy().toString().trim(),
				lifelnbIO.getChdrnum().toString().trim(), lifelnbIO.getLife().toString().trim(), "00");
		questions = new ArrayList<String>();
		for (Undqpf u : undqList) {
			if ("Y".equals(u.getAnswer().trim())) {
				questions.add(u.getQuestidf());
			}
		}
		for (String question : questions) {
			uwQuestionnaireRec = new UWQuestionnaireRec();
			uwQuestionnaireRec.setTransEffdate(datcon1rec.intDate.getbigdata());
			uwQuestionnaireRec.setContractType(chdrlnbIO.getCnttype().toString());
			UWQuestionnaireRec uwQuestionnaireRecReturn = uwQuestionnaireUtil.getUWMessage(uwQuestionnaireRec,
					wsspcomn.language.toString(), chdrlnbIO.getChdrcoy().toString(), question,
					wsaaCrtable.toString().trim(), "");
			if (!uwQuestionnaireRecReturn.getOutputUWDec().isEmpty()
					&& !uwQuestionnaireRecReturn.getOutputUWDec().contains("Accepted")
					&& !uwQuestionnaireRecReturn.getOutputUWDec().contains("0")) {
				saveLextTerm(uwQuestionnaireRecReturn);
			}
		}
	}
	private void saveLextTerm(UWQuestionnaireRec uwQuestionnaireRecReturn) {
		for (int i = 0; i < uwQuestionnaireRecReturn.getOutputUWDec().size(); i++) {
			LextTableDAM lextIO = initLextpf();
			lextIO.setInsprm(uwQuestionnaireRecReturn.getOutputSplTermRateAdj().get(i));
			lextIO.setAgerate(uwQuestionnaireRecReturn.getOutputSplTermAge().get(i));
			lextIO.setOppc(uwQuestionnaireRecReturn.getOutputSplTermLoadPer().get(i));
			lextIO.setZmortpct(uwQuestionnaireRecReturn.getOutputMortPerc().get(i));
			lextIO.setZnadjperc(uwQuestionnaireRecReturn.getOutputSplTermSAPer().get(i));
			lextIO.setOpcda(uwQuestionnaireRecReturn.getOutputSplTermCode().get(i));
			setT5657Sbstdl(lextIO);
			saveLextpf(lextIO);
			List<String> autoTermsList = (List)ThreadLocalStore.get(AUTO_TERMS_LIST);
			if(autoTermsList == null) {
				autoTermsList = new ArrayList<String>();
				ThreadLocalStore.put(AUTO_TERMS_LIST, autoTermsList);
			}
			autoTermsList.add(wsaaCrtable.toString().trim());
		}
	}
	private void checkOccupationTerm() {
		uwOccupationRec = new UWOccupationRec();
		uwOccupationRec.transEffdate.set(datcon1rec.intDate.getbigdata());
		uwOccupationRec.occupCode.set(cltsIO.getOccpcode());
		uwOccupationRec.indusType.set(cltsIO.getStatcode());
		uwOccupationRec = uwoccupationUtil.getUWOccupationRec(uwOccupationRec, chdrlnbIO.getChdrcoy().toString(),
				wsspcomn.fsuco.toString(), chdrlnbIO.getCnttype().toString(), wsaaCrtable.toString().trim());
		if (uwOccupationRec != null && isNE(uwOccupationRec.outputUWDec, SPACE)
				&& isNE(uwOccupationRec.outputUWDec, "Accepted")
				&& isNE(uwOccupationRec.outputUWDec, "0")) {
			LextTableDAM lextIO = initLextpf();
			lextIO.setInsprm(uwOccupationRec.outputSplTermRateAdj);
			lextIO.setAgerate(uwOccupationRec.outputSplTermAge);
			lextIO.setOppc(uwOccupationRec.outputSplTermLoadPer);
			lextIO.setZmortpct(uwOccupationRec.outputMortPerc);
			lextIO.setZnadjperc(uwOccupationRec.outputSplTermSAPer);
			lextIO.setOpcda(uwOccupationRec.outputSplTermCode);
			setT5657Sbstdl(lextIO);
			saveLextpf(lextIO);
			List<String> autoTermsList = (List)ThreadLocalStore.get(AUTO_TERMS_LIST);
			if(autoTermsList == null) {
				autoTermsList = new ArrayList<String>();
				ThreadLocalStore.put(AUTO_TERMS_LIST, autoTermsList);
			}
			autoTermsList.add(wsaaCrtable.toString().trim());
		}
	}

	private LextTableDAM initLextpf() {
		LextTableDAM lextIO = new LextTableDAM();

		lextIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		lextIO.setChdrnum(chdrlnbIO.getChdrnum());
		lextIO.setLife(covtlnbIO.getLife());
		lextIO.setCoverage(covtlnbIO.getCoverage());
		lextIO.setRider(covtlnbIO.getRider());

		lextIO.setSeqnbr(seqnbr++);
		lextIO.setValidflag("1");
		lextIO.setCurrfrom(chdrlnbIO.getOccdate());
		lextIO.setCurrto(varcom.vrcmMaxDate);
		lextIO.setReasind("1");
		//lextIO.setJlife(lifelnbIO.getJlife());

		lextIO.setTranno(chdrlnbIO.getTranno());
		lextIO.setExtCommDate(chdrlnbIO.getOccdate());
		lextIO.setExtCessDate(varcom.vrcmMaxDate);

		lextIO.setUwoverwrite("Y");
		lextIO.setTermid(varcom.vrcmTermid);
		lextIO.setTransactionDate(varcom.vrcmDate);
		lextIO.setTransactionTime(varcom.vrcmTime);
		lextIO.setUser(varcom.vrcmUser);

		return lextIO;
	}

private void setT5657Sbstdl(LextTableDAM lextIO) {
	itemIO.setItempfx("IT");
	itemIO.setItemcoy(covtlnbIO.getChdrcoy());
	itemIO.setItemtabl("T5657");
	
	StringBuilder wsaaT5657key = new StringBuilder(); 
	wsaaT5657key.append(wsaaCrtable.toString().trim());
	wsaaT5657key.append(lextIO.getOpcda().toString().trim());
	itemIO.setItemitem(wsaaT5657key.toString());
	itemIO.setFunction(varcom.readr);
	itemIO.setFormat(itemrec);
	SmartFileCode.execute(appVars, itemIO);
	if (isNE(itemIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(itemIO.getParams());
		fatalError600();
	}
	T5657rec t5657rec = new T5657rec();
	t5657rec.t5657Rec.set(itemIO.getGenarea());
	lextIO.setSbstdl(t5657rec.sbstdl);
}
private void saveLextpf(LextTableDAM lextIO) {
	lextIO.setFormat("LEXTREC");
	lextIO.setFunction(varcom.writr);
	SmartFileCode.execute(appVars, lextIO);
	if (isNE(lextIO.getStatuz(),varcom.oK)) {
		syserrrec.params.set(lextIO.getParams());
		fatalError600();
	}
}

protected void coverNextProg4530()
	{
		if (wsaaCover.isTrue()) {
			wsaaCrtable.set(sv.ctable);
			wsaaTrancode.set(wsaaBatckey.batcBatctrcde);
		}
		wsaaNextCovnoR.add(1);
		wsaaCoverage.set(wsaaNextCovnoR);
		covtlnbIO.setCoverage(wsaaNextCovnoR);
		wsaaRider.set(ZERO);
		covtlnbIO.setRider(ZERO);
	}

protected void riderNextProg4540()
	{
		if (wsaaCover.isTrue()) {
			wsaaCrtable.set(sv.rtable);
			wsaaTrancode.set(wsaaBatckey.batcBatctrcde);
		}
		if (isEQ(wsaaLifeorcov,"L")) {
			covtlnbIO.setCoverage(wsaaCoverage);
			wsaaRider.add(1);
			covtlnbIO.setRider(wsaaRider);
		}
		else {
			covtlnbIO.setCoverage(wsaaCoverage);
			wsaaNextRidnoR.add(1);
			covtlnbIO.setRider(wsaaNextRidno);
		}
	}

protected void keepsCovtlnb4550()
	{
		covtlnbIO.setCrtable(wsaaCrtable);
		covtlnbIO.setChdrcoy(wsspcomn.company);
		covtlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		covtlnbIO.setCntcurr(chdrlnbIO.getCntcurr());
		covtlnbIO.setLife(lifelnbIO.getLife());
		covtlnbIO.setFunction(varcom.keeps);
		covtlnbIO.setFormat(covtlnbrec);
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
	}

protected void readProgramTables4600()
	{
		try {
			readProgramTable4610();
			loadProgsToWssp4620();
		}
		catch (GOTOException e){
		}
	}

protected void readProgramTable4610()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5671);
		itemIO.setItemitem(wsaaConcatName);
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
	}

protected void loadProgsToWssp4620()
	{
		wsaaSub4.set(1);
		compute(wsaaSub5, 0).set(add(1,wsspcomn.programPtr));
		for (int loopVar4 = 0; !(loopVar4 == 4); loopVar4 += 1){
			loop24630();
		}
		wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		wsspcomn.programPtr.add(1);
		goTo(GotoLabel.exit4690);
	}

protected void loop24630()
	{
		wsspcomn.secProg[wsaaSub5.toInt()].set(t5671rec.pgm[wsaaSub4.toInt()]);
		wsaaSub4.add(1);
		wsaaSub5.add(1);
	}
}