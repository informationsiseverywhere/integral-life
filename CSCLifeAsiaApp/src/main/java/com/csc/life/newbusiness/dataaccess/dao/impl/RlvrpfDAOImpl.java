package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.RlvrpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Rlvrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class RlvrpfDAOImpl extends BaseDAOImpl<Rlvrpf> implements RlvrpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(RlvrpfDAOImpl.class);
	@Override
	public void insertRecord(Rlvrpf rlvrpf) {
		if (rlvrpf != null) {
		      
			StringBuilder sql_insert = new StringBuilder();
			sql_insert.append("INSERT INTO RLVRPF");
			sql_insert.append("(CHDRNUM,CHDRCOY,MEMACCNUM,FNDUSINUM,SRVSTRTDT,TAXFREEAMT,KIWISVRCOM,TAXEDAMT,UNTAXEDAMT,PRSRVDAMT,KIWIAMT,UNRESTRICT,RSTRICT,TOTTRFAMT,MONEYIND,USRPRF,JOBNM,DATIME,LIFE,COVERAGE,RIDER,VALIDFLAG)");
			sql_insert.append("VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement psInsert = getPrepareStatement(sql_insert.toString());
			try
			{
					psInsert.setString(1, rlvrpf.getChdrnum());
					psInsert.setString(2, rlvrpf.getChdrcoy());
					psInsert.setString(3, rlvrpf.getMemaccnum());
					psInsert.setString(4, rlvrpf.getFndusinum());
					psInsert.setInt(5, rlvrpf.getSrvstrtdt());
					psInsert.setBigDecimal(6, rlvrpf.getTaxfreeamt());
					psInsert.setBigDecimal(7, rlvrpf.getKiwisvrcom());
					psInsert.setBigDecimal(8, rlvrpf.getTaxedamt());
					psInsert.setBigDecimal(9, rlvrpf.getUntaxedamt());
					psInsert.setBigDecimal(10, rlvrpf.getPrsrvdamt());
					psInsert.setBigDecimal(11, rlvrpf.getKiwiamt());
					psInsert.setBigDecimal(12, rlvrpf.getUnrestrict());
					psInsert.setBigDecimal(13, rlvrpf.getRstrict());
					psInsert.setBigDecimal(14, rlvrpf.getTottrfamt());
					psInsert.setString(15, rlvrpf.getMoneyind());
					psInsert.setString(16, this.getUsrprf());
					psInsert.setString(17, this.getJobnm());
					psInsert.setTimestamp(18, new Timestamp(System.currentTimeMillis()));
					psInsert.setString(19, rlvrpf.getLife());//ILIFE-8123
					psInsert.setString(20, rlvrpf.getCoverage());//ILIFE-8123
					psInsert.setString(21, rlvrpf.getRider());//ILIFE-8123
					psInsert.setString(22, "1");//ILIFE-8123
					psInsert.addBatch();
					psInsert.executeBatch();	
			}
			catch (SQLException e) {
                LOGGER.error("insertRecord()", e);//IJTI-1485
                throw new SQLRuntimeException(e);
            } finally {
                close(psInsert, null);
            }
		}
		

		
	}

	@Override
	public Rlvrpf readRecord(String chdrcoy,String chdrnum) {
		Rlvrpf rlvrpfRec = null;
		    String sql_select  = "SELECT MEMACCNUM,FNDUSINUM,SRVSTRTDT,TAXFREEAMT,KIWISVRCOM,TAXEDAMT,UNTAXEDAMT,PRSRVDAMT,KIWIAMT,UNRESTRICT,RSTRICT,TOTTRFAMT,MONEYIND"
		    		+ " FROM RLVRPF WHERE CHDRCOY = ? AND CHDRNUM = ?  AND VALIDFLAG = ?";
		    
		    ResultSet rs = null;
		    PreparedStatement psSelect=null;
		    try {

		    	psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
		    	psSelect.setString(1, chdrcoy);
		    	psSelect.setString(2, chdrnum);
		    	psSelect.setString(3, "1");
		        rs = psSelect.executeQuery();
		           while (rs.next()) {
		                
		        	   		 rlvrpfRec= new Rlvrpf();
		        	   		 rlvrpfRec.setChdrcoy(chdrcoy);
		        	   		 rlvrpfRec.setChdrnum(chdrnum);
		        	   		 rlvrpfRec.setMemaccnum(rs.getString(1));
		        	   		 rlvrpfRec.setFndusinum(rs.getString(2));
		        	   		 rlvrpfRec.setSrvstrtdt(rs.getInt(3));
		                	 rlvrpfRec.setTaxfreeamt(rs.getBigDecimal(4));
		                	 rlvrpfRec.setKiwisvrcom(rs.getBigDecimal(5));
		                	 rlvrpfRec.setTaxedamt(rs.getBigDecimal(6));
		                	 rlvrpfRec.setUntaxedamt(rs.getBigDecimal(7));
		                	 rlvrpfRec.setPrsrvdamt(rs.getBigDecimal(8));
		                	 rlvrpfRec.setKiwiamt(rs.getBigDecimal(9));
		                	 rlvrpfRec.setUnrestrict(rs.getBigDecimal(10));
		                	 rlvrpfRec.setRstrict(rs.getBigDecimal(11));
		                	 rlvrpfRec.setTottrfamt(rs.getBigDecimal(12));
		                	 rlvrpfRec.setMoneyind(rs.getString(13));
		                	 
		                  }
		           
		    } catch (SQLException e) {
		           LOGGER.error("readRecord()", e);//IJTI-1485
		           throw new SQLRuntimeException(e);
		    } finally {
		           close(psSelect, rs);
		    }

		    return rlvrpfRec;
	}

	@Override
	public boolean updateRecord(Rlvrpf rlvrpf) {
		StringBuilder sql_update = new StringBuilder("");
		sql_update.append("UPDATE RLVRPF  ");
		sql_update.append("SET ");		 
		sql_update.append("MEMACCNUM=?,FNDUSINUM=?,SRVSTRTDT=?,TAXFREEAMT=?,KIWISVRCOM=?,TAXEDAMT=?,UNTAXEDAMT=?,PRSRVDAMT=?,KIWIAMT=?,"
				+ "UNRESTRICT=?,RSTRICT=?,TOTTRFAMT=?,MONEYIND=?,USRPRF=?,JOBNM=?,DATIME=? ");
		sql_update.append("WHERE CHDRNUM='"+rlvrpf.getChdrnum()+"'");
		sql_update.append("AND CHDRCOY='"+rlvrpf.getChdrcoy()+"'");

		
		PreparedStatement psUpdate = null;
		boolean isUpdated = false;
		try{
			psUpdate = getPrepareStatement(sql_update.toString());
			
			psUpdate.setString(1, rlvrpf.getMemaccnum());
			psUpdate.setString(2, rlvrpf.getFndusinum());
			psUpdate.setInt(3, rlvrpf.getSrvstrtdt());
			psUpdate.setBigDecimal(4, rlvrpf.getTaxfreeamt());
			psUpdate.setBigDecimal(5, rlvrpf.getKiwisvrcom());
			psUpdate.setBigDecimal(6, rlvrpf.getTaxedamt());
			psUpdate.setBigDecimal(7, rlvrpf.getUntaxedamt());
			psUpdate.setBigDecimal(8, rlvrpf.getPrsrvdamt());
			psUpdate.setBigDecimal(9, rlvrpf.getKiwiamt());
			psUpdate.setBigDecimal(10, rlvrpf.getUnrestrict());
			psUpdate.setBigDecimal(11, rlvrpf.getRstrict());
			psUpdate.setBigDecimal(12, rlvrpf.getTottrfamt());
			psUpdate.setString(13, rlvrpf.getMoneyind());
			psUpdate.setString(14, this.getUsrprf());
			psUpdate.setString(15, this.getJobnm());
			psUpdate.setTimestamp(16, new Timestamp(System.currentTimeMillis()));
			psUpdate.addBatch();	
			psUpdate.executeBatch();
			isUpdated = true; 
		}
		
		catch ( SQLException e) {
			LOGGER.error("updateRecord()",e);//IJTI-1485
			
		} finally {
			close(psUpdate, null);			
		}		
		return isUpdated;
	}
	
	//ILIFE-4003 STARTs smalchi2
	public void deleteRecord(Rlvrpf rlvrpf) {

		StringBuilder sqlDelete = new StringBuilder();

		sqlDelete.append("DELETE FROM RLVRPF WHERE CHDRNUM = ? AND CHDRCOY = ? ");

		PreparedStatement psRlvrpfDelete = getPrepareStatement(sqlDelete.toString());
		

		try {
			psRlvrpfDelete.setString(1, rlvrpf.getChdrnum());
			psRlvrpfDelete.setString(2, rlvrpf.getChdrcoy());
			
			int affectedCount = executeUpdate(psRlvrpfDelete);

			LOGGER.debug("deleteRlvrpf { {} } rows deleted.",affectedCount);//IJTI-1485

		} catch (SQLException e) {
			
			LOGGER.error("Exception occured in deleteRecord ",e);
		} finally {
			close(psRlvrpfDelete, null);
		}
	}
	//ILIFE-8123 -starts
	@Override
	public Rlvrpf checkRecord(String chdrcoy,String chdrnum,String life,String coverage) {
		Rlvrpf rlvrpfRec = null;
	    String sql_select  = "SELECT MEMACCNUM,FNDUSINUM,SRVSTRTDT,TAXFREEAMT,KIWISVRCOM,TAXEDAMT,UNTAXEDAMT,PRSRVDAMT,KIWIAMT,UNRESTRICT,RSTRICT,TOTTRFAMT,MONEYIND"
	    		+ " FROM RLVRPF  WHERE CHDRCOY = ? AND CHDRNUM = ?  AND LIFE = ? AND COVERAGE = ? AND VALIDFLAG = ?";
	    
	    ResultSet rs = null;
	    PreparedStatement psSelect=null;
	    try {

	    	psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
	    	psSelect.setString(1, chdrcoy);
	    	psSelect.setString(2, chdrnum);
	    	psSelect.setString(3, life);
	    	psSelect.setString(4, coverage);
	    	psSelect.setString(5, "1");
	        rs = psSelect.executeQuery();
	           while (rs.next()) {
	                
	        	   		 rlvrpfRec= new Rlvrpf();
	        	   		 rlvrpfRec.setChdrcoy(chdrcoy);
	        	   		 rlvrpfRec.setChdrnum(chdrnum);
	        	   		 rlvrpfRec.setLife(life);
	        	   		 rlvrpfRec.setCoverage(coverage);
	        	   		 rlvrpfRec.setValidFlag("1");
	        	   		 rlvrpfRec.setMemaccnum(rs.getString(1));
	        	   		 rlvrpfRec.setFndusinum(rs.getString(2));
	        	   		 rlvrpfRec.setSrvstrtdt(rs.getInt(3));
	                	 rlvrpfRec.setTaxfreeamt(rs.getBigDecimal(4));
	                	 rlvrpfRec.setKiwisvrcom(rs.getBigDecimal(5));
	                	 rlvrpfRec.setTaxedamt(rs.getBigDecimal(6));
	                	 rlvrpfRec.setUntaxedamt(rs.getBigDecimal(7));
	                	 rlvrpfRec.setPrsrvdamt(rs.getBigDecimal(8));
	                	 rlvrpfRec.setKiwiamt(rs.getBigDecimal(9));
	                	 rlvrpfRec.setUnrestrict(rs.getBigDecimal(10));
	                	 rlvrpfRec.setRstrict(rs.getBigDecimal(11));
	                	 rlvrpfRec.setTottrfamt(rs.getBigDecimal(12));
	                	 rlvrpfRec.setMoneyind(rs.getString(13));
	                	 
	                  }
	           
	    } catch (SQLException e) {
	           LOGGER.error("readRecord()", e);//IJTI-1485
	           throw new SQLRuntimeException(e);
	    } finally {
	           close(psSelect, rs);
	    }

	    return rlvrpfRec;
		
	}
	
	@Override
	public boolean updateRlvrpf(Rlvrpf rlvrpf) {
		StringBuilder sql_update = new StringBuilder("");
		sql_update.append("UPDATE RLVRPF  ");
		sql_update.append("SET ");		 
		sql_update.append("MEMACCNUM=?,FNDUSINUM=?,SRVSTRTDT=?,TAXFREEAMT=?,KIWISVRCOM=?,TAXEDAMT=?,UNTAXEDAMT=?,PRSRVDAMT=?,KIWIAMT=?,"
				+ "UNRESTRICT=?,RSTRICT=?,TOTTRFAMT=?,MONEYIND=?,USRPRF=?,JOBNM=?,DATIME=? ");
		sql_update.append("WHERE CHDRNUM='"+rlvrpf.getChdrnum()+"'");
		sql_update.append("AND CHDRCOY='"+rlvrpf.getChdrcoy()+"'");
		sql_update.append("AND LIFE='"+rlvrpf.getLife()+"'");
		sql_update.append("AND COVERAGE='"+rlvrpf.getCoverage()+"'");
		sql_update.append("AND VALIDFLAG='"+"1"+"'");

		
		PreparedStatement psUpdate = null;
		boolean isUpdated = false;
		try{
			psUpdate = getPrepareStatement(sql_update.toString());
			
			psUpdate.setString(1, rlvrpf.getMemaccnum());
			psUpdate.setString(2, rlvrpf.getFndusinum());
			psUpdate.setInt(3, rlvrpf.getSrvstrtdt());
			psUpdate.setBigDecimal(4, rlvrpf.getTaxfreeamt());
			psUpdate.setBigDecimal(5, rlvrpf.getKiwisvrcom());
			psUpdate.setBigDecimal(6, rlvrpf.getTaxedamt());
			psUpdate.setBigDecimal(7, rlvrpf.getUntaxedamt());
			psUpdate.setBigDecimal(8, rlvrpf.getPrsrvdamt());
			psUpdate.setBigDecimal(9, rlvrpf.getKiwiamt());
			psUpdate.setBigDecimal(10, rlvrpf.getUnrestrict());
			psUpdate.setBigDecimal(11, rlvrpf.getRstrict());
			psUpdate.setBigDecimal(12, rlvrpf.getTottrfamt());
			psUpdate.setString(13, rlvrpf.getMoneyind());
			psUpdate.setString(14, this.getUsrprf());
			psUpdate.setString(15, this.getJobnm());
			psUpdate.setTimestamp(16, new Timestamp(System.currentTimeMillis()));
			psUpdate.addBatch();	
			psUpdate.executeBatch();
			isUpdated = true; 
		}
		
		catch ( SQLException e) {
			LOGGER.error("updateRecord()",e);//IJTI-1485
			
		} finally {
			close(psUpdate, null);			
		}		
		return isUpdated;
	}
	
	@Override
	public boolean updateRollovRecord(Rlvrpf rlvrpf) {
		StringBuilder sql_update = new StringBuilder("");
		sql_update.append("UPDATE RLVRPF  ");
		sql_update.append("SET ");		 
		sql_update.append("VALIDFLAG=?,USRPRF=?,JOBNM=?,DATIME=? ");
		sql_update.append("WHERE CHDRNUM='"+rlvrpf.getChdrnum()+"'");
		sql_update.append("AND CHDRCOY='"+rlvrpf.getChdrcoy()+"'");
		sql_update.append("AND LIFE='"+rlvrpf.getLife()+"'");
		sql_update.append("AND COVERAGE='"+rlvrpf.getCoverage()+"'");
		
		PreparedStatement psUpdate = null;
		boolean isUpdated = false;
		try{
			psUpdate = getPrepareStatement(sql_update.toString());
			
			psUpdate.setString(1, rlvrpf.getValidFlag());
			psUpdate.setString(2, this.getUsrprf());
			psUpdate.setString(3, this.getJobnm());
			psUpdate.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			psUpdate.addBatch();	
			psUpdate.executeBatch();
			isUpdated = true; 
		}
		
		catch ( SQLException e) {
			LOGGER.error("updateRecord()",e);//IJTI-1485
			
		} finally {
			close(psUpdate, null);			
		}		
		return isUpdated;
	}
//ILIFE-8123 -ends
}
