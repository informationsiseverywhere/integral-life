/*
 * File: P5073.java
 * Date: 30 August 2009 0:02:02
 * Author: Quipoz Limited
 * 
 * Class transformed from P5073.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Sdasanc;
import com.csc.fsu.general.recordstructures.Alocnorec;
import com.csc.fsu.general.recordstructures.Sdasancrec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.screens.S5073ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.procedures.Bcbprog;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Bcbprogrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                  New Contract Issue Submenu
*                  ==========================
*
*
* Initialisation
* --------------
*
*  Retrieve todays date (DATCON1) and store for later use.
*
*
* Validation
* ----------
*
*  Key 1 - Contract proposal number (CHDRLNB)
*
*       Y = mandatory, must exist on file.
*            - CHDRLNB  -  Life  new  business  contract  header
*                 logical view  (LIFO,  keyed  on  company   and
*                 contract number, select service unit = LP).
*            - must be correct status for transaction (T5679).
*            - for maintenece  transactions,   servicing  branch
*              must be sign-on branch
*
*       N = optional, if entered must not exist on file, must be
*            within correct number range (call ALOCNO to check).
*
*       Blank = irrelevant.
*
*  Key 3 - Contract number.
*
*       Y = mandatory, must exist on file (CHDRLNB).
*
*       N = irrelevant.
*
*       Blank = irrelevant.
*
*
* Updating
* --------
*
*  Set up WSSP-FLAG:
*       - "I" for actions E and F, otherwise "M".
*  For enqury transactions ("I"), nothing else is required.
*
*  For maintenance transactions  (WSSP-FLAG  set to  'M' above),
*  soft lock the contract header (call SFTLOCK). If the contract
*  is already locked, display an error message.
*
******************Enhancements for Life Asia 1.0****************
*
* A new option is now available to postpone a proposal. This is
* in addition to the options of decline and withdrawing a
* proposal. The processing of this option is all done via tables.
*
*****************************************************************
* </pre>
*/
public class P5073 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5073");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private static final String t5679 = "T5679";
		/* FORMATS */
	private static final String chdrlnbrec = "CHDRLNBREC";
	private static final String chdrenqrec = "CHDRENQREC";
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(0).setUnsigned();
	private int wsaaSub = 0;//ILIFE-4492
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private Alocnorec alocnorec = new Alocnorec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T5679rec t5679rec = new T5679rec();
	private Batckey wsaaBatchkey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Bcbprogrec bcbprogrec = new Bcbprogrec();
	private Batcdorrec batcdorrec = new Batcdorrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Sdasancrec sdasancrec = new Sdasancrec();
	private Wssplife wssplife = new Wssplife();
	private S5073ScreenVars sv = ScreenProgram.getScreenVars( S5073ScreenVars.class);
	
	//ILIFE-4492
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> t5679List = new ArrayList();
	
	private static final String e070 = "E070";
	private static final String e073 = "E073";
	private static final String e455 = "E455";
	private static final String e544 = "E544";
	private static final String e767 = "E767";
	private static final String f321 = "F321";
	private static final String f910 = "F910";
	private static final String f918 = "F918";

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2190, 
		validateKey22220, 
		exit2290, 
		search2510, 
		exit12090, 
		keeps3070, 
		batching3080, 
		exit3090
	}

	public P5073() {
		super();
		screenVars = sv;
		new ScreenModel("S5073", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			//TMLII-268
			/* Initialization*/
			scrnparams.version.set(getWsaaVersion());
			wsspcomn.version.set(getWsaaVersion());
			scrnparams.errorline.set(varcom.vrcmMsg);
			// need to skip certain areas of validation for 2000 call
			if(isEQ(wsspcomn.sectionno, "2000")){
				// Initialization, as is done when we call processBoMainline() method - start
				appVars = getAppVars();
				// save appvars indicators before making Pnnnn call
				appVars.saveInds(1, wsaaSavedIndicAreaP);
				syserrrec.subrname.set(getWsaaProg());
				syserrrec.statuz.set(Varcom.oK);
				varcom.vrcmTranid.set(wsspcomn.tranid);
				opstatsrec.opsrecTim2000.set(COBOLFunctions.getCobolTime());
				wsspcomn.edterror.set(Varcom.oK);
				// Initialization, as is done when we call processBoMainline() method - end
				/*VALIDATE*/
				validateAction2100();
				/* Need to read CHDRLNB logical file as it is being stored in 3000 section using KEEPS */
				chdrlnbIO.setChdrcoy(wsspcomn.company);
				chdrlnbIO.setChdrnum(sv.chdrsel);
				chdrlnbIO.setFunction(varcom.readr);
				if (isNE(sv.chdrsel,SPACES)) {
					SmartFileCode.execute(appVars, chdrlnbIO);
				}
				else {
					chdrlnbIO.setStatuz(varcom.mrnf);
				}
				if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
				&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
					syserrrec.params.set(chdrlnbIO.getParams());
					fatalError600();
				}
				if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)
				&& isEQ(subprogrec.key1,"Y")) {
					sv.chdrselErr.set(e544);
					wsspcomn.edterror.set("Y");
				}
				/*CHECK-FOR-ERRORS*/
				if (isNE(sv.errorIndicators,SPACES)) {
					wsspcomn.edterror.set("Y");
				}
				/*EXIT*/
				// restore appvars indicators after Pnnnn call
				appVars.restoreInds(1, wsaaSavedIndicAreaP);
				
			} else {
				processBoMainline(sv, sv.dataArea, parmArray);
			}
			//TMLII-268
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		sv.action.set(wsspcomn.sbmaction);
		wsaaBatchkey.batcKey.set(wsspcomn.batchkey);
		if (isNE(wsaaBatchkey.batcBatcactmn,wsspcomn.acctmonth)
		|| isNE(wsaaBatchkey.batcBatcactyr,wsspcomn.acctyear)) {
			scrnparams.errorCode.set(e070);
		}
		if (isEQ(wsaaToday,0)) {
			datcon1rec.function.set(varcom.tday);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			wsaaToday.set(datcon1rec.intDate);
		}
		
		boolean ntuflg = FeaConfg.isFeatureExist(wsspcomn.company.toString(), "NBPRP125", appVars, "IT");
		if(ntuflg) {
			sv.ntuFlg.set("Y");
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
		}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		validateAction2100();
		if (isEQ(sv.actionErr,SPACES)) {
			if (isNE(scrnparams.statuz,"BACH")) {
				validateKeys2200();
			}
			else {
				verifyBatchControl2900();
			}
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void validateAction2100()
	{
		try {
			checkAgainstTable2110();
			checkSanctions2120();
		}
		catch (GOTOException e){
		}
	}

protected void checkAgainstTable2110()
	{
		subprogrec.action.set(scrnparams.action);
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(subprogrec.statuz);
			goTo(GotoLabel.exit2190);
		}
	}

protected void checkSanctions2120()
	{
		sanctnrec.function.set("SUBM");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			sv.actionErr.set(sanctnrec.statuz);
		}
	}

protected void validateKeys2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					validateKey12210();
				}
				case validateKey22220: {
					validateKey22220();
				}
				case exit2290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validateKey12210()
	{
		if (isEQ(subprogrec.key1,SPACES)) {
			goTo(GotoLabel.validateKey22220);
		}
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(sv.chdrsel);
		chdrlnbIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel,SPACES)) {
			SmartFileCode.execute(appVars, chdrlnbIO);
		}
		else {
			chdrlnbIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)
		&& isEQ(subprogrec.key1,"Y")) {
			sv.chdrselErr.set(e544);
		}
		if (isEQ(chdrlnbIO.getStatuz(),varcom.oK)
		&& isEQ(subprogrec.key1,"N")) {
			sv.chdrselErr.set(f918);
		}
		if (isEQ(sv.chdrselErr, SPACES)) {
			initialize(sdasancrec.sancRec);
			sdasancrec.function.set("VENTY");
			sdasancrec.userid.set(wsspcomn.userid);
			sdasancrec.entypfx.set(fsupfxcpy.chdr);
			sdasancrec.entycoy.set(wsspcomn.company);
			sdasancrec.entynum.set(sv.chdrsel);
			callProgram(Sdasanc.class, sdasancrec.sancRec);
			if (isNE(sdasancrec.statuz, varcom.oK)) {
				sv.chdrselErr.set(sdasancrec.statuz);
				goTo(GotoLabel.exit2290);
			}
		}
		/*    For transactions on existing proposals,*/
		/*       check the contract selected is of the correct status*/
		/*       and from correct branck.*/
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)) {
			checkStatus2400();
		}
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.chdrselErr,SPACES)
		&& isNE(wsspcomn.branch,chdrlnbIO.getCntbranch())) {
			sv.chdrselErr.set(e455);
		}
	}

protected void validateKey22220()
	{
		if (isEQ(subprogrec.key2,SPACES)
		|| isEQ(subprogrec.key2,"N")) {
			return ;
		}
		chdrlnbIO.setChdrcoy(wsspcomn.company);
		chdrlnbIO.setChdrnum(sv.chdrsel);
		chdrlnbIO.setFunction(varcom.readr);
		if (isNE(sv.chdrsel,SPACES)) {
			SmartFileCode.execute(appVars, chdrlnbIO);
		}
		else {
			chdrlnbIO.setStatuz(varcom.mrnf);
		}
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getStatuz(),varcom.mrnf)) {
			sv.chdrselErr.set(e544);
		}
	}

protected void checkStatus2400()
	{
			readStatusTable2410();
		}

protected void readStatusTable2410()
	{

		///
		t5679List = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), t5679, subprogrec.transcd.toString());
		
		if(t5679List == null || t5679List.size() == 0){
			sv.chdrselErr.set(f321);
			return ;
		}
		
		t5679rec.t5679Rec.set(StringUtil.rawToString(t5679List.get(0).getGenarea()));
		wsaaSub = 0;
		lookForStat2500();
	}

protected void lookForStat2500()
	{
	//ILIFE-4492
		wsaaSub++;
		
		for(;wsaaSub<=12;wsaaSub++){
			if (isEQ(chdrlnbIO.getStatcode(),t5679rec.cnRiskStat[wsaaSub])){
				break;
			}
		}
		
		if(wsaaSub > 12){
			sv.chdrselErr.set(e767);
			wsspcomn.edterror.set("Y");
		}
		
	}



protected void verifyBatchControl2900()
	{
		try {
			validateRequest2910();
			retrieveBatchProgs2920();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void validateRequest2910()
	{
		if (isNE(subprogrec.bchrqd,"Y")) {
			sv.actionErr.set(e073);
			goTo(GotoLabel.exit12090);
		}
	}

protected void retrieveBatchProgs2920()
	{
		bcbprogrec.transcd.set(subprogrec.transcd);
		bcbprogrec.company.set(wsspcomn.company);
		callProgram(Bcbprog.class, bcbprogrec.bcbprogRec);
		if (isNE(bcbprogrec.statuz,varcom.oK)) {
			sv.actionErr.set(bcbprogrec.statuz);
			return ;
		}
		wsspcomn.next1prog.set(bcbprogrec.nxtprog1);
		wsspcomn.next2prog.set(bcbprogrec.nxtprog2);
		wsspcomn.next3prog.set(bcbprogrec.nxtprog3);
		wsspcomn.next4prog.set(bcbprogrec.nxtprog4);
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					updateWssp3010();
				}
				case keeps3070: {
					keeps3070();
				}
				case batching3080: {
					batching3080();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateWssp3010()
	{
		wsspcomn.sbmaction.set(scrnparams.action);
		wsaaBatchkey.set(wsspcomn.batchkey);
		wsaaBatchkey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatchkey);
		wsspcomn.submenu.set(wsaaProg);
		if (isEQ(scrnparams.statuz,"BACH")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(sv.action,"A")) {
			wsspcomn.confirmationKey.set(sv.chdrsel);	//IBPLIFE-1718
		}
		wsspcomn.secProg[1].set(subprogrec.nxt1prog);
		wsspcomn.secProg[2].set(subprogrec.nxt2prog);
		wsspcomn.secProg[3].set(subprogrec.nxt3prog);
		wsspcomn.secProg[4].set(subprogrec.nxt4prog);
		if (isEQ(scrnparams.action,"F")) {
			wsspcomn.flag.set("I");
			chdrenqIO.setChdrcoy(wsspcomn.company);
			chdrenqIO.setChdrnum(sv.chdrsel);
			chdrenqIO.setFunction(varcom.reads);
			chdrenqIO.setFormat(chdrenqrec);
			SmartFileCode.execute(appVars, chdrenqIO);
			goTo(GotoLabel.keeps3070);
		}
		else {
			wsspcomn.flag.set("M");
		}
		if (isEQ(sv.action,"E")) {
			wsspcomn.flag.set("E");//IBPLIFE-568 remove unused codes
		}
		if (isEQ(sv.action,"G")) {
			wsspcomn.flag.set("G");
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			goTo(GotoLabel.batching3080);
		}
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		if (isNE(sv.chdrsel,SPACES)) {
			sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		}
		else {
			sftlockrec.entity.set(alocnorec.alocNo);
		}
		sftlockrec.transaction.set(subprogrec.transcd);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
		&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrselErr.set(f910);
		}
		if (isEQ(subprogrec.key1,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			setPrecision(chdrlnbIO.getTranno(), 0);
			chdrlnbIO.setTranno(add(chdrlnbIO.getTranno(),1));
		}
	}

protected void keeps3070()
	{
		chdrlnbIO.setFunction("KEEPS");
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
	}

protected void batching3080()
	{
		if (isEQ(subprogrec.bchrqd,"Y")
		&& isEQ(sv.errorIndicators,SPACES)) {
			updateBatchControl3100();
		}
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
			rollback();
		}
	}

protected void updateBatchControl3100()
	{
		/*AUTOMATIC-BATCHING*/
		batcdorrec.function.set("AUTO");
		batcdorrec.tranid.set(wsspcomn.tranid);
		batcdorrec.batchkey.set(wsspcomn.batchkey);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			sv.actionErr.set(batcdorrec.statuz);
		}
		wsspcomn.batchkey.set(batcdorrec.batchkey);
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isNE(scrnparams.statuz,"BACH")) {
			wsspcomn.programPtr.set(1);
		}
		else {
			wsspcomn.programPtr.set(0);
			wsspcomn.nextprog.set(wsspcomn.next1prog);
		}
		/*EXIT*/
	}

}
