package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:01:23
 * Description:
 * Copybook name: CHDRTRXKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Chdrtrxkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData chdrtrxFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData chdrtrxKey = new FixedLengthStringData(64).isAPartOf(chdrtrxFileKey, 0, REDEFINE);
  	public FixedLengthStringData chdrtrxChdrcoy = new FixedLengthStringData(1).isAPartOf(chdrtrxKey, 0);
  	public FixedLengthStringData chdrtrxChdrnum = new FixedLengthStringData(8).isAPartOf(chdrtrxKey, 1);
  	public PackedDecimalData chdrtrxTranno = new PackedDecimalData(5, 0).isAPartOf(chdrtrxKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(chdrtrxKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(chdrtrxFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrtrxFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}