package com.csc.life.newbusiness.dataaccess.dao.impl;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.csc.life.newbusiness.dataaccess.dao.BnkoutpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnkoutpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class BnkoutpfDAOImpl extends BaseDAOImpl<Bnkoutpf> implements BnkoutpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(BnkoutpfDAOImpl.class);

	@Override
	public Bnkoutpf getBankRecord(String chdrnum) {
		Bnkoutpf bnkoutpf = null;
		String sql_select = "SELECT BNKOUT,BNKOUTNAME,BNKSM,BNKSMNAME,BNKTEL,BNKTELNAME FROM BNKOUTPF WHERE chdrnum= ? ";

		ResultSet rs = null;
		PreparedStatement psSelect = null;
		try {

			psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
			psSelect.setString(1, chdrnum);

			rs = psSelect.executeQuery();
			while (rs.next()) {
				bnkoutpf = new Bnkoutpf();
				bnkoutpf.setBnkout(rs.getString(1));
				bnkoutpf.setBnkoutname(rs.getString(2));
				bnkoutpf.setBnksm(rs.getString(3));
				bnkoutpf.setBnksmname(rs.getString(4));
				bnkoutpf.setBnktel(rs.getString(5));
				bnkoutpf.setBnktelname(rs.getString(6));

			}

		} catch (SQLException e) {
			LOGGER.error("getBankRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(psSelect, rs);
		}

		return bnkoutpf;
	}

	@Override
	public void insertBnkoutpf(Bnkoutpf bnkoutpf) {

		StringBuilder sqlInsert = new StringBuilder();

		sqlInsert.append("insert into BNKOUTPF(CHDRNUM,AGNTNUM,BNKOUT,BNKOUTNAME,BNKSM,BNKSMNAME,BNKTEL,BNKTELNAME,USRPRF,JOBNM,DATIME)");

		sqlInsert.append("values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");

		PreparedStatement psAcaxpfInsert = getPrepareStatement(sqlInsert.toString());
		int index = 1;

		try {

			psAcaxpfInsert.setString(index++, bnkoutpf.getChdrnum());
			psAcaxpfInsert.setString(index++, bnkoutpf.getAgntnum());
			psAcaxpfInsert.setString(index++, bnkoutpf.getBnkout());
			psAcaxpfInsert.setString(index++, bnkoutpf.getBnkoutname());
			psAcaxpfInsert.setString(index++, bnkoutpf.getBnksm());
			psAcaxpfInsert.setString(index++, bnkoutpf.getBnksmname());
			psAcaxpfInsert.setString(index++, bnkoutpf.getBnktel());
			psAcaxpfInsert.setString(index++, bnkoutpf.getBnktelname());
			psAcaxpfInsert.setString(index++, this.getUsrprf());
			psAcaxpfInsert.setString(index++, this.getJobnm());
			psAcaxpfInsert.setTimestamp(index++, new Timestamp(System.currentTimeMillis()));

			int affectedCount = executeUpdate(psAcaxpfInsert);

			LOGGER.debug("insertIntoBnkoutpf {} rows inserted.", affectedCount);//IJTI-1561

		} catch (SQLException e) {
			LOGGER.error("insertIntobnkoutpf()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psAcaxpfInsert, null);
		}
	}

	@Override
	public void updateBnkoutRecord(Bnkoutpf bnkoutpf) {

		StringBuilder bnkoutpfUpdate = new StringBuilder(
				"UPDATE BNKOUTPF SET BNKOUT=?,BNKOUTNAME=?,BNKSM=?,BNKSMNAME=?,BNKTEL=?,BNKTELNAME=?,AGNTNUM=? WHERE CHDRNUM=?");

		PreparedStatement bnkoutUpdate = getPrepareStatement(bnkoutpfUpdate.toString());
		try {

			bnkoutUpdate.setString(1, bnkoutpf.getBnkout());
			bnkoutUpdate.setString(2, bnkoutpf.getBnkoutname());
			bnkoutUpdate.setString(3, bnkoutpf.getBnksm());
			bnkoutUpdate.setString(4, bnkoutpf.getBnksmname());
			bnkoutUpdate.setString(5, bnkoutpf.getBnktel());
			bnkoutUpdate.setString(6, bnkoutpf.getBnktelname());
			bnkoutUpdate.setString(7, bnkoutpf.getAgntnum().trim());
			bnkoutUpdate.setString(8, bnkoutpf.getChdrnum());

			bnkoutUpdate.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("bnkoutpfUpdate()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			try {
				bnkoutUpdate.close();
			} catch (SQLException e) {
				LOGGER.error("updateBnkoutRecord() finally block", e);
			}
		}

	}
	public Bnkoutpf getAgentRefcode(String bankout) {
		StringBuilder sql = new StringBuilder(
				"select clntpf.SURNAME,agntpf.AGENTREFCODE,agntpf.AGENCYMGR,agntpf.AGNTNUM,clntpf.GIVNAME  ");
		sql.append(
				" from clntpf  inner join  agntpf on agntpf.CLNTNUM=clntpf.CLNTNUM and RTRIM(LTRIM(agntpf.AGENTREFCODE))= ?");
		Bnkoutpf bnkoutpf = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			pstmt = getConnection().prepareStatement(sql.toString());
			pstmt.setString(1, bankout);

			rs = pstmt.executeQuery();
			while (rs.next()) {
				bnkoutpf = new Bnkoutpf();
				bnkoutpf.setBnkoutname(rs.getString(1).trim()+" "+rs.getString(5).trim());
				bnkoutpf.setBnkout(rs.getString(2));
				bnkoutpf.setBnksm(rs.getString(3));
				bnkoutpf.setAgntnum(rs.getString(4));

			}
		} catch (SQLException e) {
			LOGGER.error(" AgntpfDAOImpl.getAgntRec()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(pstmt, rs);
		}
		return bnkoutpf;
	}

}
