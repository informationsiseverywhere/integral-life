package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH604.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh604Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private FixedLengthStringData datetext = new FixedLengthStringData(19);
	private ZonedDecimalData hpolcnt01 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData hpolcnt02 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData hpolcnt03 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData hpolcnt04 = new ZonedDecimalData(5, 0);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private ZonedDecimalData prcnt01 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData prcnt02 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData prcnt03 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData prcnt04 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData rsiipr01 = new ZonedDecimalData(14, 2);
	private ZonedDecimalData rsiipr02 = new ZonedDecimalData(14, 2);
	private ZonedDecimalData rsiipr03 = new ZonedDecimalData(14, 2);
	private ZonedDecimalData rsiipr04 = new ZonedDecimalData(14, 2);
	private ZonedDecimalData rsiipr05 = new ZonedDecimalData(14, 2);
	private ZonedDecimalData rsiipr06 = new ZonedDecimalData(14, 2);
	private ZonedDecimalData rsiipr07 = new ZonedDecimalData(14, 2);
	private ZonedDecimalData rsiipr08 = new ZonedDecimalData(14, 2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh604Report() {
		super();
	}


	/**
	 * Print the XML for Rh604d01
	 */
	public void printRh604d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 1, 3));
		rsiipr01.setFieldName("rsiipr01");
		rsiipr01.setInternal(subString(recordData, 4, 14));
		rsiipr02.setFieldName("rsiipr02");
		rsiipr02.setInternal(subString(recordData, 18, 14));
		hpolcnt01.setFieldName("hpolcnt01");
		hpolcnt01.setInternal(subString(recordData, 32, 5));
		prcnt01.setFieldName("prcnt01");
		prcnt01.setInternal(subString(recordData, 37, 5));
		rsiipr03.setFieldName("rsiipr03");
		rsiipr03.setInternal(subString(recordData, 42, 14));
		rsiipr04.setFieldName("rsiipr04");
		rsiipr04.setInternal(subString(recordData, 56, 14));
		hpolcnt02.setFieldName("hpolcnt02");
		hpolcnt02.setInternal(subString(recordData, 70, 5));
		prcnt02.setFieldName("prcnt02");
		prcnt02.setInternal(subString(recordData, 75, 5));
		printLayout("Rh604d01",			// Record name
			new BaseData[]{			// Fields:
				cnttype,
				rsiipr01,
				rsiipr02,
				hpolcnt01,
				prcnt01,
				rsiipr03,
				rsiipr04,
				hpolcnt02,
				prcnt02
			}
		);

	}

	/**
	 * Print the XML for Rh604f01
	 */
	public void printRh604f01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(3);

		printLayout("Rh604f01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh604h01
	 */
	public void printRh604h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datetext.setFieldName("datetext");
		datetext.setInternal(subString(recordData, 1, 19));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 20, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 21, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 51, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 61, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 63, 30));
		time.setFieldName("time");
		time.set(getTime());
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 93, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 123, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 126, 30));
		printLayout("Rh604h01",			// Record name
			new BaseData[]{			// Fields:
				datetext,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				longdesc,
				pagnbr,
				currcode,
				currencynm
			}
		);

		currentPrintLine.set(10);
	}

	/**
	 * Print the XML for Rh604h02
	 */
	public void printRh604h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh604h02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh604t01
	 */
	public void printRh604t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		rsiipr05.setFieldName("rsiipr05");
		rsiipr05.setInternal(subString(recordData, 1, 14));
		rsiipr06.setFieldName("rsiipr06");
		rsiipr06.setInternal(subString(recordData, 15, 14));
		hpolcnt03.setFieldName("hpolcnt03");
		hpolcnt03.setInternal(subString(recordData, 29, 5));
		prcnt03.setFieldName("prcnt03");
		prcnt03.setInternal(subString(recordData, 34, 5));
		rsiipr07.setFieldName("rsiipr07");
		rsiipr07.setInternal(subString(recordData, 39, 14));
		rsiipr08.setFieldName("rsiipr08");
		rsiipr08.setInternal(subString(recordData, 53, 14));
		hpolcnt04.setFieldName("hpolcnt04");
		hpolcnt04.setInternal(subString(recordData, 67, 5));
		prcnt04.setFieldName("prcnt04");
		prcnt04.setInternal(subString(recordData, 72, 5));
		printLayout("Rh604t01",			// Record name
			new BaseData[]{			// Fields:
				rsiipr05,
				rsiipr06,
				hpolcnt03,
				prcnt03,
				rsiipr07,
				rsiipr08,
				hpolcnt04,
				prcnt04
			}
		);

	}


}
