package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;


/**
 * 	
 * File: InctpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:39
 * Class transformed from INCTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class InctpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 10;
	public FixedLengthStringData inctrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData inctpfRecord = inctrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(inctrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(inctrec);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(inctrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public InctpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for InctpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public InctpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for InctpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public InctpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for InctpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public InctpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("INCTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"INDXFLG, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     indxflg,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		indxflg.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getInctrec() {
  		return inctrec;
	}

	public FixedLengthStringData getInctpfRecord() {
  		return inctpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setInctrec(what);
	}

	public void setInctrec(Object what) {
  		this.inctrec.set(what);
	}

	public void setInctpfRecord(Object what) {
  		this.inctpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(inctrec.getLength());
		result.set(inctrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}