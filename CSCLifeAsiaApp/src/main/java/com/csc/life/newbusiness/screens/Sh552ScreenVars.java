package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH552
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh552ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData ageIssageFrms = new FixedLengthStringData(24).isAPartOf(dataFields, 0);
	public ZonedDecimalData[] ageIssageFrm = ZDArrayPartOfStructure(8, 3, 0, ageIssageFrms, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(ageIssageFrms, 0, FILLER_REDEFINE);
	public ZonedDecimalData ageIssageFrm01 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData ageIssageFrm02 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,3);
	public ZonedDecimalData ageIssageFrm03 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData ageIssageFrm04 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,9);
	public ZonedDecimalData ageIssageFrm05 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData ageIssageFrm06 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,15);
	public ZonedDecimalData ageIssageFrm07 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,18);
	public ZonedDecimalData ageIssageFrm08 = DD.aagefrm.copyToZonedDecimal().isAPartOf(filler,21);
	public FixedLengthStringData ageIssageTos = new FixedLengthStringData(24).isAPartOf(dataFields, 24);
	public ZonedDecimalData[] ageIssageTo = ZDArrayPartOfStructure(8, 3, 0, ageIssageTos, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(ageIssageTos, 0, FILLER_REDEFINE);
	public ZonedDecimalData ageIssageTo01 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData ageIssageTo02 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,3);
	public ZonedDecimalData ageIssageTo03 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData ageIssageTo04 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,9);
	public ZonedDecimalData ageIssageTo05 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData ageIssageTo06 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,15);
	public ZonedDecimalData ageIssageTo07 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,18);
	public ZonedDecimalData ageIssageTo08 = DD.aageto.copyToZonedDecimal().isAPartOf(filler1,21);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData currcode = DD.currcode.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData defFupMeths = new FixedLengthStringData(256).isAPartOf(dataFields, 52);
	public FixedLengthStringData[] defFupMeth = FLSArrayPartOfStructure(64, 4, defFupMeths, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(256).isAPartOf(defFupMeths, 0, FILLER_REDEFINE);
	public FixedLengthStringData defFupMeth01 = DD.deffup.copy().isAPartOf(filler2,0);
	public FixedLengthStringData defFupMeth02 = DD.deffup.copy().isAPartOf(filler2,4);
	public FixedLengthStringData defFupMeth03 = DD.deffup.copy().isAPartOf(filler2,8);
	public FixedLengthStringData defFupMeth04 = DD.deffup.copy().isAPartOf(filler2,12);
	public FixedLengthStringData defFupMeth05 = DD.deffup.copy().isAPartOf(filler2,16);
	public FixedLengthStringData defFupMeth06 = DD.deffup.copy().isAPartOf(filler2,20);
	public FixedLengthStringData defFupMeth07 = DD.deffup.copy().isAPartOf(filler2,24);
	public FixedLengthStringData defFupMeth08 = DD.deffup.copy().isAPartOf(filler2,28);
	public FixedLengthStringData defFupMeth09 = DD.deffup.copy().isAPartOf(filler2,32);
	public FixedLengthStringData defFupMeth10 = DD.deffup.copy().isAPartOf(filler2,36);
	public FixedLengthStringData defFupMeth11 = DD.deffup.copy().isAPartOf(filler2,40);
	public FixedLengthStringData defFupMeth12 = DD.deffup.copy().isAPartOf(filler2,44);
	public FixedLengthStringData defFupMeth13 = DD.deffup.copy().isAPartOf(filler2,48);
	public FixedLengthStringData defFupMeth14 = DD.deffup.copy().isAPartOf(filler2,52);
	public FixedLengthStringData defFupMeth15 = DD.deffup.copy().isAPartOf(filler2,56);
	public FixedLengthStringData defFupMeth16 = DD.deffup.copy().isAPartOf(filler2,60);
	public FixedLengthStringData defFupMeth17 = DD.deffup.copy().isAPartOf(filler2,64);
	public FixedLengthStringData defFupMeth18 = DD.deffup.copy().isAPartOf(filler2,68);
	public FixedLengthStringData defFupMeth19 = DD.deffup.copy().isAPartOf(filler2,72);
	public FixedLengthStringData defFupMeth20 = DD.deffup.copy().isAPartOf(filler2,76);
	public FixedLengthStringData defFupMeth21 = DD.deffup.copy().isAPartOf(filler2,80);
	public FixedLengthStringData defFupMeth22 = DD.deffup.copy().isAPartOf(filler2,84);
	public FixedLengthStringData defFupMeth23 = DD.deffup.copy().isAPartOf(filler2,88);
	public FixedLengthStringData defFupMeth24 = DD.deffup.copy().isAPartOf(filler2,92);
	public FixedLengthStringData defFupMeth25 = DD.deffup.copy().isAPartOf(filler2,96);
	public FixedLengthStringData defFupMeth26 = DD.deffup.copy().isAPartOf(filler2,100);
	public FixedLengthStringData defFupMeth27 = DD.deffup.copy().isAPartOf(filler2,104);
	public FixedLengthStringData defFupMeth28 = DD.deffup.copy().isAPartOf(filler2,108);
	public FixedLengthStringData defFupMeth29 = DD.deffup.copy().isAPartOf(filler2,112);
	public FixedLengthStringData defFupMeth30 = DD.deffup.copy().isAPartOf(filler2,116);
	public FixedLengthStringData defFupMeth31 = DD.deffup.copy().isAPartOf(filler2,120);
	public FixedLengthStringData defFupMeth32 = DD.deffup.copy().isAPartOf(filler2,124);
	public FixedLengthStringData defFupMeth33 = DD.deffup.copy().isAPartOf(filler2,128);
	public FixedLengthStringData defFupMeth34 = DD.deffup.copy().isAPartOf(filler2,132);
	public FixedLengthStringData defFupMeth35 = DD.deffup.copy().isAPartOf(filler2,136);
	public FixedLengthStringData defFupMeth36 = DD.deffup.copy().isAPartOf(filler2,140);
	public FixedLengthStringData defFupMeth37 = DD.deffup.copy().isAPartOf(filler2,144);
	public FixedLengthStringData defFupMeth38 = DD.deffup.copy().isAPartOf(filler2,148);
	public FixedLengthStringData defFupMeth39 = DD.deffup.copy().isAPartOf(filler2,152);
	public FixedLengthStringData defFupMeth40 = DD.deffup.copy().isAPartOf(filler2,156);
	public FixedLengthStringData defFupMeth41 = DD.deffup.copy().isAPartOf(filler2,160);
	public FixedLengthStringData defFupMeth42 = DD.deffup.copy().isAPartOf(filler2,164);
	public FixedLengthStringData defFupMeth43 = DD.deffup.copy().isAPartOf(filler2,168);
	public FixedLengthStringData defFupMeth44 = DD.deffup.copy().isAPartOf(filler2,172);
	public FixedLengthStringData defFupMeth45 = DD.deffup.copy().isAPartOf(filler2,176);
	public FixedLengthStringData defFupMeth46 = DD.deffup.copy().isAPartOf(filler2,180);
	public FixedLengthStringData defFupMeth47 = DD.deffup.copy().isAPartOf(filler2,184);
	public FixedLengthStringData defFupMeth48 = DD.deffup.copy().isAPartOf(filler2,188);
	public FixedLengthStringData defFupMeth49 = DD.deffup.copy().isAPartOf(filler2,192);
	public FixedLengthStringData defFupMeth50 = DD.deffup.copy().isAPartOf(filler2,196);
	public FixedLengthStringData defFupMeth51 = DD.deffup.copy().isAPartOf(filler2,200);
	public FixedLengthStringData defFupMeth52 = DD.deffup.copy().isAPartOf(filler2,204);
	public FixedLengthStringData defFupMeth53 = DD.deffup.copy().isAPartOf(filler2,208);
	public FixedLengthStringData defFupMeth54 = DD.deffup.copy().isAPartOf(filler2,212);
	public FixedLengthStringData defFupMeth55 = DD.deffup.copy().isAPartOf(filler2,216);
	public FixedLengthStringData defFupMeth56 = DD.deffup.copy().isAPartOf(filler2,220);
	public FixedLengthStringData defFupMeth57 = DD.deffup.copy().isAPartOf(filler2,224);
	public FixedLengthStringData defFupMeth58 = DD.deffup.copy().isAPartOf(filler2,228);
	public FixedLengthStringData defFupMeth59 = DD.deffup.copy().isAPartOf(filler2,232);
	public FixedLengthStringData defFupMeth60 = DD.deffup.copy().isAPartOf(filler2,236);
	public FixedLengthStringData defFupMeth61 = DD.deffup.copy().isAPartOf(filler2,240);
	public FixedLengthStringData defFupMeth62 = DD.deffup.copy().isAPartOf(filler2,244);
	public FixedLengthStringData defFupMeth63 = DD.deffup.copy().isAPartOf(filler2,248);
	public FixedLengthStringData defFupMeth64 = DD.deffup.copy().isAPartOf(filler2,252);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,308);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,316);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,346);
	public FixedLengthStringData zsuminfrs = new FixedLengthStringData(96).isAPartOf(dataFields, 351);
	public ZonedDecimalData[] zsuminfr = ZDArrayPartOfStructure(8, 12, 0, zsuminfrs, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(96).isAPartOf(zsuminfrs, 0, FILLER_REDEFINE);//ILJ-107
	public ZonedDecimalData zsuminfr01 = DD.zsuminfr.copyToZonedDecimal().isAPartOf(filler3,0);
	public ZonedDecimalData zsuminfr02 = DD.zsuminfr.copyToZonedDecimal().isAPartOf(filler3,12);
	public ZonedDecimalData zsuminfr03 = DD.zsuminfr.copyToZonedDecimal().isAPartOf(filler3,24);
	public ZonedDecimalData zsuminfr04 = DD.zsuminfr.copyToZonedDecimal().isAPartOf(filler3,36);
	public ZonedDecimalData zsuminfr05 = DD.zsuminfr.copyToZonedDecimal().isAPartOf(filler3,48);
	public ZonedDecimalData zsuminfr06 = DD.zsuminfr.copyToZonedDecimal().isAPartOf(filler3,60);
	public ZonedDecimalData zsuminfr07 = DD.zsuminfr.copyToZonedDecimal().isAPartOf(filler3,72);
	public ZonedDecimalData zsuminfr08 = DD.zsuminfr.copyToZonedDecimal().isAPartOf(filler3,84);
	public FixedLengthStringData zsumintos = new FixedLengthStringData(96).isAPartOf(dataFields, 447);
	public ZonedDecimalData[] zsuminto = ZDArrayPartOfStructure(8, 12, 0, zsumintos, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(96).isAPartOf(zsumintos, 0, FILLER_REDEFINE);//ILJ-107
	public ZonedDecimalData zsuminto01 = DD.zsuminto.copyToZonedDecimal().isAPartOf(filler4,0);
	public ZonedDecimalData zsuminto02 = DD.zsuminto.copyToZonedDecimal().isAPartOf(filler4,12);
	public ZonedDecimalData zsuminto03 = DD.zsuminto.copyToZonedDecimal().isAPartOf(filler4,24);
	public ZonedDecimalData zsuminto04 = DD.zsuminto.copyToZonedDecimal().isAPartOf(filler4,36);
	public ZonedDecimalData zsuminto05 = DD.zsuminto.copyToZonedDecimal().isAPartOf(filler4,48);
	public ZonedDecimalData zsuminto06 = DD.zsuminto.copyToZonedDecimal().isAPartOf(filler4,60);
	public ZonedDecimalData zsuminto07 = DD.zsuminto.copyToZonedDecimal().isAPartOf(filler4,72);
	public ZonedDecimalData zsuminto08 = DD.zsuminto.copyToZonedDecimal().isAPartOf(filler4,84);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData aagefrmsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] aagefrmErr = FLSArrayPartOfStructure(8, 4, aagefrmsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(32).isAPartOf(aagefrmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData aagefrm01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData aagefrm02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData aagefrm03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData aagefrm04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData aagefrm05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData aagefrm06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData aagefrm07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData aagefrm08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData aagetosErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] aagetoErr = FLSArrayPartOfStructure(8, 4, aagetosErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(32).isAPartOf(aagetosErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData aageto01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData aageto02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData aageto03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData aageto04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData aageto05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData aageto06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData aageto07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData aageto08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData currcodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData deffupsErr = new FixedLengthStringData(256).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] deffupErr = FLSArrayPartOfStructure(64, 4, deffupsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(256).isAPartOf(deffupsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData deffup01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData deffup02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData deffup03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData deffup04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData deffup05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData deffup06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData deffup07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData deffup08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData deffup09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData deffup10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	public FixedLengthStringData deffup11Err = new FixedLengthStringData(4).isAPartOf(filler7, 40);
	public FixedLengthStringData deffup12Err = new FixedLengthStringData(4).isAPartOf(filler7, 44);
	public FixedLengthStringData deffup13Err = new FixedLengthStringData(4).isAPartOf(filler7, 48);
	public FixedLengthStringData deffup14Err = new FixedLengthStringData(4).isAPartOf(filler7, 52);
	public FixedLengthStringData deffup15Err = new FixedLengthStringData(4).isAPartOf(filler7, 56);
	public FixedLengthStringData deffup16Err = new FixedLengthStringData(4).isAPartOf(filler7, 60);
	public FixedLengthStringData deffup17Err = new FixedLengthStringData(4).isAPartOf(filler7, 64);
	public FixedLengthStringData deffup18Err = new FixedLengthStringData(4).isAPartOf(filler7, 68);
	public FixedLengthStringData deffup19Err = new FixedLengthStringData(4).isAPartOf(filler7, 72);
	public FixedLengthStringData deffup20Err = new FixedLengthStringData(4).isAPartOf(filler7, 76);
	public FixedLengthStringData deffup21Err = new FixedLengthStringData(4).isAPartOf(filler7, 80);
	public FixedLengthStringData deffup22Err = new FixedLengthStringData(4).isAPartOf(filler7, 84);
	public FixedLengthStringData deffup23Err = new FixedLengthStringData(4).isAPartOf(filler7, 88);
	public FixedLengthStringData deffup24Err = new FixedLengthStringData(4).isAPartOf(filler7, 92);
	public FixedLengthStringData deffup25Err = new FixedLengthStringData(4).isAPartOf(filler7, 96);
	public FixedLengthStringData deffup26Err = new FixedLengthStringData(4).isAPartOf(filler7, 100);
	public FixedLengthStringData deffup27Err = new FixedLengthStringData(4).isAPartOf(filler7, 104);
	public FixedLengthStringData deffup28Err = new FixedLengthStringData(4).isAPartOf(filler7, 108);
	public FixedLengthStringData deffup29Err = new FixedLengthStringData(4).isAPartOf(filler7, 112);
	public FixedLengthStringData deffup30Err = new FixedLengthStringData(4).isAPartOf(filler7, 116);
	public FixedLengthStringData deffup31Err = new FixedLengthStringData(4).isAPartOf(filler7, 120);
	public FixedLengthStringData deffup32Err = new FixedLengthStringData(4).isAPartOf(filler7, 124);
	public FixedLengthStringData deffup33Err = new FixedLengthStringData(4).isAPartOf(filler7, 128);
	public FixedLengthStringData deffup34Err = new FixedLengthStringData(4).isAPartOf(filler7, 132);
	public FixedLengthStringData deffup35Err = new FixedLengthStringData(4).isAPartOf(filler7, 136);
	public FixedLengthStringData deffup36Err = new FixedLengthStringData(4).isAPartOf(filler7, 140);
	public FixedLengthStringData deffup37Err = new FixedLengthStringData(4).isAPartOf(filler7, 144);
	public FixedLengthStringData deffup38Err = new FixedLengthStringData(4).isAPartOf(filler7, 148);
	public FixedLengthStringData deffup39Err = new FixedLengthStringData(4).isAPartOf(filler7, 152);
	public FixedLengthStringData deffup40Err = new FixedLengthStringData(4).isAPartOf(filler7, 156);
	public FixedLengthStringData deffup41Err = new FixedLengthStringData(4).isAPartOf(filler7, 160);
	public FixedLengthStringData deffup42Err = new FixedLengthStringData(4).isAPartOf(filler7, 164);
	public FixedLengthStringData deffup43Err = new FixedLengthStringData(4).isAPartOf(filler7, 168);
	public FixedLengthStringData deffup44Err = new FixedLengthStringData(4).isAPartOf(filler7, 172);
	public FixedLengthStringData deffup45Err = new FixedLengthStringData(4).isAPartOf(filler7, 176);
	public FixedLengthStringData deffup46Err = new FixedLengthStringData(4).isAPartOf(filler7, 180);
	public FixedLengthStringData deffup47Err = new FixedLengthStringData(4).isAPartOf(filler7, 184);
	public FixedLengthStringData deffup48Err = new FixedLengthStringData(4).isAPartOf(filler7, 188);
	public FixedLengthStringData deffup49Err = new FixedLengthStringData(4).isAPartOf(filler7, 192);
	public FixedLengthStringData deffup50Err = new FixedLengthStringData(4).isAPartOf(filler7, 196);
	public FixedLengthStringData deffup51Err = new FixedLengthStringData(4).isAPartOf(filler7, 200);
	public FixedLengthStringData deffup52Err = new FixedLengthStringData(4).isAPartOf(filler7, 204);
	public FixedLengthStringData deffup53Err = new FixedLengthStringData(4).isAPartOf(filler7, 208);
	public FixedLengthStringData deffup54Err = new FixedLengthStringData(4).isAPartOf(filler7, 212);
	public FixedLengthStringData deffup55Err = new FixedLengthStringData(4).isAPartOf(filler7, 216);
	public FixedLengthStringData deffup56Err = new FixedLengthStringData(4).isAPartOf(filler7, 220);
	public FixedLengthStringData deffup57Err = new FixedLengthStringData(4).isAPartOf(filler7, 224);
	public FixedLengthStringData deffup58Err = new FixedLengthStringData(4).isAPartOf(filler7, 228);
	public FixedLengthStringData deffup59Err = new FixedLengthStringData(4).isAPartOf(filler7, 232);
	public FixedLengthStringData deffup60Err = new FixedLengthStringData(4).isAPartOf(filler7, 236);
	public FixedLengthStringData deffup61Err = new FixedLengthStringData(4).isAPartOf(filler7, 240);
	public FixedLengthStringData deffup62Err = new FixedLengthStringData(4).isAPartOf(filler7, 244);
	public FixedLengthStringData deffup63Err = new FixedLengthStringData(4).isAPartOf(filler7, 248);
	public FixedLengthStringData deffup64Err = new FixedLengthStringData(4).isAPartOf(filler7, 252);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 328);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 332);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 336);
	public FixedLengthStringData zsuminfrsErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 340);
	public FixedLengthStringData[] zsuminfrErr = FLSArrayPartOfStructure(8, 4, zsuminfrsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(32).isAPartOf(zsuminfrsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zsuminfr01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData zsuminfr02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData zsuminfr03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData zsuminfr04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData zsuminfr05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData zsuminfr06Err = new FixedLengthStringData(4).isAPartOf(filler8, 20);
	public FixedLengthStringData zsuminfr07Err = new FixedLengthStringData(4).isAPartOf(filler8, 24);
	public FixedLengthStringData zsuminfr08Err = new FixedLengthStringData(4).isAPartOf(filler8, 28);
	public FixedLengthStringData zsumintosErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 372);
	public FixedLengthStringData[] zsumintoErr = FLSArrayPartOfStructure(8, 4, zsumintosErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(32).isAPartOf(zsumintosErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zsuminto01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData zsuminto02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData zsuminto03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData zsuminto04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData zsuminto05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	public FixedLengthStringData zsuminto06Err = new FixedLengthStringData(4).isAPartOf(filler9, 20);
	public FixedLengthStringData zsuminto07Err = new FixedLengthStringData(4).isAPartOf(filler9, 24);
	public FixedLengthStringData zsuminto08Err = new FixedLengthStringData(4).isAPartOf(filler9, 28);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize() + getErrorIndicatorSize());
	public FixedLengthStringData aagefrmsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] aagefrmOut = FLSArrayPartOfStructure(8, 12, aagefrmsOut, 0);
	public FixedLengthStringData[][] aagefrmO = FLSDArrayPartOfArrayStructure(12, 1, aagefrmOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(96).isAPartOf(aagefrmsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] aagefrm01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] aagefrm02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] aagefrm03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] aagefrm04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] aagefrm05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] aagefrm06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] aagefrm07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] aagefrm08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData aagetosOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] aagetoOut = FLSArrayPartOfStructure(8, 12, aagetosOut, 0);
	public FixedLengthStringData[][] aagetoO = FLSDArrayPartOfArrayStructure(12, 1, aagetoOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(96).isAPartOf(aagetosOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] aageto01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] aageto02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] aageto03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] aageto04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] aageto05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] aageto06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] aageto07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] aageto08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] currcodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData deffupsOut = new FixedLengthStringData(768).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] deffupOut = FLSArrayPartOfStructure(64, 12, deffupsOut, 0);
	public FixedLengthStringData[][] deffupO = FLSDArrayPartOfArrayStructure(12, 1, deffupOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(768).isAPartOf(deffupsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] deffup01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] deffup02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] deffup03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] deffup04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] deffup05Out = FLSArrayPartOfStructure(12, 1, filler12, 48);
	public FixedLengthStringData[] deffup06Out = FLSArrayPartOfStructure(12, 1, filler12, 60);
	public FixedLengthStringData[] deffup07Out = FLSArrayPartOfStructure(12, 1, filler12, 72);
	public FixedLengthStringData[] deffup08Out = FLSArrayPartOfStructure(12, 1, filler12, 84);
	public FixedLengthStringData[] deffup09Out = FLSArrayPartOfStructure(12, 1, filler12, 96);
	public FixedLengthStringData[] deffup10Out = FLSArrayPartOfStructure(12, 1, filler12, 108);
	public FixedLengthStringData[] deffup11Out = FLSArrayPartOfStructure(12, 1, filler12, 120);
	public FixedLengthStringData[] deffup12Out = FLSArrayPartOfStructure(12, 1, filler12, 132);
	public FixedLengthStringData[] deffup13Out = FLSArrayPartOfStructure(12, 1, filler12, 144);
	public FixedLengthStringData[] deffup14Out = FLSArrayPartOfStructure(12, 1, filler12, 156);
	public FixedLengthStringData[] deffup15Out = FLSArrayPartOfStructure(12, 1, filler12, 168);
	public FixedLengthStringData[] deffup16Out = FLSArrayPartOfStructure(12, 1, filler12, 180);
	public FixedLengthStringData[] deffup17Out = FLSArrayPartOfStructure(12, 1, filler12, 192);
	public FixedLengthStringData[] deffup18Out = FLSArrayPartOfStructure(12, 1, filler12, 204);
	public FixedLengthStringData[] deffup19Out = FLSArrayPartOfStructure(12, 1, filler12, 216);
	public FixedLengthStringData[] deffup20Out = FLSArrayPartOfStructure(12, 1, filler12, 228);
	public FixedLengthStringData[] deffup21Out = FLSArrayPartOfStructure(12, 1, filler12, 240);
	public FixedLengthStringData[] deffup22Out = FLSArrayPartOfStructure(12, 1, filler12, 252);
	public FixedLengthStringData[] deffup23Out = FLSArrayPartOfStructure(12, 1, filler12, 264);
	public FixedLengthStringData[] deffup24Out = FLSArrayPartOfStructure(12, 1, filler12, 276);
	public FixedLengthStringData[] deffup25Out = FLSArrayPartOfStructure(12, 1, filler12, 288);
	public FixedLengthStringData[] deffup26Out = FLSArrayPartOfStructure(12, 1, filler12, 300);
	public FixedLengthStringData[] deffup27Out = FLSArrayPartOfStructure(12, 1, filler12, 312);
	public FixedLengthStringData[] deffup28Out = FLSArrayPartOfStructure(12, 1, filler12, 324);
	public FixedLengthStringData[] deffup29Out = FLSArrayPartOfStructure(12, 1, filler12, 336);
	public FixedLengthStringData[] deffup30Out = FLSArrayPartOfStructure(12, 1, filler12, 348);
	public FixedLengthStringData[] deffup31Out = FLSArrayPartOfStructure(12, 1, filler12, 360);
	public FixedLengthStringData[] deffup32Out = FLSArrayPartOfStructure(12, 1, filler12, 372);
	public FixedLengthStringData[] deffup33Out = FLSArrayPartOfStructure(12, 1, filler12, 384);
	public FixedLengthStringData[] deffup34Out = FLSArrayPartOfStructure(12, 1, filler12, 396);
	public FixedLengthStringData[] deffup35Out = FLSArrayPartOfStructure(12, 1, filler12, 408);
	public FixedLengthStringData[] deffup36Out = FLSArrayPartOfStructure(12, 1, filler12, 420);
	public FixedLengthStringData[] deffup37Out = FLSArrayPartOfStructure(12, 1, filler12, 432);
	public FixedLengthStringData[] deffup38Out = FLSArrayPartOfStructure(12, 1, filler12, 444);
	public FixedLengthStringData[] deffup39Out = FLSArrayPartOfStructure(12, 1, filler12, 456);
	public FixedLengthStringData[] deffup40Out = FLSArrayPartOfStructure(12, 1, filler12, 468);
	public FixedLengthStringData[] deffup41Out = FLSArrayPartOfStructure(12, 1, filler12, 480);
	public FixedLengthStringData[] deffup42Out = FLSArrayPartOfStructure(12, 1, filler12, 492);
	public FixedLengthStringData[] deffup43Out = FLSArrayPartOfStructure(12, 1, filler12, 504);
	public FixedLengthStringData[] deffup44Out = FLSArrayPartOfStructure(12, 1, filler12, 516);
	public FixedLengthStringData[] deffup45Out = FLSArrayPartOfStructure(12, 1, filler12, 528);
	public FixedLengthStringData[] deffup46Out = FLSArrayPartOfStructure(12, 1, filler12, 540);
	public FixedLengthStringData[] deffup47Out = FLSArrayPartOfStructure(12, 1, filler12, 552);
	public FixedLengthStringData[] deffup48Out = FLSArrayPartOfStructure(12, 1, filler12, 564);
	public FixedLengthStringData[] deffup49Out = FLSArrayPartOfStructure(12, 1, filler12, 576);
	public FixedLengthStringData[] deffup50Out = FLSArrayPartOfStructure(12, 1, filler12, 588);
	public FixedLengthStringData[] deffup51Out = FLSArrayPartOfStructure(12, 1, filler12, 600);
	public FixedLengthStringData[] deffup52Out = FLSArrayPartOfStructure(12, 1, filler12, 612);
	public FixedLengthStringData[] deffup53Out = FLSArrayPartOfStructure(12, 1, filler12, 624);
	public FixedLengthStringData[] deffup54Out = FLSArrayPartOfStructure(12, 1, filler12, 636);
	public FixedLengthStringData[] deffup55Out = FLSArrayPartOfStructure(12, 1, filler12, 648);
	public FixedLengthStringData[] deffup56Out = FLSArrayPartOfStructure(12, 1, filler12, 660);
	public FixedLengthStringData[] deffup57Out = FLSArrayPartOfStructure(12, 1, filler12, 672);
	public FixedLengthStringData[] deffup58Out = FLSArrayPartOfStructure(12, 1, filler12, 684);
	public FixedLengthStringData[] deffup59Out = FLSArrayPartOfStructure(12, 1, filler12, 696);
	public FixedLengthStringData[] deffup60Out = FLSArrayPartOfStructure(12, 1, filler12, 708);
	public FixedLengthStringData[] deffup61Out = FLSArrayPartOfStructure(12, 1, filler12, 720);
	public FixedLengthStringData[] deffup62Out = FLSArrayPartOfStructure(12, 1, filler12, 732);
	public FixedLengthStringData[] deffup63Out = FLSArrayPartOfStructure(12, 1, filler12, 744);
	public FixedLengthStringData[] deffup64Out = FLSArrayPartOfStructure(12, 1, filler12, 756);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 984);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 996);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 1008);
	public FixedLengthStringData zsuminfrsOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 1020);
	public FixedLengthStringData[] zsuminfrOut = FLSArrayPartOfStructure(8, 12, zsuminfrsOut, 0);
	public FixedLengthStringData[][] zsuminfrO = FLSDArrayPartOfArrayStructure(12, 1, zsuminfrOut, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(96).isAPartOf(zsuminfrsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zsuminfr01Out = FLSArrayPartOfStructure(12, 1, filler13, 0);
	public FixedLengthStringData[] zsuminfr02Out = FLSArrayPartOfStructure(12, 1, filler13, 12);
	public FixedLengthStringData[] zsuminfr03Out = FLSArrayPartOfStructure(12, 1, filler13, 24);
	public FixedLengthStringData[] zsuminfr04Out = FLSArrayPartOfStructure(12, 1, filler13, 36);
	public FixedLengthStringData[] zsuminfr05Out = FLSArrayPartOfStructure(12, 1, filler13, 48);
	public FixedLengthStringData[] zsuminfr06Out = FLSArrayPartOfStructure(12, 1, filler13, 60);
	public FixedLengthStringData[] zsuminfr07Out = FLSArrayPartOfStructure(12, 1, filler13, 72);
	public FixedLengthStringData[] zsuminfr08Out = FLSArrayPartOfStructure(12, 1, filler13, 84);
	public FixedLengthStringData zsumintosOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 1116);
	public FixedLengthStringData[] zsumintoOut = FLSArrayPartOfStructure(8, 12, zsumintosOut, 0);
	public FixedLengthStringData[][] zsumintoO = FLSDArrayPartOfArrayStructure(12, 1, zsumintoOut, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(96).isAPartOf(zsumintosOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zsuminto01Out = FLSArrayPartOfStructure(12, 1, filler14, 0);
	public FixedLengthStringData[] zsuminto02Out = FLSArrayPartOfStructure(12, 1, filler14, 12);
	public FixedLengthStringData[] zsuminto03Out = FLSArrayPartOfStructure(12, 1, filler14, 24);
	public FixedLengthStringData[] zsuminto04Out = FLSArrayPartOfStructure(12, 1, filler14, 36);
	public FixedLengthStringData[] zsuminto05Out = FLSArrayPartOfStructure(12, 1, filler14, 48);
	public FixedLengthStringData[] zsuminto06Out = FLSArrayPartOfStructure(12, 1, filler14, 60);
	public FixedLengthStringData[] zsuminto07Out = FLSArrayPartOfStructure(12, 1, filler14, 72);
	public FixedLengthStringData[] zsuminto08Out = FLSArrayPartOfStructure(12, 1, filler14, 84);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh552screenWritten = new LongData(0);
	public LongData Sh552protectWritten = new LongData(0);
	
	public static int[] screenPfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24};
	
	public boolean hasSubfile() {
		return false;
	}


	public Sh552ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh552screen.class;
		protectRecord = Sh552protect.class;
	}
	
	public int getDataAreaSize()
	{
		return 2159 ;
	}
	

	public int getDataFieldsSize()
	{
		return 543 ;
	}
	

	public int getErrorIndicatorSize()
	{
		return 404;
	}
	
	public int getOutputFieldSize()
	{
		return 1212;
	}
	
	public BaseData[] getscreenFields()
	{
		return new BaseData[] {company, tabl, item, longdesc, currcode, zsuminfr01, zsuminto01, defFupMeth01, ageIssageFrm01, ageIssageTo01, ageIssageFrm02, ageIssageTo02, ageIssageFrm03, ageIssageTo03, ageIssageFrm04, ageIssageTo04, ageIssageFrm05, ageIssageTo05, ageIssageFrm06, ageIssageTo06, ageIssageFrm07, ageIssageTo07, ageIssageFrm08, ageIssageTo08, defFupMeth02, defFupMeth03, defFupMeth04, defFupMeth05, defFupMeth06, defFupMeth07, defFupMeth08, zsuminfr02, zsuminto02, defFupMeth09, defFupMeth10, defFupMeth11, defFupMeth12, defFupMeth13, defFupMeth14, defFupMeth15, defFupMeth16, zsuminfr03, zsuminto03, defFupMeth17, defFupMeth18, defFupMeth19, defFupMeth20, defFupMeth21, defFupMeth22, defFupMeth23, defFupMeth24, zsuminfr04, zsuminto04, defFupMeth25, defFupMeth26, defFupMeth27, defFupMeth28, defFupMeth29, defFupMeth30, defFupMeth31, defFupMeth32, zsuminfr05, zsuminto05, defFupMeth33, defFupMeth34, defFupMeth35, defFupMeth36, defFupMeth37, defFupMeth38, defFupMeth39, defFupMeth40, zsuminfr06, zsuminto06, defFupMeth41, defFupMeth42, defFupMeth43, defFupMeth44, defFupMeth45, defFupMeth46, defFupMeth47, defFupMeth48, zsuminfr07, zsuminto07, defFupMeth49, defFupMeth50, defFupMeth51, defFupMeth52, defFupMeth53, defFupMeth54, defFupMeth55, defFupMeth56, zsuminfr08, zsuminto08, defFupMeth57, defFupMeth58, defFupMeth59, defFupMeth60, defFupMeth62, defFupMeth63, defFupMeth64, defFupMeth61};
	}
	
	public BaseData[][] getscreenOutFields()
	{
		return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, currcodeOut, zsuminfr01Out, zsuminto01Out, deffup01Out, aagefrm01Out, aageto01Out, aagefrm02Out, aageto02Out, aagefrm03Out, aageto03Out, aagefrm04Out, aageto04Out, aagefrm05Out, aageto05Out, aagefrm06Out, aageto06Out, aagefrm07Out, aageto07Out, aagefrm08Out, aageto08Out, deffup02Out, deffup03Out, deffup04Out, deffup05Out, deffup06Out, deffup07Out, deffup08Out, zsuminfr02Out, zsuminto02Out, deffup09Out, deffup10Out, deffup11Out, deffup12Out, deffup13Out, deffup14Out, deffup15Out, deffup16Out, zsuminfr03Out, zsuminto03Out, deffup17Out, deffup18Out, deffup19Out, deffup20Out, deffup21Out, deffup22Out, deffup23Out, deffup24Out, zsuminfr04Out, zsuminto04Out, deffup25Out, deffup26Out, deffup27Out, deffup28Out, deffup29Out, deffup30Out, deffup31Out, deffup32Out, zsuminfr05Out, zsuminto05Out, deffup33Out, deffup34Out, deffup35Out, deffup36Out, deffup37Out, deffup38Out, deffup39Out, deffup40Out, zsuminfr06Out, zsuminto06Out, deffup41Out, deffup42Out, deffup43Out, deffup44Out, deffup45Out, deffup46Out, deffup47Out, deffup48Out, zsuminfr07Out, zsuminto07Out, deffup49Out, deffup50Out, deffup51Out, deffup52Out, deffup53Out, deffup54Out, deffup55Out, deffup56Out, zsuminfr08Out, zsuminto08Out, deffup57Out, deffup58Out, deffup59Out, deffup60Out, deffup62Out, deffup63Out, deffup64Out, deffup61Out};
	}
	
	public BaseData[] getscreenErrFields()
	{
		return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, currcodeErr, zsuminfr01Err, zsuminto01Err, deffup01Err, aagefrm01Err, aageto01Err, aagefrm02Err, aageto02Err, aagefrm03Err, aageto03Err, aagefrm04Err, aageto04Err, aagefrm05Err, aageto05Err, aagefrm06Err, aageto06Err, aagefrm07Err, aageto07Err, aagefrm08Err, aageto08Err, deffup02Err, deffup03Err, deffup04Err, deffup05Err, deffup06Err, deffup07Err, deffup08Err, zsuminfr02Err, zsuminto02Err, deffup09Err, deffup10Err, deffup11Err, deffup12Err, deffup13Err, deffup14Err, deffup15Err, deffup16Err, zsuminfr03Err, zsuminto03Err, deffup17Err, deffup18Err, deffup19Err, deffup20Err, deffup21Err, deffup22Err, deffup23Err, deffup24Err, zsuminfr04Err, zsuminto04Err, deffup25Err, deffup26Err, deffup27Err, deffup28Err, deffup29Err, deffup30Err, deffup31Err, deffup32Err, zsuminfr05Err, zsuminto05Err, deffup33Err, deffup34Err, deffup35Err, deffup36Err, deffup37Err, deffup38Err, deffup39Err, deffup40Err, zsuminfr06Err, zsuminto06Err, deffup41Err, deffup42Err, deffup43Err, deffup44Err, deffup45Err, deffup46Err, deffup47Err, deffup48Err, zsuminfr07Err, zsuminto07Err, deffup49Err, deffup50Err, deffup51Err, deffup52Err, deffup53Err, deffup54Err, deffup55Err, deffup56Err, zsuminfr08Err, zsuminto08Err, deffup57Err, deffup58Err, deffup59Err, deffup60Err, deffup62Err, deffup63Err, deffup64Err, deffup61Err};
	}
	

	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {};
	}
	

	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {};
	}
	
	public BaseData[] getscreenDateErrFields()
	{
		return  new BaseData[] {};
	}
	
	public static int[] getScreenPfInds()
	{
		return screenPfInds;
	}

}
