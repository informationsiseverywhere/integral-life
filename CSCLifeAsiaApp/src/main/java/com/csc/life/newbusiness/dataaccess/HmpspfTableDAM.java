package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HmpspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:32
 * Class transformed from HMPSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HmpspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 96;
	public FixedLengthStringData hmpsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hmpspfRecord = hmpsrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hmpsrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(hmpsrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(hmpsrec);
	public PackedDecimalData mnth = DD.mnth.copy().isAPartOf(hmpsrec);
	public PackedDecimalData year = DD.year.copy().isAPartOf(hmpsrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(hmpsrec);
	public PackedDecimalData cntcount = DD.cntcount.copy().isAPartOf(hmpsrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(hmpsrec);
	public PackedDecimalData annprem = DD.annprem.copy().isAPartOf(hmpsrec);
	public PackedDecimalData singp = DD.singp.copy().isAPartOf(hmpsrec);
	public PackedDecimalData updateDate = DD.upddte.copy().isAPartOf(hmpsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hmpsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hmpsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hmpsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HmpspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HmpspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HmpspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HmpspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HmpspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HmpspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HmpspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HMPSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CNTBRANCH, " +
							"CNTTYPE, " +
							"MNTH, " +
							"YEAR, " +
							"CNTCURR, " +
							"CNTCOUNT, " +
							"SUMINS, " +
							"ANNPREM, " +
							"SINGP, " +
							"UPDDTE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     cntbranch,
                                     cnttype,
                                     mnth,
                                     year,
                                     cntcurr,
                                     cntcount,
                                     sumins,
                                     annprem,
                                     singp,
                                     updateDate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		cntbranch.clear();
  		cnttype.clear();
  		mnth.clear();
  		year.clear();
  		cntcurr.clear();
  		cntcount.clear();
  		sumins.clear();
  		annprem.clear();
  		singp.clear();
  		updateDate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHmpsrec() {
  		return hmpsrec;
	}

	public FixedLengthStringData getHmpspfRecord() {
  		return hmpspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHmpsrec(what);
	}

	public void setHmpsrec(Object what) {
  		this.hmpsrec.set(what);
	}

	public void setHmpspfRecord(Object what) {
  		this.hmpspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hmpsrec.getLength());
		result.set(hmpsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}