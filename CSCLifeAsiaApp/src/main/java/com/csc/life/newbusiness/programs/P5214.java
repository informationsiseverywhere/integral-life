/*
 * File: P5214.java
 * Date: 30 August 2009 0:18:44
 * Author: Quipoz Limited
 * 
 * Class transformed from P5214.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import javax.swing.JOptionPane;

import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ClrrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Clrrpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.screens.S5214ScreenVars;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
 *REMARKS.
 *
 *       ALTER/CANCEL FROM INCEPTION PROCESSING.
 *       ---------------------------------------
 *
 * Overview.
 * ---------
 *
 * This program is used to display the details of the Contract
 *  that is going to be Cancelled or Altered from Inception.
 * The Cancel from Inception is irreversible once it has been
 *  performed.
 * The Alter from Inception is used to wind the Contract back to
 *  Proposal stage (Pre-issue), so that the user can change any
 *  details on the proposal before re-issuing, still with the
 *  original Contract number.
 *
 * Initialisation.
 * ---------------
 *
 * Default the Effective date to the contract RCD.
 *
 * Read the Contract header (CHDRLNB) using the RETRV function to get
 *  the relevant data necessary for obtaining the status description
 *  and the agent details.
 *
 * Obtain the necessary descriptions for Contract Type, Contract
 *  Risk Status, Contract Premium Status and Transaction Code (for
 *  the screen title) by calling 'DESCIO' for the respective tables
 *  and then display these descriptions on the screen S5214.
 *
 * Client name
 *
 *     Read the Client Details file to get the client's name
 *
 * Paid-to date
 *
 *     Read the PAYR file to get the Paid-to date
 *          and the Billed-to Date
 *
 * Payer name
 *
 *     Read the Client Role file (CLRF) to get the client number
 *          of the payer, then read the Client details file with
 *          payer client number to get payer name
 *
 * Agent details
 *
 *     In order to read the agent file use the agent details
 *          from the contract header (READR on the AGLFLNB file).
 *
 * Read T6661 using the current transaction code (either the
 *  CFI or AFI one) so that we can get the transaction code held
 *  on the T6661 record which we can pass to the AT module to
 *  tell it what transaction to stop at when it is processing
 *  the PTRNs for the contract.
 *
 * Validation.
 * -----------
 *
 * If the user pressed 'KILL', exit from the program.
 *
 * Updating.
 * ---------
 *
 * If the user pressed 'KILL', don't continue with rest of section
 *  and exit from program.
 *
 * Call 'SFTLOCK' to transfer the contract lock to 'AT'.
 *
 * Call the AT submission module to submit REVGENAT, passing the
 *  contract number as the 'primary key'. This performs the
 *  AFI/CFI transaction.
 *
 * Tables Used.
 * ------------
 *
 * T1688 - Transaction codes
 * T3588 - Contract Premium Status
 * T3623 - Contract Risk Status
 * T5688 - Contract Structure
 * T6661 - Transaction Reversal Parameters
 *
 *****************************************************************
 * </pre>
 */
public class P5214 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5214");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO).setUnsigned();
	protected FixedLengthStringData wsaaItem = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(36);
	protected FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(28).isAPartOf(wsaaPrimaryKey, 8, FILLER).init(SPACES);

	private FixedLengthStringData wsaaPayrKey = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaPayrChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPayrKey, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaPayrKey, 8);

	private FixedLengthStringData wsaaTransactionArea = new FixedLengthStringData(200);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 0);
	private ZonedDecimalData wsaaTranDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 1).setUnsigned();
	private ZonedDecimalData wsaaTranTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 7).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTransactionArea, 13).setUnsigned();
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionArea, 19);
	private FixedLengthStringData wsaaSupflag = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 23);
	private ZonedDecimalData wsaaTodate = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 24).setUnsigned();
	private ZonedDecimalData wsaaSuppressTo = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 32).setUnsigned();
	private ZonedDecimalData wsaaTranNum = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransactionArea, 40).setUnsigned();
	private ZonedDecimalData wsaaPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaTransactionArea, 45).setUnsigned();
	private FixedLengthStringData wsaaCfiafiTranCode = new FixedLengthStringData(4).isAPartOf(wsaaTransactionArea, 53);
	private ZonedDecimalData wsaaCfiafiTranno = new ZonedDecimalData(5, 0).isAPartOf(wsaaTransactionArea, 57).setUnsigned();
	private ZonedDecimalData wsaaBbldat = new ZonedDecimalData(8, 0).isAPartOf(wsaaTransactionArea, 62).setUnsigned();
	private FixedLengthStringData wsaaFlag = new FixedLengthStringData(1).isAPartOf(wsaaTransactionArea, 70);
	private FixedLengthStringData filler1 = new FixedLengthStringData(129).isAPartOf(wsaaTransactionArea, 71, FILLER).init(SPACES);
	private String f910 = "F910";
	/* TABLES */
	private String t1688 = "T1688";
	private String t3588 = "T3588";
	private String t3623 = "T3623";
	private String t5688 = "T5688";
	private String t6661 = "T6661";
	private String clrfrec = "CLRFREC   ";
	private String payrrec = "PAYRREC   ";
	protected FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	/*Joind FSU and Life agent headers - new b*/
	//	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private Atreqrec atreqrec = new Atreqrec();
	/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	/*Client Roles by Foreign Key*/
	//	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private Clrrpf clrfIO = new Clrrpf();
	/*Client logical file with new fields*/
	//	private CltsTableDAM cltsIO = new CltsTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	/*Logical File: SMART table reference data*/
	//	private ItemTableDAM itemIO = new ItemTableDAM();
	/*Payor Details Logical File*/
	//	private PayrTableDAM payrIO = new PayrTableDAM();
	Payrpf payrIO = new Payrpf();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private T6661rec t6661rec = new T6661rec();
	private Batckey wsaaBatckey = new Batckey();
	private S5214ScreenVars sv = getLScreenVars();
	//fwang3
	private Clntpf cltsIO = new Clntpf();
	//	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	//	private Aglfpf aglflnbIO = new Aglfpf();

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO", AglfpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private ClrrpfDAO clrrpfDAO = getApplicationContext().getBean("clrrpfDAO", ClrrpfDAO.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	
	boolean ctcnl002Permission = false; // ICIL-93

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		lgnmExit, 
		plainExit, 
		payeeExit, 
		preExit, 
		exit2090, 
		exit3090
	}

	public P5214() {
		super();
		screenVars = sv;
		new ScreenModel("S5214", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

	protected void lgnm100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			//			goTo(GotoLabel.lgnmExit);
			throw new GOTOException(GotoLabel.lgnmExit);
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
	}

	protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

	protected void plain100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			//			goTo(GotoLabel.plainExit);
			throw new GOTOException(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
	}

	protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

	protected void payee100()
	{
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			//			goTo(GotoLabel.payeeExit);
			throw new GOTOException(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspcomn.longconfname.setLeft(stringVariable1.toString());
			//			goTo(GotoLabel.payeeExit);
			throw new GOTOException(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable2.toString());
	}

	protected void corpname()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspcomn.longconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

	protected void initialise1000()
	{
		start1000();
	}

	protected void start1000()
	{
		// =========set today==========
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		wsaaToday.set(datcon1rec.intDate);
		ctcnl002Permission  = FeaConfg.isFeatureExist("2", "CTCNL002", appVars, "IT");     // ICIL-93
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
		cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
				if(!cntDteFlag) {
					sv.occdateOut[varcom.nd.toInt()].set("Y");
					}
		//ILJ-49 End
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaItem.set(SPACES);
		sv.effdate.set(varcom.vrcmMaxDate);
		sv.occdate.set(varcom.vrcmMaxDate);
		sv.ptdate.set(varcom.vrcmMaxDate);
		datcon1rec.datcon1Rec.set(SPACES);
		datcon1rec.intDate.set(ZERO);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setFunction("RETRV");
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		sv.cownnum.set(chdrlnbIO.getCownnum());
		sv.payrnum.set(chdrlnbIO.getPayrnum());
		sv.agntnum.set(chdrlnbIO.getAgntnum());
		sv.occdate.set(chdrlnbIO.getOccdate());
		
		sv.effdate.set(wsaaToday);
		if(ctcnl002Permission){
			if(!isEQ(wsspcomn.occdate,varcom.vrcmMaxDate)){
				
				sv.effdate.set(wsspcomn.occdate);
			}	
		}
		sv.effdate.set(chdrlnbIO.getOccdate());
		//JOptionPane.showMessageDialog(null, wsspcomn.occdate, "alert", JOptionPane.ERROR_MESSAGE); 
		//sv.effdate.set(wsspcomn.occdate);
		
		clientDetails1100();
		agentDetails1200();
		payerDetails1300();
		tableDetails1400();
		readT66611500();
	}

	protected void clientDetails1100()
	{
		/*START*/
		//		cltsIO.setParams(SPACES);
		//		cltsIO.setClntnum(chdrlnbIO.getCownnum());
		//		cltsIO.setClntcoy(wsspcomn.fsuco);
		//		cltsIO.setClntpfx("CN");
		//		clientIo1600();
		//fwang3
		//		cltsIO = clntpfDAO.searchClntRecord("CN", wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
		//		if (null == cltsIO) {
		//			wsspcomn.longconfname.set(SPACES);
		//		} else {
		//			plainname();
		//		}
		this.clientIo1600("CN", wsspcomn.fsuco.toString(), chdrlnbIO.getCownnum().toString());
		sv.ownername.set(wsspcomn.longconfname);
		/*EXIT*/
	}

	private void clientIo1600(String pfx, String clntcoy, String clntnum) {
		//		cltsIO.setFunction(varcom.readr);
		//		SmartFileCode.execute(appVars, cltsIO);
		//		if (isNE(cltsIO.getStatuz(),varcom.oK)
		//		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
		//			syserrrec.params.set(cltsIO.getParams());
		//			fatalError600();
		//		}
		//		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
		//			wsspcomn.longconfname.set(SPACES);
		//		}
		//		else {
		//			plainname();
		//		}

		cltsIO = clntpfDAO.searchClntRecord(pfx, clntcoy, clntnum);
		if (null == cltsIO) {
			wsspcomn.longconfname.set(SPACES);
		} else {
			plainname();
		}
	}

	protected void agentDetails1200()
	{
		start1200();
	}

	protected void start1200()
	{
		//aglflnbIO.setParams(SPACES);
		//		aglflnbIO.setAgntcoy(wsspcomn.company);
		//		aglflnbIO.setAgntnum(chdrlnbIO.getAgntnum());
		//		aglflnbIO.setFunction(varcom.readr);
		//		SmartFileCode.execute(appVars, aglflnbIO);
		//		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		//		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
		//			syserrrec.params.set(aglflnbIO.getParams());
		//			syserrrec.statuz.set(aglflnbIO.getStatuz());
		//			fatalError600();
		//		}
		Aglfpf aglflnbIO = this.aglfpfDAO.searchAglflnb(wsspcomn.company.toString(), chdrlnbIO.getAgntnum().toString());
		if (null == aglflnbIO) {
			sv.agentname.set(SPACES);
		} else {
			this.clientIo1600("CN", wsspcomn.fsuco.toString(), aglflnbIO.getClntnum());
			sv.agentname.set(wsspcomn.longconfname);
		}
	}

	protected void payerDetails1300()
	{
		start1300();
	}

	protected void start1300()
	{
		//		payrIO.setParams(SPACES);
		//		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		//		payrIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		//		payrIO.setPayrseqno(ZERO);
		//		payrIO.setFunction(varcom.begn);
		//		//performance improvement -- Anjali
		//		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		//		payrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		//		payrIO.setFormat(payrrec);
		//		SmartFileCode.execute(appVars, payrIO);
		//		if (isNE(payrIO.getStatuz(),varcom.oK)
		//		&& isNE(payrIO.getStatuz(),varcom.endp)) {
		//			syserrrec.params.set(payrIO.getParams());
		//			syserrrec.statuz.set(payrIO.getStatuz());
		//			fatalError600();
		//		}
		//		if (isNE(chdrlnbIO.getChdrnum(),payrIO.getChdrnum())
		//		|| isNE(chdrlnbIO.getChdrcoy(),payrIO.getChdrcoy())
		//		|| isEQ(payrIO.getStatuz(),varcom.endp)) {
		//			syserrrec.params.set(payrIO.getParams());
		//			syserrrec.statuz.set(payrIO.getStatuz());
		//			fatalError600();
		//		}
		payrIO = payrpfDAO.getpayrRecord(chdrlnbIO.getChdrcoy().toString(), chdrlnbIO.getChdrnum().toString());
		sv.billfreq.set(payrIO.getBillfreq());
		sv.mop.set(payrIO.getBillchnl());
		sv.btdate.set(payrIO.getBtdate());
		sv.ptdate.set(payrIO.getPtdate());
		//		clrfIO.setParams(SPACES);
		//		clrfIO.setForepfx(chdrlnbIO.getChdrpfx());
		//		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaPayrChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		//		clrfIO.setForenum(wsaaPayrKey);
		//		clrfIO.setClrrrole("PY");
		//		clrfIO.setFunction(varcom.readr);
		//		clrfIO.setFormat(clrfrec);
		//		SmartFileCode.execute(appVars, clrfIO);
		//		if (isNE(clrfIO.getStatuz(),varcom.oK)) {
		//			syserrrec.params.set(clrfIO.getParams());
		//			syserrrec.statuz.set(clrfIO.getStatuz());
		//			fatalError600();
		//		}
		List<Clrrpf> list = clrrpfDAO.searchClrrRecord("PY", chdrlnbIO.getChdrpfx().toString(), payrIO.getChdrcoy(), wsaaPayrKey.toString());/* IJTI-1523 */
		if (list.isEmpty()) {
			fatalError600();
		}
		Clrrpf clrfIO = list.get(0);
		//		cltsIO.setParams(SPACES);
		//		cltsIO.setClntnum(clrfIO.getClntnum());
		//		cltsIO.setClntcoy(clrfIO.getClntcoy());
		//		cltsIO.setClntpfx(clrfIO.getClntpfx());
		//		clientIo1600();
		this.clientIo1600(clrfIO.getClntpfx(), clrfIO.getClntcoy(),	clrfIO.getClntnum());
		sv.payrnum.set(clrfIO.getClntnum());
		sv.payorname.set(wsspcomn.longconfname);
	}

	protected void tableDetails1400()
	{
		start1400();
	}

	protected void start1400()
	{
		descIO.setParams(SPACES);
		descIO.setDesctabl(t5688);
		wsaaItem.set(chdrlnbIO.getCnttype());
		getDescriptions1700();
		sv.ctypdesc.set(descIO.getLongdesc());
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3623);
		wsaaItem.set(chdrlnbIO.getStatcode());
		getDescriptions1700();
		sv.rstate.set(descIO.getShortdesc());
		descIO.setParams(SPACES);
		descIO.setDesctabl(t3588);
		wsaaItem.set(chdrlnbIO.getPstatcode());
		getDescriptions1700();
		sv.pstate.set(descIO.getShortdesc());
		descIO.setParams(SPACES);
		descIO.setDesctabl(t1688);
		wsaaItem.set(wsaaBatckey.batcBatctrcde);
		getDescriptions1700();
		sv.descrip.set(descIO.getLongdesc());
	}

	protected void readT66611500()
	{
		start1500();
	}

	protected void start1500()
	{
		//		itemIO.setParams(SPACES);
		//		itemIO.setItempfx("IT");
		//		itemIO.setItemcoy(wsspcomn.company);	
		//		itemIO.setItemtabl(t6661);
		//		itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		//		itemIO.setFunction(varcom.readr);
		//		SmartFileCode.execute(appVars, itemIO);
		//		if (isNE(itemIO.getStatuz(),varcom.oK)) {
		//			itemIO.setItemitem(wsaaBatckey.batcBatctrcde);
		//			syserrrec.params.set(itemIO.getParams());
		//			syserrrec.statuz.set(itemIO.getStatuz());
		//			fatalError600();
		//		}
		List<Itempf> items = itemDAO.getAllItemitem("IT", wsspcomn.company.toString(), t6661, wsaaBatckey.batcBatctrcde.toString());
		if (items.isEmpty()) {
			syserrrec.params.set(t6661);
			fatalError600();
		}
		t6661rec.t6661Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
	}

	//protected void clientIo1600()
	//	{
	//		/*START*/
	//		cltsIO.setFunction(varcom.readr);
	//		SmartFileCode.execute(appVars, cltsIO);
	//		if (isNE(cltsIO.getStatuz(),varcom.oK)
	//		&& isNE(cltsIO.getStatuz(),varcom.mrnf)) {
	//			syserrrec.params.set(cltsIO.getParams());
	//			fatalError600();
	//		}
	//		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
	//			wsspcomn.longconfname.set(SPACES);
	//		}
	//		else {
	//			plainname();
	//		}
	//		/*EXIT*/
	//	}

	protected void getDescriptions1700()
	{
		start1700();
	}

	protected void start1700()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(wsaaItem);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
				&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setDescitem(wsaaItem);
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc(SPACES);
			descIO.setShortdesc(SPACES);
		}
	}

	protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

	protected void preStart()
	{
		//goTo(GotoLabel.preExit);
		throw new GOTOException(GotoLabel.preExit); 
	}

	protected void screenEdit2000()
	{
		try {
			start2000();
		}
		catch (GOTOException e){
		}
	}

	protected void start2000()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			//goTo(GotoLabel.exit2090);
			throw new GOTOException(GotoLabel.exit2090); 
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	protected void update3000()
	{
		try {
			start3000();
		}
		catch (GOTOException e){
		}
	}

	protected void start3000()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			//goTo(GotoLabel.exit3090);
			throw new GOTOException(GotoLabel.exit3090); 
		}
		sftlockrec.function.set("TOAT");
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz,varcom.oK)
				&& isNE(sftlockrec.statuz,"LOCK")) {
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz,"LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			//			goTo(GotoLabel.exit3090);
			throw new GOTOException(GotoLabel.exit3090); 
		}
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("REVGENAT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(datcon1rec.intDate);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		wsaaChdrnum.set(chdrlnbIO.getChdrnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTodate.set(sv.effdate);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTranDate.set(varcom.vrcmDate);
		wsaaTranTime.set(varcom.vrcmTime);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaSupflag.set("N");
		wsaaSuppressTo.set(varcom.vrcmMaxDate);
		wsaaPlnsfx.set(ZERO);
		wsaaBbldat.set(ZERO);
		wsaaTranNum.set(1);
		wsaaCfiafiTranCode.set(t6661rec.trcode);
		wsaaCfiafiTranno.set(1);
		wsaaFlag.set(wsspcomn.flag);
		atreqrec.transArea.set(wsaaTransactionArea);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz,varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

	protected void whereNext4000()
	{
		/*START*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
	

	protected S5214ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(S5214ScreenVars.class);
	}
	
	
}
