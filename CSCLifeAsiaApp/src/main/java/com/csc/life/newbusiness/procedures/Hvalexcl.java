/*
 * File: Hvalexcl.java
 * Date: 29 August 2009 22:55:42
 * Author: Quipoz Limited
 *
 * Class transformed from HVALEXCL.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.recordstructures.Hvalletrec;
import com.csc.life.newbusiness.dataaccess.FlupTableDAM;
import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
* HVALEXCL- Validation for Exclusion Clause Letter Subroutine.
*  ************************************
*
*  
*****************************************************************
*                                                                     *
* </pre>
*/
public class Hvalexcl extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("HVALEXCL");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
		/* FORMATS */
	private static final String fluprec = "FLUPREC";
	private static final String hxclrec = "HXCLREC";
	private BsscTableDAM bsscIO = new BsscTableDAM();
	private FlupTableDAM flupIO = new FlupTableDAM();
	private HxclTableDAM hxclIO = new HxclTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Hvalletrec hvalletrec = new Hvalletrec();

	public Hvalexcl() {
		super();
	}

public void mainline(Object... parmArray)
	{
		hvalletrec.params = convertAndSetParam(hvalletrec.params, parmArray, 0);
		try {
			main010();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void main010()
	{
		/*MAIN*/
		initialise1000();
		getExclusion2000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*INIT*/
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}

protected void getExclusion2000()
	{
					start2010();
					readFlup2020();
				}

protected void start2010()
	{
		flupIO.setRecKeyData(SPACES);
		flupIO.setChdrcoy(hvalletrec.hvalChdrcoy);
		flupIO.setChdrnum(hvalletrec.hvalChdrnum);
		flupIO.setFupno(ZERO);
		flupIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		flupIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		flupIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		flupIO.setFormat(fluprec);
	}

protected void readFlup2020()
	{
		SmartFileCode.execute(appVars, flupIO);
		flupIO.setFunction(varcom.nextr);
		if (isNE(flupIO.getStatuz(),varcom.oK)
		&& isNE(flupIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(flupIO.getStatuz());
			syserrrec.params.set(flupIO.getParams());
			dbError580();
		}
		if (isNE(flupIO.getChdrcoy(),hvalletrec.hvalChdrcoy)
		|| isNE(flupIO.getChdrnum(),hvalletrec.hvalChdrnum)
		|| isEQ(flupIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isEQ(flupIO.getFupcode(),"USL")) {
			checkHxcl3000();
		}
		readFlup2020();
		return ;
	}

protected void checkHxcl3000()
	{
			start3010();
			readHxcl3020();
		}

protected void start3010()
	{
		hxclIO.setRecKeyData(SPACES);
		hxclIO.setChdrcoy(flupIO.getChdrcoy());
		hxclIO.setChdrnum(flupIO.getChdrnum());
		hxclIO.setFupno(flupIO.getFupno());
		hxclIO.setHxclseqno(ZERO);
		hxclIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hxclIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hxclIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "FUPNO");
		hxclIO.setFormat(hxclrec);
	}

protected void readHxcl3020()
	{
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(),varcom.oK)
		&& isNE(hxclIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hxclIO.getStatuz());
			syserrrec.params.set(hxclIO.getParams());
			dbError580();
		}
		if (isNE(hxclIO.getChdrcoy(),flupIO.getChdrcoy())
		|| isNE(hxclIO.getChdrnum(),flupIO.getChdrnum())
		|| isNE(hxclIO.getFupno(),flupIO.getFupno())
		|| isEQ(hxclIO.getStatuz(),varcom.endp)) {
			return ;
		}
		if (isEQ(hxclIO.getStatuz(),varcom.oK)) {
			hvalletrec.hvalStatuz.set(varcom.oK);
		}
		else {
			hvalletrec.hvalStatuz.set(varcom.mrnf);
		}
		/*EXIT*/
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.syserrType.set("1");
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.subrname.set(wsaaProg);
		callProgram(Syserr.class, syserrrec.params);
		/*EXIT*/
		hvalletrec.hvalStatuz.set(varcom.bomb);
		exitProgram();
	}

protected void systemError590()
	{
		/*START*/
		syserrrec.syserrType.set("2");
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.subrname.set(wsaaProg);
		callProgram(Syserr.class, syserrrec.params);
		/*EXIT*/
		hvalletrec.hvalStatuz.set(varcom.bomb);
		exitProgram();
	}
}
