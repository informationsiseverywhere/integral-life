package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class St500screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		St500ScreenVars sv = (St500ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.St500screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		St500ScreenVars screenVars = (St500ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.acctmonth.setClassString("");
		screenVars.jobq.setClassString("");
		screenVars.acctyear.setClassString("");
		screenVars.scheduleName.setClassString("");
		screenVars.scheduleNumber.setClassString("");
		screenVars.bbranch.setClassString("");
		screenVars.bcompany.setClassString("");
		screenVars.datefromDisp.setClassString("");
		screenVars.datetoDisp.setClassString("");
	}

/**
 * Clear all the variables in St500screen
 */
	public static void clear(VarModel pv) {
		St500ScreenVars screenVars = (St500ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.acctmonth.clear();
		screenVars.jobq.clear();
		screenVars.acctyear.clear();
		screenVars.scheduleName.clear();
		screenVars.scheduleNumber.clear();
		screenVars.bbranch.clear();
		screenVars.bcompany.clear();
		screenVars.datefromDisp.clear();
		screenVars.datefrom.clear();
		screenVars.datetoDisp.clear();
		screenVars.dateto.clear();
	}
}
