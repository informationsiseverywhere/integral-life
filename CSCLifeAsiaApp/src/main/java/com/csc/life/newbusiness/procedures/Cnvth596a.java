/*
 * File: Cnvth596a.java
 * Date: 29 August 2009 22:42:06
 * Author: Quipoz Limited
 *
 * Class transformed from CNVTH596A.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.dataaccess.ItempfTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Delete the existing records in ITEMPF with ITEM TABLE = TH596.
*   Copy the backup data of TH596 into ITEMPF.
*
*   Logic :- Loop TH596 and delete the record.
*            Loop TH596OLD (back-up data) and write to ITEMPF.
*
***********************************************************************
* </pre>
*/
public class Cnvth596a extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private DiskFileDAM itemold = new DiskFileDAM("TH596OLD");
	private ItempfTableDAM itemoldRec = new ItempfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("TH596");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/* FORMATS */
	private String itemrec1 = "ITEMREC";
		/* TABLES */
	private String th596 = "TH596";
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		beginDel2000,
		exit2000,
		readItemold3020,
		checkError3030
	}

	public Cnvth596a() {
		super();
	}

public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void control1000()
	{
		/*START*/
		wsspEdterror.set(varcom.oK);
		initialize1100();
		deleteExistingTh5962000();
		if (isEQ(wsspEdterror,varcom.oK)
		|| isEQ(wsspEdterror,varcom.endp)) {
			copyOldTh5963000();
		}
		/*EXIT*/
		stopRun();
	}

protected void initialize1100()
	{
		/*BEGIN*/
		itemIO.setRecKeyData(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(th596);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMPFX", "ITEMCOY", "ITEMTABL");
		/*EXIT*/
	}

protected void deleteExistingTh5962000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case beginDel2000: {
					beginDel2000();
				}
				case exit2000: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void beginDel2000()
	{
		callItemio2200();
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItempfx(),"IT")
		|| isNE(itemIO.getItemcoy(),lsaaCompany)
		|| isNE(itemIO.getItemtabl(),th596)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2000);
		}
		itemIO.setFunction(varcom.deltd);
		callItemio2200();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2000);
		}
		itemIO.setFunction(varcom.nextr);
		goTo(GotoLabel.beginDel2000);
	}

protected void copyOldTh5963000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					copy3010();
				}
				case readItemold3020: {
					readItemold3020();
				}
				case checkError3030: {
					checkError3030();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void copy3010()
	{
		wsspEdterror.set(varcom.oK);
		itemold.openInput();
	}

protected void readItemold3020()
	{
		itemold.readNext(itemoldRec);
		if (itemold.isAtEnd()) {
			goTo(GotoLabel.checkError3030);
		}
		if (isNE(itemoldRec.itempfx,"IT")) {
			goTo(GotoLabel.readItemold3020);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx(itemoldRec.itempfx);
		itemIO.setItemcoy(itemoldRec.itemcoy);
		itemIO.setItemtabl(itemoldRec.itemtabl);
		itemIO.setItemitem(itemoldRec.itemitem);
		itemIO.setItemseq(itemoldRec.itemseq);
		itemIO.setTranid(itemoldRec.tranid);
		itemIO.setTableprog(itemoldRec.tableprog);
		itemIO.setValidflag(itemoldRec.validflag);
		itemIO.setItmfrm(itemoldRec.itmfrm);
		itemIO.setItmto(itemoldRec.itmto);
		itemIO.setGenarea(itemoldRec.genarea);
		itemIO.setUserProfile(itemoldRec.userProfile);
		itemIO.setJobName(itemoldRec.jobName);
		itemIO.setDatime(itemoldRec.datime);
		itemIO.setFormat(itemrec1);
		itemIO.setFunction(varcom.writr);
		callItemio2200();
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			wsspEdterror.set(itemIO.getStatuz());
			goTo(GotoLabel.checkError3030);
		}
		goTo(GotoLabel.readItemold3020);
	}

protected void checkError3030()
	{
		if (isEQ(wsspEdterror,varcom.oK)) {
			lsaaStatuz.set(varcom.oK);
			appVars.commit();
		}
		else {
			lsaaStatuz.set(SPACES);
		}
		itemold.close();
		/*EXIT*/
	}

protected void callItemio2200()
	{
		/*START*/
		itemIO.setFormat(itemrec1);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError9000();
		}
		/*EXIT*/
	}

protected void fatalError9000()
	{
		/*START*/
		rollback();
		lsaaStatuz.set("BOMB");
		syserrrec.syserrType.set("1");
		syserrrec.subrname.set(wsaaProg);
		callProgram(Syserr.class, syserrrec.syserrRec);
		/*EXIT*/
		stopRun();
	}
}
