/*
 * File: P5608.java
 * Date: 30 August 2009 0:31:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P5608.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.procedures.T5608pt;
import com.csc.life.newbusiness.screens.S5608ScreenVars;
import com.csc.life.newbusiness.tablestructures.T5608rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItmdTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
* REPLACE BY TABLE DESCRIPTION.
*
*
*****************************************************************
* </pre>
*/
public class P5608 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5608");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaUpdateFlag = "N";
	private FixedLengthStringData wsaaTablistrec = new FixedLengthStringData(575);
	private DescTableDAM descIO = new DescTableDAM();
	private ItmdTableDAM itmdIO = new ItmdTableDAM();
	protected T5608rec t5608rec = getT5608rec() ;//new T5608rec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5608ScreenVars sv = getPScreenVars();//ScreenProgram.getScreenVars( S5608ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		generalArea1045, 
		other3080, 
		exit3090
	}

	public P5608() {
		super();
		screenVars = sv;
		new ScreenModel("S5608", AppVars.getInstance(), sv);
	}

	protected S5608ScreenVars getPScreenVars() {
		return ScreenProgram.getScreenVars( S5608ScreenVars.class);
	}
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initialise1010();
					readRecord1031();
					moveToScreen1040();
				case generalArea1045: 
					generalArea1045();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initialise1010()
	{
		/*INITIALISE-SCREEN*/
		sv.dataArea.set(SPACES);
		/*READ-PRIMARY-RECORD*/
		/*READ-RECORD*/
		itmdIO.setParams(SPACES);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		itmdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		/*READ-SECONDARY-RECORDS*/
	}

protected void readRecord1031()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(itmdIO.getItemItempfx());
		descIO.setDesccoy(itmdIO.getItemItemcoy());
		descIO.setDesctabl(itmdIO.getItemItemtabl());
		descIO.setDescitem(itmdIO.getItemItemitem());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void moveToScreen1040()
	{
		sv.company.set(itmdIO.getItemItemcoy());
		sv.tabl.set(itmdIO.getItemItemtabl());
		sv.item.set(itmdIO.getItemItemitem());
		sv.itmfrm.set(itmdIO.getItemItmfrm());
		sv.itmto.set(itmdIO.getItemItmto());
		sv.longdesc.set(descIO.getLongdesc());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.longdesc.set(SPACES);
		}
		t5608rec.t5608Rec.set(itmdIO.getItemGenarea());
		/*    IF THE EXTRA DATA AREA WAS NOT USED BEFORE THE NUMERIC*/
		/*    FIELDS MUST BE SET TO ZERO TO AVOID DATA EXCEPTIONS.*/
		if (isNE(itmdIO.getItemGenarea(), SPACES)) {
			goTo(GotoLabel.generalArea1045);
		}
		t5608rec.ageIssageFrm01.set(ZERO);
		t5608rec.ageIssageFrm02.set(ZERO);
		t5608rec.ageIssageFrm03.set(ZERO);
		t5608rec.ageIssageFrm04.set(ZERO);
		t5608rec.ageIssageFrm05.set(ZERO);
		t5608rec.ageIssageFrm06.set(ZERO);
		t5608rec.ageIssageFrm07.set(ZERO);
		t5608rec.ageIssageFrm08.set(ZERO);
		t5608rec.ageIssageTo01.set(ZERO);
		t5608rec.ageIssageTo02.set(ZERO);
		t5608rec.ageIssageTo03.set(ZERO);
		t5608rec.ageIssageTo04.set(ZERO);
		t5608rec.ageIssageTo05.set(ZERO);
		t5608rec.ageIssageTo06.set(ZERO);
		t5608rec.ageIssageTo07.set(ZERO);
		t5608rec.ageIssageTo08.set(ZERO);
		t5608rec.premCessageFrom01.set(ZERO);
		t5608rec.premCessageFrom02.set(ZERO);
		t5608rec.premCessageFrom03.set(ZERO);
		t5608rec.premCessageFrom04.set(ZERO);
		t5608rec.premCessageFrom05.set(ZERO);
		t5608rec.premCessageFrom06.set(ZERO);
		t5608rec.premCessageFrom07.set(ZERO);
		t5608rec.premCessageFrom08.set(ZERO);
		t5608rec.premCessageTo01.set(ZERO);
		t5608rec.premCessageTo02.set(ZERO);
		t5608rec.premCessageTo03.set(ZERO);
		t5608rec.premCessageTo04.set(ZERO);
		t5608rec.premCessageTo05.set(ZERO);
		t5608rec.premCessageTo06.set(ZERO);
		t5608rec.premCessageTo07.set(ZERO);
		t5608rec.premCessageTo08.set(ZERO);
		t5608rec.premCesstermFrom01.set(ZERO);
		t5608rec.premCesstermFrom02.set(ZERO);
		t5608rec.premCesstermFrom03.set(ZERO);
		t5608rec.premCesstermFrom04.set(ZERO);
		t5608rec.premCesstermFrom05.set(ZERO);
		t5608rec.premCesstermFrom06.set(ZERO);
		t5608rec.premCesstermFrom07.set(ZERO);
		t5608rec.premCesstermFrom08.set(ZERO);
		t5608rec.premCesstermTo01.set(ZERO);
		t5608rec.premCesstermTo02.set(ZERO);
		t5608rec.premCesstermTo03.set(ZERO);
		t5608rec.premCesstermTo04.set(ZERO);
		t5608rec.premCesstermTo05.set(ZERO);
		t5608rec.premCesstermTo06.set(ZERO);
		t5608rec.premCesstermTo07.set(ZERO);
		t5608rec.premCesstermTo08.set(ZERO);
		t5608rec.riskCessageFrom01.set(ZERO);
		t5608rec.riskCessageFrom02.set(ZERO);
		t5608rec.riskCessageFrom03.set(ZERO);
		t5608rec.riskCessageFrom04.set(ZERO);
		t5608rec.riskCessageFrom05.set(ZERO);
		t5608rec.riskCessageFrom06.set(ZERO);
		t5608rec.riskCessageFrom07.set(ZERO);
		t5608rec.riskCessageFrom08.set(ZERO);
		t5608rec.riskCessageTo01.set(ZERO);
		t5608rec.riskCessageTo02.set(ZERO);
		t5608rec.riskCessageTo03.set(ZERO);
		t5608rec.riskCessageTo04.set(ZERO);
		t5608rec.riskCessageTo05.set(ZERO);
		t5608rec.riskCessageTo06.set(ZERO);
		t5608rec.riskCessageTo07.set(ZERO);
		t5608rec.riskCessageTo08.set(ZERO);
		t5608rec.riskCesstermFrom01.set(ZERO);
		t5608rec.riskCesstermFrom02.set(ZERO);
		t5608rec.riskCesstermFrom03.set(ZERO);
		t5608rec.riskCesstermFrom04.set(ZERO);
		t5608rec.riskCesstermFrom05.set(ZERO);
		t5608rec.riskCesstermFrom06.set(ZERO);
		t5608rec.riskCesstermFrom07.set(ZERO);
		t5608rec.riskCesstermFrom08.set(ZERO);
		t5608rec.riskCesstermTo01.set(ZERO);
		t5608rec.riskCesstermTo02.set(ZERO);
		t5608rec.riskCesstermTo03.set(ZERO);
		t5608rec.riskCesstermTo04.set(ZERO);
		t5608rec.riskCesstermTo05.set(ZERO);
		t5608rec.riskCesstermTo06.set(ZERO);
		t5608rec.riskCesstermTo07.set(ZERO);
		t5608rec.riskCesstermTo08.set(ZERO);
		t5608rec.sumInsMax.set(ZERO);
		t5608rec.sumInsMin.set(ZERO);
		t5608rec.termIssageFrm01.set(ZERO);
		t5608rec.termIssageFrm02.set(ZERO);
		t5608rec.termIssageFrm03.set(ZERO);
		t5608rec.termIssageFrm04.set(ZERO);
		t5608rec.termIssageFrm05.set(ZERO);
		t5608rec.termIssageFrm06.set(ZERO);
		t5608rec.termIssageFrm07.set(ZERO);
		t5608rec.termIssageFrm08.set(ZERO);
		t5608rec.termIssageTo01.set(ZERO);
		t5608rec.termIssageTo02.set(ZERO);
		t5608rec.termIssageTo03.set(ZERO);
		t5608rec.termIssageTo04.set(ZERO);
		t5608rec.termIssageTo05.set(ZERO);
		t5608rec.termIssageTo06.set(ZERO);
		t5608rec.termIssageTo07.set(ZERO);
		t5608rec.termIssageTo08.set(ZERO);
		t5608rec.prmbasis01.set(SPACES);
		t5608rec.prmbasis02.set(SPACES);
		t5608rec.prmbasis03.set(SPACES);
		t5608rec.prmbasis04.set(SPACES);
		t5608rec.prmbasis05.set(SPACES);
		t5608rec.prmbasis06.set(SPACES);
	}

protected void generalArea1045()
	{
		sv.ageIssageFrms.set(t5608rec.ageIssageFrms);
		sv.ageIssageTos.set(t5608rec.ageIssageTos);
		sv.eaage.set(t5608rec.eaage);
		if (isEQ(itmdIO.getItemItmfrm(), 0)) {
			sv.itmfrm.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmfrm.set(itmdIO.getItemItmfrm());
		}
		if (isEQ(itmdIO.getItemItmto(), 0)) {
			sv.itmto.set(varcom.vrcmMaxDate);
		}
		else {
			sv.itmto.set(itmdIO.getItemItmto());
		}
		sv.liencds.set(t5608rec.liencds);
		sv.mortclss.set(t5608rec.mortclss);
		sv.premCessageFroms.set(t5608rec.premCessageFroms);
		sv.premCessageTos.set(t5608rec.premCessageTos);
		sv.premCesstermFroms.set(t5608rec.premCesstermFroms);
		sv.premCesstermTos.set(t5608rec.premCesstermTos);
		sv.riskCessageFroms.set(t5608rec.riskCessageFroms);
		sv.riskCessageTos.set(t5608rec.riskCessageTos);
		sv.riskCesstermFroms.set(t5608rec.riskCesstermFroms);
		sv.riskCesstermTos.set(t5608rec.riskCesstermTos);
		sv.specind.set(t5608rec.specind);
		sv.sumInsMax.set(t5608rec.sumInsMax);
		sv.sumInsMin.set(t5608rec.sumInsMin);
		sv.termIssageFrms.set(t5608rec.termIssageFrms);
		sv.termIssageTos.set(t5608rec.termIssageTos);
		sv.prmbasiss.set(t5608rec.prmbasiss);
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		screenIo2010();
		exit2090();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		/*OTHER*/
	}

protected void exit2090()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					preparation3010();
					updatePrimaryRecord3050();
					updateRecord3055();
				case other3080: 
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void preparation3010()
	{
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit3090);
		}
		/*CHECK-CHANGES*/
		wsaaUpdateFlag = "N";
		if (isEQ(wsspcomn.flag, "C")) {
			wsaaUpdateFlag = "Y";
		}
		checkChanges3100();
		if (isNE(wsaaUpdateFlag, "Y")) {
			goTo(GotoLabel.other3080);
		}
	}

protected void updatePrimaryRecord3050()
	{
		itmdIO.setFunction(varcom.readh);
		itmdIO.setDataKey(wsspsmart.itmdkey);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		itmdIO.setItemTranid(varcom.vrcmCompTranid);
	}

protected void updateRecord3055()
	{
		itmdIO.setItemTableprog(wsaaProg);
		itmdIO.setItemGenarea(t5608rec.t5608Rec);
		itmdIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, itmdIO);
		if (isNE(itmdIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itmdIO.getParams());
			fatalError600();
		}
	}

protected void checkChanges3100()
	{
		check3100();
	}

protected void check3100()
	{
		if (isNE(sv.ageIssageFrms, t5608rec.ageIssageFrms)) {
			t5608rec.ageIssageFrms.set(sv.ageIssageFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.ageIssageTos, t5608rec.ageIssageTos)) {
			t5608rec.ageIssageTos.set(sv.ageIssageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.eaage, t5608rec.eaage)) {
			t5608rec.eaage.set(sv.eaage);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmfrm, itmdIO.getItemItmfrm())) {
			itmdIO.setItemItmfrm(sv.itmfrm);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.itmto, itmdIO.getItemItmto())) {
			itmdIO.setItemItmto(sv.itmto);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.liencds, t5608rec.liencds)) {
			t5608rec.liencds.set(sv.liencds);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.mortclss, t5608rec.mortclss)) {
			t5608rec.mortclss.set(sv.mortclss);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCessageFroms, t5608rec.premCessageFroms)) {
			t5608rec.premCessageFroms.set(sv.premCessageFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCessageTos, t5608rec.premCessageTos)) {
			t5608rec.premCessageTos.set(sv.premCessageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCesstermFroms, t5608rec.premCesstermFroms)) {
			t5608rec.premCesstermFroms.set(sv.premCesstermFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.premCesstermTos, t5608rec.premCesstermTos)) {
			t5608rec.premCesstermTos.set(sv.premCesstermTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCessageFroms, t5608rec.riskCessageFroms)) {
			t5608rec.riskCessageFroms.set(sv.riskCessageFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCessageTos, t5608rec.riskCessageTos)) {
			t5608rec.riskCessageTos.set(sv.riskCessageTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCesstermFroms, t5608rec.riskCesstermFroms)) {
			t5608rec.riskCesstermFroms.set(sv.riskCesstermFroms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.riskCesstermTos, t5608rec.riskCesstermTos)) {
			t5608rec.riskCesstermTos.set(sv.riskCesstermTos);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.specind, t5608rec.specind)) {
			t5608rec.specind.set(sv.specind);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sumInsMax, t5608rec.sumInsMax)) {
			t5608rec.sumInsMax.set(sv.sumInsMax);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.sumInsMin, t5608rec.sumInsMin)) {
			t5608rec.sumInsMin.set(sv.sumInsMin);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.termIssageFrms, t5608rec.termIssageFrms)) {
			t5608rec.termIssageFrms.set(sv.termIssageFrms);
			wsaaUpdateFlag = "Y";
		}
		if (isNE(sv.termIssageTos, t5608rec.termIssageTos)) {
			t5608rec.termIssageTos.set(sv.termIssageTos);
			wsaaUpdateFlag = "Y";
		}
		
		if (isNE(sv.prmbasiss, t5608rec.prmbasiss)) {
			t5608rec.prmbasiss.set(sv.prmbasiss);
			wsaaUpdateFlag = "Y";
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

	/**
	* <pre>
	*     DUMMY CALL TO GENERATED PRINT PROGRAM TO ENSURE THAT
	*      IT IS TRANSFERED, TA/TR, ALONG WITH REST OF SUITE.
	* </pre>
	*/
protected void callPrintProgram5000()
	{
		/*START*/
		callProgram(T5608pt.class, wsaaTablistrec);
		/*EXIT*/
	}

	protected T5608rec getT5608rec() {
		return new T5608rec();
	}

	public void setT5608rec(T5608rec t5608rec) {
		this.t5608rec = t5608rec;
	}

	public String getWsaaUpdateFlag() {
		return wsaaUpdateFlag;
	}

	public void setWsaaUpdateFlag(String wsaaUpdateFlag) {
		this.wsaaUpdateFlag = wsaaUpdateFlag;
	}
}
