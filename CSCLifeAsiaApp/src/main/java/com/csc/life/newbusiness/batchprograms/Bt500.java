/*
 * File: Bt500.java
 * Date: 29 August 2009 22:36:54
 * Author: Quipoz Limited
 * 
 * Class transformed from BT500.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.financials.tablestructures.Tr204rec;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;

import com.csc.fsu.general.recordstructures.Lstatuscpy;
import com.csc.fsu.general.recordstructures.Vflagcpy;
import com.csc.life.newbusiness.dataaccess.HpavTableDAM;
import com.csc.life.newbusiness.dataaccess.HpavpfTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.HpavpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpavpf;
import com.csc.life.newbusiness.recordstructures.Pt500par;
import com.csc.life.newbusiness.reports.Rt500Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB)        <Do not delete>
*
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   Duty Stamp Report.
*
*   The basic procedure division logic is for reading via SQL and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - retrieve and set up standard report headings.
*
*   Read
*     - read first primary file record
*
*   Perform     Until End of File
*
*      Edit
*       - Check if the primary file record is required
*       - Softlock it if the record is to be updated
*
*      Update
*       - update database files
*       - write details to report while not primary file EOF
*       - look up referred to records for output details
*       - if new page, write headings
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
***********************************************************************
* </pre>
*/
public class Bt500 extends Mainb  {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlhpadpf1rs = null;
	private java.sql.PreparedStatement sqlhpadpf1ps = null;
	private java.sql.Connection sqlhpadpf1conn = null;
	private String sqlhpadpf1 = "";
	private Rt500Report printerFile = new Rt500Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BT500");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	
	
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final String descrec = "DESCREC";
	private static final String chdrrec = "CHDRREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String tr204 = "TR204";
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler1, 5);
	
	
	

	private SortFileDAM sortFile = new SortFileDAM("CLRDSRT");
	private HpavpfTableDAM hpavpf = new HpavpfTableDAM();
	private HpavpfTableDAM hpavpfRec = new HpavpfTableDAM();
	private HpavpfTableDAM sortRec = new HpavpfTableDAM();
	private static final String hpavpfrec = "HPAVPFREC";
	

		/* SQL-HPADPF */
	private FixedLengthStringData hpadrec = new FixedLengthStringData(15);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(hpadrec, 0);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(hpadrec, 1);
	private FixedLengthStringData sqlValidflag = new FixedLengthStringData(1).isAPartOf(hpadrec, 9);
	private PackedDecimalData sqlHoissdte = new PackedDecimalData(8, 0).isAPartOf(hpadrec, 10); 
	
	
	
	
	

		/*  Control indicators*/
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	

	private FixedLengthStringData wsaaFirstHeading = new FixedLengthStringData(1).init("Y");
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private String wsaaChdrFound = "";
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaCownpfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCowncoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCownnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaSinstamt06 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCurrfrom = new PackedDecimalData(8, 0);
	private BinaryData wsaaRrn = new BinaryData(9, 0);
	private ZonedDecimalData wsaaDatefrom = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDateto = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaValid = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaIf = new FixedLengthStringData(2);
	private PackedDecimalData wsaaBrnPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBrnDuty = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBrnCnt = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCoyPrem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCoyDuty = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCoyCnt = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaSduty = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaOwnme = new FixedLengthStringData(47);
	private FixedLengthStringData prevCurr = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaFirstRec = new FixedLengthStringData(1).init("Y");
	private Validator firstRec = new Validator(wsaaFirstRec, "Y");
	
	private HpavTableDAM hpavpfIO = new HpavTableDAM();
	
	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	
	
	
	

		/*   Main, standard page headings*/
	private FixedLengthStringData rt500H01 = new FixedLengthStringData(84);
	private FixedLengthStringData rt500h01O = new FixedLengthStringData(84).isAPartOf(rt500H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rt500h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rt500h01O, 10);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rt500h01O, 11);
	private FixedLengthStringData rh01Repdate = new FixedLengthStringData(10).isAPartOf(rt500h01O, 41);
	private FixedLengthStringData rh01Currcode = new FixedLengthStringData(3).isAPartOf(rt500h01O, 51);
	private FixedLengthStringData rh01Currencynm = new FixedLengthStringData(30).isAPartOf(rt500h01O, 54);

		/*  Detail line - add as many detail and total lines as required.
		              - use redefines to save WS space where applicable.*/
	private FixedLengthStringData rt500D01 = new FixedLengthStringData(87);
	private FixedLengthStringData rt500d01O = new FixedLengthStringData(87).isAPartOf(rt500D01, 0);
	private FixedLengthStringData rd01Chdrno = new FixedLengthStringData(8).isAPartOf(rt500d01O, 0);
	private FixedLengthStringData rd01Linsname = new FixedLengthStringData(47).isAPartOf(rt500d01O, 8);
	private FixedLengthStringData rd01Ccdate = new FixedLengthStringData(10).isAPartOf(rt500d01O, 55);
	private ZonedDecimalData rd01Anetpre = new ZonedDecimalData(13, 2).isAPartOf(rt500d01O, 65);
	private ZonedDecimalData rd01Astpdty = new ZonedDecimalData(9, 2).isAPartOf(rt500d01O, 78);

	private FixedLengthStringData rt500T01 = new FixedLengthStringData(27);
	private FixedLengthStringData rt500t01O = new FixedLengthStringData(27).isAPartOf(rt500T01, 0);
	private ZonedDecimalData rt01Gcnt = new ZonedDecimalData(5, 0).isAPartOf(rt500t01O, 0);
	private ZonedDecimalData rt01Anetpre = new ZonedDecimalData(13, 2).isAPartOf(rt500t01O, 5);
	private ZonedDecimalData rt01Astpdty = new ZonedDecimalData(9, 2).isAPartOf(rt500t01O, 18);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Pt500par pt500par = new Pt500par();
	private Lstatuscpy lstatuscpy = new Lstatuscpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Vflagcpy vflagcpy = new Vflagcpy();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Tr204rec tr204rec = new Tr204rec();
	private HpavpfDAO hpavpfDAO =  getApplicationContext().getBean("hpavpfDAO", HpavpfDAO.class);
	private Hpavpf hpavIO = new Hpavpf();


	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endOfFile2080, 
		exit2090, 
		readChdr212a,
		a1090Exit
	}

	public Bt500() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingDates1040();
		defineCursor1060();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		hpavpf.openOutput();
		wsspEdterror.set(varcom.oK);
		pt500par.parmRecord.set(bupaIO.getParmarea());
		/* MOVE PT500-DATEFROM         TO WSAA-DATEFROM.*/
		if (isEQ(pt500par.datefrom, varcom.vrcmMaxDate)) {
			wsaaDatefrom.set(0);
		}
		else {
			wsaaDatefrom.set(pt500par.datefrom);
		}
		wsaaDateto.set(pt500par.dateto);
		wsaaValid.set(vflagcpy.inForceVal);
		wsaaBrnCnt.set(ZERO);
		wsaaBrnPrem.set(ZERO);
		wsaaBrnDuty.set(ZERO);
		wsaaCoyCnt.set(ZERO);
		wsaaCoyPrem.set(ZERO);
		wsaaCoyDuty.set(ZERO);
		wsaaCompany.set(SPACES);
	
		hpavpfIO.setDataKey(SPACES);
		hpavpfIO.setChdrcoy("9");
        hpavpfIO.setFunction(varcom.begn);
	}
		
	

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(pt500par.dateto);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Repdate.set(datcon1rec.extDate);
	}

protected void defineCursor1060()
	{
		/*  Define the query required by declaring a cursor*/
		sqlhpadpf1 = " SELECT  CHDRCOY, CHDRNUM, VALIDFLAG, HOISSDTE" +
" FROM   " + getAppVars().getTableNameOverriden("HPADPF") + " " +
" WHERE VALIDFLAG = ?" +
" AND HOISSDTE BETWEEN ? AND ?" +
" ORDER BY CHDRNUM";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlhpadpf1conn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.HpadpfTableDAM());
			sqlhpadpf1ps = getAppVars().prepareStatementEmbeded(sqlhpadpf1conn, sqlhpadpf1, "HPADPF");
			getAppVars().setDBString(sqlhpadpf1ps, 1, wsaaValid);
			getAppVars().setDBNumber(sqlhpadpf1ps, 2, wsaaDatefrom);
			getAppVars().setDBNumber(sqlhpadpf1ps, 3, wsaaDateto);
			sqlhpadpf1rs = getAppVars().executeQuery(sqlhpadpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case endOfFile2080: 
					endOfFile2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		/*   Fetch record*/
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlhpadpf1rs)) {
				getAppVars().getDBObject(sqlhpadpf1rs, 1, sqlChdrcoy);
				getAppVars().getDBObject(sqlhpadpf1rs, 2, sqlChdrnum);
				getAppVars().getDBObject(sqlhpadpf1rs, 3, sqlValidflag);
				getAppVars().getDBObject(sqlhpadpf1rs, 4, sqlHoissdte);
			}
			else {
				goTo(GotoLabel.endOfFile2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		goTo(GotoLabel.exit2090);
	}

protected void endOfFile2080()
	{
		wsspEdterror.set(varcom.endp);
		if (isNE(wsaaCompany, SPACES)) {
			/*       PERFORM 2800-BRN-TOTAL*/
			/*    MOVE 'COMPANY TOTAL'     TO RT01-TEXTFIELD                */
			indOn.setTrue(1);
			
		}
	}

protected void edit2500()
	{
		/*EDIT*/
		/* Check record is required for processing.*/
		/* Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		checkChdr210a();
		if (isEQ(wsaaChdrFound, "N")) {
			wsspEdterror.set(SPACES);  // ILIFE-6651
			return ;
		}
	
		/*EXIT*/
	}



protected void getBranch2700()
	{
		getBranch2710();
	}

protected void getBranch2710()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(wsaaBranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
	}

protected void brnTotal2800()
	{
		/*BRN-TOTAL*/
		/* MOVE 'BRANCH  TOTAL'        TO RT01-TEXTFIELD.               */
		rt01Gcnt.set(wsaaBrnCnt);
		rt01Anetpre.set(wsaaBrnPrem);
		rt01Astpdty.set(wsaaBrnDuty);
		/*    WRITE PRINTER-REC         FROM RT500-T01*/
		/*                            FORMAT 'RT500T01'*/
		/*                        INDICATORS INDIC-AREA.*/
		wsaaCoyCnt.add(wsaaBrnCnt);
		wsaaCoyPrem.add(wsaaBrnPrem);
		wsaaCoyDuty.add(wsaaBrnDuty);
		wsaaBrnCnt.set(ZERO);
		wsaaBrnPrem.set(ZERO);
		wsaaBrnDuty.set(ZERO);
		/*EXIT*/
	}

protected void checkChdr210a()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					checkChdr211a();
				case readChdr212a: 
					readChdr212a();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void checkChdr211a()
	{
		wsaaChdrFound = "N";
		wsaaRrn.set(ZERO);
		chdrIO.setRecKeyData(SPACES);
		chdrIO.setChdrpfx(fsupfxcpy.chdr);
		chdrIO.setChdrcoy(sqlChdrcoy);
		chdrIO.setChdrnum(sqlChdrnum);
		chdrIO.setValidflag(sqlValidflag);
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		chdrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		chdrIO.setFitKeysSearch("CHDRPFX", "CHDRCOY", "CHDRNUM", "VALIDFLAG");
		chdrIO.setFormat(chdrrec);
	}

protected void readChdr212a()
	{
		SmartFileCode.execute(appVars, chdrIO);
		chdrIO.setFunction(varcom.nextr);
		if (isNE(chdrIO.getStatuz(), varcom.oK)
		&& isNE(chdrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		if (isNE(chdrIO.getChdrpfx(), fsupfxcpy.chdr)
		|| isNE(chdrIO.getChdrcoy(), sqlChdrcoy)
		|| isNE(chdrIO.getChdrnum(), sqlChdrnum)
		|| isNE(chdrIO.getValidflag(), sqlValidflag)
		|| isNE(chdrIO.getStatuz(), varcom.oK)) {
			return ;
		}
		if (isNE(chdrIO.getStmpdtydte(), 0)
		&& isNE(chdrIO.getStmpdtydte(), varcom.vrcmMaxDate)) {
			wsaaRrn.set(ZERO);
			wsaaChdrFound = "N";
			wsspEdterror.set(SPACES);  //ILIFE-6651
			return ;
		}
		if (isNE(chdrIO.getStatcode(), lstatuscpy.lstvStatIf)) {
			goTo(GotoLabel.readChdr212a);
		}
		if (isEQ(wsaaChdrFound, "Y")) {
			goTo(GotoLabel.readChdr212a);
		}
		wsaaRrn.set(chdrIO.getRrn());
		wsaaCownpfx.set(chdrIO.getCownpfx());
		wsaaCowncoy.set(chdrIO.getCowncoy());
		wsaaCownnum.set(chdrIO.getCownnum());
		wsaaCntcurr.set(chdrIO.getCntcurr());
		wsaaSinstamt06.set(chdrIO.getSinstamt06());
		wsaaChdrFound = "Y";
		goTo(GotoLabel.readChdr212a);
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (isEQ(wsaaChdrFound, "N")) {
			return ;
		}
		updateChdr310a();
		/*WRITE-DETAIL*/
		/* If first page/overflow - write standard headings*/
		
		updateHpad320();
		
	}

protected void updateChdr310a()
	{
		updChdr310a();
	}

protected void updChdr310a()
	{
		chdrIO.setParams(SPACES);
		chdrIO.setRecKeyData(SPACES);
		chdrIO.setStatuz(varcom.oK);
		chdrIO.setFunction(varcom.readd);
		chdrIO.setRrn(wsaaRrn);
		chdrIO.setFormat(chdrrec);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
		chdrIO.setStmpdtydte(wsaaDateto);
		chdrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrIO.getParams());
			fatalError600();
		}
	}


protected void updateHpad320()
{   
	initialize(hpavpfRec);
	namadrsrec.namadrsRec.set(SPACES);
	namadrsrec.clntPrefix.set(wsaaCownpfx);
	namadrsrec.clntCompany.set(wsaaCowncoy);
	namadrsrec.clntNumber.set(wsaaCownnum);
	namadrsrec.language.set(bsscIO.getLanguage());
	namadrsrec.function.set("PYNMA");
	callProgram(Namadrs.class, namadrsrec.namadrsRec);
	if (isEQ(namadrsrec.statuz, varcom.oK)) {
		wsaaOwnme.set(namadrsrec.name);
	}
	else {
		wsaaOwnme.set(SPACES);
	}
	datcon1rec.function.set(varcom.conv);
	datcon1rec.intDate.set(sqlHoissdte);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);

	getStampDuty3200();
	
	hpavpfRec.chdrcoy.set(wsaaCowncoy);
	hpavpfRec.chdrnum.set(sqlChdrnum); 
	hpavpfRec.linsname.set(wsaaOwnme);
	hpavpfRec.hoissdte.set(datcon1rec.extDate);
	hpavpfRec.anetpre.set(wsaaSinstamt06);
	hpavpfRec.asptdty.set(wsaaSduty);
	hpavpfRec.curr.set(wsaaCntcurr);
	hpavpf.write(hpavpfRec);

	
	FileSort fs1 = new FileSort(sortFile);
	fs1.addSortKey(sortRec.curr, true);
	fs1.setUsing(hpavpf);
	fs1.setGiving(hpavpf);
	fs1.sort();
	hpavpf.openInput();
	
	hpavpf.close();
}




protected void getStampDuty3200()
	{
		readTr2043210();
	}

protected void readTr2043210()
	{
		wsaaSduty.set(0);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setItemtabl(tr204);
		itdmIO.setItemitem(wsaaCntcurr);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			return ;
		}
		tr204rec.tr204Rec.set(itdmIO.getGenarea());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 5)); wsaaSub.add(1)){
			if (isGTE(wsaaSinstamt06, tr204rec.zrcptfrm[wsaaSub.toInt()])
			&& isLTE(wsaaSinstamt06, tr204rec.zrcptto[wsaaSub.toInt()])) {
				wsaaSduty.set(tr204rec.sduty[wsaaSub.toInt()]);
				wsaaSub.set(6);
			}
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}



protected void close4000()
	{

	moveData3100();
	lsaaStatuz.set(varcom.oK);
	getAppVars().freeDBConnectionIgnoreErr(sqlhpadpf1conn, sqlhpadpf1ps, sqlhpadpf1rs);
	hpavpf.close();
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
	}


protected void moveData3100()
{
		//ILIFE-6651 Starts
		List<Hpavpf> hpavpflist = hpavpfDAO.searchHpavpfRecord(hpavpf.chdrcoy.toString().trim());
		
		if (hpavpflist!=null && hpavpflist.size()>0)
		{
		for(Hpavpf pf : hpavpflist)
		{
		
	 // while(!endOfFile.isTrue())
	 // {
		//a1010Callio();
		
		
					if (firstRec.isTrue()) {
						wsaaFirstRec.set(SPACES);
						prevCurr.set(pf.getCurr());
							prepareHead2600(pf);
					}
					
					
					if (isNE(pf.getCurr(),prevCurr))
					    { 
						prevCurr.set(pf.getCurr());
						printPolicyTotal();
						printNewPage6010(pf);
						wsaaCoyCnt.set(1);
						wsaaCoyPrem.set(hpavpfIO.anetpre);
						/* ADD WSAA-DUTY20             TO WSAA-COY-DUTY.*/
						wsaaCoyDuty.set(hpavpfIO.asptdty);
					}
					else
					{
						printPolicy(pf);
						wsaaCoyCnt.add(1);
						wsaaCoyPrem.add(pf.getAnetpre());
						/* ADD WSAA-DUTY20             TO WSAA-COY-DUTY.*/
						wsaaCoyDuty.add(pf.getAsptdty());
					}
				
				//a1020ReadNext();
			  }
	  
	  
		}
		 
			printPolicyTotal();
		 
			//ILIFE-6651 ends 
}




protected void a1010Callio()
{
	hpavpfIO.setFormat(hpavpfrec);
	SmartFileCode.execute(appVars, hpavpfIO);
	if (isNE(hpavpfIO.getStatuz(),varcom.oK)
	&& isNE(hpavpfIO.getStatuz(),varcom.endp)) {
		syserrrec.params.set(hpavpfIO.getParams());
		syserrrec.statuz.set(hpavpfIO.getStatuz());
		fatalError600();
	}
	if (isEQ(hpavpfIO.getStatuz(),varcom.endp)) {
		hpavpfIO.setStatuz(varcom.endp);
		wsaaEof.set("Y");
		
	}
}

protected void a1020ReadNext()
{
	//hpavpfIO.setFunction(varcom.nextr);   //ILIFE-6651 
}


//ILIFE -6651 STARTS
protected void printPolicy(Hpavpf pf)
{
	rd01Chdrno.set(pf.getChdrnum());
	rd01Linsname.set(pf.getLinsname());
	rd01Ccdate.set(pf.getHoissdte());
	rd01Anetpre.set(pf.getAnetpre());
	rd01Astpdty.set(pf.getAsptdty());
	printerFile.printRt500d01(rt500D01, indicArea);
	
}
//ILIFE-6651 ENDS

protected void printPolicyTotal()
{
	rt01Gcnt.set(wsaaCoyCnt);
	rt01Anetpre.set(wsaaCoyPrem);
	rt01Astpdty.set(wsaaCoyDuty);
	printerFile.printRt500t01(rt500T01, indicArea);
}

protected void printNewPage6010(Hpavpf pf)
{
	setUpHeadingCompany1020();
	setUpHeadingDates1040();
	prepareHead2600(pf);
	printPolicy(pf);
	
}

protected void prepareHead2600(Hpavpf pf)
{
	prepare2610(pf);
}

protected void prepare2610(Hpavpf pf)
{
	wsaaFirstHeading.set("N");
	/*    MOVE SQL-CNTBRANCH          TO WSAA-BRANCH.*/
	/*    PERFORM 2700-GET-BRANCH.*/
	descIO.setDescpfx("IT");
	descIO.setDesccoy(bsprIO.getCompany());
	descIO.setDesctabl(t3629);
	descIO.setDescitem(pf.getCurr());
	descIO.setItemseq(SPACES);
	descIO.setLanguage(bsscIO.getLanguage());
	descIO.setFunction(varcom.readr);
	descIO.setFormat(descrec);
	SmartFileCode.execute(appVars, descIO);
	if (isNE(descIO.getStatuz(), varcom.oK)
	&& isNE(descIO.getStatuz(), varcom.mrnf)) {
		syserrrec.params.set(descIO.getParams());
		fatalError600();
	}
	rh01Currcode.set(pf.getCurr());
	rh01Currencynm.set(descIO.getLongdesc());
	printerFile.printRt500h01(rt500H01, indicArea);
	//wsaaOverflow.set("N");
}





}
