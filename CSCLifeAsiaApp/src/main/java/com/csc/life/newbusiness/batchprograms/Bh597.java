/*
 * File: Bh597.java
 * Date: 29 August 2009 21:36:37
 * Author: Quipoz Limited
 * 
 * Class transformed from BH597.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HchrmpsTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.HpddmpsTableDAM;
import com.csc.life.newbusiness.dataaccess.HptnmpsTableDAM;
import com.csc.life.newbusiness.dataaccess.HtmppfTableDAM;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.newbusiness.tablestructures.Th596rec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*    Monthly Total New Business Policy Status Splitter Extract
*    =========================================================
*
*    This program is aimed for extract all the relevant
*    information required to produce the Monthly Totalling New
*    Business Policy Status report. The extraction is mainly
*    based on PTRN and CHDR and COVR/COVT files and also TH596
*    as selection criteria. The extracted information are then
*    written to temporary files.
*
*****************************************************************
* </pre>
*/
public class Bh597 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private HtmppfTableDAM htmppf = new HtmppfTableDAM();
	private DiskFileDAM htmp01 = new DiskFileDAM("HTMP01");
	private DiskFileDAM htmp02 = new DiskFileDAM("HTMP02");
	private DiskFileDAM htmp03 = new DiskFileDAM("HTMP03");
	private DiskFileDAM htmp04 = new DiskFileDAM("HTMP04");
	private DiskFileDAM htmp05 = new DiskFileDAM("HTMP05");
	private DiskFileDAM htmp06 = new DiskFileDAM("HTMP06");
	private DiskFileDAM htmp07 = new DiskFileDAM("HTMP07");
	private DiskFileDAM htmp08 = new DiskFileDAM("HTMP08");
	private DiskFileDAM htmp09 = new DiskFileDAM("HTMP09");
	private DiskFileDAM htmp10 = new DiskFileDAM("HTMP10");
	private DiskFileDAM htmp11 = new DiskFileDAM("HTMP11");
	private DiskFileDAM htmp12 = new DiskFileDAM("HTMP12");
	private DiskFileDAM htmp13 = new DiskFileDAM("HTMP13");
	private DiskFileDAM htmp14 = new DiskFileDAM("HTMP14");
	private DiskFileDAM htmp15 = new DiskFileDAM("HTMP15");
	private DiskFileDAM htmp16 = new DiskFileDAM("HTMP16");
	private DiskFileDAM htmp17 = new DiskFileDAM("HTMP17");
	private DiskFileDAM htmp18 = new DiskFileDAM("HTMP18");
	private DiskFileDAM htmp19 = new DiskFileDAM("HTMP19");
	private DiskFileDAM htmp20 = new DiskFileDAM("HTMP20");
	private FixedLengthStringData htmp01Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp02Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp03Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp04Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp05Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp06Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp07Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp08Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp09Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp10Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp11Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp12Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp13Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp14Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp15Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp16Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp17Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp18Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp19Rec = new FixedLengthStringData(59);
	private FixedLengthStringData htmp20Rec = new FixedLengthStringData(59);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH597");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaPrintFlag = new FixedLengthStringData(1);
	private Validator printReady = new Validator(wsaaPrintFlag, "Y");
	private Validator printNotReady = new Validator(wsaaPrintFlag, "N");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaHtmpFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHtmpFn, 0, FILLER).init("HTMP");
	private FixedLengthStringData wsaaHtmpRunid = new FixedLengthStringData(2).isAPartOf(wsaaHtmpFn, 4);
	private ZonedDecimalData wsaaHtmpJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHtmpFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaTrcodeFound = new FixedLengthStringData(1);
	private Validator trcodeFound = new Validator(wsaaTrcodeFound, "Y");

	private FixedLengthStringData wsaaStatcodeFound = new FixedLengthStringData(1);
	private Validator statcodeFound = new Validator(wsaaStatcodeFound, "Y");

	private FixedLengthStringData wsaaCpStatcodeFound = new FixedLengthStringData(1);
	private Validator cpStatcodeFound = new Validator(wsaaCpStatcodeFound, "Y");

	private FixedLengthStringData wsaaFupcodeFound = new FixedLengthStringData(1);
	private Validator fupcodeFound = new Validator(wsaaFupcodeFound, "Y");

	private FixedLengthStringData wsaaCnttypeMatch = new FixedLengthStringData(1);
	private Validator cnttypeMatch = new Validator(wsaaCnttypeMatch, "Y");

	private FixedLengthStringData wsaaFirstTimeFup = new FixedLengthStringData(1);
	private Validator firstTimeFup = new Validator(wsaaFirstTimeFup, "Y");
	private ZonedDecimalData wsaaW = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTh596Sub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaTh506Sub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaTh506CrtableHeld = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaMthStart = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaMthEnd = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaRiskStatusComp = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaFupcodeComp = new FixedLengthStringData(3);
	private ZonedDecimalData wsaaPendForOtherRow = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaNoFupRow = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCertPolRow = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaEffectiveDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaEffectiveDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaEffYr = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0).setUnsigned();
	private ZonedDecimalData wsaaEffMth = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4).setUnsigned();
	private ZonedDecimalData wsaaEffDay = new ZonedDecimalData(2, 0).isAPartOf(filler3, 6).setUnsigned();
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSingp9 = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaInstprem9 = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaSumins9 = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaCntfee = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaFinalSingp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFinalInstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFinalSumin = new ZonedDecimalData(17, 2);

	private FixedLengthStringData wsaaTh596 = new FixedLengthStringData(12720);
	private FixedLengthStringData[] wsaaStoreTh596 = FLSArrayPartOfStructure(40, 318, wsaaTh596, 0);
	private FixedLengthStringData[] wsaaRowId = FLSDArrayPartOfArrayStructure(2, wsaaStoreTh596, 0);
	private FixedLengthStringData[][] wsaaTh596Trcode = FLSDArrayPartOfArrayStructure(28, 4, wsaaStoreTh596, 2);
	private FixedLengthStringData[][] wsaaTh596CnRiskStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaStoreTh596, 114);
	private FixedLengthStringData[][] wsaaTh596Fupcode = FLSDArrayPartOfArrayStructure(60, 3, wsaaStoreTh596, 138);
	private int wsaaTh596TrcodeNo = 28;
	private int wsaaTh596StatcodeNo = 12;
	private int wsaaTh596FupcodeNo = 60;
	//ILIFE-3235 STARTS
	private FixedLengthStringData wsaaTh506 = new FixedLengthStringData(490);
	private FixedLengthStringData[] wsaaStoreTh506 = FLSArrayPartOfStructure(70, 7, wsaaTh506, 0);
	//ILIFE-3235 ENDS
	private FixedLengthStringData[] wsaaTh506Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaStoreTh506, 0);
	private FixedLengthStringData[] wsaaTh506Crtable = FLSDArrayPartOfArrayStructure(4, wsaaStoreTh506, 3);

	private FixedLengthStringData wsaaFupcodeArray = new FixedLengthStringData(180);
	private FixedLengthStringData[] wsaaFupcodePr = FLSArrayPartOfStructure(60, 3, wsaaFupcodeArray, 0);
	private FixedLengthStringData wsaaFupcode = new FixedLengthStringData(3);
		/* ERRORS */
	private String ivrm = "IVRM";
	private String e308 = "E308";
	private String f151 = "F151";
	private String hpadrec = "HPADREC";
	private String itemrec = "ITEMREC";
	private String hchrmpsrec = "HCHRMPSREC";
	private String hptnmpsrec = "HPTNMPSREC";
	private String hpddmpsrec = "HPDDMPSREC";
	private String chdrlifrec = "CHDRLIFREC";
	private String fluplnbrec = "FLUPLNBREC";
	private String covtlnbrec = "COVTLNBREC";
	private String covrenqrec = "COVRENQREC";
		/* TABLES */
	private String t5674 = "T5674";
	private String t5688 = "T5688";
	private String th596 = "TH596";
	private String th506 = "TH506";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Coverage/rider transactions - new busine*/
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Follow-ups - life new business*/
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
		/*Contract Details By Status*/
	private HchrmpsTableDAM hchrmpsIO = new HchrmpsTableDAM();
		/*Contract Additional Details*/
	private HpadTableDAM hpadIO = new HpadTableDAM();
		/*Policy Additional Detail By Decision  Da*/
	private HpddmpsTableDAM hpddmpsIO = new HpddmpsTableDAM();
		/*Policy Transaction By Effective Date*/
	private HptnmpsTableDAM hptnmpsIO = new HptnmpsTableDAM();
		/*TEMP MTHLY POLICY STATUS EXTRACT FILE*/
	private HtmppfTableDAM htmppfData = new HtmppfTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
		/*TEMP MTHLY POLICY STATUS EXTRACT FILE*/
	private HtmppfTableDAM payrpfData = new HtmppfTableDAM();
	private T5674rec t5674rec = new T5674rec();
	private T5688rec t5688rec = new T5688rec();
	private Th506rec th506rec = new Th506rec();
	private Th596rec th596rec = new Th596rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next1280, 
		exit1290, 
		exit1390, 
		exit3090, 
		z110Start, 
		z180Next, 
		z190Exit, 
		z310Start, 
		z380Next, 
		z390Exit, 
		z410Start, 
		z480Next, 
		z490Exit, 
		z890Exit, 
		y280Next, 
		y290Exit, 
		y890Exit
	}

	public Bh597() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		iy.set(1);
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		wsaaEffDay.set(1);
		wsaaMthStart.set(wsaaEffectiveDate);
		if (isEQ(wsaaEffMth,12)) {
			wsaaEffYr.add(1);
			wsaaEffMth.set(1);
		}
		else {
			wsaaEffMth.add(1);
		}
		datcon1rec.intDate.set(wsaaEffectiveDate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			fatalError600();
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		datcon2rec.intDate1.set(datcon1rec.intDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaMthEnd.set(datcon2rec.intDate2);
		wsaaHtmpRunid.set(bprdIO.getSystemParam04());
		wsaaHtmpJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		hptnmpsIO.setParams(SPACES);
		hptnmpsIO.setChdrcoy(bsprIO.getCompany());
		hptnmpsIO.setPtrneff(wsaaMthEnd);
		hptnmpsIO.setTranno(99999);
		hptnmpsIO.setFormat(hptnmpsrec);
		hptnmpsIO.setFunction(varcom.begn);
		hpddmpsIO.setParams(SPACES);
		hpddmpsIO.setChdrcoy(bsprIO.getCompany());
		hpddmpsIO.setHuwdcdte(wsaaMthEnd);
		hpddmpsIO.setFormat(hpddmpsrec);
		hpddmpsIO.setFunction(varcom.begn);
		hchrmpsIO.setParams(SPACES);
		hchrmpsIO.setChdrcoy(bsprIO.getCompany());
		hchrmpsIO.setFormat(hchrmpsrec);
		hchrmpsIO.setFunction(varcom.begn);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th596);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			readTh5961200();
		}
		
		wsaaSub.set(0);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th506);
		itemIO.setItemitem(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL");
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			readTh5061300();
		}
		
		y400SpecialCases();
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaHtmpFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set("CLRTMPF");
			fatalError600();
		}
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(HTMP");
		stringVariable1.append(iz.toString());
		stringVariable1.append(") TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaHtmpFn.toString());
		stringVariable1.append(") MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		stringVariable1.append(" SEQONLY(*YES 1000)");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz,1)) {
			htmp01.openOutput();
		}
		if (isEQ(iz,2)) {
			htmp02.openOutput();
		}
		if (isEQ(iz,3)) {
			htmp03.openOutput();
		}
		if (isEQ(iz,4)) {
			htmp04.openOutput();
		}
		if (isEQ(iz,5)) {
			htmp05.openOutput();
		}
		if (isEQ(iz,6)) {
			htmp06.openOutput();
		}
		if (isEQ(iz,7)) {
			htmp07.openOutput();
		}
		if (isEQ(iz,8)) {
			htmp08.openOutput();
		}
		if (isEQ(iz,9)) {
			htmp09.openOutput();
		}
		if (isEQ(iz,10)) {
			htmp10.openOutput();
		}
		if (isEQ(iz,11)) {
			htmp11.openOutput();
		}
		if (isEQ(iz,12)) {
			htmp12.openOutput();
		}
		if (isEQ(iz,13)) {
			htmp13.openOutput();
		}
		if (isEQ(iz,14)) {
			htmp14.openOutput();
		}
		if (isEQ(iz,15)) {
			htmp15.openOutput();
		}
		if (isEQ(iz,16)) {
			htmp16.openOutput();
		}
		if (isEQ(iz,17)) {
			htmp17.openOutput();
		}
		if (isEQ(iz,18)) {
			htmp18.openOutput();
		}
		if (isEQ(iz,19)) {
			htmp19.openOutput();
		}
		if (isEQ(iz,20)) {
			htmp20.openOutput();
		}
	}

protected void readTh5961200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start1210();
				}
				case next1280: {
					next1280();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1210()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),th596)) {
			itemIO.setStatuz(varcom.endp);
			wsaaTh596Sub.set(wsaaSub);
			goTo(GotoLabel.exit1290);
		}
		if (isNE(subString(itemIO.getItemitem(), 1, 5),wsaaProg)) {
			goTo(GotoLabel.next1280);
		}
		th596rec.th596Rec.set(itemIO.getGenarea());
		wsaaSub.add(1);
		wsaaRowId[wsaaSub.toInt()].set(subString(itemIO.getItemitem(), 6, 2));
		for (wsaaY.set(1); !(isGT(wsaaY,wsaaTh596TrcodeNo)); wsaaY.add(1)){
			if (isNE(th596rec.trcode[wsaaY.toInt()],SPACES)) {
				wsaaX.add(1);
				wsaaTh596Trcode[wsaaSub.toInt()][wsaaX.toInt()].set(th596rec.trcode[wsaaY.toInt()]);
			}
		}
		wsaaX.set(0);
		for (wsaaY.set(1); !(isGT(wsaaY,wsaaTh596StatcodeNo)); wsaaY.add(1)){
			if (isNE(th596rec.cnRiskStat[wsaaY.toInt()],SPACES)) {
				wsaaX.add(1);
				wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaX.toInt()].set(th596rec.cnRiskStat[wsaaY.toInt()]);
			}
		}
		wsaaX.set(0);
		for (wsaaY.set(1); !(isGT(wsaaY,wsaaTh596FupcodeNo)); wsaaY.add(1)){
			if (isNE(th596rec.fupcdes[wsaaY.toInt()],SPACES)) {
				wsaaX.add(1);
				wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()].set(th596rec.fupcdes[wsaaY.toInt()]);
			}
		}
	}

protected void next1280()
	{
		itemIO.setFunction(varcom.nextr);
	}

protected void readTh5061300()
	{
		try {
			start1310();
			next1380();
		}
		catch (GOTOException e){
		}
	}

protected void start1310()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp)
		|| isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),th506)) {
			itemIO.setStatuz(varcom.endp);
			wsaaTh506Sub.set(wsaaSub);
			goTo(GotoLabel.exit1390);
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		wsaaSub.add(1);
		wsaaTh506Crtable[wsaaSub.toInt()].set(th506rec.crtable);
		wsaaTh506Cnttype[wsaaSub.toInt()].set(subString(itemIO.getItemitem(), 1, 3));
	}

protected void next1380()
	{
		itemIO.setFunction(varcom.nextr);
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		printReady.setTrue();
		if (isEQ(hptnmpsIO.getStatuz(),varcom.endp)) {
			if (isEQ(hchrmpsIO.getStatuz(),varcom.endp)) {
				z400ProcessHpddmps();
			}
			else {
				z300ProcessHchrmps();
			}
		}
		else {
			z100ProcessHptnmps();
		}
		if (isEQ(hptnmpsIO.getStatuz(),varcom.endp)
		&& isEQ(hchrmpsIO.getStatuz(),varcom.endp)
		&& isEQ(hpddmpsIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
		}
		else {
			wsspEdterror.set(varcom.oK);
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			update3010();
		}
		catch (GOTOException e){
		}
	}

protected void update3010()
	{
		if (printNotReady.isTrue()) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(wsaaPrevChdrnum,SPACES)) {
			wsaaPrevChdrnum.set(htmppfData.chdrnum);
		}
		if (isNE(htmppfData.chdrnum,wsaaPrevChdrnum)) {
			iy.add(1);
			wsaaPrevChdrnum.set(htmppfData.chdrnum);
		}
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(wsaaPrevChdrnum,SPACES)) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(iy,1)) {
			htmp01.write(htmppfData);
		}
		if (isEQ(iy,2)) {
			htmp02.write(htmppfData);
		}
		if (isEQ(iy,3)) {
			htmp03.write(htmppfData);
		}
		if (isEQ(iy,4)) {
			htmp04.write(htmppfData);
		}
		if (isEQ(iy,5)) {
			htmp05.write(htmppfData);
		}
		if (isEQ(iy,6)) {
			htmp06.write(htmppfData);
		}
		if (isEQ(iy,7)) {
			htmp07.write(htmppfData);
		}
		if (isEQ(iy,8)) {
			htmp08.write(htmppfData);
		}
		if (isEQ(iy,9)) {
			htmp09.write(htmppfData);
		}
		if (isEQ(iy,10)) {
			htmp10.write(htmppfData);
		}
		if (isEQ(iy,11)) {
			htmp11.write(htmppfData);
		}
		if (isEQ(iy,12)) {
			htmp12.write(htmppfData);
		}
		if (isEQ(iy,13)) {
			htmp13.write(htmppfData);
		}
		if (isEQ(iy,14)) {
			htmp14.write(htmppfData);
		}
		if (isEQ(iy,15)) {
			htmp15.write(htmppfData);
		}
		if (isEQ(iy,16)) {
			htmp16.write(htmppfData);
		}
		if (isEQ(iy,17)) {
			htmp17.write(htmppfData);
		}
		if (isEQ(iy,18)) {
			htmp18.write(htmppfData);
		}
		if (isEQ(iy,19)) {
			htmp19.write(htmppfData);
		}
		if (isEQ(iy,20)) {
			htmp20.write(htmppfData);
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz,1)) {
			htmp01.close();
		}
		if (isEQ(iz,2)) {
			htmp02.close();
		}
		if (isEQ(iz,3)) {
			htmp03.close();
		}
		if (isEQ(iz,4)) {
			htmp04.close();
		}
		if (isEQ(iz,5)) {
			htmp05.close();
		}
		if (isEQ(iz,6)) {
			htmp06.close();
		}
		if (isEQ(iz,7)) {
			htmp07.close();
		}
		if (isEQ(iz,8)) {
			htmp08.close();
		}
		if (isEQ(iz,9)) {
			htmp09.close();
		}
		if (isEQ(iz,10)) {
			htmp10.close();
		}
		if (isEQ(iz,11)) {
			htmp11.close();
		}
		if (isEQ(iz,12)) {
			htmp12.close();
		}
		if (isEQ(iz,13)) {
			htmp13.close();
		}
		if (isEQ(iz,14)) {
			htmp14.close();
		}
		if (isEQ(iz,15)) {
			htmp15.close();
		}
		if (isEQ(iz,16)) {
			htmp16.close();
		}
		if (isEQ(iz,17)) {
			htmp17.close();
		}
		if (isEQ(iz,18)) {
			htmp18.close();
		}
		if (isEQ(iz,19)) {
			htmp19.close();
		}
		if (isEQ(iz,20)) {
			htmp20.close();
		}
	}

protected void z100ProcessHptnmps()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case z110Start: {
					z110Start();
				}
				case z180Next: {
					z180Next();
				}
				case z190Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void z110Start()
	{
		SmartFileCode.execute(appVars, hptnmpsIO);
		if (isNE(hptnmpsIO.getStatuz(),varcom.oK)
		&& isNE(hptnmpsIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hptnmpsIO.getParams());
			fatalError600();
		}
		if (isEQ(hptnmpsIO.getStatuz(),varcom.endp)
		|| isNE(hptnmpsIO.getChdrcoy(),bsprIO.getCompany())
		|| isLT(hptnmpsIO.getPtrneff(),wsaaMthStart)) {
			printNotReady.setTrue();
			hptnmpsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.z190Exit);
		}
		hptnmpsIO.setFunction(varcom.nextr);
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(hptnmpsIO.getChdrcoy());
		chdrlifIO.setChdrnum(hptnmpsIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		z500FeeMth();
		wsaaTrcodeFound.set(SPACES);
		for (wsaaSub.set(1); !(trcodeFound.isTrue()
		|| (isGT(wsaaSub,wsaaTh596Sub))); wsaaSub.add(1)){
			if (isEQ(wsaaRowId[wsaaSub.toInt()],"C")
			|| isEQ(wsaaRowId[wsaaSub.toInt()],"B")) {
				for (wsaaY.set(1); !(trcodeFound.isTrue()
				|| (isGT(wsaaY,wsaaTh596TrcodeNo))
				|| (isEQ(wsaaTh596Trcode[wsaaSub.toInt()][wsaaY.toInt()],SPACES))); wsaaY.add(1)){
					if (isEQ(hptnmpsIO.getBatctrcde(),wsaaTh596Trcode[wsaaSub.toInt()][wsaaY.toInt()])) {
						if (isEQ(wsaaRowId[wsaaSub.toInt()],"N")) {
							if (isNE(chdrlifIO.getValidflag(),"1")) {
								trcodeFound.setTrue();
							}
						}
						else {
							trcodeFound.setTrue();
						}
					}
				}
			}
		}
		compute(wsaaSub, 0).set(sub(wsaaSub,1));
		if (isEQ(wsaaTrcodeFound,SPACES)) {
			goTo(GotoLabel.z180Next);
		}
		htmppfData.chdrcoy.set(chdrlifIO.getChdrcoy());
		htmppfData.chdrnum.set(chdrlifIO.getChdrnum());
		htmppfData.cntbranch.set(chdrlifIO.getCntbranch());
		htmppfData.agntnum.set(chdrlifIO.getAgntnum());
		htmppfData.cnttype.set(chdrlifIO.getCnttype());
		htmppfData.cntcurr.set(chdrlifIO.getCntcurr());
		htmppfData.occdate.set(chdrlifIO.getOccdate());
		htmppfData.hrow.set(wsaaRowId[wsaaSub.toInt()]);
		z600Calculation();
		htmppfData.sumins.set(wsaaFinalSumin);
		htmppfData.singp.set(wsaaFinalSingp);
		htmppfData.annprem.set(wsaaFinalInstprem);
		goTo(GotoLabel.z190Exit);
	}

protected void z180Next()
	{
		goTo(GotoLabel.z110Start);
	}

protected void z300ProcessHchrmps()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case z310Start: {
					z310Start();
				}
				case z380Next: {
					z380Next();
				}
				case z390Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void z310Start()
	{
		SmartFileCode.execute(appVars, hchrmpsIO);
		if (isNE(hchrmpsIO.getStatuz(),varcom.oK)
		&& isNE(hchrmpsIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hchrmpsIO.getParams());
			fatalError600();
		}
		if (isEQ(hchrmpsIO.getStatuz(),varcom.endp)
		|| isNE(hchrmpsIO.getChdrcoy(),bsprIO.getCompany())) {
			printNotReady.setTrue();
			hchrmpsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.z390Exit);
		}
		hchrmpsIO.setFunction(varcom.nextr);
		z550FeeMth();
		wsaaStatcodeFound.set(SPACES);
		wsaaFupcodeFound.set(SPACES);
		firstTimeFup.setTrue();
		for (wsaaSub.set(1); !((isGT(wsaaSub,wsaaTh596Sub))
		|| (statcodeFound.isTrue()
		&& fupcodeFound.isTrue())); wsaaSub.add(1)){
			for (wsaaY.set(1); !((isGT(wsaaY,wsaaTh596StatcodeNo))
			|| (isEQ(wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()],SPACES))
			|| statcodeFound.isTrue()); wsaaY.add(1)){
				if (isEQ(hchrmpsIO.getStatcode(),wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()])) {
					statcodeFound.setTrue();
				}
			}
			if (statcodeFound.isTrue()) {
				if (firstTimeFup.isTrue()) {
					y100FindFlup();
					wsaaFirstTimeFup.set("N");
					if (isEQ(wsaaFupcode,SPACES)) {
						hpadIO.setParams(SPACES);
						hpadIO.setChdrcoy(hchrmpsIO.getChdrcoy());
						hpadIO.setChdrnum(hchrmpsIO.getChdrnum());
						hpadIO.setFormat(hpadrec);
						hpadIO.setFunction(varcom.readr);
						SmartFileCode.execute(appVars, hpadIO);
						if (isNE(hpadIO.getStatuz(),varcom.oK)
						&& isNE(hpadIO.getStatuz(),varcom.mrnf)) {
							syserrrec.statuz.set(hpadIO.getStatuz());
							syserrrec.params.set(hpadIO.getParams());
							fatalError600();
						}
						if (isEQ(hpadIO.getHuwdcdte(),varcom.vrcmMaxDate)) {
							wsaaSub.set(wsaaNoFupRow);
							fupcodeFound.setTrue();
						}
					}
					else {
						wsaaFupcodeFound.set(SPACES);
					}
				}
				if (isEQ(wsaaFupcodeFound,SPACES)) {
					y300FlupCompare();
				}
			}
		}
		compute(wsaaSub, 0).set(sub(wsaaSub,1));
		if (isEQ(wsaaStatcodeFound,SPACES)
		|| isEQ(wsaaFupcodeFound,SPACES)) {
			goTo(GotoLabel.z380Next);
		}
		htmppfData.chdrcoy.set(hchrmpsIO.getChdrcoy());
		htmppfData.chdrnum.set(hchrmpsIO.getChdrnum());
		htmppfData.cntbranch.set(hchrmpsIO.getCntbranch());
		htmppfData.agntnum.set(hchrmpsIO.getAgntnum());
		htmppfData.cnttype.set(hchrmpsIO.getCnttype());
		htmppfData.cntcurr.set(hchrmpsIO.getCntcurr());
		htmppfData.occdate.set(hchrmpsIO.getOccdate());
		htmppfData.hrow.set(wsaaRowId[wsaaSub.toInt()]);
		y600Calculation();
		htmppfData.sumins.set(wsaaFinalSumin);
		htmppfData.singp.set(wsaaFinalSingp);
		htmppfData.annprem.set(wsaaFinalInstprem);
		goTo(GotoLabel.z390Exit);
	}

protected void z380Next()
	{
		goTo(GotoLabel.z310Start);
	}

protected void z400ProcessHpddmps()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case z410Start: {
					z410Start();
				}
				case z480Next: {
					z480Next();
				}
				case z490Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void z410Start()
	{
		SmartFileCode.execute(appVars, hpddmpsIO);
		if (isNE(hpddmpsIO.getStatuz(),varcom.oK)
		&& isNE(hpddmpsIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hpddmpsIO.getParams());
			fatalError600();
		}
		if (isEQ(hpddmpsIO.getStatuz(),varcom.endp)
		|| isNE(hpddmpsIO.getChdrcoy(),bsprIO.getCompany())
		|| isLT(hpddmpsIO.getHuwdcdte(),wsaaMthStart)) {
			printNotReady.setTrue();
			hpddmpsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.z490Exit);
		}
		hpddmpsIO.setFunction(varcom.nextr);
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(hpddmpsIO.getChdrcoy());
		chdrlifIO.setChdrnum(hpddmpsIO.getChdrnum());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		z500FeeMth();
		wsaaStatcodeFound.set(SPACES);
		wsaaSub.set(wsaaCertPolRow);
		for (wsaaY.set(1); !((isGT(wsaaY,wsaaTh596StatcodeNo))
		|| (isEQ(wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()],SPACES))
		|| statcodeFound.isTrue()); wsaaY.add(1)){
			if (isEQ(chdrlifIO.getStatcode(),wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()])) {
				statcodeFound.setTrue();
			}
		}
		if (isEQ(wsaaStatcodeFound,SPACES)) {
			goTo(GotoLabel.z480Next);
		}
		htmppfData.chdrcoy.set(chdrlifIO.getChdrcoy());
		htmppfData.chdrnum.set(chdrlifIO.getChdrnum());
		htmppfData.cntbranch.set(chdrlifIO.getCntbranch());
		htmppfData.agntnum.set(chdrlifIO.getAgntnum());
		htmppfData.cnttype.set(chdrlifIO.getCnttype());
		htmppfData.cntcurr.set(chdrlifIO.getCntcurr());
		htmppfData.occdate.set(chdrlifIO.getOccdate());
		htmppfData.hrow.set(wsaaRowId[wsaaSub.toInt()]);
		z600Calculation();
		htmppfData.sumins.set(wsaaFinalSumin);
		htmppfData.singp.set(wsaaFinalSingp);
		htmppfData.annprem.set(wsaaFinalInstprem);
		goTo(GotoLabel.z490Exit);
	}

protected void z480Next()
	{
		goTo(GotoLabel.z410Start);
	}

protected void z500FeeMth()
	{
		z510Chk();
	}

protected void z510Chk()
	{
		t5688rec.t5688Rec.set(SPACES);
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(chdrlifIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(e308);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void z550FeeMth()
	{
		z560Chk();
	}

protected void z560Chk()
	{
		t5688rec.t5688Rec.set(SPACES);
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(hchrmpsIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(hchrmpsIO.getCnttype());
		itdmIO.setItmfrm(hchrmpsIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(e308);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void z600Calculation()
	{
		z610Cal();
	}

protected void z610Cal()
	{
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(chdrlifIO.getChdrcoy());
		hpadIO.setChdrnum(chdrlifIO.getChdrnum());
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)
		&& isNE(hpadIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		wsaaTh506CrtableHeld.set(SPACES);
		wsaaCnttypeMatch.set(SPACES);
		for (wsaaSub.set(1); !(isGT(wsaaSub,wsaaTh506Sub)
		|| cnttypeMatch.isTrue()); wsaaSub.add(1)){
			if (isEQ(chdrlifIO.getCnttype(),wsaaTh506Cnttype[wsaaSub.toInt()])) {
				cnttypeMatch.setTrue();
				wsaaTh506CrtableHeld.set(wsaaTh506Crtable[wsaaSub.toInt()]);
			}
		}
		wsaaSumins9.set(ZERO);
		wsaaSingp9.set(ZERO);
		wsaaInstprem9.set(ZERO);
		if (isEQ(chdrlifIO.getValidflag(),"3")) {
			covtlnbIO.setParams(SPACES);
			covtlnbIO.setChdrcoy(chdrlifIO.getChdrcoy());
			covtlnbIO.setChdrnum(chdrlifIO.getChdrnum());
			covtlnbIO.setLife("01");
			covtlnbIO.setCoverage("01");
			covtlnbIO.setRider("00");
			covtlnbIO.setSeqnbr(ZERO);
			covtlnbIO.setFormat(covtlnbrec);
			covtlnbIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
			covtlnbIO.setStatuz(varcom.oK);
			while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
				z700ReadCovt();
			}
			
		}
		else {
			covrenqIO.setParams(SPACES);
			covrenqIO.setChdrcoy(chdrlifIO.getChdrcoy());
			covrenqIO.setChdrnum(chdrlifIO.getChdrnum());
			covrenqIO.setPlanSuffix(ZERO);
			covrenqIO.setFormat(covrenqrec);
			covrenqIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
			covrenqIO.setStatuz(varcom.oK);
			while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
				z900ReadCovr();
			}
			
			wsaaInstprem9.add(chdrlifIO.getSinstamt06());
		}
		z800CalcFee();
		wsaaCntfee.set(ZERO);
		wsaaFinalSumin.set(ZERO);
		wsaaFinalSingp.set(ZERO);
		wsaaFinalInstprem.set(ZERO);
		wsaaBillfreq9.set(chdrlifIO.getBillfreq());
		wsaaFinalSumin.set(wsaaSumins9);
		if (isNE(wsaaSingp9,ZERO)
		&& isNE(t5688rec.feemeth,SPACES)) {
			wsaaCntfee.set(mgfeelrec.mgfee);
			wsaaSingp9.add(wsaaCntfee);
		}
		wsaaFinalSingp.set(wsaaSingp9);
		if (isNE(wsaaInstprem9,ZERO)
		&& isNE(t5688rec.feemeth,SPACES)) {
			wsaaCntfee.set(mgfeelrec.mgfee);
			wsaaInstprem9.add(wsaaCntfee);
		}
		compute(wsaaInstprem9, 2).set(mult(wsaaInstprem9,wsaaBillfreq9));
		wsaaFinalInstprem.set(wsaaInstprem9);
	}

protected void z700ReadCovt()
	{
		z700Covt();
	}

protected void z700Covt()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(),chdrlifIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(),chdrlifIO.getChdrnum())
		|| isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		else {
			if (isEQ(wsaaTh506CrtableHeld,SPACES)
			&& isEQ(covtlnbIO.getRider(),"00")) {
				wsaaTh506CrtableHeld.set(covtlnbIO.getCrtable());
			}
			if (isEQ(covtlnbIO.getCrtable(),wsaaTh506CrtableHeld)) {
				wsaaSumins9.add(covtlnbIO.getSumins());
			}
			wsaaSingp9.add(covtlnbIO.getSingp());
			wsaaInstprem9.add(covtlnbIO.getInstprem());
			covtlnbIO.setFunction(varcom.nextr);
		}
	}

protected void z800CalcFee()
	{
		try {
			z810Fee();
		}
		catch (GOTOException e){
		}
	}

protected void z810Fee()
	{
		if (isEQ(t5688rec.feemeth,SPACES)) {
			goTo(GotoLabel.z890Exit);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(f151);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		if (isEQ(t5674rec.commsubr,SPACES)) {
			goTo(GotoLabel.z890Exit);
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlifIO.getCnttype());
		mgfeelrec.billfreq.set(chdrlifIO.getBillfreq());
		mgfeelrec.effdate.set(chdrlifIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlifIO.getCntcurr());
		mgfeelrec.company.set(chdrlifIO.getChdrcoy());
		callProgramX(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,varcom.oK)
		&& isNE(mgfeelrec.statuz,varcom.endp)) {
			syserrrec.statuz.set(mgfeelrec.statuz);
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			fatalError600();
		}
	}

protected void z900ReadCovr()
	{
		z910Covr();
	}

protected void z910Covr()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrenqIO.getStatuz());
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),chdrlifIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),chdrlifIO.getChdrnum())
		|| isEQ(covrenqIO.getStatuz(),varcom.endp)) {
			covrenqIO.setStatuz(varcom.endp);
		}
		else {
			if (isEQ(wsaaTh506CrtableHeld,SPACES)
			&& isEQ(covrenqIO.getRider(),"00")) {
				wsaaTh506CrtableHeld.set(covrenqIO.getCrtable());
			}
			if (isEQ(covrenqIO.getCrtable(),wsaaTh506CrtableHeld)) {
				wsaaSumins9.add(covrenqIO.getSumins());
			}
			wsaaSingp9.add(covrenqIO.getSingp());
			covrenqIO.setFunction(varcom.nextr);
		}
	}

protected void y100FindFlup()
	{
		/*Y110-FLUP*/
		initialize(wsaaFupcodeArray);
		wsaaZ.set(0);
		wsaaFupcode.set(SPACES);
		fluplnbIO.setParams(SPACES);
		fluplnbIO.setChdrcoy(hchrmpsIO.getChdrcoy());
		fluplnbIO.setChdrnum(hchrmpsIO.getChdrnum());
		fluplnbIO.setFupno(ZERO);
		fluplnbIO.setFormat(fluplnbrec);
		fluplnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluplnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluplnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(fluplnbIO.getStatuz(),varcom.endp))) {
			y200LoopAroundFlups();
		}
		
		/*Y190-EXIT*/
	}

protected void y200LoopAroundFlups()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					y210Loop();
				}
				case y280Next: {
					y280Next();
				}
				case y290Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void y210Loop()
	{
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)
		&& isNE(fluplnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		if (isNE(fluplnbIO.getChdrcoy(),hchrmpsIO.getChdrcoy())
		|| isNE(fluplnbIO.getChdrnum(),hchrmpsIO.getChdrnum())
		|| isEQ(fluplnbIO.getStatuz(),varcom.endp)) {
			fluplnbIO.setStatuz(varcom.endp);
			goTo(GotoLabel.y290Exit);
		}
		if (isEQ(fluplnbIO.getFupstat(),"R")
		|| isEQ(fluplnbIO.getFupstat(),"W")) {
			goTo(GotoLabel.y280Next);
		}
		wsaaZ.add(1);
		wsaaFupcodePr[wsaaZ.toInt()].set(fluplnbIO.getFupcode());
		wsaaFupcode.set(fluplnbIO.getFupcode());
	}

protected void y280Next()
	{
		fluplnbIO.setFunction(varcom.nextr);
	}

protected void y300FlupCompare()
	{
		/*Y310-FLUP*/
		wsaaFupcodeFound.set(SPACES);
		for (wsaaW.set(1); !((isGT(wsaaW,wsaaZ))
		|| fupcodeFound.isTrue()); wsaaW.add(1)){
			for (wsaaX.set(1); !((isGT(wsaaX,wsaaTh596FupcodeNo))
			|| (isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()],SPACES))
			|| fupcodeFound.isTrue()); wsaaX.add(1)){
				if (isEQ(wsaaFupcodePr[wsaaW.toInt()],wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()])) {
					fupcodeFound.setTrue();
				}
			}
		}
		if (isEQ(wsaaFupcodeFound,SPACES)) {
			if (isEQ(wsaaSub,wsaaTh596Sub)) {
				wsaaSub.set(wsaaPendForOtherRow);
				fupcodeFound.setTrue();
			}
			else {
				wsaaStatcodeFound.set(SPACES);
			}
		}
		/*Y390-EXIT*/
	}

protected void y400SpecialCases()
	{
		y410Sp();
	}

protected void y410Sp()
	{
		wsaaStatcodeFound.set(SPACES);
		wsaaCpStatcodeFound.set(SPACES);
		wsaaNoFupRow.set(0);
		wsaaCertPolRow.set(0);
		wsaaPendForOtherRow.set(0);
		wsaaRiskStatusComp.set(bprdIO.getSystemParam02());
		for (wsaaSub.set(1); !(isGT(wsaaSub,wsaaTh596Sub)
		|| isGT(wsaaCertPolRow,0)); wsaaSub.add(1)){
			for (wsaaY.set(1); !(isGT(wsaaY,wsaaTh596StatcodeNo)
			|| isEQ(wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()],SPACES)
			|| cpStatcodeFound.isTrue()); wsaaY.add(1)){
				if (isEQ(wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()],wsaaRiskStatusComp)) {
					cpStatcodeFound.setTrue();
				}
			}
			if (cpStatcodeFound.isTrue()) {
				if (isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][1],SPACES)) {
					wsaaCertPolRow.set(wsaaSub);
				}
			}
		}
		wsaaRiskStatusComp.set(bprdIO.getSystemParam01());
		wsaaFupcodeComp.set(bprdIO.getSystemParam03());
		for (wsaaSub.set(1); !(isGT(wsaaSub,wsaaTh596Sub)
		|| (isGT(wsaaNoFupRow,0)
		&& isGT(wsaaPendForOtherRow,0))); wsaaSub.add(1)){
			for (wsaaY.set(1); !(isGT(wsaaY,wsaaTh596StatcodeNo)
			|| isEQ(wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()],SPACES)
			|| statcodeFound.isTrue()); wsaaY.add(1)){
				if (isEQ(wsaaTh596CnRiskStat[wsaaSub.toInt()][wsaaY.toInt()],wsaaRiskStatusComp)) {
					statcodeFound.setTrue();
				}
			}
			if (statcodeFound.isTrue()) {
				if (isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][1],SPACES)) {
					if (isNE(wsaaSub,wsaaCertPolRow)) {
						wsaaNoFupRow.set(wsaaSub);
					}
				}
				else {
					for (wsaaX.set(1); !(isGT(wsaaX,wsaaTh596FupcodeNo)
					|| isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()],SPACES)
					|| isGT(wsaaPendForOtherRow,0)); wsaaX.add(1)){
						if (isEQ(wsaaTh596Fupcode[wsaaSub.toInt()][wsaaX.toInt()],wsaaFupcodeComp)) {
							wsaaPendForOtherRow.set(wsaaSub);
						}
					}
					if (isEQ(wsaaPendForOtherRow,0)) {
						wsaaStatcodeFound.set(SPACES);
					}
				}
			}
		}
	}

protected void y600Calculation()
	{
		y610Cal();
	}

protected void y610Cal()
	{
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(hchrmpsIO.getChdrcoy());
		hpadIO.setChdrnum(hchrmpsIO.getChdrnum());
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)
		&& isNE(hpadIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			syserrrec.params.set(hpadIO.getParams());
			fatalError600();
		}
		wsaaTh506CrtableHeld.set(SPACES);
		wsaaCnttypeMatch.set(SPACES);
		for (wsaaSub.set(1); !((isGT(wsaaSub,wsaaTh506Sub))
		|| cnttypeMatch.isTrue()); wsaaSub.add(1)){
			if (isEQ(hchrmpsIO.getCnttype(),wsaaTh506Cnttype[wsaaSub.toInt()])) {
				cnttypeMatch.setTrue();
				wsaaTh506CrtableHeld.set(wsaaTh506Crtable[wsaaSub.toInt()]);
			}
		}
		wsaaSumins9.set(ZERO);
		wsaaSingp9.set(ZERO);
		wsaaInstprem9.set(ZERO);
		if (isEQ(hchrmpsIO.getValidflag(),"3")) {
			covtlnbIO.setParams(SPACES);
			covtlnbIO.setChdrcoy(hchrmpsIO.getChdrcoy());
			covtlnbIO.setChdrnum(hchrmpsIO.getChdrnum());
			covtlnbIO.setLife("01");
			covtlnbIO.setCoverage("01");
			covtlnbIO.setRider("00");
			covtlnbIO.setSeqnbr(ZERO);
			covtlnbIO.setFormat(covtlnbrec);
			covtlnbIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
			covtlnbIO.setStatuz(varcom.oK);
			while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
				y700ReadCovt();
			}
			
		}
		else {
			covrenqIO.setParams(SPACES);
			covrenqIO.setChdrcoy(hchrmpsIO.getChdrcoy());
			covrenqIO.setChdrnum(hchrmpsIO.getChdrnum());
			covrenqIO.setPlanSuffix(ZERO);
			covrenqIO.setFormat(covrenqrec);
			covrenqIO.setFunction(varcom.begn);
			//performance improvement -- Anjali
			covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
			covrenqIO.setStatuz(varcom.oK);
			while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
				y900ReadCovr();
			}
			
			wsaaInstprem9.add(hchrmpsIO.getSinstamt06());
		}
		y800CalcFee();
		wsaaCntfee.set(ZERO);
		wsaaFinalSumin.set(ZERO);
		wsaaFinalSingp.set(ZERO);
		wsaaFinalInstprem.set(ZERO);
		wsaaBillfreq9.set(hchrmpsIO.getBillfreq());
		wsaaFinalSumin.set(wsaaSumins9);
		if (isNE(wsaaSingp9,ZERO)
		&& isNE(t5688rec.feemeth,SPACES)) {
			wsaaCntfee.set(mgfeelrec.mgfee);
			wsaaSingp9.add(wsaaCntfee);
		}
		wsaaFinalSingp.set(wsaaSingp9);
		if (isNE(wsaaInstprem9,ZERO)
		&& isNE(t5688rec.feemeth,SPACES)) {
			wsaaCntfee.set(mgfeelrec.mgfee);
			wsaaInstprem9.add(wsaaCntfee);
		}
		compute(wsaaInstprem9, 2).set(mult(wsaaInstprem9,wsaaBillfreq9));
		wsaaFinalInstprem.set(wsaaInstprem9);
	}

protected void y700ReadCovt()
	{
		y710Covt();
	}

protected void y710Covt()
	{
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(),varcom.oK)
		&& isNE(covtlnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covtlnbIO.getStatuz());
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(),hchrmpsIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(),hchrmpsIO.getChdrnum())
		|| isEQ(covtlnbIO.getStatuz(),varcom.endp)) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		else {
			if (isEQ(wsaaTh506CrtableHeld,SPACES)
			&& isEQ(covtlnbIO.getRider(),"00")) {
				wsaaTh506CrtableHeld.set(covtlnbIO.getCrtable());
			}
			if (isEQ(covtlnbIO.getCrtable(),wsaaTh506CrtableHeld)) {
				wsaaSumins9.add(covtlnbIO.getSumins());
			}
			wsaaSingp9.add(covtlnbIO.getSingp());
			wsaaInstprem9.add(covtlnbIO.getInstprem());
			covtlnbIO.setFunction(varcom.nextr);
		}
	}

protected void y800CalcFee()
	{
		try {
			y810Fee();
		}
		catch (GOTOException e){
		}
	}

protected void y810Fee()
	{
		if (isEQ(t5688rec.feemeth,SPACES)) {
			goTo(GotoLabel.y890Exit);
		}
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(f151);
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		if (isEQ(t5674rec.commsubr,SPACES)) {
			goTo(GotoLabel.y890Exit);
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(hchrmpsIO.getCnttype());
		mgfeelrec.billfreq.set(hchrmpsIO.getBillfreq());
		mgfeelrec.effdate.set(hchrmpsIO.getOccdate());
		mgfeelrec.cntcurr.set(hchrmpsIO.getCntcurr());
		mgfeelrec.company.set(hchrmpsIO.getChdrcoy());
		callProgramX(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz,varcom.oK)
		&& isNE(mgfeelrec.statuz,varcom.endp)) {
			syserrrec.statuz.set(mgfeelrec.statuz);
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			fatalError600();
		}
	}

protected void y900ReadCovr()
	{
		y910Covr();
	}

protected void y910Covr()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrenqIO.getStatuz());
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isNE(covrenqIO.getChdrcoy(),hchrmpsIO.getChdrcoy())
		|| isNE(covrenqIO.getChdrnum(),hchrmpsIO.getChdrnum())
		|| isEQ(covrenqIO.getStatuz(),varcom.endp)) {
			covrenqIO.setStatuz(varcom.endp);
		}
		else {
			if (isEQ(wsaaTh506CrtableHeld,SPACES)
			&& isEQ(covrenqIO.getRider(),"00")) {
				wsaaTh506CrtableHeld.set(covrenqIO.getCrtable());
			}
			if (isEQ(covrenqIO.getCrtable(),wsaaTh506CrtableHeld)) {
				wsaaSumins9.add(covrenqIO.getSumins());
			}
			wsaaSingp9.add(covrenqIO.getSingp());
			covrenqIO.setFunction(varcom.nextr);
		}
	}
}
