package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:29
 * Description:
 * Copybook name: COVTLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtlnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtlnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtlnbKey = new FixedLengthStringData(64).isAPartOf(covtlnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtlnbChdrcoy = new FixedLengthStringData(1).isAPartOf(covtlnbKey, 0);
  	public FixedLengthStringData covtlnbChdrnum = new FixedLengthStringData(8).isAPartOf(covtlnbKey, 1);
  	public FixedLengthStringData covtlnbLife = new FixedLengthStringData(2).isAPartOf(covtlnbKey, 9);
  	public FixedLengthStringData covtlnbCoverage = new FixedLengthStringData(2).isAPartOf(covtlnbKey, 11);
  	public FixedLengthStringData covtlnbRider = new FixedLengthStringData(2).isAPartOf(covtlnbKey, 13);
  	public PackedDecimalData covtlnbSeqnbr = new PackedDecimalData(3, 0).isAPartOf(covtlnbKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(covtlnbKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtlnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtlnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}