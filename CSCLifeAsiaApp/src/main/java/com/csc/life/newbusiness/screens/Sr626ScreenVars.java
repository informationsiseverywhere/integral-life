package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR626
 * @version 1.0 generated on 30/08/09 07:22
 * @author Quipoz
 */
public class Sr626ScreenVars extends SmartVarModel { 


	//MIBT-71 STARTS
	public FixedLengthStringData dataArea = new FixedLengthStringData(533); // 516 --- 533
	public FixedLengthStringData dataFields = new FixedLengthStringData(277).isAPartOf(dataArea, 0); //276 - 277
	//MIBT-71 ENDS
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData clntnames = new FixedLengthStringData(150).isAPartOf(dataFields, 18);
	public FixedLengthStringData[] clntname = FLSArrayPartOfStructure(3, 50, clntnames, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(150).isAPartOf(clntnames, 0, FILLER_REDEFINE);
	public FixedLengthStringData clntname01 = DD.clntname.copy().isAPartOf(filler,0);
	public FixedLengthStringData clntname02 = DD.clntname.copy().isAPartOf(filler,50);
	public FixedLengthStringData clntname03 = DD.clntname.copy().isAPartOf(filler,100);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,168);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,171);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,179);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,209);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,239);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,240);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,248);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,256);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,266);
	//MIBT-71 STARTS
	public FixedLengthStringData scflag = DD.action.copy().isAPartOf(dataFields,276);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 277); //60 -64
	//MIBT-71 ENDS
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData clntnamesErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] clntnameErr = FLSArrayPartOfStructure(3, 4, clntnamesErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(clntnamesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData clntname01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData clntname02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData clntname03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	//MIBT-71 STARTS
	public FixedLengthStringData scflagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60); //JJ
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 336); //180-192, 336-340
	//MIBT-71 ENDS
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData clntnamesOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] clntnameOut = FLSArrayPartOfStructure(3, 12, clntnamesOut, 0);
	public FixedLengthStringData[][] clntnameO = FLSDArrayPartOfArrayStructure(12, 1, clntnameOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(clntnamesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] clntname01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] clntname02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] clntname03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	//MIBT-71
	public FixedLengthStringData[] scflagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180); //JJ

	public FixedLengthStringData subfileArea = new FixedLengthStringData(162);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(64).isAPartOf(subfileArea, 0);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData datefrm = DD.datefrm.copyToZonedDecimal().isAPartOf(subfileFields,8);
	public ZonedDecimalData dateto = DD.dateto.copyToZonedDecimal().isAPartOf(subfileFields,16);
	public ZonedDecimalData hseqno = DD.hseqno.copyToZonedDecimal().isAPartOf(subfileFields,24);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(subfileFields,27);
	public FixedLengthStringData textone = DD.textone.copy().isAPartOf(subfileFields,31);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(24).isAPartOf(subfileArea, 64);
	public FixedLengthStringData clntnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData datefrmErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData datetoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData hseqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData textoneErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 88);
	public FixedLengthStringData[] clntnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] datefrmOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] datetoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] hseqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] textoneOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 160);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datefrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData datetoDisp = new FixedLengthStringData(10);

	public LongData Sr626screensflWritten = new LongData(0);
	public LongData Sr626screenctlWritten = new LongData(0);
	public LongData Sr626screenWritten = new LongData(0);
	public LongData Sr626protectWritten = new LongData(0);
	public GeneralTable sr626screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr626screensfl;
	}

	public Sr626ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(clntnumOut,new String[] {"01","11","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"02","12","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datefrmOut,new String[] {"04","14","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(datetoOut,new String[] {"05","15","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenSflFields = new BaseData[] {hseqno, clntnum, textone, reasoncd, datefrm, dateto};
		screenSflOutFields = new BaseData[][] {hseqnoOut, clntnumOut, textoneOut, reasoncdOut, datefrmOut, datetoOut};
		screenSflErrFields = new BaseData[] {hseqnoErr, clntnumErr, textoneErr, reasoncdErr, datefrmErr, datetoErr};
		screenSflDateFields = new BaseData[] {datefrm, dateto};
		screenSflDateErrFields = new BaseData[] {datefrmErr, datetoErr};
		screenSflDateDispFields = new BaseData[] {datefrmDisp, datetoDisp};

		//MIBT-71 STARTS
		screenFields = new BaseData[] {descrip, chdrnum, cnttype, ctypedes, rstate, pstate, cownnum, clntname01, payrnum, clntname02, agntnum, clntname03, occdate, mop, billfreq ,scflag};
		screenOutFields = new BaseData[][] {descripOut, chdrnumOut, cnttypeOut, ctypedesOut, rstateOut, pstateOut, cownnumOut, clntname01Out, payrnumOut, clntname02Out, agntnumOut, clntname03Out, occdateOut, mopOut, billfreqOut ,scflagOut};
		screenErrFields = new BaseData[] {descripErr, chdrnumErr, cnttypeErr, ctypedesErr, rstateErr, pstateErr, cownnumErr, clntname01Err, payrnumErr, clntname02Err, agntnumErr, clntname03Err, occdateErr, mopErr, billfreqErr, scflagErr};
		//MIBT-71 ENDS
		
		screenDateFields = new BaseData[] {occdate};
		screenDateErrFields = new BaseData[] {occdateErr};
		screenDateDispFields = new BaseData[] {occdateDisp};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr626screen.class;
		screenSflRecord = Sr626screensfl.class;
		screenCtlRecord = Sr626screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr626protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr626screenctl.lrec.pageSubfile);
	}
}
