/******************************************************************************
 * File Name 		: CtrspfDAO.java
 * Author			: nloganathan5
 * Creation Date	: 28 October 2016
 * Project			: Integral Life
 * Description		: The DAO Interface for CTRSPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Ctrspf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface CtrspfDAO extends BaseDAO<Ctrspf> {

	public List<Ctrspf> searchCtrspfRecord(String chdrcoy, String chdrnum) throws SQLRuntimeException;

}
