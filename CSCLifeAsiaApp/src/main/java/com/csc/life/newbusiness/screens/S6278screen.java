package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S6278screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6278ScreenVars sv = (S6278ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6278screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6278ScreenVars screenVars = (S6278ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.payrcoy.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.mandstat.setClassString("");
		screenVars.mandAmt.setClassString("");
		screenVars.timesUse.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.statdets.setClassString("");
		screenVars.branchdesc.setClassString("");
		screenVars.bankdesc.setClassString("");
		screenVars.bankaccdsc.setClassString("");
		screenVars.mandref.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.compdesc.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.curdesc.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.numsel.setClassString("");
		screenVars.facthous.setClassString("");
	}

/**
 * Clear all the variables in S6278screen
 */
	public static void clear(VarModel pv) {
		S6278ScreenVars screenVars = (S6278ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.payrcoy.clear();
		screenVars.payrnum.clear();
		screenVars.mandstat.clear();
		screenVars.mandAmt.clear();
		screenVars.timesUse.clear();
		screenVars.bankkey.clear();
		screenVars.bankacckey.clear();
		screenVars.statdets.clear();
		screenVars.branchdesc.clear();
		screenVars.bankdesc.clear();
		screenVars.bankaccdsc.clear();
		screenVars.mandref.clear();
		screenVars.ownername.clear();
		screenVars.compdesc.clear();
		screenVars.currcode.clear();
		screenVars.curdesc.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.numsel.clear();
		screenVars.facthous.clear();
	}
}
