package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5009
 * @version 1.0 generated on 30/08/09 06:29
 * @author Quipoz
 */
public class S5009ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(373);
	public FixedLengthStringData dataFields = new FixedLengthStringData(165).isAPartOf(dataArea, 0);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,2);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,10);
	public ZonedDecimalData cntsusp = DD.cntsusp.copyToZonedDecimal().isAPartOf(dataFields,13);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,33);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,41);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,71);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,88);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,89);
	public ZonedDecimalData rcptamt = DD.rcptamt.copyToZonedDecimal().isAPartOf(dataFields,136);
	public FixedLengthStringData receiptno = DD.receiptno.copy().isAPartOf(dataFields,153);
	public FixedLengthStringData transeq = DD.transeq.copy().isAPartOf(dataFields,161);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(52).isAPartOf(dataArea, 165);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData cntsuspErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData rcptamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData receiptnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData transeqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(156).isAPartOf(dataArea, 217);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] cntsuspOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] rcptamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] receiptnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] transeqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5009screenWritten = new LongData(0);
	public LongData S5009protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5009ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(receiptnoOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {transeq, chdrnum, cnttype, ctypedes, cownnum, ownername, billfreq, cntsusp, mop, instPrem, cntcurr, receiptno, rcptamt};
		screenOutFields = new BaseData[][] {transeqOut, chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, billfreqOut, cntsuspOut, mopOut, instprmOut, cntcurrOut, receiptnoOut, rcptamtOut};
		screenErrFields = new BaseData[] {transeqErr, chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, billfreqErr, cntsuspErr, mopErr, instprmErr, cntcurrErr, receiptnoErr, rcptamtErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5009screen.class;
		protectRecord = S5009protect.class;
	}

}
