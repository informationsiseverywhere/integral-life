package com.csc.life.newbusiness.procedures;



import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;	
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class P5005chk extends COBOLConvCodeModel {

		public static final String ROUTINE = QPUtilities.getThisClass();
		public int numberOfParameters = 0;
		private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("P5005CHK");
		private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		private PackedDecimalData optsDteeff = new PackedDecimalData(8, 0).setUnsigned();
		private FixedLengthStringData wsaaChkpStatuz = new FixedLengthStringData(4);
		private Syserrrec syserrrec = new Syserrrec();
		private Varcom varcom = new Varcom();
		private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
		private Clntpf clntpf = new Clntpf();
		private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);

		public P5005chk() {
			super();
		}

		/**
		* The mainline method is the default entry point to the class
		*/
	public void mainline(Object... parmArray)
		{
			wsaaChkpStatuz = convertAndSetParam(wsaaChkpStatuz, parmArray, 1);
			optsDteeff = convertAndSetParam(optsDteeff, parmArray, 0);
			try {
				mainline0000();
			}
			catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
			}
		}

	protected void mainline0000()
		{
			initialise0010();
			exit0099();
		}

	protected void initialise0010()
		{
			syserrrec.subrname.set(wsaaProg);
			lifelnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(),varcom.oK)
					&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(lifelnbIO.getParams());
				fatalError600();
			}
			
			clntpf = clntpfDAO.searchClntRecord("CN",lifelnbIO.getChdrcoy().toString(),lifelnbIO.getLifcnum().toString());
			if (null == clntpf) {
				wsaaChkpStatuz.set("NFND");
				return;
			}
			else{
					wsaaChkpStatuz.set("DFND");
			}
		
	}
	protected void exit0099()
		{
			exitProgram();
		}

	protected void fatalError600()
		{
			/*PARA*/
			syserrrec.syserrStatuz.set(syserrrec.statuz);
			callProgram(Syserr.class, syserrrec.syserrRec);
			/*EXIT*/
		}
	}

