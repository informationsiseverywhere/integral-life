package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH601
 * @version 1.0 generated on 30/08/09 07:05
 * @author Quipoz
 */
public class Sh601ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(432);
	public FixedLengthStringData dataFields = new FixedLengthStringData(80).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData lapdays = new FixedLengthStringData(16).isAPartOf(dataFields, 9);
	public ZonedDecimalData[] lapday = ZDArrayPartOfStructure(8, 2, 0, lapdays, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(lapdays, 0, FILLER_REDEFINE);
	public ZonedDecimalData lapday01 = DD.lapday.copyToZonedDecimal().isAPartOf(filler,0);
	public ZonedDecimalData lapday02 = DD.lapday.copyToZonedDecimal().isAPartOf(filler,2);
	public ZonedDecimalData lapday03 = DD.lapday.copyToZonedDecimal().isAPartOf(filler,4);
	public ZonedDecimalData lapday04 = DD.lapday.copyToZonedDecimal().isAPartOf(filler,6);
	public ZonedDecimalData lapday05 = DD.lapday.copyToZonedDecimal().isAPartOf(filler,8);
	public ZonedDecimalData lapday06 = DD.lapday.copyToZonedDecimal().isAPartOf(filler,10);
	public ZonedDecimalData lapday07 = DD.lapday.copyToZonedDecimal().isAPartOf(filler,12);
	public ZonedDecimalData lapday08 = DD.lapday.copyToZonedDecimal().isAPartOf(filler,14);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData weekss = new FixedLengthStringData(20).isAPartOf(dataFields, 60);
	public ZonedDecimalData[] weeks = ZDArrayPartOfStructure(10, 2, 0, weekss, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(weekss, 0, FILLER_REDEFINE);
	public ZonedDecimalData weeks01 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData weeks02 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,2);
	public ZonedDecimalData weeks03 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,4);
	public ZonedDecimalData weeks04 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,6);
	public ZonedDecimalData weeks05 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,8);
	public ZonedDecimalData weeks06 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,10);
	public ZonedDecimalData weeks07 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,12);
	public ZonedDecimalData weeks08 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,14);
	public ZonedDecimalData weeks09 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,16);
	public ZonedDecimalData weeks10 = DD.weeks.copyToZonedDecimal().isAPartOf(filler1,18);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(88).isAPartOf(dataArea, 80);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData lapdaysErr = new FixedLengthStringData(32).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData[] lapdayErr = FLSArrayPartOfStructure(8, 4, lapdaysErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(32).isAPartOf(lapdaysErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData lapday01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData lapday02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData lapday03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData lapday04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData lapday05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData lapday06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData lapday07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData lapday08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData weekssErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData[] weeksErr = FLSArrayPartOfStructure(10, 4, weekssErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(weekssErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData weeks01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData weeks02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData weeks03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData weeks04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData weeks05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData weeks06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData weeks07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData weeks08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData weeks09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData weeks10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(264).isAPartOf(dataArea, 168);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData lapdaysOut = new FixedLengthStringData(96).isAPartOf(outputIndicators, 24);
	public FixedLengthStringData[] lapdayOut = FLSArrayPartOfStructure(8, 12, lapdaysOut, 0);
	public FixedLengthStringData[][] lapdayO = FLSDArrayPartOfArrayStructure(12, 1, lapdayOut, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(96).isAPartOf(lapdaysOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] lapday01Out = FLSArrayPartOfStructure(12, 1, filler4, 0);
	public FixedLengthStringData[] lapday02Out = FLSArrayPartOfStructure(12, 1, filler4, 12);
	public FixedLengthStringData[] lapday03Out = FLSArrayPartOfStructure(12, 1, filler4, 24);
	public FixedLengthStringData[] lapday04Out = FLSArrayPartOfStructure(12, 1, filler4, 36);
	public FixedLengthStringData[] lapday05Out = FLSArrayPartOfStructure(12, 1, filler4, 48);
	public FixedLengthStringData[] lapday06Out = FLSArrayPartOfStructure(12, 1, filler4, 60);
	public FixedLengthStringData[] lapday07Out = FLSArrayPartOfStructure(12, 1, filler4, 72);
	public FixedLengthStringData[] lapday08Out = FLSArrayPartOfStructure(12, 1, filler4, 84);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData weekssOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 144);
	public FixedLengthStringData[] weeksOut = FLSArrayPartOfStructure(10, 12, weekssOut, 0);
	public FixedLengthStringData[][] weeksO = FLSDArrayPartOfArrayStructure(12, 1, weeksOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(120).isAPartOf(weekssOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] weeks01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] weeks02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] weeks03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] weeks04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] weeks05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] weeks06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] weeks07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] weeks08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] weeks09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData[] weeks10Out = FLSArrayPartOfStructure(12, 1, filler5, 108);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh601screenWritten = new LongData(0);
	public LongData Sh601protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh601ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, lapday01, lapday02, lapday03, lapday04, lapday05, lapday06, lapday07, lapday08, weeks01, weeks02, weeks03, weeks04, weeks05, weeks06, weeks07, weeks08, weeks09, weeks10};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, lapday01Out, lapday02Out, lapday03Out, lapday04Out, lapday05Out, lapday06Out, lapday07Out, lapday08Out, weeks01Out, weeks02Out, weeks03Out, weeks04Out, weeks05Out, weeks06Out, weeks07Out, weeks08Out, weeks09Out, weeks10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, lapday01Err, lapday02Err, lapday03Err, lapday04Err, lapday05Err, lapday06Err, lapday07Err, lapday08Err, weeks01Err, weeks02Err, weeks03Err, weeks04Err, weeks05Err, weeks06Err, weeks07Err, weeks08Err, weeks09Err, weeks10Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh601screen.class;
		protectRecord = Sh601protect.class;
	}

}
