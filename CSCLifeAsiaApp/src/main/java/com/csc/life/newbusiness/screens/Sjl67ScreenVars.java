package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sjl67ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(418);
	public FixedLengthStringData dataFields = new FixedLengthStringData(62).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields, 1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields, 9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields, 39);
	public FixedLengthStringData resultcds = new FixedLengthStringData(9).isAPartOf(dataFields, 44);
	public FixedLengthStringData[] resultcd = FLSArrayPartOfStructure(9, 1, resultcds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(9).isAPartOf(resultcds, 0, FILLER_REDEFINE);
	public FixedLengthStringData resultcd01 = DD.resultcd.copy().isAPartOf(filler1, 0);
	public FixedLengthStringData resultcd02 = DD.resultcd.copy().isAPartOf(filler1, 1);
	public FixedLengthStringData resultcd03 = DD.resultcd.copy().isAPartOf(filler1, 2);
	public FixedLengthStringData resultcd04 = DD.resultcd.copy().isAPartOf(filler1, 3);
	public FixedLengthStringData resultcd05 = DD.resultcd.copy().isAPartOf(filler1, 4);
	public FixedLengthStringData resultcd06 = DD.resultcd.copy().isAPartOf(filler1, 5);
	public FixedLengthStringData resultcd07 = DD.resultcd.copy().isAPartOf(filler1, 6);
	public FixedLengthStringData resultcd08 = DD.resultcd.copy().isAPartOf(filler1, 7);
	public FixedLengthStringData resultcd09 = DD.resultcd.copy().isAPartOf(filler1, 8);
	public FixedLengthStringData rcverors = new FixedLengthStringData(9).isAPartOf(dataFields, 53);
	public FixedLengthStringData[] rcveror = FLSArrayPartOfStructure(9, 1, rcverors, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(9).isAPartOf(rcverors, 0, FILLER_REDEFINE);
	public FixedLengthStringData rcveror01 = DD.rcveror.copy().isAPartOf(filler2, 0);
	public FixedLengthStringData rcveror02 = DD.rcveror.copy().isAPartOf(filler2, 1);
	public FixedLengthStringData rcveror03 = DD.rcveror.copy().isAPartOf(filler2, 2);
	public FixedLengthStringData rcveror04 = DD.rcveror.copy().isAPartOf(filler2, 3);
	public FixedLengthStringData rcveror05 = DD.rcveror.copy().isAPartOf(filler2, 4);
	public FixedLengthStringData rcveror06 = DD.rcveror.copy().isAPartOf(filler2, 5);
	public FixedLengthStringData rcveror07 = DD.rcveror.copy().isAPartOf(filler2, 6);
	public FixedLengthStringData rcveror08 = DD.rcveror.copy().isAPartOf(filler2, 7);
	public FixedLengthStringData rcveror09 = DD.rcveror.copy().isAPartOf(filler2, 8);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(92).isAPartOf(dataArea, 62);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData resultcdsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] resultcdErr = FLSArrayPartOfStructure(9, 4, resultcdsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(36).isAPartOf(resultcdsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData resultcd01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData resultcd02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData resultcd03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData resultcd04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData resultcd05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData resultcd06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData resultcd07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData resultcd08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData resultcd09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData rcverorsErr = new FixedLengthStringData(36).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] rcverorErr = FLSArrayPartOfStructure(9, 4, rcverorsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(36).isAPartOf(rcverorsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rcveror01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData rcveror02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData rcveror03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData rcveror04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData rcveror05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData rcveror06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData rcveror07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData rcveror08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData rcveror09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(264).isAPartOf(dataArea, 154);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData resultcdsOut = new FixedLengthStringData(108).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] resultcdOut = FLSArrayPartOfStructure(9, 12, resultcdsOut, 0);
	public FixedLengthStringData[][] resultcdO = FLSDArrayPartOfArrayStructure(12, 1, resultcdOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(108).isAPartOf(resultcdsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] resultcd01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] resultcd02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] resultcd03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData[] resultcd04Out = FLSArrayPartOfStructure(12, 1, filler5, 36);
	public FixedLengthStringData[] resultcd05Out = FLSArrayPartOfStructure(12, 1, filler5, 48);
	public FixedLengthStringData[] resultcd06Out = FLSArrayPartOfStructure(12, 1, filler5, 60);
	public FixedLengthStringData[] resultcd07Out = FLSArrayPartOfStructure(12, 1, filler5, 72);
	public FixedLengthStringData[] resultcd08Out = FLSArrayPartOfStructure(12, 1, filler5, 84);
	public FixedLengthStringData[] resultcd09Out = FLSArrayPartOfStructure(12, 1, filler5, 96);
	public FixedLengthStringData rcverorsOut = new FixedLengthStringData(108).isAPartOf(outputIndicators, 156);
	public FixedLengthStringData[] rcverorOut = FLSArrayPartOfStructure(9, 12, rcverorsOut, 0);
	public FixedLengthStringData[][] rcverorO = FLSDArrayPartOfArrayStructure(12, 1, rcverorOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(108).isAPartOf(rcverorsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rcveror01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] rcveror02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] rcveror03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] rcveror04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] rcveror05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] rcveror06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] rcveror07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] rcveror08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] rcveror09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sjl67screenWritten = new LongData(0);
	public LongData Sjl67protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sjl67ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(resultcd01Out,new String[] { "01", null, "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(resultcd02Out,new String[] { "02", null, "-02", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(resultcd03Out,new String[] { "03", null, "-03", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(resultcd04Out,new String[] { "04", null, "-04", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(resultcd05Out,new String[] { "05", null, "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(resultcd06Out,new String[] { "06", null, "-06", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(resultcd07Out,new String[] { "07", null, "-07", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(resultcd08Out,new String[] { "08", null, "-08", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(resultcd09Out,new String[] { "09", null, "-09", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(rcveror01Out,new String[] { "10", null, "-10", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(rcveror02Out,new String[] { "11", null, "-11", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(rcveror03Out,new String[] { "12", null, "-12", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(rcveror04Out,new String[] { "13", null, "-13", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(rcveror05Out,new String[] { "14", null, "-14", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(rcveror06Out,new String[] { "15", null, "-15", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(rcveror07Out,new String[] { "16", null, "-16", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(rcveror08Out,new String[] { "17", null, "-17", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(rcveror09Out,new String[] { "18", null, "-18", null, null, null, null, null, null, null, null, null });

		screenFields = new BaseData[] { company, tabl, item, longdesc, resultcd01, resultcd02, resultcd03, resultcd04,
				resultcd05, resultcd06, resultcd07, resultcd08, resultcd09, rcveror01, rcveror02, rcveror03, rcveror04,
				rcveror05, rcveror06, rcveror07, rcveror08, rcveror09};
		screenOutFields = new BaseData[][] { companyOut, tablOut, itemOut, longdescOut, resultcd01Out,
				resultcd02Out, resultcd03Out, resultcd04Out, resultcd05Out, resultcd06Out, resultcd07Out, resultcd08Out, 
				resultcd09Out, rcveror01Out, rcveror02Out, rcveror03Out, rcveror04Out, rcveror05Out, rcveror06Out, rcveror07Out,
				rcveror08Out, rcveror09Out};
		screenErrFields = new BaseData[] { companyErr, tablErr, itemErr, longdescErr, resultcd01Err,
				resultcd02Err, resultcd03Err, resultcd04Err, resultcd05Err, resultcd06Err, resultcd07Err, resultcd08Err, 
				resultcd09Err, rcveror01Err, rcveror02Err, rcveror03Err, rcveror04Err, rcveror05Err, rcveror06Err, 
				rcveror07Err, rcveror08Err, rcveror09Err};
		screenDateFields = new BaseData[] {  };
		screenDateErrFields = new BaseData[] { };
		screenDateDispFields = new BaseData[] { };

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl67screen.class;
		protectRecord = Sjl67protect.class;
	}

}
