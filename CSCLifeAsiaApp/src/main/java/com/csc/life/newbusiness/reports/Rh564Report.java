package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH564.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh564Report extends SMARTReportLayout { 

	private FixedLengthStringData aracde = new FixedLengthStringData(3);
	private FixedLengthStringData aradesc = new FixedLengthStringData(30);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private ZonedDecimalData cntcount01 = new ZonedDecimalData(6, 0);
	private ZonedDecimalData cntcount02 = new ZonedDecimalData(6, 0);
	private ZonedDecimalData cntcount03 = new ZonedDecimalData(6, 0);
	private ZonedDecimalData cntcount04 = new ZonedDecimalData(6, 0);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22);
	private FixedLengthStringData datetext01 = new FixedLengthStringData(19);
	private FixedLengthStringData datetext02 = new FixedLengthStringData(19);
	private FixedLengthStringData datetext03 = new FixedLengthStringData(19);
	private ZonedDecimalData instprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData instprem01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData instprem02 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData pcamtind01 = new FixedLengthStringData(1);
	private FixedLengthStringData pcamtind02 = new FixedLengthStringData(1);
	private FixedLengthStringData pcamtind03 = new FixedLengthStringData(1);
	private FixedLengthStringData pcamtind04 = new FixedLengthStringData(1);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData singp01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData singp02 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData tgtpcnt01 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData tgtpcnt02 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData tgtpcnt03 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData tgtpcnt04 = new ZonedDecimalData(5, 2);
	private RPGTimeData time = new RPGTimeData();
	private FixedLengthStringData zparnths01 = new FixedLengthStringData(1);
	private FixedLengthStringData zparnths02 = new FixedLengthStringData(1);
	private FixedLengthStringData zparnths03 = new FixedLengthStringData(1);
	private FixedLengthStringData zparnths04 = new FixedLengthStringData(1);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh564Report() {
		super();
	}


	/**
	 * Print the XML for Rh564d01
	 */
	public void printRh564d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datetext01.setFieldName("datetext01");
		datetext01.setInternal(subString(recordData, 1, 19));
		instprem01.setFieldName("instprem01");
		instprem01.setInternal(subString(recordData, 20, 17));
		tgtpcnt01.setFieldName("tgtpcnt01");
		tgtpcnt01.setInternal(subString(recordData, 37, 5));
		pcamtind01.setFieldName("pcamtind01");
		pcamtind01.setInternal(subString(recordData, 42, 1));
		cntcount01.setFieldName("cntcount01");
		cntcount01.setInternal(subString(recordData, 43, 6));
		datetext02.setFieldName("datetext02");
		datetext02.setInternal(subString(recordData, 49, 19));
		instprem02.setFieldName("instprem02");
		instprem02.setInternal(subString(recordData, 68, 17));
		tgtpcnt02.setFieldName("tgtpcnt02");
		tgtpcnt02.setInternal(subString(recordData, 85, 5));
		pcamtind02.setFieldName("pcamtind02");
		pcamtind02.setInternal(subString(recordData, 90, 1));
		cntcount02.setFieldName("cntcount02");
		cntcount02.setInternal(subString(recordData, 91, 6));
		zparnths01.setFieldName("zparnths01");
		zparnths01.setInternal(subString(recordData, 97, 1));
		singp01.setFieldName("singp01");
		singp01.setInternal(subString(recordData, 98, 17));
		zparnths02.setFieldName("zparnths02");
		zparnths02.setInternal(subString(recordData, 115, 1));
		tgtpcnt03.setFieldName("tgtpcnt03");
		tgtpcnt03.setInternal(subString(recordData, 116, 5));
		pcamtind03.setFieldName("pcamtind03");
		pcamtind03.setInternal(subString(recordData, 121, 1));
		cntcount03.setFieldName("cntcount03");
		cntcount03.setInternal(subString(recordData, 122, 6));
		datetext03.setFieldName("datetext03");
		datetext03.setInternal(subString(recordData, 128, 19));
		zparnths03.setFieldName("zparnths03");
		zparnths03.setInternal(subString(recordData, 147, 1));
		singp02.setFieldName("singp02");
		singp02.setInternal(subString(recordData, 148, 17));
		zparnths04.setFieldName("zparnths04");
		zparnths04.setInternal(subString(recordData, 165, 1));
		tgtpcnt04.setFieldName("tgtpcnt04");
		tgtpcnt04.setInternal(subString(recordData, 166, 5));
		pcamtind04.setFieldName("pcamtind04");
		pcamtind04.setInternal(subString(recordData, 171, 1));
		cntcount04.setFieldName("cntcount04");
		cntcount04.setInternal(subString(recordData, 172, 6));
		printLayout("Rh564d01",			// Record name
			new BaseData[]{			// Fields:
				datetext01,
				instprem01,
				tgtpcnt01,
				pcamtind01,
				cntcount01,
				datetext02,
				instprem02,
				tgtpcnt02,
				pcamtind02,
				cntcount02,
				zparnths01,
				singp01,
				zparnths02,
				tgtpcnt03,
				pcamtind03,
				cntcount03,
				datetext03,
				zparnths03,
				singp02,
				zparnths04,
				tgtpcnt04,
				pcamtind04,
				cntcount04
			}
		);

	}

	/**
	 * Print the XML for Rh564h01
	 */
	public void printRh564h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 1, 10));
		datetexc.setFieldName("datetexc");
		datetexc.setInternal(subString(recordData, 11, 22));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 33, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 36, 30));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 66, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 67, 30));
		printLayout("Rh564h01",			// Record name
			new BaseData[]{			// Fields:
				sdate,
				datetexc,
				time,
				pagnbr,
				currcode,
				currencynm,
				company,
				companynm
			}
		);

		currentPrintLine.set(9);
	}

	/**
	 * Print the XML for Rh564h02
	 */
	public void printRh564h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 1, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 3, 30));
		printLayout("Rh564h02",			// Record name
			new BaseData[]{			// Fields:
				branch,
				branchnm
			}
		);

	}

	/**
	 * Print the XML for Rh564h03
	 */
	public void printRh564h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		aracde.setFieldName("aracde");
		aracde.setInternal(subString(recordData, 1, 3));
		aradesc.setFieldName("aradesc");
		aradesc.setInternal(subString(recordData, 4, 30));
		printLayout("Rh564h03",			// Record name
			new BaseData[]{			// Fields:
				aracde,
				aradesc
			}
		);

	}

	/**
	 * Print the XML for Rh564h06
	 */
	public void printRh564h06(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 1, 3));
		cntdesc.setFieldName("cntdesc");
		cntdesc.setInternal(subString(recordData, 4, 30));
		printLayout("Rh564h06",			// Record name
			new BaseData[]{			// Fields:
				cnttype,
				cntdesc
			}
		);

	}

	/**
	 * Print the XML for Rh564h07
	 */
	public void printRh564h07(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("Rh564h07",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh564t01
	 */
	public void printRh564t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		instprem.setFieldName("instprem");
		instprem.setInternal(subString(recordData, 1, 17));
		tgtpcnt01.setFieldName("tgtpcnt01");
		tgtpcnt01.setInternal(subString(recordData, 18, 5));
		pcamtind01.setFieldName("pcamtind01");
		pcamtind01.setInternal(subString(recordData, 23, 1));
		cntcount01.setFieldName("cntcount01");
		cntcount01.setInternal(subString(recordData, 24, 6));
		zparnths01.setFieldName("zparnths01");
		zparnths01.setInternal(subString(recordData, 30, 1));
		singp.setFieldName("singp");
		singp.setInternal(subString(recordData, 31, 17));
		zparnths02.setFieldName("zparnths02");
		zparnths02.setInternal(subString(recordData, 48, 1));
		tgtpcnt02.setFieldName("tgtpcnt02");
		tgtpcnt02.setInternal(subString(recordData, 49, 5));
		pcamtind02.setFieldName("pcamtind02");
		pcamtind02.setInternal(subString(recordData, 54, 1));
		cntcount02.setFieldName("cntcount02");
		cntcount02.setInternal(subString(recordData, 55, 6));
		printLayout("Rh564t01",			// Record name
			new BaseData[]{			// Fields:
				instprem,
				tgtpcnt01,
				pcamtind01,
				cntcount01,
				zparnths01,
				singp,
				zparnths02,
				tgtpcnt02,
				pcamtind02,
				cntcount02
			}
		);

	}


}
