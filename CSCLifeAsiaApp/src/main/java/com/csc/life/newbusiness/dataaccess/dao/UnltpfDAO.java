package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Unltpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface UnltpfDAO extends BaseDAO<Unltpf> {
	public List<Unltpf> readUnltpf(Unltpf Unltpf);
	public List<Unltpf> getFundUnltpf(Unltpf Unltpf);//ILIFE-4970 
	
	public void insertUnltpfList(List<Unltpf> insertList);
}