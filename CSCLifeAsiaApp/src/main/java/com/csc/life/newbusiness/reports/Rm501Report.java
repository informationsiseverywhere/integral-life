package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RM501.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rm501Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData genldesc = new FixedLengthStringData(30);
	private FixedLengthStringData mthldesc = new FixedLengthStringData(10);
	private ZonedDecimalData occpct = new ZonedDecimalData(5, 2);
	private ZonedDecimalData occpct02 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData occprem01 = new ZonedDecimalData(9, 0);
	private ZonedDecimalData occprem02 = new ZonedDecimalData(9, 0);
	private ZonedDecimalData occprem03 = new ZonedDecimalData(9, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData year = new ZonedDecimalData(4, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rm501Report() {
		super();
	}


	/**
	 * Print the XML for Rm501d01
	 */
	public void printRm501d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		genldesc.setFieldName("genldesc");
		genldesc.setInternal(subString(recordData, 1, 30));
		occpct.setFieldName("occpct");
		occpct.setInternal(subString(recordData, 31, 5));
		occprem01.setFieldName("occprem01");
		occprem01.setInternal(subString(recordData, 36, 9));
		occprem02.setFieldName("occprem02");
		occprem02.setInternal(subString(recordData, 45, 9));
		occprem03.setFieldName("occprem03");
		occprem03.setInternal(subString(recordData, 54, 9));
		occpct02.setFieldName("occpct02");
		occpct02.setInternal(subString(recordData, 63, 5));
		printLayout("Rm501d01",			// Record name
			new BaseData[]{			// Fields:
				genldesc,
				occpct,
				occprem01,
				occprem02,
				occprem03,
				occpct02
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rm501h01
	 */
	public void printRm501h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		mthldesc.setFieldName("mthldesc");
		mthldesc.setInternal(subString(recordData, 1, 10));
		year.setFieldName("year");
		year.setInternal(subString(recordData, 11, 4));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 15, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 16, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 46, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 56, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 58, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rm501h01",			// Record name
			new BaseData[]{			// Fields:
				mthldesc,
				year,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(9);
	}

	/**
	 * Print the XML for Rm501h02
	 */
	public void printRm501h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		genldesc.setFieldName("genldesc");
		genldesc.setInternal(subString(recordData, 1, 30));
		printLayout("Rm501h02",			// Record name
			new BaseData[]{			// Fields:
				genldesc
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for Rm501s01
	 */
	public void printRm501s01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		occpct.setFieldName("occpct");
		occpct.setInternal(subString(recordData, 1, 5));
		occprem01.setFieldName("occprem01");
		occprem01.setInternal(subString(recordData, 6, 9));
		occprem02.setFieldName("occprem02");
		occprem02.setInternal(subString(recordData, 15, 9));
		occprem03.setFieldName("occprem03");
		occprem03.setInternal(subString(recordData, 24, 9));
		occpct02.setFieldName("occpct02");
		occpct02.setInternal(subString(recordData, 33, 5));
		printLayout("Rm501s01",			// Record name
			new BaseData[]{			// Fields:
				occpct,
				occprem01,
				occprem02,
				occprem03,
				occpct02
			}
		);

		currentPrintLine.add(2);
	}


}
