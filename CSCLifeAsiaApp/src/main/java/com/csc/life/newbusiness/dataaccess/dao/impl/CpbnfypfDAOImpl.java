/******************************************************************************
 * File Name 		: CpbnfypfDAOImpl.java
 * Author			: qzhang52
 * Creation Date	: 10 December 2018
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for CPBNFYPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao.impl;

import com.csc.life.newbusiness.dataaccess.dao.CpbnfypfDAO;
import com.csc.life.newbusiness.dataaccess.model.Cpbnfypf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CpbnfypfDAOImpl extends BaseDAOImpl<Cpbnfypf> implements CpbnfypfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(CpbnfypfDAOImpl.class);

	public List<Cpbnfypf> searchCpbnfypfRecord(String coy, String chdrNum) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, BNYCLT, TRANNO, VALIDFLAG, ");
		sqlSelect.append("CURRFR, CURRTO, BNYRLN, BNYCD, BNYPC, ");
		sqlSelect.append("TERMID, USER_T, TRTM, TRDT, EFFDATE, ");
		sqlSelect.append("USRPRF, JOBNM, DATIME, BNYTYPE, CLTRELN, ");
		sqlSelect.append("RELTO, SELFIND, REVCFLG, ENDDATE, SEQUENCE, PAYMMETH, BANKKEY, BANKACCKEY, AMOUNT ");
		sqlSelect.append("FROM CPBNFYPF WHERE VALIDFLAG = '1' AND SEQUENCE = '01' AND CHDRCOY=? AND CHDRNUM =? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, BNYCLT ASC, UNIQUE_NUMBER ASC");

		PreparedStatement psCpbnfypfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlCpbnfypfRs = null;
		List<Cpbnfypf> outputList = new ArrayList<Cpbnfypf>();

		try {
			psCpbnfypfSelect.setString(1, coy);
			psCpbnfypfSelect.setString(2, chdrNum);
			sqlCpbnfypfRs = executeQuery(psCpbnfypfSelect);

			while (sqlCpbnfypfRs.next()) {

				Cpbnfypf cpbnfypf = new Cpbnfypf();

				cpbnfypf.setUniqueNumber(sqlCpbnfypfRs.getInt("UNIQUE_NUMBER"));
				cpbnfypf.setChdrcoy(getCharFromString(sqlCpbnfypfRs,"CHDRCOY"));
				cpbnfypf.setChdrnum(sqlCpbnfypfRs.getString("CHDRNUM"));
				cpbnfypf.setBnyclt(sqlCpbnfypfRs.getString("BNYCLT"));
				cpbnfypf.setTranno(sqlCpbnfypfRs.getInt("TRANNO"));
				cpbnfypf.setValidflag(getCharFromString(sqlCpbnfypfRs, "VALIDFLAG"));
				cpbnfypf.setCurrfr(sqlCpbnfypfRs.getInt("CURRFR"));
				cpbnfypf.setCurrto(sqlCpbnfypfRs.getInt("CURRTO"));
				cpbnfypf.setBnyrln(sqlCpbnfypfRs.getString("BNYRLN"));
				cpbnfypf.setBnycd(getCharFromString(sqlCpbnfypfRs,"BNYCD"));
				cpbnfypf.setBnypc(sqlCpbnfypfRs.getBigDecimal("BNYPC"));
				cpbnfypf.setTermid(sqlCpbnfypfRs.getString("TERMID"));
				cpbnfypf.setUserT(sqlCpbnfypfRs.getInt("USER_T"));
				cpbnfypf.setTrtm(sqlCpbnfypfRs.getInt("TRTM"));
				cpbnfypf.setTrdt(sqlCpbnfypfRs.getInt("TRDT"));
				cpbnfypf.setEffdate(sqlCpbnfypfRs.getInt("EFFDATE"));
				cpbnfypf.setUsrprf(sqlCpbnfypfRs.getString("USRPRF"));
				cpbnfypf.setJobnm(sqlCpbnfypfRs.getString("JOBNM"));
				cpbnfypf.setDatime(sqlCpbnfypfRs.getDate("DATIME"));
				cpbnfypf.setBnytype(sqlCpbnfypfRs.getString("BNYTYPE"));
				cpbnfypf.setCltreln(sqlCpbnfypfRs.getString("CLTRELN"));
				cpbnfypf.setRelto(getCharFromString(sqlCpbnfypfRs,"RELTO"));
				cpbnfypf.setSelfind(getCharFromString(sqlCpbnfypfRs,"SELFIND"));
				cpbnfypf.setRevcflg(getCharFromString(sqlCpbnfypfRs,"REVCFLG"));
				cpbnfypf.setEnddate(sqlCpbnfypfRs.getInt("ENDDATE"));
				cpbnfypf.setSequence(sqlCpbnfypfRs.getString("SEQUENCE"));
				cpbnfypf.setPaymmeth(sqlCpbnfypfRs.getString("PAYMMETH"));
				cpbnfypf.setBankkey(sqlCpbnfypfRs.getString("BANKKEY"));
				cpbnfypf.setBankacckey(sqlCpbnfypfRs.getString("BANKACCKEY"));
				cpbnfypf.setAmount(sqlCpbnfypfRs.getBigDecimal("AMOUNT"));

				outputList.add(cpbnfypf);
			}

		} catch (SQLException e) {
				LOGGER.error("searchCpbnfypfRecord()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(psCpbnfypfSelect, sqlCpbnfypfRs);
		}

		return outputList;
	}
	
	private Character getCharFromString(ResultSet sqlpCovt1Rs , String columnName) throws SQLException{
		
		Character characterOutput = null;
		
		String stringOutput = sqlpCovt1Rs.getString(columnName);
		
		if(null != stringOutput){
			characterOutput = stringOutput.charAt(0);
		}
		
		return characterOutput;
	}

	public List<Cpbnfypf> getCpbnfyByCoyAndNumAndSeq(String coy, String chdrNum, String sequence) throws SQLRuntimeException{

		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, BNYCLT, TRANNO, VALIDFLAG, BNYRLN, BNYCD, BNYPC, CURRTO, CURRFR, TERMID, USER_T, ");
        sb.append("TRTM, TRDT, EFFDATE, USRPRF, JOBNM, DATIME, BNYTYPE, CLTRELN, RELTO, SELFIND, REVCFLG, ENDDATE, SEQUENCE, PAYMMETH, BANKKEY, BANKACCKEY, AMOUNT FROM CPBNFYPF WHERE VALIDFLAG = '1' AND CHDRCOY=? ");
        sb.append("AND CHDRNUM =? AND SEQUENCE = ? ORDER BY CHDRCOY ASC, CHDRNUM ASC, BNYTYPE ASC, BNYCLT ASC, UNIQUE_NUMBER ASC");
		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		List<Cpbnfypf> outputList = new ArrayList<Cpbnfypf>();

		try {

			ps.setString(1, coy);
			ps.setString(2, chdrNum);
			ps.setString(3, sequence);
			rs = ps.executeQuery();

			while (rs.next()) {

				Cpbnfypf cpbnfypf = new Cpbnfypf();

				cpbnfypf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
				cpbnfypf.setChdrcoy(getCharFromString(rs,"CHDRCOY"));
				cpbnfypf.setChdrnum(rs.getString("CHDRNUM"));
				cpbnfypf.setBnyclt(rs.getString("BNYCLT"));
				cpbnfypf.setTranno(rs.getInt("TRANNO"));
				cpbnfypf.setValidflag(getCharFromString(rs, "VALIDFLAG"));
				cpbnfypf.setBnyrln(rs.getString("BNYRLN"));
				cpbnfypf.setBnycd(getCharFromString(rs,"BNYCD"));
				cpbnfypf.setBnypc(rs.getBigDecimal("BNYPC"));	
				cpbnfypf.setAmount(rs.getBigDecimal("AMOUNT"));
				cpbnfypf.setCurrto(rs.getInt("CURRTO"));
				cpbnfypf.setCurrfr(rs.getInt("CURRFR"));
				cpbnfypf.setTermid(rs.getString("TERMID"));
				cpbnfypf.setUserT(rs.getInt("USER_T"));
				cpbnfypf.setTrtm(rs.getInt("TRTM"));
				cpbnfypf.setTrdt(rs.getInt("TRDT"));
				cpbnfypf.setEffdate(rs.getInt("EFFDATE"));
				cpbnfypf.setUsrprf(rs.getString("USRPRF"));
				cpbnfypf.setJobnm(rs.getString("JOBNM"));
				cpbnfypf.setDatime(rs.getDate("DATIME"));
				cpbnfypf.setBnytype(rs.getString("BNYTYPE"));
				cpbnfypf.setCltreln(rs.getString("CLTRELN"));
				cpbnfypf.setRelto(getCharFromString(rs,"RELTO"));
				cpbnfypf.setSelfind(getCharFromString(rs,"SELFIND"));
				cpbnfypf.setRevcflg(getCharFromString(rs,"REVCFLG"));
				cpbnfypf.setEnddate(rs.getInt("ENDDATE"));
				cpbnfypf.setSequence(rs.getString("SEQUENCE"));
				cpbnfypf.setPaymmeth(rs.getString("PAYMMETH"));
				cpbnfypf.setBankkey(rs.getString("BANKKEY"));
				cpbnfypf.setBankacckey(rs.getString("BANKACCKEY"));
				cpbnfypf.setAmount(rs.getBigDecimal("AMOUNT"));
				outputList.add(cpbnfypf);
			}

		} catch (SQLException e) {
				LOGGER.error("getBnfymnaByCoyAndNum()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return outputList;
	}
	

	
	public List<Cpbnfypf> getCpbnfymnaByKeyAndTranno(Cpbnfypf cpbnfypf) throws SQLRuntimeException{

		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, BNYCLT, TRANNO, VALIDFLAG, BNYRLN, BNYCD, BNYPC,AMOUNT, CURRTO, CURRFR, TERMID, USER_T, ");
        sb.append("TRTM, TRDT, EFFDATE, USRPRF, JOBNM, DATIME, BNYTYPE, CLTRELN, RELTO, SELFIND, REVCFLG, ENDDATE, SEQUENCE, PAYMMETH, BANKKEY, BANKACCKEY FROM CPBNFYPF WHERE VALIDFLAG = '1' AND CHDRCOY=? ");
        sb.append("AND CHDRNUM =? AND BNYCLT = ? AND BNYTYPE = ? AND TRANNO != ?  ORDER BY CHDRCOY ASC, CHDRNUM ASC, BNYTYPE ASC, BNYCLT ASC, UNIQUE_NUMBER ASC");
		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		Cpbnfypf result = null;
        List<Cpbnfypf> list = new ArrayList<Cpbnfypf>();
		try {
			ps.setString(1, cpbnfypf.getChdrcoy().toString());
			ps.setString(2, cpbnfypf.getChdrnum());
			ps.setString(3, cpbnfypf.getBnyclt());
			ps.setString(4, cpbnfypf.getBnytype());
			ps.setInt(5, cpbnfypf.getTranno());

			rs = ps.executeQuery();

			if (rs.next()) {

				result = new Cpbnfypf();

				result.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
				result.setChdrcoy(getCharFromString(rs,"CHDRCOY"));
				result.setChdrnum(rs.getString("CHDRNUM"));
				result.setBnyclt(rs.getString("BNYCLT"));
				result.setTranno(rs.getInt("TRANNO"));
				result.setValidflag(getCharFromString(rs, "VALIDFLAG"));
				result.setBnyrln(rs.getString("BNYRLN"));
				result.setBnycd(getCharFromString(rs,"BNYCD"));
				result.setBnypc(rs.getBigDecimal("BNYPC"));				
				result.setAmount(rs.getBigDecimal("AMOUNT"));
				result.setCurrto(rs.getInt("CURRTO"));
				result.setCurrfr(rs.getInt("CURRFR"));
				result.setTermid(rs.getString("TERMID"));
				result.setUserT(rs.getInt("USER_T"));
				result.setTrtm(rs.getInt("TRTM"));
				result.setTrdt(rs.getInt("TRDT"));
				result.setEffdate(rs.getInt("EFFDATE"));
				result.setUsrprf(rs.getString("USRPRF"));
				result.setJobnm(rs.getString("JOBNM"));
				result.setDatime(rs.getDate("DATIME"));
				result.setBnytype(rs.getString("BNYTYPE"));
				result.setCltreln(rs.getString("CLTRELN"));
				result.setRelto(getCharFromString(rs,"RELTO"));
				result.setSelfind(getCharFromString(rs,"SELFIND"));
				result.setRevcflg(getCharFromString(rs,"REVCFLG"));
				result.setEnddate(rs.getInt("ENDDATE"));
				result.setSequence(rs.getString("SEQUENCE"));
				result.setPaymmeth(rs.getString("PAYMMETH"));
				result.setBankkey(rs.getString("BANKKEY"));/*ICIL-11*/
				result.setBankacckey(rs.getString("BANKACCKEY"));
				list.add(result);
			}

		} catch (SQLException e) {
				LOGGER.error("getBnfymnaByKeyAndTranno()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return list;
	}

	public List<Cpbnfypf> getCpbnfypfByKey(Cpbnfypf cpbnfypf) throws SQLRuntimeException{
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, BNYCLT, TRANNO, VALIDFLAG, BNYRLN, BNYCD, BNYPC,AMOUNT, CURRTO, CURRFR, TERMID, USER_T, ");
		sb.append("TRTM, TRDT, EFFDATE, USRPRF, JOBNM, DATIME, BNYTYPE, CLTRELN, RELTO, SELFIND, REVCFLG, ENDDATE, SEQUENCE, PAYMMETH, BANKKEY, BANKACCKEY FROM CPBNFYPF WHERE VALIDFLAG = '1' AND SEQUENCE = '01' AND CHDRCOY=? ");
		sb.append("AND CHDRNUM =? AND BNYCLT = ? AND BNYTYPE = ? AND TRANNO != ?  ORDER BY CHDRCOY ASC, CHDRNUM ASC, BNYTYPE ASC, BNYCLT ASC, UNIQUE_NUMBER ASC");
		PreparedStatement ps = null;
		ResultSet rs = null;
		Cpbnfypf result = null;
		List<Cpbnfypf> list = new ArrayList<Cpbnfypf>();
		try {
			ps = getConnection().prepareStatement(sb.toString());
			ps.setString(1, cpbnfypf.getChdrcoy().toString());
			ps.setString(2, cpbnfypf.getChdrnum());
			ps.setString(3, cpbnfypf.getBnyclt());
			ps.setString(4, cpbnfypf.getBnytype());
			ps.setInt(5, cpbnfypf.getTranno());
			rs = ps.executeQuery();
			//ps.getConnection().commit();
			if (rs.next()) {
				result = new Cpbnfypf();
				result.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
				result.setChdrcoy(getCharFromString(rs,"CHDRCOY"));
				result.setChdrnum(rs.getString("CHDRNUM"));
				result.setBnyclt(rs.getString("BNYCLT"));
				result.setTranno(rs.getInt("TRANNO"));
				result.setValidflag(getCharFromString(rs, "VALIDFLAG"));
				result.setBnyrln(rs.getString("BNYRLN"));
				result.setBnycd(getCharFromString(rs,"BNYCD"));
				result.setBnypc(rs.getBigDecimal("BNYPC"));
				result.setAmount(rs.getBigDecimal("AMOUNT"));
				result.setCurrto(rs.getInt("CURRTO"));
				result.setCurrfr(rs.getInt("CURRFR"));
				result.setTermid(rs.getString("TERMID"));
				result.setUserT(rs.getInt("USER_T"));
				result.setTrtm(rs.getInt("TRTM"));
				result.setTrdt(rs.getInt("TRDT"));
				result.setEffdate(rs.getInt("EFFDATE"));
				result.setUsrprf(rs.getString("USRPRF"));
				result.setJobnm(rs.getString("JOBNM"));
				result.setDatime(rs.getDate("DATIME"));
				result.setBnytype(rs.getString("BNYTYPE"));
				result.setCltreln(rs.getString("CLTRELN"));
				result.setRelto(getCharFromString(rs,"RELTO"));
				result.setSelfind(getCharFromString(rs,"SELFIND"));
				result.setRevcflg(getCharFromString(rs,"REVCFLG"));
				result.setEnddate(rs.getInt("ENDDATE"));
				result.setSequence(rs.getString("SEQUENCE"));
				result.setPaymmeth(rs.getString("PAYMMETH"));
				result.setBankkey(rs.getString("BANKKEY"));/*ICIL-11*/
				result.setBankacckey(rs.getString("BANKACCKEY"));
				list.add(result);
			}

		} catch (SQLException e) {
			LOGGER.error("getBnfymnaByKeyAndTranno()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return list;
	}

	public int updateCpbnfymnaByUnqueNum(Cpbnfypf cpbnfypf) throws SQLRuntimeException{

		StringBuilder sb = new StringBuilder("UPDATE CPBNFYPF SET VALIDFLAG=?,CURRTO=? WHERE UNIQUE_NUMBER = ? ");
		
		PreparedStatement ps = getPrepareStatement(sb.toString());

		int result = 0;
		
		try {
			ps.setString(1, cpbnfypf.getValidflag().toString());
			ps.setInt(2, cpbnfypf.getCurrto());
			ps.setLong(3,cpbnfypf.getUniqueNumber());
		

			result = ps.executeUpdate();


		} catch (SQLException e) {
				LOGGER.error("updateBnfymnaByKeyAndTranno()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

		return result;
	}
	
	public int insertCpbnfypf(Cpbnfypf cpbnfypf){
		int result = 0;
		StringBuilder sb = new StringBuilder("INSERT INTO CPBNFYPF(CHDRCOY,CHDRNUM,BNYCLT,TRANNO,VALIDFLAG,CURRFR,CURRTO,BNYRLN,BNYCD,BNYPC,AMOUNT,TERMID,USER_T,TRTM,TRDT,EFFDATE,");
		sb.append("USRPRF,JOBNM,DATIME,BNYTYPE,CLTRELN,RELTO,SELFIND,REVCFLG,ENDDATE, SEQUENCE, PAYMMETH, BANKKEY, BANKACCKEY) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		
		try{
			ps.setString(1, cpbnfypf.getChdrcoy().toString());
			ps.setString(2, cpbnfypf.getChdrnum());
			ps.setString(3, cpbnfypf.getBnyclt().trim());
			ps.setInt(4, cpbnfypf.getTranno());
			ps.setString(5, cpbnfypf.getValidflag().toString());
			ps.setInt(6, cpbnfypf.getCurrfr());
			ps.setInt(7, cpbnfypf.getCurrto());
			ps.setString(8, cpbnfypf.getBnyrln() == null ? "": cpbnfypf.getBnyrln());
			ps.setString(9, cpbnfypf.getBnycd().toString());
			ps.setBigDecimal(10, cpbnfypf.getBnypc());
			ps.setBigDecimal(11,cpbnfypf.getAmount());
			
			ps.setString(12, cpbnfypf.getTermid());
			ps.setInt(13, cpbnfypf.getUserT());
			ps.setInt(14, cpbnfypf.getTrtm());
			ps.setInt(15, cpbnfypf.getTrdt());
			ps.setInt(16, cpbnfypf.getEffdate());
			ps.setString(17, getUsrprf());
			ps.setString(18, getJobnm());
			ps.setTimestamp(19, getDatime());
			ps.setString(20, cpbnfypf.getBnytype());
			ps.setString(21, cpbnfypf.getCltreln());
			ps.setString(22, cpbnfypf.getRelto().toString());
			ps.setString(23, cpbnfypf.getSelfind().toString());
			ps.setString(24, cpbnfypf.getRevcflg().toString());
			ps.setInt(25, cpbnfypf.getEnddate());
			ps.setString(26, cpbnfypf.getSequence());
			ps.setString(27, cpbnfypf.getPaymmeth());
			ps.setString(28, cpbnfypf.getBankkey());
			ps.setString(29, cpbnfypf.getBankacckey());
			result = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("insertCpbnfypf()", e);
			throw new SQLRuntimeException(e);
     	} finally {
		    close(ps, null);
     	}
		return result;
	}

	@Override
	public int getByChdrnum(String chdrNum) {
		
		int result=0;
		StringBuilder sb = new StringBuilder("SELECT COUNT(*) AS NUM FROM CPBNFYPF WHERE CHDRNUM =? ");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		
		try {

			
			ps.setString(1, chdrNum);

			rs = ps.executeQuery();

			if (rs.next()) {
				result=rs.getInt("NUM");
			}

		} catch (SQLException e) {
				LOGGER.error("getByChdrnum", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return result;
	}

	

	
	
	
}
