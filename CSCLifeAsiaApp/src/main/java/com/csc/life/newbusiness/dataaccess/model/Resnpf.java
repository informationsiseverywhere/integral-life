package com.csc.life.newbusiness.dataaccess.model;


public class Resnpf {

	private long uniqueNumber;
	private String chdrcoy;
	private String chdrnum;
	private String reasoncd;
	private String resndesc;
	private Integer tranno;
	private String trancde;
	private String termid;
	private Integer trdt;
	private Integer trtm;
	private Integer userT;
	public long getUniqueNumber() {
		return uniqueNumber;
	}
	public void setUniqueNumber(long uniqueNumber) {
		this.uniqueNumber = uniqueNumber;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getReasoncd() {
		return reasoncd;
	}
	public void setReasoncd(String reasoncd) {
		this.reasoncd = reasoncd;
	}
	public String getResndesc() {
		return resndesc;
	}
	public void setResndesc(String resndesc) {
		this.resndesc = resndesc;
	}
	public Integer getTranno() {
		return tranno;
	}
	public void setTranno(Integer tranno) {
		this.tranno = tranno;
	}
	public String getTrancde() {
		return trancde;
	}
	public void setTrancde(String trancde) {
		this.trancde = trancde;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public Integer getTrdt() {
		return trdt;
	}
	public void setTrdt(Integer trdt) {
		this.trdt = trdt;
	}
	public Integer getTrtm() {
		return trtm;
	}
	public void setTrtm(Integer trtm) {
		this.trtm = trtm;
	}
	public Integer getUserT() {
		return userT;
	}
	public void setUserT(Integer userT) {
		this.userT = userT;
	}

}
