package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * Copybook name: Tjl05REC Date: 20 December 2019 Author: vdivisala
 */
public class Tjl05rec extends ExternalData {

	// *******************************
	// Attribute Declarations
	// *******************************

	public FixedLengthStringData tjl05Rec = new FixedLengthStringData(50);
	public FixedLengthStringData billfreqs = new FixedLengthStringData(20).isAPartOf(tjl05Rec, 0);
	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(10, 2, billfreqs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01 = DD.billfreq.copy().isAPartOf(filler1, 0);
	public FixedLengthStringData billfreq02 = DD.billfreq.copy().isAPartOf(filler1, 2);
	public FixedLengthStringData billfreq03 = DD.billfreq.copy().isAPartOf(filler1, 4);
	public FixedLengthStringData billfreq04 = DD.billfreq.copy().isAPartOf(filler1, 6);
	public FixedLengthStringData billfreq05 = DD.billfreq.copy().isAPartOf(filler1, 8);
	public FixedLengthStringData billfreq06 = DD.billfreq.copy().isAPartOf(filler1, 10);
	public FixedLengthStringData billfreq07 = DD.billfreq.copy().isAPartOf(filler1, 12);
	public FixedLengthStringData billfreq08 = DD.billfreq.copy().isAPartOf(filler1, 14);
	public FixedLengthStringData billfreq09 = DD.billfreq.copy().isAPartOf(filler1, 16);
	public FixedLengthStringData billfreq10 = DD.billfreq.copy().isAPartOf(filler1, 18);
	
	public FixedLengthStringData mops = new FixedLengthStringData(10).isAPartOf(tjl05Rec, 20);
	public FixedLengthStringData[] mop = FLSArrayPartOfStructure(10, 1, mops, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(10).isAPartOf(mops, 0, FILLER_REDEFINE);
	public FixedLengthStringData mop01 = DD.mop.copy().isAPartOf(filler2, 0);
	public FixedLengthStringData mop02 = DD.mop.copy().isAPartOf(filler2, 1);
	public FixedLengthStringData mop03 = DD.mop.copy().isAPartOf(filler2, 2);
	public FixedLengthStringData mop04 = DD.mop.copy().isAPartOf(filler2, 3);
	public FixedLengthStringData mop05 = DD.mop.copy().isAPartOf(filler2, 4);
	public FixedLengthStringData mop06 = DD.mop.copy().isAPartOf(filler2, 5);
	public FixedLengthStringData mop07 = DD.mop.copy().isAPartOf(filler2, 6);
	public FixedLengthStringData mop08 = DD.mop.copy().isAPartOf(filler2, 7);
	public FixedLengthStringData mop09 = DD.mop.copy().isAPartOf(filler2, 8);
	public FixedLengthStringData mop10 = DD.mop.copy().isAPartOf(filler2, 9);
	
	public FixedLengthStringData fwcondts = new FixedLengthStringData(10).isAPartOf(tjl05Rec, 30);
	public FixedLengthStringData[] fwcondt = FLSArrayPartOfStructure(10, 1, fwcondts, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(fwcondts, 0, FILLER_REDEFINE);
	public FixedLengthStringData fwcondt01 = DD.fwcondt.copy().isAPartOf(filler3, 0);
	public FixedLengthStringData fwcondt02 = DD.fwcondt.copy().isAPartOf(filler3, 1);
	public FixedLengthStringData fwcondt03 = DD.fwcondt.copy().isAPartOf(filler3, 2);
	public FixedLengthStringData fwcondt04 = DD.fwcondt.copy().isAPartOf(filler3, 3);
	public FixedLengthStringData fwcondt05 = DD.fwcondt.copy().isAPartOf(filler3, 4);
	public FixedLengthStringData fwcondt06 = DD.fwcondt.copy().isAPartOf(filler3, 5);
	public FixedLengthStringData fwcondt07 = DD.fwcondt.copy().isAPartOf(filler3, 6);
	public FixedLengthStringData fwcondt08 = DD.fwcondt.copy().isAPartOf(filler3, 7);
	public FixedLengthStringData fwcondt09 = DD.fwcondt.copy().isAPartOf(filler3, 8);
	public FixedLengthStringData fwcondt10 = DD.fwcondt.copy().isAPartOf(filler3, 9);
	
	public FixedLengthStringData concommflgs = new FixedLengthStringData(10).isAPartOf(tjl05Rec, 40);
	public FixedLengthStringData[] concommflg = FLSArrayPartOfStructure(10, 1, concommflgs, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(concommflgs, 0, FILLER_REDEFINE);
	public FixedLengthStringData concommflg01 = DD.concommflg.copy().isAPartOf(filler4, 0);
	public FixedLengthStringData concommflg02 = DD.concommflg.copy().isAPartOf(filler4, 1);
	public FixedLengthStringData concommflg03 = DD.concommflg.copy().isAPartOf(filler4, 2);
	public FixedLengthStringData concommflg04 = DD.concommflg.copy().isAPartOf(filler4, 3);
	public FixedLengthStringData concommflg05 = DD.concommflg.copy().isAPartOf(filler4, 4);
	public FixedLengthStringData concommflg06 = DD.concommflg.copy().isAPartOf(filler4, 5);
	public FixedLengthStringData concommflg07 = DD.concommflg.copy().isAPartOf(filler4, 6);
	public FixedLengthStringData concommflg08 = DD.concommflg.copy().isAPartOf(filler4, 7);
	public FixedLengthStringData concommflg09 = DD.concommflg.copy().isAPartOf(filler4, 8);
	public FixedLengthStringData concommflg10 = DD.concommflg.copy().isAPartOf(filler4, 9);

	public void initialize() {
		COBOLFunctions.initialize(tjl05Rec);
	}

	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			tjl05Rec.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}
}