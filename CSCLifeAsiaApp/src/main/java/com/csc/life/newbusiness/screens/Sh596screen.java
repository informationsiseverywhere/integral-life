package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh596screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh596ScreenVars sv = (Sh596ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh596screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh596ScreenVars screenVars = (Sh596ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.language.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.trcode01.setClassString("");
		screenVars.trcode02.setClassString("");
		screenVars.trcode03.setClassString("");
		screenVars.trcode04.setClassString("");
		screenVars.trcode05.setClassString("");
		screenVars.trcode06.setClassString("");
		screenVars.trcode07.setClassString("");
		screenVars.trcode08.setClassString("");
		screenVars.trcode09.setClassString("");
		screenVars.trcode10.setClassString("");
		screenVars.trcode11.setClassString("");
		screenVars.trcode12.setClassString("");
		screenVars.trcode13.setClassString("");
		screenVars.trcode14.setClassString("");
		screenVars.trcode15.setClassString("");
		screenVars.trcode16.setClassString("");
		screenVars.trcode17.setClassString("");
		screenVars.trcode18.setClassString("");
		screenVars.trcode19.setClassString("");
		screenVars.trcode20.setClassString("");
		screenVars.trcode21.setClassString("");
		screenVars.trcode22.setClassString("");
		screenVars.trcode23.setClassString("");
		screenVars.trcode24.setClassString("");
		screenVars.trcode25.setClassString("");
		screenVars.trcode26.setClassString("");
		screenVars.trcode27.setClassString("");
		screenVars.trcode28.setClassString("");
		screenVars.cnRiskStat01.setClassString("");
		screenVars.cnRiskStat02.setClassString("");
		screenVars.cnRiskStat03.setClassString("");
		screenVars.cnRiskStat04.setClassString("");
		screenVars.cnRiskStat05.setClassString("");
		screenVars.cnRiskStat06.setClassString("");
		screenVars.cnRiskStat07.setClassString("");
		screenVars.cnRiskStat08.setClassString("");
		screenVars.cnRiskStat09.setClassString("");
		screenVars.cnRiskStat10.setClassString("");
		screenVars.cnRiskStat11.setClassString("");
		screenVars.cnRiskStat12.setClassString("");
		screenVars.fupcdes01.setClassString("");
		screenVars.fupcdes02.setClassString("");
		screenVars.fupcdes03.setClassString("");
		screenVars.fupcdes04.setClassString("");
		screenVars.fupcdes05.setClassString("");
		screenVars.fupcdes06.setClassString("");
		screenVars.fupcdes07.setClassString("");
		screenVars.fupcdes08.setClassString("");
		screenVars.fupcdes09.setClassString("");
		screenVars.fupcdes10.setClassString("");
		screenVars.fupcdes11.setClassString("");
		screenVars.fupcdes12.setClassString("");
		screenVars.fupcdes13.setClassString("");
		screenVars.fupcdes14.setClassString("");
		screenVars.fupcdes15.setClassString("");
		screenVars.fupcdes16.setClassString("");
		screenVars.fupcdes17.setClassString("");
		screenVars.fupcdes18.setClassString("");
		screenVars.fupcdes19.setClassString("");
		screenVars.fupcdes20.setClassString("");
		screenVars.fupcdes21.setClassString("");
		screenVars.fupcdes22.setClassString("");
		screenVars.fupcdes23.setClassString("");
		screenVars.fupcdes24.setClassString("");
		screenVars.fupcdes25.setClassString("");
		screenVars.fupcdes26.setClassString("");
		screenVars.fupcdes27.setClassString("");
		screenVars.fupcdes28.setClassString("");
		screenVars.fupcdes29.setClassString("");
		screenVars.fupcdes30.setClassString("");
		screenVars.fupcdes31.setClassString("");
		screenVars.fupcdes32.setClassString("");
		screenVars.fupcdes33.setClassString("");
		screenVars.fupcdes34.setClassString("");
		screenVars.fupcdes35.setClassString("");
		screenVars.fupcdes36.setClassString("");
		screenVars.fupcdes37.setClassString("");
		screenVars.fupcdes38.setClassString("");
		screenVars.fupcdes39.setClassString("");
		screenVars.fupcdes40.setClassString("");
		screenVars.fupcdes41.setClassString("");
		screenVars.fupcdes42.setClassString("");
		screenVars.fupcdes43.setClassString("");
		screenVars.fupcdes44.setClassString("");
		screenVars.fupcdes45.setClassString("");
		screenVars.fupcdes46.setClassString("");
		screenVars.fupcdes47.setClassString("");
		screenVars.fupcdes48.setClassString("");
		screenVars.fupcdes49.setClassString("");
		screenVars.fupcdes50.setClassString("");
		screenVars.fupcdes51.setClassString("");
		screenVars.fupcdes52.setClassString("");
		screenVars.fupcdes53.setClassString("");
		screenVars.fupcdes54.setClassString("");
		screenVars.fupcdes55.setClassString("");
		screenVars.fupcdes56.setClassString("");
		screenVars.fupcdes57.setClassString("");
		screenVars.fupcdes58.setClassString("");
		screenVars.fupcdes59.setClassString("");
		screenVars.fupcdes60.setClassString("");
		screenVars.hcondate.setClassString("");
	}

/**
 * Clear all the variables in Sh596screen
 */
	public static void clear(VarModel pv) {
		Sh596ScreenVars screenVars = (Sh596ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.language.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.trcode01.clear();
		screenVars.trcode02.clear();
		screenVars.trcode03.clear();
		screenVars.trcode04.clear();
		screenVars.trcode05.clear();
		screenVars.trcode06.clear();
		screenVars.trcode07.clear();
		screenVars.trcode08.clear();
		screenVars.trcode09.clear();
		screenVars.trcode10.clear();
		screenVars.trcode11.clear();
		screenVars.trcode12.clear();
		screenVars.trcode13.clear();
		screenVars.trcode14.clear();
		screenVars.trcode15.clear();
		screenVars.trcode16.clear();
		screenVars.trcode17.clear();
		screenVars.trcode18.clear();
		screenVars.trcode19.clear();
		screenVars.trcode20.clear();
		screenVars.trcode21.clear();
		screenVars.trcode22.clear();
		screenVars.trcode23.clear();
		screenVars.trcode24.clear();
		screenVars.trcode25.clear();
		screenVars.trcode26.clear();
		screenVars.trcode27.clear();
		screenVars.trcode28.clear();
		screenVars.cnRiskStat01.clear();
		screenVars.cnRiskStat02.clear();
		screenVars.cnRiskStat03.clear();
		screenVars.cnRiskStat04.clear();
		screenVars.cnRiskStat05.clear();
		screenVars.cnRiskStat06.clear();
		screenVars.cnRiskStat07.clear();
		screenVars.cnRiskStat08.clear();
		screenVars.cnRiskStat09.clear();
		screenVars.cnRiskStat10.clear();
		screenVars.cnRiskStat11.clear();
		screenVars.cnRiskStat12.clear();
		screenVars.fupcdes01.clear();
		screenVars.fupcdes02.clear();
		screenVars.fupcdes03.clear();
		screenVars.fupcdes04.clear();
		screenVars.fupcdes05.clear();
		screenVars.fupcdes06.clear();
		screenVars.fupcdes07.clear();
		screenVars.fupcdes08.clear();
		screenVars.fupcdes09.clear();
		screenVars.fupcdes10.clear();
		screenVars.fupcdes11.clear();
		screenVars.fupcdes12.clear();
		screenVars.fupcdes13.clear();
		screenVars.fupcdes14.clear();
		screenVars.fupcdes15.clear();
		screenVars.fupcdes16.clear();
		screenVars.fupcdes17.clear();
		screenVars.fupcdes18.clear();
		screenVars.fupcdes19.clear();
		screenVars.fupcdes20.clear();
		screenVars.fupcdes21.clear();
		screenVars.fupcdes22.clear();
		screenVars.fupcdes23.clear();
		screenVars.fupcdes24.clear();
		screenVars.fupcdes25.clear();
		screenVars.fupcdes26.clear();
		screenVars.fupcdes27.clear();
		screenVars.fupcdes28.clear();
		screenVars.fupcdes29.clear();
		screenVars.fupcdes30.clear();
		screenVars.fupcdes31.clear();
		screenVars.fupcdes32.clear();
		screenVars.fupcdes33.clear();
		screenVars.fupcdes34.clear();
		screenVars.fupcdes35.clear();
		screenVars.fupcdes36.clear();
		screenVars.fupcdes37.clear();
		screenVars.fupcdes38.clear();
		screenVars.fupcdes39.clear();
		screenVars.fupcdes40.clear();
		screenVars.fupcdes41.clear();
		screenVars.fupcdes42.clear();
		screenVars.fupcdes43.clear();
		screenVars.fupcdes44.clear();
		screenVars.fupcdes45.clear();
		screenVars.fupcdes46.clear();
		screenVars.fupcdes47.clear();
		screenVars.fupcdes48.clear();
		screenVars.fupcdes49.clear();
		screenVars.fupcdes50.clear();
		screenVars.fupcdes51.clear();
		screenVars.fupcdes52.clear();
		screenVars.fupcdes53.clear();
		screenVars.fupcdes54.clear();
		screenVars.fupcdes55.clear();
		screenVars.fupcdes56.clear();
		screenVars.fupcdes57.clear();
		screenVars.fupcdes58.clear();
		screenVars.fupcdes59.clear();
		screenVars.fupcdes60.clear();
		screenVars.hcondate.clear();
	}
}
