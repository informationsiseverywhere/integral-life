/*
 * File: T6640pt.java
 * Date: 30 August 2009 2:28:23
 * Author: Quipoz Limited
 * 
 * Class transformed from T6640PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6640.
*
*
*
***********************************************************************
* </pre>
*/
public class T6640pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("General Traditional Business Details           S6640");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(77);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 12, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 22);
	private FixedLengthStringData filler5 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 27, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 36);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 47);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(48);
	private FixedLengthStringData filler7 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Dates effective:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 22);
	private FixedLengthStringData filler8 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine003, 32, FILLER).init("  to");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 38);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(68);
	private FixedLengthStringData filler9 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Reserve Method:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine004, 28);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine004, 39, FILLER).init("Low Cost Subroutine:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine004, 60);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(64);
	private FixedLengthStringData filler12 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" Reversionary Bonus Method:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 28);
	private FixedLengthStringData filler13 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine005, 39, FILLER).init("Low Cost Rider:");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine005, 60);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(29);
	private FixedLengthStringData filler15 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Bonus/Dividend Allocation:");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 28);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(32);
	private FixedLengthStringData filler16 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine007, 0, FILLER).init(" Surrender Bonus Method:");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 28);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(76);
	private FixedLengthStringData filler17 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" Cash Dividend Method:");
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 28);
	private FixedLengthStringData filler18 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private FixedLengthStringData filler19 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine008, 39, FILLER).init("Cash Dividend Withdrawal Method:");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 72);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(41);
	private FixedLengthStringData filler20 = new FixedLengthStringData(40).isAPartOf(wsaaPrtLine009, 0, FILLER).init(" Rider can Outdate main Coverage (Y/N):");
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 40);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6640rec t6640rec = new T6640rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6640pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6640rec.t6640Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(t6640rec.reserveMeth);
		fieldNo008.set(t6640rec.lcsubr);
		fieldNo009.set(t6640rec.revBonusMeth);
		fieldNo010.set(t6640rec.lcrider);
		fieldNo011.set(t6640rec.zbondivalc);
		fieldNo012.set(t6640rec.surrenderBonusMethod);
		fieldNo013.set(t6640rec.zcshdivmth);
		fieldNo014.set(t6640rec.zdivwdrmth);
		fieldNo015.set(t6640rec.zrmandind);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
