/******************************************************************************
 * File Name 		: AckdpfDAO.java
 * Author			: sbatra9
 * Creation Date	: 17 April 2020
 * Project			: Integral Life
 * Description		: The DAO Interface for ACKDPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Ackdpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AckdpfDAO extends BaseDAO<Ackdpf> {

	public List<Ackdpf> searchAckdpfRecord(String tableId, String memName,
			int batchExtractSize, int batchID);
}
