package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5004
 * @version 1.0 generated on 30/08/09 06:29
 * @author Quipoz
 */
public class S5004ScreenVars extends SmartVarModel { 


	//public FixedLengthStringData dataArea = new FixedLengthStringData(1848);
	//public FixedLengthStringData dataFields = new FixedLengthStringData(696).isAPartOf(dataArea, 0);
	

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData addtype = DD.addtype.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,48);
	public FixedLengthStringData aiind = DD.aiind.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData apcind = DD.apcind.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData asgnind = DD.asgnind.copy().isAPartOf(dataFields,60);
	public ZonedDecimalData billcd = DD.billcd.copyToZonedDecimal().isAPartOf(dataFields,61);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,69);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,72);
	public FixedLengthStringData bnfying = DD.bnfying.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData campaign = DD.campaign.copy().isAPartOf(dataFields,75);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,81);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,89);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,91);
	public FixedLengthStringData comind = DD.comind.copy().isAPartOf(dataFields,94);
	public FixedLengthStringData conprosal = DD.conprosal.copy().isAPartOf(dataFields,95);
	public FixedLengthStringData crcind = DD.crcind.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData ctrsind = DD.ctrsind.copy().isAPartOf(dataFields,97);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,98);
	public FixedLengthStringData dlvrmode = DD.dlvrmode.copy().isAPartOf(dataFields,99);
	public FixedLengthStringData fupflg = DD.fupflg.copy().isAPartOf(dataFields,103);
	public FixedLengthStringData grpind = DD.grpind.copy().isAPartOf(dataFields,104);
	public FixedLengthStringData heading = DD.heading.copy().isAPartOf(dataFields,105);
	public ZonedDecimalData hoissdte = DD.hoissdte.copyToZonedDecimal().isAPartOf(dataFields,135);
	public ZonedDecimalData hpropdte = DD.hpropdte.copyToZonedDecimal().isAPartOf(dataFields,143);
	public ZonedDecimalData hprrcvdt = DD.hprrcvdt.copyToZonedDecimal().isAPartOf(dataFields,151);
	public ZonedDecimalData huwdcdte = DD.huwdcdte.copyToZonedDecimal().isAPartOf(dataFields,159);
	public ZonedDecimalData incomeSeqNo = DD.incseqno.copyToZonedDecimal().isAPartOf(dataFields,167);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData indxflg = DD.indxflg.copy().isAPartOf(dataFields,170);
	public FixedLengthStringData jownind = DD.jownind.copy().isAPartOf(dataFields,171);
	public FixedLengthStringData lrepnum = DD.lrepnum.copy().isAPartOf(dataFields,172);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,192);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,193);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,201);
	public FixedLengthStringData ownersel = DD.ownersel.copy().isAPartOf(dataFields,248);
	public FixedLengthStringData payind = DD.payind.copy().isAPartOf(dataFields,258);
	public ZonedDecimalData plansuff = DD.plansuff.copyToZonedDecimal().isAPartOf(dataFields,259);
	public FixedLengthStringData premStatDesc = DD.pstatdesc.copy().isAPartOf(dataFields,261);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,291);
	public FixedLengthStringData reptype = DD.reptype.copy().isAPartOf(dataFields,294);
	public FixedLengthStringData sbrdesc = DD.sbrdesc.copy().isAPartOf(dataFields,296);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(dataFields,336);
	public FixedLengthStringData statdsc = DD.statdets.copy().isAPartOf(dataFields,338);
	public FixedLengthStringData stcal = DD.stcal.copy().isAPartOf(dataFields,368);
	public FixedLengthStringData stcbl = DD.stcbl.copy().isAPartOf(dataFields,371);
	public FixedLengthStringData stccl = DD.stccl.copy().isAPartOf(dataFields,374);
	public FixedLengthStringData stcdl = DD.stcdl.copy().isAPartOf(dataFields,377);
	public FixedLengthStringData stcel = DD.stcel.copy().isAPartOf(dataFields,380);
	public FixedLengthStringData transhist = DD.transhist.copy().isAPartOf(dataFields,383);
	public ZonedDecimalData ttmprcdte = DD.ttmprcdte.copyToZonedDecimal().isAPartOf(dataFields,384);
	public FixedLengthStringData ttmprcno = DD.ttmprcno.copy().isAPartOf(dataFields,392);
	public FixedLengthStringData znfopt = DD.znfopt.copy().isAPartOf(dataFields,407);
	public FixedLengthStringData zsredtrm = DD.zsredtrm.copy().isAPartOf(dataFields,410);
	public FixedLengthStringData reqntype = DD.reqntype.copy().isAPartOf(dataFields,411);//1 ILIFE-2472
	public FixedLengthStringData zdpind = DD.zdpind.copy().isAPartOf(dataFields,412);//1 ILIFE-2472
	public FixedLengthStringData zdcind = DD.zdcind.copy().isAPartOf(dataFields,413);//1 ILIFE-2472
	public FixedLengthStringData fatcastatus=DD.fatcastatus.copy().isAPartOf(dataFields,414);
	public FixedLengthStringData fatcastatusdesc=DD.fatcastatusdesc.copy().isAPartOf(dataFields,420);
	public FixedLengthStringData zctaxind = DD.zctaxind.copy().isAPartOf(dataFields,460);//ALS-313
	public FixedLengthStringData schmno = DD.schemekey.copy().isAPartOf(dataFields,461);//ALS-317
	public FixedLengthStringData schmnme = DD.schemeName.copy().isAPartOf(dataFields,469);//ALS-317
	public FixedLengthStringData billday = DD.billday.copy().isAPartOf(dataFields,529);//ILIFE-3735
	public FixedLengthStringData zroloverind = DD.Zroloverind.copy().isAPartOf(dataFields,531);//ILIFE-3735
	public FixedLengthStringData nlgflg = DD.keya.copy().isAPartOf(dataFields,532);//ILIFE-3997
	public FixedLengthStringData refundOverpay = new FixedLengthStringData(1).isAPartOf(dataFields,533);//fwang3 China localization
	//ICIL-147 start
	public FixedLengthStringData bnkout = DD.bnkout.copy().isAPartOf(dataFields,534);
	public FixedLengthStringData bnksm = DD.bnksm.copy().isAPartOf(dataFields,554);
	public FixedLengthStringData bnktel = DD.bnktel.copy().isAPartOf(dataFields,564);
	public FixedLengthStringData bnkoutname = DD.bnkoutname.copy().isAPartOf(dataFields,574);
	public FixedLengthStringData bnksmname = DD.bnksmname.copy().isAPartOf(dataFields,621);
	public FixedLengthStringData bnktelname = DD.bnktelname.copy().isAPartOf(dataFields,668);
	//ICIL-147 end
	public FixedLengthStringData zmultOwner = DD.zmultOwner.copy().isAPartOf(dataFields,715);//ALS-4555
	
	public FixedLengthStringData rfundflg = DD.rfundflg.copy().isAPartOf(dataFields,716); //ICIL-658
	
	public FixedLengthStringData custrefnum = DD.custrefnum.copy().isAPartOf(dataFields,717); //ILIFE-8583
	
	public FixedLengthStringData concommflg = DD.concommflg.copy().isAPartOf(dataFields,817); //ILJ-40
	public ZonedDecimalData rskcommdate = DD.rskcommdate.copyToZonedDecimal().isAPartOf(dataFields,818);//ILJ-40
	public ZonedDecimalData decldate = DD.decldate.copyToZonedDecimal().isAPartOf(dataFields,826);//ILJ-40	
	
	public ZonedDecimalData premRcptDate = DD.premRcptDate.copyToZonedDecimal().isAPartOf(dataFields,834);
	public ZonedDecimalData cnfirmtnIntentDate = DD.cnfirmtnIntentDate.copyToZonedDecimal().isAPartOf(dataFields,842);
	public FixedLengthStringData ownerOccupation = DD.occup.copy().isAPartOf(dataFields,850);
	public FixedLengthStringData rskComOpt = DD.rskComOpt.copy().isAPartOf(dataFields,854);//IBPLIFE-2264
	public FixedLengthStringData flag = DD.flag.copy().isAPartOf(dataFields,855);//IBPLIFE-2264
		
//	public FixedLengthStringData errorIndicators = new FixedLengthStringData(288).isAPartOf(dataArea, 695);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	
	public FixedLengthStringData addtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData aiindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData apcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData asgnindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData billcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData bnfyingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData campaignErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData comindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData conprosalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData crcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ctrsindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData dlvrmodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData fupflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData grpindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData headingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData hoissdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData hpropdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData hprrcvdtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData huwdcdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData incseqnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData indxflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData jownindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData lrepnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData ownerselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData payindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData plansuffErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	public FixedLengthStringData pstatdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData reptypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData sbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	public FixedLengthStringData srcebusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 168);
	public FixedLengthStringData statdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 172);
	public FixedLengthStringData stcalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 176);
	public FixedLengthStringData stcblErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 180);
	public FixedLengthStringData stcclErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 184);
	public FixedLengthStringData stcdlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 188);
	public FixedLengthStringData stcelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData transhistErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 196);
	public FixedLengthStringData ttmprcdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 200);
	public FixedLengthStringData ttmprcnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 204);
	public FixedLengthStringData znfoptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 208);
	public FixedLengthStringData zsredtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 212);
	public FixedLengthStringData reqntypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 216);//4 ILIFE-2472
	public FixedLengthStringData zdpindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 220);//4 ILIFE-2472
	public FixedLengthStringData zdcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 224);//4 ILIFE-2472
	public FixedLengthStringData fatcastatusErr=new FixedLengthStringData(4).isAPartOf(errorIndicators, 228); 
	public FixedLengthStringData zctaxindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 232);//ALS-313
	public FixedLengthStringData schmnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 236);//ALS-317
	public FixedLengthStringData schmnmeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 240);
	public FixedLengthStringData billdayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 244);
	public FixedLengthStringData zroloverindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 248);
	public FixedLengthStringData nlgflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 252);//ILIFE-3997
	//ICIL-147 start
	public FixedLengthStringData bnkoutErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 256);
	public FixedLengthStringData bnksmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 260);
	public FixedLengthStringData bnktelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 264);
	public FixedLengthStringData bnkoutnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 268);
	public FixedLengthStringData bnksmnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 272);
	public FixedLengthStringData bnktelnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 276);
	//ICIL-147 end
	public FixedLengthStringData refundOverpayErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 280);//ICIL-215
		public FixedLengthStringData zmultOwnerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 284);//ALS-4555
//	public FixedLengthStringData outputIndicators = new FixedLengthStringData(864).isAPartOf(dataArea, 979);
		
		public FixedLengthStringData rfundflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 288); //ICIL-658
	public FixedLengthStringData custrefnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 292); //ILIFE-8583
	
	public FixedLengthStringData concommflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 296); //ILJ-40
	public FixedLengthStringData rskcommdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 300);//ILJ-40
	public FixedLengthStringData decldateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 304);//ILJ-40
	
	public FixedLengthStringData premRcptDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 308);
	public FixedLengthStringData cnfirmtnIntentDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 312);
	public FixedLengthStringData ownerOccupationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 316);
	public FixedLengthStringData rskComOptErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 320);//IBPLIFE-2264
		
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] addtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] aiindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] apcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] asgnindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] billcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] bnfyingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] campaignOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] comindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] conprosalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] crcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ctrsindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] dlvrmodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] fupflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] grpindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] headingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] hoissdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] hpropdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] hprrcvdtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] huwdcdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] incseqnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] indxflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] jownindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] lrepnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] ownerselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] payindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] plansuffOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	public FixedLengthStringData[] pstatdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468);
	public FixedLengthStringData[] reptypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] sbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	public FixedLengthStringData[] srcebusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 504);
	public FixedLengthStringData[] statdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 516);
	public FixedLengthStringData[] stcalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 528);
	public FixedLengthStringData[] stcblOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 540);
	public FixedLengthStringData[] stcclOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 552);
	public FixedLengthStringData[] stcdlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 564);
	public FixedLengthStringData[] stcelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public FixedLengthStringData[] transhistOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 588);
	public FixedLengthStringData[] ttmprcdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 600);
	public FixedLengthStringData[] ttmprcnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 612);
	public FixedLengthStringData[] znfoptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 624);
	public FixedLengthStringData[] zsredtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 636);
	public FixedLengthStringData[] reqntypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 648);//12 ILIFE-2472
	public FixedLengthStringData[] zdpindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 660);//12 ILIFE-2472
	public FixedLengthStringData[] zdcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 672);//12 ILIFE-2472
	public FixedLengthStringData[] fatcastatusOut= FLSArrayPartOfStructure(12, 1, outputIndicators, 684);
	public FixedLengthStringData[] zctaxindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 696);//ALS-313
    public FixedLengthStringData[] schmnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 708);//ALS-318
	public FixedLengthStringData[] schmnmeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 720);//ALS-318
	public FixedLengthStringData[] billdayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 732);
	public FixedLengthStringData[] zroloverindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 744);
	public FixedLengthStringData[] nlgflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 756);//ILIFE-3997
	public FixedLengthStringData[] refundOverpayOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 768);//fwang3 China localization
	//ICIL-147 start
	public FixedLengthStringData[] bnkoutOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 780);
	public FixedLengthStringData[] bnksmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 792);
	public FixedLengthStringData[] bnktelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 804);
	public FixedLengthStringData[] bnkoutnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 816);
	public FixedLengthStringData[] bnksmnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 828);
	public FixedLengthStringData[] bnktelnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 840);
	//ICIL-147 end
	public FixedLengthStringData[] zmultOwnerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 852);//ALS-4555
	
	public FixedLengthStringData[] rfundflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 864); //ICIL-658
	public FixedLengthStringData[] custrefnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 876);//ILIFE-8583	
	public FixedLengthStringData[] concommflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 888); //ILJ-40
	public FixedLengthStringData[] rskcommdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 900); //ILJ-40
	public FixedLengthStringData[] decldateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 912); //ILJ-40
	public FixedLengthStringData[] premRcptDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 924);
	public FixedLengthStringData[] cnfirmtnIntentDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 936);
	public FixedLengthStringData[] ownerOccupationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 948);
	public FixedLengthStringData[] rskComOptOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 960);//IBPLIFE-2264
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hoissdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hpropdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData hprrcvdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData huwdcdteDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ttmprcdteDisp = new FixedLengthStringData(10);	
	public FixedLengthStringData rskcommdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData decldateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premRcptDateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData cnfirmtnIntentDateDisp = new FixedLengthStringData(10);
	
	/* IFSU-1202 by nnaveenkumar */
	public FixedLengthStringData fatcastatusFlag = new FixedLengthStringData(10);
	/* IFSU-1202 by nnaveenkumar */
	public FixedLengthStringData multipleOwnerFlag = new FixedLengthStringData(1);
	public FixedLengthStringData bancAssuranceFlag = new FixedLengthStringData(1);	//ILIFE-8544

	public LongData S5004screenWritten = new LongData(0);
	public LongData S5004protectWritten = new LongData(0);


	public static int[] screenSflPfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 20, 21, 22, 23, 24,38,33}; 
	
	
	public boolean hasSubfile() {
		return false;
	}


	public S5004ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ownerselOut,new String[] {"01","41","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {"03","41","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hpropdteOut,new String[] {"02","40","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreqOut,new String[] {"07","43","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(indxflgOut,new String[] {"55","59","-55","56",null, null, null, null, null, null, null, null});
		fieldIndMap.put(hprrcvdtOut,new String[] {"04","40","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mopOut,new String[] {"08","45","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(incseqnoOut,new String[] {"35","50","-35","54",null, null, null, null, null, null, null, null});
		fieldIndMap.put(huwdcdteOut,new String[] {"05","42","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billcdOut,new String[] {"09","40","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(plansuffOut,new String[] {"28","49","-28","48",null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntcurrOut,new String[] {"15","40","-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billcurrOut,new String[] {"80","40","-80",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(znfoptOut,new String[] {"36","40","-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(registerOut,new String[] {"14","40","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(srcebusOut,new String[] {"10","40","-10",null, null, null, null, null, null, null, null, null});
		//smalchi2 for ILIFE-811 Starts
		fieldIndMap.put(dlvrmodeOut,new String[] {"62","40","-62",null, null, null, null, null, null, null, null, null});
		//ENDS
		fieldIndMap.put(reptypeOut,new String[] {"11","40","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lrepnumOut,new String[] {"13","40","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntselOut,new String[] {"06","40","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(campaignOut,new String[] {"12","40","-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcalOut,new String[] {"16","40","-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcblOut,new String[] {"17","40","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcclOut,new String[] {"18","40","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcdlOut,new String[] {"19","40","-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stcelOut,new String[] {"20","40","-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jownindOut,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(asgnindOut,new String[] {"24",null, "-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(apcindOut,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(addtypeOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(payindOut,new String[] {"25",null, "-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(comindOut,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crcindOut,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ddindOut,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupflgOut,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnfyingOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(grpindOut,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aiindOut,new String[] {"32","60","-32","58",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zsredtrmOut,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ttmprcnoOut,new String[] {"52","53","-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ttmprcdteOut,new String[] {"51","53","-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(transhistOut,new String[] {"61",null, "-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctrsindOut,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(indOut,new String[] {"57",null, "-57",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fatcastatusOut,new String[] {null,"90", null,null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zctaxindOut,new String[] {"63","64", "-63","65", null, null, null, null, null, null, null, null});//ALS-313
		fieldIndMap.put(schmnoOut,new String[] {"66","67", "-66","68", null, null, null, null, null, null, null, null});//ALS-318
		fieldIndMap.put(schmnmeOut,new String[] {"69","70", "-69","71", null, null, null, null, null, null, null, null});//ALS-318
		fieldIndMap.put(billdayOut,new String[] {"72","73", "-72","74", null, null, null, null, null, null, null, null});
		fieldIndMap.put(zroloverindOut,new String[] {"75","76", "-75","77", null, null, null, null, null, null, null, null});//ALS-313
		fieldIndMap.put(nlgflgOut,new String[] {"102","101", "-102",null, null, null, null, null, null, null, null, null});//ILIFE-3997
		fieldIndMap.put(reqntypeOut,new String[] {"103",null, null,null, null, null, null, null, null, null, null, null});//ILIFE-3997
		//fwang3 China localization
		fieldIndMap.put(refundOverpayOut,new String[] {"105","104","-105",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bnkoutOut,new String[] {"38","106","-38","78", null, null, null, null, null, null, null, null});//ICIL-147 
		fieldIndMap.put(bnktelOut,new String[] {"33","34","-33","79", null, null, null, null, null, null, null, null});//ICIL-147 
		fieldIndMap.put(zmultOwnerOut,new String[] {"108","109","-108","110", null, null, null, null, null, null, null, null});//ALS-4555
		fieldIndMap.put(rfundflgOut,new String[] {null ,null, null ,"120" , null, null, null, null, null, null, null, null}); //ICIL-658
		fieldIndMap.put(custrefnumOut,new String[] {null ,null, null ,"121" , null, null, null, null, null, null, null, null}); //ILIFE-8583
		fieldIndMap.put(concommflgOut,new String[] {"141" ,"140", "-141" ,"122" , null, null, null, null, null, null, null, null}); //ILJ-40
		fieldIndMap.put(rskcommdateOut,new String[] {null ,"125", null ,"123" , null, null, null, null, null, null, null, null}); //ILJ-40
		fieldIndMap.put(decldateOut,new String[] {"126" ,"139" , "-126" ,"124" , null, null, null, null, null, null, null, null}); //ILJ-40
		
		fieldIndMap.put(premRcptDateOut,new String[] {null ,"133", null ,"127" , null, null, null, null, null, null, null, null});
		fieldIndMap.put(cnfirmtnIntentDateOut,new String[] {"130" ,"134", "-130" ,"129" , null, null, null, null, null, null, null, null});
		fieldIndMap.put(ownerOccupationOut,new String[] {"132" ,"135", "-132" ,"131" , null, null, null, null, null, null, null, null});
		fieldIndMap.put(rskComOptOut,new String[] {"136" ,"137", "-136" ,"138" , null, null, null, null, null, null, null, null});
		/*screenFields = new BaseData[] {heading, chdrnum, statdsc, premStatDesc, ownersel, ownername, occdate, conprosal, hpropdte, billfreq, indxflg, hprrcvdt, mop, incomeSeqNo, huwdcdte, billcd, plansuff, hoissdte, cntcurr, billcurr, znfopt, register, srcebus, dlvrmode, reptype, lrepnum, cntbranch, sbrdesc, agntsel, agentname, campaign, stcal, stcbl, stccl, stcdl, stcel, jownind, asgnind, apcind, addtype, payind, comind, crcind, ddind, fupflg, bnfying, grpind, aiind, zsredtrm, ttmprcno, ttmprcdte, transhist, ctrsind, ind,reqntype,zdpind,zdcind,zctaxind,schmno,schmnme,billday,zroloverind,nlgflg,refundOverpay,bnkout,bnksm,bnktel,bnkoutname,bnksmname,bnktelname};
		screenOutFields = new BaseData[][] {headingOut, chdrnumOut, statdscOut, pstatdscOut, ownerselOut, ownernameOut, occdateOut, conprosalOut, hpropdteOut, billfreqOut, indxflgOut, hprrcvdtOut, mopOut, incseqnoOut, huwdcdteOut, billcdOut, plansuffOut, hoissdteOut, cntcurrOut, billcurrOut, znfoptOut, registerOut, srcebusOut, dlvrmodeOut, reptypeOut, lrepnumOut, cntbranchOut, sbrdescOut, agntselOut, agentnameOut, campaignOut, stcalOut, stcblOut, stcclOut, stcdlOut, stcelOut, jownindOut, asgnindOut, apcindOut, addtypeOut, payindOut, comindOut, crcindOut, ddindOut, fupflgOut, bnfyingOut, grpindOut, aiindOut, zsredtrmOut, ttmprcnoOut, ttmprcdteOut, transhistOut, ctrsindOut, indOut,reqntypeOut,zdpindOut,zdcindOut,zctaxindOut,schmnoOut,schmnmeOut,billdayOut,zroloverindOut,nlgflgOut,bnkoutOut,bnksmOut,bnktelOut,bnkoutnameOut,bnksmnameOut,bnktelnameOut,refundOverpayOut};
		screenErrFields = new BaseData[] {headingErr, chdrnumErr, statdscErr, pstatdscErr, ownerselErr, ownernameErr, occdateErr, conprosalErr, hpropdteErr, billfreqErr, indxflgErr, hprrcvdtErr, mopErr, incseqnoErr, huwdcdteErr, billcdErr, plansuffErr, hoissdteErr, cntcurrErr, billcurrErr, znfoptErr, registerErr, srcebusErr, dlvrmodeErr, reptypeErr, lrepnumErr, cntbranchErr, sbrdescErr, agntselErr, agentnameErr, campaignErr, stcalErr, stcblErr, stcclErr, stcdlErr, stcelErr, jownindErr, asgnindErr, apcindErr, addtypeErr, payindErr, comindErr, crcindErr, ddindErr, fupflgErr, bnfyingErr, grpindErr, aiindErr, zsredtrmErr, ttmprcnoErr, ttmprcdteErr, transhistErr, ctrsindErr, indErr,reqntypeErr,zdpindErr,zdcindErr,zctaxindErr,schmnoErr,schmnmeErr,billdayErr,zroloverindErr,nlgflgErr,bnkoutErr,bnksmErr,bnktelErr,bnkoutnameErr,bnksmnameErr,bnktelnameErr,refundOverpayErr};
		screenDateFields = new BaseData[] {occdate, hpropdte, hprrcvdt, huwdcdte, billcd, hoissdte, ttmprcdte};
		screenDateErrFields = new BaseData[] {occdateErr, hpropdteErr, hprrcvdtErr, huwdcdteErr, billcdErr, hoissdteErr, ttmprcdteErr};
		screenDateDispFields = new BaseData[] {occdateDisp, hpropdteDisp, hprrcvdtDisp, huwdcdteDisp, billcdDisp, hoissdteDisp, ttmprcdteDisp};*/
		
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5004screen.class;
		protectRecord = S5004protect.class;
	}
	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	
	
	public int getDataAreaSize() {
		return 2152;
	}
	

	public int getDataFieldsSize(){
		return 856;  
	}
	public int getErrorIndicatorSize(){
		return 324; 
	}
	
	public int getOutputFieldSize(){
		return 972; 
	}
	

	public BaseData[] getscreenFields(){
		return new BaseData[] {heading, chdrnum, statdsc, premStatDesc, ownersel, ownername, occdate, conprosal, hpropdte, billfreq, indxflg, hprrcvdt, mop, incomeSeqNo, huwdcdte, billcd, plansuff, hoissdte, cntcurr, billcurr, znfopt, register, srcebus, dlvrmode, reptype, lrepnum, cntbranch, sbrdesc, agntsel, agentname, campaign, stcal, stcbl, stccl, stcdl, stcel, jownind, asgnind, apcind, addtype, payind, comind, crcind, ddind, fupflg, bnfying, grpind, aiind, zsredtrm, ttmprcno, ttmprcdte, transhist, ctrsind, ind,reqntype,zdpind,zdcind,zctaxind,schmno,schmnme,billday,zroloverind,nlgflg,refundOverpay,bnkout,bnksm,bnktel,bnkoutname,bnksmname,bnktelname,zmultOwner,rfundflg, custrefnum, concommflg, rskcommdate, decldate, premRcptDate, cnfirmtnIntentDate, ownerOccupation, rskComOpt, flag};
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][]  { headingOut, chdrnumOut, statdscOut, pstatdscOut, ownerselOut, ownernameOut, occdateOut, conprosalOut, hpropdteOut, billfreqOut, indxflgOut, hprrcvdtOut, mopOut, incseqnoOut, huwdcdteOut, billcdOut, plansuffOut, hoissdteOut, cntcurrOut, billcurrOut, znfoptOut, registerOut, srcebusOut, dlvrmodeOut, reptypeOut, lrepnumOut, cntbranchOut, sbrdescOut, agntselOut, agentnameOut, campaignOut, stcalOut, stcblOut, stcclOut, stcdlOut, stcelOut, jownindOut, asgnindOut, apcindOut, addtypeOut, payindOut, comindOut, crcindOut, ddindOut, fupflgOut, bnfyingOut, grpindOut, aiindOut, zsredtrmOut, ttmprcnoOut, ttmprcdteOut, transhistOut, ctrsindOut, indOut,reqntypeOut,zdpindOut,zdcindOut,zctaxindOut,schmnoOut,schmnmeOut,billdayOut,zroloverindOut,nlgflgOut,bnkoutOut,bnksmOut,bnktelOut,bnkoutnameOut,bnksmnameOut,bnktelnameOut,refundOverpayOut,zmultOwnerOut,rfundflgOut, custrefnumOut, concommflgOut, rskcommdateOut, decldateOut, premRcptDateOut, cnfirmtnIntentDateOut, ownerOccupationOut, rskComOptOut};
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[]  {headingErr, chdrnumErr, statdscErr, pstatdscErr, ownerselErr, ownernameErr, occdateErr, conprosalErr, hpropdteErr, billfreqErr, indxflgErr, hprrcvdtErr, mopErr, incseqnoErr, huwdcdteErr, billcdErr, plansuffErr, hoissdteErr, cntcurrErr, billcurrErr, znfoptErr, registerErr, srcebusErr, dlvrmodeErr, reptypeErr, lrepnumErr, cntbranchErr, sbrdescErr, agntselErr, agentnameErr, campaignErr, stcalErr, stcblErr, stcclErr, stcdlErr, stcelErr, jownindErr, asgnindErr, apcindErr, addtypeErr, payindErr, comindErr, crcindErr, ddindErr, fupflgErr, bnfyingErr, grpindErr, aiindErr, zsredtrmErr, ttmprcnoErr, ttmprcdteErr, transhistErr, ctrsindErr, indErr,reqntypeErr,zdpindErr,zdcindErr,zctaxindErr,schmnoErr,schmnmeErr,billdayErr,zroloverindErr,nlgflgErr,bnkoutErr,bnksmErr,bnktelErr,bnkoutnameErr,bnksmnameErr,bnktelnameErr,refundOverpayErr,zmultOwnerErr,rfundflgErr,custrefnumErr, concommflgErr, rskcommdateErr, decldateErr, premRcptDateErr, cnfirmtnIntentDateErr, ownerOccupationErr, rskComOptErr};
		
	}
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {occdate, hpropdte, hprrcvdt, huwdcdte, billcd, hoissdte, ttmprcdte, rskcommdate, decldate, premRcptDate, cnfirmtnIntentDate};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {occdateDisp, hpropdteDisp, hprrcvdtDisp, huwdcdteDisp, billcdDisp, hoissdteDisp, ttmprcdteDisp, rskcommdateDisp, decldateDisp, premRcptDateDisp, cnfirmtnIntentDateDisp};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {occdateErr, hpropdteErr, hprrcvdtErr, huwdcdteErr, billcdErr, hoissdteErr, ttmprcdteErr, rskcommdateErr, decldateErr, premRcptDateErr, cnfirmtnIntentDateErr};
	}
	
}
