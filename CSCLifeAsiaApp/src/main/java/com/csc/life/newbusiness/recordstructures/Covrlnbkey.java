package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:23
 * Description:
 * Copybook name: COVRLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrlnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrlnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrlnbKey = new FixedLengthStringData(64).isAPartOf(covrlnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrlnbChdrcoy = new FixedLengthStringData(1).isAPartOf(covrlnbKey, 0);
  	public FixedLengthStringData covrlnbChdrnum = new FixedLengthStringData(8).isAPartOf(covrlnbKey, 1);
  	public FixedLengthStringData covrlnbLife = new FixedLengthStringData(2).isAPartOf(covrlnbKey, 9);
  	public FixedLengthStringData covrlnbCoverage = new FixedLengthStringData(2).isAPartOf(covrlnbKey, 11);
  	public FixedLengthStringData covrlnbRider = new FixedLengthStringData(2).isAPartOf(covrlnbKey, 13);
  	public PackedDecimalData covrlnbPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrlnbKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(46).isAPartOf(covrlnbKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrlnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrlnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}