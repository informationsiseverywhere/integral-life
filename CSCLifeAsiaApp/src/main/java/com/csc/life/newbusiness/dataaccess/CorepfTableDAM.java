package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CorepfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:32
 * Class transformed from COREPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CorepfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 22;
	public FixedLengthStringData corerec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData corepfRecord = corerec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(corerec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(corerec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(corerec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(corerec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(corerec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(corerec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(corerec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(corerec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CorepfTableDAM() {
  		super();
  		setColumns();
  		journalled = false;
	}

	/**
	* Constructor for CorepfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CorepfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CorepfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CorepfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CorepfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CorepfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("COREPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"PSTATCODE, " +
							"STATCODE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     pstatcode,
                                     statcode,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		pstatcode.clear();
  		statcode.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCorerec() {
  		return corerec;
	}

	public FixedLengthStringData getCorepfRecord() {
  		return corepfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCorerec(what);
	}

	public void setCorerec(Object what) {
  		this.corerec.set(what);
	}

	public void setCorepfRecord(Object what) {
  		this.corepfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(corerec.getLength());
		result.set(corerec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}