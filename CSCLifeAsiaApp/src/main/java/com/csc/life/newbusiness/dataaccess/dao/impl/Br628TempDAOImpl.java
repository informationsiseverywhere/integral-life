package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.Br628TempDAO;
import com.csc.life.newbusiness.dataaccess.model.Br628DTO;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class Br628TempDAOImpl extends BaseDAOImpl<Br628DTO> implements Br628TempDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Br628TempDAOImpl.class);
    
	@Override
	public int buildTempData(Br628DTO dto) {
		deleteTempData(dto.getTableName());
		return insertTempData(dto);
	}

	private void deleteTempData(String tableName) {
		String sqlStr = "DELETE FROM " + tableName;
		PreparedStatement ps = getPrepareStatement(sqlStr);
		try {
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("deleteTempData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	private int insertTempData(Br628DTO dto) {
		StringBuilder sqlStr = new StringBuilder("INSERT INTO ");
		sqlStr.append(dto.getTableName());
		sqlStr.append(" (chdrcoy,chdrnum,validflag,zsufcdte,procflg,member_name) ");
		sqlStr.append("select CHDRCOY, CHDRNUM, VALIDFLAG, ZSUFCDTE, PROCFLG, ? from HPADPF ");
		sqlStr.append("where CHDRCOY = ? AND CHDRNUM BETWEEN ? AND ? AND VALIDFLAG = ? AND (ZSUFCDTE < ? OR ZSUFCDTE = ?) AND ZSUFCDTE <> 0 AND ZSUFCDTE <> ? AND PROCFLG <> ? ORDER BY CHDRCOY, CHDRNUM");
		
		PreparedStatement ps = getPrepareStatement(sqlStr.toString());
		try {
			ps.setString(1, dto.getThreadNo());
			ps.setString(2, dto.getCompany());
			ps.setString(3, dto.getChdrnumfrom());
			ps.setString(4, dto.getChdrnumto());
			ps.setString(5, dto.getValidflag1());
			ps.setInt(6, dto.getEffdate());
			ps.setInt(7, dto.getEffdate());
			ps.setInt(8, dto.getMaxdate());
			ps.setString(9, dto.getYes());
			
			return ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertTempData()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

}
