package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.common.DD;
import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RM500.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rm500Report extends SMARTReportLayout { 

	private FixedLengthStringData addr01 = new FixedLengthStringData(DD.cltaddr.length);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData addr02 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr03 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr04 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addr05 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addrss01 = new FixedLengthStringData(DD.cltaddr.length);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData addrss02 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addrss03 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addrss04 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData addrss05 = new FixedLengthStringData(DD.cltaddr.length);
	private FixedLengthStringData ageadm = new FixedLengthStringData(1);
	private FixedLengthStringData agntnum = new FixedLengthStringData(8);
	private ZonedDecimalData agrossprem = new ZonedDecimalData(17, 2);
	private FixedLengthStringData billfreq = new FixedLengthStringData(2);
	private FixedLengthStringData bnynam = new FixedLengthStringData(47);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData cntbranch = new FixedLengthStringData(2);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData date01 = new FixedLengthStringData(10);
	private FixedLengthStringData date02 = new FixedLengthStringData(10);
	private FixedLengthStringData date04 = new FixedLengthStringData(10);
	private FixedLengthStringData datecfrom = new FixedLengthStringData(10);
	private FixedLengthStringData datecto = new FixedLengthStringData(10);
	private ZonedDecimalData extr = new ZonedDecimalData(17, 2);
	private FixedLengthStringData lifename = new FixedLengthStringData(47);
	private FixedLengthStringData liferel = new FixedLengthStringData(2);
	private FixedLengthStringData nffeit = new FixedLengthStringData(4);
	private ZonedDecimalData nofmbr = new ZonedDecimalData(7, 0);
	private FixedLengthStringData ownername = new FixedLengthStringData(47);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData pflg = new FixedLengthStringData(1);
	private ZonedDecimalData premsusp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData rcesage = new ZonedDecimalData(3, 0);
	private ZonedDecimalData rcestrm = new ZonedDecimalData(3, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData sex = new FixedLengthStringData(1);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rm500Report() {
		super();
	}


	/**
	 * Print the XML for Rm500d01
	 */
	public void printRm500d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		lifename.setFieldName("lifename");
		lifename.setInternal(subString(recordData, 9, 47));
		sex.setFieldName("sex");
		sex.setInternal(subString(recordData, 56, 1));
		rcesage.setFieldName("rcesage");
		rcesage.setInternal(subString(recordData, 57, 3));
		billfreq.setFieldName("billfreq");
		billfreq.setInternal(subString(recordData, 60, 2));
		rcestrm.setFieldName("rcestrm");
		rcestrm.setInternal(subString(recordData, 62, 3));
		agntnum.setFieldName("agntnum");
		agntnum.setInternal(subString(recordData, 65, 8));
		cntbranch.setFieldName("cntbranch");
		cntbranch.setInternal(subString(recordData, 73, 2));
		pflg.setFieldName("pflg");
		pflg.setInternal(subString(recordData, 75, 1));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 76, 3));
		date01.setFieldName("date01");
		date01.setInternal(subString(recordData, 79, 10));
		date02.setFieldName("date02");
		date02.setInternal(subString(recordData, 89, 10));
		singp.setFieldName("singp");
		singp.setInternal(subString(recordData, 99, 17));
		agrossprem.setFieldName("agrossprem");
		agrossprem.setInternal(subString(recordData, 116, 17));
		extr.setFieldName("extr");
		extr.setInternal(subString(recordData, 133, 17));
		premsusp.setFieldName("premsusp");
		premsusp.setInternal(subString(recordData, 150, 17));
		ageadm.setFieldName("ageadm");
		ageadm.setInternal(subString(recordData, 167, 1));
		date04.setFieldName("date04");
		date04.setInternal(subString(recordData, 168, 10));
		nffeit.setFieldName("nffeit");
		nffeit.setInternal(subString(recordData, 178, 4));
		sumins.setFieldName("sumins");
		sumins.setInternal(subString(recordData, 182, 17));
		ownername.setFieldName("ownername");
		ownername.setInternal(subString(recordData, 199, 47));
		printLayout("Rm500d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				lifename,
				sex,
				rcesage,
				billfreq,
				rcestrm,
				agntnum,
				cntbranch,
				pflg,
				cnttype,
				date01,
				date02,
				singp,
				agrossprem,
				extr,
				premsusp,
				ageadm,
				date04,
				nffeit,
				sumins,
				ownername
			}
		);

		currentPrintLine.add(3);
	}

	/**
	 * Print the XML for Rm500d02
	 */
	public void printRm500d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		addr01.setFieldName("addr01");
		addr01.setInternal(subString(recordData, 1, DD.cltaddr.length));/*pmujavadiya ILIFE-3212*/
		addr02.setFieldName("addr02");
		addr02.setInternal(subString(recordData, 1+DD.cltaddr.length, DD.cltaddr.length));
		addr03.setFieldName("addr03");
		addr03.setInternal(subString(recordData, 1+DD.cltaddr.length*2, DD.cltaddr.length));
		addr04.setFieldName("addr04");
		addr04.setInternal(subString(recordData, 1+DD.cltaddr.length*3, DD.cltaddr.length));
		addr05.setFieldName("addr05");
		addr05.setInternal(subString(recordData, 1+DD.cltaddr.length*4, DD.cltaddr.length));/*pmujavadiya ILIFE-3212*/
		printLayout("Rm500d02",			// Record name
			new BaseData[]{			// Fields:
				addr01,
				addr02,
				addr03,
				addr04,
				addr05
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rm500d03
	 */
	public void printRm500d03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		bnynam.setFieldName("bnynam");
		bnynam.setInternal(subString(recordData, 1, 47));
		liferel.setFieldName("liferel");
		liferel.setInternal(subString(recordData, 48, 2));
		addrss01.setFieldName("addrss01");
		addrss01.setInternal(subString(recordData, 50, DD.cltaddr.length));/*pmujavadiya ILIFE-3212*/
		addrss02.setFieldName("addrss02");
		addrss02.setInternal(subString(recordData, 50+DD.cltaddr.length, DD.cltaddr.length));
		addrss03.setFieldName("addrss03");
		addrss03.setInternal(subString(recordData, 50+DD.cltaddr.length*2, DD.cltaddr.length));
		addrss04.setFieldName("addrss04");
		addrss04.setInternal(subString(recordData, 50+DD.cltaddr.length*3, DD.cltaddr.length));
		addrss05.setFieldName("addrss05");
		addrss05.setInternal(subString(recordData, 50+DD.cltaddr.length*4, DD.cltaddr.length));/*pmujavadiya ILIFE-3212*/
		printLayout("Rm500d03",			// Record name
			new BaseData[]{			// Fields:
				bnynam,
				liferel,
				addrss01,
				addrss02,
				addrss03,
				addrss04,
				addrss05
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for Rm500d04
	 */
	public void printRm500d04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		printLayout("Rm500d04",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rm500h01
	 */
	public void printRm500h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 1, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 31, 10));
		datecfrom.setFieldName("datecfrom");
		datecfrom.setInternal(subString(recordData, 41, 10));
		datecto.setFieldName("datecto");
		datecto.setInternal(subString(recordData, 51, 10));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rm500h01",			// Record name
			new BaseData[]{			// Fields:
				companynm,
				sdate,
				datecfrom,
				datecto,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(18);
	}

	/**
	 * Print the XML for Rm500t01
	 */
	public void printRm500t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		nofmbr.setFieldName("nofmbr");
		nofmbr.setInternal(subString(recordData, 1, 7));
		printLayout("Rm500t01",			// Record name
			new BaseData[]{			// Fields:
				nofmbr
			}
		);

		currentPrintLine.add(1);
	}


}
