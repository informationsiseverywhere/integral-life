package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CtrspfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:37
 * Class transformed from CTRSPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CtrspfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 79;
	public FixedLengthStringData ctrsrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData ctrspfRecord = ctrsrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(ctrsrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(ctrsrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(ctrsrec);
	public FixedLengthStringData clntnum = DD.clntnum.copy().isAPartOf(ctrsrec);
	public PackedDecimalData datefrm = DD.datefrm.copy().isAPartOf(ctrsrec);
	public PackedDecimalData dateto = DD.dateto.copy().isAPartOf(ctrsrec);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(ctrsrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(ctrsrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(ctrsrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(ctrsrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CtrspfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CtrspfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CtrspfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CtrspfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CtrspfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CtrspfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CtrspfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("CTRSPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"SEQNO, " +
							"CLNTNUM, " +
							"DATEFRM, " +
							"DATETO, " +
							"REASONCD, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     seqno,
                                     clntnum,
                                     datefrm,
                                     dateto,
                                     reasoncd,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		seqno.clear();
  		clntnum.clear();
  		datefrm.clear();
  		dateto.clear();
  		reasoncd.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCtrsrec() {
  		return ctrsrec;
	}

	public FixedLengthStringData getCtrspfRecord() {
  		return ctrspfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCtrsrec(what);
	}

	public void setCtrsrec(Object what) {
  		this.ctrsrec.set(what);
	}

	public void setCtrspfRecord(Object what) {
  		this.ctrspfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(ctrsrec.getLength());
		result.set(ctrsrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}