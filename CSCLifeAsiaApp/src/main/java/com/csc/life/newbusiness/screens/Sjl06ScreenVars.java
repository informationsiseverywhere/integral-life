package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sjl06 Date: 15 December 2019 Author: agoel51
 */
public class Sjl06ScreenVars extends SmartVarModel {

	private static final long serialVersionUID = 1L;
	public FixedLengthStringData dataArea = new FixedLengthStringData(2856);
	public FixedLengthStringData dataFields = new FixedLengthStringData(648).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields, 1);
	public ZonedDecimalData itmfrm = DD.itmfrm.copyToZonedDecimal().isAPartOf(dataFields, 9);
	public ZonedDecimalData itmto = DD.itmto.copyToZonedDecimal().isAPartOf(dataFields, 17);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields, 25);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields, 55);
	
	public FixedLengthStringData mops = new FixedLengthStringData(5).isAPartOf(dataFields, 60);
	public FixedLengthStringData[] mop = FLSArrayPartOfStructure(5, 1, mops, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(5).isAPartOf(mops, 0, FILLER_REDEFINE);
	public FixedLengthStringData mop01 = DD.mop.copy().isAPartOf(filler1,0);
	public FixedLengthStringData mop02 = DD.mop.copy().isAPartOf(filler1,1);
	public FixedLengthStringData mop03 = DD.mop.copy().isAPartOf(filler1,2);
	public FixedLengthStringData mop04 = DD.mop.copy().isAPartOf(filler1,3);
	public FixedLengthStringData mop05 = DD.mop.copy().isAPartOf(filler1,4);
	
	public FixedLengthStringData billfreqs = new FixedLengthStringData(20).isAPartOf(dataFields, 65);
	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(10, 2, billfreqs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01 = DD.billfreq.copy().isAPartOf(filler,0);
	public FixedLengthStringData billfreq02 = DD.billfreq.copy().isAPartOf(filler,2);
	public FixedLengthStringData billfreq03 = DD.billfreq.copy().isAPartOf(filler,4);
	public FixedLengthStringData billfreq04 = DD.billfreq.copy().isAPartOf(filler,6);
	public FixedLengthStringData billfreq05 = DD.billfreq.copy().isAPartOf(filler,8);
	public FixedLengthStringData billfreq06 = DD.billfreq.copy().isAPartOf(filler,10);
	public FixedLengthStringData billfreq07 = DD.billfreq.copy().isAPartOf(filler,12);
	public FixedLengthStringData billfreq08 = DD.billfreq.copy().isAPartOf(filler,14);
	public FixedLengthStringData billfreq09 = DD.billfreq.copy().isAPartOf(filler,16);
	public FixedLengthStringData billfreq10 = DD.billfreq.copy().isAPartOf(filler,18);
	
	public FixedLengthStringData contitem = DD.contitem.copy().isAPartOf(dataFields,85);
	public ZonedDecimalData minsumin = DD.minsumin.copyToZonedDecimal().isAPartOf(dataFields,93);
	public ZonedDecimalData minDhb = DD.minDhb.copyToZonedDecimal().isAPartOf(dataFields,108);
	public ZonedDecimalData minsum = DD.minsum.copyToZonedDecimal().isAPartOf(dataFields,123);
	
	public FixedLengthStringData instprms = new FixedLengthStringData(85).isAPartOf(dataFields, 138);
	public ZonedDecimalData[] instprm = ZDArrayPartOfStructure(5, 17, 0, instprms, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(85).isAPartOf(instprms, 0, FILLER_REDEFINE);
	public ZonedDecimalData instPrem01 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,0);
	public ZonedDecimalData instPrem02 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,17);
	public ZonedDecimalData instPrem03 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,34);
	public ZonedDecimalData instPrem04 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,51);
	public ZonedDecimalData instPrem05 = DD.instprm.copyToZonedDecimal().isAPartOf(filler8,68);
	
	public FixedLengthStringData ctables = new FixedLengthStringData(300).isAPartOf(dataFields, 223);
	public FixedLengthStringData[] ctable = FLSArrayPartOfStructure(75, 4, ctables, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(300).isAPartOf(ctables, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctable01 = DD.ctable.copy().isAPartOf(filler11,0);
	public FixedLengthStringData ctable02 = DD.ctable.copy().isAPartOf(filler11,4);
	public FixedLengthStringData ctable03 = DD.ctable.copy().isAPartOf(filler11,8);
	public FixedLengthStringData ctable04 = DD.ctable.copy().isAPartOf(filler11,12);
	public FixedLengthStringData ctable05 = DD.ctable.copy().isAPartOf(filler11,16);
	public FixedLengthStringData ctable06 = DD.ctable.copy().isAPartOf(filler11,20);
	public FixedLengthStringData ctable07 = DD.ctable.copy().isAPartOf(filler11,24);
	public FixedLengthStringData ctable08 = DD.ctable.copy().isAPartOf(filler11,28);
	public FixedLengthStringData ctable09 = DD.ctable.copy().isAPartOf(filler11,32);
	public FixedLengthStringData ctable10 = DD.ctable.copy().isAPartOf(filler11,36);
	public FixedLengthStringData ctable11 = DD.ctable.copy().isAPartOf(filler11,40);
	public FixedLengthStringData ctable12 = DD.ctable.copy().isAPartOf(filler11,44);
	public FixedLengthStringData ctable13 = DD.ctable.copy().isAPartOf(filler11,48);
	public FixedLengthStringData ctable14 = DD.ctable.copy().isAPartOf(filler11,52);
	public FixedLengthStringData ctable15 = DD.ctable.copy().isAPartOf(filler11,56);
	public FixedLengthStringData ctable16 = DD.ctable.copy().isAPartOf(filler11,60);
	public FixedLengthStringData ctable17 = DD.ctable.copy().isAPartOf(filler11,64);
	public FixedLengthStringData ctable18 = DD.ctable.copy().isAPartOf(filler11,68);
	public FixedLengthStringData ctable19 = DD.ctable.copy().isAPartOf(filler11,72);
	public FixedLengthStringData ctable20 = DD.ctable.copy().isAPartOf(filler11,76);
	public FixedLengthStringData ctable21 = DD.ctable.copy().isAPartOf(filler11,80);
	public FixedLengthStringData ctable22 = DD.ctable.copy().isAPartOf(filler11,84);
	public FixedLengthStringData ctable23 = DD.ctable.copy().isAPartOf(filler11,88);
	public FixedLengthStringData ctable24 = DD.ctable.copy().isAPartOf(filler11,92);
	public FixedLengthStringData ctable25 = DD.ctable.copy().isAPartOf(filler11,96);
	public FixedLengthStringData ctable26 = DD.ctable.copy().isAPartOf(filler11,100);
	public FixedLengthStringData ctable27 = DD.ctable.copy().isAPartOf(filler11,104);
	public FixedLengthStringData ctable28 = DD.ctable.copy().isAPartOf(filler11,108);
	public FixedLengthStringData ctable29 = DD.ctable.copy().isAPartOf(filler11,112);
	public FixedLengthStringData ctable30 = DD.ctable.copy().isAPartOf(filler11,116);
	public FixedLengthStringData ctable31 = DD.ctable.copy().isAPartOf(filler11,120);
	public FixedLengthStringData ctable32 = DD.ctable.copy().isAPartOf(filler11,124);
	public FixedLengthStringData ctable33 = DD.ctable.copy().isAPartOf(filler11,128);
	public FixedLengthStringData ctable34 = DD.ctable.copy().isAPartOf(filler11,132);
	public FixedLengthStringData ctable35 = DD.ctable.copy().isAPartOf(filler11,136);
	public FixedLengthStringData ctable36 = DD.ctable.copy().isAPartOf(filler11,140);
	public FixedLengthStringData ctable37 = DD.ctable.copy().isAPartOf(filler11,144);
	public FixedLengthStringData ctable38 = DD.ctable.copy().isAPartOf(filler11,148);
	public FixedLengthStringData ctable39 = DD.ctable.copy().isAPartOf(filler11,152);
	public FixedLengthStringData ctable40 = DD.ctable.copy().isAPartOf(filler11,156);
	public FixedLengthStringData ctable41 = DD.ctable.copy().isAPartOf(filler11,160);
	public FixedLengthStringData ctable42 = DD.ctable.copy().isAPartOf(filler11,164);
	public FixedLengthStringData ctable43 = DD.ctable.copy().isAPartOf(filler11,168);
	public FixedLengthStringData ctable44 = DD.ctable.copy().isAPartOf(filler11,172);
	public FixedLengthStringData ctable45 = DD.ctable.copy().isAPartOf(filler11,176);
	public FixedLengthStringData ctable46 = DD.ctable.copy().isAPartOf(filler11,180);
	public FixedLengthStringData ctable47 = DD.ctable.copy().isAPartOf(filler11,184);
	public FixedLengthStringData ctable48 = DD.ctable.copy().isAPartOf(filler11,188);
	public FixedLengthStringData ctable49 = DD.ctable.copy().isAPartOf(filler11,192);
	public FixedLengthStringData ctable50 = DD.ctable.copy().isAPartOf(filler11,196);
	public FixedLengthStringData ctable51 = DD.ctable.copy().isAPartOf(filler11,200);
	public FixedLengthStringData ctable52 = DD.ctable.copy().isAPartOf(filler11,204);
	public FixedLengthStringData ctable53 = DD.ctable.copy().isAPartOf(filler11,208);
	public FixedLengthStringData ctable54 = DD.ctable.copy().isAPartOf(filler11,212);
	public FixedLengthStringData ctable55 = DD.ctable.copy().isAPartOf(filler11,216);
	public FixedLengthStringData ctable56 = DD.ctable.copy().isAPartOf(filler11,220);
	public FixedLengthStringData ctable57 = DD.ctable.copy().isAPartOf(filler11,224);
	public FixedLengthStringData ctable58 = DD.ctable.copy().isAPartOf(filler11,228);
	public FixedLengthStringData ctable59 = DD.ctable.copy().isAPartOf(filler11,232);
	public FixedLengthStringData ctable60 = DD.ctable.copy().isAPartOf(filler11,236);
	public FixedLengthStringData ctable61 = DD.ctable.copy().isAPartOf(filler11,240);
	public FixedLengthStringData ctable62 = DD.ctable.copy().isAPartOf(filler11,244);
	public FixedLengthStringData ctable63 = DD.ctable.copy().isAPartOf(filler11,248);
	public FixedLengthStringData ctable64 = DD.ctable.copy().isAPartOf(filler11,252);
	public FixedLengthStringData ctable65 = DD.ctable.copy().isAPartOf(filler11,256);
	public FixedLengthStringData ctable66 = DD.ctable.copy().isAPartOf(filler11,260);
	public FixedLengthStringData ctable67 = DD.ctable.copy().isAPartOf(filler11,264);
	public FixedLengthStringData ctable68 = DD.ctable.copy().isAPartOf(filler11,268);
	public FixedLengthStringData ctable69 = DD.ctable.copy().isAPartOf(filler11,272);
	public FixedLengthStringData ctable70 = DD.ctable.copy().isAPartOf(filler11,276);
	public FixedLengthStringData ctable71 = DD.ctable.copy().isAPartOf(filler11,280);
	public FixedLengthStringData ctable72 = DD.ctable.copy().isAPartOf(filler11,284);
	public FixedLengthStringData ctable73 = DD.ctable.copy().isAPartOf(filler11,288);
	public FixedLengthStringData ctable74 = DD.ctable.copy().isAPartOf(filler11,292);
	public FixedLengthStringData ctable75 = DD.ctable.copy().isAPartOf(filler11,296);
	
	public FixedLengthStringData hosbens = new FixedLengthStringData(108).isAPartOf(dataFields, 523);
	public FixedLengthStringData[] hosben = FLSArrayPartOfStructure(27, 4, hosbens, 0);
	public FixedLengthStringData filler13 = new FixedLengthStringData(108).isAPartOf(hosbens, 0, FILLER_REDEFINE);
	public FixedLengthStringData hosben01 = DD.ctable.copy().isAPartOf(filler13,0);
	public FixedLengthStringData hosben02 = DD.ctable.copy().isAPartOf(filler13,4);
	public FixedLengthStringData hosben03 = DD.ctable.copy().isAPartOf(filler13,8);
	public FixedLengthStringData hosben04 = DD.ctable.copy().isAPartOf(filler13,12);
	public FixedLengthStringData hosben05 = DD.ctable.copy().isAPartOf(filler13,16);
	public FixedLengthStringData hosben06 = DD.ctable.copy().isAPartOf(filler13,20);
	public FixedLengthStringData hosben07 = DD.ctable.copy().isAPartOf(filler13,24);
	public FixedLengthStringData hosben08 = DD.ctable.copy().isAPartOf(filler13,28);
	public FixedLengthStringData hosben09 = DD.ctable.copy().isAPartOf(filler13,32);
	public FixedLengthStringData hosben10 = DD.ctable.copy().isAPartOf(filler13,36);
	public FixedLengthStringData hosben11 = DD.ctable.copy().isAPartOf(filler13,40);
	public FixedLengthStringData hosben12 = DD.ctable.copy().isAPartOf(filler13,44);
	public FixedLengthStringData hosben13 = DD.ctable.copy().isAPartOf(filler13,48);
	public FixedLengthStringData hosben14 = DD.ctable.copy().isAPartOf(filler13,52);
	public FixedLengthStringData hosben15 = DD.ctable.copy().isAPartOf(filler13,56);
	public FixedLengthStringData hosben16 = DD.ctable.copy().isAPartOf(filler13,60);
	public FixedLengthStringData hosben17 = DD.ctable.copy().isAPartOf(filler13,64);
	public FixedLengthStringData hosben18 = DD.ctable.copy().isAPartOf(filler13,68);
	public FixedLengthStringData hosben19 = DD.ctable.copy().isAPartOf(filler13,72);
	public FixedLengthStringData hosben20 = DD.ctable.copy().isAPartOf(filler13,76);
	public FixedLengthStringData hosben21 = DD.ctable.copy().isAPartOf(filler13,80);
	public FixedLengthStringData hosben22 = DD.ctable.copy().isAPartOf(filler13,84);
	public FixedLengthStringData hosben23 = DD.ctable.copy().isAPartOf(filler13,88);
	public FixedLengthStringData hosben24 = DD.ctable.copy().isAPartOf(filler13,92);
	public FixedLengthStringData hosben25 = DD.ctable.copy().isAPartOf(filler13,96);
	public FixedLengthStringData hosben26 = DD.ctable.copy().isAPartOf(filler13,100);
	public FixedLengthStringData hosben27 = DD.ctable.copy().isAPartOf(filler13,104);
	
	public FixedLengthStringData fupcdes = new FixedLengthStringData(12).isAPartOf(dataFields, 631);
	public FixedLengthStringData[] fupcde = FLSArrayPartOfStructure(3, 4, fupcdes, 0);
	public FixedLengthStringData filler16 = new FixedLengthStringData(12).isAPartOf(fupcdes, 0, FILLER_REDEFINE);
	public FixedLengthStringData fupcdes01 = DD.fupcdes.copy().isAPartOf(filler16, 0);
	public FixedLengthStringData fupcdes02 = DD.fupcdes.copy().isAPartOf(filler16, 4);
	public FixedLengthStringData fupcdes03 = DD.fupcdes.copy().isAPartOf(filler16, 8);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(dataFields,643);
	public ZonedDecimalData cdateduration = DD.cdateduration.copyToZonedDecimal().isAPartOf(dataFields, 644);
	public ZonedDecimalData juvenileage = DD.juvenileage.copyToZonedDecimal().isAPartOf(dataFields, 646);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(552).isAPartOf(dataArea, 648);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData itmfrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itmtoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	
	public FixedLengthStringData mopsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] mopErr = FLSArrayPartOfStructure(5, 4, mopsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(20).isAPartOf(mopsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mop01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData mop02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData mop03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData mop04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData mop05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	
	public FixedLengthStringData billfreqsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] billfreqErr = FLSArrayPartOfStructure(10, 4, billfreqsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(billfreqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData billfreq02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData billfreq03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData billfreq04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData billfreq05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData billfreq06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData billfreq07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData billfreq08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData billfreq09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData billfreq10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	
	public FixedLengthStringData contitemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData minsuminErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData minDhbErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData minsumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	
	public FixedLengthStringData instprmsErr = new FixedLengthStringData(20).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData[] instprmErr = FLSArrayPartOfStructure(5, 4, instprmsErr, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(20).isAPartOf(instprmsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData instPrem01Err = new FixedLengthStringData(4).isAPartOf(filler9, 0);
	public FixedLengthStringData instPrem02Err = new FixedLengthStringData(4).isAPartOf(filler9, 4);
	public FixedLengthStringData instPrem03Err = new FixedLengthStringData(4).isAPartOf(filler9, 8);
	public FixedLengthStringData instPrem04Err = new FixedLengthStringData(4).isAPartOf(filler9, 12);
	public FixedLengthStringData instPrem05Err = new FixedLengthStringData(4).isAPartOf(filler9, 16);
	
	public FixedLengthStringData ctablesErr = new FixedLengthStringData(300).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData[] ctableErr = FLSArrayPartOfStructure(75, 4, ctablesErr, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(300).isAPartOf(ctablesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctable01Err = new FixedLengthStringData(4).isAPartOf(filler2, 0);
	public FixedLengthStringData ctable02Err = new FixedLengthStringData(4).isAPartOf(filler2, 4);
	public FixedLengthStringData ctable03Err = new FixedLengthStringData(4).isAPartOf(filler2, 8);
	public FixedLengthStringData ctable04Err = new FixedLengthStringData(4).isAPartOf(filler2, 12);
	public FixedLengthStringData ctable05Err = new FixedLengthStringData(4).isAPartOf(filler2, 16);
	public FixedLengthStringData ctable06Err = new FixedLengthStringData(4).isAPartOf(filler2, 20);
	public FixedLengthStringData ctable07Err = new FixedLengthStringData(4).isAPartOf(filler2, 24);
	public FixedLengthStringData ctable08Err = new FixedLengthStringData(4).isAPartOf(filler2, 28);
	public FixedLengthStringData ctable09Err = new FixedLengthStringData(4).isAPartOf(filler2, 32);
	public FixedLengthStringData ctable10Err = new FixedLengthStringData(4).isAPartOf(filler2, 36);
	public FixedLengthStringData ctable11Err = new FixedLengthStringData(4).isAPartOf(filler2, 40);
	public FixedLengthStringData ctable12Err = new FixedLengthStringData(4).isAPartOf(filler2, 44);
	public FixedLengthStringData ctable13Err = new FixedLengthStringData(4).isAPartOf(filler2, 48);
	public FixedLengthStringData ctable14Err = new FixedLengthStringData(4).isAPartOf(filler2, 52);
	public FixedLengthStringData ctable15Err = new FixedLengthStringData(4).isAPartOf(filler2, 56);
	public FixedLengthStringData ctable16Err = new FixedLengthStringData(4).isAPartOf(filler2, 60);
	public FixedLengthStringData ctable17Err = new FixedLengthStringData(4).isAPartOf(filler2, 64);
	public FixedLengthStringData ctable18Err = new FixedLengthStringData(4).isAPartOf(filler2, 68);
	public FixedLengthStringData ctable19Err = new FixedLengthStringData(4).isAPartOf(filler2, 72);
	public FixedLengthStringData ctable20Err = new FixedLengthStringData(4).isAPartOf(filler2, 76);
	public FixedLengthStringData ctable21Err = new FixedLengthStringData(4).isAPartOf(filler2, 80);
	public FixedLengthStringData ctable22Err = new FixedLengthStringData(4).isAPartOf(filler2, 84);
	public FixedLengthStringData ctable23Err = new FixedLengthStringData(4).isAPartOf(filler2, 88);
	public FixedLengthStringData ctable24Err = new FixedLengthStringData(4).isAPartOf(filler2, 92);
	public FixedLengthStringData ctable25Err = new FixedLengthStringData(4).isAPartOf(filler2, 96);
	public FixedLengthStringData ctable26Err = new FixedLengthStringData(4).isAPartOf(filler2, 100);
	public FixedLengthStringData ctable27Err = new FixedLengthStringData(4).isAPartOf(filler2, 104);
	public FixedLengthStringData ctable28Err = new FixedLengthStringData(4).isAPartOf(filler2, 108);
	public FixedLengthStringData ctable29Err = new FixedLengthStringData(4).isAPartOf(filler2, 112);
	public FixedLengthStringData ctable30Err = new FixedLengthStringData(4).isAPartOf(filler2, 116);
	public FixedLengthStringData ctable31Err = new FixedLengthStringData(4).isAPartOf(filler2, 120);
	public FixedLengthStringData ctable32Err = new FixedLengthStringData(4).isAPartOf(filler2, 124);
	public FixedLengthStringData ctable33Err = new FixedLengthStringData(4).isAPartOf(filler2, 128);
	public FixedLengthStringData ctable34Err = new FixedLengthStringData(4).isAPartOf(filler2, 132);
	public FixedLengthStringData ctable35Err = new FixedLengthStringData(4).isAPartOf(filler2, 136);
	public FixedLengthStringData ctable36Err = new FixedLengthStringData(4).isAPartOf(filler2, 140);
	public FixedLengthStringData ctable37Err = new FixedLengthStringData(4).isAPartOf(filler2, 144);
	public FixedLengthStringData ctable38Err = new FixedLengthStringData(4).isAPartOf(filler2, 148);
	public FixedLengthStringData ctable39Err = new FixedLengthStringData(4).isAPartOf(filler2, 152);
	public FixedLengthStringData ctable40Err = new FixedLengthStringData(4).isAPartOf(filler2, 156);
	public FixedLengthStringData ctable41Err = new FixedLengthStringData(4).isAPartOf(filler2, 160);
	public FixedLengthStringData ctable42Err = new FixedLengthStringData(4).isAPartOf(filler2, 164);
	public FixedLengthStringData ctable43Err = new FixedLengthStringData(4).isAPartOf(filler2, 168);
	public FixedLengthStringData ctable44Err = new FixedLengthStringData(4).isAPartOf(filler2, 172);
	public FixedLengthStringData ctable45Err = new FixedLengthStringData(4).isAPartOf(filler2, 176);
	public FixedLengthStringData ctable46Err = new FixedLengthStringData(4).isAPartOf(filler2, 180);
	public FixedLengthStringData ctable47Err = new FixedLengthStringData(4).isAPartOf(filler2, 184);
	public FixedLengthStringData ctable48Err = new FixedLengthStringData(4).isAPartOf(filler2, 188);
	public FixedLengthStringData ctable49Err = new FixedLengthStringData(4).isAPartOf(filler2, 192);
	public FixedLengthStringData ctable50Err = new FixedLengthStringData(4).isAPartOf(filler2, 196);
	public FixedLengthStringData ctable51Err = new FixedLengthStringData(4).isAPartOf(filler2, 200);
	public FixedLengthStringData ctable52Err = new FixedLengthStringData(4).isAPartOf(filler2, 204);
	public FixedLengthStringData ctable53Err = new FixedLengthStringData(4).isAPartOf(filler2, 208);
	public FixedLengthStringData ctable54Err = new FixedLengthStringData(4).isAPartOf(filler2, 212);
	public FixedLengthStringData ctable55Err = new FixedLengthStringData(4).isAPartOf(filler2, 216);
	public FixedLengthStringData ctable56Err = new FixedLengthStringData(4).isAPartOf(filler2, 220);
	public FixedLengthStringData ctable57Err = new FixedLengthStringData(4).isAPartOf(filler2, 224);
	public FixedLengthStringData ctable58Err = new FixedLengthStringData(4).isAPartOf(filler2, 228);
	public FixedLengthStringData ctable59Err = new FixedLengthStringData(4).isAPartOf(filler2, 232);
	public FixedLengthStringData ctable60Err = new FixedLengthStringData(4).isAPartOf(filler2, 236);
	public FixedLengthStringData ctable61Err = new FixedLengthStringData(4).isAPartOf(filler2, 240);
	public FixedLengthStringData ctable62Err = new FixedLengthStringData(4).isAPartOf(filler2, 244);
	public FixedLengthStringData ctable63Err = new FixedLengthStringData(4).isAPartOf(filler2, 248);
	public FixedLengthStringData ctable64Err = new FixedLengthStringData(4).isAPartOf(filler2, 252);
	public FixedLengthStringData ctable65Err = new FixedLengthStringData(4).isAPartOf(filler2, 256);
	public FixedLengthStringData ctable66Err = new FixedLengthStringData(4).isAPartOf(filler2, 260);
	public FixedLengthStringData ctable67Err = new FixedLengthStringData(4).isAPartOf(filler2, 264);
	public FixedLengthStringData ctable68Err = new FixedLengthStringData(4).isAPartOf(filler2, 268);
	public FixedLengthStringData ctable69Err = new FixedLengthStringData(4).isAPartOf(filler2, 272);
	public FixedLengthStringData ctable70Err = new FixedLengthStringData(4).isAPartOf(filler2, 276);
	public FixedLengthStringData ctable71Err = new FixedLengthStringData(4).isAPartOf(filler2, 280);
	public FixedLengthStringData ctable72Err = new FixedLengthStringData(4).isAPartOf(filler2, 284);
	public FixedLengthStringData ctable73Err = new FixedLengthStringData(4).isAPartOf(filler2, 288);
	public FixedLengthStringData ctable74Err = new FixedLengthStringData(4).isAPartOf(filler2, 292);
	public FixedLengthStringData ctable75Err = new FixedLengthStringData(4).isAPartOf(filler2, 296);
	
    public FixedLengthStringData hosbensErr = new FixedLengthStringData(108).isAPartOf(errorIndicators, 420);
	public FixedLengthStringData[] hosbenErr = FLSArrayPartOfStructure(27, 4, hosbensErr, 0);
	public FixedLengthStringData filler14 = new FixedLengthStringData(108).isAPartOf(hosbensErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData hosben01Err = new FixedLengthStringData(4).isAPartOf(filler14, 0);
	public FixedLengthStringData hosben02Err = new FixedLengthStringData(4).isAPartOf(filler14, 4);
	public FixedLengthStringData hosben03Err = new FixedLengthStringData(4).isAPartOf(filler14, 8);
	public FixedLengthStringData hosben04Err = new FixedLengthStringData(4).isAPartOf(filler14, 12);
	public FixedLengthStringData hosben05Err = new FixedLengthStringData(4).isAPartOf(filler14, 16);
	public FixedLengthStringData hosben06Err = new FixedLengthStringData(4).isAPartOf(filler14, 20);
	public FixedLengthStringData hosben07Err = new FixedLengthStringData(4).isAPartOf(filler14, 24);
	public FixedLengthStringData hosben08Err = new FixedLengthStringData(4).isAPartOf(filler14, 28);
	public FixedLengthStringData hosben09Err = new FixedLengthStringData(4).isAPartOf(filler14, 32);
	public FixedLengthStringData hosben10Err = new FixedLengthStringData(4).isAPartOf(filler14, 36);
	public FixedLengthStringData hosben11Err = new FixedLengthStringData(4).isAPartOf(filler14, 40);
	public FixedLengthStringData hosben12Err = new FixedLengthStringData(4).isAPartOf(filler14, 44);
	public FixedLengthStringData hosben13Err = new FixedLengthStringData(4).isAPartOf(filler14, 48);
	public FixedLengthStringData hosben14Err = new FixedLengthStringData(4).isAPartOf(filler14, 52);
	public FixedLengthStringData hosben15Err = new FixedLengthStringData(4).isAPartOf(filler14, 56);
	public FixedLengthStringData hosben16Err = new FixedLengthStringData(4).isAPartOf(filler14, 60);
	public FixedLengthStringData hosben17Err = new FixedLengthStringData(4).isAPartOf(filler14, 64);
	public FixedLengthStringData hosben18Err = new FixedLengthStringData(4).isAPartOf(filler14, 68);
	public FixedLengthStringData hosben19Err = new FixedLengthStringData(4).isAPartOf(filler14, 72);
	public FixedLengthStringData hosben20Err = new FixedLengthStringData(4).isAPartOf(filler14, 76);
	public FixedLengthStringData hosben21Err = new FixedLengthStringData(4).isAPartOf(filler14, 80);
	public FixedLengthStringData hosben22Err = new FixedLengthStringData(4).isAPartOf(filler14, 84);
	public FixedLengthStringData hosben23Err = new FixedLengthStringData(4).isAPartOf(filler14, 88);
	public FixedLengthStringData hosben24Err = new FixedLengthStringData(4).isAPartOf(filler14, 92);
	public FixedLengthStringData hosben25Err = new FixedLengthStringData(4).isAPartOf(filler14, 96);
	public FixedLengthStringData hosben26Err = new FixedLengthStringData(4).isAPartOf(filler14, 100);
	public FixedLengthStringData hosben27Err = new FixedLengthStringData(4).isAPartOf(filler14, 104);
	
	public FixedLengthStringData fupcdesErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 528);
	public FixedLengthStringData[] fupcdeErr = FLSArrayPartOfStructure(3, 4, fupcdesErr, 0);
	public FixedLengthStringData filler17 = new FixedLengthStringData(12).isAPartOf(fupcdesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData fupcdes01Err = new FixedLengthStringData(4).isAPartOf(filler17, 0);
	public FixedLengthStringData fupcdes02Err = new FixedLengthStringData(4).isAPartOf(filler17, 4);
	public FixedLengthStringData fupcdes03Err = new FixedLengthStringData(4).isAPartOf(filler17, 8);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 540);
	public FixedLengthStringData cdatedurationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 544);
	public FixedLengthStringData juvenileageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 548);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1656).isAPartOf(dataArea, 1200);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] itmfrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itmtoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	
	public FixedLengthStringData mopsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(5, 12, mopsOut, 0);
	public FixedLengthStringData[][] mopO = FLSDArrayPartOfArrayStructure(12, 1, mopOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(60).isAPartOf(mopsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mop01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] mop02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] mop03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] mop04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] mop05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	
	public FixedLengthStringData billfreqsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(10, 12, billfreqsOut, 0);
	public FixedLengthStringData[][] billfreqO = FLSDArrayPartOfArrayStructure(12, 1, billfreqOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(120).isAPartOf(billfreqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] billfreq01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] billfreq02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] billfreq03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] billfreq04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] billfreq05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] billfreq06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] billfreq07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] billfreq08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] billfreq09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] billfreq10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	
	public FixedLengthStringData[] contitemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] minsuminOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] minDhbOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] minsumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	
	
	public FixedLengthStringData instPremsOut = new FixedLengthStringData(60).isAPartOf(outputIndicators, 300);
	public FixedLengthStringData[] instPremOut = FLSArrayPartOfStructure(5, 12, instPremsOut, 0);
	public FixedLengthStringData[][] instPremO = FLSDArrayPartOfArrayStructure(12, 1, instPremOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(60).isAPartOf(instPremsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] instPrem01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] instPrem02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] instPrem03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] instPrem04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] instPrem05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	
	public FixedLengthStringData ctablesOut = new FixedLengthStringData(900).isAPartOf(outputIndicators, 360);
	public FixedLengthStringData[] ctableOut = FLSArrayPartOfStructure(75, 12, ctablesOut, 0);
	public FixedLengthStringData[][] ctableO = FLSDArrayPartOfArrayStructure(12, 1, ctableOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(900).isAPartOf(ctablesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ctable01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] ctable02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] ctable03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] ctable04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] ctable05Out = FLSArrayPartOfStructure(12, 1, filler12, 48);
	public FixedLengthStringData[] ctable06Out = FLSArrayPartOfStructure(12, 1, filler12, 60);
	public FixedLengthStringData[] ctable07Out = FLSArrayPartOfStructure(12, 1, filler12, 72);
	public FixedLengthStringData[] ctable08Out = FLSArrayPartOfStructure(12, 1, filler12, 84);
	public FixedLengthStringData[] ctable09Out = FLSArrayPartOfStructure(12, 1, filler12, 96);
	public FixedLengthStringData[] ctable10Out = FLSArrayPartOfStructure(12, 1, filler12, 108);
	public FixedLengthStringData[] ctable11Out = FLSArrayPartOfStructure(12, 1, filler12, 120);
	public FixedLengthStringData[] ctable12Out = FLSArrayPartOfStructure(12, 1, filler12, 132);
	public FixedLengthStringData[] ctable13Out = FLSArrayPartOfStructure(12, 1, filler12, 144);
	public FixedLengthStringData[] ctable14Out = FLSArrayPartOfStructure(12, 1, filler12, 156);
	public FixedLengthStringData[] ctable15Out = FLSArrayPartOfStructure(12, 1, filler12, 168);
	public FixedLengthStringData[] ctable16Out = FLSArrayPartOfStructure(12, 1, filler12, 180);
	public FixedLengthStringData[] ctable17Out = FLSArrayPartOfStructure(12, 1, filler12, 192);
	public FixedLengthStringData[] ctable18Out = FLSArrayPartOfStructure(12, 1, filler12, 204);
	public FixedLengthStringData[] ctable19Out = FLSArrayPartOfStructure(12, 1, filler12, 216);
	public FixedLengthStringData[] ctable20Out = FLSArrayPartOfStructure(12, 1, filler12, 228);
	public FixedLengthStringData[] ctable21Out = FLSArrayPartOfStructure(12, 1, filler12, 240);
	public FixedLengthStringData[] ctable22Out = FLSArrayPartOfStructure(12, 1, filler12, 252);
	public FixedLengthStringData[] ctable23Out = FLSArrayPartOfStructure(12, 1, filler12, 264);
	public FixedLengthStringData[] ctable24Out = FLSArrayPartOfStructure(12, 1, filler12, 276);
	public FixedLengthStringData[] ctable25Out = FLSArrayPartOfStructure(12, 1, filler12, 288);
	public FixedLengthStringData[] ctable26Out = FLSArrayPartOfStructure(12, 1, filler12, 300);
	public FixedLengthStringData[] ctable27Out = FLSArrayPartOfStructure(12, 1, filler12, 312);
	public FixedLengthStringData[] ctable28Out = FLSArrayPartOfStructure(12, 1, filler12, 324);
	public FixedLengthStringData[] ctable29Out = FLSArrayPartOfStructure(12, 1, filler12, 336);
	public FixedLengthStringData[] ctable30Out = FLSArrayPartOfStructure(12, 1, filler12, 348);
	public FixedLengthStringData[] ctable31Out = FLSArrayPartOfStructure(12, 1, filler12, 360);
	public FixedLengthStringData[] ctable32Out = FLSArrayPartOfStructure(12, 1, filler12, 372);
	public FixedLengthStringData[] ctable33Out = FLSArrayPartOfStructure(12, 1, filler12, 384);
	public FixedLengthStringData[] ctable34Out = FLSArrayPartOfStructure(12, 1, filler12, 396);
	public FixedLengthStringData[] ctable35Out = FLSArrayPartOfStructure(12, 1, filler12, 408);
	public FixedLengthStringData[] ctable36Out = FLSArrayPartOfStructure(12, 1, filler12, 420);
	public FixedLengthStringData[] ctable37Out = FLSArrayPartOfStructure(12, 1, filler12, 432);
	public FixedLengthStringData[] ctable38Out = FLSArrayPartOfStructure(12, 1, filler12, 444);
	public FixedLengthStringData[] ctable39Out = FLSArrayPartOfStructure(12, 1, filler12, 456);
	public FixedLengthStringData[] ctable40Out = FLSArrayPartOfStructure(12, 1, filler12, 468);
	public FixedLengthStringData[] ctable41Out = FLSArrayPartOfStructure(12, 1, filler12, 480);
	public FixedLengthStringData[] ctable42Out = FLSArrayPartOfStructure(12, 1, filler12, 492);
	public FixedLengthStringData[] ctable43Out = FLSArrayPartOfStructure(12, 1, filler12, 504);
	public FixedLengthStringData[] ctable44Out = FLSArrayPartOfStructure(12, 1, filler12, 516);
	public FixedLengthStringData[] ctable45Out = FLSArrayPartOfStructure(12, 1, filler12, 528);
	public FixedLengthStringData[] ctable46Out = FLSArrayPartOfStructure(12, 1, filler12, 540);
	public FixedLengthStringData[] ctable47Out = FLSArrayPartOfStructure(12, 1, filler12, 552);
	public FixedLengthStringData[] ctable48Out = FLSArrayPartOfStructure(12, 1, filler12, 564);
	public FixedLengthStringData[] ctable49Out = FLSArrayPartOfStructure(12, 1, filler12, 576);
	public FixedLengthStringData[] ctable50Out = FLSArrayPartOfStructure(12, 1, filler12, 588);
	public FixedLengthStringData[] ctable51Out = FLSArrayPartOfStructure(12, 1, filler12, 600);
	public FixedLengthStringData[] ctable52Out = FLSArrayPartOfStructure(12, 1, filler12, 612);
	public FixedLengthStringData[] ctable53Out = FLSArrayPartOfStructure(12, 1, filler12, 624);
	public FixedLengthStringData[] ctable54Out = FLSArrayPartOfStructure(12, 1, filler12, 636);
	public FixedLengthStringData[] ctable55Out = FLSArrayPartOfStructure(12, 1, filler12, 648);
	public FixedLengthStringData[] ctable56Out = FLSArrayPartOfStructure(12, 1, filler12, 660);
	public FixedLengthStringData[] ctable57Out = FLSArrayPartOfStructure(12, 1, filler12, 672);
	public FixedLengthStringData[] ctable58Out = FLSArrayPartOfStructure(12, 1, filler12, 684);
	public FixedLengthStringData[] ctable59Out = FLSArrayPartOfStructure(12, 1, filler12, 696);
	public FixedLengthStringData[] ctable60Out = FLSArrayPartOfStructure(12, 1, filler12, 708);
	public FixedLengthStringData[] ctable61Out = FLSArrayPartOfStructure(12, 1, filler12, 720);
	public FixedLengthStringData[] ctable62Out = FLSArrayPartOfStructure(12, 1, filler12, 732);
	public FixedLengthStringData[] ctable63Out = FLSArrayPartOfStructure(12, 1, filler12, 744);
	public FixedLengthStringData[] ctable64Out = FLSArrayPartOfStructure(12, 1, filler12, 756);
	public FixedLengthStringData[] ctable65Out = FLSArrayPartOfStructure(12, 1, filler12, 768);
	public FixedLengthStringData[] ctable66Out = FLSArrayPartOfStructure(12, 1, filler12, 780);
	public FixedLengthStringData[] ctable67Out = FLSArrayPartOfStructure(12, 1, filler12, 792);
	public FixedLengthStringData[] ctable68Out = FLSArrayPartOfStructure(12, 1, filler12, 804);
	public FixedLengthStringData[] ctable69Out = FLSArrayPartOfStructure(12, 1, filler12, 816);
	public FixedLengthStringData[] ctable70Out = FLSArrayPartOfStructure(12, 1, filler12, 828);
	public FixedLengthStringData[] ctable71Out = FLSArrayPartOfStructure(12, 1, filler12, 840);
	public FixedLengthStringData[] ctable72Out = FLSArrayPartOfStructure(12, 1, filler12, 852);
	public FixedLengthStringData[] ctable73Out = FLSArrayPartOfStructure(12, 1, filler12, 864);
	public FixedLengthStringData[] ctable74Out = FLSArrayPartOfStructure(12, 1, filler12, 876);
	public FixedLengthStringData[] ctable75Out = FLSArrayPartOfStructure(12, 1, filler12, 888);
	
	public FixedLengthStringData hosbensOut = new FixedLengthStringData(324).isAPartOf(outputIndicators, 1260);
	public FixedLengthStringData[] hosbenOut = FLSArrayPartOfStructure(27, 12, hosbensOut, 0);
	public FixedLengthStringData[][] hosbenO = FLSDArrayPartOfArrayStructure(12, 1, hosbenOut, 0);
	public FixedLengthStringData filler15 = new FixedLengthStringData(324).isAPartOf(hosbensOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] hosben01Out = FLSArrayPartOfStructure(12, 1, filler15, 0);
	public FixedLengthStringData[] hosben02Out = FLSArrayPartOfStructure(12, 1, filler15, 12);
	public FixedLengthStringData[] hosben03Out = FLSArrayPartOfStructure(12, 1, filler15, 24);
	public FixedLengthStringData[] hosben04Out = FLSArrayPartOfStructure(12, 1, filler15, 36);
	public FixedLengthStringData[] hosben05Out = FLSArrayPartOfStructure(12, 1, filler15, 48);
	public FixedLengthStringData[] hosben06Out = FLSArrayPartOfStructure(12, 1, filler15, 60);
	public FixedLengthStringData[] hosben07Out = FLSArrayPartOfStructure(12, 1, filler15, 72);
	public FixedLengthStringData[] hosben08Out = FLSArrayPartOfStructure(12, 1, filler15, 84);
	public FixedLengthStringData[] hosben09Out = FLSArrayPartOfStructure(12, 1, filler15, 96);
	public FixedLengthStringData[] hosben10Out = FLSArrayPartOfStructure(12, 1, filler15, 108);
	public FixedLengthStringData[] hosben11Out = FLSArrayPartOfStructure(12, 1, filler15, 120);
	public FixedLengthStringData[] hosben12Out = FLSArrayPartOfStructure(12, 1, filler15, 132);
	public FixedLengthStringData[] hosben13Out = FLSArrayPartOfStructure(12, 1, filler15, 144);
	public FixedLengthStringData[] hosben14Out = FLSArrayPartOfStructure(12, 1, filler15, 156);
	public FixedLengthStringData[] hosben15Out = FLSArrayPartOfStructure(12, 1, filler15, 168);
	public FixedLengthStringData[] hosben16Out = FLSArrayPartOfStructure(12, 1, filler15, 180);
	public FixedLengthStringData[] hosben17Out = FLSArrayPartOfStructure(12, 1, filler15, 192);
	public FixedLengthStringData[] hosben18Out = FLSArrayPartOfStructure(12, 1, filler15, 204);
	public FixedLengthStringData[] hosben19Out = FLSArrayPartOfStructure(12, 1, filler15, 216);
	public FixedLengthStringData[] hosben20Out = FLSArrayPartOfStructure(12, 1, filler15, 228);
	public FixedLengthStringData[] hosben21Out = FLSArrayPartOfStructure(12, 1, filler15, 240);
	public FixedLengthStringData[] hosben22Out = FLSArrayPartOfStructure(12, 1, filler15, 252);
	public FixedLengthStringData[] hosben23Out = FLSArrayPartOfStructure(12, 1, filler15, 264);
	public FixedLengthStringData[] hosben24Out = FLSArrayPartOfStructure(12, 1, filler15, 276);
	public FixedLengthStringData[] hosben25Out = FLSArrayPartOfStructure(12, 1, filler15, 288);
	public FixedLengthStringData[] hosben26Out = FLSArrayPartOfStructure(12, 1, filler15, 300);
	public FixedLengthStringData[] hosben27Out = FLSArrayPartOfStructure(12, 1, filler15, 312);

	public FixedLengthStringData fupcdesOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 1584);
	public FixedLengthStringData[] fupcdeOut = FLSArrayPartOfStructure(3, 12, fupcdesOut, 0);
	public FixedLengthStringData[][] fupcdeO = FLSDArrayPartOfArrayStructure(12, 1, fupcdeOut, 0);
	public FixedLengthStringData filler18 = new FixedLengthStringData(36).isAPartOf(fupcdesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] fupcdes01Out = FLSArrayPartOfStructure(12, 1, filler18, 0);
	public FixedLengthStringData[] fupcdes02Out = FLSArrayPartOfStructure(12, 1, filler18, 12);
	public FixedLengthStringData[] fupcdes03Out = FLSArrayPartOfStructure(12, 1, filler18, 24);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 1620);
	public FixedLengthStringData[] cdatedurationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 1632);
	public FixedLengthStringData[] juvenileageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 1644);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData itmfrmDisp = new FixedLengthStringData(10);
	public FixedLengthStringData itmtoDisp = new FixedLengthStringData(10);

	public LongData Sjl06screenWritten = new LongData(0);
	public LongData Sjl06protectWritten = new LongData(0);

	@Override
	public boolean hasSubfile() {
		return false;
	}

	public Sjl06ScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected void initialiseScreenVars() {
		fieldIndMap.put(mop01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq01Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(mop02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq02Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(mop03Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq03Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(mop04Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq04Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(mop05Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq05Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(billfreq06Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq07Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq08Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq09Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreq10Out,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(contitemOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minsuminOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minDhbOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(minsumOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instPrem01Out,new String[] {"17",null, "-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instPrem02Out,new String[] {"18",null, "-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instPrem03Out,new String[] {"19",null, "-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instPrem04Out,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(instPrem05Out,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable01Out,new String[] {"26",null, "-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable02Out,new String[] {"27",null, "-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable03Out,new String[] {"28",null, "-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable04Out,new String[] {"29",null, "-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable05Out,new String[] {"30",null, "-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable06Out,new String[] {"31",null, "-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable07Out,new String[] {"32",null, "-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable08Out,new String[] {"33",null, "-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable09Out,new String[] {"34",null, "-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable10Out,new String[] {"35",null, "-35",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable11Out,new String[] {"36",null, "-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable12Out,new String[] {"37",null, "-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable13Out,new String[] {"38",null, "-38",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable14Out,new String[] {"39",null, "-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable15Out,new String[] {"40",null, "-40",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable16Out,new String[] {"41",null, "-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable17Out,new String[] {"42",null, "-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable18Out,new String[] {"43",null, "-43",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable19Out,new String[] {"44",null, "-44",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable20Out,new String[] {"45",null, "-45",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable21Out,new String[] {"46",null, "-46",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable22Out,new String[] {"47",null, "-47",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable23Out,new String[] {"48",null, "-48",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable24Out,new String[] {"49",null, "-49",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable25Out,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable26Out,new String[] {"51",null, "-51",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable27Out,new String[] {"52",null, "-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable28Out,new String[] {"53",null, "-53",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable29Out,new String[] {"54",null, "-54",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable30Out,new String[] {"55",null, "-55",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable31Out,new String[] {"56",null, "-56",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable32Out,new String[] {"57",null, "-57",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable33Out,new String[] {"58",null, "-58",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable34Out,new String[] {"59",null, "-59",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable35Out,new String[] {"60",null, "-60",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable36Out,new String[] {"61",null, "-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable37Out,new String[] {"62",null, "-52",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable38Out,new String[] {"63",null, "-63",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable39Out,new String[] {"64",null, "-64",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable40Out,new String[] {"65",null, "-65",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable41Out,new String[] {"66",null, "-66",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable42Out,new String[] {"67",null, "-67",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable43Out,new String[] {"68",null, "-68",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable44Out,new String[] {"69",null, "-69",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctable45Out,new String[] {"70",null, "-70",null, null, null, null, null, null, null, null, null});
		
		fieldIndMap.put(hosben01Out,new String[] {"71",null, "-61",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben02Out,new String[] {"72",null, "-72",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben03Out,new String[] {"73",null, "-73",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben04Out,new String[] {"74",null, "-74",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben05Out,new String[] {"75",null, "-75",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben06Out,new String[] {"76",null, "-76",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben07Out,new String[] {"77",null, "-77",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben08Out,new String[] {"78",null, "-78",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben09Out,new String[] {"79",null, "-79",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben10Out,new String[] {"80",null, "-80",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben11Out,new String[] {"81",null, "-81",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben12Out,new String[] {"82",null, "-82",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben13Out,new String[] {"83",null, "-83",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben14Out,new String[] {"84",null, "-84",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben15Out,new String[] {"85",null, "-85",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben16Out,new String[] {"86",null, "-86",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben17Out,new String[] {"87",null, "-87",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben18Out,new String[] {"88",null, "-88",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben19Out,new String[] {"89",null, "-89",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben20Out,new String[] {"90",null, "-90",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben21Out,new String[] {"91",null, "-91",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben22Out,new String[] {"92",null, "-92",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben23Out,new String[] {"93",null, "-93",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben24Out,new String[] {"94",null, "-94",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben25Out,new String[] {"95",null, "-95",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben26Out,new String[] {"96",null, "-96",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben26Out,new String[] {"100",null, "-100",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(hosben27Out,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes01Out,new String[] {"101",null, "-101",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes02Out,new String[] {"102",null, "-102",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupcdes03Out,new String[] {"103",null, "-103",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cdatedurationOut,new String[] {"104",null,"-104",null,null,null,null,null,null,null,null,null});
		fieldIndMap.put(juvenileageOut,new String[] {"105",null,"-104",null,null,null,null,null,null,null,null,null});
		
		screenFields = new BaseData[] { company, tabl, item, longdesc, itmfrm, itmto,mop01, billfreq01, mop02, billfreq02,
										mop03, billfreq03, mop04, billfreq04, mop05, billfreq05, billfreq06, billfreq07, billfreq08, billfreq09, 
										billfreq10, contitem, minsumin, minDhb , minsum, instPrem01, instPrem02, instPrem03, instPrem04, instPrem05, 
										ctable01, ctable02, ctable03, ctable04, ctable05, ctable06, ctable07, ctable08, ctable09, ctable10, ctable11, 
										ctable12, ctable13, ctable14, ctable15, ctable16, ctable17, ctable18, ctable19, ctable20, ctable21, ctable22, 
										ctable23, ctable24, ctable25, ctable26, ctable27, ctable28, ctable29, ctable30, ctable31, ctable32, ctable33, 
										ctable34, ctable35, ctable36, ctable37, ctable38, ctable39, ctable40, ctable41, ctable42, ctable43, ctable44, 
										ctable45, ctable46, ctable47, ctable48, ctable49, ctable50, ctable51, ctable52, ctable53, ctable54, ctable55, 
										ctable56, ctable57, ctable58, ctable59, ctable60, ctable61, ctable62, ctable63, ctable64, ctable65, ctable66, 
										ctable67, ctable68, ctable69, ctable70, ctable71, ctable72, ctable73, ctable74, ctable75, hosben01, hosben02, 
										hosben03, hosben04, hosben05, hosben06, hosben07, hosben08, hosben09, hosben10, hosben11, hosben12, hosben13, 
										hosben14, hosben15, hosben16, hosben17, hosben18, hosben19, hosben20, hosben21, hosben22, hosben23, hosben24, 
										hosben25, hosben26, hosben27, fupcdes01, fupcdes02, fupcdes03, language, cdateduration, juvenileage};
		screenOutFields = new BaseData[][] { companyOut, tablOut, itemOut, longdescOut, itmfrmOut, itmtoOut,  mop01Out,
											billfreq01Out, mop02Out, billfreq02Out,  mop03Out, billfreq03Out, mop04Out, billfreq04Out, mop05Out,  
											billfreq05Out, billfreq06Out, billfreq07Out, billfreq08Out, billfreq09Out, billfreq10Out, contitemOut,  
											minsuminOut, minDhbOut, minsumOut, instPrem01Out,instPrem02Out,instPrem03Out,instPrem04Out,instPrem05Out, 
											ctable01Out, ctable02Out, ctable03Out, ctable04Out, ctable05Out, ctable06Out, ctable07Out, ctable08Out,  
											ctable09Out, ctable10Out, ctable11Out, ctable12Out, ctable13Out, ctable14Out, ctable15Out, ctable16Out,  
											ctable17Out, ctable18Out, ctable19Out, ctable20Out, ctable21Out, ctable22Out, ctable23Out, ctable24Out,  
											ctable25Out, ctable26Out, ctable27Out, ctable28Out, ctable29Out, ctable30Out, ctable31Out, ctable32Out,  
											ctable33Out, ctable34Out, ctable35Out, ctable36Out, ctable37Out, ctable38Out, ctable39Out, ctable40Out,  
											ctable41Out, ctable42Out, ctable43Out, ctable44Out, ctable45Out, ctable46Out, ctable47Out, ctable48Out,  
											ctable49Out, ctable50Out, ctable51Out, ctable52Out, ctable53Out, ctable54Out, ctable55Out, ctable56Out,  
											ctable57Out, ctable58Out, ctable59Out, ctable60Out, ctable61Out, ctable62Out, ctable63Out, ctable64Out,  
											ctable65Out, ctable66Out, ctable67Out, ctable68Out, ctable69Out, ctable70Out, ctable71Out, ctable72Out,  
											ctable73Out, ctable74Out, ctable75Out, hosben01Out, hosben02Out, hosben03Out, hosben04Out, hosben05Out,  
											hosben06Out, hosben07Out, hosben08Out, hosben09Out, hosben10Out, hosben11Out, hosben12Out, hosben13Out,  
											hosben14Out, hosben15Out, hosben16Out, hosben17Out, hosben18Out, hosben19Out, hosben20Out, hosben21Out, 
											hosben22Out, hosben23Out, hosben24Out, hosben25Out, hosben26Out, hosben27Out, fupcdes01Out, fupcdes02Out, 
											fupcdes03Out, languageOut, cdatedurationOut, juvenileageOut};
		screenErrFields = new BaseData[] { companyErr, tablErr, itemErr, longdescErr, itmfrmErr, itmtoErr,  mop01Err, 
										   billfreq01Err, mop02Err, billfreq02Err,  mop03Err, billfreq03Err, mop04Err, billfreq04Err, mop05Err, 
										   billfreq05Err, billfreq06Err, billfreq07Err, billfreq08Err, billfreq09Err, billfreq10Err, contitemErr, 
										   minsuminErr,minDhbErr,minsumErr,instPrem01Err,instPrem02Err,instPrem03Err,instPrem04Err,instPrem05Err,  
										   ctable01Err, ctable02Err, ctable03Err, ctable04Err, ctable05Err, ctable06Err, ctable07Err, ctable08Err, 
										   ctable09Err, ctable10Err, ctable11Err, ctable12Err, ctable13Err, ctable14Err, ctable15Err, ctable16Err, 
										   ctable17Err, ctable18Err, ctable19Err, ctable20Err, ctable21Err, ctable22Err, ctable23Err, ctable24Err, 
										   ctable25Err, ctable26Err, ctable27Err, ctable28Err, ctable29Err, ctable30Err, ctable31Err, ctable32Err, 
										   ctable33Err, ctable34Err, ctable35Err, ctable36Err,  ctable37Err, ctable38Err, ctable39Err, ctable40Err, 
										   ctable41Err, ctable42Err, ctable43Err, ctable44Err, ctable45Err, ctable46Err, ctable47Err, ctable48Err, 
										   ctable49Err, ctable50Err,   ctable51Err, ctable52Err, ctable53Err, ctable54Err, ctable55Err, ctable56Err, 
										   ctable57Err, ctable58Err, ctable59Err, ctable60Err, ctable61Err, ctable62Err, ctable63Err, ctable64Err, 
										   ctable65Err, ctable66Err, ctable67Err, ctable68Err, ctable69Err, ctable70Err, ctable71Err, ctable72Err, 
										   ctable73Err, ctable74Err, ctable75Err,  hosben01Err, hosben02Err, hosben03Err, hosben04Err, hosben05Err, 
										   hosben06Err, hosben07Err, hosben08Err, hosben09Err, hosben10Err, hosben11Err, hosben12Err, hosben13Err, 
										   hosben14Err, hosben15Err, hosben16Err, hosben17Err, hosben18Err, hosben19Err, hosben20Err, hosben21Err, 
										   hosben22Err, hosben23Err, hosben24Err, hosben25Err, hosben26Err, hosben27Err, fupcdes01Err, fupcdes02Err, 
										   fupcdes03Err, languageErr, cdatedurationErr, juvenileageErr};
		screenDateFields = new BaseData[] { itmfrm, itmto };
		screenDateErrFields = new BaseData[] { itmfrmErr, itmtoErr };
		screenDateDispFields = new BaseData[] { itmfrmDisp, itmtoDisp };

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl06screen.class;
		protectRecord = Sjl06protect.class;
	}
}