package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5213
 * @version 1.0 generated on 30/08/09 06:37
 * @author Quipoz
 */
public class S5213ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(67);
	public FixedLengthStringData dataFields = new FixedLengthStringData(19).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrsel = DD.chdrsel.copy().isAPartOf(dataFields,1);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,11);
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 19);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
			
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 31);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] chdrselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5213screenWritten = new LongData(0);
	public LongData S5213protectWritten = new LongData(0);
	
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public boolean hasSubfile() {
		return false;
	}


	public S5213ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(actionOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"03",null, "-03","04", null, null, null, null, null, null, null, null});
		
		screenFields = new BaseData[] {chdrsel, action,effdate};
		screenOutFields = new BaseData[][] {chdrselOut, actionOut,effdateOut};
		screenErrFields = new BaseData[] {chdrselErr, actionErr,effdateErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S5213screen.class;
		protectRecord = S5213protect.class;
	}

}
