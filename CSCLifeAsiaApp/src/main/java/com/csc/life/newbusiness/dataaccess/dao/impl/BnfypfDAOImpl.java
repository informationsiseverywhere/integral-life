/******************************************************************************
 * File Name 		: BnfypfDAOImpl.java
 * Author			: nloganathan5
 * Creation Date	: 28 October 2016
 * Project			: Integral Life
 * Description		: The DAO Implementation Class for BNFYPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.BnfypfDAO;
import com.csc.life.newbusiness.dataaccess.model.Bnfypf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class BnfypfDAOImpl extends BaseDAOImpl<Bnfypf> implements BnfypfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(BnfypfDAOImpl.class);

	public List<Bnfypf> searchBnfypfRecord(String coy, String chdrNum) throws SQLRuntimeException{

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, BNYCLT, TRANNO, VALIDFLAG, ");
		sqlSelect.append("CURRFR, CURRTO, BNYRLN, BNYCD, BNYPC, ");
		sqlSelect.append("TERMID, USER_T, TRTM, TRDT, EFFDATE, ");
		sqlSelect.append("USRPRF, JOBNM, DATIME, BNYTYPE, CLTRELN, ");
		sqlSelect.append("RELTO, SELFIND, REVCFLG ");
		sqlSelect.append("FROM BNFYPF WHERE CHDRCOY=? AND CHDRNUM =? ");
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, BNYCLT ASC, UNIQUE_NUMBER ASC");


		PreparedStatement psBnfypfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlBnfypfRs = null;
		List<Bnfypf> outputList = new ArrayList<Bnfypf>();

		try {

			psBnfypfSelect.setString(1, coy);
			psBnfypfSelect.setString(2, chdrNum);

			sqlBnfypfRs = executeQuery(psBnfypfSelect);

			while (sqlBnfypfRs.next()) {

				Bnfypf bnfypf = new Bnfypf();

				bnfypf.setUniqueNumber(sqlBnfypfRs.getInt("UNIQUE_NUMBER"));
				bnfypf.setChdrcoy(getCharFromString(sqlBnfypfRs,"CHDRCOY"));
				bnfypf.setChdrnum(sqlBnfypfRs.getString("CHDRNUM"));
				bnfypf.setBnyclt(sqlBnfypfRs.getString("BNYCLT"));
				bnfypf.setTranno(sqlBnfypfRs.getInt("TRANNO"));
				bnfypf.setValidflag(getCharFromString(sqlBnfypfRs, "VALIDFLAG"));
				bnfypf.setCurrfr(sqlBnfypfRs.getInt("CURRFR"));
				bnfypf.setCurrto(sqlBnfypfRs.getInt("CURRTO"));
				bnfypf.setBnyrln(sqlBnfypfRs.getString("BNYRLN"));
				bnfypf.setBnycd(getCharFromString(sqlBnfypfRs,"BNYCD"));
				bnfypf.setBnypc(sqlBnfypfRs.getBigDecimal("BNYPC"));
				bnfypf.setTermid(sqlBnfypfRs.getString("TERMID"));
				bnfypf.setUserT(sqlBnfypfRs.getInt("USER_T"));
				bnfypf.setTrtm(sqlBnfypfRs.getInt("TRTM"));
				bnfypf.setTrdt(sqlBnfypfRs.getInt("TRDT"));
				bnfypf.setEffdate(sqlBnfypfRs.getInt("EFFDATE"));
				bnfypf.setUsrprf(sqlBnfypfRs.getString("USRPRF"));
				bnfypf.setJobnm(sqlBnfypfRs.getString("JOBNM"));
				bnfypf.setDatime(sqlBnfypfRs.getDate("DATIME"));
				bnfypf.setBnytype(sqlBnfypfRs.getString("BNYTYPE"));
				bnfypf.setCltreln(sqlBnfypfRs.getString("CLTRELN"));
				bnfypf.setRelto(getCharFromString(sqlBnfypfRs,"RELTO"));
				bnfypf.setSelfind(getCharFromString(sqlBnfypfRs,"SELFIND"));
				bnfypf.setRevcflg(getCharFromString(sqlBnfypfRs,"REVCFLG"));

				outputList.add(bnfypf);
			}

		} catch (SQLException e) {
				LOGGER.error("searchBnfypfRecord()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(psBnfypfSelect, sqlBnfypfRs);
		}

		return outputList;
	}
	
	private Character getCharFromString(ResultSet sqlpCovt1Rs , String columnName) throws SQLException{
		
		Character characterOutput = null;
		
		String stringOutput = sqlpCovt1Rs.getString(columnName);
		
		if(null != stringOutput){
			characterOutput = stringOutput.charAt(0);
		}
		
		return characterOutput;
	}
	
	 public void updateBnfyRecord(List<Bnfypf> updateBnfypflist) {
	        if (updateBnfypflist != null && updateBnfypflist.isEmpty()) {
	            String SQL_UPDATE = "UPDATE BNFYPF SET TRANNO=?,VALIDFLAG=?,JOBNM=?,USRPRF=?,DATIME=? WHERE UNIQUE_NUMBER=? ";

	            PreparedStatement psBnfyUpdate = getPrepareStatement(SQL_UPDATE);
	            try {
	                for (Bnfypf c : updateBnfypflist) {
	                	psBnfyUpdate.setInt(1,c.getTranno());
	                	psBnfyUpdate.setInt(2,c.getValidflag());
	                	psBnfyUpdate.setString(3, getJobnm());
	                	psBnfyUpdate.setString(4, getUsrprf());
	                	psBnfyUpdate.setTimestamp(5, getDatime());
	                	psBnfyUpdate.setLong(6, c.getUniqueNumber());
	                	psBnfyUpdate.addBatch();
	                }
	                psBnfyUpdate.executeBatch();
	            } catch (SQLException e) {
	                LOGGER.error("updateBnfyRecord()", e);//IJTI-1561
	                throw new SQLRuntimeException(e);
	            } finally {
	                close(psBnfyUpdate, null);
	            }
	        }
	    }
	
	public List<Bnfypf> getBnfymnaByCoyAndNum(String coy, String chdrNum) throws SQLRuntimeException{

		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, BNYCLT, TRANNO, VALIDFLAG, BNYRLN, BNYCD, BNYPC, CURRTO, CURRFR, TERMID, USER_T, ");
        sb.append("TRTM, TRDT, EFFDATE, USRPRF, JOBNM, DATIME, BNYTYPE, CLTRELN, RELTO, SELFIND, REVCFLG, ENDDATE, SEQUENCE, PAYMMETH, BANKKEY, BANKACCKEY FROM BNFYPF WHERE VALIDFLAG = '1' AND CHDRCOY=? ");
        sb.append("AND CHDRNUM =? ORDER BY CHDRCOY ASC, CHDRNUM ASC, BNYTYPE ASC, BNYCLT ASC, UNIQUE_NUMBER ASC");
		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		List<Bnfypf> outputList = new ArrayList<Bnfypf>();

		try {

			ps.setString(1, coy);
			ps.setString(2, chdrNum);

			rs = ps.executeQuery();

			while (rs.next()) {

				Bnfypf bnfypf = new Bnfypf();

				bnfypf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
				bnfypf.setChdrcoy(getCharFromString(rs,"CHDRCOY"));
				bnfypf.setChdrnum(rs.getString("CHDRNUM"));
				bnfypf.setBnyclt(rs.getString("BNYCLT"));
				bnfypf.setTranno(rs.getInt("TRANNO"));
				bnfypf.setValidflag(getCharFromString(rs, "VALIDFLAG"));
				bnfypf.setBnyrln(rs.getString("BNYRLN"));
				bnfypf.setBnycd(getCharFromString(rs,"BNYCD"));
				bnfypf.setBnypc(rs.getBigDecimal("BNYPC"));				
				bnfypf.setCurrto(rs.getInt("CURRTO"));
				bnfypf.setCurrfr(rs.getInt("CURRFR"));
				bnfypf.setTermid(rs.getString("TERMID"));
				bnfypf.setUserT(rs.getInt("USER_T"));
				bnfypf.setTrtm(rs.getInt("TRTM"));
				bnfypf.setTrdt(rs.getInt("TRDT"));
				bnfypf.setEffdate(rs.getInt("EFFDATE"));
				bnfypf.setUsrprf(rs.getString("USRPRF"));
				bnfypf.setJobnm(rs.getString("JOBNM"));
				bnfypf.setDatime(rs.getDate("DATIME"));
				bnfypf.setBnytype(rs.getString("BNYTYPE"));
				bnfypf.setCltreln(rs.getString("CLTRELN"));
				bnfypf.setRelto(getCharFromString(rs,"RELTO"));
				bnfypf.setSelfind(getCharFromString(rs,"SELFIND"));
				bnfypf.setRevcflg(getCharFromString(rs,"REVCFLG"));
                bnfypf.setEnddate(rs.getInt("ENDDATE"));
                bnfypf.setSequence(rs.getString("SEQUENCE"));
                bnfypf.setPaymmeth(rs.getString("PAYMMETH"));
                bnfypf.setBankkey(rs.getString("BANKKEY"));
                bnfypf.setBankacckey(rs.getString("BANKACCKEY"));
				outputList.add(bnfypf);
			}

		} catch (SQLException e) {
				LOGGER.error("getBnfymnaByCoyAndNum()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return outputList;
	}
	

	
	public List<Bnfypf> getBnfymnaByKeyAndTranno(Bnfypf bnfypf) throws SQLRuntimeException{

		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, BNYCLT, TRANNO, VALIDFLAG, BNYRLN, BNYCD, BNYPC, CURRTO, CURRFR, TERMID, USER_T, ");
        sb.append("TRTM, TRDT, EFFDATE, USRPRF, JOBNM, DATIME, BNYTYPE, CLTRELN, RELTO, SELFIND, REVCFLG, ENDDATE, SEQUENCE, PAYMMETH, BANKKEY, BANKACCKEY FROM BNFYPF WHERE VALIDFLAG = '1' AND CHDRCOY=? ");
        sb.append("AND CHDRNUM =? AND BNYCLT = ? AND BNYTYPE = ? AND TRANNO != ?  ORDER BY CHDRCOY ASC, CHDRNUM ASC, BNYTYPE ASC, BNYCLT ASC, UNIQUE_NUMBER ASC");
		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		Bnfypf result = null;
        List<Bnfypf> list = new ArrayList<Bnfypf>();
		try {

			ps.setString(1, bnfypf.getChdrcoy().toString());
			ps.setString(2, bnfypf.getChdrnum());
			ps.setString(3, bnfypf.getBnyclt());
			ps.setString(4, bnfypf.getBnytype());
			ps.setInt(5, bnfypf.getTranno());

			rs = ps.executeQuery();

			if (rs.next()) {

				result = new Bnfypf();

				result.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
				result.setChdrcoy(getCharFromString(rs,"CHDRCOY"));
				result.setChdrnum(rs.getString("CHDRNUM"));
				result.setBnyclt(rs.getString("BNYCLT"));
				result.setTranno(rs.getInt("TRANNO"));
				result.setValidflag(getCharFromString(rs, "VALIDFLAG"));
				result.setBnyrln(rs.getString("BNYRLN"));
				result.setBnycd(getCharFromString(rs,"BNYCD"));
				result.setBnypc(rs.getBigDecimal("BNYPC"));				
				result.setCurrto(rs.getInt("CURRTO"));
				result.setCurrfr(rs.getInt("CURRFR"));
				result.setTermid(rs.getString("TERMID"));
				result.setUserT(rs.getInt("USER_T"));
				result.setTrtm(rs.getInt("TRTM"));
				result.setTrdt(rs.getInt("TRDT"));
				result.setEffdate(rs.getInt("EFFDATE"));
				result.setUsrprf(rs.getString("USRPRF"));
				result.setJobnm(rs.getString("JOBNM"));
				result.setDatime(rs.getDate("DATIME"));
				result.setBnytype(rs.getString("BNYTYPE"));
				result.setCltreln(rs.getString("CLTRELN"));
				result.setRelto(getCharFromString(rs,"RELTO"));
				result.setSelfind(getCharFromString(rs,"SELFIND"));
				result.setRevcflg(getCharFromString(rs,"REVCFLG"));
				result.setEnddate(rs.getInt("ENDDATE"));
				result.setSequence(rs.getString("SEQUENCE"));
				result.setPaymmeth(rs.getString("PAYMMETH"));
				result.setBankkey(rs.getString("BANKKEY"));/*ICIL-11*/
				result.setBankacckey(rs.getString("BANKACCKEY"));
				list.add(result);
			}

		} catch (SQLException e) {
				LOGGER.error("getBnfymnaByKeyAndTranno()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return list;
	}
	
	public int updateBnfymnaByUnqueNum(Bnfypf bnfypf) throws SQLRuntimeException{

		StringBuilder sb = new StringBuilder("UPDATE BNFYPF SET VALIDFLAG=?,CURRTO=? WHERE UNIQUE_NUMBER = ? ");
		
		PreparedStatement ps = getPrepareStatement(sb.toString());

		int result = 0;
		
		try {
			ps.setString(1, bnfypf.getValidflag().toString());
			ps.setInt(2, bnfypf.getCurrto());
			ps.setLong(3,bnfypf.getUniqueNumber());
		

			result = ps.executeUpdate();


		} catch (SQLException e) {
				LOGGER.error("updateBnfymnaByKeyAndTranno()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

		return result;
	}
	
	public int insertBnfypf(Bnfypf bnfypf){
		int result = 0;
		StringBuilder sb = new StringBuilder("INSERT INTO BNFYPF(CHDRCOY,CHDRNUM,BNYCLT,TRANNO,VALIDFLAG,CURRFR,CURRTO,BNYRLN,BNYCD,BNYPC,TERMID,USER_T,TRTM,TRDT,EFFDATE,");
		sb.append("USRPRF,JOBNM,DATIME,BNYTYPE,CLTRELN,RELTO,SELFIND,REVCFLG,ENDDATE, SEQUENCE, PAYMMETH, BANKKEY, BANKACCKEY) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		
		try{
			ps.setString(1, bnfypf.getChdrcoy().toString());
			ps.setString(2, bnfypf.getChdrnum());
			ps.setString(3, bnfypf.getBnyclt().trim());
			ps.setInt(4, bnfypf.getTranno());
			ps.setString(5, bnfypf.getValidflag().toString());
			ps.setInt(6, bnfypf.getCurrfr());
			ps.setInt(7, bnfypf.getCurrto());
			ps.setString(8, bnfypf.getBnyrln() == null ? "": bnfypf.getBnyrln());
			ps.setString(9, bnfypf.getBnycd().toString());
			ps.setBigDecimal(10, bnfypf.getBnypc());
			ps.setString(11, bnfypf.getTermid());
			ps.setInt(12, bnfypf.getUserT());
			ps.setInt(13, bnfypf.getTrtm());
			ps.setInt(14, bnfypf.getTrdt());
			ps.setInt(15, bnfypf.getEffdate());
			ps.setString(16, getUsrprf());
			ps.setString(17, getJobnm());
			ps.setTimestamp(18, getDatime());
			ps.setString(19, bnfypf.getBnytype());
			ps.setString(20, bnfypf.getCltreln());
			ps.setString(21, bnfypf.getRelto().toString());
			ps.setString(22, bnfypf.getSelfind().toString());
			ps.setString(23, bnfypf.getRevcflg().toString());
			ps.setInt(24, bnfypf.getEnddate());
			ps.setString(25, bnfypf.getSequence());
			ps.setString(26, bnfypf.getPaymmeth());
			ps.setString(27, bnfypf.getBankkey());
			ps.setString(28, bnfypf.getBankacckey());
			result = ps.executeUpdate();
			
		}catch (SQLException e) {
			LOGGER.error("insertBnfypf()", e);
			throw new SQLRuntimeException(e);
     	} finally {
		    close(ps, null);
     	}
		return result;
	}

	@Override
	public List<Bnfypf> getBnfymnaByCoyAndNumAndBtype(String coy,
			String chdrNum, String btype) throws SQLRuntimeException {
		StringBuilder sb = new StringBuilder("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, BNYCLT, TRANNO, VALIDFLAG, BNYRLN, BNYCD, BNYPC, CURRTO, CURRFR, TERMID, USER_T, ");
        sb.append("TRTM, TRDT, EFFDATE, USRPRF, JOBNM, DATIME, BNYTYPE, CLTRELN, RELTO, SELFIND, REVCFLG, ENDDATE, SEQUENCE, PAYMMETH, BANKKEY, BANKACCKEY FROM BNFYPF WHERE VALIDFLAG = '1' AND CHDRCOY=? ");
        sb.append("AND CHDRNUM =? AND BNYTYPE=? ORDER BY CHDRCOY ASC, CHDRNUM ASC, BNYTYPE ASC, BNYCLT ASC, UNIQUE_NUMBER ASC");
		
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		List<Bnfypf> outputList = new ArrayList<Bnfypf>();

		try {

			ps.setString(1, coy);
			ps.setString(2, chdrNum);
			ps.setString(3, btype);

			rs = ps.executeQuery();

			while (rs.next()) {

				Bnfypf bnfypf = new Bnfypf();

				bnfypf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
				bnfypf.setChdrcoy(getCharFromString(rs,"CHDRCOY"));
				bnfypf.setChdrnum(rs.getString("CHDRNUM"));
				bnfypf.setBnyclt(rs.getString("BNYCLT"));
				bnfypf.setTranno(rs.getInt("TRANNO"));
				bnfypf.setValidflag(getCharFromString(rs, "VALIDFLAG"));
				bnfypf.setBnyrln(rs.getString("BNYRLN"));
				bnfypf.setBnycd(getCharFromString(rs,"BNYCD"));
				bnfypf.setBnypc(rs.getBigDecimal("BNYPC"));				
				bnfypf.setCurrto(rs.getInt("CURRTO"));
				bnfypf.setCurrfr(rs.getInt("CURRFR"));
				bnfypf.setTermid(rs.getString("TERMID"));
				bnfypf.setUserT(rs.getInt("USER_T"));
				bnfypf.setTrtm(rs.getInt("TRTM"));
				bnfypf.setTrdt(rs.getInt("TRDT"));
				bnfypf.setEffdate(rs.getInt("EFFDATE"));
				bnfypf.setUsrprf(rs.getString("USRPRF"));
				bnfypf.setJobnm(rs.getString("JOBNM"));
				bnfypf.setDatime(rs.getDate("DATIME"));
				bnfypf.setBnytype(rs.getString("BNYTYPE"));
				bnfypf.setCltreln(rs.getString("CLTRELN"));
				bnfypf.setRelto(getCharFromString(rs,"RELTO"));
				bnfypf.setSelfind(getCharFromString(rs,"SELFIND"));
				bnfypf.setRevcflg(getCharFromString(rs,"REVCFLG"));
                bnfypf.setEnddate(rs.getInt("ENDDATE"));
                bnfypf.setSequence(rs.getString("SEQUENCE"));
                bnfypf.setPaymmeth(rs.getString("PAYMMETH"));
                bnfypf.setBankkey(rs.getString("BANKKEY"));
                bnfypf.setBankacckey(rs.getString("BANKACCKEY"));
				outputList.add(bnfypf);
			}

		} catch (SQLException e) {
				LOGGER.error("getBnfymnaByCoyAndNum()", e);
				throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return outputList;
	}
	
}
