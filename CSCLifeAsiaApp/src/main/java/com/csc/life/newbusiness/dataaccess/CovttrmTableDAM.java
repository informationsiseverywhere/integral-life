package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovttrmTableDAM.java
 * Date: Sun, 30 Aug 2009 03:36:06
 * Class transformed from COVTTRM.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class CovttrmTableDAM extends CovtpfTableDAM {

	public CovttrmTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("COVTTRM");
	}
	
	public String getTable() {
		return TABLE;
	}

	
	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", SEQNBR";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "SEQNBR, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "CRTABLE, " +
		            "RCESDTE, " +
		            "PCESDTE, " +
		            "RCESAGE, " +
		            "PCESAGE, " +
		            "RCESTRM, " +
		            "PCESTRM, " +
		            "SUMINS, " +
		            "SINGP, " +
		            "INSTPREM, " +
		            "ZBINSTPREM, " +
		            "ZLINSTPREM, " +
		            "MORTCLS, " +
		            "LIENCD, " +
		            "JLIFE, " +
		            "POLINC, " +
		            "NUMAPP, " +
		            "SEX01, " +
		            "SEX02, " +
		            "ANBCCD01, " +
		            "ANBCCD02, " +
		            "BILLFREQ, " +
		            "BILLCHNL, " +
		            "EFFDATE, " +
		            "PAYRSEQNO, " +
		            "CNTCURR, " +
		            "BCESAGE, " +
		            "BCESTRM, " +
		            "BCESDTE, " +
		            "BAPPMETH, " +
		            "ZDIVOPT, " +
		            "PAYCOY, " +
		            "PAYCLT, " +
		            "PAYMTH, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "PAYCURR, " +
		            "FACTHOUS, " +
		            //added for ILIFE-604
		            "PLNSFX, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "LOADPER, " + //TSD-306
			      	"RATEADJ, " + //TSD-306
			      	"FLTMORT, " + //TSD-306
			      	"PREMADJ, " + //TSD-306
			      	"AGEADJ, " + //TSD-306
			      	"LNKGNO," + //ILIFE-6941
			      	"LNKGSUBREFNO," +//ILIFE-6941
			      	"TPDTYPE," + //ILIFE-7118
			      	"LNKGIND,"+
			      	"ZSTPDUTY01, " +
			      	"RISKPREM," + //ILIFE-7845
			      	"COMMPREM," + //IBPLIFE-5237
			      	"ZCLSTATE," +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "SEQNBR ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "SEQNBR DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               seqnbr,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               crtable,
                               riskCessDate,
                               premCessDate,
                               riskCessAge,
                               premCessAge,
                               riskCessTerm,
                               premCessTerm,
                               sumins,
                               singp,
                               instprem,
                               zbinstprem,
                               zlinstprem,
                               mortcls,
                               liencd,
                               jlife,
                               polinc,
                               numapp,
                               sex01,
                               sex02,
                               anbAtCcd01,
                               anbAtCcd02,
                               billfreq,
                               billchnl,
                               effdate,
                               payrseqno,
                               cntcurr,
                               benCessAge,
                               benCessTerm,
                               benCessDate,
                               bappmeth,
                               zdivopt,
                               paycoy,
                               payclt,
                               paymth,
                               bankkey,
                               bankacckey,
                               paycurr,
                               facthous,
                               //added for ILIFE-604
                               planSuffix,
                               userProfile,
                               jobName,
                               datime,
                               loadper, //TSD-306
                               rateadj, //TSD-306
                               fltmort, //TSD-306
                               premadj, //TSD-306
                               ageadj, //TSD-306
                               lnkgno, //ILIFE-6941
                               lnkgsubrefno,//ILIFE-6941
                               tpdtype, //ILIFE-7118
                               lnkgind,
                               zstpduty01,
                               riskprem, //ILIFE-7845
                               commPrem, //IBPLIFE-5237
                               zclstate,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(47);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getSeqnbr().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, seqnbr);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(seqnbr.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(377);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getCrtable().toInternal()
					+ getRiskCessDate().toInternal()
					+ getPremCessDate().toInternal()
					+ getRiskCessAge().toInternal()
					+ getPremCessAge().toInternal()
					+ getRiskCessTerm().toInternal()
					+ getPremCessTerm().toInternal()
					+ getSumins().toInternal()
					+ getSingp().toInternal()
					+ getInstprem().toInternal()
					+ getZbinstprem().toInternal()
					+ getZlinstprem().toInternal()
					+ getMortcls().toInternal()
					+ getLiencd().toInternal()
					+ getJlife().toInternal()
					+ getPolinc().toInternal()
					+ getNumapp().toInternal()
					+ getSex01().toInternal()
					+ getSex02().toInternal()
					+ getAnbAtCcd01().toInternal()
					+ getAnbAtCcd02().toInternal()
					+ getBillfreq().toInternal()
					+ getBillchnl().toInternal()
					+ getEffdate().toInternal()
					+ getPayrseqno().toInternal()
					+ getCntcurr().toInternal()
					+ getBenCessAge().toInternal()
					+ getBenCessTerm().toInternal()
					+ getBenCessDate().toInternal()
					+ getBappmeth().toInternal()
					+ getZdivopt().toInternal()
					+ getPaycoy().toInternal()
					+ getPayclt().toInternal()
					+ getPaymth().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getPaycurr().toInternal()
					+ getFacthous().toInternal()
					//added for ILIFE-604
					+ getPlanSuffix().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getLoadper().toString() //TSD-306
					+ getRateadj().toString() //TSD-306
					+ getFltmort().toString() //TSD-306
					+ getPremadj().toString()
					+ getAgeadj().toString()//TSD-306
					+ getLnkgno().toInternal()//ILIFE-6941
					+ getLnkgsubrefno().toInternal()
					+ getTpdtype().toInternal()//ILIFE-6941
					+getLnkgind().toInternal()
					+ getZstpduty01().toString()
					+ getRiskprem().toInternal() //ILIFE-7845
					+ getCommPrem().toInternal()  //IBPLIFE-5237
					+ getZclstate().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, riskCessDate);
			what = ExternalData.chop(what, premCessDate);
			what = ExternalData.chop(what, riskCessAge);
			what = ExternalData.chop(what, premCessAge);
			what = ExternalData.chop(what, riskCessTerm);
			what = ExternalData.chop(what, premCessTerm);
			what = ExternalData.chop(what, sumins);
			what = ExternalData.chop(what, singp);
			what = ExternalData.chop(what, instprem);
			what = ExternalData.chop(what, zbinstprem);
			what = ExternalData.chop(what, zlinstprem);
			what = ExternalData.chop(what, mortcls);
			what = ExternalData.chop(what, liencd);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, polinc);
			what = ExternalData.chop(what, numapp);
			what = ExternalData.chop(what, sex01);
			what = ExternalData.chop(what, sex02);
			what = ExternalData.chop(what, anbAtCcd01);
			what = ExternalData.chop(what, anbAtCcd02);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, payrseqno);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, benCessAge);
			what = ExternalData.chop(what, benCessTerm);
			what = ExternalData.chop(what, benCessDate);
			what = ExternalData.chop(what, bappmeth);
			what = ExternalData.chop(what, zdivopt);
			what = ExternalData.chop(what, paycoy);
			what = ExternalData.chop(what, payclt);
			what = ExternalData.chop(what, paymth);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, paycurr);
			what = ExternalData.chop(what, facthous);
			//added for ILIFE-604
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			what = ExternalData.chop(what, loadper); //TSD-306
			what = ExternalData.chop(what, rateadj); //TSD-306
			what = ExternalData.chop(what, fltmort); //TSD-306
			what = ExternalData.chop(what, premadj); //TSD-306
			what = ExternalData.chop(what, ageadj); //TSD-306
			what = ExternalData.chop(what, lnkgno); //ILIFE-6941
			what = ExternalData.chop(what, lnkgsubrefno); //ILIFE-6941
			what = ExternalData.chop(what, tpdtype);//ILIFE-7118
			what = ExternalData.chop(what, lnkgind);
			what = ExternalData.chop(what, zstpduty01);
			what = ExternalData.chop(what, riskprem); //ILIFE-7845
			what = ExternalData.chop(what, commPrem); //IBPLIFE-5237
			what = ExternalData.chop(what, zclstate);
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getSeqnbr() {
		return seqnbr;
	}
	public void setSeqnbr(Object what) {
		setSeqnbr(what, false);
	}
	public void setSeqnbr(Object what, boolean rounded) {
		if (rounded)
			seqnbr.setRounded(what);
		else
			seqnbr.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public PackedDecimalData getRiskCessDate() {
		return riskCessDate;
	}
	public void setRiskCessDate(Object what) {
		setRiskCessDate(what, false);
	}
	public void setRiskCessDate(Object what, boolean rounded) {
		if (rounded)
			riskCessDate.setRounded(what);
		else
			riskCessDate.set(what);
	}	
	public PackedDecimalData getPremCessDate() {
		return premCessDate;
	}
	public void setPremCessDate(Object what) {
		setPremCessDate(what, false);
	}
	public void setPremCessDate(Object what, boolean rounded) {
		if (rounded)
			premCessDate.setRounded(what);
		else
			premCessDate.set(what);
	}	
	public PackedDecimalData getRiskCessAge() {
		return riskCessAge;
	}
	public void setRiskCessAge(Object what) {
		setRiskCessAge(what, false);
	}
	public void setRiskCessAge(Object what, boolean rounded) {
		if (rounded)
			riskCessAge.setRounded(what);
		else
			riskCessAge.set(what);
	}	
	public PackedDecimalData getPremCessAge() {
		return premCessAge;
	}
	public void setPremCessAge(Object what) {
		setPremCessAge(what, false);
	}
	public void setPremCessAge(Object what, boolean rounded) {
		if (rounded)
			premCessAge.setRounded(what);
		else
			premCessAge.set(what);
	}	
	public PackedDecimalData getRiskCessTerm() {
		return riskCessTerm;
	}
	public void setRiskCessTerm(Object what) {
		setRiskCessTerm(what, false);
	}
	public void setRiskCessTerm(Object what, boolean rounded) {
		if (rounded)
			riskCessTerm.setRounded(what);
		else
			riskCessTerm.set(what);
	}	
	public PackedDecimalData getPremCessTerm() {
		return premCessTerm;
	}
	public void setPremCessTerm(Object what) {
		setPremCessTerm(what, false);
	}
	public void setPremCessTerm(Object what, boolean rounded) {
		if (rounded)
			premCessTerm.setRounded(what);
		else
			premCessTerm.set(what);
	}	
	public PackedDecimalData getSumins() {
		return sumins;
	}
	public void setSumins(Object what) {
		setSumins(what, false);
	}
	public void setSumins(Object what, boolean rounded) {
		if (rounded)
			sumins.setRounded(what);
		else
			sumins.set(what);
	}	
	public PackedDecimalData getSingp() {
		return singp;
	}
	public void setSingp(Object what) {
		setSingp(what, false);
	}
	public void setSingp(Object what, boolean rounded) {
		if (rounded)
			singp.setRounded(what);
		else
			singp.set(what);
	}	
	public PackedDecimalData getInstprem() {
		return instprem;
	}
	public void setInstprem(Object what) {
		setInstprem(what, false);
	}
	public void setInstprem(Object what, boolean rounded) {
		if (rounded)
			instprem.setRounded(what);
		else
			instprem.set(what);
	}	
	public PackedDecimalData getZbinstprem() {
		return zbinstprem;
	}
	public void setZbinstprem(Object what) {
		setZbinstprem(what, false);
	}
	public void setZbinstprem(Object what, boolean rounded) {
		if (rounded)
			zbinstprem.setRounded(what);
		else
			zbinstprem.set(what);
	}	
	public PackedDecimalData getZlinstprem() {
		return zlinstprem;
	}
	public void setZlinstprem(Object what) {
		setZlinstprem(what, false);
	}
	public void setZlinstprem(Object what, boolean rounded) {
		if (rounded)
			zlinstprem.setRounded(what);
		else
			zlinstprem.set(what);
	}	
	public FixedLengthStringData getMortcls() {
		return mortcls;
	}
	public void setMortcls(Object what) {
		mortcls.set(what);
	}	
	public FixedLengthStringData getLiencd() {
		return liencd;
	}
	public void setLiencd(Object what) {
		liencd.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public PackedDecimalData getPolinc() {
		return polinc;
	}
	public void setPolinc(Object what) {
		setPolinc(what, false);
	}
	public void setPolinc(Object what, boolean rounded) {
		if (rounded)
			polinc.setRounded(what);
		else
			polinc.set(what);
	}	
	public PackedDecimalData getNumapp() {
		return numapp;
	}
	public void setNumapp(Object what) {
		setNumapp(what, false);
	}
	public void setNumapp(Object what, boolean rounded) {
		if (rounded)
			numapp.setRounded(what);
		else
			numapp.set(what);
	}	
	public FixedLengthStringData getSex01() {
		return sex01;
	}
	public void setSex01(Object what) {
		sex01.set(what);
	}	
	public FixedLengthStringData getSex02() {
		return sex02;
	}
	public void setSex02(Object what) {
		sex02.set(what);
	}	
	public PackedDecimalData getAnbAtCcd01() {
		return anbAtCcd01;
	}
	public void setAnbAtCcd01(Object what) {
		setAnbAtCcd01(what, false);
	}
	public void setAnbAtCcd01(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd01.setRounded(what);
		else
			anbAtCcd01.set(what);
	}	
	public PackedDecimalData getAnbAtCcd02() {
		return anbAtCcd02;
	}
	public void setAnbAtCcd02(Object what) {
		setAnbAtCcd02(what, false);
	}
	public void setAnbAtCcd02(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd02.setRounded(what);
		else
			anbAtCcd02.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(Object what) {
		setPayrseqno(what, false);
	}
	public void setPayrseqno(Object what, boolean rounded) {
		if (rounded)
			payrseqno.setRounded(what);
		else
			payrseqno.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public PackedDecimalData getBenCessAge() {
		return benCessAge;
	}
	public void setBenCessAge(Object what) {
		setBenCessAge(what, false);
	}
	public void setBenCessAge(Object what, boolean rounded) {
		if (rounded)
			benCessAge.setRounded(what);
		else
			benCessAge.set(what);
	}	
	public PackedDecimalData getBenCessTerm() {
		return benCessTerm;
	}
	public void setBenCessTerm(Object what) {
		setBenCessTerm(what, false);
	}
	public void setBenCessTerm(Object what, boolean rounded) {
		if (rounded)
			benCessTerm.setRounded(what);
		else
			benCessTerm.set(what);
	}	
	public PackedDecimalData getBenCessDate() {
		return benCessDate;
	}
	public void setBenCessDate(Object what) {
		setBenCessDate(what, false);
	}
	public void setBenCessDate(Object what, boolean rounded) {
		if (rounded)
			benCessDate.setRounded(what);
		else
			benCessDate.set(what);
	}	
	public FixedLengthStringData getBappmeth() {
		return bappmeth;
	}
	public void setBappmeth(Object what) {
		bappmeth.set(what);
	}	
	public FixedLengthStringData getZdivopt() {
		return zdivopt;
	}
	public void setZdivopt(Object what) {
		zdivopt.set(what);
	}	
	public FixedLengthStringData getPaycoy() {
		return paycoy;
	}
	public void setPaycoy(Object what) {
		paycoy.set(what);
	}	
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	public void setPayclt(Object what) {
		payclt.set(what);
	}	
	public FixedLengthStringData getPaymth() {
		return paymth;
	}
	public void setPaymth(Object what) {
		paymth.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getPaycurr() {
		return paycurr;
	}
	public void setPaycurr(Object what) {
		paycurr.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//added for ILIFE-604
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	//TSD-306 Start
	public PackedDecimalData getLoadper() {
		return loadper;
	}
	public void setLoadper (Object loadper) {
		this.loadper.set(loadper);
	}
	public PackedDecimalData getRateadj() {
		return rateadj;
	}
	public void setRateadj (Object rateadj) {
		this.rateadj.set(rateadj);
	}
	public PackedDecimalData getFltmort() {
		return fltmort;
	}
	public void setFltmort (Object fltmort) {
		this.fltmort.set(fltmort);
	}
	public PackedDecimalData getPremadj() {
		return premadj;
	}
	public void setPremadj (Object premadj) {
		this.premadj.set(premadj);
	}
	public PackedDecimalData getAgeadj() {
		return ageadj;
	}
	public void setAgeadj (Object ageadj) {
		this.ageadj.set(ageadj);
	}
	//TSD-306 End
	
	/*ILIFE-6941 - Start*/
	public FixedLengthStringData getLnkgsubrefno() {
		return lnkgsubrefno;
	}
	public void setLnkgsubrefno(Object what) {
		lnkgsubrefno.set(what);
	}	
	public FixedLengthStringData getLnkgno() {
		return lnkgno;
	}
	public void setLnkgno(Object what) {
		lnkgno.set(what);
	}	
	/*ILIFE-6941 - End*/
	
	/*ILIFE-7118-starts*/
	public FixedLengthStringData getTpdtype() {
		return tpdtype;
	}
	public void setTpdtype(Object what) {
		tpdtype.set(what);
	}
	/*ILIFE-7118-ends*/
	//ILIFE-7845
	public PackedDecimalData getRiskprem() {
		return riskprem;
	}

	public void setRiskprem(Object riskprem) {
		this.riskprem.set(riskprem);
	}
	
	
	/* IBPLIFE -5237 starts */
	public PackedDecimalData getCommPrem() {
		return commPrem;
	}

	public void setCommPrem(Object commPrem) {
		this.commPrem.set(commPrem);
	}
	
/* IBPLIFE -5237 ends */
	
	
		//ILIFE-7845
	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSexs() {
		return new FixedLengthStringData(sex01.toInternal()
										+ sex02.toInternal());
	}
	public void setSexs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSexs().getLength()).init(obj);
	
		what = ExternalData.chop(what, sex01);
		what = ExternalData.chop(what, sex02);
	}
	public FixedLengthStringData getSex(BaseData indx) {
		return getSex(indx.toInt());
	}
	public FixedLengthStringData getSex(int indx) {

		switch (indx) {
			case 1 : return sex01;
			case 2 : return sex02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSex(BaseData indx, Object what) {
		setSex(indx.toInt(), what);
	}
	public void setSex(int indx, Object what) {

		switch (indx) {
			case 1 : setSex01(what);
					 break;
			case 2 : setSex02(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getAnbccds() {
		return new FixedLengthStringData(anbAtCcd01.toInternal()
										+ anbAtCcd02.toInternal());
	}
	public void setAnbccds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getAnbccds().getLength()).init(obj);
	
		what = ExternalData.chop(what, anbAtCcd01);
		what = ExternalData.chop(what, anbAtCcd02);
	}
	public PackedDecimalData getAnbccd(BaseData indx) {
		return getAnbccd(indx.toInt());
	}
	public PackedDecimalData getAnbccd(int indx) {

		switch (indx) {
			case 1 : return anbAtCcd01;
			case 2 : return anbAtCcd02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setAnbccd(BaseData indx, Object what) {
		setAnbccd(indx, what, false);
	}
	public void setAnbccd(BaseData indx, Object what, boolean rounded) {
		setAnbccd(indx.toInt(), what, rounded);
	}
	public void setAnbccd(int indx, Object what) {
		setAnbccd(indx, what, false);
	}
	public void setAnbccd(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAnbAtCcd01(what, rounded);
					 break;
			case 2 : setAnbAtCcd02(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		seqnbr.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		crtable.clear();
		riskCessDate.clear();
		premCessDate.clear();
		riskCessAge.clear();
		premCessAge.clear();
		riskCessTerm.clear();
		premCessTerm.clear();
		sumins.clear();
		singp.clear();
		instprem.clear();
		zbinstprem.clear();
		zlinstprem.clear();
		mortcls.clear();
		liencd.clear();
		jlife.clear();
		polinc.clear();
		numapp.clear();
		sex01.clear();
		sex02.clear();
		anbAtCcd01.clear();
		anbAtCcd02.clear();
		billfreq.clear();
		billchnl.clear();
		effdate.clear();
		payrseqno.clear();
		cntcurr.clear();
		benCessAge.clear();
		benCessTerm.clear();
		benCessDate.clear();
		bappmeth.clear();
		zdivopt.clear();
		paycoy.clear();
		payclt.clear();
		paymth.clear();
		bankkey.clear();
		bankacckey.clear();
		paycurr.clear();
		facthous.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();	
		//added for ILIFE-604
		planSuffix.clear();
		//TSD-306 Start
		loadper.clear();
		rateadj.clear();
		fltmort.clear();
		premadj.clear();
		ageadj.clear();
		//TSD-306 End
		lnkgind.clear();
		zstpduty01.clear();
		riskprem.clear(); //ILIFE-7845
		zclstate.clear();
	}

	public PackedDecimalData getZstpduty01() {
		return zstpduty01;
	}
	public void setZstpduty01 (Object zstpduty01) {
		this.zstpduty01.set(zstpduty01);
	}
	public FixedLengthStringData getLnkgind() {
		return lnkgind;
	}
	public void setLnkgind(Object what) {
		lnkgind.set(what);
	}
	
	public FixedLengthStringData getZclstate() {
		return zclstate;
	}
	public void setZclstate(Object what) {
		zclstate.set(what);
	}
	
}