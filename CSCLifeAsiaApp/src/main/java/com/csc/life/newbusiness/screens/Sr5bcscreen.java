package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr5bcscreen extends ScreenRecord{
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {0, 0, 0, 0}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr5bcScreenVars sv = (Sr5bcScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr5bcscreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr5bcScreenVars screenVars = ( Sr5bcScreenVars )pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.chdrcoy.setClassString("");
		screenVars.startdteDisp.setClassString("");
		screenVars.startdte.setClassString("");
		screenVars.mbrAccNum.setClassString("");
		screenVars.USIFndNum.setClassString("");
		screenVars.taxFreeCompAmt.setClassString("");
		screenVars.kiwiCompAmt.setClassString("");
		screenVars.taxAmt.setClassString("");
		screenVars.nonTaxAmt.setClassString("");
		screenVars.presAmt.setClassString("");
		screenVars.kiwiPresAmt.setClassString("");
		screenVars.unRestrictAmt.setClassString("");
		screenVars.restrictAmt.setClassString("");
		screenVars.totTranAmt.setClassString("");
		screenVars.moneyInd.setClassString("");		
	}
	
	/**
	 * Clear all the variables in Sr5bcscreen
	 */
	public static void clear(VarModel pv) {
		Sr5bcScreenVars screenVars = (Sr5bcScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.chdrcoy.clear();
		screenVars.startdte.clear();
		screenVars.startdteDisp.clear();
		screenVars.mbrAccNum.clear();
		screenVars.USIFndNum.clear();
		screenVars.taxFreeCompAmt.clear();
		screenVars.kiwiCompAmt.clear();
		screenVars.taxAmt.clear();
		screenVars.nonTaxAmt.clear();
		screenVars.presAmt.clear();
		screenVars.kiwiPresAmt.clear();
		screenVars.unRestrictAmt.clear();
		screenVars.restrictAmt.clear();
		screenVars.totTranAmt.clear();
		screenVars.moneyInd.clear();
			
	}

}
