package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:40
 * Description:
 * Copybook name: FLUPUNDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Flupundkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData flupundFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData flupundKey = new FixedLengthStringData(256).isAPartOf(flupundFileKey, 0, REDEFINE);
  	public FixedLengthStringData flupundChdrcoy = new FixedLengthStringData(1).isAPartOf(flupundKey, 0);
  	public FixedLengthStringData flupundChdrnum = new FixedLengthStringData(8).isAPartOf(flupundKey, 1);
  	public FixedLengthStringData flupundLife = new FixedLengthStringData(2).isAPartOf(flupundKey, 9);
  	public FixedLengthStringData flupundJlife = new FixedLengthStringData(2).isAPartOf(flupundKey, 11);
  	public FixedLengthStringData flupundFupcode = new FixedLengthStringData(3).isAPartOf(flupundKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(flupundKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(flupundFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		flupundFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}