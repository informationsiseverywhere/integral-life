package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:24
 * Description:
 * Copybook name: RACTAFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ractafikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ractafiFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ractafiKey = new FixedLengthStringData(64).isAPartOf(ractafiFileKey, 0, REDEFINE);
  	public FixedLengthStringData ractafiChdrcoy = new FixedLengthStringData(1).isAPartOf(ractafiKey, 0);
  	public FixedLengthStringData ractafiChdrnum = new FixedLengthStringData(8).isAPartOf(ractafiKey, 1);
  	public FixedLengthStringData ractafiLife = new FixedLengthStringData(2).isAPartOf(ractafiKey, 9);
  	public FixedLengthStringData ractafiCoverage = new FixedLengthStringData(2).isAPartOf(ractafiKey, 11);
  	public FixedLengthStringData ractafiRider = new FixedLengthStringData(2).isAPartOf(ractafiKey, 13);
  	public FixedLengthStringData ractafiRasnum = new FixedLengthStringData(8).isAPartOf(ractafiKey, 15);
  	public FixedLengthStringData ractafiRatype = new FixedLengthStringData(4).isAPartOf(ractafiKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(37).isAPartOf(ractafiKey, 27, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ractafiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ractafiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}