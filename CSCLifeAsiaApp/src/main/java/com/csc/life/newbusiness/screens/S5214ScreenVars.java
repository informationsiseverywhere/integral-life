package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5214
 * @version 1.0 generated on 30/08/09 06:37
 * @author Quipoz
 */
public class S5214ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(dataFields,47);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,55);
	public ZonedDecimalData btdate = DD.btdate.copyToZonedDecimal().isAPartOf(dataFields,57);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,73);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData ctypdesc = DD.ctypdesc.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,114);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,144);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,152);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,153);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,161);
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields,208);
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields,255);
	public FixedLengthStringData pstate = DD.pstate.copy().isAPartOf(dataFields,263);
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields,273);
	public FixedLengthStringData rstate = DD.rstate.copy().isAPartOf(dataFields,281);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData btdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData ctypdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData pstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData rstateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] btdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] ctypdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] pstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] rstateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData btdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);

	public LongData S5214screenWritten = new LongData(0);
	public LongData S5214protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5214ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(cnttypeOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5214screen.class;
		protectRecord = S5214protect.class;
	}
	public BaseData[] getscreenDateFields()
	{
		return new BaseData[] {occdate,btdate ,ptdate, effdate };
	}
	public BaseData[] getscreenDateDispFields()
	{
		return new BaseData[] {occdateDisp, btdateDisp, ptdateDisp, effdateDisp};//SGS001
	}
	public BaseData[] getscreenDateErrFields()
	{
		return new BaseData[] {occdateErr, btdateErr, ptdateErr, effdateErr};
	}
	
	
	public int getDataAreaSize() {
		return 579;
	}
	public int getDataFieldsSize(){
		return 291;
	}
	public int getErrorIndicatorSize(){
		return 72 ; 
	}
	public int getOutputFieldSize(){
		return 216; 
	}
	
	public BaseData[] getscreenFields(){
		return new BaseData[] {descrip, chdrnum, rstate, cnttype, cownnum, payrnum, agntnum, billfreq, ctypdesc, pstate, ownername, payorname, agentname, occdate, mop, btdate, ptdate, effdate};
	}
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][]   {descripOut, chdrnumOut, rstateOut, cnttypeOut, cownnumOut, payrnumOut, agntnumOut, billfreqOut, ctypdescOut, pstateOut, ownernameOut, payornameOut, agentnameOut, occdateOut, mopOut, btdateOut, ptdateOut, effdateOut};
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[]  {descripErr, chdrnumErr, rstateErr, cnttypeErr, cownnumErr, payrnumErr, agntnumErr, billfreqErr, ctypdescErr, pstateErr, ownernameErr, payornameErr, agentnameErr, occdateErr, mopErr, btdateErr, ptdateErr, effdateErr};
		
	}
}
