package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:39
 * Description:
 * Copybook name: FLUPALTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Flupaltkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData flupaltFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData flupaltKey = new FixedLengthStringData(256).isAPartOf(flupaltFileKey, 0, REDEFINE);
  	public FixedLengthStringData flupaltChdrcoy = new FixedLengthStringData(1).isAPartOf(flupaltKey, 0);
  	public FixedLengthStringData flupaltChdrnum = new FixedLengthStringData(8).isAPartOf(flupaltKey, 1);
  	public PackedDecimalData flupaltFupno = new PackedDecimalData(2, 0).isAPartOf(flupaltKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(245).isAPartOf(flupaltKey, 11, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(flupaltFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		flupaltFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}