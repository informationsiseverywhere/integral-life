/*
 * File: Cnvt5565.java
 * Date: December 3, 2013 2:17:13 AM ICT
 * Author: CSC
 *
 * Class transformed from CNVT5565.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.agents.tablestructures.T5565rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for T5565, field has changed from FUPCODE t
*   FUPCDES.
*
****************************************************************** ****
* </pre>
*/
public class Cnvt5565 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT5565");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t5565 = "T5565";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5565rec t5565rec = new T5565rec();
	private Varcom varcom = new Varcom();
	private T5565OldRecInner t5565OldRecInner = new T5565OldRecInner();

	public Cnvt5565() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(t5565);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T5565 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), t5565)) {
			getAppVars().addDiagnostic("Table T5565 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}

		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T5565 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT55652080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		t5565OldRecInner.t5565OldRec.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			t5565rec.anbAtCcd[ix.toInt()].set(t5565OldRecInner.t5565OldAnbAtCcd[ix.toInt()]);
		}
		for (ix.set(1); !(isGT(ix, 9)); ix.add(1)){
			t5565rec.termMax[ix.toInt()].set(t5565OldRecInner.t5565OldTermMax[ix.toInt()]);
		}
		for (ix.set(1); !(isGT(ix, 90)); ix.add(1)){
			t5565rec.prcnt[ix.toInt()].set(t5565OldRecInner.t5565OldPrcnt[ix.toInt()]);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(t5565rec.t5565Rec);
	}

protected void rewriteT55652080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), t5565)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T5565");
		}
	}
/*
 * Class transformed  from Data Structure T5565-OLD-REC--INNER
 */
private static final class T5565OldRecInner {

	private FixedLengthStringData t5565OldRec = new FixedLengthStringData(500);
	private FixedLengthStringData t5565OldAnbAtCcds = new FixedLengthStringData(30).isAPartOf(t5565OldRec, 0);
	private ZonedDecimalData[] t5565OldAnbAtCcd = ZDArrayPartOfStructure(10, 3, 0, t5565OldAnbAtCcds, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(t5565OldAnbAtCcds, 0, FILLER_REDEFINE);
	private ZonedDecimalData t55650ldAnbAtCcd01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
	private ZonedDecimalData t55650ldAnbAtCcd02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
	private ZonedDecimalData t55650ldAnbAtCcd03 = new ZonedDecimalData(3, 0).isAPartOf(filler, 6);
	private ZonedDecimalData t55650ldAnbAtCcd04 = new ZonedDecimalData(3, 0).isAPartOf(filler, 9);
	private ZonedDecimalData t55650ldAnbAtCcd05 = new ZonedDecimalData(3, 0).isAPartOf(filler, 12);
	private ZonedDecimalData t55650ldAnbAtCcd06 = new ZonedDecimalData(3, 0).isAPartOf(filler, 15);
	private ZonedDecimalData t55650ldAnbAtCcd07 = new ZonedDecimalData(3, 0).isAPartOf(filler, 18);
	private ZonedDecimalData t55650ldAnbAtCcd08 = new ZonedDecimalData(3, 0).isAPartOf(filler, 21);
	private ZonedDecimalData t55650ldAnbAtCcd09 = new ZonedDecimalData(3, 0).isAPartOf(filler, 24);
	private ZonedDecimalData t55650ldAnbAtCcd10 = new ZonedDecimalData(3, 0).isAPartOf(filler, 27);
	private FixedLengthStringData t5565OldPrcnts = new FixedLengthStringData(450).isAPartOf(t5565OldRec, 30);
	private ZonedDecimalData[] t5565OldPrcnt = ZDArrayPartOfStructure(90, 5, 2, t5565OldPrcnts, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(450).isAPartOf(t5565OldPrcnts, 0, FILLER_REDEFINE);
	private ZonedDecimalData t5565OldPrcnt01 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 0);
	private ZonedDecimalData t5565OldPrcnt02 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 5);
	private ZonedDecimalData t5565OldPrcnt03 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 10);
	private ZonedDecimalData t5565OldPrcnt04 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 15);
	private ZonedDecimalData t5565OldPrcnt05 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 20);
	private ZonedDecimalData t5565OldPrcnt06 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 25);
	private ZonedDecimalData t5565OldPrcnt07 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 30);
	private ZonedDecimalData t5565OldPrcnt08 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 35);
	private ZonedDecimalData t5565OldPrcnt09 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 40);
	private ZonedDecimalData t5565OldPrcnt10 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 45);
	private ZonedDecimalData t5565OldPrcnt11 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 50);
	private ZonedDecimalData t5565OldPrcnt12 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 55);
	private ZonedDecimalData t5565OldPrcnt13 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 60);
	private ZonedDecimalData t5565OldPrcnt14 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 65);
	private ZonedDecimalData t5565OldPrcnt15 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 70);
	private ZonedDecimalData t5565OldPrcnt16 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 75);
	private ZonedDecimalData t5565OldPrcnt17 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 80);
	private ZonedDecimalData t5565OldPrcnt18 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 85);
	private ZonedDecimalData t5565OldPrcnt19 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 90);
	private ZonedDecimalData t5565OldPrcnt20 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 95);
	private ZonedDecimalData t5565OldPrcnt21 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 100);
	private ZonedDecimalData t5565OldPrcnt22 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 105);
	private ZonedDecimalData t5565OldPrcnt23 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 110);
	private ZonedDecimalData t5565OldPrcnt24 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 115);
	private ZonedDecimalData t5565OldPrcnt25 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 120);
	private ZonedDecimalData t5565OldPrcnt26 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 125);
	private ZonedDecimalData t5565OldPrcnt27 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 130);
	private ZonedDecimalData t5565OldPrcnt28 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 135);
	private ZonedDecimalData t5565OldPrcnt29 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 140);
	private ZonedDecimalData t5565OldPrcnt30 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 145);
	private ZonedDecimalData t5565OldPrcnt31 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 150);
	private ZonedDecimalData t5565OldPrcnt32 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 155);
	private ZonedDecimalData t5565OldPrcnt33 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 160);
	private ZonedDecimalData t5565OldPrcnt34 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 165);
	private ZonedDecimalData t5565OldPrcnt35 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 170);
	private ZonedDecimalData t5565OldPrcnt36 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 175);
	private ZonedDecimalData t5565OldPrcnt37 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 180);
	private ZonedDecimalData t5565OldPrcnt38 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 185);
	private ZonedDecimalData t5565OldPrcnt39 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 190);
	private ZonedDecimalData t5565OldPrcnt40 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 195);
	private ZonedDecimalData t5565OldPrcnt41 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 200);
	private ZonedDecimalData t5565OldPrcnt42 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 205);
	private ZonedDecimalData t5565OldPrcnt43 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 210);
	private ZonedDecimalData t5565OldPrcnt44 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 215);
	private ZonedDecimalData t5565OldPrcnt45 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 220);
	private ZonedDecimalData t5565OldPrcnt46 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 225);
	private ZonedDecimalData t5565OldPrcnt47 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 230);
	private ZonedDecimalData t5565OldPrcnt48 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 235);
	private ZonedDecimalData t5565OldPrcnt49 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 240);
	private ZonedDecimalData t5565OldPrcnt50 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 245);
	private ZonedDecimalData t5565OldPrcnt51 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 250);
	private ZonedDecimalData t5565OldPrcnt52 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 255);
	private ZonedDecimalData t5565OldPrcnt53 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 260);
	private ZonedDecimalData t5565OldPrcnt54 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 265);
	private ZonedDecimalData t5565OldPrcnt55 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 270);
	private ZonedDecimalData t5565OldPrcnt56 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 275);
	private ZonedDecimalData t5565OldPrcnt57 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 280);
	private ZonedDecimalData t5565OldPrcnt58 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 285);
	private ZonedDecimalData t5565OldPrcnt59 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 290);
	private ZonedDecimalData t5565OldPrcnt60 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 295);
	private ZonedDecimalData t5565OldPrcnt61 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 300);
	private ZonedDecimalData t5565OldPrcnt62 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 305);
	private ZonedDecimalData t5565OldPrcnt63 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 310);
	private ZonedDecimalData t5565OldPrcnt64 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 315);
	private ZonedDecimalData t5565OldPrcnt65 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 320);
	private ZonedDecimalData t5565OldPrcnt66 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 325);
	private ZonedDecimalData t5565OldPrcnt67 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 330);
	private ZonedDecimalData t5565OldPrcnt68 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 335);
	private ZonedDecimalData t5565OldPrcnt69 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 340);
	private ZonedDecimalData t5565OldPrcnt70 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 345);
	private ZonedDecimalData t5565OldPrcnt71 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 350);
	private ZonedDecimalData t5565OldPrcnt72 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 355);
	private ZonedDecimalData t5565OldPrcnt73 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 360);
	private ZonedDecimalData t5565OldPrcnt74 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 365);
	private ZonedDecimalData t5565OldPrcnt75 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 370);
	private ZonedDecimalData t5565OldPrcnt76 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 375);
	private ZonedDecimalData t5565OldPrcnt77 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 380);
	private ZonedDecimalData t5565OldPrcnt78 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 385);
	private ZonedDecimalData t5565OldPrcnt79 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 390);
	private ZonedDecimalData t5565OldPrcnt80 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 395);
	private ZonedDecimalData t5565OldPrcnt81 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 400);
	private ZonedDecimalData t5565OldPrcnt82 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 405);
	private ZonedDecimalData t5565OldPrcnt83 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 410);
	private ZonedDecimalData t5565OldPrcnt84 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 415);
	private ZonedDecimalData t5565OldPrcnt85 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 420);
	private ZonedDecimalData t5565OldPrcnt86 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 425);
	private ZonedDecimalData t5565OldPrcnt87 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 430);
	private ZonedDecimalData t5565OldPrcnt88 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 435);
	private ZonedDecimalData t5565OldPrcnt89 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 440);
	private ZonedDecimalData t5565OldPrcnt90 = new ZonedDecimalData(5, 2).isAPartOf(filler1, 445);
	private FixedLengthStringData t5565OldTermMaxs = new FixedLengthStringData(18).isAPartOf(t5565OldRec, 480);
	private ZonedDecimalData[] t5565OldTermMax = ZDArrayPartOfStructure(9, 2, 0, t5565OldTermMaxs, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(18).isAPartOf(t5565OldTermMaxs, 0, FILLER_REDEFINE);
	private ZonedDecimalData t5565OldTermMax01 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 0);
	private ZonedDecimalData t5565OldTermMax02 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 2);
	private ZonedDecimalData t5565OldTermMax03 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 4);
	private ZonedDecimalData t5565OldTermMax04 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 6);
	private ZonedDecimalData t5565OldTermMax05 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 8);
	private ZonedDecimalData t5565OldTermMax06 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 10);
	private ZonedDecimalData t5565OldTermMax07 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 12);
	private ZonedDecimalData t5565OldTermMax08 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 14);
	private ZonedDecimalData t5565OldTermMax09 = new ZonedDecimalData(2, 0).isAPartOf(filler2, 16);
}
}
