package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:26
 * Description:
 * Copybook name: TR52QREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr52qrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr52qRec = new FixedLengthStringData(500);
  	public FixedLengthStringData daexpys = new FixedLengthStringData(6).isAPartOf(tr52qRec, 0);
  	public ZonedDecimalData[] daexpy = ZDArrayPartOfStructure(2, 3, 0, daexpys, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(6).isAPartOf(daexpys, 0, FILLER_REDEFINE);
  	public ZonedDecimalData daexpy01 = new ZonedDecimalData(3, 0).isAPartOf(filler, 0);
  	public ZonedDecimalData daexpy02 = new ZonedDecimalData(3, 0).isAPartOf(filler, 3);
  	public FixedLengthStringData dlvrmodes = new FixedLengthStringData(36).isAPartOf(tr52qRec, 6);
  	public FixedLengthStringData[] dlvrmode = FLSArrayPartOfStructure(9, 4, dlvrmodes, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(36).isAPartOf(dlvrmodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData dlvrmode01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData dlvrmode02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData dlvrmode03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData dlvrmode04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData dlvrmode05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData dlvrmode06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData dlvrmode07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData dlvrmode08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData dlvrmode09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData letterTypes = new FixedLengthStringData(16).isAPartOf(tr52qRec, 42);
  	public FixedLengthStringData[] letterType = FLSArrayPartOfStructure(2, 8, letterTypes, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(16).isAPartOf(letterTypes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData letterType01 = new FixedLengthStringData(8).isAPartOf(filler2, 0);
  	public FixedLengthStringData letterType02 = new FixedLengthStringData(8).isAPartOf(filler2, 8);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(442).isAPartOf(tr52qRec, 58, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr52qRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr52qRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}