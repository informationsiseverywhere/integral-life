package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD;
import com.csc.life.newbusiness.dataaccess.dao.ResnpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Resnpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ResnpfDAOImpl extends BaseDAOImpl<Resnpf> implements ResnpfDAO{
	private static final Logger LOGGER = LoggerFactory.getLogger(ResnpfDAOImpl.class);
	@Override
	public void insertResnpf(Resnpf resnpf) {
		if (resnpf != null) {
		      
			StringBuilder sql_insert = new StringBuilder();
			sql_insert.append("INSERT INTO Resnpf");
			sql_insert.append("(CHDRNUM,CHDRCOY, REASONCD,RESNDESC,TRANNO,TRANCDE,TERMID,TRDT,TRTM,USER_T) ");
			sql_insert.append("VALUES(?,?,?,?,?,?,?,?,?,?)");
			PreparedStatement psInsert = getPrepareStatement(sql_insert.toString());
			try
			{
					psInsert.setString(1, resnpf.getChdrnum());
					psInsert.setString(2, resnpf.getChdrcoy());
					psInsert.setString(3, resnpf.getReasoncd());
					psInsert.setString(4, resnpf.getResndesc()== null ? " " : resnpf.getResndesc()); //ILB-947
					psInsert.setInt(5, resnpf.getTranno());
					psInsert.setString(6, resnpf.getTrancde());
					psInsert.setString(7, resnpf.getTermid()== null ? " " : resnpf.getTermid()); //ILB-947
					psInsert.setInt(8, resnpf.getTrdt());
					psInsert.setInt(9, resnpf.getTrtm());
					psInsert.setInt(10, resnpf.getUserT());
					psInsert.executeUpdate();	
			}
			catch (SQLException e) {
                LOGGER.error("insertResnpf()",  e);
                throw new SQLRuntimeException(e);
            } finally {
                close(psInsert, null);
            }
		}
		

		
	}

	@Override
	public Resnpf readResnpf(String chdrcoy,String chdrnum,String trancde) {
		Resnpf resnpf = null;
		    String sql_select  = "SELECT UNIQUE_NUMBER,REASONCD,RESNDESC,TRANNO,TRANCDE,TERMID,TRDT,TRTM,USER_T FROM RESNPF "
		    		+ "WHERE CHDRCOY = ? AND CHDRNUM = ? AND TRANCDE=?";
		    
		    ResultSet rs = null;
		    PreparedStatement psSelect=null;
		    try {
		    	psSelect = getPrepareStatement(sql_select);/* IJTI-1523 */
		    	psSelect.setString(1, chdrcoy);
		    	psSelect.setString(2, chdrnum);
		    	psSelect.setString(3, trancde);
		        rs = psSelect.executeQuery();
		           while (rs.next()) {
		        	   resnpf= new Resnpf();
		        	   		 resnpf.setChdrcoy(chdrcoy);
		        	   		 resnpf.setChdrnum(chdrnum);
		        	   		 resnpf.setReasoncd(rs.getString("REASONCD"));
		        	   		 resnpf.setResndesc(rs.getString("RESNDESC"));
		        	   		 resnpf.setTranno(rs.getInt("TRANNO"));
		                	 resnpf.setTrancde(rs.getString("TRANCDE"));
		                	 resnpf.setTermid(rs.getString("TERMID"));
		                	 resnpf.setTrdt(rs.getInt("TRDT"));
		                	 resnpf.setTrtm(rs.getInt("TRTM"));
		                	 resnpf.setUserT(rs.getInt("USER_T"));
		                	 resnpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
		                  }
		           
		    } catch (SQLException e) {
		           LOGGER.error("readResnpf()" , e);
		           throw new SQLRuntimeException(e);
		    } finally {
		           close(psSelect, rs);
		    }
		    return resnpf;
	}

	public boolean deleteResnpf(Resnpf resnpf) {
		StringBuilder sqlDelete = new StringBuilder();
		sqlDelete.append("DELETE FROM Resnpf WHERE UNIQUE_NUMBER = ? ");
		PreparedStatement psResnpfDelete = getPrepareStatement(sqlDelete.toString());
		boolean returnFlag = false;
		Resnpf queryResnpf = this.readResnpf(resnpf.getChdrcoy(), resnpf.getChdrnum(), resnpf.getTrancde());
		if(queryResnpf == null){
			return false;
		}
		try {
			psResnpfDelete.setLong(1, queryResnpf.getUniqueNumber());
			int affectedCount = executeUpdate(psResnpfDelete);
			returnFlag = true;
			LOGGER.debug("deleteResnpf {} rows deleted.", affectedCount);//IJTI-1561

		} catch (SQLException e) {
			returnFlag = false;
			LOGGER.error("Exception occured in deleteResnpf ",e);
		} finally {
			close(psResnpfDelete, null);
		}
		return returnFlag;
	}
	
	public int updateResnpf(Resnpf resnpf) {
		StringBuilder sql = new StringBuilder("UPDATE RESNPF SET REASONCD=?, TRANCDE=?, TRDT=?, TRTM=?, USER_T=? WHERE CHDRCOY=? AND CHDRNUM=? AND TRANNO=?");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		int result = 0;
		
		try {
			ps.setString(1, resnpf.getReasoncd());
			ps.setString(2, resnpf.getTrancde());
			ps.setInt(3, resnpf.getTrdt());
			ps.setInt(4, resnpf.getTrtm());
			ps.setInt(5, resnpf.getUserT());
			ps.setString(6, resnpf.getChdrcoy());
			ps.setString(7, resnpf.getChdrnum());
			ps.setInt(8, resnpf.getTranno());
			
			result = ps.executeUpdate();
		}catch(SQLException e) {
			LOGGER.error("Exception occured in updateResnpf ",e);
		}finally {
			close(ps, null);
		}
       return result;
	}
	public void updtOrInstResnpf(Resnpf resnpf) {
		if(updateResnpf(resnpf)==0) {
			insertResnpf(resnpf);
		}
	}
	
	public Map<String, Resnpf> searchResnpfMap(String coy, String chdrnum) {

		Map<String, Resnpf> resnpfMap = new HashMap<>();
		String sql = "SELECT UNIQUE_NUMBER,TRANNO,TRANCDE,REASONCD,RESNDESC,CHDRCOY,CHDRNUM FROM RESNPF WHERE CHDRCOY = ? AND CHDRNUM = ? ";

		try (PreparedStatement ps = getPrepareStatement(sql)) {
			ps.setString(1, coy);
			ps.setString(2, StringUtil.fillSpace(chdrnum.trim(), DD.chdrnum.length)); 
			ResultSet rs = executeQuery(ps);
			while (rs.next()) {
				Resnpf result = new Resnpf();
				result.setUniqueNumber(rs.getLong("UNIQUE_NUMBER"));
				result.setTranno(rs.getInt("TRANNO"));
				result.setChdrcoy("CHDRCOY");
    	   		result.setChdrnum("CHDRNUM");
    	   		result.setReasoncd(rs.getString("REASONCD"));
    	   		result.setResndesc(rs.getString("RESNDESC"));
    	   		result.setTranno(rs.getInt("TRANNO"));
            	result.setTrancde(rs.getString("TRANCDE"));
				resnpfMap.put(result.getTrancde(), result);
		                  }
		           
		  } catch (SQLException e) {
		           throw new SQLRuntimeException(e);
		  }
		return resnpfMap;
	}
}
