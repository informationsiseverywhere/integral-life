/*
 * File: Tt515pt.java
 * Date: 30 August 2009 2:47:15
 * Author: Quipoz Limited
 * 
 * Class transformed from TT515PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.Tt515rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TT515.
*
*
*****************************************************************
* </pre>
*/
public class Tt515pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(46).isAPartOf(wsaaPrtLine001, 30, FILLER).init("Extended Term Table                      ST515");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(74);
	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 0, FILLER).init("Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 9);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 10, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 20);
	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 25, FILLER).init("  Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 44);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(70);
	private FixedLengthStringData filler7 = new FixedLengthStringData(15).isAPartOf(wsaaPrtLine003, 0, FILLER).init("  Valid from:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 15);
	private FixedLengthStringData filler8 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 25, FILLER).init(SPACES);
	private FixedLengthStringData filler9 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 30, FILLER).init("To:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine003, 35);
	private FixedLengthStringData filler10 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine003, 45, FILLER).init(SPACES);
	private FixedLengthStringData filler11 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine003, 50, FILLER).init("Risk Unit:");
	private ZonedDecimalData fieldNo007 = new ZonedDecimalData(9, 0).isAPartOf(wsaaPrtLine003, 61).setPattern("ZZZZZZZZZ");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(76);
	private FixedLengthStringData filler12 = new FixedLengthStringData(76).isAPartOf(wsaaPrtLine004, 0, FILLER).init("  Year:     1      2      3      4      5      6      7 8      9     10");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(78);
	private FixedLengthStringData filler13 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(69).isAPartOf(wsaaPrtLine005, 9, FILLER).init("------ ------ ------ ------ ------ ------ ------ ------ ------ ------");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(78);
	private FixedLengthStringData filler15 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler16 = new FixedLengthStringData(69).isAPartOf(wsaaPrtLine006, 9, FILLER).init("Yr Day Yr Day Yr Day Yr Day Yr Day Yr Day Yr Day Yr Day Yr Day Yr Day");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(78);
	private FixedLengthStringData filler17 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine007, 0, FILLER).init("  0+");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 9).setPattern("ZZ");
	private FixedLengthStringData filler18 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 12).setPattern("ZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo010 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 16).setPattern("ZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 19).setPattern("ZZZ");
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 23).setPattern("ZZ");
	private FixedLengthStringData filler22 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 26).setPattern("ZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 30).setPattern("ZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 33).setPattern("ZZZ");
	private FixedLengthStringData filler25 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo016 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 37).setPattern("ZZ");
	private FixedLengthStringData filler26 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 40).setPattern("ZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 44).setPattern("ZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo019 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 47).setPattern("ZZZ");
	private FixedLengthStringData filler29 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 51).setPattern("ZZ");
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 54).setPattern("ZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo022 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 58).setPattern("ZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 61).setPattern("ZZZ");
	private FixedLengthStringData filler33 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 65).setPattern("ZZ");
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo025 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 68).setPattern("ZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine007, 72).setPattern("ZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine007, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(78);
	private FixedLengthStringData filler37 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" 10+");
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 9).setPattern("ZZ");
	private FixedLengthStringData filler38 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 12).setPattern("ZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 16).setPattern("ZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 19).setPattern("ZZZ");
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 23).setPattern("ZZ");
	private FixedLengthStringData filler42 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 26).setPattern("ZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo034 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 30).setPattern("ZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 33).setPattern("ZZZ");
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 37).setPattern("ZZ");
	private FixedLengthStringData filler46 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo037 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 40).setPattern("ZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 44).setPattern("ZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 47).setPattern("ZZZ");
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo040 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 51).setPattern("ZZ");
	private FixedLengthStringData filler50 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 54).setPattern("ZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 58).setPattern("ZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo043 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 61).setPattern("ZZZ");
	private FixedLengthStringData filler53 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 65).setPattern("ZZ");
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 68).setPattern("ZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo046 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine008, 72).setPattern("ZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine008, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(78);
	private FixedLengthStringData filler57 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine009, 0, FILLER).init(" 20+");
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 9).setPattern("ZZ");
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo049 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 12).setPattern("ZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo050 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 16).setPattern("ZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo051 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 19).setPattern("ZZZ");
	private FixedLengthStringData filler61 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo052 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 23).setPattern("ZZ");
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo053 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 26).setPattern("ZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo054 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 30).setPattern("ZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo055 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 33).setPattern("ZZZ");
	private FixedLengthStringData filler65 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo056 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 37).setPattern("ZZ");
	private FixedLengthStringData filler66 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo057 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 40).setPattern("ZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo058 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 44).setPattern("ZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo059 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 47).setPattern("ZZZ");
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo060 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 51).setPattern("ZZ");
	private FixedLengthStringData filler70 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo061 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 54).setPattern("ZZZ");
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo062 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 58).setPattern("ZZ");
	private FixedLengthStringData filler72 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo063 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 61).setPattern("ZZZ");
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo064 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 65).setPattern("ZZ");
	private FixedLengthStringData filler74 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo065 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 68).setPattern("ZZZ");
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo066 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine009, 72).setPattern("ZZ");
	private FixedLengthStringData filler76 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo067 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine009, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(78);
	private FixedLengthStringData filler77 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine010, 0, FILLER).init(" 30+");
	private ZonedDecimalData fieldNo068 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 9).setPattern("ZZ");
	private FixedLengthStringData filler78 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo069 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 12).setPattern("ZZZ");
	private FixedLengthStringData filler79 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo070 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 16).setPattern("ZZ");
	private FixedLengthStringData filler80 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo071 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 19).setPattern("ZZZ");
	private FixedLengthStringData filler81 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo072 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 23).setPattern("ZZ");
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo073 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 26).setPattern("ZZZ");
	private FixedLengthStringData filler83 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo074 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 30).setPattern("ZZ");
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo075 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 33).setPattern("ZZZ");
	private FixedLengthStringData filler85 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo076 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 37).setPattern("ZZ");
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo077 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 40).setPattern("ZZZ");
	private FixedLengthStringData filler87 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo078 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 44).setPattern("ZZ");
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo079 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 47).setPattern("ZZZ");
	private FixedLengthStringData filler89 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo080 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 51).setPattern("ZZ");
	private FixedLengthStringData filler90 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo081 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 54).setPattern("ZZZ");
	private FixedLengthStringData filler91 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo082 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 58).setPattern("ZZ");
	private FixedLengthStringData filler92 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo083 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 61).setPattern("ZZZ");
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo084 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 65).setPattern("ZZ");
	private FixedLengthStringData filler94 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo085 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 68).setPattern("ZZZ");
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo086 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 72).setPattern("ZZ");
	private FixedLengthStringData filler96 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo087 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine010, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(78);
	private FixedLengthStringData filler97 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" 40+");
	private ZonedDecimalData fieldNo088 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 9).setPattern("ZZ");
	private FixedLengthStringData filler98 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo089 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 12).setPattern("ZZZ");
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo090 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 16).setPattern("ZZ");
	private FixedLengthStringData filler100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo091 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 19).setPattern("ZZZ");
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo092 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 23).setPattern("ZZ");
	private FixedLengthStringData filler102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo093 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 26).setPattern("ZZZ");
	private FixedLengthStringData filler103 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo094 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 30).setPattern("ZZ");
	private FixedLengthStringData filler104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo095 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 33).setPattern("ZZZ");
	private FixedLengthStringData filler105 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo096 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 37).setPattern("ZZ");
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo097 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 40).setPattern("ZZZ");
	private FixedLengthStringData filler107 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo098 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 44).setPattern("ZZ");
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo099 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 47).setPattern("ZZZ");
	private FixedLengthStringData filler109 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo100 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 51).setPattern("ZZ");
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo101 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 54).setPattern("ZZZ");
	private FixedLengthStringData filler111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo102 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 58).setPattern("ZZ");
	private FixedLengthStringData filler112 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo103 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 61).setPattern("ZZZ");
	private FixedLengthStringData filler113 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo104 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 65).setPattern("ZZ");
	private FixedLengthStringData filler114 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo105 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 68).setPattern("ZZZ");
	private FixedLengthStringData filler115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo106 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine011, 72).setPattern("ZZ");
	private FixedLengthStringData filler116 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo107 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine011, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(78);
	private FixedLengthStringData filler117 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" 50+");
	private ZonedDecimalData fieldNo108 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 9).setPattern("ZZ");
	private FixedLengthStringData filler118 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo109 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 12).setPattern("ZZZ");
	private FixedLengthStringData filler119 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo110 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 16).setPattern("ZZ");
	private FixedLengthStringData filler120 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo111 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 19).setPattern("ZZZ");
	private FixedLengthStringData filler121 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo112 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 23).setPattern("ZZ");
	private FixedLengthStringData filler122 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo113 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 26).setPattern("ZZZ");
	private FixedLengthStringData filler123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo114 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 30).setPattern("ZZ");
	private FixedLengthStringData filler124 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo115 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 33).setPattern("ZZZ");
	private FixedLengthStringData filler125 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo116 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 37).setPattern("ZZ");
	private FixedLengthStringData filler126 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo117 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 40).setPattern("ZZZ");
	private FixedLengthStringData filler127 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo118 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 44).setPattern("ZZ");
	private FixedLengthStringData filler128 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo119 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 47).setPattern("ZZZ");
	private FixedLengthStringData filler129 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo120 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 51).setPattern("ZZ");
	private FixedLengthStringData filler130 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo121 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 54).setPattern("ZZZ");
	private FixedLengthStringData filler131 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo122 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 58).setPattern("ZZ");
	private FixedLengthStringData filler132 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo123 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 61).setPattern("ZZZ");
	private FixedLengthStringData filler133 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo124 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 65).setPattern("ZZ");
	private FixedLengthStringData filler134 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo125 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 68).setPattern("ZZZ");
	private FixedLengthStringData filler135 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo126 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine012, 72).setPattern("ZZ");
	private FixedLengthStringData filler136 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo127 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine012, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(78);
	private FixedLengthStringData filler137 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" 60+");
	private ZonedDecimalData fieldNo128 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 9).setPattern("ZZ");
	private FixedLengthStringData filler138 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo129 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 12).setPattern("ZZZ");
	private FixedLengthStringData filler139 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo130 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 16).setPattern("ZZ");
	private FixedLengthStringData filler140 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo131 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 19).setPattern("ZZZ");
	private FixedLengthStringData filler141 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo132 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 23).setPattern("ZZ");
	private FixedLengthStringData filler142 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo133 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 26).setPattern("ZZZ");
	private FixedLengthStringData filler143 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo134 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 30).setPattern("ZZ");
	private FixedLengthStringData filler144 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo135 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 33).setPattern("ZZZ");
	private FixedLengthStringData filler145 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo136 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 37).setPattern("ZZ");
	private FixedLengthStringData filler146 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo137 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 40).setPattern("ZZZ");
	private FixedLengthStringData filler147 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo138 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 44).setPattern("ZZ");
	private FixedLengthStringData filler148 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo139 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 47).setPattern("ZZZ");
	private FixedLengthStringData filler149 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo140 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 51).setPattern("ZZ");
	private FixedLengthStringData filler150 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo141 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 54).setPattern("ZZZ");
	private FixedLengthStringData filler151 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo142 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 58).setPattern("ZZ");
	private FixedLengthStringData filler152 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo143 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 61).setPattern("ZZZ");
	private FixedLengthStringData filler153 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo144 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 65).setPattern("ZZ");
	private FixedLengthStringData filler154 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo145 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 68).setPattern("ZZZ");
	private FixedLengthStringData filler155 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo146 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 72).setPattern("ZZ");
	private FixedLengthStringData filler156 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo147 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine013, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(78);
	private FixedLengthStringData filler157 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" 70+");
	private ZonedDecimalData fieldNo148 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 9).setPattern("ZZ");
	private FixedLengthStringData filler158 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo149 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 12).setPattern("ZZZ");
	private FixedLengthStringData filler159 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo150 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 16).setPattern("ZZ");
	private FixedLengthStringData filler160 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo151 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 19).setPattern("ZZZ");
	private FixedLengthStringData filler161 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo152 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 23).setPattern("ZZ");
	private FixedLengthStringData filler162 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo153 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 26).setPattern("ZZZ");
	private FixedLengthStringData filler163 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo154 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 30).setPattern("ZZ");
	private FixedLengthStringData filler164 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo155 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 33).setPattern("ZZZ");
	private FixedLengthStringData filler165 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo156 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 37).setPattern("ZZ");
	private FixedLengthStringData filler166 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo157 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 40).setPattern("ZZZ");
	private FixedLengthStringData filler167 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo158 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 44).setPattern("ZZ");
	private FixedLengthStringData filler168 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo159 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 47).setPattern("ZZZ");
	private FixedLengthStringData filler169 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo160 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 51).setPattern("ZZ");
	private FixedLengthStringData filler170 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo161 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 54).setPattern("ZZZ");
	private FixedLengthStringData filler171 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo162 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 58).setPattern("ZZ");
	private FixedLengthStringData filler172 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo163 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 61).setPattern("ZZZ");
	private FixedLengthStringData filler173 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo164 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 65).setPattern("ZZ");
	private FixedLengthStringData filler174 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo165 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 68).setPattern("ZZZ");
	private FixedLengthStringData filler175 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo166 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine014, 72).setPattern("ZZ");
	private FixedLengthStringData filler176 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo167 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(78);
	private FixedLengthStringData filler177 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" 80+");
	private ZonedDecimalData fieldNo168 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 9).setPattern("ZZ");
	private FixedLengthStringData filler178 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo169 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 12).setPattern("ZZZ");
	private FixedLengthStringData filler179 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo170 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 16).setPattern("ZZ");
	private FixedLengthStringData filler180 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo171 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 19).setPattern("ZZZ");
	private FixedLengthStringData filler181 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo172 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 23).setPattern("ZZ");
	private FixedLengthStringData filler182 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo173 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 26).setPattern("ZZZ");
	private FixedLengthStringData filler183 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo174 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 30).setPattern("ZZ");
	private FixedLengthStringData filler184 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo175 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 33).setPattern("ZZZ");
	private FixedLengthStringData filler185 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo176 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 37).setPattern("ZZ");
	private FixedLengthStringData filler186 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo177 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 40).setPattern("ZZZ");
	private FixedLengthStringData filler187 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 43, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo178 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 44).setPattern("ZZ");
	private FixedLengthStringData filler188 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 46, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo179 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 47).setPattern("ZZZ");
	private FixedLengthStringData filler189 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 50, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo180 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 51).setPattern("ZZ");
	private FixedLengthStringData filler190 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 53, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo181 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 54).setPattern("ZZZ");
	private FixedLengthStringData filler191 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 57, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo182 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 58).setPattern("ZZ");
	private FixedLengthStringData filler192 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 60, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo183 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 61).setPattern("ZZZ");
	private FixedLengthStringData filler193 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 64, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo184 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 65).setPattern("ZZ");
	private FixedLengthStringData filler194 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 67, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo185 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 68).setPattern("ZZZ");
	private FixedLengthStringData filler195 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 71, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo186 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 72).setPattern("ZZ");
	private FixedLengthStringData filler196 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 74, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo187 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 75).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(43);
	private FixedLengthStringData filler197 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" 90+");
	private ZonedDecimalData fieldNo188 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 9).setPattern("ZZ");
	private FixedLengthStringData filler198 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 11, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo189 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 12).setPattern("ZZZ");
	private FixedLengthStringData filler199 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 15, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo190 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 16).setPattern("ZZ");
	private FixedLengthStringData filler200 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 18, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo191 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 19).setPattern("ZZZ");
	private FixedLengthStringData filler201 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 22, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo192 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 23).setPattern("ZZ");
	private FixedLengthStringData filler202 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 25, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo193 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 26).setPattern("ZZZ");
	private FixedLengthStringData filler203 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 29, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo194 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 30).setPattern("ZZ");
	private FixedLengthStringData filler204 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 32, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo195 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 33).setPattern("ZZZ");
	private FixedLengthStringData filler205 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 36, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo196 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 37).setPattern("ZZ");
	private FixedLengthStringData filler206 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 39, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo197 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine016, 40).setPattern("ZZZ");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Tt515rec tt515rec = new Tt515rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tt515pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tt515rec.tt515Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		datcon1rec.intDate.set(tablistrec.itmfrm);
		callDatcon1100();
		fieldNo005.set(datcon1rec.extDate);
		datcon1rec.intDate.set(tablistrec.itmto);
		callDatcon1100();
		fieldNo006.set(datcon1rec.extDate);
		fieldNo007.set(tt515rec.unit);
		fieldNo008.set(tt515rec.tyearno01);
		fieldNo010.set(tt515rec.tyearno02);
		fieldNo012.set(tt515rec.tyearno03);
		fieldNo014.set(tt515rec.tyearno04);
		fieldNo016.set(tt515rec.tyearno05);
		fieldNo018.set(tt515rec.tyearno06);
		fieldNo020.set(tt515rec.tyearno07);
		fieldNo022.set(tt515rec.tyearno08);
		fieldNo024.set(tt515rec.tyearno09);
		fieldNo026.set(tt515rec.tyearno10);
		fieldNo028.set(tt515rec.tyearno11);
		fieldNo030.set(tt515rec.tyearno12);
		fieldNo032.set(tt515rec.tyearno13);
		fieldNo034.set(tt515rec.tyearno14);
		fieldNo036.set(tt515rec.tyearno15);
		fieldNo038.set(tt515rec.tyearno16);
		fieldNo040.set(tt515rec.tyearno17);
		fieldNo042.set(tt515rec.tyearno18);
		fieldNo044.set(tt515rec.tyearno19);
		fieldNo046.set(tt515rec.tyearno20);
		fieldNo048.set(tt515rec.tyearno21);
		fieldNo050.set(tt515rec.tyearno22);
		fieldNo052.set(tt515rec.tyearno23);
		fieldNo054.set(tt515rec.tyearno24);
		fieldNo056.set(tt515rec.tyearno25);
		fieldNo058.set(tt515rec.tyearno26);
		fieldNo060.set(tt515rec.tyearno27);
		fieldNo062.set(tt515rec.tyearno28);
		fieldNo064.set(tt515rec.tyearno29);
		fieldNo066.set(tt515rec.tyearno30);
		fieldNo068.set(tt515rec.tyearno31);
		fieldNo070.set(tt515rec.tyearno32);
		fieldNo072.set(tt515rec.tyearno33);
		fieldNo074.set(tt515rec.tyearno34);
		fieldNo076.set(tt515rec.tyearno35);
		fieldNo078.set(tt515rec.tyearno36);
		fieldNo080.set(tt515rec.tyearno37);
		fieldNo082.set(tt515rec.tyearno38);
		fieldNo084.set(tt515rec.tyearno39);
		fieldNo086.set(tt515rec.tyearno40);
		fieldNo088.set(tt515rec.tyearno41);
		fieldNo090.set(tt515rec.tyearno42);
		fieldNo092.set(tt515rec.tyearno43);
		fieldNo094.set(tt515rec.tyearno44);
		fieldNo096.set(tt515rec.tyearno45);
		fieldNo098.set(tt515rec.tyearno46);
		fieldNo100.set(tt515rec.tyearno47);
		fieldNo102.set(tt515rec.tyearno48);
		fieldNo104.set(tt515rec.tyearno49);
		fieldNo106.set(tt515rec.tyearno50);
		fieldNo108.set(tt515rec.tyearno51);
		fieldNo110.set(tt515rec.tyearno52);
		fieldNo112.set(tt515rec.tyearno53);
		fieldNo114.set(tt515rec.tyearno54);
		fieldNo116.set(tt515rec.tyearno55);
		fieldNo118.set(tt515rec.tyearno56);
		fieldNo120.set(tt515rec.tyearno57);
		fieldNo122.set(tt515rec.tyearno58);
		fieldNo124.set(tt515rec.tyearno59);
		fieldNo126.set(tt515rec.tyearno60);
		fieldNo128.set(tt515rec.tyearno61);
		fieldNo130.set(tt515rec.tyearno62);
		fieldNo132.set(tt515rec.tyearno63);
		fieldNo134.set(tt515rec.tyearno64);
		fieldNo136.set(tt515rec.tyearno65);
		fieldNo138.set(tt515rec.tyearno66);
		fieldNo140.set(tt515rec.tyearno67);
		fieldNo142.set(tt515rec.tyearno68);
		fieldNo144.set(tt515rec.tyearno69);
		fieldNo146.set(tt515rec.tyearno70);
		fieldNo148.set(tt515rec.tyearno71);
		fieldNo150.set(tt515rec.tyearno72);
		fieldNo152.set(tt515rec.tyearno73);
		fieldNo154.set(tt515rec.tyearno74);
		fieldNo156.set(tt515rec.tyearno75);
		fieldNo158.set(tt515rec.tyearno76);
		fieldNo160.set(tt515rec.tyearno77);
		fieldNo162.set(tt515rec.tyearno78);
		fieldNo164.set(tt515rec.tyearno79);
		fieldNo166.set(tt515rec.tyearno80);
		fieldNo168.set(tt515rec.tyearno81);
		fieldNo170.set(tt515rec.tyearno82);
		fieldNo172.set(tt515rec.tyearno83);
		fieldNo174.set(tt515rec.tyearno84);
		fieldNo176.set(tt515rec.tyearno85);
		fieldNo178.set(tt515rec.tyearno86);
		fieldNo180.set(tt515rec.tyearno87);
		fieldNo182.set(tt515rec.tyearno88);
		fieldNo184.set(tt515rec.tyearno89);
		fieldNo186.set(tt515rec.tyearno90);
		fieldNo188.set(tt515rec.tyearno91);
		fieldNo190.set(tt515rec.tyearno92);
		fieldNo192.set(tt515rec.tyearno93);
		fieldNo194.set(tt515rec.tyearno94);
		fieldNo196.set(tt515rec.tyearno95);
		fieldNo009.set(tt515rec.tdayno01);
		fieldNo011.set(tt515rec.tdayno02);
		fieldNo013.set(tt515rec.tdayno03);
		fieldNo015.set(tt515rec.tdayno04);
		fieldNo017.set(tt515rec.tdayno05);
		fieldNo019.set(tt515rec.tdayno06);
		fieldNo021.set(tt515rec.tdayno07);
		fieldNo023.set(tt515rec.tdayno08);
		fieldNo025.set(tt515rec.tdayno09);
		fieldNo027.set(tt515rec.tdayno10);
		fieldNo029.set(tt515rec.tdayno11);
		fieldNo031.set(tt515rec.tdayno12);
		fieldNo033.set(tt515rec.tdayno13);
		fieldNo035.set(tt515rec.tdayno14);
		fieldNo037.set(tt515rec.tdayno15);
		fieldNo039.set(tt515rec.tdayno16);
		fieldNo041.set(tt515rec.tdayno17);
		fieldNo043.set(tt515rec.tdayno18);
		fieldNo045.set(tt515rec.tdayno19);
		fieldNo047.set(tt515rec.tdayno20);
		fieldNo049.set(tt515rec.tdayno21);
		fieldNo051.set(tt515rec.tdayno22);
		fieldNo053.set(tt515rec.tdayno23);
		fieldNo055.set(tt515rec.tdayno24);
		fieldNo057.set(tt515rec.tdayno25);
		fieldNo059.set(tt515rec.tdayno26);
		fieldNo061.set(tt515rec.tdayno27);
		fieldNo063.set(tt515rec.tdayno28);
		fieldNo065.set(tt515rec.tdayno29);
		fieldNo067.set(tt515rec.tdayno30);
		fieldNo069.set(tt515rec.tdayno31);
		fieldNo071.set(tt515rec.tdayno32);
		fieldNo073.set(tt515rec.tdayno33);
		fieldNo075.set(tt515rec.tdayno34);
		fieldNo077.set(tt515rec.tdayno35);
		fieldNo079.set(tt515rec.tdayno36);
		fieldNo081.set(tt515rec.tdayno37);
		fieldNo083.set(tt515rec.tdayno38);
		fieldNo085.set(tt515rec.tdayno39);
		fieldNo087.set(tt515rec.tdayno40);
		fieldNo089.set(tt515rec.tdayno41);
		fieldNo091.set(tt515rec.tdayno42);
		fieldNo093.set(tt515rec.tdayno43);
		fieldNo095.set(tt515rec.tdayno44);
		fieldNo097.set(tt515rec.tdayno45);
		fieldNo099.set(tt515rec.tdayno46);
		fieldNo101.set(tt515rec.tdayno47);
		fieldNo103.set(tt515rec.tdayno48);
		fieldNo105.set(tt515rec.tdayno49);
		fieldNo107.set(tt515rec.tdayno50);
		fieldNo109.set(tt515rec.tdayno51);
		fieldNo111.set(tt515rec.tdayno52);
		fieldNo113.set(tt515rec.tdayno53);
		fieldNo115.set(tt515rec.tdayno54);
		fieldNo117.set(tt515rec.tdayno55);
		fieldNo119.set(tt515rec.tdayno56);
		fieldNo121.set(tt515rec.tdayno57);
		fieldNo123.set(tt515rec.tdayno58);
		fieldNo125.set(tt515rec.tdayno59);
		fieldNo127.set(tt515rec.tdayno60);
		fieldNo129.set(tt515rec.tdayno61);
		fieldNo131.set(tt515rec.tdayno62);
		fieldNo133.set(tt515rec.tdayno63);
		fieldNo135.set(tt515rec.tdayno64);
		fieldNo137.set(tt515rec.tdayno65);
		fieldNo139.set(tt515rec.tdayno66);
		fieldNo141.set(tt515rec.tdayno67);
		fieldNo143.set(tt515rec.tdayno68);
		fieldNo145.set(tt515rec.tdayno69);
		fieldNo147.set(tt515rec.tdayno70);
		fieldNo149.set(tt515rec.tdayno71);
		fieldNo151.set(tt515rec.tdayno72);
		fieldNo153.set(tt515rec.tdayno73);
		fieldNo155.set(tt515rec.tdayno74);
		fieldNo157.set(tt515rec.tdayno75);
		fieldNo159.set(tt515rec.tdayno76);
		fieldNo161.set(tt515rec.tdayno77);
		fieldNo163.set(tt515rec.tdayno78);
		fieldNo165.set(tt515rec.tdayno79);
		fieldNo167.set(tt515rec.tdayno80);
		fieldNo169.set(tt515rec.tdayno81);
		fieldNo171.set(tt515rec.tdayno82);
		fieldNo173.set(tt515rec.tdayno83);
		fieldNo175.set(tt515rec.tdayno84);
		fieldNo177.set(tt515rec.tdayno85);
		fieldNo179.set(tt515rec.tdayno86);
		fieldNo181.set(tt515rec.tdayno87);
		fieldNo183.set(tt515rec.tdayno88);
		fieldNo185.set(tt515rec.tdayno89);
		fieldNo187.set(tt515rec.tdayno90);
		fieldNo189.set(tt515rec.tdayno91);
		fieldNo191.set(tt515rec.tdayno92);
		fieldNo193.set(tt515rec.tdayno93);
		fieldNo195.set(tt515rec.tdayno94);
		fieldNo197.set(tt515rec.tdayno95);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
