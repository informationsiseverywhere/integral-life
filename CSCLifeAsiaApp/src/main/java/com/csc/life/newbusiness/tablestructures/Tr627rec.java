package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:19
 * Description:
 * Copybook name: TR627REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tr627rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tr627Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData eaage = new FixedLengthStringData(1).isAPartOf(tr627Rec, 0);
  	public ZonedDecimalData zsufcage = new ZonedDecimalData(3, 0).isAPartOf(tr627Rec, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(496).isAPartOf(tr627Rec, 4, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tr627Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tr627Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}