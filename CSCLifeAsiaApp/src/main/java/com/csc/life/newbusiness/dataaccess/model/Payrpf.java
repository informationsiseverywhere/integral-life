package com.csc.life.newbusiness.dataaccess.model;

import java.sql.Date;

import com.csc.common.DD;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class Payrpf {

	private long unique_number;
	private String chdrcoy;
	private String chdrnum;
	private int payrseqno;
	private int effdate;
	private String validflag;
	private String billchnl;
	private String billfreq;
	private int billcd;
	private int nextdate;
	private int btdate;
	private int ptdate;
	private String billcurr;
	private String cntcurr;
	private double sinstamt01;
	private double sinstamt02;
	private double sinstamt03;
	private double sinstamt04;
	private double sinstamt05;
	private double sinstamt06;
	private String taxrelmth;
	private String billsupr;
	private int billspfrom;
	private int billspto;
	private String mandref;
	private String grupkey;
	private int incomeSeqNo;
	private String notssupr;
	private int notsspfrom;
	private int notsspto;
	private String aplsupr;
	private int aplspfrom;
	private int aplspto;
	private int tranno;
	private double outstamt;
	private String pstatcode;
	private String grupcoy;
	private String grupnum;
	private String membsel;
	private String billnet;
	private String termid;
	private int transactionDate;
	private int transactionTime;
	private int user;
	private String userProfile;
	private String jobName;
	private Date datime ;
	private String billday ;
	private String billmonth ;
	private String duedd ;
	private String duemm ;
	private String zmandref ;
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public int getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public String getValidflag() {
		return validflag;
	}
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	public String getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}
	public String getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}
	public int getBillcd() {
		return billcd;
	}
	public void setBillcd(int billcd) {
		this.billcd = billcd;
	}
	public int getNextdate() {
		return nextdate;
	}
	public void setNextdate(int nextdate) {
		this.nextdate = nextdate;
	}
	public int getBtdate() {
		return btdate;
	}
	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}
	public int getPtdate() {
		return ptdate;
	}
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}
	public String getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(String billcurr) {
		this.billcurr = billcurr;
	}
	public String getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(String cntcurr) {
		this.cntcurr = cntcurr;
	}
	public double getSinstamt01() {
		return sinstamt01;
	}
	public void setSinstamt01(double sinstamt01) {
		this.sinstamt01 = sinstamt01;
	}
	public double getSinstamt02() {
		return sinstamt02;
	}
	public void setSinstamt02(double sinstamt02) {
		this.sinstamt02 = sinstamt02;
	}
	public double getSinstamt03() {
		return sinstamt03;
	}
	public void setSinstamt03(double sinstamt03) {
		this.sinstamt03 = sinstamt03;
	}
	public double getSinstamt04() {
		return sinstamt04;
	}
	public void setSinstamt04(double sinstamt04) {
		this.sinstamt04 = sinstamt04;
	}
	public double getSinstamt05() {
		return sinstamt05;
	}
	public void setSinstamt05(double sinstamt05) {
		this.sinstamt05 = sinstamt05;
	}
	public double getSinstamt06() {
		return sinstamt06;
	}
	public void setSinstamt06(double sinstamt06) {
		this.sinstamt06 = sinstamt06;
	}
	public String getTaxrelmth() {
		return taxrelmth;
	}
	public void setTaxrelmth(String taxrelmth) {
		this.taxrelmth = taxrelmth;
	}
	public String getBillsupr() {
		return billsupr;
	}
	public void setBillsupr(String billsupr) {
		this.billsupr = billsupr;
	}
	public int getBillspfrom() {
		return billspfrom;
	}
	public void setBillspfrom(int billspfrom) {
		this.billspfrom = billspfrom;
	}
	public int getBillspto() {
		return billspto;
	}
	public void setBillspto(int billspto) {
		this.billspto = billspto;
	}
	public String getMandref() {
		return mandref;
	}
	public void setMandref(String mandref) {
		this.mandref = mandref;
	}
	public String getGrupkey() {
		return grupkey;
	}
	public void setGrupkey(String grupkey) {
		this.grupkey = grupkey;
	}
	public int getIncomeSeqNo() {
		return incomeSeqNo;
	}
	public void setIncomeSeqNo(int incomeSeqNo) {
		this.incomeSeqNo = incomeSeqNo;
	}
	public String getNotssupr() {
		return notssupr;
	}
	public void setNotssupr(String notssupr) {
		this.notssupr = notssupr;
	}
	public int getNotsspfrom() {
		return notsspfrom;
	}
	public void setNotsspfrom(int notsspfrom) {
		this.notsspfrom = notsspfrom;
	}
	public int getNotsspto() {
		return notsspto;
	}
	public void setNotsspto(int notsspto) {
		this.notsspto = notsspto;
	}
	public String getAplsupr() {
		return aplsupr;
	}
	public void setAplsupr(String aplsupr) {
		this.aplsupr = aplsupr;
	}
	public int getAplspfrom() {
		return aplspfrom;
	}
	public void setAplspfrom(int aplspfrom) {
		this.aplspfrom = aplspfrom;
	}
	public int getAplspto() {
		return aplspto;
	}
	public void setAplspto(int aplspto) {
		this.aplspto = aplspto;
	}
	public int getTranno() {
		return tranno;
	}
	public void setTranno(int tranno) {
		this.tranno = tranno;
	}
	public double getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(double outstamt) {
		this.outstamt = outstamt;
	}
	public String getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	public String getGrupcoy() {
		return grupcoy;
	}
	public void setGrupcoy(String grupcoy) {
		this.grupcoy = grupcoy;
	}
	public String getGrupnum() {
		return grupnum;
	}
	public void setGrupnum(String grupnum) {
		this.grupnum = grupnum;
	}
	public String getMembsel() {
		return membsel;
	}
	public void setMembsel(String membsel) {
		this.membsel = membsel;
	}
	public String getBillnet() {
		return billnet;
	}
	public void setBillnet(String billnet) {
		this.billnet = billnet;
	}
	public String getTermid() {
		return termid;
	}
	public void setTermid(String termid) {
		this.termid = termid;
	}
	public int getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(int transactionDate) {
		this.transactionDate = transactionDate;
	}
	public int getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(int transactionTime) {
		this.transactionTime = transactionTime;
	}
	public int getUser() {
		return user;
	}
	public void setUser(int user) {
		this.user = user;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public Date getDatime() {
		return new Date(datime.getTime());//IJTI-316
	}
	public void setDatime(Date datime) {
		this.datime = new Date(datime.getTime());//IJTI-314
	}
	public String getBillday() {
		return billday;
	}
	public void setBillday(String billday) {
		this.billday = billday;
	}
	public String getBillmonth() {
		return billmonth;
	}
	public void setBillmonth(String billmonth) {
		this.billmonth = billmonth;
	}
	public String getDuedd() {
		return duedd;
	}
	public void setDuedd(String duedd) {
		this.duedd = duedd;
	}
	public String getDuemm() {
		return duemm;
	}
	public void setDuemm(String duemm) {
		this.duemm = duemm;
	}
	public String getZmandref() {
		return zmandref;
	}
	public void setZmandref(String zmandref) {
		this.zmandref = zmandref;
	}
	
	
}
