package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:24
 * Description:
 * Copybook name: RACTLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ractlnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData ractlnbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData ractlnbKey = new FixedLengthStringData(64).isAPartOf(ractlnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData ractlnbChdrcoy = new FixedLengthStringData(1).isAPartOf(ractlnbKey, 0);
  	public FixedLengthStringData ractlnbChdrnum = new FixedLengthStringData(8).isAPartOf(ractlnbKey, 1);
  	public FixedLengthStringData ractlnbLife = new FixedLengthStringData(2).isAPartOf(ractlnbKey, 9);
  	public FixedLengthStringData ractlnbCoverage = new FixedLengthStringData(2).isAPartOf(ractlnbKey, 11);
  	public FixedLengthStringData ractlnbRider = new FixedLengthStringData(2).isAPartOf(ractlnbKey, 13);
  	public FixedLengthStringData ractlnbRasnum = new FixedLengthStringData(8).isAPartOf(ractlnbKey, 15);
  	public FixedLengthStringData ractlnbRatype = new FixedLengthStringData(4).isAPartOf(ractlnbKey, 23);
  	public FixedLengthStringData filler = new FixedLengthStringData(37).isAPartOf(ractlnbKey, 27, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(ractlnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		ractlnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}