/******************************************************************************
 * File Name 		: AsgnpfDAO.java
 * Author			: nloganathan5
 * Creation Date	: 28 October 2016
 * Project			: Integral Life
 * Description		: The DAO Interface for ASGNPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Asgnpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface AsgnpfDAO extends BaseDAO<Asgnpf> {

	public List<Asgnpf> searchAsgnpfRecord(String chdrcoy, String chdrnum) throws SQLRuntimeException;

}
