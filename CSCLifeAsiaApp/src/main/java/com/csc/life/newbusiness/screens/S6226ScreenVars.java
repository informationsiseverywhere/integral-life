package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.life.contractservicing.screens.S6226hide;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for S6226
 * @version 1.0 generated on 30/08/09 06:51
 * @author Quipoz
 */
public class S6226ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(3);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(dataFields, 0, FILLER);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 1);
	public FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(errorIndicators, 0, FILLER);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(1).isAPartOf(dataArea, 2);
	public FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(outputIndicators, 0, FILLER);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(204);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(74).isAPartOf(subfileArea, 0);
	public FixedLengthStringData assigneeName = DD.asgname.copy().isAPartOf(subfileFields,0);
	public FixedLengthStringData asgnsel = DD.asgnsel.copy().isAPartOf(subfileFields,30);
	public ZonedDecimalData commfrom = DD.commfrom.copyToZonedDecimal().isAPartOf(subfileFields,40);
	public ZonedDecimalData commto = DD.commto.copyToZonedDecimal().isAPartOf(subfileFields,48);
	public FixedLengthStringData hasgnnum = DD.hasgnnum.copy().isAPartOf(subfileFields,56);
	public ZonedDecimalData hseqno = DD.hseqno.copyToZonedDecimal().isAPartOf(subfileFields,66);
	public FixedLengthStringData reasoncd = DD.reasoncd.copy().isAPartOf(subfileFields,69);
	public FixedLengthStringData updteflag = DD.updteflag.copy().isAPartOf(subfileFields,73);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(32).isAPartOf(subfileArea, 74);
	public FixedLengthStringData asgnameErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData asgnselErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData commfromErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData commtoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData hasgnnumErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData hseqnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData reasoncdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData updteflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(96).isAPartOf(subfileArea, 106);
	public FixedLengthStringData[] asgnameOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] asgnselOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] commfromOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] commtoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] hasgnnumOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] hseqnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] reasoncdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] updteflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 202);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData commfromDisp = new FixedLengthStringData(10);
	public FixedLengthStringData commtoDisp = new FixedLengthStringData(10);

	public LongData S6226screensflWritten = new LongData(0);
	public LongData S6226screenctlWritten = new LongData(0);
	public LongData S6226screenWritten = new LongData(0);
	public LongData S6226windowWritten = new LongData(0);
	public LongData S6226hideWritten = new LongData(0);
	public LongData S6226protectWritten = new LongData(0);
	public GeneralTable s6226screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return s6226screensfl;
	}

	public S6226ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(asgnselOut,new String[] {"36","26","-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(reasoncdOut,new String[] {"37","27","-37",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(commfromOut,new String[] {"11","21","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(commtoOut,new String[] {"12","22","-12",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {updteflag, hasgnnum, hseqno, asgnsel, assigneeName, reasoncd, commfrom, commto};
		screenSflOutFields = new BaseData[][] {updteflagOut, hasgnnumOut, hseqnoOut, asgnselOut, asgnameOut, reasoncdOut, commfromOut, commtoOut};
		screenSflErrFields = new BaseData[] {updteflagErr, hasgnnumErr, hseqnoErr, asgnselErr, asgnameErr, reasoncdErr, commfromErr, commtoErr};
		screenSflDateFields = new BaseData[] {commfrom, commto};
		screenSflDateErrFields = new BaseData[] {commfromErr, commtoErr};
		screenSflDateDispFields = new BaseData[] {commfromDisp, commtoDisp};

		screenFields = new BaseData[] {};
		screenOutFields = new BaseData[][] {};
		screenErrFields = new BaseData[] {};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = S6226screen.class;
		screenSflRecord = S6226screensfl.class;
		screenCtlRecord = S6226screenctl.class;
		initialiseSubfileArea();
		protectRecord = S6226protect.class;
		hideRecord = S6226hide.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(S6226screenctl.lrec.pageSubfile);
	}
}
