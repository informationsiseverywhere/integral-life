/*
 * File: Th506pt.java
 * Date: 30 August 2009 2:34:11
 * Author: Quipoz Limited
 * 
 * Class transformed from TH506PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TH506.
*
*
*
***********************************************************************
* </pre>
*/
public class Th506pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(75);
	private FixedLengthStringData filler1 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(49).isAPartOf(wsaaPrtLine001, 26, FILLER).init("Contract Additional Details                 SH506");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 10);
	private FixedLengthStringData filler4 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine002, 11, FILLER).init(SPACES);
	private FixedLengthStringData filler5 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine002, 17, FILLER).init("Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 24);
	private FixedLengthStringData filler6 = new FixedLengthStringData(9).isAPartOf(wsaaPrtLine002, 29, FILLER).init("   Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 38);
	private FixedLengthStringData filler7 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(74);
	private FixedLengthStringData filler8 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Proposal Date Mandatory......:");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine003, 33);
	private FixedLengthStringData filler9 = new FixedLengthStringData(36).isAPartOf(wsaaPrtLine003, 34, FILLER).init("  (Y/N)              Basic Plan:");
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine003, 70);

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(73);
	private FixedLengthStringData filler10 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" U/W Decision Date Mand.......:");
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 33);
	private FixedLengthStringData filler11 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine004, 34, FILLER).init("  (Y/N)              Default Source:");
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 71);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(41);
	private FixedLengthStringData filler12 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine005, 0, FILLER).init(" RCD Based on Transaction Date:");
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 33);
	private FixedLengthStringData filler13 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 34, FILLER).init("  (Y/N)");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(41);
	private FixedLengthStringData filler14 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine006, 0, FILLER).init(" Temp. Receipt Details Mand...:");
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 33);
	private FixedLengthStringData filler15 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 34, FILLER).init("  (Y/N)");

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(41);
	private FixedLengthStringData filler16 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine007, 0, FILLER).init(" Enable Component Add Anytime.:");
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 33);
	private FixedLengthStringData filler17 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 34, FILLER).init("  (Y/N)");

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(48);
	private FixedLengthStringData filler18 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine008, 0, FILLER).init(" Back Dated Risk Commencement Date Calculation:-");

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(46);
	private FixedLengthStringData filler19 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 5, FILLER).init("Frequency :");
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 18);
	private FixedLengthStringData filler21 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine009, 20, FILLER).init("   (by month,by year,etc.)");

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(20);
	private FixedLengthStringData filler22 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler23 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 5, FILLER).init("Factor    :");
	private ZonedDecimalData fieldNo013 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine010, 18).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(63);
	private FixedLengthStringData filler24 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine011, 0, FILLER).init(" Only Allowed in Branch:");
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 25);
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 27, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 29);
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 31, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 33);
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 37);
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 39, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 41);
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 43, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 45);
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 49);
	private FixedLengthStringData filler31 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 53);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 57);
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 59, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 61);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(69);
	private FixedLengthStringData filler34 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine012, 0, FILLER).init(" ETI Processing:");
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 17);
	private FixedLengthStringData filler35 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine012, 20, FILLER).init(SPACES);
	private FixedLengthStringData filler36 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine012, 28, FILLER).init("ETI SI to include Accum Dividend:   (Y/N)");

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(68);
	private FixedLengthStringData filler37 = new FixedLengthStringData(17).isAPartOf(wsaaPrtLine013, 0, FILLER).init(" RPU Processing:");
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 17);
	private FixedLengthStringData filler38 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine013, 20, FILLER).init(SPACES);
	private FixedLengthStringData filler39 = new FixedLengthStringData(34).isAPartOf(wsaaPrtLine013, 28, FILLER).init("No.of years inforce before surrend");
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 62);
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 63, FILLER).init("r:");
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine013, 66).setPattern("ZZ");

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(24);
	private FixedLengthStringData filler41 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine014, 0, FILLER).init(" No.of Days to NTU :");
	private ZonedDecimalData fieldNo028 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine014, 21).setPattern("ZZZ");

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(65);
	private FixedLengthStringData filler42 = new FixedLengthStringData(63).isAPartOf(wsaaPrtLine015, 0, FILLER).init(" Less than No of BenBill Charges Trigger Warning Letter. . . :");
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine015, 63).setPattern("ZZ");
	//TMLII-294 AC-01-007 START
	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(30);
	private FixedLengthStringData filler44 = new FixedLengthStringData(27).isAPartOf(wsaaPrtLine015, 0, FILLER).init("SUN Dimension 3) Prod Name:");
	private ZonedDecimalData fieldNo031 = new ZonedDecimalData(3, 0).isAPartOf(wsaaPrtLine015, 27);
	//TMLII-294 AC-01-007 END

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(65);
	private FixedLengthStringData filler43 = new FixedLengthStringData(63).isAPartOf(wsaaPrtLine016, 0, FILLER).init(" Less than No of BenBill Charges Trigger Insufficient Letter.:");
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrtLine016, 63).setPattern("ZZ");
	
	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(41);
	private FixedLengthStringData filler45 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine018, 0, FILLER).init(" Enable Component Add Anytime.:");
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 33);
	private FixedLengthStringData filler46 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine018, 34, FILLER).init("  (Y/N)");
	
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Th506rec th506rec = new Th506rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Th506pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		th506rec.th506Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(th506rec.mandatory01);
		fieldNo006.set(th506rec.crtable);
		fieldNo007.set(th506rec.mandatory02);
		fieldNo009.set(th506rec.ind);
		fieldNo010.set(th506rec.cflg);
		fieldNo011.set(th506rec.indic);
		fieldNo012.set(th506rec.freqcy);
		fieldNo013.set(th506rec.cnt);
		fieldNo014.set(th506rec.branch01);
		fieldNo015.set(th506rec.branch02);
		fieldNo016.set(th506rec.branch03);
		fieldNo017.set(th506rec.branch04);
		fieldNo018.set(th506rec.branch05);
		fieldNo019.set(th506rec.branch06);
		fieldNo020.set(th506rec.branch07);
		fieldNo021.set(th506rec.branch08);
		fieldNo022.set(th506rec.branch09);
		fieldNo023.set(th506rec.branch10);
		fieldNo008.set(th506rec.zzsrce);
		fieldNo008.set(th506rec.calcmeth);
		fieldNo024.set(th506rec.znfopt01);
		fieldNo026.set(th506rec.confirm);
		fieldNo025.set(th506rec.znfopt02);
		fieldNo027.set(th506rec.yearInforce);
		fieldNo028.set(th506rec.expdays);
		fieldNo029.set(th506rec.nofbbwrn);
		fieldNo030.set(th506rec.nofbbisf);
		//TMLII-294 AC-01-007
		fieldNo031.set(th506rec.zdmsion);
		fieldNo032.set(th506rec.fatcaFlag);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		//TMLII-294 AC-01-007 START
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		//TMLII-294 AC-01-007 END
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
