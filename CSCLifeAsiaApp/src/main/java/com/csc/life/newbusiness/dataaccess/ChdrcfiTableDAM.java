package com.csc.life.newbusiness.dataaccess;

import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChdrcfiTableDAM.java
 * Date: Sun, 30 Aug 2009 03:31:35
 * Class transformed from CHDRCFI.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChdrcfiTableDAM extends ChdrpfTableDAM {

	public ChdrcfiTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CHDRCFI");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRPFX, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "SERVUNIT, " +
		            "CNTTYPE, " +
		            "TRANNO, " +
		            "TRANID, " +
		            "VALIDFLAG, " +
		            "AVLISU, " +
		            "STATCODE, " +
		            "STATDATE, " +
		            "STATTRAN, " +
		            "PSTCDE, " +
		            "PSTDAT, " +
		            "PSTTRN, " +
		            "CCDATE, " +
		            "COWNNUM, " +
		            "PAYRNUM, " +
		            "DESPNUM, " +
		            "ASGNNUM, " +
		            "BILLFREQ, " +
		            "BILLCD, " +
		            "BTDATE, " +
		            "STMDTE, " +
		            "PTDATE, " +
		            "CNTCURR, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrpfx,
                               chdrcoy,
                               chdrnum,
                               servunit,
                               cnttype,
                               tranno,
                               tranid,
                               validflag,
                               avlisu,
                               statcode,
                               statdate,
                               stattran,
                               pstatcode,
                               pstatdate,
                               pstattran,
                               ccdate,
                               cownnum,
                               payrnum,
                               despnum,
                               asgnnum,
                               billfreq,
                               billcd,
                               btdate,
                               statementDate,
                               ptdate,
                               cntcurr,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(55);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller20.setInternal(chdrcoy.toInternal());
	nonKeyFiller30.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(163);
		
		nonKeyData.set(
					getChdrpfx().toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getServunit().toInternal()
					+ getCnttype().toInternal()
					+ getTranno().toInternal()
					+ getTranid().toInternal()
					+ getValidflag().toInternal()
					+ getAvlisu().toInternal()
					+ getStatcode().toInternal()
					+ getStatdate().toInternal()
					+ getStattran().toInternal()
					+ getPstatcode().toInternal()
					+ getPstatdate().toInternal()
					+ getPstattran().toInternal()
					+ getCcdate().toInternal()
					+ getCownnum().toInternal()
					+ getPayrnum().toInternal()
					+ getDespnum().toInternal()
					+ getAsgnnum().toInternal()
					+ getBillfreq().toInternal()
					+ getBillcd().toInternal()
					+ getBtdate().toInternal()
					+ getStatementDate().toInternal()
					+ getPtdate().toInternal()
					+ getCntcurr().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrpfx);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, servunit);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, avlisu);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, statdate);
			what = ExternalData.chop(what, stattran);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, pstatdate);
			what = ExternalData.chop(what, pstattran);
			what = ExternalData.chop(what, ccdate);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, payrnum);
			what = ExternalData.chop(what, despnum);
			what = ExternalData.chop(what, asgnnum);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, statementDate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(Object what) {
		chdrpfx.set(what);
	}	
	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getAvlisu() {
		return avlisu;
	}
	public void setAvlisu(Object what) {
		avlisu.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public PackedDecimalData getStatdate() {
		return statdate;
	}
	public void setStatdate(Object what) {
		setStatdate(what, false);
	}
	public void setStatdate(Object what, boolean rounded) {
		if (rounded)
			statdate.setRounded(what);
		else
			statdate.set(what);
	}	
	public PackedDecimalData getStattran() {
		return stattran;
	}
	public void setStattran(Object what) {
		setStattran(what, false);
	}
	public void setStattran(Object what, boolean rounded) {
		if (rounded)
			stattran.setRounded(what);
		else
			stattran.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public PackedDecimalData getPstatdate() {
		return pstatdate;
	}
	public void setPstatdate(Object what) {
		setPstatdate(what, false);
	}
	public void setPstatdate(Object what, boolean rounded) {
		if (rounded)
			pstatdate.setRounded(what);
		else
			pstatdate.set(what);
	}	
	public PackedDecimalData getPstattran() {
		return pstattran;
	}
	public void setPstattran(Object what) {
		setPstattran(what, false);
	}
	public void setPstattran(Object what, boolean rounded) {
		if (rounded)
			pstattran.setRounded(what);
		else
			pstattran.set(what);
	}	
	public PackedDecimalData getCcdate() {
		return ccdate;
	}
	public void setCcdate(Object what) {
		setCcdate(what, false);
	}
	public void setCcdate(Object what, boolean rounded) {
		if (rounded)
			ccdate.setRounded(what);
		else
			ccdate.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(Object what) {
		payrnum.set(what);
	}	
	public FixedLengthStringData getDespnum() {
		return despnum;
	}
	public void setDespnum(Object what) {
		despnum.set(what);
	}	
	public FixedLengthStringData getAsgnnum() {
		return asgnnum;
	}
	public void setAsgnnum(Object what) {
		asgnnum.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getStatementDate() {
		return statementDate;
	}
	public void setStatementDate(Object what) {
		setStatementDate(what, false);
	}
	public void setStatementDate(Object what, boolean rounded) {
		if (rounded)
			statementDate.setRounded(what);
		else
			statementDate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		chdrpfx.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		servunit.clear();
		cnttype.clear();
		tranno.clear();
		tranid.clear();
		validflag.clear();
		avlisu.clear();
		statcode.clear();
		statdate.clear();
		stattran.clear();
		pstatcode.clear();
		pstatdate.clear();
		pstattran.clear();
		ccdate.clear();
		cownnum.clear();
		payrnum.clear();
		despnum.clear();
		asgnnum.clear();
		billfreq.clear();
		billcd.clear();
		btdate.clear();
		statementDate.clear();
		ptdate.clear();
		cntcurr.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}