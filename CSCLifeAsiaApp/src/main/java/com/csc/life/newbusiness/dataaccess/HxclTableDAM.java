package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HxclTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:28
 * Class transformed from HXCL.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HxclTableDAM extends HxclpfTableDAM {

	public HxclTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HXCL");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", FUPNO"
		             + ", HXCLSEQNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "FUPNO, " +
		            "HXCLSEQNO, " +
		            "HXCLNOTE01, " +
		            "HXCLNOTE02, " +
		            "HXCLNOTE03, " +
		            "HXCLNOTE04, " +
		            "HXCLNOTE05, " +
		            "HXCLNOTE06, " +
		            "USER_T, " +
		            "FUPCDE, " +
		            "HXCLLETTYP, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "FUPNO ASC, " +
		            "HXCLSEQNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "FUPNO DESC, " +
		            "HXCLSEQNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               fupno,
                               hxclseqno,
                               hxclnote01,
                               hxclnote02,
                               hxclnote03,
                               hxclnote04,
                               hxclnote05,
                               hxclnote06,
                               user,
                               fupcode,
                               hxcllettyp,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(51);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getFupno().toInternal()
					+ getHxclseqno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, fupno);
			what = ExternalData.chop(what, hxclseqno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(fupno.toInternal());
	nonKeyFiller40.setInternal(hxclseqno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(524);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ getHxclnote01().toInternal()
					+ getHxclnote02().toInternal()
					+ getHxclnote03().toInternal()
					+ getHxclnote04().toInternal()
					+ getHxclnote05().toInternal()
					+ getHxclnote06().toInternal()
					+ getUser().toInternal()
					+ getFupcode().toInternal()
					+ getHxcllettyp().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, hxclnote01);
			what = ExternalData.chop(what, hxclnote02);
			what = ExternalData.chop(what, hxclnote03);
			what = ExternalData.chop(what, hxclnote04);
			what = ExternalData.chop(what, hxclnote05);
			what = ExternalData.chop(what, hxclnote06);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, fupcode);
			what = ExternalData.chop(what, hxcllettyp);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getFupno() {
		return fupno;
	}
	public void setFupno(Object what) {
		setFupno(what, false);
	}
	public void setFupno(Object what, boolean rounded) {
		if (rounded)
			fupno.setRounded(what);
		else
			fupno.set(what);
	}
	public PackedDecimalData getHxclseqno() {
		return hxclseqno;
	}
	public void setHxclseqno(Object what) {
		setHxclseqno(what, false);
	}
	public void setHxclseqno(Object what, boolean rounded) {
		if (rounded)
			hxclseqno.setRounded(what);
		else
			hxclseqno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getHxclnote01() {
		return hxclnote01;
	}
	public void setHxclnote01(Object what) {
		hxclnote01.set(what);
	}	
	public FixedLengthStringData getHxclnote02() {
		return hxclnote02;
	}
	public void setHxclnote02(Object what) {
		hxclnote02.set(what);
	}	
	public FixedLengthStringData getHxclnote03() {
		return hxclnote03;
	}
	public void setHxclnote03(Object what) {
		hxclnote03.set(what);
	}	
	public FixedLengthStringData getHxclnote04() {
		return hxclnote04;
	}
	public void setHxclnote04(Object what) {
		hxclnote04.set(what);
	}	
	public FixedLengthStringData getHxclnote05() {
		return hxclnote05;
	}
	public void setHxclnote05(Object what) {
		hxclnote05.set(what);
	}	
	public FixedLengthStringData getHxclnote06() {
		return hxclnote06;
	}
	public void setHxclnote06(Object what) {
		hxclnote06.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getFupcode() {
		return fupcode;
	}
	public void setFupcode(Object what) {
		fupcode.set(what);
	}	
	public FixedLengthStringData getHxcllettyp() {
		return hxcllettyp;
	}
	public void setHxcllettyp(Object what) {
		hxcllettyp.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getHxclnotes() {
		return new FixedLengthStringData(hxclnote01.toInternal()
										+ hxclnote02.toInternal()
										+ hxclnote03.toInternal()
										+ hxclnote04.toInternal()
										+ hxclnote05.toInternal()
										+ hxclnote06.toInternal());
	}
	public void setHxclnotes(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getHxclnotes().getLength()).init(obj);
	
		what = ExternalData.chop(what, hxclnote01);
		what = ExternalData.chop(what, hxclnote02);
		what = ExternalData.chop(what, hxclnote03);
		what = ExternalData.chop(what, hxclnote04);
		what = ExternalData.chop(what, hxclnote05);
		what = ExternalData.chop(what, hxclnote06);
	}
	public FixedLengthStringData getHxclnote(BaseData indx) {
		return getHxclnote(indx.toInt());
	}
	public FixedLengthStringData getHxclnote(int indx) {

		switch (indx) {
			case 1 : return hxclnote01;
			case 2 : return hxclnote02;
			case 3 : return hxclnote03;
			case 4 : return hxclnote04;
			case 5 : return hxclnote05;
			case 6 : return hxclnote06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setHxclnote(BaseData indx, Object what) {
		setHxclnote(indx.toInt(), what);
	}
	public void setHxclnote(int indx, Object what) {

		switch (indx) {
			case 1 : setHxclnote01(what);
					 break;
			case 2 : setHxclnote02(what);
					 break;
			case 3 : setHxclnote03(what);
					 break;
			case 4 : setHxclnote04(what);
					 break;
			case 5 : setHxclnote05(what);
					 break;
			case 6 : setHxclnote06(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		fupno.clear();
		hxclseqno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		hxclnote01.clear();
		hxclnote02.clear();
		hxclnote03.clear();
		hxclnote04.clear();
		hxclnote05.clear();
		hxclnote06.clear();
		user.clear();
		fupcode.clear();
		hxcllettyp.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}