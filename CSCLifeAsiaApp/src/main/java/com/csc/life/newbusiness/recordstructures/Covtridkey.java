package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:30
 * Description:
 * Copybook name: COVTRIDKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covtridkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covtridFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covtridKey = new FixedLengthStringData(64).isAPartOf(covtridFileKey, 0, REDEFINE);
  	public FixedLengthStringData covtridChdrcoy = new FixedLengthStringData(1).isAPartOf(covtridKey, 0);
  	public FixedLengthStringData covtridChdrnum = new FixedLengthStringData(8).isAPartOf(covtridKey, 1);
  	public FixedLengthStringData covtridLife = new FixedLengthStringData(2).isAPartOf(covtridKey, 9);
  	public FixedLengthStringData covtridCoverage = new FixedLengthStringData(2).isAPartOf(covtridKey, 11);
  	public FixedLengthStringData covtridCrtable = new FixedLengthStringData(4).isAPartOf(covtridKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(47).isAPartOf(covtridKey, 17, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covtridFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covtridFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}