package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;


public class Sr57kScreenVars extends SmartVarModel { 


	//rec length = 298 + 72 + 216 = 586
	final int recLen = 586;
	public FixedLengthStringData dataArea = new FixedLengthStringData(recLen);
	
	//length = 8 + 3 + 30 + 10 + 10 + 8 + 47 + 8 + 47 + 1 + 1 + 1 + 1 + 30 + 30 + 8 + 8 + 47 = 298
	public FixedLengthStringData dataFields = new FixedLengthStringData(298).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields); //8
	public FixedLengthStringData cnttyp = DD.cnttyp.copy().isAPartOf(dataFields); //3
	public FixedLengthStringData ctypedes  = DD.ctypedes.copy().isAPartOf(dataFields); //30
	public FixedLengthStringData chdrstatus = DD.chdrstatus.copy().isAPartOf(dataFields); //10
	public FixedLengthStringData premstatus = DD.premstatus.copy().isAPartOf(dataFields); //10
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields); //8
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields); //47
	public FixedLengthStringData lifenum = DD.lifenum.copy().isAPartOf(dataFields); //8
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields); //47
	public FixedLengthStringData reqntype01 = DD.reqntype.copy().isAPartOf(dataFields); //1
	public FixedLengthStringData reqntype02 = DD.reqntype.copy().isAPartOf(dataFields); //1
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields); //1
	public FixedLengthStringData crcind = DD.crcind.copy().isAPartOf(dataFields); //1
	public FixedLengthStringData mopdesc = DD.mopdesc.copy().isAPartOf(dataFields); //30
	public FixedLengthStringData mopdesc02 = DD.mopdesc02.copy().isAPartOf(dataFields); //30
	public ZonedDecimalData ptdate = DD.ptdate.copyToZonedDecimal().isAPartOf(dataFields);//8
	
	/* TSD 321 Phase 2 STARTS */
	public FixedLengthStringData payrnum = DD.payrnum.copy().isAPartOf(dataFields, 243); //8
	public FixedLengthStringData payorname = DD.payorname.copy().isAPartOf(dataFields, 251); //47
	/* TSD 321 Phase 2 ENDS */
	
	
	//length = 18 * 4 = 72
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 298);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData ctypedesErr  = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData chdrstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData premstatusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifenumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData reqntype01Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData reqntype02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData crcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData mopdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData mopdesc02Err = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData ptdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	
	/* TSD 321 Phase 2 STARTS */
	public FixedLengthStringData payrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData payornameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	/* TSD 321 Phase 2 ENDS */
	
	//length = 18 * 12 = 216
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(216).isAPartOf(dataArea, 370);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] chdrstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] premstatusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifenumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] reqntype01Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] reqntype02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] crcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] mopdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] mopdesc02Out = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] ptdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	
	/* TSD 321 Phase 2 STARTS */
	public FixedLengthStringData[] payrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] payornameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	/* TSD 321 Phase 2 ENDS */
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public LongData Sr57kscreenWritten = new LongData(0);
	public LongData Sr57kwindowWritten = new LongData(0);
	public LongData Sr57kprotectWritten = new LongData(0);
	public FixedLengthStringData ptdateDisp = new FixedLengthStringData(10);
	
	@Override
	public boolean hasSubfile() {
		return false;
	}


	public Sr57kScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected void initialiseScreenVars() {
		
		/* TSD 321 Phase 2 STARTS */
		fieldIndMap.put(payornameOut,new String[] {" NN"," NN "," NN "," NN ",null, null, null, null, null, null, null, null});
		fieldIndMap.put(payrnumOut,new String[] {" NN"," NN "," NN "," NN ",null, null, null, null, null, null, null, null});
		/* TSD 321 Phase 2 ENDS */
		
		screenFields = new BaseData[] {chdrnum, cnttyp, ctypedes, chdrstatus, premstatus, cownnum, ownername, lifenum, lifename, reqntype01, reqntype02, ddind, crcind, payrnum, payorname};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypOut, ctypedesOut, chdrstatusOut, premstatusOut, cownnumOut, ownernameOut, lifenumOut, lifenameOut, reqntype01Out, reqntype02Out, ddindOut, crcindOut, payrnumOut, payornameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypErr, ctypedesErr, chdrstatusErr, premstatusErr, cownnumErr, ownernameErr, lifenumErr, lifenameErr, reqntype01Err, reqntype02Err, ddindErr, crcindErr, payrnumErr, payornameErr};

		screenDateFields = new BaseData[] {ptdate};
		screenDateErrFields = new BaseData[] {ptdateErr};
		screenDateDispFields = new BaseData[] {ptdateDisp};
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr57kscreen.class;
		protectRecord = Sr57kprotect.class;
	}

}
