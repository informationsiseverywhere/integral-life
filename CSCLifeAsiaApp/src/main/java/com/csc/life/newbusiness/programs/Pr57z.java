/*
 * File: P6276.java
 * Date: 30 August 2009 0:42:03
 * Author: Quipoz Limited
 * 
 * Class transformed from P6276.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.life.newbusiness.screens.Sr57zScreenVars;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.procedures.Subprog;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Subprogrec;
import com.csc.smart.recordstructures.Wsspwindow;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* P6276 Mandate Scroll Selection
*--------------------------------
*
* This program will be used during the process of obtaining a
* mandate for use on a contract. It will have been passed the
* client number of a contract that is to pay the Direct Debit
* and the Billing Date that will be used on the contract.
*
* It will display all the existing mandates for that payer
* and allow the user to select one for processing by programs
* higher up the stack. As a mandate may only be used on one
* Billing Day in a month any mandates that are found for the
* payer but do not match the Billing Day of the Billing Date
* passed should be displayed but the user must be prevented
* from selecting them.
*
* If the user does not wish to select any of the mandates
* displayed then the Create Mandate Function key can be used
* to create one.
*
* If there are no mandates at all for the payer or no
* mandates that are suitable, (i.e. no match on Billing Day),
* then the display of the screen should be by-passed and
* control should proceed as if the user has pressed the Create
* Mandate Function key. This should only happen if the user is
* sanctioned to create mandates, otherwise it will continue as
* if nothing has been selected.
*
* Pressing the Create Mandate Fkey leads  to  the  Add  Mandate
* Screen where the user will enter the remaining mandate screen
* details and a new Mandate record will be created from there.
*
* Sufficient details will be displayed on each line for the user
* to decide if any of the existing mandates are usable on the
* contract in question.
*
*****************************************************************
* </pre>
*/
public class Pr57z extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR57Z");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaClnt = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaPayrcoy = new FixedLengthStringData(1).isAPartOf(wsaaClnt, 0);
	private FixedLengthStringData wsaaPayrnum = new FixedLengthStringData(8).isAPartOf(wsaaClnt, 1);

	private FixedLengthStringData wsaaClntTemp = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaPayrcoyTemp = new FixedLengthStringData(1).isAPartOf(wsaaClntTemp, 0);
	private FixedLengthStringData wsaaPayrnumTemp = new FixedLengthStringData(8).isAPartOf(wsaaClntTemp, 1);

	private FixedLengthStringData wsaaBillcd = new FixedLengthStringData(10);
	private ZonedDecimalData wsaaBilldd = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillcd, 0).setUnsigned();
	private ZonedDecimalData wsaaBillmm = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillcd, 3).setUnsigned();
	private ZonedDecimalData wsaaBillyyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaBillcd, 6).setUnsigned();

	private FixedLengthStringData wsaaEffdate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaEffyyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffdate, 0).setUnsigned();
	private ZonedDecimalData wsaaEffmm = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffdate, 4).setUnsigned();
	private ZonedDecimalData wsaaEffdd = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffdate, 6).setUnsigned();
	private ZonedDecimalData wsaaEffdate9 = new ZonedDecimalData(8, 0).isAPartOf(wsaaEffdate, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaDummyBillcd = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaDummyBillyyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaDummyBillcd, 0).setUnsigned();
	private ZonedDecimalData wsaaDummyBillmm = new ZonedDecimalData(2, 0).isAPartOf(wsaaDummyBillcd, 4).setUnsigned();
	private ZonedDecimalData wsaaDummyBilldd = new ZonedDecimalData(2, 0).isAPartOf(wsaaDummyBillcd, 6).setUnsigned();

		/* MISC */
	private FixedLengthStringData wsaaSelectRecord = new FixedLengthStringData(1);
	private Validator wsaaRecordSelected = new Validator(wsaaSelectRecord, "Y");
	private Validator wsaaRecordNotSelected = new Validator(wsaaSelectRecord, "N");
	private PackedDecimalData wsaaSub = new PackedDecimalData(1, 0);
	private PackedDecimalData wsaaValidSelectRecs = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCount = new PackedDecimalData(3, 0);
	private static final String h052 = "H052";
	private static final String h061 = "H061";
		/* FORMATS */
	private static final String mandrec = "MANDREC";
	private MandTableDAM mandIO = new MandTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Subprogrec subprogrec = new Subprogrec();
	private Wsspwindow wsspwindow = new Wsspwindow();
	private Sr57zScreenVars sv = ScreenProgram.getScreenVars( Sr57zScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		validateSubfile2050, 
		checkForErrors2060, 
		exit2190, 
		exit2690
	}

	public Pr57z() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57z", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspwindow.userArea = convertAndSetParam(wsspwindow.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaClnt.set(SPACES);
		wsaaClntTemp.set(SPACES);
		wsaaValidSelectRecs.set(ZERO);
		wsaaSelectRecord.set("N");
		scrnparams.function.set(varcom.sclr);
		processScreen("SR57Z", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/*     READ THE MANDATE FILE*/
		/*    KEY = CO. NAME, CLIENT NUMBER, MANDATE REFERENCE NUMBER*/
		/*    KEY = MAND-PAYRCOY, MAND-PAYRNUM, MAND-MANDREF*/
		mandIO.setParams(SPACES);
		mandIO.setPayrcoy(wsspcomn.fsuco);
		wsaaPayrcoy.set(wsspcomn.fsuco);
		mandIO.setPayrnum(wsspwindow.payrnum);
		wsaaPayrnum.set(wsspwindow.payrnum);
		mandIO.setMandref("99999");
		mandIO.setFunction(varcom.begn);
		mandIO.setFormat(mandrec);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(),varcom.endp)
		&& isNE(mandIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
		/* IF RECORD FOUND SET UP SUBFILE*/
		wsaaCount.set(ZERO);
		if (isEQ(mandIO.getStatuz(),varcom.oK)) {
			wsaaPayrcoyTemp.set(mandIO.getPayrcoy());
			wsaaPayrnumTemp.set(mandIO.getPayrnum());
			while ( !((isEQ(wsaaCount,17))
			|| (isEQ(mandIO.getStatuz(),varcom.endp))
			|| (isNE(wsaaClnt,wsaaClntTemp)))) {
				loadScreen1100();
			}
			
			if (isNE(mandIO.getStatuz(),varcom.endp)
			&& isEQ(wsaaClnt,wsaaClntTemp)) {
				scrnparams.subfileMore.set("Y");
			}
			else {
				scrnparams.subfileMore.set("N");
			}
			scrnparams.subfileRrn.set(1);
		}
	}

	/**
	* <pre>
	*   LOAD SUBFILE SCREEN
	* </pre>
	*/
protected void loadScreen1100()
	{
					screenLoad1110();
					readNextr1130();
				}

protected void screenLoad1110()
	{
		if (isNE(mandIO.getValidflag(), "1")
		|| isNE(mandIO.getCrcind(), SPACES)
		|| isNE(mandIO.getPayind(),"P")) { //ILIFE-2472  
			return ;
		}
		sv.select.set(SPACES);
		sv.mandref.set(mandIO.getMandref());
		sv.effdate.set(mandIO.getEffdate());
		sv.bankkey.set(mandIO.getBankkey());
		sv.bankacckey.set(mandIO.getBankacckey());
		sv.mandstat.set(mandIO.getMandstat());
		wsaaBillcd.set(SPACES);
		wsaaEffdate.set(SPACES);
		wsaaBillcd.set(wsspwindow.billcd);
		wsaaEffdate9.set(mandIO.getEffdate());
		wsaaDummyBillyyyy.set(wsaaBillyyyy);
		wsaaDummyBillmm.set(wsaaBillmm);
		wsaaDummyBilldd.set(wsaaBilldd);
		if (isLT(wsaaDummyBillcd,wsaaEffdate)) {
			sv.selectOut[varcom.pr.toInt()].set("Y");
		}
		else {
			wsaaValidSelectRecs.add(1);
			sv.selectOut[varcom.pr.toInt()].set("N");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SR57Z", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		wsaaCount.add(1);
	}

protected void readNextr1130()
	{
		mandIO.setFunction(varcom.nextr);
		mandIO.setFormat(mandrec);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(),varcom.endp)
		&& isNE(mandIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
		if (isNE(mandIO.getStatuz(),varcom.endp)) {
			wsaaPayrcoyTemp.set(mandIO.getPayrcoy());
			wsaaPayrnumTemp.set(mandIO.getPayrnum());
		}
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*if (isEQ(wsaaValidSelectRecs,ZERO)) {
			wsspcomn.edterror.set(varcom.oK);
			wsaaValidSelectRecs.set(1);
			scrnparams.statuz.set(varcom.insr);
			validation2100();
			wsspcomn.sectionno.set("3000");
			return ;
			*//******     GO TO    2010-VALIDATE-SCREEN                   <D509CS>*//*
		}*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'SR57ZIO' USING SCRN-SCREEN-PARAMS                      */
		/*                         SR57Z-DATA-AREA                         */
		/*                         SR57Z-SUBFILE-AREA.                     */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*                                                         <D509CS>*/
		validation2100();
		/*EXIT*/
	}

	/**
	* <pre>
	*                                                         <D509CS>
	* </pre>
	*/
protected void validation2100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validateScreen2010();
				case validateSubfile2050: 
					validateSubfile2050();
				case checkForErrors2060: 
					checkForErrors2060();
				case exit2190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*************************                                 <D509CS>
	* </pre>
	*/
protected void validateScreen2010()
	{
		/*                                                         <D509CS>*/
		/*     F9 PRESSED TO CALC                                  <D509CS>*/
		/*                                                         <D509CS>*/
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.checkForErrors2060);
		}
		/*                                                         <D509CS>*/
		/*     F11 PRESSED TO KILL                                 <D509CS>*/
		/*                                                         <D509CS>*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2190);
		}
		/*                                                         <D509CS>*/
		/*     PAGE UP THE SCROLLING SCREEN                        <D509CS>*/
		/*                                                         <D509CS>*/
		if (isEQ(scrnparams.statuz,varcom.rolu)) {
			wsaaCount.set(ZERO);
			while ( !((isEQ(wsaaCount,17))
			|| (isEQ(mandIO.getStatuz(),varcom.endp))
			|| (isNE(wsaaClnt,wsaaClntTemp)))) {
				loadScreen1100();
			}
			
			if (isNE(mandIO.getStatuz(),varcom.endp)
			&& isEQ(wsaaClnt,wsaaClntTemp)) {
				scrnparams.subfileMore.set("Y");
			}
			else {
				scrnparams.subfileMore.set("N");
			}
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
		if (isNE(scrnparams.statuz,varcom.insr)) {
			goTo(GotoLabel.validateSubfile2050);
		}
		/*                                                         <D509CS>*/
		/*    Convert command key pressed to submenu action.       <D509CS>*/
		/*                                                         <D509CS>*/
		subprogrec.action.set("A");
		wsspcomn.flag.set("C");
		/*                                                         <D509CS>*/
		/*    Calling from Billing Change.                         <D509CS>*/
		/*                                                         <D509CS>*/
		if (isNE(wsspcomn.bchaction,SPACES)) {
			subprogrec.action.set(wsspcomn.bchaction);
		}
		/*                                                         <D509CS>*/
		/*    Retrieve Program Switching / Sanctions.              <D509CS>*/
		/*                                                         <D509CS>*/
		subprogrec.company.set(wsspcomn.company);
		subprogrec.progCode.set(wsaaProg);
		callProgram(Subprog.class, subprogrec.subprogRec);
		if (isNE(subprogrec.statuz,varcom.oK)) {
			scrnparams.errorCode.set(subprogrec.statuz);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
		/*                                                         <D509CS>*/
		/* Check sanction.                                         <D509CS>*/
		/*                                                         <D509CS>*/
		sanctnrec.function.set("TNCD");
		sanctnrec.userid.set(wsspcomn.userid);
		sanctnrec.company.set(wsspcomn.company);
		sanctnrec.branch.set(wsspcomn.branch);
		sanctnrec.transcd.set(subprogrec.transcd);
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isEQ(sanctnrec.statuz,varcom.bomb)) {
			syserrrec.statuz.set(sanctnrec.statuz);
			fatalError600();
		}
		if (isNE(sanctnrec.statuz,varcom.oK)) {
			scrnparams.errorCode.set(h052);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2190);
		}
		wsspcomn.sbmaction.set(subprogrec.action);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaBatckey.batcBatctrcde.set(subprogrec.transcd);
		wsspcomn.batchkey.set(wsaaBatckey);
		/*                                                         <D509CS>*/
		/*    Setup Switching in Stack.                            <D509CS>*/
		/*                                                         <D509CS>*/
		compute(wsaaSub, 0).set(add(wsspcomn.programPtr,1));
		wsspcomn.secProg[wsaaSub.toInt()].set(subprogrec.nxt1prog);
		wsaaSub.add(1);
		wsspcomn.secProg[wsaaSub.toInt()].set(subprogrec.nxt2prog);
		wsaaSub.add(1);
		wsspcomn.secProg[wsaaSub.toInt()].set(subprogrec.nxt3prog);
		wsaaSub.add(1);
		wsspcomn.secProg[wsaaSub.toInt()].set(subprogrec.nxt4prog);
		wsaaSub.add(1);
		goTo(GotoLabel.exit2190);
	}

	/**
	* <pre>
	*                                                         <D509CS>
	*    Validate fields                                      <D509CS>
	*                                                         <D509CS>
	* </pre>
	*/
protected void validateSubfile2050()
	{
		scrnparams.subfileRrn.set(1);
		scrnparams.function.set(varcom.sstrt);
		processScreen("SR57Z", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.oK)
		&& isNE(sv.select,SPACES)) {
			wsaaSelectRecord.set("Y");
		}
		else {
			wsaaSelectRecord.set("N");
		}
		while ( !(isEQ(scrnparams.statuz,varcom.endp))) {
			validateSubfile2600();
		}
		
	}

protected void checkForErrors2060()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*          (Screen validation)
	* </pre>
	*/
protected void validateSubfile2600()
	{
		try {
			readNextModifiedRecord2680();
			updateErrorIndicators2670();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	/**
	* <pre>
	*    Validate subfile fields
	* </pre>
	*/
protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		processScreen("SR57Z", sv);
		if (isNE(scrnparams.statuz,varcom.oK)
		&& isNE(scrnparams.statuz,varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz,varcom.endp)
		|| isEQ(sv.select,SPACES)) {
			goTo(GotoLabel.exit2690);
		}
		if (wsaaRecordNotSelected.isTrue()) {
			wsaaSelectRecord.set("Y");
			goTo(GotoLabel.exit2690);
		}
	}

protected void updateErrorIndicators2670()
	{
		wsspcomn.edterror.set("Y");
		sv.selectOut[varcom.pr.toInt()].set("N");
		sv.selectErr.set(h061);
		scrnparams.function.set(varcom.supd);
		processScreen("SR57Z", sv);
		if (isNE(scrnparams.statuz,varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
			updateDatabase3010();
		}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)
		|| isEQ(scrnparams.statuz,varcom.insr)) {
			mandIO.setPayrcoy(wsspcomn.fsuco);
			mandIO.setFormat(mandrec);
			mandIO.setPayrnum(wsspwindow.payrnum);
			wsspcomn.chdrCownnum.set(wsspwindow.payrnum);
			mandIO.setTransactionDate(varcom.vrcmDate);
			mandIO.setTermid(varcom.vrcmTermid);
			mandIO.setUser(varcom.vrcmUser);
			mandIO.setTransactionTime(varcom.vrcmTime);
			mandIO.setTimesUse(999);
			mandIO.setStatChangeDate(varcom.vrcmMaxDate);
			mandIO.setLastUseDate(varcom.vrcmMaxDate);
			mandIO.setEffdate(varcom.vrcmMaxDate);
			mandIO.setLastUseAmt(ZERO);
			mandIO.setMandAmt(ZERO);
			mandIO.setFunction(varcom.keeps);
			SmartFileCode.execute(appVars, mandIO);
			return ;
		}
		scrnparams.function.set(varcom.sstrt);
		processScreen("SR57Z", sv);
		while ( !(isEQ(scrnparams.statuz,varcom.endp)
		|| isNE(sv.select,SPACES))) {
			scrnparams.function.set(varcom.srdn);
			processScreen("SR57Z", sv);
		}
		
		if (isNE(scrnparams.statuz,varcom.endp)) {
			wsspwindow.bankkey.set(sv.bankkey);
			wsspwindow.bankacckey.set(sv.bankacckey);
			wsspwindow.value.set(sv.mandref);
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		if(isEQ(scrnparams.statuz, varcom.kill)){
			wsspcomn.secProg[wsspcomn.programPtr.toInt()].set(SPACES);
		}
		/*EXIT*/
	}
}
