package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:43
 * Description:
 * Copybook name: HPADKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hpadkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hpadFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData hpadKey = new FixedLengthStringData(256).isAPartOf(hpadFileKey, 0, REDEFINE);
  	public FixedLengthStringData hpadChdrcoy = new FixedLengthStringData(1).isAPartOf(hpadKey, 0);
  	public FixedLengthStringData hpadChdrnum = new FixedLengthStringData(8).isAPartOf(hpadKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(hpadKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hpadFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hpadFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}