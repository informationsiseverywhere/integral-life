package com.csc.life.newbusiness.dataaccess.model;

public class Br628DTO {
	private String tableName;
	private String company;
	private String chdrnumfrom;
	private String chdrnumto;
	private String validflag1;
	private int effdate;
	private int maxdate;
	private String yes;
	private String threadNo;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getChdrnumfrom() {
		return chdrnumfrom;
	}

	public void setChdrnumfrom(String chdrnumfrom) {
		this.chdrnumfrom = chdrnumfrom;
	}

	public String getChdrnumto() {
		return chdrnumto;
	}

	public void setChdrnumto(String chdrnumto) {
		this.chdrnumto = chdrnumto;
	}

	public String getValidflag1() {
		return validflag1;
	}

	public void setValidflag1(String validflag1) {
		this.validflag1 = validflag1;
	}

	public int getEffdate() {
		return effdate;
	}

	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	public int getMaxdate() {
		return maxdate;
	}

	public void setMaxdate(int maxdate) {
		this.maxdate = maxdate;
	}

	public String getYes() {
		return yes;
	}

	public void setYes(String yes) {
		this.yes = yes;
	}

	public String getThreadNo() {
		return threadNo;
	}

	public void setThreadNo(String threadNo) {
		this.threadNo = threadNo;
	}

}
