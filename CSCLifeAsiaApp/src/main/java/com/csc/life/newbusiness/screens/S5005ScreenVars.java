package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import java.util.HashMap;
import java.util.Map;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5005
 * @version 1.0 generated on 30/08/09 06:29
 * @author Quipoz
 */
public class S5005ScreenVars extends SmartVarModel { 

	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
//	public FixedLengthStringData dataArea = new FixedLengthStringData(715);//ICIL-160
	/*
	public FixedLengthStringData dataFields = new FixedLengthStringData(207).isAPartOf(dataArea, 0);*/
	
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	public ZonedDecimalData anbage = DD.anbage.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData bmi = DD.bmi.copyToZonedDecimal().isAPartOf(dataFields,3);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,7);
	public FixedLengthStringData clrskind = DD.clrskind.copy().isAPartOf(dataFields,15);
	public ZonedDecimalData dob = DD.dob.copyToZonedDecimal().isAPartOf(dataFields,16);
	public FixedLengthStringData dummy = DD.dummy.copy().isAPartOf(dataFields,24);
	public ZonedDecimalData height = DD.height.copyToZonedDecimal().isAPartOf(dataFields,25);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData lifesel = DD.lifesel.copy().isAPartOf(dataFields,79);
	public FixedLengthStringData occup = DD.occup.copy().isAPartOf(dataFields,89);
	public FixedLengthStringData industry = DD.occupationClass.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData optdscs = new FixedLengthStringData(60).isAPartOf(dataFields, 95);
	public FixedLengthStringData[] optdsc = FLSArrayPartOfStructure(4, 15, optdscs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(optdscs, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01 = DD.optdsc.copy().isAPartOf(filler,0);
	public FixedLengthStringData optdsc02 = DD.optdsc.copy().isAPartOf(filler,15);
	public FixedLengthStringData optdsc03 = DD.optdsc.copy().isAPartOf(filler,30);
	public FixedLengthStringData optdsc04 = DD.optdsc.copy().isAPartOf(filler,45);
	public FixedLengthStringData optinds = new FixedLengthStringData(4).isAPartOf(dataFields, 155);
	public FixedLengthStringData[] optind = FLSArrayPartOfStructure(4, 1, optinds, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(optinds, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01 = DD.optind.copy().isAPartOf(filler1,0);
	public FixedLengthStringData optind02 = DD.optind.copy().isAPartOf(filler1,1);
	public FixedLengthStringData optind03 = DD.optind.copy().isAPartOf(filler1,2);
	public FixedLengthStringData optind04 = DD.optind.copy().isAPartOf(filler1,3);
	public FixedLengthStringData pursuits = new FixedLengthStringData(12).isAPartOf(dataFields, 159);
	public FixedLengthStringData[] pursuit = FLSArrayPartOfStructure(2, 6, pursuits, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(12).isAPartOf(pursuits, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01 = DD.pursuit.copy().isAPartOf(filler2,0);
	public FixedLengthStringData pursuit02 = DD.pursuit.copy().isAPartOf(filler2,6);
	public FixedLengthStringData relation = DD.relation.copy().isAPartOf(dataFields,171);
	public FixedLengthStringData rollit = DD.rollit.copy().isAPartOf(dataFields,173);
	public FixedLengthStringData selection = DD.selection.copy().isAPartOf(dataFields,174);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(dataFields,178);
	public FixedLengthStringData smoking = DD.smoking.copy().isAPartOf(dataFields,179);
	public ZonedDecimalData weight = DD.weight.copyToZonedDecimal().isAPartOf(dataFields,181);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,190);
	public FixedLengthStringData relationwithowner = DD.relationwithowner.copy().isAPartOf(dataFields,203); //fwang3 ICIL-4
	public FixedLengthStringData occupationClass = DD.occupationClass.copy().isAPartOf(dataFields, 207);//ICIL-160
	
	//public FixedLengthStringData errorIndicators = new FixedLengthStringData(124).isAPartOf(dataArea, 207);//fwang3 ICIL-4
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	public FixedLengthStringData anbageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData bmiErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData clrskindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData dobErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData dummyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData heightErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData lifeselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData occupErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData optdscsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData[] optdscErr = FLSArrayPartOfStructure(4, 4, optdscsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(optdscsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optdsc01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData optdsc02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData optdsc03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData optdsc04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData optindsErr = new FixedLengthStringData(16).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData[] optindErr = FLSArrayPartOfStructure(4, 4, optindsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(optindsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData optind01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData optind02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData optind03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData optind04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData pursuitsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData[] pursuitErr = FLSArrayPartOfStructure(2, 4, pursuitsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(pursuitsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData pursuit01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData pursuit02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData relationErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData rollitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData selectionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData sexErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData smokingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData weightErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData relationwithownerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116); //fwang3
	public FixedLengthStringData occupationClassErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120); //ICIL-160
	public FixedLengthStringData industryErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(384).isAPartOf(dataArea,331);//fwang3
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	public FixedLengthStringData[] anbageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] bmiOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] clrskindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] dobOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] dummyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] heightOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] lifeselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] occupOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData optdscsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 144);
	public FixedLengthStringData[] optdscOut = FLSArrayPartOfStructure(4, 12, optdscsOut, 0);
	public FixedLengthStringData[][] optdscO = FLSDArrayPartOfArrayStructure(12, 1, optdscOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(48).isAPartOf(optdscsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optdsc01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] optdsc02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] optdsc03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] optdsc04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData optindsOut = new FixedLengthStringData(48).isAPartOf(outputIndicators, 192);
	public FixedLengthStringData[] optindOut = FLSArrayPartOfStructure(4, 12, optindsOut, 0);
	public FixedLengthStringData[][] optindO = FLSDArrayPartOfArrayStructure(12, 1, optindOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(48).isAPartOf(optindsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] optind01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] optind02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] optind03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] optind04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData pursuitsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 240);
	public FixedLengthStringData[] pursuitOut = FLSArrayPartOfStructure(2, 12, pursuitsOut, 0);
	public FixedLengthStringData[][] pursuitO = FLSDArrayPartOfArrayStructure(12, 1, pursuitOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(pursuitsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] pursuit01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] pursuit02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] relationOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] rollitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] selectionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] sexOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] smokingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] weightOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] relationwithownerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360); //fwang3
	public FixedLengthStringData[] occupationClassOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372); //ICIL-160
	public FixedLengthStringData[] industryOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384); 
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData dobDisp = new FixedLengthStringData(10);

	public LongData S5005screenWritten = new LongData(0);
	public LongData S5005protectWritten = new LongData(0);
	private Map<String,Map<String, String>> industryOccuMap = new HashMap<>();
	public int uwFlag = 0;
	
	public static int[] screenSflPfInds = new int[] {1, 2, 3, 4, 5, 6, 15, 16, 17, 18, 20, 21, 22, 23, 24,38, 60, 61, 62}; 
	
	public boolean hasSubfile() {
		return false;
	}


	public Map<String, Map<String, String>> getIndustryOccuMap() {
		return industryOccuMap;
	}


	public void setIndustryOccuMap(Map<String, Map<String, String>> industryOccuMap) {
		this.industryOccuMap = industryOccuMap;
	}


	public S5005ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(jlifeOut,new String[] {null, null, null, "51",null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeselOut,new String[] {"01","13","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(sexOut,new String[] {"02","13","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dobOut,new String[] {"03","13","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dummyOut,new String[] {null, null, null, "54",null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {null, null, "14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(anbageOut,new String[] {"05","13","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectionOut,new String[] {"06","13","-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(clrskindOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(smokingOut,new String[] {"08","13","-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(occupOut,new String[] {"09","13","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pursuit01Out,new String[] {"10","13","-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pursuit02Out,new String[] {"11","13","-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(relationOut,new String[] {"12","52","-12","52",null, null, null, null, null, null, null, null});
		fieldIndMap.put(rollitOut,new String[] {null, null, null, "53",null, null, null, null, null, null, null, null});
		fieldIndMap.put(heightOut,new String[] {"21","22","-21","25",null, null, null, null, null, null, null, null});
		fieldIndMap.put(weightOut,new String[] {"24","22","-24","25",null, null, null, null, null, null, null, null});
		fieldIndMap.put(bmiOut,new String[] {null, null, null, "23",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc02Out,new String[] {null, null, null, "15",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind02Out,new String[] {"27","15","-27","15",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optdsc03Out,new String[] {null, null, null, "17",null, null, null, null, null, null, null, null});
		fieldIndMap.put(optind03Out,new String[] {"29","17","-27","17",null, null, null, null, null, null, null, null});
		fieldIndMap.put(relationwithownerOut,new String[] {"30","31","-30","32",null, null, null, null, null, null, null, null});//fwang3
		fieldIndMap.put(optind04Out,new String[] {null, null, null, "55",null, null, null, null, null, null, null, null});
		fieldIndMap.put(occupationClassOut,new String[] {"60", "61", "-60", "62",null, null, null, null, null, null, null, null});
		fieldIndMap.put(industryOut,new String[] {"38", "13", "-38", null,null, null, null, null, null, null, null, null});
		/*screenFields = new BaseData[] {chdrnum, life, jlife, lifesel, lifename, sex, dob, dummy, zagelit, anbage, selection, clrskind, smoking, occup, pursuit01, pursuit02, relation, rollit, optdsc01, optind01, height, weight, bmi, optdsc02, optind02, optdsc03, optind03,relationwithowner, optdsc04, optind04,occupationClass};
		screenOutFields = new BaseData[][] {chdrnumOut, lifeOut, jlifeOut, lifeselOut, lifenameOut, sexOut, dobOut, dummyOut, zagelitOut, anbageOut, selectionOut, clrskindOut, smokingOut, occupOut, pursuit01Out, pursuit02Out, relationOut, rollitOut, optdsc01Out, optind01Out, heightOut, weightOut, bmiOut, optdsc02Out, optind02Out, optdsc03Out, optind03Out,relationwithownerOut, optdsc04Out, optind04Out, occupationClassOut};
		screenErrFields = new BaseData[] {chdrnumErr, lifeErr, jlifeErr, lifeselErr, lifenameErr, sexErr, dobErr, dummyErr, zagelitErr, anbageErr, selectionErr, clrskindErr, smokingErr, occupErr, pursuit01Err, pursuit02Err, relationErr, rollitErr, optdsc01Err, optind01Err, heightErr, weightErr, bmiErr, optdsc02Err, optind02Err, optdsc03Err, optind03Err,relationwithownerErr, optdsc04Err, optind04Err, occupationClassErr};
		screenDateFields = new BaseData[] {dob};
		screenDateErrFields = new BaseData[] {dobErr};
		screenDateDispFields = new BaseData[] {dobDisp};*/

		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5005screen.class;
		protectRecord = S5005protect.class;
	}

	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	
	
	public int getDataAreaSize() {
		return 733;
	}
	

	public int getDataFieldsSize(){
		return 209;  
	}
	public int getErrorIndicatorSize(){
		return 128; 
	}
	
	public int getOutputFieldSize(){
		return 396; 
	}


	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, life, jlife, lifesel, lifename, sex, dob, dummy, zagelit, anbage, selection, clrskind, smoking, occup,industry, pursuit01, pursuit02, relation, rollit, optdsc01, optind01, height, weight, bmi, optdsc02, optind02, optdsc03, optind03,relationwithowner, optdsc04, optind04,occupationClass};   //IFSU-2036
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {chdrnumOut, lifeOut, jlifeOut, lifeselOut, lifenameOut, sexOut, dobOut, dummyOut, zagelitOut, anbageOut, selectionOut, clrskindOut, smokingOut, occupOut,industryOut, pursuit01Out, pursuit02Out, relationOut, rollitOut, optdsc01Out, optind01Out, heightOut, weightOut, bmiOut, optdsc02Out, optind02Out, optdsc03Out, optind03Out,relationwithownerOut, optdsc04Out, optind04Out, occupationClassOut}; //IFSU-2036
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {chdrnumErr, lifeErr, jlifeErr, lifeselErr, lifenameErr, sexErr, dobErr, dummyErr, zagelitErr, anbageErr, selectionErr, clrskindErr, smokingErr, occupErr, industryErr,pursuit01Err, pursuit02Err, relationErr, rollitErr, optdsc01Err, optind01Err, heightErr, weightErr, bmiErr, optdsc02Err, optind02Err, optdsc03Err, optind03Err,relationwithownerErr, optdsc04Err, optind04Err, occupationClassErr};  //IFSU-2036
		
	}
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {dob};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {dobDisp};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {dobErr};
	}
}
