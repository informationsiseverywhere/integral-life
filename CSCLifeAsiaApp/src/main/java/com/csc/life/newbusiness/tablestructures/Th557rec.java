package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:24
 * Description:
 * Copybook name: TH557REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th557rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th557Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zflpxtras = new FixedLengthStringData(180).isAPartOf(th557Rec, 0);
  	public FixedLengthStringData[] zflpxtra = FLSArrayPartOfStructure(3, 60, zflpxtras, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(180).isAPartOf(zflpxtras, 0, FILLER_REDEFINE);
  	public FixedLengthStringData zflpxtra01 = new FixedLengthStringData(60).isAPartOf(filler, 0);
  	public FixedLengthStringData zflpxtra02 = new FixedLengthStringData(60).isAPartOf(filler, 60);
  	public FixedLengthStringData zflpxtra03 = new FixedLengthStringData(60).isAPartOf(filler, 120);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(320).isAPartOf(th557Rec, 180, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th557Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th557Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}