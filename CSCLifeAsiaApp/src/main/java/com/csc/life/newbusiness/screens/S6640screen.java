package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6640screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 15, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6640ScreenVars sv = (S6640ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6640screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6640ScreenVars screenVars = (S6640ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.reserveMeth.setClassString("");
		screenVars.lcsubr.setClassString("");
		screenVars.revBonusMeth.setClassString("");
		screenVars.lcrider.setClassString("");
		screenVars.zbondivalc.setClassString("");
		screenVars.surrenderBonusMethod.setClassString("");
		screenVars.zcshdivmth.setClassString("");
		screenVars.zdivwdrmth.setClassString("");
		screenVars.zrmandind.setClassString("");
	}

/**
 * Clear all the variables in S6640screen
 */
	public static void clear(VarModel pv) {
		S6640ScreenVars screenVars = (S6640ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.reserveMeth.clear();
		screenVars.lcsubr.clear();
		screenVars.revBonusMeth.clear();
		screenVars.lcrider.clear();
		screenVars.zbondivalc.clear();
		screenVars.surrenderBonusMethod.clear();
		screenVars.zcshdivmth.clear();
		screenVars.zdivwdrmth.clear();
		screenVars.zrmandind.clear();
	}
}
