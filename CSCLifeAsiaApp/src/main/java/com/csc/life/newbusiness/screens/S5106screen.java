package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5106screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5106ScreenVars sv = (S5106ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5106screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5106ScreenVars screenVars = (S5106ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.ddind.setClassString("");
		screenVars.crcind.setClassString("");
		screenVars.grpind.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.occdateDisp.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.register.setClassString("");
		screenVars.cntbranch.setClassString("");
		screenVars.srcebus.setClassString("");
		screenVars.dlvrmode.setClassString("");
		screenVars.agntsel.setClassString("");
		screenVars.agntname.setClassString("");
		screenVars.ownersel.setClassString("");
		screenVars.ownername.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.billcdDisp.setClassString("");
		screenVars.lifesel.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.anbage.setClassString("");
		screenVars.smoking.setClassString("");
		screenVars.mortcls.setClassString("");
		screenVars.unitpkg.setClassString("");
		screenVars.prmamt.setClassString("");
		screenVars.adjind.setClassString("");
		screenVars.apcind.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.taxamt.setClassString("");
	}

/**
 * Clear all the variables in S5106screen
 */
	public static void clear(VarModel pv) {
		S5106ScreenVars screenVars = (S5106ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.ddind.clear();
		screenVars.crcind.clear();
		screenVars.grpind.clear();
		screenVars.descrip.clear();
		screenVars.occdateDisp.clear();
		screenVars.occdate.clear();
		screenVars.chdrnum.clear();
		screenVars.register.clear();
		screenVars.cntbranch.clear();
		screenVars.srcebus.clear();
		screenVars.dlvrmode.clear();
		screenVars.agntsel.clear();
		screenVars.agntname.clear();
		screenVars.ownersel.clear();
		screenVars.ownername.clear();
		screenVars.mop.clear();
		screenVars.billfreq.clear();
		screenVars.billcdDisp.clear();
		screenVars.billcd.clear();
		screenVars.lifesel.clear();
		screenVars.lifename.clear();
		screenVars.anbage.clear();
		screenVars.smoking.clear();
		screenVars.mortcls.clear();
		screenVars.unitpkg.clear();
		screenVars.prmamt.clear();
		screenVars.adjind.clear();
		screenVars.apcind.clear();
		screenVars.zagelit.clear();
		screenVars.taxamt.clear();
	}
}
