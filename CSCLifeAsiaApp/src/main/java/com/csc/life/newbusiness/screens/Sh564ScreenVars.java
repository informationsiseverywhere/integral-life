package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH564
 * @version 1.0 generated on 30/08/09 07:04
 * @author Quipoz
 */
public class Sh564ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(280);
	public FixedLengthStringData dataFields = new FixedLengthStringData(56).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctmonth = DD.acctmonth.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acctyear = DD.acctyear.copyToZonedDecimal().isAPartOf(dataFields,2);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData bbranch = DD.bbranch.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData bcompany = DD.bcompany.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData branch = DD.branch.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData scheduleName = DD.bschednam.copy().isAPartOf(dataFields,14);
	public ZonedDecimalData scheduleNumber = DD.bschednum.copyToZonedDecimal().isAPartOf(dataFields,24);
	public FixedLengthStringData chdrtype = DD.chdrtype.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData ctrlbrks = new FixedLengthStringData(3).isAPartOf(dataFields, 35);
	public FixedLengthStringData[] ctrlbrk = FLSArrayPartOfStructure(3, 1, ctrlbrks, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(ctrlbrks, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctrlbrk01 = DD.ctrlbrk.copy().isAPartOf(filler,0);
	public FixedLengthStringData ctrlbrk02 = DD.ctrlbrk.copy().isAPartOf(filler,1);
	public FixedLengthStringData ctrlbrk03 = DD.ctrlbrk.copy().isAPartOf(filler,2);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,38);
	public FixedLengthStringData jobq = DD.jobq.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 56);
	public FixedLengthStringData acctmonthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acctyearErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bcompanyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData branchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bschednamErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData bschednumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData chdrtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData ctrlbrksErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] ctrlbrkErr = FLSArrayPartOfStructure(3, 4, ctrlbrksErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(ctrlbrksErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ctrlbrk01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData ctrlbrk02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData ctrlbrk03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jobqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 112);
	public FixedLengthStringData[] acctmonthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acctyearOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bcompanyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] branchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bschednamOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] bschednumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData ctrlbrksOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] ctrlbrkOut = FLSArrayPartOfStructure(3, 12, ctrlbrksOut, 0);
	public FixedLengthStringData[][] ctrlbrkO = FLSDArrayPartOfArrayStructure(12, 1, ctrlbrkOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(ctrlbrksOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ctrlbrk01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] ctrlbrk02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] ctrlbrk03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jobqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData Sh564screenWritten = new LongData(0);
	public LongData Sh564protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh564ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ctrlbrk01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctrlbrk02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ctrlbrk03Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {scheduleName, acctmonth, scheduleNumber, acctyear, effdate, bcompany, jobq, bbranch, branch, ctrlbrk01, aracde, ctrlbrk02, chdrtype, ctrlbrk03};
		screenOutFields = new BaseData[][] {bschednamOut, acctmonthOut, bschednumOut, acctyearOut, effdateOut, bcompanyOut, jobqOut, bbranchOut, branchOut, ctrlbrk01Out, aracdeOut, ctrlbrk02Out, chdrtypeOut, ctrlbrk03Out};
		screenErrFields = new BaseData[] {bschednamErr, acctmonthErr, bschednumErr, acctyearErr, effdateErr, bcompanyErr, jobqErr, bbranchErr, branchErr, ctrlbrk01Err, aracdeErr, ctrlbrk02Err, chdrtypeErr, ctrlbrk03Err};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh564screen.class;
		protectRecord = Sh564protect.class;
	}

}
