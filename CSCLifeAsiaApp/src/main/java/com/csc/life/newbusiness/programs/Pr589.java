/*
 * File: Pr589.java
 * Date: 30 August 2009 1:48:09
 * Author: Quipoz Limited
 * 
 * Class transformed from PR589.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.contractservicing.dataaccess.FupeTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.FlupenqTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.HxclTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.screens.Sr589ScreenVars;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Optswchrec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Optswch;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart.recordstructures.Varcom;


/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
* Initialise
* ----------
*
* Retrieve the contract header information from the CHDRENQ I/O
* module.
* Read the clients name.
*
* Do the following :
*    - If ROLU -> * First page = 'N'
*                 * Start with the last processed follow-up
*    - If ROLD -> * Read the first follow-up of current screen
*                 * Read back SUPFILE-PAGE + 1 times
*                 * Check if first page
*                 * Start with current follow-up
*    - ELSE       * First page = 'Y'
*                 * Start with first follow-up
*
* Fill the subfile page with follow-up records for the contract.
* If the page is not full -> add blank lines to the subfile in
* order to display a full page.
*
* Set SCRN-SUBFILE-MORE to Y or N to display a '+' if there are
* more follow-ups to show.
*
* Retrieve todays date (DATCON1) and store for later use.
*
*
* Validation
* ----------
*
* Validate the changed fields in the subfile page
*
* If ROLD and First page
*    - Error ## roldown not allowed ##
* If ROLU
*    - If any new blank subfile lines
*         - Error ## rolup not allowed ##
*
* Update
* ------
*
* Delete/write/rewrite follow-ups
*
* Store the first follow-up key of current screen ## for ROLU ##
*
* Add Validation to  presence of follow up letter type for
* printing of either standard follow up letter or a reminder
*
* Add the option of supplying Doctor details when entering
* Follow Ups.
*
* When a Doctor is required, output a warning message, but
* enable user to exit without aborting.
*****************************************************************
* </pre>
*/
public class Pr589 extends ScreenProgCS {
	private StringUtil stringUtil = new StringUtil();
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR589");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private FixedLengthStringData wsaaSubfileMore = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).setUnsigned();
	private FixedLengthStringData wsaaDateYyyymmdd = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaDateYymmdd = new FixedLengthStringData(6).isAPartOf(wsaaDate, 0, REDEFINE);
	private ZonedDecimalData wsaaDateYy = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateYymmdd, 0).setUnsigned();
	private ZonedDecimalData wsaaDateMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateYymmdd, 2).setUnsigned();
	private ZonedDecimalData wsaaDateDd = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateYymmdd, 4).setUnsigned();

	private FixedLengthStringData wsaaDateDdmmyy = new FixedLengthStringData(6);
	private ZonedDecimalData wsaaDateDd1 = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateDdmmyy, 0).setUnsigned();
	private ZonedDecimalData wsaaDateMm1 = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateDdmmyy, 2).setUnsigned();
	private ZonedDecimalData wsaaDateYy1 = new ZonedDecimalData(2, 0).isAPartOf(wsaaDateDdmmyy, 4).setUnsigned();
	private ZonedDecimalData wsaaRrn = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData wsaaOptionCode = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1);
	private Validator foundSelection = new Validator(wsaaFoundSelection, "Y");

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/* TABLES */
	private static final String t5688 = "T5688";
	private static final String t5661 = "T5661";
	private static final String t5660 = "T5660";
	private static final String flupenqrec = "FLUPENQREC";
	private static final String hpadrec = "HPADREC";
	private static final String hxclrec = "HXCLREC";
	private static final String fuperec = "FUPEREC";
	// ILIFE-3396 PERFORMANCE BY OPAL
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FlupenqTableDAM flupenqIO = new FlupenqTableDAM();
	private FupeTableDAM fupeIO = new FupeTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private HxclTableDAM hxclIO = new HxclTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private T5661rec t5661rec = new T5661rec();
	private Optswchrec optswchrec = new Optswchrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sr589ScreenVars sv = ScreenProgram.getScreenVars( Sr589ScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private WsaaUserFieldsInner wsaaUserFieldsInner = new WsaaUserFieldsInner();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1390, 
		continue2620, 
		updateErrorIndicators2670, 
		switch4170
	}

	public Pr589() {
		super();
		screenVars = sv;
		new ScreenModel("Sr589", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}

	// -- MLIL 271 -- //
	protected void largename() {
		/* LGNM-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/* LGNM-EXIT */
	}

	// -- MLIL 271 -- //
	protected void plainname() {
		/* PLAIN-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();

		} else if (isNE(cltsIO.getGivname(), SPACES)) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = ",";

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			wsspcomn.longconfname.set(fullName);

		} else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/* PLAIN-EXIT */
	}

	// -- MLIL 271 -- //
	protected void payeename() {
		/* PAYEE-100 */
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
		} else if (isEQ(cltsIO.getEthorig(), "1")) {
			String firstName = cltsIO.getGivname().toString();
			String lastName = cltsIO.getSurname().toString();
			String delimiter = "";
			String salute = cltsIO.getSalutl().toString();

			String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
			String fullNameWithSalute = stringUtil.includeSalute(salute, fullName);

			wsspcomn.longconfname.set(fullNameWithSalute);
		} else {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable2.addExpression(". ");
			stringVariable2.addExpression(cltsIO.getGivname(), "  ");
			stringVariable2.addExpression(" ");
			stringVariable2.addExpression(cltsIO.getSurname(), "  ");
			stringVariable2.setStringInto(wsspcomn.longconfname);
		}
		/* PAYEE-EXIT */
	}
	// -- MLIL 271 -- //
	protected void corpname() {
		/* PAYEE-1001 */
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME DELIMITED SIZE */
		/* CLTS-GIVNAME DELIMITED ' ' */
		String firstName = cltsIO.getLgivname().toString();
		String lastName = cltsIO.getLsurname().toString();
		String delimiter = "";

		// this way we can override StringUtil behaviour in formatName()
		String fullName = getStringUtil().plainName(firstName, lastName, delimiter);
		wsspcomn.longconfname.set(fullName);
		/* CORP-EXIT */
	}
	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaFoundSelection.set("N");
		/* Dummy subfile initalisation for prototype - relpace with SCLR*/
		scrnparams.function.set(varcom.sclr);
		processScreen("SR589", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		/* Dummy field initilisation for prototype version.*/
		// ILIFE-3396 PERFORMANCE BY OPAL
		chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		//ILIFE-3805 Starts
		if(null==chdrpf){
			chdrenqIO.setFunction(Varcom.retrv);
			chdrenqIO.setFormat(chdrenqrec);
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), Varcom.oK)) {
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
			else {
				chdrpf = chdrpfDAO.getChdrpf(chdrenqIO.getChdrcoy().toString(), chdrenqIO.getChdrnum().toString());
				if(null==chdrpf) {
					fatalError600();
				}
				else {
					chdrpfDAO.setCacheObject(chdrpf);     
				}
				
				
			}
			
		}
			//ILIFE-3805 Ends
		/* Read the contract definition description from table*/
		/* T5688 for the contract held on the client header record.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(chdrpf.getCnttype());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrpf.getChdrnum());
		sv.cnttype.set(chdrpf.getCnttype());
		sv.cownnum.set(chdrpf.getCownnum());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/*       MOVE ALL '?'             TO SR589-FUPREMARK               */
			sv.ctypedes.fill("?");		// ILIFE-3002 Modified by bkonduru2
		}
		else {
			/*       MOVE DESC-LONGDESC       TO SR589-FUPREMARK.              */
			sv.ctypedes.set(descIO.getLongdesc());			// ILIFE-3002 Modified by bkonduru2
		}
		/* Look up the contract details of the client owner (CLTS)*/
		/* and format the name as a CONFIRMATION NAME.*/
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrpf.getCownnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		/* Get the confirmation name.*/
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.ownernameErr.set(errorsInner.e040);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
		/* Retrieve Doctor details, if present on HPAD file.*/
		hpadIO.setDataArea(SPACES);
		hpadIO.setChdrcoy(chdrpf.getChdrcoy());
		hpadIO.setChdrnum(chdrpf.getChdrnum());
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		/* Get the Doctor confirmation name.*/
		if (isNE(hpadIO.getZdoctor(), SPACES)) {
			sv.zdoctor.set(hpadIO.getZdoctor());
			cltsIO.setClntpfx("CN");
			cltsIO.setClntcoy(wsspcomn.fsuco);
			cltsIO.setClntnum(hpadIO.getZdoctor());
			cltsIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, cltsIO);
			if (isNE(cltsIO.getStatuz(), varcom.oK)
			&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(cltsIO.getParams());
				syserrrec.statuz.set(cltsIO.getStatuz());
				fatalError600();
			}
			if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
				sv.zdoctorErr.set(errorsInner.e040);
			}
			else {
				namadrsrec.namadrsRec.set(SPACES);
				namadrsrec.clntkey.set(cltsIO.getDataKey());
				namadrsrec.language.set(wsspcomn.language);
				namadrsrec.function.set(wsaaUserFieldsInner.wsaaPayeeName);
				callProgram(Namadrs.class, namadrsrec.namadrsRec);
				if (isNE(namadrsrec.statuz, varcom.oK)) {
					syserrrec.params.set(cltsIO.getDataKey());
					syserrrec.statuz.set(namadrsrec.statuz);
					fatalError600();
				}
				sv.zdocname.set(namadrsrec.name);
			}
		}
		/* Set follow-up record to the start position*/
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
			flupenqIO.setRecKeyData(wsaaUserFieldsInner.wsaaFirstFlupKey);
		}
		else {
			if (isNE(wsaaUserFieldsInner.wsaaRoluRold, varcom.rolu)) {
				flupenqIO.setParams(SPACES);
				flupenqIO.setChdrcoy(chdrpf.getChdrcoy());
				flupenqIO.setChdrnum(chdrpf.getChdrnum());
				flupenqIO.setFuptype(SPACES);
				flupenqIO.setTransactionDate(ZERO);
				flupenqIO.setTranno(ZERO);
				flupenqIO.setFupno(ZERO);
			}
		}
		wsaaUserFieldsInner.wsaaFirstPage.set("Y");
		flupenqIO.setFunction(varcom.begn);
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rolu)) {
			flupenqIO.setFormat(flupenqrec);
			SmartFileCode.execute(appVars, flupenqIO);
			flupenqIO.setFunction(varcom.nextr);
			wsaaUserFieldsInner.wsaaFirstPage.set("N");
		}
		flupenqIO.setFormat(flupenqrec);
		SmartFileCode.execute(appVars, flupenqIO);
		/* Check for I/O error. Note that the ENDP condition is tested*/
		/* for later.*/
		if (isNE(flupenqIO.getStatuz(), varcom.oK)
		&& isNE(flupenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(flupenqIO.getParams());
			fatalError600();
		}
		/* Go one page back.*/
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
			resetTopRecord1100();
		}
		/* Load subfile, starting with current follow-up*/
		wsaaUserFieldsInner.wsaaCount.set(ZERO);
		wsaaSubfileMore.set(SPACES);
		while ( !(isEQ(wsaaUserFieldsInner.wsaaCount, sv.subfilePage)
		|| isEQ(wsaaSubfileMore, "N"))) {
			loadSubfile1300();
		}
		
		if (isEQ(flupenqIO.getStatuz(), varcom.endp)
				|| isNE(flupenqIO.getChdrcoy(), chdrpf.getChdrcoy())
				|| isNE(flupenqIO.getChdrnum(), chdrpf.getChdrnum())) {
					scrnparams.subfileMore.set("N");
				}
		else {
			scrnparams.subfileMore.set("Y");
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Reset WSAA-ROLU-ROLD here because if F3, F15, F16,F17 or F18 is*/
		/* pressed in the subfile-screen WSAA-ROLU-ROLD is not set to the*/
		/* SCRN-STATUZ.*/
		wsaaUserFieldsInner.wsaaRoluRold.set(varcom.oK);
		/* Intialise Call to OPTSWCH.*/
		optswchrec.optsFunction.set("INIT");
		optswchrec.optsCallingProg.set(wsaaProg);
		optswchrec.optsDteeff.set(ZERO);
		optswchrec.optsCompany.set(wsspcomn.company);
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsLanguage.set(wsspcomn.language);
		varcom.vrcmTranid.set(wsspcomn.tranid);
		optswchrec.optsUser.set(varcom.vrcmUser);
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
//	}
	}

protected void resetTopRecord1100()
	{
		resetTop1110();
	}

protected void resetTop1110()
	{
		/* If ENDP -> start with last follow-up for contract*/
		/* ELSE    -> read-prev from current follow-up.*/
		if (isEQ(flupenqIO.getStatuz(), varcom.endp)) {
			flupenqIO.setFunction(varcom.endr);
			flupenqIO.setStatuz(varcom.oK);
		}
		else {
			flupenqIO.setFunction(varcom.nextp);
		}
		/* Read back #records in one page + 1.*/
		for (wsaaUserFieldsInner.wsaaCount.set(1); !((setPrecision(wsaaUserFieldsInner.wsaaCount, 0)
		&& isGT(wsaaUserFieldsInner.wsaaCount, add(sv.subfilePage, 1)))
		|| isEQ(flupenqIO.getStatuz(), varcom.endp)); wsaaUserFieldsInner.wsaaCount.add(1)){
			readPrevRecord1200();
		}
		/* See if this is the first page and reset the top record for this*/
		/* page.*/
		if (isEQ(flupenqIO.getStatuz(), varcom.endp)
		|| isNE(flupenqIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(flupenqIO.getChdrnum(), chdrpf.getChdrnum())) {
			wsaaUserFieldsInner.wsaaFirstPage.set("Y");
			flupenqIO.setChdrcoy(chdrpf.getChdrcoy());
			flupenqIO.setChdrnum(chdrpf.getChdrnum());
			flupenqIO.setFuptype(SPACES);
			flupenqIO.setTransactionDate(ZERO);
			flupenqIO.setTranno(ZERO);
			flupenqIO.setFupno(ZERO);
			flupenqIO.setFunction(varcom.begn);
			flupenqIO.setFormat(flupenqrec);
			SmartFileCode.execute(appVars, flupenqIO);
			/*   Check for I/O error. Note that the ENDP*/
			/*   condition is tested for later.*/
			if (isNE(flupenqIO.getStatuz(), varcom.oK)
			&& isNE(flupenqIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(flupenqIO.getParams());
				fatalError600();
			}
		}
		else {
			if ((setPrecision(wsaaUserFieldsInner.wsaaCount, 0)
			&& isGT(wsaaUserFieldsInner.wsaaCount, add(sv.subfilePage, 1)))) {
				wsaaUserFieldsInner.wsaaFirstPage.set("N");
				flupenqIO.setFunction(varcom.nextr);
				flupenqIO.setFormat(flupenqrec);
				SmartFileCode.execute(appVars, flupenqIO);
				/*     Check for I/O error. Note that the ENDP*/
				/*     condition is tested for later.*/
				if (isNE(flupenqIO.getStatuz(), varcom.oK)
				&& isNE(flupenqIO.getStatuz(), varcom.endp)) {
					syserrrec.params.set(flupenqIO.getParams());
					fatalError600();
				}
			}
		}
	}

protected void readPrevRecord1200()
	{
		/*READ-PREV*/
		flupenqIO.setFormat(flupenqrec);
		SmartFileCode.execute(appVars, flupenqIO);
		/* Check for I/O error. Note that the ENDP condition is tested*/
		/* for later.*/
		if (isNE(flupenqIO.getStatuz(), varcom.oK)
		&& isNE(flupenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(flupenqIO.getParams());
			fatalError600();
		}
		flupenqIO.setFunction(varcom.nextp);
		/*EXIT*/
	}

protected void loadSubfile1300()
	{
		try {
			load1310();
			addSubfileRecord1320();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void load1310()
	{
		wsaaUserFieldsInner.wsaaCount.add(1);
		/* Record belong to this contract or ENDP ? If not, empty line*/
		/* on in the subfile page.*/
		sv.crtdate.set(varcom.vrcmMaxDate);
		if (isNE(flupenqIO.getChdrcoy(), chdrpf.getChdrcoy())
		|| isNE(flupenqIO.getChdrnum(), chdrpf.getChdrnum())
		|| isEQ(flupenqIO.getStatuz(), varcom.endp)) {
			sv.subfileFields.set(SPACES);
			sv.date_var.set(SPACES);
			sv.lifeno.set(ZERO);
			sv.tranno.set(ZERO);
			sv.transactionDate.set(ZERO);
			sv.jlife.set(SPACES);
			wsaaUserFieldsInner.wsaaFupno.add(1);
			sv.fupno.set(wsaaUserFieldsInner.wsaaFupno);
			sv.selectOut[varcom.nd.toInt()].set("Y");
			wsaaSubfileMore.set("N");
			sv.ind.set(SPACES);
			sv.crtuser.set(wsspcomn.userid);
			sv.crtdate.set(varcom.vrcmMaxDate);
			sv.fuprcvd.set(99999999);
			sv.exprdate.set(99999999);
			goTo(GotoLabel.exit1390);
		}
		/*       GO TO 1320-ADD-SUBFILE-RECORD.*/
		/* Record belongs to this contract, put details in the current*/
		/* line of the subfile page.*/
		sv.fupcode.set(flupenqIO.getFupcode());
		sv.fuptype.set(flupenqIO.getFuptype());
		/* MOVE FLUPENQ-TRANSACTION-DATE                                */
		/*                             TO WSAA-DATE.                    */
		/* PERFORM 1400-CONVERT-TRAN-DATE.                              */
		/* MOVE WSAA-DATE-DDMMYYYY     TO SR589-DATE.                   */
		/* MOVE WSAA-DATE-YYYYMMDD     TO SR589-DATE.           <LA4592>*/
		datcon1rec.intDate.set(flupenqIO.getFupremdt());
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		sv.date_var.set(datcon1rec.extDate);
		sv.fupno.set(flupenqIO.getFupno());
		wsaaUserFieldsInner.wsaaFupno.set(flupenqIO.getFupno());
		sv.lifeno.set(flupenqIO.getLife());
		sv.tranno.set(flupenqIO.getTranno());
		sv.transactionDate.set(flupenqIO.getTransactionDate());
		sv.jlife.set(flupenqIO.getJlife());
		/*    MOVE FLUPENQ-FUPREMK        TO SR589-FUPREMARK.              */
		sv.fupremk.set(flupenqIO.getFupremk());
		sv.fuprcvd.set(flupenqIO.getFuprcvd());
		sv.exprdate.set(flupenqIO.getExprdate());
		sv.fupstat.set(flupenqIO.getFupstat());
		sv.uprflag.set("N");
		readFupe1650();
		if (isEQ(fupeIO.getStatuz(), varcom.oK)) {
			sv.ind.set("+");
		}
		else {
			sv.ind.set(SPACES);
		}
		sv.crtuser.set(flupenqIO.getCrtuser());
		if (isNE(flupenqIO.getCrtdate(), NUMERIC)) {
			flupenqIO.setCrtdate(varcom.vrcmMaxDate);
		}
		sv.crtdate.set(flupenqIO.getCrtdate());
		checkHxcl1500();
		flupenqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, flupenqIO);
		/* Check for I/O error. Note that the ENDP condition is tested*/
		/* for later.*/
		if (isNE(flupenqIO.getStatuz(), varcom.oK)
		&& isNE(flupenqIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(flupenqIO.getParams());
			fatalError600();
		}
	}

protected void addSubfileRecord1320()
	{
		if (isEQ(wsspcomn.flag, "I")|| isEQ(wsspcomn.flag, "C")) //MIBT-233
			{
			sv.fupcdeOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		processScreen("SR589", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void convertTranDate1400()
	{
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.edit);
		/* MOVE WSAA-DATE-DDMMYY       TO DTC1-EXT-DATE.                */
		datcon1rec.extDate.set(wsaaDateYymmdd);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* MOVE DTC1-EXT-DATE          TO WSAA-DATE-DDMMYYYY.           */
		/*EXIT*/
	}

protected void checkHxcl1500()
	{
		hxcl1510();
	}

protected void hxcl1510()
	{
		/*     MOVE SPACES                TO SR589-IND.            <V72L11>*/
		hxclIO.setParams(SPACES);
		hxclIO.setChdrcoy(flupenqIO.getChdrcoy());
		hxclIO.setChdrnum(flupenqIO.getChdrnum());
		hxclIO.setFupno(flupenqIO.getFupno());
		hxclIO.setHxclseqno(ZERO);
		hxclIO.setFormat(hxclrec);
		hxclIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		hxclIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hxclIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "FUPNO");
		SmartFileCode.execute(appVars, hxclIO);
		if (isNE(hxclIO.getStatuz(), varcom.oK)
		&& isNE(hxclIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(hxclIO.getParams());
			fatalError600();
		}
		if (isEQ(hxclIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(hxclIO.getChdrcoy(), flupenqIO.getChdrcoy())
		|| isNE(hxclIO.getChdrnum(), flupenqIO.getChdrnum())
		|| isNE(hxclIO.getFupno(), flupenqIO.getFupno())) {
			return ;
		}
		sv.ind.set("+");
	}

	/**
	* <pre>
	* Read FUPE to determine whether any record exists                
	* </pre>
	*/
protected void readFupe1650()
	{
		readFupe1651();
	}

protected void readFupe1651()
	{
		fupeIO.setParams(SPACES);
		fupeIO.setChdrcoy(flupenqIO.getChdrcoy());
		fupeIO.setChdrnum(flupenqIO.getChdrnum());
		fupeIO.setFupno(flupenqIO.getFupno());
		fupeIO.setClamnum(SPACES);
		fupeIO.setTranno(flupenqIO.getTranno());
		fupeIO.setDocseq(ZERO);
		fupeIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fupeIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fupeIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "FUPNO", "TRANNO");
		fupeIO.setFormat(fuperec);
		SmartFileCode.execute(appVars, fupeIO);
		if (isNE(fupeIO.getStatuz(), varcom.oK)
		&& isNE(fupeIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(fupeIO.getStatuz());
			syserrrec.params.set(fupeIO.getParams());
			fatalError600();
		}
		if (isNE(fupeIO.getChdrcoy(), flupenqIO.getChdrcoy())
		|| isNE(fupeIO.getChdrnum(), flupenqIO.getChdrnum())
		|| isNE(fupeIO.getFupno(), flupenqIO.getFupno())
		|| isNE(fupeIO.getClamnum(), flupenqIO.getClamnum())
		|| isNE(fupeIO.getTranno(), flupenqIO.getTranno())) {
			fupeIO.setStatuz(varcom.endp);
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			return ;
		}
		if (isEQ(wsspcomn.flag, "I")) {
			sv.zdoctorOut[varcom.pr.toInt()].set("Y");
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		screenIo2010();
		checkStatuz2070();
	}

protected void screenIo2010()
	{
		/*    CALL 'SR589IO'              USING SCRN-SCREEN-PARAMS*/
		/*                                SR589-DATA-AREA*/
		/*                                SR589-SUBFILE-AREA.*/
		/* Screen errors are now handled in the calling program.*/
		/*    PERFORM 200-SCREEN-ERRORS.*/
		wsspcomn.edterror.set(varcom.oK);
		wsaaUserFieldsInner.wsaaRoluRold.set(scrnparams.statuz);
		/*VALIDATE-SCREEN*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			/* OR WSSP-FLAG                = 'I'*/
			return ;
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*VALIDATE-SUBFILE*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
	}

protected void checkStatuz2070()
	{
		/*  Do not allow any action on the screen if an error was found*/
		/*  in the above VALIDATE section.*/
		if (isNE(wsspcomn.edterror, "Y")) {
			if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rolu)
			|| isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
				checkRoluRold2100();
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*               (Screen validation)
	* </pre>
	*/
protected void checkRoluRold2100()
	{
		check2110();
		checkForErrors2180();
	}

protected void check2110()
	{
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
			if (isEQ(wsaaUserFieldsInner.wsaaFirstPage, "Y")) {
				scrnparams.errorCode.set(errorsInner.f498);
			}
			return ;
		}
		/* If this is a ROLU.*/
		if (isEQ(wsspcomn.flag, "I")) {
			if (isEQ(scrnparams.subfileMore, "N")) {
				scrnparams.errorCode.set(errorsInner.f499);
			}
			return ;
		}
	}

protected void checkForErrors2180()
	{
		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
			wsaaUserFieldsInner.wsaaRoluRold.set(varcom.calc);
		}
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation2610();
				case continue2620: 
					continue2620();
				case updateErrorIndicators2670: 
					updateErrorIndicators2670();
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		/* Check if fields have been blanked out i.e. deleted.*/
		if (isEQ(sv.fupcode, SPACES)
		&& isEQ(sv.fuptype, SPACES)
		&& isEQ(sv.fupremk, SPACES)
		&& isEQ(sv.fupstat, SPACES)
		&& (isEQ(sv.date_var, ZERO)
		|| isEQ(sv.date_var, 99999999)
		|| isEQ(sv.date_var, SPACES))
		&& isEQ(sv.lifeno, ZERO)
		&& isEQ(sv.tranno, ZERO)
		&& isEQ(sv.jlife, SPACES)
		&& isEQ(sv.uprflag, "N")) {
			sv.uprflag.set("D");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Fields have been blanked out for a new record*/
		if (isEQ(sv.fupcode, SPACES)
		&& isEQ(sv.fuptype, SPACES)
		&& isEQ(sv.fupremk, SPACES)
		&& isEQ(sv.fupstat, SPACES)
		&& (isEQ(sv.date_var, ZERO)
		|| isEQ(sv.date_var, 99999999)
		|| isEQ(sv.date_var, SPACES))
		&& isEQ(sv.lifeno, ZERO)
		&& isEQ(sv.tranno, ZERO)
		&& isEQ(sv.jlife, SPACES)
		&& isEQ(sv.uprflag, " ")) {
			sv.uprflag.set("X");
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Check follow up status.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5660);
		itemIO.setItemitem(sv.fupstat);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.fupstsErr.set(errorsInner.e558);
		}
		/* Check follow up code.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5661);
		/*    MOVE SR589-FUPCODE          TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcode);
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			sv.fupcdeErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		/* Check presence of letter type in case of printing of either*/
		/* standard follow up letter or reminder.*/
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		if (isEQ(sv.fupstat, "L")) {
			if (isEQ(t5661rec.zlettype, SPACES)) {
				sv.fupcdeErr.set(errorsInner.hl41);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		if (isEQ(sv.fupstat, "M")) {
			if (isEQ(t5661rec.zlettyper, SPACES)) {
				sv.fupcdeErr.set(errorsInner.hl41);
				goTo(GotoLabel.updateErrorIndicators2670);
			}
		}
		/* If the remark is blank then default to desc from T5661.*/
		/*    IF SR589-FUPREMARK          NOT = SPACES                     */
		if (isNE(sv.fupremk, SPACES)) {
			goTo(GotoLabel.continue2620);
		}
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5661);
		descIO.setLanguage(wsspcomn.language);
		/*    MOVE SR589-FUPCODE          TO DESC-DESCITEM.                */
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcode);
		descIO.setDescitem(wsaaT5661Key);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			/*       MOVE E557                TO SR589-FUPREMARK-ERR           */
			sv.fuprmkErr.set(errorsInner.e557);
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		else {
			/*       MOVE DESC-LONGDESC       TO SR589-FUPREMARK.              */
			sv.fupremk.set(descIO.getLongdesc());
		}
	}

protected void continue2620()
	{
		/* If the date is blank the use 'todays' date.*/
		if ((isEQ(sv.date_var, ZERO)
		|| isEQ(sv.date_var, SPACES)
		|| isEQ(sv.date_var, 99999999))
		&& isEQ(sv.dateErr, SPACES)) {
			if (isEQ(sv.fupstat, "L")) {
				setReminderDate5000();
			}
			else {
				sv.date_var.set(datcon1rec.intDate);
			}
		}
		/* Validate that the life number is not ZEROES.*/
		if (isEQ(sv.lifeno, ZERO)) {
			sv.lifenoErr.set(errorsInner.h031);
		}
		/* Validate joint life, if entered.*/
		if (isNE(sv.jlife, SPACES)) {
			lifelnbIO.setParams(SPACES);
			lifelnbIO.setChdrcoy(chdrpf.getChdrcoy());
			lifelnbIO.setChdrnum(chdrpf.getChdrnum());
			lifelnbIO.setLife(sv.lifeno);
			lifelnbIO.setJlife(sv.jlife);
			lifelnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, lifelnbIO);
			if (isNE(lifelnbIO.getStatuz(), varcom.oK)
			&& isNE(lifelnbIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(lifelnbIO.getParams());
				fatalError600();
			}
			if (isEQ(lifelnbIO.getStatuz(), varcom.mrnf)) {
				sv.jlifeErr.set(errorsInner.e350);
			}
		}
		if (isNE(sv.select, SPACES)) {
			optswchrec.optsFunction.set("CHCK");
			optswchrec.optsCallingProg.set(wsaaProg);
			optswchrec.optsDteeff.set(ZERO);
			optswchrec.optsCompany.set(wsspcomn.company);
			optswchrec.optsItemCompany.set(wsspcomn.company);
			optswchrec.optsLanguage.set(wsspcomn.language);
			varcom.vrcmTranid.set(wsspcomn.tranid);
			optswchrec.optsUser.set(varcom.vrcmUser);
			optswchrec.optsSelType.set("L");
			optswchrec.optsSelOptno.set(sv.select);
			callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
			if (isNE(optswchrec.optsStatuz, varcom.oK)) {
				sv.selectErr.set(optswchrec.optsStatuz);
			}
		}
		/* The subfile record has been changed so update the hidden*/
		/* flag.*/
		if (isEQ(sv.uprflag, "N")) {
			sv.uprflag.set("U");
		}
		if (isEQ(sv.uprflag, SPACES)) {
			sv.uprflag.set("A");
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		processScreen("SR589", sv);
		if (isNE(scrnparams.statuz, varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srnch);
		processScreen("SR589", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void findDoctFlup2700()
	{
		read2700();
	}

protected void read2700()
	{
		/*    CALL 'SR589IO'           USING SCRN-SCREEN-PARAMS*/
		/*                                   SR589-DATA-AREA*/
		/*                                   SR589-SUBFILE-AREA.*/
		processScreen("SR589", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.params.set(scrnparams.screenParams);
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			return ;
		}
		/* Check if this Follow Up requires a Doctor to be supplied.*/
		if (isNE(sv.fupcode, SPACES)) {
			itemIO.setParams(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(t5661);
			/*       MOVE SR589-FUPCODE       TO ITEM-ITEMITEM                 */
			wsaaT5661Lang.set(wsspcomn.language);
			wsaaT5661Fupcode.set(sv.fupcode);
			itemIO.setItemitem(wsaaT5661Key);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			t5661rec.t5661Rec.set(itemIO.getGenarea());
			/* Set indicator if this Follow Up requires Doctor details.*/
		}
		scrnparams.function.set(varcom.srdn);
	}

	/**
	* <pre>
	*    Sections performed from the 2600 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		/*UPDATE-DATABASE*/
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			return ;
		}
		/*  Update database files as required*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		for (wsaaUserFieldsInner.wsaaCount.set(1); !(isGT(wsaaUserFieldsInner.wsaaCount, sv.subfilePage)); wsaaUserFieldsInner.wsaaCount.add(1)){
			updateFollowUp3100();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DO THE FOLLOW UP FILE UPDATING
	* </pre>
	*/
protected void updateFollowUp3100()
	{
		updateFollowUp3110();
	}

protected void updateFollowUp3110()
	{
		/* Get the subfile record using the relative record number.*/
		scrnparams.subfileRrn.set(wsaaUserFieldsInner.wsaaCount);
		scrnparams.function.set(varcom.sread);
		processScreen("SR589", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)
		&& isNE(scrnparams.statuz, varcom.mrnf)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* Set up key.*/
		flupenqIO.setParams(SPACES);
		flupenqIO.setChdrcoy(chdrpf.getChdrcoy());
		flupenqIO.setChdrnum(chdrpf.getChdrnum());
		flupenqIO.setFuptype(sv.fuptype);
		flupenqIO.setTransactionDate(sv.transactionDate);
		flupenqIO.setTranno(sv.tranno);
		flupenqIO.setFupno(sv.fupno);
		/*Save first record in the subfile for ROLD.*/
		if (isEQ(wsaaUserFieldsInner.wsaaCount, 1)) {
			wsaaUserFieldsInner.wsaaFirstFlupKey.set(flupenqIO.getRecKeyData());
		}
		/* The subfile record has not been changed.*/
		if (isEQ(sv.uprflag, SPACES)
		|| isEQ(sv.uprflag, "N")
		|| isEQ(wsspcomn.flag, "I")) {
			return ;
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rolu)
		|| isEQ(wsaaUserFieldsInner.wsaaRoluRold, varcom.rold)) {
			wsspcomn.nextprog.set(wsaaProg);
			return ;
		}
		else {
			optswchCall4100();
		}
		/*EXIT*/
	}

protected void optswchCall4100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call4110();
					lineSwitching4120();
				case switch4170: 
					switch4170();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call4110()
	{
		wsspcomn.nextprog.set(wsaaProg);
	}

	/**
	* <pre>
	* Check for Line Switching
	* </pre>
	*/
protected void lineSwitching4120()
	{
		if (isNE(wsspcomn.secActn[wsspcomn.programPtr.toInt()], "*")) {
			scrnparams.subfileRrn.set(ZERO);
		}
		else {
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		scrnparams.statuz.set(varcom.oK);
		scrnparams.function.set(varcom.sstrt);
		processScreen("SR589", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		while ( !(isNE(sv.select, SPACES)
		|| isEQ(scrnparams.statuz, varcom.endp))) {
			if (isNE(sv.ind, "+")) {
				readFupe1650();
				if (isEQ(fupeIO.getStatuz(), varcom.endp)) {
					sv.ind.set(SPACES);
				}
				else {
					sv.ind.set("+");
				}
			}
			flupenqIO.setChdrcoy(chdrpf.getChdrcoy());
			flupenqIO.setChdrnum(chdrpf.getChdrnum());
			flupenqIO.setFupno(sv.fupno);
			checkHxcl1500();
			scrnparams.function.set(varcom.supd);
			processScreen("SR589", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
			scrnparams.function.set(varcom.srdn);
			processScreen("SR589", sv);
			if (isNE(scrnparams.statuz, varcom.oK)
			&& isNE(scrnparams.statuz, varcom.endp)) {
				syserrrec.statuz.set(scrnparams.statuz);
				fatalError600();
			}
		}
		
		if (isEQ(scrnparams.statuz, varcom.endp)) {
			goTo(GotoLabel.switch4170);
		}
		/* ...if found, "select" the subfile line to switch to PopUp*/
		/* program.*/
		/* READR and KEEPS the FLUP record for the next program*/
		flupenqIO.setParams(SPACES);
		flupenqIO.setChdrcoy(wsspcomn.company);
		flupenqIO.setChdrnum(sv.chdrnum);
		flupenqIO.setFuptype(sv.fuptype);
		flupenqIO.setTransactionDate(sv.transactionDate);
		flupenqIO.setTranno(sv.tranno);
		flupenqIO.setFupno(sv.fupno);
		flupenqIO.setFormat(flupenqrec);
		flupenqIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, flupenqIO);
		if (isNE(flupenqIO.getStatuz(), varcom.oK)
		&& isNE(flupenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(flupenqIO.getParams());
			fatalError600();
		}
		if (isEQ(flupenqIO.getStatuz(), varcom.mrnf)) {
			sv.select.set(errorsInner.h387);
			goTo(GotoLabel.switch4170);
		}
		flupenqIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, flupenqIO);
		if (isNE(flupenqIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(flupenqIO.getParams());
			fatalError600();
		}
		wsaaRrn.set(scrnparams.subfileRrn);
		optswchrec.optsSelType.set("L");
		optswchrec.optsSelOptno.set(sv.select);
		optswchrec.optsSelCode.set(SPACES);
		/* Update the subfile line.*/
		sv.select.set(SPACES);
		sv.uprflag.set("N");
		scrnparams.function.set(varcom.supd);
		processScreen("SR589", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

protected void switch4170()
	{
		optswchrec.optsItemCompany.set(wsspcomn.company);
		optswchrec.optsFunction.set("STCK");
		callProgram(Optswch.class, optswchrec.rec, wsspcomn.secProgs, wsspcomn.secActns, wsspcomn.programPtr, wsspcomn.flag);
		if (isNE(optswchrec.optsStatuz, varcom.oK)
		&& isNE(optswchrec.optsStatuz, varcom.endp)) {
			syserrrec.params.set(optswchrec.rec);
			syserrrec.statuz.set(optswchrec.optsStatuz);
			fatalError600();
		}
		/*    MOVE OPTS-IND(1)            TO SR589-ZSAIND.*/
		if (isEQ(optswchrec.optsStatuz, varcom.oK)) {
			wsspcomn.programPtr.add(1);
			scrnparams.subfileRrn.set(wsaaRrn);
		}
		else {
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
	}

protected void setReminderDate5000()
	{
		setRemdt5010();
	}

protected void setRemdt5010()
	{
		/*  Calculate the date of issuing reminder letter*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5661);
		/*    MOVE SR589-FUPCODE          TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(wsspcomn.language);
		wsaaT5661Fupcode.set(sv.fupcode);
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(datcon1rec.intDate);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(t5661rec.zelpdays);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		/* MOVE DTC2-INT-DATE-2        TO SR589-FUPREMDT.*/
		sv.date_var.set(datcon2rec.intDate2);
	}
/*
 * Class transformed  from Data Structure WSAA-USER-FIELDS--INNER
 */
private static final class WsaaUserFieldsInner { 
		/* WSAA-USER-FIELDS */
	private FixedLengthStringData wsaaDoctFlup = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayeeName = new FixedLengthStringData(5).init("PYNMN");
	private FixedLengthStringData wsaaFirstDisplay = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaRoluRold = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();
	private FixedLengthStringData wsaaFirstPage = new FixedLengthStringData(1);
	private PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0).init(ZERO);

	private FixedLengthStringData wsaaFirstFlupKey = new FixedLengthStringData(64);
	private FixedLengthStringData wsaaFlupKeyChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaFirstFlupKey, 0);
	private FixedLengthStringData wsaaFlupKeyChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaFirstFlupKey, 1);
	private FixedLengthStringData wsaaFlupKeyFuptype = new FixedLengthStringData(1).isAPartOf(wsaaFirstFlupKey, 9);
	private PackedDecimalData wsaaFlupKeyTrdt = new PackedDecimalData(6, 0).isAPartOf(wsaaFirstFlupKey, 10);
	private PackedDecimalData wsaaFlupKeyTranno = new PackedDecimalData(5, 0).isAPartOf(wsaaFirstFlupKey, 14);
	private PackedDecimalData wsaaFlupKeySeqno = new PackedDecimalData(2, 0).isAPartOf(wsaaFirstFlupKey, 17);
	private FixedLengthStringData filler = new FixedLengthStringData(45).isAPartOf(wsaaFirstFlupKey, 19, FILLER).init(SPACES);
}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
	private FixedLengthStringData e040 = new FixedLengthStringData(4).init("E040");
	private FixedLengthStringData f498 = new FixedLengthStringData(4).init("F498");
	private FixedLengthStringData f499 = new FixedLengthStringData(4).init("F499");
	private FixedLengthStringData e558 = new FixedLengthStringData(4).init("E558");
	private FixedLengthStringData e557 = new FixedLengthStringData(4).init("E557");
	private FixedLengthStringData e350 = new FixedLengthStringData(4).init("E350");
	private FixedLengthStringData h031 = new FixedLengthStringData(4).init("H031");
	private FixedLengthStringData hl41 = new FixedLengthStringData(4).init("HL41");
	private FixedLengthStringData h387 = new FixedLengthStringData(4).init("H387");
}

	public StringUtil getStringUtil() {
		return stringUtil;
	}

}
