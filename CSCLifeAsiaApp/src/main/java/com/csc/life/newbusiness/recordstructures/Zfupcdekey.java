package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:18
 * Description:
 * Copybook name: ZFUPCDEKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zfupcdekey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zfupcdeFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData zfupcdeKey = new FixedLengthStringData(256).isAPartOf(zfupcdeFileKey, 0, REDEFINE);
  	public FixedLengthStringData zfupcdeChdrcoy = new FixedLengthStringData(1).isAPartOf(zfupcdeKey, 0);
  	public FixedLengthStringData zfupcdeChdrnum = new FixedLengthStringData(8).isAPartOf(zfupcdeKey, 1);
  	public FixedLengthStringData zfupcdeLife = new FixedLengthStringData(2).isAPartOf(zfupcdeKey, 9);
  	public FixedLengthStringData zfupcdeJlife = new FixedLengthStringData(2).isAPartOf(zfupcdeKey, 11);
  	public FixedLengthStringData zfupcdeFupcode = new FixedLengthStringData(3).isAPartOf(zfupcdeKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(zfupcdeKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zfupcdeFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zfupcdeFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}