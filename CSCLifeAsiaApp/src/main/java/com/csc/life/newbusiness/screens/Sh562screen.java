package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh562screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh562ScreenVars sv = (Sh562ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh562screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh562ScreenVars screenVars = (Sh562ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.scheduleName.setClassString("");
		screenVars.acctmonth.setClassString("");
		screenVars.scheduleNumber.setClassString("");
		screenVars.acctyear.setClassString("");
		screenVars.effdateDisp.setClassString("");
		screenVars.bcompany.setClassString("");
		screenVars.jobq.setClassString("");
		screenVars.bbranch.setClassString("");
		screenVars.znbprod.setClassString("");
	}

/**
 * Clear all the variables in Sh562screen
 */
	public static void clear(VarModel pv) {
		Sh562ScreenVars screenVars = (Sh562ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.scheduleName.clear();
		screenVars.acctmonth.clear();
		screenVars.scheduleNumber.clear();
		screenVars.acctyear.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.bcompany.clear();
		screenVars.jobq.clear();
		screenVars.bbranch.clear();
		screenVars.znbprod.clear();
	}
}
