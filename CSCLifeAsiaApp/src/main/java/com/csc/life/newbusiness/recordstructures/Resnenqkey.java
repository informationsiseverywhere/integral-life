package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:50
 * Description:
 * Copybook name: RESNENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Resnenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData resnenqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData resnenqKey = new FixedLengthStringData(256).isAPartOf(resnenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData resnenqChdrcoy = new FixedLengthStringData(1).isAPartOf(resnenqKey, 0);
  	public FixedLengthStringData resnenqChdrnum = new FixedLengthStringData(8).isAPartOf(resnenqKey, 1);
  	public FixedLengthStringData resnenqTrancde = new FixedLengthStringData(4).isAPartOf(resnenqKey, 9);
  	public PackedDecimalData resnenqTranno = new PackedDecimalData(5, 0).isAPartOf(resnenqKey, 13);
  	public FixedLengthStringData filler = new FixedLengthStringData(240).isAPartOf(resnenqKey, 16, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(resnenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		resnenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}