package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:06
 * Description:
 * Copybook name: LIFECFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifecfikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData lifecfiFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData lifecfiKey = new FixedLengthStringData(256).isAPartOf(lifecfiFileKey, 0, REDEFINE);
  	public FixedLengthStringData lifecfiChdrcoy = new FixedLengthStringData(1).isAPartOf(lifecfiKey, 0);
  	public FixedLengthStringData lifecfiChdrnum = new FixedLengthStringData(8).isAPartOf(lifecfiKey, 1);
  	public FixedLengthStringData lifecfiLife = new FixedLengthStringData(2).isAPartOf(lifecfiKey, 9);
  	public FixedLengthStringData lifecfiJlife = new FixedLengthStringData(2).isAPartOf(lifecfiKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(243).isAPartOf(lifecfiKey, 13, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(lifecfiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifecfiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}