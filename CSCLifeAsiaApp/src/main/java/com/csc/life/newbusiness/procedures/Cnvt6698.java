/*
 * File: Cnvt6698.java
 * Date: December 3, 2013 2:17:29 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVT6698.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.annuities.tablestructures.T6698rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table T6698, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvt6698 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT6698");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t6698 = "T6698";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T6698rec t6698rec = new T6698rec();
	private Varcom varcom = new Varcom();
	private T6698RecOldInner t6698RecOldInner = new T6698RecOldInner();

	public Cnvt6698() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(t6698);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T6698 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), t6698)) {
			getAppVars().addDiagnostic("Table T6698 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T6698 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT66982080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		t6698RecOldInner.t6698RecOld.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		t6698rec.dssnum.set(t6698RecOldInner.t6698DssnumOld);
		t6698rec.earningCap.set(t6698RecOldInner.t6698EarningCapOld);
		t6698rec.inrevnum.set(t6698RecOldInner.t6698InrevnumOld);
		t6698rec.taxrelpc.set(t6698RecOldInner.t6698TaxrelpcOld);
		for (ix.set(1); !(isGT(ix, 10)); ix.add(1)){
			t6698rec.pclimit[ix.toInt()].set(t6698RecOldInner.t6698PclimitOld[ix.toInt()]);
			t6698rec.ageIssageTo[ix.toInt()].set(t6698RecOldInner.t6698ToageOld[ix.toInt()]);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(t6698rec.t6698Rec);
	}

protected void rewriteT66982080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), t6698)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T6698");
		}
	}
/*
 * Class transformed  from Data Structure T6698-REC-OLD--INNER
 */
private static final class T6698RecOldInner { 

	private FixedLengthStringData t6698RecOld = new FixedLengthStringData(500);
	private FixedLengthStringData t6698DssnumOld = new FixedLengthStringData(8).isAPartOf(t6698RecOld, 0);
	private ZonedDecimalData t6698EarningCapOld = new ZonedDecimalData(11, 0).isAPartOf(t6698RecOld, 8);
	private FixedLengthStringData t6698InrevnumOld = new FixedLengthStringData(8).isAPartOf(t6698RecOld, 19);
	private FixedLengthStringData t6698PclimitsOld = new FixedLengthStringData(50).isAPartOf(t6698RecOld, 27);
	private ZonedDecimalData[] t6698PclimitOld = ZDArrayPartOfStructure(10, 5, 2, t6698PclimitsOld, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(50).isAPartOf(t6698PclimitsOld, 0, FILLER_REDEFINE);
	private ZonedDecimalData t6698Pclimit01Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 0);
	private ZonedDecimalData t6698Pclimit02Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 5);
	private ZonedDecimalData t6698Pclimit03Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 10);
	private ZonedDecimalData t6698Pclimit04Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 15);
	private ZonedDecimalData t6698Pclimit05Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 20);
	private ZonedDecimalData t6698Pclimit06Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 25);
	private ZonedDecimalData t6698Pclimit07Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 30);
	private ZonedDecimalData t6698Pclimit08Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 35);
	private ZonedDecimalData t6698Pclimit09Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 40);
	private ZonedDecimalData t6698Pclimit10Old = new ZonedDecimalData(5, 2).isAPartOf(filler, 45);
	private ZonedDecimalData t6698TaxrelpcOld = new ZonedDecimalData(5, 2).isAPartOf(t6698RecOld, 77);
	private FixedLengthStringData t6698ToagesOld = new FixedLengthStringData(20).isAPartOf(t6698RecOld, 82);
	private ZonedDecimalData[] t6698ToageOld = ZDArrayPartOfStructure(10, 2, 0, t6698ToagesOld, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(t6698ToagesOld, 0, FILLER_REDEFINE);
	private ZonedDecimalData t6698Toage01Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
	private ZonedDecimalData t6698Toage02Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
	private ZonedDecimalData t6698Toage03Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
	private ZonedDecimalData t6698Toage04Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
	private ZonedDecimalData t6698Toage05Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
	private ZonedDecimalData t6698Toage06Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
	private ZonedDecimalData t6698Toage07Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
	private ZonedDecimalData t6698Toage08Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
	private ZonedDecimalData t6698Toage09Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
	private ZonedDecimalData t6698Toage10Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 18);
}
}
