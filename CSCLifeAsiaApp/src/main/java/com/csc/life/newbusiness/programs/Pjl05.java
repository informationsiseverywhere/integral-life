/*
 * File: Pjl05.java
 * Date: 20 December 2019
 * Author: vdivisala
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.math.BigDecimal;

import com.csc.life.newbusiness.screens.Sjl05ScreenVars;
import com.csc.life.newbusiness.tablestructures.Tjl05rec;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.util.SmartTableDataFactory;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 * This Contract Commencement flag is for the benefit of those insured with monthly payment to avoid a higher premium
 * Due to the birthday falling in between the Risk Commencement Date and a day before the Contract Date.
 * </pre>
 */
public class Pjl05 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJL05");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	/* ERRORS */
	private String e026 = "E026";
	private String e027 = "E027";
	private String rl39 = "RL39";
	private String h357 = "H357";
	private String h366 = "H366";

	private String wsaaItempfx;
	private String wsaaItemcoy;
	private String wsaaItemitem;
	private String wsaaItemtabl;
	private String wsaaItemseq;
	private String wsaaItemlang;
	private ZonedDecimalData wsaaSeq = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaFirstTime = "Y";
	private Tjl05rec tjl05rec = new Tjl05rec();
	private DescpfDAO iafDescDAO = getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao",ItemDAO.class);
	private Itempf itempf = null;
	private Itemkey wsaaItemkey = new Itemkey();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sjl05ScreenVars sv = ScreenProgram.getScreenVars(Sjl05ScreenVars.class);
	private boolean isDataChanged = false;

	public Pjl05() {
		super();
		screenVars = sv;
		new ScreenModel("Sjl05", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
		}
	}

	protected void initialise1000() {
		sv.errorIndicators.set(SPACES);
		wsaaItemseq = wsaaItemkey.itemItemseq.toString();
		if (isNE(wsaaFirstTime, "Y")) {
			if (isNE(wsaaSeq, ZERO)) {
				wsaaItemseq = wsaaSeq.toString();
			}
		} else {
			sv.dataArea.set(SPACES);
		}
		syserrrec.subrname.set(wsaaProg);
		wsaaItemkey.set(wsspsmart.itemkey);
		wsaaItempfx = wsaaItemkey.itemItempfx.toString();
		wsaaItemcoy = wsaaItemkey.itemItemcoy.toString();
		wsaaItemitem = wsaaItemkey.itemItemitem.toString();
		wsaaItemtabl = wsaaItemkey.itemItemtabl.toString();
		wsaaItemlang = wsspcomn.language.toString();
		sv.company.set(wsaaItemkey.itemItemcoy);
		sv.tabl.set(wsaaItemkey.itemItemtabl);
		sv.item.set(wsaaItemkey.itemItemitem);
		String longdesc = iafDescDAO.getItemLongDesc(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem,
				wsaaItemlang);
		sv.longdesc.set(longdesc);

		itempf = itemDAO.findItemBySeq(wsaaItempfx, wsaaItemcoy, wsaaItemtabl, wsaaItemitem, "1", wsaaItemseq);
		if (itempf == null) {
			if(isEQ(wsspcomn.flag, "I")) {
				scrnparams.errorCode.set(e026);
				if(isGT(wsaaSeq,ZERO)) {
					wsaaSeq.subtract(1);
				}
			} else {
				tjl05rec.tjl05Rec.set(SPACES);
			}
		} else {
			tjl05rec.tjl05Rec.set(itempf.getGenareaString());
		}

		sv.billfreqs.set(tjl05rec.billfreqs);
		sv.mops.set(tjl05rec.mops);
		sv.fwcondts.set(tjl05rec.fwcondts);
		sv.concommflgs.set(tjl05rec.concommflgs);
		wsaaFirstTime = "N";
	}

	@SuppressWarnings("static-access")
	protected void preScreenEdit() {
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
	}

	@SuppressWarnings("static-access")
	protected void screenEdit2000() {
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rold) && isEQ(wsaaSeq, ZERO)) {
			scrnparams.errorCode.set(e027);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu) && isEQ(wsaaSeq, "99")) {
			scrnparams.errorCode.set(e026);
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		validate2010();
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	protected void validate2010() {
		// No validation needed for blank entries.
		if (isEQ(sv.mops, SPACES) && isEQ(sv.billfreqs, SPACES) && isEQ(sv.fwcondts, SPACES) && isEQ(sv.concommflgs, SPACES)) {
			return;
		}
		for (int i = 1; i <= 10; i++) {
			if (isEQ(sv.mop[i], SPACES)) {
				if (isNE(sv.billfreq[i], SPACES) || isNE(sv.fwcondt[i], SPACES) || isNE(sv.concommflg[i], SPACES)) {
					sv.mopErr[i].set(h366);
				}
				// No blank entries in between
				for (int j = i + 1; j <= 10; j++) {
					if (isNE(sv.mop[j], SPACES)) {
						sv.mopErr[i].set(rl39);
					}
				}
			} else {
				// Duplicate entries not allowed
				for (int j = i + 1; j <= 10; j++) {
					if (isNE(sv.mop[j], SPACES) && isEQ(sv.mop[i], sv.mop[j])) {
						if(isNE(sv.billfreq[i], SPACES) && isNE(sv.billfreq[j], SPACES) && isEQ(sv.billfreq[i], sv.billfreq[j])) {
							if(isNE(sv.fwcondt[i], SPACES) && isNE(sv.fwcondt[j], SPACES) && isEQ(sv.fwcondt[i], sv.fwcondt[j])) {
								if(isNE(sv.concommflg[i], SPACES) && isNE(sv.concommflg[j], SPACES) && isEQ(sv.concommflg[i], sv.concommflg[j])) {
									sv.mopErr[i].set(h357);
									sv.mopErr[j].set(h357);
									sv.billfreqErr[i].set(h357);
									sv.billfreqErr[j].set(h357);
									sv.fwcondtErr[i].set(h357);
									sv.fwcondtErr[j].set(h357);
									sv.concommflgErr[i].set(h357);
									sv.concommflgErr[j].set(h357);
								}
							}
						}
					}
				}
				if (isEQ(sv.billfreq[i], SPACES)) {
					sv.billfreqErr[i].set(h366);
				}
				if (isEQ(sv.fwcondt[i], SPACES)) {
					sv.fwcondtErr[i].set(h366);
				}
				if (isEQ(sv.concommflg[i], SPACES)) {
					sv.concommflgErr[i].set(h366);
				}
			}
		}
	}

	protected void update3000() {
		if (isEQ(wsspcomn.flag, "I")) {
			return;
		}
		if (isNE(sv.billfreqs, tjl05rec.billfreqs)) {
			tjl05rec.billfreqs.set(sv.billfreqs);
			isDataChanged = true;
		}
		if (isNE(sv.mops, tjl05rec.mops)) {
			tjl05rec.mops.set(sv.mops);
			isDataChanged = true;
		}
		if (isNE(sv.fwcondts, tjl05rec.fwcondts)) {
			tjl05rec.fwcondts.set(sv.fwcondts);
			isDataChanged = true;
		}
		if (isNE(sv.concommflgs, tjl05rec.concommflgs)) {
			tjl05rec.concommflgs.set(sv.concommflgs);
			isDataChanged = true;
		}
		if(isDataChanged) {
			if(itempf==null) {
				writeData();
			} else {
				itempf.setGenarea(tjl05rec.tjl05Rec.toString().getBytes());
				itempf.setGenareaj(SmartTableDataFactory.getInstance(appVars.getAppConfig().getSmartTableDataFormat())
						.getGENAREAJString(tjl05rec.tjl05Rec.toString().getBytes(), tjl05rec));
				itempf.setItempfx(wsaaItempfx);
				itempf.setItemcoy(wsaaItemcoy);
				itempf.setItemtabl(wsaaItemtabl);
				itempf.setItemitem(wsaaItemitem);
				itempf.setItemseq(wsaaItemseq);
				itemDAO.updateSmartTableItem(itempf);
			}
		}
	}
	
	protected void writeData() {
		Itempf item = new Itempf();
		item.setItempfx(wsaaItempfx);
		item.setItemcoy(wsaaItemcoy);
		item.setItemtabl(wsaaItemtabl);
		item.setItemitem(wsaaItemitem);
		item.setItemseq(wsaaSeq.toString());
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		item.setTranid(varcom.vrcmCompTranid.toString());
		item.setTableprog(wsaaProg.toString());
		item.setValidflag("1");
		item.setItmfrm(new BigDecimal(0));
		item.setItmto(new BigDecimal(0));
		item.setGenarea(tjl05rec.tjl05Rec.toString().getBytes());
		item.setGenareaj("");
		itemDAO.insertSmartTableItem(item);
	}

	@SuppressWarnings("static-access")
	protected void whereNext4000() {
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			wsaaSeq.add(1);
		} else if (isEQ(scrnparams.statuz, varcom.rold)) {
			wsaaSeq.subtract(1);
		} else {
			wsspcomn.programPtr.add(1);
			wsaaSeq.set(ZERO);
		}
	}
}