/*
 * File: Bm500.java
 * Date: 29 August 2009 21:46:37
 * Author: Quipoz Limited
 * 
 * Class transformed from BM500.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.common.DD;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.CovtmjaTableDAM;
import com.csc.life.enquiries.dataaccess.BnfyenqTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.BnfylnbTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Pm510par;
import com.csc.life.newbusiness.reports.Rm500Report;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This batch program will read HPAD and select the First Issue
*  Date failed between the From To Date passed from the parameter
*  screen then extract and print out this Life Policy Register
*  Report.
*
*****************************************************************
* </pre>
*/
public class Bm500 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rm500Report printerFile = new Rm500Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(512);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BM500");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaFreq = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaMprm = new PackedDecimalData(17, 2).setUnsigned();
	private PackedDecimalData wsaaAddprm = new PackedDecimalData(13, 2).setUnsigned();

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1);
	private Validator validContract = new Validator(wsaaValidContract, "Y");
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(7, 0).setUnsigned();
	private String wsaaUwrdec = "";
	private FixedLengthStringData wsaaAgeadm = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOwnername = new FixedLengthStringData(47);
	private FixedLengthStringData wsaaAssuredname = new FixedLengthStringData(47);
	private ZonedDecimalData wsaaAge = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaLine = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaBrn = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaValid = "";

	private FixedLengthStringData wsaaBillchnlCnttype = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBillchnl = new FixedLengthStringData(1).isAPartOf(wsaaBillchnlCnttype, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaBillchnlCnttype, 1);
	private FixedLengthStringData wsaaCovCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCovChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCovChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCovLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovJlife = new FixedLengthStringData(2);
	private PackedDecimalData wsaaCovRcesdte = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCovRcestrm = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaCovSumins = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCovZbinstprem = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String descrec = "DESCREC";
	private String covrrec = "COVRREC";
	private String itemrec = "ITEMREC";
	private String chdrenqrec = "CHDRENQREC";
	private String lifelnbrec = "LIFELNBREC";
	private String covtmjarec = "COVTMJAREC";
	private String t1693 = "T1693";
	private String t5679 = "T5679";
	private String t5687 = "T5687";
		/* CONTROL-TOTALS */
	private int ct01 = 1;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rm500H01 = new FixedLengthStringData(60);
	private FixedLengthStringData rm500h01O = new FixedLengthStringData(60).isAPartOf(rm500H01, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rm500h01O, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rm500h01O, 30);
	private FixedLengthStringData rh01Datefrom = new FixedLengthStringData(10).isAPartOf(rm500h01O, 40);
	private FixedLengthStringData rh01Dateto = new FixedLengthStringData(10).isAPartOf(rm500h01O, 50);

	private FixedLengthStringData rm500D01 = new FixedLengthStringData(245);
	private FixedLengthStringData rm500d01O = new FixedLengthStringData(245).isAPartOf(rm500D01, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(rm500d01O, 0);
	private FixedLengthStringData rd01Lifename = new FixedLengthStringData(47).isAPartOf(rm500d01O, 8);
	private FixedLengthStringData rd01Sex = new FixedLengthStringData(1).isAPartOf(rm500d01O, 55);
	private ZonedDecimalData rd01Rcesage = new ZonedDecimalData(3, 0).isAPartOf(rm500d01O, 56);
	private FixedLengthStringData rd01Billfreq = new FixedLengthStringData(2).isAPartOf(rm500d01O, 59);
	private ZonedDecimalData rd01Rcestrm = new ZonedDecimalData(3, 0).isAPartOf(rm500d01O, 61);
	private FixedLengthStringData rd01Agntnum = new FixedLengthStringData(8).isAPartOf(rm500d01O, 64);
	private FixedLengthStringData rd01Cntbranch = new FixedLengthStringData(2).isAPartOf(rm500d01O, 72);
	private FixedLengthStringData rd01Pflg = new FixedLengthStringData(1).isAPartOf(rm500d01O, 74);
	private FixedLengthStringData rd01Cnttype = new FixedLengthStringData(3).isAPartOf(rm500d01O, 75);
	private FixedLengthStringData rd01Date01 = new FixedLengthStringData(10).isAPartOf(rm500d01O, 78);
	private FixedLengthStringData rd01Date02 = new FixedLengthStringData(10).isAPartOf(rm500d01O, 88);
	private ZonedDecimalData rd01Singp = new ZonedDecimalData(17, 2).isAPartOf(rm500d01O, 98);
	private ZonedDecimalData rd01Agrossprem = new ZonedDecimalData(17, 2).isAPartOf(rm500d01O, 115);
	private ZonedDecimalData rd01Extr = new ZonedDecimalData(17, 2).isAPartOf(rm500d01O, 132);
	private ZonedDecimalData rd01Premsusp = new ZonedDecimalData(17, 2).isAPartOf(rm500d01O, 149);
	private FixedLengthStringData rd01Ageadm = new FixedLengthStringData(1).isAPartOf(rm500d01O, 166);
	private FixedLengthStringData rd01Date04 = new FixedLengthStringData(10).isAPartOf(rm500d01O, 167);
	private FixedLengthStringData rd01Nffeit = new FixedLengthStringData(4).isAPartOf(rm500d01O, 177);
	private ZonedDecimalData rd01Sumins = new ZonedDecimalData(17, 2).isAPartOf(rm500d01O, 181);
	private FixedLengthStringData rd01Ownername = new FixedLengthStringData(47).isAPartOf(rm500d01O, 198);

	private FixedLengthStringData rm500D03 = new FixedLengthStringData(49+DD.cltaddr.length*5);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData rm500d03O = new FixedLengthStringData(49+DD.cltaddr.length*5).isAPartOf(rm500D03, 0);
	private FixedLengthStringData rd03Bnynam = new FixedLengthStringData(47).isAPartOf(rm500d03O, 0);
	private FixedLengthStringData rd03Liferel = new FixedLengthStringData(2).isAPartOf(rm500d03O, 47);
	private FixedLengthStringData rd03Addrss01 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d03O, 49);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData rd03Addrss02 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d03O, 49+DD.cltaddr.length);
	private FixedLengthStringData rd03Addrss03 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d03O, 49+DD.cltaddr.length*2);
	private FixedLengthStringData rd03Addrss04 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d03O, 49+DD.cltaddr.length*3);
	private FixedLengthStringData rd03Addrss05 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d03O, 49+DD.cltaddr.length*4);

	private FixedLengthStringData rm500D02 = new FixedLengthStringData(DD.cltaddr.length*5);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData rm500d02O = new FixedLengthStringData(DD.cltaddr.length*5).isAPartOf(rm500D02, 0);
	private FixedLengthStringData rd02Addr01 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d02O, 0);/*pmujavadiya ILIFE-3212*/
	private FixedLengthStringData rd02Addr02 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d02O, DD.cltaddr.length);
	private FixedLengthStringData rd02Addr03 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d02O, DD.cltaddr.length*2);
	private FixedLengthStringData rd02Addr04 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d02O, DD.cltaddr.length*3);
	private FixedLengthStringData rd02Addr05 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rm500d02O, DD.cltaddr.length*4);

	private FixedLengthStringData rm500D04 = new FixedLengthStringData(198);

	private FixedLengthStringData rm500T01 = new FixedLengthStringData(205);
	private FixedLengthStringData rm500t01O = new FixedLengthStringData(7).isAPartOf(rm500T01, 0);
	private ZonedDecimalData rt01Nofmbr = new ZonedDecimalData(7, 0).isAPartOf(rm500t01O, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract Beneficiaries - Contract Enquir*/
	private BnfyenqTableDAM bnfyenqIO = new BnfyenqTableDAM();
		/*Beneficiaries - life new business*/
	private BnfylnbTableDAM bnfylnbIO = new BnfylnbTableDAM();
		/*Contract header file*/
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
		/*Contract Enquiry - Contract Header.*/
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
		/*Contract Header Life Fields*/
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Coverage transaction Record - Major Alts*/
	private CovtmjaTableDAM covtmjaIO = new CovtmjaTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Contract Additional Details*/
	private HpadTableDAM hpadIO = new HpadTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life Options/Extras logical file*/
	private LextTableDAM lextIO = new LextTableDAM();
		/*Life record*/
	private LifeTableDAM lifeIO = new LifeTableDAM();
		/*Life and joint life details - new busine*/
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Pm510par pm510par = new Pm510par();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr2490, 
		nextrLext3890, 
		a150Continue
	}

	public Bm500() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		pm510par.parmRecord.set(bupaIO.getParmarea());
		wsaaCount.set(ZERO);
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		wsaaBrn.set(bprdIO.getSystemParam04());
		hpadIO.setRecKeyData(SPACES);
		hpadIO.setFunction(varcom.begn);
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(pm510par.datefrm);
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Datefrom.set(datcon1rec.extDate);
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(pm510par.dateto);
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Dateto.set(datcon1rec.extDate);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void readFile2000()
	{
		readFile2010();
	}

protected void readFile2010()
	{
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(),varcom.oK)
		&& isNE(hpadIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		if (isEQ(hpadIO.getStatuz(),varcom.endp)) {
			wsspEdterror.set(varcom.endp);
			rt01Nofmbr.set(wsaaCount);
			printerFile.printRm500t01(rm500T01);
		}
		else {
			wsaaValid = "N";
			if (isGTE(hpadIO.getHoissdte(),pm510par.datefrm)
			&& isLTE(hpadIO.getHoissdte(),pm510par.dateto)) {
				wsaaValid = "Y";
			}
			else {
				wsspEdterror.set(SPACES);
				hpadIO.setFunction(varcom.nextr);
			}
		}
	}

protected void edit2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					edit2510();
				}
				case nextr2490: {
					nextr2490();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void edit2510()
	{
		if (isEQ(wsaaValid,"N")) {
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.nextr2490);
		}
		chdrenqIO.setChdrcoy(hpadIO.getChdrcoy());
		chdrenqIO.setChdrnum(hpadIO.getChdrnum());
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat(chdrenqrec);
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(),varcom.oK)
		&& isNE(chdrenqIO.getStatuz(),varcom.mrnf)) {
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(),varcom.mrnf)) {
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.nextr2490);
		}
		if (isNE(wsaaBrn,"**")) {
			if (isNE(chdrenqIO.getCntbranch(),wsaaBrn)) {
				wsspEdterror.set(SPACES);
				goTo(GotoLabel.nextr2490);
			}
		}
		if (isEQ(wsaaValidContract,"N")) {
			wsspEdterror.set(SPACES);
			goTo(GotoLabel.nextr2490);
		}
	}

protected void nextr2490()
	{
		hpadIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
		writeDetail3080();
		skipOption3085();
	}

protected void update3010()
	{
		covrIO.setGenDate(ZERO);
		covrIO.setGenTime(ZERO);
		covrIO.setRrn(ZERO);
		covrIO.setPlanSuffix(ZERO);
		covrIO.setTranno(ZERO);
		covrIO.setChdrcoy(bsprIO.getCompany());
		covrIO.setChdrnum(chdrenqIO.getChdrnum());
		covrIO.setLife("01");
		covrIO.setCoverage("01");
		covrIO.setRider("00");
		covrIO.setPlanSuffix(ZERO);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(),varcom.oK)
		&& isNE(covrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(covrIO.getStatuz());
			syserrrec.params.set(covrIO.getParams());
			fatalError600();
		}
		if (isEQ(covrIO.getStatuz(),varcom.mrnf)) {
			covtmjaIO.setDataKey(SPACES);
			covtmjaIO.setChdrcoy(covrIO.getChdrcoy());
			covtmjaIO.setChdrnum(covrIO.getChdrnum());
			covtmjaIO.setLife(covrIO.getLife());
			covtmjaIO.setCoverage(covrIO.getCoverage());
			covtmjaIO.setRider(covrIO.getRider());
			covtmjaIO.setPlanSuffix(covrIO.getPlanSuffix());
			covtmjaIO.setFormat(covtmjarec);
			covtmjaIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, covtmjaIO);
			if (isNE(covtmjaIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(covtmjaIO.getStatuz());
				syserrrec.params.set(covtmjaIO.getParams());
				fatalError600();
			}
			wsaaCovChdrcoy.set(covtmjaIO.getChdrcoy());
			wsaaCovChdrnum.set(covtmjaIO.getChdrnum());
			wsaaCovCrtable.set(covtmjaIO.getCrtable());
			wsaaCovLife.set(covtmjaIO.getLife());
			wsaaCovJlife.set(covtmjaIO.getJlife());
			wsaaCovRcesdte.set(covtmjaIO.getRiskCessDate());
			wsaaCovRcestrm.set(covtmjaIO.getRiskCessTerm());
			wsaaCovSumins.set(covtmjaIO.getSumins());
			wsaaCovZbinstprem.set(covtmjaIO.getZbinstprem());
		}
		else {
			wsaaCovChdrcoy.set(covrIO.getChdrcoy());
			wsaaCovChdrnum.set(covrIO.getChdrnum());
			wsaaCovCrtable.set(covrIO.getCrtable());
			wsaaCovLife.set(covrIO.getLife());
			wsaaCovJlife.set(covrIO.getJlife());
			wsaaCovRcesdte.set(covrIO.getRiskCessDate());
			wsaaCovRcestrm.set(covrIO.getRiskCessTerm());
			wsaaCovSumins.set(covrIO.getSumins());
			wsaaCovZbinstprem.set(covrIO.getZbinstprem());
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(wsaaCovCrtable);
		itdmIO.setItmfrm(chdrenqIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),wsaaCovCrtable)
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			itdmIO.setItemcoy(bsprIO.getCompany());
			itdmIO.setItemtabl(t5687);
			itdmIO.setItemitem(wsaaCovCrtable);
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(wsaaCovChdrcoy);
		lifelnbIO.setChdrnum(wsaaCovChdrnum);
		lifelnbIO.setLife(wsaaCovLife);
		if (isEQ(wsaaCovJlife,SPACES)) {
			lifelnbIO.setJlife("00");
		}
		else {
			lifelnbIO.setJlife(wsaaCovJlife);
		}
		lifelnbIO.setFormat(lifelnbrec);
		lifelnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(),varcom.oK)
		&& isNE(lifelnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(lifelnbIO.getStatuz());
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		wsaaAgeadm.set(lifelnbIO.getAgeadm());
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(lifelnbIO.getLifcnum());
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(bsprIO.getFsuco());
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)
		&& isNE(namadrsrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		wsaaAssuredname.set(namadrsrec.name);
		datcon3rec.intDate1.set(lifelnbIO.getCltdob());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz,varcom.oK)) {
			syserrrec.params.set(datcon3rec.datcon3Rec);
			syserrrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaAge.set(datcon3rec.freqFactor);
	}

protected void writeDetail3080()
	{
		rd01Chdrnum.set(chdrenqIO.getChdrnum());
		rd01Billfreq.set(chdrenqIO.getBillfreq());
		wsaaFreq.set(chdrenqIO.getBillfreq());
		rd01Cntbranch.set(chdrenqIO.getCntbranch());
		rd01Agntnum.set(chdrenqIO.getAgntnum());
		rd01Cnttype.set(chdrenqIO.getCnttype());
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(chdrenqIO.getOccdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rd01Date01.set(datcon1rec.extDate);
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.intDate1.set(wsaaCovRcesdte);
		datcon2rec.frequency.set(chdrenqIO.getBillfreq());
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		datcon1rec.intDate.set(datcon2rec.intDate2);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz,varcom.oK)) {
			syserrrec.statuz.set(datcon1rec.statuz);
			fatalError600();
		}
		rd01Date04.set(datcon1rec.extDate);
		rd01Lifename.set(wsaaAssuredname);
		rd01Sex.set(lifelnbIO.getCltsex());
		rd01Nffeit.set(t5687rec.nonForfeitMethod);
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(wsaaCovRcesdte);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rd01Date02.set(datcon1rec.extDate);
		if (isEQ(wsaaCovRcestrm,ZERO)) {
			datcon3rec.intDate1.set(chdrenqIO.getOccdate());
			datcon3rec.intDate2.set(wsaaCovRcesdte);
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
			rd01Rcestrm.set(datcon3rec.freqFactor);
		}
		else {
			rd01Rcestrm.set(wsaaCovRcestrm);
		}
		rd01Rcesage.set(wsaaAge);
		rd01Sumins.set(wsaaCovSumins);
		rd01Singp.set(wsaaCovZbinstprem);
		compute(wsaaMprm, 2).set(mult(rd01Singp,wsaaFreq));
		rd01Agrossprem.set(wsaaMprm);
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(chdrenqIO.getChdrcoy());
		chdrlifIO.setChdrnum(chdrenqIO.getChdrnum());
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(chdrlifIO.getStatuz());
			syserrrec.params.set(chdrlifIO.getParams());
			fatalError600();
		}
		compute(wsaaAddprm, 2).set(sub(chdrlifIO.getSinstamt01(),rd01Singp));
		rd01Extr.set(wsaaAddprm);
		rd01Premsusp.set(chdrlifIO.getSinstamt01());
		rd01Pflg.set(t5687rec.statFund);
		rd01Ageadm.set(wsaaAgeadm);
		if (isNE(wsaaChdrnum,hpadIO.getChdrnum())
		&& isGTE(wsaaLine,28)) {
			wsaaOverflow.set("Y");
			wsaaLine.set(ZERO);
		}
		wsaaChdrnum.set(hpadIO.getChdrnum());
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRm500h01(rm500H01, indicArea);
			wsaaOverflow.set("N");
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(chdrenqIO.getCownnum());
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(chdrenqIO.getCowncoy());
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)
		&& isNE(namadrsrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		wsaaOwnername.set(namadrsrec.name);
		if (isEQ(wsaaOwnername,wsaaAssuredname)) {
			rd01Ownername.set(SPACES);
		}
		else {
			rd01Ownername.set(wsaaOwnername);
		}
		wsaaLine.add(1);
		printerFile.printRm500d01(rm500D01, indicArea);
		rd02Addr01.set(namadrsrec.addr1);
		rd02Addr02.set(namadrsrec.addr2);
		rd02Addr03.set(namadrsrec.addr3);
		rd02Addr04.set(namadrsrec.addr4);
		rd02Addr05.set(namadrsrec.addr5);
		wsaaCount.add(1);
		if (newPageReq.isTrue()) {
			wsaaLine.set(ZERO);
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRm500h01(rm500H01, indicArea);
			wsaaOverflow.set("N");
		}
		wsaaLine.add(1);
		printerFile.printRm500d02(rm500D02, indicArea);
	}

protected void skipOption3085()
	{
		bnfyenqIO.setDataArea(SPACES);
		bnfyenqIO.setChdrcoy(chdrenqIO.getChdrcoy());
		bnfyenqIO.setChdrnum(chdrenqIO.getChdrnum());
		bnfyenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		bnfyenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		bnfyenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, bnfyenqIO);
		if (isNE(bnfyenqIO.getStatuz(),varcom.oK)
		&& isNE(bnfyenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(bnfyenqIO.getParams());
			fatalError600();
		}
		while ( !(isNE(bnfyenqIO.getChdrcoy(),chdrenqIO.getChdrcoy())
		|| isNE(bnfyenqIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isEQ(bnfyenqIO.getStatuz(),varcom.endp))) {
			a100ProcessBnfy();
		}
		
		if (newPageReq.isTrue()) {
			wsaaLine.set(ZERO);
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRm500h01(rm500H01, indicArea);
			wsaaOverflow.set("N");
		}
		wsaaLine.add(1);
		printerFile.printRm500d04(rm500D04, indicArea);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
		/*NEXTR-COVR*/
		hpadIO.setFunction(varcom.nextr);
	}

protected void checkLext3800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3800();
				}
				case nextrLext3890: {
					nextrLext3890();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3800()
	{
		SmartFileCode.execute(appVars, lextIO);
		if (isEQ(lextIO.getStatuz(),varcom.endp)
		|| isNE(lextIO.getStatuz(),varcom.oK)
		|| isNE(lextIO.getChdrnum(),chdrenqIO.getChdrnum())
		|| isEQ(wsaaUwrdec,"Y")) {
			goTo(GotoLabel.nextrLext3890);
		}
		wsaaUwrdec = "Y";
	}

protected void nextrLext3890()
	{
		lextIO.setFunction(varcom.nextr);
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a100ProcessBnfy()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a100Para();
				}
				case a150Continue: {
					a150Continue();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a100Para()
	{
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(bnfyenqIO.getBnyclt());
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(chdrenqIO.getCowncoy());
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.function.set("PYNMN");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)
		&& isNE(namadrsrec.statuz,varcom.mrnf)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		if (isEQ(namadrsrec.statuz,varcom.mrnf)) {
			goTo(GotoLabel.a150Continue);
		}
		rd03Bnynam.set(namadrsrec.name);
		rd03Addrss01.set(namadrsrec.addr1);
		rd03Addrss02.set(namadrsrec.addr2);
		rd03Addrss03.set(namadrsrec.addr3);
		rd03Addrss04.set(namadrsrec.addr4);
		rd03Addrss05.set(namadrsrec.addr5);
		rd03Liferel.set(bnfyenqIO.getBnyrln());
		if (newPageReq.isTrue()) {
			wsaaLine.set(ZERO);
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printRm500h01(rm500H01, indicArea);
			wsaaOverflow.set("N");
		}
		wsaaLine.add(1);
		printerFile.printRm500d03(rm500D03, indicArea);
	}

protected void a150Continue()
	{
		bnfyenqIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, bnfyenqIO);
		if (isNE(bnfyenqIO.getStatuz(),varcom.oK)
		&& isNE(bnfyenqIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(bnfyenqIO.getParams());
			fatalError600();
		}
		/*A190-EXIT*/
	}
}
