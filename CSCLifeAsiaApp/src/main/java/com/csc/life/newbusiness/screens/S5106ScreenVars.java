package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5106
 * @version 1.0 generated on 30/08/09 06:34
 * @author Quipoz
 */
public class S5106ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(745);
	public FixedLengthStringData dataFields = new FixedLengthStringData(297).isAPartOf(dataArea, 0);
	public FixedLengthStringData adjind = DD.adjind.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntname = DD.agntname.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,31);
	public ZonedDecimalData anbage = DD.anbage.copyToZonedDecimal().isAPartOf(dataFields,41);
	public FixedLengthStringData apcind = DD.apcind.copy().isAPartOf(dataFields,44);
	public ZonedDecimalData billcd = DD.billcd.copyToZonedDecimal().isAPartOf(dataFields,45);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,55);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,63);
	public FixedLengthStringData crcind = DD.crcind.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData ddind = DD.ddind.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,67);
	public FixedLengthStringData dlvrmode = DD.dlvrmode.copy().isAPartOf(dataFields,97);
	public FixedLengthStringData grpind = DD.grpind.copy().isAPartOf(dataFields,101);
	public FixedLengthStringData lifename = DD.lifename.copy().isAPartOf(dataFields,102);
	public FixedLengthStringData lifesel = DD.lifesel.copy().isAPartOf(dataFields,149);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,159);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(dataFields,160);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,161);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,169);
	public FixedLengthStringData ownersel = DD.ownersel.copy().isAPartOf(dataFields,216);
	public ZonedDecimalData prmamt = DD.prmamt.copyToZonedDecimal().isAPartOf(dataFields,226);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,243);
	public FixedLengthStringData smoking = DD.smoking.copy().isAPartOf(dataFields,246);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(dataFields,248);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,250);
	public ZonedDecimalData unitpkg = DD.unitpkg.copyToZonedDecimal().isAPartOf(dataFields,267);
	public FixedLengthStringData zagelit = DD.zagelit.copy().isAPartOf(dataFields,284);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(112).isAPartOf(dataArea, 297);
	public FixedLengthStringData adjindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData anbageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData apcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData billcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData crcindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData ddindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData dlvrmodeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData grpindErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lifenameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lifeselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData mortclsErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ownerselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData prmamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData smokingErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData srcebusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData unitpkgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData zagelitErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(336).isAPartOf(dataArea, 409);
	public FixedLengthStringData[] adjindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] anbageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] apcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] billcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] crcindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] ddindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] dlvrmodeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] grpindOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lifenameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lifeselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] mortclsOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ownerselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] prmamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] smokingOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] srcebusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] unitpkgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] zagelitOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);

	public LongData S5106screenWritten = new LongData(0);
	public LongData S5106protectWritten = new LongData(0);
	public FixedLengthStringData iljCntDteFlag = new FixedLengthStringData(1);  //ILJ-49

	public boolean hasSubfile() {
		return false;
	}


	public S5106ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(occdateOut,new String[] {"01","50","-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrnumOut,new String[] {"22",null, "-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(registerOut,new String[] {"02","50","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(srcebusOut,new String[] {"04","50","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(dlvrmodeOut,new String[] {"24","50","-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntselOut,new String[] {"05","50","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(agntnameOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ownerselOut,new String[] {"07","50","-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ownernameOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mopOut,new String[] {"09","50","-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billfreqOut,new String[] {"12","50","-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(billcdOut,new String[] {"13","50","-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifeselOut,new String[] {"14","50","-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifenameOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(anbageOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(smokingOut,new String[] {"17","50","-17",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mortclsOut,new String[] {"18","50","-18",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(unitpkgOut,new String[] {"19","50","-19",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(prmamtOut,new String[] {null, null, null, "30",null, null, null, null, null, null, null, null});
		fieldIndMap.put(adjindOut,new String[] {"20",null, "-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(apcindOut,new String[] {"21",null, "-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zagelitOut,new String[] {"23",null, "-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "40",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {ddind, crcind, grpind, descrip, occdate, chdrnum, register, cntbranch, srcebus, dlvrmode, agntsel, agntname, ownersel, ownername, mop, billfreq, billcd, lifesel, lifename, anbage, smoking, mortcls, unitpkg, prmamt, adjind, apcind, zagelit, taxamt};
		screenOutFields = new BaseData[][] {ddindOut, crcindOut, grpindOut, descripOut, occdateOut, chdrnumOut, registerOut, cntbranchOut, srcebusOut, dlvrmodeOut, agntselOut, agntnameOut, ownerselOut, ownernameOut, mopOut, billfreqOut, billcdOut, lifeselOut, lifenameOut, anbageOut, smokingOut, mortclsOut, unitpkgOut, prmamtOut, adjindOut, apcindOut, zagelitOut, taxamtOut};
		screenErrFields = new BaseData[] {ddindErr, crcindErr, grpindErr, descripErr, occdateErr, chdrnumErr, registerErr, cntbranchErr, srcebusErr, dlvrmodeErr, agntselErr, agntnameErr, ownerselErr, ownernameErr, mopErr, billfreqErr, billcdErr, lifeselErr, lifenameErr, anbageErr, smokingErr, mortclsErr, unitpkgErr, prmamtErr, adjindErr, apcindErr, zagelitErr, taxamtErr};
		screenDateFields = new BaseData[] {occdate, billcd};
		screenDateErrFields = new BaseData[] {occdateErr, billcdErr};
		screenDateDispFields = new BaseData[] {occdateDisp, billcdDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5106screen.class;
		protectRecord = S5106protect.class;
	}

}
