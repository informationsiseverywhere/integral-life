/*
 * File: Cnvt5646.java
 * Date: December 3, 2013 2:17:15 AM ICT
 * Author: CSC
 * 
 * Class transformed from CNVT5646.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.productdefinition.tablestructures.T5646rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*   Conversion program for table T5646, Replace INSTPR with INSTPR 1,
*   add new fields INSTPR02 to INSTPR11.
*
*
****************************************************************** ****
* </pre>
*/
public class Cnvt5646 extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("CNVT5646");
	private final String wsaaEof = "";
		/* TABLES */
	private static final String t5646 = "T5646";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData ix = new ZonedDecimalData(3, 0).setUnsigned();
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5646rec t5646rec = new T5646rec();
	private Varcom varcom = new Varcom();
	private T5646RecOldInner t5646RecOldInner = new T5646RecOldInner();

	public Cnvt5646() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 1);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 0);
		try {
			control1000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void control1000()
	{
		start1000();
		exit1000();
	}

protected void start1000()
	{
		/* Read all the ITEM records an convert...*/
		wsaaCount.set(ZERO);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(lsaaCompany);
		itemIO.setItemtabl(t5646);
		itemIO.setItemitem(SPACES);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			getAppVars().addDiagnostic("CONVERT T5646 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			getAppVars().addDiagnostic("KEY - "+itemIO.getItemitem());
			return ;
		}
		if (isNE(itemIO.getItemtabl(), t5646)) {
			getAppVars().addDiagnostic("Table T5646 not found");
			return ;
		}
		/* Process each record on the file.*/
		while ( !(isEQ(wsaaEof, "Y")
		|| isNE(itemIO.getStatuz(), varcom.oK))) {
			processTable2000();
		}
		
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			getAppVars().addDiagnostic("CONVERT T5646 ERROR - "+itemIO.getFunction().toString() +"-STATUS-"+itemIO.getStatuz());
			return ;
		}
		lsaaStatuz.set("****");
		appVars.commit();
	}

protected void exit1000()
	{
		stopRun();
	}

protected void processTable2000()
	{
		update2000();
		rewriteT56462080();
	}

protected void update2000()
	{
		/* Move GENAREA to old copybook.*/
		t5646RecOldInner.t5646RecOld.set(itemIO.getGenarea());
		/* Then map into new copybook and populate century.*/
		t5646rec.agelimit.set(t5646RecOldInner.t5646AgelimitOld);
		for (ix.set(1); !(isGT(ix, 12)); ix.add(1)){
			t5646rec.ageIssageFrm[ix.toInt()].set(t5646RecOldInner.t5646AgfrmOld[ix.toInt()]);
			t5646rec.ageIssageTo[ix.toInt()].set(t5646RecOldInner.t5646AgtoOld[ix.toInt()]);
			t5646rec.factorsa[ix.toInt()].set(t5646RecOldInner.t5646FactorsaOld[ix.toInt()]);
		}
		itemIO.setGenarea(SPACES);
		itemIO.setGenarea(t5646rec.t5646Rec);
	}

protected void rewriteT56462080()
	{
		wsaaCount.add(1);
		itemIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			return ;
		}
		/*READ*/
		itemIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemtabl(), t5646)) {
			itemIO.setStatuz(varcom.endp);
			getAppVars().addDiagnostic(wsaaCount.toString() +" items converted for T5646");
		}
	}
/*
 * Class transformed  from Data Structure T5646-REC-OLD--INNER
 */
private static final class T5646RecOldInner { 

	private FixedLengthStringData t5646RecOld = new FixedLengthStringData(500);
	private ZonedDecimalData t5646AgelimitOld = new ZonedDecimalData(2, 0).isAPartOf(t5646RecOld, 0);
	private FixedLengthStringData t5646AgfrmsOld = new FixedLengthStringData(24).isAPartOf(t5646RecOld, 2);
	private ZonedDecimalData[] t5646AgfrmOld = ZDArrayPartOfStructure(12, 2, 0, t5646AgfrmsOld, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(t5646AgfrmsOld, 0, FILLER_REDEFINE);
	private ZonedDecimalData t5646Agfrm01Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 0);
	private ZonedDecimalData t5646Agfrm02Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 2);
	private ZonedDecimalData t5646Agfrm03Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 4);
	private ZonedDecimalData t5646Agfrm04Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 6);
	private ZonedDecimalData t5646Agfrm05Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 8);
	private ZonedDecimalData t5646Agfrm06Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 10);
	private ZonedDecimalData t5646Agfrm07Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 12);
	private ZonedDecimalData t5646Agfrm08Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 14);
	private ZonedDecimalData t5646Agfrm09Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 16);
	private ZonedDecimalData t5646Agfrm10Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 18);
	private ZonedDecimalData t5646Agfrm11Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 20);
	private ZonedDecimalData t5646Agfrm12Old = new ZonedDecimalData(2, 0).isAPartOf(filler, 22);
	private FixedLengthStringData t5646AgtosOld = new FixedLengthStringData(24).isAPartOf(t5646RecOld, 26);
	private ZonedDecimalData[] t5646AgtoOld = ZDArrayPartOfStructure(12, 2, 0, t5646AgtosOld, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(t5646AgtosOld, 0, FILLER_REDEFINE);
	private ZonedDecimalData t5646Agto01Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 0);
	private ZonedDecimalData t5646Agto02Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 2);
	private ZonedDecimalData t5646Agto03Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4);
	private ZonedDecimalData t5646Agto04Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6);
	private ZonedDecimalData t5646Agto05Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 8);
	private ZonedDecimalData t5646Agto06Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 10);
	private ZonedDecimalData t5646Agto07Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 12);
	private ZonedDecimalData t5646Agto08Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 14);
	private ZonedDecimalData t5646Agto09Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 16);
	private ZonedDecimalData t5646Agto10Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 18);
	private ZonedDecimalData t5646Agto11Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 20);
	private ZonedDecimalData t5646Agto12Old = new ZonedDecimalData(2, 0).isAPartOf(filler1, 22);
	private FixedLengthStringData t5646FactorsasOld = new FixedLengthStringData(84).isAPartOf(t5646RecOld, 50);
	private ZonedDecimalData[] t5646FactorsaOld = ZDArrayPartOfStructure(12, 7, 4, t5646FactorsasOld, 0);
	private FixedLengthStringData filler2 = new FixedLengthStringData(84).isAPartOf(t5646FactorsasOld, 0, FILLER_REDEFINE);
	private ZonedDecimalData t5646Factorsa01Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 0);
	private ZonedDecimalData t5646Factorsa02Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 7);
	private ZonedDecimalData t5646Factorsa03Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 14);
	private ZonedDecimalData t5646Factorsa04Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 21);
	private ZonedDecimalData t5646Factorsa05Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 28);
	private ZonedDecimalData t5646Factorsa06Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 35);
	private ZonedDecimalData t5646Factorsa07Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 42);
	private ZonedDecimalData t5646Factorsa08Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 49);
	private ZonedDecimalData t5646Factorsa09Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 56);
	private ZonedDecimalData t5646Factorsa10Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 63);
	private ZonedDecimalData t5646Factorsa11Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 70);
	private ZonedDecimalData t5646Factorsa12Old = new ZonedDecimalData(7, 4).isAPartOf(filler2, 77);
}
}
