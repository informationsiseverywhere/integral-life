package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:46
 * Description:
 * Copybook name: MRTAARCKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Mrtaarckey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData mrtaarcFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData mrtaarcKey = new FixedLengthStringData(64).isAPartOf(mrtaarcFileKey, 0, REDEFINE);
  	public FixedLengthStringData mrtaarcChdrcoy = new FixedLengthStringData(1).isAPartOf(mrtaarcKey, 0);
  	public FixedLengthStringData mrtaarcChdrnum = new FixedLengthStringData(8).isAPartOf(mrtaarcKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(55).isAPartOf(mrtaarcKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(mrtaarcFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		mrtaarcFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}