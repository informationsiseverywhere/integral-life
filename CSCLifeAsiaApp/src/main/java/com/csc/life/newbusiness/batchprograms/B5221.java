package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ZchxpfTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*  SPLITTER PROGRAM (For Contract Issue [L2BCHISSUE] batch processing)
*  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*
* This program is to be run before the Contract Issue (B5222) program.
* It is run in single-thread, and writes the selected CHDR
* records to multiple files . Each of these files will be read
* by a copy of B5222 run in multi-thread.
*
* SQL will be used to access the CHDR physical file. The
* splitter program will extract CHDR records that meet the
* following criteria:
*
* i    chdrpfx    =  'CH'
* ii   validflag  =  '3'
* ii   chdrnum    between  <p6671-chdrnumfrom>
*                     and  <p6671-chdrnumto>
* iii  chdroy     =  <run-company>
*      order by CHDRPFX, CHDRCOY, CHDRNUM
*
* Control totals used in this program:
*
*    01  -  Number of Threads Used
*    02  -  No. of Proposals Read
*    03  -  No. Proposals Extracted
*
*
* Before the Splitter process is invoked the, the program CRTTMPF
* should be run. The CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's subsequent threads field. The temporary file
* will have the name ZCHXIS9999 where ZCHX is the name of temporary
* header file, IS is the first two characters of the 4th system parameter 
* and 9999 is the schedule run number. Each member added to the temporary 
* file will be called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and for each record it selects it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is spread the transaction records
* evenly across the thread members.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members Splitter programs
* should not be doing any further updates.
*
* There should be no non-standard CL commands in the CL Pgm which
* calls this program.
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The MTBE will be able to submit multiple versions of the program
* which use the file members created from this program. It is
* important that the process which runs the splitter program has
* the same 'number of subsequent threads' as the following
* process has defined in 'number of threads this process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
*****************************************************************
* </pre>
* 
* @author vhukumagrawa
*
*/
public class B5221 extends Mainb{
	
	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlchdrCursorrs = null;
	private java.sql.PreparedStatement sqlchdrCursorps = null;
	private java.sql.Connection sqlchdrCursorconn = null;
	private String sqlchdrCursor = "";
	private int chdrCursorLoopIndex = 0;
	public ZchxpfTableDAM zchxpf = new ZchxpfTableDAM();
	private DiskFileDAM zchx01 = new DiskFileDAM("ZCHX01");
	private DiskFileDAM zchx02 = new DiskFileDAM("ZCHX02");
	private DiskFileDAM zchx03 = new DiskFileDAM("ZCHX03");
	private DiskFileDAM zchx04 = new DiskFileDAM("ZCHX04");
	private DiskFileDAM zchx05 = new DiskFileDAM("ZCHX05");
	private DiskFileDAM zchx06 = new DiskFileDAM("ZCHX06");
	private DiskFileDAM zchx07 = new DiskFileDAM("ZCHX07");
	private DiskFileDAM zchx08 = new DiskFileDAM("ZCHX08");
	private DiskFileDAM zchx09 = new DiskFileDAM("ZCHX09");
	private DiskFileDAM zchx10 = new DiskFileDAM("ZCHX10");
	private DiskFileDAM zchx11 = new DiskFileDAM("ZCHX11");
	private DiskFileDAM zchx12 = new DiskFileDAM("ZCHX12");
	private DiskFileDAM zchx13 = new DiskFileDAM("ZCHX13");
	private DiskFileDAM zchx14 = new DiskFileDAM("ZCHX14");
	private DiskFileDAM zchx15 = new DiskFileDAM("ZCHX15");
	private DiskFileDAM zchx16 = new DiskFileDAM("ZCHX16");
	private DiskFileDAM zchx17 = new DiskFileDAM("ZCHX17");
	private DiskFileDAM zchx18 = new DiskFileDAM("ZCHX18");
	private DiskFileDAM zchx19 = new DiskFileDAM("ZCHX19");
	private DiskFileDAM zchx20 = new DiskFileDAM("ZCHX20");
	private ZchxpfTableDAM zchxpfRec = new ZchxpfTableDAM();
	private FixedLengthStringData zchx01Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx02Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx03Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx04Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx05Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx06Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx07Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx08Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx09Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx10Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx11Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx12Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx13Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx14Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx15Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx16Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx17Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx18Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx19Rec = new FixedLengthStringData(31);
	private FixedLengthStringData zchx20Rec = new FixedLengthStringData(31);
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B5221");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	
	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private String wsaa3 = "3";
	
	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	
	private FixedLengthStringData wsaaZchxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaZchxFn, 0, FILLER).init("ZCHX");
	private FixedLengthStringData wsaaZchxRunid = new FixedLengthStringData(2).isAPartOf(wsaaZchxFn, 4);
	private ZonedDecimalData wsaaZchxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaZchxFn, 6).setUnsigned();
	
	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");
	
	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);
	
	private FixedLengthStringData wsaaFetchArray = new FixedLengthStringData(31000);
	private FixedLengthStringData[] wsaaChdrData = FLSArrayPartOfStructure(1000, 31, wsaaFetchArray, 0);
	private FixedLengthStringData[] wsaaChdrpfx = FLSDArrayPartOfArrayStructure(2, wsaaChdrData, 0);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaChdrData, 2);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaChdrData, 3);
	private FixedLengthStringData[] wsaaStatcode = FLSDArrayPartOfArrayStructure(2, wsaaChdrData, 11);
	private FixedLengthStringData[] wsaaPstcde = FLSDArrayPartOfArrayStructure(2, wsaaChdrData, 13);
	
	/* WSAA-HOST-VARIABLES */
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	
	/* TABLES */
	private String t5679 = "T5679";
	/* ERRORS */
	private String ivrm = "IVRM";
	private String esql = "ESQL";
	/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	
	private IntegerData wsaaInd = new IntegerData();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaValidStat = "";
	
	/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	
	private P6671par p6671par = new P6671par();
	private T5679rec t5679rec = new T5679rec();
	
	private ZchxpfTableDAM zchxpfData = new ZchxpfTableDAM();	
	private boolean onePflag = false;	
	private static final String NBPRP124="NBPRP124";
	private static final String L2BCHISUJP="L2BCHISUJP"; 
	
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090
	}
	
	public B5221(){
		super();
	}
	
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
		}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
		}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
		}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
		}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
		}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
		}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
		}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
		}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
		}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
		}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
		}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
		}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
		}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
		}
	
	public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	
	protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}
	
	protected void initialise1000()
	{
		initialise1010();
	}

	protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		if (isGT(bprdIO.getThreadsSubsqntProc(),20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaCompany.set(bsprIO.getCompany());
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum,SPACES)
		&& isEQ(p6671par.chdrnum1,SPACES)) {
			wsaaChdrnumFrom.set(LOVALUES);
			wsaaChdrnumTo.set(HIVALUES);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		
		wsaaZchxRunid.set(bprdIO.getSystemParam04());
		wsaaZchxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		onePflag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), NBPRP124, appVars, "IT");
		sqlchdrCursor = " SELECT  CHDRPFX, CHDRCOY, CHDRNUM, STATCODE, PSTCDE" +
		" FROM   " + appVars.getTableNameOverriden("CHDR") + " " +
		" WHERE CHDRPFX = ?" +
		" AND CHDRCOY = ?" +
		" AND VALIDFLAG = ?" +
		" AND CHDRNUM >= ?" +
		" AND CHDRNUM <= ?" +
		" ORDER BY CHDRPFX, CHDRCOY, CHDRNUM";
		
		iy.set(1);
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlchdrCursorconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlchdrCursorps = appVars.prepareStatementEmbeded(sqlchdrCursorconn, sqlchdrCursor, "CHDR");
			appVars.setDBString(sqlchdrCursorps, 1, "CH");
			appVars.setDBString(sqlchdrCursorps, 2, wsaaCompany.toString());
			appVars.setDBString(sqlchdrCursorps, 3, wsaa3);
			appVars.setDBString(sqlchdrCursorps, 4, wsaaChdrnumFrom.toString());
			appVars.setDBString(sqlchdrCursorps, 5, wsaaChdrnumTo.toString());
			sqlchdrCursorrs = appVars.executeQuery(sqlchdrCursorps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}
	
	protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

	protected void openThreadMember1110()
	{
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(),1),iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaZchxFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz,varcom.oK)) {
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set("CLRTMPF");
			fatalError600();
		}
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(ZCHX");
		stringVariable1.append(iz.toString());
		stringVariable1.append(") TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaZchxFn.toString());
		stringVariable1.append(") MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		stringVariable1.append(" SEQONLY(*YES 1000)");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		if (isEQ(iz,1)) {
			zchx01.openOutput();
		}
		if (isEQ(iz,2)) {
			zchx02.openOutput();
		}
		if (isEQ(iz,3)) {
			zchx03.openOutput();
		}
		if (isEQ(iz,4)) {
			zchx04.openOutput();
		}
		if (isEQ(iz,5)) {
			zchx05.openOutput();
		}
		if (isEQ(iz,6)) {
			zchx06.openOutput();
		}
		if (isEQ(iz,7)) {
			zchx07.openOutput();
		}
		if (isEQ(iz,8)) {
			zchx08.openOutput();
		}
		if (isEQ(iz,9)) {
			zchx09.openOutput();
		}
		if (isEQ(iz,10)) {
			zchx10.openOutput();
		}
		if (isEQ(iz,11)) {
			zchx11.openOutput();
		}
		if (isEQ(iz,12)) {
			zchx12.openOutput();
		}
		if (isEQ(iz,13)) {
			zchx13.openOutput();
		}
		if (isEQ(iz,14)) {
			zchx14.openOutput();
		}
		if (isEQ(iz,15)) {
			zchx15.openOutput();
		}
		if (isEQ(iz,16)) {
			zchx16.openOutput();
		}
		if (isEQ(iz,17)) {
			zchx17.openOutput();
		}
		if (isEQ(iz,18)) {
			zchx18.openOutput();
		}
		if (isEQ(iz,19)) {
			zchx19.openOutput();
		}
		if (isEQ(iz,20)) {
			zchx20.openOutput();
		}
	}

	protected void initialiseArray1500()
	{
		/*START*/
		wsaaChdrpfx[wsaaInd.toInt()].set(SPACES);
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaStatcode[wsaaInd.toInt()].set(SPACES);
		wsaaPstcde[wsaaInd.toInt()].set(SPACES);
		/*EXIT*/
	}
	
	protected void readFile2000() 
	{
		try {
			readFile2010();
		}
		catch (GOTOException e){
		}
	}
	
	protected void readFile2010()
	{
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isNE(wsaaInd,1)) {
			goTo(GotoLabel.exit2090);
		}
		sqlerrorflag = false;
		try {
			for (chdrCursorLoopIndex = 1; isLTE(chdrCursorLoopIndex,wsaaRowsInBlock.toInt())
			&& sqlchdrCursorrs.next(); chdrCursorLoopIndex++ ){
				appVars.getDBObject(sqlchdrCursorrs, 1, wsaaChdrpfx[chdrCursorLoopIndex]);
				appVars.getDBObject(sqlchdrCursorrs, 2, wsaaChdrcoy[chdrCursorLoopIndex]);
				appVars.getDBObject(sqlchdrCursorrs, 3, wsaaChdrnum[chdrCursorLoopIndex]);
				appVars.getDBObject(sqlchdrCursorrs, 4, wsaaStatcode[chdrCursorLoopIndex]);
				appVars.getDBObject(sqlchdrCursorrs, 5, wsaaPstcde[chdrCursorLoopIndex]);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		if (isEQ(sqlca.getErrorCode(),100)
			&& isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
				wsspEdterror.set(varcom.endp);
			} else if (isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
				wsspEdterror.set(varcom.endp);
			} else {
				if (firstTime.isTrue()) {
					wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
					wsaaFirstTime.set("N");
			}
		}
		
	}
	
	protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}
	
	protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		for (wsaaInd.set(1); !(isGT(wsaaInd,wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}
	
	protected void loadThreads3100()
	{
		/*START*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()],wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		while ( !(isGT(wsaaInd,wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()],wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}
	
	protected void loadSameContracts3200()
	{
		// validate if risk status is valid
		wsaaValidStat = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)); wsaaSub.add(1)){			
			if(onePflag && L2BCHISUJP.equals(bsscIO.getScheduleName().trim())){
				checkStat3251();
			}else{
				checkStat3250();
			}
		}
		if(isEQ(wsaaValidStat,"Y")){
			start3260();
		}
		wsaaInd.add(1);
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		if (isLTE(wsaaInd,wsaaRowsInBlock) && isEQ(wsaaChdrnum[wsaaInd.toInt()],SPACES)) {
			wsaaEofInBlock.set("Y");
		}
		
	}
	
	protected void checkStat3250()
	{
		/*GO*/
		if (isEQ(wsaaStatcode[wsaaInd.toInt()],t5679rec.cnRiskStat[wsaaSub.toInt()])) {
			wsaaSub.set(13);
			wsaaValidStat = "Y";
		}		
		/*EXIT*/
	}
	
	protected void checkStat3251()
	{
		/*GO*/
		if (isEQ(wsaaStatcode[wsaaInd.toInt()],t5679rec.cnRiskStat[wsaaSub.toInt()]) && isEQ(wsaaPstcde[wsaaInd.toInt()],t5679rec.cnPremStat[wsaaSub.toInt()])) {
			wsaaSub.set(13);
			wsaaValidStat = "Y";
		}		
		/*EXIT*/
	}
	
	protected void start3260()
	{
		zchxpfData.chdrpfx.set(wsaaChdrpfx[wsaaInd.toInt()]);
		zchxpfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		zchxpfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		zchxpfData.statcode.set(wsaaStatcode[wsaaInd.toInt()]);
		zchxpfData.pstcde.set(wsaaPstcde[wsaaInd.toInt()]);
		if (isGT(iy,bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy,1)) {
			zchx01.write(zchxpfData);
		}
		if (isEQ(iy,2)) {
			zchx02.write(zchxpfData);
		}
		if (isEQ(iy,3)) {
			zchx03.write(zchxpfData);
		}
		if (isEQ(iy,4)) {
			zchx04.write(zchxpfData);
		}
		if (isEQ(iy,5)) {
			zchx05.write(zchxpfData);
		}
		if (isEQ(iy,6)) {
			zchx06.write(zchxpfData);
		}
		if (isEQ(iy,7)) {
			zchx07.write(zchxpfData);
		}
		if (isEQ(iy,8)) {
			zchx08.write(zchxpfData);
		}
		if (isEQ(iy,9)) {
			zchx09.write(zchxpfData);
		}
		if (isEQ(iy,10)) {
			zchx10.write(zchxpfData);
		}
		if (isEQ(iy,11)) {
			zchx11.write(zchxpfData);
		}
		if (isEQ(iy,12)) {
			zchx12.write(zchxpfData);
		}
		if (isEQ(iy,13)) {
			zchx13.write(zchxpfData);
		}
		if (isEQ(iy,14)) {
			zchx14.write(zchxpfData);
		}
		if (isEQ(iy,15)) {
			zchx15.write(zchxpfData);
		}
		if (isEQ(iy,16)) {
			zchx16.write(zchxpfData);
		}
		if (isEQ(iy,17)) {
			zchx17.write(zchxpfData);
		}
		if (isEQ(iy,18)) {
			zchx18.write(zchxpfData);
		}
		if (isEQ(iy,19)) {
			zchx19.write(zchxpfData);
		}
		if (isEQ(iy,20)) {
			zchx20.write(zchxpfData);
		}
		contotrec.totval.set(1);
		contotrec.totno.set(ct03);
		callContot001();
		
	}
	
	protected void commit3500() {
		/*COMMIT*/
		/*EXIT*/
	}
	
	protected void rollback3600() {
		/*ROLLBACK*/
		/*EXIT*/
	}
	
	protected void close4000() {

		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlchdrCursorconn, sqlchdrCursorps, sqlchdrCursorrs);
		for (iz.set(1); !(isGT(iz,bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	
	}
	
	protected void close4100()
	{
		close4110();
	}
	
	protected void close4110()
	{
		if (isEQ(iz,1)) {
			zchx01.close();
		}
		if (isEQ(iz,2)) {
			zchx02.close();
		}
		if (isEQ(iz,3)) {
			zchx03.close();
		}
		if (isEQ(iz,4)) {
			zchx04.close();
		}
		if (isEQ(iz,5)) {
			zchx05.close();
		}
		if (isEQ(iz,6)) {
			zchx06.close();
		}
		if (isEQ(iz,7)) {
			zchx07.close();
		}
		if (isEQ(iz,8)) {
			zchx08.close();
		}
		if (isEQ(iz,9)) {
			zchx09.close();
		}
		if (isEQ(iz,10)) {
			zchx10.close();
		}
		if (isEQ(iz,11)) {
			zchx11.close();
		}
		if (isEQ(iz,12)) {
			zchx12.close();
		}
		if (isEQ(iz,13)) {
			zchx13.close();
		}
		if (isEQ(iz,14)) {
			zchx14.close();
		}
		if (isEQ(iz,15)) {
			zchx15.close();
		}
		if (isEQ(iz,16)) {
			zchx16.close();
		}
		if (isEQ(iz,17)) {
			zchx17.close();
		}
		if (isEQ(iz,18)) {
			zchx18.close();
		}
		if (isEQ(iz,19)) {
			zchx19.close();
		}
		if (isEQ(iz,20)) {
			zchx20.close();
		}
	}

	protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set("ESQL");
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

}
