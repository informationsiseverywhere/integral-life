package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:26
 * Description:
 * Copybook name: ZSTREFFKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zstreffkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zstreffFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zstreffKey = new FixedLengthStringData(64).isAPartOf(zstreffFileKey, 0, REDEFINE);
  	public FixedLengthStringData zstreffChdrcoy = new FixedLengthStringData(1).isAPartOf(zstreffKey, 0);
  	public FixedLengthStringData zstreffCntbranch = new FixedLengthStringData(2).isAPartOf(zstreffKey, 1);
  	public PackedDecimalData zstreffEffdate = new PackedDecimalData(8, 0).isAPartOf(zstreffKey, 3);
  	public FixedLengthStringData zstreffBatctrcde = new FixedLengthStringData(4).isAPartOf(zstreffKey, 8);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(zstreffKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zstreffFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zstreffFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}