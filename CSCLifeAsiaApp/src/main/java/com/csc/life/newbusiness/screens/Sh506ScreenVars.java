package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH506
 * @version 1.0 generated on 30/08/09 07:01
 * @author Quipoz
 */
public class Sh506ScreenVars extends SmartVarModel { 

	//TMLII-294 AC-01-001 START
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	//TMLII-294 AC-01-001 START
	public FixedLengthStringData branchs = new FixedLengthStringData(20).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] branch = FLSArrayPartOfStructure(10, 2, branchs, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(branchs, 0, FILLER_REDEFINE);
	public FixedLengthStringData branch01 = DD.branch.copy().isAPartOf(filler,0);
	public FixedLengthStringData branch02 = DD.branch.copy().isAPartOf(filler,2);
	public FixedLengthStringData branch03 = DD.branch.copy().isAPartOf(filler,4);
	public FixedLengthStringData branch04 = DD.branch.copy().isAPartOf(filler,6);
	public FixedLengthStringData branch05 = DD.branch.copy().isAPartOf(filler,8);
	public FixedLengthStringData branch06 = DD.branch.copy().isAPartOf(filler,10);
	public FixedLengthStringData branch07 = DD.branch.copy().isAPartOf(filler,12);
	public FixedLengthStringData branch08 = DD.branch.copy().isAPartOf(filler,14);
	public FixedLengthStringData branch09 = DD.branch.copy().isAPartOf(filler,16);
	public FixedLengthStringData branch10 = DD.branch.copy().isAPartOf(filler,18);
	public FixedLengthStringData cflg = DD.cflg.copy().isAPartOf(dataFields,20);
	public ZonedDecimalData cnt = DD.cnt.copyToZonedDecimal().isAPartOf(dataFields,21);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData confirm = DD.confirm.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,25);
	public ZonedDecimalData expdays = DD.expdays.copyToZonedDecimal().isAPartOf(dataFields,29);
	public FixedLengthStringData freqcy = DD.freqcy.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData mandatorys = new FixedLengthStringData(2).isAPartOf(dataFields, 74);
	public FixedLengthStringData[] mandatory = FLSArrayPartOfStructure(2, 1, mandatorys, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(2).isAPartOf(mandatorys, 0, FILLER_REDEFINE);
	public FixedLengthStringData mandatory01 = DD.mand.copy().isAPartOf(filler1,0);
	public FixedLengthStringData mandatory02 = DD.mand.copy().isAPartOf(filler1,1);
	public ZonedDecimalData nofbbisf = DD.nofbbisf.copyToZonedDecimal().isAPartOf(dataFields,76);
	public ZonedDecimalData nofbbwrn = DD.nofbbwrn.copyToZonedDecimal().isAPartOf(dataFields,78);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,80);
	public FixedLengthStringData znfopts = new FixedLengthStringData(6).isAPartOf(dataFields, 85);
	public FixedLengthStringData[] znfopt = FLSArrayPartOfStructure(2, 3, znfopts, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(6).isAPartOf(znfopts, 0, FILLER_REDEFINE);
	public FixedLengthStringData znfopt01 = DD.znfopt.copy().isAPartOf(filler2,0);
	public FixedLengthStringData znfopt02 = DD.znfopt.copy().isAPartOf(filler2,3);
	public FixedLengthStringData zzsrce = DD.zzsrce.copy().isAPartOf(dataFields,91);
	public ZonedDecimalData yearInforce = DD.zzyrinf.copyToZonedDecimal().isAPartOf(dataFields,93);
	public FixedLengthStringData calcmeth = DD.calcmeth.copy().isAPartOf(dataFields,95);
	//TMLII-294 AC-01-007 START
	public FixedLengthStringData zdmsion = DD.zdmsion.copy().isAPartOf(dataFields,100);
	public FixedLengthStringData fatcaFlag = DD.fatcaFlag.copy().isAPartOf(dataFields,115);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	//TMLII-294 AC-01-007 END
	public FixedLengthStringData branchsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] branchErr = FLSArrayPartOfStructure(10, 4, branchsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(40).isAPartOf(branchsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData branch01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData branch02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData branch03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData branch04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData branch05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData branch06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData branch07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData branch08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData branch09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData branch10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData cflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData confirmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData expdaysErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData freqcyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData mandsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData[] mandErr = FLSArrayPartOfStructure(2, 4, mandsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(mandsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mand01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData mand02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData nofbbisfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData nofbbwrnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData znfoptsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData[] znfoptErr = FLSArrayPartOfStructure(2, 4, znfoptsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(znfoptsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData znfopt01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData znfopt02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData zzsrceErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData zzyrinfErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData calcmethErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	//TMLII-294 AC-01-007 START
	public FixedLengthStringData zdmsionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData fatcaFlagErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(372).isAPartOf(dataArea, 223);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	//TMLII-294 AC-01-007 END
	public FixedLengthStringData branchsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] branchOut = FLSArrayPartOfStructure(10, 12, branchsOut, 0);
	public FixedLengthStringData[][] branchO = FLSDArrayPartOfArrayStructure(12, 1, branchOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(120).isAPartOf(branchsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] branch01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] branch02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] branch03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] branch04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] branch05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] branch06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] branch07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] branch08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] branch09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] branch10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] cflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] confirmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] expdaysOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] freqcyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData mandsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 252);
	public FixedLengthStringData[] mandOut = FLSArrayPartOfStructure(2, 12, mandsOut, 0);
	public FixedLengthStringData[][] mandO = FLSDArrayPartOfArrayStructure(12, 1, mandOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(24).isAPartOf(mandsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mand01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] mand02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] nofbbisfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] nofbbwrnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData znfoptsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 312);
	public FixedLengthStringData[] znfoptOut = FLSArrayPartOfStructure(2, 12, znfoptsOut, 0);
	public FixedLengthStringData[][] znfoptO = FLSDArrayPartOfArrayStructure(12, 1, znfoptOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(znfoptsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] znfopt01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] znfopt02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] zzsrceOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] zzyrinfOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] calcmethOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	//TMLII-294 AC-01-007 START
	public FixedLengthStringData[] zdmsionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	//TMLII-294 AC-01-007 END
	public FixedLengthStringData[] fatcaFlagOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public FixedLengthStringData btchpr01Flag  = new FixedLengthStringData(1); //btchpr01Flag smalchi2


	public LongData Sh506screenWritten = new LongData(0);
	public LongData Sh506protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh506ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(mand01Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtableOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(mand02Out,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(indOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cflgOut,new String[] {"50",null, "-50",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(freqcyOut,new String[] {"12",null, "-12",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch01Out,new String[] {"20","01","-20",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch02Out,new String[] {"21","01","-21",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch03Out,new String[] {"22","01","-22",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch04Out,new String[] {"23","01","-23",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch05Out,new String[] {"24","01","-24",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch06Out,new String[] {"25","01","-25",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch07Out,new String[] {"26","01","-26",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch08Out,new String[] {"27","01","-27",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch09Out,new String[] {"28","01","-28",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(branch10Out,new String[] {"29","01","-29",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fatcaFlagOut,new String[] {"30","31","-30","32", null, null, null, null, null, null, null, null});

		screenFields = getscreenFields();
		screenErrFields = getscreenErrFields();
		screenOutFields = getscreenOutFields();
		
		screenDateFields = getscreenDateDispFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh506screen.class;
		protectRecord = Sh506protect.class;
	}
	
	public BaseData[] getscreenDateFields()
	{
		
		return new BaseData[] {};
		
	}
	
	public BaseData[] getscreenDateErrFields()
	{
		
		return new BaseData[] {};
		
	}
	
	public BaseData[] getscreenDateDispFields()
	{
		
		return new BaseData[] {};
		
	}
	
	public BaseData[] getscreenFields()
	{
		
		return new BaseData[] {company, tabl, item, longdesc, mandatory01, crtable, mandatory02, ind, cflg, indic, freqcy, cnt, branch01, branch02, branch03, branch04, branch05, branch06, branch07, branch08, branch09, branch10, zzsrce, znfopt01, confirm, znfopt02, yearInforce, expdays, nofbbwrn, nofbbisf,fatcaFlag};
		
	}
	
	public BaseData[][] getscreenOutFields()
	{
		
		return new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, mand01Out, crtableOut, mand02Out, indOut, cflgOut, indicOut, freqcyOut, cntOut, branch01Out, branch02Out, branch03Out, branch04Out, branch05Out, branch06Out, branch07Out, branch08Out, branch09Out, branch10Out, zzsrceOut, znfopt01Out, confirmOut, znfopt02Out, zzyrinfOut, expdaysOut, nofbbwrnOut, nofbbisfOut,fatcaFlagOut};
		
	}
	
	public BaseData[] getscreenErrFields()
	{
		
		return new BaseData[] {companyErr, tablErr, itemErr, longdescErr, mand01Err, crtableErr, mand02Err, indErr, cflgErr, indicErr, freqcyErr, cntErr, branch01Err, branch02Err, branch03Err, branch04Err, branch05Err, branch06Err, branch07Err, branch08Err, branch09Err, branch10Err, zzsrceErr, znfopt01Err, confirmErr, znfopt02Err, zzyrinfErr, expdaysErr, nofbbwrnErr, nofbbisfErr,fatcaFlagErr};
		
	}
	
	public int getDataAreaSize()
	{
		return 644;
	}
	
	public int getDataFieldsSize()
	{
		return 116;
	}
	public int getErrorIndicatorSize()
	{
		return 132;
	}
	public int getOutputFieldSize()
	{
		return 396;
	}

}
