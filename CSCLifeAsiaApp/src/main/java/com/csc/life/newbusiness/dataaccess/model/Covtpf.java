package com.csc.life.newbusiness.dataaccess.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Covtpf implements Serializable{
private long uniqueNumber;
private String chdrcoy;
private String chdrnum;
private String life;
private String coverage;
private String rider;
private String termid;
private int trdt;
private int trtm;
private int userT;
private String crtable;
private int rcesdte;
private int pcesdte;
private int rcesage;
private int pcesage;
private int rcestrm;
private int pcestrm;
private BigDecimal sumins;
private String liencd;
private String mortcls;
private String jlife;
private String rsunin;
private int rundte;
private int polinc;
private int numapp;
private int effdate;
private String sex01;
private String sex02;
private int anbccd01;
private int anbccd02;
private String billfreq;
private String billchnl;
private int seqnbr;
private BigDecimal singp;
private BigDecimal instprem;
private int plnsfx;
private int payrseqno;
private String cntcurr;
private int bcesage;
private int bcestrm;
private int bcesdte;
private String bappmeth;
private BigDecimal zbinstprem;
private BigDecimal zlinstprem;
private String zdivopt;
private String paycoy;
private String payclt;
private String paymth;
private String bankkey;
private String bankacckey;
private String paycurr;
private String facthous;
private String usrprf;
private String jobnm;
private String datime;
private BigDecimal loadper;
private BigDecimal rateadj;
private BigDecimal fltmort;
private BigDecimal premadj;
private BigDecimal ageadj;
private BigDecimal zstpduty01;
private String zclstate;
private String lnkgno;
private String lnkgsubrefno;
private String tpdtype;
private String lnkgind;
private String gmib; //ILIFE-8709
private String gmdb; //ILIFE-8709
private String gmwb; //ILIFE-8709
private String singpremtype; //ILIFE-8098
private BigDecimal riskprem; //ILIFE-7845
private BigDecimal cashvalarer;
private BigDecimal commPrem;  //IBPLIFE-5237

public Covtpf() {
	chdrcoy = "";
	chdrnum = "";
	life = "";
	coverage = "";
	rider = "";
	termid = "";
	trdt = 0;
	trtm = 0;
	userT = 0;
	crtable = "";
	rcesdte = 0;
	pcesdte = 0;
	rcesage = 0;
	pcesage = 0;
	rcestrm = 0;
	pcestrm = 0;
	sumins = BigDecimal.ZERO;
	liencd = "";
	mortcls = "";
	jlife = "";
	rsunin = "";
	rundte = 0;
	polinc = 0;
	numapp = 0;
	effdate = 0;
	sex01 = "";
	sex02 = "";
	anbccd01 = 0;
	anbccd02 = 0;
	billfreq = "";
	billchnl = "";
	seqnbr = 0;
	singp = BigDecimal.ZERO;
	instprem = BigDecimal.ZERO;
	plnsfx = 0;
	payrseqno = 0;
	cntcurr = "";
	bcesage = 0;
	bcestrm = 0;
	bcesdte = 0;
	bappmeth = "";
	zbinstprem = BigDecimal.ZERO;
	zlinstprem = BigDecimal.ZERO;
	zdivopt = "";
	paycoy = "";
	payclt = "";
	paymth = "";
	bankkey = "";
	bankacckey = "";
	paycurr = "";
	facthous = "";
	usrprf = "";
	jobnm = "";
	datime = "";
	loadper = BigDecimal.ZERO;
	rateadj = BigDecimal.ZERO;
	fltmort = BigDecimal.ZERO;
	premadj = BigDecimal.ZERO;
	ageadj = BigDecimal.ZERO;
	zstpduty01 = BigDecimal.ZERO;
	zclstate = "";
	lnkgno = "";
	lnkgsubrefno = "";
	tpdtype = "";
	lnkgind = "";
	singpremtype = "";//ILIFE-8098
	gmib = ""; //ILIFE-8709
	gmdb = ""; //ILIFE-8709
	gmwb = ""; //ILIFE-8709
	riskprem= BigDecimal.ZERO; //ILIFE-8709
	cashvalarer = BigDecimal.ZERO;
}
//ILIFE-8709 start
public Covtpf(Covtpf covtpf) {
	this.chdrcoy=covtpf.chdrcoy;
	this.chdrnum=covtpf.chdrnum;
	this.life=covtpf.life;
	this.jlife=covtpf.jlife;
	this.coverage=covtpf.coverage;
	this.rider=covtpf.rider;
	this.termid=covtpf.termid;
	this.trdt=covtpf.trdt;
	this.trtm=covtpf.trtm;
	this.crtable=covtpf.crtable;
	this.rcesdte=covtpf.rcesdte;
	this.pcesdte=covtpf.pcesdte;
	this.rcesage=covtpf.rcesage;
	this.pcesage=covtpf.pcesage;
	this.rcestrm=covtpf.rcestrm;
	this.pcestrm=covtpf.pcestrm;
	this.sumins=covtpf.sumins;
	this.liencd=covtpf.liencd;
	this.mortcls=covtpf.mortcls;
	this.jlife=covtpf.jlife;
	this.rsunin=covtpf.rsunin;
	this.rundte=covtpf.rundte;
	this.polinc=covtpf.polinc;
	this.numapp = covtpf.numapp;
	this.effdate=covtpf.effdate;
	this.sex01=covtpf.sex01;
	this.sex02=covtpf.sex02;
	this.anbccd01=covtpf.anbccd01;
	this.anbccd02=covtpf.anbccd02;
	this.billfreq=covtpf.billfreq;
	this.billchnl=covtpf.billchnl;
	this.seqnbr=covtpf.seqnbr;
	this.singp=covtpf.singp;
	this.instprem=covtpf.instprem;
	this.plnsfx=covtpf.plnsfx;
	this.payrseqno=covtpf.payrseqno;
	this.cntcurr=covtpf.cntcurr;
	this.bcesage=covtpf.bcesage;
	this.bcestrm=covtpf.bcestrm;
	this.bcesdte=covtpf.bcestrm;
	this.bappmeth=covtpf.bappmeth;
	this.zbinstprem=covtpf.zbinstprem;
  	this.zlinstprem=covtpf.zlinstprem;
  	this.zdivopt=covtpf.zdivopt;
  	this.paycoy=covtpf.paycoy;
  	this.payclt=covtpf.payclt;
  	this.paymth=covtpf.paymth;
  	this.bankkey=covtpf.bankkey;
  	this.bankacckey=covtpf.bankacckey;
  	this.paycurr=covtpf.paycurr;
  	this.facthous=covtpf.facthous;
  	this.usrprf=covtpf.usrprf;
  	this.jobnm=covtpf.jobnm;
  	this.datime=covtpf.datime;
  	this.loadper=covtpf.loadper;
  	this.rateadj=covtpf.rateadj;
  	this.fltmort=covtpf.fltmort;
  	this.premadj=covtpf.premadj;
  	this.ageadj=covtpf.ageadj;
  	this.zstpduty01=covtpf.zstpduty01;
  	this.zclstate=covtpf.zclstate;
  	this.lnkgno=covtpf.lnkgno;
  	this.lnkgsubrefno=covtpf.lnkgsubrefno;
  	this.tpdtype=covtpf.tpdtype;
  	this.lnkgind=covtpf.lnkgind;
  	this.gmib=covtpf.gmib;
  	this.gmdb=covtpf.gmdb;
  	this.gmwb=covtpf.gmwb;
  	this.riskprem=covtpf.riskprem;
  	this.uniqueNumber=covtpf.uniqueNumber; //ILIFE-8792
	
//ILIFE-8709 end
}

public BigDecimal getRiskprem() {
	return riskprem;
}
public void setRiskprem(BigDecimal riskprem) {
	this.riskprem = riskprem;
}
public long getUniqueNumber() {
	return uniqueNumber;
}
public void setUniqueNumber(long uniqueNumber) {
	this.uniqueNumber = uniqueNumber;
}
public String getChdrcoy() {
	return chdrcoy;
}
public void setChdrcoy(String chdrcoy) {
	this.chdrcoy = chdrcoy;
}
public String getChdrnum() {
	return chdrnum;
}
public void setChdrnum(String chdrnum) {
	this.chdrnum = chdrnum;
}
public String getLife() {
	return life;
}
public void setLife(String life) {
	this.life = life;
}
public String getCoverage() {
	return coverage;
}
public void setCoverage(String coverage) {
	this.coverage = coverage;
}
public String getRider() {
	return rider;
}
public void setRider(String rider) {
	this.rider = rider;
}
public String getTermid() {
	return termid;
}
public void setTermid(String termid) {
	this.termid = termid;
}
public int getTrdt() {
	return trdt;
}
public void setTrdt(int trdt) {
	this.trdt = trdt;
}
public int getTrtm() {
	return trtm;
}
public void setTrtm(int trtm) {
	this.trtm = trtm;
}
public int getUserT() {
	return userT;
}
public void setUserT(int userT) {
	this.userT = userT;
}
public String getCrtable() {
	return crtable;
}
public void setCrtable(String crtable) {
	this.crtable = crtable;
}
public int getRcesdte() {
	return rcesdte;
}
public void setRcesdte(int rcesdte) {
	this.rcesdte = rcesdte;
}
public int getPcesdte() {
	return pcesdte;
}
public void setPcesdte(int pcesdte) {
	this.pcesdte = pcesdte;
}
public int getRcesage() {
	return rcesage;
}
public void setRcesage(int rcesage) {
	this.rcesage = rcesage;
}
public int getPcesage() {
	return pcesage;
}
public void setPcesage(int pcesage) {
	this.pcesage = pcesage;
}
public int getRcestrm() {
	return rcestrm;
}
public void setRcestrm(int rcestrm) {
	this.rcestrm = rcestrm;
}
public int getPcestrm() {
	return pcestrm;
}
public void setPcestrm(int pcestrm) {
	this.pcestrm = pcestrm;
}
public BigDecimal getSumins() {
	return sumins;
}
public void setSumins(BigDecimal sumins) {
	this.sumins = sumins;
}
public String getLiencd() {
	return liencd;
}
public void setLiencd(String liencd) {
	this.liencd = liencd;
}
public String getMortcls() {
	return mortcls;
}
public void setMortcls(String mortcls) {
	this.mortcls = mortcls;
}
public String getJlife() {
	return jlife;
}
public void setJlife(String jlife) {
	this.jlife = jlife;
}
public String getRsunin() {
	return rsunin;
}
public void setRsunin(String rsunin) {
	this.rsunin = rsunin;
}
public int getRundte() {
	return rundte;
}
public void setRundte(int rundte) {
	this.rundte = rundte;
}
public int getPolinc() {
	return polinc;
}
public void setPolinc(int polinc) {
	this.polinc = polinc;
}
public int getNumapp() {
	return numapp;
}
public void setNumapp(int numapp) {
	this.numapp = numapp;
}
public int getEffdate() {
	return effdate;
}
public void setEffdate(int effdate) {
	this.effdate = effdate;
}
public String getSex01() {
	return sex01;
}
public void setSex01(String sex01) {
	this.sex01 = sex01;
}
public String getSex02() {
	return sex02;
}
public void setSex02(String sex02) {
	this.sex02 = sex02;
}
public int getAnbccd01() {
	return anbccd01;
}
public void setAnbccd01(int anbccd01) {
	this.anbccd01 = anbccd01;
}
public int getAnbccd02() {
	return anbccd02;
}
public void setAnbccd02(int anbccd02) {
	this.anbccd02 = anbccd02;
}
public String getBillfreq() {
	return billfreq;
}
public void setBillfreq(String billfreq) {
	this.billfreq = billfreq;
}
public String getBillchnl() {
	return billchnl;
}
public void setBillchnl(String billchnl) {
	this.billchnl = billchnl;
}
public int getSeqnbr() {
	return seqnbr;
}
public void setSeqnbr(int seqnbr) {
	this.seqnbr = seqnbr;
}
public BigDecimal getSingp() {
	return singp;
}
public void setSingp(BigDecimal singp) {
	this.singp = singp;
}
public BigDecimal getInstprem() {
	return instprem;
}
public void setInstprem(BigDecimal instprem) {
	this.instprem = instprem;
}
public int getPlnsfx() {
	return plnsfx;
}
public void setPlnsfx(int plnsfx) {
	this.plnsfx = plnsfx;
}
public int getPayrseqno() {
	return payrseqno;
}
public void setPayrseqno(int payrseqno) {
	this.payrseqno = payrseqno;
}
public String getCntcurr() {
	return cntcurr;
}
public void setCntcurr(String cntcurr) {
	this.cntcurr = cntcurr;
}
public int getBcesage() {
	return bcesage;
}
public void setBcesage(int bcesage) {
	this.bcesage = bcesage;
}
public int getBcestrm() {
	return bcestrm;
}
public void setBcestrm(int bcestrm) {
	this.bcestrm = bcestrm;
}
public int getBcesdte() {
	return bcesdte;
}
public void setBcesdte(int bcesdte) {
	this.bcesdte = bcesdte;
}
public String getBappmeth() {
	return bappmeth;
}
public void setBappmeth(String bappmeth) {
	this.bappmeth = bappmeth;
}
public BigDecimal getZbinstprem() {
	return zbinstprem;
}
public void setZbinstprem(BigDecimal zbinstprem) {
	this.zbinstprem = zbinstprem;
}
public BigDecimal getZlinstprem() {
	return zlinstprem;
}
public void setZlinstprem(BigDecimal zlinstprem) {
	this.zlinstprem = zlinstprem;
}
public String getZdivopt() {
	return zdivopt;
}
public void setZdivopt(String zdivopt) {
	this.zdivopt = zdivopt;
}
public String getPaycoy() {
	return paycoy;
}
public void setPaycoy(String paycoy) {
	this.paycoy = paycoy;
}
public String getPayclt() {
	return payclt;
}
public void setPayclt(String payclt) {
	this.payclt = payclt;
}
public String getPaymth() {
	return paymth;
}
public void setPaymth(String paymth) {
	this.paymth = paymth;
}
public String getBankkey() {
	return bankkey;
}
public void setBankkey(String bankkey) {
	this.bankkey = bankkey;
}
public String getBankacckey() {
	return bankacckey;
}
public void setBankacckey(String bankacckey) {
	this.bankacckey = bankacckey;
}
public String getPaycurr() {
	return paycurr;
}
public void setPaycurr(String paycurr) {
	this.paycurr = paycurr;
}
public String getFacthous() {
	return facthous;
}
public void setFacthous(String facthous) {
	this.facthous = facthous;
}
public String getUsrprf() {
	return usrprf;
}
public void setUsrprf(String usrprf) {
	this.usrprf = usrprf;
}
public String getJobnm() {
	return jobnm;
}
public void setJobnm(String jobnm) {
	this.jobnm = jobnm;
}
public String getDatime() {
	return datime;
}
public void setDatime(String datime) {
	this.datime = datime;
}
public BigDecimal getLoadper() {
	return loadper;
}
public void setLoadper(BigDecimal loadper) {
	this.loadper = loadper;
}
public BigDecimal getRateadj() {
	return rateadj;
}
public void setRateadj(BigDecimal rateadj) {
	this.rateadj = rateadj;
}
public BigDecimal getFltmort() {
	return fltmort;
}
public void setFltmort(BigDecimal fltmort) {
	this.fltmort = fltmort;
}
public BigDecimal getPremadj() {
	return premadj;
}
public void setPremadj(BigDecimal premadj) {
	this.premadj = premadj;
}
public BigDecimal getAgeadj() {
	return ageadj;
}
public void setAgeadj(BigDecimal ageadj) {
	this.ageadj = ageadj;
}
public BigDecimal getZstpduty01() {
	return zstpduty01;
}
public void setZstpduty01(BigDecimal zstpduty01) {
	this.zstpduty01 = zstpduty01;
}
public String getZclstate() {
	return zclstate;
}
public void setZclstate(String zclstate) {
	this.zclstate = zclstate;
}
public String getLnkgno() {
	return lnkgno;
}
public void setLnkgno(String lnkgno) {
	this.lnkgno = lnkgno;
}
public String getLnkgsubrefno() {
	return lnkgsubrefno;
}
public void setLnkgsubrefno(String lnkgsubrefno) {
	this.lnkgsubrefno = lnkgsubrefno;
}
public String getTpdtype() {
	return tpdtype;
}
public void setTpdtype(String tpdtype) {
	this.tpdtype = tpdtype;
}

public String getLnkgind() {
	return lnkgind;
}
public void setLnkgind(String lnkgind) {
	this.lnkgind=lnkgind;
}   
/*ILIFE-8098 Start*/
/**
 * @return the singpremtype
 */
public String getSingpremtype() {
	return singpremtype;
}
/**
 * @param singpremtype the singpremtype to set
 */
public void setSingpremtype(String singpremtype) {
	this.singpremtype = singpremtype;
}
/*ILIFE-8098 END*/
 //ILIFE-8709 start
public String getGmib() {
	return gmib;
}
public void setGmib(String gmib) {
	this.gmib = gmib;
}
public String getGmdb() {
	return gmdb;
}
public void setGmdb(String gmdb) {
	this.gmdb = gmdb;
}
public String getGmwb() {
	return gmwb;
}
public void setGmwb(String gmwb) {
	this.gmwb = gmwb;
}
 //ILIFE-8709 end
public BigDecimal getCashvalarer() {
	return cashvalarer;
}
public void setCashvalarer(BigDecimal cashvalarer) {
	this.cashvalarer = cashvalarer;
}
//IBPLIFE-5237
	public BigDecimal getCommPrem() {
		return commPrem;
	}
	public void setCommPrem(BigDecimal commPrem) {
		this.commPrem = commPrem;
	}	
}