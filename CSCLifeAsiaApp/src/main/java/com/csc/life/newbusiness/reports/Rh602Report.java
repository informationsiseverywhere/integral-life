package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH602.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh602Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData dummy = new FixedLengthStringData(1);
	private FixedLengthStringData forsdesc = new FixedLengthStringData(10);
	private ZonedDecimalData lapday01 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData lapday02 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData lapday03 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData lapday04 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData lapday05 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData lapday06 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData lapday07 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData lapday08 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData weeks01 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData weeks02 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData weeks03 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData weeks04 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData weeks05 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData weeks06 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData weeks07 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData weeks08 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData weeks09 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData weeks10 = new ZonedDecimalData(2, 0);
	private ZonedDecimalData year = new ZonedDecimalData(4, 0);
	private ZonedDecimalData zpolcnt = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt11 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt12 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt13 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt14 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt15 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt16 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt17 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt21 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt22 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt23 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt24 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt25 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData zpolcnt26 = new ZonedDecimalData(5, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh602Report() {
		super();
	}


	/**
	 * Print the XML for Rh602d01
	 */
	public void printRh602d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(7).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		zpolcnt.setFieldName("zpolcnt");
		zpolcnt.setInternal(subString(recordData, 1, 5));
		printLayout("Rh602d01",			// Record name
			new BaseData[]{			// Fields:
				zpolcnt
			}
			, new Object[] {			// indicators
				new Object[]{"ind01", indicArea.charAt(1)},
				new Object[]{"ind02", indicArea.charAt(2)},
				new Object[]{"ind03", indicArea.charAt(3)},
				new Object[]{"ind04", indicArea.charAt(4)},
				new Object[]{"ind05", indicArea.charAt(5)},
				new Object[]{"ind06", indicArea.charAt(6)},
				new Object[]{"ind07", indicArea.charAt(7)}
			}
		);

	}

	/**
	 * Print the XML for Rh602d02
	 */
	public void printRh602d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(24).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		zpolcnt11.setFieldName("zpolcnt11");
		zpolcnt11.setInternal(subString(recordData, 1, 5));
		zpolcnt12.setFieldName("zpolcnt12");
		zpolcnt12.setInternal(subString(recordData, 6, 5));
		zpolcnt13.setFieldName("zpolcnt13");
		zpolcnt13.setInternal(subString(recordData, 11, 5));
		zpolcnt14.setFieldName("zpolcnt14");
		zpolcnt14.setInternal(subString(recordData, 16, 5));
		zpolcnt15.setFieldName("zpolcnt15");
		zpolcnt15.setInternal(subString(recordData, 21, 5));
		zpolcnt16.setFieldName("zpolcnt16");
		zpolcnt16.setInternal(subString(recordData, 26, 5));
		zpolcnt17.setFieldName("zpolcnt17");
		zpolcnt17.setInternal(subString(recordData, 31, 5));
		printLayout("Rh602d02",			// Record name
			new BaseData[]{			// Fields:
				zpolcnt11,
				zpolcnt12,
				zpolcnt13,
				zpolcnt14,
				zpolcnt15,
				zpolcnt16,
				zpolcnt17
			}
			, new Object[] {			// indicators
				new Object[]{"ind12", indicArea.charAt(12)},
				new Object[]{"ind24", indicArea.charAt(24)}
			}
		);

	}

	/**
	 * Print the XML for Rh602d03
	 */
	public void printRh602d03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(42).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		zpolcnt21.setFieldName("zpolcnt21");
		zpolcnt21.setInternal(subString(recordData, 1, 5));
		zpolcnt22.setFieldName("zpolcnt22");
		zpolcnt22.setInternal(subString(recordData, 6, 5));
		zpolcnt23.setFieldName("zpolcnt23");
		zpolcnt23.setInternal(subString(recordData, 11, 5));
		zpolcnt24.setFieldName("zpolcnt24");
		zpolcnt24.setInternal(subString(recordData, 16, 5));
		zpolcnt25.setFieldName("zpolcnt25");
		zpolcnt25.setInternal(subString(recordData, 21, 5));
		zpolcnt26.setFieldName("zpolcnt26");
		zpolcnt26.setInternal(subString(recordData, 26, 5));
		printLayout("Rh602d03",			// Record name
			new BaseData[]{			// Fields:
				zpolcnt21,
				zpolcnt22,
				zpolcnt23,
				zpolcnt24,
				zpolcnt25,
				zpolcnt26
			}
			, new Object[] {			// indicators
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind21", indicArea.charAt(21)},
				new Object[]{"ind28", indicArea.charAt(28)},
				new Object[]{"ind35", indicArea.charAt(35)},
				new Object[]{"ind42", indicArea.charAt(42)}
			}
		);

	}

	/**
	 * Print the XML for Rh602e01
	 */
	public void printRh602e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		dummy.setFieldName("dummy");
		dummy.setInternal(subString(recordData, 1, 1));
		printLayout("Rh602e01",			// Record name
			new BaseData[]{			// Fields:
				dummy
			}
		);

	}

	/**
	 * Print the XML for Rh602h01
	 */
	public void printRh602h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		forsdesc.setFieldName("forsdesc");
		forsdesc.setInternal(subString(recordData, 1, 10));
		year.setFieldName("year");
		year.setInternal(subString(recordData, 11, 4));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 15, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 16, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 46, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 56, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 58, 30));
		time.setFieldName("time");
		time.set(getTime());
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 88, 3));
		cntdesc.setFieldName("cntdesc");
		cntdesc.setInternal(subString(recordData, 91, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rh602h01",			// Record name
			new BaseData[]{			// Fields:
				forsdesc,
				year,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				cnttype,
				cntdesc,
				pagnbr
			}
		);

		currentPrintLine.set(9);
	}

	/**
	 * Print the XML for Rh602h02
	 */
	public void printRh602h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		dummy.setFieldName("dummy");
		dummy.setInternal(subString(recordData, 1, 1));
		printLayout("Rh602h02",			// Record name
			new BaseData[]{			// Fields:
				dummy
			}
		);

	}

	/**
	 * Print the XML for Rh602h03
	 */
	public void printRh602h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(72).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		lapday01.setFieldName("lapday01");
		lapday01.setInternal(subString(recordData, 1, 2));
		lapday02.setFieldName("lapday02");
		lapday02.setInternal(subString(recordData, 3, 2));
		lapday03.setFieldName("lapday03");
		lapday03.setInternal(subString(recordData, 5, 2));
		lapday04.setFieldName("lapday04");
		lapday04.setInternal(subString(recordData, 7, 2));
		lapday05.setFieldName("lapday05");
		lapday05.setInternal(subString(recordData, 9, 2));
		lapday06.setFieldName("lapday06");
		lapday06.setInternal(subString(recordData, 11, 2));
		lapday07.setFieldName("lapday07");
		lapday07.setInternal(subString(recordData, 13, 2));
		lapday08.setFieldName("lapday08");
		lapday08.setInternal(subString(recordData, 15, 2));
		printLayout("Rh602h03",			// Record name
			new BaseData[]{			// Fields:
				lapday01,
				lapday02,
				lapday03,
				lapday04,
				lapday05,
				lapday06,
				lapday07,
				lapday08
			}
			, new Object[] {			// indicators
				new Object[]{"ind71", indicArea.charAt(71)},
				new Object[]{"ind72", indicArea.charAt(72)}
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rh602h04
	 */
	public void printRh602h04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(74).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		weeks01.setFieldName("weeks01");
		weeks01.setInternal(subString(recordData, 1, 2));
		weeks02.setFieldName("weeks02");
		weeks02.setInternal(subString(recordData, 3, 2));
		weeks03.setFieldName("weeks03");
		weeks03.setInternal(subString(recordData, 5, 2));
		weeks04.setFieldName("weeks04");
		weeks04.setInternal(subString(recordData, 7, 2));
		weeks05.setFieldName("weeks05");
		weeks05.setInternal(subString(recordData, 9, 2));
		weeks06.setFieldName("weeks06");
		weeks06.setInternal(subString(recordData, 11, 2));
		weeks07.setFieldName("weeks07");
		weeks07.setInternal(subString(recordData, 13, 2));
		weeks08.setFieldName("weeks08");
		weeks08.setInternal(subString(recordData, 15, 2));
		weeks09.setFieldName("weeks09");
		weeks09.setInternal(subString(recordData, 17, 2));
		weeks10.setFieldName("weeks10");
		weeks10.setInternal(subString(recordData, 19, 2));
		printLayout("Rh602h04",			// Record name
			new BaseData[]{			// Fields:
				weeks01,
				weeks02,
				weeks03,
				weeks04,
				weeks05,
				weeks06,
				weeks07,
				weeks08,
				weeks09,
				weeks10
			}
			, new Object[] {			// indicators
				new Object[]{"ind73", indicArea.charAt(73)},
				new Object[]{"ind74", indicArea.charAt(74)}
			}
		);

	}

	/**
	 * Print the XML for Rh602u01
	 */
	public void printRh602u01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh602u01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}


}
