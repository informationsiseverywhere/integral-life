/*
 * File: Pt008.java
*/
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.accounting.dataaccess.CcdtTableDAM;
import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.procedures.Usrcypind;
import com.csc.fsu.general.recordstructures.Usrcypirec;
import com.csc.fsu.general.tablestructures.T3678rec;
import com.csc.fsu.general.tablestructures.T3684rec;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.screens.Sr57jScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.CreditCardUtility;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.MaskingUtil;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr57j extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR57J");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);
	private final String wsaaLargeName = "LGNMS";

	private FixedLengthStringData wsbbBankdesc = new FixedLengthStringData(60);
	private FixedLengthStringData wsbbBankdescLine1 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 0);
	private FixedLengthStringData wsbbBankdescLine2 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 30);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);

	private FixedLengthStringData wsaaPaychgFlg = new FixedLengthStringData(1).init(SPACES);
	private Validator paymentChange = new Validator(wsaaPaychgFlg, "Y");


	private FixedLengthStringData wsaaSaveChdrnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaValidCcStatus = new FixedLengthStringData(2);
	private Validator validCcStatus = new Validator(wsaaValidCcStatus, "IF");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaDate = new FixedLengthStringData(8).isAPartOf(wsaaToday, 0, REDEFINE);
	private ZonedDecimalData wsaaYyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaDate, 0).setUnsigned();
	private ZonedDecimalData wsaaMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 4).setUnsigned();

		/* TABLES */
	private CcdtTableDAM ccdtIO = new CcdtTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private T3678rec t3678rec = new T3678rec();
	private T3684rec t3684rec = new T3684rec();
	private Usrcypirec usrcypirec = new Usrcypirec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wssplife wssplife = new Wssplife();
	private Sr57jScreenVars sv = ScreenProgram.getScreenVars( Sr57jScreenVars.class);
	private FixedLengthStringData wsaaMandateFlag = new FixedLengthStringData(1);
	private Validator mandateFound = new Validator(wsaaMandateFlag, "Y");
	
	//ILB-489 by wli31
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private BabrpfDAO babrpfDao = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private Mandpf mandccdIO;	
	private Clbapf clblIO = null;
	private Babrpf babrIO = null;
	private Chdrpf chdrpf = new Chdrpf();
	private Payrpf payrpf = new Payrpf();
	private PayrTableDAM payrIO = new PayrTableDAM();//ILIFE-8393
	private Descpf descpf = null;
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();//ILIFE-8393
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090
	}

	public Pr57j() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57j", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	@Override
	protected void initialise1000()	{
		initialise1010();
	}

	protected void initialise1010()	{
		wsspcomn.bchaction.set(SPACES);
		sv.dataArea.set(SPACES);
		wsaaSaveChdrnum.set(SPACES);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		//added for ILIFE-8393
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		wsaaPaychgFlg.set(" ");
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			wsaaPaychgFlg.set("Y");
			chdrpf = chdrpfDAO.getCacheObject(chdrpf);
			if (chdrpf == null) {
				fatalError600();
			}
			chdrlnbIO.setParams(SPACES);
			chdrlnbIO.setFormat("CHDRLNBREC");
			chdrlnbIO.setChdrcoy(chdrpf.getChdrcoy());
			chdrlnbIO.setChdrnum(chdrpf.getChdrnum());
			chdrlnbIO.setValidflag("1");
			chdrlnbIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(chdrlnbIO.getStatuz());
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
		}
		wsaaSaveChdrnum.set(chdrlnbIO.getChdrnum());

		payrpf = payrpfDAO.getCacheObject(payrpf);
		if(null==payrpf) {
			payrIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)&& isNE(payrIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}else {
				payrpf = payrpfDAO.getpayrRecord(payrIO.getChdrcoy().toString(), payrIO.getChdrnum().toString());
				if(null==payrpf) {
					fatalError600();
				}else {
					payrpfDAO.setCacheObject(payrpf);
				}
			}
		}

		//end
		/*    Retrieve the payer details.*/
		
		sv.payrnum.set(chdrlnbIO.getCownnum());
		sv.numsel.set(chdrlnbIO.getCownnum());
		/* TSD 321 Phase 2 Starts */
		if (isNE(chdrlnbIO.getPayclt(),SPACES)) {
			sv.payrnum.set(chdrlnbIO.getPayclt());
			sv.numsel.set(chdrlnbIO.getCownnum());
		}
		/* TSD 321 Phase 2 Ends */
		a1000GetPayorname();
		sv.payorname.set(namadrsrec.name);
		wsaaClntkey.set(SPACES);
		if (isNE(chdrlnbIO.getCownnum(), SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrlnbIO.getCownpfx());
			stringVariable1.addExpression(chdrlnbIO.getCowncoy());
			stringVariable1.addExpression(chdrlnbIO.getCownnum());
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		sv.currcode.set(SPACES);
		sv.facthous.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.billcd.set(payrpf.getBillcd());
		if (isNE(chdrlnbIO.getZmandref(), SPACES)) {
			sv.ccmndref.set(chdrlnbIO.getZmandref());
		}
		else {
			sv.ccmndref.set(SPACES);
			return ;
		}
		if (isEQ(chdrlnbIO.getZmandref(), SPACES)) {
			return ;
		}
		/* Get client bank account details from MANDPF*/
	
		mandccdIO = mandpfDAO.searchMandpfRecordData(wsspcomn.fsuco.toString(),sv.payrnum.toString(),chdrlnbIO.getZmandref().toString(),"MANDCCD");
		if (mandccdIO == null) {
			sv.ccmndrefErr.set("H929");
			return ;
		}
		/* Read CLBL using information obtained from MAND                  */
		sv.bankkey.set(mandccdIO.getBankkey());
		sv.bankacckey.set(mandccdIO.getBankacckey());
		String bankacckey = mandccdIO.getBankacckey();
		String creditCardInformation=CreditCardUtility.getInstance().getCreditCard(mandccdIO.getBankacckey());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			bankacckey = creditCardInformation;
			sv.bankacckey.set(creditCardInformation);
		}
		sv.effdate.set(mandccdIO.getEffdate());
		sv.mandstat.set(mandccdIO.getMandstat());
		a1400Decryption();
		//ILIFE-2823 Starts
		ccdtIO.setParams(SPACES);
		ccdtIO.setFormat("CCDTREC");
		ccdtIO.setCrdtcard(mandccdIO.getBankacckey());
		ccdtIO.setCrdtcard(creditCardInformation);
		ccdtIO.setCrcardexpy("9999");
		ccdtIO.setCrcardexpm("99");
		ccdtIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ccdtIO);
		if (isNE(ccdtIO.getStatuz(), varcom.oK)
		&& isNE(ccdtIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ccdtIO.getStatuz());
			syserrrec.params.set(ccdtIO.getParams());
			fatalError600();
		}
		/* If invalid Credit Card status issue an error*/
		wsaaValidCcStatus.set(ccdtIO.getCrcstat());

		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), "TR338",ccdtIO.getBnkactyp().toString(),wsspcomn.fsuco.toString(),wsspcomn.language.toString());
		if (descpf != null) {
			sv.cdlongdesc.set(descpf.getLongdesc());
		}
		else {
			descpf = descDAO.getdescData(smtpfxcpy.item.toString(), "TR338",ccdtIO.getBnkactyp().toString(),wsspcomn.fsuco.toString(),"E");
			if (descpf != null) {
				sv.cdlongdesc.set(descpf.getLongdesc());
			}
			else {
				sv.cdlongdesc.fill(" ");
			}
		}
		//ILIFE-2823 Ends
		String referenceNumber=CreditCardUtility.getInstance().getReference(bankacckey.trim());/* IJTI-1523 */
		if(referenceNumber!=null && !referenceNumber.isEmpty()){
			bankacckey = referenceNumber;
		}
		clblIO = clbapfDAO.searchClblData(mandccdIO.getBankkey(), bankacckey, wsspcomn.fsuco.toString(),chdrlnbIO.getPayclt().toString());
		if(clblIO == null){
			fatalError600();
		}
		/* If the payor number is not the same as the client number*/
		if (isNE(chdrlnbIO.getCownnum(), clblIO.getClntnum())) {
				sv.payrnumErr.set("F373");
		}
		if (isNE(clblIO.getClntnum(), sv.payrnum)) {
			sv.facthous.set(SPACES);
			sv.longdesc.set(SPACES);
			sv.bankkey.set(SPACES);
			sv.bankacckey.set(SPACES);
			return ;
		}
		/* Get the bank/branch description.*/
		babrIO = babrpfDao.searchBankkey(sv.bankkey.toString());
		/*    Set screen fields*/
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		//a1400Decryption();
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		sv.facthous.set(clblIO.getFacthous());
		if (isNE(sv.facthous, SPACES)) {
			a1200GetFacthousDesc();
		}
		/* If the current to date is less than today, the record is        */
		/* inactive                                                        */
		if (isNE(clblIO.getCurrto(), ZERO)) {
			if (isLT(clblIO.getCurrto(), wsaaToday)) {
				sv.bankacckeyErr.set("RFJX");
				return ;
			}
		}
		/* If the current from date is greater than the effective date     */
		/* of the mandate                                                  */
		if (paymentChange.isTrue()) {
			if (isGT(clblIO.getCurrfrom(), mandccdIO.getEffdate())) {
				sv.bankacckeyErr.set("RFKG");
				return ;
			}
		}
		/* If the current to date of is less than the effective date       */
		/* of the mandate                                                  */
		if (paymentChange.isTrue()) {
			if (isNE(clblIO.getCurrto(), ZERO)) {
				if (isLT(clblIO.getCurrto(), mandccdIO.getEffdate())) {
					sv.bankacckeyErr.set("RFKG");
					return ;
				}
			}
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* If in enquiry mode, protect field*/
		if (isEQ(wsspcomn.flag, "I")) {
			sv.ccmndrefOut[varcom.pr.toInt()].set("Y");
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void screenIo2010() {
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/*    If F12 pressed bypass validation.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/*    If F5 pressed then set a srceen error so the screen*/
		/*    is re-displayed.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if(isEQ(sv.payrnum, SPACES)){
			sv.payrnum.set(chdrlnbIO.getCownnum());
		}
		if (!"".equals(chdrpf.getCownnum())) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrlnbIO.getCownpfx());
			stringVariable1.addExpression(chdrlnbIO.getCowncoy());
			stringVariable1.addExpression(sv.payrnum);
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		/* TSD 321 Phase 2 Starts */
		Clntpf cltsIO = clntpfDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(), sv.payrnum.toString());
		//TODO: check cltsrec
//		cltsIO.setFormat(cltsrec);
		
		if (cltsIO == null) {
			sv.payrnumErr.set("E335");
			wsspcomn.edterror.set("Y");
			sv.payorname.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
		a1000GetPayorname();
		sv.payorname.set(namadrsrec.name);
		/* TSD 321 Phase 2 Ends */

//		mandccdIO = mandpfDAO.searchMandpfRecordData(wsspcomn.fsuco.toString(),sv.payrnum.toString(),"99999","MANDCCD");
//		if(mandccdIO == null) {
//			sv.ccmndrefErr.set("RFSM");
//			wsspcomn.edterror.set("Y");
//			goTo(GotoLabel.exit2090);
//		}
		/* To ensure that a mandate reference number is entered*/
		if (isEQ(sv.ccmndref, SPACES)) {
			sv.ccmndrefErr.set("H926");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Is mandate reference number entered numeric ?*/
		if (isNE(sv.ccmndref, SPACES)
		&& isNE(sv.ccmndref, NUMERIC)) {
			sv.ccmndrefErr.set("H421");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Read mandate file*/
		mandccdIO = mandpfDAO.searchMandpfRecordData(wsspcomn.fsuco.toString(),sv.payrnum.toString(),sv.ccmndref.toString(),"MANDCCD");
		if (mandccdIO == null||
		    "C".equals(mandccdIO.getPayind())) {
			/* TSD 321 Phase 2 Starts */
			//sv.ccmndrefErr.set("h928);
			sv.ccmndrefErr.set("RFSM");
			/* TSD 321 Phase 2 Ends */
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Move bank account details to corresponding fields*/
		sv.bankkey.set(mandccdIO.getBankkey());
		sv.bankacckey.set(mandccdIO.getBankacckey());
		String creditCardInformation=CreditCardUtility.getInstance().getCreditCard(sv.bankacckey.toString().trim());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			sv.bankacckey.set(creditCardInformation);
		}
		sv.effdate.set(mandccdIO.getEffdate());
		sv.mandstat.set(mandccdIO.getMandstat());
		a1400Decryption();
		
		//Check Feature Configuration for MTL321 and proceed to do this feature only if this feature is set to true
		//validate client role
		validateClientRole2010();

	}
	
	/* TSD 321 Phase 2 Starts */
	protected void validateClientRole2010() {
		
		wsaaChdrnum.set(payrpf.getChdrnum());
		wsaaPayrseqno.set(payrpf.getPayrseqno());
		
		Clrfpf clrfIO = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrlnbIO.getChdrpfx().toString(), payrpf.getChdrcoy(),wsaaForenum.toString(), "PY");
		if(clrfIO != null && !clrfIO.getClntnum().equals(sv.payrnum.toString())) {
			clrfIO = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrlnbIO.getChdrpfx().toString(), payrpf.getChdrcoy(),wsaaForenum.toString(), "LA");
			if(clrfIO != null && !clrfIO.getClntnum().equals(sv.payrnum.toString())) {
				clrfIO = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrlnbIO.getChdrpfx().toString(), payrpf.getChdrcoy(),wsaaForenum.toString(), "OW");
			}
			if(clrfIO == null || !clrfIO.getClntnum().equals(sv.payrnum.toString())) {
				scrnparams.errorCode.set("RFSN");
			}
		}
	}
	/* TSD 321 Phase 2 Ends */

	protected void validate2020() {
		Itempf itempf = null;
		if (paymentChange.isTrue()) {
			itempf = new Itempf();
		    itempf.setItempfx(smtpfxcpy.item.toString());
			itempf.setItemcoy(wsspcomn.fsuco.toString());
			itempf.setItemtabl("T3678");
			itempf.setItemitem(mandccdIO.getMandstat().trim());
		    itempf = itemDAO.getItempfRecord(itempf);
		    if (itempf == null) {
				fatalError600();
			}
		    t3678rec.t3678Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			if (isEQ(t3678rec.gonogoflg, "N")) {
				sv.ccmndrefErr.set("I014");
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		/* Get the bank and branch descriptions in case*/
		/* the mandate has changed*/
		/* Check the bank/branch.*/
		
		babrIO = babrpfDao.searchBankkey(sv.bankkey.toString());
		/* Bank/Branch description not found.*/
		if (babrIO == null) {
			sv.bankkeyErr.set("E756");
			goTo(GotoLabel.checkForErrors2080);
		}
		/* Move the bank key description to the screen.*/
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		/* Get the client number.*/
		
		String bankacckey = sv.bankacckey.toString();
		String creditCardInformation=CreditCardUtility.getInstance().getCreditCard(sv.bankacckey.toString().trim());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			bankacckey = creditCardInformation;
		}
		String referenceNumber=CreditCardUtility.getInstance().getReference(bankacckey.trim());
		if(referenceNumber!=null && !referenceNumber.isEmpty()){
			bankacckey = referenceNumber;
		}
//		ix.set(20);
//		while ( !(isNE(subString(bankacckey, ix, 1), SPACES))) {
//			compute(ix, 0).set(sub(ix, 1));
//		}
		
		/*if (isNE(tr29urec.subrname, SPACES)
		&& isEQ(subString(clblIO.getBankacckey(), 1, ix), NUMERIC)) {
			cipherrec.lfunc.set("ENCR");
			cipherrec.lsrc.set(clblIO.getBankacckey());
			a1600CallTr29uSubrname();
			clblIO.setBankacckey(cipherrec.lrcv);
		}*/
		//clblIO.setBankacckey(sv.bankacckey);
		
		clblIO = clbapfDAO.searchClblData(sv.bankkey.toString().trim(), bankacckey, wsspcomn.fsuco.toString(),sv.payrnum.toString());
		/* Client bank details not found.*/
		if (clblIO == null) {
			sv.bankacckeyErr.set("G600");
			goTo(GotoLabel.checkForErrors2080);
		}
		/* If the client number is not the same as the payor number.*/
		if (!clblIO.getClntnum().equals(sv.payrnum.toString())) {
			sv.bankacckeyErr.set("F373");
			goTo(GotoLabel.checkForErrors2080);
		}
		/* If the current to date is less than today, the record is*/
		/* inactive*/
		if (clblIO.getCurrto() != 0) {
			if (clblIO.getCurrto() < wsaaToday.toInt()) {
				sv.bankacckeyErr.set("RFJX");
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		/* If the current from date is greater than the effective date*/
		/* of the mandate*/
		if (paymentChange.isTrue()) {
			if (clblIO.getCurrfrom() > mandccdIO.getEffdate()) {
				sv.bankacckeyErr.set("RFKG");
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		/* If the current to date of is less than the effective date*/
		/* of the mandate*/
		if (paymentChange.isTrue()) {
			if (clblIO.getCurrto() != 0) {
				if (clblIO.getCurrto() < mandccdIO.getEffdate()) {
					sv.bankacckeyErr.set("RFKG");
					goTo(GotoLabel.checkForErrors2080);
				}
			}
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
		if (isNE(sv.facthous, SPACES)) {
			a1200GetFacthousDesc();
		}
		/*  An indicator has been included in T3684 (Factoring House)*/
		/*  extra data screen to allow user to indicate if the same*/
		/*  mandate reference can be used by more than 1 policy or*/
		/*  it has to be unique per policy.  Program to incorporate*/
		/*  this change.*/
		
		itempf = new Itempf();
	    itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(wsspcomn.fsuco.toString());
		itempf.setItemtabl("T3684");
		itempf.setItemitem(sv.facthous.toString().trim());
	    itempf = itemDAO.getItempfRecord(itempf);
	    if (itempf == null) {
			fatalError600();
		}
	    t3684rec.t3684Rec.set(StringUtil.rawToString(itempf.getGenarea()));

		ccdtIO.setParams(SPACES);
		ccdtIO.setFormat("CCDTREC");
		ccdtIO.setCrdtcard(mandccdIO.getBankacckey());
		creditCardInformation=CreditCardUtility.getInstance().getCreditCard(ccdtIO.getCrdtcard().toString().trim());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			ccdtIO.setCrdtcard(creditCardInformation);
		}
		ccdtIO.setCrcardexpy("9999");
		ccdtIO.setCrcardexpm("99");
		ccdtIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ccdtIO);
		if (isNE(ccdtIO.getStatuz(), varcom.oK)
		&& isNE(ccdtIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ccdtIO.getStatuz());
			syserrrec.params.set(ccdtIO.getParams());
			fatalError600();
		}
		/* If invalid Credit Card status issue an error*/
		wsaaValidCcStatus.set(ccdtIO.getCrcstat());
		//ILIFE-2823 Starts
		descpf = descDAO.getdescData(smtpfxcpy.item.toString(), "TR338",ccdtIO.getBnkactyp().toString(),wsspcomn.fsuco.toString(),wsspcomn.language.toString());
		
		if (descpf != null) {
			sv.cdlongdesc.set(descpf.getLongdesc());
		}
		else {
			descpf = descDAO.getdescData(smtpfxcpy.item.toString(), "TR338",ccdtIO.getBnkactyp().toString(),wsspcomn.fsuco.toString(),"E");
			if (descpf != null) {
				sv.cdlongdesc.set(descpf.getLongdesc());
			}
			else {
				sv.cdlongdesc.fill(" ");
			}
		}
		//ILIFE-2823 Ends
		if (!validCcStatus.isTrue()) {
			sv.bankacckeyErr.set("RFH1");
		}
		/* Check if the credit card is expired.*/
		if (isLT(ccdtIO.getCrcardexpy(), wsaaYyyy)) {
			sv.bankacckeyErr.set("RPEZ");
		}
		if (isEQ(ccdtIO.getCrcardexpy(), wsaaYyyy)) {
			if (isLT(ccdtIO.getCrcardexpm(), wsaaMm)) {
				sv.bankacckeyErr.set("RPEZ");
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
protected void usedMandateChk2200()
{
	para2200();
}

protected void para2200()
{
	SmartFileCode.execute(appVars, clrrIO);
	if (isNE(clrrIO.getStatuz(), varcom.oK)
	&& isNE(clrrIO.getStatuz(), varcom.endp)) {
		syserrrec.statuz.set(clrrIO.getStatuz());
		syserrrec.params.set(clrrIO.getParams());
		fatalError600();
	}
	if (isNE(clrrIO.getStatuz(), varcom.oK)
	|| isNE(clrrIO.getClntpfx(), "CN")
	|| isNE(clrrIO.getClntcoy(), wsspcomn.fsuco)
	|| isNE(clrrIO.getClntnum(), sv.payrnum)
	|| isNE(clrrIO.getClrrrole(), "PY")) {
		clrrIO.setStatuz(varcom.endp);
		return ;
	}
	if (isEQ(clrrIO.getStatuz(), varcom.oK)
	&& isEQ(clrrIO.getClntpfx(), "CN")
	&& isEQ(clrrIO.getClntcoy(), wsspcomn.fsuco)
	&& isEQ(clrrIO.getClntnum(), sv.payrnum)
	&& isEQ(clrrIO.getClrrrole(), "PY")) {
		chdrenqIO.setParams(SPACES);
		chdrenqIO.setFunction(varcom.readr);
		chdrenqIO.setFormat("CHDRENQREC");
		chdrenqIO.setChdrcoy(clrrIO.getForecoy());
		chdrenqIO.setChdrnum(subString(clrrIO.getForenum(), 1, 8));
		SmartFileCode.execute(appVars, chdrenqIO);
		if (isNE(chdrenqIO.getStatuz(), varcom.oK)
		&& isNE(chdrenqIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(chdrenqIO.getStatuz());
			syserrrec.params.set(chdrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrenqIO.getStatuz(), varcom.oK)
		&& isEQ(chdrenqIO.getMandref(), sv.ccmndref)
		&& isNE(chdrenqIO.getChdrnum(), wsaaSaveChdrnum)) {
			wsaaMandateFlag.set("Y");
		}
		else {
			clrrIO.setFunction(varcom.nextr);
		}
	}
}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
	@Override
	protected void update3000()	{
		updateDatabase3010();
	}

	protected void updateDatabase3010()	{
		/*  Update database files as required / WSSP*/
		wsspcomn.clntkey.set(wsaaClntkey);
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		if (paymentChange.isTrue()) {
			paymentChange3100();
			return ;
		} 

		
		chdrlnbIO.setZmandref(sv.ccmndref);
		chdrlnbIO.setPayclt(sv.payrnum);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
			
		/* Update the mandate reference no. on the payer record.*/
		payrpf.setZmandref(sv.ccmndref.toString());
		payrpf.setDatime(varcom.vrcmTime.toString());//MPTD-211
		payrpfDAO.updatePayrpfByZmandref(payrpf);
	}

	protected void paymentChange3100()	{
		para3100();
	}

	protected void para3100()	{
		/*  If called from Payment Change, the updated mandate details*/
		/*  are retrieved from PAYR and CHDRMJA via RETRV. Therefore,*/
		/*  we need to perform a KEEPS on these so that the changes*/
		/*  are available.*/
		chdrpf.setZmandref(sv.ccmndref.toString());
		/* TSD 321 Phase 2 Starts */
		chdrpf.setPayclt(sv.payrnum.toString());
		chdrpfDAO.setCacheObject(chdrpf);
		
		payrpf.setZmandref(sv.ccmndref.toString());
		payrpf.setBillchnl("R");
		payrpfDAO.setCacheObject(payrpf);
	}
	protected Descpf readDesc(String desctabl, String descitem){
		Descpf descpf = descDAO.getdescData(smtpfxcpy.item.toString(), desctabl,descitem,wsspcomn.fsuco.toString(),wsspcomn.language.toString());
		return descpf;
	}
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a1000GetPayorname()
	{
		a1010Namadrs();
	}

protected void a1010Namadrs()
	{
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(sv.payrnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void a1200GetFacthousDesc(){
	descpf = readDesc("T3684",sv.facthous.toString().trim());
	if (descpf == null) {
		fatalError600();
	}
	sv.longdesc.set(descpf.getLongdesc());
}

protected void a1400Decryption()
	{
		a1400Start();
	}

protected void a1400Start()
	{
		/*   Calculate the length of credit card number                    */
//		ix.set(20);
//		while ( !(isNE(subString(sv.bankacckey, ix, 1), SPACES))) {
//			compute(ix, 0).set(sub(ix, 1));
//		}
		
		
		/*   Determine if user can view the full value of credit card      */
		/*   number when in enquiry mode                                   */
		if (isEQ(wsspcomn.flag, "I")
		/*&& isNE(tr29urec.subrname, SPACES)*/) {
			usrcypirec.rec.set(SPACES);
			usrcypirec.func.set("CHCK");
			usrcypirec.userid.set(wsspcomn.userid);
			callProgram(Usrcypind.class, usrcypirec.rec);
			if (!(isEQ(usrcypirec.indic, "Y")
			&& isEQ(usrcypirec.statuz, "****"))) {
				/*cipherrec.lfunc.set("MASK");
				cipherrec.lsrc.set(sv.bankacckey);
				a1600CallTr29uSubrname();
				sv.bankacckey.set(cipherrec.lrcv);*/
				if(!StringUtil.isEmptyOrSpace(sv.bankacckey.toString()))
                {
                      String masked = MaskingUtil.getInstance().maskString(sv.bankacckey.trim(), "************xxxx", 'x', '*', false);/* IJTI-1523 */
                      sv.bankacckey.set(masked);  
                }
			}
		}
	}
protected void a1600CallTr29uSubrname()
{
	/*A1610-START*/
	/*cipherrec.lrcv.set(SPACES);
	cipherrec.lsts.set(SPACES);
	callProgram(tr29urec.subrname, cipherrec.lfunc, cipherrec.lsrc, cipherrec.lrcv, cipherrec.lsts);
	if (isNE(cipherrec.lsts, varcom.oK)) {
		syserrrec.statuz.set(cipherrec.lsts);
		syserrrec.params.set(cipherrec.lfunc);
		fatalError600();
	}*/
	/*A1690-EXIT*/
}
}