package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:26
 * Description:
 * Copybook name: HCHRMPSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hchrmpskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hchrmpsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hchrmpsKey = new FixedLengthStringData(64).isAPartOf(hchrmpsFileKey, 0, REDEFINE);
  	public FixedLengthStringData hchrmpsChdrcoy = new FixedLengthStringData(1).isAPartOf(hchrmpsKey, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(63).isAPartOf(hchrmpsKey, 1, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hchrmpsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hchrmpsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}