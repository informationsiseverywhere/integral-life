package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:09
 * Description:
 * Copybook name: T5704REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T5704rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t5704Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData contitem = new FixedLengthStringData(8).isAPartOf(t5704Rec, 0);
  	public FixedLengthStringData crtables = new FixedLengthStringData(40).isAPartOf(t5704Rec, 8);
  	public FixedLengthStringData[] crtable = FLSArrayPartOfStructure(10, 4, crtables, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(40).isAPartOf(crtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData crtable01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData crtable02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData crtable03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData crtable04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData crtable05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData crtable06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData crtable07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData crtable08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData crtable09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData crtable10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public ZonedDecimalData mbaseinc = new ZonedDecimalData(11, 2).isAPartOf(t5704Rec, 48);
  	public ZonedDecimalData mbaseunt = new ZonedDecimalData(11, 2).isAPartOf(t5704Rec, 59);
  	public FixedLengthStringData rtables = new FixedLengthStringData(40).isAPartOf(t5704Rec, 70);
  	public FixedLengthStringData[] rtable = FLSArrayPartOfStructure(10, 4, rtables, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(rtables, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rtable01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData rtable02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData rtable03 = new FixedLengthStringData(4).isAPartOf(filler1, 8);
  	public FixedLengthStringData rtable04 = new FixedLengthStringData(4).isAPartOf(filler1, 12);
  	public FixedLengthStringData rtable05 = new FixedLengthStringData(4).isAPartOf(filler1, 16);
  	public FixedLengthStringData rtable06 = new FixedLengthStringData(4).isAPartOf(filler1, 20);
  	public FixedLengthStringData rtable07 = new FixedLengthStringData(4).isAPartOf(filler1, 24);
  	public FixedLengthStringData rtable08 = new FixedLengthStringData(4).isAPartOf(filler1, 28);
  	public FixedLengthStringData rtable09 = new FixedLengthStringData(4).isAPartOf(filler1, 32);
  	public FixedLengthStringData rtable10 = new FixedLengthStringData(4).isAPartOf(filler1, 36);
  	public FixedLengthStringData sinsbsds = new FixedLengthStringData(10).isAPartOf(t5704Rec, 110);
  	public FixedLengthStringData[] sinsbsd = FLSArrayPartOfStructure(10, 1, sinsbsds, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(10).isAPartOf(sinsbsds, 0, FILLER_REDEFINE);
  	public FixedLengthStringData sinsbsd01 = new FixedLengthStringData(1).isAPartOf(filler2, 0);
  	public FixedLengthStringData sinsbsd02 = new FixedLengthStringData(1).isAPartOf(filler2, 1);
  	public FixedLengthStringData sinsbsd03 = new FixedLengthStringData(1).isAPartOf(filler2, 2);
  	public FixedLengthStringData sinsbsd04 = new FixedLengthStringData(1).isAPartOf(filler2, 3);
  	public FixedLengthStringData sinsbsd05 = new FixedLengthStringData(1).isAPartOf(filler2, 4);
  	public FixedLengthStringData sinsbsd06 = new FixedLengthStringData(1).isAPartOf(filler2, 5);
  	public FixedLengthStringData sinsbsd07 = new FixedLengthStringData(1).isAPartOf(filler2, 6);
  	public FixedLengthStringData sinsbsd08 = new FixedLengthStringData(1).isAPartOf(filler2, 7);
  	public FixedLengthStringData sinsbsd09 = new FixedLengthStringData(1).isAPartOf(filler2, 8);
  	public FixedLengthStringData sinsbsd10 = new FixedLengthStringData(1).isAPartOf(filler2, 9);
  	public FixedLengthStringData unitpcts = new FixedLengthStringData(60).isAPartOf(t5704Rec, 120);
  	public ZonedDecimalData[] unitpct = ZDArrayPartOfStructure(10, 6, 2, unitpcts, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(60).isAPartOf(unitpcts, 0, FILLER_REDEFINE);
  	public ZonedDecimalData unitpct01 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 0);
  	public ZonedDecimalData unitpct02 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 6);
  	public ZonedDecimalData unitpct03 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 12);
  	public ZonedDecimalData unitpct04 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 18);
  	public ZonedDecimalData unitpct05 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 24);
  	public ZonedDecimalData unitpct06 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 30);
  	public ZonedDecimalData unitpct07 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 36);
  	public ZonedDecimalData unitpct08 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 42);
  	public ZonedDecimalData unitpct09 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 48);
  	public ZonedDecimalData unitpct10 = new ZonedDecimalData(6, 2).isAPartOf(filler3, 54);
  	public FixedLengthStringData unitpkgs = new FixedLengthStringData(170).isAPartOf(t5704Rec, 180);
  	public ZonedDecimalData[] unitpkg = ZDArrayPartOfStructure(10, 17, 2, unitpkgs, 0);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(170).isAPartOf(unitpkgs, 0, FILLER_REDEFINE);
  	public ZonedDecimalData unitpkg01 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 0);
  	public ZonedDecimalData unitpkg02 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 17);
  	public ZonedDecimalData unitpkg03 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 34);
  	public ZonedDecimalData unitpkg04 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 51);
  	public ZonedDecimalData unitpkg05 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 68);
  	public ZonedDecimalData unitpkg06 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 85);
  	public ZonedDecimalData unitpkg07 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 102);
  	public ZonedDecimalData unitpkg08 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 119);
  	public ZonedDecimalData unitpkg09 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 136);
  	public ZonedDecimalData unitpkg10 = new ZonedDecimalData(17, 2).isAPartOf(filler4, 153);
  	public FixedLengthStringData filler5 = new FixedLengthStringData(150).isAPartOf(t5704Rec, 350, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t5704Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t5704Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}