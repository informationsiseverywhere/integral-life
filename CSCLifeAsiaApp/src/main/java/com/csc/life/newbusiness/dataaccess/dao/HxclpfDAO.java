package com.csc.life.newbusiness.dataaccess.dao;

import com.csc.life.newbusiness.dataaccess.model.Hxclpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface HxclpfDAO extends BaseDAO<Hxclpf> {
	public Hxclpf readRecord(String chdrcoy, String chdrnum, int fupno);
	public void deleteRecords(String chdrcoy, String chdrnum, int fupno);
	public Hxclpf readHxcl(String chdrcoy, String chdrnum, int fupno); //IBPLIFE-504

}
