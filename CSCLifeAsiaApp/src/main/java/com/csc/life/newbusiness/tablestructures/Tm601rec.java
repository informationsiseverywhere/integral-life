package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:50
 * Description:
 * Copybook name: TM601REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tm601rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tm601Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData genlcdexs = new FixedLengthStringData(420).isAPartOf(tm601Rec, 0);
  	public FixedLengthStringData[] genlcdex = FLSArrayPartOfStructure(30, 14, genlcdexs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(420).isAPartOf(genlcdexs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData genlcdex01 = new FixedLengthStringData(14).isAPartOf(filler, 0);
  	public FixedLengthStringData genlcdex02 = new FixedLengthStringData(14).isAPartOf(filler, 14);
  	public FixedLengthStringData genlcdex03 = new FixedLengthStringData(14).isAPartOf(filler, 28);
  	public FixedLengthStringData genlcdex04 = new FixedLengthStringData(14).isAPartOf(filler, 42);
  	public FixedLengthStringData genlcdex05 = new FixedLengthStringData(14).isAPartOf(filler, 56);
  	public FixedLengthStringData genlcdex06 = new FixedLengthStringData(14).isAPartOf(filler, 70);
  	public FixedLengthStringData genlcdex07 = new FixedLengthStringData(14).isAPartOf(filler, 84);
  	public FixedLengthStringData genlcdex08 = new FixedLengthStringData(14).isAPartOf(filler, 98);
  	public FixedLengthStringData genlcdex09 = new FixedLengthStringData(14).isAPartOf(filler, 112);
  	public FixedLengthStringData genlcdex10 = new FixedLengthStringData(14).isAPartOf(filler, 126);
  	public FixedLengthStringData genlcdex11 = new FixedLengthStringData(14).isAPartOf(filler, 140);
  	public FixedLengthStringData genlcdex12 = new FixedLengthStringData(14).isAPartOf(filler, 154);
  	public FixedLengthStringData genlcdex13 = new FixedLengthStringData(14).isAPartOf(filler, 168);
  	public FixedLengthStringData genlcdex14 = new FixedLengthStringData(14).isAPartOf(filler, 182);
  	public FixedLengthStringData genlcdex15 = new FixedLengthStringData(14).isAPartOf(filler, 196);
  	public FixedLengthStringData genlcdex16 = new FixedLengthStringData(14).isAPartOf(filler, 210);
  	public FixedLengthStringData genlcdex17 = new FixedLengthStringData(14).isAPartOf(filler, 224);
  	public FixedLengthStringData genlcdex18 = new FixedLengthStringData(14).isAPartOf(filler, 238);
  	public FixedLengthStringData genlcdex19 = new FixedLengthStringData(14).isAPartOf(filler, 252);
  	public FixedLengthStringData genlcdex20 = new FixedLengthStringData(14).isAPartOf(filler, 266);
  	public FixedLengthStringData genlcdex21 = new FixedLengthStringData(14).isAPartOf(filler, 280);
  	public FixedLengthStringData genlcdex22 = new FixedLengthStringData(14).isAPartOf(filler, 294);
  	public FixedLengthStringData genlcdex23 = new FixedLengthStringData(14).isAPartOf(filler, 308);
  	public FixedLengthStringData genlcdex24 = new FixedLengthStringData(14).isAPartOf(filler, 322);
  	public FixedLengthStringData genlcdex25 = new FixedLengthStringData(14).isAPartOf(filler, 336);
  	public FixedLengthStringData genlcdex26 = new FixedLengthStringData(14).isAPartOf(filler, 350);
  	public FixedLengthStringData genlcdex27 = new FixedLengthStringData(14).isAPartOf(filler, 364);
  	public FixedLengthStringData genlcdex28 = new FixedLengthStringData(14).isAPartOf(filler, 378);
  	public FixedLengthStringData genlcdex29 = new FixedLengthStringData(14).isAPartOf(filler, 392);
  	public FixedLengthStringData genlcdex30 = new FixedLengthStringData(14).isAPartOf(filler, 406);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(80).isAPartOf(tm601Rec, 420, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tm601Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tm601Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}