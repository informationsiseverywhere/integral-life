package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sh585screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sh585ScreenVars sv = (Sh585ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sh585screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sh585ScreenVars screenVars = (Sh585ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.hxclnote01.setClassString("");
		screenVars.hxclnote02.setClassString("");
		screenVars.hxclnote03.setClassString("");
		screenVars.hxclnote04.setClassString("");
		screenVars.hxclnote05.setClassString("");
		screenVars.hxclnote06.setClassString("");
	}

/**
 * Clear all the variables in Sh585screen
 */
	public static void clear(VarModel pv) {
		Sh585ScreenVars screenVars = (Sh585ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.hxclnote01.clear();
		screenVars.hxclnote02.clear();
		screenVars.hxclnote03.clear();
		screenVars.hxclnote04.clear();
		screenVars.hxclnote05.clear();
		screenVars.hxclnote06.clear();
	}
}
