package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:42
 * Description:
 * Copybook name: T6640REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6640rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6640Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData lcrider = new FixedLengthStringData(4).isAPartOf(t6640Rec, 0);
  	public FixedLengthStringData lcsubr = new FixedLengthStringData(8).isAPartOf(t6640Rec, 4);
  	public FixedLengthStringData reserveMeth = new FixedLengthStringData(4).isAPartOf(t6640Rec, 12);
  	public FixedLengthStringData revBonusMeth = new FixedLengthStringData(4).isAPartOf(t6640Rec, 16);
  	public FixedLengthStringData surrenderBonusMethod = new FixedLengthStringData(4).isAPartOf(t6640Rec, 20);
  	public FixedLengthStringData zbondivalc = new FixedLengthStringData(1).isAPartOf(t6640Rec, 24);
  	public FixedLengthStringData zcshdivmth = new FixedLengthStringData(4).isAPartOf(t6640Rec, 25);
  	public FixedLengthStringData zdivwdrmth = new FixedLengthStringData(4).isAPartOf(t6640Rec, 29);
  	public FixedLengthStringData zrmandind = new FixedLengthStringData(1).isAPartOf(t6640Rec, 33);
  	public FixedLengthStringData filler = new FixedLengthStringData(466).isAPartOf(t6640Rec, 34, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6640Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6640Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}