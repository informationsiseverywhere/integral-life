package com.csc.life.newbusiness.recordstructures;

import com.csc.common.DD;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: agoel51
 * @version
 * Creation Date: Fri, 14 Feb 2020 21:14:37
 * Description:
 * Copybook name: ANTISOCLKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Antisoclkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData antisocialKey = new FixedLengthStringData(253);
  	//public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(antisocialKey, 0);
  	public FixedLengthStringData kjSName = new FixedLengthStringData(120).isAPartOf(antisocialKey, 0);
 	public FixedLengthStringData kjGName = new FixedLengthStringData(120).isAPartOf(antisocialKey, 120);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(antisocialKey, 240);
	public FixedLengthStringData clntype = new FixedLengthStringData(1).isAPartOf(antisocialKey, 244);
	public FixedLengthStringData antisocnum = new FixedLengthStringData(8).isAPartOf(antisocialKey, 245);

	public void initialize() {
		COBOLFunctions.initialize(antisocialKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			antisocialKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}