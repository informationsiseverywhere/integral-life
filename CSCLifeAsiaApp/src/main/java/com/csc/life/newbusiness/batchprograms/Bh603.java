/*
 * File: Bh603.java
 * Date: 29 August 2009 21:39:07
 * Author: Quipoz Limited
 * 
 * Class transformed from BH603.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.sql.SQLException;

import com.csc.life.enquiries.dataaccess.CovrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.HifxTableDAM;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This is a monthly extract program for pertaining all in-force
*  policies (both for single and regular premium policies) as at
*  month-end. So this program is suggested to run on the last
*  day of every month.
*
*  The information will be stored on the HIFXPF file. For each
*  month/year/channel/product combinations, there will exist
*  2 HIFXPF records:
*  Type 'R' - Regular premium totals
*  Type 'S' - Single  premium totals
*
*  N.B. If the extract is RE-RUN after the initial creation of
*       the HIFXPF records for a certain month/year, then the
*       records will be overwritten with the new information
*       as at the latest run date.
*
*****************************************************************
* </pre>
*/
public class Bh603 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlchdrCursorrs = null;
	private java.sql.PreparedStatement sqlchdrCursorps = null;
	private java.sql.Connection sqlchdrCursorconn = null;
	private String sqlchdrCursor = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH603");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String itemrec = "ITEMREC";
	private String hifxrec = "HIFXREC";
	private String covrenqrec = "COVRENQREC";
	private String t5679 = "T5679";
	private String th506 = "TH506";
		/* ERRORS */
	private String esql = "ESQL";
	private ZonedDecimalData wsaaSumins = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaAnnprem = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaSingp = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaSqlBillfreq9 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSqlSinstamt069 = new ZonedDecimalData(17, 2).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaCovrMatch = new FixedLengthStringData(1);
	private Validator covrMatch = new Validator(wsaaCovrMatch, "Y");

	private FixedLengthStringData wsaaRiderMatch = new FixedLengthStringData(1);
	private Validator riderMatch = new Validator(wsaaRiderMatch, "Y");

	private FixedLengthStringData wsaaEffectiveDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffectiveDate, 0).setUnsigned();
	private ZonedDecimalData wsaaMnth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffectiveDate, 4).setUnsigned();
	private ZonedDecimalData wsaaDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffectiveDate, 6).setUnsigned();
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);

	private FixedLengthStringData wsaaCntStatiArray = new FixedLengthStringData(72);
	private FixedLengthStringData wsaaCntStati = new FixedLengthStringData(72).isAPartOf(wsaaCntStatiArray, 0);
	private FixedLengthStringData[] wsaaCntStatus = FLSArrayPartOfStructure(12, 2, wsaaCntStati, 0);
	private FixedLengthStringData[] wsaaCovStatus = FLSArrayPartOfStructure(12, 2, wsaaCntStati, 24);
	private FixedLengthStringData[] wsaaRidStatus = FLSArrayPartOfStructure(12, 2, wsaaCntStati, 48);
	private FixedLengthStringData wsaaSqlStati = new FixedLengthStringData(24).isAPartOf(wsaaCntStati, 0, REDEFINE);
	private FixedLengthStringData wsaaCnRiskStat01 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 0);
	private FixedLengthStringData wsaaCnRiskStat02 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 2);
	private FixedLengthStringData wsaaCnRiskStat03 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 4);
	private FixedLengthStringData wsaaCnRiskStat04 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 6);
	private FixedLengthStringData wsaaCnRiskStat05 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 8);
	private FixedLengthStringData wsaaCnRiskStat06 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 10);
	private FixedLengthStringData wsaaCnRiskStat07 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 12);
	private FixedLengthStringData wsaaCnRiskStat08 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 14);
	private FixedLengthStringData wsaaCnRiskStat09 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 16);
	private FixedLengthStringData wsaaCnRiskStat10 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 18);
	private FixedLengthStringData wsaaCnRiskStat11 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 20);
	private FixedLengthStringData wsaaCnRiskStat12 = new FixedLengthStringData(2).isAPartOf(wsaaSqlStati, 22);

	private FixedLengthStringData wsaaSqlChdrDets = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaSqlChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaSqlChdrDets, 0);
	private FixedLengthStringData wsaaSqlCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlChdrDets, 8);
	private FixedLengthStringData wsaaSqlCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlChdrDets, 10);
	private FixedLengthStringData wsaaSqlCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaSqlChdrDets, 13);
	private FixedLengthStringData wsaaSqlBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaSqlChdrDets, 16);
	private PackedDecimalData wsaaSqlSinstamt06 = new PackedDecimalData(17, 2).isAPartOf(wsaaSqlChdrDets, 18);
	private PackedDecimalData wsaaSqlInsttot02 = new PackedDecimalData(17, 2).isAPartOf(wsaaSqlChdrDets, 27);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Contract Enquiry - Coverage Details.*/
	private CovrenqTableDAM covrenqIO = new CovrenqTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*In-Force Status Mthly Extract Logical*/
	private HifxTableDAM hifxIO = new HifxTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private T5679rec t5679rec = new T5679rec();
	private Th506rec th506rec = new Th506rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1190, 
		eof2080, 
		exit2090, 
		regPrem3020, 
		exit3090, 
		exit3190, 
		exit3290
	}

	public Bh603() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaCntStatiArray.set(SPACES);
		wsaaCompany.set(bsprIO.getCompany());
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaSub.set(1);
		wsaaSub1.set(1);
		wsaaSub2.set(1);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,12)); wsaaCnt.add(1)){
			if (isEQ(isNE(t5679rec.cnRiskStat[wsaaCnt.toInt()],SPACES),true)){
				wsaaCntStatus[wsaaSub.toInt()].set(t5679rec.cnRiskStat[wsaaCnt.toInt()]);
				wsaaSub.add(1);
			}
			if (isEQ(isNE(t5679rec.covRiskStat[wsaaCnt.toInt()],SPACES),true)){
				wsaaCovStatus[wsaaSub1.toInt()].set(t5679rec.covRiskStat[wsaaCnt.toInt()]);
				wsaaSub1.add(1);
			}
			if (isEQ(isNE(t5679rec.ridRiskStat[wsaaCnt.toInt()],SPACES),true)){
				wsaaRidStatus[wsaaSub2.toInt()].set(t5679rec.ridRiskStat[wsaaCnt.toInt()]);
				wsaaSub2.add(1);
			}
		}
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		hifxIO.setParams(SPACES);
		hifxIO.setCompany(bsprIO.getCompany());
		hifxIO.setYear(wsaaYear);
		hifxIO.setMnth(wsaaMnth);
		hifxIO.setFormat(hifxrec);
		hifxIO.setFunction(varcom.begnh);
		while ( !(isEQ(hifxIO.getStatuz(),varcom.endp))) {
			loopHifx1100();
		}
		
		sqlchdrCursor = " SELECT  CHDRNUM, CNTBRANCH, CNTTYPE, CNTCURR, BILLFREQ, SINSTAMT06, INSTTOT02" +
" FROM   " + appVars.getTableNameOverriden("CHDRPF") + " " +
" WHERE (CHDRCOY = ?" +
" AND STATCODE IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" +
" AND VALIDFLAG = '1'" +
" AND SERVUNIT = 'LP')";
		sqlerrorflag = false;
		try {
			sqlchdrCursorconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlchdrCursorps = appVars.prepareStatementEmbeded(sqlchdrCursorconn, sqlchdrCursor, "CHDRPF");
			appVars.setDBString(sqlchdrCursorps, 1, wsaaCompany.toString());
			appVars.setDBString(sqlchdrCursorps, 2, wsaaCnRiskStat01.toString());
			appVars.setDBString(sqlchdrCursorps, 3, wsaaCnRiskStat02.toString());
			appVars.setDBString(sqlchdrCursorps, 4, wsaaCnRiskStat03.toString());
			appVars.setDBString(sqlchdrCursorps, 5, wsaaCnRiskStat04.toString());
			appVars.setDBString(sqlchdrCursorps, 6, wsaaCnRiskStat05.toString());
			appVars.setDBString(sqlchdrCursorps, 7, wsaaCnRiskStat06.toString());
			appVars.setDBString(sqlchdrCursorps, 8, wsaaCnRiskStat07.toString());
			appVars.setDBString(sqlchdrCursorps, 9, wsaaCnRiskStat08.toString());
			appVars.setDBString(sqlchdrCursorps, 10, wsaaCnRiskStat09.toString());
			appVars.setDBString(sqlchdrCursorps, 11, wsaaCnRiskStat10.toString());
			appVars.setDBString(sqlchdrCursorps, 12, wsaaCnRiskStat11.toString());
			appVars.setDBString(sqlchdrCursorps, 13, wsaaCnRiskStat12.toString());
			sqlchdrCursorrs = appVars.executeQuery(sqlchdrCursorps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
	}

protected void loopHifx1100()
	{
		try {
			loop1110();
		}
		catch (GOTOException e){
		}
	}

protected void loop1110()
	{
		hifxio3100();
		if (isEQ(hifxIO.getStatuz(),varcom.endp)
		|| isNE(hifxIO.getCompany(),wsaaCompany)
		|| isNE(hifxIO.getYear(),wsaaYear)
		|| isNE(hifxIO.getMnth(),wsaaMnth)) {
			hifxIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		hifxIO.setFunction(varcom.delet);
		hifxio3100();
		hifxIO.setFunction(varcom.nextr);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		wsspEdterror.set(varcom.oK);
		sqlerrorflag = false;
		try {
			if (sqlchdrCursorrs.next()) {
				appVars.getDBObject(sqlchdrCursorrs, 1, wsaaSqlChdrnum);
				appVars.getDBObject(sqlchdrCursorrs, 2, wsaaSqlCntbranch);
				appVars.getDBObject(sqlchdrCursorrs, 3, wsaaSqlCnttype);
				appVars.getDBObject(sqlchdrCursorrs, 4, wsaaSqlCntcurr);
				appVars.getDBObject(sqlchdrCursorrs, 5, wsaaSqlBillfreq);
				appVars.getDBObject(sqlchdrCursorrs, 6, wsaaSqlSinstamt06);
				appVars.getDBObject(sqlchdrCursorrs, 7, wsaaSqlInsttot02);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError9000();
		}
		wsaaSumins.set(ZERO);
		wsaaAnnprem.set(ZERO);
		wsaaSingp.set(ZERO);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		appVars.freeDBConnectionIgnoreErr(sqlchdrCursorconn, sqlchdrCursorps, sqlchdrCursorrs);
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		/*EXIT*/
	}

protected void update3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					update3010();
				}
				case regPrem3020: {
					regPrem3020();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void update3010()
	{
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th506);
		itemIO.setItemitem(wsaaSqlCnttype);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		wsaaSqlSinstamt069.set(wsaaSqlSinstamt06);
		wsaaSqlBillfreq9.set(wsaaSqlBillfreq);
		if (isNE(wsaaSqlBillfreq,"00")) {
			compute(wsaaSqlSinstamt069, 2).set(mult(wsaaSqlSinstamt069,wsaaSqlBillfreq9));
			wsaaAnnprem.add(wsaaSqlSinstamt069);
		}
		else {
			wsaaAnnprem.add(wsaaSqlInsttot02);
		}
		covrenqIO.setParams(SPACES);
		covrenqIO.setChdrcoy(wsaaCompany);
		covrenqIO.setChdrnum(wsaaSqlChdrnum);
		covrenqIO.setLife("01");
		covrenqIO.setCoverage("01");
		covrenqIO.setRider("00");
		covrenqIO.setPlanSuffix(ZERO);
		covrenqIO.setFormat(covrenqrec);
		covrenqIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covrenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrenqIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covrenqIO.setStatuz(varcom.oK);
		while ( !(isEQ(covrenqIO.getStatuz(),varcom.endp))) {
			covrenq3200();
		}
		
		if (isEQ(wsaaSqlBillfreq,"00")
		|| isNE(wsaaSingp,0)) {
			hifxKey3300();
		}
		else {
			goTo(GotoLabel.regPrem3020);
		}
		if (isEQ(wsaaSqlBillfreq,"00")) {
			setPrecision(hifxIO.getSingp(), 2);
			hifxIO.setSingp(add(hifxIO.getSingp(),wsaaAnnprem));
			setPrecision(hifxIO.getSumins(), 2);
			hifxIO.setSumins(add(hifxIO.getSumins(),wsaaSumins));
		}
		setPrecision(hifxIO.getSingp(), 2);
		hifxIO.setSingp(add(hifxIO.getSingp(),wsaaSingp));
		hifxio3100();
		goTo(GotoLabel.exit3090);
	}

protected void regPrem3020()
	{
		if (isNE(wsaaSqlBillfreq,"00")) {
			hifxKey3300();
		}
		else {
			goTo(GotoLabel.exit3090);
		}
		setPrecision(hifxIO.getAnnprem(), 2);
		hifxIO.setAnnprem(add(hifxIO.getAnnprem(),wsaaAnnprem));
		setPrecision(hifxIO.getSumins(), 2);
		hifxIO.setSumins(add(hifxIO.getSumins(),wsaaSumins));
		hifxio3100();
	}

protected void hifxio3100()
	{
		try {
			hifx3110();
		}
		catch (GOTOException e){
		}
	}

protected void hifx3110()
	{
		SmartFileCode.execute(appVars, hifxIO);
		if (isEQ(hifxIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit3190);
		}
		if (isNE(hifxIO.getStatuz(),varcom.oK)
		&& isNE(hifxIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hifxIO.getStatuz());
			syserrrec.params.set(hifxIO.getParams());
			fatalError600();
		}
	}

protected void covrenq3200()
	{
		try {
			covrenq3210();
			next3280();
		}
		catch (GOTOException e){
		}
	}

protected void covrenq3210()
	{
		SmartFileCode.execute(appVars, covrenqIO);
		if (isNE(covrenqIO.getStatuz(),varcom.oK)
		&& isNE(covrenqIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(covrenqIO.getStatuz());
			syserrrec.params.set(covrenqIO.getParams());
			fatalError600();
		}
		if (isEQ(covrenqIO.getStatuz(),varcom.endp)
		|| isNE(covrenqIO.getChdrcoy(),wsaaCompany)
		|| isNE(covrenqIO.getChdrnum(),wsaaSqlChdrnum)) {
			covrenqIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3290);
		}
		if (isEQ(th506rec.crtable,SPACES)) {
			if (isEQ(covrenqIO.getCoverage(),"01")
			&& isEQ(covrenqIO.getRider(),"00")) {
				z100CoverCheck();
				if (covrMatch.isTrue()) {
					wsaaSumins.add(covrenqIO.getSumins());
				}
			}
		}
		else {
			if (isEQ(covrenqIO.getCrtable(),th506rec.crtable)) {
				z100CoverCheck();
				if (covrMatch.isTrue()) {
					wsaaSumins.add(covrenqIO.getSumins());
				}
			}
		}
		if (isEQ(wsaaCovrMatch,SPACES)) {
			z200RiderCheck();
		}
		if (riderMatch.isTrue()
		|| covrMatch.isTrue()) {
			wsaaSingp.add(covrenqIO.getSingp());
		}
	}

protected void next3280()
	{
		covrenqIO.setFunction(varcom.nextr);
	}

protected void z100CoverCheck()
	{
		/*Z110-COVER*/
		wsaaCovrMatch.set(SPACES);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,12)
		|| isEQ(wsaaCovStatus[wsaaCnt.toInt()],SPACES)
		|| covrMatch.isTrue()); wsaaCnt.add(1)){
			if (isEQ(covrenqIO.getStatcode(),wsaaCovStatus[wsaaCnt.toInt()])) {
				covrMatch.setTrue();
			}
		}
		/*Z190-EXIT*/
	}

protected void z200RiderCheck()
	{
		/*Z210-RIDER*/
		wsaaRiderMatch.set(SPACES);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,12)
		|| isEQ(wsaaRidStatus[wsaaCnt.toInt()],SPACES)
		|| riderMatch.isTrue()); wsaaCnt.add(1)){
			if (isEQ(covrenqIO.getStatcode(),wsaaRidStatus[wsaaCnt.toInt()])) {
				riderMatch.setTrue();
			}
		}
		/*Z290-EXIT*/
	}

protected void hifxKey3300()
	{
		key3310();
	}

protected void key3310()
	{
		hifxIO.setParams(SPACES);
		hifxIO.setMnth(wsaaMnth);
		hifxIO.setYear(wsaaYear);
		hifxIO.setCompany(wsaaCompany);
		hifxIO.setCntbranch(wsaaSqlCntbranch);
		hifxIO.setCntcurr(wsaaSqlCntcurr);
		hifxIO.setCnttype(wsaaSqlCnttype);
		if (isNE(wsaaSqlBillfreq,"00")) {
			hifxIO.setFieldType("R");
		}
		if (isEQ(wsaaSqlBillfreq,"00")
		|| isNE(wsaaSingp,0)) {
			hifxIO.setFieldType("S");
		}
		hifxIO.setFormat(hifxrec);
		hifxIO.setFunction(varcom.readh);
		hifxio3100();
		if (isEQ(hifxIO.getStatuz(),varcom.mrnf)) {
			initialize(hifxIO.getSumins());
			initialize(hifxIO.getAnnprem());
			initialize(hifxIO.getSingp());
			initialize(hifxIO.getCntcount());
			initialize(hifxIO.getEffdate());
			hifxIO.setFunction(varcom.writr);
		}
		else {
			hifxIO.setFunction(varcom.updat);
		}
		hifxIO.setEffdate(bsscIO.getEffectiveDate());
		setPrecision(hifxIO.getCntcount(), 0);
		hifxIO.setCntcount(add(hifxIO.getCntcount(),1));
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError9000()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
