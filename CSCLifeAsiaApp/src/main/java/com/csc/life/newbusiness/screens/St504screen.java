package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:50
 * @author Quipoz
 */
public class St504screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 19, 1, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		St504ScreenVars sv = (St504ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.St504screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		St504ScreenVars screenVars = (St504ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.riskunit.setClassString("");
		screenVars.lfact01.setClassString("");
		screenVars.lfact02.setClassString("");
		screenVars.lfact03.setClassString("");
		screenVars.lfact04.setClassString("");
		screenVars.lfact05.setClassString("");
		screenVars.lfact06.setClassString("");
		screenVars.lfact07.setClassString("");
		screenVars.lfact08.setClassString("");
		screenVars.lfact09.setClassString("");
		screenVars.lfact10.setClassString("");
		screenVars.lfact11.setClassString("");
		screenVars.lfact12.setClassString("");
		screenVars.lfact13.setClassString("");
		screenVars.lfact14.setClassString("");
		screenVars.lfact15.setClassString("");
		screenVars.lfact16.setClassString("");
		screenVars.lfact17.setClassString("");
		screenVars.lfact18.setClassString("");
		screenVars.lfact19.setClassString("");
		screenVars.lfact20.setClassString("");
		screenVars.lfact21.setClassString("");
		screenVars.lfact22.setClassString("");
		screenVars.lfact23.setClassString("");
		screenVars.lfact24.setClassString("");
		screenVars.lfact25.setClassString("");
		screenVars.lfact26.setClassString("");
		screenVars.lfact27.setClassString("");
		screenVars.lfact28.setClassString("");
		screenVars.lfact29.setClassString("");
		screenVars.lfact30.setClassString("");
		screenVars.lfact31.setClassString("");
		screenVars.lfact32.setClassString("");
		screenVars.lfact33.setClassString("");
		screenVars.lfact34.setClassString("");
		screenVars.lfact35.setClassString("");
		screenVars.lfact36.setClassString("");
		screenVars.lfact37.setClassString("");
		screenVars.lfact38.setClassString("");
		screenVars.lfact39.setClassString("");
		screenVars.lfact40.setClassString("");
		screenVars.lfact41.setClassString("");
		screenVars.lfact42.setClassString("");
		screenVars.lfact43.setClassString("");
		screenVars.lfact44.setClassString("");
		screenVars.lfact45.setClassString("");
		screenVars.lfact46.setClassString("");
		screenVars.lfact47.setClassString("");
		screenVars.lfact48.setClassString("");
		screenVars.lfact49.setClassString("");
		screenVars.lfact50.setClassString("");
		screenVars.lfact51.setClassString("");
		screenVars.lfact52.setClassString("");
		screenVars.lfact53.setClassString("");
		screenVars.lfact54.setClassString("");
		screenVars.lfact55.setClassString("");
		screenVars.lfact56.setClassString("");
		screenVars.lfact57.setClassString("");
		screenVars.lfact58.setClassString("");
		screenVars.lfact59.setClassString("");
		screenVars.lfact60.setClassString("");
		screenVars.lfact61.setClassString("");
		screenVars.lfact62.setClassString("");
		screenVars.lfact63.setClassString("");
		screenVars.lfact64.setClassString("");
		screenVars.lfact65.setClassString("");
		screenVars.lfact66.setClassString("");
		screenVars.lfact67.setClassString("");
		screenVars.lfact68.setClassString("");
		screenVars.lfact69.setClassString("");
		screenVars.lfact70.setClassString("");
		screenVars.lfact71.setClassString("");
		screenVars.lfact72.setClassString("");
		screenVars.lfact73.setClassString("");
		screenVars.lfact74.setClassString("");
		screenVars.lfact75.setClassString("");
		screenVars.lfact76.setClassString("");
		screenVars.lfact77.setClassString("");
		screenVars.lfact78.setClassString("");
		screenVars.lfact79.setClassString("");
		screenVars.lfact80.setClassString("");
		screenVars.lfact81.setClassString("");
		screenVars.lfact82.setClassString("");
		screenVars.lfact83.setClassString("");
		screenVars.lfact84.setClassString("");
		screenVars.lfact85.setClassString("");
		screenVars.lfact86.setClassString("");
		screenVars.lfact87.setClassString("");
		screenVars.lfact88.setClassString("");
		screenVars.lfact89.setClassString("");
		screenVars.lfact90.setClassString("");
		screenVars.lfact91.setClassString("");
		screenVars.lfact92.setClassString("");
		screenVars.lfact93.setClassString("");
		screenVars.lfact94.setClassString("");
		screenVars.lfact95.setClassString("");
		screenVars.lfact96.setClassString("");
		screenVars.lfact97.setClassString("");
		screenVars.lfact98.setClassString("");
		screenVars.lfact99.setClassString("");
		screenVars.modfac01.setClassString("");
		screenVars.modfac02.setClassString("");
		screenVars.modfac03.setClassString("");
		screenVars.modfac04.setClassString("");
		screenVars.modfac05.setClassString("");
		screenVars.modfac06.setClassString("");
		screenVars.modfac07.setClassString("");
		screenVars.modfac08.setClassString("");
		screenVars.modfac09.setClassString("");
		screenVars.modfac10.setClassString("");
		screenVars.modfac11.setClassString("");
		screenVars.lfactor.setClassString("");
		screenVars.disccntmeth.setClassString("");
	}

/**
 * Clear all the variables in St504screen
 */
	public static void clear(VarModel pv) {
		St504ScreenVars screenVars = (St504ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.riskunit.clear();
		screenVars.lfact01.clear();
		screenVars.lfact02.clear();
		screenVars.lfact03.clear();
		screenVars.lfact04.clear();
		screenVars.lfact05.clear();
		screenVars.lfact06.clear();
		screenVars.lfact07.clear();
		screenVars.lfact08.clear();
		screenVars.lfact09.clear();
		screenVars.lfact10.clear();
		screenVars.lfact11.clear();
		screenVars.lfact12.clear();
		screenVars.lfact13.clear();
		screenVars.lfact14.clear();
		screenVars.lfact15.clear();
		screenVars.lfact16.clear();
		screenVars.lfact17.clear();
		screenVars.lfact18.clear();
		screenVars.lfact19.clear();
		screenVars.lfact20.clear();
		screenVars.lfact21.clear();
		screenVars.lfact22.clear();
		screenVars.lfact23.clear();
		screenVars.lfact24.clear();
		screenVars.lfact25.clear();
		screenVars.lfact26.clear();
		screenVars.lfact27.clear();
		screenVars.lfact28.clear();
		screenVars.lfact29.clear();
		screenVars.lfact30.clear();
		screenVars.lfact31.clear();
		screenVars.lfact32.clear();
		screenVars.lfact33.clear();
		screenVars.lfact34.clear();
		screenVars.lfact35.clear();
		screenVars.lfact36.clear();
		screenVars.lfact37.clear();
		screenVars.lfact38.clear();
		screenVars.lfact39.clear();
		screenVars.lfact40.clear();
		screenVars.lfact41.clear();
		screenVars.lfact42.clear();
		screenVars.lfact43.clear();
		screenVars.lfact44.clear();
		screenVars.lfact45.clear();
		screenVars.lfact46.clear();
		screenVars.lfact47.clear();
		screenVars.lfact48.clear();
		screenVars.lfact49.clear();
		screenVars.lfact50.clear();
		screenVars.lfact51.clear();
		screenVars.lfact52.clear();
		screenVars.lfact53.clear();
		screenVars.lfact54.clear();
		screenVars.lfact55.clear();
		screenVars.lfact56.clear();
		screenVars.lfact57.clear();
		screenVars.lfact58.clear();
		screenVars.lfact59.clear();
		screenVars.lfact60.clear();
		screenVars.lfact61.clear();
		screenVars.lfact62.clear();
		screenVars.lfact63.clear();
		screenVars.lfact64.clear();
		screenVars.lfact65.clear();
		screenVars.lfact66.clear();
		screenVars.lfact67.clear();
		screenVars.lfact68.clear();
		screenVars.lfact69.clear();
		screenVars.lfact70.clear();
		screenVars.lfact71.clear();
		screenVars.lfact72.clear();
		screenVars.lfact73.clear();
		screenVars.lfact74.clear();
		screenVars.lfact75.clear();
		screenVars.lfact76.clear();
		screenVars.lfact77.clear();
		screenVars.lfact78.clear();
		screenVars.lfact79.clear();
		screenVars.lfact80.clear();
		screenVars.lfact81.clear();
		screenVars.lfact82.clear();
		screenVars.lfact83.clear();
		screenVars.lfact84.clear();
		screenVars.lfact85.clear();
		screenVars.lfact86.clear();
		screenVars.lfact87.clear();
		screenVars.lfact88.clear();
		screenVars.lfact89.clear();
		screenVars.lfact90.clear();
		screenVars.lfact91.clear();
		screenVars.lfact92.clear();
		screenVars.lfact93.clear();
		screenVars.lfact94.clear();
		screenVars.lfact95.clear();
		screenVars.lfact96.clear();
		screenVars.lfact97.clear();
		screenVars.lfact98.clear();
		screenVars.lfact99.clear();
		screenVars.modfac01.clear();
		screenVars.modfac02.clear();
		screenVars.modfac03.clear();
		screenVars.modfac04.clear();
		screenVars.modfac05.clear();
		screenVars.modfac06.clear();
		screenVars.modfac07.clear();
		screenVars.modfac08.clear();
		screenVars.modfac09.clear();
		screenVars.modfac10.clear();
		screenVars.modfac11.clear();
		screenVars.lfactor.clear();
		screenVars.disccntmeth.clear();
	}
}
