/******************************************************************************
 * File Name 		: CpbnfypfDAO.java
 * Author			:
 * Creation Date	: 05 December 2018
 * Project			: Integral Life
 * Description		: The DAO Interface for CPBNFYPF table.
 * ****************************************************************************
 * | Mod Date	| Mod Desc									| Mod Ticket Id   |
 * ****************************************************************************
 * | *			|		*									|	*			  |
 * ***************************************************************************/
package com.csc.life.newbusiness.dataaccess.dao;

import com.csc.life.newbusiness.dataaccess.model.Cpbnfypf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

import java.util.List;

public interface CpbnfypfDAO extends BaseDAO<Cpbnfypf> {
	public List<Cpbnfypf> searchCpbnfypfRecord(String coy, String chdrNum) throws SQLRuntimeException;
	public List<Cpbnfypf> getCpbnfyByCoyAndNumAndSeq(String coy, String chdrNum,String sequence) throws SQLRuntimeException;
	public List<Cpbnfypf> getCpbnfymnaByKeyAndTranno(Cpbnfypf cpbnfypf) throws SQLRuntimeException;
	public int updateCpbnfymnaByUnqueNum(Cpbnfypf cpbnfypf) throws SQLRuntimeException;
	public List<Cpbnfypf> getCpbnfypfByKey(Cpbnfypf cpbnfypf) throws SQLRuntimeException;
	public int insertCpbnfypf(Cpbnfypf cpbnfypf);
    public int getByChdrnum(String chdrNum);
}
