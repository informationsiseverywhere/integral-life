package com.csc.life.newbusiness.dataaccess.dao;

import java.util.List;

import com.csc.life.newbusiness.dataaccess.model.Br629DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br629TempDAO extends BaseDAO<Br629DTO> {
	public void buildTempData(String tableName, String clntpfx, String clntcoy);

	public List<Br629DTO> findTempDataResult(int batchExtractSize, int batchID);
	
	
}