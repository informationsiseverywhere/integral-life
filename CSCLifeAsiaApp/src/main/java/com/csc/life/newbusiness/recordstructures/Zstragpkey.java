package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:25
 * Description:
 * Copybook name: ZSTRAGPKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zstragpkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zstragpFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zstragpKey = new FixedLengthStringData(64).isAPartOf(zstragpFileKey, 0, REDEFINE);
  	public FixedLengthStringData zstragpChdrcoy = new FixedLengthStringData(1).isAPartOf(zstragpKey, 0);
  	public FixedLengthStringData zstragpCntbranch = new FixedLengthStringData(2).isAPartOf(zstragpKey, 1);
  	public FixedLengthStringData zstragpAracde = new FixedLengthStringData(3).isAPartOf(zstragpKey, 3);
  	public FixedLengthStringData zstragpAgntnum = new FixedLengthStringData(8).isAPartOf(zstragpKey, 6);
  	public FixedLengthStringData zstragpCnttype = new FixedLengthStringData(3).isAPartOf(zstragpKey, 14);
  	public PackedDecimalData zstragpEffdate = new PackedDecimalData(8, 0).isAPartOf(zstragpKey, 17);
  	public PackedDecimalData zstragpInstprem = new PackedDecimalData(17, 2).isAPartOf(zstragpKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(33).isAPartOf(zstragpKey, 31, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zstragpFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zstragpFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}