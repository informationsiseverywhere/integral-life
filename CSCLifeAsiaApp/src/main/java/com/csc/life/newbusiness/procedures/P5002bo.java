/*
 * File: P5002bo.java
 * Date: 29 August 2009 23:52:34
 * Author: Quipoz Limited
 * 
 * Class transformed from P5002BO.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.fsu.general.recordstructures.Sdartnrec;
import com.csc.life.newbusiness.programs.P5002;
import com.csc.life.newbusiness.screens.S5002ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.recordstructures.Boverrrec;
import com.csc.smart.recordstructures.Ldrhdr;
import com.csc.smart.recordstructures.Msgdta;
import com.csc.smart.recordstructures.Sessiono;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.Mainx;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.ExitSection;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;


/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*              P5002 Business Object
*
*  This object processes only one method
*
*         INQ - Inquiry
*
***********************************************************************
* </pre>
*/
public class P5002bo extends Mainx{

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(12).init("P5002BO     ");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaInKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaInChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaInKey, 0);
	private Ldrhdr ldrhdr = new Ldrhdr();
	private Msgdta requestData = new Msgdta();
	private Msgdta responseData = new Msgdta();
//	private Scrnparams scrnparams = new Scrnparams();
	private Sdartnrec sdartnrec = new Sdartnrec();
	private Sessiono sessiono = new Sessiono();
	private Boverrrec validationError = new Boverrrec();
	private Varcom varcom = new Varcom();
	private Ldrhdr wsaaLdrhdr = new Ldrhdr();
	private Msgdta wsaaRequest = new Msgdta();
	private Msgdta wsaaResponse = new Msgdta();
//	private Wsspcomn wsspcomn = new Wsspcomn();
	private Wssplife wssplife = new Wssplife();
	private S5002ScreenVars sv1 = ScreenProgram.getScreenVars( S5002ScreenVars.class);
    //add by Tom Chi
	private P5002 p5002;
	//end
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		aExit, 
		exit8190
	}
	
	public P5002bo() {
		super();
		wsaaProg = new FixedLengthStringData(12).init("P5002BO     ");
		wsaaVersion = new FixedLengthStringData(2).init("01");		
	}

public void mainline(Object... parmArray)
	{
		sdartnrec.rec = convertAndSetParam(sdartnrec.rec, parmArray, 2);
		responseData.data = convertAndSetParam(responseData.data, parmArray, 1);
		ldrhdr.header = convertAndSetParam(ldrhdr.header, parmArray, 0);
		try {
			process();
		}
		catch (COBOLExitProgramException e) {
		}
	}

private void process()
	{
//		initialise1000();
//		callPrograms2000();
//		aInquiry();
//		updateWsspTemp8000();
//		updateWssp9000();
		super.mainline();
	}

protected void initialise1000()
	{
		/*
		 * add by Tom chi
		 * initialize the scrnparams, it will used by validating input(such as P2465.processBo)
		 */
		scrnparams.logo.set(wsspcomn.logo);
		scrnparams.company.set(wsspcomn.company);
		scrnparams.fsuCompany.set(wsspcomn.fsuco);
		scrnparams.language.set(wsspcomn.language);
	    //end	
		/*START*/
		scrnparams.statuz.set(varcom.oK);
		ldrhdr.errlvl.set("0");
		/*EXIT*/
		/*
		 * add by Tom Chi
		 * because remove wsspledg from Mainx, so wsspledg set value here
		 */
		wssplife.userArea.set(bowscpy.userArea);
		//end				
	}

protected void callPrograms2000()
	{
		/*CALL*/
		wsspcomn.edterror.set(varcom.oK);
		wsspcomn.company.set(sdartnrec.sdarCompany);
		aInquiry();
		/*EXIT*/
	}

protected void aInquiry()
	{
		aP5002Start();
	}

protected void aP5002Start()
	{
		wsspcomn.sectionno.set("1000");
		p5002 = new P5002();
		p5002.processBo(new Object[] {wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea,null});		
//		callProgram(P5002.class, wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		wsspcomn.sectionno.set("PRE");
		p5002.processBo(new Object[] {wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea,null});
		//callProgram(P5002.class, wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		wsspcomn.sbmaction.set("E");
		scrnparams.action.set("E");
		sv1.action.set("E");
		wsaaInKey.set(sdartnrec.sdarInKey);
		sv1.chdrsel.set(wsaaInChdrnum);
		wsspcomn.sectionno.set("2000");
		p5002.processBo(new Object[] {wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea,"5002",new S5002ScreenVars()});
		//callProgram(P5002.class, wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea);
		setErrorCode(sv1);
		checkError050();
		 /*
		 * tom chi
		 */
		try {
			setupValidatedError(ldrhdr, validationError, responseData);
		} catch (ExitSection e) {
			goTo(GotoLabel.aExit); 
		}
		wsspcomn.sectionno.set("3000");
		p5002.processBo(new Object[] {wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea,null});
		//callProgram(P5002.class, wsspcomn.commonArea, wssplife.userArea, scrnparams.screenParams, sv1.dataArea);
		checkError050();
		sdartnrec.sdarBatchkey.set(wsspcomn.batchkey);
	}

protected void updateWsspTemp8000()
	{
		/*PARA*/
		sessiono.wssp.set(wsspcomn.commonArea);
		wsaaResponse.set(sessiono.rec);
		wsaaLdrhdr.set(ldrhdr.header);
		wsaaLdrhdr.objid.set("SESSION");
		wsaaLdrhdr.vrbid.set("UPD  ");
		callProgram("SESSION", wsaaLdrhdr, wsaaRequest, wsaaResponse);
		/*EXIT*/
	}

protected void updateWssp9000()
	{
	    //add by tom chi
		updateWsspTemp8000();
	    //end	
		/*PASS*/
		/*EXIT*/
	}

    @Override
    protected FixedLengthStringData getWsaaProg() {
        return wsaaProg;
    }

    @Override
    protected FixedLengthStringData getWsaaVersion() {
        return wsaaVersion;
    }

}
