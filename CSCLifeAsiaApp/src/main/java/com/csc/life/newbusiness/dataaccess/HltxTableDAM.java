package com.csc.life.newbusiness.dataaccess;

import com.csc.life.terminationclaims.dataaccess.HltxpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HltxTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:09
 * Class transformed from HLTX.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HltxTableDAM extends HltxpfTableDAM {

	public HltxTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HLTX");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "REQCOY"
		             + ", LETTYPE"
		             + ", CLNTCOY"
		             + ", CLNTNUM"
		             + ", LETSEQNO";
		
		QUALIFIEDCOLUMNS = 
		            "CLNTCOY, " +
		            "CLNTNUM, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "REQCOY, " +
		            "LETTYPE, " +
		            "LETSEQNO, " +
		            "HLNGCHAR01, " +
		            "HLNGCHAR02, " +
		            "HLNGCHAR03, " +
		            "HLNGCHAR04, " +
		            "HLNGCHAR05, " +
		            "HLNGCHAR06, " +
		            "HLNGCHAR07, " +
		            "HLNGCHAR08, " +
		            "HSHTCHAR01, " +
		            "HSHTCHAR02, " +
		            "HSHTCHAR03, " +
		            "HSHTCHAR04, " +
		            "HSHTCHAR05, " +
		            "HSHTCHAR06, " +
		            "HSHTCHAR07, " +
		            "HSHTCHAR08, " +
		            "HNUMFLD01, " +
		            "HNUMFLD02, " +
		            "HNUMFLD03, " +
		            "HNUMFLD04, " +
		            "HNUMFLD05, " +
		            "HNUMFLD06, " +
		            "HNUMFLD07, " +
		            "HNUMFLD08, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "REQCOY ASC, " +
		            "LETTYPE ASC, " +
		            "CLNTCOY ASC, " +
		            "CLNTNUM ASC, " +
		            "LETSEQNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "REQCOY DESC, " +
		            "LETTYPE DESC, " +
		            "CLNTCOY DESC, " +
		            "CLNTNUM DESC, " +
		            "LETSEQNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               clntcoy,
                               clntnum,
                               chdrcoy,
                               chdrnum,
                               requestCompany,
                               letterType,
                               letterSeqno,
                               hlngchar01,
                               hlngchar02,
                               hlngchar03,
                               hlngchar04,
                               hlngchar05,
                               hlngchar06,
                               hlngchar07,
                               hlngchar08,
                               hshtchar01,
                               hshtchar02,
                               hshtchar03,
                               hshtchar04,
                               hshtchar05,
                               hshtchar06,
                               hshtchar07,
                               hshtchar08,
                               hnumfld01,
                               hnumfld02,
                               hnumfld03,
                               hnumfld04,
                               hnumfld05,
                               hnumfld06,
                               hnumfld07,
                               hnumfld08,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(42);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getRequestCompany().toInternal()
					+ getLetterType().toInternal()
					+ getClntcoy().toInternal()
					+ getClntnum().toInternal()
					+ getLetterSeqno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, requestCompany);
			what = ExternalData.chop(what, letterType);
			what = ExternalData.chop(what, clntcoy);
			what = ExternalData.chop(what, clntnum);
			what = ExternalData.chop(what, letterSeqno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(4);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(clntcoy.toInternal());
	nonKeyFiller2.setInternal(clntnum.toInternal());
	nonKeyFiller5.setInternal(requestCompany.toInternal());
	nonKeyFiller6.setInternal(letterType.toInternal());
	nonKeyFiller7.setInternal(letterSeqno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(469);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ nonKeyFiller7.toInternal()
					+ getHlngchar01().toInternal()
					+ getHlngchar02().toInternal()
					+ getHlngchar03().toInternal()
					+ getHlngchar04().toInternal()
					+ getHlngchar05().toInternal()
					+ getHlngchar06().toInternal()
					+ getHlngchar07().toInternal()
					+ getHlngchar08().toInternal()
					+ getHshtchar01().toInternal()
					+ getHshtchar02().toInternal()
					+ getHshtchar03().toInternal()
					+ getHshtchar04().toInternal()
					+ getHshtchar05().toInternal()
					+ getHshtchar06().toInternal()
					+ getHshtchar07().toInternal()
					+ getHshtchar08().toInternal()
					+ getHnumfld01().toInternal()
					+ getHnumfld02().toInternal()
					+ getHnumfld03().toInternal()
					+ getHnumfld04().toInternal()
					+ getHnumfld05().toInternal()
					+ getHnumfld06().toInternal()
					+ getHnumfld07().toInternal()
					+ getHnumfld08().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, hlngchar01);
			what = ExternalData.chop(what, hlngchar02);
			what = ExternalData.chop(what, hlngchar03);
			what = ExternalData.chop(what, hlngchar04);
			what = ExternalData.chop(what, hlngchar05);
			what = ExternalData.chop(what, hlngchar06);
			what = ExternalData.chop(what, hlngchar07);
			what = ExternalData.chop(what, hlngchar08);
			what = ExternalData.chop(what, hshtchar01);
			what = ExternalData.chop(what, hshtchar02);
			what = ExternalData.chop(what, hshtchar03);
			what = ExternalData.chop(what, hshtchar04);
			what = ExternalData.chop(what, hshtchar05);
			what = ExternalData.chop(what, hshtchar06);
			what = ExternalData.chop(what, hshtchar07);
			what = ExternalData.chop(what, hshtchar08);
			what = ExternalData.chop(what, hnumfld01);
			what = ExternalData.chop(what, hnumfld02);
			what = ExternalData.chop(what, hnumfld03);
			what = ExternalData.chop(what, hnumfld04);
			what = ExternalData.chop(what, hnumfld05);
			what = ExternalData.chop(what, hnumfld06);
			what = ExternalData.chop(what, hnumfld07);
			what = ExternalData.chop(what, hnumfld08);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getRequestCompany() {
		return requestCompany;
	}
	public void setRequestCompany(Object what) {
		requestCompany.set(what);
	}
	public FixedLengthStringData getLetterType() {
		return letterType;
	}
	public void setLetterType(Object what) {
		letterType.set(what);
	}
	public FixedLengthStringData getClntcoy() {
		return clntcoy;
	}
	public void setClntcoy(Object what) {
		clntcoy.set(what);
	}
	public FixedLengthStringData getClntnum() {
		return clntnum;
	}
	public void setClntnum(Object what) {
		clntnum.set(what);
	}
	public PackedDecimalData getLetterSeqno() {
		return letterSeqno;
	}
	public void setLetterSeqno(Object what) {
		setLetterSeqno(what, false);
	}
	public void setLetterSeqno(Object what, boolean rounded) {
		if (rounded)
			letterSeqno.setRounded(what);
		else
			letterSeqno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}	
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}	
	public FixedLengthStringData getHlngchar01() {
		return hlngchar01;
	}
	public void setHlngchar01(Object what) {
		hlngchar01.set(what);
	}	
	public FixedLengthStringData getHlngchar02() {
		return hlngchar02;
	}
	public void setHlngchar02(Object what) {
		hlngchar02.set(what);
	}	
	public FixedLengthStringData getHlngchar03() {
		return hlngchar03;
	}
	public void setHlngchar03(Object what) {
		hlngchar03.set(what);
	}	
	public FixedLengthStringData getHlngchar04() {
		return hlngchar04;
	}
	public void setHlngchar04(Object what) {
		hlngchar04.set(what);
	}	
	public FixedLengthStringData getHlngchar05() {
		return hlngchar05;
	}
	public void setHlngchar05(Object what) {
		hlngchar05.set(what);
	}	
	public FixedLengthStringData getHlngchar06() {
		return hlngchar06;
	}
	public void setHlngchar06(Object what) {
		hlngchar06.set(what);
	}	
	public FixedLengthStringData getHlngchar07() {
		return hlngchar07;
	}
	public void setHlngchar07(Object what) {
		hlngchar07.set(what);
	}	
	public FixedLengthStringData getHlngchar08() {
		return hlngchar08;
	}
	public void setHlngchar08(Object what) {
		hlngchar08.set(what);
	}	
	public FixedLengthStringData getHshtchar01() {
		return hshtchar01;
	}
	public void setHshtchar01(Object what) {
		hshtchar01.set(what);
	}	
	public FixedLengthStringData getHshtchar02() {
		return hshtchar02;
	}
	public void setHshtchar02(Object what) {
		hshtchar02.set(what);
	}	
	public FixedLengthStringData getHshtchar03() {
		return hshtchar03;
	}
	public void setHshtchar03(Object what) {
		hshtchar03.set(what);
	}	
	public FixedLengthStringData getHshtchar04() {
		return hshtchar04;
	}
	public void setHshtchar04(Object what) {
		hshtchar04.set(what);
	}	
	public FixedLengthStringData getHshtchar05() {
		return hshtchar05;
	}
	public void setHshtchar05(Object what) {
		hshtchar05.set(what);
	}	
	public FixedLengthStringData getHshtchar06() {
		return hshtchar06;
	}
	public void setHshtchar06(Object what) {
		hshtchar06.set(what);
	}	
	public FixedLengthStringData getHshtchar07() {
		return hshtchar07;
	}
	public void setHshtchar07(Object what) {
		hshtchar07.set(what);
	}	
	public FixedLengthStringData getHshtchar08() {
		return hshtchar08;
	}
	public void setHshtchar08(Object what) {
		hshtchar08.set(what);
	}	
	public PackedDecimalData getHnumfld01() {
		return hnumfld01;
	}
	public void setHnumfld01(Object what) {
		setHnumfld01(what, false);
	}
	public void setHnumfld01(Object what, boolean rounded) {
		if (rounded)
			hnumfld01.setRounded(what);
		else
			hnumfld01.set(what);
	}	
	public PackedDecimalData getHnumfld02() {
		return hnumfld02;
	}
	public void setHnumfld02(Object what) {
		setHnumfld02(what, false);
	}
	public void setHnumfld02(Object what, boolean rounded) {
		if (rounded)
			hnumfld02.setRounded(what);
		else
			hnumfld02.set(what);
	}	
	public PackedDecimalData getHnumfld03() {
		return hnumfld03;
	}
	public void setHnumfld03(Object what) {
		setHnumfld03(what, false);
	}
	public void setHnumfld03(Object what, boolean rounded) {
		if (rounded)
			hnumfld03.setRounded(what);
		else
			hnumfld03.set(what);
	}	
	public PackedDecimalData getHnumfld04() {
		return hnumfld04;
	}
	public void setHnumfld04(Object what) {
		setHnumfld04(what, false);
	}
	public void setHnumfld04(Object what, boolean rounded) {
		if (rounded)
			hnumfld04.setRounded(what);
		else
			hnumfld04.set(what);
	}	
	public PackedDecimalData getHnumfld05() {
		return hnumfld05;
	}
	public void setHnumfld05(Object what) {
		setHnumfld05(what, false);
	}
	public void setHnumfld05(Object what, boolean rounded) {
		if (rounded)
			hnumfld05.setRounded(what);
		else
			hnumfld05.set(what);
	}	
	public PackedDecimalData getHnumfld06() {
		return hnumfld06;
	}
	public void setHnumfld06(Object what) {
		setHnumfld06(what, false);
	}
	public void setHnumfld06(Object what, boolean rounded) {
		if (rounded)
			hnumfld06.setRounded(what);
		else
			hnumfld06.set(what);
	}	
	public PackedDecimalData getHnumfld07() {
		return hnumfld07;
	}
	public void setHnumfld07(Object what) {
		setHnumfld07(what, false);
	}
	public void setHnumfld07(Object what, boolean rounded) {
		if (rounded)
			hnumfld07.setRounded(what);
		else
			hnumfld07.set(what);
	}	
	public PackedDecimalData getHnumfld08() {
		return hnumfld08;
	}
	public void setHnumfld08(Object what) {
		setHnumfld08(what, false);
	}
	public void setHnumfld08(Object what, boolean rounded) {
		if (rounded)
			hnumfld08.setRounded(what);
		else
			hnumfld08.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getHshtchars() {
		return new FixedLengthStringData(hshtchar01.toInternal()
										+ hshtchar02.toInternal()
										+ hshtchar03.toInternal()
										+ hshtchar04.toInternal()
										+ hshtchar05.toInternal()
										+ hshtchar06.toInternal()
										+ hshtchar07.toInternal()
										+ hshtchar08.toInternal());
	}
	public void setHshtchars(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getHshtchars().getLength()).init(obj);
	
		what = ExternalData.chop(what, hshtchar01);
		what = ExternalData.chop(what, hshtchar02);
		what = ExternalData.chop(what, hshtchar03);
		what = ExternalData.chop(what, hshtchar04);
		what = ExternalData.chop(what, hshtchar05);
		what = ExternalData.chop(what, hshtchar06);
		what = ExternalData.chop(what, hshtchar07);
		what = ExternalData.chop(what, hshtchar08);
	}
	public FixedLengthStringData getHshtchar(BaseData indx) {
		return getHshtchar(indx.toInt());
	}
	public FixedLengthStringData getHshtchar(int indx) {

		switch (indx) {
			case 1 : return hshtchar01;
			case 2 : return hshtchar02;
			case 3 : return hshtchar03;
			case 4 : return hshtchar04;
			case 5 : return hshtchar05;
			case 6 : return hshtchar06;
			case 7 : return hshtchar07;
			case 8 : return hshtchar08;
			default: return null; // Throw error instead?
		}
	
	}
	public void setHshtchar(BaseData indx, Object what) {
		setHshtchar(indx.toInt(), what);
	}
	public void setHshtchar(int indx, Object what) {

		switch (indx) {
			case 1 : setHshtchar01(what);
					 break;
			case 2 : setHshtchar02(what);
					 break;
			case 3 : setHshtchar03(what);
					 break;
			case 4 : setHshtchar04(what);
					 break;
			case 5 : setHshtchar05(what);
					 break;
			case 6 : setHshtchar06(what);
					 break;
			case 7 : setHshtchar07(what);
					 break;
			case 8 : setHshtchar08(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getHnumflds() {
		return new FixedLengthStringData(hnumfld01.toInternal()
										+ hnumfld02.toInternal()
										+ hnumfld03.toInternal()
										+ hnumfld04.toInternal()
										+ hnumfld05.toInternal()
										+ hnumfld06.toInternal()
										+ hnumfld07.toInternal()
										+ hnumfld08.toInternal());
	}
	public void setHnumflds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getHnumflds().getLength()).init(obj);
	
		what = ExternalData.chop(what, hnumfld01);
		what = ExternalData.chop(what, hnumfld02);
		what = ExternalData.chop(what, hnumfld03);
		what = ExternalData.chop(what, hnumfld04);
		what = ExternalData.chop(what, hnumfld05);
		what = ExternalData.chop(what, hnumfld06);
		what = ExternalData.chop(what, hnumfld07);
		what = ExternalData.chop(what, hnumfld08);
	}
	public PackedDecimalData getHnumfld(BaseData indx) {
		return getHnumfld(indx.toInt());
	}
	public PackedDecimalData getHnumfld(int indx) {

		switch (indx) {
			case 1 : return hnumfld01;
			case 2 : return hnumfld02;
			case 3 : return hnumfld03;
			case 4 : return hnumfld04;
			case 5 : return hnumfld05;
			case 6 : return hnumfld06;
			case 7 : return hnumfld07;
			case 8 : return hnumfld08;
			default: return null; // Throw error instead?
		}
	
	}
	public void setHnumfld(BaseData indx, Object what) {
		setHnumfld(indx, what, false);
	}
	public void setHnumfld(BaseData indx, Object what, boolean rounded) {
		setHnumfld(indx.toInt(), what, rounded);
	}
	public void setHnumfld(int indx, Object what) {
		setHnumfld(indx, what, false);
	}
	public void setHnumfld(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setHnumfld01(what, rounded);
					 break;
			case 2 : setHnumfld02(what, rounded);
					 break;
			case 3 : setHnumfld03(what, rounded);
					 break;
			case 4 : setHnumfld04(what, rounded);
					 break;
			case 5 : setHnumfld05(what, rounded);
					 break;
			case 6 : setHnumfld06(what, rounded);
					 break;
			case 7 : setHnumfld07(what, rounded);
					 break;
			case 8 : setHnumfld08(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getHlngchars() {
		return new FixedLengthStringData(hlngchar01.toInternal()
										+ hlngchar02.toInternal()
										+ hlngchar03.toInternal()
										+ hlngchar04.toInternal()
										+ hlngchar05.toInternal()
										+ hlngchar06.toInternal()
										+ hlngchar07.toInternal()
										+ hlngchar08.toInternal());
	}
	public void setHlngchars(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getHlngchars().getLength()).init(obj);
	
		what = ExternalData.chop(what, hlngchar01);
		what = ExternalData.chop(what, hlngchar02);
		what = ExternalData.chop(what, hlngchar03);
		what = ExternalData.chop(what, hlngchar04);
		what = ExternalData.chop(what, hlngchar05);
		what = ExternalData.chop(what, hlngchar06);
		what = ExternalData.chop(what, hlngchar07);
		what = ExternalData.chop(what, hlngchar08);
	}
	public FixedLengthStringData getHlngchar(BaseData indx) {
		return getHlngchar(indx.toInt());
	}
	public FixedLengthStringData getHlngchar(int indx) {

		switch (indx) {
			case 1 : return hlngchar01;
			case 2 : return hlngchar02;
			case 3 : return hlngchar03;
			case 4 : return hlngchar04;
			case 5 : return hlngchar05;
			case 6 : return hlngchar06;
			case 7 : return hlngchar07;
			case 8 : return hlngchar08;
			default: return null; // Throw error instead?
		}
	
	}
	public void setHlngchar(BaseData indx, Object what) {
		setHlngchar(indx.toInt(), what);
	}
	public void setHlngchar(int indx, Object what) {

		switch (indx) {
			case 1 : setHlngchar01(what);
					 break;
			case 2 : setHlngchar02(what);
					 break;
			case 3 : setHlngchar03(what);
					 break;
			case 4 : setHlngchar04(what);
					 break;
			case 5 : setHlngchar05(what);
					 break;
			case 6 : setHlngchar06(what);
					 break;
			case 7 : setHlngchar07(what);
					 break;
			case 8 : setHlngchar08(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		requestCompany.clear();
		letterType.clear();
		clntcoy.clear();
		clntnum.clear();
		letterSeqno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		chdrcoy.clear();
		chdrnum.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		nonKeyFiller7.clear();
		hlngchar01.clear();
		hlngchar02.clear();
		hlngchar03.clear();
		hlngchar04.clear();
		hlngchar05.clear();
		hlngchar06.clear();
		hlngchar07.clear();
		hlngchar08.clear();
		hshtchar01.clear();
		hshtchar02.clear();
		hshtchar03.clear();
		hshtchar04.clear();
		hshtchar05.clear();
		hshtchar06.clear();
		hshtchar07.clear();
		hshtchar08.clear();
		hnumfld01.clear();
		hnumfld02.clear();
		hnumfld03.clear();
		hnumfld04.clear();
		hnumfld05.clear();
		hnumfld06.clear();
		hnumfld07.clear();
		hnumfld08.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}