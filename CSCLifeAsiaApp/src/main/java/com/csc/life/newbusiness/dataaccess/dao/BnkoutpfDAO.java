package com.csc.life.newbusiness.dataaccess.dao;


import com.csc.life.newbusiness.dataaccess.model.Bnkoutpf;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public interface BnkoutpfDAO {
	public Bnkoutpf getBankRecord(String chdrnum);
	public void updateBnkoutRecord(Bnkoutpf Bnkoutpf);
	public void insertBnkoutpf(Bnkoutpf bnkoutpf)throws SQLRuntimeException;
	public Bnkoutpf getAgentRefcode(String bankout);

}
