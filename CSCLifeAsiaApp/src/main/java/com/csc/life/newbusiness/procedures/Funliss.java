/*
 * File: Funliss.java
 * Date: 29 August 2009 22:49:11
 * Author: Quipoz Limited
 * 
 * Class transformed from FUNLISS.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.InciTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UnltunlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnTableDAM;
import com.csc.life.unitlinkedprocessing.procedures.Unitaloc;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5535rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5536rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5537rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5545rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*      UNIT LINKED GENERIC ISSUE SUBROUTINE
*
* OVERVIEW.
* ---------
*
* FUNLISS has been cloned from UNLISS. The difference is that
* Table T5545 is read to get the enhanced unit allocation,
* because the enhamced unit allocation will vary depending on
* which date the payment was received from the client.  The
* subroutine was changed to read T5545 with the receipt date.
*
* FUNLISS is the generic  subroutine  which  will  be  called  to
* perform the processing  necessary  on  the  issue  of  a  Unit
* Linked generic component.  It  will  be  called   (via  T5671,
* Coverage/Rider Switching) from within an AT module  for   each
* Unit Linked component found on a proposal being issued.
*
* FUNLISS will handle Coverage/Riders created at  proposal  issue
* and any new components added via automatic increases.
*
* It will perform the following functions:
*
* 1. Convert the UNLTUNL records to ULNKUNL records  and  delete
* UNLTUNL records afterwards.  (Note  that  'invest  by  amount'
* records will be converted into  'invest by percentage' values,
* even if the percentage amount  indicator on the UNLT is set to
* 'A'.)
*
* 2. Write an INCI record, (not for single premium only)
*
* 3. Write unit Fund transaction records:
*      UTRN record for the uninvested portion of the premium,
*      UTRN record for the Initial Units,
*      UTRN record for the Accumulation Units,
*
* 4. Update the Coverage/Rider record via COVRUNL.
*
* PROCESSING.
* -----------
*
* The processing takes place in five phases:
*
*      1. Obtain Existing Information.
*      2. Convert Fund Direction Transaction (UNLT) records to
*      ULNK records.
*      3. Create INCI (Increase) records.
*      4. Allocation - create UTRN records.
*      (This will be done by calling the subroutine UNITALOC in
*      the case of regular premium contracts.)
*      5. Update the newly created Coverage/Rider record with
*      relevant data fields.
*
* 1. Obtain Existing Information.
* -------------------------------
*
* Read the Contract Header using  the  RETRV  function  and  the
* logical view CHDRLNB. This will  return  the  Contract  Header
* details for the component being processed.
*
* Read the Coverage/Rider record using the  key  passed  in  the
* linkage area,  a  function  of  READH  and  the  logical  view
* COVRUNL.
*
* The following dated tables are used by this program:
*
* T5687 - read using  the  Coverage/Rider code (COVR-CRTABLE) as
* key.
*
* T5540 - read using the Coverage/Rider code (as above).
*
* T5519 - if the Initial  Unit  Discount  Basis  from  T5540  is
* non-blank then use it to read T5519.
*
* T5535 - if the Loyalty/Persistency code  (LTYPST)  from  T5540
* is non-blank then use it to read T5535.
*
* Read T6647 with the Transaction Code (passed  in  the linkage)
* and CHDR-Contract Type as a key.   This  is  the  Unit  Linked
* Contract Details table and holds the  rules  specific  to that
* product and transaction.
*
* Read T5537 with a key of allocation basis  (from  T5540),  Sex
* and transaction code. If the full key is not found,  then  '*'
* is substituted for sex and the  table  read  again.  If  still
* unsuccessful, Allocation Basis,  Sex  and Transaction Code set
* as '****' is searched for. If still  unsuccessful,  Allocation
* Basis, Sex as '*' and Transaction code as '****'  is  searched
* for. If still unsuccessful, '********' is searched  for.  This
* table holds a matrix of  Allocation  Basis  codes  held  in  a
* two-dimensional table with  Age as the vertical ('y') axis and
* Term as the horizontal ('x')  axis.  Locate  the occurrence of
* Age that is greater than or equal to the  Age  Next  Birthday.
* Locate the occurrence of Term that is greater  than  or  equal
* to the Term. Locate the appropriate method.
* Move the Item Code at  this  position  to  a  working  storage
* variable for Allocation Basis  (WSAA-ALLOC-BASIS)  for  future
* use.
*
* Read T5536 with a key of  the  Allocation  Basis  just  found.
* Find the Billing Frequency  on  the  table  that  matches  the
* billing frequency from  the  contract  header  (BILLFREQ)  and
* move   its  associated  group  of  three  items:  MAX-PERIODS,
* PC-UNITS  and PC-INIT-UNITS to store for future use. Calculate
* PC-ACCUM-UNITS  as  (100 - PC-INIT-UNITS),  i.e.  if  not 100%
* allocated   to   initial   units,   the   remainder    becomes
* accumulation units.
*
* 2. Normal Processing.
* ---------------------
*
* This section deals  with  the  conversion  of  UNLT  to   ULNK
* records.  If  plan  processing  is  applicable  (and  this  is
* typically  only  the  case   in  UK  sites),   the   following
* information is of relevance, otherwise, go to Section 3.
*
* Plan Processing.
* ----------------
*
* When the COVTUNL records are  converted  into  COVR records by
* the Proposal Issue suite, one COVR record will  be created for
* each policy within the Plan except for the  first   group   of
* identical policies where one COVR record will  be created that
* represents a group of policies  that  are  identical,  i.e.  a
* summarised record. The  number  in  this  group  will  be  the
* smallest group represented  by  the  first  COVTUNL record for
* each Coverage/Rider. All other groups  will  be  'broken  out'
* into   individual   COVR  records.    The  following   example
* illustrates this.
*
*   Number of Policies in Plan = 10
*
*   Coverage #1, 3 COVTUNL records in groups of 5
*                                               3
*                                               2
*
*   Coverage #2, 2 COVTUNL records in groups of 3
*                                               7
* When the COVTUNL records are converted  into COVR records, the
* system will look at the first group of  definitions  for  each
* Coverage/Rider component (marked with  a  double  asterisk  in
* the above example) and select the  smallest  group -  in  this
* case 3. However,  the  COVTUNL  record  for  Coverage/Rider #1
* represents 5 policies.  This  will  have  to be converted to 3
* COVR records - #1 representing 3/5 of its  policies and #2 and
* #3 representing 1/5  each.   All  other  COVTUNL  records  for
* COVTUNL #1 will be  'broken out'  fully giving one COVR record
* per policy represented.
*
* The Plan Suffix will be '0000' on  the first COVR record as it
* represents a group of policies. The  other  COVR  records will
* have their  'Plan Suffixes'  allocated  sequentially  starting
* from a number  that allows for all the policies on the summary
* group and going  up  to  a  number equivalent to the number of
* policies in the plan.
*
* The following  example  shows  the  previous  COVTUNL  records
* converted to COVR  records.  The  largest  number  that can be
* summarised by a Coverage/Rider group for  the  whole  Plan  is
* '3', so the first group on Coverage/Rider #1  has to be broken
* down itself.
*
*   COV/RIDER      COVTUNL      COVR          PLAN SUFFIX
*                    No.
*                 Applicable
*
*      01             5          3/5              0000
*                                1/5              0004
*                                1/5              0005
*                     3          1/3              0006
*                                1/3              0007
*                                1/3              0008
*                     2          1/2              0009
*                                1/2              0010
*   - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*      02             3          3/3              0000
*                     7          1/7              0004
*                                1/7              0005
*                                1/7              0006
*                                1/7              0007
*                                1/7              0008
*                                1/7              0009
*                                1/7              0010
*
* The UNLTUNL records will be converted as follows:
*
* The ULNK records will mirror exactly the COVR  records created
* by  the  Proposal  Issue  suite.   The   group   of   policies
* represented  by  the  first  ULNK  record will be the smallest
* group for any Coverage/Rider component  defined  for the Plan.
* Note that this may be less than the group represented  by  the
* first COVTUNL and UNLTUNL record as in the above example.
*
* The number summarised by the first COVR record  will  be  held
* on the Contract Header record. This will tell the  program how
* many policies are to be summarised by the first  ULNK  record.
* All other UNLTUNL records will be broken down giving  one ULNK
* record per policy  and  each  having  its  own   'Plan Suffix'
* number. This will be  done  by  one  pass  through the UNLTUNL
* data-set on the first call  for  a  Coverage/Rider  component.
* The program maintains a flag  that  lets it know whether it is
* on its first pass or on subsequent passes.
*
* In the following example, it can  be  seen  that  the  UNLTUNL
* records first matched the COVTUNL  records  one  for one. When
* this program runs, the COVTUNL records will  already have been
* converted  into  the  COVR  records  shown  with  their  'Plan
* Suffixes'  in their keys. By looking at the Contract Header to
* see how many  policies  are  being  summarised,   it  will  be
* possible   to   convert  all  the  UNLTUNL  records  for  THIS
* Coverage/Rider component  in  one  pass.   Subsequent calls to
* this module for the SAME Coverage/Rider  component  will cause
* the program to read the corresponding ULNK record  created  on
* the first pass. The 'Plan Suffix' may be used to identify  the
* correct ULNK record.
*
* Note: this subroutine will be  called  once  for  every   COVR
* record created by the Proposal Issue suite.
*
*   COV/     COVTUNL --> COVR    PLAN   UNLTUNL --> ULNK   PLAN
*   RIDER      No.              SUFFIX    No.             SUFFIX
*           Applicable                 Applicable
*
*    01         5         3/5    0000      5        3/5    0000
*                         1/5    0004               1/5    0004
*                         1/5    0005               1/5    0005
*               3         1/3    0006      3        1/3    0006
*                         1/3    0007               1/3    0007
*                         1/3    0008               1/3    0008
*               2         1/2    0009      2        1/2    0009
*                         1/2    0010               1/2    0010
*   - - - - - - - - - - - - - - - - - - - - - - - - - - - -- - -
*    02         3         3/3    0000      3        3/3    0000
*               7         1/7    0004      7        1/7    0004
*                         1/7    0005               1/7    0005
*                         1/7    0006               1/7    0006
*                         1/7    0007               1/7    0007
*                         1/7    0008               1/7    0008
*                         1/7    0009               1/7    0009
*                         1/7    0010               1/7    0010
*
* 3. Issue - Create INCI Record.
* ------------------------------
*
* Set up the fields for creating the  INCI  record  (Increases).
* The sources of the fields are:
*
*  Company Number      -  Passed in Linkage
*  Contract Number     -    "     "    "
*  Life                -    "     "    "
*  Coverage            -    "     "    "
*  Rider               -    "     "    "
*  Plan Suffix         - Calculated as described previously
*  Validflag           - '1'
*  Dormant Flag        - 'N'
*  INCI Number         - 001
*  TRANNO              - Passed in Linkage
*  RCDATE              - Contract Header
*  PREM-CESS-DATE      - Coverage/Rider
*  CRTABLE             -     "      "
*  ORIG-PREM           - COVRUNL-INSTPREM
*  CURR-PREM           - This either moves INSTPREM or
*                        WSAA-PREMIUM-PAID which is derived
*                        earlier by multiplying the INSTPREM by
*                        the FREQ-FACTOR.
*
* INCI Calculations.
* ------------------
*
* Other fields are calculated as follows:
*
* Here  we  are  first concerned with a group of fields on  the
* INCI record that occur in small groups of four:  INCI-PCUNIT,
* INCI-USPLITPC, INCI-PREMST and INCI-PREMCURR.
*
* Set INCI-PREMCURR  and  INCI-PREMST  to  the   Premium  No. of
* periods applicable from T5536.
*
* Set INCI-PCUNIT to the PCUINT from T5536.
*
* Set INCI-USPLITPC to PC-INIT-UNITS.
*
* The calling program  will  pass  as  one of its parameters the
* 'Premium   Available'.   This  may  differ  from  the   actual
* Instalment  Premium on the Coverage/Rider record.In most cases
* it will be  the  same  as the coverage although some contracts
* may be issued if there  is  no  money  actually  available  in
* suspense at the time of issue. If the  'Premium Available'  is
* zero   then   the   allocation  processing,   including  these
* calculations, should be skipped  entirely.  If  this  is   the
* case, then write the  INCI  record  now.   If  performing  the
* allocation processing,  then the 'Premium Available' should be
* used in case it differs from the Instalment Premium.
*
* First find the period with  which  we  are going to work. This
* will be the  first  of  the  INCI-PREMCURR  fields   that   is
* non-zero.
*
* If the split  for  the  period  is zero, then there is no more
* allocation to be done. Check   INCI-PCUNIT  and  INCI-USPLITPC
* and if both  are  zero  then   finish  with   the   allocation
* processing.
*
* Default the   enhancement  percentage  (PC-ENHANCE)   and life
* enhancement (LIFE-ENHANCE) to 100. If the Enhanced  Allocation
* code from T6647 (key = Contract Type)   is  not  spaces,  then
* read   T5545  with  the  Transaction  Code  and  the  Enhanced
* Allocation  Code  as the key. Match the Billing Frequency with
* that on T5545 and  take the corresponding Enhanced Percentages
* and Enhanced Premiums  group.   Match  the  Instalment Premium
* against the list to see if it is greater than  or equal to any
* of  the  Enhanced   Premiums.   If  it  is   then   take   the
* corresponding  Enhanced  Percentage  as  PC-ENHANCE  (Enhanced
* Percentage) to be used in later calculations.
*
* If the amount  of  the  Premium  Available  covers  the  first
* period and into  the  next  period, first allocate the premium
* to the current period and at  the  end  allocate the remainder
* to the next period.
*
* Set NET-PREMIUM to PREMIUM-AVAILABLE.
*
* If NET-PREMIUM is greater  than  INCI-PREMCURR  then  subtract
* INCI-PREMCURR  from  NET-PREMIUM  and  set  the  remainder  as
* NEXT-PERIOD and set NET-PREMIUM to INCI-PREMCURR.
*
* Calculate Current Period.
*
* Obtain the % Initial  Units  (PC-INIT-UNITS)  - from T5536 and
* the calculated %  Accumulation  units  (PC-ACCUM-UNITS).  Note
* that these figures  are still held as integers and are not yet
* held as percentage  multipliers.   To  obtain  these  figures,
* perform the  following  calculations.   The  receiving  fields
* should be defined as S9(03)V9(07) COMP-3.
*
* Divide  PC-UNITS, PC-INIT-UNITS, PC-ACCUM-UNITS and PC-ENHANCE
* by 100 rounding the answer.
*
* Calculate the number of UNITS (rounded) as PC-UNITS *
* PC-ENHANCE.
*
* Subtract the number of UNITS from 1 giving the UNINVESTED.
*
* Calculate the INIT-UNITS (rounded)        as PC-UNITS
*                                            * PC-INIT-UNITS
*                                            * PC-ENHANCE.
*
* Calculate the ACCUM-UNITS (rounded)       as PC-UNITS
*                                            * PC-ACCUM-UNITS
*                                            * PC-ENHANCE.
*
* Calculate the UNITS-VALUE (rounded)       as PC-UNITS
*                                            * NET-PREMIUM.
*
* Calculate the UNINVESTED-VALUE (rounded)  as UNINVESTED
*                                            * NET-PREMIUM.
*
* Calculate the INIT-UNITS-VALUE (rounded)  as INIT-UNITS
*                                            * NET-PREMIUM.
*
* Calculate the ACCUM-UNITS-VALUE (rounded) as ACCUM-UNITS
*                                            * NET-PREMIUM.
*
* If the  NEXT-PERIOD  is  greater  than zero, then perform  the
* calculations again for the next period using  the  NEXT-PERIOD
* as the new NET-PREMIUM.
*
* If the NEXT-PERIOD is not zero, subtract the second
* NET-PREMIUM from the second INCI-PREMCURR.
*
* Subtract the first NET-PREMIUM from the first INCI-PREMCURR.
*
* Write the INCI record.
*
* 4. Allocation - Create UTRN records.
* ------------------------------------
*
* Allocation of units  and  the  writing  of  UTRN  records  for
* Regular and Single premiums are dealt with separately.  Single
* premiums are allocated within this  module,  regular  premiums
* are handled by  calling  UNITALOC.  To  cater  for  components
* which   have   a  Lump  Sum  paid  in  at  New  Business,  the
* appropriate  sections are only performed if the premium amount
* in question is non-zero.
*
* Monies Date - Unit Allocation Date.
* -----------------------------------
*
* The Monies Date,  the  effective  date  for  Unit  Allocation,
* should be calculated. The code returned  from  T6647  will  be
* either 'BD' - Business Date, 'DD' - Due Date,  which for Issue
* is the Risk Commencement Date or 'LO' -  Later  Of  the  above
* two. This will determine which  date  is to  be  used  as  the
* Monies Date by  Unit  Allocation.  If  it  is  'BD'  then  the
* current system  date will be used, if it is 'DD' then the Risk
* Commencement Date  will  be  used.   If  it  is  'LO' then the
* greater of the above two dates will be  used.   However,  this
* will be overridden if the user has entered  a  'Reserve  Date'
* which will be stored on the COVR record. If the  Reserve  Date
* from COVR is non zero then use this as the Monies Date.
*
* The allocation of units  will  be  on  either  a  'Now'  or  a
* 'Deferred'  basis.    This  depends  on  the  value   of   the
* 'Allocation'   field  from  T6647.    If  this  is  'D',  then
* allocation is  deferred, and if this is 'N' then allocation is
* immediate.
*
* Allocation.
* -----------
*
* First determine  the fund split by reading the ULNKUNL record.
* The correct ULNKUNL  record will be obtained by using the same
* key as is on the corresponding COVR record being processed.
*
* The following Allocation  Processing  will  be  performed once
* for the current period and possibly again if a  second  set of
* details was set up in the INCI record due to the  presence  of
* a greater premium than was required.
*
* A UTRN record will be created for the  uninvested  portion  of
* the premium. Note that there may be two sets of  INCI  details
* on the INCI record if there  is  enough  premium  to  allocate
* into the next period. If so  then  TWO  UTRN  records  will be
* created.The usual key and transaction  data  will  be  set  up
* plus the following:
*
*  UTRN-RLDGCOY - Company from CHDR
*  UTRN-RLDGACCT  - Contract Number from CHDR
*  UTRN-JOB-NAME-ORIG - spaces
*  UTRN-JOBNO-ORIG  - zero
*  UTRN-JOBNO-EXTRACT - zero
*  UTRN-JOBNO-UPDATE- zero
*  UTRN-JOBNO-PRICE - zero
*  UTRN-STAT-FUND - from COVRUNL
*  UTRN-STAT-SECT -   "     "
*  UTRN-STAT-SUBSECT- "     "
*  UTRN-UNIT-VIRTUAL-FUND - spaces
*  UTRN-UNIT-SUB-ACCOUNT- spaces
*  UTRN-EFFDATE - Risk Commencement Date
*  UTRN-STRPDATE  - zero
*  UTRN-NOW-DEFER-IND - 'N' - Now or 'D' -Deferred
*  UTRN-UNIT-TYPE - spaces
*  UTRN-NOF-UNITS - zero
*  UTRN-NOF-DUNITS  - zero
*  UTRN-UL-AMOUNT - Uninvested Amount from INCI record
*  UTRN-MONIES-DATE - The previously determined Monies Date
*  UTRN-PRICE-DATE-USED - Risk Commencement Date
*  UTRN-PRICE-USED  - zero
*  UTRN-BARE-PRICE  - zero
*  UTRN-CRTABLE - from COVRUNL
*  UTRN-CNTCURR - Contract Currency from CHDR
*  UTRN-CSHD-EFFD-IND - spaces
*  UTRN-FEEDBACK-IND- spaces
*  UTRN-INCI-NUM  - INCI number from the INCI record
*  UTRN-INCI-PERD - INCI period, i.e. 1 or 2
*  UTRN-INCI-PREMIUM- Uninvested Amount from INCI record
*  UTRN-SURR-PENALTY-FACTOR - zero
*
* The following processing will be carried out for each fund  in
* the fund split as specified on the ULNKUNL record. There is  a
* maximum of ten funds on the ULNKUNL table. The  processing  is
* divided into two  very  similar  parts:  firstly  for  Initial
* Units and then secondly for Accumulation Units.
*
*      For the Pricing  Date  the  Monies  Date  is  used  if no
*      Reserve  Units  Date  is  held  on  the  COVRUNL  record,
*      otherwise the COVRUNL Reserve Units Date is used.
*
*      Set the UTRN-UNIT-SUB-ACCOUNT to 'INIT', for Initial
*      Units.
*
*      Calculate  the  basic  premium  (unenhanced) for the fund
*      split. It may  be  used  later  for  windbacks.   If  the
*      ULNKUNL record  indicates  to use a percentage, calculate
*      the following:
*
*      INCI-FUND-AMT (rounded) = UNIT-PC
*                              * Percent/Amount (from ULNKUNL)
*                              / 100.
*
*      If the ULNKUNL record  indicates  that an amount has been
*      entered and not a percentage do the following
*      calculation:
*
*      INCI-FUND-AMT (rounded) = UNIT-PC
*                              * Percent/Amount (from ULNKUNL).
*
*      This result is then rounded to  two  decimal  places   by
*      adding zero to it with  the  rounding  clause  and  using
*      UTRN-INCI-AMT as the receiving field defined as
*      S9(11)V9(02) COMP-3.
*
*      The result - UTRN-INCI-AMT -  is later placed on the UTRN
*      record.
*
*      The Enhanced Premium  Amount  is  calculated  as  follows
*      where percentage are concerned:
*
*      FUND-AMT rounded = TEMP-AMT
*                       * Percentage/Amount (from ULNKUNL)
*                       / 100
*
*      where   TEMP-AMT  is  either   the   INIT-UNITS-VAL    or
*      ACCUM-UNITS-VAL calculated earlier depending  on  whether
*      initial units or accumulation units are  being  processed
*      - and as follows where an amount is concerned:
*
*      FUND-AMT rounded = UNIT-PC
*                       * Percentage/Amount (from ULNKUNL).
*
*      This result is also then rounded to  two  decimal  places
*      by adding zero to it with the rounding clause  and  using
*      UTRN-AMT as the receiving field defined as S9(11)V9(02)
*      COMP-3.
*
*      The number of units on the actual money  amount  must  be
*      calculated to two decimal places and rounded, whereas  up
*      to now it has been handled with seven decimal places.
*
*      When creating each UTRN record  note  that  the  Contract
*      Currency may differ from the Fund  Currency  as  held  on
*      T5515. In this case the Price and Units held on the  UTRN
*      record will have to be altered to take this into account.
*
* A partial UTRN record will have the UTRN-JOBNO,  UTRN-PRICEDT,
* UTRN-UNITS, UTRN-DUNITS, JOBNO-PRICE, OFFER-PRICE and
* BARE-PRICE set to zero.
*
* 5. Coverage Calculations.
* -------------------------
*
* The total premium for the coverage will have  been  passed  in
* the Linkage Section.
*
* Certain dates are calculated and stored on the COVR as
* follows:
*
*   ANNIVERSARY PROCESSING DATE
*   CONVERT INITIAL UNITS DATE
*   REVIEW PROCESSING DATE
*   UNIT STATEMENT DATE
*   CPI-DATE
*   INIT-UNIT-CANC-DATE
*   EXTRA-ALLOC-DATE
*   INIT-UNIT-INCREASE-DATE
*
* The dates are originally  set  to  all  '9's  and   then   the
* following logic is performed  which may or may not alter them.
* Anniversary Processing  Date -   this  processing  is   always
* performed:
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1   -  OCCDATE
*      DTC2-FREQUENCY    -  '01'
*      DTC2-FREQ-FACTOR  -  1
*
*      Call DATCON2 and use the returned date to call DATCON2  a
*      second time with the parameters set as follows:
*
*      DTC2-INT-DATE-1   -  DTC2-INT-DATE-2
*      DTC2-FREQUENCY    -  'DY'
*      DTC2-FREQ-FACTOR  -  -1
*
*      Call DATCON2 and use the returned date as the
*      Anniversary Processing Date.
*
* Convert Initial Units Date  -  this  processing  is  dependent
* upon the retrieved data from a  read  of  T5519.   Read  T5519
* using the Initial Unit Discount  Basis  as  a  key.   If   the
* returned TERM-TO-RUN is 'Y' then perform  the  'Term  to  Run'
* processing, otherwise if the  'Whole  of  Life'  is  'Y'  then
* perform the 'Whole  of  Life'  processing,  otherwise  if  the
* FIXDTRM is not zero  then perform the 'Fixed Term' processing.
* If none of these has  been  set, then there is an error - give
* error message number E686   and  perform  the   system   error
* processing.
*
*      TERM TO RUN - set the following fields for a call to
*      DATCON2:
*
*      DTC2-INT-DATE-1  -  Risk Cessation Date
*      DTC2-FREQUENCY   -  'DY'
*      DTC2-FREQ-FACTOR -  -1
*
*      Call DATCON2 and use the returned date as the Convert
*      Initial Units Date.
*
*      WHOLE OF LIFE - set the Convert Initial Units Date to
*      all '9's.
*
*      FIXED TERM - set the following fields for a call to
*      DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '01'
*      DTC2-FREQ-FACTOR -  Fixed Term from T5519
*
*      Call DATCON2 and use the returned date to call  DATCON2 a
*      second time with the parameters set as follows:
*
*      DTC2-INT-DATE-1  -  DTC2-INT-DATE-2
*      DTC2-FREQUENCY   -  'DY'
*      DTC2-FREQ-FACTOR -  -1
*
*      Call DATCON2 and use the returned date as the Convert
*      Initial Units Date.
*
* Review Processing Date - perform this processing only  if this
* field (RVWPROC) from the COVRUNL is non-zero.
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '01'
*      DTC2-FREQ-FACTOR -  RVWPROC
*
*      Call DATCON2 and use the returned date as the Review
*      Processing Date.
*
* Unit Statement Date - this processing is always performed:
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '01'
*      DTC2-FREQ-FACTOR -  1
*
*      Call DATCON2 and use the returned date as the Unit
*      Statement Date.
*
* Initial Unit Increase Date -  If  the  Initial  Unit  Discount
* Basis  from  T5540  is  blank  then  skip   this   processing,
* otherwise:
*
*      If the Differing Price flag (from T5519) is  not  'Y'  OR
*      the With Unit Adjustment flag  (from T5519)  is  not  'Y'
*      then skip this processing,  otherwise   set  the  Initial
*      Unit Increase Date to the Anniversary Processing Date.
*
* Initial Unit Cancellation Date - If the Initial Unit  Discount
* Basis from T5540 is blank then skip this processing,
* otherwise:
*
*      If the Unit Deduction Flag from T5519  is  'Y'  then  set
*      the Initial Unit Cancellation  Date  as  the  Anniversary
*      Processing Date.
*
* Extra Allocation  Date  -  if  the  Loyalty/Persistency   code
* (LTYPST) is spaces then skip this processing, otherwise:
*
*      Set the following fields for a call to DATCON2:
*
*      DTC2-INT-DATE-1  -  RCDATE
*      DTC2-FREQUENCY   -  '12'
*      DTC2-FREQ-FACTOR -  'After Duration' (1) from T5535
*
*      Call DATCON2 and use the returned date as the Extra
*      Allocation Date.
*
* Any date not modified by the previous processing  is  left  as
* all '9's.
*
* Update the COVRUNL record.
*
* MODIFICATIONS.
* --------------
*
* Tables accessed and their keys:
*
* T5687 - key = Coverage/Rider code - CRTABLE (from COVRUNL)
* T5540 - key = Coverage/Rider code - CRTABLE (from COVRUNL)
* T6647 - key = Transaction Code + Contract Type (from CHDRLNB)
* T5537 - key = Basis Select(from T5540) + sex + Transaction
* Code
* T5536 - key = Allocation Basis  (from T5537)
* T5545 - key = Enhanced Allocation Code(from T6647)
* T5519 - key = Initial Unit Discount Basis (from T5540)
* T5533 - key = Loyalty/Persistency Code(from T5540)
* T5515 - key = Fund Code
*
*
*
*****************************************************************
* </pre>
*/
public class Funliss extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "FUNLISS";
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5537Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBasisSelect = new FixedLengthStringData(3).isAPartOf(wsaaT5537Key, 0).init(SPACES);
	private FixedLengthStringData wsaaT5537Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5537Key, 3).init(SPACES);
	private FixedLengthStringData wsaaTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5537Key, 4).init(SPACES);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTrCde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaContract = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 4);

	private FixedLengthStringData wsaaT5544Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaSwmeth = new FixedLengthStringData(4).isAPartOf(wsaaT5544Key, 0);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5544Key, 4);
	private FixedLengthStringData wsaaJlifeExists = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaLifeCltsex = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaLifeAnbAtCcd = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaJlifeCltsex = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaJlifeAnbAtCcd = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaAllocBasis = new FixedLengthStringData(4).init(SPACES);
	private ZonedDecimalData wsaaA = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaB = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaY = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaZ = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private String wsaaFirstUnltunl = "Y";
	private PackedDecimalData wsaaNofPeriods = new PackedDecimalData(5, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0).init(0);
	private ZonedDecimalData wsaaMoniesDate = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(13, 2).init(ZERO);
	private PackedDecimalData wsaaPremiumPaid = new PackedDecimalData(13, 2).init(ZERO);
	private PackedDecimalData wsaaSingPrem = new PackedDecimalData(13, 2).init(ZERO);
	private PackedDecimalData wsaaEnhancePerc = new PackedDecimalData(5, 2).init(ZERO);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private PackedDecimalData wsaaCallCount = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaT5545Item = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5545Enhall = new FixedLengthStringData(4).isAPartOf(wsaaT5545Item, 0);
	private FixedLengthStringData wsaaT5545Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5545Item, 4);

		/* WSAA-T5536-VALUES */
	private FixedLengthStringData wsaaMaxPeriods = new FixedLengthStringData(28);
	private ZonedDecimalData[] wsaaMaxPeriod = ZDArrayPartOfStructure(7, 4, 0, wsaaMaxPeriods, 0);

	private FixedLengthStringData wsaaPcUnits = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaPcUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaPcUnits, 0);

	private FixedLengthStringData wsaaPcInitUnits = new FixedLengthStringData(35);
	private ZonedDecimalData[] wsaaPcInitUnit = ZDArrayPartOfStructure(7, 5, 2, wsaaPcInitUnits, 0);
	private ZonedDecimalData wsaaDateSplit = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(wsaaDateSplit, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaYearSplit = new ZonedDecimalData(4, 0).isAPartOf(filler, 0).setUnsigned();
	private ZonedDecimalData wsaaUalprcTot = new ZonedDecimalData(9, 2);
	private ZonedDecimalData wsaaUalprcRunTot = new ZonedDecimalData(9, 2);
		/* WSAA-UALPRCS */
	private ZonedDecimalData[] wsaaUalprc = ZDInittedArray(10, 9, 2);
	private PackedDecimalData wsaaRiskCessTerm = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaFact = new PackedDecimalData(3, 0);
		/* ERRORS */
	private static final String e308 = "E308";
	private static final String e616 = "E616";
	private static final String e706 = "E706";
	private static final String e707 = "E707";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrunlTableDAM covrunlIO = new CovrunlTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private InciTableDAM inciIO = new InciTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private UlnkTableDAM ulnkIO = new UlnkTableDAM();
	private UlnkrnlTableDAM ulnkrnlIO = new UlnkrnlTableDAM();
	private UnltunlTableDAM unltunlIO = new UnltunlTableDAM();
	private UtrnTableDAM utrnIO = new UtrnTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5687rec t5687rec = new T5687rec();
	private T6646rec t6646rec = new T6646rec();
	private T5539rec t5539rec = new T5539rec();
	private T5540rec t5540rec = new T5540rec();
	private T5544rec t5544rec = new T5544rec();
	private T6647rec t6647rec = new T6647rec();
	private T5519rec t5519rec = new T5519rec();
	private T5537rec t5537rec = new T5537rec();
	private T5536rec t5536rec = new T5536rec();
	private T5545rec t5545rec = new T5545rec();
	private T5515rec t5515rec = new T5515rec();
	private T5535rec t5535rec = new T5535rec();
	private T5645rec t5645rec = new T5645rec();
	private T6659rec t6659rec = new T6659rec();
	private T5688rec t5688rec = new T5688rec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Isuallrec isuallrec = new Isuallrec();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaAmountHoldersInner wsaaAmountHoldersInner = new WsaaAmountHoldersInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		cont10450, 
		cont10460, 
		keepsChdrmja, 
		readItem10515, 
		cont10520, 
		yOrdinate10540, 
		increment10560, 
		xOrdinate10580, 
		increment10585, 
		nextFreq10640, 
		endLoop10650, 
		exit10690, 
		nextRecord20100, 
		read20200, 
		next31800, 
		nextPeriod32200, 
		exit40090, 
		extraAllocDate50400, 
		unitStmntDate50500, 
		updateCovrunl50700, 
		exit50090, 
		fundAllocation60050, 
		nextFreq60140, 
		endLoop60150, 
		exit60190, 
		comlvlacc61925, 
		cont61950, 
		enhanb63200, 
		enhanc63300, 
		enhand63400, 
		enhane63500, 
		enhanf63600, 
		nearExit63800, 
		next64150, 
		accumUnit64200, 
		next64250, 
		nearExit64800, 
		termToRun65200, 
		fixedTerm65300, 
		exit65900
	}

	public Funliss() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		isuallrec.isuallRec = convertAndSetParam(isuallrec.isuallRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		/*PARA*/
		syserrrec.subrname.set(wsaaSubr);
		isuallrec.statuz.set(varcom.oK);
		obtainInfo10000();
		/*    Hopefully the first time this is called it will get the Summa*/
		/*    COVT and hence convert all the UNLTs to ULNKs!*/
		if ((isEQ(isuallrec.planSuffix, 0))
		|| (isEQ(isuallrec.convertUnlt, "Y"))) {
			convertUnltToUlnk20000();
		}
		/* If it is a single premium transaction, no need to go through*/
		/* 30000- and 40000- to write INCI record and allocate premium.*/
		/* Must create INCI and allocate regular premium if this is*/
		/* not a single premium only contract.*/
		if (isNE(chdrlnbIO.getBillfreq(), "00")
		&& isNE(chdrlnbIO.getBillfreq(), "  ")) {
			inciDetails30000();
			allocation40000();
		}
		/* Now, is single premium only, or regular premium contract with*/
		/* lump sum at issue, allocate the single premium.*/
		if (isEQ(chdrlnbIO.getBillfreq(), "00")
		|| isEQ(chdrlnbIO.getBillfreq(), "  ")
		|| isNE(wsaaSingPrem, ZERO)) {
			singlePremiumProcess60000();
		}
		updateCovr50000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void obtainInfo10000()
	{
		/*PARA*/
		varcom.vrcmTime.set(getCobolTime());
		getFiles10100();
		getT568710200();
		getT554010300();
		getT664710400();
		getT554410450();
		getT553710500();
		getT553610600();
		getT551910800();
		getT553511100();
		getT564511200();
		getT665911300();
		getT568811400();
	}

protected void getFiles10100()
	{
		para10110();
	}

protected void para10110()
	{
		/* Retrieve contract header details.*/
		chdrlnbIO.setDataKey(SPACES);
		chdrlnbIO.setChdrcoy(isuallrec.company);
		chdrlnbIO.setChdrnum(isuallrec.chdrnum);
		/*    Need to Hold record as we will have to update the Unit*/
		/*    Statement Date.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			dbError8100();
		}
		/*Retrieve the CHDRMJA logical view as this contains the free*/
		/*switch information fields which will be updated.*/
		chdrmjaIO.setDataKey(SPACES);
		chdrmjaIO.setDataKey(chdrlnbIO.getDataKey());
		chdrmjaIO.setStatuz(varcom.oK);
		chdrmjaIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			dbError8100();
		}
		/* Retrieve LIFE details.*/
		wsaaLifeCltsex.set(SPACES);
		wsaaJlifeExists.set(SPACES);
		wsaaLifeAnbAtCcd.set(0);
		wsaaJlifeAnbAtCcd.set(99);
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(isuallrec.company);
		lifelnbIO.setChdrnum(isuallrec.chdrnum);
		lifelnbIO.setLife(isuallrec.life);
		lifelnbIO.setJlife("00");
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE");
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(lifelnbIO.getParams());
			dbError8100();
		}
		/* Need to see if a joint life exists. If so, use the younger of*/
		/* the two lives to find the unit allocation basis.*/
		wsaaLifeCltsex.set(lifelnbIO.getCltsex());
		wsaaLifeAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		lifelnbIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, lifelnbIO);
		if ((isNE(lifelnbIO.getStatuz(), varcom.oK))
		&& (isNE(lifelnbIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(lifelnbIO.getParams());
			dbError8100();
		}
		if ((isNE(isuallrec.company, lifelnbIO.getChdrcoy()))
		|| (isNE(isuallrec.chdrnum, lifelnbIO.getChdrnum()))
		|| (isNE(isuallrec.life, lifelnbIO.getLife()))
		|| (isEQ(lifelnbIO.getStatuz(), varcom.endp))) {
			wsaaJlifeExists.set("N");
		}
		else {
			wsaaJlifeExists.set("Y");
		}
		if (isEQ(wsaaJlifeExists, "Y")) {
			wsaaJlifeAnbAtCcd.set(lifelnbIO.getAnbAtCcd());
		}
		/* LIFELNB-CLTSEX and LIFELNB-ANB-AT-CCD are used to access*/
		/* T5536 and T5537, so we have to make sure that they have the*/
		/* correct informations in them.*/
		if (isGTE(wsaaJlifeAnbAtCcd, wsaaLifeAnbAtCcd)) {
			lifelnbIO.setCltsex(wsaaLifeCltsex);
			lifelnbIO.setAnbAtCcd(wsaaLifeAnbAtCcd);
		}
		/*  Read and hold coverage/rider record.*/
		covrunlIO.setChdrcoy(isuallrec.company);
		covrunlIO.setChdrnum(isuallrec.chdrnum);
		covrunlIO.setLife(isuallrec.life);
		covrunlIO.setCoverage(isuallrec.coverage);
		covrunlIO.setRider(isuallrec.rider);
		covrunlIO.setPlanSuffix(isuallrec.planSuffix);
		covrunlIO.setFunction(varcom.readh);
		covrunlIO.setFormat("COVRUNLREC");
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			dbError8100();
		}
		/* Calculate the premium for use within the processing.*/
		wsaaSingPrem.set(covrunlIO.getSingp());
		if (isNE(covrunlIO.getInstprem(), ZERO)) {
			wsaaInstprem.set(covrunlIO.getInstprem());
			compute(wsaaPremiumPaid, 5).set(mult(covrunlIO.getInstprem(), isuallrec.freqFactor));
		}
		else {
			wsaaPremiumPaid.set(covrunlIO.getSingp());
			wsaaInstprem.set(covrunlIO.getSingp());
		}
	}

protected void getT568710200()
	{
		para10210();
	}

protected void para10210()
	{
		/*  Read T5687 for general coverage/rider details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5687);
		itdmIO.setItemitem(covrunlIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5687))
		|| (isNE(itdmIO.getItemitem(), covrunlIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5687);
			itdmIO.setItemitem(covrunlIO.getCrtable());
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT554010300()
	{
		para10310();
	}

protected void para10310()
	{
		/*  Read T5540 for general unit linked details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItemitem(covrunlIO.getCrtable());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5540))
		|| (isNE(itdmIO.getItemitem(), covrunlIO.getCrtable()))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5540);
			itdmIO.setItemitem(covrunlIO.getCrtable());
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT664710400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10410();
				case cont10450: 
					cont10450();
				case cont10460: 
					cont10460();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10410()
	{
		/* Read T6647 for unit linked contract details.*/
		/* Key is transaction code plus contract type.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTrCde.set(isuallrec.batctrcde);
		wsaaContract.set(chdrlnbIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(), wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t6647);
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			goTo(GotoLabel.cont10450);
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
			goTo(GotoLabel.cont10460);
		}
	}

protected void cont10450()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t6647);
		wsaaTrCde.set(isuallrec.batctrcde);
		wsaaContract.set("***");
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t6647))
		|| (isNE(itdmIO.getItemitem(), wsaaT6647Key))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t6647);
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t6647rec.t6647Rec.set(itdmIO.getGenarea());
		}
	}

protected void cont10460()
	{
		/* Calculate the monies date.*/
		if (isNE(covrunlIO.getReserveUnitsDate(), ZERO)
		&& isNE(covrunlIO.getReserveUnitsDate(), varcom.vrcmMaxDate)) {
			wsaaMoniesDate.set(covrunlIO.getReserveUnitsDate());
			return ;
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaMoniesDate.set(datcon1rec.intDate);
		if (isEQ(t6647rec.efdcode, "BD")) {
			return ;
		}
		if (isEQ(t6647rec.efdcode, "RD")) {
			wsaaMoniesDate.set(isuallrec.runDate);
		}
		if (isEQ(t6647rec.efdcode, "DD")) {
			wsaaMoniesDate.set(chdrlnbIO.getOccdate());
		}
		if (isEQ(t6647rec.efdcode, "LO")) {
			if (isGT(chdrlnbIO.getOccdate(), wsaaMoniesDate)) {
				wsaaMoniesDate.set(chdrlnbIO.getOccdate());
			}
		}
	}

protected void getT554410450()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10450();
					freeSwitches();
				case keepsChdrmja: 
					keepsChdrmja();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10450()
	{
		/* Read T5544 for free switch information.*/
		/* Key is switch method from T6647 and contract currency.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5544);
		wsaaSwmeth.set(t6647rec.swmeth);
		wsaaCntcurr.set(chdrlnbIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5544Key);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5544))
		|| (isNE(itdmIO.getItemitem(), wsaaT5544Key))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			if (isEQ(wsaaSwmeth, SPACES)) {
				goTo(GotoLabel.keepsChdrmja);
			}
			else {
				itdmIO.setItemcoy(isuallrec.company);
				itdmIO.setItemtabl(tablesInner.t5544);
				itdmIO.setItemitem(wsaaT5544Key);
				itdmIO.setItmfrm(chdrlnbIO.getOccdate());
				syserrrec.params.set(itdmIO.getParams());
				dbError8100();
			}
		}
		else {
			t5544rec.t5544Rec.set(itdmIO.getGenarea());
		}
	}

protected void freeSwitches()
	{
		chdrmjaIO.setFreeSwitchesLeft(t5544rec.noOfFreeSwitches);
		chdrmjaIO.setFreeSwitchesUsed(ZERO);
		chdrmjaIO.setLastSwitchDate(chdrmjaIO.getOccdate());
	}

protected void keepsChdrmja()
	{
		chdrmjaIO.setFunction(varcom.rewrt);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmjaIO.getParams());
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			systemError8000();
		}
		/*EXIT1*/
	}

protected void getT553710500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10510();
				case readItem10515: 
					readItem10515();
				case cont10520: 
					cont10520();
				case yOrdinate10540: 
					yOrdinate10540();
				case increment10560: 
					increment10560();
				case xOrdinate10580: 
					xOrdinate10580();
				case increment10585: 
					increment10585();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10510()
	{
		/* Read T5537 for unit allocation basis.*/
		wsaaBasisSelect.set(t5540rec.allbas);
		wsaaTranCode.set(isuallrec.batctrcde);
		wsaaT5537Sex.set(lifelnbIO.getCltsex());
	}

protected void readItem10515()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5537);
		itemIO.setItemitem(wsaaT5537Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont10520);
		}
		/*    If NO record found then try first with the basis, * for*/
		/*    Sex and Transaction code.*/
		wsaaT5537Sex.set("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont10520);
		}
		/*    If NO record found then try secondly with the basis, Sex*/
		/*    and '****'  Transaction code.*/
		wsaaT5537Sex.set(lifelnbIO.getCltsex());
		wsaaTranCode.fill("*");
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont10520);
		}
		/* If no match is found, try again with the global transaction*/
		/* code.*/
		wsaaTranCode.fill("*");
		/* If no match is found, try again with the global sex code*/
		wsaaT5537Sex.set("*");
		/*    When doing a global change and the global data is being*/
		/*    moved to the wsaa-t5537-key make sure that you move the*/
		/*    wsaa-t5537-key to the ITEM-ITEMITEM.*/
		itemIO.setItemitem(wsaaT5537Key);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		if (isNE(itemIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.cont10520);
		}
		/* And again if no match is found to prevent system falling over*/
		/* read T5537 with global key.*/
		itemIO.setItemitem("********");
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))
		&& (isNE(itemIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
	}

protected void cont10520()
	{
		t5537rec.t5537Rec.set(itemIO.getGenarea());
	}

	/**
	* <pre>
	*  Work out the array's Y index based on ANB-AT-CCD.
	* </pre>
	*/
protected void yOrdinate10540()
	{
		wsaaZ.set(0);
	}

protected void increment10560()
	{
		wsaaZ.add(1);
		/* Check the continuation field for a key to re-read table.*/
		if (isGT(wsaaZ, 10)
		&& isNE(t5537rec.agecont, SPACES)) {
			readAgecont80000();
			goTo(GotoLabel.yOrdinate10540);
		}
		else {
			if (isGT(wsaaZ, 10)
			&& isEQ(t5537rec.agecont, SPACES)) {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(e706);
				dbError8100();
			}
		}
		if (isLTE(wsaaZ, 10)) {
			/*        IF  LIFELNB-ANB-AT-CCD           > T5537-TOAGE(WSAA-Z)   */
			if (isGT(lifelnbIO.getAnbAtCcd(), t5537rec.ageIssageTo[wsaaZ.toInt()])) {
				goTo(GotoLabel.increment10560);
			}
			else {
				wsaaY.set(wsaaZ);
			}
		}
		else {
			wsaaT5537Key.set(t5537rec.agecont);
			goTo(GotoLabel.readItem10515);
		}
	}

	/**
	* <pre>
	*  Work out the array's X index based on TERM.
	* </pre>
	*/
protected void xOrdinate10580()
	{
		wsaaZ.set(0);
	}

protected void increment10585()
	{
		wsaaZ.add(1);
		/* Check the term continuation field for a key to re-read table.*/
		if (isGT(wsaaZ, 9)
		&& isNE(t5537rec.trmcont, SPACES)) {
			readTermcont90000();
			goTo(GotoLabel.xOrdinate10580);
		}
		else {
			if (isGT(wsaaZ, 9)
			&& isEQ(t5537rec.trmcont, SPACES)) {
				syserrrec.params.set(itemIO.getDataArea());
				syserrrec.statuz.set(e707);
				dbError8100();
			}
		}
		/*    Always calculate the term.*/
		calculateTerm12000();
		wsaaTerm.set(wsaaRiskCessTerm);
		if (isLTE(wsaaZ, 9)) {
			if (isGT(wsaaTerm, t5537rec.toterm[wsaaZ.toInt()])) {
				goTo(GotoLabel.increment10585);
			}
			else {
				wsaaX.set(wsaaZ);
			}
		}
		else {
			wsaaT5537Key.set(t5537rec.trmcont);
			goTo(GotoLabel.readItem10515);
		}
		/* Now we have the correct position(WSAA-X, WSAA-Y) of the basis.*/
		/* All we have to do now is to work out the corresponding position*/
		/* in the copybook by using these co-ordinates.(The copybook is*/
		/* defined as one-dimensional.)*/
		wsaaY.subtract(1);
		compute(wsaaIndex, 0).set(mult(wsaaY, 9));
		wsaaIndex.add(wsaaX);
		wsaaAllocBasis.set(t5537rec.actAllocBasis[wsaaIndex.toInt()]);
	}

protected void getT553610600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para10610();
					gotRecord10620();
				case nextFreq10640: 
					nextFreq10640();
				case endLoop10650: 
					endLoop10650();
				case exit10690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para10610()
	{
		/* Read T5536 for unit allocation basis details.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5536);
		itdmIO.setItemitem(wsaaAllocBasis);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5536))
		|| (isNE(itdmIO.getItemitem(), wsaaAllocBasis))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5536);
			itdmIO.setItemitem(wsaaAllocBasis);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t5536rec.t5536Rec.set(itdmIO.getGenarea());
		}
	}

	/**
	* <pre>
	* There are 6 sets of details in the table, each set is led by
	* the billing frequency. So to find the correct set of details,
	* match each of these frequencies with the billing frequency
	* specified in the contract header record.
	* </pre>
	*/
protected void gotRecord10620()
	{
		wsaaZ.set(0);
	}

protected void nextFreq10640()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ, 6)) {
			goTo(GotoLabel.endLoop10650);
		}
		if (isEQ(t5536rec.billfreq[wsaaZ.toInt()], SPACES)) {
			goTo(GotoLabel.nextFreq10640);
		}
		if (isNE(t5536rec.billfreq[wsaaZ.toInt()], chdrlnbIO.getBillfreq())) {
			goTo(GotoLabel.nextFreq10640);
		}
		/* The appropriate allocation basis details found.*/
		if (isEQ(wsaaZ, 1)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodas);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsas);
			wsaaPcUnits.set(t5536rec.pcUnitsas);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ, 2)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodbs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsbs);
			wsaaPcUnits.set(t5536rec.pcUnitsbs);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ, 3)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodcs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitscs);
			wsaaPcUnits.set(t5536rec.pcUnitscs);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ, 4)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodds);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsds);
			wsaaPcUnits.set(t5536rec.pcUnitsds);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ, 5)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodes);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitses);
			wsaaPcUnits.set(t5536rec.pcUnitses);
			goTo(GotoLabel.exit10690);
		}
		if (isEQ(wsaaZ, 6)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodfs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsfs);
			wsaaPcUnits.set(t5536rec.pcUnitsfs);
			goTo(GotoLabel.exit10690);
		}
	}

protected void endLoop10650()
	{
		isuallrec.statuz.set(e616);
		exit090();
	}

protected void getT664610700()
	{
		para10710();
	}

protected void para10710()
	{
		/* Read T6646 for initial unit discount factor for whole of life.*/
		if (isEQ(t5540rec.wholeIuDiscFact, SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t6646);
		itemIO.setItemitem(t5540rec.wholeIuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		t6646rec.t6646Rec.set(itemIO.getGenarea());
	}

protected void getT551910800()
	{
		para10810();
	}

protected void para10810()
	{
		/*  Read T5519 for initial unit discount basis.*/
		if (isEQ(t5540rec.iuDiscBasis, SPACES)) {
			return ;
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5519);
		itdmIO.setItemitem(t5540rec.iuDiscBasis);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5519))
		|| (isNE(itdmIO.getItemitem(), t5540rec.iuDiscBasis))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5519);
			itdmIO.setItemitem(t5540rec.iuDiscBasis);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t5519rec.t5519Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT553910900()
	{
		para10910();
	}

protected void para10910()
	{
		/*  Read T5539 for initial unit discount factor for term.*/
		if (isEQ(t5540rec.iuDiscFact, SPACES)) {
			return ;
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5539);
		itemIO.setItemitem(t5540rec.iuDiscFact);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			dbError8100();
		}
		t5539rec.t5539Rec.set(itemIO.getGenarea());
	}

protected void getT553511100()
	{
		para11110();
	}

protected void para11110()
	{
		/*  Read T5535 for loyalty allocation.*/
		if (isEQ(t5540rec.ltypst, SPACES)) {
			return ;
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5535);
		itdmIO.setItemitem(t5540rec.ltypst);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.oK))
		&& (isNE(itdmIO.getStatuz(), varcom.endp))) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5535))
		|| (isNE(itdmIO.getItemitem(), t5540rec.ltypst))
		|| (isEQ(itdmIO.getStatuz(), varcom.endp))) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t5535);
			itdmIO.setItemitem(t5540rec.ltypst);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t5535rec.t5535Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT564511200()
	{
		para11210();
	}

protected void para11210()
	{
		/*  Read T5645 for transaction accounting rules.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(isuallrec.company);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaSubr);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			dbError8100();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void getT665911300()
	{
		para11310();
	}

protected void para11310()
	{
		/* Read T6659 to get the frequency by using the unit statement*/
		/* method from T6647 as the key.*/
		if (isEQ(t6647rec.unitStatMethod, SPACES)) {
			t6659rec.t6659Rec.set(SPACES);
			return ;
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isNE(itdmIO.getItemcoy(), isuallrec.company)
		|| isNE(itdmIO.getItemtabl(), tablesInner.t6659)
		|| isNE(itdmIO.getItemitem(), t6647rec.unitStatMethod)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemcoy(isuallrec.company);
			itdmIO.setItemtabl(tablesInner.t6659);
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			itdmIO.setItmfrm(chdrlnbIO.getOccdate());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
	}

protected void getT568811400()
	{
		para11410();
	}

protected void para11410()
	{
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(itdmIO.getStatuz());
			dbError8100();
			if (isNE(itdmIO.getItemcoy(), chdrlnbIO.getChdrcoy())
			|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)
			|| isNE(itdmIO.getItemitem(), chdrlnbIO.getCnttype())
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				itdmIO.setItemitem(chdrlnbIO.getCnttype());
				syserrrec.params.set(itdmIO.getParams());
				syserrrec.statuz.set(e308);
				dbError8100();
			}
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
	}

protected void calculateTerm12000()
	{
		/*PARA*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrunlIO.getCrrcd());
		datcon3rec.intDate2.set(covrunlIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor, wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
		/*EXIT*/
	}

protected void convertUnltToUlnk20000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para20010();
				case nextRecord20100: 
					nextRecord20100();
				case read20200: 
					read20200();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para20010()
	{
		unltunlIO.setDataArea(SPACES);
		unltunlIO.setChdrcoy(isuallrec.company);
		unltunlIO.setChdrnum(isuallrec.chdrnum);
		unltunlIO.setLife(isuallrec.life);
		unltunlIO.setCoverage(isuallrec.coverage);
		unltunlIO.setRider(isuallrec.rider);
		unltunlIO.setSeqnbr(ZERO);
		wsaaFirstUnltunl = "Y";
		unltunlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		unltunlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		unltunlIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER");
		goTo(GotoLabel.read20200);
	}

protected void nextRecord20100()
	{
		unltunlIO.setFunction(varcom.nextr);
	}

protected void read20200()
	{
		unltunlio28000();
		if ((isEQ(unltunlIO.getStatuz(), varcom.endp))) {
			return ;
		}
		if ((isNE(unltunlIO.getChdrcoy(), isuallrec.company))
		|| (isNE(unltunlIO.getChdrnum(), isuallrec.chdrnum))
		|| (isNE(unltunlIO.getLife(), isuallrec.life))
		|| (isNE(unltunlIO.getCoverage(), isuallrec.coverage))
		|| (isNE(unltunlIO.getRider(), isuallrec.rider))) {
			return ;
		}
		convertUnltunl22000();
		/* At this point delete the last UNLTUNL record read since all*/
		/* the informations have been fully used(converted into ULNK)*/
		/* and will no longer be useful.*/
		unltunlIO.setFunction(varcom.readh);
		unltunlio28000();
		unltunlIO.setFunction(varcom.delet);
		unltunlio28000();
		setPrecision(unltunlIO.getSeqnbr(), 0);
		unltunlIO.setSeqnbr(add(unltunlIO.getSeqnbr(), 1));
		goTo(GotoLabel.nextRecord20100);
	}

protected void convertUnltunl22000()
	{
		convert22000();
	}

protected void convert22000()
	{
		storePrc23200();
		if (isEQ(wsaaFirstUnltunl, "Y")) {
			firstUnltunl23000();
			wsaaFirstUnltunl = "N";
			return ;
		}
		/* Write any other split records which relevant to this UNLTUNL.*/
		/* For 'invest by amount', we will actually convert it into*/
		/* 'invest by percentage'. This will make life easier for future*/
		/* processing, such as premium increase and enhanced allocation.*/
		/* First we accumulate the total premiums.*/
		if (isEQ(unltunlIO.getPercOrAmntInd(), "A")) {
			wsaaUalprcTot.set(0);
			for (wsaaA.set(1); !(isGT(wsaaA, 10)); wsaaA.add(1)){
				amount23100();
			}
		}
		/* Once we have the total premium, we can then calculate the*/
		/* equalivant investing percentage for each non blank fund.*/
		/* Then perform the processing as though it is 'invest by*/
		/* percentage'.*/
		if (isEQ(unltunlIO.getPercOrAmntInd(), "A")) {
			wsaaUalprcRunTot.set(0);
			for (wsaaA.set(1); !(isGT(wsaaA, 10)
			|| isEQ(wsaaUalprc[wsaaA.toInt()], 0)); wsaaA.add(1)){
				amountToPerc23300();
			}
		}
		for (wsaaA.set(unltunlIO.getNumapp()); !(isEQ(wsaaA, ZERO)); wsaaA.add(-1)){
			splits24000();
		}
	}

protected void firstUnltunl23000()
	{
		para23010();
	}

protected void para23010()
	{
		wsaaPlanSuffix.set(0);
		compute(wsaaB, 0).set(sub(unltunlIO.getNumapp(), chdrlnbIO.getPolsum()));
		/* For 'invest by amount', we will actually convert it into*/
		/* 'invest by percentage'. This will make life easier for future*/
		/* processing, such as premium increase and enhanced allocation.*/
		/* First we accumulate the total premiums.*/
		if (isEQ(unltunlIO.getPercOrAmntInd(), "A")) {
			wsaaUalprcTot.set(0);
			for (wsaaA.set(1); !(isGT(wsaaA, 10)); wsaaA.add(1)){
				amount23100();
			}
		}
		/* Once we have the total premium, we can then calculate the*/
		/* equalivant investing percentage for each non blank fund.*/
		/* Then perform the processing as though it is 'invest by*/
		/* percentage'.*/
		if (isEQ(unltunlIO.getPercOrAmntInd(), "A")) {
			wsaaUalprcRunTot.set(0);
			for (wsaaA.set(1); !(isGT(wsaaA, 10)
			|| isEQ(wsaaUalprc[wsaaA.toInt()], 0)); wsaaA.add(1)){
				amountToPerc23300();
			}
		}
		setupUlnk29000();
		if (isEQ(isuallrec.convertUnlt, "Y")) {
			ulnkIO.setPlanSuffix(isuallrec.planSuffix);
		}
		ulnkIO.setFunction(varcom.updat);
		ulnkio29500();
		/* If the POLINC > 1 recalculate and increment*/
		/* the WSAA-PLAN-SUFFIX.*/
		if (isGT(chdrlnbIO.getPolinc(), 1)) {
			compute(wsaaPlanSuffix, 0).set(add(wsaaPlanSuffix, chdrlnbIO.getPolsum()));
			wsaaPlanSuffix.add(1);
		}
		/* Calculate new plan suffix.*/
		/* Write any other duplicate records for the first split.*/
		if (isEQ(wsaaB, ZERO)) {
			return ;
		}
		for (wsaaA.set(wsaaB); !(isEQ(wsaaA, ZERO)); wsaaA.add(-1)){
			splits24000();
		}
	}

protected void storePrc23200()
	{
		/*PARA*/
		wsaaUalprc[1].set(unltunlIO.getUalprc(1));
		wsaaUalprc[2].set(unltunlIO.getUalprc(2));
		wsaaUalprc[3].set(unltunlIO.getUalprc(3));
		wsaaUalprc[4].set(unltunlIO.getUalprc(4));
		wsaaUalprc[5].set(unltunlIO.getUalprc(5));
		wsaaUalprc[6].set(unltunlIO.getUalprc(6));
		wsaaUalprc[7].set(unltunlIO.getUalprc(7));
		wsaaUalprc[8].set(unltunlIO.getUalprc(8));
		wsaaUalprc[9].set(unltunlIO.getUalprc(9));
		wsaaUalprc[10].set(unltunlIO.getUalprc(10));
		/*EXIT*/
	}

protected void amount23100()
	{
		/*PARA*/
		compute(wsaaUalprcTot, 2).set(add(wsaaUalprcTot, wsaaUalprc[wsaaA.toInt()]));
		/*EXIT*/
	}

protected void amountToPerc23300()
	{
		/*PARA*/
		/* The following is to avoid rounding error.*/
		/* Keep a running total and use the remainder as the percentage*/
		/* when the last fund on the record is encountered.*/
		if (isEQ(wsaaA, 10)) {
			compute(wsaaUalprc[wsaaA.toInt()], 2).set(sub(100, wsaaUalprcRunTot));
			return ;
		}
		if ((setPrecision(wsaaUalprc[add(wsaaA, 1).toInt()], 0)
		&& isEQ(wsaaUalprc[add(wsaaA, 1).toInt()], 0))) {
			compute(wsaaUalprc[wsaaA.toInt()], 2).set(sub(100, wsaaUalprcRunTot));
			return ;
		}
		/* Converting to percentage.*/
		compute(wsaaUalprc[wsaaA.toInt()], 3).setRounded(div(mult(wsaaUalprc[wsaaA.toInt()], 100), wsaaUalprcTot));
		wsaaUalprcRunTot.add(wsaaUalprc[wsaaA.toInt()]);
		/*EXIT*/
	}

protected void splits24000()
	{
		/*PARA*/
		setupUlnk29000();
		ulnkIO.setFunction(varcom.updat);
		ulnkio29500();
		wsaaPlanSuffix.add(1);
		/*EXIT*/
	}

protected void unltunlio28000()
	{
		/*READ*/
		unltunlIO.setFormat(formatsInner.unltunlrec);
		SmartFileCode.execute(appVars, unltunlIO);
		if (isNE(unltunlIO.getStatuz(), varcom.oK)) {
			if (isNE(unltunlIO.getStatuz(), varcom.endp)) {
				syserrrec.dbparams.set(unltunlIO.getParams());
				syserrrec.statuz.set(unltunlIO.getStatuz());
				dbError8100();
			}
		}
		/*EXIT*/
	}

protected void setupUlnk29000()
	{
		setup29010();
	}

protected void setup29010()
	{
		ulnkIO.setChdrcoy(unltunlIO.getChdrcoy());
		ulnkIO.setChdrnum(unltunlIO.getChdrnum());
		ulnkIO.setLife(unltunlIO.getLife());
		ulnkIO.setJlife("00");
		ulnkIO.setCoverage(unltunlIO.getCoverage());
		ulnkIO.setRider(unltunlIO.getRider());
		ulnkIO.setPlanSuffix(wsaaPlanSuffix);
		ulnkIO.setUalfnds(unltunlIO.getUalfnds());
		ulnkIO.setUalprc(1, wsaaUalprc[1]);
		ulnkIO.setUalprc(2, wsaaUalprc[2]);
		ulnkIO.setUalprc(3, wsaaUalprc[3]);
		ulnkIO.setUalprc(4, wsaaUalprc[4]);
		ulnkIO.setUalprc(5, wsaaUalprc[5]);
		ulnkIO.setUalprc(6, wsaaUalprc[6]);
		ulnkIO.setUalprc(7, wsaaUalprc[7]);
		ulnkIO.setUalprc(8, wsaaUalprc[8]);
		ulnkIO.setUalprc(9, wsaaUalprc[9]);
		ulnkIO.setUalprc(10, wsaaUalprc[10]);
		ulnkIO.setUspcprs(unltunlIO.getUspcprs());
		ulnkIO.setTranno(unltunlIO.getTranno());
		ulnkIO.setPercOrAmntInd(unltunlIO.getPercOrAmntInd());
		ulnkIO.setCurrto(99999999);
		ulnkIO.setPremTopupInd(unltunlIO.getPremTopupInd());
		ulnkIO.setValidflag("1");
		ulnkIO.setCurrfrom(isuallrec.effdate);
	}

protected void ulnkio29500()
	{
		/*CALL*/
		ulnkIO.setFormat(formatsInner.ulnkrec);
		SmartFileCode.execute(appVars, ulnkIO);
		if (isNE(ulnkIO.getStatuz(), varcom.oK)) {
			syserrrec.dbparams.set(ulnkIO.getParams());
			syserrrec.statuz.set(ulnkIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void inciDetails30000()
	{
		/*PARA*/
		initialiseInci31000();
		setUpInciDetails32000();
		writeInci39000();
		/*EXIT*/
	}

protected void initialiseInci31000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para31010();
				case next31800: 
					next31800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para31010()
	{
		inciIO.setChdrcoy(isuallrec.company);
		inciIO.setChdrnum(isuallrec.chdrnum);
		inciIO.setLife(isuallrec.life);
		inciIO.setCoverage(isuallrec.coverage);
		inciIO.setRider(isuallrec.rider);
		inciIO.setPlanSuffix(isuallrec.planSuffix);
		inciIO.setInciNum("001");
		inciIO.setSeqno(1);
		inciIO.setValidflag("1");
		inciIO.setDormantFlag("N");
		inciIO.setTranno(chdrlnbIO.getTranno());
		inciIO.setRcdate(chdrlnbIO.getOccdate());
		inciIO.setPremCessDate(covrunlIO.getPremCessDate());
		inciIO.setCrtable(covrunlIO.getCrtable());
		inciIO.setOrigPrem(wsaaInstprem);
		/* If calculated wsaa premium paid is equal to zero move*/
		/*    covr instprem to inci curr prem else move*/
		/*    wsaa premium paid.*/
		/* Set the orig-prem and the current premium = to the      <D9604>*/
		/* instprem.                                               <D9604>*/
		/*                                                         <D9604>*/
		/*    IF WSAA-PREMIUM-PAID        = ZERO                   <D9604> */
		/*       MOVE WSAA-INSTPREM       TO INCI-CURR-PREM        <D9604> */
		/*    ELSE                                                 <D9604> */
		/*       MOVE WSAA-PREMIUM-PAID   TO INCI-CURR-PREM        <D9604> */
		/*    END-IF.                                              <D9604> */
		/*                                                         <D9604>*/
		inciIO.setCurrPrem(wsaaInstprem);
		inciIO.setUser(isuallrec.user);
		inciIO.setTermid(isuallrec.termid);
		inciIO.setTransactionDate(isuallrec.transactionDate);
		inciIO.setTransactionTime(isuallrec.transactionTime);
		wsaaZ.set(ZERO);
	}

protected void next31800()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ, 4)) {
			return ;
		}
		inciIO.setPremst(wsaaZ, ZERO);
		inciIO.setPremcurr(wsaaZ, ZERO);
		inciIO.setPcunit(wsaaZ, ZERO);
		inciIO.setUsplitpc(wsaaZ, ZERO);
		goTo(GotoLabel.next31800);
	}

protected void setUpInciDetails32000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para32010();
				case nextPeriod32200: 
					nextPeriod32200();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para32010()
	{
		wsaaZ.set(0);
	}

protected void nextPeriod32200()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ, 4)) {
			return ;
		}
		inciIO.setPcunit(wsaaZ, wsaaPcUnit[wsaaZ.toInt()]);
		inciIO.setUsplitpc(wsaaZ, wsaaPcInitUnit[wsaaZ.toInt()]);
		if (isEQ(wsaaMaxPeriod[wsaaZ.toInt()], 0)) {
			wsaaMaxPeriod[wsaaZ.toInt()].set(9999);
			inciIO.setPremst(wsaaZ, 0);
			inciIO.setPremcurr(wsaaZ, 0);
			goTo(GotoLabel.nextPeriod32200);
		}
		/* Work out the length of the present period.(4 periods in total)*/
		if (isGT(wsaaZ, 1)) {
			compute(wsaaX, 0).set(sub(wsaaZ, 1));
			compute(wsaaNofPeriods, 0).set(sub(wsaaMaxPeriod[wsaaZ.toInt()], wsaaMaxPeriod[wsaaX.toInt()]));
		}
		else {
			wsaaNofPeriods.set(wsaaMaxPeriod[wsaaZ.toInt()]);
		}
		/* Work out the the total premiums to pay for the present period.*/
		/* At this stage this total should be the same as the total*/
		/* premiums needed for the present period since no instalment has*/
		/* been paid yet(not at this point anyway!).*/
		setPrecision(inciIO.getPremcurr(wsaaZ), 2);
		inciIO.setPremcurr(wsaaZ, mult(wsaaInstprem, wsaaNofPeriods));
		inciIO.setPremst(wsaaZ, inciIO.getPremcurr(wsaaZ));
		goTo(GotoLabel.nextPeriod32200);
	}

protected void writeInci39000()
	{
		/*WRITE-REC*/
		inciIO.setFunction(varcom.updat);
		inciIO.setFormat(formatsInner.incirec);
		SmartFileCode.execute(appVars, inciIO);
		if (isNE(inciIO.getStatuz(), varcom.oK)) {
			syserrrec.dbparams.set(inciIO.getParams());
			syserrrec.statuz.set(inciIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void allocation40000()
	{
		try {
			para40010();
			termFound40060();
			call10070();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void para40010()
	{
		/* If premium is zero skip all allocation processing.*/
		if (isEQ(wsaaInstprem, ZERO)) {
			goTo(GotoLabel.exit40090);
		}
		/* Set up correct parameters and call the subroutine UNITALOC*/
		/* to allocate the premium to buy units(i.e. writing UTRNs).*/
		rnlallrec.company.set(isuallrec.company);
		rnlallrec.chdrnum.set(isuallrec.chdrnum);
		rnlallrec.life.set(isuallrec.life);
		rnlallrec.coverage.set(isuallrec.coverage);
		rnlallrec.rider.set(isuallrec.rider);
		rnlallrec.planSuffix.set(isuallrec.planSuffix);
		rnlallrec.billfreq.set(chdrlnbIO.getBillfreq());
		rnlallrec.billcd.set(chdrlnbIO.getBillcd());
		rnlallrec.duedate.set(chdrlnbIO.getInstfrom());
		rnlallrec.tranno.set(chdrlnbIO.getTranno());
		/*    MOVE T5645-SACSCODE-01      TO RNLA-SACSCODE.*/
		/*    MOVE T5645-GLMAP-01         TO RNLA-GENLCDE.*/
		/*    MOVE T5645-SACSTYPE-01      TO RNLA-SACSTYP.*/
		/*    MOVE T5645-SACSCODE-02      TO RNLA-SACSCODE-02.*/
		/*    MOVE T5645-GLMAP-02         TO RNLA-GENLCDE-02.*/
		/*    MOVE T5645-SACSTYPE-02      TO RNLA-SACSTYP-02.*/
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			rnlallrec.sacscode.set(t5645rec.sacscode03);
			rnlallrec.genlcde.set(t5645rec.glmap03);
			rnlallrec.sacstyp.set(t5645rec.sacstype03);
			rnlallrec.sacscode02.set(t5645rec.sacscode04);
			rnlallrec.genlcde02.set(t5645rec.glmap04);
			rnlallrec.sacstyp02.set(t5645rec.sacstype04);
		}
		else {
			rnlallrec.sacscode.set(t5645rec.sacscode01);
			rnlallrec.genlcde.set(t5645rec.glmap01);
			rnlallrec.sacstyp.set(t5645rec.sacstype01);
			rnlallrec.sacscode02.set(t5645rec.sacscode02);
			rnlallrec.genlcde02.set(t5645rec.glmap02);
			rnlallrec.sacstyp02.set(t5645rec.sacstype02);
		}
		rnlallrec.cnttype.set(chdrlnbIO.getCnttype());
		rnlallrec.cntcurr.set(chdrlnbIO.getCntcurr());
		rnlallrec.batccoy.set(isuallrec.batccoy);
		rnlallrec.batcbrn.set(isuallrec.batcbrn);
		rnlallrec.batcactyr.set(isuallrec.batcactyr);
		rnlallrec.batcactmn.set(isuallrec.batcactmn);
		rnlallrec.batctrcde.set(isuallrec.batctrcde);
		rnlallrec.batcbatch.set(isuallrec.batcbatch);
		rnlallrec.user.set(isuallrec.user);
		rnlallrec.crtable.set(covrunlIO.getCrtable());
		rnlallrec.crdate.set(covrunlIO.getCrrcd());
		rnlallrec.moniesDate.set(wsaaMoniesDate);
		rnlallrec.anbAtCcd.set(covrunlIO.getAnbAtCcd());
		/* Calculate the term left to run if risk cessation term is 0.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrunlIO.getCrrcd());
		datcon3rec.intDate2.set(covrunlIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor, wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
	}

protected void termFound40060()
	{
		rnlallrec.termLeftToRun.set(wsaaRiskCessTerm);
		/*MOVE ISUA-COVR-INSTPREM     TO RNLA-COVR-INSTPREM.   <D9604> */
		/*                                                         <D9604>*/
		/* Use the pro-rata premium if premium has been received,  <D9604>*/
		/* otherwise use the coverage premium passed.              <D9604>*/
		/* Tot recd should only be set up when a pro-rata premium  <D9604>*/
		/* has been received.                                      <D9604>*/
		/*                                                         <D9604>*/
		if (isNE(wsaaPremiumPaid, isuallrec.covrInstprem)) {
			rnlallrec.covrInstprem.set(wsaaPremiumPaid);
			rnlallrec.totrecd.set(wsaaPremiumPaid);
		}
		else {
			rnlallrec.covrInstprem.set(isuallrec.covrInstprem);
			rnlallrec.totrecd.set(0);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rnlallrec.effdate.set(datcon1rec.intDate);
		/* UNITALOC only deals with one instalment at a time, so if more*/
		/* than one instalment is to be paid, UNITALOC has to be called*/
		/* repeatedly.*/
		if (isEQ(chdrlnbIO.getBtdate(), chdrlnbIO.getOccdate())) {
			goTo(GotoLabel.exit40090);
		}
		/* Check FUNCTION field to see if we should be calling UNITALOC*/
		/* or not - we always want to call it unless this subroutine has*/
		/* been invoked as part of Automatic Increase processing.*/
		if (isNE(isuallrec.function, SPACES)) {
			goTo(GotoLabel.exit40090);
		}
	}

protected void call10070()
	{
		callProgram(Unitaloc.class, rnlallrec.rnlallRec);
		if (isNE(rnlallrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(rnlallrec.statuz);
			syserrrec.params.set(rnlallrec.rnlallRec);
			systemError8000();
		}
	}

protected void updateCovr50000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					convertAnniversaryDate50010();
					unitCancellationDate50100();
					unitConversionDate50200();
				case extraAllocDate50400: 
					extraAllocDate50400();
				case unitStmntDate50500: 
					unitStmntDate50500();
					iuIncrDate50600();
				case updateCovrunl50700: 
					updateCovrunl50700();
				case exit50090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*    And also CHDR!
	* </pre>
	*/
protected void convertAnniversaryDate50010()
	{
		/*    MOVE CHDRLNB-OCCDATE        TO DTC2-INT-DATE-1.              */
		datcon2rec.intDate1.set(covrunlIO.getCrrcd());
		datcon2rec.freqFactor.set(1);
		datcon2rec.frequency.set("01");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError8000();
		}
		datcon2rec.intDate1.set(datcon2rec.intDate2);
		datcon2rec.freqFactor.set(-1);
		datcon2rec.frequency.set("DY");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError8000();
		}
		covrunlIO.setAnnivProcDate(datcon2rec.intDate2);
	}

protected void unitCancellationDate50100()
	{
		if (isEQ(t5540rec.iuDiscBasis, SPACES)) {
			goTo(GotoLabel.extraAllocDate50400);
		}
		if (isEQ(t5519rec.unitdeduc, "Y")) {
			/*         MOVE CHDRLNB-OCCDATE   TO DTC2-INT-DATE-1               */
			datcon2rec.intDate1.set(covrunlIO.getCrrcd());
			datcon2rec.frequency.set(t5519rec.initUnitChargeFreq);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon2rec.statuz);
				syserrrec.params.set(datcon2rec.datcon2Rec);
				systemError8000();
			}
			else {
				covrunlIO.setInitUnitCancDate(datcon2rec.intDate2);
				goTo(GotoLabel.extraAllocDate50400);
			}
		}
	}

protected void unitConversionDate50200()
	{
		if (isNE(t5519rec.unitadj, "Y")) {
			goTo(GotoLabel.extraAllocDate50400);
		}
		if (isEQ(t5519rec.fixdtrm, 0)) {
			covrunlIO.setConvertInitialUnits(covrunlIO.getRiskCessDate());
			goTo(GotoLabel.extraAllocDate50400);
		}
		wsaaDateSplit.set(chdrlnbIO.getOccdate());
		wsaaYearSplit.add(t5519rec.fixdtrm);
		covrunlIO.setConvertInitialUnits(wsaaDateSplit);
	}

protected void extraAllocDate50400()
	{
		if (isEQ(t5540rec.ltypst, SPACES)) {
			goTo(GotoLabel.unitStmntDate50500);
		}
		/*    MOVE CHDRLNB-OCCDATE        TO DTC2-INT-DATE-1.              */
		datcon2rec.intDate1.set(covrunlIO.getCrrcd());
		datcon2rec.freqFactor.set(t5535rec.unitAfterDur01);
		datcon2rec.frequency.set("12");
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			goTo(GotoLabel.exit50090);
		}
		covrunlIO.setReviewProcessing(datcon2rec.intDate2);
	}

protected void unitStmntDate50500()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		/* Use the frequency from T6659 to calculate the unit statement*/
		/* date. Increment the risk commencement date by the frequency*/
		/* obtained, this will give the unit statement date.*/
		if (isEQ(t6659rec.freq, SPACES)) {
			datcon2rec.frequency.set("00");
		}
		else {
			datcon2rec.frequency.set(t6659rec.freq);
		}
		datcon2rec.intDate1.set(chdrlnbIO.getOccdate());
		datcon2rec.intDate2.set(0);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.statuz.set(datcon2rec.statuz);
			syserrrec.params.set(datcon2rec.datcon2Rec);
			systemError8000();
		}
		chdrlnbIO.setStatementDate(datcon2rec.intDate2);
		covrunlIO.setUnitStatementDate(datcon2rec.intDate2);
		chdrlnbIO.setFunction(varcom.keeps);
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			systemError8000();
		}
	}

protected void iuIncrDate50600()
	{
		if (isEQ(t5540rec.iuDiscBasis, SPACES)) {
			goTo(GotoLabel.updateCovrunl50700);
		}
		if (isNE(t5519rec.diffprice, "Y")
		|| isNE(t5519rec.unitadj, "Y")) {
			goTo(GotoLabel.updateCovrunl50700);
		}
		covrunlIO.setInitUnitIncrsDate(covrunlIO.getAnnivProcDate());
	}

protected void updateCovrunl50700()
	{
		/* Update COVRUNL.*/
		covrunlIO.setChdrcoy(isuallrec.company);
		covrunlIO.setChdrnum(isuallrec.chdrnum);
		covrunlIO.setLife(isuallrec.life);
		covrunlIO.setCoverage(isuallrec.coverage);
		covrunlIO.setRider(isuallrec.rider);
		covrunlIO.setPlanSuffix(isuallrec.planSuffix);
		covrunlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrunlIO);
		if (isNE(covrunlIO.getStatuz(), varcom.oK)) {
			covrunlIO.setParams(covrunlIO.getParams());
			dbError8100();
		}
	}

protected void singlePremiumProcess60000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para60010();
				case fundAllocation60050: 
					fundAllocation60050();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para60010()
	{
		/* Need the allocation basis for single premiums if we are dealing*/
		/* with a lump sum.*/
		if (isNE(covrunlIO.getInstprem(), 0)) {
			getSingpAllocDets60100();
		}
		wsaaAmountHoldersInner.wsaaEnhanceAlloc.set(0);
		/* Calculate the uninvested allocation.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 3).setRounded(div((mult(wsaaSingPrem, (sub(100, wsaaPcUnit[1])))), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaNonInvestPrem);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaNonInvestPrem.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying initial units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 3).setRounded(div(mult((sub(wsaaSingPrem, wsaaAmountHoldersInner.wsaaNonInvestPrem)), wsaaPcInitUnit[1]), 100));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		/* Calculate the allocation for buying accumulation units.*/
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 3).setRounded(sub(wsaaSingPrem, (add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaAllocInit))));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		/* Check for any enhancement for these preimums from T6647.*/
		/* (T6647 has been read in 10400-GET-T6647 section.)*/
		if (isEQ(t6647rec.enhall, SPACES)) {
			goTo(GotoLabel.fundAllocation60050);
		}
		/* Read T5545 to get the enhancement percentage.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5545);
		itdmIO.setItmfrm(chdrlnbIO.getSinstfrom());
		wsaaT5545Cntcurr.set(chdrlnbIO.getCntcurr());
		wsaaT5545Enhall.set(t6647rec.enhall);
		itdmIO.setItemitem(wsaaT5545Item);
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.endp))
		&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5545))
		|| (isNE(itdmIO.getItemitem(), wsaaT5545Item))) {
			itdmIO.setGenarea(SPACES);
		}
		t5545rec.t5545Rec.set(itdmIO.getGenarea());
		if (isEQ(t5545rec.t5545Rec, SPACES)) {
			goTo(GotoLabel.fundAllocation60050);
		}
		/* Look for the correct percentage.*/
		wsaaEnhancePerc.set(0);
		wsaaIndex.set(1);
		while ( !((isGT(wsaaIndex, 6))
		|| (isNE(wsaaEnhancePerc, 0)))) {
			matchEnhanceBasis63000();
		}
		
		if (isEQ(wsaaEnhancePerc, 0)) {
			goTo(GotoLabel.fundAllocation60050);
		}
		/* Calculate the enhanced premiums.*/
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocInit);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocInit.set(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaOldAllocInit.set(wsaaAmountHoldersInner.wsaaAllocInit);
		compute(wsaaAmountHoldersInner.wsaaAllocInit, 3).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, wsaaEnhancePerc), 100));
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(sub(wsaaAmountHoldersInner.wsaaOldAllocInit, wsaaAmountHoldersInner.wsaaAllocInit));
		zrdecplrec.amountIn.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		a000CallRounding();
		wsaaAmountHoldersInner.wsaaAllocAccum.set(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaOldAllocAccum.set(wsaaAmountHoldersInner.wsaaAllocAccum);
		compute(wsaaAmountHoldersInner.wsaaAllocAccum, 3).setRounded(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaEnhancePerc), 100));
		compute(wsaaAmountHoldersInner.wsaaEnhanceAlloc, 0).set(add(wsaaAmountHoldersInner.wsaaEnhanceAlloc, (sub(wsaaAmountHoldersInner.wsaaOldAllocAccum, wsaaAmountHoldersInner.wsaaAllocAccum))));
	}

protected void fundAllocation60050()
	{
		/* Read ULNKRNL to get the fund split for the coverage/rider.*/
		ulnkrnlIO.setDataKey(SPACES);
		ulnkrnlIO.setChdrcoy(isuallrec.company);
		ulnkrnlIO.setChdrnum(isuallrec.chdrnum);
		ulnkrnlIO.setLife(isuallrec.life);
		ulnkrnlIO.setCoverage(isuallrec.coverage);
		ulnkrnlIO.setRider(isuallrec.rider);
		ulnkrnlIO.setPlanSuffix(isuallrec.planSuffix);
		ulnkrnlIO.setFunction(varcom.readr);
		ulnkrnlIO.setFormat(formatsInner.ulnkrnlrec);
		SmartFileCode.execute(appVars, ulnkrnlIO);
		if (isNE(ulnkrnlIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(ulnkrnlIO.getStatuz());
			syserrrec.params.set(ulnkrnlIO.getParams());
			dbError8100();
		}
		/* Calculate initial unit discount factor.*/
		/* Write a UTRN record for each fund split.*/
		initUnitDiscFactor65000();
		wsaaIndex.set(1);
		wsaaAmountHoldersInner.wsaaRunInitTot.set(0);
		wsaaAmountHoldersInner.wsaaRunAccumTot.set(0);
		while ( !((isGT(wsaaIndex, 10))
		|| (isEQ(ulnkrnlIO.getUalfnd(wsaaIndex), SPACES)))) {
			fundSplitAllocation64000();
		}
		
		/* Write a UTRN record for the uninvested premium.*/
		compute(wsaaAmountHoldersInner.wsaaNonInvestPrem, 0).set(add(wsaaAmountHoldersInner.wsaaNonInvestPrem, wsaaAmountHoldersInner.wsaaEnhanceAlloc));
		if (isEQ(wsaaAmountHoldersInner.wsaaNonInvestPrem, 0)) {
			return ;
		}
		utrnIO.setParams(SPACES);
		checkExistingUtrn66000();
		if (isEQ(utrnIO.getStatuz(), varcom.oK)) {
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(add(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaNonInvestPrem));
			rewriteExistingUtrn70000();
		}
		else {
			utrnIO.setUnitSubAccount("NVST");
			utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaNonInvestPrem);
			utrnIO.setDiscountFactor(0);
			setUpWriteUtrn61000();
		}
	}

protected void getSingpAllocDets60100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para60100();
				case nextFreq60140: 
					nextFreq60140();
				case endLoop60150: 
					endLoop60150();
				case exit60190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para60100()
	{
		/** There are 6 sets of details in the table, each set is led by*/
		/** the billing frequency. So to find the correct set of details,*/
		/** match each of these frequencies with a billing frequency of*/
		/** '00', for single premiums.*/
		/*GOT-RECORD*/
		wsaaZ.set(0);
	}

protected void nextFreq60140()
	{
		wsaaZ.add(1);
		if (isGT(wsaaZ, 6)) {
			goTo(GotoLabel.endLoop60150);
		}
		if (isEQ(t5536rec.billfreq[wsaaZ.toInt()], SPACES)) {
			goTo(GotoLabel.nextFreq60140);
		}
		if (isNE(t5536rec.billfreq[wsaaZ.toInt()], "00")) {
			goTo(GotoLabel.nextFreq60140);
		}
		/* The appropriate allocation basis details found.*/
		if (isEQ(wsaaZ, 1)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodas);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsas);
			wsaaPcUnits.set(t5536rec.pcUnitsas);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ, 2)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodbs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsbs);
			wsaaPcUnits.set(t5536rec.pcUnitsbs);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ, 3)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodcs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitscs);
			wsaaPcUnits.set(t5536rec.pcUnitscs);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ, 4)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodds);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsds);
			wsaaPcUnits.set(t5536rec.pcUnitsds);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ, 5)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodes);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitses);
			wsaaPcUnits.set(t5536rec.pcUnitses);
			goTo(GotoLabel.exit60190);
		}
		if (isEQ(wsaaZ, 6)) {
			wsaaMaxPeriods.set(t5536rec.maxPeriodfs);
			wsaaPcInitUnits.set(t5536rec.pcInitUnitsfs);
			wsaaPcUnits.set(t5536rec.pcUnitsfs);
			goTo(GotoLabel.exit60190);
		}
	}

protected void endLoop60150()
	{
		isuallrec.statuz.set(e616);
		exit090();
	}

protected void setUpWriteUtrn61000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para61010();
				case comlvlacc61925: 
					comlvlacc61925();
				case cont61950: 
					cont61950();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para61010()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(isuallrec.company);
		itdmIO.setItemtabl(tablesInner.t5515);
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setItemitem(utrnIO.getUnitVirtualFund());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), varcom.endp))
		&& (isNE(itdmIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			dbError8100();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setGenarea(SPACES);
		}
		if ((isNE(itdmIO.getItemcoy(), isuallrec.company))
		|| (isNE(itdmIO.getItemtabl(), tablesInner.t5515))
		|| (isNE(itdmIO.getItemitem(), utrnIO.getUnitVirtualFund()))) {
			itdmIO.setGenarea(SPACES);
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		utrnIO.setChdrcoy(isuallrec.company);
		utrnIO.setChdrnum(isuallrec.chdrnum);
		utrnIO.setCoverage(isuallrec.coverage);
		utrnIO.setLife(isuallrec.life);
		utrnIO.setRider(isuallrec.rider);
		utrnIO.setPlanSuffix(isuallrec.planSuffix);
		utrnIO.setTranno(chdrlnbIO.getTranno());
		utrnIO.setTransactionTime(varcom.vrcmTime);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		utrnIO.setTransactionDate(datcon1rec.intDate);
		utrnIO.setUser(isuallrec.user);
		utrnIO.setBatccoy(isuallrec.batccoy);
		utrnIO.setBatcbrn(isuallrec.batcbrn);
		utrnIO.setBatcactyr(isuallrec.batcactyr);
		utrnIO.setBatcactmn(isuallrec.batcactmn);
		utrnIO.setBatctrcde(isuallrec.batctrcde);
		utrnIO.setBatcbatch(isuallrec.batcbatch);
		utrnIO.setCrtable(covrunlIO.getCrtable());
		utrnIO.setCntcurr(chdrlnbIO.getCntcurr());
		if (isEQ(t5688rec.comlvlacc, "Y")) {
			goTo(GotoLabel.comlvlacc61925);
		}
		if (isEQ(utrnIO.getUnitSubAccount(), "NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode02);
			utrnIO.setSacstyp(t5645rec.sacstype02);
			utrnIO.setGenlcde(t5645rec.glmap02);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode01);
			utrnIO.setSacstyp(t5645rec.sacstype01);
			utrnIO.setGenlcde(t5645rec.glmap01);
		}
		goTo(GotoLabel.cont61950);
	}

protected void comlvlacc61925()
	{
		if (isEQ(utrnIO.getUnitSubAccount(), "NVST")) {
			utrnIO.setSacscode(t5645rec.sacscode04);
			utrnIO.setSacstyp(t5645rec.sacstype04);
			utrnIO.setGenlcde(t5645rec.glmap04);
		}
		else {
			utrnIO.setSacscode(t5645rec.sacscode03);
			utrnIO.setSacstyp(t5645rec.sacstype03);
			utrnIO.setGenlcde(t5645rec.glmap03);
		}
	}

protected void cont61950()
	{
		utrnIO.setContractType(chdrlnbIO.getCnttype());
		utrnIO.setSvp(1);
		utrnIO.setCrComDate(covrunlIO.getCrrcd());
		utrnIO.setFundCurrency(t5515rec.currcode);
		if (isLT(utrnIO.getContractAmount(), 0)) {
			utrnIO.setNowDeferInd(t6647rec.dealin);
		}
		else {
			utrnIO.setNowDeferInd(t6647rec.aloind);
		}
		utrnIO.setMoniesDate(wsaaMoniesDate);
		utrnIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnIO.setJobnoPrice(0);
		utrnIO.setFundRate(0);
		utrnIO.setStrpdate(0);
		utrnIO.setUstmno(0);
		utrnIO.setNofUnits(0);
		utrnIO.setNofDunits(0);
		utrnIO.setPriceDateUsed(0);
		utrnIO.setPriceUsed(0);
		utrnIO.setUnitBarePrice(0);
		utrnIO.setInciPerd01(0);
		utrnIO.setInciPerd02(0);
		utrnIO.setInciprm01(0);
		utrnIO.setInciprm02(0);
		utrnIO.setFundAmount(0);
		utrnIO.setSurrenderPercent(0);
		utrnIO.setInciNum(0);
		utrnIO.setFunction(varcom.writr);
		utrnIO.setFormat(formatsInner.utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if (isNE(utrnIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(utrnIO.getStatuz());
			syserrrec.params.set(utrnIO.getParams());
			dbError8100();
		}
	}

protected void matchEnhanceBasis63000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para63010();
					enhana63100();
				case enhanb63200: 
					enhanb63200();
				case enhanc63300: 
					enhanc63300();
				case enhand63400: 
					enhand63400();
				case enhane63500: 
					enhane63500();
				case enhanf63600: 
					enhanf63600();
				case nearExit63800: 
					nearExit63800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para63010()
	{
		/* First match the billing frequency.*/
		/* We are only dealing with Single Prems here, so check for '00'*/
		if (isEQ(t5545rec.billfreq[wsaaIndex.toInt()], SPACES)) {
			goTo(GotoLabel.nearExit63800);
		}
		if (isNE(t5545rec.billfreq[wsaaIndex.toInt()], "00")) {
			goTo(GotoLabel.nearExit63800);
		}
	}

	/**
	* <pre>
	* Once the billing frequency is matched, match the premium.
	* There are 5 premiums in each billing frequency set, so
	* match each premium individually.
	* </pre>
	*/
protected void enhana63100()
	{
		if (isNE(wsaaIndex, 1)) {
			goTo(GotoLabel.enhanb63200);
		}
		if ((isGTE(isuallrec.covrSingp, t5545rec.enhanaPrm05))
		&& (isNE(t5545rec.enhanaPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhanaPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp, t5545rec.enhanaPrm04))
			&& (isNE(t5545rec.enhanaPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhanaPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp, t5545rec.enhanaPrm03))
				&& (isNE(t5545rec.enhanaPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhanaPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp, t5545rec.enhanaPrm02))
					&& (isNE(t5545rec.enhanaPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhanaPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanaPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhanb63200()
	{
		if (isNE(wsaaIndex, 2)) {
			goTo(GotoLabel.enhanc63300);
		}
		if ((isGTE(isuallrec.covrSingp, t5545rec.enhanbPrm05))
		&& (isNE(t5545rec.enhanbPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhanbPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp, t5545rec.enhanbPrm04))
			&& (isNE(t5545rec.enhanbPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhanbPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp, t5545rec.enhanbPrm03))
				&& (isNE(t5545rec.enhanbPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhanbPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp, t5545rec.enhanbPrm02))
					&& (isNE(t5545rec.enhanbPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhanbPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanbPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhanc63300()
	{
		if (isNE(wsaaIndex, 3)) {
			goTo(GotoLabel.enhand63400);
		}
		if ((isGTE(isuallrec.covrSingp, t5545rec.enhancPrm05))
		&& (isNE(t5545rec.enhancPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhancPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp, t5545rec.enhancPrm04))
			&& (isNE(t5545rec.enhancPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhancPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp, t5545rec.enhancPrm03))
				&& (isNE(t5545rec.enhancPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhancPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp, t5545rec.enhancPrm02))
					&& (isNE(t5545rec.enhancPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhancPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhancPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhand63400()
	{
		if (isNE(wsaaIndex, 4)) {
			goTo(GotoLabel.enhane63500);
		}
		if ((isGTE(isuallrec.covrSingp, t5545rec.enhandPrm05))
		&& (isNE(t5545rec.enhandPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhandPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp, t5545rec.enhandPrm04))
			&& (isNE(t5545rec.enhandPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhandPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp, t5545rec.enhandPrm03))
				&& (isNE(t5545rec.enhandPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhandPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp, t5545rec.enhandPrm02))
					&& (isNE(t5545rec.enhandPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhandPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhandPc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhane63500()
	{
		if (isNE(wsaaIndex, 5)) {
			goTo(GotoLabel.enhanf63600);
		}
		if ((isGTE(isuallrec.covrSingp, t5545rec.enhanePrm05))
		&& (isNE(t5545rec.enhanePrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhanePc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp, t5545rec.enhanePrm04))
			&& (isNE(t5545rec.enhanePrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhanePc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp, t5545rec.enhanePrm03))
				&& (isNE(t5545rec.enhanePrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhanePc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp, t5545rec.enhanePrm02))
					&& (isNE(t5545rec.enhanePrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhanePc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanePc01);
					}
				}
			}
		}
		goTo(GotoLabel.nearExit63800);
	}

protected void enhanf63600()
	{
		if (isNE(wsaaIndex, 6)) {
			goTo(GotoLabel.nearExit63800);
		}
		if ((isGTE(isuallrec.covrSingp, t5545rec.enhanfPrm05))
		&& (isNE(t5545rec.enhanfPrm05, 0))) {
			wsaaEnhancePerc.set(t5545rec.enhanfPc05);
		}
		else {
			if ((isGTE(isuallrec.covrSingp, t5545rec.enhanfPrm04))
			&& (isNE(t5545rec.enhanfPrm04, 0))) {
				wsaaEnhancePerc.set(t5545rec.enhanfPc04);
			}
			else {
				if ((isGTE(isuallrec.covrSingp, t5545rec.enhanfPrm03))
				&& (isNE(t5545rec.enhanfPrm03, 0))) {
					wsaaEnhancePerc.set(t5545rec.enhanfPc03);
				}
				else {
					if ((isGTE(isuallrec.covrSingp, t5545rec.enhanfPrm02))
					&& (isNE(t5545rec.enhanfPrm02, 0))) {
						wsaaEnhancePerc.set(t5545rec.enhanfPc02);
					}
					else {
						wsaaEnhancePerc.set(t5545rec.enhanfPc01);
					}
				}
			}
		}
	}

protected void nearExit63800()
	{
		wsaaIndex.add(1);
		/*EXIT*/
	}

protected void fundSplitAllocation64000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					initUnit64100();
				case next64150: 
					next64150();
				case accumUnit64200: 
					accumUnit64200();
				case next64250: 
					next64250();
				case nearExit64800: 
					nearExit64800();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void initUnit64100()
	{
		/* To avoid rounding error, use the remainder(total investment -*/
		/* invested so far) to invest on the last fund.*/
		/* Invest the initial units premium in the funds specified in*/
		/* the ULNKRNL record. Write a UTRN record for each fund.*/
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocInit, 0)) {
			goTo(GotoLabel.accumUnit64200);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaIndex, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaRunInitTot));
			goTo(GotoLabel.next64150);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex, 1)), SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocInit, wsaaAmountHoldersInner.wsaaRunInitTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocInit, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
		}
	}

protected void next64150()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		a000CallRounding();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunInitTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaUtrnContractAmount.set(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaIndex));
		utrnIO.setUnitType("I");
		checkExistingUtrn66000();
		if (isEQ(utrnIO.getStatuz(), varcom.oK)) {
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(add(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaUtrnContractAmount));
			rewriteExistingUtrn70000();
		}
		else {
			utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaUtrnContractAmount);
			utrnIO.setUnitSubAccount("INIT");
			utrnIO.setDiscountFactor(wsaaAmountHoldersInner.wsaaInitUnitDiscFact);
			setUpWriteUtrn61000();
		}
	}

	/**
	* <pre>
	* Invest the accumulation units premium in the funds specified in
	* the ULNKRNL record. Write a UTRN record for each fund.
	* </pre>
	*/
protected void accumUnit64200()
	{
		if (isEQ(wsaaAmountHoldersInner.wsaaAllocAccum, 0)) {
			goTo(GotoLabel.nearExit64800);
		}
		utrnIO.setParams(SPACES);
		if (isEQ(wsaaIndex, 10)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaAmountHoldersInner.wsaaRunAccumTot));
			goTo(GotoLabel.next64250);
		}
		if (isEQ(ulnkrnlIO.getUalfnd(add(wsaaIndex, 1)), SPACES)) {
			setPrecision(utrnIO.getContractAmount(), 0);
			utrnIO.setContractAmount(sub(wsaaAmountHoldersInner.wsaaAllocAccum, wsaaAmountHoldersInner.wsaaRunAccumTot));
		}
		else {
			setPrecision(utrnIO.getContractAmount(), 1);
			utrnIO.setContractAmount(div(mult(wsaaAmountHoldersInner.wsaaAllocAccum, ulnkrnlIO.getUalprc(wsaaIndex)), 100), true);
		}
	}

protected void next64250()
	{
		zrdecplrec.amountIn.set(utrnIO.getContractAmount());
		a000CallRounding();
		utrnIO.setContractAmount(zrdecplrec.amountOut);
		wsaaAmountHoldersInner.wsaaRunAccumTot.add(utrnIO.getContractAmount());
		wsaaAmountHoldersInner.wsaaUtrnContractAmount.set(utrnIO.getContractAmount());
		utrnIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(wsaaIndex));
		utrnIO.setUnitType("A");
		checkExistingUtrn66000();
		if (isEQ(utrnIO.getStatuz(), varcom.oK)) {
			setPrecision(utrnIO.getContractAmount(), 2);
			utrnIO.setContractAmount(add(utrnIO.getContractAmount(), wsaaAmountHoldersInner.wsaaUtrnContractAmount));
			rewriteExistingUtrn70000();
		}
		else {
			utrnIO.setContractAmount(wsaaAmountHoldersInner.wsaaUtrnContractAmount);
			utrnIO.setUnitSubAccount("ACUM");
			utrnIO.setDiscountFactor(0);
			setUpWriteUtrn61000();
		}
	}

protected void nearExit64800()
	{
		wsaaIndex.add(1);
		/*EXIT*/
	}

protected void initUnitDiscFactor65000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					para65010();
					wholeOfLife65100();
				case termToRun65200: 
					termToRun65200();
					termFound65250();
				case fixedTerm65300: 
					fixedTerm65300();
				case exit65900: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para65010()
	{
		wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(1);
		if (((isEQ(t5540rec.iuDiscBasis, SPACES))
		|| (isEQ(t5540rec.iuDiscFact, SPACES)))
		&& (isEQ(t5540rec.wholeIuDiscFact, SPACES))) {
			goTo(GotoLabel.exit65900);
		}
	}

	/**
	* <pre>
	* There are 3 methods to obtain the discount factor, which one to
	* use is dependent on the field entries in T5540 & T5519 which hav
	* been read at inialisation stage.(Only one method is allowed.)
	* </pre>
	*/
protected void wholeOfLife65100()
	{
		/* If whole of life(T5540-WOL-IU-DISC-FACT) factor is not space,*/
		/* then use the age at next birthday to read off T6646 to obtain*/
		/* the discount factor.*/
		if (isEQ(t5540rec.wholeIuDiscFact, SPACES)) {
			goTo(GotoLabel.termToRun65200);
		}
		if (isEQ(covrunlIO.getAnbAtCcd(), 0)) {
			goTo(GotoLabel.exit65900);
		}
		getT664610700();
		/*    MOVE T6646-DFACT(COVRUNL-ANB-AT-CCD) TO                      */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(covrunlIO.getAnbAtCcd(), 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.dfact[covrunlIO.getAnbAtCcd().toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(covrunlIO.getAnbAtCcd(), 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t6646rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit65900);
	}

protected void termToRun65200()
	{
		/* If the discount basis(T5540-IU-DISC-BASIS) is not space, then*/
		/* use this basis to access T5519. If the fixed term(T5519-FIXDTRM)*/
		/* is not zero, use it to read off T5539 to obtain the discount*/
		/* factor, otherwise use the term left to run to read off T5539 to*/
		/* obtain the discount factor. T5539 is accessed by the discount*/
		/* factor(T5540-IU-DISC-FACT) fro T5540.*/
		if (isNE(t5519rec.fixdtrm, 0)
		&& (isGT(wsaaRiskCessTerm, t5519rec.fixdtrm))) {
			goTo(GotoLabel.fixedTerm65300);
		}
		/* Always Calculate The Term Left.*/
		/* Calculate the term left to run if risk cessation term is 0.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrunlIO.getCrrcd());
		datcon3rec.intDate2.set(covrunlIO.getRiskCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			datcon3rec.freqFactor.set(0);
		}
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);
		if (isGT(datcon3rec.freqFactor, wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}
	}

protected void termFound65250()
	{
		getT553910900();
		/*    MOVE T5539-DFACT(WSAA-RISK-CESS-TERM)    TO                  */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(wsaaRiskCessTerm, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[wsaaRiskCessTerm.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(wsaaRiskCessTerm, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		goTo(GotoLabel.exit65900);
	}

protected void fixedTerm65300()
	{
		/* use this fixed term to read off T5539 to obtain the*/
		/* discount factor.*/
		getT553910900();
		/*    MOVE T5539-DFACT(T5519-FIXDTRM) TO                           */
		/*         WSAA-INIT-UNIT-DISC-FACT.                               */
		if (isLT(t5519rec.fixdtrm, 100)) {
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		}
		else {
			compute(wsaaFact, 0).set(sub(t5519rec.fixdtrm, 99));
			wsaaAmountHoldersInner.wsaaInitUnitDiscFact.set(t5539rec.fact[wsaaFact.toInt()]);
		}
		compute(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 1).setRounded(div(wsaaAmountHoldersInner.wsaaInitUnitDiscFact, 100000));
		return ;
	}

protected void checkExistingUtrn66000()
	{
		para66010();
	}

protected void para66010()
	{
		utrnIO.setChdrcoy(isuallrec.company);
		utrnIO.setChdrnum(isuallrec.chdrnum);
		utrnIO.setCoverage(isuallrec.coverage);
		utrnIO.setLife(isuallrec.life);
		utrnIO.setRider(isuallrec.rider);
		utrnIO.setPlanSuffix(isuallrec.planSuffix);
		utrnIO.setTranno(chdrlnbIO.getTranno());
		utrnIO.setFunction(varcom.readh);
		utrnIO.setFormat(formatsInner.utrnrec);
		SmartFileCode.execute(appVars, utrnIO);
		if ((isNE(utrnIO.getStatuz(), varcom.oK))
		&& (isNE(utrnIO.getStatuz(), varcom.mrnf))) {
			syserrrec.params.set(utrnIO.getParams());
			syserrrec.statuz.set(utrnIO.getStatuz());
			dbError8100();
		}
	}

protected void rewriteExistingUtrn70000()
	{
		/*PARA*/
		utrnIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, utrnIO);
		if ((isNE(utrnIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(utrnIO.getParams());
			syserrrec.statuz.set(utrnIO.getStatuz());
			dbError8100();
		}
		/*EXIT*/
	}

protected void readAgecont80000()
	{
		/*READ-AGECONT*/
		itemIO.setItemitem(t5537rec.agecont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		wsaaIndex.subtract(99);
		/*EXIT*/
	}

protected void readTermcont90000()
	{
		/*READ-TERMCONT*/
		itemIO.setItemitem(t5537rec.trmcont);
		SmartFileCode.execute(appVars, itemIO);
		if ((isNE(itemIO.getStatuz(), varcom.oK))) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getDataArea());
			dbError8100();
		}
		t5537rec.t5537Rec.set(itemIO.getGenarea());
		/*EXIT*/
	}

protected void systemError8000()
	{
		se8000();
		seExit8090();
	}

protected void se8000()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void seExit8090()
	{
		isuallrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void dbError8100()
	{
		db8100();
		dbExit8190();
	}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		isuallrec.statuz.set(varcom.bomb);
		exit090();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(isuallrec.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		zrdecplrec.batctrcde.set(isuallrec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			systemError8000();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-AMOUNT-HOLDERS--INNER
 */
private static final class WsaaAmountHoldersInner { 
		/* WSAA-AMOUNT-HOLDERS */
	private ZonedDecimalData wsaaNonInvestPrem = new ZonedDecimalData(14, 3).init(0);
	private ZonedDecimalData wsaaAllocInit = new ZonedDecimalData(14, 3).init(0);
	private ZonedDecimalData wsaaRunInitTot = new ZonedDecimalData(13, 2).init(0);
	private ZonedDecimalData wsaaAllocAccum = new ZonedDecimalData(14, 3).init(0);
	private ZonedDecimalData wsaaRunAccumTot = new ZonedDecimalData(13, 2).init(0);
	private ZonedDecimalData wsaaInitUnitDiscFact = new ZonedDecimalData(10, 5).init(0);
	private ZonedDecimalData wsaaOldAllocAccum = new ZonedDecimalData(13, 2).init(0);
	private ZonedDecimalData wsaaOldAllocInit = new ZonedDecimalData(13, 2).init(0);
	private ZonedDecimalData wsaaEnhanceAlloc = new ZonedDecimalData(13, 2).init(0);
	private ZonedDecimalData wsaaUtrnContractAmount = new ZonedDecimalData(15, 2).init(0);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
	private FixedLengthStringData ulnkrec = new FixedLengthStringData(10).init("ULNKREC");
	private FixedLengthStringData utrnrec = new FixedLengthStringData(10).init("UTRNREC");
	private FixedLengthStringData ulnkrnlrec = new FixedLengthStringData(10).init("ULNKRNLREC");
	private FixedLengthStringData unltunlrec = new FixedLengthStringData(10).init("UNLTUNLREC");
	private FixedLengthStringData incirec = new FixedLengthStringData(10).init("INCIREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
}
/*
 * Class transformed  from Data Structure TABLES--INNER
 */
private static final class TablesInner { 
		/* TABLES */
	private FixedLengthStringData t5687 = new FixedLengthStringData(5).init("T5687");
	private FixedLengthStringData t5515 = new FixedLengthStringData(5).init("T5515");
	private FixedLengthStringData t5540 = new FixedLengthStringData(5).init("T5540");
	private FixedLengthStringData t5544 = new FixedLengthStringData(5).init("T5544");
	private FixedLengthStringData t5519 = new FixedLengthStringData(5).init("T5519");
	private FixedLengthStringData t5545 = new FixedLengthStringData(5).init("T5545");
	private FixedLengthStringData t6647 = new FixedLengthStringData(5).init("T6647");
	private FixedLengthStringData t5537 = new FixedLengthStringData(5).init("T5537");
	private FixedLengthStringData t5535 = new FixedLengthStringData(5).init("T5535");
	private FixedLengthStringData t5536 = new FixedLengthStringData(5).init("T5536");
	private FixedLengthStringData t5645 = new FixedLengthStringData(5).init("T5645");
	private FixedLengthStringData t5539 = new FixedLengthStringData(5).init("T5539");
	private FixedLengthStringData t6646 = new FixedLengthStringData(5).init("T6646");
	private FixedLengthStringData t6659 = new FixedLengthStringData(5).init("T6659");
	private FixedLengthStringData t5688 = new FixedLengthStringData(5).init("T5688");
}
}
