package com.csc.life.newbusiness.dataaccess.model;

public class Hpavpf {

	private long unique_number;
	private String chdrcoy; 
	private String chdrnum;  
	private String linsname;
	private int hoissdte; 
	private int anetpre;
	private int asptdty;
	private String curr;
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getLinsname() {
		return linsname;
	}
	public void setLinsname(String linsname) {
		this.linsname = linsname;
	}
	public int getHoissdte() {
		return hoissdte;
	}
	public void setHoissdte(int hoissdte) {
		this.hoissdte = hoissdte;
	}
	public int getAnetpre() {
		return anetpre;
	}
	public void setAnetpre(int anetpre) {
		this.anetpre = anetpre;
	}
	public int getAsptdty() {
		return asptdty;
	}
	public void setAsptdty(int asptdty) {
		this.asptdty = asptdty;
	}
	public String getCurr() {
		return curr;
	}
	public void setCurr(String curr) {
		this.curr = curr;
	}
	
	
	
	
}
