package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:42
 * Description:
 * Copybook name: HLTXKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Hltxkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData hltxFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData hltxKey = new FixedLengthStringData(64).isAPartOf(hltxFileKey, 0, REDEFINE);
  	public FixedLengthStringData hltxRequestCompany = new FixedLengthStringData(1).isAPartOf(hltxKey, 0);
  	public FixedLengthStringData hltxLetterType = new FixedLengthStringData(8).isAPartOf(hltxKey, 1);
  	public FixedLengthStringData hltxClntcoy = new FixedLengthStringData(1).isAPartOf(hltxKey, 9);
  	public FixedLengthStringData hltxClntnum = new FixedLengthStringData(8).isAPartOf(hltxKey, 10);
  	public PackedDecimalData hltxLetterSeqno = new PackedDecimalData(7, 0).isAPartOf(hltxKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(hltxKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(hltxFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		hltxFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}