/*
 * File: Br628.java
 * Date: 29 August 2009 22:29:34
 * Author: Quipoz Limited
 * 
 * Class transformed from BR628.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;

import com.csc.life.newbusiness.dataaccess.HpaxpfTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.Br628TempDAO;
import com.csc.life.newbusiness.dataaccess.model.Br628DTO;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.procedures.Contot;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   This batch program is a splitter program responsible for
*   extracting all HPADs with Process Flag not Y.
*
*   The extract records are held in the HPAXPF members created
*   in the prior process.  The HPAXPF members are then accessed
*   by the subsequent program to do the actual processing.
*
*   Initialise
*     - set contract number from and to.
*     - using SQL, extract data from HPADPF.
*     - define an array to hold HPAX records.
*
*    Read
*     - fetch a block of data into an array for HPAX records.
*
*    Perform    Until End of File
*
*      Edit
*       - dummy
*
*      Update
*       - write HPAX records from array to member(s).
*
*      Read next primary file record
*
*    End Perform
*
*   Close
*     - close SQL
*     - close all opened files.
*
*   Control totals:
*     01 - No. of threads used
*     02 - no. of records extracted
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*
****************************************************************** ****
* </pre>
*/
public class Br628 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag = false;
	private java.sql.ResultSet sqlhpaxpf1rs = null;
	private java.sql.PreparedStatement sqlhpaxpf1ps = null;
	private java.sql.Connection sqlhpaxpf1conn = null;
	private String sqlhpaxpf1 = "";
	private int hpaxpf1LoopIndex = 0;
	private DiskFileDAM hpax01 = new DiskFileDAM("HPAX01");
	private DiskFileDAM hpax02 = new DiskFileDAM("HPAX02");
	private DiskFileDAM hpax03 = new DiskFileDAM("HPAX03");
	private DiskFileDAM hpax04 = new DiskFileDAM("HPAX04");
	private DiskFileDAM hpax05 = new DiskFileDAM("HPAX05");
	private DiskFileDAM hpax06 = new DiskFileDAM("HPAX06");
	private DiskFileDAM hpax07 = new DiskFileDAM("HPAX07");
	private DiskFileDAM hpax08 = new DiskFileDAM("HPAX08");
	private DiskFileDAM hpax09 = new DiskFileDAM("HPAX09");
	private DiskFileDAM hpax10 = new DiskFileDAM("HPAX10");
	private DiskFileDAM hpax11 = new DiskFileDAM("HPAX11");
	private DiskFileDAM hpax12 = new DiskFileDAM("HPAX12");
	private DiskFileDAM hpax13 = new DiskFileDAM("HPAX13");
	private DiskFileDAM hpax14 = new DiskFileDAM("HPAX14");
	private DiskFileDAM hpax15 = new DiskFileDAM("HPAX15");
	private DiskFileDAM hpax16 = new DiskFileDAM("HPAX16");
	private DiskFileDAM hpax17 = new DiskFileDAM("HPAX17");
	private DiskFileDAM hpax18 = new DiskFileDAM("HPAX18");
	private DiskFileDAM hpax19 = new DiskFileDAM("HPAX19");
	private DiskFileDAM hpax20 = new DiskFileDAM("HPAX20");
	private HpaxpfTableDAM hpaxpfData = new HpaxpfTableDAM();
		/*    Change the record length to that of the temporary file.
		    This can be found by doing a DSPFD of the file being
		    duplicated by the CRTTMPF process.*/
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR628");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");

	/*    HPAX member parameters*/
	private FixedLengthStringData wsaaHpaxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaHpaxFn, 0, FILLER).init("HPAX");
	private FixedLengthStringData wsaaRunid = new FixedLengthStringData(2).isAPartOf(wsaaHpaxFn, 4);
	private ZonedDecimalData wsaaJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHpaxFn, 6).setUnsigned();
		/*    Host variables*/
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaEffdate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaValidflag1 = new FixedLengthStringData(1).init("1");
	private FixedLengthStringData wsaaYes = new FixedLengthStringData(1).init("Y");
	private ZonedDecimalData wsaaMaxDate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaThread = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThread, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThread, 6).setUnsigned();
	private FixedLengthStringData filler2 = new FixedLengthStringData(1).isAPartOf(wsaaThread, 9, FILLER).init(SPACES);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
		/*    Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*    Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaHpaxData = FLSInittedArray (1000, 16);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaHpaxData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaHpaxData, 1);
	private FixedLengthStringData[] wsaaValidflag = FLSDArrayPartOfArrayStructure(1, wsaaHpaxData, 9);
	private PackedDecimalData[] wsaaZsufcdte = PDArrayPartOfArrayStructure(8, 0, wsaaHpaxData, 10);
	private FixedLengthStringData[] wsaaProcflg = FLSDArrayPartOfArrayStructure(1, wsaaHpaxData, 15);
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private P6671par p6671par = new P6671par();

    private Br628TempDAO tempDAO = getApplicationContext().getBean("br628TempDAO", Br628TempDAO.class);
	
    private int ctrCT02;
	
	public Br628() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getLsaaStatuz() {
		return lsaaStatuz;
	}

	protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
		this.lsaaStatuz = lsaaStatuz;
	}

	protected FixedLengthStringData getLsaaBsscrec() {
		return lsaaBsscrec;
	}

	protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
		this.lsaaBsscrec = lsaaBsscrec;
	}

	protected FixedLengthStringData getLsaaBsprrec() {
		return lsaaBsprrec;
	}

	protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
		this.lsaaBsprrec = lsaaBsprrec;
	}

	protected FixedLengthStringData getLsaaBprdrec() {
		return lsaaBprdrec;
	}

	protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
		this.lsaaBprdrec = lsaaBprdrec;
	}

	protected FixedLengthStringData getLsaaBuparec() {
		return lsaaBuparec;
	}

	protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
		this.lsaaBuparec = lsaaBuparec;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void restart0900() {
		/* RESTART */
		/** The restart section is being handled in section 1100. */
		/** This is because the code is applicable to every run, */
		/** not just restarts. */
		/* EXIT */
	}

	protected void initialise1000() {
		/* Check that the restart method is compatible with the program. */
		/* This program is restartable but will always re-run from */
		/* the beginning. This is because the temporary file members */
		/* are not under commitment control. */
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.syserrType.set("2");
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/* The next thing we must do is construct the name of the */
		/* temporary file which we will be working with. */
		wsaaRunid.set(bprdIO.getSystemParam04());
		wsaaJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());

		/* Do ovrdbf for each temporary file member. Note that we have */
		/* allowed for a maximum of 20. */
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		
		wsaaCompany.set(bsprIO.getCompany());
		wsaaEffdate.set(bsscIO.getEffectiveDate());
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		wsaaMaxDate.set(varcom.vrcmMaxDate);
		if (isEQ(p6671par.chdrnum, SPACES) && isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		} else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		
		// add by fwang3 for batch
		Br628DTO dto = new Br628DTO();
		dto.setTableName(wsaaHpaxFn.toString());
		dto.setCompany(wsaaCompany.toString());
		dto.setChdrnumfrom(wsaaChdrnumFrom.toString());
		dto.setChdrnumto(wsaaChdrnumTo.toString());
		dto.setValidflag1(wsaaValidflag1.toString());
		dto.setEffdate(wsaaEffdate.toInt());
		dto.setMaxdate(wsaaMaxDate.toInt());
		dto.setYes(wsaaYes.toString());
		dto.setThreadNo(wsaaThread.toString());
		ctrCT02 = tempDAO.buildTempData(dto);
	}
	
	
	private void openThreadMember1100() {
		/*    Clear the member for this thread in case we are in restart*/
		/*    mode in which case it might already contain data.*/
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaHpaxFn, wsaaThread, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			syserrrec.syserrType.set("2");
			syserrrec.subrname.set("CLRTMPF");
			syserrrec.statuz.set(wsaaStatuz);
			fatalError600();
		}
	}


	protected void readFile2000() {
		 
		wsspEdterror.set(varcom.endp);
	}


	protected void edit2500() {
		/* READ */
		//wsspEdterror.set(varcom.endp);
		/* EXIT */
	}

	protected void update3000() {
//		ctrCT02++;
	}



	protected void commit3500() {
		commitControlTotals();
	}
	
	protected void commitControlTotals(){
		/*    Log the number of threads being used*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		callContot001();
		
		/*    Log the number of extracted records.*/
		contotrec.totno.set(ct02);
		contotrec.totval.set(ctrCT02);
		callContot001();
		ctrCT02 = 0;
	}


	protected void rollback3600() {
		/* ROLLBACK */
		/** There is no pre-rollback processing to be done */
		/* EXIT */
	}

	protected void close4000() {

	}



	protected void sqlError500() {
		/* CALL-SYSTEM-ERROR */
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set("ESQL");
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
