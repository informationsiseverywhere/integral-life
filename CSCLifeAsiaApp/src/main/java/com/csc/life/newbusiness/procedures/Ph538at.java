/*
 * File: Ph538at.java
 * Date: 30 August 2009 1:06:45
 * Author: Quipoz Limited
 *
 * Class transformed from PH538AT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceAll;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.general.dataaccess.LetcpcmTableDAM;
import com.csc.fsu.general.recordstructures.Letcmntkey;
import com.csc.fsu.general.tablestructures.T3609rec;
import com.csc.fsu.printing.dataaccess.LetcmntTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atmodrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS
*
*       SCHEDULE REPRINT - AT MODULE
*       ----------------------------
*
*  This program will be run under AT and will reprint the
*  schedule requested via PH538.
*  The program will carry out the following functions:
*
*  1.  Retrieve the LETCMNT record for reprint from the primary
*      key in ATRQ.  Only process if the letter status is '1' not
*      printed.
*
*  2.  Call the printing routine defined in T3609 for the form
*      type in LETCMNT.(For details, refer program B2703)
*
*  3.  Update LETCMNT letter status to '2' printed.
*
*  4.  Release soft lock on the contract.
*
*  Processing:
*
*  Letter Request Record.
*
*  The ATMOD-PRIMARY-KEY will contain the LETC key which refers
*  to the letter type for the schedule to be printed.
*  Update the LETC record, set LETTER-STATUS = '2' and print date
*  = today.
*
*  Notes.
*
*  Tables:
*  T3609 - Standard Letter Control            Key: Letter Type
*
*  LETC Record:
*  . LETCMNT-DATA-KEY          -  ATMOD Primary Key
*  . LETCMNT-LETTER-PRINT-DATE -  Today
*  . LETCMNT-STATUS            -  '2'
*
****************************************************************** ****
* </pre>
*/
public class Ph538at extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(8).init("PH538AT");

	private FixedLengthStringData wsaaSkip = new FixedLengthStringData(1).init(SPACES);
	private Validator skipped = new Validator(wsaaSkip, "Y");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);
	private FixedLengthStringData wsaaCommentTest = new FixedLengthStringData(1);
	private static final String wsaaFsuCoy = "";

		/* One Command line is made up of:
		 1st Character: To Identify New Command
		 Rest of Characters: May Hold the Command*/
	private FixedLengthStringData wsaaCommand = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaCommand1 = new FixedLengthStringData(1).isAPartOf(wsaaCommand, 0);
	private FixedLengthStringData wsaaCommandDetails = new FixedLengthStringData(59).isAPartOf(wsaaCommand, 1);
		/* WSAA-SUBSCRIPTS */
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
		/* WSAA-THIS-AND-THAT */
	private PackedDecimalData wsaaQcmdexcInd = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaItemitem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPcPrintInd = new FixedLengthStringData(1).init(SPACES);

		/* Hold the length of the command to be Executed
		 in QCMDEXC.*/
	private FixedLengthStringData wsaaCommandLength = new FixedLengthStringData(8);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).isAPartOf(wsaaCommandLength, 0);

		/* Command Lines.*/
	private FixedLengthStringData wsaaQcmdexcCommand = new FixedLengthStringData(1440);
	private FixedLengthStringData[] wsaaQcmdexcCmd = FLSArrayPartOfStructure(24, 60, wsaaQcmdexcCommand, 0);

	private FixedLengthStringData wsaaQcmdexcArea = new FixedLengthStringData(60);
	private FixedLengthStringData wsaaQcmdexcPgmInd = new FixedLengthStringData(3).isAPartOf(wsaaQcmdexcArea, 5);
		/*03  WSAA-QCMDEXC-SBR.                                         */
	private FixedLengthStringData wsaaQcmdexcPgm = new FixedLengthStringData(10).isAPartOf(wsaaQcmdexcArea, 9);
		/* FORMATS */
	private static final String letcrec = "LETCREC";
	private static final String letcpcmrec = "LETCPCMREC";
		/* TABLES */
	private static final String t3609 = "T3609";
	private static final String t2634 = "T2634";
	private IntegerData qcmdexcInd = new IntegerData();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LetcmntTableDAM letcmntIO = new LetcmntTableDAM();
	private LetcpcmTableDAM letcpcmIO = new LetcpcmTableDAM();
	private Letcmntkey wsaaLetcmntkey = new Letcmntkey();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T3609rec t3609rec = new T3609rec();
	private Atmodrec atmodrec = new Atmodrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readNext1180
	}

	public Ph538at() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		atmodrec.atmodRec = convertAndSetParam(atmodrec.atmodRec, parmArray, 0);
		try {
			mainline0000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline0000()
	{
		mainline0001();
		exit0009();
	}

protected void mainline0001()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon1rec.datcon1Rec);
			xxxxFatalError();
		}
		wsaaToday.set(datcon1rec.intDate);
		/* PERFORM 1000-PRINT-LETTER.                                   */
		/* IF  NOT SKIPPED                                              */
		/*     PERFORM 2000-UPDATE-LETC.                                */
		/* We need to loop on LETCMNT because it is possible that a        */
		/* letter type has many of sub letter type.                        */
		letcmntIO.setDataKey(atmodrec.primaryKey);
		wsaaLetcmntkey.letcmntFileKey.set(atmodrec.primaryKey);
		letcmntIO.setLetterSeqno(0);
		letcmntIO.setFormat(letcrec);
		letcmntIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, letcmntIO);
		if (isNE(letcmntIO.getStatuz(), varcom.oK)
		&& isNE(letcmntIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(letcmntIO.getParams());
			xxxxFatalError();
		}
		if (isNE(letcmntIO.getRequestCompany(), wsaaLetcmntkey.letcmntRequestCompany)
		|| isNE(letcmntIO.getLetterType(), wsaaLetcmntkey.letcmntLetterType)
		|| isNE(letcmntIO.getClntcoy(), wsaaLetcmntkey.letcmntClntcoy)
		|| isNE(letcmntIO.getRdocnum(), wsaaLetcmntkey.letcmntRdocnum)
		|| isNE(letcmntIO.getClntnum(), wsaaLetcmntkey.letcmntClntnum)) {
			letcmntIO.setStatuz(varcom.endp);
		}
		while ( !(isEQ(letcmntIO.getStatuz(), varcom.endp))) {
			processThruLetcmnt1100();
		}

		releaseSoftlock3000();
	}

protected void exit0009()
	{
		exitProgram();
	}

	/**
	* <pre>
	* Bomb out of this AT module using the fatal error section    *
	* copied from MAINF.                                          *
	* </pre>
	*/
protected void xxxxFatalError()
	{
		xxxxFatalErrors();
		xxxxErrorProg();
	}

protected void xxxxFatalErrors()
	{
		if (isEQ(syserrrec.statuz, varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType, "2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void xxxxErrorProg()
	{
		/* AT*/
		atmodrec.statuz.set(varcom.bomb);
		/*XXXX-EXIT*/
		exitProgram();
	}

	/**
	* <pre>
	*1000-PRINT-LETTER SECTION.
	*1000-READH-LETC.
	*    Read & hold LETC record
	*****MOVE ATMD-PRIMARY-KEY        TO LETCMNT-DATA-KEY.
	*****MOVE LETCREC                 TO LETCMNT-FORMAT.
	*****MOVE READH                   TO LETCMNT-FUNCTION.
	*****CALL 'LETCMNTIO'  USING LETCMNT-PARAMS.
	*****IF LETCMNT-STATUZ            NOT = O-K
	*****   MOVE LETCMNT-PARAMS       TO SYSR-PARAMS
	*****   PERFORM XXXX-FATAL-ERROR
	*****ELSE
	*****   IF LETCMNT-LETTER-STATUS  NOT = '1'
	*****      MOVE 'Y'               TO WSAA-SKIP
	*****      GO TO 1090-EXIT
	*****   END-IF
	*****END-IF.
	*1020-PRINT.
	**Read table T3609 with LETC-LETTER-TYPE.
	*****MOVE SPACES                 TO ITEM-PARAMS.
	*****MOVE 'IT'                   TO ITEM-ITEMPFX.
	*****MOVE LETCMNT-REQUEST-COMPANY TO ITEM-ITEMCOY.
	*****MOVE T3609                  TO ITEM-ITEMTABL.
	*****MOVE LETCMNT-LETTER-TYPE    TO ITEM-ITEMITEM.
	*****MOVE BEGN                   TO ITEM-FUNCTION.
	*****CALL 'ITEMIO' USING ITEM-PARAMS.
	*****IF ITEM-STATUZ NOT = O-K
	*****   MOVE ITEM-PARAMS         TO SYSR-PARAMS
	*****   PERFORM XXXX-FATAL-ERROR
	*****END-IF.
	*****MOVE ITEM-ITEMITEM          TO WSAA-ITEMITEM.
	*****MOVE ITEM-GENAREA           TO T3609-T3609-REC.
	*****PERFORM 1400-PROCESS-TABLE-ITEM.
	*1090-EXIT.
	*****EXIT.
	* </pre>
	*/
protected void processThruLetcmnt1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1110();
					updtRec1170();
				case readNext1180:
					readNext1180();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start1110()
	{
		if (isNE(letcmntIO.getLetterStatus(), "1")) {
			goTo(GotoLabel.readNext1180);
		}
		/* Read table T3609 with LETCMNT-LETTER-TYPE/LETCMNT-HSUBLET       */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcmntIO.getRequestCompany());
		itemIO.setItemtabl(t3609);
		if (isNE(letcmntIO.getHsublet(), SPACES)) {
			itemIO.setItemitem(letcmntIO.getHsublet());
		}
		else {
			itemIO.setItemitem(letcmntIO.getLetterType());
		}
		itemIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		wsaaItemitem.set(itemIO.getItemitem());
		t3609rec.t3609Rec.set(itemIO.getGenarea());
		processTableItem1400();
	}

protected void updtRec1170()
	{
		updateLetc2000();
	}

protected void readNext1180()
	{
		letcmntIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, letcmntIO);
		if (isNE(letcmntIO.getStatuz(), varcom.oK)
		&& isNE(letcmntIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(letcmntIO.getParams());
			xxxxFatalError();
		}
		if (isNE(letcmntIO.getRequestCompany(), wsaaLetcmntkey.letcmntRequestCompany)
		|| isNE(letcmntIO.getLetterType(), wsaaLetcmntkey.letcmntLetterType)
		|| isNE(letcmntIO.getClntcoy(), wsaaLetcmntkey.letcmntClntcoy)
		|| isNE(letcmntIO.getRdocnum(), wsaaLetcmntkey.letcmntRdocnum)
		|| isNE(letcmntIO.getClntnum(), wsaaLetcmntkey.letcmntClntnum)) {
			letcmntIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void processTableItem1400()
	{
		tableItem1400();
	}

protected void tableItem1400()
	{
		wsaaSub.set(1);
		qcmdexcInd.set(1);
		wsaaCommandLength.set(ZERO);
		wsaaCommand.set(SPACES);
		wsaaCommandDetails.set("X");
		/*  Process the Commands until*/
		/*  (1) The command is spaces or*/
		/*  (2) The item changes or*/
		/*  (3) The Item status is ENDP.*/
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItempfx(), "IT")
		|| isNE(itemIO.getItemcoy(), letcmntIO.getRequestCompany())
		|| isNE(itemIO.getItemtabl(), t3609)
		|| isNE(itemIO.getItemitem(), wsaaItemitem)
		|| isEQ(wsaaCommand, SPACES))) {
			wsaaCommand1.set(SPACES);
			while ( !(isEQ(itemIO.getStatuz(), varcom.endp)
			|| isNE(itemIO.getItempfx(), "IT")
			|| isNE(itemIO.getItemcoy(), letcmntIO.getRequestCompany())
			|| isNE(itemIO.getItemtabl(), t3609)
			|| isNE(itemIO.getItemitem(), wsaaItemitem)
			|| isEQ(wsaaCommand, SPACES)
			|| isNE(wsaaCommand1, SPACES))) {
				/* Calculating the length of the QCMDEXC Command because*/
				/* QCMDEXC Requires the String Field & The Length if the*/
				/* Command to be Processed.*/
				processLine1600();
			}

			wsaaQcmdexcInd.set(qcmdexcInd);
			compute(wsaaQcmdexcLength, 5).set(sub(wsaaQcmdexcInd, 1));
			compute(wsaaQcmdexcLength, 5).set(mult(wsaaQcmdexcLength, 60));
			parameterSubstitution1900();
			/* Call the command using Pgm id as define in table T3609 or*/
			/* command QCMDEXC*/
			wsaaQcmdexcArea.set(wsaaQcmdexcCmd[1]);
			/*    T3609 will define subroutine to perform schedule printing*/
			/*  Note: The exact format calling the subroutine has not been*/
			/*        finalised at this stage and the parameters passed.*/
			/*        Make sure this is re-visited when decided.*/
			if ((isEQ(wsaaQcmdexcPgmInd, "SBR"))){
				wsaaQcmdexcPgm.set(inspectReplaceAll(wsaaQcmdexcPgm, ")", SPACES));
				/*     CALL WSAA-QCMDEXC-SBR USING LETCMNT-PARAMS      */
				/*              CALL WSAA-QCMDEXC-PGM USING LETCMNT-PARAMS<FSA87*/
				/*             IF LETCMNT-STATUZ NOT = O-K                      */
				/*                 MOVE LETCMNT-PARAMS TO SYSR-PARAMS           */
				/*                 PERFORM XXXX-FATAL-ERROR                     */
				/*             END-IF                                           */
				/* PC printing calls the routine using HLETCMN file                */
				checkPcPrinting1500();
				if (isEQ(wsaaPcPrintInd, "Y")) {
					/*                CALL WSAA-QCMDEXC-PGM USING HLETCMN-PARAMS<PCP*/
					/*                IF HLETCMN-STATUZ NOT = O-K           <PCPPRT>*/
					/*                   MOVE HLETCMN-PARAMS TO SYSR-PARAMS <PCPPRT>*/
					FixedLengthStringData groupTEMP = letcpcmIO.getParams();
					callProgram(wsaaQcmdexcPgm, groupTEMP);
					letcpcmIO.setParams(groupTEMP);
					if (isNE(letcpcmIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(letcpcmIO.getParams());
						xxxxFatalError();
					}
				}
				else {
					FixedLengthStringData groupTEMP2 = letcmntIO.getParams();
					callProgram(wsaaQcmdexcPgm, groupTEMP2);
					letcmntIO.setParams(groupTEMP2);
					if (isNE(letcmntIO.getStatuz(), varcom.oK)) {
						syserrrec.params.set(letcmntIO.getParams());
						xxxxFatalError();
					}
				}
			}
			else if ((isNE(wsaaQcmdexcPgmInd, "SBR"))){
				com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexcCommand, wsaaQcmdexcLength);
				/*    Resetting the Index Back to 1*/
				qcmdexcInd.set(1);
			}
		}

	}

protected void checkPcPrinting1500()
	{
		start1510();
	}

protected void start1510()
	{
		/* Read TH625, if item found means this is a PC printing.          */
		/* Then we need to read HLETCMN file and use its params as         */
		/* a linkage to call the subroutine. (All PC printing routine      */
		/* use HLETRMN for its linkage section).                           */
		itemIO.setParams(SPACES);
		wsaaPcPrintInd.set(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(letcmntIO.getRequestCompany());
		/* MOVE TH625                  TO ITEM-ITEMTABL.                */
		itemIO.setItemtabl(t2634);
		itemIO.setItemitem(letcmntIO.getHsublet());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			xxxxFatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			wsaaPcPrintInd.set("Y");
			readHletcmn1550();
		}
	}

protected void readHletcmn1550()
	{
		start1551();
	}

protected void start1551()
	{
		/*                                                      <PCPPRT>*/
		/* INITIALIZE                     HLETCMN-PARAMS.       <PCPPRT>*/
		/* MOVE LETCMNT-REQUEST-COMPANY                         <PCPPRT>*/
		/*                             TO HLETCMN-REQUEST-COMPANY.CPPRT>*/
		/* MOVE LETCMNT-LETTER-TYPE    TO HLETCMN-LETTER-TYPE.  <PCPPRT>*/
		/* MOVE LETCMNT-CLNTCOY        TO HLETCMN-CLNTCOY.      <PCPPRT>*/
		/* MOVE LETCMNT-RDOCNUM        TO HLETCMN-RDOCNUM.      <PCPPRT>*/
		/* MOVE LETCMNT-CLNTNUM        TO HLETCMN-CLNTNUM.      <PCPPRT>*/
		/* MOVE LETCMNT-HSUBLET        TO HLETCMN-HSUBLET.      <PCPPRT>*/
		/* MOVE LETCMNT-LETTER-SEQNO   TO HLETCMN-LETTER-SEQNO. <PCPPRT>*/
		/*                                                      <PCPPRT>*/
		/* MOVE HLETCMNREC             TO HLETCMN-FORMAT.       <PCPPRT>*/
		/* MOVE READR                  TO HLETCMN-FUNCTION.     <PCPPRT>*/
		/*                                                      <PCPPRT>*/
		/* CALL 'HLETCMNIO'         USING HLETCMN-PARAMS.       <PCPPRT>*/
		/*                                                      <PCPPRT>*/
		/* IF HLETCMN-STATUZ           NOT = O-K                <PCPPRT>*/
		/*    MOVE HLETCMN-PARAMS      TO SYSR-PARAMS           <PCPPRT>*/
		/*    PERFORM XXXX-FATAL-ERROR                          <PCPPRT>*/
		/* END-IF.                                              <PCPPRT>*/
		letcpcmIO.setParams(SPACES);
		letcpcmIO.setRequestCompany(letcmntIO.getRequestCompany());
		letcpcmIO.setLetterType(letcmntIO.getLetterType());
		letcpcmIO.setClntcoy(letcmntIO.getClntcoy());
		letcpcmIO.setRdocnum(letcmntIO.getRdocnum());
		letcpcmIO.setClntnum(letcmntIO.getClntnum());
		letcpcmIO.setHsublet(letcmntIO.getHsublet());
		letcpcmIO.setLetterSeqno(letcmntIO.getLetterSeqno());
		letcpcmIO.setFormat(letcpcmrec);
		letcpcmIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, letcpcmIO);
		if (isNE(letcpcmIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(letcpcmIO.getParams());
			xxxxFatalError();
		}
	}

protected void processLine1600()
	{
		line1600();
	}

protected void line1600()
	{
		wsaaCommentTest.set(t3609rec.letterControl[wsaaSub.toInt()]);
		if (isNE(wsaaCommentTest, "*")) {
			wsaaQcmdexcCmd[qcmdexcInd.toInt()].set(t3609rec.letterControl[wsaaSub.toInt()]);
			qcmdexcInd.add(1);
		}
		if (isLT(wsaaSub, 8)) {
			wsaaSub.add(1);
		}
		else {
			wsaaSub.set(1);
			itemIO.setFunction(varcom.nextr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(itemIO.getParams());
				xxxxFatalError();
			}
			else {
				t3609rec.t3609Rec.set(itemIO.getGenarea());
			}
		}
		if (isEQ(wsaaCommentTest, "*")) {
			wsaaCommand.fill("*");
			wsaaCommand1.set(SPACES);
		}
		else {
			wsaaCommand.set(t3609rec.letterControl[wsaaSub.toInt()]);
		}
	}

protected void parameterSubstitution1900()
	{
		/*SUBSTITUTION*/
		/*    If the command string has the user number replace it with*/
		/*    something numeric.*/
		wsaaQcmdexcCommand.set(inspectReplaceAll(wsaaQcmdexcCommand, "UUUUUU", "999999"));
		/*    If the command string has the client company . . .*/
		wsaaQcmdexcCommand.set(inspectReplaceAll(wsaaQcmdexcCommand, "!", wsaaFsuCoy));
		/*EXIT*/
	}

protected void updateLetc2000()
	{
		/*UPDATE*/
		/*    Update the letter status and the letter print date*/
		/*    on all the LETC records with a letter type the same*/
		/*    As the letter type just read on T3609.*/
		letcmntIO.setLetterStatus("2");
		letcmntIO.setLetterPrintDate(wsaaToday);
		letcmntIO.setFormat(letcrec);
		letcmntIO.setFunction(varcom.rewrt);
		/*  MOVE UPDAT                  TO LETCMNT-FUNCTION.             */
		SmartFileCode.execute(appVars, letcmntIO);
		if (isNE(letcmntIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(letcmntIO.getParams());
			xxxxFatalError();
		}
		/*EXIT*/
	}

protected void releaseSoftlock3000()
	{
		release3000();
	}

protected void release3000()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(atmodrec.company);
		/* MOVE LETCMNT-OTHER-KEYS(1:8) TO SFTL-ENTITY.                 */
		sftlockrec.entity.set(wsaaLetcmntkey.letcmntRdocnum);
		sftlockrec.enttyp.set("CH");
		sftlockrec.transaction.set(atmodrec.batchKey);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			xxxxFatalError();
		}
	}
}
