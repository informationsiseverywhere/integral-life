package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgcmpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:41
 * Class transformed from AGCMPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgcmpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 256;
	public FixedLengthStringData agcmrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agcmpfRecord = agcmrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(agcmrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(agcmrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agcmrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(agcmrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(agcmrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(agcmrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(agcmrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(agcmrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(agcmrec);
	public PackedDecimalData efdate = DD.efdate.copy().isAPartOf(agcmrec);
	public PackedDecimalData annprem = DD.annprem.copy().isAPartOf(agcmrec);
	public FixedLengthStringData basicCommMeth = DD.bascmeth.copy().isAPartOf(agcmrec);
	public PackedDecimalData initcom = DD.initcom.copy().isAPartOf(agcmrec);
	public FixedLengthStringData bascpy = DD.bascpy.copy().isAPartOf(agcmrec);
	public PackedDecimalData compay = DD.compay.copy().isAPartOf(agcmrec);
	public PackedDecimalData comern = DD.comern.copy().isAPartOf(agcmrec);
	public FixedLengthStringData srvcpy = DD.srvcpy.copy().isAPartOf(agcmrec);
	public PackedDecimalData scmdue = DD.scmdue.copy().isAPartOf(agcmrec);
	public PackedDecimalData scmearn = DD.scmearn.copy().isAPartOf(agcmrec);
	public FixedLengthStringData rnwcpy = DD.rnwcpy.copy().isAPartOf(agcmrec);
	public PackedDecimalData rnlcdue = DD.rnlcdue.copy().isAPartOf(agcmrec);
	public PackedDecimalData rnlcearn = DD.rnlcearn.copy().isAPartOf(agcmrec);
	public FixedLengthStringData agentClass = DD.agcls.copy().isAPartOf(agcmrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(agcmrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(agcmrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(agcmrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(agcmrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(agcmrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(agcmrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(agcmrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(agcmrec);
	public PackedDecimalData seqno = DD.seqno.copy().isAPartOf(agcmrec);
	public PackedDecimalData ptdate = DD.ptdate.copy().isAPartOf(agcmrec);
	public FixedLengthStringData cedagent = DD.cedagent.copy().isAPartOf(agcmrec);
	public FixedLengthStringData ovrdcat = DD.ovrdcat.copy().isAPartOf(agcmrec);
	public FixedLengthStringData dormantFlag = DD.dormflag.copy().isAPartOf(agcmrec);
	public PackedDecimalData initCommGst = DD.initCommGst.copy().isAPartOf(agcmrec);
	public PackedDecimalData rnwlCommGst = DD.rnwlCommGst.copy().isAPartOf(agcmrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agcmrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agcmrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agcmrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgcmpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgcmpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgcmpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgcmpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgcmpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgcmpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgcmpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGCMPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"AGNTNUM, " +
							"LIFE, " +
							"JLIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"TRANNO, " +
							"EFDATE, " +
							"ANNPREM, " +
							"BASCMETH, " +
							"INITCOM, " +
							"BASCPY, " +
							"COMPAY, " +
							"COMERN, " +
							"SRVCPY, " +
							"SCMDUE, " +
							"SCMEARN, " +
							"RNWCPY, " +
							"RNLCDUE, " +
							"RNLCEARN, " +
							"AGCLS, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"CRTABLE, " +
							"CURRFROM, " +
							"CURRTO, " +
							"VALIDFLAG, " +
							"SEQNO, " +
							"PTDATE, " +
							"CEDAGENT, " +
							"OVRDCAT, " +
							"DORMFLAG, " +
							"INITCOMMGST" +
							"RNWLCOMMGST" +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     agntnum,
                                     life,
                                     jlife,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     tranno,
                                     efdate,
                                     annprem,
                                     basicCommMeth,
                                     initcom,
                                     bascpy,
                                     compay,
                                     comern,
                                     srvcpy,
                                     scmdue,
                                     scmearn,
                                     rnwcpy,
                                     rnlcdue,
                                     rnlcearn,
                                     agentClass,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     crtable,
                                     currfrom,
                                     currto,
                                     validflag,
                                     seqno,
                                     ptdate,
                                     cedagent,
                                     ovrdcat,
                                     dormantFlag,
                                     initCommGst,
                                     rnwlCommGst,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		agntnum.clear();
  		life.clear();
  		jlife.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		tranno.clear();
  		efdate.clear();
  		annprem.clear();
  		basicCommMeth.clear();
  		initcom.clear();
  		bascpy.clear();
  		compay.clear();
  		comern.clear();
  		srvcpy.clear();
  		scmdue.clear();
  		scmearn.clear();
  		rnwcpy.clear();
  		rnlcdue.clear();
  		rnlcearn.clear();
  		agentClass.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		crtable.clear();
  		currfrom.clear();
  		currto.clear();
  		validflag.clear();
  		seqno.clear();
  		ptdate.clear();
  		cedagent.clear();
  		ovrdcat.clear();
  		dormantFlag.clear();
  		initCommGst.clear();
  		rnwlCommGst.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgcmrec() {
  		return agcmrec;
	}

	public FixedLengthStringData getAgcmpfRecord() {
  		return agcmpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgcmrec(what);
	}

	public void setAgcmrec(Object what) {
  		this.agcmrec.set(what);
	}

	public void setAgcmpfRecord(Object what) {
  		this.agcmpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agcmrec.getLength());
		result.set(agcmrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}