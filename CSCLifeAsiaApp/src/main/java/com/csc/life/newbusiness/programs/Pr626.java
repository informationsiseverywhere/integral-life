/*
 * File: Pr626.java
 * Date: 30 August 2009 1:49:18
 * Author: Quipoz Limited
 *
 * Class transformed from PR626.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.fsu.agents.dataaccess.AgntTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.procedures.Cltreln;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Clntkey;
import com.csc.fsu.clients.recordstructures.Clntrlsrec;
import com.csc.fsu.clients.recordstructures.Cltrelnrec;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.fsu.general.procedures.Bldenrl;
import com.csc.fsu.general.recordstructures.Bldenrlrec;
import com.csc.fsu.general.recordstructures.Chdrkey;
import com.csc.fsu.general.tablestructures.Tr386rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CtrsTableDAM;

import com.csc.life.newbusiness.screens.Sr626ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* Initialise
* ----------
*
*  Retrieve the contract header information from the CHDRLNB I/O
*  module.
*
*  Display Contract Owner, Contract Payor & Agency number and
*  names.
*
*  Display others information for the contract.
*
*  To load the Trustees information
*
*    - Read all the Trustee record/s from CTRSPF
*    - To add all the Trustee record/s to the sub-file area
*    - Multiple subfile lines are used.
*    - Initialise the subfile with blank records after exitsing
*      records loads. For thosed contracts without Trustee,
*      the whole page should be initialized with blank lines.
*    - For each record / Trustee, the name should be display and
*      the information should from CLTS file.
*    - The Type, date from / to should be input
*    - To Add, modify or delete trustee record details
*    - The deletion is done by blanking the details
*
* Validation
* ----------
*
*  If in enquiry  mode  (WSSP-FLAG  =  'I')  protect  the entire
* Screen prior to output.
*
*  If  the  'KILL'  function  key  was  pressed,  skip  all  the
* validation. Otherwise, the following valiadtion should take
* place.
*    - The type is table driven from T5500 for reason code
*    - The Commence from date can Not be after the Commence
*      to date
*    - The Commence from date can Not be before the contract risk
*      commencement date
*    - The duplicated Trustees is Not allowaled
*
* If in enquiry mode, skip all the validation.
*
* Updating
* --------
*
*  If the 'KILL' function  key  was  pressed, or  if in enquiry
* mode, skip the updating only.
*
*  To delete all the existing trustee records from CTRSPF and
* CLRRPF with Truestee roll - 'TR' for the contract.
*
*  To create CTRS records for each trustee from the subfile
*  To create CLRR records for each Trueste with roll - 'TR'
*  for contract.
*
*
***********************************************************************
* </pre>
*/
public class Pr626 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR626");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaPrevClntnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaTotSubfileKeyIn = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaSeqno = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaSflCnt = new ZonedDecimalData(3, 0).setUnsigned();
	private final String wsaaLargeName = "LGNMS";
	private String wsaaCltrelnFunction = "";
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private ZonedDecimalData storedSflRrn = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData currSflRrn = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData nextStrRrn = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData nextSflRrn = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData maxSflCnt = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);

	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4);
	private Validator contractCrt = new Validator(wsaaBatctrcde, "T600","TA7A");
	private Validator contractAlt = new Validator(wsaaBatctrcde, "TR6D");
	private Validator proposalEnq = new Validator(wsaaBatctrcde, "T509");
	private Validator contractEnq = new Validator(wsaaBatctrcde, "T609");
	private Validator inquiry = new Validator(wsaaBatctrcde, "T609","T509");

	private FixedLengthStringData wsaaFoundSubfileError = new FixedLengthStringData(1).init("N");
	private Validator foundSubfileError = new Validator(wsaaFoundSubfileError, "Y");
		/* ERRORS */
	private static final String e048 = "E048";
	private static final String e017 = "E017";
	private static final String rlag = "RLAG";
	private static final String f782 = "F782";
	private static final String f851 = "F851";
	private static final String h359 = "H359";
	private static final String rgig = "RGIG";
	private static final String rgk5 = "RGK5";
		/* TABLES */
	private static final String t3588 = "T3588";
	private static final String t3623 = "T3623";
	private static final String t5688 = "T5688";
	private static final String tr386 = "TR386";
	private static final String clrfrec = "CLRFREC";
	private static final String chdrmnarec = "CHDRMNAREC";
	private static final String itemrec = "ITEMREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String cltsrec = "CLTSREC";
	private static final String ctrsrec = "CTRSREC";
	private AgntTableDAM agntIO = new AgntTableDAM();
	// ILIFE-3396 PERFORMANCE BY OPAL
	private Chdrpf chdrpf = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO" , ChdrpfDAO.class);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	protected ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CtrsTableDAM ctrsIO = new CtrsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private Clntkey wsaaClntkey = new Clntkey();
	private Chdrkey wsaaChdrkey = new Chdrkey();
	private Clntrlsrec clntrlsrec = new Clntrlsrec();
	private Cltrelnrec cltrelnrec = new Cltrelnrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	protected Namadrsrec namadrsrec = new Namadrsrec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Tr386rec tr386rec = new Tr386rec();
	private Bldenrlrec bldenrlrec = new Bldenrlrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Wssplife wssplife = new Wssplife();
	private Sr626ScreenVars sv = getLScreenVars();
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 
	private boolean batchEntryperm = false;
	private static final String NBPRP119 ="NBPRP119";

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1100,
		exit1200,
		exit2090,
		updateErrorIndicators2670,
		readNextModifiedRecord2680,
		exit3100
	}

	public Pr626() {
		super();
		screenVars = sv;
		new ScreenModel("Sr626", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.subfileArea = convertAndSetParam(sv.subfileArea, parmArray, 4);
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End 
		batchEntryperm = FeaConfg.isFeatureExist(wsspcomn.company.toString(), NBPRP119, appVars, "IT"); 
		sv.subfileArea.set(SPACES);
		sv.occdate.set(varcom.vrcmMaxDate);
		wsaaSflCnt.set(ZERO);
		wsaaTotSubfileKeyIn.set(ZERO);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaBatctrcde.set(wsaaBatckey.batcBatctrcde);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		retrvFiles1100();
		getScreenLiterals1110();
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		sv.occdate.set(chdrlnbIO.getOccdate());
		sv.mop.set(chdrlnbIO.getBillchnl());
		sv.billfreq.set(chdrlnbIO.getBillfreq());
		/* Read T5688 for contract header long description.*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrlnbIO.getCnttype());
		a1000ReadDesc();
		sv.ctypedes.set(descIO.getLongdesc());
		/* Retrieve contract status from T3623*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrlnbIO.getStatcode());
		a1000ReadDesc();
		sv.rstate.set(descIO.getShortdesc());
		/* Retrieve premium status from T3588*/
		descIO.setDataKey(SPACES);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrlnbIO.getPstatcode());
		a1000ReadDesc();
		sv.pstate.set(descIO.getShortdesc());
		/* Owner name*/
		sv.cownnum.set(chdrlnbIO.getCownnum());
		wsaaClntkey.clntClntnum.set(chdrlnbIO.getCownnum());
		b1000CallNamadrs();
		sv.clntname01.set(namadrsrec.name);
		/* Payer name*/
		a1200ReadClrf();
		sv.payrnum.set(clrfIO.getClntnum());
		wsaaClntkey.clntClntnum.set(clrfIO.getClntnum());
		b1000CallNamadrs();
		sv.clntname02.set(namadrsrec.name);
		/* Agent name*/
		sv.agntnum.set(chdrlnbIO.getAgntnum());
		a1100ReadAgnt();
		wsaaClntkey.clntClntnum.set(agntIO.getClntnum());
		b1000CallNamadrs();
		sv.clntname03.set(namadrsrec.name);
		/* Subfile PROCESSING*/
		scrnparams.function.set(varcom.sclr);
		screenIo9000();
		ctrsIO.setDataArea(SPACES);
		ctrsIO.setStatuz(varcom.oK);
		ctrsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ctrsIO.setChdrnum(chdrlnbIO.getChdrnum());
		ctrsIO.setSeqno(ZERO);
		ctrsIO.setFormat(SPACES);
		ctrsIO.setFunction(varcom.begn);
//Start Life Performance atiwari23
		ctrsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ctrsIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
		//end;

		while ( !(isEQ(ctrsIO.getStatuz(), varcom.endp))) {
			loadSubfile1200();
		}

		while ( !(isEQ(wsaaSflCnt, sv.subfilePage))) {
			loadBlankSubfile1210();
		}

		if (!inquiry.isTrue()) {
			scrnparams.subfileMore.set("Y");
		}
		scrnparams.subfileRrn.set(1);
		//MIBT-71
		sv.scflag.set(wsspcomn.flag);
	}

protected void retrvFiles1100()
	{
		try {
			start1100();
			readChdrlnb1100();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start1100()
	{
		/* The calling program using different CHDR logical files:*/
		/* Contract Create and Proposal Enquiry use CHDRLNB*/
		/* Contract Enquiry uses CHDRENQ*/
		/* Minor Alteration uses CHDRMNA*/
		/* So, this program needs to RETRV diffiernt CHDR logical files.*/
		if (contractCrt.isTrue()
		|| proposalEnq.isTrue()) {
			chdrlnbIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(chdrlnbIO.getParams());
				syserrrec.statuz.set(chdrlnbIO.getStatuz());
				fatalError600();
			}
			goTo(GotoLabel.exit1100);
		}
		if (contractEnq.isTrue()) {
			// ILIFE-3396 PERFORMANCE BY OPAL
			chdrpf = chdrpfDAO.getCacheObject(chdrpf);
			if(chdrpf!=null)
				{
				chdrlnbIO.setChdrcoy(chdrpf.getChdrcoy());
				chdrlnbIO.setChdrnum(chdrpf.getChdrnum());
			}
		}
		if (contractAlt.isTrue()) {
			chdrmnaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmnaIO);
			if (isEQ(chdrmnaIO.getStatuz(), varcom.oK)) {
				chdrlnbIO.setChdrcoy(chdrmnaIO.getChdrcoy());
				chdrlnbIO.setChdrnum(chdrmnaIO.getChdrnum());
			}
			else {
				syserrrec.params.set(chdrmnaIO.getParams());
				syserrrec.statuz.set(chdrmnaIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void readChdrlnb1100()
	{
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
	}

protected void getScreenLiterals1110()
	{
		start1110();
	}

protected void start1110()
	{
		/* Read TR386 to get screen header description.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr386);
		itemIO.setItemitem(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(wsspcomn.language);
		stringVariable1.addExpression(wsaaProg);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr386rec.tr386Rec.set(itemIO.getGenarea());
		if (contractCrt.isTrue()) {
			sv.descrip.set(tr386rec.progdesc01);
		}
		if (contractAlt.isTrue()) {
			sv.descrip.set(tr386rec.progdesc02);
		}
		if (contractEnq.isTrue()
		|| proposalEnq.isTrue()) {
			sv.descrip.set(tr386rec.progdesc03);
		}
	}

protected void loadSubfile1200()
	{
		try {
			start1200();
			nextr1200();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start1200()
	{
		SmartFileCode.execute(appVars, ctrsIO);
		if (isNE(ctrsIO.getStatuz(), varcom.oK)
		&& isNE(ctrsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ctrsIO.getParams());
			syserrrec.statuz.set(ctrsIO.getStatuz());
			fatalError600();
		}
		if (isEQ(ctrsIO.getStatuz(), varcom.endp)
		|| isNE(ctrsIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(ctrsIO.getChdrnum(), chdrlnbIO.getChdrnum())) {
			ctrsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1200);
		}
		sv.hseqno.set(ctrsIO.getSeqno());
		sv.clntnum.set(ctrsIO.getClntnum());
		if (isNE(ctrsIO.getClntnum(), SPACES)) {
			wsaaClntkey.clntClntnum.set(ctrsIO.getClntnum());
			b1000CallNamadrs();
			sv.textone.set(namadrsrec.name);
		}
		sv.reasoncd.set(ctrsIO.getReasoncd());
		sv.datefrm.set(ctrsIO.getDatefrm());
		sv.dateto.set(ctrsIO.getDateto());
		if (isEQ(ctrsIO.getDateto(), varcom.vrcmMaxDate)) {
			sv.reasoncdOut[varcom.pr.toInt()].set("N");
			sv.clntnumOut[varcom.pr.toInt()].set("N");
			sv.datefrmOut[varcom.pr.toInt()].set("N");
		}
		else {
			sv.reasoncdOut[varcom.pr.toInt()].set("Y");
			sv.clntnumOut[varcom.pr.toInt()].set("Y");
			sv.datefrmOut[varcom.pr.toInt()].set("Y");
		}
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		if (isEQ(wsaaSflCnt, sv.subfilePage)) {
			wsaaSflCnt.set(1);
		}
		else {
			wsaaSflCnt.add(1);
		}
	}

protected void nextr1200()
	{
		ctrsIO.setFunction(varcom.nextr);
	}

protected void loadBlankSubfile1210()
	{
		/*START*/
		sv.initialiseSubfileArea();
		sv.hseqno.set(ZERO);
		sv.datefrm.set(varcom.vrcmMaxDate);
		sv.dateto.set(varcom.vrcmMaxDate);
		scrnparams.function.set(varcom.sadd);
		screenIo9000();
		wsaaSflCnt.add(1);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    This section will handle any action required on the screen **/
		/*    before the screen is painted.                              **/
		if (isEQ(wsspcomn.flag, "I")
		|| inquiry.isTrue()) {
			scrnparams.function.set(varcom.prot);
		}
		return ;
		/*PRE-EXIT*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void screenEdit2000()
	{
		try {
			screenIo2010();
			validateSubfile2060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void screenIo2010()
	{
		/* Skip validation if Enquiry or Function key F12 is pressed.*/
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| inquiry.isTrue()) {
			goTo(GotoLabel.exit2090);
		}
		/* F5 = Refresh screen*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		if (isEQ(scrnparams.statuz, varcom.rolu)) {
			loadSubfile2100();
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*VALIDATE-SCREEN*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void validateSubfile2060()
	{
		wsaaTotSubfileKeyIn.set(ZERO);
		wsaaFoundSubfileError.set("N");
		scrnparams.function.set(varcom.sstrt);
		screenIo9000();
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			validateSubfile2600();
		}

		if (isGT(wsaaTotSubfileKeyIn, 1)
		&& !foundSubfileError.isTrue()) {
			checkDuplicateTrustee2700();
		}
		scrnparams.subfileRrn.set(1);
	}

protected void loadSubfile2100()
	{
		/*BEGIN*/
		wsaaSflCnt.set(ZERO);
		while ( !(isEQ(wsaaSflCnt, sv.subfilePage))) {
			loadBlankSubfile1210();
		}

		scrnparams.subfileMore.set("Y");
		/*EXIT*/
	}

protected void validateSubfile2600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					validation2610();
				case updateErrorIndicators2670:
					updateErrorIndicators2670();
				case readNextModifiedRecord2680:
					readNextModifiedRecord2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation2610()
	{
		sv.textone.set(SPACES);
		/* Check if client is valided.*/
		if (isEQ(sv.clntnum, SPACES)
		&& isEQ(sv.reasoncd, SPACES)
		&& isEQ(sv.datefrm, varcom.vrcmMaxDate)
		&& isEQ(sv.dateto, varcom.vrcmMaxDate)) {
			goTo(GotoLabel.updateErrorIndicators2670);
		}
		if (isEQ(sv.clntnum, SPACES)
		&& (isNE(sv.reasoncd, SPACES)
		|| isNE(sv.datefrm, varcom.vrcmMaxDate)
		|| isNE(sv.dateto, varcom.vrcmMaxDate))) {
			sv.clntnumErr.set(rgk5);
		}
		if (isNE(sv.clntnum, SPACES)) {
			if (isEQ(sv.reasoncd, SPACES)) {
				sv.reasoncdErr.set(rlag);
			}
			if(batchEntryperm){
				sv.datefrm.set(chdrlnbIO.getOccdate());
			}
			
			if (isEQ(sv.datefrm, varcom.vrcmMaxDate)) {
				sv.datefrmErr.set(f851);
			}
			wsaaClntkey.clntClntnum.set(sv.clntnum);
			b1000CallNamadrs();
			if (isEQ(namadrsrec.statuz, varcom.mrnf)) {
				sv.clntnumErr.set(rgig);
			}
			else {
				sv.textone.set(namadrsrec.name);
			}
			wsaaTotSubfileKeyIn.add(1);
		}
		if (isGT(sv.datefrm, sv.dateto)) {
			sv.datetoErr.set(e017);
		}
		if (isLT(sv.datefrm, chdrlnbIO.getOccdate())) {
			sv.datefrmErr.set(h359);
		}
		/*    Check Death Date < Orig Comm Date                            */
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntnum(sv.clntnum);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isEQ(cltsIO.getStatuz(), varcom.oK)) {
			if (isNE(cltsIO.getCltdod(), varcom.vrcmMaxDate)
			&& isGT(chdrlnbIO.getOccdate(), cltsIO.getCltdod()) || isLTE(cltsIO.getCltdod(), wsaaToday)) {
				sv.clntnumErr.set(f782);
			}
		}
	}

protected void updateErrorIndicators2670()
	{
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
			wsaaFoundSubfileError.set("Y");
		}
		scrnparams.function.set(varcom.supd);
		screenIo9000();
		if (isNE(sv.errorSubfile, SPACES)) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.readNextModifiedRecord2680);
		}
	}

protected void readNextModifiedRecord2680()
	{
		scrnparams.function.set(varcom.srdn);
		screenIo9000();
		/*EXIT*/
	}

protected void checkDuplicateTrustee2700()
	{
		start2700();
	}

protected void start2700()
	{
		compute(maxSflCnt, 0).set(sub(scrnparams.subfileEnd, 1));
		for (currSflRrn.set(1); !(isGT(currSflRrn, maxSflCnt)); currSflRrn.add(1)){
			scrnparams.subfileRrn.set(currSflRrn);
			scrnparams.function.set(varcom.sread);
			screenIo9000();
			if (isNE(sv.clntnum, SPACES)) {
				wsaaPrevClntnum.set(sv.clntnum);
				storedSflRrn.set(currSflRrn);
			}
			compute(nextStrRrn, 0).set(add(currSflRrn, 1));
			for (nextSflRrn.set(nextStrRrn); !(isGT(nextSflRrn, scrnparams.subfileEnd)); nextSflRrn.add(1)){
				checkNextSubfile2710();
			}
		}
	}

protected void checkNextSubfile2710()
	{
		start2710();
	}

protected void start2710()
	{
		scrnparams.subfileRrn.set(nextSflRrn);
		scrnparams.function.set(varcom.sread);
		screenIo9000();
		if (isEQ(sv.clntnum, SPACES)) {
			return ;
		}
		if (isEQ(sv.clntnum, wsaaPrevClntnum)) {
			sv.clntnumErr.set(e048);
			scrnparams.function.set(varcom.supd);
			screenIo9000();
			scrnparams.subfileRrn.set(storedSflRrn);
			scrnparams.function.set(varcom.sread);
			screenIo9000();
			sv.clntnumErr.set(e048);
			scrnparams.function.set(varcom.supd);
			screenIo9000();
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		if (isEQ(scrnparams.statuz, varcom.kill)
		|| isEQ(wsspcomn.flag, "I")
		|| inquiry.isTrue()) {
			if (contractAlt.isTrue()) {
				callSftlock3600();
			}
			return ;
		}
		/* Remove trustees and client role*/
		ctrsIO.setDataArea(SPACES);
		ctrsIO.setStatuz(varcom.oK);
		ctrsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ctrsIO.setChdrnum(chdrlnbIO.getChdrnum());
		ctrsIO.setSeqno(ZERO);
		ctrsIO.setFormat(ctrsrec);
		ctrsIO.setFunction(varcom.begn);
		while ( !(isEQ(ctrsIO.getStatuz(), varcom.endp))) {
			removeTrustees3100();
		}

		/* Update trustees and client role*/
		wsaaSeqno.set(ZERO);
		scrnparams.function.set(varcom.sstrt);
		screenIo9000();
		while ( !(isEQ(scrnparams.statuz, varcom.endp))) {
			updateTrustees3200();
		}

		/* For Minor Alteration, release CHDRMNA,then update it with TRANNO*/
		/* increased by 1; write a new record to PTRNPF.*/
		if (contractAlt.isTrue()) {
			updateChdrmna3300();
			updatePtrn3400();
			callBldenrl3500();
			callSftlock3600();
		}
	}

protected void removeTrustees3100()
	{
		try {
			start3100();
			nextr3100();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start3100()
	{
		SmartFileCode.execute(appVars, ctrsIO);
		if (isNE(ctrsIO.getStatuz(), varcom.oK)
		&& isNE(ctrsIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(ctrsIO.getParams());
			syserrrec.statuz.set(ctrsIO.getStatuz());
			fatalError600();
		}
		if (isEQ(ctrsIO.getStatuz(), varcom.endp)
		|| isNE(ctrsIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(ctrsIO.getChdrnum(), chdrlnbIO.getChdrnum())) {
			ctrsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3100);
		}
		/* Remove trustee client role*/
		wsaaClntkey.clntClntpfx.set(fsupfxcpy.clnt);
		wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
		wsaaClntkey.clntClntnum.set(ctrsIO.getClntnum());
		wsaaChdrkey.chdrChdrpfx.set(fsupfxcpy.chdr);
		wsaaChdrkey.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		wsaaChdrkey.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		wsaaCltrelnFunction = "DEL";
		b2000CallCltreln();
		/* Delete trustee records*/
		ctrsIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ctrsIO);
		if (isNE(ctrsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ctrsIO.getParams());
			syserrrec.statuz.set(ctrsIO.getStatuz());
			fatalError600();
		}
		ctrsIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, ctrsIO);
		if (isNE(ctrsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ctrsIO.getParams());
			syserrrec.statuz.set(ctrsIO.getStatuz());
			fatalError600();
		}
	}

protected void nextr3100()
	{
		ctrsIO.setFunction(varcom.nextr);
	}

protected void updateTrustees3200()
	{
		start3200();
		nextSubfile3200();
	}

protected void start3200()
	{
		if (isEQ(sv.clntnum, SPACES)) {
			return ;
		}
		writeCtrs3210();
		/* Add trustee client role*/
		wsaaClntkey.clntClntpfx.set(fsupfxcpy.clnt);
		wsaaClntkey.clntClntcoy.set(wsspcomn.fsuco);
		wsaaClntkey.clntClntnum.set(sv.clntnum);
		wsaaChdrkey.chdrChdrpfx.set(fsupfxcpy.chdr);
		wsaaChdrkey.chdrChdrcoy.set(chdrlnbIO.getChdrcoy());
		wsaaChdrkey.chdrChdrnum.set(chdrlnbIO.getChdrnum());
		wsaaCltrelnFunction = "ADD";
		b2000CallCltreln();
	}

protected void nextSubfile3200()
	{
		scrnparams.function.set(varcom.srdn);
		screenIo9000();
		/*EXIT*/
	}

protected void writeCtrs3210()
	{
		start3210();
	}

protected void start3210()
	{
		wsaaSeqno.add(1);
		ctrsIO.setParams(SPACES);
		ctrsIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ctrsIO.setChdrnum(chdrlnbIO.getChdrnum());
		ctrsIO.setSeqno(wsaaSeqno);
		ctrsIO.setClntnum(sv.clntnum);
		ctrsIO.setReasoncd(sv.reasoncd);
		ctrsIO.setDatefrm(sv.datefrm);
		ctrsIO.setDateto(sv.dateto);
		ctrsIO.setFormat(ctrsrec);
		ctrsIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ctrsIO);
		if (isNE(ctrsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ctrsIO.getParams());
			syserrrec.statuz.set(ctrsIO.getStatuz());
			fatalError600();
		}
	}

protected void updateChdrmna3300()
	{
		start3300();
	}

protected void start3300()
	{
		chdrmnaIO.setFunction(varcom.rlse);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmnaIO.getStatuz());
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		chdrmnaIO.setChdrnum(chdrlnbIO.getChdrnum());
		chdrmnaIO.setFunction(varcom.readr);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmnaIO.getStatuz());
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		setPrecision(chdrmnaIO.getTranno(), 0);
		chdrmnaIO.setTranno(add(chdrmnaIO.getTranno(), 1));
		chdrmnaIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			syserrrec.statuz.set(chdrmnaIO.getStatuz());
			fatalError600();
		}
	}

protected void updatePtrn3400()
	{
		start3400();
	}

protected void start3400()
	{
		ptrnIO.setParams(SPACES);
		ptrnIO.setDataKey(wsspcomn.batchkey);
		ptrnIO.setChdrpfx(chdrlnbIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlnbIO.getChdrnum());
		ptrnIO.setRecode(SPACES);
		ptrnIO.setTranno(chdrmnaIO.getTranno());
		ptrnIO.setPtrneff(wsaaToday);
		ptrnIO.setTransactionDate(varcom.vrcmDate);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setUser(varcom.vrcmUser);
		ptrnIO.setDatesub(wsaaToday);
		ptrnIO.setCrtuser(wsspcomn.userid);
		ptrnIO.setPrtflg(SPACES);
		ptrnIO.setValidflag("1");
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(ptrnIO.getParams());
			syserrrec.statuz.set(ptrnIO.getStatuz());
			fatalError600();
		}
	}

protected void callBldenrl3500()
	{
		/*START*/
		initialize(bldenrlrec.bldenrlrec);
		bldenrlrec.prefix.set(chdrmnaIO.getChdrpfx());
		bldenrlrec.company.set(chdrmnaIO.getChdrcoy());
		bldenrlrec.uentity.set(chdrmnaIO.getChdrnum());
		callProgram(Bldenrl.class, bldenrlrec.bldenrlrec);
		if (isNE(bldenrlrec.statuz, varcom.oK)) {
			syserrrec.params.set(bldenrlrec.bldenrlrec);
			syserrrec.statuz.set(bldenrlrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void callSftlock3600()
	{
		start3600();
	}

protected void start3600()
	{
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrlnbIO.getChdrnum());
		sftlockrec.enttyp.set(fsupfxcpy.chdr);
		sftlockrec.transaction.set(wsaaBatckey.batcBatctrcde);
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void screenIo9000()
	{
		/*SCREEN*/
		processScreen("SR626", sv);
		if (isNE(scrnparams.statuz, varcom.oK)
		&& isNE(scrnparams.statuz, varcom.endp)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void a1000ReadDesc()
	{
		/*A1000-START*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.company);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			descIO.setShortdesc("??????????");
			descIO.setLongdesc("??????????????????????????????");
		}
		/*A1000-EXIT*/
	}

protected void a1100ReadAgnt()
	{
		/*A1100-START*/
		agntIO.setParams(SPACES);
		agntIO.setAgntpfx(chdrlnbIO.getAgntpfx());
		agntIO.setAgntcoy(chdrlnbIO.getAgntcoy());
		agntIO.setAgntnum(chdrlnbIO.getAgntnum());
		agntIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agntIO);
		if (isNE(agntIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agntIO.getStatuz());
			syserrrec.params.set(agntIO.getParams());
			fatalError600();
		}
		/*A1100-EXIT*/
	}

protected void a1200ReadClrf()
	{
		a1200Start();
	}

protected void a1200Start()
	{
		clrfIO.setDataArea(SPACES);
		clrfIO.setForepfx(chdrlnbIO.getChdrpfx());
		clrfIO.setForecoy(chdrlnbIO.getChdrcoy());
		wsaaChdrnum.set(chdrlnbIO.getChdrnum());
		wsaaPayrseqno.set("1");
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole(clntrlsrec.payr);
		clrfIO.setFormat(clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)
		&& isNE(clrfIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
	}

protected void b1000CallNamadrs()
	{
		b1000Namadrs();
	}

protected void b1000Namadrs()
	{
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(wsaaClntkey.clntClntnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)
		&& isNE(namadrsrec.statuz, varcom.mrnf)) {
			syserrrec.params.set(namadrsrec.namadrsRec);
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
	}

protected void b2000CallCltreln()
	{
		b2000Start();
	}

protected void b2000Start()
	{
		cltrelnrec.clntpfx.set(wsaaClntkey.clntClntpfx);
		cltrelnrec.clntcoy.set(wsaaClntkey.clntClntcoy);
		cltrelnrec.clntnum.set(wsaaClntkey.clntClntnum);
		cltrelnrec.clrrrole.set(clntrlsrec.trustee);
		cltrelnrec.forepfx.set(wsaaChdrkey.chdrChdrpfx);
		cltrelnrec.forecoy.set(wsaaChdrkey.chdrChdrcoy);
		cltrelnrec.forenum.set(wsaaChdrkey.chdrChdrnum);
		cltrelnrec.function.set(wsaaCltrelnFunction);
		callProgram(Cltreln.class, cltrelnrec.cltrelnRec);
		if (isNE(cltrelnrec.statuz, varcom.oK)) {
			syserrrec.params.set(cltrelnrec.cltrelnRec);
			syserrrec.statuz.set(cltrelnrec.statuz);
			fatalError600();
		}
	}

	protected Sr626ScreenVars getLScreenVars() {
		return ScreenProgram.getScreenVars(Sr626ScreenVars.class);
	}

}
