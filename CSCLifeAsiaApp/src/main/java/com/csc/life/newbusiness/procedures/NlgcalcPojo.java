package com.csc.life.newbusiness.procedures;

import java.math.BigDecimal;

public class NlgcalcPojo {

	private String function;
	private String status;
	private String fsuco;
	private String chdrcoy;
	private String chdrnum;
	private int tranno;
	private int effdate;
	private int occdate;
	private int ptdate;
	private int btdate;
	private String billfreq;
	private String cnttype;
	private int cltdob;
	private String language;
	private int batcactyr;
	private int batcactmn;
	private String batctrcde;
	private String billchnl;
	private String Cntcurr;
	private BigDecimal inputAmt;
	private int frmdate;
	private int todate;
	private BigDecimal nlgBalance;
	private BigDecimal totTopup;
	private BigDecimal totWdrAmt;
	private BigDecimal ovduePrem;
	private BigDecimal unpaidPrem;
	private int currAge;
	private int yrsInf;
	private String nlgFlag;
	private String transMode;

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFsuco() {
		return fsuco;
	}

	public void setFsuco(String fsuco) {
		this.fsuco = fsuco;
	}

	public String getChdrcoy() {
		return chdrcoy;
	}

	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	public String getChdrnum() {
		return chdrnum;
	}

	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	public int getTranno() {
		return tranno;
	}

	public void setTranno(int tranno) {
		this.tranno = tranno;
	}

	public int getEffdate() {
		return effdate;
	}

	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}

	public int getOccdate() {
		return occdate;
	}

	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}

	public int getPtdate() {
		return ptdate;
	}

	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}

	public int getBtdate() {
		return btdate;
	}

	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}

	public String getBillfreq() {
		return billfreq;
	}

	public void setBillfreq(String billfreq) {
		this.billfreq = billfreq;
	}

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public int getCltdob() {
		return cltdob;
	}

	public void setCltdob(int cltdob) {
		this.cltdob = cltdob;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public int getBatcactyr() {
		return batcactyr;
	}

	public void setBatcactyr(int batcactyr) {
		this.batcactyr = batcactyr;
	}

	public int getBatcactmn() {
		return batcactmn;
	}

	public void setBatcactmn(int batcactmn) {
		this.batcactmn = batcactmn;
	}

	public String getBatctrcde() {
		return batctrcde;
	}

	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}

	public String getBillchnl() {
		return billchnl;
	}

	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}

	public String getCntcurr() {
		return Cntcurr;
	}

	public void setCntcurr(String cntcurr) {
		Cntcurr = cntcurr;
	}

	public BigDecimal getInputAmt() {
		return inputAmt;
	}

	public void setInputAmt(BigDecimal inputAmt) {
		this.inputAmt = inputAmt;
	}

	public int getFrmdate() {
		return frmdate;
	}

	public void setFrmdate(int frmdate) {
		this.frmdate = frmdate;
	}

	public int getTodate() {
		return todate;
	}

	public void setTodate(int todate) {
		this.todate = todate;
	}

	public BigDecimal getNlgBalance() {
		return nlgBalance;
	}

	public void setNlgBalance(BigDecimal nlgBalance) {
		this.nlgBalance = nlgBalance;
	}

	public BigDecimal getTotTopup() {
		return totTopup;
	}

	public void setTotTopup(BigDecimal totTopup) {
		this.totTopup = totTopup;
	}

	public BigDecimal getTotWdrAmt() {
		return totWdrAmt;
	}

	public void setTotWdrAmt(BigDecimal totWdrAmt) {
		this.totWdrAmt = totWdrAmt;
	}

	public BigDecimal getOvduePrem() {
		return ovduePrem;
	}

	public void setOvduePrem(BigDecimal ovduePrem) {
		this.ovduePrem = ovduePrem;
	}

	public BigDecimal getUnpaidPrem() {
		return unpaidPrem;
	}

	public void setUnpaidPrem(BigDecimal unpaidPrem) {
		this.unpaidPrem = unpaidPrem;
	}

	public int getCurrAge() {
		return currAge;
	}

	public void setCurrAge(int currAge) {
		this.currAge = currAge;
	}

	public int getYrsInf() {
		return yrsInf;
	}

	public void setYrsInf(int yrsInf) {
		this.yrsInf = yrsInf;
	}

	public String getNlgFlag() {
		return nlgFlag;
	}

	public void setNlgFlag(String nlgFlag) {
		this.nlgFlag = nlgFlag;
	}

	public String getTransMode() {
		return transMode;
	}

	public void setTransMode(String transMode) {
		this.transMode = transMode;
	}

	@Override
	public String toString() {
		return "NlgcalcPojo [function=" + function + ", status=" + status + ", fsuco=" + fsuco + ", chdrcoy=" + chdrcoy
				+ ", chdrnum=" + chdrnum + ", tranno=" + tranno + ", effdate=" + effdate + ", occdate=" + occdate
				+ ", ptdate=" + ptdate + ", btdate=" + btdate + ", billfreq=" + billfreq + ", cnttype=" + cnttype
				+ ", cltdob=" + cltdob + ", language=" + language + ", batcactyr=" + batcactyr + ", batcactmn="
				+ batcactmn + ", batctrcde=" + batctrcde + ", billchnl=" + billchnl + ", Cntcurr=" + Cntcurr
				+ ", inputAmt=" + inputAmt + ", frmdate=" + frmdate + ", todate=" + todate + ", nlgBalance="
				+ nlgBalance + ", totTopup=" + totTopup + ", totWdrAmt=" + totWdrAmt + ", ovduePrem=" + ovduePrem
				+ ", unpaidPrem=" + unpaidPrem + ", currAge=" + currAge + ", yrsInf=" + yrsInf + ", nlgFlag=" + nlgFlag
				+ ", transMode=" + transMode + "]";
	}
}