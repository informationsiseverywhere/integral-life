package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:08:47
 * Description:
 * Copybook name: PH534PAR
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ph534par extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData parmRecord = new FixedLengthStringData(11);
  	public FixedLengthStringData aracde = new FixedLengthStringData(3).isAPartOf(parmRecord, 0);
  	public FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(parmRecord, 3);
  	public FixedLengthStringData chdrtype = new FixedLengthStringData(3).isAPartOf(parmRecord, 5);
  	public FixedLengthStringData ctrlbrks = new FixedLengthStringData(3).isAPartOf(parmRecord, 8);
  	public FixedLengthStringData[] ctrlbrk = FLSArrayPartOfStructure(3, 1, ctrlbrks, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(3).isAPartOf(ctrlbrks, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ctrlbrk01 = new FixedLengthStringData(1).isAPartOf(filler, 0);
  	public FixedLengthStringData ctrlbrk02 = new FixedLengthStringData(1).isAPartOf(filler, 1);
  	public FixedLengthStringData ctrlbrk03 = new FixedLengthStringData(1).isAPartOf(filler, 2);


	public void initialize() {
		COBOLFunctions.initialize(parmRecord);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		parmRecord.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}