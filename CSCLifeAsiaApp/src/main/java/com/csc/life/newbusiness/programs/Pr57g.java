/*
 * File: PT007.java
*/
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.fsu.agents.dataaccess.dao.ClbapfDAO;
import com.csc.fsu.agents.dataaccess.model.Clbapf;
import com.csc.fsu.clients.dataaccess.dao.BabrpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClrfpfDAO;
import com.csc.fsu.clients.dataaccess.model.Babrpf;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.clients.dataaccess.model.Clrfpf;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.MandTableDAM;
import com.csc.fsu.general.dataaccess.dao.MandpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.model.Mandpf;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;//ILIFE-8363
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.screens.Sr57gScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.DescDAO;
import com.csc.smart400framework.dataaccess.model.Descpf;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

public class Pr57g extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR57G");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);
	private final String wsaaLargeName = "LGNMS";

	private FixedLengthStringData wsbbBankdesc = new FixedLengthStringData(60);
	private FixedLengthStringData wsbbBankdescLine1 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 0);
	private FixedLengthStringData wsbbBankdescLine2 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 30);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(9);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
		/* ERRORS */
		/* TABLES */
	private static final String mandrec = "MANDREC"; //ILIFE-2472

	private MandTableDAM mandIO = new MandTableDAM(); //ILIFE-2472
	private Namadrsrec namadrsrec = new Namadrsrec();
		/*Payor Details Logical File*/
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wssplife wssplife = new Wssplife();
	private Sr57gScreenVars sv = ScreenProgram.getScreenVars( Sr57gScreenVars.class);
	
	/* IFSU-852 Changes start */
	boolean ispermission = false;
	/* IFSU-852 Changes end */

	//ILB-489 by wli31
	private BabrpfDAO babrpfDao = getApplicationContext().getBean("babrpfDAO", BabrpfDAO.class);
	private DescDAO descDAO =  getApplicationContext().getBean("descDAO", DescDAO.class);
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	private ClbapfDAO clbapfDAO = getApplicationContext().getBean("clbapfDAO", ClbapfDAO.class);
	private ClrfpfDAO clrfpfDAO = getApplicationContext().getBean("clrfpfDAO", ClrfpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private MandpfDAO mandpfDAO = getApplicationContext().getBean("mandpfDAO", MandpfDAO.class);
	private Mandpf mandlnbIO = null;
	private Babrpf babrIO = null;
	private Clbapf clblIO = null;
	private Payrpf payrpf = new Payrpf();
	/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();//ILIFE-8393
	private PayrTableDAM payrIO = new PayrTableDAM();//ILIFE-8393
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090
	}

	public Pr57g() {
		super();
		screenVars = sv;
		new ScreenModel("Sr57g", AppVars.getInstance(), sv);
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}
	
	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
	@Override
	protected void initialise1000() {
		initialise1010();
	}

	protected void initialise1010()	{
		wsspcomn.clonekey.set("V");
		wsspcomn.bchaction.set(SPACES);
		sv.dataArea.set(SPACES);
		
		/*IFSU-852 changes starts*/			
		ispermission = false;
		ispermission = FeaConfg.isFeatureExist(wsspcomn.fsuco.toString(), "MTL29603", appVars,"IT");
		if(!ispermission){
			sv.mrbnkOut[varcom.nd.toInt()].set("Y");
		}
		/*IFSU-852 changes end*/	
		/*chdrpf = chdrpfDAO.getCacheObject(chdrpf);
		if (chdrpf == null) {
			fatalError600();
		} commented for ILIFE-8393*/
		//added for ILIFE-8393

		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		payrpf = payrpfDAO.getCacheObject(payrpf);
		if(null==payrpf) {
			payrIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(),varcom.oK)&& isNE(payrIO.getStatuz(),varcom.mrnf)) {
				syserrrec.params.set(payrIO.getParams());
				fatalError600();
			}else {
				payrpf = payrpfDAO.getpayrRecord(payrIO.getChdrcoy().toString(), payrIO.getChdrnum().toString());
				if(null==payrpf) {
					fatalError600();
				}else {
					payrpfDAO.setCacheObject(payrpf);
				}
			}
		}
		//end
		sv.payrnum.set(chdrlnbIO.getCownnum());
		sv.numsel.set(chdrlnbIO.getCownnum());
		//ILIFE-2472-START
		if (isNE(chdrlnbIO.getPayclt(), SPACES)) {
			sv.payrnum.set(chdrlnbIO.getPayclt());
			sv.numsel.set(chdrlnbIO.getCownnum());
		}
		//ILIFE-2472-END
		a1000GetPayorname();
		sv.payorname.set(namadrsrec.name);
		wsaaClntkey.set(SPACES);
		//ILIFE-2472-START
		if (isNE(chdrlnbIO.getCownnum(),SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrlnbIO.getCownpfx());
			stringVariable1.addExpression(chdrlnbIO.getCowncoy());
			stringVariable1.addExpression(chdrlnbIO.getCownnum());
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		//ILIFE-2472-END
		sv.currcode.set(SPACES);
		sv.facthous.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.billcd.set(payrpf.getBillcd());
		keepMand();
		//ILIFE-2472-START
		if (isNE(chdrlnbIO.getZmandref(), SPACES)) {
			sv.mandref.set(chdrlnbIO.getZmandref());
		}
		else {
			sv.mandref.set(SPACES);
			return;
		}
		if (isEQ(chdrlnbIO.getZmandref(),SPACES)) {
			return ;
		}
		//ILIFE-2472-END
		mandlnbIO = mandpfDAO.searchMandpfRecordData(wsspcomn.fsuco.toString(),sv.payrnum.toString(),chdrlnbIO.getZmandref().toString(),"MANDLNB");
		
		if (mandlnbIO == null) {
			sv.mandrefErr.set("H929");
			return ;
		}
		sv.bankkey.set(mandlnbIO.getBankkey());

		clblIO = clbapfDAO.searchClblData(mandlnbIO.getBankkey(), mandlnbIO.getBankacckey(), wsspcomn.fsuco.toString(),sv.payrnum.toString());
		if (clblIO == null) {
			fatalError600();
		}
		//ILIFE-2472-START
		/*if (isNE(chdrlnbIO.getCownnum(),clblIO.getClntnum())) {
			sv.payrnumErr.set("f373);
			return ;
		}*/
		//ILIFE-2472-END
		if (!clblIO.getClntnum().equals(sv.payrnum.toString())) {
			sv.facthous.set(SPACES);
			sv.longdesc.set(SPACES);
			sv.bankkey.set(SPACES);
			sv.bankacckey.set(SPACES);
			return ;
		}

		babrIO = babrpfDao.searchBankkey(sv.bankkey.toString());
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		if(ispermission) { // IFSU-852
			sv.mrbnk.set(clblIO.getMrbnk()); 			// ILIFE-2476
			sv.bnkbrn.set(clblIO.getBnkbrn()); 			// ILIFE-2476
		}
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		sv.facthous.set(clblIO.getFacthous());
		if (isNE(sv.facthous,SPACES)) {
			a1200GetFacthousDesc();
		}
	}

protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.clntkey.set(SPACES);
		/* If in enquiry mode, protect field                               */
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot); //ILIFE-2472
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void screenIo2010() {
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag,"I")) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		//ILIFE-2472-START
		if(isEQ(sv.payrnum, SPACES)){
			sv.payrnum.set(chdrlnbIO.getCownnum());
		}
		wsspcomn.chdrCownnum.set(sv.payrnum);
		Clntpf cltsIO = clntpfDAO.searchClntRecord("CN",wsspcomn.fsuco.toString(), sv.payrnum.toString());
		
		keepMand(); //ILIFE-2472
		if (cltsIO == null) {
			sv.payrnumErr.set("E335");
			wsspcomn.edterror.set("Y");
			sv.payorname.set(SPACES);
			goTo(GotoLabel.exit2090);
		}
		a1000GetPayorname();
		sv.payorname.set(namadrsrec.name);
		//ILIFE-2472-END
		mandIO.setPayrcoy(wsspcomn.fsuco);
		mandIO.setPayrnum(sv.payrnum);
		mandIO.setMandref("99999");
		mandIO.setFunction(varcom.begn);
		mandIO.setFormat(mandrec);
		SmartFileCode.execute(appVars, mandIO);
		if ((isNE(mandIO.getStatuz(), varcom.endp))
		&& (isNE(mandIO.getStatuz(), varcom.oK))) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
		if(isEQ(mandIO.getStatuz(), varcom.endp) 
		|| isNE(mandIO.getPayrcoy(), wsspcomn.fsuco)
		|| isNE(mandIO.getPayrnum(), sv.payrnum)) {
			sv.mandrefErr.set("RFSM");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.mandref,SPACES)) {
			sv.mandrefErr.set("H926");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isNE(sv.mandref, SPACES)) {
			if (isNE(sv.mandref,NUMERIC)) {
				sv.mandrefErr.set("H421");
				wsspcomn.edterror.set("Y");
				goTo(GotoLabel.exit2090);
			}
		}

		mandlnbIO = mandpfDAO.searchMandpfRecordData(wsspcomn.fsuco.toString(),sv.payrnum.toString(),sv.mandref.toString(),"MANDLNB");
		//ILIFE-2472-START
		if (mandlnbIO == null
		|| "I".equals(mandlnbIO.getPayind())) {
			//Change mandate not found (h928) to new error message
			//sv.mandrefErr.set("h928);
			sv.mandrefErr.set("RFSM");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		//ILIFE-2472-END
		if (isEQ(mandlnbIO.getCrcind(), "C")) {
			sv.mandrefErr.set("H928");
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Move bank account details to corresponding fields*/
		sv.bankkey.set(mandlnbIO.getBankkey());
		sv.bankacckey.set(mandlnbIO.getBankacckey());
		validateClientRole2010();//ILIFE-2472
	}

	//ILIFE-2472-START
	protected void validateClientRole2010() {
		wsaaChdrnum.set(payrpf.getChdrnum());
		wsaaPayrseqno.set(payrpf.getPayrseqno());
		
		Clrfpf clrfIO = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrlnbIO.getChdrpfx().toString(), payrpf.getChdrcoy(),wsaaForenum.toString(), "PY");
		if(clrfIO != null && !clrfIO.getClntnum().equals(sv.payrnum.toString())) {
			clrfIO = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrlnbIO.getChdrpfx().toString(), payrpf.getChdrcoy(),wsaaForenum.toString(), "LA");
			if(clrfIO != null && !clrfIO.getClntnum().equals(sv.payrnum.toString())) {
				clrfIO = clrfpfDAO.readClrfByForepfxAndForecoyAndForenumAndRole(chdrlnbIO.getChdrpfx().toString(), payrpf.getChdrcoy(),wsaaForenum.toString(), "OW");
			}
			if(clrfIO == null || !clrfIO.getClntnum().equals(sv.payrnum.toString())) {
				scrnparams.errorCode.set("RFSN");
			}
		}
	}
	//ILIFE-2472-END

	protected void validate2020() {
		babrIO = babrpfDao.searchBankkey(sv.bankkey.toString());
		if (babrIO == null) {
			sv.bankkeyErr.set("E756");
			goTo(GotoLabel.checkForErrors2080);
		}
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);

		clblIO = clbapfDAO.searchClblData(sv.bankkey.toString(), sv.bankacckey.toString(), wsspcomn.fsuco.toString(),sv.payrnum.toString());
		
		if (clblIO == null) {
			sv.bankacckeyErr.set("G600");
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(clblIO.getClntnum(),sv.payrnum)) {
			sv.bankacckeyErr.set("F373");
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isNE(chdrlnbIO.getBillcurr(),clblIO.getCurrcode())) {
			sv.bankacckeyErr.set("F955");
			goTo(GotoLabel.checkForErrors2080);
		}
		if(ispermission) { //IFSU-852
			sv.mrbnk.set(clblIO.getMrbnk()); 			// ILIFE-2476
		    sv.bnkbrn.set(clblIO.getBnkbrn()); 			// ILIFE-2476
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
		if (isNE(sv.facthous,SPACES)) {
			a1200GetFacthousDesc();
		}
	}

	//ILIFE-2472 Starts
	protected void keepMand(){
		mandIO.setPayrcoy(wsspcomn.fsuco);
		mandIO.setFormat(mandrec);
		mandIO.setMandref(sv.mandref);
		mandIO.setPayrnum(sv.payrnum);
		wsspcomn.chdrCownnum.set(sv.payrnum);
		mandIO.setTransactionDate(varcom.vrcmDate);
		mandIO.setTermid(varcom.vrcmTermid);
		mandIO.setUser(varcom.vrcmUser);
		mandIO.setTransactionTime(varcom.vrcmTime);
		mandIO.setTimesUse(999);
		mandIO.setStatChangeDate(varcom.vrcmMaxDate);
		mandIO.setLastUseDate(varcom.vrcmMaxDate);
		mandIO.setEffdate(varcom.vrcmMaxDate);
		mandIO.setLastUseAmt(ZERO);
		mandIO.setMandAmt(ZERO);
		mandIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, mandIO);
		if (isNE(mandIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(mandIO.getParams());
			fatalError600();
		}
	}
	//ILIFE-2472 Ends
	
	protected void checkForErrors2080()	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}
//ILIFE-2476 Starts
	protected void bankbrndsc() {
		if(isEQ(sv.bnkbrn, SPACES)){
			sv.bnkbrn.set("000"+ sv.bankacckey.sub1string(1, 3));
		}
		List<Babrpf> lstBabrpf = babrpfDao.getRecordsByBankKey(sv.bnkbrn.toString());
		if(lstBabrpf.isEmpty()){
			sv.bnkbrndesc.set("Branch Name not exist");
		}else{
			for(Babrpf babrpf : lstBabrpf){
				if(babrpf.getBankkey().substring(5, 6).equals(sv.bnkbrn.toString()) && "Y".equals(babrpf.getAppflag())){
					sv.bnkbrndesc.set(babrpf.getBankdesc());
				}
			}
		}
	}
		//ILIFE-2476 Ends

	@Override
	protected void update3000()	{
		updateDatabase3010();
	}

	protected void updateDatabase3010()	{
		if (isNE(wsaaClntkey,SPACES)) {
			wsspcomn.clntkey.set(wsaaClntkey);
		}
		if (isEQ(wsspcomn.flag,"I")) {
			return ;
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			return ;
		}
		chdrlnbIO.setZmandref(sv.mandref);
		chdrlnbIO.setPayclt(sv.payrnum);
		//ILIFE-2472-END
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		payrpf.setZmandref(sv.mandref.toString());
		payrpf.setDatime(varcom.vrcmTime.toString());//MPTD-211
		payrpfDAO.updatePayrpfByZmandref(payrpf);
	}

	@Override
	protected void whereNext4000()	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		wsspcomn.clonekey.set(SPACES);
		/*EXIT*/
	}

	protected void a1000GetPayorname()	{
		/*A1010-NAMADRS*/
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(sv.payrnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		/*A1090-EXIT*/
	}

	protected void a1200GetFacthousDesc() {
		 Descpf descpf = descDAO.getdescData("IT", "T3684",sv.facthous.toString(),wsspcomn.fsuco.toString(),wsspcomn.language.toString());
		if (descpf == null) {
			fatalError600();
		}
		sv.longdesc.set(descpf.getLongdesc());
	}

}