package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sjl05 Date: 20 December 2019 Author: vdivisala
 */
public class Sjl05ScreenVars extends SmartVarModel {

	public FixedLengthStringData dataArea = new FixedLengthStringData(798);
	public FixedLengthStringData dataFields = new FixedLengthStringData(94).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields, 1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields, 9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields, 39);
	
	public FixedLengthStringData billfreqs = new FixedLengthStringData(20).isAPartOf(dataFields, 44);
	public FixedLengthStringData[] billfreq = FLSArrayPartOfStructure(10, 2, billfreqs, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(20).isAPartOf(billfreqs, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01 = DD.billfreq.copy().isAPartOf(filler1, 0);
	public FixedLengthStringData billfreq02 = DD.billfreq.copy().isAPartOf(filler1, 2);
	public FixedLengthStringData billfreq03 = DD.billfreq.copy().isAPartOf(filler1, 4);
	public FixedLengthStringData billfreq04 = DD.billfreq.copy().isAPartOf(filler1, 6);
	public FixedLengthStringData billfreq05 = DD.billfreq.copy().isAPartOf(filler1, 8);
	public FixedLengthStringData billfreq06 = DD.billfreq.copy().isAPartOf(filler1, 10);
	public FixedLengthStringData billfreq07 = DD.billfreq.copy().isAPartOf(filler1, 12);
	public FixedLengthStringData billfreq08 = DD.billfreq.copy().isAPartOf(filler1, 14);
	public FixedLengthStringData billfreq09 = DD.billfreq.copy().isAPartOf(filler1, 16);
	public FixedLengthStringData billfreq10 = DD.billfreq.copy().isAPartOf(filler1, 18);
	
	public FixedLengthStringData mops = new FixedLengthStringData(10).isAPartOf(dataFields, 64);
	public FixedLengthStringData[] mop = FLSArrayPartOfStructure(10, 1, mops, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(10).isAPartOf(mops, 0, FILLER_REDEFINE);
	public FixedLengthStringData mop01 = DD.mop.copy().isAPartOf(filler2, 0);
	public FixedLengthStringData mop02 = DD.mop.copy().isAPartOf(filler2, 1);
	public FixedLengthStringData mop03 = DD.mop.copy().isAPartOf(filler2, 2);
	public FixedLengthStringData mop04 = DD.mop.copy().isAPartOf(filler2, 3);
	public FixedLengthStringData mop05 = DD.mop.copy().isAPartOf(filler2, 4);
	public FixedLengthStringData mop06 = DD.mop.copy().isAPartOf(filler2, 5);
	public FixedLengthStringData mop07 = DD.mop.copy().isAPartOf(filler2, 6);
	public FixedLengthStringData mop08 = DD.mop.copy().isAPartOf(filler2, 7);
	public FixedLengthStringData mop09 = DD.mop.copy().isAPartOf(filler2, 8);
	public FixedLengthStringData mop10 = DD.mop.copy().isAPartOf(filler2, 9);
	
	public FixedLengthStringData fwcondts = new FixedLengthStringData(10).isAPartOf(dataFields, 74);
	public FixedLengthStringData[] fwcondt = FLSArrayPartOfStructure(10, 1, fwcondts, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(10).isAPartOf(fwcondts, 0, FILLER_REDEFINE);
	public FixedLengthStringData fwcondt01 = DD.fwcondt.copy().isAPartOf(filler3, 0);
	public FixedLengthStringData fwcondt02 = DD.fwcondt.copy().isAPartOf(filler3, 1);
	public FixedLengthStringData fwcondt03 = DD.fwcondt.copy().isAPartOf(filler3, 2);
	public FixedLengthStringData fwcondt04 = DD.fwcondt.copy().isAPartOf(filler3, 3);
	public FixedLengthStringData fwcondt05 = DD.fwcondt.copy().isAPartOf(filler3, 4);
	public FixedLengthStringData fwcondt06 = DD.fwcondt.copy().isAPartOf(filler3, 5);
	public FixedLengthStringData fwcondt07 = DD.fwcondt.copy().isAPartOf(filler3, 6);
	public FixedLengthStringData fwcondt08 = DD.fwcondt.copy().isAPartOf(filler3, 7);
	public FixedLengthStringData fwcondt09 = DD.fwcondt.copy().isAPartOf(filler3, 8);
	public FixedLengthStringData fwcondt10 = DD.fwcondt.copy().isAPartOf(filler3, 9);
	
	public FixedLengthStringData concommflgs = new FixedLengthStringData(10).isAPartOf(dataFields, 84);
	public FixedLengthStringData[] concommflg = FLSArrayPartOfStructure(10, 1, concommflgs, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(concommflgs, 0, FILLER_REDEFINE);
	public FixedLengthStringData concommflg01 = DD.concommflg.copy().isAPartOf(filler4, 0);
	public FixedLengthStringData concommflg02 = DD.concommflg.copy().isAPartOf(filler4, 1);
	public FixedLengthStringData concommflg03 = DD.concommflg.copy().isAPartOf(filler4, 2);
	public FixedLengthStringData concommflg04 = DD.concommflg.copy().isAPartOf(filler4, 3);
	public FixedLengthStringData concommflg05 = DD.concommflg.copy().isAPartOf(filler4, 4);
	public FixedLengthStringData concommflg06 = DD.concommflg.copy().isAPartOf(filler4, 5);
	public FixedLengthStringData concommflg07 = DD.concommflg.copy().isAPartOf(filler4, 6);
	public FixedLengthStringData concommflg08 = DD.concommflg.copy().isAPartOf(filler4, 7);
	public FixedLengthStringData concommflg09 = DD.concommflg.copy().isAPartOf(filler4, 8);
	public FixedLengthStringData concommflg10 = DD.concommflg.copy().isAPartOf(filler4, 9);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(176).isAPartOf(dataArea, 94);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	
	public FixedLengthStringData billfreqsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] billfreqErr = FLSArrayPartOfStructure(10, 4, billfreqsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(40).isAPartOf(billfreqsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData billfreq01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData billfreq02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData billfreq03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData billfreq04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData billfreq05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData billfreq06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData billfreq07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData billfreq08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData billfreq09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData billfreq10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	
	public FixedLengthStringData mopsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData[] mopErr = FLSArrayPartOfStructure(10, 4, mopsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(40).isAPartOf(mopsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData mop01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData mop02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData mop03Err = new FixedLengthStringData(4).isAPartOf(filler6, 8);
	public FixedLengthStringData mop04Err = new FixedLengthStringData(4).isAPartOf(filler6, 12);
	public FixedLengthStringData mop05Err = new FixedLengthStringData(4).isAPartOf(filler6, 16);
	public FixedLengthStringData mop06Err = new FixedLengthStringData(4).isAPartOf(filler6, 20);
	public FixedLengthStringData mop07Err = new FixedLengthStringData(4).isAPartOf(filler6, 24);
	public FixedLengthStringData mop08Err = new FixedLengthStringData(4).isAPartOf(filler6, 28);
	public FixedLengthStringData mop09Err = new FixedLengthStringData(4).isAPartOf(filler6, 32);
	public FixedLengthStringData mop10Err = new FixedLengthStringData(4).isAPartOf(filler6, 36);
	
	public FixedLengthStringData fwcondtsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData[] fwcondtErr = FLSArrayPartOfStructure(10, 4, fwcondtsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(40).isAPartOf(fwcondtsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData fwcondt01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData fwcondt02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData fwcondt03Err = new FixedLengthStringData(4).isAPartOf(filler7, 8);
	public FixedLengthStringData fwcondt04Err = new FixedLengthStringData(4).isAPartOf(filler7, 12);
	public FixedLengthStringData fwcondt05Err = new FixedLengthStringData(4).isAPartOf(filler7, 16);
	public FixedLengthStringData fwcondt06Err = new FixedLengthStringData(4).isAPartOf(filler7, 20);
	public FixedLengthStringData fwcondt07Err = new FixedLengthStringData(4).isAPartOf(filler7, 24);
	public FixedLengthStringData fwcondt08Err = new FixedLengthStringData(4).isAPartOf(filler7, 28);
	public FixedLengthStringData fwcondt09Err = new FixedLengthStringData(4).isAPartOf(filler7, 32);
	public FixedLengthStringData fwcondt10Err = new FixedLengthStringData(4).isAPartOf(filler7, 36);
	
	public FixedLengthStringData concommflgsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData[] concommflgErr = FLSArrayPartOfStructure(10, 4, concommflgsErr, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(40).isAPartOf(concommflgsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData concommflg01Err = new FixedLengthStringData(4).isAPartOf(filler8, 0);
	public FixedLengthStringData concommflg02Err = new FixedLengthStringData(4).isAPartOf(filler8, 4);
	public FixedLengthStringData concommflg03Err = new FixedLengthStringData(4).isAPartOf(filler8, 8);
	public FixedLengthStringData concommflg04Err = new FixedLengthStringData(4).isAPartOf(filler8, 12);
	public FixedLengthStringData concommflg05Err = new FixedLengthStringData(4).isAPartOf(filler8, 16);
	public FixedLengthStringData concommflg06Err = new FixedLengthStringData(4).isAPartOf(filler8, 20);
	public FixedLengthStringData concommflg07Err = new FixedLengthStringData(4).isAPartOf(filler8, 24);
	public FixedLengthStringData concommflg08Err = new FixedLengthStringData(4).isAPartOf(filler8, 28);
	public FixedLengthStringData concommflg09Err = new FixedLengthStringData(4).isAPartOf(filler8, 32);
	public FixedLengthStringData concommflg10Err = new FixedLengthStringData(4).isAPartOf(filler8, 36);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(528).isAPartOf(dataArea, 270);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	
	public FixedLengthStringData billfreqsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(10, 12, billfreqsOut, 0);
	public FixedLengthStringData[][] billfreqO = FLSDArrayPartOfArrayStructure(12, 1, billfreqOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(120).isAPartOf(billfreqsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] billfreq01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] billfreq02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData[] billfreq03Out = FLSArrayPartOfStructure(12, 1, filler9, 24);
	public FixedLengthStringData[] billfreq04Out = FLSArrayPartOfStructure(12, 1, filler9, 36);
	public FixedLengthStringData[] billfreq05Out = FLSArrayPartOfStructure(12, 1, filler9, 48);
	public FixedLengthStringData[] billfreq06Out = FLSArrayPartOfStructure(12, 1, filler9, 60);
	public FixedLengthStringData[] billfreq07Out = FLSArrayPartOfStructure(12, 1, filler9, 72);
	public FixedLengthStringData[] billfreq08Out = FLSArrayPartOfStructure(12, 1, filler9, 84);
	public FixedLengthStringData[] billfreq09Out = FLSArrayPartOfStructure(12, 1, filler9, 96);
	public FixedLengthStringData[] billfreq10Out = FLSArrayPartOfStructure(12, 1, filler9, 108);
	
	public FixedLengthStringData mopsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 168);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(10, 12, mopsOut, 0);
	public FixedLengthStringData[][] mopO = FLSDArrayPartOfArrayStructure(12, 1, mopOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(120).isAPartOf(mopsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] mop01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] mop02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData[] mop03Out = FLSArrayPartOfStructure(12, 1, filler10, 24);
	public FixedLengthStringData[] mop04Out = FLSArrayPartOfStructure(12, 1, filler10, 36);
	public FixedLengthStringData[] mop05Out = FLSArrayPartOfStructure(12, 1, filler10, 48);
	public FixedLengthStringData[] mop06Out = FLSArrayPartOfStructure(12, 1, filler10, 60);
	public FixedLengthStringData[] mop07Out = FLSArrayPartOfStructure(12, 1, filler10, 72);
	public FixedLengthStringData[] mop08Out = FLSArrayPartOfStructure(12, 1, filler10, 84);
	public FixedLengthStringData[] mop09Out = FLSArrayPartOfStructure(12, 1, filler10, 96);
	public FixedLengthStringData[] mop10Out = FLSArrayPartOfStructure(12, 1, filler10, 108);
	
	public FixedLengthStringData fwcondtsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 288);
	public FixedLengthStringData[] fwcondtOut = FLSArrayPartOfStructure(10, 12, fwcondtsOut, 0);
	public FixedLengthStringData[][] fwcondtO = FLSDArrayPartOfArrayStructure(12, 1, fwcondtOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(120).isAPartOf(fwcondtsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] fwcondt01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] fwcondt02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] fwcondt03Out = FLSArrayPartOfStructure(12, 1, filler11, 24);
	public FixedLengthStringData[] fwcondt04Out = FLSArrayPartOfStructure(12, 1, filler11, 36);
	public FixedLengthStringData[] fwcondt05Out = FLSArrayPartOfStructure(12, 1, filler11, 48);
	public FixedLengthStringData[] fwcondt06Out = FLSArrayPartOfStructure(12, 1, filler11, 60);
	public FixedLengthStringData[] fwcondt07Out = FLSArrayPartOfStructure(12, 1, filler11, 72);
	public FixedLengthStringData[] fwcondt08Out = FLSArrayPartOfStructure(12, 1, filler11, 84);
	public FixedLengthStringData[] fwcondt09Out = FLSArrayPartOfStructure(12, 1, filler11, 96);
	public FixedLengthStringData[] fwcondt10Out = FLSArrayPartOfStructure(12, 1, filler11, 108);
	
	public FixedLengthStringData concommflgsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 408);
	public FixedLengthStringData[] concommflgOut = FLSArrayPartOfStructure(10, 12, concommflgsOut, 0);
	public FixedLengthStringData[][] concommflgO = FLSDArrayPartOfArrayStructure(12, 1, concommflgOut, 0);
	public FixedLengthStringData filler12 = new FixedLengthStringData(120).isAPartOf(concommflgsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] concommflg01Out = FLSArrayPartOfStructure(12, 1, filler12, 0);
	public FixedLengthStringData[] concommflg02Out = FLSArrayPartOfStructure(12, 1, filler12, 12);
	public FixedLengthStringData[] concommflg03Out = FLSArrayPartOfStructure(12, 1, filler12, 24);
	public FixedLengthStringData[] concommflg04Out = FLSArrayPartOfStructure(12, 1, filler12, 36);
	public FixedLengthStringData[] concommflg05Out = FLSArrayPartOfStructure(12, 1, filler12, 48);
	public FixedLengthStringData[] concommflg06Out = FLSArrayPartOfStructure(12, 1, filler12, 60);
	public FixedLengthStringData[] concommflg07Out = FLSArrayPartOfStructure(12, 1, filler12, 72);
	public FixedLengthStringData[] concommflg08Out = FLSArrayPartOfStructure(12, 1, filler12, 84);
	public FixedLengthStringData[] concommflg09Out = FLSArrayPartOfStructure(12, 1, filler12, 96);
	public FixedLengthStringData[] concommflg10Out = FLSArrayPartOfStructure(12, 1, filler12, 108);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sjl05screenWritten = new LongData(0);
	public LongData Sjl05protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}

	public Sjl05ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(billfreq01Out, new String[] { "05", "45", "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(billfreq02Out, new String[] { "06", "46", "-06", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(billfreq03Out, new String[] { "07", "47", "-07", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(billfreq04Out, new String[] { "08", "48", "-08", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(billfreq05Out, new String[] { "09", "49", "-09", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(billfreq06Out, new String[] { "10", "50", "-10", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(billfreq07Out, new String[] { "11", "51", "-11", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(billfreq08Out, new String[] { "12", "52", "-12", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(billfreq09Out, new String[] { "13", "53", "-13", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(billfreq10Out, new String[] { "14", "54", "-14", null, null, null, null, null, null, null, null, null });
		
		fieldIndMap.put(mop01Out, new String[] { "15", "55", "-15", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(mop02Out, new String[] { "16", "56", "-16", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(mop03Out, new String[] { "17", "57", "-17", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(mop04Out, new String[] { "18", "58", "-18", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(mop05Out, new String[] { "19", "59", "-19", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(mop06Out, new String[] { "20", "60", "-20", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(mop07Out, new String[] { "21", "61", "-21", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(mop08Out, new String[] { "22", "62", "-22", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(mop09Out, new String[] { "23", "63", "-23", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(mop10Out, new String[] { "24", "64", "-24", null, null, null, null, null, null, null, null, null });
		
		fieldIndMap.put(fwcondt01Out, new String[] { "25", "65", "-25", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fwcondt02Out, new String[] { "26", "66", "-26", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fwcondt03Out, new String[] { "27", "67", "-27", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fwcondt04Out, new String[] { "28", "68", "-28", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fwcondt05Out, new String[] { "29", "69", "-29", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fwcondt06Out, new String[] { "30", "70", "-30", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fwcondt07Out, new String[] { "31", "71", "-31", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fwcondt08Out, new String[] { "32", "72", "-32", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fwcondt09Out, new String[] { "33", "73", "-33", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fwcondt10Out, new String[] { "34", "74", "-34", null, null, null, null, null, null, null, null, null });
		
		fieldIndMap.put(concommflg01Out, new String[] { "35", "75", "-35", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(concommflg02Out, new String[] { "36", "76", "-36", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(concommflg03Out, new String[] { "37", "77", "-37", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(concommflg04Out, new String[] { "38", "78", "-38", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(concommflg05Out, new String[] { "39", "79", "-39", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(concommflg06Out, new String[] { "40", "80", "-40", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(concommflg07Out, new String[] { "41", "81", "-41", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(concommflg08Out, new String[] { "42", "82", "-42", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(concommflg09Out, new String[] { "43", "83", "-43", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(concommflg10Out, new String[] { "44", "84", "-44", null, null, null, null, null, null, null, null, null });

		screenFields = new BaseData[] { company, tabl, item, longdesc, billfreq01, billfreq02, billfreq03, billfreq04, billfreq05, billfreq06, billfreq07, billfreq08, billfreq09, billfreq10, mop01, mop02, mop03, mop04, mop05, mop06, mop07, mop08, mop09, mop10, fwcondt01, fwcondt02, fwcondt03, fwcondt04, fwcondt05, fwcondt06, fwcondt07, fwcondt08, fwcondt09, fwcondt10, concommflg01, concommflg02, concommflg03, concommflg04, concommflg05, concommflg06, concommflg07, concommflg08, concommflg09, concommflg10 };
		screenOutFields = new BaseData[][] { companyOut, tablOut, itemOut, longdescOut, billfreq01Out, billfreq02Out, billfreq03Out, billfreq04Out, billfreq05Out, billfreq06Out, billfreq07Out, billfreq08Out, billfreq09Out, billfreq10Out, mop01Out, mop02Out, mop03Out, mop04Out, mop05Out, mop06Out, mop07Out, mop08Out, mop09Out, mop10Out, fwcondt01Out, fwcondt02Out, fwcondt03Out, fwcondt04Out, fwcondt05Out, fwcondt06Out, fwcondt07Out, fwcondt08Out, fwcondt09Out, fwcondt10Out, concommflg01Out, concommflg02Out, concommflg03Out, concommflg04Out, concommflg05Out, concommflg06Out, concommflg07Out, concommflg08Out, concommflg09Out, concommflg10Out };
		screenErrFields = new BaseData[] { companyErr, tablErr, itemErr, longdescErr, billfreq01Err, billfreq02Err, billfreq03Err, billfreq04Err, billfreq05Err, billfreq06Err, billfreq07Err, billfreq08Err, billfreq09Err, billfreq10Err, mop01Err, mop02Err, mop03Err, mop04Err, mop05Err, mop06Err, mop07Err, mop08Err, mop09Err, mop10Err, fwcondt01Err, fwcondt02Err, fwcondt03Err, fwcondt04Err, fwcondt05Err, fwcondt06Err, fwcondt07Err, fwcondt08Err, fwcondt09Err, fwcondt10Err, concommflg01Err, concommflg02Err, concommflg03Err, concommflg04Err, concommflg05Err, concommflg06Err, concommflg07Err, concommflg08Err, concommflg09Err, concommflg10Err };
		
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl05screen.class;
		protectRecord = Sjl05protect.class;
	}
}