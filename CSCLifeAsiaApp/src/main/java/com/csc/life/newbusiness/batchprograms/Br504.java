/*
 * File: Br504.java
 * Date: 29 August 2009 22:10:11
 * Author: Quipoz Limited
 *
 * Class transformed from BR504.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.common.DD;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.printing.dataaccess.LetcdhnTableDAM;
import com.csc.life.newbusiness.reports.Rr504Report;
import com.csc.life.unitlinkedprocessing.dataaccess.CovtunlTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.procedures.Batcdor;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*                Letter of Acceptance
*                --------------------
*
*  This batch program prints the letter of acceptance before the
*  issue of contract. It is called by the Standard Letter
*  Printing Batch program (L2LETTERS) with letter-type ACCEPT.
*  Before submitting the batch job, a letter request must be
*  requested to create a LETC record.
*
*****************************************************************
* </pre>
*/
public class Br504 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rr504Report printerFile = new Rr504Report();
	private FixedLengthStringData printerRecord = new FixedLengthStringData(350);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR504");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaTotalSingp = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaEditSingp = new ZonedDecimalData(17, 2).setPattern("***,***,***,***,**9.99-");
	private String wsaaLetterType = "ACCEPT";
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private int wsaaMaxLines = 36;
	private FixedLengthStringData wsspLongconfname = new FixedLengthStringData(47);

	private FixedLengthStringData wsaaOtherKey = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaOtherKey, 0).init(SPACES);
	private FixedLengthStringData filler = new FixedLengthStringData(22).isAPartOf(wsaaOtherKey, 8, FILLER).init(SPACES);
	private String chdrrec = "CHDRREC";
	private String covtunlrec = "COVTUNLREC";
	private String letcdhnrec = "LETCDHNREC";
		/* TABLES */
	private String t3590 = "T3590";
	private String t3645 = "T3645";
	private String t5687 = "T5687";
	private String t5688 = "T5688";

	private FixedLengthStringData rr504h01Record = new FixedLengthStringData(137+DD.cltaddr.length*5); //Starts ILIFE-3212
	private FixedLengthStringData rr504h01O = new FixedLengthStringData(137+DD.cltaddr.length*5).isAPartOf(rr504h01Record, 0);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22).isAPartOf(rr504h01O, 0);
	private FixedLengthStringData clntnm = new FixedLengthStringData(37).isAPartOf(rr504h01O, 22);
	private FixedLengthStringData addr1 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr504h01O, 59);
	private FixedLengthStringData addr2 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr504h01O, 59+DD.cltaddr.length*1);
	private FixedLengthStringData addr3 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr504h01O, 59+DD.cltaddr.length*2);
	private FixedLengthStringData addr4 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr504h01O, 59+DD.cltaddr.length*3);
	private FixedLengthStringData addr5 = new FixedLengthStringData(DD.cltaddr.length).isAPartOf(rr504h01O, 59+DD.cltaddr.length*4);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rr504h01O, 59+DD.cltaddr.length*5);
	private FixedLengthStringData rcesdt = new FixedLengthStringData(10).isAPartOf(rr504h01O, 67+DD.cltaddr.length*5);
	private FixedLengthStringData payfrqd = new FixedLengthStringData(10).isAPartOf(rr504h01O, 77+DD.cltaddr.length*5);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(rr504h01O, 87+DD.cltaddr.length*5);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(rr504h01O, 117+DD.cltaddr.length*5);
	private FixedLengthStringData atranamt = new FixedLengthStringData(17).isAPartOf(rr504h01O, 120+DD.cltaddr.length*5); //End ILIFE-3212

	private FixedLengthStringData rr504d01Record = new FixedLengthStringData(64);
	private FixedLengthStringData rr504d01O = new FixedLengthStringData(64).isAPartOf(rr504d01Record, 0);
	private FixedLengthStringData crtabled = new FixedLengthStringData(30).isAPartOf(rr504d01O, 0);
	private ZonedDecimalData sumins = new ZonedDecimalData(17, 2).isAPartOf(rr504d01O, 30);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2).isAPartOf(rr504d01O, 47);

	private FixedLengthStringData rr504d02Record = new FixedLengthStringData(2);
	private Batcdorrec batcdorrec = new Batcdorrec();
		/*Contract header file*/
	private ChdrTableDAM chdrIO = new ChdrTableDAM();
		/*Client logical file with new fields*/
	private CltsTableDAM cltsIO = new CltsTableDAM();
		/*Coverage transactions - unit linked*/
	private CovtunlTableDAM covtunlIO = new CovtunlTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
		/*Letter Control File for DD Dishonours.*/
	private LetcdhnTableDAM letcdhnIO = new LetcdhnTableDAM();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextLetcdhn1080,
		exit1090,
		exit2190,
		exit2290,
		lgnmExit,
		plainExit,
		payeeExit
	}

	public Br504() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		open110();
		openBatch120();
		mainProcess130();
		closeBatch170();
		out185();
	}

protected void open110()
	{
		printerFile.openOutput();
	}

protected void openBatch120()
	{
		batcdorrec.function.set("OPEN");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.runparm1);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
	}

protected void mainProcess130()
	{
		datcon6rec.datcon6Rec.set(SPACES);
		datcon6rec.language.set(runparmrec.language);
		datcon6rec.company.set(runparmrec.company);
		datcon6rec.intDate1.set(runparmrec.effdate);
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		if (isNE(datcon6rec.statuz,varcom.oK)) {
			conerrrec.params.set(datcon6rec.datcon6Rec);
			databaseError006();
		}
		datetexc.set(datcon6rec.intDate2);
		letcdhnIO.setParams(SPACES);
		letcdhnIO.setLetterSeqno(ZERO);
		letcdhnIO.setClntnum(ZERO);
		letcdhnIO.setClntcoy(ZERO);
		letcdhnIO.setRequestCompany(runparmrec.company);
		letcdhnIO.setLetterType(wsaaLetterType);
		letcdhnIO.setFormat(letcdhnrec);
		letcdhnIO.setFunction(varcom.begn);
		while ( !(isEQ(letcdhnIO.getStatuz(),varcom.endp))) {
			mainProcessing1000();
		}

	}

protected void closeBatch170()
	{
		batcdorrec.function.set("CLOSE");
		batcdorrec.tranid.set(runparmrec.tranid);
		batcdorrec.company.set(runparmrec.company);
		batcdorrec.branch.set(runparmrec.batcbranch);
		batcdorrec.actyear.set(runparmrec.acctyear);
		batcdorrec.actmonth.set(runparmrec.acctmonth);
		batcdorrec.trcde.set(runparmrec.runparm1);
		callProgram(Batcdor.class, batcdorrec.batcdorRec);
		if (isNE(batcdorrec.statuz,varcom.oK)) {
			conerrrec.statuz.set(batcdorrec.statuz);
			conerrrec.params.set(batcdorrec.batcdorRec);
			systemError005();
		}
	}

protected void out185()
	{
		printerFile.close();
		/*EXIT*/
	}

protected void mainProcessing1000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1100();
				}
				case nextLetcdhn1080: {
					nextLetcdhn1080();
				}
				case exit1090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1100()
	{
		SmartFileCode.execute(appVars, letcdhnIO);
		if (isNE(letcdhnIO.getStatuz(),varcom.oK)
		&& isNE(letcdhnIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(letcdhnIO.getParams());
			databaseError006();
		}
		if (isEQ(letcdhnIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit1090);
		}
		if (isNE(letcdhnIO.getLetterType(),wsaaLetterType)) {
			goTo(GotoLabel.nextLetcdhn1080);
		}
		processLetcdhn2000();
	}

protected void nextLetcdhn1080()
	{
		letcdhnIO.setFunction(varcom.nextr);
	}

protected void processLetcdhn2000()
	{
		para2010();
	}

protected void para2010()
	{
		wsaaOtherKey.set(letcdhnIO.getOtherKeys());
		clientDetails2300();
		chdrnum.set(wsaaChdrnum);
		chdrIO.setParams(SPACES);
		chdrIO.setChdrpfx(fsupfxcpy.chdr);
		chdrIO.setChdrcoy(letcdhnIO.getRequestCompany());
		chdrIO.setChdrnum(wsaaChdrnum);
		chdrIO.setValidflag("3");
		chdrIO.setCurrfrom(varcom.vrcmMaxDate);
		chdrIO.setFormat(chdrrec);
		chdrIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, chdrIO);
		if (isNE(chdrIO.getStatuz(),varcom.oK)
		&& isNE(chdrIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(chdrIO.getParams());
			databaseError006();
		}
		readT35902400();
		readT56882500();
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(chdrIO.getOccdate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rcesdt.set(datcon1rec.extDate);
		wsaaTotalSingp.set(ZERO);
		covtunlIO.setParams(SPACES);
		covtunlIO.setChdrcoy(letcdhnIO.getRequestCompany());
		covtunlIO.setChdrnum(wsaaChdrnum);
		covtunlIO.setLife("01");
		covtunlIO.setCoverage(SPACES);
		covtunlIO.setRider(SPACES);
		covtunlIO.setSeqnbr(ZERO);
		covtunlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtunlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtunlIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covtunlIO.setFormat(covtunlrec);
		while ( !(isEQ(covtunlIO.getStatuz(),varcom.endp))) {
			accumPremium2100();
		}

		cntcurr.set(chdrIO.getCntcurr());
		wsaaEditSingp.set(wsaaTotalSingp);
		atranamt.set(wsaaEditSingp);
		printHeader2700();
		covtunlIO.setParams(SPACES);
		covtunlIO.setChdrcoy(letcdhnIO.getRequestCompany());
		covtunlIO.setChdrnum(wsaaChdrnum);
		covtunlIO.setLife("01");
		covtunlIO.setCoverage(SPACES);
		covtunlIO.setRider(SPACES);
		covtunlIO.setSeqnbr(ZERO);
		covtunlIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtunlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtunlIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covtunlIO.setFormat(covtunlrec);
		while ( !(isEQ(covtunlIO.getStatuz(),varcom.endp))) {
			printDetails2200();
		}

	}

protected void accumPremium2100()
	{
		try {
			para2110();
		}
		catch (GOTOException e){
		}
	}

protected void para2110()
	{
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(),varcom.oK)
		&& isNE(covtunlIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(covtunlIO.getParams());
			databaseError006();
		}
		if (isNE(covtunlIO.getChdrcoy(),letcdhnIO.getRequestCompany())
		|| isNE(covtunlIO.getChdrnum(),wsaaChdrnum)
		|| isEQ(covtunlIO.getStatuz(),varcom.endp)) {
			covtunlIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2190);
		}
		wsaaTotalSingp.add(covtunlIO.getSingp());
		covtunlIO.setFunction(varcom.nextr);
	}

protected void printDetails2200()
	{
		try {
			para2210();
		}
		catch (GOTOException e){
		}
	}

protected void para2210()
	{
		SmartFileCode.execute(appVars, covtunlIO);
		if (isNE(covtunlIO.getStatuz(),varcom.oK)
		&& isNE(covtunlIO.getStatuz(),varcom.endp)) {
			conerrrec.dbparams.set(covtunlIO.getParams());
			databaseError006();
		}
		if (isNE(covtunlIO.getChdrcoy(),letcdhnIO.getRequestCompany())
		|| isNE(covtunlIO.getChdrnum(),wsaaChdrnum)
		|| isEQ(covtunlIO.getStatuz(),varcom.endp)) {
			covtunlIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2290);
		}
		sumins.set(covtunlIO.getSumins());
		singp.set(covtunlIO.getSingp());
		readT56872800();
		if (isGT(wsaaCount,wsaaMaxLines)) {
			printerFile.printRr504d02(rr504d02Record);
			printHeader2700();
		}
		printerFile.printRr504d01(rr504d01Record);
		wsaaCount.add(1);
		covtunlIO.setFunction(varcom.nextr);
	}

protected void clientDetails2300()
	{
		para2310();
	}

protected void para2310()
	{
		cltsIO.setParams(SPACES);
		cltsIO.setClntpfx(fsupfxcpy.clnt);
		cltsIO.setClntcoy(letcdhnIO.getClntcoy());
		cltsIO.setClntnum(letcdhnIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(cltsIO.getParams());
			databaseError006();
		}
		plainname();
		clntnm.set(wsspLongconfname);
		addr1.set(SPACES);
		addr2.set(SPACES);
		addr3.set(SPACES);
		addr4.set(SPACES);
		addr5.set(SPACES);
		addr1.set(cltsIO.getCltaddr01());
		if (isEQ(addr1,SPACES)) {
			addr1.set(cltsIO.getCltaddr02());
		}
		else {
			addr2.set(cltsIO.getCltaddr02());
		}
		if (isEQ(addr1,SPACES)) {
			addr1.set(cltsIO.getCltaddr03());
		}
		else {
			if (isEQ(addr2,SPACES)) {
				addr2.set(cltsIO.getCltaddr03());
			}
			else {
				addr3.set(cltsIO.getCltaddr03());
			}
		}
		if (isEQ(addr1,SPACES)) {
			addr1.set(cltsIO.getCltaddr04());
		}
		else {
			if (isEQ(addr2,SPACES)) {
				addr2.set(cltsIO.getCltaddr04());
			}
			else {
				if (isEQ(addr3,SPACES)) {
					addr3.set(cltsIO.getCltaddr04());
				}
				else {
					addr4.set(cltsIO.getCltaddr04());
				}
			}
		}
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(cltsIO.getClntcoy());
		descIO.setDesctabl(t3645);
		descIO.setDescitem(cltsIO.getCtrycode());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			addr5.fill("?");
		}
		else {
			if (isEQ(addr1,SPACES)) {
				StringBuilder stringVariable1 = new StringBuilder();
				stringVariable1.append(delimitedExp(descIO.getLongdesc(), "  "));
				stringVariable1.append(" ");
				stringVariable1.append(delimitedExp(cltsIO.getCltpcode(), " "));
				addr1.setLeft(stringVariable1.toString());
			}
			else {
				if (isEQ(addr2,SPACES)) {
					StringBuilder stringVariable2 = new StringBuilder();
					stringVariable2.append(delimitedExp(descIO.getLongdesc(), "  "));
					stringVariable2.append(" ");
					stringVariable2.append(delimitedExp(cltsIO.getCltpcode(), " "));
					addr2.setLeft(stringVariable2.toString());
				}
				else {
					if (isEQ(addr3,SPACES)) {
						StringBuilder stringVariable3 = new StringBuilder();
						stringVariable3.append(delimitedExp(descIO.getLongdesc(), "  "));
						stringVariable3.append(" ");
						stringVariable3.append(delimitedExp(cltsIO.getCltpcode(), " "));
						addr3.setLeft(stringVariable3.toString());
					}
					else {
						if (isEQ(addr4,SPACES)) {
							StringBuilder stringVariable4 = new StringBuilder();
							stringVariable4.append(delimitedExp(descIO.getLongdesc(), "  "));
							stringVariable4.append(" ");
							stringVariable4.append(delimitedExp(cltsIO.getCltpcode(), " "));
							addr4.setLeft(stringVariable4.toString());
						}
						else {
							StringBuilder stringVariable5 = new StringBuilder();
							stringVariable5.append(delimitedExp(descIO.getLongdesc(), "  "));
							stringVariable5.append(" ");
							stringVariable5.append(delimitedExp(cltsIO.getCltpcode(), " "));
							addr5.setLeft(stringVariable5.toString());
						}
					}
				}
			}
		}
	}

protected void readT35902400()
	{
		para2410();
	}

protected void para2410()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t3590);
		descIO.setDescitem(chdrIO.getBillfreq());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		payfrqd.set(descIO.getLongdesc());
	}

protected void readT56882500()
	{
		para2510();
	}

protected void para2510()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrIO.getCnttype());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		cntdesc.set(descIO.getLongdesc());
	}

protected void printHeader2700()
	{
		/*PARA*/
		printerFile.printRr504h01(rr504h01Record);
		wsaaCount.set(31);
		/*EXIT*/
	}

protected void readT56872800()
	{
		para2810();
	}

protected void para2810()
	{
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t5687);
		descIO.setDescitem(covtunlIO.getCrtable());
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.dbparams.set(descIO.getParams());
			databaseError006();
		}
		crtabled.set(descIO.getLongdesc());
	}

protected void largename()
	{
		try {
			lgnm100();
		}
		catch (GOTOException e){
		}
	}

protected void lgnm100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.lgnmExit);
		}
		wsspLongconfname.set(cltsIO.getSurname());
	}

protected void plainname()
	{
		try {
			plain100();
		}
		catch (GOTOException e){
		}
	}

protected void plain100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.plainExit);
		}
		if (isNE(cltsIO.getGivname(),SPACES)) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(", ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
		}
		else {
			wsspLongconfname.set(cltsIO.getSurname());
		}
	}

protected void payeename()
	{
		try {
			payee100();
		}
		catch (GOTOException e){
		}
	}

protected void payee100()
	{
		wsspLongconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(),"C")) {
			corpname();
			goTo(GotoLabel.payeeExit);
		}
		if (isEQ(cltsIO.getEthorig(),"1")) {
			StringBuilder stringVariable1 = new StringBuilder();
			stringVariable1.append(delimitedExp(cltsIO.getSalutl(), "  "));
			stringVariable1.append(". ");
			stringVariable1.append(delimitedExp(cltsIO.getSurname(), "  "));
			stringVariable1.append(" ");
			stringVariable1.append(delimitedExp(cltsIO.getGivname(), "  "));
			wsspLongconfname.setLeft(stringVariable1.toString());
			goTo(GotoLabel.payeeExit);
		}
		StringBuilder stringVariable2 = new StringBuilder();
		stringVariable2.append(delimitedExp(cltsIO.getSalutl(), "  "));
		stringVariable2.append(". ");
		stringVariable2.append(delimitedExp(cltsIO.getGivname(), "  "));
		stringVariable2.append(" ");
		stringVariable2.append(delimitedExp(cltsIO.getSurname(), "  "));
		wsspLongconfname.setLeft(stringVariable2.toString());
	}

protected void corpname()
	{
		/*PAYEE-100*/
		wsspLongconfname.set(SPACES);
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(delimitedExp(cltsIO.getLsurname(), "  "));
		stringVariable1.append(" ");
		stringVariable1.append(delimitedExp(cltsIO.getLgivname(), "  "));
		wsspLongconfname.setLeft(stringVariable1.toString());
		/*CORP-EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*WRITE-DETAIL*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*EXIT*/
	}
}
