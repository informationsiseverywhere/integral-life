package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR589
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sr589ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(263);
	public FixedLengthStringData dataFields = new FixedLengthStringData(151).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData zdocname = DD.zdocname.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData zdoctor = DD.zdoctor.copy().isAPartOf(dataFields,143);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 151);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData zdocnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData zdoctorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 179);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] zdocnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] zdoctorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(383);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(109).isAPartOf(subfileArea, 0);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(subfileFields,0);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData date_var = DD.dateVar.copy().isAPartOf(subfileFields,18);
	public ZonedDecimalData exprdate = DD.exprdate.copyToZonedDecimal().isAPartOf(subfileFields,28);
	public FixedLengthStringData fupcode = DD.fupcde.copy().isAPartOf(subfileFields,36);
	public ZonedDecimalData fupno = DD.fupno.copyToZonedDecimal().isAPartOf(subfileFields,39);
	public ZonedDecimalData fuprcvd = DD.fuprcvd.copyToZonedDecimal().isAPartOf(subfileFields,41);
	public FixedLengthStringData fupremk = DD.fuprmk.copy().isAPartOf(subfileFields,49);
	public FixedLengthStringData fupstat = DD.fupsts.copy().isAPartOf(subfileFields,89);
	public FixedLengthStringData fuptype = DD.fuptyp.copy().isAPartOf(subfileFields,90);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(subfileFields,91);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,92);
	public ZonedDecimalData lifeno = DD.lifeno.copyToZonedDecimal().isAPartOf(subfileFields,94);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,96);
	public ZonedDecimalData tranno = DD.tranno.copyToZonedDecimal().isAPartOf(subfileFields,97);
	public ZonedDecimalData transactionDate = DD.trdt.copyToZonedDecimal().isAPartOf(subfileFields,102);
	public FixedLengthStringData uprflag = DD.uprflag.copy().isAPartOf(subfileFields,108);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(68).isAPartOf(subfileArea, 109);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData dateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData exprdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData fupcdeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData fupnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData fuprcvdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData fuprmkErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData fupstsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData fuptypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData lifenoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData trannoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData trdtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData uprflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(204).isAPartOf(subfileArea, 177);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] dateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] exprdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] fupcdeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] fupnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] fuprcvdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] fuprmkOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] fupstsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] fuptypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] lifenoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] trannoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] trdtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public FixedLengthStringData[] uprflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 381);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData exprdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fuprcvdDisp = new FixedLengthStringData(10);

	public LongData Sr589screensflWritten = new LongData(0);
	public LongData Sr589screenctlWritten = new LongData(0);
	public LongData Sr589screenWritten = new LongData(0);
	public LongData Sr589protectWritten = new LongData(0);
	public GeneralTable sr589screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr589screensfl;
	}

	public Sr589ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(selectOut,new String[] {"39","38","-39",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {fupno, transactionDate, uprflag, crtuser, fuptype, fupcode, lifeno, jlife, fupstat, date_var, crtdate, tranno, ind, select, fupremk, fuprcvd, exprdate};
		screenSflOutFields = new BaseData[][] {fupnoOut, trdtOut, uprflagOut, crtuserOut, fuptypOut, fupcdeOut, lifenoOut, jlifeOut, fupstsOut, dateOut, crtdateOut, trannoOut, indOut, selectOut, fuprmkOut, fuprcvdOut, exprdateOut};
		screenSflErrFields = new BaseData[] {fupnoErr, trdtErr, uprflagErr, crtuserErr, fuptypErr, fupcdeErr, lifenoErr, jlifeErr, fupstsErr, dateErr, crtdateErr, trannoErr, indErr, selectErr, fuprmkErr, fuprcvdErr, exprdateErr};
		screenSflDateFields = new BaseData[] {crtdate, fuprcvd, exprdate};
		screenSflDateErrFields = new BaseData[] {crtdateErr, fuprcvdErr, exprdateErr};
		screenSflDateDispFields = new BaseData[] {crtdateDisp, fuprcvdDisp, exprdateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername, zdoctor, zdocname};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, zdoctorOut, zdocnameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, zdoctorErr, zdocnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr589screen.class;
		screenSflRecord = Sr589screensfl.class;
		screenCtlRecord = Sr589screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr589protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr589screenctl.lrec.pageSubfile);
	}
}
