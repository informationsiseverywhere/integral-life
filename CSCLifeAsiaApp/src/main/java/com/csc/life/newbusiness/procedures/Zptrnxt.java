/*
 * File: Zptrnxt.java
 * Date: 30 August 2009 2:55:27
 * Author: Quipoz Limited
 *
 * Class transformed from ZPTRNXT.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.recordstructures.Ptrnxtrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.smart.dataaccess.BakyTableDAM;
import com.csc.smart.dataaccess.BprdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.recordstructures.Bakykey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
* This subroutine returns the next PTRN record to
* the calling program.
*
* Function:
*
*     No functions are required by this subroutine
*
* Statii:
*
*     **** - OK
*     BOMB - System/Data base error
*
* Linkage Area:
*
*      Function                PIC X(05)
*      Status                  PIC X(04)
*      PTRN data               PIC X(140)
*      (These fields are contained within PTRNXT)
*
*****************************************************************
* </pre>
*/
public class Zptrnxt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String wsaaFirst = "Y";
		/* FORMATS */
	private String bakyrec = "BAKYREC";
	private String ptrnrec = "PTRNREC";

	private FixedLengthStringData wsccStparms = new FixedLengthStringData(8);
	private FixedLengthStringData wsccProgram = new FixedLengthStringData(5).isAPartOf(wsccStparms, 0);
	private FixedLengthStringData wsccJobStep = new FixedLengthStringData(3).isAPartOf(wsccStparms, 5);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
		/*Logical File: Extracted Batch Keys*/
	private BakyTableDAM bakyIO = new BakyTableDAM();
		/*Process Schedule Rules Multi Thread Batc*/
	private BprdTableDAM bprdIO = new BprdTableDAM();
		/*Schedule File - Multi-Thread Batch*/
	private BsscTableDAM bsscIO = new BsscTableDAM();
		/*Policy transaction history logical file*/
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Ptrnxtrec ptrnxtrec = new Ptrnxtrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Syserrrec syserrrec = new Syserrrec();
	private Varcom varcom = new Varcom();
	private Bakykey wsaaBakykey = new Bakykey();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextBatch002,
		nextPtrn004,
		fatalError008,
		exit009
	}

	public Zptrnxt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		ptrnxtrec.ptrnxtRec = convertAndSetParam(ptrnxtrec.ptrnxtRec, parmArray, 0);
		try {
			zptrnxtSection();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void zptrnxtSection()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					getParams000();
				}
				case nextBatch002: {
					nextBatch002();
					firstPtrn003();
				}
				case nextPtrn004: {
					nextPtrn004();
				}
				case fatalError008: {
					fatalError008();
				}
				case exit009: {
					exit009();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void getParams000()
	{
		ptrnxtrec.statuz.set(varcom.oK);
		if (isNE(wsaaFirst,"Y")) {
			goTo(GotoLabel.nextPtrn004);
		}
		wsaaFirst = "N";
		bsscIO.setDataArea(lsaaBsscrec);
		bprdIO.setDataArea(lsaaBprdrec);
		wsccStparms.set(bprdIO.getSystemParam02());
		wsaaBakykey.set(SPACES);
		wsaaBakykey.bakyJctlpfx.set(smtpfxcpy.jctl);
		wsaaBakykey.bakyJctlcoy.set(bprdIO.getCompany());
		wsaaBakykey.bakyJctljn.set(bsscIO.getScheduleName());
		wsaaBakykey.bakyJctlactyr.set(bsscIO.getAcctYear());
		wsaaBakykey.bakyJctlactmn.set(bsscIO.getAcctMonth());
		wsaaBakykey.bakyJctljbn.set(bsscIO.getScheduleNumber());
		wsaaBakykey.bakyCurrprog.set(wsccProgram);
		wsaaBakykey.bakyJobstep.set(wsccJobStep);
		wsaaBakykey.bakyBatccoy.set(bprdIO.getCompany());
		wsaaBakykey.bakyBatcactyr.set(ZERO);
		wsaaBakykey.bakyBatcactmn.set(ZERO);
		bakyIO.setDataKey(wsaaBakykey.bakyKey);
		bakyIO.setFunction(varcom.begn);
		bakyIO.setFormat(bakyrec);
	}

protected void nextBatch002()
	{
		SmartFileCode.execute(appVars, bakyIO);
		if (isEQ(bakyIO.getStatuz(),varcom.mrnf)) {
			bakyIO.setStatuz(varcom.endp);
		}
		if (isEQ(bakyIO.getStatuz(),varcom.endp)) {
			ptrnxtrec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit009);
		}
		if (isNE(bakyIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(bakyIO.getStatuz());
			syserrrec.params.set(bakyIO.getParams());
			goTo(GotoLabel.fatalError008);
		}
		if (isNE(bakyIO.getJctlpfx(),wsaaBakykey.bakyJctlpfx)
		|| isNE(bakyIO.getJctlcoy(),wsaaBakykey.bakyJctlcoy)
		|| isNE(bakyIO.getJctljn(),wsaaBakykey.bakyJctljn)
		|| isNE(bakyIO.getJctlactyr(),wsaaBakykey.bakyJctlactyr)
		|| isNE(bakyIO.getJctlactmn(),wsaaBakykey.bakyJctlactmn)
		|| isNE(bakyIO.getJctljbn(),wsaaBakykey.bakyJctljbn)
		|| isNE(bakyIO.getCurrprog(),wsaaBakykey.bakyCurrprog)
		|| isNE(bakyIO.getJobstep(),wsaaBakykey.bakyJobstep)) {
			ptrnxtrec.statuz.set(varcom.endp);
			goTo(GotoLabel.exit009);
		}
	}

protected void firstPtrn003()
	{
		ptrnIO.setBatcpfx(bakyIO.getBatcpfx());
		ptrnIO.setBatccoy(bakyIO.getBatccoy());
		ptrnIO.setBatcbrn(bakyIO.getBatcbrn());
		ptrnIO.setBatcactyr(bakyIO.getBatcactyr());
		ptrnIO.setBatcactmn(bakyIO.getBatcactmn());
		ptrnIO.setBatctrcde(bakyIO.getBatctrcde());
		ptrnIO.setBatcbatch(bakyIO.getBatcbatch());
		ptrnIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ptrnIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
		ptrnIO.setFormat(ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isEQ(ptrnIO.getStatuz(),varcom.mrnf)) {
			ptrnIO.setStatuz(varcom.endp);
		}
		if (isEQ(ptrnIO.getStatuz(),varcom.endp)) {
			bakyIO.setFunction(varcom.nextr);
			goTo(GotoLabel.nextBatch002);
		}
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			goTo(GotoLabel.fatalError008);
		}
		if (isNE(ptrnIO.getBatcpfx(),bakyIO.getBatcpfx())
		|| isNE(ptrnIO.getBatccoy(),bakyIO.getBatccoy())
		|| isNE(ptrnIO.getBatcbrn(),bakyIO.getBatcbrn())
		|| isNE(ptrnIO.getBatcactyr(),bakyIO.getBatcactyr())
		|| isNE(ptrnIO.getBatcactmn(),bakyIO.getBatcactmn())
		|| isNE(ptrnIO.getBatctrcde(),bakyIO.getBatctrcde())
		|| isNE(ptrnIO.getBatcbatch(),bakyIO.getBatcbatch())) {
			bakyIO.setFunction(varcom.nextr);
			goTo(GotoLabel.nextBatch002);
		}
		ptrnxtrec.ptrnData.set(ptrnIO.getDataArea());
		goTo(GotoLabel.exit009);
	}

protected void nextPtrn004()
	{
		ptrnIO.setFunction(varcom.nextr);
		ptrnIO.setFormat(ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isEQ(ptrnIO.getStatuz(),varcom.mrnf)) {
			ptrnIO.setStatuz(varcom.endp);
		}
		if (isEQ(ptrnIO.getStatuz(),varcom.endp)) {
			bakyIO.setFunction(varcom.nextr);
			goTo(GotoLabel.nextBatch002);
		}
		if (isNE(ptrnIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(ptrnIO.getStatuz());
			syserrrec.params.set(ptrnIO.getParams());
			goTo(GotoLabel.fatalError008);
		}
		if (isNE(ptrnIO.getBatcpfx(),bakyIO.getBatcpfx())
		|| isNE(ptrnIO.getBatccoy(),bakyIO.getBatccoy())
		|| isNE(ptrnIO.getBatcbrn(),bakyIO.getBatcbrn())
		|| isNE(ptrnIO.getBatcactyr(),bakyIO.getBatcactyr())
		|| isNE(ptrnIO.getBatcactmn(),bakyIO.getBatcactmn())
		|| isNE(ptrnIO.getBatctrcde(),bakyIO.getBatctrcde())
		|| isNE(ptrnIO.getBatcbatch(),bakyIO.getBatcbatch())) {
			bakyIO.setFunction(varcom.nextr);
			goTo(GotoLabel.nextBatch002);
		}
		ptrnxtrec.ptrnData.set(ptrnIO.getDataArea());
		goTo(GotoLabel.exit009);
	}

protected void fatalError008()
	{
		if ((isNE(syserrrec.statuz,SPACES))
		|| (isNE(syserrrec.syserrStatuz,SPACES))) {
			syserrrec.syserrType.set("4");
		}
		else {
			syserrrec.syserrType.set("3");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void exit009()
	{
		exitProgram();
	}
}
