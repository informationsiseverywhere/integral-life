package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5074
 * @version 1.0 generated on 30/08/09 06:33
 * @author Quipoz
 */
public class S5074ScreenVars extends SmartVarModel { 


	//public FixedLengthStringData dataArea = new FixedLengthStringData(1043);
	//public FixedLengthStringData dataFields = new FixedLengthStringData(435).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(getDataAreaSize());
	public FixedLengthStringData dataFields = new FixedLengthStringData(getDataFieldsSize()).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData agentname = DD.agentname.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,47);
	public ZonedDecimalData billcd = DD.billcd.copyToZonedDecimal().isAPartOf(dataFields,57);
	public FixedLengthStringData billcurr = DD.billcurr.copy().isAPartOf(dataFields,65);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(dataFields,68);
	public FixedLengthStringData campaign = DD.campaign.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,86);
	public ZonedDecimalData cntfee = DD.cntfee.copyToZonedDecimal().isAPartOf(dataFields,89);
	public ZonedDecimalData cntsusp = DD.cntsusp.copyToZonedDecimal().isAPartOf(dataFields,106);
	public FixedLengthStringData conprosal = DD.conprosal.copy().isAPartOf(dataFields,123);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,124);
	public FixedLengthStringData hcurrcd = DD.hcurrcd.copy().isAPartOf(dataFields,154);
	public ZonedDecimalData instPrem = DD.instprm.copyToZonedDecimal().isAPartOf(dataFields,157);
	public ZonedDecimalData lassured = DD.lassured.copyToZonedDecimal().isAPartOf(dataFields,174);
	public FixedLengthStringData lrepnum = DD.lrepnum.copy().isAPartOf(dataFields,176);
	public FixedLengthStringData mop = DD.mop.copy().isAPartOf(dataFields,196);
	public ZonedDecimalData occdate = DD.occdate.copyToZonedDecimal().isAPartOf(dataFields,197);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,205);
	public FixedLengthStringData ownersel = DD.ownersel.copy().isAPartOf(dataFields,252);
	public ZonedDecimalData polinc = DD.polinc.copyToZonedDecimal().isAPartOf(dataFields,262);
	public ZonedDecimalData prmdepst = DD.prmdepst.copyToZonedDecimal().isAPartOf(dataFields,266);
	public FixedLengthStringData premStatDesc = DD.pstatdsc.copy().isAPartOf(dataFields,283);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,293);
	public FixedLengthStringData reptype = DD.reptype.copy().isAPartOf(dataFields,296);
	public FixedLengthStringData sbrdesc = DD.sbrdesc.copy().isAPartOf(dataFields,298);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(dataFields,338);
	public FixedLengthStringData statdsc = DD.statdsc.copy().isAPartOf(dataFields,340);
	public FixedLengthStringData stcal = DD.stcal.copy().isAPartOf(dataFields,350);
	public FixedLengthStringData stcbl = DD.stcbl.copy().isAPartOf(dataFields,353);
	public FixedLengthStringData stccl = DD.stccl.copy().isAPartOf(dataFields,356);
	public FixedLengthStringData stcdl = DD.stcdl.copy().isAPartOf(dataFields,359);
	public FixedLengthStringData stcel = DD.stcel.copy().isAPartOf(dataFields,362);
	public ZonedDecimalData taxamt = DD.taxamt.copyToZonedDecimal().isAPartOf(dataFields,365);
	public ZonedDecimalData tolerance = DD.toler.copyToZonedDecimal().isAPartOf(dataFields,382);
	public ZonedDecimalData totamnt = DD.totamnt.copyToZonedDecimal().isAPartOf(dataFields,399);
	public ZonedDecimalData totlprm = DD.totlprm.copyToZonedDecimal().isAPartOf(dataFields,417);
	
	public FixedLengthStringData concommflg = DD.concommflg.copy().isAPartOf(dataFields,435); //ILJ-44
	public ZonedDecimalData rskcommdate = DD.rskcommdate.copyToZonedDecimal().isAPartOf(dataFields,436);//ILJ-44
	public ZonedDecimalData decldate = DD.decldate.copyToZonedDecimal().isAPartOf(dataFields,444);//ILJ-44	
	public ZonedDecimalData premRcptDate = DD.premRcptDate.copyToZonedDecimal().isAPartOf(dataFields,452);//ILJ-44	
	
//	public FixedLengthStringData errorIndicators = new FixedLengthStringData(152).isAPartOf(dataArea, 435);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(getErrorIndicatorSize()).isAPartOf(dataArea, getDataFieldsSize());
	
	public FixedLengthStringData agentnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData billcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData billcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData billfreqErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData campaignErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cntfeeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cntsuspErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData conprosalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData hcurrcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData instprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData lassuredErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData lrepnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData mopErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData occdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData ownerselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData polincErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData prmdepstErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData pstatdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData reptypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData sbrdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData srcebusErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData statdscErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData stcalErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData stcblErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData stcclErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 124);
	public FixedLengthStringData stcdlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 128);
	public FixedLengthStringData stcelErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData taxamtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 136);
	public FixedLengthStringData tolerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 140);
	public FixedLengthStringData totamntErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 144);
	public FixedLengthStringData totlprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 148);
	//ILJ-44
	public FixedLengthStringData concommflgErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 152); 
	public FixedLengthStringData rskcommdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 156);
	public FixedLengthStringData decldateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 160);
	public FixedLengthStringData premRcptDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 164);
	//end
	
	//public FixedLengthStringData outputIndicators = new FixedLengthStringData(456).isAPartOf(dataArea, 587);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(getOutputFieldSize()).isAPartOf(dataArea, getDataFieldsSize()+getErrorIndicatorSize());
	
	public FixedLengthStringData[] agentnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] billcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] billcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] billfreqOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] campaignOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cntfeeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cntsuspOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] conprosalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] hcurrcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] instprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] lassuredOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] lrepnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] mopOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] occdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] ownerselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] polincOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] prmdepstOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] pstatdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] reptypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] sbrdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] srcebusOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] statdscOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] stcalOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] stcblOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public FixedLengthStringData[] stcclOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 372);
	public FixedLengthStringData[] stcdlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 384);
	public FixedLengthStringData[] stcelOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 396);
	public FixedLengthStringData[] taxamtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 408);
	public FixedLengthStringData[] tolerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 420);
	public FixedLengthStringData[] totamntOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 432);
	public FixedLengthStringData[] totlprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 444);
	//ILJ-44 start
	public FixedLengthStringData[] concommflgOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 456); 
	public FixedLengthStringData[] rskcommdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 468); 
	public FixedLengthStringData[] decldateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 480);
	public FixedLengthStringData[] premRcptDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 492);
	//end 
		/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData billcdDisp = new FixedLengthStringData(10);
	public FixedLengthStringData occdateDisp = new FixedLengthStringData(10);
	//ILJ-44
	public FixedLengthStringData rskcommdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData decldateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData premRcptDateDisp = new FixedLengthStringData(10);
	//end

	public LongData S5074screenWritten = new LongData(0);
	public LongData S5074protectWritten = new LongData(0);

	
	public static int[] screenSflPfInds = new int[] {1, 2, 3, 4, 5, 15, 16, 17, 18, 21, 22, 23, 24}; 
	
	
	public boolean hasSubfile() {
		return false;
	}


	public S5074ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(chdrnumOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(polincOut,new String[] {null, null, null, "48",null, null, null, null, null, null, null, null});
		fieldIndMap.put(hcurrcdOut,new String[] {null, null, null, "10",null, null, null, null, null, null, null, null});
		fieldIndMap.put(totlprmOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(totamntOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(taxamtOut,new String[] {null, null, null, "03",null, null, null, null, null, null, null, null});
		fieldIndMap.put(occdateOut,new String[] {null,null, null,"18", null, null, null, null, null, null, null, null});//ILJ-49
		//ILJ-44
		fieldIndMap.put(concommflgOut,new String[] {null ,null, null ,"122" , null, null, null, null, null, null, null, null}); //ILJ-40
		fieldIndMap.put(rskcommdateOut,new String[] {null ,"125", null ,"123" , null, null, null, null, null, null, null, null}); //ILJ-40
		fieldIndMap.put(decldateOut,new String[] {null ,null, null ,"124" , null, null, null, null, null, null, null, null}); //ILJ-40
		fieldIndMap.put(premRcptDateOut,new String[] {null ,null, null ,"121" , null, null, null, null, null, null, null, null}); 
		//end
		/*screenFields = new BaseData[] {chdrnum, ctypedes, statdsc, premStatDesc, ownersel, ownername, occdate, lassured, billfreq, conprosal, mop, billcd, polinc, cntcurr, billcurr, register, srcebus, reptype, lrepnum, cntbranch, sbrdesc, agntsel, agentname, campaign, stcal, stcbl, stccl, stcdl, stcel, hcurrcd, instPrem, cntsusp, cntfee, tolerance, totlprm, totamnt, prmdepst, taxamt};
		screenOutFields = new BaseData[][] {chdrnumOut, ctypedesOut, statdscOut, pstatdscOut, ownerselOut, ownernameOut, occdateOut, lassuredOut, billfreqOut, conprosalOut, mopOut, billcdOut, polincOut, cntcurrOut, billcurrOut, registerOut, srcebusOut, reptypeOut, lrepnumOut, cntbranchOut, sbrdescOut, agntselOut, agentnameOut, campaignOut, stcalOut, stcblOut, stcclOut, stcdlOut, stcelOut, hcurrcdOut, instprmOut, cntsuspOut, cntfeeOut, tolerOut, totlprmOut, totamntOut, prmdepstOut, taxamtOut};
		screenErrFields = new BaseData[] {chdrnumErr, ctypedesErr, statdscErr, pstatdscErr, ownerselErr, ownernameErr, occdateErr, lassuredErr, billfreqErr, conprosalErr, mopErr, billcdErr, polincErr, cntcurrErr, billcurrErr, registerErr, srcebusErr, reptypeErr, lrepnumErr, cntbranchErr, sbrdescErr, agntselErr, agentnameErr, campaignErr, stcalErr, stcblErr, stcclErr, stcdlErr, stcelErr, hcurrcdErr, instprmErr, cntsuspErr, cntfeeErr, tolerErr, totlprmErr, totamntErr, prmdepstErr, taxamtErr};
		screenDateFields = new BaseData[] {occdate, billcd};
		screenDateErrFields = new BaseData[] {occdateErr, billcdErr};
		screenDateDispFields = new BaseData[] {occdateDisp, billcdDisp};*/

		screenFields = getscreenFields();
		screenOutFields = getscreenOutFields();
		screenErrFields = getscreenErrFields();
		screenDateFields = getscreenDateFields();
		screenDateErrFields = getscreenDateErrFields();
		screenDateDispFields = getscreenDateDispFields();
		
		
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5074screen.class;
		protectRecord = S5074protect.class;
	}
	public static int[] getScreenSflPfInds(){
		return screenSflPfInds;
	}
	
	
	public int getDataAreaSize() {
		return 1132;
	}
	

	public int getDataFieldsSize(){
		return 460;  
	}
	public int getErrorIndicatorSize(){
		return 168; 
	}
	
	public int getOutputFieldSize(){
		return 504; 
	}


	public BaseData[] getscreenFields(){
		return new BaseData[] {chdrnum, ctypedes, statdsc, premStatDesc, ownersel, ownername, occdate, lassured, billfreq, conprosal, mop, billcd, polinc, cntcurr, billcurr, register, srcebus, reptype, lrepnum, cntbranch, sbrdesc, agntsel, agentname, campaign, stcal, stcbl, stccl, stcdl, stcel, hcurrcd, instPrem, cntsusp, cntfee, tolerance, totlprm, totamnt, prmdepst, taxamt, concommflg, rskcommdate, decldate, premRcptDate};   //IFSU-2036
	}
	
	public BaseData[][] getscreenOutFields(){
		return new BaseData[][] {chdrnumOut, ctypedesOut, statdscOut, pstatdscOut, ownerselOut, ownernameOut, occdateOut, lassuredOut, billfreqOut, conprosalOut, mopOut, billcdOut, polincOut, cntcurrOut, billcurrOut, registerOut, srcebusOut, reptypeOut, lrepnumOut, cntbranchOut, sbrdescOut, agntselOut, agentnameOut, campaignOut, stcalOut, stcblOut, stcclOut, stcdlOut, stcelOut, hcurrcdOut, instprmOut, cntsuspOut, cntfeeOut, tolerOut, totlprmOut, totamntOut, prmdepstOut, taxamtOut, concommflgOut, rskcommdateOut, decldateOut, premRcptDateOut}; //IFSU-2036
	}
	
	public BaseData[] getscreenErrFields(){
		return new BaseData[] {chdrnumErr, ctypedesErr, statdscErr, pstatdscErr, ownerselErr, ownernameErr, occdateErr, lassuredErr, billfreqErr, conprosalErr, mopErr, billcdErr, polincErr, cntcurrErr, billcurrErr, registerErr, srcebusErr, reptypeErr, lrepnumErr, cntbranchErr, sbrdescErr, agntselErr, agentnameErr, campaignErr, stcalErr, stcblErr, stcclErr, stcdlErr, stcelErr, hcurrcdErr, instprmErr, cntsuspErr, cntfeeErr, tolerErr, totlprmErr, totamntErr, prmdepstErr, taxamtErr, concommflgErr, rskcommdateErr, decldateErr, premRcptDateErr};  //IFSU-2036
		
	}
	
	public BaseData[] getscreenDateFields(){
		return new BaseData[] {occdate, billcd, rskcommdate, decldate, premRcptDate};
	}
	

	public BaseData[] getscreenDateDispFields(){
		return new BaseData[] {occdateDisp, billcdDisp, rskcommdateDisp, decldateDisp, premRcptDateDisp};
	}
	
	
	public BaseData[] getscreenDateErrFields(){
		return new BaseData[] {occdateErr, billcdErr, rskcommdateErr, decldateErr, premRcptDateErr};
	}
}
