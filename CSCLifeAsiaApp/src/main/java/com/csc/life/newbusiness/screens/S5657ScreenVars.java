package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5657
 * @version 1.0 generated on 30/08/09 06:47
 * @author Quipoz
 */
public class S5657ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(204);
	public FixedLengthStringData dataFields = new FixedLengthStringData(60).isAPartOf(dataArea, 0);
	public ZonedDecimalData agerate = DD.agerate.copyToZonedDecimal().isAPartOf(dataFields,0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,3);
	public ZonedDecimalData insprm = DD.insprm.copyToZonedDecimal().isAPartOf(dataFields,4);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,18);
	public ZonedDecimalData oppc = DD.oppc.copyToZonedDecimal().isAPartOf(dataFields,48);
	public FixedLengthStringData sbstdl = DD.sbstdl.copy().isAPartOf(dataFields,53);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,54);
	public FixedLengthStringData tabtype = DD.tabtype.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 60);
	public FixedLengthStringData agerateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData insprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData oppcErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData sbstdlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData tabtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 96);
	public FixedLengthStringData[] agerateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] insprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] oppcOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] sbstdlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] tabtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5657screenWritten = new LongData(0);
	public LongData S5657protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5657ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, agerate, oppc, insprm, sbstdl, tabtype};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, agerateOut, oppcOut, insprmOut, sbstdlOut, tabtypeOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, agerateErr, oppcErr, insprmErr, sbstdlErr, tabtypeErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5657screen.class;
		protectRecord = S5657protect.class;
	}

}
