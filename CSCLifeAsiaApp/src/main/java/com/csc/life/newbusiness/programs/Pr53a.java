/*
 * File: Pr53a.java
 * Date: December 3, 2013 3:28:27 AM ICT
 * Author: CSC
 * 
 * Class transformed from PR53A.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.accounting.dataaccess.CcdtTableDAM;
import com.csc.fsu.clients.dataaccess.BabrTableDAM;
import com.csc.fsu.clients.dataaccess.ClblTableDAM;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.ClrrTableDAM;
import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.fsu.general.dataaccess.MandccdTableDAM;
import com.csc.fsu.general.procedures.Usrcypind;
import com.csc.fsu.general.recordstructures.Cipherrec;
import com.csc.fsu.general.recordstructures.Usrcypirec;
import com.csc.fsu.general.tablestructures.T3678rec;
import com.csc.fsu.general.tablestructures.T3684rec;
import com.csc.fsu.general.tablestructures.Tr29urec;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.enquiries.dataaccess.ChdrenqTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.screens.Sr53aScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.CreditCardUtility;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.MaskingUtil;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*
*
* CREDIT CARD MANDATE DETAILS.
*
*
* Partial window displaying the bank code, bank description,
* account number and account description detailing the
* client's account to be debited.
*
* Retrieves the contract header details from the contract header
* file (CHDRLNB).    Using RETRV.
*
* Hidden screen fields are used to hold the payer number, the
* contract currency and the factoring house.
*
* The bank code and account details are retrieved from the
* Client Bank Account Details (CLBL) file. If the client for
* the bank details is not the same as the 'hidden' payer then
* the payer has changed so the bank details are thus not
* applicable.
*
* The bank code and account descriptions are retrieved from
* the BABR file.
*
* The CLIENT NUMBER must always be the same as the hidden
* PAYER.
*
* If 'CALC' is pressed then the screen is redisplayed.
*
* If 'KILL' is pressed then all validation is ignored and the
* client header record is left as it was on entry to this
* program.
*
* Windowing on the account field allows new bank details to
* be created (automatically). This is controlled externally to
* this program - the details being returned to this program
* automatically in the hidden fields in the screen.
*
*
****************************************************************** ****
* </pre>
*/
public class Pr53a extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PR53A");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaClntkey = new FixedLengthStringData(12).init(SPACES);
	private final String wsaaLargeName = "LGNMS";

	private FixedLengthStringData wsbbBankdesc = new FixedLengthStringData(60);
	private FixedLengthStringData wsbbBankdescLine1 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 0);
	private FixedLengthStringData wsbbBankdescLine2 = new FixedLengthStringData(30).isAPartOf(wsbbBankdesc, 30);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);

	private FixedLengthStringData wsaaBillchgFlg = new FixedLengthStringData(1).init(SPACES);
	private Validator billingChange = new Validator(wsaaBillchgFlg, "Y");

	private FixedLengthStringData wsaaMandateFlag = new FixedLengthStringData(1);
	private Validator mandateFound = new Validator(wsaaMandateFlag, "Y");
	private FixedLengthStringData wsaaSaveChdrnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaValidCcStatus = new FixedLengthStringData(2);
	private Validator validCcStatus = new Validator(wsaaValidCcStatus, "IF");
	private static final String wsaaPayer = "PY";
	private static final String wsaaBillchnl = "R";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaDate = new FixedLengthStringData(8).isAPartOf(wsaaToday, 0, REDEFINE);
	private ZonedDecimalData wsaaYyyy = new ZonedDecimalData(4, 0).isAPartOf(wsaaDate, 0).setUnsigned();
	private ZonedDecimalData wsaaMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaDate, 4).setUnsigned();

	private FixedLengthStringData wsaaTr29uErr = new FixedLengthStringData(1).init("N");
	private Validator tr29uErr = new Validator(wsaaTr29uErr, "Y");
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
		/* TABLES */
	private static final String t3678 = "T3678";
	private static final String t3684 = "T3684";
	private static final String tr29u = "TR29U";
	private BabrTableDAM babrIO = new BabrTableDAM();
	private CcdtTableDAM ccdtIO = new CcdtTableDAM();
	private ChdrenqTableDAM chdrenqIO = new ChdrenqTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private ClblTableDAM clblIO = new ClblTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private ClrrTableDAM clrrIO = new ClrrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MandccdTableDAM mandccdIO = new MandccdTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private T3678rec t3678rec = new T3678rec();
	private T3684rec t3684rec = new T3684rec();
	private Tr29urec tr29urec = new Tr29urec();
	private Cipherrec cipherrec = new Cipherrec();
	private Usrcypirec usrcypirec = new Usrcypirec();
	private Namadrsrec namadrsrec = new Namadrsrec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Wssplife wssplife = new Wssplife();
	private Sr53aScreenVars sv = ScreenProgram.getScreenVars( Sr53aScreenVars.class);
	private ErrorsInner errorsInner = new ErrorsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private Batckey wsaaBatckey = new Batckey();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090
	}

	public Pr53a() {
		super();
		screenVars = sv;
		new ScreenModel("Sr53a", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspcomn.bchaction.set(SPACES);
		sv.dataArea.set(SPACES);
		wsaaSaveChdrnum.set(SPACES);
		datcon1rec.function.set("TDAY");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		/*    Retrieve contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
		&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		if (isEQ(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			wsaaBillchgFlg.set("Y");
			chdrmjaIO.setFormat(formatsInner.chdrmjarec);
			chdrmjaIO.setFunction(varcom.retrv);
			SmartFileCode.execute(appVars, chdrmjaIO);
			if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(chdrmjaIO.getStatuz());
				syserrrec.params.set(chdrmjaIO.getParams());
				fatalError600();
			}
			chdrlnbIO.setParams(SPACES);
			chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
			chdrlnbIO.setFunction(varcom.readr);
			chdrlnbIO.setDataKey(chdrmjaIO.getDataKey());
			SmartFileCode.execute(appVars, chdrlnbIO);
			if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
				syserrrec.statuz.set(chdrlnbIO.getStatuz());
				syserrrec.params.set(chdrlnbIO.getParams());
				fatalError600();
			}
		}
		wsaaSaveChdrnum.set(chdrlnbIO.getChdrnum());
		/* Check if the Encryption/Decryption routine is setup in          */
		/* the TR29U Table. If not, no further encryption/decryption       */
		/* processing is needed                                            */
		/*itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(tr29u);
		itemIO.setItemitem(wsspcomn.fsuco);
		itemIO.setItempfx("IT");
		itemIO.setFunction("READR");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		tr29urec.tr29uRec.set(itemIO.getGenarea());*/
		/*    Retrieve the payer details.*/
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/*    Read the client role file to get the payer number.*/
		clrfIO.setForepfx(chdrlnbIO.getChdrpfx());
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFormat(formatsInner.clrfrec);
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		/* Set up hidden fields.*/
		/* If the payor number is spaces use the owner number.*/
		sv.payrnum.set(clrfIO.getClntnum());
		sv.numsel.set(clrfIO.getClntnum());
		a1000GetPayorname();
		sv.payorname.set(namadrsrec.name);
		wsaaClntkey.set(SPACES);
		if (isNE(clrfIO.getClntnum(), SPACES)) {
			wsaaClntkey.set(wsspcomn.clntkey);
			wsspcomn.clntkey.set(SPACES);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clrfIO.getClntpfx());
			stringVariable1.addExpression(clrfIO.getClntcoy());
			stringVariable1.addExpression(clrfIO.getClntnum());
			stringVariable1.setStringInto(wsspcomn.clntkey);
		}
		sv.currcode.set(SPACES);
		sv.facthous.set(SPACES);
		sv.longdesc.set(SPACES);
		sv.billcd.set(payrIO.getBillcd());
		/* First time in (create bank details) or*/
		/* not first time in (modify bank details)*/
		if (isNE(payrIO.getMandref(), SPACES)) {
			if (isEQ(payrIO.getBillchnl(), "R")) {
				sv.ccmndref.set(payrIO.getMandref());
			}
			else {
				sv.ccmndref.set(SPACES);
				return ;
			}
		}
		/* Instead of using CHDRLNB-BANKKEY and CHDRLNB-BANKACCKEY*/
		/* to read CLBL, we will use the information obtained from*/
		/* MANDPF*/
		if (isEQ(payrIO.getMandref(), SPACES)) {
			return ;
		}
		/* Get client bank account details from MANDPF*/
		mandccdIO.setParams(SPACES);
		mandccdIO.setPayrcoy(wsspcomn.fsuco);
		mandccdIO.setPayrnum(sv.payrnum);
		if (billingChange.isTrue()) {
			mandccdIO.setMandref(chdrmjaIO.getMandref());
		}
		else {
			mandccdIO.setMandref(chdrlnbIO.getMandref());
		}
		mandccdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandccdIO);
		if (isNE(mandccdIO.getStatuz(), varcom.oK)
		&& isNE(mandccdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(mandccdIO.getParams());
			syserrrec.statuz.set(mandccdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(mandccdIO.getStatuz(), varcom.mrnf)) {
			sv.ccmndrefErr.set(errorsInner.h929);
			return ;
		}
		/* Read CLBL using information obtained from MAND                  */
		clblIO.setParams(SPACES);
		clblIO.setBankkey(mandccdIO.getBankkey());
		sv.bankkey.set(mandccdIO.getBankkey());
		String creditCardInformation=CreditCardUtility.getInstance().getCreditCard(mandccdIO.getBankacckey().toString().trim());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			mandccdIO.setBankacckey(creditCardInformation);
		}
		
		clblIO.setBankacckey(mandccdIO.getBankacckey());
		
		sv.bankacckey.set(mandccdIO.getBankacckey());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			sv.bankacckey.set(creditCardInformation);
		}
		sv.effdate.set(mandccdIO.getEffdate());
		sv.mandstat.set(mandccdIO.getMandstat());
		//ILIFE-2823 Starts
		ccdtIO.setParams(SPACES);
		ccdtIO.setFormat(formatsInner.ccdtrec);
		ccdtIO.setCrdtcard(mandccdIO.getBankacckey());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			ccdtIO.setCrdtcard(creditCardInformation);
		}
		ccdtIO.setCrcardexpy("9999");
		ccdtIO.setCrcardexpm("99");
		ccdtIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ccdtIO);
		if (isNE(ccdtIO.getStatuz(), varcom.oK)
		&& isNE(ccdtIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ccdtIO.getStatuz());
			syserrrec.params.set(ccdtIO.getParams());
			fatalError600();
		}
		/* If invalid Credit Card status issue an error*/
		wsaaValidCcStatus.set(ccdtIO.getCrcstat());
		descIO.setParams(SPACES);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		descIO.setDesctabl("TR338");
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(ccdtIO.getBnkactyp());
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.cdlongdesc.set(descIO.getLongdesc());
		}
		else {
			descIO.setLanguage("E");
			SmartFileCode.execute(appVars, descIO);
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.cdlongdesc.set(descIO.getLongdesc());
			}
			else {
				sv.cdlongdesc.fill(" ");
			}
		}
		//ILIFE-2823 Ends
		a1400Decryption();
		clblIO.setClntcoy(wsspcomn.fsuco);
		if (isEQ(clrfIO.getClntnum(), SPACES)) {
			clblIO.setClntnum(chdrlnbIO.getCownnum());
		}
		else {
			clblIO.setClntnum(clrfIO.getClntnum());
		}
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(clblIO.getParams());
			syserrrec.statuz.set(clblIO.getStatuz());
			fatalError600();
		}
		/* If the payor number is not the same as the client number*/
		if (isEQ(clrfIO.getClntnum(), SPACES)) {
			if (isNE(chdrlnbIO.getCownnum(), clblIO.getClntnum())) {
				sv.payrnumErr.set(errorsInner.f373);
				return ;
			}
		}
		else {
			if (isNE(clrfIO.getClntnum(), clblIO.getClntnum())) {
				sv.payrnumErr.set(errorsInner.f373);
				return ;
			}
		}
		/* If the client number is not the same as the payor number*/
		/* then remove the contents of the hidden fields-facthous,*/
		/* and leave the input fields blank.*/
		if (isNE(clblIO.getClntnum(), sv.payrnum)) {
			sv.facthous.set(SPACES);
			sv.longdesc.set(SPACES);
			sv.bankkey.set(SPACES);
			sv.bankacckey.set(SPACES);
			return ;
		}
		/* Get the bank/branch description.*/
		babrIO.setParams(SPACES);
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(), varcom.oK)
		&& isNE(babrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			syserrrec.statuz.set(babrIO.getStatuz());
			fatalError600();
		}
		/*    Set screen fields*/
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
//		a1400Decryption();
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		sv.facthous.set(clblIO.getFacthous());
		if (isNE(sv.facthous, SPACES)) {
			a1200GetFacthousDesc();
		}
		if (billingChange.isTrue()) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(t3678);
			itemIO.setItemitem(mandccdIO.getMandstat());
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			t3678rec.t3678Rec.set(itemIO.getGenarea());
			if (isEQ(t3678rec.gonogoflg, "N")) {
				sv.ccmndrefErr.set(errorsInner.i014);
				return ;
			}
		}
		/* If the current to date is less than today, the record is        */
		/* inactive                                                        */
		if (isNE(clblIO.getCurrto(), ZERO)) {
			if (isLT(clblIO.getCurrto(), wsaaToday)) {
				sv.bankacckeyErr.set(errorsInner.rfjx);
				return ;
			}
		}
		/* If the current from date is greater than the effective date     */
		/* of the mandate                                                  */
		if (billingChange.isTrue()) {
			if (isGT(clblIO.getCurrfrom(), mandccdIO.getEffdate())) {
				sv.bankacckeyErr.set(errorsInner.rfkg);
				return ;
			}
		}
		/* If the current to date of is less than the effective date       */
		/* of the mandate                                                  */
		if (billingChange.isTrue()) {
			if (isNE(clblIO.getCurrto(), ZERO)) {
				if (isLT(clblIO.getCurrto(), mandccdIO.getEffdate())) {
					sv.bankacckeyErr.set(errorsInner.rfkg);
					return ;
				}
			}
		}
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/* If in enquiry mode, protect field*/
		if (isEQ(wsspcomn.flag, "I")) {
			sv.ccmndrefOut[varcom.pr.toInt()].set("Y");
			scrnparams.function.set(varcom.prot); //ILIFE-2472
		}
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					screenIo2010();
					validate2020();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/*    If F12 pressed bypass validation.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/*    If F5 pressed then set a srceen error so the screen*/
		/*    is re-displayed.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/* To ensure that a mandate reference number is entered*/
		if (isEQ(sv.ccmndref, SPACES)) {
			sv.ccmndrefErr.set(errorsInner.h926);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Is mandate reference number entered numeric ?*/
		if (isNE(sv.ccmndref, SPACES)
		&& isNE(sv.ccmndref, NUMERIC)) {
			sv.ccmndrefErr.set(errorsInner.h421);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Read mandate file*/
		mandccdIO.setParams(SPACES);
		mandccdIO.setPayrcoy(wsspcomn.fsuco);
		mandccdIO.setPayrnum(sv.payrnum);
		mandccdIO.setMandref(sv.ccmndref);
		mandccdIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, mandccdIO);
		if (isNE(mandccdIO.getStatuz(), varcom.oK)
		&& isNE(mandccdIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(mandccdIO.getParams());
			syserrrec.statuz.set(mandccdIO.getStatuz());
			fatalError600();
		}
		if (isEQ(mandccdIO.getStatuz(), varcom.mrnf) 
				|| isEQ(mandccdIO.getPayind(), "P")) { //ILIFE-2472
			sv.ccmndrefErr.set(errorsInner.h928);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/* Move bank account details to corresponding fields*/
		sv.bankkey.set(mandccdIO.getBankkey());
		sv.bankacckey.set(mandccdIO.getBankacckey());
		String creditCardInformation=CreditCardUtility.getInstance().getCreditCard(sv.bankacckey.toString().trim());
		if(creditCardInformation!=null && !creditCardInformation.isEmpty()){
			sv.bankacckey.set(creditCardInformation);
		}
		sv.effdate.set(mandccdIO.getEffdate());
		sv.mandstat.set(mandccdIO.getMandstat());
		a1400Decryption();
	}

protected void validate2020()
	{
		/* Read T3678 and check the Go/No Go flag for billing change part. */
//		if (billingChange.isTrue()) { //MIBT-362
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(wsspcomn.fsuco);
			itemIO.setItemtabl(t3678);
			itemIO.setItemitem(mandccdIO.getMandstat());
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
			t3678rec.t3678Rec.set(itemIO.getGenarea());
			if (isEQ(t3678rec.gonogoflg, "N")) {
				sv.ccmndrefErr.set(errorsInner.i014);
				goTo(GotoLabel.checkForErrors2080);
			}
//		}
		/* Get the bank and branch descriptions in case*/
		/* the mandate has changed*/
		/* Check the bank/branch.*/
		babrIO.setParams(SPACES);
		babrIO.setBankkey(sv.bankkey);
		babrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, babrIO);
		if (isNE(babrIO.getStatuz(), varcom.oK)
		&& isNE(babrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(babrIO.getParams());
			syserrrec.statuz.set(babrIO.getStatuz());
			fatalError600();
		}
		/* Bank/Branch description not found.*/
		if (isEQ(babrIO.getStatuz(), varcom.mrnf)) {
			sv.bankkeyErr.set(errorsInner.e756);
			goTo(GotoLabel.checkForErrors2080);
		}
		/* Move the bank key description to the screen.*/
		wsbbBankdesc.set(babrIO.getBankdesc());
		sv.bankdesc.set(wsbbBankdescLine1);
		sv.branchdesc.set(wsbbBankdescLine2);
		/* Get the client number.*/
		clblIO.setParams(SPACES);
		clblIO.setBankkey(sv.bankkey);
		clblIO.setBankacckey(sv.bankacckey);
	
		ix.set(20);
		while ( !(isNE(subString(clblIO.getBankacckey(), ix, 1), SPACES))) {
			compute(ix, 0).set(sub(ix, 1));
		}
		
		/*if (isNE(tr29urec.subrname, SPACES)
		&& isEQ(subString(clblIO.getBankacckey(), 1, ix), NUMERIC)) {
			cipherrec.lfunc.set("ENCR");
			cipherrec.lsrc.set(clblIO.getBankacckey());
			a1600CallTr29uSubrname();
			clblIO.setBankacckey(cipherrec.lrcv);
		}*/
		clblIO.setClntcoy(wsspcomn.fsuco);
		clblIO.setClntnum(sv.payrnum);
		clblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clblIO);
		if (isNE(clblIO.getStatuz(), varcom.oK)
		&& isNE(clblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(clblIO.getParams());
			syserrrec.statuz.set(clblIO.getStatuz());
			fatalError600();
		}
		/* Client bank details not found.*/
		if (isEQ(clblIO.getStatuz(), varcom.mrnf)) {
			sv.bankacckeyErr.set(errorsInner.g600);
			goTo(GotoLabel.checkForErrors2080);
		}
		/* If the client number is not the same as the payor number.*/
		if (isNE(clblIO.getClntnum(), sv.payrnum)) {
			sv.bankacckeyErr.set(errorsInner.f373);
			goTo(GotoLabel.checkForErrors2080);
		}
		/* If the current to date is less than today, the record is*/
		/* inactive*/
		if (isNE(clblIO.getCurrto(), ZERO)) {
			if (isLT(clblIO.getCurrto(), wsaaToday)) {
				sv.bankacckeyErr.set(errorsInner.rfjx);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		/* If the current from date is greater than the effective date*/
		/* of the mandate*/
		if (billingChange.isTrue()) {
			if (isGT(clblIO.getCurrfrom(), mandccdIO.getEffdate())) {
				sv.bankacckeyErr.set(errorsInner.rfkg);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		/* If the current to date of is less than the effective date*/
		/* of the mandate*/
		if (billingChange.isTrue()) {
			if (isNE(clblIO.getCurrto(), ZERO)) {
				if (isLT(clblIO.getCurrto(), mandccdIO.getEffdate())) {
					sv.bankacckeyErr.set(errorsInner.rfkg);
					goTo(GotoLabel.checkForErrors2080);
				}
			}
		}
		sv.bankaccdsc.set(clblIO.getBankaccdsc());
		sv.facthous.set(clblIO.getFacthous());
		if (isNE(sv.facthous, SPACES)) {
			a1200GetFacthousDesc();
		}
		/*  An indicator has been included in T3684 (Factoring House)*/
		/*  extra data screen to allow user to indicate if the same*/
		/*  mandate reference can be used by more than 1 policy or*/
		/*  it has to be unique per policy.  Program to incorporate*/
		/*  this change.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(wsspcomn.fsuco);
		itemIO.setItemtabl(t3684);
		itemIO.setItemitem(sv.facthous);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t3684rec.t3684Rec.set(itemIO.getGenarea());
		/*  T3684-MANDOPT 'S' indicates one mandate one policy*/
		/*  T3684-MANDOPT 'M' indicates one mandate many policy*/
		if (isEQ(t3684rec.mandopt, "S")) {
			clrrIO.setParams(SPACES);
			clrrIO.setFunction(varcom.begn);
			clrrIO.setFormat(formatsInner.clrrrec);
			clrrIO.setClntcoy(wsspcomn.fsuco);
			clrrIO.setClntpfx("CN");
			clrrIO.setClntnum(sv.payrnum);
			clrrIO.setClrrrole(wsaaPayer);
			wsaaMandateFlag.set("N");
			while ( !(isEQ(clrrIO.getStatuz(), varcom.endp)
			|| mandateFound.isTrue())) {
				usedMandateChk2200();
			}
			
			if (mandateFound.isTrue()) {
				sv.ccmndrefErr.set(errorsInner.rpfh);
				goTo(GotoLabel.checkForErrors2080);
			}
		}
		/* Read CCDT to check Credit Card Status*/
		ccdtIO.setParams(SPACES);
		ccdtIO.setFormat(formatsInner.ccdtrec);
		ccdtIO.setCrdtcard(mandccdIO.getBankacckey());
		
		ccdtIO.setCrcardexpy("9999");
		ccdtIO.setCrcardexpm("99");
		ccdtIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, ccdtIO);
		if (isNE(ccdtIO.getStatuz(), varcom.oK)
		&& isNE(ccdtIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ccdtIO.getStatuz());
			syserrrec.params.set(ccdtIO.getParams());
			fatalError600();
		}
		/* If invalid Credit Card status issue an error*/
		wsaaValidCcStatus.set(ccdtIO.getCrcstat());
		//ILIFE-2823 Starts
		descIO.setParams(SPACES);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		descIO.setDesctabl("TR338");
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setLanguage(wsspcomn.language);
		descIO.setDescitem(ccdtIO.getBnkactyp());
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.cdlongdesc.set(descIO.getLongdesc());
		}
		else {
			descIO.setLanguage("E");
			SmartFileCode.execute(appVars, descIO);
			if (isEQ(descIO.getStatuz(), varcom.oK)) {
				sv.cdlongdesc.set(descIO.getLongdesc());
			}
			else {
				sv.cdlongdesc.fill(" ");
			}
		}
		//ILIFE-2823 Ends
		if (!validCcStatus.isTrue()) {
			sv.bankacckeyErr.set(errorsInner.rfh1);
		}
		/* Check if the credit card is expired.*/
		if (isLT(ccdtIO.getCrcardexpy(), wsaaYyyy)) {
			sv.bankacckeyErr.set(errorsInner.rpez);
		}
		if (isEQ(ccdtIO.getCrcardexpy(), wsaaYyyy)) {
			if (isLT(ccdtIO.getCrcardexpm(), wsaaMm)) {
				sv.bankacckeyErr.set(errorsInner.rpez);
			}
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void usedMandateChk2200()
	{
		para2200();
	}

protected void para2200()
	{
		SmartFileCode.execute(appVars, clrrIO);
		if (isNE(clrrIO.getStatuz(), varcom.oK)
		&& isNE(clrrIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(clrrIO.getStatuz());
			syserrrec.params.set(clrrIO.getParams());
			fatalError600();
		}
		if (isNE(clrrIO.getStatuz(), varcom.oK)
		|| isNE(clrrIO.getClntpfx(), "CN")
		|| isNE(clrrIO.getClntcoy(), wsspcomn.fsuco)
		|| isNE(clrrIO.getClntnum(), sv.payrnum)
		|| isNE(clrrIO.getClrrrole(), wsaaPayer)) {
			clrrIO.setStatuz(varcom.endp);
			return ;
		}
		if (isEQ(clrrIO.getStatuz(), varcom.oK)
		&& isEQ(clrrIO.getClntpfx(), "CN")
		&& isEQ(clrrIO.getClntcoy(), wsspcomn.fsuco)
		&& isEQ(clrrIO.getClntnum(), sv.payrnum)
		&& isEQ(clrrIO.getClrrrole(), wsaaPayer)) {
			chdrenqIO.setParams(SPACES);
			chdrenqIO.setFunction(varcom.readr);
			chdrenqIO.setFormat(formatsInner.chdrenqrec);
			chdrenqIO.setChdrcoy(clrrIO.getForecoy());
			chdrenqIO.setChdrnum(subString(clrrIO.getForenum(), 1, 8));
			SmartFileCode.execute(appVars, chdrenqIO);
			if (isNE(chdrenqIO.getStatuz(), varcom.oK)
			&& isNE(chdrenqIO.getStatuz(), varcom.mrnf)) {
				syserrrec.statuz.set(chdrenqIO.getStatuz());
				syserrrec.params.set(chdrenqIO.getParams());
				fatalError600();
			}
			if (isEQ(chdrenqIO.getStatuz(), varcom.oK)
			&& isEQ(chdrenqIO.getMandref(), sv.ccmndref)
			&& isNE(chdrenqIO.getChdrnum(), wsaaSaveChdrnum)) {
				wsaaMandateFlag.set("Y");
			}
			else {
				clrrIO.setFunction(varcom.nextr);
			}
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		updateDatabase3010();
	}

protected void updateDatabase3010()
	{
		/*  Update database files as required / WSSP*/
		wsspcomn.clntkey.set(wsaaClntkey);
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		if (billingChange.isTrue()) {
			billChange3100();
			return ;
		}
		/* Update the contract header record using KEEPS.*/
		chdrlnbIO.setMandref(sv.ccmndref);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			fatalError600();
		}
		/* Release the payr I/O module.*/
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			syserrrec.statuz.set(payrIO.getStatuz());
			fatalError600();
		}
		/* Update the mandate reference no. on the payer record.*/
		wsaaBatckey.set(wsspcomn.batchkey);
		if(isNE(wsaaBatckey.batcBatctrcde.toString(),"TAFP") || isNE(wsaaBatckey.batcBatctrcde.toString(),"TAFQ")
		|| isNE(wsaaBatckey.batcBatctrcde.toString(),"TAFR")){			
			payrIO.setMandref(sv.ccmndref);
			payrIO.setFunction(varcom.updat);
			SmartFileCode.execute(appVars, payrIO);
			if (isNE(payrIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(payrIO.getParams());
				syserrrec.statuz.set(payrIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void billChange3100()
	{
		para3100();
	}

protected void para3100()
	{
		/*  If called from Billing Change, the updated mandate details*/
		/*  are retrieved from PAYR and CHDRMJA via RETRV. Therefore,*/
		/*  we need to perform a KEEPS on these so that the changes*/
		/*  are available.*/
	
		chdrmjaIO.setParams(SPACES);
		chdrmjaIO.setFunction(varcom.retrv);
		chdrmjaIO.setFormat(formatsInner.chdrmjarec);
		chdrmjaIO.setDataKey(chdrlnbIO.getDataKey());
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		
		chdrmjaIO.setMandref(sv.ccmndref);
		chdrmjaIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrmjaIO.getStatuz());
			syserrrec.params.set(chdrmjaIO.getParams());
			fatalError600();
		}
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		payrIO.setMandref(sv.ccmndref);
		payrIO.setBillchnl(wsaaBillchnl);
		payrIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void a1000GetPayorname()
	{
		a1010Namadrs();
	}

protected void a1010Namadrs()
	{
		initialize(namadrsrec.namadrsRec);
		namadrsrec.clntPrefix.set(fsupfxcpy.clnt);
		namadrsrec.clntCompany.set(wsspcomn.fsuco);
		namadrsrec.clntNumber.set(sv.payrnum);
		namadrsrec.language.set(wsspcomn.language);
		namadrsrec.function.set(wsaaLargeName);
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			syserrrec.params.set(namadrsrec.namadrsRec);
			fatalError600();
		}
	}

protected void a1200GetFacthousDesc()
	{
		a1210Desc();
	}

protected void a1210Desc()
	{
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsspcomn.fsuco);
		descIO.setDesctabl(t3684);
		descIO.setDescitem(sv.facthous);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFormat(formatsInner.descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			syserrrec.statuz.set(descIO.getStatuz());
			fatalError600();
		}
		sv.longdesc.set(descIO.getLongdesc());
	}

protected void a1400Decryption()
	{
		a1400Start();
	}

protected void a1400Start()
	{
		/*   Calculate the length of credit card number                    */
		ix.set(20);
		while ( !(isNE(subString(sv.bankacckey, ix, 1), SPACES))) {
			compute(ix, 0).set(sub(ix, 1));
		}
		
		/*   Propmt error when trying yo display the encrypted credit      */
		/*   card number while TR29U-SUBRNAME is set to spaces.            
		if (isEQ(tr29urec.subrname, SPACES)
		&& isNE(subString(sv.bankacckey, 1, ix), NUMERIC)) {
			tr29uErr.setTrue();
			sv.bankacckey.set(SPACES);
			return ;
		}*/
		/* Do credit card number decryption before display                 */
		/* No need to do decryption for not encrypted credit card          */
		/* number data although TR29U-SUBRNAME is not set to SPACES       
		if (isNE(tr29urec.subrname, SPACES)
		&& isNE(subString(sv.bankacckey, 1, ix), NUMERIC)) {
			cipherrec.lfunc.set("DECR");
			cipherrec.lsrc.set(sv.bankacckey);
			a1600CallTr29uSubrname();
			sv.bankacckey.set(cipherrec.lrcv);
		} */
		/*   Determine if user can view the full value of credit card      */
		/*   number when in enquiry mode                                   */
		if (isEQ(wsspcomn.flag, "I")
		/*&& isNE(tr29urec.subrname, SPACES)*/) {
			usrcypirec.rec.set(SPACES);
			usrcypirec.func.set("CHCK");
			usrcypirec.userid.set(wsspcomn.userid);
			callProgram(Usrcypind.class, usrcypirec.rec);
			if (!(isEQ(usrcypirec.indic, "Y")
			&& isEQ(usrcypirec.statuz, "****"))) {
				/*cipherrec.lfunc.set("MASK");
				cipherrec.lsrc.set(sv.bankacckey);
				a1600CallTr29uSubrname();
				sv.bankacckey.set(cipherrec.lrcv);*/
				if(!StringUtil.isEmptyOrSpace(sv.bankacckey.toString()))
                {
                      String masked = MaskingUtil.getInstance().maskString(sv.bankacckey.trim(), "************xxxx", 'x', '*', false);/* IJTI-1523 */
                      sv.bankacckey.set(masked);  
                }
			}
		}
	}

protected void a1600CallTr29uSubrname()
	{
		/*A1610-START*/
		/*cipherrec.lrcv.set(SPACES);
		cipherrec.lsts.set(SPACES);
		callProgram(tr29urec.subrname, cipherrec.lfunc, cipherrec.lsrc, cipherrec.lrcv, cipherrec.lsts);
		if (isNE(cipherrec.lsts, varcom.oK)) {
			syserrrec.statuz.set(cipherrec.lsts);
			syserrrec.params.set(cipherrec.lfunc);
			fatalError600();
		}*/
		/*A1690-EXIT*/
	}
/*
 * Class transformed  from Data Structure ERRORS--INNER
 */
private static final class ErrorsInner { 
		/* ERRORS */
	private FixedLengthStringData e756 = new FixedLengthStringData(4).init("E756");
	private FixedLengthStringData f373 = new FixedLengthStringData(4).init("F373");
	private FixedLengthStringData g600 = new FixedLengthStringData(4).init("G600");
	private FixedLengthStringData h421 = new FixedLengthStringData(4).init("H421");
	private FixedLengthStringData h926 = new FixedLengthStringData(4).init("H926");
	private FixedLengthStringData h928 = new FixedLengthStringData(4).init("H928");
	private FixedLengthStringData h929 = new FixedLengthStringData(4).init("H929");
	private FixedLengthStringData i014 = new FixedLengthStringData(4).init("I014");
	private FixedLengthStringData rfh1 = new FixedLengthStringData(4).init("RFH1");
	private FixedLengthStringData rfjx = new FixedLengthStringData(4).init("RFJX");
	private FixedLengthStringData rfkg = new FixedLengthStringData(4).init("RFKG");
	private FixedLengthStringData rpez = new FixedLengthStringData(4).init("RPEZ");
	private FixedLengthStringData rpfh = new FixedLengthStringData(4).init("RPFH");
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData ccdtrec = new FixedLengthStringData(10).init("CCDTREC");
	private FixedLengthStringData clrfrec = new FixedLengthStringData(10).init("CLRFREC");
	private FixedLengthStringData clrrrec = new FixedLengthStringData(10).init("CLRRREC");
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData chdrenqrec = new FixedLengthStringData(10).init("CHDRENQREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).init("CHDRMJAREC");
}
}
