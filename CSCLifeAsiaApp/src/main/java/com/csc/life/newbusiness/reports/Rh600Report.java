package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH600.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh600Report extends SMARTReportLayout { 

	private ZonedDecimalData adblims01 = new ZonedDecimalData(11, 2);
	private ZonedDecimalData adblims02 = new ZonedDecimalData(11, 2);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private FixedLengthStringData dummy = new FixedLengthStringData(1);
	private FixedLengthStringData forsdesc = new FixedLengthStringData(10);
	private ZonedDecimalData hpolcnt = new ZonedDecimalData(5, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totcont = new ZonedDecimalData(10, 0);
	private ZonedDecimalData year = new ZonedDecimalData(4, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh600Report() {
		super();
	}


	/**
	 * Print the XML for Rh600d01
	 */
	public void printRh600d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(18).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		hpolcnt.setFieldName("hpolcnt");
		hpolcnt.setInternal(subString(recordData, 1, 5));
		totcont.setFieldName("totcont");
		totcont.setInternal(subString(recordData, 6, 10));
		adblims01.setFieldName("adblims01");
		adblims01.setInternal(subString(recordData, 16, 11));
		adblims02.setFieldName("adblims02");
		adblims02.setInternal(subString(recordData, 27, 11));
		printLayout("Rh600d01"+indicArea.locate("1"),			// Record name
			new BaseData[]{			// Fields:
				hpolcnt,
				totcont,
				adblims01,
				adblims02
			}
			, new Object[] {			// indicators
				new Object[]{"ind01", indicArea.charAt(1)},
				new Object[]{"ind02", indicArea.charAt(2)},
				new Object[]{"ind03", indicArea.charAt(3)},
				new Object[]{"ind04", indicArea.charAt(4)},
				new Object[]{"ind05", indicArea.charAt(5)},
				new Object[]{"ind06", indicArea.charAt(6)},
				new Object[]{"ind07", indicArea.charAt(7)},
				new Object[]{"ind08", indicArea.charAt(8)},
				new Object[]{"ind09", indicArea.charAt(9)},
				new Object[]{"ind10", indicArea.charAt(10)},
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind12", indicArea.charAt(12)},
				new Object[]{"ind13", indicArea.charAt(13)},
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind15", indicArea.charAt(15)},
				new Object[]{"ind16", indicArea.charAt(16)},
				new Object[]{"ind17", indicArea.charAt(17)},
				new Object[]{"ind18", indicArea.charAt(18)}
			}
		);

	}

	/**
	 * Print the XML for Rh600e01
	 */
	public void printRh600e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		dummy.setFieldName("dummy");
		dummy.setInternal(subString(recordData, 1, 1));
		printLayout("Rh600e01",			// Record name
			new BaseData[]{			// Fields:
				dummy
			}
		);

	}

	/**
	 * Print the XML for Rh600h01
	 */
	public void printRh600h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		forsdesc.setFieldName("forsdesc");
		forsdesc.setInternal(subString(recordData, 32, 10));
		year.setFieldName("year");
		year.setInternal(subString(recordData, 42, 4));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 46, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 56, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 58, 30));
		time.setFieldName("time");
		time.set(getTime());
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 88, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 91, 30));
		cnttype.setFieldName("cnttype");
		cnttype.setInternal(subString(recordData, 121, 3));
		cntdesc.setFieldName("cntdesc");
		cntdesc.setInternal(subString(recordData, 124, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rh600h01",			// Record name
			new BaseData[]{			// Fields:
				company,
				companynm,
				forsdesc,
				year,
				sdate,
				branch,
				branchnm,
				time,
				currcode,
				currencynm,
				cnttype,
				cntdesc,
				pagnbr
			}
		);

		currentPrintLine.set(10);
	}

	/**
	 * Print the XML for Rh600h02
	 */
	public void printRh600h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh600h02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rh600u01
	 */
	public void printRh600u01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh600u01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}


}
