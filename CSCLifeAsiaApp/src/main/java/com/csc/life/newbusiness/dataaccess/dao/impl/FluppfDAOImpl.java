package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.common.DD; //ILB-486
import com.csc.life.newbusiness.dataaccess.dao.FluppfDAO;
import com.csc.life.newbusiness.dataaccess.model.Fluppf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.COBOLFramework.util.StringUtil; //ILB-486
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class FluppfDAOImpl extends BaseDAOImpl<Fluppf> implements FluppfDAO{

	private static final Logger LOGGER = LoggerFactory.getLogger(FluppfDAOImpl.class);

	public List<Fluppf> searchFlupRecordByChdrNum(String chdrcoy, String chdrNum) throws SQLRuntimeException{
		
		StringBuilder sqlFlupSelect = new StringBuilder();
		
		sqlFlupSelect.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, FUPNO, TRANNO, FUPTYP, LIFE, JLIFE, ");
		sqlFlupSelect.append("FUPCDE, FUPSTS, FUPDT, FUPRMK, CLAMNUM, USER_T, TERMID, TRDT, TRTM, ");
		sqlFlupSelect.append("ZAUTOIND, USRPRF, JOBNM, DATIME, EFFDATE, CRTUSER, CRTDATE, ");
		sqlFlupSelect.append("LSTUPUSER, ZLSTUPDT, FUPRCVD, EXPRDATE ");
		sqlFlupSelect.append("FROM FLUPPF WHERE CHDRCOY = ? AND CHDRNUM = ? ");
		sqlFlupSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, FUPNO ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psFlupSelect = getPrepareStatement(sqlFlupSelect.toString());
		ResultSet sqlFlupRs = null;
		List<Fluppf> flupSearchResult = new ArrayList<Fluppf>();
		try {
			
			psFlupSelect.setString(1, chdrcoy);
			psFlupSelect.setString(2, chdrNum);
			
			sqlFlupRs = executeQuery(psFlupSelect);

			while (sqlFlupRs.next()) {
				
				Fluppf fluppf = new Fluppf();
				
				fluppf.setUniqueNumber(sqlFlupRs.getInt("UNIQUE_NUMBER"));
				fluppf.setChdrcoy(sqlFlupRs.getString("CHDRCOY")==null? ' ' : sqlFlupRs.getString("CHDRCOY").charAt(0));//IJTI-1410
				fluppf.setChdrnum(sqlFlupRs.getString("CHDRNUM"));
				fluppf.setFupNo(sqlFlupRs.getInt("FUPNO"));
				fluppf.setTranNo(sqlFlupRs.getInt("TRANNO"));
				fluppf.setFupTyp(sqlFlupRs.getString("FUPTYP")==null? ' ' : sqlFlupRs.getString("FUPTYP").charAt(0));//IJTI-1410
				fluppf.setLife(sqlFlupRs.getString("LIFE"));
				fluppf.setjLife(sqlFlupRs.getString("JLIFE"));
				fluppf.setFupCde(sqlFlupRs.getString("FUPCDE"));
				fluppf.setFupSts(sqlFlupRs.getString("FUPSTS")==null? ' ' : sqlFlupRs.getString("FUPSTS").charAt(0));//IJTI-1410
				fluppf.setFupDt(sqlFlupRs.getInt("FUPDT"));
				fluppf.setFupRmk(sqlFlupRs.getString("FUPRMK"));
				fluppf.setClamNum(sqlFlupRs.getString("CLAMNUM"));
				fluppf.setUserT(sqlFlupRs.getInt("USER_T"));
				fluppf.setTermId(sqlFlupRs.getString("TERMID"));
				fluppf.setTrdt(sqlFlupRs.getInt("TRDT"));
				fluppf.setTrtm(sqlFlupRs.getInt("TRTM"));
				fluppf.setzAutoInd(sqlFlupRs.getString("ZAUTOIND")==null? ' ' : sqlFlupRs.getString("ZAUTOIND").charAt(0));//IJTI-1410
				fluppf.setUsrPrf(sqlFlupRs.getString("USRPRF"));
				fluppf.setJobNm(sqlFlupRs.getString("JOBNM"));
				fluppf.setDatime(sqlFlupRs.getDate("DATIME"));
				fluppf.setEffDate(sqlFlupRs.getInt("EFFDATE"));
				fluppf.setCrtUser(sqlFlupRs.getString("CRTUSER"));
				fluppf.setCrtDate(sqlFlupRs.getInt("CRTDATE"));
				fluppf.setLstUpUser(sqlFlupRs.getString("LSTUPUSER"));
				fluppf.setzLstUpDt(sqlFlupRs.getInt("ZLSTUPDT"));
				fluppf.setFuprcvd(sqlFlupRs.getInt("FUPRCVD"));
				fluppf.setExprDate(sqlFlupRs.getInt("EXPRDATE"));
				
				flupSearchResult.add(fluppf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchFlupRecordByChdrNum()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psFlupSelect, sqlFlupRs);
		}
		return flupSearchResult;
	}

public Fluppf readFupsts(String chdrcoy, String chdrNum) throws SQLRuntimeException{
		
		StringBuilder sqlFlupSelect = new StringBuilder();
		
		sqlFlupSelect.append("SELECT CHDRCOY, CHDRNUM, FUPCDE, FUPSTS ");
		sqlFlupSelect.append("FROM FLUPPF WHERE CHDRCOY = ? AND CHDRNUM = ?");
		

		PreparedStatement psFlupSelect = getPrepareStatement(sqlFlupSelect.toString());
		ResultSet sqlFlupRs = null;
		Fluppf fluppf = new Fluppf();
		try {
			
			psFlupSelect.setString(1, chdrcoy);
			psFlupSelect.setString(2, chdrNum);
			
			sqlFlupRs = executeQuery(psFlupSelect);

			while (sqlFlupRs.next()) {
			
				fluppf.setChdrcoy(sqlFlupRs.getString("CHDRCOY")==null? ' ' : sqlFlupRs.getString("CHDRCOY").charAt(0));//IJTI-1410
				fluppf.setChdrnum(sqlFlupRs.getString("CHDRNUM"));
				fluppf.setFupTyp(sqlFlupRs.getString("FUPTYP")==null? ' ' : sqlFlupRs.getString("FUPTYP").charAt(0));//IJTI-1410
				fluppf.setFupCde(sqlFlupRs.getString("FUPCDE"));
				fluppf.setFupSts(sqlFlupRs.getString("FUPSTS")==null? ' ' : sqlFlupRs.getString("FUPSTS").charAt(0));//IJTI-1410
			} 
		}catch (SQLException e) {
				LOGGER.error("searchFlupRecordByChdrNum()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psFlupSelect, sqlFlupRs);
			}
			return fluppf;
		
}	

public LinkedHashSet<String> getFollowUps(String clntnum,String clntcoy ) throws SQLRuntimeException{
	
	StringBuilder sqlFlupSelect = new StringBuilder();
	
	sqlFlupSelect.append("SELECT * FROM FLUPPF ");
	sqlFlupSelect.append("WHERE CHDRNUM IN (SELECT CHDRNUM FROM CHDRPF WHERE COWNNUM = ? and COWNCOY = ?) ORDER BY CHDRNUM DESC ");
	
	
	PreparedStatement psFlupSelect = getPrepareStatement(sqlFlupSelect.toString());
	ResultSet sqlFlupRs = null;

	LinkedHashSet<String> fluppfList = new LinkedHashSet<String>();
	try {
	
		psFlupSelect = getPrepareStatement(sqlFlupSelect.toString());
		psFlupSelect.setString(1, clntnum);
		psFlupSelect.setString(2, clntcoy);
		
		sqlFlupRs = executeQuery(psFlupSelect);

		while (sqlFlupRs.next()) {
			String fupsts = sqlFlupRs.getString("FUPSTS") ;
			fluppfList.add(fupsts);
		} 
	}catch (SQLException e) {
			LOGGER.error("searchFlupRecordByChdrNum()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psFlupSelect, sqlFlupRs);
		}
		return fluppfList;
	
}	
public void insertFlupRecord(Fluppf fluppf) {
	StringBuilder sb = new StringBuilder("");
	sb.append("INSERT INTO FLUPPF(CHDRCOY,CHDRNUM,FUPNO,TRANNO,FUPTYP,LIFE,JLIFE,FUPCDE,FUPSTS,FUPDT,FUPRMK,CLAMNUM,USER_T,TERMID,TRDT,TRTM,ZAUTOIND,USRPRF,JOBNM,"
			+ "DATIME,"
			+ "EFFDATE,CRTUSER,CRTDATE,LSTUPUSER,ZLSTUPDT,FUPRCVD,EXPRDATE) ");
	sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
	LOGGER.info(sb.toString());
	PreparedStatement ps = null;
	ResultSet rs = null;
	try{
		ps = getPrepareStatement(sb.toString());
		//ps.setLong(1,fluppf.getUniqueNumber());
		ps.setString(1,fluppf.getChdrcoy().toString());
        ps.setString(2,fluppf.getChdrnum());
        ps.setInt(3,fluppf.getFupNo());
		ps.setInt(4,fluppf.getTranNo());
        ps.setString(5,fluppf.getFupTyp().toString());
        ps.setString(6,fluppf.getLife());
		ps.setString(7,fluppf.getjLife());
        ps.setString(8,fluppf.getFupCde());
        ps.setString(9,fluppf.getFupSts().toString());
        ps.setInt(10,fluppf.getFupDt());
        ps.setString(11,fluppf.getFupRmk());
        ps.setString(12,fluppf.getClamNum());
		ps.setInt(13, fluppf.getUserT());
		ps.setString(14, fluppf.getTermId());
		ps.setInt(15, fluppf.getTrdt());
		ps.setInt(16, fluppf.getTrtm());
		ps.setString(17, (fluppf.getzAutoInd()==null? "" : fluppf.getzAutoInd().toString()));
        ps.setString(18, this.getUsrprf());
        ps.setString(19,this.getJobnm());
        ps.setTimestamp(20, new Timestamp(System.currentTimeMillis()));
        ps.setInt(21, fluppf.getEffDate());
        ps.setString(22, (fluppf.getCrtUser()==null? "" :fluppf.getCrtUser()));
        ps.setInt(23, fluppf.getCrtDate());
        ps.setString(24, (fluppf.getLstUpUser()==null? "" :fluppf.getLstUpUser() ));
        ps.setInt(25, fluppf.getzLstUpDt());
        ps.setInt(26, fluppf.getFuprcvd());
        ps.setInt(27, fluppf.getExprDate());    
     
    	ps.executeUpdate();	
      }
	    catch (SQLException e) {
		LOGGER.error("insertClftRecord()", e);//IJTI-1561
		throw new SQLRuntimeException(e);
	} finally {
		close(ps, rs);			
	}		
	return;
}
	public void deleteFluppfRecord(List<Fluppf> fluprevIOList){
			String sql = "DELETE FROM FLUPPF WHERE UNIQUE_NUMBER=? ";
			PreparedStatement ps = getPrepareStatement(sql);
			try {
				for(Fluppf fluppf:fluprevIOList){
					ps.setLong(1, fluppf.getUniqueNumber());
					ps.addBatch();
				}
				ps.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("deleteFluppfRecord()", e);//IJTI-1561
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
	}
	public void deleteRecordbyCode(Fluppf fluppf){
		String sql = "DELETE FROM FLUPPF WHERE CHDRNUM=? AND FUPCDE= ? ";
		PreparedStatement ps = getPrepareStatement(sql);
      	ResultSet rs=null; 
	        try {
	        	 ps.setString(1,fluppf.getChdrnum().trim());
	      		 ps.setString(2,fluppf.getFupCde().trim());
	      		 ps.execute();
	          
	        } catch (SQLException e) {
	            LOGGER.error("delete peoutPf()", e);
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
}
	public void deleteRecord(String chdrNum, String fupcode, String beneficiary){
	
		String sql = "DELETE FROM FLUPPF WHERE CHDRNUM=? AND FUPCDE= ? AND FUPRMK  like '%"+ beneficiary.trim()+"%' ";
		
		PreparedStatement ps = getPrepareStatement(sql);
      	ResultSet rs=null; 
	        try {
	        	 ps.setString(1,chdrNum);
	      		 ps.setString(2,fupcode);
	      		 ps.execute();
	          
	        } catch (SQLException e) {
	            LOGGER.error("deleteRecord()", e);
	            throw new SQLRuntimeException(e);
	        } finally {
	            close(ps, rs);
	        }
}
public List<Fluppf> searchFlupRecord(String chdrcoy, String chdrNum, String Clamnum) throws SQLRuntimeException{
		
		StringBuilder sqlFlupSelect = new StringBuilder();
		
		sqlFlupSelect.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, FUPNO, TRANNO, FUPTYP, LIFE, JLIFE, ");
		sqlFlupSelect.append("FUPCDE, FUPSTS, FUPDT, FUPRMK, CLAMNUM, USER_T, TERMID, TRDT, TRTM, ");
		sqlFlupSelect.append("ZAUTOIND, USRPRF, JOBNM, DATIME, EFFDATE, CRTUSER, CRTDATE, ");
		sqlFlupSelect.append("LSTUPUSER, ZLSTUPDT, FUPRCVD, EXPRDATE ");
		sqlFlupSelect.append("FROM FLUPPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND CLAMNUM=? ");
		sqlFlupSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, FUPNO ASC, UNIQUE_NUMBER DESC ");

		PreparedStatement psFlupSelect = getPrepareStatement(sqlFlupSelect.toString());
		ResultSet sqlFlupRs = null;
		List<Fluppf> flupSearchResult = new ArrayList<Fluppf>();
		try {
			
			psFlupSelect.setString(1, chdrcoy);
			psFlupSelect.setString(2, chdrNum);
			psFlupSelect.setString(3, Clamnum);
			sqlFlupRs = executeQuery(psFlupSelect);

			while (sqlFlupRs.next()) {
				
				Fluppf fluppf = new Fluppf();
				
				fluppf.setUniqueNumber(sqlFlupRs.getInt("UNIQUE_NUMBER"));
				fluppf.setChdrcoy(sqlFlupRs.getString("CHDRCOY")==null? ' ' : sqlFlupRs.getString("CHDRCOY").charAt(0));//IJTI-1410
				fluppf.setChdrnum(sqlFlupRs.getString("CHDRNUM"));
				fluppf.setFupNo(sqlFlupRs.getInt("FUPNO"));
				fluppf.setTranNo(sqlFlupRs.getInt("TRANNO"));
				fluppf.setFupTyp(sqlFlupRs.getString("FUPTYP")==null? ' ' : sqlFlupRs.getString("FUPTYP").charAt(0));//IJTI-1410
				fluppf.setLife(sqlFlupRs.getString("LIFE"));
				fluppf.setjLife(sqlFlupRs.getString("JLIFE"));
				fluppf.setFupCde(sqlFlupRs.getString("FUPCDE"));
				fluppf.setFupSts(sqlFlupRs.getString("FUPSTS")==null? ' ' : sqlFlupRs.getString("FUPSTS").charAt(0));//IJTI-1410
				fluppf.setFupDt(sqlFlupRs.getInt("FUPDT"));
				fluppf.setFupRmk(sqlFlupRs.getString("FUPRMK"));
				fluppf.setClamNum(sqlFlupRs.getString("CLAMNUM"));
				fluppf.setUserT(sqlFlupRs.getInt("USER_T"));
				fluppf.setTermId(sqlFlupRs.getString("TERMID"));
				fluppf.setTrdt(sqlFlupRs.getInt("TRDT"));
				fluppf.setTrtm(sqlFlupRs.getInt("TRTM"));
				fluppf.setzAutoInd(sqlFlupRs.getString("ZAUTOIND")==null? ' ' : sqlFlupRs.getString("ZAUTOIND").charAt(0));//IJTI-1410
				fluppf.setUsrPrf(sqlFlupRs.getString("USRPRF"));
				fluppf.setJobNm(sqlFlupRs.getString("JOBNM"));
				fluppf.setDatime(sqlFlupRs.getDate("DATIME"));
				fluppf.setEffDate(sqlFlupRs.getInt("EFFDATE"));
				fluppf.setCrtUser(sqlFlupRs.getString("CRTUSER"));
				fluppf.setCrtDate(sqlFlupRs.getInt("CRTDATE"));
				fluppf.setLstUpUser(sqlFlupRs.getString("LSTUPUSER"));
				fluppf.setzLstUpDt(sqlFlupRs.getInt("ZLSTUPDT"));
				fluppf.setFuprcvd(sqlFlupRs.getInt("FUPRCVD"));
				fluppf.setExprDate(sqlFlupRs.getInt("EXPRDATE"));
				
				flupSearchResult.add(fluppf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchFlupRecordByChdrNum()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psFlupSelect, sqlFlupRs);
		}
		return flupSearchResult;
	}

public Map<String, List<Fluppf>> searchFlupRecord(String chdrcoy, List<String> chdrList) {
	StringBuilder sb = new StringBuilder("");
	sb.append("SELECT * FROM FLUPPF ");
	sb.append("WHERE CHDRCOY=? AND ");
	sb.append(getSqlInStr("CHDRNUM", chdrList));
	sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, FUPNO ASC, UNIQUE_NUMBER DESC ");
	PreparedStatement ps = getPrepareStatement(sb.toString());
	ResultSet rs = null;
	Map<String, List<Fluppf>> fluppfMap = new HashMap<String, List<Fluppf>>();		
	try {
		ps.setString(1, chdrcoy);
		rs = ps.executeQuery();
		while (rs.next()) {
			Fluppf fluppf = new Fluppf();

			fluppf.setUniqueNumber(rs.getInt("UNIQUE_NUMBER"));
			fluppf.setChdrcoy(rs.getString("CHDRCOY").charAt(0));
			fluppf.setChdrnum(rs.getString("CHDRNUM"));
			fluppf.setFupNo(rs.getInt("FUPNO"));
			fluppf.setTranNo(rs.getInt("TRANNO"));
			fluppf.setFupTyp(rs.getString("FUPTYP")==null? ' ' : rs.getString("FUPTYP").charAt(0));//IJTI-1410
			fluppf.setLife(rs.getString("LIFE"));
			fluppf.setjLife(rs.getString("JLIFE"));
			fluppf.setFupCde(rs.getString("FUPCDE"));
			fluppf.setFupSts(rs.getString("FUPSTS")==null? ' ' : rs.getString("FUPSTS").charAt(0));//IJTI-1410
			fluppf.setFupDt(rs.getInt("FUPDT"));
			fluppf.setFupRmk(rs.getString("FUPRMK"));
			fluppf.setClamNum(rs.getString("CLAMNUM"));
			fluppf.setUserT(rs.getInt("USER_T"));
			fluppf.setTermId(rs.getString("TERMID"));
			fluppf.setTrdt(rs.getInt("TRDT"));
			fluppf.setTrtm(rs.getInt("TRTM"));
			fluppf.setzAutoInd(rs.getString("ZAUTOIND")==null? ' ' : rs.getString("ZAUTOIND").charAt(0));//IJTI-1410
			fluppf.setUsrPrf(rs.getString("USRPRF"));
			fluppf.setJobNm(rs.getString("JOBNM"));
			fluppf.setDatime(rs.getDate("DATIME"));
			fluppf.setEffDate(rs.getInt("EFFDATE"));
			fluppf.setCrtUser(rs.getString("CRTUSER"));
			fluppf.setCrtDate(rs.getInt("CRTDATE"));
			fluppf.setLstUpUser(rs.getString("LSTUPUSER"));
			fluppf.setzLstUpDt(rs.getInt("ZLSTUPDT"));
			fluppf.setFuprcvd(rs.getInt("FUPRCVD"));
			fluppf.setExprDate(rs.getInt("EXPRDATE"));  

			if (fluppfMap.containsKey(fluppf.getChdrnum())) {
				fluppfMap.get(fluppf.getChdrnum()).add(fluppf);
			} else {
				List<Fluppf> flupList = new ArrayList<Fluppf>();
				flupList.add(fluppf);
				fluppfMap.put(fluppf.getChdrnum(), flupList);
			}				

		}
	}catch (SQLException e) {
		LOGGER.error("searchflupRecord()", e);//IJTI-1561
		throw new SQLRuntimeException(e);
	} finally {
		close(ps, rs);
	}
	return fluppfMap;		

}
	public boolean checkFluppfRecordByChdrnum(String chdrcoy, String chdrNum) throws SQLRuntimeException{
		String sqlFlupSelect = "SELECT count(*) FROM FLUPCLM WHERE 1=1  and CHDRCOY=? and CHDRNUM=? ";					
		
		PreparedStatement psFlupSelect = getPrepareStatement(sqlFlupSelect);
		ResultSet sqlFlupRs = null;
		Fluppf fluppf = null;
		try {
			psFlupSelect.setString(1, chdrcoy);
			psFlupSelect.setString(2, chdrNum);
			sqlFlupRs = executeQuery(psFlupSelect);
			sqlFlupRs.next();
			if(sqlFlupRs.getInt(1) > 0) {
				return true;
			}
			return false;
		} catch (SQLException e) {
			LOGGER.error("searchFlupRecordByChdrNum()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psFlupSelect, sqlFlupRs);
		}
	}
	public void insertFlupRecord(List<Fluppf> fluplnbList) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO FLUPPF(CHDRCOY,CHDRNUM,FUPNO,TRANNO,FUPTYP,LIFE,JLIFE,FUPCDE,FUPSTS,FUPDT,FUPRMK,CLAMNUM,USER_T,TERMID,TRDT,TRTM,ZAUTOIND,USRPRF,JOBNM,"
				+ "DATIME,"
				+ "EFFDATE,CRTUSER,CRTDATE,LSTUPUSER,ZLSTUPDT,FUPRCVD,EXPRDATE) ");
		sb.append("VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		LOGGER.info(sb.toString());
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			for (Fluppf fluppf : fluplnbList) {
				ps = getPrepareStatement(sb.toString());
				// ps.setLong(1,fluppf.getUniqueNumber());
				ps.setString(1, (fluppf.getChdrcoy() == null ? "" : fluppf.getChdrcoy().toString()));
				ps.setString(2, fluppf.getChdrnum());
				ps.setInt(3, fluppf.getFupNo());
				ps.setInt(4, fluppf.getTranNo());
				ps.setString(5, (fluppf.getFupTyp() == null ? "" : fluppf.getFupTyp().toString()));
				ps.setString(6, fluppf.getLife());
				ps.setString(7, fluppf.getjLife());
				ps.setString(8, fluppf.getFupCde().trim());
				ps.setString(9, (fluppf.getFupSts() == null ? "" : fluppf.getFupSts().toString()));
				ps.setInt(10, fluppf.getFupDt());
				ps.setString(11, fluppf.getFupRmk());
				ps.setString(12, fluppf.getClamNum());
				ps.setInt(13, fluppf.getUserT());
				ps.setString(14, fluppf.getTermId());
				ps.setInt(15, fluppf.getTrdt());
				ps.setInt(16, fluppf.getTrtm());
				ps.setString(17, (fluppf.getzAutoInd() == null ? "" : fluppf.getzAutoInd().toString()));
				ps.setString(18, this.getUsrprf());
				ps.setString(19, this.getJobnm());
				ps.setTimestamp(20, new Timestamp(System.currentTimeMillis()));
				ps.setInt(21, fluppf.getEffDate());
				ps.setString(22,(fluppf.getCrtUser() == null ? "" : fluppf.getCrtUser()));
				ps.setInt(23, fluppf.getCrtDate());
				ps.setString(24,(fluppf.getLstUpUser() == null ? "" : fluppf.getLstUpUser()));
				ps.setInt(25, fluppf.getzLstUpDt());
				ps.setInt(26, fluppf.getFuprcvd());
				ps.setInt(27, fluppf.getExprDate());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertClftRecord()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return;
	}
	public List<Fluppf> getFluplnbRecordForPerformance(String chdrcoy, String chdrnum) throws SQLRuntimeException{
		StringBuilder sqlFlupSelect = new StringBuilder();
	//	sqlFlupSelect.append("SELECT CHDRCOY,LTRIM(RTRIM(CHDRNUM)) FROM FLUPLNB WHERE CHDRCOY = ? AND CHDRNUM = ?"); //ILB-486
		sqlFlupSelect.append("SELECT CHDRCOY,CHDRNUM FROM FLUPLNB WHERE CHDRCOY = ? AND CHDRNUM = ?"); //ILB-486
		PreparedStatement psFlupSelect = null;
		ResultSet sqlFlupRs = null;
		Fluppf fluppf = null;
		List<Fluppf> fluppfList = new ArrayList<Fluppf>();
		try {
		    
			psFlupSelect = getPrepareStatement(sqlFlupSelect.toString());
			psFlupSelect.setString(1, chdrcoy);
		//	psFlupSelect.setString(2, chdrnum); //ILB-486
			psFlupSelect.setString(2, StringUtil.fillSpace(chdrnum == null ? "":  chdrnum.trim(), DD.chdrnum.length)); //ILB-486
			sqlFlupRs = psFlupSelect.executeQuery();
			while (sqlFlupRs.next()) {
				fluppf = new Fluppf();
				fluppf.setChdrcoy(sqlFlupRs.getString(1)==null? ' ' : sqlFlupRs.getString(1).charAt(0));//IJTI-1410
				fluppf.setChdrnum(sqlFlupRs.getString(2) );
				fluppfList.add(fluppf);
			} 
		}catch (SQLException e) {
				LOGGER.error("getFluplnbRecordForPerformance()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(psFlupSelect, sqlFlupRs);
			}
			return fluppfList;
	}
	//ILJ-108 Starts
	public List<Fluppf> getFluplnbRecordsForLife(String chdrcoy, String chdrnum, String life)throws SQLRuntimeException{

		StringBuilder sqlFlupSelect = new StringBuilder();
				
				sqlFlupSelect.append("SELECT UNIQUE_NUMBER, CHDRCOY, CHDRNUM, FUPNO, TRANNO, FUPTYP, LIFE, JLIFE, ");
				sqlFlupSelect.append("FUPCDE, FUPSTS, FUPDT, FUPRMK, CLAMNUM, USER_T, TERMID, TRDT, TRTM, ");
				sqlFlupSelect.append("ZAUTOIND, USRPRF, JOBNM, DATIME, EFFDATE, CRTUSER, CRTDATE, ");
				sqlFlupSelect.append("LSTUPUSER, ZLSTUPDT, FUPRCVD, EXPRDATE ");
				sqlFlupSelect.append("FROM FLUPPF WHERE CHDRCOY = ? AND CHDRNUM = ? AND LIFE = ? ");
				sqlFlupSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, FUPNO ASC, UNIQUE_NUMBER DESC ");

				PreparedStatement psFlupSelect = getPrepareStatement(sqlFlupSelect.toString());
				ResultSet sqlFlupRs = null;
				List<Fluppf> flupSearchResult = new ArrayList<>();
				try {
					
					psFlupSelect.setString(1, chdrcoy);
					psFlupSelect.setString(2, chdrnum);
					psFlupSelect.setString(3, life);
					
					sqlFlupRs = executeQuery(psFlupSelect);

					while (sqlFlupRs.next()) {
						
						Fluppf fluppf = new Fluppf();
						
						fluppf.setUniqueNumber(sqlFlupRs.getInt("UNIQUE_NUMBER"));
						fluppf.setChdrcoy(sqlFlupRs.getString("CHDRCOY")==null? ' ' : sqlFlupRs.getString("CHDRCOY").charAt(0));
						fluppf.setChdrnum(sqlFlupRs.getString("CHDRNUM"));
						fluppf.setFupNo(sqlFlupRs.getInt("FUPNO"));
						fluppf.setTranNo(sqlFlupRs.getInt("TRANNO"));
						fluppf.setFupTyp(sqlFlupRs.getString("FUPTYP")==null? ' ' : sqlFlupRs.getString("FUPTYP").charAt(0));
						fluppf.setLife(sqlFlupRs.getString("LIFE"));
						fluppf.setjLife(sqlFlupRs.getString("JLIFE"));
						fluppf.setFupCde(sqlFlupRs.getString("FUPCDE"));
						fluppf.setFupSts(sqlFlupRs.getString("FUPSTS")==null? ' ' : sqlFlupRs.getString("FUPSTS").charAt(0));
						fluppf.setFupDt(sqlFlupRs.getInt("FUPDT"));
						fluppf.setFupRmk(sqlFlupRs.getString("FUPRMK"));
						fluppf.setClamNum(sqlFlupRs.getString("CLAMNUM"));
						fluppf.setUserT(sqlFlupRs.getInt("USER_T"));
						fluppf.setTermId(sqlFlupRs.getString("TERMID"));
						fluppf.setTrdt(sqlFlupRs.getInt("TRDT"));
						fluppf.setTrtm(sqlFlupRs.getInt("TRTM"));
						fluppf.setzAutoInd(sqlFlupRs.getString("ZAUTOIND")==null? ' ' : sqlFlupRs.getString("ZAUTOIND").charAt(0));
						fluppf.setUsrPrf(sqlFlupRs.getString("USRPRF"));
						fluppf.setJobNm(sqlFlupRs.getString("JOBNM"));
						fluppf.setDatime(sqlFlupRs.getDate("DATIME"));
						fluppf.setEffDate(sqlFlupRs.getInt("EFFDATE"));
						fluppf.setCrtUser(sqlFlupRs.getString("CRTUSER"));
						fluppf.setCrtDate(sqlFlupRs.getInt("CRTDATE"));
						fluppf.setLstUpUser(sqlFlupRs.getString("LSTUPUSER"));
						fluppf.setzLstUpDt(sqlFlupRs.getInt("ZLSTUPDT"));
						fluppf.setFuprcvd(sqlFlupRs.getInt("FUPRCVD"));
						fluppf.setExprDate(sqlFlupRs.getInt("EXPRDATE"));
						
						flupSearchResult.add(fluppf);
					}
				} catch (SQLException e) {
					LOGGER.error("searchFlupRecordByChdrNum()", e);
					throw new SQLRuntimeException(e);
				} finally {
					close(psFlupSelect, sqlFlupRs);
				}
				return flupSearchResult;
			}
			//ILJ-108 ends
	@Override
	public List<Fluppf> getFluppfRecordByChdrnumAndTyp(Fluppf fluppf)
	{
		String sql="SELECT FUPSTS,FUPCDE FROM FLUPPF WHERE CHDRCOY = ? AND CHDRNUM=? AND FUPTYP=?";
		List<Fluppf> data = new ArrayList<>();
		try(PreparedStatement ps = getPrepareStatement(sql))
		{
			ps.setString(1, fluppf.getChdrcoy()+"");
			ps.setString(2, fluppf.getChdrnum());
			ps.setString(3, fluppf.getFupTyp()+"");
			try(ResultSet rs = ps.executeQuery())
			{				
				while(rs.next())
				{
					fluppf = new Fluppf();
					fluppf.setFupCde(rs.getString("FUPCDE"));
					fluppf.setFupSts(rs.getString("FUPSTS")==null? ' ' : rs.getString("FUPSTS").charAt(0));
					data.add(fluppf);
				}
				return data;
			}
		}
		catch (SQLException e) {
			LOGGER.error("getFluppfRecordByChdrnumAndTyp() { }", e);
			throw new SQLRuntimeException(e);
		}			
		}
	public boolean isCheckExistFluppf(Fluppf fluppf){
		StringBuilder sqlSchemeSelect1 = new StringBuilder(
				"SELECT COUNT(*) FROM Fluppf WHERE CHDRCOY=? AND CHDRNUM=? AND FUPNO=?  AND TRANNO = ? AND FUPCDE = ? and FUPSTS=? ");
		PreparedStatement stmt = null;
		ResultSet rs = null;
		int count = 0;
		try {
			stmt = getPrepareStatement(sqlSchemeSelect1.toString()); 
			stmt.setString(1, fluppf.getChdrcoy().toString());
			stmt.setString(2, fluppf.getChdrnum());
			stmt.setInt(3, fluppf.getFupNo());
			stmt.setInt(4, fluppf.getTranNo());
			stmt.setString(5, fluppf.getFupCde());
			stmt.setString(6, fluppf.getFupSts().toString());

			rs = stmt.executeQuery();

			while (rs.next()) {
				count = rs.getInt(1);

			}
		} catch (SQLException e) {
			LOGGER.error("isCheckExistFluppf", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(stmt, rs);
		}
		if (count > 0) {
			return true;

		}
		return false;
	}
}

