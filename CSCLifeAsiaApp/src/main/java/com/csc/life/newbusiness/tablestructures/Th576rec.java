package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:26
 * Description:
 * Copybook name: TH576REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th576rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th576Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData unit = new ZonedDecimalData(9, 0).isAPartOf(th576Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(491).isAPartOf(th576Rec, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th576Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th576Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}