package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:03:39
 * Description:
 * Copybook name: FLUPENQKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Flupenqkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData flupenqFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData flupenqKey = new FixedLengthStringData(256).isAPartOf(flupenqFileKey, 0, REDEFINE);
  	public FixedLengthStringData flupenqChdrcoy = new FixedLengthStringData(1).isAPartOf(flupenqKey, 0);
  	public FixedLengthStringData flupenqChdrnum = new FixedLengthStringData(8).isAPartOf(flupenqKey, 1);
  	public FixedLengthStringData flupenqFuptype = new FixedLengthStringData(1).isAPartOf(flupenqKey, 9);
  	public PackedDecimalData flupenqTransactionDate = new PackedDecimalData(6, 0).isAPartOf(flupenqKey, 10);
  	public PackedDecimalData flupenqTranno = new PackedDecimalData(5, 0).isAPartOf(flupenqKey, 14);
  	public PackedDecimalData flupenqFupno = new PackedDecimalData(2, 0).isAPartOf(flupenqKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(237).isAPartOf(flupenqKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(flupenqFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		flupenqFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}