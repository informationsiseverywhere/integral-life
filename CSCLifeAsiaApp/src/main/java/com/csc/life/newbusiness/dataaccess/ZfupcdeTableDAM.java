package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZfupcdeTableDAM.java
 * Date: Sun, 30 Aug 2009 03:53:07
 * Class transformed from ZFUPCDE.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZfupcdeTableDAM extends FluppfTableDAM {

	public ZfupcdeTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZFUPCDE");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", JLIFE"
		             + ", FUPCDE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "FUPCDE, " +
		            "FUPTYP, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "EFFDATE, " +
		            "CRTUSER, " +
		            "CRTDATE, " +
		            "LSTUPUSER, " +
		            "ZLSTUPDT, " +
		            "FUPRCVD, " +
		            "EXPRDATE, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "JLIFE ASC, " +
		            "FUPCDE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "JLIFE DESC, " +
		            "FUPCDE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               fupcode,
                               fuptype,
                               userProfile,
                               jobName,
                               datime,
                               effdate,
                               crtuser,
                               crtdate,
                               lstupuser,
                               zlstupdt,
                               fuprcvd,
                               exprdate,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(240);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ getFupcode().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, fupcode);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(jlife.toInternal());
	nonKeyFiller50.setInternal(fupcode.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(108);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ getFuptype().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getEffdate().toInternal()
					+ getCrtuser().toInternal()
					+ getCrtdate().toInternal()
					+ getLstupuser().toInternal()
					+ getZlstupdt().toInternal()
					+ getFuprcvd().toInternal()
					+ getExprdate().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, fuptype);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, crtuser);
			what = ExternalData.chop(what, crtdate);
			what = ExternalData.chop(what, lstupuser);
			what = ExternalData.chop(what, zlstupdt);
			what = ExternalData.chop(what, fuprcvd);
			what = ExternalData.chop(what, exprdate);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}
	public FixedLengthStringData getFupcode() {
		return fupcode;
	}
	public void setFupcode(Object what) {
		fupcode.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getFuptype() {
		return fuptype;
	}
	public void setFuptype(Object what) {
		fuptype.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getCrtuser() {
		return crtuser;
	}
	public void setCrtuser(Object what) {
		crtuser.set(what);
	}	
	public PackedDecimalData getCrtdate() {
		return crtdate;
	}
	public void setCrtdate(Object what) {
		setCrtdate(what, false);
	}
	public void setCrtdate(Object what, boolean rounded) {
		if (rounded)
			crtdate.setRounded(what);
		else
			crtdate.set(what);
	}	
	public FixedLengthStringData getLstupuser() {
		return lstupuser;
	}
	public void setLstupuser(Object what) {
		lstupuser.set(what);
	}	
	public PackedDecimalData getZlstupdt() {
		return zlstupdt;
	}
	public void setZlstupdt(Object what) {
		setZlstupdt(what, false);
	}
	public void setZlstupdt(Object what, boolean rounded) {
		if (rounded)
			zlstupdt.setRounded(what);
		else
			zlstupdt.set(what);
	}	
	public PackedDecimalData getFuprcvd() {
		return fuprcvd;
	}
	public void setFuprcvd(Object what) {
		setFuprcvd(what, false);
	}
	public void setFuprcvd(Object what, boolean rounded) {
		if (rounded)
			fuprcvd.setRounded(what);
		else
			fuprcvd.set(what);
	}	
	public PackedDecimalData getExprdate() {
		return exprdate;
	}
	public void setExprdate(Object what) {
		setExprdate(what, false);
	}
	public void setExprdate(Object what, boolean rounded) {
		if (rounded)
			exprdate.setRounded(what);
		else
			exprdate.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		jlife.clear();
		fupcode.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		fuptype.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();
		effdate.clear();
		crtuser.clear();
		crtdate.clear();
		lstupuser.clear();
		zlstupdt.clear();
		fuprcvd.clear();
		exprdate.clear();		
	}


}