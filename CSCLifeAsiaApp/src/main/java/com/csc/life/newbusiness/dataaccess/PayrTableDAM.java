package com.csc.life.newbusiness.dataaccess;

import com.csc.fsu.general.dataaccess.PayrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: PayrTableDAM.java
 * Date: Sun, 30 Aug 2009 03:44:11
 * Class transformed from PAYR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class PayrTableDAM extends PayrpfTableDAM {

	public PayrTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("PAYR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", PAYRSEQNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "PAYRSEQNO, " +
		            "EFFDATE, " +
		            "VALIDFLAG, " +
		            "BILLCHNL, " +
		            "BILLFREQ, " +
		            "BILLCD, " +
		            "NEXTDATE, " +
		            "BTDATE, " +
		            "PTDATE, " +
		            "BILLCURR, " +
		            "CNTCURR, " +
		            "SINSTAMT01, " +
		            "SINSTAMT02, " +
		            "SINSTAMT03, " +
		            "SINSTAMT04, " +
		            "SINSTAMT05, " +
		            "SINSTAMT06, " +
		            "OUTSTAMT, " +
		            "TAXRELMTH, " +
		            "BILLSUPR, " +
		            "APLSUPR, " +
		            "NOTSSUPR, " +
		            "BILLSPFROM, " +
		            "APLSPFROM, " +
		            "NOTSSPFROM, " +
		            "BILLSPTO, " +
		            "APLSPTO, " +
		            "NOTSSPTO, " +
		            "MANDREF, " +
		            "GRUPKEY, " +
		            "INCSEQNO, " +
		            "TRANNO, " +
		            "GRUPCOY, " +
		            "GRUPNUM, " +
		            "MEMBSEL, " +
		            "BILLNET, " +
		            "BILLDAY, " +
		            "BILLMONTH, " +
		            "DUEDD, " +
		            "DUEMM, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "PSTCDE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "ZMANDREF, " + 	//ILIFE-2472
		            "PRORCNTFEE, " +
		            "PRORAMT, " +
		            "ORGBILLCD, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "PAYRSEQNO ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "PAYRSEQNO DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               payrseqno,
                               effdate,
                               validflag,
                               billchnl,
                               billfreq,
                               billcd,
                               nextdate,
                               btdate,
                               ptdate,
                               billcurr,
                               cntcurr,
                               sinstamt01,
                               sinstamt02,
                               sinstamt03,
                               sinstamt04,
                               sinstamt05,
                               sinstamt06,
                               outstamt,
                               taxrelmth,
                               billsupr,
                               aplsupr,
                               notssupr,
                               billspfrom,
                               aplspfrom,
                               notsspfrom,
                               billspto,
                               aplspto,
                               notsspto,
                               mandref,
                               grupkey,
                               incomeSeqNo,
                               tranno,
                               grupcoy,
                               grupnum,
                               membsel,
                               billnet,
                               billday,
                               billmonth,
                               duedd,
                               duemm,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               pstatcode,
                               userProfile,
                               jobName,
                               datime,
                               zmandref, //ILIFE-2472
                               prorcntfee,//ILIFE-8509
                               proramt,
                               orgbillcd, //IBPLIFE-4826
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(54);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getPayrseqno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, payrseqno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(payrseqno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(362);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ getEffdate().toInternal()
					+ getValidflag().toInternal()
					+ getBillchnl().toInternal()
					+ getBillfreq().toInternal()
					+ getBillcd().toInternal()
					+ getNextdate().toInternal()
					+ getBtdate().toInternal()
					+ getPtdate().toInternal()
					+ getBillcurr().toInternal()
					+ getCntcurr().toInternal()
					+ getSinstamt01().toInternal()
					+ getSinstamt02().toInternal()
					+ getSinstamt03().toInternal()
					+ getSinstamt04().toInternal()
					+ getSinstamt05().toInternal()
					+ getSinstamt06().toInternal()
					+ getOutstamt().toInternal()
					+ getTaxrelmth().toInternal()
					+ getBillsupr().toInternal()
					+ getAplsupr().toInternal()
					+ getNotssupr().toInternal()
					+ getBillspfrom().toInternal()
					+ getAplspfrom().toInternal()
					+ getNotsspfrom().toInternal()
					+ getBillspto().toInternal()
					+ getAplspto().toInternal()
					+ getNotsspto().toInternal()
					+ getMandref().toInternal()
					+ getGrupkey().toInternal()
					+ getIncomeSeqNo().toInternal()
					+ getTranno().toInternal()
					+ getGrupcoy().toInternal()
					+ getGrupnum().toInternal()
					+ getMembsel().toInternal()
					+ getBillnet().toInternal()
					+ getBillday().toInternal()
					+ getBillmonth().toInternal()
					+ getDuedd().toInternal()
					+ getDuemm().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getPstatcode().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getZmandref().toInternal()	//ILIFE-2472
					+ getProrcntfee().toInternal()
					+ getProramt().toInternal() //ILIFE-8509
		            + getOrgbillcd().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, nextdate);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, billcurr);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, sinstamt01);
			what = ExternalData.chop(what, sinstamt02);
			what = ExternalData.chop(what, sinstamt03);
			what = ExternalData.chop(what, sinstamt04);
			what = ExternalData.chop(what, sinstamt05);
			what = ExternalData.chop(what, sinstamt06);
			what = ExternalData.chop(what, outstamt);
			what = ExternalData.chop(what, taxrelmth);
			what = ExternalData.chop(what, billsupr);
			what = ExternalData.chop(what, aplsupr);
			what = ExternalData.chop(what, notssupr);
			what = ExternalData.chop(what, billspfrom);
			what = ExternalData.chop(what, aplspfrom);
			what = ExternalData.chop(what, notsspfrom);
			what = ExternalData.chop(what, billspto);
			what = ExternalData.chop(what, aplspto);
			what = ExternalData.chop(what, notsspto);
			what = ExternalData.chop(what, mandref);
			what = ExternalData.chop(what, grupkey);
			what = ExternalData.chop(what, incomeSeqNo);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, grupcoy);
			what = ExternalData.chop(what, grupnum);
			what = ExternalData.chop(what, membsel);
			what = ExternalData.chop(what, billnet);
			what = ExternalData.chop(what, billday);
			what = ExternalData.chop(what, billmonth);
			what = ExternalData.chop(what, duedd);
			what = ExternalData.chop(what, duemm);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			what = ExternalData.chop(what, zmandref);	//ILIFE-2472
			what = ExternalData.chop(what, prorcntfee);//ILIFE-8509
			what = ExternalData.chop(what, proramt);
			what = ExternalData.chop(what, orgbillcd);
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public PackedDecimalData getPayrseqno() {
		return payrseqno;
	}
	public void setPayrseqno(Object what) {
		setPayrseqno(what, false);
	}
	public void setPayrseqno(Object what, boolean rounded) {
		if (rounded)
			payrseqno.setRounded(what);
		else
			payrseqno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public PackedDecimalData getOrgbillcd() {
		return orgbillcd;
	}
	public void setOrgbillcd(Object what) {
		setOrgbillcd(what, false);
	}
	public void setOrgbillcd(Object what, boolean rounded) {
		if (rounded)
			orgbillcd.setRounded(what);
		else
			orgbillcd.set(what);
	}	
	public PackedDecimalData getNextdate() {
		return nextdate;
	}
	public void setNextdate(Object what) {
		setNextdate(what, false);
	}
	public void setNextdate(Object what, boolean rounded) {
		if (rounded)
			nextdate.setRounded(what);
		else
			nextdate.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public FixedLengthStringData getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(Object what) {
		billcurr.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public PackedDecimalData getSinstamt01() {
		return sinstamt01;
	}
	public void setSinstamt01(Object what) {
		setSinstamt01(what, false);
	}
	public void setSinstamt01(Object what, boolean rounded) {
		if (rounded)
			sinstamt01.setRounded(what);
		else
			sinstamt01.set(what);
	}	
	public PackedDecimalData getSinstamt02() {
		return sinstamt02;
	}
	public void setSinstamt02(Object what) {
		setSinstamt02(what, false);
	}
	public void setSinstamt02(Object what, boolean rounded) {
		if (rounded)
			sinstamt02.setRounded(what);
		else
			sinstamt02.set(what);
	}	
	public PackedDecimalData getSinstamt03() {
		return sinstamt03;
	}
	public void setSinstamt03(Object what) {
		setSinstamt03(what, false);
	}
	public void setSinstamt03(Object what, boolean rounded) {
		if (rounded)
			sinstamt03.setRounded(what);
		else
			sinstamt03.set(what);
	}	
	public PackedDecimalData getSinstamt04() {
		return sinstamt04;
	}
	public void setSinstamt04(Object what) {
		setSinstamt04(what, false);
	}
	public void setSinstamt04(Object what, boolean rounded) {
		if (rounded)
			sinstamt04.setRounded(what);
		else
			sinstamt04.set(what);
	}	
	public PackedDecimalData getSinstamt05() {
		return sinstamt05;
	}
	public void setSinstamt05(Object what) {
		setSinstamt05(what, false);
	}
	public void setSinstamt05(Object what, boolean rounded) {
		if (rounded)
			sinstamt05.setRounded(what);
		else
			sinstamt05.set(what);
	}	
	public PackedDecimalData getSinstamt06() {
		return sinstamt06;
	}
	public void setSinstamt06(Object what) {
		setSinstamt06(what, false);
	}
	public void setSinstamt06(Object what, boolean rounded) {
		if (rounded)
			sinstamt06.setRounded(what);
		else
			sinstamt06.set(what);
	}	
	public PackedDecimalData getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(Object what) {
		setOutstamt(what, false);
	}
	public void setOutstamt(Object what, boolean rounded) {
		if (rounded)
			outstamt.setRounded(what);
		else
			outstamt.set(what);
	}	
	public FixedLengthStringData getTaxrelmth() {
		return taxrelmth;
	}
	public void setTaxrelmth(Object what) {
		taxrelmth.set(what);
	}	
	public FixedLengthStringData getBillsupr() {
		return billsupr;
	}
	public void setBillsupr(Object what) {
		billsupr.set(what);
	}	
	public FixedLengthStringData getAplsupr() {
		return aplsupr;
	}
	public void setAplsupr(Object what) {
		aplsupr.set(what);
	}	
	public FixedLengthStringData getNotssupr() {
		return notssupr;
	}
	public void setNotssupr(Object what) {
		notssupr.set(what);
	}	
	public PackedDecimalData getBillspfrom() {
		return billspfrom;
	}
	public void setBillspfrom(Object what) {
		setBillspfrom(what, false);
	}
	public void setBillspfrom(Object what, boolean rounded) {
		if (rounded)
			billspfrom.setRounded(what);
		else
			billspfrom.set(what);
	}	
	public PackedDecimalData getAplspfrom() {
		return aplspfrom;
	}
	public void setAplspfrom(Object what) {
		setAplspfrom(what, false);
	}
	public void setAplspfrom(Object what, boolean rounded) {
		if (rounded)
			aplspfrom.setRounded(what);
		else
			aplspfrom.set(what);
	}	
	public PackedDecimalData getNotsspfrom() {
		return notsspfrom;
	}
	public void setNotsspfrom(Object what) {
		setNotsspfrom(what, false);
	}
	public void setNotsspfrom(Object what, boolean rounded) {
		if (rounded)
			notsspfrom.setRounded(what);
		else
			notsspfrom.set(what);
	}	
	public PackedDecimalData getBillspto() {
		return billspto;
	}
	public void setBillspto(Object what) {
		setBillspto(what, false);
	}
	public void setBillspto(Object what, boolean rounded) {
		if (rounded)
			billspto.setRounded(what);
		else
			billspto.set(what);
	}	
	public PackedDecimalData getAplspto() {
		return aplspto;
	}
	public void setAplspto(Object what) {
		setAplspto(what, false);
	}
	public void setAplspto(Object what, boolean rounded) {
		if (rounded)
			aplspto.setRounded(what);
		else
			aplspto.set(what);
	}	
	public PackedDecimalData getNotsspto() {
		return notsspto;
	}
	public void setNotsspto(Object what) {
		setNotsspto(what, false);
	}
	public void setNotsspto(Object what, boolean rounded) {
		if (rounded)
			notsspto.setRounded(what);
		else
			notsspto.set(what);
	}	
	public FixedLengthStringData getMandref() {
		return mandref;
	}
	public void setMandref(Object what) {
		mandref.set(what);
	}	
	public FixedLengthStringData getGrupkey() {
		return grupkey;
	}
	public void setGrupkey(Object what) {
		grupkey.set(what);
	}	
	public PackedDecimalData getIncomeSeqNo() {
		return incomeSeqNo;
	}
	public void setIncomeSeqNo(Object what) {
		setIncomeSeqNo(what, false);
	}
	public void setIncomeSeqNo(Object what, boolean rounded) {
		if (rounded)
			incomeSeqNo.setRounded(what);
		else
			incomeSeqNo.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getGrupcoy() {
		return grupcoy;
	}
	public void setGrupcoy(Object what) {
		grupcoy.set(what);
	}	
	public FixedLengthStringData getGrupnum() {
		return grupnum;
	}
	public void setGrupnum(Object what) {
		grupnum.set(what);
	}	
	public FixedLengthStringData getMembsel() {
		return membsel;
	}
	public void setMembsel(Object what) {
		membsel.set(what);
	}	
	public FixedLengthStringData getBillnet() {
		return billnet;
	}
	public void setBillnet(Object what) {
		billnet.set(what);
	}	
	public FixedLengthStringData getBillday() {
		return billday;
	}
	public void setBillday(Object what) {
		billday.set(what);
	}	
	public FixedLengthStringData getBillmonth() {
		return billmonth;
	}
	public void setBillmonth(Object what) {
		billmonth.set(what);
	}	
	public FixedLengthStringData getDuedd() {
		return duedd;
	}
	public void setDuedd(Object what) {
		duedd.set(what);
	}	
	public FixedLengthStringData getDuemm() {
		return duemm;
	}
	public void setDuemm(Object what) {
		duemm.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//ILIFE-2472
	public FixedLengthStringData getZmandref() {
		return zmandref;
	}
	//ILIFE-2472
	public void setZmandref(Object what) {
		zmandref.set(what);
	}
	//ILIFE-8509
	public PackedDecimalData getProrcntfee() {
		return prorcntfee;
	}
	public void setProrcntfee(Object what) {
		setProrcntfee(what, false);
	}
	public void setProrcntfee(Object what, boolean rounded) {
		if (rounded)
			prorcntfee.setRounded(what);
		else
			prorcntfee.set(what);
	}
	public PackedDecimalData getProramt() {
		return proramt;
	}
	public void setProramt(Object what) {
		setProramt(what, false);
	}
	public void setProramt(Object what, boolean rounded) {
		if (rounded)
			proramt.setRounded(what);
		else
			proramt.set(what);
	}

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSinstamts() {
		return new FixedLengthStringData(sinstamt01.toInternal()
										+ sinstamt02.toInternal()
										+ sinstamt03.toInternal()
										+ sinstamt04.toInternal()
										+ sinstamt05.toInternal()
										+ sinstamt06.toInternal());
	}
	public void setSinstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSinstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, sinstamt01);
		what = ExternalData.chop(what, sinstamt02);
		what = ExternalData.chop(what, sinstamt03);
		what = ExternalData.chop(what, sinstamt04);
		what = ExternalData.chop(what, sinstamt05);
		what = ExternalData.chop(what, sinstamt06);
	}
	public PackedDecimalData getSinstamt(BaseData indx) {
		return getSinstamt(indx.toInt());
	}
	public PackedDecimalData getSinstamt(int indx) {

		switch (indx) {
			case 1 : return sinstamt01;
			case 2 : return sinstamt02;
			case 3 : return sinstamt03;
			case 4 : return sinstamt04;
			case 5 : return sinstamt05;
			case 6 : return sinstamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSinstamt(BaseData indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(BaseData indx, Object what, boolean rounded) {
		setSinstamt(indx.toInt(), what, rounded);
	}
	public void setSinstamt(int indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSinstamt01(what, rounded);
					 break;
			case 2 : setSinstamt02(what, rounded);
					 break;
			case 3 : setSinstamt03(what, rounded);
					 break;
			case 4 : setSinstamt04(what, rounded);
					 break;
			case 5 : setSinstamt05(what, rounded);
					 break;
			case 6 : setSinstamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		payrseqno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		effdate.clear();
		validflag.clear();
		billchnl.clear();
		billfreq.clear();
		billcd.clear();
		nextdate.clear();
		btdate.clear();
		ptdate.clear();
		billcurr.clear();
		cntcurr.clear();
		sinstamt01.clear();
		sinstamt02.clear();
		sinstamt03.clear();
		sinstamt04.clear();
		sinstamt05.clear();
		sinstamt06.clear();
		outstamt.clear();
		taxrelmth.clear();
		billsupr.clear();
		aplsupr.clear();
		notssupr.clear();
		billspfrom.clear();
		aplspfrom.clear();
		notsspfrom.clear();
		billspto.clear();
		aplspto.clear();
		notsspto.clear();
		mandref.clear();
		grupkey.clear();
		incomeSeqNo.clear();
		tranno.clear();
		grupcoy.clear();
		grupnum.clear();
		membsel.clear();
		billnet.clear();
		billday.clear();
		billmonth.clear();
		duedd.clear();
		duemm.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		pstatcode.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		zmandref.clear(); //ILIFE-2472
		prorcntfee.clear();//ILIFE-8509
		proramt.clear();
		orgbillcd.clear();
	}


}