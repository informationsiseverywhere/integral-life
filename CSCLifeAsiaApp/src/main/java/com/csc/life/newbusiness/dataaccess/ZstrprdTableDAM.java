package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZstrprdTableDAM.java
 * Date: Sun, 30 Aug 2009 03:53:36
 * Class transformed from ZSTRPRD.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZstrprdTableDAM extends ZstrpfTableDAM {

	public ZstrprdTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZSTRPRD");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CNTBRANCH"
		             + ", CNTTYPE"
		             + ", EFFDATE"
		             + ", BATCTRCDE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRPFX, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "TRANNO, " +
		            "AGNTNUM, " +
		            "ARACDE, " +
		            "CNTBRANCH, " +
		            "COMMYR, " +
		            "CNTTYPE, " +
		            "STATCODE, " +
		            "PSTATCODE, " +
		            "CNTCURR, " +
		            "SRCEBUS, " +
		            "BATCTRCDE, " +
		            "EFFDATE, " +
		            "SINGP, " +
		            "INSTPREM, " +
		            "SUMINS, " +
		            "BILLFREQ, " +
		            "OCCDATE, " +
		            "CNTFEE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CNTBRANCH ASC, " +
		            "CNTTYPE ASC, " +
		            "EFFDATE DESC, " +
		            "BATCTRCDE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CNTBRANCH DESC, " +
		            "CNTTYPE DESC, " +
		            "EFFDATE ASC, " +
		            "BATCTRCDE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrpfx,
                               chdrcoy,
                               chdrnum,
                               tranno,
                               agntnum,
                               aracde,
                               cntbranch,
                               commyr,
                               cnttype,
                               statcode,
                               pstatcode,
                               cntcurr,
                               srcebus,
                               batctrcde,
                               effdate,
                               singp,
                               instprem,
                               sumins,
                               billfreq,
                               occdate,
                               cntfee,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(49);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getCntbranch().toInternal()
					+ getCnttype().toInternal()
					+ getEffdate().toInternal()
					+ getBatctrcde().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller9 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller14 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller15 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller2.setInternal(chdrcoy.toInternal());
	nonKeyFiller7.setInternal(cntbranch.toInternal());
	nonKeyFiller9.setInternal(cnttype.toInternal());
	nonKeyFiller14.setInternal(batctrcde.toInternal());
	nonKeyFiller15.setInternal(effdate.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(140);
		
		nonKeyData.set(
					getChdrpfx().toInternal()
					+ nonKeyFiller2.toInternal()
					+ getChdrnum().toInternal()
					+ getTranno().toInternal()
					+ getAgntnum().toInternal()
					+ getAracde().toInternal()
					+ nonKeyFiller7.toInternal()
					+ getCommyr().toInternal()
					+ nonKeyFiller9.toInternal()
					+ getStatcode().toInternal()
					+ getPstatcode().toInternal()
					+ getCntcurr().toInternal()
					+ getSrcebus().toInternal()
					+ nonKeyFiller14.toInternal()
					+ nonKeyFiller15.toInternal()
					+ getSingp().toInternal()
					+ getInstprem().toInternal()
					+ getSumins().toInternal()
					+ getBillfreq().toInternal()
					+ getOccdate().toInternal()
					+ getCntfee().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrpfx);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, aracde);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, commyr);
			what = ExternalData.chop(what, nonKeyFiller9);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, nonKeyFiller14);
			what = ExternalData.chop(what, nonKeyFiller15);
			what = ExternalData.chop(what, singp);
			what = ExternalData.chop(what, instprem);
			what = ExternalData.chop(what, sumins);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, cntfee);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(Object what) {
		chdrpfx.set(what);
	}	
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public FixedLengthStringData getAracde() {
		return aracde;
	}
	public void setAracde(Object what) {
		aracde.set(what);
	}	
	public PackedDecimalData getCommyr() {
		return commyr;
	}
	public void setCommyr(Object what) {
		setCommyr(what, false);
	}
	public void setCommyr(Object what, boolean rounded) {
		if (rounded)
			commyr.setRounded(what);
		else
			commyr.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}	
	public PackedDecimalData getSingp() {
		return singp;
	}
	public void setSingp(Object what) {
		setSingp(what, false);
	}
	public void setSingp(Object what, boolean rounded) {
		if (rounded)
			singp.setRounded(what);
		else
			singp.set(what);
	}	
	public PackedDecimalData getInstprem() {
		return instprem;
	}
	public void setInstprem(Object what) {
		setInstprem(what, false);
	}
	public void setInstprem(Object what, boolean rounded) {
		if (rounded)
			instprem.setRounded(what);
		else
			instprem.set(what);
	}	
	public PackedDecimalData getSumins() {
		return sumins;
	}
	public void setSumins(Object what) {
		setSumins(what, false);
	}
	public void setSumins(Object what, boolean rounded) {
		if (rounded)
			sumins.setRounded(what);
		else
			sumins.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public PackedDecimalData getCntfee() {
		return cntfee;
	}
	public void setCntfee(Object what) {
		setCntfee(what, false);
	}
	public void setCntfee(Object what, boolean rounded) {
		if (rounded)
			cntfee.setRounded(what);
		else
			cntfee.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		cntbranch.clear();
		cnttype.clear();
		effdate.clear();
		batctrcde.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		chdrpfx.clear();
		nonKeyFiller2.clear();
		chdrnum.clear();
		tranno.clear();
		agntnum.clear();
		aracde.clear();
		nonKeyFiller7.clear();
		commyr.clear();
		nonKeyFiller9.clear();
		statcode.clear();
		pstatcode.clear();
		cntcurr.clear();
		srcebus.clear();
		nonKeyFiller14.clear();
		nonKeyFiller15.clear();
		singp.clear();
		instprem.clear();
		sumins.clear();
		billfreq.clear();
		occdate.clear();
		cntfee.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}