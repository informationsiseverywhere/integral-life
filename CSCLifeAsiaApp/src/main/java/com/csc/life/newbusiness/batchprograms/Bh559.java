/*
 * File: Bh559.java
 * Date: 29 August 2009 21:32:38
 * Author: Quipoz Limited
 *
 * Class transformed from BH559.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceLeading;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectTally;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Srvunitcpy;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.FluplnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.reports.Rh559Report;
import com.csc.life.newbusiness.tablestructures.Th506rec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ErorTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspcomn;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.ValueRange;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*    Daily Outstanding Proposals Report
*
*    This program is to report on policies which remain unissued
*    due to outstanding information required.
*
*    Only proposals which falls on either 21, 42, 63 or 84+ days
*    from the date the proposal was first received, are to be
*    output.
*
*    Main logic:
*    Exec SQL on CHDR selecting on company and policy status
*    corresponding to proposal.
*    Fetch each queried CHDR, read HPAD for proposal received
*    date.  If the elapsed day falls in the valid reporting dates,
*    process this policy by reading all the necessary information
*    to appear on the report.
*
*   Control totals:
*     01  -  No. of Records Read
*     02  -  No. of Policies Reported
*     03  -  Total for Policies
*
*
*****************************************************************
* </pre>
*/
public class Bh559 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlchdrpf1rs = null;
	private java.sql.PreparedStatement sqlchdrpf1ps = null;
	private java.sql.Connection sqlchdrpf1conn = null;
	private String sqlchdrpf1 = "";
	private Rh559Report printerFile = new Rh559Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(116);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH559");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaFileSts = new FixedLengthStringData(2).init(SPACES);

	private ZonedDecimalData wsaaValidDays = new ZonedDecimalData(3, 0).setUnsigned();
	private Validator validDay = new Validator(wsaaValidDays, 21,42,63,new ValueRange("84","999"));
	private ZonedDecimalData wsaaOsday = new ZonedDecimalData(3, 0).isAPartOf(wsaaValidDays, 0, REDEFINE).setPattern("ZZ9");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaLinecnt = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	////ILIFE-2536-STARTS
	private ZonedDecimalData wsaaSingp = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaSumins = new ZonedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaInstprem = new ZonedDecimalData(17, 2).init(0);
	//ILIFE-2536-ENDS
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq9 = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreq, 0, REDEFINE).setUnsigned();

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private static final String esql = "ESQL";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t5661 = "T5661";
	private static final String t5674 = "T5674";
	private static final String t5679 = "T5679";
	private static final String t5688 = "T5688";
	private static final String th506 = "TH506";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaOverflowErr = new FixedLengthStringData(1).init("Y");
	private Validator newPageReqErr = new Validator(wsaaOverflowErr, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private FixedLengthStringData wsaaSrvuLife = new FixedLengthStringData(2);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/*   Main, standard page headings*/
	private FixedLengthStringData rh559B00 = new FixedLengthStringData(1);

	private FixedLengthStringData rh559H01 = new FixedLengthStringData(95);
	private FixedLengthStringData rh559h01O = new FixedLengthStringData(95).isAPartOf(rh559H01, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rh559h01O, 0);
	private FixedLengthStringData rh01ReportDate = new FixedLengthStringData(22).isAPartOf(rh559h01O, 10);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rh559h01O, 32);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rh559h01O, 33);
	private FixedLengthStringData rh01Cntbranch = new FixedLengthStringData(2).isAPartOf(rh559h01O, 63);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rh559h01O, 65);

	private FixedLengthStringData rh559D01 = new FixedLengthStringData(140);//ILIFE-2536
	private FixedLengthStringData rh559d01O = new FixedLengthStringData(140).isAPartOf(rh559D01, 0);//ILIFE-2536
	private FixedLengthStringData rd01Entity = new FixedLengthStringData(16).isAPartOf(rh559d01O, 0);
	private FixedLengthStringData rd01Clntsname = new FixedLengthStringData(30).isAPartOf(rh559d01O, 16);
	private FixedLengthStringData rd01IdNumber = new FixedLengthStringData(10).isAPartOf(rh559d01O, 46);
	private FixedLengthStringData rd01Cntcurr = new FixedLengthStringData(3).isAPartOf(rh559d01O, 56);
	//ILIFE-2536-STARTS
	private ZonedDecimalData rd01Sumins = new ZonedDecimalData(17, 2).isAPartOf(rh559d01O, 59);
	private ZonedDecimalData rd01ModalPrem = new ZonedDecimalData(17, 2).isAPartOf(rh559d01O, 76);
	private ZonedDecimalData rd01AnnualPrem = new ZonedDecimalData(17, 2).isAPartOf(rh559d01O, 93);
	private FixedLengthStringData rd01Descr = new FixedLengthStringData(30).isAPartOf(rh559d01O, 110);
	//ILIFE-2536-ENDS

	private FixedLengthStringData rh559E01 = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ErorTableDAM erorIO = new ErorTableDAM();
	private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Wsspcomn wsspcomn = new Wsspcomn();
	private T5661rec t5661rec = new T5661rec();
	private T5674rec t5674rec = new T5674rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private Th506rec th506rec = new Th506rec();
	private Srvunitcpy srvunitcpy = new Srvunitcpy();
	private FormatsInner formatsInner = new FormatsInner();
	private SqlChdrpfInner sqlChdrpfInner = new SqlChdrpfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2060,
		exit2090,
		chdrDetails2520,
		exit2590
	}

	public Bh559() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspcomn.edterror;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1020();
		setUpHeadingCompany1030();
	}

protected void initialise1010()
	{
		wsspcomn.edterror.set(varcom.oK);
		wsaaCompany.set(bsprIO.getCompany());
		wsaaSrvuLife.set(srvunitcpy.lifeVal);
		/* Read T5679 for the Proposal Risk Status,*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		wsaaFileSts.set(t5679rec.setCnRiskStat);
		/* Exec SQL on CHDRPF*/
		sqlchdrpf1 = " SELECT  CHDRCOY, CHDRNUM, CNTBRANCH, CNTTYPE, CNTCURR, BILLFREQ, OCCDATE, COWNPFX, COWNCOY, COWNNUM" +
" FROM   " + getAppVars().getTableNameOverriden("CHDRPF") + " " +
" WHERE (CHDRCOY = ?)" +
" AND (STATCODE = ?)" +
" AND (SERVUNIT = ?)" +
" ORDER BY CNTBRANCH";
		/* Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlchdrpf1conn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlchdrpf1ps = getAppVars().prepareStatementEmbeded(sqlchdrpf1conn, sqlchdrpf1, "CHDRPF");
			getAppVars().setDBString(sqlchdrpf1ps, 1, wsaaCompany);
			getAppVars().setDBString(sqlchdrpf1ps, 2, wsaaFileSts);
			getAppVars().setDBString(sqlchdrpf1ps, 3, wsaaSrvuLife);
			sqlchdrpf1rs = getAppVars().executeQuery(sqlchdrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/* Open required files.*/
		printerFile.openOutput();
	}

protected void setUpHeadingDates1020()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		/*  Call DATCON6 to format the date of report.*/
		datcon6rec.language.set(bsscIO.getLanguage());
		datcon6rec.company.set(bsprIO.getCompany());
		datcon6rec.intDate1.set(bsscIO.getEffectiveDate());
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		rh01ReportDate.set(datcon6rec.intDate2);
	}

protected void setUpHeadingCompany1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(bsprIO.getCompany());
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readFile2010();
				case eof2060:
					eof2060();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlchdrpf1rs)) {
				getAppVars().getDBObject(sqlchdrpf1rs, 1, sqlChdrpfInner.chdrcoy);
				getAppVars().getDBObject(sqlchdrpf1rs, 2, sqlChdrpfInner.chdrnum);
				getAppVars().getDBObject(sqlchdrpf1rs, 3, sqlChdrpfInner.cntbranch);
				getAppVars().getDBObject(sqlchdrpf1rs, 4, sqlChdrpfInner.cnttype);
				getAppVars().getDBObject(sqlchdrpf1rs, 5, sqlChdrpfInner.cntcurr);
				getAppVars().getDBObject(sqlchdrpf1rs, 6, sqlChdrpfInner.billfreq);
				getAppVars().getDBObject(sqlchdrpf1rs, 7, sqlChdrpfInner.occdate);
				getAppVars().getDBObject(sqlchdrpf1rs, 8, sqlChdrpfInner.cownpfx);
				getAppVars().getDBObject(sqlchdrpf1rs, 9, sqlChdrpfInner.cowncoy);
				getAppVars().getDBObject(sqlchdrpf1rs, 10, sqlChdrpfInner.cownnum);
			}
			else {
				goTo(GotoLabel.eof2060);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2060()
	{
		wsspcomn.edterror.set(varcom.endp);
	}

protected void edit2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					edit2510();
				case chdrDetails2520:
					chdrDetails2520();
				case exit2590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void edit2510()
	{
		wsspcomn.edterror.set(varcom.oK);
		/* Number of Records Read.*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/* Read HPAD for proposal received date.*/
		hpadIO.setParams(SPACES);
		hpadIO.setChdrcoy(sqlChdrpfInner.chdrcoy);
		hpadIO.setChdrnum(sqlChdrpfInner.chdrnum);
		hpadIO.setFormat(formatsInner.hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
		&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(hpadIO.getParams());
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		if (isEQ(hpadIO.getStatuz(), varcom.mrnf)) {
			wsaaValidDays.set(ZERO);
			goTo(GotoLabel.exit2590);
		}
		datcon3rec.intDate1.set(hpadIO.getHprrcvdt());
		datcon3rec.intDate2.set(bsscIO.getEffectiveDate());
		datcon3rec.frequency.set("DY");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		wsaaValidDays.set(datcon3rec.freqFactor);
		if (!validDay.isTrue()) {
			goTo(GotoLabel.exit2590);
		}
		wsaaOsday.set(inspectReplaceLeading(wsaaOsday, "0", SPACES));
		/* Number of Policies Reported.*/
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		/*  Format branch description on change of code.*/
		if (isEQ(sqlChdrpfInner.cntbranch, wsaaBranch)) {
			goTo(GotoLabel.chdrDetails2520);
		}
		wsaaBranch.set(sqlChdrpfInner.cntbranch);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(sqlChdrpfInner.cntbranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(formatsInner.descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Cntbranch.set(sqlChdrpfInner.cntbranch);
		rh01Branchnm.set(descIO.getLongdesc());
		wsaaLinecnt.set(61);
	}

protected void chdrDetails2520()
	{
		rh559D01.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlChdrpfInner.cnttype);
		stringVariable1.addExpression(sqlChdrpfInner.chdrnum);
		stringVariable1.setStringInto(rd01Entity);
		/* Read TH506 for basic plan sum insured.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th506);
		itemIO.setItemitem(sqlChdrpfInner.cnttype);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th506rec.th506Rec.set(itemIO.getGenarea());
		/* Read through COVTLNB to accumulate single and instalment premium*/
		covtlnbIO.setDataKey(SPACES);
		covtlnbIO.setChdrcoy(sqlChdrpfInner.chdrcoy);
		covtlnbIO.setChdrnum(sqlChdrpfInner.chdrnum);
		covtlnbIO.setLife("01");
		covtlnbIO.setCoverage("01");
		covtlnbIO.setRider("00");
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFormat(formatsInner.covtlnbrec);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		covtlnbIO.setStatuz(varcom.oK);
		wsaaSumins.set(ZERO);
		wsaaSingp.set(ZERO);
		wsaaInstprem.set(ZERO);
		while ( !(isEQ(covtlnbIO.getStatuz(), varcom.endp))) {
			SmartFileCode.execute(appVars, covtlnbIO);
			if (isNE(covtlnbIO.getStatuz(), varcom.oK)
			&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
				syserrrec.params.set(covtlnbIO.getParams());
				fatalError600();
			}
			if (isNE(covtlnbIO.getChdrcoy(), sqlChdrpfInner.chdrcoy)
			|| isNE(covtlnbIO.getChdrnum(), sqlChdrpfInner.chdrnum)
			|| isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
				covtlnbIO.setStatuz(varcom.endp);
			}
			else {
				if (isEQ(th506rec.crtable, SPACES)
				&& isEQ(covtlnbIO.getRider(), "00")) {
					th506rec.crtable.set(covtlnbIO.getCrtable());
				}
				if (isEQ(covtlnbIO.getCrtable(), th506rec.crtable)) {
					wsaaSumins.add(covtlnbIO.getSumins());
				}
				wsaaSingp.add(covtlnbIO.getSingp());
				wsaaInstprem.add(covtlnbIO.getInstprem());
			}
			covtlnbIO.setFunction(varcom.nextr);
		}

		/* Incorporate fee to premium.*/
		calcFee2600();
		/* Set up print detail, the first line.*/
		indOn.setTrue(30);
		rd01Cntcurr.set(sqlChdrpfInner.cntcurr);
		rd01Sumins.set(wsaaSumins);
		compute(rd01ModalPrem, 2).set(add(wsaaSingp, wsaaInstprem));
		wsaaBillfreq.set(sqlChdrpfInner.billfreq);
		compute(rd01AnnualPrem, 2).set(mult(wsaaInstprem, wsaaBillfreq9));
		/* Read LIFELNB for 1st life insured detail*/
		lifelnbIO.setDataKey(SPACES);
		lifelnbIO.setChdrcoy(sqlChdrpfInner.chdrcoy);
		lifelnbIO.setChdrnum(sqlChdrpfInner.chdrnum);
		lifelnbIO.setLife("01");
		lifelnbIO.setJlife("00");
		lifelnbIO.setFormat(formatsInner.lifelnbrec);
		lifelnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		lifelnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		lifelnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		lifelnbIO.setStatuz(varcom.oK);
		while ( !(isEQ(lifelnbIO.getStatuz(), varcom.endp)
		|| isNE(rd01Clntsname, SPACES))) {
			readLifeInsured2700();
		}

		/* Read FLUPLNB for 1st followup detail if available*/
		fluplnbIO.setDataKey(SPACES);
		fluplnbIO.setChdrcoy(sqlChdrpfInner.chdrcoy);
		fluplnbIO.setChdrnum(sqlChdrpfInner.chdrnum);
		fluplnbIO.setFupno(ZERO);
		fluplnbIO.setFormat(formatsInner.fluplnbrec);
		fluplnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluplnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluplnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		fluplnbIO.setStatuz(varcom.oK);
		while ( !(isEQ(fluplnbIO.getStatuz(), varcom.endp)
		|| isNE(rd01Descr, SPACES))) {
			readFollowup2800();
		}

		if (isEQ(fluplnbIO.getStatuz(), varcom.endp)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("(");
			stringVariable2.addExpression(wsaaOsday);
			stringVariable2.addExpression(" Days)");
			stringVariable2.setStringInto(rd01Descr);
		}
		/* Write print record, the first line.*/
		z100CheckOvrflow();
		printerFile.printRh559b00(rh559B00, indicArea);
		wsaaLinecnt.add(1);
		z200PrintDetail();
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaInstprem);
		callContot001();
		indOff.setTrue(30);
		/* Set up print detail, the second line.*/
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(hpadIO.getHprrcvdt());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		datcon1rec.extDate.set(inspectReplaceLeading(datcon1rec.extDate, "0", SPACES));
		StringUtil stringVariable3 = new StringUtil();
		stringVariable3.addExpression("(");
		stringVariable3.addExpression(datcon1rec.extDate);
		stringVariable3.addExpression(")");
		stringVariable3.setStringInto(rd01Entity);
		/* Read LIFELNB for next life insured detail*/
		if (isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			while ( !(isEQ(lifelnbIO.getStatuz(), varcom.endp)
			|| isNE(rd01Clntsname, SPACES))) {
				readLifeInsured2700();
			}

		}
		/* Read FLUPLNB for next followup detail if available*/
		if (isNE(fluplnbIO.getStatuz(), varcom.endp)) {
			while ( !(isEQ(fluplnbIO.getStatuz(), varcom.endp)
			|| isNE(rd01Descr, SPACES))) {
				readFollowup2800();
			}

			if (isEQ(fluplnbIO.getStatuz(), varcom.endp)) {
				StringUtil stringVariable4 = new StringUtil();
				stringVariable4.addExpression("(");
				stringVariable4.addExpression(wsaaOsday);
				stringVariable4.addExpression(" Days)");
				stringVariable4.setStringInto(rd01Descr);
			}
		}
		/* Write print record, the second line.*/
		z200PrintDetail();
		/* Print the rest of LIFELNB or FLUPLNB details.*/
		while ( !(isEQ(lifelnbIO.getStatuz(), varcom.endp)
		&& isEQ(fluplnbIO.getStatuz(), varcom.endp))) {
			if (isNE(lifelnbIO.getStatuz(), varcom.endp)) {
				while ( !(isEQ(lifelnbIO.getStatuz(), varcom.endp)
				|| isNE(rd01Clntsname, SPACES))) {
					readLifeInsured2700();
				}

			}
			if (isNE(fluplnbIO.getStatuz(), varcom.endp)) {
				while ( !(isEQ(fluplnbIO.getStatuz(), varcom.endp)
				|| isNE(rd01Descr, SPACES))) {
					readFollowup2800();
				}

				if (isEQ(fluplnbIO.getStatuz(), varcom.endp)) {
					StringUtil stringVariable5 = new StringUtil();
					stringVariable5.addExpression("(");
					stringVariable5.addExpression(wsaaOsday);
					stringVariable5.addExpression(" Days)");
					stringVariable5.setStringInto(rd01Descr);
				}
			}
			/* Write print record, the rest.*/
			if (isNE(rh559D01, SPACES)) {
				z200PrintDetail();
			}
		}

	}

protected void calcFee2600()
	{
		calcFee2610();
	}

protected void calcFee2610()
	{
		/* Read T5688 for fee method, then T5674 for the actual fee*/
		/* calculation subroutine.  Call to the subroutine, and add*/
		/* fee to installment premium.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5688);
		itemIO.setItemitem(sqlChdrpfInner.cnttype);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5688rec.t5688Rec.set(itemIO.getGenarea());
		if (isEQ(t5688rec.feemeth, SPACES)) {
			return ;
		}
		itemIO.setItemtabl(t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(sqlChdrpfInner.cnttype);
		mgfeelrec.billfreq.set(sqlChdrpfInner.billfreq);
		mgfeelrec.effdate.set(sqlChdrpfInner.occdate);
		mgfeelrec.cntcurr.set(sqlChdrpfInner.cntcurr);
		mgfeelrec.company.set(bsprIO.getCompany());
		callProgramX(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, varcom.oK)
		&& isNE(mgfeelrec.statuz, varcom.endp)) {
			syserrrec.params.set(mgfeelrec.mgfeelRec);
			fatalError600();
		}
		wsaaInstprem.add(mgfeelrec.mgfee);
	}

protected void readLifeInsured2700()
	{
		readLife2710();
	}

protected void readLife2710()
	{
		SmartFileCode.execute(appVars, lifelnbIO);
		if (isNE(lifelnbIO.getStatuz(), varcom.oK)
		&& isNE(lifelnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(lifelnbIO.getParams());
			fatalError600();
		}
		if (isNE(lifelnbIO.getChdrcoy(), sqlChdrpfInner.chdrcoy)
		|| isNE(lifelnbIO.getChdrnum(), sqlChdrpfInner.chdrnum)
		|| isEQ(lifelnbIO.getStatuz(), varcom.endp)) {
			lifelnbIO.setStatuz(varcom.endp);
			return ;
		}
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx(sqlChdrpfInner.cownpfx);
		cltsIO.setClntcoy(sqlChdrpfInner.cowncoy);
		cltsIO.setClntnum(lifelnbIO.getLifcnum());
		cltsIO.setFormat(formatsInner.cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		else {
			if (isEQ(cltsIO.getStatuz(), varcom.mrnf)) {
				return ;
			}
		}
		plainname();
		rd01IdNumber.set(cltsIO.getSecuityno());
		if (isEQ(lifelnbIO.getJlife(), "00")) {
			rd01Clntsname.set(wsspcomn.longconfname);
		}
		else {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(wsspcomn.longconfname);
			stringVariable1.setStringInto(rd01Clntsname);
		}
		lifelnbIO.setFunction(varcom.nextr);
	}

	/**
	* <pre>
	* Name Formatting Routine.
	* </pre>
	*/
protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	* </pre>
	*/
protected void readFollowup2800()
	{
		read2810();
	}

protected void read2810()
	{
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(), varcom.oK)
		&& isNE(fluplnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(fluplnbIO.getParams());
			fatalError600();
		}
		if (isNE(fluplnbIO.getChdrcoy(), sqlChdrpfInner.chdrcoy)
		|| isNE(fluplnbIO.getChdrnum(), sqlChdrpfInner.chdrnum)
		|| isEQ(fluplnbIO.getStatuz(), varcom.endp)) {
			fluplnbIO.setStatuz(varcom.endp);
			return ;
		}
		/* Read T5661 with the follow-up codes of this policy and*/
		/* map for the completed statii.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5661);
		/*    MOVE FLUPLNB-FUPCODE        TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(bsscIO.getLanguage());
		wsaaT5661Fupcode.set(fluplnbIO.getFupcode());
		itemIO.setItemitem(wsaaT5661Key);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(formatsInner.itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t5661rec.t5661Rec.set(itemIO.getGenarea());
		wsaaSub.set(ZERO);
		wsaaSub.add(inspectTally(t5661rec.fuposss, " ", "CHARACTERS", fluplnbIO.getFupstat(), null));
		if (isLT(wsaaSub, 10)) {
			fluplnbIO.setFunction(varcom.nextr);
			return ;
		}
		/* Get description for follow-up code.*/
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5661);
		/*    MOVE FLUPLNB-FUPCODE        TO DESC-DESCITEM.                */
		wsaaT5661Lang.set(bsscIO.getLanguage());
		wsaaT5661Fupcode.set(fluplnbIO.getFupcode());
		descIO.setDescitem(wsaaT5661Key);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			rd01Descr.set(descIO.getLongdesc());
		}
		fluplnbIO.setFunction(varcom.nextr);
	}

protected void update3000()
	{
		/*UPDATE*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/* Write "End Of Report".*/
		z100CheckOvrflow();
		printerFile.printRh559e01(rh559E01, indicArea);
		/*  Close any open files.*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void z100CheckOvrflow()
	{
		/*Z110-CHECK*/
		/* Line count exceeds 60, skip to next page.*/
		if (isLTE(wsaaLinecnt, 60)) {
			return ;
		}
		/* Output Header.*/
		printerFile.printRh559h01(rh559H01, indicArea);
		wsaaLinecnt.set(10);
		/*Z190-EXIT*/
	}

protected void z200PrintDetail()
	{
		/*Z210-PRINT*/
		z100CheckOvrflow();
		printerFile.printRh559d01(rh559D01, indicArea);
		rh559D01.set(SPACES);
		wsaaLinecnt.add(1);
		/*Z290-EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData descrec = new FixedLengthStringData(10).init("DESCREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hpadrec = new FixedLengthStringData(10).init("HPADREC");
	private FixedLengthStringData cltsrec = new FixedLengthStringData(10).init("CLTSREC");
	private FixedLengthStringData lifelnbrec = new FixedLengthStringData(10).init("LIFELNBREC");
	private FixedLengthStringData covtlnbrec = new FixedLengthStringData(10).init("COVTLNBREC");
	private FixedLengthStringData fluplnbrec = new FixedLengthStringData(10).init("FLUPLNBREC");
}
/*
 * Class transformed  from Data Structure SQL-CHDRPF--INNER
 */
private static final class SqlChdrpfInner {

		/* SQL-CHDRPF
		 COPY DDS-ALL-FORMATS-I OF CHDRPF.                            */
	private FixedLengthStringData chdrrec = new FixedLengthStringData(35);
	private FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(chdrrec, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(chdrrec, 1);
	private FixedLengthStringData cntbranch = new FixedLengthStringData(2).isAPartOf(chdrrec, 9);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(chdrrec, 11);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(chdrrec, 14);
	private FixedLengthStringData billfreq = new FixedLengthStringData(2).isAPartOf(chdrrec, 17);
	private PackedDecimalData occdate = new PackedDecimalData(8, 0).isAPartOf(chdrrec, 19);
	private FixedLengthStringData cownpfx = new FixedLengthStringData(2).isAPartOf(chdrrec, 24);
	private FixedLengthStringData cowncoy = new FixedLengthStringData(1).isAPartOf(chdrrec, 26);
	private FixedLengthStringData cownnum = new FixedLengthStringData(8).isAPartOf(chdrrec, 27);
}
}
