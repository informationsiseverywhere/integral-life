package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: UnltpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:46
 * Class transformed from UNLTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class UnltpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 396;/*ILIFE-4036*/
	public FixedLengthStringData unltrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData unltpfRecord = unltrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(unltrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(unltrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(unltrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(unltrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(unltrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(unltrec);
	public PackedDecimalData currfrom = DD.currfrom.copy().isAPartOf(unltrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund01 = DD.ualfnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund02 = DD.ualfnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund03 = DD.ualfnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund04 = DD.ualfnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund05 = DD.ualfnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund06 = DD.ualfnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund07 = DD.ualfnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund08 = DD.ualfnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund09 = DD.ualfnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitAllocFund10 = DD.ualfnd.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt01 = DD.ualprc.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt02 = DD.ualprc.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt03 = DD.ualprc.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt04 = DD.ualprc.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt05 = DD.ualprc.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt06 = DD.ualprc.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt07 = DD.ualprc.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt08 = DD.ualprc.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt09 = DD.ualprc.copy().isAPartOf(unltrec);
	public PackedDecimalData unitAllocPercAmt10 = DD.ualprc.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund01 = DD.udafnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund02 = DD.udafnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund03 = DD.udafnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund04 = DD.udafnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund05 = DD.udafnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund06 = DD.udafnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund07 = DD.udafnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund08 = DD.udafnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund09 = DD.udafnd.copy().isAPartOf(unltrec);
	public FixedLengthStringData unitDeallocFund10 = DD.udafnd.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt01 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt02 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt03 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt04 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt05 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt06 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt07 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt08 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt09 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitDeallocPercAmt10 = DD.udalpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice01 = DD.uspcpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice02 = DD.uspcpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice03 = DD.uspcpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice04 = DD.uspcpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice05 = DD.uspcpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice06 = DD.uspcpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice07 = DD.uspcpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice08 = DD.uspcpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice09 = DD.uspcpr.copy().isAPartOf(unltrec);
	public PackedDecimalData unitSpecPrice10 = DD.uspcpr.copy().isAPartOf(unltrec);
	public FixedLengthStringData percOrAmntInd = DD.prcamtind.copy().isAPartOf(unltrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(unltrec);
	public FixedLengthStringData premTopupInd = DD.ptopup.copy().isAPartOf(unltrec);
	public PackedDecimalData seqnbr = DD.seqnbr.copy().isAPartOf(unltrec);
	public PackedDecimalData numapp = DD.numapp.copy().isAPartOf(unltrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(unltrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(unltrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(unltrec);
	public FixedLengthStringData fndspl = DD.fndspl.copy().isAPartOf(unltrec);
	

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public UnltpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for UnltpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public UnltpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for UnltpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public UnltpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for UnltpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public UnltpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("UNLTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"VALIDFLAG, " +
							"CURRFROM, " +
							"TRANNO, " +
							"UALFND01, " +
							"UALFND02, " +
							"UALFND03, " +
							"UALFND04, " +
							"UALFND05, " +
							"UALFND06, " +
							"UALFND07, " +
							"UALFND08, " +
							"UALFND09, " +
							"UALFND10, " +
							"UALPRC01, " +
							"UALPRC02, " +
							"UALPRC03, " +
							"UALPRC04, " +
							"UALPRC05, " +
							"UALPRC06, " +
							"UALPRC07, " +
							"UALPRC08, " +
							"UALPRC09, " +
							"UALPRC10, " +
							"UDAFND01, " +
							"UDAFND02, " +
							"UDAFND03, " +
							"UDAFND04, " +
							"UDAFND05, " +
							"UDAFND06, " +
							"UDAFND07, " +
							"UDAFND08, " +
							"UDAFND09, " +
							"UDAFND10, " +
							"UDALPR01, " +
							"UDALPR02, " +
							"UDALPR03, " +
							"UDALPR04, " +
							"UDALPR05, " +
							"UDALPR06, " +
							"UDALPR07, " +
							"UDALPR08, " +
							"UDALPR09, " +
							"UDALPR10, " +
							"USPCPR01, " +
							"USPCPR02, " +
							"USPCPR03, " +
							"USPCPR04, " +
							"USPCPR05, " +
							"USPCPR06, " +
							"USPCPR07, " +
							"USPCPR08, " +
							"USPCPR09, " +
							"USPCPR10, " +
							"PRCAMTIND, " +
							"CURRTO, " +
							"PTOPUP, " +
							"SEQNBR, " +
							"NUMAPP, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"FNDSPL, " +/*ILIFE-4036*/
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     validflag,
                                     currfrom,
                                     tranno,
                                     unitAllocFund01,
                                     unitAllocFund02,
                                     unitAllocFund03,
                                     unitAllocFund04,
                                     unitAllocFund05,
                                     unitAllocFund06,
                                     unitAllocFund07,
                                     unitAllocFund08,
                                     unitAllocFund09,
                                     unitAllocFund10,
                                     unitAllocPercAmt01,
                                     unitAllocPercAmt02,
                                     unitAllocPercAmt03,
                                     unitAllocPercAmt04,
                                     unitAllocPercAmt05,
                                     unitAllocPercAmt06,
                                     unitAllocPercAmt07,
                                     unitAllocPercAmt08,
                                     unitAllocPercAmt09,
                                     unitAllocPercAmt10,
                                     unitDeallocFund01,
                                     unitDeallocFund02,
                                     unitDeallocFund03,
                                     unitDeallocFund04,
                                     unitDeallocFund05,
                                     unitDeallocFund06,
                                     unitDeallocFund07,
                                     unitDeallocFund08,
                                     unitDeallocFund09,
                                     unitDeallocFund10,
                                     unitDeallocPercAmt01,
                                     unitDeallocPercAmt02,
                                     unitDeallocPercAmt03,
                                     unitDeallocPercAmt04,
                                     unitDeallocPercAmt05,
                                     unitDeallocPercAmt06,
                                     unitDeallocPercAmt07,
                                     unitDeallocPercAmt08,
                                     unitDeallocPercAmt09,
                                     unitDeallocPercAmt10,
                                     unitSpecPrice01,
                                     unitSpecPrice02,
                                     unitSpecPrice03,
                                     unitSpecPrice04,
                                     unitSpecPrice05,
                                     unitSpecPrice06,
                                     unitSpecPrice07,
                                     unitSpecPrice08,
                                     unitSpecPrice09,
                                     unitSpecPrice10,
                                     percOrAmntInd,
                                     currto,
                                     premTopupInd,
                                     seqnbr,
                                     numapp,
                                     userProfile,
                                     jobName,
                                     datime,
                                     fndspl,/*ILIFE-4036*/
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		validflag.clear();
  		currfrom.clear();
  		tranno.clear();
  		unitAllocFund01.clear();
  		unitAllocFund02.clear();
  		unitAllocFund03.clear();
  		unitAllocFund04.clear();
  		unitAllocFund05.clear();
  		unitAllocFund06.clear();
  		unitAllocFund07.clear();
  		unitAllocFund08.clear();
  		unitAllocFund09.clear();
  		unitAllocFund10.clear();
  		unitAllocPercAmt01.clear();
  		unitAllocPercAmt02.clear();
  		unitAllocPercAmt03.clear();
  		unitAllocPercAmt04.clear();
  		unitAllocPercAmt05.clear();
  		unitAllocPercAmt06.clear();
  		unitAllocPercAmt07.clear();
  		unitAllocPercAmt08.clear();
  		unitAllocPercAmt09.clear();
  		unitAllocPercAmt10.clear();
  		unitDeallocFund01.clear();
  		unitDeallocFund02.clear();
  		unitDeallocFund03.clear();
  		unitDeallocFund04.clear();
  		unitDeallocFund05.clear();
  		unitDeallocFund06.clear();
  		unitDeallocFund07.clear();
  		unitDeallocFund08.clear();
  		unitDeallocFund09.clear();
  		unitDeallocFund10.clear();
  		unitDeallocPercAmt01.clear();
  		unitDeallocPercAmt02.clear();
  		unitDeallocPercAmt03.clear();
  		unitDeallocPercAmt04.clear();
  		unitDeallocPercAmt05.clear();
  		unitDeallocPercAmt06.clear();
  		unitDeallocPercAmt07.clear();
  		unitDeallocPercAmt08.clear();
  		unitDeallocPercAmt09.clear();
  		unitDeallocPercAmt10.clear();
  		unitSpecPrice01.clear();
  		unitSpecPrice02.clear();
  		unitSpecPrice03.clear();
  		unitSpecPrice04.clear();
  		unitSpecPrice05.clear();
  		unitSpecPrice06.clear();
  		unitSpecPrice07.clear();
  		unitSpecPrice08.clear();
  		unitSpecPrice09.clear();
  		unitSpecPrice10.clear();
  		percOrAmntInd.clear();
  		currto.clear();
  		premTopupInd.clear();
  		seqnbr.clear();
  		numapp.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		fndspl.clear();/*ILIFE-4036*/
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getUnltrec() {
  		return unltrec;
	}

	public FixedLengthStringData getUnltpfRecord() {
  		return unltpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setUnltrec(what);
	}

	public void setUnltrec(Object what) {
  		this.unltrec.set(what);
	}

	public void setUnltpfRecord(Object what) {
  		this.unltpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(unltrec.getLength());
		result.set(unltrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}