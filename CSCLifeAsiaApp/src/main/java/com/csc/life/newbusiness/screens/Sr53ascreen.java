package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 12/3/13 4:15 AM
 * @author CSC
 */
public class Sr53ascreen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {8, 23, 11, 73}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr53aScreenVars sv = (Sr53aScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr53ascreenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr53aScreenVars screenVars = (Sr53aScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.currcode.setClassString("");
		screenVars.numsel.setClassString("");
		screenVars.billcdDisp.setClassString("");
		screenVars.payrnum.setClassString("");
		screenVars.payorname.setClassString("");
		screenVars.ccmndref.setClassString("");
		screenVars.facthous.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.bankkey.setClassString("");
		screenVars.bankdesc.setClassString("");
		screenVars.branchdesc.setClassString("");
		screenVars.bankacckey.setClassString("");
		screenVars.bankaccdsc.setClassString("");
		screenVars.cdlongdesc.setClassString("");
		screenVars.mandstat.setClassString("");
		screenVars.effdateDisp.setClassString("");
	}

/**
 * Clear all the variables in Sr53ascreen
 */
	public static void clear(VarModel pv) {
		Sr53aScreenVars screenVars = (Sr53aScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.currcode.clear();
		screenVars.numsel.clear();
		screenVars.billcdDisp.clear();
		screenVars.billcd.clear();
		screenVars.payrnum.clear();
		screenVars.payorname.clear();
		screenVars.ccmndref.clear();
		screenVars.facthous.clear();
		screenVars.longdesc.clear();
		screenVars.bankkey.clear();
		screenVars.bankdesc.clear();
		screenVars.branchdesc.clear();
		screenVars.bankacckey.clear();
		screenVars.bankaccdsc.clear();
		screenVars.cdlongdesc.clear();
		screenVars.mandstat.clear();
		screenVars.effdateDisp.clear();
	}
}
