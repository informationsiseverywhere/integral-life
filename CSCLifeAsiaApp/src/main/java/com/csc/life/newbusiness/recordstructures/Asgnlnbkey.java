package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:18
 * Description:
 * Copybook name: ASGNLNBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Asgnlnbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData asgnlnbFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData asgnlnbKey = new FixedLengthStringData(256).isAPartOf(asgnlnbFileKey, 0, REDEFINE);
  	public FixedLengthStringData asgnlnbChdrcoy = new FixedLengthStringData(1).isAPartOf(asgnlnbKey, 0);
  	public FixedLengthStringData asgnlnbChdrnum = new FixedLengthStringData(8).isAPartOf(asgnlnbKey, 1);
  	public PackedDecimalData asgnlnbSeqno = new PackedDecimalData(2, 0).isAPartOf(asgnlnbKey, 9);
  	public FixedLengthStringData asgnlnbAsgnnum = new FixedLengthStringData(8).isAPartOf(asgnlnbKey, 11);
  	public FixedLengthStringData filler = new FixedLengthStringData(237).isAPartOf(asgnlnbKey, 19, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(asgnlnbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		asgnlnbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}