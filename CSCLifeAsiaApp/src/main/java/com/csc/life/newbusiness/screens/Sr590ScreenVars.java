package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

/**
 * Screen variables for SR590
 * @version 1.0 generated on 30/08/09 07:21
 * @author Quipoz
 */
public class Sr590ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(263);
	public FixedLengthStringData dataFields = new FixedLengthStringData(151).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData cownnum = DD.cownnum.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData ctypedes = DD.ctypedes.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData ownername = DD.ownername.copy().isAPartOf(dataFields,49);
	public FixedLengthStringData zdocname = DD.zdocname.copy().isAPartOf(dataFields,96);
	public FixedLengthStringData zdoctor = DD.zdoctor.copy().isAPartOf(dataFields,143);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(28).isAPartOf(dataArea, 151);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData cnttypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData cownnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData ctypedesErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData ownernameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData zdocnameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData zdoctorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(84).isAPartOf(dataArea, 179);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] cnttypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] cownnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] ctypedesOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] ownernameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] zdocnameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] zdoctorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);

	public FixedLengthStringData subfileArea = new FixedLengthStringData(397);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(107).isAPartOf(subfileArea, 0);
	public FixedLengthStringData actn = DD.actn.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData crtdate = DD.crtdate.copyToZonedDecimal().isAPartOf(subfileFields,1);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(subfileFields,9);
	public ZonedDecimalData exprdate = DD.exprdate.copyToZonedDecimal().isAPartOf(subfileFields,19);
	public FixedLengthStringData fupcdes = DD.fupcdes.copy().isAPartOf(subfileFields,27);
	public ZonedDecimalData fupremdt = DD.fupdt.copyToZonedDecimal().isAPartOf(subfileFields,31);
	public ZonedDecimalData fupno = DD.fupno.copyToZonedDecimal().isAPartOf(subfileFields,39);
	public ZonedDecimalData fuprcvd = DD.fuprcvd.copyToZonedDecimal().isAPartOf(subfileFields,41);
	public FixedLengthStringData fupremk = DD.fuprmk.copy().isAPartOf(subfileFields,49);
	public FixedLengthStringData fupstat = DD.fupsts.copy().isAPartOf(subfileFields,89);
	public FixedLengthStringData fuptype = DD.fuptyp.copy().isAPartOf(subfileFields,90);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(subfileFields,91);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(subfileFields,92);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(subfileFields,94);
	public ZonedDecimalData lifeno = DD.lifeno.copyToZonedDecimal().isAPartOf(subfileFields,95);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,97);
	public FixedLengthStringData uprflag = DD.uprflag.copy().isAPartOf(subfileFields,98);
	public FixedLengthStringData zitem = DD.zitem.copy().isAPartOf(subfileFields,99);
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(72).isAPartOf(subfileArea, 107);
	public FixedLengthStringData actnErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData crtdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData crtuserErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData exprdateErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData fupcdesErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData fupdtErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData fupnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData fuprcvdErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	public FixedLengthStringData fuprmkErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 32);
	public FixedLengthStringData fupstsErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 36);
	public FixedLengthStringData fuptypErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 40);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 44);
	public FixedLengthStringData jlifeErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 48);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 52);
	public FixedLengthStringData lifenoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 56);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 60);
	public FixedLengthStringData uprflagErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 64);
	public FixedLengthStringData zitemErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 68);
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(216).isAPartOf(subfileArea, 179);
	public FixedLengthStringData[] actnOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] crtdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] crtuserOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] exprdateOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] fupcdesOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] fupdtOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] fupnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 72);
	public FixedLengthStringData[] fuprcvdOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 84);
	public FixedLengthStringData[] fuprmkOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 96);
	public FixedLengthStringData[] fupstsOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 108);
	public FixedLengthStringData[] fuptypOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 120);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 132);
	public FixedLengthStringData[] jlifeOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 144);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 156);
	public FixedLengthStringData[] lifenoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 168);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 180);
	public FixedLengthStringData[] uprflagOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 192);
	public FixedLengthStringData[] zitemOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 204);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 395);
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public FixedLengthStringData crtdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData exprdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fupremdtDisp = new FixedLengthStringData(10);
	public FixedLengthStringData fuprcvdDisp = new FixedLengthStringData(10);

	public LongData Sr590screensflWritten = new LongData(0);
	public LongData Sr590screenctlWritten = new LongData(0);
	public LongData Sr590screenWritten = new LongData(0);
	public LongData Sr590protectWritten = new LongData(0);
	public GeneralTable sr590screensfl = new GeneralTable(AppVars.getInstance());

	public boolean hasSubfile() {
		return true;
	}

	public GeneralTable getScreenSubfileTable() {
		return sr590screensfl;
	}

	public Sr590ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(fupcdesOut,new String[] {"30","38","-30",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(lifenoOut,new String[] {"31","38","-31",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(jlifeOut,new String[] {"36","38","-36",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupstsOut,new String[] {"32","38","-32",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fupdtOut,new String[] {"33","38","-33",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtdateOut,new String[] {null, null, null, "40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuprmkOut,new String[] {"34","38","-34",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(selectOut,new String[] {"39","38","-39",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuptypOut,new String[] {null, null, null, "40",null, null, null, null, null, null, null, null});
		fieldIndMap.put(fuprcvdOut,new String[] {"41","38","-41",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(exprdateOut,new String[] {"42","38","-42",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zdoctorOut,new String[] {"35","38","-35",null, null, null, null, null, null, null, null, null});
		screenSflFields = new BaseData[] {fupno, uprflag, zitem, indic, crtuser, language, fupcdes, lifeno, jlife, fupstat, fupremdt, crtdate, fupremk, actn, select, fuptype, fuprcvd, exprdate};
		screenSflOutFields = new BaseData[][] {fupnoOut, uprflagOut, zitemOut, indicOut, crtuserOut, languageOut, fupcdesOut, lifenoOut, jlifeOut, fupstsOut, fupdtOut, crtdateOut, fuprmkOut, actnOut, selectOut, fuptypOut, fuprcvdOut, exprdateOut};
		screenSflErrFields = new BaseData[] {fupnoErr, uprflagErr, zitemErr, indicErr, crtuserErr, languageErr, fupcdesErr, lifenoErr, jlifeErr, fupstsErr, fupdtErr, crtdateErr, fuprmkErr, actnErr, selectErr, fuptypErr, fuprcvdErr, exprdateErr};
		screenSflDateFields = new BaseData[] {fupremdt, crtdate, fuprcvd, exprdate};
		screenSflDateErrFields = new BaseData[] {fupdtErr, crtdateErr, fuprcvdErr, exprdateErr};
		screenSflDateDispFields = new BaseData[] {fupremdtDisp, crtdateDisp, fuprcvdDisp, exprdateDisp};

		screenFields = new BaseData[] {chdrnum, cnttype, ctypedes, cownnum, ownername, zdoctor, zdocname};
		screenOutFields = new BaseData[][] {chdrnumOut, cnttypeOut, ctypedesOut, cownnumOut, ownernameOut, zdoctorOut, zdocnameOut};
		screenErrFields = new BaseData[] {chdrnumErr, cnttypeErr, ctypedesErr, cownnumErr, ownernameErr, zdoctorErr, zdocnameErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		screenRecord = Sr590screen.class;
		screenSflRecord = Sr590screensfl.class;
		screenCtlRecord = Sr590screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sr590protect.class;
	}

	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sr590screenctl.lrec.pageSubfile);
	}
}
