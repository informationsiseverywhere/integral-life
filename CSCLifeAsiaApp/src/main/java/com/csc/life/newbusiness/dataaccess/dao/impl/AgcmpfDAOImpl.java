package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.AgcmpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgcmpfDAOImpl extends BaseDAOImpl<Agcmpf> implements AgcmpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(AgcmpfDAOImpl.class);

    public Map<String, List<Agcmpf>> searchAgcmpf(String coy, List<String> chdrnumList) {
        StringBuilder sql = new StringBuilder();
        sql.append(
                "SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,USER_T,CRTABLE,CURRFROM,CURRTO,VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG FROM AGCMPF WHERE CHDRCOY=? AND ")
                .append(getSqlInStr("CHDRNUM", chdrnumList))
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,COVERAGE ASC, RIDER ASC,PLNSFX ASC,SEQNO ASC, AGNTNUM ASC,UNIQUE_NUMBER ASC ");
        PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        Map<String, List<Agcmpf>> agcmpfMap = new HashMap<String, List<Agcmpf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
                Agcmpf agcmpf = new Agcmpf();
                agcmpf.setUniqueNumber(rs.getLong(1));
                agcmpf.setChdrcoy(rs.getString(2));
                agcmpf.setChdrnum(rs.getString(3));
                agcmpf.setAgntnum(rs.getString(4));
                agcmpf.setLife(rs.getString(5));
                agcmpf.setJlife(rs.getString(6));
                agcmpf.setCoverage(rs.getString(7));
                agcmpf.setRider(rs.getString(8));
                agcmpf.setPlanSuffix(rs.getInt(9));
                agcmpf.setTranno(rs.getInt(10));
                agcmpf.setEfdate(rs.getInt(11));
                agcmpf.setAnnprem(rs.getBigDecimal(12));
                agcmpf.setBasicCommMeth(rs.getString(13));
                agcmpf.setInitcom(rs.getBigDecimal(14));
                agcmpf.setBascpy(rs.getString(15));
                agcmpf.setCompay(rs.getBigDecimal(16));
                agcmpf.setComern(rs.getBigDecimal(17));
                agcmpf.setSrvcpy(rs.getString(18));
                agcmpf.setScmdue(rs.getBigDecimal(19));
                agcmpf.setScmearn(rs.getBigDecimal(20));
                agcmpf.setRnwcpy(rs.getString(21));
                agcmpf.setRnlcdue(rs.getBigDecimal(22));
                agcmpf.setRnlcearn(rs.getBigDecimal(23));
                agcmpf.setAgentClass(rs.getString(24));
                agcmpf.setTermid(rs.getString(25));
                agcmpf.setTransactionDate(rs.getInt(26));
                agcmpf.setTransactionTime(rs.getInt(27));
                agcmpf.setUser(rs.getInt(28));
                agcmpf.setCrtable(rs.getString(29));
                agcmpf.setCurrfrom(rs.getInt(30));
                agcmpf.setCurrto(rs.getInt(31));
                agcmpf.setValidflag(rs.getString(32));
                agcmpf.setSeqno(rs.getInt(33));
                agcmpf.setPtdate(rs.getInt(34));
                agcmpf.setCedagent(rs.getString(35));
                agcmpf.setOvrdcat(rs.getString(36));
                agcmpf.setDormantFlag(rs.getString(37));

                String chdrnum = agcmpf.getChdrnum();
                if (agcmpfMap.containsKey(chdrnum)) {
                    agcmpfMap.get(chdrnum).add(agcmpf);
                } else {
                    List<Agcmpf> agcmpfSearchResult = new LinkedList<Agcmpf>();
                    agcmpfSearchResult.add(agcmpf);
                    agcmpfMap.put(chdrnum, agcmpfSearchResult);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchAgcmpf()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return agcmpfMap;
    }
 public Agcmpf searchAgcmRecord(Agcmpf agcmpf) throws SQLRuntimeException{
		StringBuilder sqlSelect = new StringBuilder();
		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, AGNTNUM, LIFE, JLIFE, ");
		sqlSelect.append("COVERAGE, RIDER, PLNSFX,AGNTNUM,CEDAGENT ");
		sqlSelect.append("FROM AGCM WHERE LTRIM(RTRIM(CHDRCOY)) = ? AND LTRIM(RTRIM(CHDRNUM)) = ? ");
		if(agcmpf.getLife() != null && agcmpf.getCoverage() != null && agcmpf.getRider() != null
				&& agcmpf.getPlanSuffix() >=0 && agcmpf.getAgntnum() != null)
		{
			sqlSelect.append(" AND LIFE = ? AND COVERAGE = ? AND RIDER = ? AND PLNSFX = ? AND LTRIM(RTRIM(Agntnum)) = ? ");
		}
		sqlSelect.append("ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, RIDER ASC, PLNSFX ASC, SEQNO ASC, UNIQUE_NUMBER DESC ");
		PreparedStatement psAgcmpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlAgcmpfRs = null;
		try {

			psAgcmpfSelect.setString(1, agcmpf.getChdrcoy());
			psAgcmpfSelect.setString(2, agcmpf.getChdrnum());
			if(agcmpf.getLife() != null && agcmpf.getCoverage() != null && agcmpf.getRider() != null
					&& agcmpf.getPlanSuffix() >=0 && agcmpf.getAgntnum() != null)
			{
				psAgcmpfSelect.setString(3, agcmpf.getLife());
				psAgcmpfSelect.setString(4, agcmpf.getCoverage());
				psAgcmpfSelect.setString(5, agcmpf.getAgntnum());
				psAgcmpfSelect.setInt(6, agcmpf.getPlanSuffix());
				psAgcmpfSelect.setString(7, agcmpf.getAgntnum());
			}
			sqlAgcmpfRs = executeQuery(psAgcmpfSelect);

			while (sqlAgcmpfRs.next()) {
				agcmpf = new Agcmpf();
				agcmpf.setUniqueNumber(sqlAgcmpfRs.getInt("UNIQUE_NUMBER"));
				agcmpf.setChdrcoy(sqlAgcmpfRs.getString("CHDRCOY"));
				agcmpf.setChdrnum(sqlAgcmpfRs.getString("CHDRNUM"));
				agcmpf.setAgntnum(sqlAgcmpfRs.getString("AGNTNUM"));
				agcmpf.setLife(sqlAgcmpfRs.getString("LIFE"));
				agcmpf.setJlife(sqlAgcmpfRs.getString("JLIFE"));
				agcmpf.setCoverage(sqlAgcmpfRs.getString("COVERAGE"));
				agcmpf.setRider(sqlAgcmpfRs.getString("RIDER"));
				agcmpf.setPlanSuffix(sqlAgcmpfRs.getInt("PLNSFX"));
				agcmpf.setAgntnum(sqlAgcmpfRs.getString("AGNTNUM"));
				agcmpf.setCedagent(sqlAgcmpfRs.getString("CEDAGENT"));
				
			}

		} catch (SQLException e) {
			LOGGER.error("searchAgcmpfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psAgcmpfSelect, sqlAgcmpfRs);
		}

		return agcmpf;
	}
    public void updateAgcmByUniqNum(List<Agcmpf> agList) {
        if (agList != null && agList.size() > 0) {
            StringBuilder sb = new StringBuilder();
            sb.append("UPDATE AGCMPF SET PTDATE=?,JOBNM=?,USRPRF=?,DATIME=?,COMPAY=?,COMERN=?,SCMEARN=?,SCMDUE=?,RNLCDUE=?,RNLCEARN=?,INITCOMMGST=?,RNWLCOMMGST=?,ANNPREM=?,VALIDFLAG=?,CURRTO=? WHERE UNIQUE_NUMBER=? ");
            PreparedStatement ps = getPrepareStatement(sb.toString());
            try {
                for (Agcmpf a : agList) {
                    ps.setInt(1, a.getPtdate());
                    ps.setString(2, getJobnm());
                    ps.setString(3, getUsrprf());
                    ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
                    ps.setBigDecimal(5, a.getCompay());
                    ps.setBigDecimal(6, a.getComern());
                    ps.setBigDecimal(7, a.getScmearn());
                    ps.setBigDecimal(8, a.getScmdue());
                    ps.setBigDecimal(9, a.getRnlcdue());
                    ps.setBigDecimal(10, a.getRnlcearn());
                    ps.setBigDecimal(11, a.getInitCommGst());
                    ps.setBigDecimal(12, a.getRnwlCommGst());
                    ps.setBigDecimal(13, a.getAnnprem());//IJS-424
                    ps.setString(14, a.getValidflag());
                    ps.setInt(15, a.getCurrto());
                    ps.setLong(16, a.getUniqueNumber());
                    ps.addBatch();
                }
                ps.executeBatch();
            } catch (SQLException e) {
                LOGGER.error("updateAgcmByUniqNum()", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(ps, null);
            }
        }
    }
    

	@Override
	public List<Agcmpf> readAgcmpfData(String chdrcoy, String chdrnum, String agntnum) {
		
		StringBuilder sql = new StringBuilder();
        sql.append(
                "SELECT * FROM AGCMPF WHERE CHDRCOY=? AND CHDRNUM=? AND AGNTNUM=? ORDER BY UNIQUE_NUMBER DESC ");
        PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        List<Agcmpf> agcmpfList = new ArrayList<>();
        try {
        	ps.setString(1, chdrcoy);
			ps.setString(2, chdrnum);
			ps.setString(3, agntnum);
            rs = executeQuery(ps);

            while (rs.next()) {
                Agcmpf agcmpf = new Agcmpf();
                agcmpf.setUniqueNumber(rs.getLong(1));
                agcmpf.setChdrcoy(rs.getString(2));
                agcmpf.setChdrnum(rs.getString(3));
                agcmpf.setAgntnum(rs.getString(4));
                agcmpf.setLife(rs.getString(5));
                agcmpf.setJlife(rs.getString(6));
                agcmpf.setCoverage(rs.getString(7));
                agcmpf.setRider(rs.getString(8));
                agcmpf.setPlanSuffix(rs.getInt(9));
                agcmpf.setTranno(rs.getInt(10));
                agcmpf.setEfdate(rs.getInt(11));
                agcmpf.setAnnprem(rs.getBigDecimal(12));
                agcmpf.setBasicCommMeth(rs.getString(13));
                agcmpf.setInitcom(rs.getBigDecimal(14));
                agcmpf.setBascpy(rs.getString(15));
                agcmpf.setCompay(rs.getBigDecimal(16));
                agcmpf.setComern(rs.getBigDecimal(17));
                agcmpf.setSrvcpy(rs.getString(18));
                agcmpf.setScmdue(rs.getBigDecimal(19));
                agcmpf.setScmearn(rs.getBigDecimal(20));
                agcmpf.setRnwcpy(rs.getString(21));
                agcmpf.setRnlcdue(rs.getBigDecimal(22));
                agcmpf.setRnlcearn(rs.getBigDecimal(23));
                agcmpf.setAgentClass(rs.getString(24));
                agcmpf.setTermid(rs.getString(25));
                agcmpf.setTransactionDate(rs.getInt(26));
                agcmpf.setTransactionTime(rs.getInt(27));
                agcmpf.setUser(rs.getInt(28));
                agcmpf.setCrtable(rs.getString(29));
                agcmpf.setCurrfrom(rs.getInt(30));
                agcmpf.setCurrto(rs.getInt(31));
                agcmpf.setValidflag(rs.getString(32));
                agcmpf.setSeqno(rs.getInt(33));
                agcmpf.setPtdate(rs.getInt(34));
                agcmpf.setCedagent(rs.getString(35));
                agcmpf.setOvrdcat(rs.getString(36));
                agcmpf.setDormantFlag(rs.getString(37));
                agcmpfList.add(agcmpf);    
            }
        }
        catch (SQLException e) {
            LOGGER.error("readAgcmpfData()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } 
        finally {
            close(ps, rs);
        }
        return agcmpfList;
	}
	
    public Map<String, List<Agcmpf>> searchAgcmrnl(String coy, List<String> chdrnumList) {
        StringBuilder sql = new StringBuilder();
        sql.append(
                "SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,USER_T,CRTABLE,CURRFROM,CURRTO,VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG,INITCOMMGST,RNWLCOMMGST FROM AGCMPF ")
        		.append(" WHERE CHDRCOY=? ")
        		//.append (" AND VALIDFLAG = '1' AND DORMFLAG <> 'Y' OR RTRIM(LTRIM(VALIDFLAG)) = '' AND DORMFLAG <> 'Y' AND ")
        		.append (" AND VALIDFLAG = '1' AND DORMFLAG <> 'Y' AND ")
                .append(getSqlInStr("CHDRNUM", chdrnumList))
                //.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,COVERAGE ASC, RIDER ASC,PLNSFX ASC,SEQNO ASC, AGNTNUM ASC,UNIQUE_NUMBER ASC ");
                .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,COVERAGE ASC, RIDER ASC,PLNSFX ASC,SEQNO ASC, AGNTNUM ASC ");
        PreparedStatement ps = getPrepareStatement(sql.toString());
        ResultSet rs = null;
        Map<String, List<Agcmpf>> agcmpfMap = new HashMap<String, List<Agcmpf>>();
        try {
            ps.setInt(1, Integer.parseInt(coy));
            rs = executeQuery(ps);

            while (rs.next()) {
                Agcmpf agcmpf = new Agcmpf();
                agcmpf.setUniqueNumber(rs.getLong(1));
                agcmpf.setChdrcoy(rs.getString(2));
                agcmpf.setChdrnum(rs.getString(3));
                agcmpf.setAgntnum(rs.getString(4));
                agcmpf.setLife(rs.getString(5));
                agcmpf.setJlife(rs.getString(6));
                agcmpf.setCoverage(rs.getString(7));
                agcmpf.setRider(rs.getString(8));
                agcmpf.setPlanSuffix(rs.getInt(9));
                agcmpf.setTranno(rs.getInt(10));
                agcmpf.setEfdate(rs.getInt(11));
                agcmpf.setAnnprem(rs.getBigDecimal(12));
                agcmpf.setBasicCommMeth(rs.getString(13));
                agcmpf.setInitcom(rs.getBigDecimal(14));
                agcmpf.setBascpy(rs.getString(15));
                agcmpf.setCompay(rs.getBigDecimal(16));
                agcmpf.setComern(rs.getBigDecimal(17));
                agcmpf.setSrvcpy(rs.getString(18));
                agcmpf.setScmdue(rs.getBigDecimal(19));
                agcmpf.setScmearn(rs.getBigDecimal(20));
                agcmpf.setRnwcpy(rs.getString(21));
                agcmpf.setRnlcdue(rs.getBigDecimal(22));
                agcmpf.setRnlcearn(rs.getBigDecimal(23));
                agcmpf.setAgentClass(rs.getString(24));
                agcmpf.setTermid(rs.getString(25));
                agcmpf.setTransactionDate(rs.getInt(26));
                agcmpf.setTransactionTime(rs.getInt(27));
                agcmpf.setUser(rs.getInt(28));
                agcmpf.setCrtable(rs.getString(29));
                agcmpf.setCurrfrom(rs.getInt(30));
                agcmpf.setCurrto(rs.getInt(31));
                agcmpf.setValidflag(rs.getString(32));
                agcmpf.setSeqno(rs.getInt(33));
                agcmpf.setPtdate(rs.getInt(34));
                agcmpf.setCedagent(rs.getString(35));
                agcmpf.setOvrdcat(rs.getString(36));
                agcmpf.setDormantFlag(rs.getString(37));
                agcmpf.setInitCommGst(rs.getBigDecimal(38));
                agcmpf.setRnwlCommGst(rs.getBigDecimal(39));

                String chdrnum = agcmpf.getChdrnum();
                if (agcmpfMap.containsKey(chdrnum)) {
                    agcmpfMap.get(chdrnum).add(agcmpf);
                } else {
                    List<Agcmpf> agcmpfSearchResult = new LinkedList<Agcmpf>();
                    agcmpfSearchResult.add(agcmpf);
                    agcmpfMap.put(chdrnum, agcmpfSearchResult);
                }
            }

        } catch (SQLException e) {
            LOGGER.error("searchAgcmpf()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, rs);
        }
        return agcmpfMap;
    }
    
	public void insertAgcmpfList(List<Agcmpf> agcmpfList) {
		StringBuilder sb = new StringBuilder("");
		sb.append("INSERT INTO AGCMPF(CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,CRTABLE,CURRFROM,CURRTO,VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG, USER_T, USRPRF, JOBNM, DATIME, INITCOMMGST, RNWLCOMMGST) ");
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps = null;
		try {
			ps = getPrepareStatement(sb.toString());
			for (Agcmpf agcmpf : agcmpfList) {
				ps.setString(1, agcmpf.getChdrcoy());
				ps.setString(2, agcmpf.getChdrnum());
				ps.setString(3, agcmpf.getAgntnum());
				ps.setString(4, agcmpf.getLife());
				ps.setString(5, agcmpf.getJlife());
				ps.setString(6, agcmpf.getCoverage());
				ps.setString(7, agcmpf.getRider());
				ps.setInt(8, agcmpf.getPlanSuffix());
				ps.setInt(9, agcmpf.getTranno());
				ps.setInt(10, agcmpf.getEfdate());
				ps.setBigDecimal(11, agcmpf.getAnnprem());
				ps.setString(12, agcmpf.getBasicCommMeth());
				ps.setBigDecimal(13, agcmpf.getInitcom());
				ps.setString(14, agcmpf.getBascpy());
				ps.setBigDecimal(15, agcmpf.getCompay());
				ps.setBigDecimal(16, agcmpf.getComern());
				ps.setString(17, agcmpf.getSrvcpy());
				ps.setBigDecimal(18, agcmpf.getScmdue());
				ps.setBigDecimal(19, agcmpf.getScmearn());
				ps.setString(20, agcmpf.getRnwcpy());
				ps.setBigDecimal(21, agcmpf.getRnlcdue());
				ps.setBigDecimal(22, agcmpf.getRnlcearn());
				ps.setString(23, agcmpf.getAgentClass());
				ps.setString(24, agcmpf.getTermid());
				ps.setInt(25, agcmpf.getTransactionDate());
				ps.setInt(26, agcmpf.getTransactionTime());
				ps.setString(27, agcmpf.getCrtable());
				ps.setInt(28, agcmpf.getCurrfrom());
				ps.setInt(29, agcmpf.getCurrto());
				ps.setString(30, agcmpf.getValidflag());
				ps.setInt(31, agcmpf.getSeqno());
				ps.setInt(32, agcmpf.getPtdate());
				ps.setString(33, agcmpf.getCedagent());
				ps.setString(34, agcmpf.getOvrdcat());
				ps.setString(35, agcmpf.getDormantFlag());
				ps.setInt(36, agcmpf.getUser());
				ps.setString(37, this.getUsrprf());
				ps.setString(38, this.getJobnm());
				ps.setTimestamp(39, getDatime());
				ps.setBigDecimal(40, agcmpf.getInitCommGst());
				ps.setBigDecimal(41, agcmpf.getRnwlCommGst());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("insertAgcmpfList()", e);//IJTI-1561
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	 public void updateInvalidAgcmByUniqNum(List<Agcmpf> agList) {
	        if (agList != null && agList.size() > 0) {
	            StringBuilder sb = new StringBuilder();
	            sb.append("UPDATE AGCMPF SET VALIDFLAG=?,CURRTO=? WHERE UNIQUE_NUMBER=? ");
	            PreparedStatement ps = getPrepareStatement(sb.toString());
	            try {
	                for (Agcmpf a : agList) {
	                    ps.setString(1, a.getValidflag());
	                    ps.setInt(2, a.getCurrto());
	                    ps.setLong(3, a.getUniqueNumber());
	                    ps.addBatch();
	                }
	                ps.executeBatch();
	            } catch (SQLException e) {
	                LOGGER.error("updateAgcmByUniqNum()", e);//IJTI-1561
	                throw new SQLRuntimeException(e);
	            } finally {
	                close(ps, null);
	            }
	        }
	 }
}
