package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S5306
 * @version 1.0 generated on 30/08/09 06:39
 * @author Quipoz
 */
public class S5306ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(57);
	public FixedLengthStringData dataFields = new FixedLengthStringData(25).isAPartOf(dataArea, 0);
	public FixedLengthStringData deltkey = DD.deltkey.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(dataFields,24);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(8).isAPartOf(dataArea, 25);
	public FixedLengthStringData deltkeyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 33);
	public FixedLengthStringData[] deltkeyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S5306screenWritten = new LongData(0);
	public LongData S5306windowWritten = new LongData(0);
	public LongData S5306protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S5306ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {deltkey, select};
		screenOutFields = new BaseData[][] {deltkeyOut, selectOut};
		screenErrFields = new BaseData[] {deltkeyErr, selectErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S5306screen.class;
		protectRecord = S5306protect.class;
	}

}
