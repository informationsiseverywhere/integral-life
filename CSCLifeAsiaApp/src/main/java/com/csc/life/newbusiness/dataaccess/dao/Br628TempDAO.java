package com.csc.life.newbusiness.dataaccess.dao;

import com.csc.life.newbusiness.dataaccess.model.Br628DTO;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br628TempDAO extends BaseDAO<Br628DTO> {
	int buildTempData(Br628DTO dto);
}
