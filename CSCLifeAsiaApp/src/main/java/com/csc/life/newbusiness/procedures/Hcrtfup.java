/*
 * File: Hcrtfup.java
 * Date: 29 August 2009 22:51:52
 * Author: Quipoz Limited
 * 
 * Class transformed from HCRTFUP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.dataaccess.dao.FluppfDAO;
import com.csc.fsu.clients.dataaccess.model.Fluppf;
import com.csc.fsu.general.dataaccess.dao.ItempfDAO;
import com.csc.fsu.general.dataaccess.model.Itempf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.ZfupcdeTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.CovtpfDAO;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.life.newbusiness.dataaccess.model.Covtpf;
import com.csc.life.newbusiness.recordstructures.Hcrtfuprec;
import com.csc.life.newbusiness.tablestructures.Th552rec;
import com.csc.life.newbusiness.tablestructures.Th553rec;
import com.csc.life.newbusiness.tablestructures.Th554rec;
import com.csc.life.newbusiness.tablestructures.Th555rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5661rec;
import com.csc.life.productdefinition.tablestructures.T5677rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Itemkey;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*   CREATE AUTOMATIC FOLLOW-UPS.
*
*   Loop through all FLUPLNB records until end-of-file or change
*   in key, delete record if the follow-up status = T5661 default
*   initial status.
*
*   The highest follow-up number (FUPNO) is stored for writing
*   new records.
*
*   Get the main coverage from COVTLNB using CFUP-LIFE, coverage
*   = '01', rider = '00'.  Using this component code, read TH554.
*   If number of units in COVTLNB is less than or equal to the
*   maximum number of units in TH554 and this is the only policy
*   for this life, exit subroutine.
*
*   Check the staff code set in the client file using the life
*   client number.  If staff code had been entered, first use
****the combination key of contract type + 'S' to read TH522, if
*   the combination key of contract type + 'S' to read TH552, if
*   item not found, then use only the contract type as key.  If
****the staff code is not entered, simply read TH522 using the
*   the staff code is not entered, simply read TH552 using the
****contract type.  If item is not set in TH522, exit subroutine.
*   contract type.  If item is not set in TH552, exit subroutine.
*
*   Accumulate the total sum assured for every component of every
*   contract of this life.  This accumulated amount and the age
*   of the life assured is then used to obtain the follow-up
****method kept in TH522.  T5677 is then read using transaction
*   method kept in TH552.  T5677 is then read using transaction
****code + TH522-follow-up-method.  Each follow-up code in T5677
*   code + TH552-follow-up-method.  Each follow-up code in T5677
*   is checked for existence in FLUPPF and a new record written
*   if it does not exist.
*
*****************************************************************
* </pre>
*/
public class Hcrtfup extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "HCRTFUP";
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaExitSubr = new FixedLengthStringData(1).init("Y");
	private Validator exitSubroutine = new Validator(wsaaExitSubr, "Y");

	protected FixedLengthStringData wsaaValidFupmeth = new FixedLengthStringData(1).init("N");
	protected Validator validFupmeth = new Validator(wsaaValidFupmeth, "Y");
	protected String wsaaMethFound = "N";
		/* WSAA-SUM-INSUREDS */
	private PackedDecimalData wsaaSi = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotalSi = new PackedDecimalData(17, 2);
	private static final int wsaaMaxSi = 99999999;
	protected ZonedDecimalData wsaaFupno = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	protected String wsaaValidRisk = "N";
	protected ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	protected ZonedDecimalData wsaaRow = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	protected ZonedDecimalData wsaaCol = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	protected ZonedDecimalData wsaaMethSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private FixedLengthStringData wsaaFupmeth = new FixedLengthStringData(4);
	protected FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	protected ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();	
	protected FixedLengthStringData wsaaTh553Key = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaT5661Key = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaT5661Lang = new FixedLengthStringData(1).isAPartOf(wsaaT5661Key, 0);
	private FixedLengthStringData wsaaT5661Fupcode = new FixedLengthStringData(3).isAPartOf(wsaaT5661Key, 1);
		/* ERRORS */
	private static final String e058 = "E058";
	private static final String e557 = "E557";
	private static final String f321 = "F321";
	private static final String g344 = "G344";
	private static final String hl45 = "HL45";
		/* FORMATS */
	private static final String cltsrec = "CLTSREC";
	private static final String descrec = "DESCREC";
	private static final String zfupcderec = "ZFUPCDEREC";	
		/* TABLES */
	private static final String t5661 = "T5661";
	private CltsTableDAM cltsIO = new CltsTableDAM();
	//protected CovrincTableDAM covrincIO = new CovrincTableDAM();
	//private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	//protected CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	protected DescTableDAM descIO = new DescTableDAM();
	//private FluplnbTableDAM fluplnbIO = new FluplnbTableDAM();
	//private ItdmTableDAM itdmIO = new ItdmTableDAM();
	//private ItemTableDAM itemIO = new ItemTableDAM();
	private ZfupcdeTableDAM zfupcdeIO = new ZfupcdeTableDAM();
	//protected ZlifelcTableDAM zlifelcIO = new ZlifelcTableDAM();
	private Itemkey wsaaTh552Key = new Itemkey();
	private Varcom varcom = new Varcom();
	protected Datcon1rec datcon1rec = new Datcon1rec();
	protected Datcon2rec datcon2rec = new Datcon2rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	protected Syserrrec syserrrec = new Syserrrec();
	private T5661rec t5661rec = new T5661rec();
	private T5677rec t5677rec = new T5677rec();
	private T5679rec t5679rec = new T5679rec();
	private Th552rec th552rec = new Th552rec();
	private Th553rec th553rec = new Th553rec();
	private Th554rec th554rec = new Th554rec();
	private Th555rec th555rec = new Th555rec();
	protected Hcrtfuprec hcrtfuprec = getHcrtfuprec();
	private Fluppf fluppf = null;
	private FluppfDAO fluppfDAO = getApplicationContext().getBean("flupDAO",FluppfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private CovtpfDAO covtpfDAO = getApplicationContext().getBean("covtpfDAO", CovtpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ItempfDAO itempfDAO = getApplicationContext().getBean("itempfDAO", ItempfDAO.class);
	private Lifepf lifepf = null;
	private Covtpf covtpf = null;
	private Covrpf covrpf = null;

	private List<Clntpf> clntlist = new LinkedList<Clntpf>();
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);
	
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		covrlnb2500, 
		th5542800, 
		loopCovtlnb4110, 
		readNext4180, 
		exit4190, 
		loopCovrinc4210, 
		readNext4280, 
		exit4290
	}

	public Hcrtfup() {
		super();
	}

	protected Hcrtfuprec getHcrtfuprec() {
		return new Hcrtfuprec(); 
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		hcrtfuprec.crtfupRec = convertAndSetParam(hcrtfuprec.crtfupRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
			para010();
			exit090();
		}

protected void para010()
	{
		syserrrec.subrname.set(wsaaSubr);
		hcrtfuprec.statuz.set(varcom.oK);
		wsaaSi.set(ZERO);
		wsaaTotalSi.set(ZERO);
		if (isEQ(hcrtfuprec.function,varcom.calc)) {
			totalSumins4000();
			hcrtfuprec.totalSumins.set(wsaaTotalSi);
			return ;
		}
		deleteFollowUp1000();
		hcrtfuprec.firstLife.set(SPACES);
		premiumUnit2000();
		if (exitSubroutine.isTrue()) {
			return ;
		}
		staffCode3000();
		if (exitSubroutine.isTrue()) {
			return ;
		}
		totalSumins4000();
		followupMethod5000();
		if (isEQ(wsaaFupmeth,SPACES)) {
			return ;
		}
		createFollowups6000();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void deleteFollowUp1000()
	{
		para1000();
		//loopFollowUps1010();
	}

protected void para1000()
	{
		/* Read every record in follow-up file for this policy, look for*/
		/* follow-ups which are automatically created previously.  Check th*/
		/* follow-ups statii against the default initial status in T5661, i*/
		/* they match, delete the follow-up.*/
		/**fluplnbIO.setStatuz(varcom.oK);
		fluplnbIO.setDataKey(SPACES);
		fluplnbIO.setChdrcoy(hcrtfuprec.chdrcoy);
		fluplnbIO.setChdrnum(hcrtfuprec.chdrnum);
		fluplnbIO.setFupno(ZERO);
		wsaaFupno.set(ZERO);
		fluplnbIO.setFormat(fluplnbrec);
		fluplnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		fluplnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		fluplnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");*/
		List<Fluppf> data = fluppfDAO.searchFlupRecordByChdrNum(hcrtfuprec.chdrcoy.toString(),hcrtfuprec.chdrnum.toString());
		Iterator<Fluppf> it = data.iterator();
		while(it.hasNext())
		{
			fluppf = it.next();
			loopFollowUps1010();
		}
		
	}

protected void loopFollowUps1010()
	{
		/**SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)
		&& isNE(fluplnbIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			dbError8100();
		}
		if (isEQ(fluplnbIO.getStatuz(),varcom.endp)
		|| isNE(fluplnbIO.getChdrcoy(),hcrtfuprec.chdrcoy)
		|| isNE(fluplnbIO.getChdrnum(),hcrtfuprec.chdrnum)) {
			return ;
		}*/
		if (isNE(fluppf.getzAutoInd(),"Y")
		|| isNE(hcrtfuprec.firstLife,"Y")) {
			if (isGT(fluppf.getFupNo(),wsaaFupno)) {
				wsaaFupno.set(fluppf.getFupNo());
			}
		//	fluplnbIO.setFunction(varcom.nextr);
		//	loopFollowUps1010();
			return ;
		}
				
		/*    MOVE FLUPLNB-FUPCODE        TO ITEM-ITEMITEM.                */
		wsaaT5661Lang.set(hcrtfuprec.language);
		wsaaT5661Fupcode.set(fluppf.getFupCde());		
		List<Itempf> data = itempfDAO.findItemByTable("IT",hcrtfuprec.chdrcoy.toString(),"T5661",wsaaT5661Key.toString(),"1");
		if(data.isEmpty())
		{			
			syserrrec.statuz.set(e557);
			syserrrec.params.set(getErrorMessage("IT",hcrtfuprec.chdrcoy.toString(),"T5661",wsaaT5661Key.toString(),"1"));
			dbError8100();
		}		
		Itempf itempf = data.get(0);
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isNE(fluppf.getFupSts(),t5661rec.fupstat)
		|| isNE(fluppf.getFupTyp(),"P")) {
			if (isGT(fluppf.getFupNo(),wsaaFupno)) {
				wsaaFupno.set(fluppf.getFupNo());
			}
//			fluplnbIO.setFunction(varcom.nextr);
//			loopFollowUps1010();
			return ;
		}
		/*  Delete the follow-up record*/
		/**fluplnbIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			dbError8100();
		}*/
		/**fluplnbIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			dbError8100();
		}
		fluplnbIO.setFunction(varcom.nextr);
		loopFollowUps1010();
		return ;*/
		fluppfDAO.deleteByID(fluppf);
	}

	protected String getErrorMessage(String ...value)
	{
		StringBuilder error = new StringBuilder();
		for(String val:value)
		{
			error.append(val);
		}
		return error.toString();
	}

protected void premiumUnit2000()
	{	
			/*  If the policy is premium-unit based (the main component is set*/
		/*  TH554), skip all other validation if this is the only policy*/
		/*  attached to the life and TH554- maximum number of units is not*/
		/*  exceeded.*/
		Covtpf covtpf = covtpfDAO.getCovtlnbData(hcrtfuprec.chdrcoy.toString(),hcrtfuprec.chdrnum.toString(),hcrtfuprec.life.toString(),"01","00");
		if(covtpf == null)
		{
			covrlnb2500();
		}
		else
		{
			wsaaCrtable.set(covtpf.getCrtable());
			wsaaEffdate.set(covtpf.getEffdate());
		}
		th5542800(); //ILIFE-9040
	}

protected void covrlnb2500()
	{
	
		covrpf = new Covrpf();
		/*covrpf.setChdrcoy(hcrtfuprec.chdrcoy.toString());
		covrpf.setChdrnum(hcrtfuprec.chdrnum.toString());
		covrpf.setLife(hcrtfuprec.life.toString());
		covrpf.setCoverage("01");
		covrpf.setRider("00");*/
		
		List<Covrpf> data = covrpfDAO.searchValidCovrpfRecord(hcrtfuprec.chdrcoy.toString(),hcrtfuprec.chdrnum.toString(),hcrtfuprec.life.toString(),"01","00");
		if(data.isEmpty())
		{
			syserrrec.statuz.set(g344);			
			syserrrec.params.set(getErrorMessage("COVRPF ", hcrtfuprec.chdrcoy.toString(),hcrtfuprec.chdrnum.toString(),hcrtfuprec.life.toString(),"0100"));
			dbError8100();
		}
		covrpf = data.get(0);
		wsaaCrtable.set(covrpf.getCrtable());
		wsaaEffdate.set(covrpf.getCrrcd());
	}

protected void th5542800()
	{
		Itempf itempf = itempfDAO.readItdmpf("IT", hcrtfuprec.chdrcoy.toString(), "TH554", wsaaEffdate.toInt(), wsaaCrtable.toString());		
		if (itempf == null) {
			wsaaExitSubr.set("N");
			return ;
		}
		/* MOVE ITDM-GENAREA           TO TH524-TH524-REC.              */
		th554rec.th554Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void staffCode3000()
	{
			para3000();
		}

protected void para3000()
	{
		/*Obtain the staff code from CLTS, read TH522 accordingly.        */
		/* Obtain the staff code from CLTS, read TH552 accordingly.        */
		/*
		cltsIO.setStatuz(varcom.oK);
		cltsIO.setDataKey(SPACES);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntcoy(hcrtfuprec.fsuco);
		cltsIO.setClntnum(hcrtfuprec.lifcnum);
		cltsIO.setFormat(cltsrec);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(),varcom.oK)
		&& isNE(cltsIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(cltsIO.getStatuz());
			syserrrec.params.set(cltsIO.getParams());
			dbError8100();
		}
		if (isEQ(cltsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(e058);
			cltsIO.setStatuz(e058);
			syserrrec.params.set(cltsIO.getParams());
			dbError8100();
		}
		*/
		//IBPLIFE-8672
		clntlist = clntpfDAO.readStatcode(hcrtfuprec.fsuco.toString(), hcrtfuprec.lifcnum.toString());
		if (clntlist.isEmpty()) {
			syserrrec.statuz.set(e058);
			cltsIO.setStatuz(e058);
			dbError8100();
		}
		//if (isNE(cltsIO.getStatcode(),SPACES)) {
		if (!clntlist.isEmpty() && isNE(clntlist.get(0).getStatcode(),SPACES)) { //IBPLIFE-8672
			List<Itempf> data = itempfDAO.findItemByTable("IT",hcrtfuprec.chdrcoy.toString(),"TH552",hcrtfuprec.cnttype.trim()+"S","1");
			if(!data.isEmpty())
			{			
				Itempf itempf = data.get(0);								
				wsaaExitSubr.set("N");
				/*          MOVE ITEM-DATA-KEY  TO WSAA-TH522-KEY                */
				/*          MOVE ITEM-GENAREA   TO TH522-TH522-REC               */
				setTh552Key("IT",hcrtfuprec.chdrcoy.toString(),"TH552",itempf.getItemitem());
				th552rec.th552Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				return ;
			}								
		}
		String itemitem = hcrtfuprec.cnttype.toString();
		if (isNE(hcrtfuprec.lrkcls,SPACES)) {
			itemitem = hcrtfuprec.lrkcls.toString();
		}
		List<Itempf> data = itempfDAO.findItemByTable("IT",hcrtfuprec.chdrcoy.toString(),"TH552",itemitem,"1");
		if(data.isEmpty())
		{			
			wsaaExitSubr.set("Y");
		}
		else
		{
			Itempf itempf = data.get(0);								
			wsaaExitSubr.set("N");
			/*          MOVE ITEM-DATA-KEY  TO WSAA-TH522-KEY                */
			/*          MOVE ITEM-GENAREA   TO TH522-TH522-REC               */
			setTh552Key("IT",hcrtfuprec.chdrcoy.toString(),"TH552",itempf.getItemitem()); // ILIFE-9040
			th552rec.th552Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		}
	}

protected void setTh552Key(String itempfx, String itemcoy, String itemtabl, String itemitem)
{
	wsaaTh552Key.itemItempfx.set(itempfx);
	wsaaTh552Key.itemItemcoy.set(itemcoy);
	wsaaTh552Key.itemItemtabl.set(itemtabl);
	wsaaTh552Key.itemItemitem.set(itemitem);
}

protected void totalSumins4000()
	{
		//para4000();
		lifeio4010();
	}

protected void para4000()
	{
		/*  Loop through LIFEPF using logical fflupLC, for each record,*/
		/*  accumulate the total sum assured in COVTPF or in COVRPF.*/
//		zlifelcIO.setStatuz(varcom.oK);
//		zlifelcIO.setDataKey(SPACES);
//		zlifelcIO.setChdrcoy(hcrtfuprec.chdrcoy);
//		zlifelcIO.setLifcnum(hcrtfuprec.lifcnum);
//		zlifelcIO.setFormat(zlifelcrec);
//		zlifelcIO.setFunction(varcom.begn);
//		//performance improvement -- Anjali
//		zlifelcIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);
	}

protected void lifeio4010()
	{
//		SmartFileCode.execute(appVars, zlifelcIO);
//		if (isNE(zlifelcIO.getStatuz(),varcom.oK)
//		&& isNE(zlifelcIO.getStatuz(),varcom.endp)) {
//			syserrrec.statuz.set(zlifelcIO.getStatuz());
//			syserrrec.params.set(zlifelcIO.getParams());
//			dbError8100();
//		}
//		if (isEQ(zlifelcIO.getStatuz(),varcom.endp)
//		|| isNE(zlifelcIO.getChdrcoy(),hcrtfuprec.chdrcoy)
//		|| isNE(zlifelcIO.getLifcnum(),hcrtfuprec.lifcnum)) {
//			return ;
//		}
		List<Lifepf> data = lifepfDAO.searchLifeRecordByLifcNum(hcrtfuprec.chdrcoy.toString(),hcrtfuprec.lifcnum.toString(),"2","01");
		Iterator<Lifepf> it = data.iterator();
		while(it.hasNext())
		{
			lifepf = it.next();
			if (isEQ(lifepf.getValidflag(),"3")) {
				proposalSi4100();
			}
			else {
				contractSi4200();
			}
		}
//		zlifelcIO.setFunction(varcom.nextr);
//		lifeio4010();
//		return ;
	}

protected void proposalSi4100()
	{
	
		List<Covtpf> data = covtpfDAO.getCovtlnbRecord(lifepf.getChdrcoy(),lifepf.getChdrnum(),lifepf.getLife(),lifepf.getJlife());		
		Iterator<Covtpf> it = data.iterator();
		while(it.hasNext())
		{
			covtpf = it.next();
			wsaaCrtable.set(covtpf.getCrtable());
			wsaaEffdate.set(covtpf.getEffdate());
			underwritingInd4400();
			loopCovtlnb4111CustomerSpecific();
		}
		
	}


protected void loopCovtlnb4111CustomerSpecific(){
	/*  IF  TH525-ZUNDWRT           = 'Y'                            */
	if (isEQ(th555rec.zundwrt,"Y")) {
		/*      IF  COVTLNB-CNTCURR     NOT = TH522-CURRCODE             */
		if (isNE(covtpf.getCntcurr(),th552rec.currcode)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hcrtfuprec.cnttype);
			stringVariable1.addExpression(covtpf.getCntcurr());
			stringVariable1.setStringInto(wsaaTh553Key);
			currencyRate4300();
			compute(wsaaSi, 8).setRounded(mult(covtpf.getSumins(),th553rec.scrate));
			zrdecplrec.amountIn.set(wsaaSi);
			a000CallRounding();
			wsaaSi.set(zrdecplrec.amountOut);
		}
		else {
			wsaaSi.set(covtpf.getSumins());
		}
		wsaaTotalSi.add(wsaaSi);
	}
}


protected void contractSi4200()
	{
		covrpf = new Covrpf();
		covrpf.setChdrcoy(lifepf.getChdrcoy());
		covrpf.setChdrnum(lifepf.getChdrnum());
		covrpf.setLife(lifepf.getLife());
		covrpf.setJlife("01");
		List<Covrpf> data = covrpfDAO.getCovrpfByChdrnumLife(covrpf);
		Iterator<Covrpf> it = data.iterator();
		while(it.hasNext())
		{
			covrpf = it.next();
			wsaaCrtable.set(covrpf.getCrtable());
			wsaaEffdate.set(covrpf.getCrrcd());
			underwritingInd4400();
			loopCovrinc4212CustomerSpecific();
		}
		
		
	}


protected void loopCovrinc4212CustomerSpecific(){
	
	/*  IF  TH525-ZUNDWRT           = 'Y'                            */
	if (isEQ(th555rec.zundwrt,"Y")) {
		riskStatus4500();
		if (isEQ(wsaaValidRisk,"Y")) {
			/*          IF  COVRINC-PREM-CURRENCY NOT = TH522-CURRCODE       */
			if (isNE(covrpf.getPremCurrency(),th552rec.currcode)) {
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression(hcrtfuprec.cnttype);
				stringVariable1.addExpression(covrpf.getPremCurrency());
				stringVariable1.setStringInto(wsaaTh553Key);
				currencyRate4300();
				compute(wsaaSi, 8).setRounded(mult(covrpf.getSumins(),th553rec.scrate));
				zrdecplrec.amountIn.set(wsaaSi);
				a000CallRounding();
				wsaaSi.set(zrdecplrec.amountOut);
			}
			else {
				wsaaSi.set(covrpf.getSumins());
			}
			wsaaTotalSi.add(wsaaSi);
		}
	}
}

protected void currencyRate4300()
	{
		para4300();
	}

protected void para4300()
	{
		Itempf itempf = itempfDAO.readItdmpf("IT", hcrtfuprec.chdrcoy.toString(), "TH553", wsaaEffdate.toInt(), wsaaTh553Key.toString());			
		if (itempf == null) {
			syserrrec.statuz.set(hl45);			
			syserrrec.params.set(getErrorMessage("IT", hcrtfuprec.chdrcoy.toString(), "TH553" ,wsaaEffdate.toString() ,wsaaTh553Key.toString()));
			dbError8100();
		}
		/*  MOVE ITDM-GENAREA           TO TH523-TH523-REC.              */
		th553rec.th553Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void underwritingInd4400()
	{
			para4400();
		}

protected void para4400()
	{
		Itempf itempf = itempfDAO.readItdmpf("IT", hcrtfuprec.chdrcoy.toString(), "TH555", wsaaEffdate.toInt(), wsaaCrtable.toString());			
		if (itempf == null) {
			/*      MOVE HL15               TO SYSR-STATUZ                   */
			/*                                 ITDM-STATUZ                   */
			/*      MOVE WSAA-PREV-KEY      TO ITDM-DATA-KEY                 */
			/*      MOVE ITDM-PARAMS        TO SYSR-PARAMS                   */
			th555rec.zundwrt.set("N");
			return ;
		}
		/*  MOVE ITDM-GENAREA           TO TH523-TH523-REC.              */	
		th555rec.th555Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void riskStatus4500()
	{
		para4500();
	}

protected void para4500()
	{
	
	List<Itempf> data = itempfDAO.findItemByTable("IT",hcrtfuprec.chdrcoy.toString(),"T5679",hcrtfuprec.batctrcde.toString(),"1");
	if(data.isEmpty())
	{			
		syserrrec.statuz.set(f321);
		syserrrec.params.set(getErrorMessage("IT",hcrtfuprec.chdrcoy.toString(),"T5679",hcrtfuprec.batctrcde.toString(),"1"));
		dbError8100();
	}
		Itempf itempf = data.get(0);										
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
		wsaaValidRisk = "N";
		for (wsaaSub.set(1); !(isGT(wsaaSub,12)
		|| isEQ(wsaaValidRisk,"Y")); wsaaSub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()],covrpf.getStatcode())) {
				wsaaValidRisk = "Y";
			}
		}
	}

protected void followupMethod5000()
	{
		/*Use previously stored TH522 item-key to read TH522 again.  Loop*/
		/*  Use previously stored TH552 item-key to read TH552 again.  Loop*/
		/*  through ITEM to look for the valid range of sum insured, then*/
		/*  check for the valid range of age.  Obtain the corresponding*/
		/*  follow-up method from the matrix.*/
		wsaaValidFupmeth.set("N");
		wsaaFupmeth.set(SPACES);
		List<Itempf> data = itempfDAO.findItemByTable("IT",hcrtfuprec.chdrcoy.toString(),"Th552",wsaaTh552Key.itemItemitem.toString(),"1");
		//Iterator<Itempf> it = data.iterator();  //ILIFE-9040
		for(Itempf itempf:data) //ILIFE-9040
		{
				//Itempf itempf = data.get(0);	//ILIFE-9040									
				th552rec.th552Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
				for (wsaaSub.set(1); !(isGT(wsaaSub,8)
						|| isEQ(wsaaValidFupmeth,"Y")); wsaaSub.add(1)){
					if (isEQ(th552rec.zsuminto[wsaaSub.toInt()],wsaaMaxSi)) {
						wsaaRow.set(wsaaSub);
						wsaaValidFupmeth.set("Y");
					}
					/*    IF WSAA-TOTAL-SI          NOT < TH522-ZSUMINFR (WSAA-SUB)  */
					/*    AND WSAA-TOTAL-SI         NOT > TH522-ZSUMINTO (WSAA-SUB)  */
					if (isGTE(wsaaTotalSi,th552rec.zsuminfr[wsaaSub.toInt()])
					&& isLTE(wsaaTotalSi,th552rec.zsuminto[wsaaSub.toInt()])) {
						wsaaRow.set(wsaaSub);
						wsaaValidFupmeth.set("Y");
					}
					/*    IF TH522-ZSUMINTO (WSAA-SUB) = 0                           */
					if (isEQ(th552rec.zsuminto[wsaaSub.toInt()],0)) {
						compute(wsaaRow, 0).set(sub(wsaaSub,1));
						wsaaValidFupmeth.set("Y");
					}
					if (validFupmeth.isTrue()) {
						wsaaMethFound = "N";
						for (wsaaCol.set(1); !(isGT(wsaaCol, 8)
						|| isEQ(wsaaMethFound,"Y")); wsaaCol.add(1)){
							if (isEQ(wsaaCol, 8)
							|| (isGTE(hcrtfuprec.anbccd, th552rec.ageIssageFrm[wsaaCol.toInt()])
							&& isLTE(hcrtfuprec.anbccd, th552rec.ageIssageTo[wsaaCol.toInt()]))) {
								/*               COMPUTE WSAA-METH-SUB = (WSAA-ROW - 1) * 9        */
								compute(wsaaMethSub, 0).set(add(mult((sub(wsaaRow, 1)), 8), wsaaCol));
								/*             MOVE TH522-DEF-FUP-METH (WSAA-METH-SUB)           */
								if(th552rec.defFupMeth[wsaaMethSub.toInt()] != null) {
									wsaaFupmeth.set(th552rec.defFupMeth[wsaaMethSub.toInt()]);
									wsaaMethFound = "Y";
								}
							}
						}
					}
			}
			if (!validFupmeth.isTrue()) {
				continue;
			}
		}
	}

protected void createFollowups6000()
	{
		para6000();
	}

protected void para6000()
	{
		/*  Using the transaction code passed from linkage and the follow-u*/
		/*method in TH522, read T5677.  For every follow-up code found in*/
		/*  method in TH552, read T5677.  For every follow-up code found in*/
		/*  T5677, check if a corresponding follow-up code for the life is*/
		/*  already found in FLUPPF using logical ZFUPCDE.  If not, create*/
		/*  record in FLUPPF using logical FLUPLNB.*/
		List<Itempf> data = itempfDAO.findItemByTable("IT",hcrtfuprec.chdrcoy.toString(),"T5677",hcrtfuprec.batctrcde.toString()+wsaaFupmeth.toString(),"1");			
		if (data.isEmpty()) {
			syserrrec.statuz.set(Varcom.mrnf);
			syserrrec.params.set(getErrorMessage("IT",hcrtfuprec.chdrcoy.toString(),"T5677",hcrtfuprec.batctrcde.toString(),wsaaFupmeth.toString(),"1"));
			dbError8100();
		}
		Itempf itempf = data.get(0);
		t5677rec.t5677Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		for (wsaaSub.set(1); !(isGT(wsaaSub,16)); wsaaSub.add(1)){
			if (isNE(t5677rec.fupcdes[wsaaSub.toInt()],SPACES)) {
				fupUpdCrt6100();
			}
		}
	}

protected void fupUpdCrt6100()
	{
			para6100();
		}

protected void para6100()
	{
		/*  First, check if follow-up already exist in FLUPPF, if not, crea*/
		/*  a follow-up record using logical FLUPLNB.*/
		zfupcdeIO.setStatuz(varcom.oK);
		zfupcdeIO.setDataKey(SPACES);
		zfupcdeIO.setChdrcoy(hcrtfuprec.chdrcoy);
		zfupcdeIO.setChdrnum(hcrtfuprec.chdrnum);
		zfupcdeIO.setLife(hcrtfuprec.life);
		zfupcdeIO.setJlife(hcrtfuprec.jlife);
		/*    MOVE T5677-FUPCODE(WSAA-SUB) TO ZFUPCDE-FUPCODE.             */
		zfupcdeIO.setFupcode(t5677rec.fupcdes[wsaaSub.toInt()]);
		zfupcdeIO.setFormat(zfupcderec);
		zfupcdeIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, zfupcdeIO);
		if (isNE(zfupcdeIO.getStatuz(),varcom.oK)
		&& isNE(zfupcdeIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(zfupcdeIO.getStatuz());
			syserrrec.params.set(zfupcdeIO.getParams());
			dbError8100();
		}
		if (isEQ(zfupcdeIO.getStatuz(),varcom.oK)) {
			return ;
		}
		fupStatus6200();
		fluppf = new Fluppf();
		fluppf.setFupSts(t5661rec.fupstat.toString().charAt(0));
		fluppf.setChdrcoy(hcrtfuprec.chdrcoy.charat(0));
		fluppf.setChdrnum(hcrtfuprec.chdrnum.toString());
		fluppf.setLife(hcrtfuprec.life.toString());
		fluppf.setjLife(hcrtfuprec.jlife.toString());
		wsaaFupno.add(1);
		fluppf.setFupNo(wsaaFupno.toInt());
		fluppf.setTranNo(hcrtfuprec.tranno.toInt());
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* When present, advance todays' date by T5661-Reminder-Days.*/
		if (isNE(t5661rec.zelpdays,ZERO)) {
			datcon2rec.intDate1.set(datcon1rec.intDate);
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(t5661rec.zelpdays);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			fluppf.setFupDt(datcon2rec.intDate2.toInt());
		}
		else {
			fluppf.setFupDt(datcon1rec.intDate.toInt());
		}
		/*    MOVE T5677-FUPCODE (WSAA-SUB) TO FLUPLNB-FUPCODE.            */
		fluppf.setFupCde(t5677rec.fupcdes[wsaaSub.toInt()].toString());
		fluppf.setFupTyp('P');
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			fluppf.setFupRmk("????????????????????????????????????????");
		}
		else {
			fluppf.setFupRmk(descIO.getLongdesc().toString());
		}
		fluppf.setTrdt(hcrtfuprec.transactionDate.toInt());
		fluppf.setTrtm(hcrtfuprec.transactionTime.toInt());
		fluppf.setUserT(hcrtfuprec.user.toInt());
		fluppf.setzAutoInd('Y');
		fluppf.setEffDate(datcon1rec.intDate.toInt());
		fluppf.setCrtDate(datcon1rec.intDate.toInt());
		fluppf.setzLstUpDt(datcon1rec.intDate.toInt());
		fluppf.setFuprcvd(varcom.vrcmMaxDate.toInt());
		fluppf.setExprDate(varcom.vrcmMaxDate.toInt());
		//fluppf.setFormat(fluplnbrec);
		//fluppf.setFunction(varcom.writr);
		/**SmartFileCode.execute(appVars, fluplnbIO);
		if (isNE(fluplnbIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(fluplnbIO.getStatuz());
			syserrrec.params.set(fluplnbIO.getParams());
			dbError8100();
		}*/
		fluppfDAO.insertFlupRecord(fluppf); //ILIFE-9040
	}

protected void fupStatus6200()
	{
		wsaaT5661Lang.set(hcrtfuprec.language);
		wsaaT5661Fupcode.set(t5677rec.fupcdes[wsaaSub.toInt()]);
		List<Itempf> data = itempfDAO.findItemByTable("IT",hcrtfuprec.chdrcoy.toString(),"T5661",wsaaT5661Key.toString(),"1");			
		if (data.isEmpty()) {
			syserrrec.statuz.set(Varcom.mrnf);
			syserrrec.params.set(getErrorMessage("IT",hcrtfuprec.chdrcoy.toString(),"T5661",wsaaT5661Key.toString(),"1"));
			dbError8100();
		}
		Itempf itempf = data.get(0);
		t5661rec.t5661Rec.set(StringUtil.rawToString(itempf.getGenarea()));		
		descIO.setStatuz(varcom.oK);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(hcrtfuprec.chdrcoy);
		descIO.setDesctabl(t5661);
		/*    MOVE T5677-FUPCODE (WSAA-SUB) TO DESC-DESCITEM.              */
		/*    MOVE T5677-FUPCDES (WSAA-SUB) TO DESC-DESCITEM.      <LA4314>*/
		wsaaT5661Lang.set(hcrtfuprec.language);
		wsaaT5661Fupcode.set(t5677rec.fupcdes[wsaaSub.toInt()]);
		descIO.setDescitem(wsaaT5661Key);
		descIO.setLanguage(hcrtfuprec.language);
		descIO.setFormat(descrec);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(descIO.getStatuz());
			syserrrec.params.set(descIO.getParams());
			dbError8100();
		}
	}

protected void dbError8100()
	{
			db8100();
			dbExit8190();
		}

protected void db8100()
	{
		if (isEQ(syserrrec.statuz,varcom.bomb)) {
			return ;
		}
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		if (isNE(syserrrec.syserrType,"2")) {
			syserrrec.syserrType.set("1");
		}
		callProgram(Syserr.class, syserrrec.syserrRec);
	}

protected void dbExit8190()
	{
		hcrtfuprec.statuz.set(varcom.bomb);
		exit090();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(hcrtfuprec.chdrcoy);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(th552rec.currcode);
		zrdecplrec.batctrcde.set(hcrtfuprec.batctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zfupcdeIO.getStatuz());
			syserrrec.params.set(zfupcdeIO.getParams());
			dbError8100();
		}
		/*A900-EXIT*/
	}



public String getWsaaSubr() {
	return wsaaSubr;
}

public PackedDecimalData getWsaaSi() {
	return wsaaSi;
}

public void setWsaaSi(PackedDecimalData wsaaSi) {
	this.wsaaSi = wsaaSi;
}

public PackedDecimalData getWsaaTotalSi() {
	return wsaaTotalSi;
}

public void setWsaaTotalSi(PackedDecimalData wsaaTotalSi) {
	this.wsaaTotalSi = wsaaTotalSi;
}

public Validator getExitSubroutine() {
	return exitSubroutine;
}

public void setExitSubroutine(Validator exitSubroutine) {
	this.exitSubroutine = exitSubroutine;
}

public FixedLengthStringData getWsaaFupmeth() {
	return wsaaFupmeth;
}

public void setWsaaFupmeth(FixedLengthStringData wsaaFupmeth) {
	this.wsaaFupmeth = wsaaFupmeth;
}

public Th552rec getTh552rec() {
	return th552rec;
}

public void setTh552rec(Th552rec th552rec) {
	this.th552rec = th552rec;
}

public FixedLengthStringData getWsaaExitSubr() {
	return wsaaExitSubr;
}

public void setWsaaExitSubr(FixedLengthStringData wsaaExitSubr) {
	this.wsaaExitSubr = wsaaExitSubr;
}

public static int getWsaamaxsi() {
	return wsaaMaxSi;
}

public Fluppf getFluplnbIO() {
	return fluppf;
}

public void setFluplnbIO(Fluppf fluplnbIO) {
	this.fluppf = fluplnbIO;
}


public FixedLengthStringData getWsaaT5661Lang() {
	return wsaaT5661Lang;
}

public void setWsaaT5661Lang(FixedLengthStringData wsaaT5661Lang) {
	this.wsaaT5661Lang = wsaaT5661Lang;
}

public FixedLengthStringData getWsaaT5661Fupcode() {
	return wsaaT5661Fupcode;
}

public void setWsaaT5661Fupcode(FixedLengthStringData wsaaT5661Fupcode) {
	this.wsaaT5661Fupcode = wsaaT5661Fupcode;
}

public FixedLengthStringData getWsaaT5661Key() {
	return wsaaT5661Key;
}

public void setWsaaT5661Key(FixedLengthStringData wsaaT5661Key) {
	this.wsaaT5661Key = wsaaT5661Key;
}

public T5661rec getT5661rec() {
	return t5661rec;
}

public void setT5661rec(T5661rec t5661rec) {
	this.t5661rec = t5661rec;
}

public Th555rec getTh555rec() {
	return th555rec;
}

public void setTh555rec(Th555rec th555rec) {
	this.th555rec = th555rec;
}

public Th553rec getTh553rec() {
	return th553rec;
}

public void setTh553rec(Th553rec th553rec) {
	this.th553rec = th553rec;
}

public Zrdecplrec getZrdecplrec() {
	return zrdecplrec;
}

public void setZrdecplrec(Zrdecplrec zrdecplrec) {
	this.zrdecplrec = zrdecplrec;
}

public T5677rec getT5677rec() {
	return t5677rec;
}

public void setT5677rec(T5677rec t5677rec) {
	this.t5677rec = t5677rec;
}




}
