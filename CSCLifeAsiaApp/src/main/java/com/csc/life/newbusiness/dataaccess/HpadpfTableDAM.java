package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HpadpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:32
 * Class transformed from HPADPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HpadpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 128;
	public FixedLengthStringData hpadrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hpadpfRecord = hpadrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hpadrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hpadrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(hpadrec);
	public PackedDecimalData hpropdte = DD.hpropdte.copy().isAPartOf(hpadrec);
	public PackedDecimalData hprrcvdt = DD.hprrcvdt.copy().isAPartOf(hpadrec);
	public PackedDecimalData hissdte = DD.hissdte.copy().isAPartOf(hpadrec);
	public PackedDecimalData huwdcdte = DD.huwdcdte.copy().isAPartOf(hpadrec);
	public PackedDecimalData hoissdte = DD.hoissdte.copy().isAPartOf(hpadrec);
	public FixedLengthStringData zdoctor = DD.zdoctor.copy().isAPartOf(hpadrec);
	public FixedLengthStringData znfopt = DD.znfopt.copy().isAPartOf(hpadrec);
	public PackedDecimalData zsufcdte = DD.zsufcdte.copy().isAPartOf(hpadrec);
	public FixedLengthStringData procflg = DD.procflg.copy().isAPartOf(hpadrec);
	public FixedLengthStringData dlvrmode = DD.dlvrmode.copy().isAPartOf(hpadrec);
	public PackedDecimalData despdate = DD.despdate.copy().isAPartOf(hpadrec);
	public PackedDecimalData packdate = DD.packdate.copy().isAPartOf(hpadrec);
	public PackedDecimalData remdte = DD.remdte.copy().isAPartOf(hpadrec);
	public PackedDecimalData deemdate = DD.deemdate.copy().isAPartOf(hpadrec);
	public PackedDecimalData nextActDate = DD.nxtdte.copy().isAPartOf(hpadrec);
	public FixedLengthStringData incexc = DD.incexc.copy().isAPartOf(hpadrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hpadrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hpadrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hpadrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HpadpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HpadpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HpadpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HpadpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpadpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HpadpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpadpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HPADPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"VALIDFLAG, " +
							"HPROPDTE, " +
							"HPRRCVDT, " +
							"HISSDTE, " +
							"HUWDCDTE, " +
							"HOISSDTE, " +
							"ZDOCTOR, " +
							"ZNFOPT, " +
							"ZSUFCDTE, " +
							"PROCFLG, " +
							"DLVRMODE, " +
							"DESPDATE, " +
							"PACKDATE, " +
							"REMDTE, " +
							"DEEMDATE, " +
							"NXTDTE, " +
							"INCEXC, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     validflag,
                                     hpropdte,
                                     hprrcvdt,
                                     hissdte,
                                     huwdcdte,
                                     hoissdte,
                                     zdoctor,
                                     znfopt,
                                     zsufcdte,
                                     procflg,
                                     dlvrmode,
                                     despdate,
                                     packdate,
                                     remdte,
                                     deemdate,
                                     nextActDate,
                                     incexc,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		validflag.clear();
  		hpropdte.clear();
  		hprrcvdt.clear();
  		hissdte.clear();
  		huwdcdte.clear();
  		hoissdte.clear();
  		zdoctor.clear();
  		znfopt.clear();
  		zsufcdte.clear();
  		procflg.clear();
  		dlvrmode.clear();
  		despdate.clear();
  		packdate.clear();
  		remdte.clear();
  		deemdate.clear();
  		nextActDate.clear();
  		incexc.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHpadrec() {
  		return hpadrec;
	}

	public FixedLengthStringData getHpadpfRecord() {
  		return hpadpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHpadrec(what);
	}

	public void setHpadrec(Object what) {
  		this.hpadrec.set(what);
	}

	public void setHpadpfRecord(Object what) {
  		this.hpadpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hpadrec.getLength());
		result.set(hpadrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}