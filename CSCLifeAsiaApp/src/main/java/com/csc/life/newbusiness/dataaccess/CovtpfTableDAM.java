package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: CovtpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:36
 * Class transformed from COVTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */

public class CovtpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 488;
	public FixedLengthStringData covtrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData covtpfRecord = covtrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(covtrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(covtrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(covtrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(covtrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(covtrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(covtrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(covtrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(covtrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(covtrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(covtrec);
	public PackedDecimalData riskCessDate = DD.rcesdte.copy().isAPartOf(covtrec);
	public PackedDecimalData premCessDate = DD.pcesdte.copy().isAPartOf(covtrec);
	public PackedDecimalData riskCessAge = DD.rcesage.copy().isAPartOf(covtrec);
	public PackedDecimalData premCessAge = DD.pcesage.copy().isAPartOf(covtrec);
	public PackedDecimalData riskCessTerm = DD.rcestrm.copy().isAPartOf(covtrec);
	public PackedDecimalData premCessTerm = DD.pcestrm.copy().isAPartOf(covtrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(covtrec);
	public FixedLengthStringData liencd = DD.liencd.copy().isAPartOf(covtrec);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(covtrec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(covtrec);
	public FixedLengthStringData reserveUnitsInd = DD.rsunin.copy().isAPartOf(covtrec);
	public PackedDecimalData reserveUnitsDate = DD.rundte.copy().isAPartOf(covtrec);
	public PackedDecimalData polinc = DD.polinc.copy().isAPartOf(covtrec);
	public PackedDecimalData numapp = DD.numapp.copy().isAPartOf(covtrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(covtrec);
	public FixedLengthStringData sex01 = DD.sex.copy().isAPartOf(covtrec);
	public FixedLengthStringData sex02 = DD.sex.copy().isAPartOf(covtrec);
	public PackedDecimalData anbAtCcd01 = DD.anbccd.copy().isAPartOf(covtrec);
	public PackedDecimalData anbAtCcd02 = DD.anbccd.copy().isAPartOf(covtrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(covtrec);
	public FixedLengthStringData billchnl = DD.billchnl.copy().isAPartOf(covtrec);
	public PackedDecimalData seqnbr = DD.seqnbr.copy().isAPartOf(covtrec);
	public PackedDecimalData singp = DD.singp.copy().isAPartOf(covtrec);
	public PackedDecimalData instprem = DD.instprem.copy().isAPartOf(covtrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(covtrec);
	public PackedDecimalData payrseqno = DD.payrseqno.copy().isAPartOf(covtrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(covtrec);
	public PackedDecimalData benCessAge = DD.bcesage.copy().isAPartOf(covtrec);
	public PackedDecimalData benCessTerm = DD.bcestrm.copy().isAPartOf(covtrec);
	public PackedDecimalData benCessDate = DD.bcesdte.copy().isAPartOf(covtrec);
	public FixedLengthStringData bappmeth = DD.bappmeth.copy().isAPartOf(covtrec);
	public PackedDecimalData zbinstprem = DD.zbinstprem.copy().isAPartOf(covtrec);
	public PackedDecimalData zlinstprem = DD.zlinstprem.copy().isAPartOf(covtrec);
	public FixedLengthStringData zdivopt = DD.zdivopt.copy().isAPartOf(covtrec);
	public FixedLengthStringData paycoy = DD.paycoy.copy().isAPartOf(covtrec);
	public FixedLengthStringData payclt = DD.payclt.copy().isAPartOf(covtrec);
	public FixedLengthStringData paymth = DD.paymth.copy().isAPartOf(covtrec);
	public FixedLengthStringData bankkey = DD.bankkey.copy().isAPartOf(covtrec);
	public FixedLengthStringData bankacckey = DD.bankacckey.copy().isAPartOf(covtrec);
	public FixedLengthStringData paycurr = DD.paycurr.copy().isAPartOf(covtrec);
	public FixedLengthStringData facthous = DD.facthous.copy().isAPartOf(covtrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(covtrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(covtrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(covtrec);
	//TSD-306 Start
	public PackedDecimalData loadper = DD.loadper.copy().isAPartOf(covtrec);
	public PackedDecimalData rateadj = DD.rateadj.copy().isAPartOf(covtrec);
	public PackedDecimalData fltmort = DD.fltmort.copy().isAPartOf(covtrec);
	public PackedDecimalData premadj = DD.premadj.copy().isAPartOf(covtrec);
	public PackedDecimalData ageadj = DD.adjustageamt.copy().isAPartOf(covtrec); 
	//TSD-306 End
	public PackedDecimalData zstpduty01 = DD.zstpduty.copy().isAPartOf(covtrec);
	public FixedLengthStringData zclstate=DD.zclstate.copy().isAPartOf(covtrec);
	//BRD-NBP-012-start
	public FixedLengthStringData gmib=DD.bnfgrp.copy().isAPartOf(covtrec);
	public FixedLengthStringData gmdb=DD.functn.copy().isAPartOf(covtrec);
	public FixedLengthStringData gmwb=DD.caccy.copy().isAPartOf(covtrec);
	/*ILIFE-6941 start*/
	public FixedLengthStringData lnkgno = DD.lnkgno.copy().isAPartOf(covtrec); 
	public FixedLengthStringData lnkgsubrefno = DD.lnkgsubrefno.copy().isAPartOf(covtrec); 
	public FixedLengthStringData tpdtype=DD.tpdtype.copy().isAPartOf(covtrec);//ILIFE-7118
	public FixedLengthStringData singpremtype=DD.sngprmtyp.copy().isAPartOf(covtrec);//ILIFE-7805
	/*ILIFE-6941 end*/
	//BRD-NBP-012 -end
	public FixedLengthStringData lnkgind=DD.jpjlnkind.copy().isAPartOf(covtrec);
	public PackedDecimalData cashvalarer = DD.cashvalare.copy().isAPartOf(covtrec);
	public PackedDecimalData riskprem = DD.riskprem.copy().isAPartOf(covtrec); //ILIFE-7845
	public PackedDecimalData commPrem = DD.coyprem.copy().isAPartOf(covtrec);  //IBPLIFE-5237
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public CovtpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for CovtpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public CovtpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for CovtpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovtpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for CovtpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public CovtpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("COVTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"CRTABLE, " +
							"RCESDTE, " +
							"PCESDTE, " +
							"RCESAGE, " +
							"PCESAGE, " +
							"RCESTRM, " +
							"PCESTRM, " +
							"SUMINS, " +
							"LIENCD, " +
							"MORTCLS, " +
							"JLIFE, " +
							"RSUNIN, " +
							"RUNDTE, " +
							"POLINC, " +
							"NUMAPP, " +
							"EFFDATE, " +
							"SEX01, " +
							"SEX02, " +
							"ANBCCD01, " +
							"ANBCCD02, " +
							"BILLFREQ, " +
							"BILLCHNL, " +
							"SEQNBR, " +
							"SINGP, " +
							"INSTPREM, " +
							"PLNSFX, " +
							"PAYRSEQNO, " +
							"CNTCURR, " +
							"BCESAGE, " +
							"BCESTRM, " +
							"BCESDTE, " +
							"BAPPMETH, " +
							"ZBINSTPREM, " +
							"ZLINSTPREM, " +
							"ZDIVOPT, " +
							"PAYCOY, " +
							"PAYCLT, " +
							"PAYMTH, " +
							"BANKKEY, " +
							"BANKACCKEY, " +
							"PAYCURR, " +
							"FACTHOUS, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"LOADPER ," + //TSD-306
						    "RATEADJ, " + //TSD-306
						    "FLTMORT, " + //TSD-306
						    "PREMADJ, " + //TSD-306
						    "AGEADJ, " + //TSD-306
						    "ZSTPDUTY01, " +
						    "ZCLSTATE, " +
						    "GMIB, " +    //BRD-NBP-012 
						    "GMDB, " +    
						    "GMWB, " +
						    "LNKGNO, " +
						    "LNKGSUBREFNO, " +
						    "TPDTYPE," + //ILIFE-7118
						    "LNKGIND," +
						    "CASHVALARER," +   //ICIL-753
							"SINGPREMTYPE," +//ILIFE-7805
							"RISKPREM," + //ILIFE-7845
							"COMMPREM," + //IBPLIFE-5237
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     crtable,
                                     riskCessDate,
                                     premCessDate,
                                     riskCessAge,
                                     premCessAge,
                                     riskCessTerm,
                                     premCessTerm,
                                     sumins,
                                     liencd,
                                     mortcls,
                                     jlife,
                                     reserveUnitsInd,
                                     reserveUnitsDate,
                                     polinc,
                                     numapp,
                                     effdate,
                                     sex01,
                                     sex02,
                                     anbAtCcd01,
                                     anbAtCcd02,
                                     billfreq,
                                     billchnl,
                                     seqnbr,
                                     singp,
                                     instprem,
                                     planSuffix,
                                     payrseqno,
                                     cntcurr,
                                     benCessAge,
                                     benCessTerm,
                                     benCessDate,
                                     bappmeth,
                                     zbinstprem,
                                     zlinstprem,
                                     zdivopt,
                                     paycoy,
                                     payclt,
                                     paymth,
                                     bankkey,
                                     bankacckey,
                                     paycurr,
                                     facthous,
                                     userProfile,
                                     jobName,
                                     datime,
                                     loadper, //TSD-306
                                 	 rateadj, //TSD-306
                                 	 fltmort, //TSD-306
                                 	 premadj, //TSD-306
                                     ageadj, //TSD-306
                                     zstpduty01,
                                     zclstate,
                                     gmib,  //BRD-NBP-012 
                                     gmdb,  //BRD-NBP-012 
                                     gmwb,  //BRD-NBP-012 
                                     lnkgno,
                                     lnkgsubrefno,
                                     tpdtype, //ILIFE-7118
                                     lnkgind,
                                     cashvalarer,  //ICIL-753
									 singpremtype, //ILIFE-7805
									 riskprem, //ILIFE-7845
									 commPrem, //IBPLIFE-5237
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		crtable.clear();
  		riskCessDate.clear();
  		premCessDate.clear();
  		riskCessAge.clear();
  		premCessAge.clear();
  		riskCessTerm.clear();
  		premCessTerm.clear();
  		sumins.clear();
  		liencd.clear();
  		mortcls.clear();
  		jlife.clear();
  		reserveUnitsInd.clear();
  		reserveUnitsDate.clear();
  		polinc.clear();
  		numapp.clear();
  		effdate.clear();
  		sex01.clear();
  		sex02.clear();
  		anbAtCcd01.clear();
  		anbAtCcd02.clear();
  		billfreq.clear();
  		billchnl.clear();
  		seqnbr.clear();
  		singp.clear();
  		instprem.clear();
  		planSuffix.clear();
  		payrseqno.clear();
  		cntcurr.clear();
  		benCessAge.clear();
  		benCessTerm.clear();
  		benCessDate.clear();
  		bappmeth.clear();
  		zbinstprem.clear();
  		zlinstprem.clear();
  		zdivopt.clear();
  		paycoy.clear();
  		payclt.clear();
  		paymth.clear();
  		bankkey.clear();
  		bankacckey.clear();
  		paycurr.clear();
  		facthous.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		//TSD-306 Start
  		loadper.clear();
		rateadj.clear();
		fltmort.clear();
		premadj.clear();
		ageadj.clear(); 
		//TSD-306 End
		zstpduty01.clear();
		zclstate.clear();
		//BRD-NBP-012 -start
		gmib.clear();
		gmdb.clear();
		gmwb.clear();
		//BRD-NBP-012-end
		lnkgno.clear();
		lnkgsubrefno.clear();
		tpdtype.clear(); //ILIFE-7118
		lnkgind.clear();
		cashvalarer.clear(); //ICIL-753
		singpremtype.clear();//ILIFE-7805
		riskprem.clear(); //ILIFE-7845
		commPrem.clear(); //IBPLIFE-5237
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getCovtrec() {
  		return covtrec;
	}

	public FixedLengthStringData getCovtpfRecord() {
  		return covtpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setCovtrec(what);
	}

	public void setCovtrec(Object what) {
  		this.covtrec.set(what);
	}

	public void setCovtpfRecord(Object what) {
  		this.covtpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(covtrec.getLength());
		result.set(covtrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}