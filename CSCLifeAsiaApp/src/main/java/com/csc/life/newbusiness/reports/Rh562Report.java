package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH562.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh562Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22);
	private FixedLengthStringData datetexc01 = new FixedLengthStringData(22);
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private ZonedDecimalData instprem01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData instprem02 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData instprem03 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData instprem04 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData pcamtind01 = new FixedLengthStringData(1);
	private FixedLengthStringData pcamtind02 = new FixedLengthStringData(1);
	private FixedLengthStringData pcamtind03 = new FixedLengthStringData(1);
	private FixedLengthStringData pcamtind04 = new FixedLengthStringData(1);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData tgtpcnt01 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData tgtpcnt02 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData tgtpcnt03 = new ZonedDecimalData(5, 2);
	private ZonedDecimalData tgtpcnt04 = new ZonedDecimalData(5, 2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData znofpol01 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData znofpol02 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData znofpol03 = new ZonedDecimalData(5, 0);
	private ZonedDecimalData znofpol04 = new ZonedDecimalData(5, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh562Report() {
		super();
	}


	/**
	 * Print the XML for Rh562d01
	 */
	public void printRh562d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		cntdesc.setFieldName("cntdesc");
		cntdesc.setInternal(subString(recordData, 1, 30));
		instprem01.setFieldName("instprem01");
		instprem01.setInternal(subString(recordData, 31, 17));
		tgtpcnt01.setFieldName("tgtpcnt01");
		tgtpcnt01.setInternal(subString(recordData, 48, 5));
		pcamtind01.setFieldName("pcamtind01");
		pcamtind01.setInternal(subString(recordData, 53, 1));
		znofpol01.setFieldName("znofpol01");
		znofpol01.setInternal(subString(recordData, 54, 5));
		instprem02.setFieldName("instprem02");
		instprem02.setInternal(subString(recordData, 59, 17));
		tgtpcnt02.setFieldName("tgtpcnt02");
		tgtpcnt02.setInternal(subString(recordData, 76, 5));
		pcamtind02.setFieldName("pcamtind02");
		pcamtind02.setInternal(subString(recordData, 81, 1));
		znofpol02.setFieldName("znofpol02");
		znofpol02.setInternal(subString(recordData, 82, 5));
		instprem03.setFieldName("instprem03");
		instprem03.setInternal(subString(recordData, 87, 17));
		tgtpcnt03.setFieldName("tgtpcnt03");
		tgtpcnt03.setInternal(subString(recordData, 104, 5));
		pcamtind03.setFieldName("pcamtind03");
		pcamtind03.setInternal(subString(recordData, 109, 1));
		znofpol03.setFieldName("znofpol03");
		znofpol03.setInternal(subString(recordData, 110, 5));
		instprem04.setFieldName("instprem04");
		instprem04.setInternal(subString(recordData, 115, 17));
		tgtpcnt04.setFieldName("tgtpcnt04");
		tgtpcnt04.setInternal(subString(recordData, 132, 5));
		pcamtind04.setFieldName("pcamtind04");
		pcamtind04.setInternal(subString(recordData, 137, 1));
		znofpol04.setFieldName("znofpol04");
		znofpol04.setInternal(subString(recordData, 138, 5));
		printLayout("Rh562d01",			// Record name
			new BaseData[]{			// Fields:
				cntdesc,
				instprem01,
				tgtpcnt01,
				pcamtind01,
				znofpol01,
				instprem02,
				tgtpcnt02,
				pcamtind02,
				znofpol02,
				instprem03,
				tgtpcnt03,
				pcamtind03,
				znofpol03,
				instprem04,
				tgtpcnt04,
				pcamtind04,
				znofpol04
			}
		);

	}

	/**
	 * Print the XML for Rh562h01
	 */
	public void printRh562h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datetexc.setFieldName("datetexc");
		datetexc.setInternal(subString(recordData, 1, 22));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 23, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 24, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 54, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 64, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 66, 30));
		time.setFieldName("time");
		time.set(getTime());
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 96, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 99, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rh562h01",			// Record name
			new BaseData[]{			// Fields:
				datetexc,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				currcode,
				currencynm,
				pagnbr
			}
		);

		currentPrintLine.set(9);
	}

	/**
	 * Print the XML for Rh562h02
	 */
	public void printRh562h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh562h02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh562h03
	 */
	public void printRh562h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh562h03",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh562h04
	 */
	public void printRh562h04(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(22).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		datetexc01.setFieldName("datetexc01");
		datetexc01.setInternal(subString(recordData, 1, 22));
		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 23, 10));
		//IJTI-320 START
		if(indicArea != null){
		printLayout("Rh562h04",			// Record name
			new BaseData[]{			// Fields:
				datetexc01,
				effdate
			}
			, new Object[] {			// indicators
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind12", indicArea.charAt(12)},
				new Object[]{"ind13", indicArea.charAt(13)},
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind15", indicArea.charAt(15)},
				new Object[]{"ind16", indicArea.charAt(16)},
				new Object[]{"ind17", indicArea.charAt(17)},
				new Object[]{"ind18", indicArea.charAt(18)},
				new Object[]{"ind19", indicArea.charAt(19)},
				new Object[]{"ind20", indicArea.charAt(20)},
				new Object[]{"ind21", indicArea.charAt(21)},
				new Object[]{"ind22", indicArea.charAt(22)}
			}
		);
		}
		//IJTI-320 END

	}

	/**
	 * Print the XML for Rh562h05
	 */
	public void printRh562h05(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh562h05",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh562h06
	 */
	public void printRh562h06(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rh562h06",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh562t01
	 */
	public void printRh562t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		instprem01.setFieldName("instprem01");
		instprem01.setInternal(subString(recordData, 1, 17));
		tgtpcnt01.setFieldName("tgtpcnt01");
		tgtpcnt01.setInternal(subString(recordData, 18, 5));
		pcamtind01.setFieldName("pcamtind01");
		pcamtind01.setInternal(subString(recordData, 23, 1));
		znofpol01.setFieldName("znofpol01");
		znofpol01.setInternal(subString(recordData, 24, 5));
		instprem02.setFieldName("instprem02");
		instprem02.setInternal(subString(recordData, 29, 17));
		tgtpcnt02.setFieldName("tgtpcnt02");
		tgtpcnt02.setInternal(subString(recordData, 46, 5));
		pcamtind02.setFieldName("pcamtind02");
		pcamtind02.setInternal(subString(recordData, 51, 1));
		znofpol02.setFieldName("znofpol02");
		znofpol02.setInternal(subString(recordData, 52, 5));
		instprem03.setFieldName("instprem03");
		instprem03.setInternal(subString(recordData, 57, 17));
		tgtpcnt03.setFieldName("tgtpcnt03");
		tgtpcnt03.setInternal(subString(recordData, 74, 5));
		pcamtind03.setFieldName("pcamtind03");
		pcamtind03.setInternal(subString(recordData, 79, 1));
		znofpol03.setFieldName("znofpol03");
		znofpol03.setInternal(subString(recordData, 80, 5));
		instprem04.setFieldName("instprem04");
		instprem04.setInternal(subString(recordData, 85, 17));
		tgtpcnt04.setFieldName("tgtpcnt04");
		tgtpcnt04.setInternal(subString(recordData, 102, 5));
		pcamtind04.setFieldName("pcamtind04");
		pcamtind04.setInternal(subString(recordData, 107, 1));
		znofpol04.setFieldName("znofpol04");
		znofpol04.setInternal(subString(recordData, 108, 5));
		printLayout("Rh562t01",			// Record name
			new BaseData[]{			// Fields:
				instprem01,
				tgtpcnt01,
				pcamtind01,
				znofpol01,
				instprem02,
				tgtpcnt02,
				pcamtind02,
				znofpol02,
				instprem03,
				tgtpcnt03,
				pcamtind03,
				znofpol03,
				instprem04,
				tgtpcnt04,
				pcamtind04,
				znofpol04
			}
		);

	}

	/**
	 * Print the XML for Rh562t02
	 */
	public void printRh562t02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(3);

		instprem01.setFieldName("instprem01");
		instprem01.setInternal(subString(recordData, 1, 17));
		tgtpcnt01.setFieldName("tgtpcnt01");
		tgtpcnt01.setInternal(subString(recordData, 18, 5));
		pcamtind01.setFieldName("pcamtind01");
		pcamtind01.setInternal(subString(recordData, 23, 1));
		znofpol01.setFieldName("znofpol01");
		znofpol01.setInternal(subString(recordData, 24, 5));
		instprem02.setFieldName("instprem02");
		instprem02.setInternal(subString(recordData, 29, 17));
		tgtpcnt02.setFieldName("tgtpcnt02");
		tgtpcnt02.setInternal(subString(recordData, 46, 5));
		pcamtind02.setFieldName("pcamtind02");
		pcamtind02.setInternal(subString(recordData, 51, 1));
		znofpol02.setFieldName("znofpol02");
		znofpol02.setInternal(subString(recordData, 52, 5));
		instprem03.setFieldName("instprem03");
		instprem03.setInternal(subString(recordData, 57, 17));
		tgtpcnt03.setFieldName("tgtpcnt03");
		tgtpcnt03.setInternal(subString(recordData, 74, 5));
		pcamtind03.setFieldName("pcamtind03");
		pcamtind03.setInternal(subString(recordData, 79, 1));
		znofpol03.setFieldName("znofpol03");
		znofpol03.setInternal(subString(recordData, 80, 5));
		instprem04.setFieldName("instprem04");
		instprem04.setInternal(subString(recordData, 85, 17));
		tgtpcnt04.setFieldName("tgtpcnt04");
		tgtpcnt04.setInternal(subString(recordData, 102, 5));
		pcamtind04.setFieldName("pcamtind04");
		pcamtind04.setInternal(subString(recordData, 107, 1));
		znofpol04.setFieldName("znofpol04");
		znofpol04.setInternal(subString(recordData, 108, 5));
		printLayout("Rh562t02",			// Record name
			new BaseData[]{			// Fields:
				instprem01,
				tgtpcnt01,
				pcamtind01,
				znofpol01,
				instprem02,
				tgtpcnt02,
				pcamtind02,
				znofpol02,
				instprem03,
				tgtpcnt03,
				pcamtind03,
				znofpol03,
				instprem04,
				tgtpcnt04,
				pcamtind04,
				znofpol04
			}
		);

	}


}
