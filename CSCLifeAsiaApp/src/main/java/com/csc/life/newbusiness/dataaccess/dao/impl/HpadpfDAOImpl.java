package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class HpadpfDAOImpl extends BaseDAOImpl<Hpadpf> implements HpadpfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(HpadpfDAOImpl.class);

	@Override
	public void updateHpadRcds(List<Hpadpf> updateList) {
		if (updateList == null || updateList.isEmpty()) {
			return;
		}
		String stmt = "UPDATE HPADPF SET PROCFLG=? WHERE CHDRCOY=? and CHDRNUM=? ";
		PreparedStatement ps = getPrepareStatement(stmt);
		try {
			for (Hpadpf c : updateList) {
				ps.setString(1, c.getProcflg());
				ps.setString(2, c.getChdrcoy());
				ps.setString(3, c.getChdrnum());
				ps.addBatch();
			}
			ps.executeBatch();
		} catch (SQLException e) {
			LOGGER.error("updateHpadRcds()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}

	}

	@Override
	public Hpadpf getHpadData(String chdrcoy, String chdrnum) {
		// ILIFE-5127
		 StringBuilder sqlCovrSelect1 = new StringBuilder(
                 "SELECT CHDRCOY,CHDRNUM,HISSDTE,PACKDATE,DEEMDATE, HPROPDTE, HPRRCVDT, DESPDATE, ZNFOPT, DLVRMODE,"
                 + "HUWDCDTE,ZDOCTOR,HOISSDTE,REFUNDOVERPAY,RSKCOMMDATE, DECLDATE, FIRSTPRMRCPDATE, "
                 + "CNFIRMTNINTENTDATE, UNIQUE_NUMBER, TNTAPDATE FROM HPADPF WHERE CHDRCOY = ? "
                 + "AND CHDRNUM = ? "); //ICIL-297 ILJ-40,  //ILIFE-8804 //IBPLIFE-2470
         sqlCovrSelect1
                 .append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC");
         PreparedStatement ps = getPrepareStatement(sqlCovrSelect1.toString());
       
 		ResultSet rs = null;
 		Hpadpf hpadpf = null;

 		try {
 			//ILB-508 Start by dpuhawan
 			//ps.setInt(1, Integer.parseInt(chdrcoy));
 			ps.setString(1, chdrcoy.trim());
 			//ILB-508 End
 			ps.setString(2, chdrnum);
 			
 			rs = ps.executeQuery();

 			while (rs.next()) {
 				hpadpf = new Hpadpf();
 				
 				hpadpf.setChdrcoy(rs.getString(1));
 				hpadpf.setChdrnum(rs.getString(2));
 				hpadpf.setHissdte(rs.getInt(3));
 				hpadpf.setPackdate(rs.getInt("PACKDATE")); //ICIL-297
 				hpadpf.setDeemdate(rs.getInt("DEEMDATE")); //ICIL-297
 				hpadpf.setDespdate(rs.getInt("DESPDATE")); //ILIFE-8804
 				hpadpf.setHpropdte(rs.getInt("HPROPDTE"));
 				hpadpf.setHprrcvdt(rs.getInt("HPRRCVDT"));
 				hpadpf.setZnfopt(rs.getString("ZNFOPT"));
 				hpadpf.setDlvrmode(rs.getString("DLVRMODE"));
 				hpadpf.setHuwdcdte(rs.getInt("HUWDCDTE"));
 	 			hpadpf.setZdoctor(rs.getString("ZDOCTOR")==null? " " : rs.getString("ZDOCTOR"));
 				hpadpf.setHoissdte(rs.getInt("HOISSDTE"));
 				hpadpf.setRefundOverpay(rs.getString("REFUNDOVERPAY"));//fwang3 ICIL-4
 				//RSKCOMMDATE, DECLDATE,
 				hpadpf.setRskcommdate(rs.getInt("RSKCOMMDATE"));//ILJ-40
 				hpadpf.setDecldate(rs.getInt("DECLDATE"));//ILJ-40
 				hpadpf.setFirstprmrcpdate(rs.getInt("FIRSTPRMRCPDATE"));//ILJ-40
 				hpadpf.setCnfirmtnIntentDate(rs.getInt("CNFIRMTNINTENTDATE"));//ILJ-62
 				hpadpf.setUniqueNumber(rs.getLong("UNIQUE_NUMBER")); // fwang3 fix bug ILIFE-6803 
 			// ILIFE-5127
 				hpadpf.setTntapdate(rs.getInt("TNTAPDATE"));//IBPLIFE-2470
			}
		} catch (SQLException e) {
			LOGGER.error("getHpadData()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}

		return hpadpf;			
	
	}

	@Override
	public void updateRecord(Hpadpf pf) {
		String stmt = "UPDATE HPADPF SET Hpropdte=?,Hprrcvdt=?,Hissdte=?,Hoissdte=?,Huwdcdte=?,"
				+ " Zdoctor=?, Znfopt=?, Zsufcdte=?, Validflag=?, Procflg=?, Dlvrmode=?,"
				+ " Despdate=?, Packdate=?, Remdte=?, Deemdate=?, Nxtdte=?, Incexc=?,"
				+ " REFUNDOVERPAY=?, RSKCOMMDATE=?, DECLDATE=?, CNFIRMTNINTENTDATE =?,"
				+ " FIRSTPRMRCPDATE=?, TNTAPDATE=? WHERE unique_number=? "; //ILIFE-8709, ILIFE-8804
		PreparedStatement ps = getPrepareStatement(stmt);
		try {
			int i=1;
			ps.setInt(i++, pf.getHpropdte());
			ps.setInt(i++, pf.getHprrcvdt());
			ps.setInt(i++, pf.getHissdte());
			ps.setInt(i++, pf.getHoissdte()); //ILIFE-8709
			ps.setInt(i++, pf.getHuwdcdte());
			ps.setString(i++, pf.getZdoctor()); //ILIFE-8804
			ps.setString(i++, pf.getZnfopt());
			ps.setInt(i++, pf.getZsufcdte());
			ps.setString(i++, pf.getValidflag());
			ps.setString(i++, pf.getProcflg()); //ILIFE-8804
			ps.setString(i++, pf.getDlvrmode());
			ps.setInt(i++, pf.getDespdate());
			ps.setInt(i++, pf.getPackdate());
			ps.setInt(i++, pf.getRemdte());
			ps.setInt(i++, pf.getDeemdate());
			ps.setInt(i++, pf.getNxtdte());
			ps.setString(i++, pf.getIncexc()); //ILIFE-8804
			ps.setString(i++, pf.getRefundOverpay()); //fwang3 ICIL-4
			ps.setInt(i++, pf.getRskcommdate());//ILJ-40
            ps.setInt(i++, pf.getDecldate());//ILJ-40
            ps.setInt(i++, pf.getCnfirmtnIntentDate());//ILJ-62
            ps.setInt(i++, pf.getFirstprmrcpdate());
            ps.setInt(i++, pf.getTntapdate());
			ps.setLong(i++, pf.getUniqueNumber());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}

	@Override
	public void insertRecord(Hpadpf pf) {
		String stmt = "INSERT INTO HPADPF (CHDRCOY,CHDRNUM,VALIDFLAG,HPROPDTE,HPRRCVDT,HISSDTE,"
				+ "HUWDCDTE,HOISSDTE,ZNFOPT,ZSUFCDTE,DLVRMODE,DESPDATE,PACKDATE,REMDTE,DEEMDATE,"
				+ "NXTDTE,USRPRF,JOBNM,DATIME,REFUNDOVERPAY, RSKCOMMDATE, DECLDATE,"
				+ " CNFIRMTNINTENTDATE, FIRSTPRMRCPDATE, TNTAPDATE) VALUES(?,?,?,?,"
				+ "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = getPrepareStatement(stmt);
		try {
			int i=1;
			ps.setString(i++, pf.getChdrcoy());
			ps.setString(i++, pf.getChdrnum());
			ps.setString(i++, pf.getValidflag());
			ps.setInt(i++, pf.getHpropdte());
			ps.setInt(i++, pf.getHprrcvdt());
			ps.setInt(i++, pf.getHissdte());
			ps.setInt(i++, pf.getHuwdcdte());
			ps.setInt(i++, pf.getHoissdte());
			ps.setString(i++, pf.getZnfopt());
			ps.setInt(i++, pf.getZsufcdte());
			ps.setString(i++, pf.getDlvrmode());
			ps.setInt(i++, pf.getDespdate());
			ps.setInt(i++, pf.getPackdate());
			ps.setInt(i++, pf.getRemdte());
			ps.setInt(i++, pf.getDeemdate());
			ps.setInt(i++, pf.getNxtdte());
			ps.setString(i++, this.getUsrprf());
			ps.setString(i++, this.getJobnm());
            ps.setTimestamp(i++, new Timestamp(System.currentTimeMillis()));
            ps.setString(i++, pf.getRefundOverpay());//fwang3 ICIL-4
            ps.setInt(i++, pf.getRskcommdate());//ILJ-40
            ps.setInt(i++, pf.getDecldate());//ILJ-40
            ps.setInt(i++, pf.getCnfirmtnIntentDate());//ILJ-62
            ps.setInt(i++, pf.getFirstprmrcpdate());
            ps.setInt(i++, pf.getTntapdate());
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("insertRecord()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	@Override
	public void updateDoctor(String chdrcoy, String chdrnum,String doctor){
		String stmt = "UPDATE HPADPF SET ZDOCTOR=?, JOBNM=?,USRPRF=?,DATIME=? WHERE CHDRCOY ='"+ chdrcoy + "' AND CHDRNUM = '"+ chdrnum+"'" ;
		PreparedStatement ps = getPrepareStatement(stmt);
		try {
		
			ps.setString(1, doctor);
			ps.setString(2, getJobnm());
            ps.setString(3, getUsrprf());
            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateDoctor()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	//ILIFE-6289
	public void updateHuwdcdte(String chdrcoy, String chdrnum,int huwdcdte){
		String sql = "UPDATE HPADPF SET HUWDCDTE=?, JOBNM=?,USRPRF=?,DATIME=? WHERE CHDRCOY ='"+ chdrcoy + "' AND CHDRNUM = '"+ chdrnum+"'" ;
		PreparedStatement ps = getPrepareStatement(sql);
		try {
		
			ps.setInt(1, huwdcdte);
			ps.setString(2, getJobnm());
            ps.setString(3, getUsrprf());
            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
			ps.executeUpdate();
		} catch (SQLException e) {
			LOGGER.error("updateHuwdcdte()", e);//IJTI-1485
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, null);
		}
	}
	
	//ILJ-42
		public void updateFirstprmrcpdate(String chdrcoy, String chdrnum,int firstPremRcpDt){
			String sql = "UPDATE HPADPF SET FIRSTPRMRCPDATE=?, JOBNM=?,USRPRF=?,DATIME=? WHERE CHDRCOY ='"+ chdrcoy + "' AND CHDRNUM = '"+ chdrnum+"'" ;
			PreparedStatement ps = getPrepareStatement(sql);
			try {
			
				ps.setInt(1, firstPremRcpDt);
				ps.setString(2, getJobnm());
	            ps.setString(3, getUsrprf());
	            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateFirstprmrcpdate()", e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}
		
		public void updateRiskCommDate(String chdrcoy, String chdrnum,int riskCommDate){
			String sql = "UPDATE HPADPF SET RSKCOMMDATE=?, JOBNM=?,USRPRF=?,DATIME=? WHERE CHDRCOY=? AND CHDRNUM=?" ;
			PreparedStatement ps = getPrepareStatement(sql);
			try {
			
				ps.setInt(1, riskCommDate);
				ps.setString(2, getJobnm());
	            ps.setString(3, getUsrprf());
	            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
	            ps.setString(5, chdrcoy);
	            ps.setString(6, chdrnum);
				ps.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateRiskCommDate()", e);//IJTI-1485
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
		}
		
		@Override
		public void updateRecordsByUniqueNumber(List<Hpadpf> data) {
			String stmt = "UPDATE HPADPF SET REMDTE=?,DEEMDATE=?,NXTDTE=?,USRPRF=?,JOBNM=?,DATIME=? WHERE UNIQUE_NUMBER=?"; 
			
			try(PreparedStatement ps = getPrepareStatement(stmt)) {				
				for(Hpadpf pf : data)
				{					
					ps.setInt(1, pf.getRemdte());
					ps.setInt(2, pf.getDeemdate());
					ps.setInt(3, pf.getNxtdte());
					ps.setString(4,getUsrprf());
					ps.setString(5,getJobnm());
					ps.setTimestamp(6, getDatime());
					ps.setLong(7, pf.getUniqueNumber());
					ps.addBatch();
				}				
				ps.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateRecordsByUniqueNumber()", e);
				throw new SQLRuntimeException(e);
			}
		}
		
		//IBPLIFE-2470 -- STARTS
		public void updateTntapdate(String chdrcoy, String chdrnum, int tntapdate){
			String sql = "UPDATE HPADPF SET TNTAPDATE=?, JOBNM=?,USRPRF=?,DATIME=? WHERE "
					+ "CHDRCOY ='"+ chdrcoy + "' AND CHDRNUM = '"+ chdrnum+"'" ;
			PreparedStatement ps = getPrepareStatement(sql);
			try {
			
				ps.setInt(1, tntapdate);
				ps.setString(2, getJobnm());
	            ps.setString(3, getUsrprf());
	            ps.setTimestamp(4, new Timestamp(System.currentTimeMillis()));
				ps.executeUpdate();
			} catch (SQLException e) {
				LOGGER.error("updateHuwdcdte()", e);
			} finally {
				close(ps, null);			
			}
		}
		//IBPLIFE-2470 -- ENDS
	   public boolean updateFirstprmrcpdateList(List<Hpadpf> hpadpfList){
			String sql = "UPDATE HPADPF SET FIRSTPRMRCPDATE=?, JOBNM=?,USRPRF=?,DATIME=? WHERE CHDRCOY =? AND CHDRNUM = ? " ;
			PreparedStatement ps = null;
			ResultSet rs = null;
			boolean isUpdated = false;
			try {
				ps = getPrepareStatement(sql);
				int ctr;
				for(Hpadpf hpadpf : hpadpfList){
					ctr = 0;
					ps.setInt(++ctr, hpadpf.getFirstprmrcpdate());
					ps.setString(++ctr, getJobnm());
		            ps.setString(++ctr, getUsrprf());
		            ps.setTimestamp(++ctr, new Timestamp(System.currentTimeMillis()));
		            ps.setString(++ctr, hpadpf.getChdrcoy());
		            ps.setString(++ctr, hpadpf.getChdrnum());
		            ps.addBatch();	
				}
				ps.executeBatch();
				isUpdated = true;
			} catch (SQLException e) {
				LOGGER.error("updateFirstprmrcpdateList()", e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, null);
			}
			return isUpdated;
		}
}
