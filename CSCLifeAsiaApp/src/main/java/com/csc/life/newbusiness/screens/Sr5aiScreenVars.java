package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Sr5aiScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(3056);
	public FixedLengthStringData dataFields = new FixedLengthStringData(3008).isAPartOf(dataArea, 0);
	public FixedLengthStringData excda = DD.excda.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData excltxt = new FixedLengthStringData(1500).isAPartOf(dataFields,8);
	public FixedLengthStringData exadtxt = new FixedLengthStringData(1500).isAPartOf(dataFields,1508);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 3008);
	public FixedLengthStringData excdaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData excltxtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData exadtxtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 3020);
	public FixedLengthStringData[] excdaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] excltxtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] exadtxtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	/*Row position*/
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
		/*Column position*/
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sr5aiscreenWritten = new LongData(0);
	public LongData Sr5aiprotectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


public Sr5aiScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
fieldIndMap.put(excdaOut,new String[] {"02","05", "-02",null, null, null, null, null, null, null, null, null});
fieldIndMap.put(exadtxtOut,new String[] {"03","04", "-03",null, null, null, null, null, null, null, null, null});
fieldIndMap.put(excltxtOut,new String[] {"06","07", "-06",null, null, null, null, null, null, null, null, null});
screenFields = new BaseData[] {excda,excltxt,exadtxt};
		screenOutFields = new BaseData[][] {excdaOut,excltxtOut,exadtxtOut};
		screenErrFields = new BaseData[] {excdaErr,excltxtErr,exadtxtErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sr5aiscreen.class;
		protectRecord = Sr5aiprotect.class;
	}
}
