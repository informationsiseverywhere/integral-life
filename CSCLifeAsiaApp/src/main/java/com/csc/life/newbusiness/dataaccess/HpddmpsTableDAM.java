package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HpddmpsTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:16
 * Class transformed from HPDDMPS.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HpddmpsTableDAM extends HpadpfTableDAM {

	public HpddmpsTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HPDDMPS");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", HUWDCDTE";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "VALIDFLAG, " +
		            "HPROPDTE, " +
		            "HPRRCVDT, " +
		            "HISSDTE, " +
		            "HUWDCDTE, " +
		            "HOISSDTE, " +
		            "ZDOCTOR, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "HUWDCDTE DESC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "HUWDCDTE ASC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               validflag,
                               hpropdte,
                               hprrcvdt,
                               hissdte,
                               huwdcdte,
                               hoissdte,
                               zdoctor,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(58);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getHuwdcdte().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, huwdcdte);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller7 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller7.setInternal(huwdcdte.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(89);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ getChdrnum().toInternal()
					+ getValidflag().toInternal()
					+ getHpropdte().toInternal()
					+ getHprrcvdt().toInternal()
					+ getHissdte().toInternal()
					+ nonKeyFiller7.toInternal()
					+ getHoissdte().toInternal()
					+ getZdoctor().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, hpropdte);
			what = ExternalData.chop(what, hprrcvdt);
			what = ExternalData.chop(what, hissdte);
			what = ExternalData.chop(what, nonKeyFiller7);
			what = ExternalData.chop(what, hoissdte);
			what = ExternalData.chop(what, zdoctor);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public PackedDecimalData getHuwdcdte() {
		return huwdcdte;
	}
	public void setHuwdcdte(Object what) {
		setHuwdcdte(what, false);
	}
	public void setHuwdcdte(Object what, boolean rounded) {
		if (rounded)
			huwdcdte.setRounded(what);
		else
			huwdcdte.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getHpropdte() {
		return hpropdte;
	}
	public void setHpropdte(Object what) {
		setHpropdte(what, false);
	}
	public void setHpropdte(Object what, boolean rounded) {
		if (rounded)
			hpropdte.setRounded(what);
		else
			hpropdte.set(what);
	}	
	public PackedDecimalData getHprrcvdt() {
		return hprrcvdt;
	}
	public void setHprrcvdt(Object what) {
		setHprrcvdt(what, false);
	}
	public void setHprrcvdt(Object what, boolean rounded) {
		if (rounded)
			hprrcvdt.setRounded(what);
		else
			hprrcvdt.set(what);
	}	
	public PackedDecimalData getHissdte() {
		return hissdte;
	}
	public void setHissdte(Object what) {
		setHissdte(what, false);
	}
	public void setHissdte(Object what, boolean rounded) {
		if (rounded)
			hissdte.setRounded(what);
		else
			hissdte.set(what);
	}	
	public PackedDecimalData getHoissdte() {
		return hoissdte;
	}
	public void setHoissdte(Object what) {
		setHoissdte(what, false);
	}
	public void setHoissdte(Object what, boolean rounded) {
		if (rounded)
			hoissdte.setRounded(what);
		else
			hoissdte.set(what);
	}	
	public FixedLengthStringData getZdoctor() {
		return zdoctor;
	}
	public void setZdoctor(Object what) {
		zdoctor.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		huwdcdte.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		chdrnum.clear();
		validflag.clear();
		hpropdte.clear();
		hprrcvdt.clear();
		hissdte.clear();
		nonKeyFiller7.clear();
		hoissdte.clear();
		zdoctor.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}