package com.csc.life.newbusiness.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.newbusiness.dataaccess.dao.UwrspfDAO;
import com.csc.life.newbusiness.dataaccess.model.Uwrspf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class UwrspfDAOImpl extends BaseDAOImpl<Uwrspf> implements UwrspfDAO {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(UwrspfDAOImpl.class);
	public void insertUwrsData(List<Uwrspf> uwrspfList){
		StringBuilder sb = new StringBuilder();
		sb.append(" INSERT INTO UWRSPF ( CHDRCOY , CHDRNUM , REASON , TRANNO , TRANCDE , CRTABLE, ");
		sb.append(" TERMID , TRDT  , TRTM  , USER_T ) ");	
		sb.append(" VALUES(?,?,?,?,?,?,?,?,?,?) ");
		PreparedStatement ps=null;
		try{
			ps=getConnection().prepareStatement(sb.toString());
			for (Uwrspf uwrspf : uwrspfList) {
				int i=1;
				ps.setString(i++, uwrspf.getChdrcoy());
				ps.setString(i++, uwrspf.getChdrnum());
				ps.setString(i++, uwrspf.getReason());
				ps.setInt(i++, uwrspf.getTranno());
				ps.setString(i++, uwrspf.getTrancde());
				ps.setString(i++, uwrspf.getCrtable());
				ps.setString(i++, uwrspf.getTermid());
				ps.setInt(i++, uwrspf.getTrdt());
				ps.setInt(i++, uwrspf.getTrtm());
				ps.setInt(i++, uwrspf.getUser());
				ps.addBatch();
            }		
			ps.executeBatch();
		}
		catch(SQLException e){
			LOGGER.error("insertUwrsData()", e);
            throw new SQLRuntimeException(e);
		}
		finally {
            close(ps, null);
        }
	}
	
	public void updateUwrsData(List<Uwrspf> uwrspfList){
		StringBuilder sb = new StringBuilder();
		sb.append(" UPDATE UWRSPF SET REASON=?,TERMID =?, TRDT=? , TRTM=?  , USER_T=? ");	
		sb.append(" WHERE UNIQUE_NUMBER=? ");
		PreparedStatement ps=null;
		try{
			ps=getConnection().prepareStatement(sb.toString());
			for (Uwrspf uwrspf : uwrspfList) {
				int i=1;
				ps.setString(i++, uwrspf.getReason());
				ps.setString(i++, uwrspf.getTermid());
				ps.setInt(i++, uwrspf.getTrdt());
				ps.setInt(i++, uwrspf.getTrtm());
				ps.setInt(i++, uwrspf.getUser());
				ps.setLong(i++, uwrspf.getUniqueNumber());
				ps.addBatch();
            }		
			ps.executeBatch();
		}
		catch(SQLException e){
			LOGGER.error("updateUwrsData()", e);
            throw new SQLRuntimeException(e);
		}
		finally {
            close(ps, null);
        }
	}
	
	public void deleteUwrsData(Uwrspf uwrspf) {
		  String sql = "DELETE FROM UWRSPF WHERE CHDRCOY=? AND CHDRNUM=? AND CRTABLE=? ";
			PreparedStatement ps = getPrepareStatement(sql);
			ResultSet rs = null;
			try{
			    ps.setString(1,uwrspf.getChdrcoy());
			    ps.setString(2,uwrspf.getChdrnum());
			    ps.setString(3,uwrspf.getCrtable());
			    ps.executeUpdate();				
				
			}catch (SQLException e) {
				LOGGER.error("deleteCovrpfRecord()",e);
				throw new SQLRuntimeException(e);
			} finally {
				close(ps, rs);			
			}				
	}
	
	public List<Uwrspf> searchUwrspfRecord(String chdrcoy, String chdrnum) {
        StringBuilder sqlSelect1 = new StringBuilder();
        sqlSelect1.append("SELECT UNIQUE_NUMBER,CHDRNUM,CHDRCOY,CRTABLE,REASON,TRANNO,TRANCDE ");
        sqlSelect1.append("FROM UWRSPF WHERE CHDRCOY=? AND CHDRNUM = ? "); 
        
        PreparedStatement psSelect = getPrepareStatement(sqlSelect1.toString());
        ResultSet sqlrs = null;
        List<Uwrspf> uwrspfSearchResult = new ArrayList<>();
        try {
        	psSelect.setString(1, chdrcoy);
        	psSelect.setString(2, chdrnum);
        	sqlrs = executeQuery(psSelect);
            while (sqlrs.next()) {
            	Uwrspf uwrspf = new Uwrspf();
            	uwrspf.setUniqueNumber(sqlrs.getLong(1));
                uwrspf.setChdrnum(sqlrs.getString(2));
                uwrspf.setChdrcoy(sqlrs.getString(3));
                uwrspf.setCrtable(sqlrs.getString(4));
                uwrspf.setReason(sqlrs.getString(5));
				uwrspf.setTranno(sqlrs.getInt(6));
                uwrspf.setTrancde(sqlrs.getString(7));
                uwrspfSearchResult.add(uwrspf);
            }

        } catch (SQLException e) {
			LOGGER.error("searchUwrspfRecord()", e);
            throw new SQLRuntimeException(e);
        } finally {
            close(psSelect, sqlrs);
        }
        return uwrspfSearchResult;
    }
}
