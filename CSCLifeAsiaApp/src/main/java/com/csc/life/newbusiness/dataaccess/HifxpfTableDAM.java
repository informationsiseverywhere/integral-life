package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HifxpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:29
 * Class transformed from HIFXPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HifxpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 97;
	public FixedLengthStringData hifxrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hifxpfRecord = hifxrec;
	
	public PackedDecimalData mnth = DD.mnth.copy().isAPartOf(hifxrec);
	public PackedDecimalData year = DD.year.copy().isAPartOf(hifxrec);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(hifxrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(hifxrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(hifxrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(hifxrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(hifxrec);
	public PackedDecimalData annprem = DD.annprem.copy().isAPartOf(hifxrec);
	public PackedDecimalData singp = DD.singp.copy().isAPartOf(hifxrec);
	public PackedDecimalData cntcount = DD.cntcount.copy().isAPartOf(hifxrec);
	public FixedLengthStringData fieldType = DD.type.copy().isAPartOf(hifxrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(hifxrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(hifxrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(hifxrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(hifxrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public HifxpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HifxpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HifxpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HifxpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HifxpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HifxpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HifxpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HIFXPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"MNTH, " +
							"YEAR, " +
							"COMPANY, " +
							"CNTBRANCH, " +
							"CNTTYPE, " +
							"CNTCURR, " +
							"SUMINS, " +
							"ANNPREM, " +
							"SINGP, " +
							"CNTCOUNT, " +
							"TYPE_T, " +
							"EFFDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     mnth,
                                     year,
                                     company,
                                     cntbranch,
                                     cnttype,
                                     cntcurr,
                                     sumins,
                                     annprem,
                                     singp,
                                     cntcount,
                                     fieldType,
                                     effdate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		mnth.clear();
  		year.clear();
  		company.clear();
  		cntbranch.clear();
  		cnttype.clear();
  		cntcurr.clear();
  		sumins.clear();
  		annprem.clear();
  		singp.clear();
  		cntcount.clear();
  		fieldType.clear();
  		effdate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHifxrec() {
  		return hifxrec;
	}

	public FixedLengthStringData getHifxpfRecord() {
  		return hifxpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHifxrec(what);
	}

	public void setHifxrec(Object what) {
  		this.hifxrec.set(what);
	}

	public void setHifxpfRecord(Object what) {
  		this.hifxpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hifxrec.getLength());
		result.set(hifxrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}