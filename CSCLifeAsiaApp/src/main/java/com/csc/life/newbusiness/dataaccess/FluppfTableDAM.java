package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FluppfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:53
 * Class transformed from FLUPPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FluppfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 184;
	public FixedLengthStringData fluprec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData fluppfRecord = fluprec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(fluprec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(fluprec);
	public PackedDecimalData fupno = DD.fupno.copy().isAPartOf(fluprec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(fluprec);
	public FixedLengthStringData fuptype = DD.fuptyp.copy().isAPartOf(fluprec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(fluprec);
	public FixedLengthStringData jlife = DD.jlife.copy().isAPartOf(fluprec);
	public FixedLengthStringData fupcode = DD.fupcde.copy().isAPartOf(fluprec);
	public FixedLengthStringData fupstat = DD.fupsts.copy().isAPartOf(fluprec);
	public PackedDecimalData fupremdt = DD.fupdt.copy().isAPartOf(fluprec);
	public FixedLengthStringData fupremk = DD.fuprmk.copy().isAPartOf(fluprec);
	public FixedLengthStringData clamnum = DD.clamnum.copy().isAPartOf(fluprec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(fluprec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(fluprec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(fluprec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(fluprec);
	public FixedLengthStringData zautoind = DD.zautoind.copy().isAPartOf(fluprec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(fluprec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(fluprec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(fluprec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(fluprec);
	public FixedLengthStringData crtuser = DD.crtuser.copy().isAPartOf(fluprec);
	public PackedDecimalData crtdate = DD.crtdate.copy().isAPartOf(fluprec);
	public FixedLengthStringData lstupuser = DD.lstupuser.copy().isAPartOf(fluprec);
	public PackedDecimalData zlstupdt = DD.zlstupdt.copy().isAPartOf(fluprec);
	public PackedDecimalData fuprcvd = DD.fuprcvd.copy().isAPartOf(fluprec);
	public PackedDecimalData exprdate = DD.exprdate.copy().isAPartOf(fluprec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public FluppfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for FluppfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public FluppfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for FluppfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public FluppfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for FluppfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public FluppfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("FLUPPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"FUPNO, " +
							"TRANNO, " +
							"FUPTYP, " +
							"LIFE, " +
							"JLIFE, " +
							"FUPCDE, " +
							"FUPSTS, " +
							"FUPDT, " +
							"FUPRMK, " +
							"CLAMNUM, " +
							"USER_T, " +
							"TERMID, " +
							"TRDT, " +
							"TRTM, " +
							"ZAUTOIND, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"EFFDATE, " +
							"CRTUSER, " +
							"CRTDATE, " +
							"LSTUPUSER, " +
							"ZLSTUPDT, " +
							"FUPRCVD, " +
							"EXPRDATE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     fupno,
                                     tranno,
                                     fuptype,
                                     life,
                                     jlife,
                                     fupcode,
                                     fupstat,
                                     fupremdt,
                                     fupremk,
                                     clamnum,
                                     user,
                                     termid,
                                     transactionDate,
                                     transactionTime,
                                     zautoind,
                                     userProfile,
                                     jobName,
                                     datime,
                                     effdate,
                                     crtuser,
                                     crtdate,
                                     lstupuser,
                                     zlstupdt,
                                     fuprcvd,
                                     exprdate,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		fupno.clear();
  		tranno.clear();
  		fuptype.clear();
  		life.clear();
  		jlife.clear();
  		fupcode.clear();
  		fupstat.clear();
  		fupremdt.clear();
  		fupremk.clear();
  		clamnum.clear();
  		user.clear();
  		termid.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		zautoind.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		effdate.clear();
  		crtuser.clear();
  		crtdate.clear();
  		lstupuser.clear();
  		zlstupdt.clear();
  		fuprcvd.clear();
  		exprdate.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getFluprec() {
  		return fluprec;
	}

	public FixedLengthStringData getFluppfRecord() {
  		return fluppfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setFluprec(what);
	}

	public void setFluprec(Object what) {
  		this.fluprec.set(what);
	}

	public void setFluppfRecord(Object what) {
  		this.fluppfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(fluprec.getLength());
		result.set(fluprec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}