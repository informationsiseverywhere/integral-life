package com.csc.life.newbusiness.procedures;

import com.csc.life.contractservicing.recordstructures.LincpfHelper;
import com.csc.life.newbusiness.tablestructures.Tjl47rec;

/**
 * Service for LINC processing via New Business Transactions.
 * 
 * @author gsaluja2
 *
 */
public interface LincNBService {
	/**
	 * <pre>
	 * Handles request from New Business Proposal Transaction.
	 * If TJL06 criteria is met then
	 * 		1. If a record exists in LINCPF and SLNCPF then existing
	 * 			record is updated.
	 * 		2. If record exists in LINCPF and does not exist in SLNCPF
	 * 				a new record is inserted in case any modification is done
	 * 				on existing policy
	 * 		3. If a record does not exist in LINCPF and SLNCPF both
	 * 				a new record is inserted in LINCPF and SLNCPF
	 * </pre>
	 * 
	 * @param lincpfHelper
	 * @param tjl47rec
	 */
	public void newBusinessProposal(LincpfHelper lincpfHelper, Tjl47rec tjl47rec);
	/**
	 * <pre>
	 * Handles request from Contract Issue Transaction.
	 * If TJL06 criteria is met and
	 * 		If a record exists in LINCPF then existing record is 
	 * 		updated and a new record is inserted in SLNCPF.
	 * </pre>
	 * 
	 * @param lincpfHelper
	 * @param tjl47rec
	 */
	public void newBusinessIssuance(LincpfHelper lincpfHelper, Tjl47rec tjl47rec);
}
