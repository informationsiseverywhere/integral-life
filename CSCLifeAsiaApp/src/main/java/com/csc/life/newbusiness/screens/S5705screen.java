package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class S5705screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5705ScreenVars sv = (S5705ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S5705screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5705ScreenVars screenVars = (S5705ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.itmfrmDisp.setClassString("");
		screenVars.itmtoDisp.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.billcurr.setClassString("");
		screenVars.register.setClassString("");
		screenVars.cntbranch.setClassString("");
		screenVars.srcebus.setClassString("");
		screenVars.incomeSeqNo.setClassString("");
		screenVars.plansuff.setClassString("");
		screenVars.campaign.setClassString("");
		screenVars.billfreq.setClassString("");
		screenVars.mop.setClassString("");
		screenVars.rcdateDisp.setClassString("");
		screenVars.agntsel.setClassString("");
		screenVars.reptype.setClassString("");
		screenVars.lrepnum.setClassString("");
		screenVars.stcal.setClassString("");
		screenVars.stcbl.setClassString("");
		screenVars.stccl.setClassString("");
		screenVars.stcdl.setClassString("");
		screenVars.stcel.setClassString("");
		screenVars.selection.setClassString("");
		screenVars.smkrqd.setClassString("");
		screenVars.mortality.setClassString("");
		screenVars.pursuit01.setClassString("");
		screenVars.pursuit02.setClassString("");
	}

/**
 * Clear all the variables in S5705screen
 */
	public static void clear(VarModel pv) {
		S5705ScreenVars screenVars = (S5705ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.itmfrmDisp.clear();
		screenVars.itmfrm.clear();
		screenVars.itmtoDisp.clear();
		screenVars.itmto.clear();
		screenVars.cntcurr.clear();
		screenVars.billcurr.clear();
		screenVars.register.clear();
		screenVars.cntbranch.clear();
		screenVars.srcebus.clear();
		screenVars.incomeSeqNo.clear();
		screenVars.plansuff.clear();
		screenVars.campaign.clear();
		screenVars.billfreq.clear();
		screenVars.mop.clear();
		screenVars.rcdateDisp.clear();
		screenVars.rcdate.clear();
		screenVars.agntsel.clear();
		screenVars.reptype.clear();
		screenVars.lrepnum.clear();
		screenVars.stcal.clear();
		screenVars.stcbl.clear();
		screenVars.stccl.clear();
		screenVars.stcdl.clear();
		screenVars.stcel.clear();
		screenVars.selection.clear();
		screenVars.smkrqd.clear();
		screenVars.mortality.clear();
		screenVars.pursuit01.clear();
		screenVars.pursuit02.clear();
	}
}
