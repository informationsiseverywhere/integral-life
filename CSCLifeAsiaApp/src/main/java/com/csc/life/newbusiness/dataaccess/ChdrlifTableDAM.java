package com.csc.life.newbusiness.dataaccess;

import com.csc.fsu.general.dataaccess.ChdrpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ChdrlifTableDAM.java
 * Date: Sun, 30 Aug 2009 03:31:49
 * Class transformed from CHDRLIF.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ChdrlifTableDAM extends ChdrpfTableDAM {

	public ChdrlifTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("CHDRLIF");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRPFX, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "SERVUNIT, " +
		            "CNTTYPE, " +
		            "POLINC, " +
		            "POLSUM, " +
		            "NXTSFX, " +
		            "TRANNO, " +
		            "TRANID, " +
		            "VALIDFLAG, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "AVLISU, " +
		            "STATCODE, " +
		            "STATDATE, " +
		            "STATTRAN, " +
		            "PSTCDE, " +
		            "PSTDAT, " +
		            "PSTTRN, " +
		            "TRANLUSED, " +
		            "OCCDATE, " +
		            "CCDATE, " +
		            "REPTYPE, " +
		            "REPNUM, " +
		            "COWNPFX, " +
		            "COWNCOY, " +
		            "COWNNUM, " +
		            "JOWNNUM, " +
		            "PAYRPFX, " +
		            "PAYRCOY, " +
		            "PAYRNUM, " +
		            "DESPPFX, " +
		            "DESPCOY, " +
		            "DESPNUM, " +
		            "ASGNPFX, " +
		            "ASGNCOY, " +
		            "ASGNNUM, " +
		            "CNTBRANCH, " +
		            "AGNTPFX, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "CNTCURR, " +
		            "BILLCURR, " +
		            "PAYPLAN, " +
		            "ACCTMETH, " +
		            "BILLFREQ, " +
		            "BILLCHNL, " +
		            "COLLCHNL, " +
		            "BILLDAY, " +
		            "BILLMONTH, " +
		            "BILLCD, " +
		            "BTDATE, " +
		            "PTDATE, " +
		            "SINSTFROM, " +
		            "SINSTTO, " +
		            "SINSTAMT01, " +
		            "SINSTAMT02, " +
		            "SINSTAMT03, " +
		            "SINSTAMT04, " +
		            "SINSTAMT05, " +
		            "SINSTAMT06, " +
		            "INSTFROM, " +
		            "INSTTO, " +
		            "INSTJCTL, " +
		            "INSTTOT01, " +
		            "INSTTOT02, " +
		            "INSTTOT03, " +
		            "INSTTOT04, " +
		            "INSTTOT05, " +
		            "INSTTOT06, " +
		            "OUTSTAMT, " +
		            "FACTHOUS, " +
		            "BANKKEY, " +
		            "BANKACCKEY, " +
		            "MANDREF, " +
		            "GRUPKEY, " +
		            "MEMBSEL, " +
		            "CAMPAIGN, " +
		            "SRCEBUS, " +
		            "STCA, " +
		            "STCB, " +
		            "STCC, " +
		            "STCD, " +
		            "STCE, " +
		            "APLSUPR, " +
		            "BILLSUPR, " +
		            "COMMSUPR, " +
		            "LAPSSUPR, " +
		            "MAILSUPR, " +
		            "NOTSSUPR, " +
		            "RNWLSUPR, " +
		            "APLSPFROM, " +
		            "BILLSPFROM, " +
		            "COMMSPFROM, " +
		            "LAPSSPFROM, " +
		            "MAILSPFROM, " +
		            "NOTSSPFROM, " +
		            "RNWLSPFROM, " +
		            "APLSPTO, " +
		            "BILLSPTO, " +
		            "COMMSPTO, " +
		            "LAPSSPTO, " +
		            "MAILSPTO, " +
		            "NOTSSPTO, " +
		            "RNWLSPTO, " +
		            "REG, " +
		            "STMDTE, " +
		            "BILLDATE01, " +
		            "BILLDATE02, " +
		            "BILLDATE03, " +
		            "BILLDATE04, " +
		            "STMPDTYDTE, " +
		            "TFRSWUSED, " +
		            "TFRSWLEFT, " +
		            "LASTSWDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
		            "ZMANDREF," +//ILIFE-2472
		            "REQNTYPE," +//ILIFE-2472
		            "PAYCLT," + //ILIFE-2472 PH2
		            "NLGFLG, " +	//ILIFE-3997
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrpfx,
                               chdrcoy,
                               chdrnum,
                               servunit,
                               cnttype,
                               polinc,
                               polsum,
                               nxtsfx,
                               tranno,
                               tranid,
                               validflag,
                               currfrom,
                               currto,
                               avlisu,
                               statcode,
                               statdate,
                               stattran,
                               pstatcode,
                               pstatdate,
                               pstattran,
                               tranlused,
                               occdate,
                               ccdate,
                               reptype,
                               repnum,
                               cownpfx,
                               cowncoy,
                               cownnum,
                               jownnum,
                               payrpfx,
                               payrcoy,
                               payrnum,
                               desppfx,
                               despcoy,
                               despnum,
                               asgnpfx,
                               asgncoy,
                               asgnnum,
                               cntbranch,
                               agntpfx,
                               agntcoy,
                               agntnum,
                               cntcurr,
                               billcurr,
                               payplan,
                               acctmeth,
                               billfreq,
                               billchnl,
                               collchnl,
                               billday,
                               billmonth,
                               billcd,
                               btdate,
                               ptdate,
                               sinstfrom,
                               sinstto,
                               sinstamt01,
                               sinstamt02,
                               sinstamt03,
                               sinstamt04,
                               sinstamt05,
                               sinstamt06,
                               instfrom,
                               instto,
                               instjctl,
                               insttot01,
                               insttot02,
                               insttot03,
                               insttot04,
                               insttot05,
                               insttot06,
                               outstamt,
                               facthous,
                               bankkey,
                               bankacckey,
                               mandref,
                               grupkey,
                               membsel,
                               campaign,
                               srcebus,
                               chdrstcda,
                               chdrstcdb,
                               chdrstcdc,
                               chdrstcdd,
                               chdrstcde,
                               aplsupr,
                               billsupr,
                               commsupr,
                               lapssupr,
                               mailsupr,
                               notssupr,
                               rnwlsupr,
                               aplspfrom,
                               billspfrom,
                               commspfrom,
                               lapsspfrom,
                               mailspfrom,
                               notsspfrom,
                               rnwlspfrom,
                               aplspto,
                               billspto,
                               commspto,
                               lapsspto,
                               mailspto,
                               notsspto,
                               rnwlspto,
                               register,
                               statementDate,
                               billdate01,
                               billdate02,
                               billdate03,
                               billdate04,
                               stmpdtydte,
                               freeSwitchesUsed,
                               freeSwitchesLeft,
                               lastSwitchDate,
                               userProfile,
                               jobName,
                               datime,
                               zmandref,//ILIFE-2472
                               reqntype,//ILIFE-2472
                               payclt, //ILIFE-2472 PH2
                               nlgflg,	//ILIFE-3997
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller20.setInternal(chdrcoy.toInternal());
	nonKeyFiller30.setInternal(chdrnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		//FixedLengthStringData nonKeyData = new FixedLengthStringData(627);
		//FixedLengthStringData nonKeyData = new FixedLengthStringData(633);// ILIFE-2472
		FixedLengthStringData nonKeyData = new FixedLengthStringData(641);//ILIFE-2472 PH2
		nonKeyData.set(
					getChdrpfx().toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getServunit().toInternal()
					+ getCnttype().toInternal()
					+ getPolinc().toInternal()
					+ getPolsum().toInternal()
					+ getNxtsfx().toInternal()
					+ getTranno().toInternal()
					+ getTranid().toInternal()
					+ getValidflag().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getAvlisu().toInternal()
					+ getStatcode().toInternal()
					+ getStatdate().toInternal()
					+ getStattran().toInternal()
					+ getPstatcode().toInternal()
					+ getPstatdate().toInternal()
					+ getPstattran().toInternal()
					+ getTranlused().toInternal()
					+ getOccdate().toInternal()
					+ getCcdate().toInternal()
					+ getReptype().toInternal()
					+ getRepnum().toInternal()
					+ getCownpfx().toInternal()
					+ getCowncoy().toInternal()
					+ getCownnum().toInternal()
					+ getJownnum().toInternal()
					+ getPayrpfx().toInternal()
					+ getPayrcoy().toInternal()
					+ getPayrnum().toInternal()
					+ getDesppfx().toInternal()
					+ getDespcoy().toInternal()
					+ getDespnum().toInternal()
					+ getAsgnpfx().toInternal()
					+ getAsgncoy().toInternal()
					+ getAsgnnum().toInternal()
					+ getCntbranch().toInternal()
					+ getAgntpfx().toInternal()
					+ getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getCntcurr().toInternal()
					+ getBillcurr().toInternal()
					+ getPayplan().toInternal()
					+ getAcctmeth().toInternal()
					+ getBillfreq().toInternal()
					+ getBillchnl().toInternal()
					+ getCollchnl().toInternal()
					+ getBillday().toInternal()
					+ getBillmonth().toInternal()
					+ getBillcd().toInternal()
					+ getBtdate().toInternal()
					+ getPtdate().toInternal()
					+ getSinstfrom().toInternal()
					+ getSinstto().toInternal()
					+ getSinstamt01().toInternal()
					+ getSinstamt02().toInternal()
					+ getSinstamt03().toInternal()
					+ getSinstamt04().toInternal()
					+ getSinstamt05().toInternal()
					+ getSinstamt06().toInternal()
					+ getInstfrom().toInternal()
					+ getInstto().toInternal()
					+ getInstjctl().toInternal()
					+ getInsttot01().toInternal()
					+ getInsttot02().toInternal()
					+ getInsttot03().toInternal()
					+ getInsttot04().toInternal()
					+ getInsttot05().toInternal()
					+ getInsttot06().toInternal()
					+ getOutstamt().toInternal()
					+ getFacthous().toInternal()
					+ getBankkey().toInternal()
					+ getBankacckey().toInternal()
					+ getMandref().toInternal()
					+ getGrupkey().toInternal()
					+ getMembsel().toInternal()
					+ getCampaign().toInternal()
					+ getSrcebus().toInternal()
					+ getChdrstcda().toInternal()
					+ getChdrstcdb().toInternal()
					+ getChdrstcdc().toInternal()
					+ getChdrstcdd().toInternal()
					+ getChdrstcde().toInternal()
					+ getAplsupr().toInternal()
					+ getBillsupr().toInternal()
					+ getCommsupr().toInternal()
					+ getLapssupr().toInternal()
					+ getMailsupr().toInternal()
					+ getNotssupr().toInternal()
					+ getRnwlsupr().toInternal()
					+ getAplspfrom().toInternal()
					+ getBillspfrom().toInternal()
					+ getCommspfrom().toInternal()
					+ getLapsspfrom().toInternal()
					+ getMailspfrom().toInternal()
					+ getNotsspfrom().toInternal()
					+ getRnwlspfrom().toInternal()
					+ getAplspto().toInternal()
					+ getBillspto().toInternal()
					+ getCommspto().toInternal()
					+ getLapsspto().toInternal()
					+ getMailspto().toInternal()
					+ getNotsspto().toInternal()
					+ getRnwlspto().toInternal()
					+ getRegister().toInternal()
					+ getStatementDate().toInternal()
					+ getBilldate01().toInternal()
					+ getBilldate02().toInternal()
					+ getBilldate03().toInternal()
					+ getBilldate04().toInternal()
					+ getStmpdtydte().toInternal()
					+ getFreeSwitchesUsed().toInternal()
					+ getFreeSwitchesLeft().toInternal()
					+ getLastSwitchDate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal()
					+ getZmandref().toInternal()//ILIFE-2472
					+ getReqntype().toInternal()//ILIFE-2472
					+ getPayclt().toInternal() //ILIFE-2472 PH2
					+ getNlgflg().toInternal());	//ILIFE-3997
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrpfx);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, servunit);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, polinc);
			what = ExternalData.chop(what, polsum);
			what = ExternalData.chop(what, nxtsfx);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, tranid);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, avlisu);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, statdate);
			what = ExternalData.chop(what, stattran);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, pstatdate);
			what = ExternalData.chop(what, pstattran);
			what = ExternalData.chop(what, tranlused);
			what = ExternalData.chop(what, occdate);
			what = ExternalData.chop(what, ccdate);
			what = ExternalData.chop(what, reptype);
			what = ExternalData.chop(what, repnum);
			what = ExternalData.chop(what, cownpfx);
			what = ExternalData.chop(what, cowncoy);
			what = ExternalData.chop(what, cownnum);
			what = ExternalData.chop(what, jownnum);
			what = ExternalData.chop(what, payrpfx);
			what = ExternalData.chop(what, payrcoy);
			what = ExternalData.chop(what, payrnum);
			what = ExternalData.chop(what, desppfx);
			what = ExternalData.chop(what, despcoy);
			what = ExternalData.chop(what, despnum);
			what = ExternalData.chop(what, asgnpfx);
			what = ExternalData.chop(what, asgncoy);
			what = ExternalData.chop(what, asgnnum);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, agntpfx);
			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, billcurr);
			what = ExternalData.chop(what, payplan);
			what = ExternalData.chop(what, acctmeth);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, billchnl);
			what = ExternalData.chop(what, collchnl);
			what = ExternalData.chop(what, billday);
			what = ExternalData.chop(what, billmonth);
			what = ExternalData.chop(what, billcd);
			what = ExternalData.chop(what, btdate);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, sinstfrom);
			what = ExternalData.chop(what, sinstto);
			what = ExternalData.chop(what, sinstamt01);
			what = ExternalData.chop(what, sinstamt02);
			what = ExternalData.chop(what, sinstamt03);
			what = ExternalData.chop(what, sinstamt04);
			what = ExternalData.chop(what, sinstamt05);
			what = ExternalData.chop(what, sinstamt06);
			what = ExternalData.chop(what, instfrom);
			what = ExternalData.chop(what, instto);
			what = ExternalData.chop(what, instjctl);
			what = ExternalData.chop(what, insttot01);
			what = ExternalData.chop(what, insttot02);
			what = ExternalData.chop(what, insttot03);
			what = ExternalData.chop(what, insttot04);
			what = ExternalData.chop(what, insttot05);
			what = ExternalData.chop(what, insttot06);
			what = ExternalData.chop(what, outstamt);
			what = ExternalData.chop(what, facthous);
			what = ExternalData.chop(what, bankkey);
			what = ExternalData.chop(what, bankacckey);
			what = ExternalData.chop(what, mandref);
			what = ExternalData.chop(what, grupkey);
			what = ExternalData.chop(what, membsel);
			what = ExternalData.chop(what, campaign);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, chdrstcda);
			what = ExternalData.chop(what, chdrstcdb);
			what = ExternalData.chop(what, chdrstcdc);
			what = ExternalData.chop(what, chdrstcdd);
			what = ExternalData.chop(what, chdrstcde);
			what = ExternalData.chop(what, aplsupr);
			what = ExternalData.chop(what, billsupr);
			what = ExternalData.chop(what, commsupr);
			what = ExternalData.chop(what, lapssupr);
			what = ExternalData.chop(what, mailsupr);
			what = ExternalData.chop(what, notssupr);
			what = ExternalData.chop(what, rnwlsupr);
			what = ExternalData.chop(what, aplspfrom);
			what = ExternalData.chop(what, billspfrom);
			what = ExternalData.chop(what, commspfrom);
			what = ExternalData.chop(what, lapsspfrom);
			what = ExternalData.chop(what, mailspfrom);
			what = ExternalData.chop(what, notsspfrom);
			what = ExternalData.chop(what, rnwlspfrom);
			what = ExternalData.chop(what, aplspto);
			what = ExternalData.chop(what, billspto);
			what = ExternalData.chop(what, commspto);
			what = ExternalData.chop(what, lapsspto);
			what = ExternalData.chop(what, mailspto);
			what = ExternalData.chop(what, notsspto);
			what = ExternalData.chop(what, rnwlspto);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, statementDate);
			what = ExternalData.chop(what, billdate01);
			what = ExternalData.chop(what, billdate02);
			what = ExternalData.chop(what, billdate03);
			what = ExternalData.chop(what, billdate04);
			what = ExternalData.chop(what, stmpdtydte);
			what = ExternalData.chop(what, freeSwitchesUsed);
			what = ExternalData.chop(what, freeSwitchesLeft);
			what = ExternalData.chop(what, lastSwitchDate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);	
			what = ExternalData.chop(what, zmandref);//ILIFE-2472
			what = ExternalData.chop(what, reqntype);//ILIFE-2472	
			what = ExternalData.chop(what, payclt); //ILIFE-2472 PH2
			what = ExternalData.chop(what, nlgflg);	//ILIFE-3997
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(Object what) {
		chdrpfx.set(what);
	}	
	public FixedLengthStringData getServunit() {
		return servunit;
	}
	public void setServunit(Object what) {
		servunit.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public PackedDecimalData getPolinc() {
		return polinc;
	}
	public void setPolinc(Object what) {
		setPolinc(what, false);
	}
	public void setPolinc(Object what, boolean rounded) {
		if (rounded)
			polinc.setRounded(what);
		else
			polinc.set(what);
	}	
	public PackedDecimalData getPolsum() {
		return polsum;
	}
	public void setPolsum(Object what) {
		setPolsum(what, false);
	}
	public void setPolsum(Object what, boolean rounded) {
		if (rounded)
			polsum.setRounded(what);
		else
			polsum.set(what);
	}	
	public PackedDecimalData getNxtsfx() {
		return nxtsfx;
	}
	public void setNxtsfx(Object what) {
		setNxtsfx(what, false);
	}
	public void setNxtsfx(Object what, boolean rounded) {
		if (rounded)
			nxtsfx.setRounded(what);
		else
			nxtsfx.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getTranid() {
		return tranid;
	}
	public void setTranid(Object what) {
		tranid.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getAvlisu() {
		return avlisu;
	}
	public void setAvlisu(Object what) {
		avlisu.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public PackedDecimalData getStatdate() {
		return statdate;
	}
	public void setStatdate(Object what) {
		setStatdate(what, false);
	}
	public void setStatdate(Object what, boolean rounded) {
		if (rounded)
			statdate.setRounded(what);
		else
			statdate.set(what);
	}	
	public PackedDecimalData getStattran() {
		return stattran;
	}
	public void setStattran(Object what) {
		setStattran(what, false);
	}
	public void setStattran(Object what, boolean rounded) {
		if (rounded)
			stattran.setRounded(what);
		else
			stattran.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public PackedDecimalData getPstatdate() {
		return pstatdate;
	}
	public void setPstatdate(Object what) {
		setPstatdate(what, false);
	}
	public void setPstatdate(Object what, boolean rounded) {
		if (rounded)
			pstatdate.setRounded(what);
		else
			pstatdate.set(what);
	}	
	public PackedDecimalData getPstattran() {
		return pstattran;
	}
	public void setPstattran(Object what) {
		setPstattran(what, false);
	}
	public void setPstattran(Object what, boolean rounded) {
		if (rounded)
			pstattran.setRounded(what);
		else
			pstattran.set(what);
	}	
	public PackedDecimalData getTranlused() {
		return tranlused;
	}
	public void setTranlused(Object what) {
		setTranlused(what, false);
	}
	public void setTranlused(Object what, boolean rounded) {
		if (rounded)
			tranlused.setRounded(what);
		else
			tranlused.set(what);
	}	
	public PackedDecimalData getOccdate() {
		return occdate;
	}
	public void setOccdate(Object what) {
		setOccdate(what, false);
	}
	public void setOccdate(Object what, boolean rounded) {
		if (rounded)
			occdate.setRounded(what);
		else
			occdate.set(what);
	}	
	public PackedDecimalData getCcdate() {
		return ccdate;
	}
	public void setCcdate(Object what) {
		setCcdate(what, false);
	}
	public void setCcdate(Object what, boolean rounded) {
		if (rounded)
			ccdate.setRounded(what);
		else
			ccdate.set(what);
	}	
	public FixedLengthStringData getReptype() {
		return reptype;
	}
	public void setReptype(Object what) {
		reptype.set(what);
	}	
	public FixedLengthStringData getRepnum() {
		return repnum;
	}
	public void setRepnum(Object what) {
		repnum.set(what);
	}	
	public FixedLengthStringData getCownpfx() {
		return cownpfx;
	}
	public void setCownpfx(Object what) {
		cownpfx.set(what);
	}	
	public FixedLengthStringData getCowncoy() {
		return cowncoy;
	}
	public void setCowncoy(Object what) {
		cowncoy.set(what);
	}	
	public FixedLengthStringData getCownnum() {
		return cownnum;
	}
	public void setCownnum(Object what) {
		cownnum.set(what);
	}	
	public FixedLengthStringData getJownnum() {
		return jownnum;
	}
	public void setJownnum(Object what) {
		jownnum.set(what);
	}	
	public FixedLengthStringData getPayrpfx() {
		return payrpfx;
	}
	public void setPayrpfx(Object what) {
		payrpfx.set(what);
	}	
	public FixedLengthStringData getPayrcoy() {
		return payrcoy;
	}
	public void setPayrcoy(Object what) {
		payrcoy.set(what);
	}	
	public FixedLengthStringData getPayrnum() {
		return payrnum;
	}
	public void setPayrnum(Object what) {
		payrnum.set(what);
	}	
	public FixedLengthStringData getDesppfx() {
		return desppfx;
	}
	public void setDesppfx(Object what) {
		desppfx.set(what);
	}	
	public FixedLengthStringData getDespcoy() {
		return despcoy;
	}
	public void setDespcoy(Object what) {
		despcoy.set(what);
	}	
	public FixedLengthStringData getDespnum() {
		return despnum;
	}
	public void setDespnum(Object what) {
		despnum.set(what);
	}	
	public FixedLengthStringData getAsgnpfx() {
		return asgnpfx;
	}
	public void setAsgnpfx(Object what) {
		asgnpfx.set(what);
	}	
	public FixedLengthStringData getAsgncoy() {
		return asgncoy;
	}
	public void setAsgncoy(Object what) {
		asgncoy.set(what);
	}	
	public FixedLengthStringData getAsgnnum() {
		return asgnnum;
	}
	public void setAsgnnum(Object what) {
		asgnnum.set(what);
	}	
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}	
	public FixedLengthStringData getAgntpfx() {
		return agntpfx;
	}
	public void setAgntpfx(Object what) {
		agntpfx.set(what);
	}	
	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}	
	public FixedLengthStringData getBillcurr() {
		return billcurr;
	}
	public void setBillcurr(Object what) {
		billcurr.set(what);
	}	
	public FixedLengthStringData getPayplan() {
		return payplan;
	}
	public void setPayplan(Object what) {
		payplan.set(what);
	}	
	public FixedLengthStringData getAcctmeth() {
		return acctmeth;
	}
	public void setAcctmeth(Object what) {
		acctmeth.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getBillchnl() {
		return billchnl;
	}
	public void setBillchnl(Object what) {
		billchnl.set(what);
	}	
	public FixedLengthStringData getCollchnl() {
		return collchnl;
	}
	public void setCollchnl(Object what) {
		collchnl.set(what);
	}	
	public FixedLengthStringData getBillday() {
		return billday;
	}
	public void setBillday(Object what) {
		billday.set(what);
	}	
	public FixedLengthStringData getBillmonth() {
		return billmonth;
	}
	public void setBillmonth(Object what) {
		billmonth.set(what);
	}	
	public PackedDecimalData getBillcd() {
		return billcd;
	}
	public void setBillcd(Object what) {
		setBillcd(what, false);
	}
	public void setBillcd(Object what, boolean rounded) {
		if (rounded)
			billcd.setRounded(what);
		else
			billcd.set(what);
	}	
	public PackedDecimalData getBtdate() {
		return btdate;
	}
	public void setBtdate(Object what) {
		setBtdate(what, false);
	}
	public void setBtdate(Object what, boolean rounded) {
		if (rounded)
			btdate.setRounded(what);
		else
			btdate.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public PackedDecimalData getSinstfrom() {
		return sinstfrom;
	}
	public void setSinstfrom(Object what) {
		setSinstfrom(what, false);
	}
	public void setSinstfrom(Object what, boolean rounded) {
		if (rounded)
			sinstfrom.setRounded(what);
		else
			sinstfrom.set(what);
	}	
	public PackedDecimalData getSinstto() {
		return sinstto;
	}
	public void setSinstto(Object what) {
		setSinstto(what, false);
	}
	public void setSinstto(Object what, boolean rounded) {
		if (rounded)
			sinstto.setRounded(what);
		else
			sinstto.set(what);
	}	
	public PackedDecimalData getSinstamt01() {
		return sinstamt01;
	}
	public void setSinstamt01(Object what) {
		setSinstamt01(what, false);
	}
	public void setSinstamt01(Object what, boolean rounded) {
		if (rounded)
			sinstamt01.setRounded(what);
		else
			sinstamt01.set(what);
	}	
	public PackedDecimalData getSinstamt02() {
		return sinstamt02;
	}
	public void setSinstamt02(Object what) {
		setSinstamt02(what, false);
	}
	public void setSinstamt02(Object what, boolean rounded) {
		if (rounded)
			sinstamt02.setRounded(what);
		else
			sinstamt02.set(what);
	}	
	public PackedDecimalData getSinstamt03() {
		return sinstamt03;
	}
	public void setSinstamt03(Object what) {
		setSinstamt03(what, false);
	}
	public void setSinstamt03(Object what, boolean rounded) {
		if (rounded)
			sinstamt03.setRounded(what);
		else
			sinstamt03.set(what);
	}	
	public PackedDecimalData getSinstamt04() {
		return sinstamt04;
	}
	public void setSinstamt04(Object what) {
		setSinstamt04(what, false);
	}
	public void setSinstamt04(Object what, boolean rounded) {
		if (rounded)
			sinstamt04.setRounded(what);
		else
			sinstamt04.set(what);
	}	
	public PackedDecimalData getSinstamt05() {
		return sinstamt05;
	}
	public void setSinstamt05(Object what) {
		setSinstamt05(what, false);
	}
	public void setSinstamt05(Object what, boolean rounded) {
		if (rounded)
			sinstamt05.setRounded(what);
		else
			sinstamt05.set(what);
	}	
	public PackedDecimalData getSinstamt06() {
		return sinstamt06;
	}
	public void setSinstamt06(Object what) {
		setSinstamt06(what, false);
	}
	public void setSinstamt06(Object what, boolean rounded) {
		if (rounded)
			sinstamt06.setRounded(what);
		else
			sinstamt06.set(what);
	}	
	public PackedDecimalData getInstfrom() {
		return instfrom;
	}
	public void setInstfrom(Object what) {
		setInstfrom(what, false);
	}
	public void setInstfrom(Object what, boolean rounded) {
		if (rounded)
			instfrom.setRounded(what);
		else
			instfrom.set(what);
	}	
	public PackedDecimalData getInstto() {
		return instto;
	}
	public void setInstto(Object what) {
		setInstto(what, false);
	}
	public void setInstto(Object what, boolean rounded) {
		if (rounded)
			instto.setRounded(what);
		else
			instto.set(what);
	}	
	public FixedLengthStringData getInstjctl() {
		return instjctl;
	}
	public void setInstjctl(Object what) {
		instjctl.set(what);
	}	
	public PackedDecimalData getInsttot01() {
		return insttot01;
	}
	public void setInsttot01(Object what) {
		setInsttot01(what, false);
	}
	public void setInsttot01(Object what, boolean rounded) {
		if (rounded)
			insttot01.setRounded(what);
		else
			insttot01.set(what);
	}	
	public PackedDecimalData getInsttot02() {
		return insttot02;
	}
	public void setInsttot02(Object what) {
		setInsttot02(what, false);
	}
	public void setInsttot02(Object what, boolean rounded) {
		if (rounded)
			insttot02.setRounded(what);
		else
			insttot02.set(what);
	}	
	public PackedDecimalData getInsttot03() {
		return insttot03;
	}
	public void setInsttot03(Object what) {
		setInsttot03(what, false);
	}
	public void setInsttot03(Object what, boolean rounded) {
		if (rounded)
			insttot03.setRounded(what);
		else
			insttot03.set(what);
	}	
	public PackedDecimalData getInsttot04() {
		return insttot04;
	}
	public void setInsttot04(Object what) {
		setInsttot04(what, false);
	}
	public void setInsttot04(Object what, boolean rounded) {
		if (rounded)
			insttot04.setRounded(what);
		else
			insttot04.set(what);
	}	
	public PackedDecimalData getInsttot05() {
		return insttot05;
	}
	public void setInsttot05(Object what) {
		setInsttot05(what, false);
	}
	public void setInsttot05(Object what, boolean rounded) {
		if (rounded)
			insttot05.setRounded(what);
		else
			insttot05.set(what);
	}	
	public PackedDecimalData getInsttot06() {
		return insttot06;
	}
	public void setInsttot06(Object what) {
		setInsttot06(what, false);
	}
	public void setInsttot06(Object what, boolean rounded) {
		if (rounded)
			insttot06.setRounded(what);
		else
			insttot06.set(what);
	}	
	public PackedDecimalData getOutstamt() {
		return outstamt;
	}
	public void setOutstamt(Object what) {
		setOutstamt(what, false);
	}
	public void setOutstamt(Object what, boolean rounded) {
		if (rounded)
			outstamt.setRounded(what);
		else
			outstamt.set(what);
	}	
	public FixedLengthStringData getFacthous() {
		return facthous;
	}
	public void setFacthous(Object what) {
		facthous.set(what);
	}	
	public FixedLengthStringData getBankkey() {
		return bankkey;
	}
	public void setBankkey(Object what) {
		bankkey.set(what);
	}	
	public FixedLengthStringData getBankacckey() {
		return bankacckey;
	}
	public void setBankacckey(Object what) {
		bankacckey.set(what);
	}	
	public FixedLengthStringData getMandref() {
		return mandref;
	}
	public void setMandref(Object what) {
		mandref.set(what);
	}	
	public FixedLengthStringData getGrupkey() {
		return grupkey;
	}
	public void setGrupkey(Object what) {
		grupkey.set(what);
	}	
	public FixedLengthStringData getMembsel() {
		return membsel;
	}
	public void setMembsel(Object what) {
		membsel.set(what);
	}	
	public FixedLengthStringData getCampaign() {
		return campaign;
	}
	public void setCampaign(Object what) {
		campaign.set(what);
	}	
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}	
	public FixedLengthStringData getChdrstcda() {
		return chdrstcda;
	}
	public void setChdrstcda(Object what) {
		chdrstcda.set(what);
	}	
	public FixedLengthStringData getChdrstcdb() {
		return chdrstcdb;
	}
	public void setChdrstcdb(Object what) {
		chdrstcdb.set(what);
	}	
	public FixedLengthStringData getChdrstcdc() {
		return chdrstcdc;
	}
	public void setChdrstcdc(Object what) {
		chdrstcdc.set(what);
	}	
	public FixedLengthStringData getChdrstcdd() {
		return chdrstcdd;
	}
	public void setChdrstcdd(Object what) {
		chdrstcdd.set(what);
	}	
	public FixedLengthStringData getChdrstcde() {
		return chdrstcde;
	}
	public void setChdrstcde(Object what) {
		chdrstcde.set(what);
	}	
	public FixedLengthStringData getAplsupr() {
		return aplsupr;
	}
	public void setAplsupr(Object what) {
		aplsupr.set(what);
	}	
	public FixedLengthStringData getBillsupr() {
		return billsupr;
	}
	public void setBillsupr(Object what) {
		billsupr.set(what);
	}	
	public FixedLengthStringData getCommsupr() {
		return commsupr;
	}
	public void setCommsupr(Object what) {
		commsupr.set(what);
	}	
	public FixedLengthStringData getLapssupr() {
		return lapssupr;
	}
	public void setLapssupr(Object what) {
		lapssupr.set(what);
	}	
	public FixedLengthStringData getMailsupr() {
		return mailsupr;
	}
	public void setMailsupr(Object what) {
		mailsupr.set(what);
	}	
	public FixedLengthStringData getNotssupr() {
		return notssupr;
	}
	public void setNotssupr(Object what) {
		notssupr.set(what);
	}	
	public FixedLengthStringData getRnwlsupr() {
		return rnwlsupr;
	}
	public void setRnwlsupr(Object what) {
		rnwlsupr.set(what);
	}	
	public PackedDecimalData getAplspfrom() {
		return aplspfrom;
	}
	public void setAplspfrom(Object what) {
		setAplspfrom(what, false);
	}
	public void setAplspfrom(Object what, boolean rounded) {
		if (rounded)
			aplspfrom.setRounded(what);
		else
			aplspfrom.set(what);
	}	
	public PackedDecimalData getBillspfrom() {
		return billspfrom;
	}
	public void setBillspfrom(Object what) {
		setBillspfrom(what, false);
	}
	public void setBillspfrom(Object what, boolean rounded) {
		if (rounded)
			billspfrom.setRounded(what);
		else
			billspfrom.set(what);
	}	
	public PackedDecimalData getCommspfrom() {
		return commspfrom;
	}
	public void setCommspfrom(Object what) {
		setCommspfrom(what, false);
	}
	public void setCommspfrom(Object what, boolean rounded) {
		if (rounded)
			commspfrom.setRounded(what);
		else
			commspfrom.set(what);
	}	
	public PackedDecimalData getLapsspfrom() {
		return lapsspfrom;
	}
	public void setLapsspfrom(Object what) {
		setLapsspfrom(what, false);
	}
	public void setLapsspfrom(Object what, boolean rounded) {
		if (rounded)
			lapsspfrom.setRounded(what);
		else
			lapsspfrom.set(what);
	}	
	public PackedDecimalData getMailspfrom() {
		return mailspfrom;
	}
	public void setMailspfrom(Object what) {
		setMailspfrom(what, false);
	}
	public void setMailspfrom(Object what, boolean rounded) {
		if (rounded)
			mailspfrom.setRounded(what);
		else
			mailspfrom.set(what);
	}	
	public PackedDecimalData getNotsspfrom() {
		return notsspfrom;
	}
	public void setNotsspfrom(Object what) {
		setNotsspfrom(what, false);
	}
	public void setNotsspfrom(Object what, boolean rounded) {
		if (rounded)
			notsspfrom.setRounded(what);
		else
			notsspfrom.set(what);
	}	
	public PackedDecimalData getRnwlspfrom() {
		return rnwlspfrom;
	}
	public void setRnwlspfrom(Object what) {
		setRnwlspfrom(what, false);
	}
	public void setRnwlspfrom(Object what, boolean rounded) {
		if (rounded)
			rnwlspfrom.setRounded(what);
		else
			rnwlspfrom.set(what);
	}	
	public PackedDecimalData getAplspto() {
		return aplspto;
	}
	public void setAplspto(Object what) {
		setAplspto(what, false);
	}
	public void setAplspto(Object what, boolean rounded) {
		if (rounded)
			aplspto.setRounded(what);
		else
			aplspto.set(what);
	}	
	public PackedDecimalData getBillspto() {
		return billspto;
	}
	public void setBillspto(Object what) {
		setBillspto(what, false);
	}
	public void setBillspto(Object what, boolean rounded) {
		if (rounded)
			billspto.setRounded(what);
		else
			billspto.set(what);
	}	
	public PackedDecimalData getCommspto() {
		return commspto;
	}
	public void setCommspto(Object what) {
		setCommspto(what, false);
	}
	public void setCommspto(Object what, boolean rounded) {
		if (rounded)
			commspto.setRounded(what);
		else
			commspto.set(what);
	}	
	public PackedDecimalData getLapsspto() {
		return lapsspto;
	}
	public void setLapsspto(Object what) {
		setLapsspto(what, false);
	}
	public void setLapsspto(Object what, boolean rounded) {
		if (rounded)
			lapsspto.setRounded(what);
		else
			lapsspto.set(what);
	}	
	public PackedDecimalData getMailspto() {
		return mailspto;
	}
	public void setMailspto(Object what) {
		setMailspto(what, false);
	}
	public void setMailspto(Object what, boolean rounded) {
		if (rounded)
			mailspto.setRounded(what);
		else
			mailspto.set(what);
	}	
	public PackedDecimalData getNotsspto() {
		return notsspto;
	}
	public void setNotsspto(Object what) {
		setNotsspto(what, false);
	}
	public void setNotsspto(Object what, boolean rounded) {
		if (rounded)
			notsspto.setRounded(what);
		else
			notsspto.set(what);
	}	
	public PackedDecimalData getRnwlspto() {
		return rnwlspto;
	}
	public void setRnwlspto(Object what) {
		setRnwlspto(what, false);
	}
	public void setRnwlspto(Object what, boolean rounded) {
		if (rounded)
			rnwlspto.setRounded(what);
		else
			rnwlspto.set(what);
	}	
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}	
	public PackedDecimalData getStatementDate() {
		return statementDate;
	}
	public void setStatementDate(Object what) {
		setStatementDate(what, false);
	}
	public void setStatementDate(Object what, boolean rounded) {
		if (rounded)
			statementDate.setRounded(what);
		else
			statementDate.set(what);
	}	
	public PackedDecimalData getBilldate01() {
		return billdate01;
	}
	public void setBilldate01(Object what) {
		setBilldate01(what, false);
	}
	public void setBilldate01(Object what, boolean rounded) {
		if (rounded)
			billdate01.setRounded(what);
		else
			billdate01.set(what);
	}	
	public PackedDecimalData getBilldate02() {
		return billdate02;
	}
	public void setBilldate02(Object what) {
		setBilldate02(what, false);
	}
	public void setBilldate02(Object what, boolean rounded) {
		if (rounded)
			billdate02.setRounded(what);
		else
			billdate02.set(what);
	}	
	public PackedDecimalData getBilldate03() {
		return billdate03;
	}
	public void setBilldate03(Object what) {
		setBilldate03(what, false);
	}
	public void setBilldate03(Object what, boolean rounded) {
		if (rounded)
			billdate03.setRounded(what);
		else
			billdate03.set(what);
	}	
	public PackedDecimalData getBilldate04() {
		return billdate04;
	}
	public void setBilldate04(Object what) {
		setBilldate04(what, false);
	}
	public void setBilldate04(Object what, boolean rounded) {
		if (rounded)
			billdate04.setRounded(what);
		else
			billdate04.set(what);
	}	
	public PackedDecimalData getStmpdtydte() {
		return stmpdtydte;
	}
	public void setStmpdtydte(Object what) {
		setStmpdtydte(what, false);
	}
	public void setStmpdtydte(Object what, boolean rounded) {
		if (rounded)
			stmpdtydte.setRounded(what);
		else
			stmpdtydte.set(what);
	}	
	public PackedDecimalData getFreeSwitchesUsed() {
		return freeSwitchesUsed;
	}
	public void setFreeSwitchesUsed(Object what) {
		setFreeSwitchesUsed(what, false);
	}
	public void setFreeSwitchesUsed(Object what, boolean rounded) {
		if (rounded)
			freeSwitchesUsed.setRounded(what);
		else
			freeSwitchesUsed.set(what);
	}	
	public PackedDecimalData getFreeSwitchesLeft() {
		return freeSwitchesLeft;
	}
	public void setFreeSwitchesLeft(Object what) {
		setFreeSwitchesLeft(what, false);
	}
	public void setFreeSwitchesLeft(Object what, boolean rounded) {
		if (rounded)
			freeSwitchesLeft.setRounded(what);
		else
			freeSwitchesLeft.set(what);
	}	
	public PackedDecimalData getLastSwitchDate() {
		return lastSwitchDate;
	}
	public void setLastSwitchDate(Object what) {
		setLastSwitchDate(what, false);
	}
	public void setLastSwitchDate(Object what, boolean rounded) {
		if (rounded)
			lastSwitchDate.setRounded(what);
		else
			lastSwitchDate.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	//start ILIFE-2472
	public void setZmandref(Object what){
	  zmandref.set(what);
	}
	
	public FixedLengthStringData getZmandref(){
		return zmandref;
	}
	
	public FixedLengthStringData getReqntype() {
		return reqntype;
	}
	
	public void setReqntype(Object what) {
		reqntype.set(what);
	}
	//END ILIFE-2472
	
	//ILIFE-2472 PH2
	public FixedLengthStringData getPayclt() {
		return payclt;
	}
	//ILIFE-2472 PH2	
	public void setPayclt(Object what) {
		payclt.set(what);
	}
	//ILIFE-3997 Starts
	public FixedLengthStringData getNlgflg() {
		return nlgflg;
	}
	public void setNlgflg(Object what) {
		nlgflg.set(what);
	}
	//ILIFE-3997 End
	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getSuprtos() {
		return new FixedLengthStringData(aplspto.toInternal()
										+ billspto.toInternal()
										+ commspto.toInternal()
										+ lapsspto.toInternal()
										+ mailspto.toInternal()
										+ notsspto.toInternal()
										+ rnwlspto.toInternal());
	}
	public void setSuprtos(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuprtos().getLength()).init(obj);
	
		what = ExternalData.chop(what, aplspto);
		what = ExternalData.chop(what, billspto);
		what = ExternalData.chop(what, commspto);
		what = ExternalData.chop(what, lapsspto);
		what = ExternalData.chop(what, mailspto);
		what = ExternalData.chop(what, notsspto);
		what = ExternalData.chop(what, rnwlspto);
	}
	public PackedDecimalData getSuprto(BaseData indx) {
		return getSuprto(indx.toInt());
	}
	public PackedDecimalData getSuprto(int indx) {

		switch (indx) {
			case 1 : return aplspto;
			case 2 : return billspto;
			case 3 : return commspto;
			case 4 : return lapsspto;
			case 5 : return mailspto;
			case 6 : return notsspto;
			case 7 : return rnwlspto;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSuprto(BaseData indx, Object what) {
		setSuprto(indx, what, false);
	}
	public void setSuprto(BaseData indx, Object what, boolean rounded) {
		setSuprto(indx.toInt(), what, rounded);
	}
	public void setSuprto(int indx, Object what) {
		setSuprto(indx, what, false);
	}
	public void setSuprto(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAplspto(what, rounded);
					 break;
			case 2 : setBillspto(what, rounded);
					 break;
			case 3 : setCommspto(what, rounded);
					 break;
			case 4 : setLapsspto(what, rounded);
					 break;
			case 5 : setMailspto(what, rounded);
					 break;
			case 6 : setNotsspto(what, rounded);
					 break;
			case 7 : setRnwlspto(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSuprfroms() {
		return new FixedLengthStringData(aplspfrom.toInternal()
										+ billspfrom.toInternal()
										+ commspfrom.toInternal()
										+ lapsspfrom.toInternal()
										+ mailspfrom.toInternal()
										+ notsspfrom.toInternal()
										+ rnwlspfrom.toInternal());
	}
	public void setSuprfroms(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuprfroms().getLength()).init(obj);
	
		what = ExternalData.chop(what, aplspfrom);
		what = ExternalData.chop(what, billspfrom);
		what = ExternalData.chop(what, commspfrom);
		what = ExternalData.chop(what, lapsspfrom);
		what = ExternalData.chop(what, mailspfrom);
		what = ExternalData.chop(what, notsspfrom);
		what = ExternalData.chop(what, rnwlspfrom);
	}
	public PackedDecimalData getSuprfrom(BaseData indx) {
		return getSuprfrom(indx.toInt());
	}
	public PackedDecimalData getSuprfrom(int indx) {

		switch (indx) {
			case 1 : return aplspfrom;
			case 2 : return billspfrom;
			case 3 : return commspfrom;
			case 4 : return lapsspfrom;
			case 5 : return mailspfrom;
			case 6 : return notsspfrom;
			case 7 : return rnwlspfrom;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSuprfrom(BaseData indx, Object what) {
		setSuprfrom(indx, what, false);
	}
	public void setSuprfrom(BaseData indx, Object what, boolean rounded) {
		setSuprfrom(indx.toInt(), what, rounded);
	}
	public void setSuprfrom(int indx, Object what) {
		setSuprfrom(indx, what, false);
	}
	public void setSuprfrom(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setAplspfrom(what, rounded);
					 break;
			case 2 : setBillspfrom(what, rounded);
					 break;
			case 3 : setCommspfrom(what, rounded);
					 break;
			case 4 : setLapsspfrom(what, rounded);
					 break;
			case 5 : setMailspfrom(what, rounded);
					 break;
			case 6 : setNotsspfrom(what, rounded);
					 break;
			case 7 : setRnwlspfrom(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSuprflags() {
		return new FixedLengthStringData(aplsupr.toInternal()
										+ billsupr.toInternal()
										+ commsupr.toInternal()
										+ lapssupr.toInternal()
										+ mailsupr.toInternal()
										+ notssupr.toInternal()
										+ rnwlsupr.toInternal());
	}
	public void setSuprflags(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSuprflags().getLength()).init(obj);
	
		what = ExternalData.chop(what, aplsupr);
		what = ExternalData.chop(what, billsupr);
		what = ExternalData.chop(what, commsupr);
		what = ExternalData.chop(what, lapssupr);
		what = ExternalData.chop(what, mailsupr);
		what = ExternalData.chop(what, notssupr);
		what = ExternalData.chop(what, rnwlsupr);
	}
	public FixedLengthStringData getSuprflag(BaseData indx) {
		return getSuprflag(indx.toInt());
	}
	public FixedLengthStringData getSuprflag(int indx) {

		switch (indx) {
			case 1 : return aplsupr;
			case 2 : return billsupr;
			case 3 : return commsupr;
			case 4 : return lapssupr;
			case 5 : return mailsupr;
			case 6 : return notssupr;
			case 7 : return rnwlsupr;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSuprflag(BaseData indx, Object what) {
		setSuprflag(indx.toInt(), what);
	}
	public void setSuprflag(int indx, Object what) {

		switch (indx) {
			case 1 : setAplsupr(what);
					 break;
			case 2 : setBillsupr(what);
					 break;
			case 3 : setCommsupr(what);
					 break;
			case 4 : setLapssupr(what);
					 break;
			case 5 : setMailsupr(what);
					 break;
			case 6 : setNotssupr(what);
					 break;
			case 7 : setRnwlsupr(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getSinstamts() {
		return new FixedLengthStringData(sinstamt01.toInternal()
										+ sinstamt02.toInternal()
										+ sinstamt03.toInternal()
										+ sinstamt04.toInternal()
										+ sinstamt05.toInternal()
										+ sinstamt06.toInternal());
	}
	public void setSinstamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getSinstamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, sinstamt01);
		what = ExternalData.chop(what, sinstamt02);
		what = ExternalData.chop(what, sinstamt03);
		what = ExternalData.chop(what, sinstamt04);
		what = ExternalData.chop(what, sinstamt05);
		what = ExternalData.chop(what, sinstamt06);
	}
	public PackedDecimalData getSinstamt(BaseData indx) {
		return getSinstamt(indx.toInt());
	}
	public PackedDecimalData getSinstamt(int indx) {

		switch (indx) {
			case 1 : return sinstamt01;
			case 2 : return sinstamt02;
			case 3 : return sinstamt03;
			case 4 : return sinstamt04;
			case 5 : return sinstamt05;
			case 6 : return sinstamt06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setSinstamt(BaseData indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(BaseData indx, Object what, boolean rounded) {
		setSinstamt(indx.toInt(), what, rounded);
	}
	public void setSinstamt(int indx, Object what) {
		setSinstamt(indx, what, false);
	}
	public void setSinstamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setSinstamt01(what, rounded);
					 break;
			case 2 : setSinstamt02(what, rounded);
					 break;
			case 3 : setSinstamt03(what, rounded);
					 break;
			case 4 : setSinstamt04(what, rounded);
					 break;
			case 5 : setSinstamt05(what, rounded);
					 break;
			case 6 : setSinstamt06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getInsttots() {
		return new FixedLengthStringData(insttot01.toInternal()
										+ insttot02.toInternal()
										+ insttot03.toInternal()
										+ insttot04.toInternal()
										+ insttot05.toInternal()
										+ insttot06.toInternal());
	}
	public void setInsttots(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getInsttots().getLength()).init(obj);
	
		what = ExternalData.chop(what, insttot01);
		what = ExternalData.chop(what, insttot02);
		what = ExternalData.chop(what, insttot03);
		what = ExternalData.chop(what, insttot04);
		what = ExternalData.chop(what, insttot05);
		what = ExternalData.chop(what, insttot06);
	}
	public PackedDecimalData getInsttot(BaseData indx) {
		return getInsttot(indx.toInt());
	}
	public PackedDecimalData getInsttot(int indx) {

		switch (indx) {
			case 1 : return insttot01;
			case 2 : return insttot02;
			case 3 : return insttot03;
			case 4 : return insttot04;
			case 5 : return insttot05;
			case 6 : return insttot06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setInsttot(BaseData indx, Object what) {
		setInsttot(indx, what, false);
	}
	public void setInsttot(BaseData indx, Object what, boolean rounded) {
		setInsttot(indx.toInt(), what, rounded);
	}
	public void setInsttot(int indx, Object what) {
		setInsttot(indx, what, false);
	}
	public void setInsttot(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setInsttot01(what, rounded);
					 break;
			case 2 : setInsttot02(what, rounded);
					 break;
			case 3 : setInsttot03(what, rounded);
					 break;
			case 4 : setInsttot04(what, rounded);
					 break;
			case 5 : setInsttot05(what, rounded);
					 break;
			case 6 : setInsttot06(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getBilldates() {
		return new FixedLengthStringData(billdate01.toInternal()
										+ billdate02.toInternal()
										+ billdate03.toInternal()
										+ billdate04.toInternal());
	}
	public void setBilldates(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getBilldates().getLength()).init(obj);
	
		what = ExternalData.chop(what, billdate01);
		what = ExternalData.chop(what, billdate02);
		what = ExternalData.chop(what, billdate03);
		what = ExternalData.chop(what, billdate04);
	}
	public PackedDecimalData getBilldate(BaseData indx) {
		return getBilldate(indx.toInt());
	}
	public PackedDecimalData getBilldate(int indx) {

		switch (indx) {
			case 1 : return billdate01;
			case 2 : return billdate02;
			case 3 : return billdate03;
			case 4 : return billdate04;
			default: return null; // Throw error instead?
		}
	
	}
	public void setBilldate(BaseData indx, Object what) {
		setBilldate(indx, what, false);
	}
	public void setBilldate(BaseData indx, Object what, boolean rounded) {
		setBilldate(indx.toInt(), what, rounded);
	}
	public void setBilldate(int indx, Object what) {
		setBilldate(indx, what, false);
	}
	public void setBilldate(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setBilldate01(what, rounded);
					 break;
			case 2 : setBilldate02(what, rounded);
					 break;
			case 3 : setBilldate03(what, rounded);
					 break;
			case 4 : setBilldate04(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		chdrpfx.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		servunit.clear();
		cnttype.clear();
		polinc.clear();
		polsum.clear();
		nxtsfx.clear();
		tranno.clear();
		tranid.clear();
		validflag.clear();
		currfrom.clear();
		currto.clear();
		avlisu.clear();
		statcode.clear();
		statdate.clear();
		stattran.clear();
		pstatcode.clear();
		pstatdate.clear();
		pstattran.clear();
		tranlused.clear();
		occdate.clear();
		ccdate.clear();
		reptype.clear();
		repnum.clear();
		cownpfx.clear();
		cowncoy.clear();
		cownnum.clear();
		jownnum.clear();
		payrpfx.clear();
		payrcoy.clear();
		payrnum.clear();
		desppfx.clear();
		despcoy.clear();
		despnum.clear();
		asgnpfx.clear();
		asgncoy.clear();
		asgnnum.clear();
		cntbranch.clear();
		agntpfx.clear();
		agntcoy.clear();
		agntnum.clear();
		cntcurr.clear();
		billcurr.clear();
		payplan.clear();
		acctmeth.clear();
		billfreq.clear();
		billchnl.clear();
		collchnl.clear();
		billday.clear();
		billmonth.clear();
		billcd.clear();
		btdate.clear();
		ptdate.clear();
		sinstfrom.clear();
		sinstto.clear();
		sinstamt01.clear();
		sinstamt02.clear();
		sinstamt03.clear();
		sinstamt04.clear();
		sinstamt05.clear();
		sinstamt06.clear();
		instfrom.clear();
		instto.clear();
		instjctl.clear();
		insttot01.clear();
		insttot02.clear();
		insttot03.clear();
		insttot04.clear();
		insttot05.clear();
		insttot06.clear();
		outstamt.clear();
		facthous.clear();
		bankkey.clear();
		bankacckey.clear();
		mandref.clear();
		grupkey.clear();
		membsel.clear();
		campaign.clear();
		srcebus.clear();
		chdrstcda.clear();
		chdrstcdb.clear();
		chdrstcdc.clear();
		chdrstcdd.clear();
		chdrstcde.clear();
		aplsupr.clear();
		billsupr.clear();
		commsupr.clear();
		lapssupr.clear();
		mailsupr.clear();
		notssupr.clear();
		rnwlsupr.clear();
		aplspfrom.clear();
		billspfrom.clear();
		commspfrom.clear();
		lapsspfrom.clear();
		mailspfrom.clear();
		notsspfrom.clear();
		rnwlspfrom.clear();
		aplspto.clear();
		billspto.clear();
		commspto.clear();
		lapsspto.clear();
		mailspto.clear();
		notsspto.clear();
		rnwlspto.clear();
		register.clear();
		statementDate.clear();
		billdate01.clear();
		billdate02.clear();
		billdate03.clear();
		billdate04.clear();
		stmpdtydte.clear();
		freeSwitchesUsed.clear();
		freeSwitchesLeft.clear();
		lastSwitchDate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
		zmandref.clear(); //ILIFE-2472
  		reqntype.clear(); //ILIFE-2472	
  		payclt.clear();//ILIFE-2472 PH2
  		nlgflg.clear();//ILIFE-3997
	}


}