package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:11
 * Description:
 * Copybook name: LINSCFIKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Linscfikey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData linscfiFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData linscfiKey = new FixedLengthStringData(256).isAPartOf(linscfiFileKey, 0, REDEFINE);
  	public FixedLengthStringData linscfiChdrcoy = new FixedLengthStringData(1).isAPartOf(linscfiKey, 0);
  	public FixedLengthStringData linscfiChdrnum = new FixedLengthStringData(8).isAPartOf(linscfiKey, 1);
  	public FixedLengthStringData filler = new FixedLengthStringData(247).isAPartOf(linscfiKey, 9, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(linscfiFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linscfiFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}