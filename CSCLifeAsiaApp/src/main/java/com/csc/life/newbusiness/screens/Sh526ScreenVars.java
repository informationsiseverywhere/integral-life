package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SH526
 * @version 1.0 generated on 30/08/09 07:01
 * @author Quipoz
 */
public class Sh526ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(351);
	public FixedLengthStringData dataFields = new FixedLengthStringData(207).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData zfooter = DD.zfooter.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData ztelno = DD.ztelno.copy().isAPartOf(dataFields,122);
	public FixedLengthStringData zunders = new FixedLengthStringData(75).isAPartOf(dataFields, 132);
	public FixedLengthStringData[] zunder = FLSArrayPartOfStructure(3, 25, zunders, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(75).isAPartOf(zunders, 0, FILLER_REDEFINE);
	public FixedLengthStringData zunder01 = DD.zunder.copy().isAPartOf(filler,0);
	public FixedLengthStringData zunder02 = DD.zunder.copy().isAPartOf(filler,25);
	public FixedLengthStringData zunder03 = DD.zunder.copy().isAPartOf(filler,50);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(36).isAPartOf(dataArea, 207);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData zfooterErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData ztelnoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData zundersErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData[] zunderErr = FLSArrayPartOfStructure(3, 4, zundersErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(12).isAPartOf(zundersErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData zunder01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData zunder02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData zunder03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 243);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] zfooterOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] ztelnoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData zundersOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 72);
	public FixedLengthStringData[] zunderOut = FLSArrayPartOfStructure(3, 12, zundersOut, 0);
	public FixedLengthStringData[][] zunderO = FLSDArrayPartOfArrayStructure(12, 1, zunderOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(36).isAPartOf(zundersOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] zunder01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] zunder02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] zunder03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sh526screenWritten = new LongData(0);
	public LongData Sh526protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sh526ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(ztelnoOut,new String[] {"02","01","-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zunder01Out,new String[] {"03","01","-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zunder02Out,new String[] {"04","01","-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zunder03Out,new String[] {"05","01","-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(zfooterOut,new String[] {"08","01","-08",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, ztelno, zunder01, zunder02, zunder03, zfooter};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, ztelnoOut, zunder01Out, zunder02Out, zunder03Out, zfooterOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, ztelnoErr, zunder01Err, zunder02Err, zunder03Err, zfooterErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sh526screen.class;
		protectRecord = Sh526protect.class;
	}

}
