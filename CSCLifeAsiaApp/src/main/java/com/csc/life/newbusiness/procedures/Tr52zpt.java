/*
 * File: Tr52zpt.java
 * Date: December 3, 2013 3:59:41 AM ICT
 * Author: CSC
 * 
 * Class transformed from TR52ZPT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.newbusiness.tablestructures.Tr52zrec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        PRINT SUBROUTINE FOR TR52Z.
*
*
*
****************************************************************** ****
* </pre>
*/
public class Tr52zpt extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String oK = "****";
	private Tabuffrec tabuffrec = new Tabuffrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Tr52zrec tr52zrec = new Tr52zrec();
	private Tablistrec tablistrec = new Tablistrec();
	private GeneralCopyLinesInner generalCopyLinesInner = new GeneralCopyLinesInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public Tr52zpt() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		tr52zrec.tr52zRec.set(tablistrec.generalArea);
		generalCopyLinesInner.fieldNo001.set(tablistrec.company);
		generalCopyLinesInner.fieldNo002.set(tablistrec.tabl);
		generalCopyLinesInner.fieldNo003.set(tablistrec.item);
		generalCopyLinesInner.fieldNo004.set(tablistrec.longdesc);
		generalCopyLinesInner.fieldNo005.set(tr52zrec.bnytype01);
		generalCopyLinesInner.fieldNo007.set(tr52zrec.selfind01);
		generalCopyLinesInner.fieldNo008.set(tr52zrec.cltreln01);
		generalCopyLinesInner.fieldNo009.set(tr52zrec.revcflg01);
		generalCopyLinesInner.fieldNo010.set(tr52zrec.cltreln02);
		generalCopyLinesInner.fieldNo011.set(tr52zrec.revcflg02);
		generalCopyLinesInner.fieldNo012.set(tr52zrec.cltreln03);
		generalCopyLinesInner.fieldNo013.set(tr52zrec.revcflg03);
		generalCopyLinesInner.fieldNo014.set(tr52zrec.cltreln04);
		generalCopyLinesInner.fieldNo015.set(tr52zrec.revcflg04);
		generalCopyLinesInner.fieldNo016.set(tr52zrec.cltreln05);
		generalCopyLinesInner.fieldNo017.set(tr52zrec.revcflg05);
		generalCopyLinesInner.fieldNo021.set(tr52zrec.cltreln06);
		generalCopyLinesInner.fieldNo022.set(tr52zrec.revcflg06);
		generalCopyLinesInner.fieldNo023.set(tr52zrec.cltreln07);
		generalCopyLinesInner.fieldNo024.set(tr52zrec.revcflg07);
		generalCopyLinesInner.fieldNo025.set(tr52zrec.cltreln08);
		generalCopyLinesInner.fieldNo026.set(tr52zrec.revcflg08);
		generalCopyLinesInner.fieldNo027.set(tr52zrec.cltreln09);
		generalCopyLinesInner.fieldNo028.set(tr52zrec.revcflg09);
		generalCopyLinesInner.fieldNo029.set(tr52zrec.cltreln10);
		generalCopyLinesInner.fieldNo030.set(tr52zrec.revcflg10);
		generalCopyLinesInner.fieldNo018.set(tr52zrec.bnytype02);
		generalCopyLinesInner.fieldNo020.set(tr52zrec.selfind02);
		generalCopyLinesInner.fieldNo034.set(tr52zrec.cltreln11);
		generalCopyLinesInner.fieldNo035.set(tr52zrec.revcflg11);
		generalCopyLinesInner.fieldNo036.set(tr52zrec.cltreln12);
		generalCopyLinesInner.fieldNo037.set(tr52zrec.revcflg12);
		generalCopyLinesInner.fieldNo038.set(tr52zrec.cltreln13);
		generalCopyLinesInner.fieldNo039.set(tr52zrec.revcflg13);
		generalCopyLinesInner.fieldNo040.set(tr52zrec.cltreln14);
		generalCopyLinesInner.fieldNo041.set(tr52zrec.revcflg14);
		generalCopyLinesInner.fieldNo042.set(tr52zrec.cltreln15);
		generalCopyLinesInner.fieldNo043.set(tr52zrec.revcflg15);
		generalCopyLinesInner.fieldNo047.set(tr52zrec.cltreln16);
		generalCopyLinesInner.fieldNo048.set(tr52zrec.revcflg16);
		generalCopyLinesInner.fieldNo049.set(tr52zrec.cltreln17);
		generalCopyLinesInner.fieldNo050.set(tr52zrec.revcflg17);
		generalCopyLinesInner.fieldNo051.set(tr52zrec.cltreln18);
		generalCopyLinesInner.fieldNo052.set(tr52zrec.revcflg18);
		generalCopyLinesInner.fieldNo053.set(tr52zrec.cltreln19);
		generalCopyLinesInner.fieldNo054.set(tr52zrec.revcflg19);
		generalCopyLinesInner.fieldNo055.set(tr52zrec.cltreln20);
		generalCopyLinesInner.fieldNo056.set(tr52zrec.revcflg20);
		generalCopyLinesInner.fieldNo031.set(tr52zrec.bnytype03);
		generalCopyLinesInner.fieldNo033.set(tr52zrec.selfind03);
		generalCopyLinesInner.fieldNo060.set(tr52zrec.cltreln21);
		generalCopyLinesInner.fieldNo061.set(tr52zrec.revcflg21);
		generalCopyLinesInner.fieldNo062.set(tr52zrec.cltreln22);
		generalCopyLinesInner.fieldNo063.set(tr52zrec.revcflg22);
		generalCopyLinesInner.fieldNo064.set(tr52zrec.cltreln23);
		generalCopyLinesInner.fieldNo065.set(tr52zrec.revcflg23);
		generalCopyLinesInner.fieldNo066.set(tr52zrec.cltreln24);
		generalCopyLinesInner.fieldNo067.set(tr52zrec.revcflg24);
		generalCopyLinesInner.fieldNo068.set(tr52zrec.cltreln25);
		generalCopyLinesInner.fieldNo069.set(tr52zrec.revcflg25);
		generalCopyLinesInner.fieldNo073.set(tr52zrec.cltreln26);
		generalCopyLinesInner.fieldNo074.set(tr52zrec.revcflg26);
		generalCopyLinesInner.fieldNo075.set(tr52zrec.cltreln27);
		generalCopyLinesInner.fieldNo076.set(tr52zrec.revcflg27);
		generalCopyLinesInner.fieldNo077.set(tr52zrec.cltreln28);
		generalCopyLinesInner.fieldNo078.set(tr52zrec.revcflg28);
		generalCopyLinesInner.fieldNo079.set(tr52zrec.cltreln29);
		generalCopyLinesInner.fieldNo080.set(tr52zrec.revcflg29);
		generalCopyLinesInner.fieldNo081.set(tr52zrec.cltreln30);
		generalCopyLinesInner.fieldNo082.set(tr52zrec.revcflg30);
		generalCopyLinesInner.fieldNo044.set(tr52zrec.bnytype04);
		generalCopyLinesInner.fieldNo046.set(tr52zrec.selfind04);
		generalCopyLinesInner.fieldNo086.set(tr52zrec.cltreln31);
		generalCopyLinesInner.fieldNo087.set(tr52zrec.revcflg31);
		generalCopyLinesInner.fieldNo088.set(tr52zrec.cltreln32);
		generalCopyLinesInner.fieldNo089.set(tr52zrec.revcflg32);
		generalCopyLinesInner.fieldNo090.set(tr52zrec.cltreln33);
		generalCopyLinesInner.fieldNo091.set(tr52zrec.revcflg33);
		generalCopyLinesInner.fieldNo092.set(tr52zrec.cltreln34);
		generalCopyLinesInner.fieldNo093.set(tr52zrec.revcflg34);
		generalCopyLinesInner.fieldNo094.set(tr52zrec.cltreln35);
		generalCopyLinesInner.fieldNo095.set(tr52zrec.revcflg35);
		generalCopyLinesInner.fieldNo099.set(tr52zrec.cltreln36);
		generalCopyLinesInner.fieldNo100.set(tr52zrec.revcflg36);
		generalCopyLinesInner.fieldNo101.set(tr52zrec.cltreln37);
		generalCopyLinesInner.fieldNo102.set(tr52zrec.revcflg37);
		generalCopyLinesInner.fieldNo103.set(tr52zrec.cltreln38);
		generalCopyLinesInner.fieldNo104.set(tr52zrec.revcflg38);
		generalCopyLinesInner.fieldNo105.set(tr52zrec.cltreln39);
		generalCopyLinesInner.fieldNo106.set(tr52zrec.revcflg39);
		generalCopyLinesInner.fieldNo107.set(tr52zrec.cltreln40);
		generalCopyLinesInner.fieldNo108.set(tr52zrec.revcflg40);
		generalCopyLinesInner.fieldNo057.set(tr52zrec.bnytype05);
		generalCopyLinesInner.fieldNo059.set(tr52zrec.selfind05);
		generalCopyLinesInner.fieldNo112.set(tr52zrec.cltreln41);
		generalCopyLinesInner.fieldNo113.set(tr52zrec.revcflg41);
		generalCopyLinesInner.fieldNo114.set(tr52zrec.cltreln42);
		generalCopyLinesInner.fieldNo115.set(tr52zrec.revcflg42);
		generalCopyLinesInner.fieldNo116.set(tr52zrec.cltreln43);
		generalCopyLinesInner.fieldNo117.set(tr52zrec.revcflg43);
		generalCopyLinesInner.fieldNo118.set(tr52zrec.cltreln44);
		generalCopyLinesInner.fieldNo119.set(tr52zrec.revcflg44);
		generalCopyLinesInner.fieldNo120.set(tr52zrec.cltreln45);
		generalCopyLinesInner.fieldNo121.set(tr52zrec.revcflg45);
		generalCopyLinesInner.fieldNo125.set(tr52zrec.cltreln46);
		generalCopyLinesInner.fieldNo126.set(tr52zrec.revcflg46);
		generalCopyLinesInner.fieldNo127.set(tr52zrec.cltreln47);
		generalCopyLinesInner.fieldNo128.set(tr52zrec.revcflg47);
		generalCopyLinesInner.fieldNo129.set(tr52zrec.cltreln48);
		generalCopyLinesInner.fieldNo130.set(tr52zrec.revcflg48);
		generalCopyLinesInner.fieldNo131.set(tr52zrec.cltreln49);
		generalCopyLinesInner.fieldNo132.set(tr52zrec.revcflg49);
		generalCopyLinesInner.fieldNo133.set(tr52zrec.cltreln50);
		generalCopyLinesInner.fieldNo134.set(tr52zrec.revcflg50);
		generalCopyLinesInner.fieldNo070.set(tr52zrec.bnytype06);
		generalCopyLinesInner.fieldNo072.set(tr52zrec.selfind06);
		generalCopyLinesInner.fieldNo083.set(tr52zrec.bnytype07);
		generalCopyLinesInner.fieldNo085.set(tr52zrec.selfind07);
		generalCopyLinesInner.fieldNo096.set(tr52zrec.bnytype08);
		generalCopyLinesInner.fieldNo098.set(tr52zrec.selfind08);
		generalCopyLinesInner.fieldNo109.set(tr52zrec.bnytype09);
		generalCopyLinesInner.fieldNo111.set(tr52zrec.selfind09);
		generalCopyLinesInner.fieldNo122.set(tr52zrec.bnytype10);
		generalCopyLinesInner.fieldNo124.set(tr52zrec.selfind10);
		generalCopyLinesInner.fieldNo006.set(tr52zrec.manopt01);
		generalCopyLinesInner.fieldNo019.set(tr52zrec.manopt02);
		generalCopyLinesInner.fieldNo032.set(tr52zrec.manopt03);
		generalCopyLinesInner.fieldNo045.set(tr52zrec.manopt04);
		generalCopyLinesInner.fieldNo058.set(tr52zrec.manopt05);
		generalCopyLinesInner.fieldNo071.set(tr52zrec.manopt06);
		generalCopyLinesInner.fieldNo084.set(tr52zrec.manopt07);
		generalCopyLinesInner.fieldNo097.set(tr52zrec.manopt08);
		generalCopyLinesInner.fieldNo110.set(tr52zrec.manopt09);
		generalCopyLinesInner.fieldNo123.set(tr52zrec.manopt10);
		generalCopyLinesInner.fieldNo135.set(tr52zrec.gitem);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(generalCopyLinesInner.wsaaPrtLine017);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure GENERAL-COPY-LINES--INNER
 */
private static final class GeneralCopyLinesInner { 

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(24).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(52).isAPartOf(wsaaPrtLine001, 24, FILLER).init("Contract Mandatory Beneficiary Table           SR52Z");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(79);
	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 0, FILLER).init(" Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 11);
	private FixedLengthStringData filler4 = new FixedLengthStringData(11).isAPartOf(wsaaPrtLine002, 12, FILLER).init("   Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 23);
	private FixedLengthStringData filler5 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 28, FILLER).init("    Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 38);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 49);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(71);
	private FixedLengthStringData filler7 = new FixedLengthStringData(71).isAPartOf(wsaaPrtLine003, 0, FILLER).init(" Benf  Mandatory  Allow Self     Valid Relationships and Revocable Flag");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(26);
	private FixedLengthStringData filler8 = new FixedLengthStringData(26).isAPartOf(wsaaPrtLine004, 0, FILLER).init(" Type     ?          (Y/N)");

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(70);
	private FixedLengthStringData filler9 = new FixedLengthStringData(33).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(37).isAPartOf(wsaaPrtLine005, 33, FILLER).init("1        2        3        4        5");

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(73);
	private FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 2);
	private FixedLengthStringData filler12 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine006, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 10);
	private FixedLengthStringData filler13 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine006, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 23);
	private FixedLengthStringData filler14 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 31);
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 36);
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 40);
	private FixedLengthStringData filler17 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 45);
	private FixedLengthStringData filler18 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 49);
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 54);
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 58);
	private FixedLengthStringData filler21 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 63);
	private FixedLengthStringData filler22 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine006, 67);
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 72);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(73);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 2);
	private FixedLengthStringData filler25 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine007, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 10);
	private FixedLengthStringData filler26 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine007, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 23);
	private FixedLengthStringData filler27 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 31);
	private FixedLengthStringData filler28 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 36);
	private FixedLengthStringData filler29 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 40);
	private FixedLengthStringData filler30 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 45);
	private FixedLengthStringData filler31 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 49);
	private FixedLengthStringData filler32 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 54);
	private FixedLengthStringData filler33 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 58);
	private FixedLengthStringData filler34 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 63);
	private FixedLengthStringData filler35 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine007, 67);
	private FixedLengthStringData filler36 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 72);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(73);
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 2);
	private FixedLengthStringData filler38 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine008, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 10);
	private FixedLengthStringData filler39 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine008, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 23);
	private FixedLengthStringData filler40 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 31);
	private FixedLengthStringData filler41 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 36);
	private FixedLengthStringData filler42 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 40);
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 45);
	private FixedLengthStringData filler44 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 49);
	private FixedLengthStringData filler45 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 54);
	private FixedLengthStringData filler46 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 58);
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 63);
	private FixedLengthStringData filler48 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine008, 67);
	private FixedLengthStringData filler49 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 72);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(73);
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 2);
	private FixedLengthStringData filler51 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine009, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 10);
	private FixedLengthStringData filler52 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine009, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 23);
	private FixedLengthStringData filler53 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 31);
	private FixedLengthStringData filler54 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 36);
	private FixedLengthStringData filler55 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 40);
	private FixedLengthStringData filler56 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 45);
	private FixedLengthStringData filler57 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 49);
	private FixedLengthStringData filler58 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 54);
	private FixedLengthStringData filler59 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 58);
	private FixedLengthStringData filler60 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 63);
	private FixedLengthStringData filler61 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine009, 67);
	private FixedLengthStringData filler62 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 72);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(73);
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 2);
	private FixedLengthStringData filler64 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine010, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 10);
	private FixedLengthStringData filler65 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine010, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 23);
	private FixedLengthStringData filler66 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 31);
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 36);
	private FixedLengthStringData filler68 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 40);
	private FixedLengthStringData filler69 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 45);
	private FixedLengthStringData filler70 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 49);
	private FixedLengthStringData filler71 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 54);
	private FixedLengthStringData filler72 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 58);
	private FixedLengthStringData filler73 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 63);
	private FixedLengthStringData filler74 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine010, 67);
	private FixedLengthStringData filler75 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 72);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(73);
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 2);
	private FixedLengthStringData filler77 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine011, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 10);
	private FixedLengthStringData filler78 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine011, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 23);
	private FixedLengthStringData filler79 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 31);
	private FixedLengthStringData filler80 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 36);
	private FixedLengthStringData filler81 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 40);
	private FixedLengthStringData filler82 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 45);
	private FixedLengthStringData filler83 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 49);
	private FixedLengthStringData filler84 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 54);
	private FixedLengthStringData filler85 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 58);
	private FixedLengthStringData filler86 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 63);
	private FixedLengthStringData filler87 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine011, 67);
	private FixedLengthStringData filler88 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 72);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(73);
	private FixedLengthStringData filler89 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 2);
	private FixedLengthStringData filler90 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine012, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 10);
	private FixedLengthStringData filler91 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine012, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 23);
	private FixedLengthStringData filler92 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 31);
	private FixedLengthStringData filler93 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 36);
	private FixedLengthStringData filler94 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 40);
	private FixedLengthStringData filler95 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 45);
	private FixedLengthStringData filler96 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 49);
	private FixedLengthStringData filler97 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 54);
	private FixedLengthStringData filler98 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 58);
	private FixedLengthStringData filler99 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo093 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 63);
	private FixedLengthStringData filler100 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo094 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine012, 67);
	private FixedLengthStringData filler101 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo095 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 72);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(73);
	private FixedLengthStringData filler102 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo096 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 2);
	private FixedLengthStringData filler103 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine013, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo097 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 10);
	private FixedLengthStringData filler104 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine013, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo098 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 23);
	private FixedLengthStringData filler105 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo099 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 31);
	private FixedLengthStringData filler106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo100 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 36);
	private FixedLengthStringData filler107 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo101 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 40);
	private FixedLengthStringData filler108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo102 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 45);
	private FixedLengthStringData filler109 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo103 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 49);
	private FixedLengthStringData filler110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo104 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 54);
	private FixedLengthStringData filler111 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo105 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 58);
	private FixedLengthStringData filler112 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo106 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 63);
	private FixedLengthStringData filler113 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo107 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine013, 67);
	private FixedLengthStringData filler114 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo108 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 72);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(73);
	private FixedLengthStringData filler115 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo109 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 2);
	private FixedLengthStringData filler116 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine014, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo110 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 10);
	private FixedLengthStringData filler117 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine014, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo111 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 23);
	private FixedLengthStringData filler118 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo112 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 31);
	private FixedLengthStringData filler119 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo113 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 36);
	private FixedLengthStringData filler120 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo114 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 40);
	private FixedLengthStringData filler121 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo115 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 45);
	private FixedLengthStringData filler122 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo116 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 49);
	private FixedLengthStringData filler123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo117 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 54);
	private FixedLengthStringData filler124 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo118 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 58);
	private FixedLengthStringData filler125 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo119 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 63);
	private FixedLengthStringData filler126 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo120 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine014, 67);
	private FixedLengthStringData filler127 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo121 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 72);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(73);
	private FixedLengthStringData filler128 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo122 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 2);
	private FixedLengthStringData filler129 = new FixedLengthStringData(6).isAPartOf(wsaaPrtLine015, 4, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo123 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 10);
	private FixedLengthStringData filler130 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine015, 11, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo124 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 23);
	private FixedLengthStringData filler131 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine015, 24, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo125 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 31);
	private FixedLengthStringData filler132 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 35, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo126 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 36);
	private FixedLengthStringData filler133 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo127 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 40);
	private FixedLengthStringData filler134 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 44, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo128 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 45);
	private FixedLengthStringData filler135 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 46, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo129 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 49);
	private FixedLengthStringData filler136 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo130 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 54);
	private FixedLengthStringData filler137 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 55, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo131 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 58);
	private FixedLengthStringData filler138 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 62, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo132 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 63);
	private FixedLengthStringData filler139 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 64, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo133 = new FixedLengthStringData(4).isAPartOf(wsaaPrtLine015, 67);
	private FixedLengthStringData filler140 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 71, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo134 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 72);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(70);
	private FixedLengthStringData filler141 = new FixedLengthStringData(41).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler142 = new FixedLengthStringData(21).isAPartOf(wsaaPrtLine016, 41, FILLER).init("Continuation item:");
	private FixedLengthStringData fieldNo135 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine016, 62);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(28);
	private FixedLengthStringData filler143 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine017, 0, FILLER).init(" F1=Help  F3=Exit  F4=Prompt");
}
}
