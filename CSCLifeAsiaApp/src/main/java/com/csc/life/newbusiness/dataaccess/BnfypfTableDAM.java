package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: BnfypfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:24:03
 * Class transformed from BNFYPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class BnfypfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 121+2;
	public FixedLengthStringData bnfyrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData bnfypfRecord = bnfyrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData bnyclt = DD.bnyclt.copy().isAPartOf(bnfyrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData validflag = DD.validflag.copy().isAPartOf(bnfyrec);
	public PackedDecimalData currfrom = DD.currfr.copy().isAPartOf(bnfyrec);
	public PackedDecimalData currto = DD.currto.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData bnyrln = DD.bnyrln.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData bnycd = DD.bnycd.copy().isAPartOf(bnfyrec);
	public PackedDecimalData bnypc = DD.bnypc.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData termid = DD.termid.copy().isAPartOf(bnfyrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(bnfyrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(bnfyrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(bnfyrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData bnytype = DD.bnytype.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData cltreln = DD.cltreln.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData relto = DD.relto.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData selfind = DD.selfind.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData revcflg = DD.revcflg.copy().isAPartOf(bnfyrec);
	public PackedDecimalData enddate = DD.effdate.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(bnfyrec);
	public FixedLengthStringData sequence = new FixedLengthStringData(2).isAPartOf(bnfyrec); //fwang3 ICIL-4
	
	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public BnfypfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for BnfypfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public BnfypfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for BnfypfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public BnfypfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for BnfypfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public BnfypfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("BNFYPF");
	}

	@Override
	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"BNYCLT, " +
							"TRANNO, " +
							"VALIDFLAG, " +
							"CURRFR, " +
							"CURRTO, " +
							"BNYRLN, " +
							"BNYCD, " +
							"BNYPC, " +
							"TERMID, " +
							"USER_T, " +
							"TRTM, " +
							"TRDT, " +
							"EFFDATE, " +
							"BNYTYPE, " +
							"CLTRELN, " +
							"RELTO, " +
							"SELFIND, " +
							"REVCFLG, " +
							"ENDDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"SEQUENCE, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     bnyclt,
                                     tranno,
                                     validflag,
                                     currfrom,
                                     currto,
                                     bnyrln,
                                     bnycd,
                                     bnypc,
                                     termid,
                                     user,
                                     transactionTime,
                                     transactionDate,
                                     effdate,
                                     bnytype,
                                     cltreln,
                                     relto,
                                     selfind,
                                     revcflg,
                                     enddate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     sequence,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/
	@Override
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		bnyclt.clear();
  		tranno.clear();
  		validflag.clear();
  		currfrom.clear();
  		currto.clear();
  		bnyrln.clear();
  		bnycd.clear();
  		bnypc.clear();
  		termid.clear();
  		user.clear();
  		transactionTime.clear();
  		transactionDate.clear();
  		effdate.clear();
  		bnytype.clear();
  		cltreln.clear();
  		relto.clear();
  		selfind.clear();
  		revcflg.clear();
  		enddate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
  		sequence.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getBnfyrec() {
  		return bnfyrec;
	}

	public FixedLengthStringData getBnfypfRecord() {
  		return bnfypfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/
	@Override
	public void set(Object what) {
  		setBnfyrec(what);
	}

	public void setBnfyrec(Object what) {
  		this.bnfyrec.set(what);
	}

	public void setBnfypfRecord(Object what) {
  		this.bnfypfRecord.set(what);
	}
	
	@Override
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(bnfyrec.getLength());
		result.set(bnfyrec);
  		return result;
	}
	
	@Override
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}