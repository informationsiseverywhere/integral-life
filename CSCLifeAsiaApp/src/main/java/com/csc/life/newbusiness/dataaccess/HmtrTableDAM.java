package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: HmtrTableDAM.java
 * Date: Sun, 30 Aug 2009 03:41:11
 * Class transformed from HMTR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class HmtrTableDAM extends HmtrpfTableDAM {

	public HmtrTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("HMTR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CNTBRANCH"
		             + ", CNTTYPE"
		             + ", YEAR"
		             + ", MNTH";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CNTBRANCH, " +
		            "CNTTYPE, " +
		            "MNTH, " +
		            "YEAR, " +
		            "CNTCOUNT, " +
		            "UPDDTE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CNTBRANCH ASC, " +
		            "CNTTYPE ASC, " +
		            "YEAR ASC, " +
		            "MNTH ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CNTBRANCH DESC, " +
		            "CNTTYPE DESC, " +
		            "YEAR DESC, " +
		            "MNTH DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               cntbranch,
                               cnttype,
                               mnth,
                               year,
                               cntcount,
                               updateDate,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(53);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getCntbranch().toInternal()
					+ getCnttype().toInternal()
					+ getYear().toInternal()
					+ getMnth().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, year);
			what = ExternalData.chop(what, mnth);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(cntbranch.toInternal());
	nonKeyFiller3.setInternal(cnttype.toInternal());
	nonKeyFiller4.setInternal(mnth.toInternal());
	nonKeyFiller5.setInternal(year.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(66);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ getCntcount().toInternal()
					+ getUpdateDate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, cntcount);
			what = ExternalData.chop(what, updateDate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}
	public PackedDecimalData getYear() {
		return year;
	}
	public void setYear(Object what) {
		setYear(what, false);
	}
	public void setYear(Object what, boolean rounded) {
		if (rounded)
			year.setRounded(what);
		else
			year.set(what);
	}
	public PackedDecimalData getMnth() {
		return mnth;
	}
	public void setMnth(Object what) {
		setMnth(what, false);
	}
	public void setMnth(Object what, boolean rounded) {
		if (rounded)
			mnth.setRounded(what);
		else
			mnth.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getCntcount() {
		return cntcount;
	}
	public void setCntcount(Object what) {
		setCntcount(what, false);
	}
	public void setCntcount(Object what, boolean rounded) {
		if (rounded)
			cntcount.setRounded(what);
		else
			cntcount.set(what);
	}	
	public PackedDecimalData getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Object what) {
		setUpdateDate(what, false);
	}
	public void setUpdateDate(Object what, boolean rounded) {
		if (rounded)
			updateDate.setRounded(what);
		else
			updateDate.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		cntbranch.clear();
		cnttype.clear();
		year.clear();
		mnth.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		cntcount.clear();
		updateDate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}