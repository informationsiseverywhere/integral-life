/*
 * File: Bh600.java
 * Date: 29 August 2009 21:38:23
 * Author: Quipoz Limited
 * 
 * Class transformed from BH600.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.HmpsTableDAM;
import com.csc.life.newbusiness.reports.Rh600Report;
import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This is the monthly report, detailing all New Business tran-
*  sactions for that month by Policy Status. The Outstanding cases
*  from the previous month are brought forward to the current
*  month, to which are added all of the new cases submitted this
*  month. The current month's transactions are split into 3
*  categories - Completed, Cancelled and Outstanding. These are
*  subtracted from the month's brought forward transactions to
*  produce what will be the subsequent month's brought forward
*  transactions.
*
*  In all cases the Basic Sum Assured, the Annualised Premium and
*  the Single Premium values are shown as well as a total of the
*  cases for each category.
*
*  This program reads the extract file HTMPPF produced by BH597
*  and sort the records in the following order:
*       Company
*       Channel
*       Product
*
*****************************************************************
* </pre>
*/
public class Bh600 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlhtmpCursorrs = null;
	private java.sql.PreparedStatement sqlhtmpCursorps = null;
	private java.sql.Connection sqlhtmpCursorconn = null;
	private String sqlhtmpCursor = "";
	private Rh600Report printerFile = new Rh600Report();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH600");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		   be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCounter = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaR = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaS = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaV = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstRecord = new FixedLengthStringData(1);
	private Validator firstRecord = new Validator(wsaaFirstRecord, "Y");
	private ZonedDecimalData wsaaAccumulator = new ZonedDecimalData(2, 0).init(1).setUnsigned();

	private FixedLengthStringData wsaaRowFound = new FixedLengthStringData(1);
	private Validator rowFound = new Validator(wsaaRowFound, "Y");

	private FixedLengthStringData wsaaTtlRowFound = new FixedLengthStringData(1);
	private Validator ttlRowFound = new Validator(wsaaTtlRowFound, "Y");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaInamt = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFinalSingp = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFinalInstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaFinalSumin = new ZonedDecimalData(17, 2);

	private FixedLengthStringData wsaaHmpsArray = new FixedLengthStringData(780);
	private FixedLengthStringData[] wsaaArrayHmps = FLSArrayPartOfStructure(20, 39, wsaaHmpsArray, 0);
	private FixedLengthStringData[] wsaaHmpsCntbranch = FLSDArrayPartOfArrayStructure(2, wsaaArrayHmps, 0);
	private FixedLengthStringData[] wsaaHmpsCnttype = FLSDArrayPartOfArrayStructure(3, wsaaArrayHmps, 2);
	private FixedLengthStringData[] wsaaHmpsCntcurr = FLSDArrayPartOfArrayStructure(3, wsaaArrayHmps, 5);
	private PackedDecimalData[] wsaaHmpsCntcount = PDArrayPartOfArrayStructure(6, 0, wsaaArrayHmps, 8);
	private PackedDecimalData[] wsaaHmpsSingp = PDArrayPartOfArrayStructure(17, 2, wsaaArrayHmps, 12);
	private PackedDecimalData[] wsaaHmpsAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaArrayHmps, 21);
	private PackedDecimalData[] wsaaHmpsSumins = PDArrayPartOfArrayStructure(17, 2, wsaaArrayHmps, 30);
	private ZonedDecimalData wsaaNoOfRows = new ZonedDecimalData(2, 0).init(18).setUnsigned();
	private FixedLengthStringData wsaaPrevCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPrevCntbranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPrevCnttype = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

	private FixedLengthStringData wsaaSqlHtmppf = new FixedLengthStringData(54);
	private FixedLengthStringData wsaaSqlChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlHtmppf, 0);
	private FixedLengthStringData wsaaSqlChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaSqlHtmppf, 1);
	private FixedLengthStringData wsaaSqlCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlHtmppf, 9);
	private FixedLengthStringData wsaaSqlAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaSqlHtmppf, 11);
	private FixedLengthStringData wsaaSqlCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlHtmppf, 19);
	private FixedLengthStringData wsaaSqlCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaSqlHtmppf, 22);
	private FixedLengthStringData wsaaSqlHrow = new FixedLengthStringData(2).isAPartOf(wsaaSqlHtmppf, 25);
	private PackedDecimalData wsaaSqlSumins = new PackedDecimalData(17, 2).isAPartOf(wsaaSqlHtmppf, 27);
	private PackedDecimalData wsaaSqlAnnprem = new PackedDecimalData(17, 2).isAPartOf(wsaaSqlHtmppf, 36);
	private PackedDecimalData wsaaSqlSingp = new PackedDecimalData(17, 2).isAPartOf(wsaaSqlHtmppf, 45);

	private FixedLengthStringData wsaaPrevDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaPrevYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaPrevDate, 0).setUnsigned();
	private ZonedDecimalData wsaaPrevMnth = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrevDate, 4).setUnsigned();
	private ZonedDecimalData wsaaPrevDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaPrevDate, 6).setUnsigned();

	private FixedLengthStringData wsaaEffectiveDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaEffectiveDate, 0).setUnsigned();
	private ZonedDecimalData wsaaMnth = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffectiveDate, 4).setUnsigned();
	private ZonedDecimalData wsaaDay = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffectiveDate, 6).setUnsigned();
	private static final int wsaaNoOfTtlRows = 4;

	private FixedLengthStringData wsaaTtlRows = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTtlComp = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 0).init("D ");
	private FixedLengthStringData wsaaTtlIssue = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 2).init("H ");
	private FixedLengthStringData wsaaTtlCancel = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 4).init("M ");
	private FixedLengthStringData wsaaTtlOutstg = new FixedLengthStringData(2).isAPartOf(wsaaTtlRows, 6).init("W ");

	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaTtlRows, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaTtlRowId = FLSArrayPartOfStructure(4, 2, filler4, 0);
	private FixedLengthStringData wsaaHeaderMonth = new FixedLengthStringData(10);

	private FixedLengthStringData wsaaDetailWrite = new FixedLengthStringData(1);
	private Validator writeDetail = new Validator(wsaaDetailWrite, "Y");

	private FixedLengthStringData wsaaNewBusArray = new FixedLengthStringData(690);
	private FixedLengthStringData[] wsaaArrayLines = FLSArrayPartOfStructure(23, 30, wsaaNewBusArray, 0);
	private PackedDecimalData[] wsaaCount = PDArrayPartOfArrayStructure(4, 0, wsaaArrayLines, 0);
	private PackedDecimalData[] wsaaSumin = PDArrayPartOfArrayStructure(17, 2, wsaaArrayLines, 3);
	private PackedDecimalData[] wsaaInstprem = PDArrayPartOfArrayStructure(17, 2, wsaaArrayLines, 12);
	private PackedDecimalData[] wsaaSingp = PDArrayPartOfArrayStructure(17, 2, wsaaArrayLines, 21);

	private FixedLengthStringData wsaaNewBusSArray = new FixedLengthStringData(690);
	private FixedLengthStringData[] wsaaArrayLinesS = FLSArrayPartOfStructure(23, 30, wsaaNewBusSArray, 0);
	private PackedDecimalData[] wsaaCountS = PDArrayPartOfArrayStructure(4, 0, wsaaArrayLinesS, 0);
	private PackedDecimalData[] wsaaSuminS = PDArrayPartOfArrayStructure(17, 2, wsaaArrayLinesS, 3);
	private PackedDecimalData[] wsaaInstpremS = PDArrayPartOfArrayStructure(17, 2, wsaaArrayLinesS, 12);
	private PackedDecimalData[] wsaaSingpS = PDArrayPartOfArrayStructure(17, 2, wsaaArrayLinesS, 21);

	private FixedLengthStringData wsaaNewBusTArray = new FixedLengthStringData(690);
	private FixedLengthStringData[] wsaaArrayLinesT = FLSArrayPartOfStructure(23, 30, wsaaNewBusTArray, 0);
	private PackedDecimalData[] wsaaCountT = PDArrayPartOfArrayStructure(4, 0, wsaaArrayLinesT, 0);
	private PackedDecimalData[] wsaaSuminT = PDArrayPartOfArrayStructure(17, 2, wsaaArrayLinesT, 3);
	private PackedDecimalData[] wsaaInstpremT = PDArrayPartOfArrayStructure(17, 2, wsaaArrayLinesT, 12);
	private PackedDecimalData[] wsaaSingpT = PDArrayPartOfArrayStructure(17, 2, wsaaArrayLinesT, 21);

	private FixedLengthStringData wsaaTtlForCo = new FixedLengthStringData(1);
	private Validator cttlForCo = new Validator(wsaaTtlForCo, "C");
	private Validator sttlForCo = new Validator(wsaaTtlForCo, "S");
	private Validator ttlForCo = new Validator(wsaaTtlForCo, "T");
		/*  HTMP parameters.                                               */
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(150);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(150);

	private FixedLengthStringData wsaaHtmpFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler5 = new FixedLengthStringData(4).isAPartOf(wsaaHtmpFn, 0, FILLER).init("HTMP");
	private FixedLengthStringData wsaaHtmpRunid = new FixedLengthStringData(2).isAPartOf(wsaaHtmpFn, 4);
	private ZonedDecimalData wsaaHtmpJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaHtmpFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler6 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private static final String wsaaBranchnm = "All Branches";
	private static final String wsaaAllCnttype = "***";
	private static final String wsaaCntdesc = "All Products                  ";
	private static final String wsaaUnknownCntdesc = "????????????                  ";
		/* WSAA-OUTPUT-FIELDS */
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaSdate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaCurrcode = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaCurrencynm = new FixedLengthStringData(30);
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String hmpsrec = "HMPSREC";
		/* ERRORS */
	private static final String esql = "ESQL";
	private static final String ivrm = "IVRM";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String th565 = "TH565";
	private static final String t5688 = "T5688";

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private HmpsTableDAM hmpsIO = new HmpsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Th565rec th565rec = new Th565rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private PrinterRecInner printerRecInner = new PrinterRecInner();
	private WsaaCalendarMonthsInner wsaaCalendarMonthsInner = new WsaaCalendarMonthsInner();
	private WsaaDtlRowsInner wsaaDtlRowsInner = new WsaaDtlRowsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		eof2080, 
		exit2090, 
		z380Next, 
		z390Exit
	}

	public Bh600() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Restarting of this program is handled by MAINB,                 */
		/** using a restart method of '3'.                                  */
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		readTh5651010();
		setUpHeadingDates1040();
		setUpHeadingCurrency1050();
	}

protected void initialise1010()
	{
		/* Open required files.*/
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(),"1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*  Point to correct member of HTMPPF.                             */
		varcom.vrcmDate.set(getCobolDate());
		wsaaHtmpRunid.set(bprdIO.getSystemParam04());
		wsaaHtmpJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(HTMPPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaHtmpFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    OPEN INPUT HTMPPF.                                           */
		initialize(wsaaNewBusArray);
		initialize(wsaaNewBusSArray);
		initialize(wsaaNewBusTArray);
		for (wsaaSub.set(1); !(isGT(wsaaSub,99)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		wsaaEffectiveDate.set(bsscIO.getEffectiveDate());
		wsaaPrevDate.set(bsscIO.getEffectiveDate());
		wsaaHeaderMonth.set(wsaaCalendarMonthsInner.wsaaCalendarMonth[wsaaMnth.toInt()]);
		if (isEQ(wsaaPrevMnth,1)) {
			compute(wsaaPrevYear, 0).set(sub(wsaaPrevYear,1));
			wsaaPrevMnth.set(12);
		}
		else {
			compute(wsaaPrevMnth, 0).set(sub(wsaaPrevMnth,1));
		}
	}

protected void readTh5651010()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th565);
		itemIO.setItemitem(bprdIO.getAuthCode());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th565rec.th565Rec.set(itemIO.getGenarea());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaSdate.set(datcon1rec.extDate);
		wsaaToday.set(datcon1rec.intDate);
	}

protected void setUpHeadingCurrency1050()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(th565rec.currcode);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaCurrcode.set(th565rec.currcode);
		wsaaCurrencynm.set(descIO.getLongdesc());
		firstRecord.setTrue();
		sqlhtmpCursor = " SELECT  CHDRCOY, CHDRNUM, CNTBRANCH, AGNTNUM, CNTTYPE, CNTCURR, HROW, SUMINS, ANNPREM, SINGP" +
" FROM   " + getAppVars().getTableNameOverriden("HTMPPF") + " " +
" ORDER BY CHDRCOY, CNTBRANCH, CNTTYPE, HROW";
		sqlerrorflag = false;
		try {
			sqlhtmpCursorconn = getAppVars().getDBConnectionForTable(new com.csc.life.newbusiness.dataaccess.HtmppfTableDAM());
			sqlhtmpCursorps = getAppVars().prepareStatementEmbeded(sqlhtmpCursorconn, sqlhtmpCursor, "HTMPPF");
			sqlhtmpCursorrs = getAppVars().executeQuery(sqlhtmpCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFile2010();
				case eof2080: 
					eof2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlhtmpCursorrs)) {
				getAppVars().getDBObject(sqlhtmpCursorrs, 1, wsaaSqlChdrcoy);
				getAppVars().getDBObject(sqlhtmpCursorrs, 2, wsaaSqlChdrnum);
				getAppVars().getDBObject(sqlhtmpCursorrs, 3, wsaaSqlCntbranch);
				getAppVars().getDBObject(sqlhtmpCursorrs, 4, wsaaSqlAgntnum);
				getAppVars().getDBObject(sqlhtmpCursorrs, 5, wsaaSqlCnttype);
				getAppVars().getDBObject(sqlhtmpCursorrs, 6, wsaaSqlCntcurr);
				getAppVars().getDBObject(sqlhtmpCursorrs, 7, wsaaSqlHrow);
				getAppVars().getDBObject(sqlhtmpCursorrs, 8, wsaaSqlSumins);
				getAppVars().getDBObject(sqlhtmpCursorrs, 9, wsaaSqlAnnprem);
				getAppVars().getDBObject(sqlhtmpCursorrs, 10, wsaaSqlSingp);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		if (firstRecord.isTrue()) {
			wsaaPrevCompany.set(wsaaSqlChdrcoy);
			wsaaPrevCntbranch.set(wsaaSqlCntbranch);
			wsaaPrevCnttype.set(wsaaSqlCnttype);
			wsaaFirstRecord.set("N");
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		/* Close cursor to read HTMP records.                              */
		getAppVars().freeDBConnectionIgnoreErr(sqlhtmpCursorconn, sqlhtmpCursorps, sqlhtmpCursorrs);
		if (isEQ(wsaaFirstRecord,"N")) {
			z100ObtainOtherRows();
			cttlForCo.setTrue();
			addSubttl5400();
			wsaaCnttype.set(wsaaPrevCnttype);
			header3300();
			printTotals3200();
			sttlForCo.setTrue();
			addTtl5500();
			header3300();
			printTotals3200();
			ttlForCo.setTrue();
			header3300();
			printTotals3200();
			printerRecInner.dummy.set(SPACES);
			printerFile.printRh600e01(printerRecInner.printerRec);
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		/*  Check record is required for processing.*/
		/*  Softlock the record if it is to be updated.*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (isNE(wsaaSqlCnttype,wsaaPrevCnttype)
		|| isNE(wsaaSqlCntbranch,wsaaPrevCntbranch)) {
			z100ObtainOtherRows();
			cttlForCo.setTrue();
			addSubttl5400();
			wsaaCnttype.set(wsaaPrevCnttype);
			header3300();
			printTotals3200();
			wsaaPrevCnttype.set(wsaaSqlCnttype);
		}
		if (isNE(wsaaSqlCntbranch,wsaaPrevCntbranch)) {
			addTtl5500();
			sttlForCo.setTrue();
			header3300();
			printTotals3200();
			wsaaPrevCntbranch.set(wsaaSqlCntbranch);
		}
		if (isNE(wsaaSqlChdrcoy,wsaaPrevCompany)) {
			wsaaPrevCompany.set(wsaaSqlChdrcoy);
		}
		wsaaRowFound.set(SPACES);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfRows)
		|| rowFound.isTrue()); wsaaCnt.add(1)){
			if (isEQ(wsaaSqlHrow, wsaaDtlRowsInner.wsaaRowId[wsaaCnt.toInt()])) {
				rowFound.setTrue();
				wsaaCounter.set(wsaaCnt);
			}
		}
		if (rowFound.isTrue()) {
			wsaaFinalInstprem.set(0);
			wsaaFinalSumin.set(0);
			wsaaFinalSingp.set(0);
			if (isNE(wsaaSqlCntcurr,wsaaCurrcode)) {
				if (isNE(wsaaSqlAnnprem,0)) {
					wsaaInamt.set(wsaaSqlAnnprem);
					currConv6000();
					wsaaFinalInstprem.add(conlinkrec.amountOut);
				}
				if (isNE(wsaaSqlSumins,0)) {
					wsaaInamt.set(wsaaSqlSumins);
					currConv6000();
					wsaaFinalSumin.add(conlinkrec.amountOut);
				}
				if (isNE(wsaaSqlSingp,0)) {
					wsaaInamt.set(wsaaSqlSingp);
					currConv6000();
					wsaaFinalSingp.add(conlinkrec.amountOut);
				}
			}
			else {
				wsaaFinalInstprem.set(wsaaSqlAnnprem);
				wsaaFinalSumin.set(wsaaSqlSumins);
				wsaaFinalSingp.set(wsaaSqlSingp);
			}
			addDetails5300();
			addTtls5000();
		}
	}

protected void printTotals3200()
	{
		writeDetail3280();
	}

protected void writeDetail3280()
	{
		/* If first page/overflow - write standard headings*/
		if (newPageReq.isTrue()) {
			printerFile.printRh600h01(printerRecInner.printerRec);
			wsaaOverflow.set("N");
		}
		printerFile.printRh600u01(printerRecInner.printerRec);
		printerFile.printRh600h02(printerRecInner.printerRec);
		printerFile.printRh600u01(printerRecInner.printerRec);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfRows)); wsaaCnt.add(1)){
			if (isEQ(wsaaTtlForCo,"C")){
				moveDetail5600();
			}
			else if (isEQ(wsaaTtlForCo,"S")){
				moveSubttl5700();
			}
			else if (isEQ(wsaaTtlForCo,"T")){
				moveTtl5800();
			}
		}
		wsaaOverflow.set("Y");
	}

protected void header3300()
	{
		header3310();
		setUpHeadingBranch3330();
	}

protected void header3310()
	{
		wsaaCompany.set(wsaaPrevCompany);
		wsaaBranch.set(wsaaPrevCntbranch);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		printerRecInner.company.set(wsaaCompany);
		printerRecInner.companynm.set(descIO.getLongdesc());
		printerRecInner.forsdesc.set(wsaaHeaderMonth);
		printerRecInner.sdate.set(wsaaSdate);
		printerRecInner.year.set(wsaaYear);
	}

protected void setUpHeadingBranch3330()
	{
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesctabl(t1692);
		descIO.setDesccoy(wsaaCompany);
		descIO.setDescitem(wsaaBranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (ttlForCo.isTrue()) {
			printerRecInner.branch.set(SPACES);
			printerRecInner.branchnm.set(wsaaBranchnm);
		}
		else {
			printerRecInner.branch.set(wsaaBranch);
			printerRecInner.branchnm.set(descIO.getLongdesc());
		}
		/* Get Product*/
		descIO.setParams(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(wsaaCompany);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(wsaaCnttype);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			printerRecInner.cntdesc.set(wsaaUnknownCntdesc);
		}
		if (ttlForCo.isTrue()
		|| sttlForCo.isTrue()) {
			printerRecInner.cnttype.set(wsaaAllCnttype);
			printerRecInner.cntdesc.set(wsaaCntdesc);
		}
		else {
			printerRecInner.cnttype.set(wsaaCnttype);
			printerRecInner.cntdesc.set(descIO.getLongdesc());
		}
		/*  Get Currency from TH565.*/
		printerRecInner.currcode.set(wsaaCurrcode);
		printerRecInner.currencynm.set(wsaaCurrencynm);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*   Close all files, and delete the override function             */
		/*    CLOSE HTMPPF.                                                */
		wsaaQcmdexc.set(SPACES);
		wsaaQcmdexc.set("DLTOVR FILE(HTMPPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void addTtls5000()
	{
		/*ADD*/
		wsaaTtlRowFound.set(SPACES);
		for (wsaaSub.set(1); !(ttlRowFound.isTrue()
		|| isGT(wsaaSub,wsaaNoOfTtlRows)); wsaaSub.add(1)){
			if (isLT(wsaaDtlRowsInner.wsaaRowId[wsaaCounter.toInt()], wsaaTtlRowId[wsaaSub.toInt()])) {
				for (wsaaV.set(wsaaCounter); !(ttlRowFound.isTrue()); wsaaV.add(1)){
					if (isEQ(wsaaDtlRowsInner.wsaaRowId[wsaaV.toInt()], wsaaTtlRowId[wsaaSub.toInt()])) {
						ttlRowFound.setTrue();
						wsaaCounter.set(wsaaV);
					}
				}
			}
		}
		if (ttlRowFound.isTrue()) {
			addDetails5300();
		}
		/*EXIT*/
	}

protected void addDetails5300()
	{
		/*ADD*/
		wsaaCount[wsaaCounter.toInt()].add(wsaaAccumulator);
		wsaaInstprem[wsaaCounter.toInt()].add(wsaaFinalInstprem);
		wsaaSingp[wsaaCounter.toInt()].add(wsaaFinalSingp);
		wsaaSumin[wsaaCounter.toInt()].add(wsaaFinalSumin);
		/*EXIT*/
	}

protected void addSubttl5400()
	{
		/*SUM*/
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfRows)); wsaaCnt.add(1)){
			wsaaCountS[wsaaCnt.toInt()].add(wsaaCount[wsaaCnt.toInt()]);
			wsaaInstpremS[wsaaCnt.toInt()].add(wsaaInstprem[wsaaCnt.toInt()]);
			wsaaSingpS[wsaaCnt.toInt()].add(wsaaSingp[wsaaCnt.toInt()]);
			wsaaSuminS[wsaaCnt.toInt()].add(wsaaSumin[wsaaCnt.toInt()]);
		}
		/*EXIT*/
	}

protected void addTtl5500()
	{
		/*SUM*/
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfRows)); wsaaCnt.add(1)){
			wsaaCountT[wsaaCnt.toInt()].add(wsaaCountS[wsaaCnt.toInt()]);
			wsaaInstpremT[wsaaCnt.toInt()].add(wsaaInstpremS[wsaaCnt.toInt()]);
			wsaaSingpT[wsaaCnt.toInt()].add(wsaaSingpS[wsaaCnt.toInt()]);
			wsaaSuminT[wsaaCnt.toInt()].add(wsaaSuminS[wsaaCnt.toInt()]);
		}
		/*EXIT*/
	}

protected void moveDetail5600()
	{
		/*DTL*/
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfRows)); wsaaCnt.add(1)){
			printerRecInner.hpolcnt.set(wsaaCount[wsaaCnt.toInt()]);
			printerRecInner.totcont.set(wsaaSumin[wsaaCnt.toInt()]);
			printerRecInner.adblims01.set(wsaaInstprem[wsaaCnt.toInt()]);
			printerRecInner.adblims02.set(wsaaSingp[wsaaCnt.toInt()]);
			printRow5900();
		}
		initialize(wsaaNewBusArray);
		/*EXIT*/
	}

protected void moveSubttl5700()
	{
		/*STL*/
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfRows)); wsaaCnt.add(1)){
			printerRecInner.hpolcnt.set(wsaaCountS[wsaaCnt.toInt()]);
			printerRecInner.totcont.set(wsaaSuminS[wsaaCnt.toInt()]);
			printerRecInner.adblims01.set(wsaaInstpremS[wsaaCnt.toInt()]);
			printerRecInner.adblims02.set(wsaaSingpS[wsaaCnt.toInt()]);
			printRow5900();
		}
		initialize(wsaaNewBusSArray);
		/*EXIT*/
	}

protected void moveTtl5800()
	{
		/*TTL*/
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,wsaaNoOfRows)); wsaaCnt.add(1)){
			printerRecInner.hpolcnt.set(wsaaCountT[wsaaCnt.toInt()]);
			printerRecInner.totcont.set(wsaaSuminT[wsaaCnt.toInt()]);
			printerRecInner.adblims01.set(wsaaInstpremT[wsaaCnt.toInt()]);
			printerRecInner.adblims02.set(wsaaSingpT[wsaaCnt.toInt()]);
			printRow5900();
		}
		initialize(wsaaNewBusTArray);
		/*EXIT*/
	}

protected void printRow5900()
	{
		row5910();
	}

protected void row5910()
	{
		wsaaTtlRowFound.set(SPACES);
		for (wsaaSub.set(1); !(ttlRowFound.isTrue()
		|| isGT(wsaaSub,wsaaNoOfTtlRows)); wsaaSub.add(1)){
			if (isEQ(wsaaDtlRowsInner.wsaaRowId[wsaaCnt.toInt()], wsaaTtlRowId[wsaaSub.toInt()])) {
				ttlRowFound.setTrue();
			}
		}
		indOn.setTrue(wsaaCnt);
		if (ttlRowFound.isTrue()) {
			printerFile.printRh600u01(printerRecInner.printerRec);
			printerFile.printRh600d01(printerRecInner.printerRec, indicArea);
			printerFile.printRh600u01(printerRecInner.printerRec);
		}
		else {
			printerFile.printRh600d01(printerRecInner.printerRec, indicArea);
		}
		indOff.setTrue(wsaaCnt);
	}

protected void z100ObtainOtherRows()
	{
			z110OtherRows();
			z150WriteCfwd();
		}

protected void z110OtherRows()
	{
		/* Get Brought Forward Figures (Row A).                            */
		hmpsIO.setParams(SPACES);
		hmpsIO.setChdrcoy(bsprIO.getCompany());
		hmpsIO.setCntbranch(wsaaPrevCntbranch);
		hmpsIO.setCnttype(wsaaPrevCnttype);
		hmpsIO.setCntcurr(th565rec.currcode);
		hmpsIO.setYear(wsaaPrevYear);
		hmpsIO.setMnth(wsaaPrevMnth);
		hmpsIO.setFormat(hmpsrec);
		hmpsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hmpsIO);
		if (isNE(hmpsIO.getStatuz(),varcom.oK)
		&& isNE(hmpsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hmpsIO.getStatuz());
			syserrrec.params.set(hmpsIO.getParams());
			fatalError600();
		}
		wsaaFinalInstprem.set(0);
		wsaaFinalSumin.set(0);
		wsaaFinalSingp.set(0);
		if (isEQ(hmpsIO.getStatuz(),varcom.mrnf)) {
			wsaaAccumulator.set(0);
		}
		else {
			wsaaAccumulator.set(hmpsIO.getCntcount());
			if (isNE(hmpsIO.getCntcurr(),wsaaCurrcode)) {
				if (isNE(hmpsIO.getAnnprem(),ZERO)) {
					wsaaInamt.set(hmpsIO.getAnnprem());
					currConv6000();
					wsaaFinalInstprem.add(conlinkrec.amountOut);
				}
				if (isNE(hmpsIO.getSumins(),ZERO)) {
					wsaaInamt.set(hmpsIO.getSumins());
					currConv6000();
					wsaaFinalSumin.add(conlinkrec.amountOut);
				}
				if (isNE(hmpsIO.getSingp(),ZERO)) {
					wsaaInamt.set(hmpsIO.getSingp());
					currConv6000();
					wsaaFinalSingp.add(conlinkrec.amountOut);
				}
			}
			else {
				wsaaFinalInstprem.set(hmpsIO.getAnnprem());
				wsaaFinalSingp.set(hmpsIO.getSingp());
				wsaaFinalSumin.set(hmpsIO.getSumins());
			}
		}
		wsaaCounter.set(1);
		addDetails5300();
		addTtls5000();
		wsaaAccumulator.set(1);
	}

protected void z150WriteCfwd()
	{
		/* Write Carried Forward Figures for this month.                   */
		hmpsIO.setYear(wsaaYear);
		hmpsIO.setMnth(wsaaMnth);
		hmpsIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, hmpsIO);
		if (isNE(hmpsIO.getStatuz(),varcom.oK)
		&& isNE(hmpsIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(hmpsIO.getStatuz());
			syserrrec.params.set(hmpsIO.getParams());
			fatalError600();
		}
		if (isEQ(hmpsIO.getStatuz(),varcom.mrnf)) {
			if (isEQ(wsaaCount[wsaaNoOfRows.toInt()],0)) {
				return ;
			}
			hmpsIO.setCntcount(wsaaCount[wsaaNoOfRows.toInt()]);
			hmpsIO.setAnnprem(wsaaInstprem[wsaaNoOfRows.toInt()]);
			hmpsIO.setSingp(wsaaSingp[wsaaNoOfRows.toInt()]);
			hmpsIO.setSumins(wsaaSumin[wsaaNoOfRows.toInt()]);
			hmpsIO.setUpdateDate(wsaaToday);
			hmpsIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, hmpsIO);
			if (isNE(hmpsIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(hmpsIO.getStatuz());
				syserrrec.params.set(hmpsIO.getParams());
				fatalError600();
			}
		}
		else {
			hmpsIO.setFunction(varcom.updat);
			if (isLT(hmpsIO.getUpdateDate(),wsaaToday)) {
				if (isEQ(wsaaCount[wsaaNoOfRows.toInt()],0)) {
					hmpsIO.setFunction(varcom.delet);
				}
				hmpsIO.setCntcount(wsaaCount[wsaaNoOfRows.toInt()]);
				hmpsIO.setAnnprem(wsaaInstprem[wsaaNoOfRows.toInt()]);
				hmpsIO.setSingp(wsaaSingp[wsaaNoOfRows.toInt()]);
				hmpsIO.setSumins(wsaaSumin[wsaaNoOfRows.toInt()]);
				hmpsIO.setUpdateDate(wsaaToday);
			}
			SmartFileCode.execute(appVars, hmpsIO);
			if (isNE(hmpsIO.getStatuz(),varcom.oK)) {
				syserrrec.statuz.set(hmpsIO.getStatuz());
				syserrrec.params.set(hmpsIO.getParams());
				fatalError600();
			}
		}
	}

protected void z200ObtainExtraRows()
	{
		z210ExtraRows();
	}

protected void z210ExtraRows()
	{
		/* Get Extra Brought Forward Figures (Row A) if there is no        */
		/* corresponding contract type and contract branch combination     */
		/* for this month.                                                 */
		hmpsIO.setParams(SPACES);
		hmpsIO.setChdrcoy(bsprIO.getCompany());
		hmpsIO.setCntbranch(wsaaPrevCntbranch);
		hmpsIO.setCnttype(wsaaPrevCnttype);
		hmpsIO.setCntcurr(th565rec.currcode);
		hmpsIO.setYear(wsaaPrevYear);
		hmpsIO.setMnth(wsaaPrevMnth);
		hmpsIO.setFormat(hmpsrec);
		hmpsIO.setFunction(varcom.begn);
		wsaaR.set(0);
		initialize(wsaaHmpsArray);
		while ( !(isEQ(hmpsIO.getStatuz(),varcom.endp))) {
			z300ReadHmps();
		}
		
		for (wsaaS.set(1); !(isGT(wsaaS,wsaaR)); wsaaS.add(1)){
			z500ExtraProcess();
		}
	}

protected void z300ReadHmps()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					z310Hmps();
				case z380Next: 
					z380Next();
				case z390Exit: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void z310Hmps()
	{
		SmartFileCode.execute(appVars, hmpsIO);
		if (isNE(hmpsIO.getStatuz(),varcom.oK)
		&& isNE(hmpsIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(hmpsIO.getStatuz());
			syserrrec.params.set(hmpsIO.getParams());
			fatalError600();
		}
		if (isEQ(hmpsIO.getStatuz(),varcom.endp)
		|| (isEQ(hmpsIO.getCnttype(),wsaaSqlCnttype)
		&& isEQ(hmpsIO.getCntbranch(),wsaaSqlCntbranch))) {
			hmpsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.z390Exit);
		}
		if (isLTE(hmpsIO.getCnttype(),wsaaPrevCnttype)
		&& isLTE(hmpsIO.getCntbranch(),wsaaPrevCntbranch)) {
			goTo(GotoLabel.z380Next);
		}
		wsaaR.add(1);
		wsaaHmpsCnttype[wsaaR.toInt()].set(hmpsIO.getCnttype());
		wsaaHmpsCntcount[wsaaR.toInt()].set(hmpsIO.getCntcount());
		wsaaHmpsCntcurr[wsaaR.toInt()].set(hmpsIO.getCntcurr());
		wsaaHmpsAnnprem[wsaaR.toInt()].set(hmpsIO.getAnnprem());
		wsaaHmpsSingp[wsaaR.toInt()].set(hmpsIO.getSingp());
		wsaaHmpsSumins[wsaaR.toInt()].set(hmpsIO.getSumins());
	}

protected void z380Next()
	{
		hmpsIO.setFunction(varcom.nextr);
	}

protected void z500ExtraProcess()
	{
		z510Extra();
	}

protected void z510Extra()
	{
		wsaaFinalInstprem.set(0);
		wsaaFinalSumin.set(0);
		wsaaFinalSingp.set(0);
		wsaaAccumulator.set(wsaaHmpsCntcount[wsaaS.toInt()]);
		if (isNE(wsaaHmpsCntcurr[wsaaS.toInt()],wsaaCurrcode)) {
			if (isNE(wsaaHmpsAnnprem[wsaaS.toInt()],ZERO)) {
				wsaaInamt.set(wsaaHmpsAnnprem[wsaaS.toInt()]);
				currConv6000();
				wsaaFinalInstprem.add(conlinkrec.amountOut);
			}
			if (isNE(wsaaHmpsSumins[wsaaS.toInt()],ZERO)) {
				wsaaInamt.set(wsaaHmpsSumins[wsaaS.toInt()]);
				currConv6000();
				wsaaFinalSumin.add(conlinkrec.amountOut);
			}
			if (isNE(wsaaHmpsSingp[wsaaS.toInt()],ZERO)) {
				wsaaInamt.set(wsaaHmpsSingp[wsaaS.toInt()]);
				currConv6000();
				wsaaFinalSingp.add(conlinkrec.amountOut);
			}
		}
		else {
			wsaaFinalInstprem.set(wsaaHmpsAnnprem[wsaaS.toInt()]);
			wsaaFinalSingp.set(wsaaHmpsSingp[wsaaS.toInt()]);
			wsaaFinalSumin.set(wsaaHmpsSumins[wsaaS.toInt()]);
		}
		wsaaCounter.set(1);
		addDetails5300();
		addTtls5000();
		wsaaAccumulator.set(1);
		addSubttl5400();
		wsaaCnttype.set(wsaaHmpsCnttype[wsaaS.toInt()]);
		header3300();
		printTotals3200();
	}

protected void currConv6000()
	{
		conv6110();
	}

protected void conv6110()
	{
		conlinkrec.clnk002Rec.set(SPACES);
		conlinkrec.company.set(wsaaSqlChdrcoy);
		conlinkrec.cashdate.set(wsaaToday);
		conlinkrec.currIn.set(wsaaSqlCntcurr);
		conlinkrec.currOut.set(wsaaCurrcode);
		conlinkrec.amountIn.set(wsaaInamt);
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.function.set("REAL");
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isEQ(conlinkrec.statuz,"BOMB")) {
			syserrrec.statuz.set(conlinkrec.statuz);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(wsaaCurrcode);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure PRINTER-REC--INNER
 */
private static final class PrinterRecInner { 

		/*FD  HTMPPF                          LABEL RECORDS STANDARD       
		    DATA RECORDS                    ARE HTMPPF-REC.              
		01  HTMPPF-REC.                                                  
		    COPY DDS-ALL-FORMATS            OF HTMPPF.                   */
	private FixedLengthStringData printerRec = new FixedLengthStringData(153);
	private FixedLengthStringData rh600Record = new FixedLengthStringData(153).isAPartOf(printerRec, 0);
	private FixedLengthStringData rh600h01O = new FixedLengthStringData(153).isAPartOf(rh600Record, 0, REDEFINE);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rh600h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rh600h01O, 1);
	private FixedLengthStringData forsdesc = new FixedLengthStringData(10).isAPartOf(rh600h01O, 31);
	private ZonedDecimalData year = new ZonedDecimalData(4, 0).isAPartOf(rh600h01O, 41);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(rh600h01O, 45);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(rh600h01O, 55);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(rh600h01O, 57);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(rh600h01O, 87);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30).isAPartOf(rh600h01O, 90);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rh600h01O, 120);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(rh600h01O, 123);
	private FixedLengthStringData rh600d01O = new FixedLengthStringData(37).isAPartOf(rh600Record, 0, REDEFINE);
	private ZonedDecimalData hpolcnt = new ZonedDecimalData(5, 0).isAPartOf(rh600d01O, 0);
	private ZonedDecimalData totcont = new ZonedDecimalData(10, 0).isAPartOf(rh600d01O, 5);
	private ZonedDecimalData adblims01 = new ZonedDecimalData(11, 2).isAPartOf(rh600d01O, 15);
	private ZonedDecimalData adblims02 = new ZonedDecimalData(11, 2).isAPartOf(rh600d01O, 26);
	private FixedLengthStringData rh600e01O = new FixedLengthStringData(1).isAPartOf(rh600Record, 0, REDEFINE);
	private FixedLengthStringData dummy = new FixedLengthStringData(1).isAPartOf(rh600e01O, 0);
}
/*
 * Class transformed  from Data Structure WSAA-DTL-ROWS--INNER
 */
private static final class WsaaDtlRowsInner { 

	private FixedLengthStringData wsaaDtlRows = new FixedLengthStringData(36);
	private FixedLengthStringData rowA = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 0).init("A ");
	private FixedLengthStringData rowB = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 2).init("B ");
	private FixedLengthStringData rowC = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 4).init("C ");
	private FixedLengthStringData rowD = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 6).init("D ");
	private FixedLengthStringData rowE = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 8).init("E ");
	private FixedLengthStringData rowG = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 10).init("G ");
	private FixedLengthStringData rowH = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 12).init("H ");
	private FixedLengthStringData rowI = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 14).init("I ");
	private FixedLengthStringData rowJ = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 16).init("J ");
	private FixedLengthStringData rowK = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 18).init("K ");
	private FixedLengthStringData rowM = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 20).init("M ");
	private FixedLengthStringData rowQ = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 22).init("Q ");
	private FixedLengthStringData rowR = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 24).init("R ");
	private FixedLengthStringData rowS = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 26).init("S ");
	private FixedLengthStringData rowT = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 28).init("T ");
	private FixedLengthStringData rowU = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 30).init("U ");
	private FixedLengthStringData rowV = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 32).init("V ");
	private FixedLengthStringData rowW = new FixedLengthStringData(2).isAPartOf(wsaaDtlRows, 34).init("W ");

	private FixedLengthStringData filler = new FixedLengthStringData(36).isAPartOf(wsaaDtlRows, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaRowId = FLSArrayPartOfStructure(18, 2, filler, 0);
}
/*
 * Class transformed  from Data Structure WSAA-CALENDAR-MONTHS--INNER
 */
private static final class WsaaCalendarMonthsInner { 

	private FixedLengthStringData wsaaCalendarMonths = new FixedLengthStringData(120);
	private FixedLengthStringData wsaaJanuary = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 0).init("JANUARY   ");
	private FixedLengthStringData wsaaFebruary = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 10).init("FEBRUARY  ");
	private FixedLengthStringData wsaaMarch = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 20).init("MARCH     ");
	private FixedLengthStringData wsaaApril = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 30).init("APRIL     ");
	private FixedLengthStringData wsaaMay = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 40).init("MAY       ");
	private FixedLengthStringData wsaaJune = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 50).init("JUNE      ");
	private FixedLengthStringData wsaaJuly = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 60).init("JULY      ");
	private FixedLengthStringData wsaaAugust = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 70).init("AUGUST    ");
	private FixedLengthStringData wsaaSeptember = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 80).init("SEPTEMBER ");
	private FixedLengthStringData wsaaOctober = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 90).init("OCTOBER   ");
	private FixedLengthStringData wsaaNovember = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 100).init("NOVEMBER  ");
	private FixedLengthStringData wsaaDecember = new FixedLengthStringData(10).isAPartOf(wsaaCalendarMonths, 110).init("DECEMBER  ");

	private FixedLengthStringData filler = new FixedLengthStringData(120).isAPartOf(wsaaCalendarMonths, 0, FILLER_REDEFINE);
	private FixedLengthStringData[] wsaaCalendarMonth = FLSArrayPartOfStructure(12, 10, filler, 0);
}
}
