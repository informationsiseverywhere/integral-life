package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:20:48
 * Description:
 * Copybook name: TT505REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tt505rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tt505Rec = new FixedLengthStringData(343);
  	public FixedLengthStringData lfacts = new FixedLengthStringData(297).isAPartOf(tt505Rec, 0);
  	public PackedDecimalData[] lfact = PDArrayPartOfStructure(99, 5, 4, lfacts, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(297).isAPartOf(lfacts, 0, FILLER_REDEFINE);
  	public PackedDecimalData lfact01 = new PackedDecimalData(5, 4).isAPartOf(filler, 0);
  	public PackedDecimalData lfact02 = new PackedDecimalData(5, 4).isAPartOf(filler, 3);
  	public PackedDecimalData lfact03 = new PackedDecimalData(5, 4).isAPartOf(filler, 6);
  	public PackedDecimalData lfact04 = new PackedDecimalData(5, 4).isAPartOf(filler, 9);
  	public PackedDecimalData lfact05 = new PackedDecimalData(5, 4).isAPartOf(filler, 12);
  	public PackedDecimalData lfact06 = new PackedDecimalData(5, 4).isAPartOf(filler, 15);
  	public PackedDecimalData lfact07 = new PackedDecimalData(5, 4).isAPartOf(filler, 18);
  	public PackedDecimalData lfact08 = new PackedDecimalData(5, 4).isAPartOf(filler, 21);
  	public PackedDecimalData lfact09 = new PackedDecimalData(5, 4).isAPartOf(filler, 24);
  	public PackedDecimalData lfact10 = new PackedDecimalData(5, 4).isAPartOf(filler, 27);
  	public PackedDecimalData lfact11 = new PackedDecimalData(5, 4).isAPartOf(filler, 30);
  	public PackedDecimalData lfact12 = new PackedDecimalData(5, 4).isAPartOf(filler, 33);
  	public PackedDecimalData lfact13 = new PackedDecimalData(5, 4).isAPartOf(filler, 36);
  	public PackedDecimalData lfact14 = new PackedDecimalData(5, 4).isAPartOf(filler, 39);
  	public PackedDecimalData lfact15 = new PackedDecimalData(5, 4).isAPartOf(filler, 42);
  	public PackedDecimalData lfact16 = new PackedDecimalData(5, 4).isAPartOf(filler, 45);
  	public PackedDecimalData lfact17 = new PackedDecimalData(5, 4).isAPartOf(filler, 48);
  	public PackedDecimalData lfact18 = new PackedDecimalData(5, 4).isAPartOf(filler, 51);
  	public PackedDecimalData lfact19 = new PackedDecimalData(5, 4).isAPartOf(filler, 54);
  	public PackedDecimalData lfact20 = new PackedDecimalData(5, 4).isAPartOf(filler, 57);
  	public PackedDecimalData lfact21 = new PackedDecimalData(5, 4).isAPartOf(filler, 60);
  	public PackedDecimalData lfact22 = new PackedDecimalData(5, 4).isAPartOf(filler, 63);
  	public PackedDecimalData lfact23 = new PackedDecimalData(5, 4).isAPartOf(filler, 66);
  	public PackedDecimalData lfact24 = new PackedDecimalData(5, 4).isAPartOf(filler, 69);
  	public PackedDecimalData lfact25 = new PackedDecimalData(5, 4).isAPartOf(filler, 72);
  	public PackedDecimalData lfact26 = new PackedDecimalData(5, 4).isAPartOf(filler, 75);
  	public PackedDecimalData lfact27 = new PackedDecimalData(5, 4).isAPartOf(filler, 78);
  	public PackedDecimalData lfact28 = new PackedDecimalData(5, 4).isAPartOf(filler, 81);
  	public PackedDecimalData lfact29 = new PackedDecimalData(5, 4).isAPartOf(filler, 84);
  	public PackedDecimalData lfact30 = new PackedDecimalData(5, 4).isAPartOf(filler, 87);
  	public PackedDecimalData lfact31 = new PackedDecimalData(5, 4).isAPartOf(filler, 90);
  	public PackedDecimalData lfact32 = new PackedDecimalData(5, 4).isAPartOf(filler, 93);
  	public PackedDecimalData lfact33 = new PackedDecimalData(5, 4).isAPartOf(filler, 96);
  	public PackedDecimalData lfact34 = new PackedDecimalData(5, 4).isAPartOf(filler, 99);
  	public PackedDecimalData lfact35 = new PackedDecimalData(5, 4).isAPartOf(filler, 102);
  	public PackedDecimalData lfact36 = new PackedDecimalData(5, 4).isAPartOf(filler, 105);
  	public PackedDecimalData lfact37 = new PackedDecimalData(5, 4).isAPartOf(filler, 108);
  	public PackedDecimalData lfact38 = new PackedDecimalData(5, 4).isAPartOf(filler, 111);
  	public PackedDecimalData lfact39 = new PackedDecimalData(5, 4).isAPartOf(filler, 114);
  	public PackedDecimalData lfact40 = new PackedDecimalData(5, 4).isAPartOf(filler, 117);
  	public PackedDecimalData lfact41 = new PackedDecimalData(5, 4).isAPartOf(filler, 120);
  	public PackedDecimalData lfact42 = new PackedDecimalData(5, 4).isAPartOf(filler, 123);
  	public PackedDecimalData lfact43 = new PackedDecimalData(5, 4).isAPartOf(filler, 126);
  	public PackedDecimalData lfact44 = new PackedDecimalData(5, 4).isAPartOf(filler, 129);
  	public PackedDecimalData lfact45 = new PackedDecimalData(5, 4).isAPartOf(filler, 132);
  	public PackedDecimalData lfact46 = new PackedDecimalData(5, 4).isAPartOf(filler, 135);
  	public PackedDecimalData lfact47 = new PackedDecimalData(5, 4).isAPartOf(filler, 138);
  	public PackedDecimalData lfact48 = new PackedDecimalData(5, 4).isAPartOf(filler, 141);
  	public PackedDecimalData lfact49 = new PackedDecimalData(5, 4).isAPartOf(filler, 144);
  	public PackedDecimalData lfact50 = new PackedDecimalData(5, 4).isAPartOf(filler, 147);
  	public PackedDecimalData lfact51 = new PackedDecimalData(5, 4).isAPartOf(filler, 150);
  	public PackedDecimalData lfact52 = new PackedDecimalData(5, 4).isAPartOf(filler, 153);
  	public PackedDecimalData lfact53 = new PackedDecimalData(5, 4).isAPartOf(filler, 156);
  	public PackedDecimalData lfact54 = new PackedDecimalData(5, 4).isAPartOf(filler, 159);
  	public PackedDecimalData lfact55 = new PackedDecimalData(5, 4).isAPartOf(filler, 162);
  	public PackedDecimalData lfact56 = new PackedDecimalData(5, 4).isAPartOf(filler, 165);
  	public PackedDecimalData lfact57 = new PackedDecimalData(5, 4).isAPartOf(filler, 168);
  	public PackedDecimalData lfact58 = new PackedDecimalData(5, 4).isAPartOf(filler, 171);
  	public PackedDecimalData lfact59 = new PackedDecimalData(5, 4).isAPartOf(filler, 174);
  	public PackedDecimalData lfact60 = new PackedDecimalData(5, 4).isAPartOf(filler, 177);
  	public PackedDecimalData lfact61 = new PackedDecimalData(5, 4).isAPartOf(filler, 180);
  	public PackedDecimalData lfact62 = new PackedDecimalData(5, 4).isAPartOf(filler, 183);
  	public PackedDecimalData lfact63 = new PackedDecimalData(5, 4).isAPartOf(filler, 186);
  	public PackedDecimalData lfact64 = new PackedDecimalData(5, 4).isAPartOf(filler, 189);
  	public PackedDecimalData lfact65 = new PackedDecimalData(5, 4).isAPartOf(filler, 192);
  	public PackedDecimalData lfact66 = new PackedDecimalData(5, 4).isAPartOf(filler, 195);
  	public PackedDecimalData lfact67 = new PackedDecimalData(5, 4).isAPartOf(filler, 198);
  	public PackedDecimalData lfact68 = new PackedDecimalData(5, 4).isAPartOf(filler, 201);
  	public PackedDecimalData lfact69 = new PackedDecimalData(5, 4).isAPartOf(filler, 204);
  	public PackedDecimalData lfact70 = new PackedDecimalData(5, 4).isAPartOf(filler, 207);
  	public PackedDecimalData lfact71 = new PackedDecimalData(5, 4).isAPartOf(filler, 210);
  	public PackedDecimalData lfact72 = new PackedDecimalData(5, 4).isAPartOf(filler, 213);
  	public PackedDecimalData lfact73 = new PackedDecimalData(5, 4).isAPartOf(filler, 216);
  	public PackedDecimalData lfact74 = new PackedDecimalData(5, 4).isAPartOf(filler, 219);
  	public PackedDecimalData lfact75 = new PackedDecimalData(5, 4).isAPartOf(filler, 222);
  	public PackedDecimalData lfact76 = new PackedDecimalData(5, 4).isAPartOf(filler, 225);
  	public PackedDecimalData lfact77 = new PackedDecimalData(5, 4).isAPartOf(filler, 228);
  	public PackedDecimalData lfact78 = new PackedDecimalData(5, 4).isAPartOf(filler, 231);
  	public PackedDecimalData lfact79 = new PackedDecimalData(5, 4).isAPartOf(filler, 234);
  	public PackedDecimalData lfact80 = new PackedDecimalData(5, 4).isAPartOf(filler, 237);
  	public PackedDecimalData lfact81 = new PackedDecimalData(5, 4).isAPartOf(filler, 240);
  	public PackedDecimalData lfact82 = new PackedDecimalData(5, 4).isAPartOf(filler, 243);
  	public PackedDecimalData lfact83 = new PackedDecimalData(5, 4).isAPartOf(filler, 246);
  	public PackedDecimalData lfact84 = new PackedDecimalData(5, 4).isAPartOf(filler, 249);
  	public PackedDecimalData lfact85 = new PackedDecimalData(5, 4).isAPartOf(filler, 252);
  	public PackedDecimalData lfact86 = new PackedDecimalData(5, 4).isAPartOf(filler, 255);
  	public PackedDecimalData lfact87 = new PackedDecimalData(5, 4).isAPartOf(filler, 258);
  	public PackedDecimalData lfact88 = new PackedDecimalData(5, 4).isAPartOf(filler, 261);
  	public PackedDecimalData lfact89 = new PackedDecimalData(5, 4).isAPartOf(filler, 264);
  	public PackedDecimalData lfact90 = new PackedDecimalData(5, 4).isAPartOf(filler, 267);
  	public PackedDecimalData lfact91 = new PackedDecimalData(5, 4).isAPartOf(filler, 270);
  	public PackedDecimalData lfact92 = new PackedDecimalData(5, 4).isAPartOf(filler, 273);
  	public PackedDecimalData lfact93 = new PackedDecimalData(5, 4).isAPartOf(filler, 276);
  	public PackedDecimalData lfact94 = new PackedDecimalData(5, 4).isAPartOf(filler, 279);
  	public PackedDecimalData lfact95 = new PackedDecimalData(5, 4).isAPartOf(filler, 282);
  	public PackedDecimalData lfact96 = new PackedDecimalData(5, 4).isAPartOf(filler, 285);
  	public PackedDecimalData lfact97 = new PackedDecimalData(5, 4).isAPartOf(filler, 288);
  	public PackedDecimalData lfact98 = new PackedDecimalData(5, 4).isAPartOf(filler, 291);
  	public PackedDecimalData lfact99 = new PackedDecimalData(5, 4).isAPartOf(filler, 294);
  	public FixedLengthStringData modfacs = new FixedLengthStringData(33).isAPartOf(tt505Rec, 297);
  	public PackedDecimalData[] modfac = PDArrayPartOfStructure(11, 5, 4, modfacs, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(33).isAPartOf(modfacs, 0, FILLER_REDEFINE);
  	public PackedDecimalData modfac01 = new PackedDecimalData(5, 4).isAPartOf(filler1, 0);
  	public PackedDecimalData modfac02 = new PackedDecimalData(5, 4).isAPartOf(filler1, 3);
  	public PackedDecimalData modfac03 = new PackedDecimalData(5, 4).isAPartOf(filler1, 6);
  	public PackedDecimalData modfac04 = new PackedDecimalData(5, 4).isAPartOf(filler1, 9);
  	public PackedDecimalData modfac05 = new PackedDecimalData(5, 4).isAPartOf(filler1, 12);
  	public PackedDecimalData modfac06 = new PackedDecimalData(5, 4).isAPartOf(filler1, 15);
  	public PackedDecimalData modfac07 = new PackedDecimalData(5, 4).isAPartOf(filler1, 18);
  	public PackedDecimalData modfac08 = new PackedDecimalData(5, 4).isAPartOf(filler1, 21);
  	public PackedDecimalData modfac09 = new PackedDecimalData(5, 4).isAPartOf(filler1, 24);
  	public PackedDecimalData modfac10 = new PackedDecimalData(5, 4).isAPartOf(filler1, 27);
  	public PackedDecimalData modfac11 = new PackedDecimalData(5, 4).isAPartOf(filler1, 30);
  	public ZonedDecimalData riskunit = new ZonedDecimalData(6, 0).isAPartOf(tt505Rec, 330);
  	public FixedLengthStringData disccntmeth = new FixedLengthStringData(4).isAPartOf(tt505Rec, 336);
  	public PackedDecimalData lfactor = new PackedDecimalData(5, 4).isAPartOf(tt505Rec, 340);


	public void initialize() {
		COBOLFunctions.initialize(tt505Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tt505Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}