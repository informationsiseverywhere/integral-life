package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class S5005screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
//	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 6, 15, 16, 17, 18, 20, 21, 22, 23, 24, 60, 61, 62}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 23, 2, 80}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S5005ScreenVars sv = (S5005ScreenVars) pv;
		clearInds(av, sv.getScreenSflPfInds());
		write(lrec, sv.S5005screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S5005ScreenVars screenVars = (S5005ScreenVars)pv;
		/*screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.life.setClassString("");
		screenVars.jlife.setClassString("");
		screenVars.lifesel.setClassString("");
		screenVars.lifename.setClassString("");
		screenVars.sex.setClassString("");
		screenVars.dobDisp.setClassString("");
		screenVars.dummy.setClassString("");
		screenVars.zagelit.setClassString("");
		screenVars.anbage.setClassString("");
		screenVars.selection.setClassString("");
		screenVars.clrskind.setClassString("");
		screenVars.smoking.setClassString("");
		screenVars.occup.setClassString("");
		screenVars.pursuit01.setClassString("");
		screenVars.pursuit02.setClassString("");
		screenVars.relation.setClassString("");
		screenVars.rollit.setClassString("");
		screenVars.optdsc01.setClassString("");
		screenVars.optind01.setClassString("");
		screenVars.height.setClassString("");
		screenVars.weight.setClassString("");
		screenVars.bmi.setClassString("");
		screenVars.optdsc02.setClassString("");
		screenVars.optind02.setClassString("");
		screenVars.optdsc03.setClassString("");
		screenVars.optind03.setClassString("");
		screenVars.relationwithowner.setClassString("");
		screenVars.optind04.setClassString("");
		screenVars.occupationClass.setClassString("");//ICIL-160
*/	
		ScreenRecord.setClassStringFormatting(pv);	
	}

/**
 * Clear all the variables in S5005screen
 */
	public static void clear(VarModel pv) {
		S5005ScreenVars screenVars = (S5005ScreenVars) pv;
		/*screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.chdrnum.clear();
		screenVars.life.clear();
		screenVars.jlife.clear();
		screenVars.lifesel.clear();
		screenVars.lifename.clear();
		screenVars.sex.clear();
		screenVars.dobDisp.clear();
		screenVars.dob.clear();
		screenVars.dummy.clear();
		screenVars.zagelit.clear();
		screenVars.anbage.clear();
		screenVars.selection.clear();
		screenVars.clrskind.clear();
		screenVars.smoking.clear();
		screenVars.occup.clear();
		screenVars.pursuit01.clear();
		screenVars.pursuit02.clear();
		screenVars.relation.clear();
		screenVars.rollit.clear();
		screenVars.optdsc01.clear();
		screenVars.optind01.clear();
		screenVars.height.clear();
		screenVars.weight.clear();
		screenVars.bmi.clear();
		screenVars.optdsc02.clear();
		screenVars.optind02.clear();
		screenVars.optdsc03.clear();
		screenVars.optind03.clear();
		screenVars.relationwithowner.clear();
		screenVars.optind04.clear();
		screenVars.occupationClass.clear();*/
		
		ScreenRecord.clear(pv);
	}
}
