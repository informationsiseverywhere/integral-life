package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:24
 * Description:
 * Copybook name: TH555REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th555rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th555Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData zundwrt = new FixedLengthStringData(1).isAPartOf(th555Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(499).isAPartOf(th555Rec, 1, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th555Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th555Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}