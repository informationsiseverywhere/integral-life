/*
 * File: P5090.java
 * Date: 30 August 2009 0:06:31
 * Author: Quipoz Limited
 * 
 * Class transformed from P5090.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.dataaccess.GrpsTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.screens.S5090ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*                  Group Membership Details
*                  ========================
*
* Overview
* --------
*
* A pop-up window will capture the group number and member number
* for a contract proposal.  The window will be selected from
* proposal create screen.  Windowing on the group number will lead
* to the Group Maintenance Scroll Selection.
*
* Initialise
* ----------
*
* Retrieves the contract header details from the contract header
* file (CHDRLNB).    Using RETRV.
*
* The group details are retrieved from the Group Master (GRPS)
* file.
*
* Validation
* ----------
*
* If 'KILL' is pressed then all validation is ignored and the
* contract header record is left as it was on entry to this
* program.
*
* Windowing on the group number field allows new details to
* be created (automatically). This is controlled externally to
* this program.
*
* If 'CALC' is pressed then the screen is redisplayed.
*
* Updating
* --------
*
* Skip this section if 'KILL' pressed.
*
* Store membership details on the contract header using the
* KEEPS function.
*
*****************************************************************
* </pre>
*/
public class P5090 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5090");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
	private static final String e186 = "E186";
	private static final String e714 = "E714";
	private static final String e978 = "E978";
	private static final String e979 = "E979";
	private static final String t086 = "T086";
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private GrpsTableDAM grpsIO = new GrpsTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private Wssplife wssplife = new Wssplife();
	private S5090ScreenVars sv = ScreenProgram.getScreenVars( S5090ScreenVars.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		checkForErrors2080, 
		exit2090
	}

	public P5090() {
		super();
		screenVars = sv;
		new ScreenModel("S5090", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		start1000();
	}

protected void start1000()
	{
		sv.dataArea.set(SPACES);
		/*    Dummy field initilisation for prototype version.*/
		/*    Set screen fields*/
		/*    Retrieve contract header information.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			chdrlnbIO.setParams(chdrlnbIO.getParams());
			fatalError600();
		}
		/*    Retrieve the payer details.*/
		payrIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		if (isEQ(wsspcomn.flag, "I")) {
			sv.grupnumOut[varcom.pr.toInt()].set("Y");
			sv.membselOut[varcom.pr.toInt()].set("Y");
			sv.mplnumOut[varcom.pr.toInt()].set("Y");
		}
		if (isEQ(payrIO.getGrupnum(),SPACES)) {
			sv.grupnum.set(SPACES);
			sv.grupname.set(SPACES);
			sv.membsel.set(SPACES);
			sv.mplnum.set(SPACES);
			return ;
		}
		grpsIO.setGrupcoy(payrIO.getGrupcoy());
		grpsIO.setGrupnum(payrIO.getGrupnum());
		grpsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, grpsIO);
		if (isNE(grpsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(grpsIO.getParams());
			fatalError600();
		}
		sv.grupname.set(grpsIO.getGrupname());
		sv.grupnum.set(payrIO.getGrupnum());
		sv.membsel.set(chdrlnbIO.getMembsel());
		sv.mplnum.set(chdrlnbIO.getMplnum());
	}

	/**
	* <pre>
	*    Sections performed from the 1000 section above.
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2000();
				case checkForErrors2080: 
					checkForErrors2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2000()
	{
		/*    CALL 'S5090IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5090-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(wsspcomn.flag, "I")) {
			goTo(GotoLabel.exit2090);
		}
		/*    If F11 pressed bypass validation.*/
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			goTo(GotoLabel.exit2090);
		}
		/*    If F9 pressed then set a srceen error so the screen*/
		/*    is re-displayed.*/
		if (isEQ(scrnparams.statuz, varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
		/*    Validate fields*/
		if (isEQ(sv.grupnum, SPACES)) {
			sv.grupnumErr.set(e186);
			goTo(GotoLabel.checkForErrors2080);
		}
		grpsIO.setGrupcoy(chdrlnbIO.getChdrcoy());
		grpsIO.setGrupnum(sv.grupnum);
		grpsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, grpsIO);
		if (isNE(grpsIO.getStatuz(), varcom.oK)
		&& isNE(grpsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(grpsIO.getParams());
			fatalError600();
		}
		/* Check Group Number entered is not a deleted Group, if*/
		/* so display error message - 'Cannot attach to del Grp'.*/
		if (isEQ(grpsIO.getValidflag(), "2")) {
			sv.grupnumErr.set(t086);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(grpsIO.getStatuz(), varcom.mrnf)) {
			sv.grupnumErr.set(e714);
			goTo(GotoLabel.checkForErrors2080);
		}
		sv.grupname.set(grpsIO.getGrupname());
		if (isNE(grpsIO.getMembreq(), SPACES)
		&& isEQ(sv.membsel, SPACES)) {
			sv.membselErr.set(e978);
			goTo(GotoLabel.checkForErrors2080);
		}
		if (isEQ(grpsIO.getMembreq(), SPACES)
		&& isNE(sv.membsel, SPACES)) {
			sv.membselErr.set(e979);
			goTo(GotoLabel.checkForErrors2080);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

	/**
	* <pre>
	*    Sections performed from the 2000 section above.
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		start3000();
	}

protected void start3000()
	{
		/*  Update database files as required / WSSP*/
		if (isEQ(wsspcomn.flag, "I")) {
			return ;
		}
		if (isEQ(scrnparams.statuz, varcom.kill)) {
			return ;
		}
		/* Update the contract header record using KEEPS.*/
		chdrlnbIO.setGrupkey(sv.grupnum);
		chdrlnbIO.setMembsel(sv.membsel);
		chdrlnbIO.setMplnum(sv.mplnum);
		chdrlnbIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			chdrlnbIO.setParams(chdrlnbIO.getParams());
			fatalError600();
		}
		/* Release the payr I/O module.*/
		payrIO.setFunction(varcom.rlse);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		/* Update the group key on the payer record.*/
		payrIO.setGrupnum(sv.grupnum);
		payrIO.setMembsel(sv.membsel);
		payrIO.setGrupcoy(wsspcomn.company);
		payrIO.setBillnet(grpsIO.getBillnet());
		payrIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

	/**
	* <pre>
	*    Sections performed from the 3000 section above.
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*START*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
