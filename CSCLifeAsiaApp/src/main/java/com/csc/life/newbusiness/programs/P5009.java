/*
 * File: P5009.java
 * Date: 29 August 2009 23:54:24
 * Author: Quipoz Limited
 *
 * Class transformed from P5009.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.ArrayList;
import java.util.List;

import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.financials.dataaccess.AdocsciTableDAM;
import com.csc.fsu.financials.dataaccess.dao.AdocpfDAO;
import com.csc.fsu.financials.dataaccess.dao.model.Adocpf;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.RdocTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.reinsurance.dataaccess.TtrcTableDAM;
import com.csc.life.annuities.recordstructures.Prasrec;
import com.csc.life.annuities.tablestructures.T6687rec;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.CovtlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.newbusiness.screens.S5009ScreenVars;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                         APPLY CASH
*Initialise               ----------
*----------
*
*  On the first  time  into  the  program,  read  the  financial
*  accounting  rules   for   the  transaction  (T5645,  item  is
*  transaction number passed in WSSP). Also read the sub-account
*  type record for the  second posting (posting no. 02 on T5645)
*  to get the sign (T3695). Call DATCON1 to get today's date.
*
*  Retrieve the contract header (CHDRLNB) for the contract being
*  worked on. Using these details, look up:
*
*       - the contract type description from T5688,
*
*       - the contract owner  (CLTS)  and  format  the  name for
*            confirmation.
*
*  Calculate the amounts to be shown on the screen as follows:
*
*       Contract Suspense  -  read  the  sub-account balance for
*            posting 02 (as  defined  on  T5645) for the current
*            contract. Apply  the  sign for the sub-account type
*            (i.e. if the  sign  is -ve, negate the balance read
*            when displaying it on the screen).
*
*       Contract Fee  -  read  the  contract  definition details
*            (T5688) for the  contract type held on the contract
*            header. If the  contract  fee  method is not blank,
*            look up the  subroutine  used  to calculate if from
*            T5674 and call  it  with the required linkage area.
*            This  subroutine   will  return  the  contract  fee
*            amount. If there  is  no  contract fee method, this
*            amount is zero.
*
*       Premium  -  read through the coverage/rider transactions
*            (COVTLNB).  For  each  record  read, accumulate all
*            single  premiums  payable  and all regular premiums
*            payable.  A  premium  is a single premium amount if
*            the  billing  frequency  for  the whole contract is
*            '00',  or the coverage/rider definition (T5687) has
*            a single premium indicator of 'Y'.  Otherwise it is
*            regular  premium.  Once  all the premiums have been
*            accumulated,  adjust  the  regular premiums (if not
*            zero)  by  the number of frequencies required prior
*            to  issue.  To  do  this,  call  DATCON3  with  the
*            contract  commencement  date,  billing commencement
*            date   and  billing  frequency  from  the  contract
*            header.  This  will  return a factor.  Multiply the
*            regular  premium  amount  bY this factor and add it
*            the   the   single   premium  amount  to  give  the
*            "instatement"   premium  to  be  displayed  on  the
*            screen.
*
*Validation
*----------
*
*  If in enquiry  mode  (WSSP-FLAG  =  'I'),  protect the screen
*  prior to calling the screen I/O module.
*
*  If KILL is pressed or in enquiry mode, skip all validation.
*
*  Check the receipt  number entered by attempting to read RDOC.
*  The key is as follows:
*            Prefix of 'CA'
*            Company is sign-on company
*            Receipt number as entered,
*            Sequence number '0001'.
*
*  There are four possible errors:
*
*       1) The receipt is not found (MRNF),
*
*       2) The receipt was  not  posted  to the company suspense
*            (Sub-ledger code and  type  not  equal  to  the  01
*            posting  account   for  this  transaction  -  T5645
*            above),
*
*       3) The receipt currency  is not the same as the contract
*            currency,
*
*       4) The  suspense  receipt  has  already been transferred
*            ("entity" not blank).
*
*  If CALC is pressed, re-display the screen showing the receipt
*  amount.
*
*Updating
*--------
*
*  If KILL is pressed or in enquiry mode, skip all updating.
*
*  Update  the  sub-account number on the receipt (RDOC) for the
*  number  entered with the contract number to which it is being
*  applied. This contract number is moved to the "entity", along
*  with  a  'CH' prefix and the sign-on company to their related
*  fields.
*
*  Call  LIFRTRN  twice.  Once  for  posting  '01'  and once for
*  posting '02'  as  defined  on T5645 read in the 1000 section.
*  Set up an pass the linkage area as follows:
*            Function - 01=NPSTW, 02=PSTW
*            Batch key - from WSSP
*            Document number - receipt number entered
*            Sequence  number  -  add 1 to the previous sequence
*                 number each time (originally read from RDOC as
*                 1, so it will be 2 for the first posting and 3
*                 for the second)
*            Sub-account  code  and  type,  GL  map, GL sign and
*                 control  total  number - from applicable T5645
*                 entry
*            Company codes (sub-ledger and GL) - batch company
*            Subsidiary account - contract number
*            Original and accounting currency codes, amounts and
*                 exchange rate from the RDOC record read
*            Transaction reference - contract number
*            Transaction description - from the RDOC above
*            Posting  month  and  year  -  blank  (they  will be
*                 defaulted)
*            Effective date - contract commencement date
*            Reconciliation amount - original currency amount
*            Reconciled date - today
*            Transaction ID - from WSSP
*            Substitution code 1 - contract type
*
* Next
* ----
*
*  If  enter  was  pressed,  set  WSSP-NEXTPROG  to  the current
*  program (WSAA_PROG), add one to the program pointer and exit.
*
*  If ROLU pressed:
*
*       Re-calculate  contract  suspense  -  apply  the previous
*            receipt  amount (using the sign for accounting rule
*            02)  to  the  contract suspense amount read earlier
*            (see  1000 section). Adjust to the sub-account code
*            sign  (T3695  as in the 1000 section) and move this
*            amount to the screen.
*
*       Receipt no and amount - blank out previous values.
*
*       Re-display the screen - set WSSP-NEXTPROG to the current
*            screen name (SCRN-SCRNAME).
*
*****************************************************************
* </pre>
*/
public class P5009 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P5009");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaFactor = new PackedDecimalData(11, 5);
	private PackedDecimalData wsaaRcptamt = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntfee = new PackedDecimalData(17, 2).init(0);
	private String wsaaSpaces = "N";
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaTax = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaCntfeeTax = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaTranseq = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaTranseqPic9 = new ZonedDecimalData(4, 0).isAPartOf(wsaaTranseq, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgacctChdr = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgacctRcpt = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 8);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	private FixedLengthStringData wsaaReceiptno = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPayrCntcurr = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);
	private static final String e677 = "E677";
	private static final String e678 = "E678";
	private static final String e680 = "E680";
	private static final String e681 = "E681";
		/* TABLES */
	private static final String t3695 = "T3695";
	private static final String t5645 = "T5645";
	private static final String t5674 = "T5674";
	private static final String t5688 = "T5688";
	private static final String t6687 = "T6687";
	private static final String tr52d = "TR52D";
	private static final String tr52e = "TR52E";
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AdocsciTableDAM adocsciIO = new AdocsciTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private CovtlnbTableDAM covtlnbIO = new CovtlnbTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private RdocTableDAM rdocIO = new RdocTableDAM();
	private TtrcTableDAM ttrcIO = new TtrcTableDAM();
	private Batckey wsaaBatckey = new Batckey();
	private T3695rec t3695rec = new T3695rec();
	private T5645rec t5645rec = new T5645rec();
	private T5674rec t5674rec = new T5674rec();
	private T5688rec t5688rec = new T5688rec();
	private T6687rec t6687rec = new T6687rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Prasrec prasrec = new Prasrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private S5009ScreenVars sv = ScreenProgram.getScreenVars( S5009ScreenVars.class);
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaBillingInformationInner wsaaBillingInformationInner = new WsaaBillingInformationInner();
	private AdocpfDAO  adocpfDAO =  getApplicationContext().getBean("adocpfDAO", AdocpfDAO.class);
	private List<Adocpf> adocpfList = new ArrayList<Adocpf>();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		readseqCovtlnb1310,
		continue1380,
		exit1390,
		callAdocsci1830,
		nextAdocsci1840,
		exit1890,
		checkForErrors2040,
		exit2090,
		exit3090,
		exit4090
	}

	public P5009() {
		super();
		screenVars = sv;
		new ScreenModel("S5009", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		readAccRuleTable1020();
		getSign1020();
		retreiveHeader1030();
		readContractLongdesc1040();
		retreiveClientDetails1050();
		clientNameFormat1060();
		loadPayerDetails1065();
		calcSuspense1070();
		calcPremium1090();
		adjustRegularPrem1100();
	}

protected void initialise1010()
	{
		sv.dataArea.set(SPACES);
		/* MOVE ZERO                                                    */
		/*      TO S5009-CNTFEE                   .                     */
		sv.cntsusp.set(ZERO);
		sv.instPrem.set(ZERO);
		sv.rcptamt.set(ZERO);
		/* MOVE ZERO                      WSAA-INSTPRM,                 */
		/*                                WSAA-SINGP,                   */
		/*                                WSAA-REGPRM,                  */
		wsaaRcptamt.set(ZERO);
		wsaaFactor.set(ZERO);
		wsaaReceiptno.set(SPACES);
		wsaaTax.set(ZERO);
		wsaaCntfeeTax.set(ZERO);
		wsaaBatckey.set(wsspcomn.batchkey);
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, 9))) {
			wsaaBillingInformationInner.wsaaIncomeSeqNo[wsaaSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBtdate[wsaaSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRegprem[wsaaSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaSingp[wsaaSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaSpTax[wsaaSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaRpTax[wsaaSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaInstprm[wsaaSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaPremTax[wsaaSub.toInt()].set(ZERO);
			wsaaBillingInformationInner.wsaaBillfreq[wsaaSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntnum[wsaaSub.toInt()].set(SPACES);
			wsaaBillingInformationInner.wsaaClntcoy[wsaaSub.toInt()].set(SPACES);
			wsaaSub.add(1);
		}

	}

protected void readAccRuleTable1020()
	{
		/*    Read the Program  table T5645 for the Financial Accounting*/
		/*    Rules for the transaction.*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFunction("READR");
		callItem1400();
		t5645rec.t5645Rec.set(itemIO.getGenarea());
	}

protected void getSign1020()
	{
		/*    Read the table T3695 for the field sign of the 02 posting.*/
		itemIO.setItemtabl(t3695);
		itemIO.setItemitem(t5645rec.sacstype02);
		itemIO.setFunction("READR");
		callItem1400();
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		/*GET-DATE*/
		/*    Get the current date from DATCON1.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	}

protected void retreiveHeader1030()
	{
		/*    Read CHDRLNB (retrv) in order to obtain the contract header.*/
		chdrlnbIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		defaultReceiptno1800();
		/* Read table TR52D                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52d);
		itemIO.setItemitem(chdrlnbIO.getRegister());
		itemIO.setItempfx("IT");
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52d);
			itemIO.setItemitem("***");
			itemIO.setItempfx("IT");
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

protected void readContractLongdesc1040()
	{
		/*    Read Contract header Long description  from table T5688 for*/
		/*    the contract type held on CHDRLND.*/
		descIO.setDataArea(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(chdrlnbIO.getChdrcoy());
		descIO.setDesctabl("T5688");
		descIO.setDescitem(chdrlnbIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrlnbIO.getChdrnum());
		sv.cnttype.set(chdrlnbIO.getCnttype());
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			sv.ctypedes.fill("?");
		}
		else {
			sv.ctypedes.set(descIO.getLongdesc());
		}
		/* MOVE DESC-LONGDESC          TO S5009-CTYPEDES.               */
		sv.cownnum.set(chdrlnbIO.getCownnum());
	}

protected void retreiveClientDetails1050()
	{
		/*    Read the Client details for the associated Life.*/
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntpfx("CN");
		/*    MOVE WSSP-COMPANY           TO CLTS-CLNTCOY.                 */
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntnum(chdrlnbIO.getCownnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
	}

protected void clientNameFormat1060()
	{
		/*    Format the Client name extracted. Special Format.*/
		/*    SEE Copy Confname in the procedure division.*/
		if (isEQ(cltsIO.getSalutl(), SPACES)) {
			plainname();
		}
		else {
			payeename();
		}
		sv.ownername.set(wsspcomn.longconfname);
	}

protected void loadPayerDetails1065()
	{
		/* Load the WS table by reading all the payer                      */
		/* and payer role recored for this contract.                       */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		payrIO.setChdrnum(chdrlnbIO.getChdrnum());
		payrIO.setValidflag("1");
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		payrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		payrIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		while ( !(isEQ(payrIO.getStatuz(), "ENDP")
		|| isNE(payrIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(payrIO.getChdrnum(), chdrlnbIO.getChdrnum()))) {
			loadPayerDetails1600();
		}

	}

protected void calcSuspense1070()
	{
		/*    Read  the sub account balance for  the contract and apply*/
		/*    the sign for the sub account type.*/
		acblIO.setDataArea(SPACES);
		acblIO.setRldgacct(ZERO);
		acblIO.setRldgcoy(wsspcomn.company);
		acblIO.setRldgacct(chdrlnbIO.getChdrnum());
		acblIO.setOrigcurr(chdrlnbIO.getCntcurr());
		acblIO.setSacscode(t5645rec.sacscode02);
		acblIO.setSacstyp(t5645rec.sacstype02);
		acblIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(acblIO.getParams());
			fatalError600();
		}
		if (isEQ(acblIO.getStatuz(), varcom.mrnf)) {
			sv.cntsusp.set(ZERO);
		}
		else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(sv.cntsusp, 2).set(mult(acblIO.getSacscurbal(), -1));
			}
			else {
				sv.cntsusp.set(acblIO.getSacscurbal());
			}
		}
	}

	/**
	* <pre>
	*****MOVE SPACES                 TO SACSLNB-DATA-AREA.
	*****MOVE WSSP-COMPANY           TO SACSLNB-CHDRCOY.
	*****MOVE CHDRLNB-CHDRNUM        TO SACSLNB-CHDRNUM.
	*****MOVE CHDRLNB-CNTCURR        TO SACSLNB-CNTCURR.
	*****MOVE T5645-SACSCODE-02      TO SACSLNB-SACSCODE.
	*****MOVE T5645-SACSTYPE-02      TO SACSLNB-SACSTYP.
	*****MOVE READR                  TO SACSLNB-FUNCTION.
	*****CALL 'SACSLNBIO' USING SACSLNB-PARAMS.
	*****IF SACSLNB-STATUZ           NOT = O-K
	*****                        AND NOT = MRNF
	*****   MOVE SACSLNB-PARAMS      TO SYSR-PARAMS
	*****   PERFORM 600-FATAL-ERROR.
	*****IF SACSLNB-STATUZ           = MRNF
	*****   MOVE ZEROS               TO S5009-CNTSUSP
	*****ELSE
	*****   IF T3695-SIGN            = '-'
	*****      MULTIPLY SACSLNB-SACSCURBAL BY -1
	*****                            GIVING S5009-CNTSUSP
	*****   ELSE
	*****      MOVE SACSLNB-SACSCURBAL TO S5009-CNTSUSP.
	*1080-CALC-CONTRACT-FEE.
	**** Read the contract definition details T5688 for the contract
	**** type held on the contract header.
	**** MOVE SPACES                 TO ITDM-DATA-KEY.
	**** MOVE T5688                  TO ITDM-ITEMTABL.
	**** MOVE CHDRLNB-CNTTYPE        TO ITDM-ITEMITEM.
	**** PERFORM 1500-CALL-ITDM.
	**** MOVE ITDM-GENAREA TO T5688-T5688-REC.
	* </pre>
	*/
protected void calcPremium1090()
	{
		/*    Read through the Coverage/Rider transactions COVTLNB.*/
		sv.cntcurr.set(chdrlnbIO.getCntcurr());
		sv.billfreq.set(chdrlnbIO.getBillfreq());
		sv.mop.set(chdrlnbIO.getBillchnl());
		covtlnbIO.setParams(SPACES);
		covtlnbIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		covtlnbIO.setChdrnum(chdrlnbIO.getChdrnum());
		covtlnbIO.setSeqnbr(ZERO);
		covtlnbIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		covtlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covtlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		while ( !(isEQ(covtlnbIO.getStatuz(),varcom.endp))) {
			calcPremium1300();
		}

	}

	/**
	* <pre>
	**** MOVE 0                      TO WSAA-INSTPRM.
	* </pre>
	*/
protected void adjustRegularPrem1100()
	{
		/* Change the premium pro-rata to calculated on a payer            */
		/* by payer basis.                                                 */
		/* If The Calculated Regular Premium is equal to zeros then     */
		/* move zeros to the screen fields.                             */
		/* IF WSAA-REGPREM             = ZERO                           */
		/*    GO TO 1180-ADD-IN-SINGLE-PREMIUM.                         */
		/* Get the frequency Factor from DATCON3 for Regular premiums.  */
		/* MOVE CHDRLNB-BILLCD         TO DTC3-INT-DATE-1.              */
		/* MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-1.              */
		/* MOVE CHDRLNB-OCCDATE        TO DTC3-INT-DATE-2.              */
		/* MOVE CHDRLNB-BILLCD         TO DTC3-INT-DATE-2.              */
		/* MOVE CHDRLNB-BTDATE         TO DTC3-INT-DATE-2.              */
		/* MOVE CHDRLNB-BILLFREQ       TO DTC3-FREQUENCY.               */
		/* CALL 'DATCON3' USING DTC3-DATCON3-REC.                       */
		/* IF DTC3-STATUZ              NOT = O-K                        */
		/*    GO TO 1190-EXIT.                                          */
		/* Use the DATCON3 Frequency Factor to calculate the Instate-   */
		/* ment Premium.                                                */
		/* MOVE DTC3-FREQ-FACTOR       TO WSAA-FACTOR.                  */
		/* MULTIPLY WSAA-REGPREM       BY WSAA-FACTOR                   */
		/*                             GIVING WSAA-INSTPRM.             */
		/*1180-ADD-IN-SINGLE-PREMIUM.                                      */
		/* ADD WSAA-SINGP              TO WSAA-INSTPRM.                 */
		/* MOVE WSAA-INSTPRM           TO S5009-INST-PREM.              */
		/* Look up the tax relief method on T5688.                         */
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlnbIO.getCnttype());
		callItdm1500();
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/* Look up the subroutine on T6687.                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(t6687);
		itemIO.setItemitem(t5688rec.taxrelmth);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t6687rec.t6687Rec.set(itemIO.getGenarea());
		}
		else {
			t6687rec.t6687Rec.set(SPACES);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Adjust the regular premiums (if not zero) by the number         */
		/* of frequencies required prior to issue.                         */
		/* If the contract has a tax relief method deduct the tax          */
		/* relief amount from the amount due shown on the screen.          */
		wsaaSub.set(1);
		while ( !(isGT(wsaaSub, 9)
		|| isEQ(wsaaBillingInformationInner.wsaaClntnum[wsaaSub.toInt()], SPACES))) {
			adjustPremium1700();
		}

		/* Add the contract fee from the CHDR to the Amount Due            */
		/*    Calculate the contract fee by using the subroutine found in  */
		/*    table T5674. See 1200 Section.                               */
		if (isNE(t5688rec.feemeth, SPACES)) {
			calcFee1200();
			if (isNE(wsaaCntfee, ZERO)) {
				zrdecplrec.amountIn.set(wsaaCntfee);
				callRounding8000();
				wsaaCntfee.set(zrdecplrec.amountOut);
			}
			if (isNE(wsaaCntfeeTax, ZERO)) {
				zrdecplrec.amountIn.set(wsaaCntfeeTax);
				callRounding8000();
				wsaaCntfeeTax.set(zrdecplrec.amountOut);
			}
			sv.instPrem.add(wsaaCntfee);
		}
		sv.instPrem.add(wsaaCntfeeTax);
	}

protected void calcFee1200()
	{
		readSubroutineTable1210();
	}

protected void readSubroutineTable1210()
	{
		/*    Reference T5674 to obtain the subroutine required to work    */
		/*    out the Fee amount by the correct method.                    */
		itemIO.setItemtabl(t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction("READR");
		callItem1400();
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		mgfeelrec.cnttype.set(chdrlnbIO.getCnttype());
		mgfeelrec.billfreq.set(chdrlnbIO.getBillfreq());
		mgfeelrec.effdate.set(chdrlnbIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlnbIO.getCntcurr());
		mgfeelrec.company.set(wsspcomn.company);
		/* Check subroutine NOT = SPACES before attempting call.           */
		if (isEQ(t5674rec.commsubr, SPACES)) {
			wsaaCntfee.set(0);
			return ;
		}
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, varcom.oK)
		&& isNE(mgfeelrec.statuz, varcom.endp)) {
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		/*IF S5009-INST-PREM      NOT = 0                         <013>*/
		compute(wsaaCntfee, 5).set(mult(mgfeelrec.mgfee, wsaaFactor));
		wsaaCntfeeTax.set(ZERO);
		checkCalcContTax7100();
		compute(wsaaCntfeeTax, 6).setRounded(mult(wsaaTax, wsaaFactor));
		/*  Add single premium fee if regular paying contract with SP      */
		if (isEQ(wsaaBillingInformationInner.wsaaSingp[1], ZERO)
		|| isEQ(chdrlnbIO.getBillfreq(), "00")) {
			return ;
		}
		mgfeelrec.billfreq.set("00");
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, varcom.oK)
		&& isNE(mgfeelrec.statuz, varcom.endp)) {
			syserrrec.statuz.set(mgfeelrec.statuz);
			fatalError600();
		}
		wsaaCntfee.add(mgfeelrec.mgfee);
		checkCalcContTax7100();
		compute(wsaaCntfeeTax, 3).setRounded(add(wsaaCntfeeTax, wsaaTax));
	}

	/**
	* <pre>
	*1200-CALC-FEE SECTION.
	*1210-READ-SUBROUTINE-TABLE.
	**** Reference T5674 to obtain the subroutine required to work
	**** out the Fee amount by the correct method.
	**** MOVE T5674                  TO ITEM-ITEMTABL
	**** MOVE T5688-FEEMETH          TO ITEM-ITEMITEM
	**** MOVE 'READR'                TO ITEM-FUNCTION
	**** PERFORM 1400-CALL-ITEM.
	**** MOVE ITEM-GENAREA TO T5674-T5674-REC.
	**** MOVE CHDRLNB-CNTTYPE        TO MGFL-CNTTYPE.
	**** MOVE CHDRLNB-BILLFREQ       TO MGFL-BILLFREQ.
	**** MOVE CHDRLNB-OCCDATE        TO MGFL-EFFDATE.
	**** MOVE CHDRLNB-CNTCURR        TO MGFL-CNTCURR.
	**** MOVE WSSP-COMPANY           TO MGFL-COMPANY.
	**Check subroutine NOT = SPACES before attempting call.
	**** IF T5674-COMMSUBR           = SPACES
	****    MOVE 0                   TO S5009-CNTFEE
	****    GO TO 1290-EXIT.
	**** CALL T5674-COMMSUBR USING MGFL-MGFEEL-REC.
	**** IF MGFL-STATUZ              NOT = O-K
	****                         AND NOT = ENDP
	****    MOVE MGFL-STATUZ         TO SYSR-STATUZ
	****    PERFORM 600-FATAL-ERROR.
	**** MOVE MGFL-MGFEE             TO S5009-CNTFEE.
	**** IF WSAA-INSTPRM             = 0
	****    MOVE 0                   TO S5009-CNTFEE
	**** ELSE
	****    MULTIPLY MGFL-MGFEE      BY WSAA-FACTOR
	****                             GIVING S5009-CNTFEE.
	*1290-EXIT.
	**** EXIT.
	* </pre>
	*/
protected void calcPremium1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case readseqCovtlnb1310:
					readseqCovtlnb1310();
					singleOrRegular1330();
				case continue1380:
					continue1380();
				case exit1390:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readseqCovtlnb1310()
	{
		/*    Read each record and accumulate all single and regular*/
		/*    premiums payable.*/
		SmartFileCode.execute(appVars, covtlnbIO);
		if (isNE(covtlnbIO.getStatuz(), varcom.oK)
		&& isNE(covtlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covtlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covtlnbIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(covtlnbIO.getChdrnum(), chdrlnbIO.getChdrnum())) {
			covtlnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(covtlnbIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit1390);
		}
		/*COVER-RIDER-DEFINITION*/
		/**    Check the Coverage/Rider Definition T5687 for a single*/
		/**    Premium indicator of 'Y'.*/
		/***** MOVE SPACES                 TO ITDM-DATA-KEY.                */
		/***** MOVE T5687                  TO ITDM-ITEMTABL.                */
		/***** MOVE COVTLNB-CRTABLE        TO ITDM-ITEMITEM.                */
		/***** PERFORM 1400-CALL-ITEM.                                      */
		/***** MOVE ITEM-GENAREA TO T5687-T5687-REC.                        */
	}

protected void singleOrRegular1330()
	{
		/*    Single or Regular Premium?*/
		/* IF CHDRLNB-BILLFREQ         = '00'                           */
		/* OR T5687-SINGLE-PREM-IND    = 'Y'                            */
		/*    ADD COVTLNB-SINGP        TO WSAA-SINGP                    */
		/* ELSE                                                         */
		/*    ADD COVTLNB-SINGP        TO WSAA-REGPREM.                 */
		/* MOVE NEXTR                  TO COVTLNB-FUNCTION.             */
		/* Add the premium into the relevant entry in the WS table*/
		/* depending  on the payer number on the COVT.*/
		wsaaSub.set(covtlnbIO.getPayrseqno());
		wsaaBillingInformationInner.wsaaSingp[wsaaSub.toInt()].add(covtlnbIO.getSingp());
		if (isEQ(covtlnbIO.getInstprem(), 0)) {
			goTo(GotoLabel.continue1380);
		}
		wsaaBillingInformationInner.wsaaRegprem[wsaaSub.toInt()].add(covtlnbIO.getInstprem());
	}

	/**
	* <pre>
	**** ADD COVTLNB-SINGP           TO WSAA-SINGP.
	**** IF COVTLNB-INSTPREM         = 0
	****    GO                       TO 1380-CONTINUE.
	****  ADD COVTLNB-INSTPREM       TO WSAA-REGPREM.
	* </pre>
	*/
protected void continue1380()
	{
		checkCalcCompTax7000();
		covtlnbIO.setFunction(varcom.nextr);
		goTo(GotoLabel.readseqCovtlnb1310);
	}

protected void callItem1400()
	{
		/*CALL-TABLE-ITEM*/
		/*    Call module for the reference of tables.*/
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItempfx("IT");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void callItdm1500()
	{
		/*CALL-TABLE-ITDM*/
		/*    Call module for the reference of tables. DATED.....*/
		itdmIO.setItemcoy(chdrlnbIO.getChdrcoy());
		itdmIO.setItmfrm(chdrlnbIO.getOccdate());
		itdmIO.setFunction("BEGN");
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			fatalError600();
		}
		/*EXIT*/
	}

protected void loadPayerDetails1600()
	{
		loadPayer1610();
	}

protected void loadPayer1610()
	{
		/* Read the client role file to get the payer number.              */
		clrfIO.setForepfx(chdrlnbIO.getChdrpfx());
		clrfIO.setForecoy(payrIO.getChdrcoy());
		wsaaChdrnum.set(payrIO.getChdrnum());
		wsaaPayrseqno.set(payrIO.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(clrfIO.getStatuz());
			syserrrec.params.set(clrfIO.getParams());
			fatalError600();
		}
		/* Load the working storage table using the payer sequence         */
		/* number as the subscript.                                        */
		wsaaSub.set(payrIO.getPayrseqno());
		wsaaBillingInformationInner.wsaaIncomeSeqNo[wsaaSub.toInt()].set(payrIO.getIncomeSeqNo());
		wsaaBillingInformationInner.wsaaBillfreq[wsaaSub.toInt()].set(payrIO.getBillfreq());
		wsaaBillingInformationInner.wsaaBtdate[wsaaSub.toInt()].set(payrIO.getBtdate());
		wsaaBillingInformationInner.wsaaClntnum[wsaaSub.toInt()].set(clrfIO.getClntnum());
		wsaaBillingInformationInner.wsaaClntcoy[wsaaSub.toInt()].set(clrfIO.getClntcoy());
		payrIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
	}

protected void adjustPremium1700()
	{
		adjustPremium1710();
	}

protected void adjustPremium1710()
	{
		/*    Get the frequency Factor from DATCON3.                       */
		if (isNE(wsaaBillingInformationInner.wsaaRegprem[wsaaSub.toInt()], ZERO)) {
			datcon3rec.intDate1.set(chdrlnbIO.getOccdate());
			datcon3rec.intDate2.set(wsaaBillingInformationInner.wsaaBtdate[wsaaSub.toInt()]);
			datcon3rec.frequency.set(wsaaBillingInformationInner.wsaaBillfreq[wsaaSub.toInt()]);
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz, varcom.oK)) {
				syserrrec.statuz.set(datcon3rec.statuz);
				fatalError600();
			}
			/* Calculate the instalment premium.                               */
			wsaaFactor.set(datcon3rec.freqFactor);
			compute(wsaaBillingInformationInner.wsaaInstprm[wsaaSub.toInt()], 5).set(mult(wsaaBillingInformationInner.wsaaRegprem[wsaaSub.toInt()], wsaaFactor));
		}
		/* Add in the single premium.                                      */
		wsaaBillingInformationInner.wsaaInstprm[wsaaSub.toInt()].add(wsaaBillingInformationInner.wsaaSingp[wsaaSub.toInt()]);
		/* If the tax relief method is not spaces calculate the tax        */
		/* relief amount and deduct it from the premium before displaying  */
		/* it on screen.                                                   */
		if (isNE(t5688rec.taxrelmth, SPACES)) {
			prasrec.clntnum.set(wsaaBillingInformationInner.wsaaClntnum[wsaaSub.toInt()]);
			prasrec.clntcoy.set(wsaaBillingInformationInner.wsaaClntcoy[wsaaSub.toInt()]);
			prasrec.incomeSeqNo.set(wsaaBillingInformationInner.wsaaIncomeSeqNo[wsaaSub.toInt()]);
			prasrec.cnttype.set(chdrlnbIO.getCnttype());
			prasrec.taxrelmth.set(t5688rec.taxrelmth);
			prasrec.effdate.set(datcon1rec.intDate);
			prasrec.company.set(chdrlnbIO.getChdrcoy());
			prasrec.grossprem.set(wsaaBillingInformationInner.wsaaInstprm[wsaaSub.toInt()]);
			prasrec.statuz.set(varcom.oK);
			callProgram(t6687rec.taxrelsub, prasrec.prascalcRec);
			if (isNE(prasrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(prasrec.statuz);
				syserrrec.subrname.set(t6687rec.taxrelsub);
				fatalError600();
			}
			wsaaBillingInformationInner.wsaaInstprm[wsaaSub.toInt()].subtract(prasrec.taxrelamt);
		}
		compute(wsaaBillingInformationInner.wsaaPremTax[wsaaSub.toInt()], 6).setRounded(add((mult(wsaaBillingInformationInner.wsaaRpTax[wsaaSub.toInt()], wsaaFactor)), wsaaBillingInformationInner.wsaaSpTax[wsaaSub.toInt()]));
		sv.instPrem.set(wsaaBillingInformationInner.wsaaInstprm[1]);
		sv.instPrem.add(wsaaBillingInformationInner.wsaaPremTax[wsaaSub.toInt()]);
		wsaaSub.add(1);
	}

protected void defaultReceiptno1800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readTtrc1810();
					searchBegin1820();
				case callAdocsci1830:
					callAdocsci1830();
				case nextAdocsci1840:
					nextAdocsci1840();
				case exit1890:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readTtrc1810()
	{
		ttrcIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		ttrcIO.setChdrnum(chdrlnbIO.getChdrnum());
		ttrcIO.setEffdate(varcom.vrcmMaxDate);
		ttrcIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		ttrcIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		ttrcIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		ttrcIO.setFormat(formatsInner.ttrcrec);
		SmartFileCode.execute(appVars, ttrcIO);
		if (isNE(ttrcIO.getStatuz(), varcom.oK)
		&& isNE(ttrcIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(ttrcIO.getStatuz());
			syserrrec.params.set(ttrcIO.getParams());
			fatalError600();
		}
		if (isEQ(ttrcIO.getStatuz(), varcom.endp)
		|| isNE(ttrcIO.getChdrcoy(), chdrlnbIO.getChdrcoy())
		|| isNE(ttrcIO.getChdrnum(), chdrlnbIO.getChdrnum())
		|| isEQ(ttrcIO.getTtmprcno(), SPACES)) {
			goTo(GotoLabel.exit1890);
		}
	}

protected void searchBegin1820()
	{
		/*adocsciIO.setDataKey(SPACES);
		adocsciIO.setAdocAcmvJrnseq(ZERO);
		adocsciIO.setFunction(varcom.begn);*/
	}

protected void callAdocsci1830()
	{
		/*adocsciIO.setFormat(SPACES);
		SmartFileCode.execute(appVars, adocsciIO);
		if (isNE(adocsciIO.getStatuz(), varcom.oK)
		&& isNE(adocsciIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(adocsciIO.getStatuz());
			syserrrec.params.set(adocsciIO.getParams());
			fatalError600();
		}
		if (isEQ(adocsciIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit1890);
		}*/
		adocpfList = adocpfDAO.getAdocsciRecords();
		for(Adocpf a : adocpfList) {
			if (isEQ(a.getRecordFormat(), formatsInner.rtrnrec)) {
				if (isEQ(a.getAdocRtrnrec().getTrandesc(), ttrcIO.getTtmprcno())) {
					sv.receiptno.set(a.getAdocRtrnrec().getRdocnum());
					wsaaReceiptno.set(a.getAdocRtrnrec().getRdocnum());
					wsaaTranseqPic9.set(a.getAdocRtrnrec().getJrnseq());
					sv.transeq.set(wsaaTranseq);
					sv.rcptamt.set(a.getAdocRtrnrec().getOrigamt());
					goTo(GotoLabel.exit1890);
				}
				//goTo(GotoLabel.nextAdocsci1840);
			}
			if (isEQ(a.getRecordFormat(), formatsInner.acmvrec)) {
				if (isEQ(a.getAdocAcmvrec().getTrandesc(), ttrcIO.getTtmprcno())) {
					sv.receiptno.set(a.getAdocAcmvrec().getRdocnum());
					wsaaReceiptno.set(a.getAdocAcmvrec().getRdocnum());
					wsaaTranseqPic9.set(a.getAdocAcmvrec().getJrnseq());
					sv.transeq.set(wsaaTranseq);
					sv.rcptamt.set(a.getAdocAcmvrec().getOrigamt());
					goTo(GotoLabel.exit1890);
				}
				//goTo(GotoLabel.nextAdocsci1840);
			}
		}
	}

protected void nextAdocsci1840()
	{
		/*adocsciIO.setFunction(varcom.nextr);
		goTo(GotoLabel.callAdocsci1830);*/
	}

	/**
	* <pre>
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		/*    If Enquiry then Protect the screen.                          */
		if (isEQ(wsspcomn.flag, "I")) {
			scrnparams.function.set(varcom.prot);
		}
		/*    Call the screen I/O module.                                  */
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					screenIo2010();
					readRdoc2020();
					validateReceipt2030();
				case checkForErrors2040:
					checkForErrors2040();
					calcRedisplay2050();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		/*    CALL 'S5009IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          S5009-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*    If Enquiry or Termination requested then skip Validation.*/
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(scrnparams.statuz, "KILL")) {
			goTo(GotoLabel.exit2090);
		}
		/* IF   S5009-RECEIPTNO        = SPACES                         */
		/*    MOVE 'Y'        TO WSAA-SPACES                            */
		/*    GO TO 2090-EXIT.                                          */
		if (isEQ(sv.receiptno, SPACES)) {
			wsaaSpaces = "Y";
			goTo(GotoLabel.exit2090);
		}
		else {
			wsaaSpaces = "N";
		}
	}

protected void readRdoc2020()
	{
		/*    Attempt to Read the Receipt record held on RTRN.*/
		rdocIO.setDataArea(SPACES);
		rdocIO.setRdocpfx("CA");
		rdocIO.setRdoccoy(wsspcomn.company);
		rdocIO.setRdocnum(sv.receiptno);
		/* MOVE '0001'                 TO RDOC-TRANSEQ.                 */
		/* MOVE S5009-TRANSEQ          TO RDOC-TRANSEQ.         <LA3285>*/
		rdocIO.setTranseq("0001");
		rdocIO.setFunction(varcom.begnh);
		SmartFileCode.execute(appVars, rdocIO);
	}

protected void validateReceipt2030()
	{
		if (isNE(rdocIO.getStatuz(), varcom.oK)
		&& isNE(rdocIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(rdocIO.getParams());
			fatalError600();
		}
		/*    If the Receipt is not found then Error*/
		if (isNE(rdocIO.getRdocpfx(), "CA")
		|| isNE(rdocIO.getRdoccoy(), wsspcomn.company)
		|| isNE(rdocIO.getRdocnum(), sv.receiptno)
		|| isNE(rdocIO.getTranseq(), "0001")
		|| isEQ(rdocIO.getStatuz(), varcom.endp)) {
			sv.receiptnoErr.set(e677);
			goTo(GotoLabel.checkForErrors2040);
		}
		/*    If the Receipt was not posted to the company suspense Error*/
		if (isNE(rdocIO.getSacscode(), t5645rec.sacscode01)
		|| isNE(rdocIO.getSacstyp(), t5645rec.sacstype01)) {
			sv.receiptnoErr.set(e678);
			goTo(GotoLabel.checkForErrors2040);
		}
		/*    If the Receipt currency is not the same as the contract*/
		/*    currency then Error.*/
		/* The comparison should be between the receipt LEDGER          */
		/* currency and the payer currency.                             */
		/* IF RDOC-GENLCUR             NOT = CHDRLNB-CNTCURR    <CAS1.0>*/
		/* IF RDOC-ORIGCCY          NOT = WSAA-PAYR-CNTCURR     <CAS1.0>*/
		/*    MOVE E679                TO S5009-RECEIPTNO-ERR           */
		/*    GO TO 2040-CHECK-FOR-ERRORS.*/
		/*    If the Suspense Receipt has already been transferred Error.*/
		if (isNE(rdocIO.getRldgacct(), SPACES)) {
			sv.receiptnoErr.set(e680);
			goTo(GotoLabel.checkForErrors2040);
		}
		if (isEQ(sv.receiptno, SPACES)) {
			sv.receiptnoErr.set(e677);
			goTo(GotoLabel.checkForErrors2040);
		}
	}

protected void checkForErrors2040()
	{
		/*    Set flag for screen errors.*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			/*  MOVE SPACES              TO WSSP-EDTERROR.                */
			wsspcomn.edterror.set(varcom.oK);
		}
	}

protected void calcRedisplay2050()
	{
		/*    If Calculation is requested then Re-display the screen*/
		/*    Showing the receipt amount.*/
		if (isNE(scrnparams.statuz, "CALC")
		&& isNE(scrnparams.statuz, varcom.oK)) {
			sv.receiptnoErr.set(e681);
		}
		if ((isEQ(scrnparams.statuz, "CALC")
		|| isEQ(scrnparams.statuz, varcom.oK))
		&& (isEQ(wsspcomn.edterror, SPACES)
		|| isEQ(wsspcomn.edterror, varcom.oK))) {
			if (isNE(sv.receiptno, wsaaReceiptno)) {
				scrnparams.statuz.set("CALC");
			}
			sv.rcptamt.set(rdocIO.getOrigamt());
			wsaaReceiptno.set(rdocIO.getRdocnum());
		}
		else {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(scrnparams.statuz, "CALC")) {
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			updateRdoc3010();
			updateLifrtrn3010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateRdoc3010()
	{
		/*    If Enquiry or Termination requested then skip Updating.*/
		if (isEQ(wsspcomn.flag, "I")
		|| isEQ(scrnparams.statuz, "KILL")
		|| isEQ(wsaaSpaces, "Y")) {
			goTo(GotoLabel.exit3090);
		}
		/*    Update the information on the Receipt File RDOC.*/
		wsaaRldgacctChdr.set(chdrlnbIO.getChdrnum());
		wsaaRldgacctRcpt.set(sv.receiptno);
		rdocIO.setRldgacct(wsaaRldgacct);
		rdocIO.setRldgpfx("CH");
		rdocIO.setRldgcoy(wsspcomn.company);
		rdocIO.setFunction("REWRT");
		SmartFileCode.execute(appVars, rdocIO);
		if (isNE(rdocIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(rdocIO.getParams());
			fatalError600();
		}
	}

protected void updateLifrtrn3010()
	{
		/*    Call LIFRTRN Twice.*/
		/*       Once for the Posting '01' and*/
		setupFields013020();
		lifrtrnUpdte3100();
		/*       Once for the Posting '02'*/
		setupFields023030();
		lifrtrnUpdte3100();
		goTo(GotoLabel.exit3090);
	}

protected void setupFields013020()
	{
		/*    Fields associated with Posting '01' only.*/
		lifrtrnrec.function.set("NPSTW");
		wsaaTranseq.set(rdocIO.getTranseq());
		compute(lifrtrnrec.jrnseq, 0).set(add(1, wsaaTranseqPic9));
		/* MOVE T5645-SACSCODE-01      TO LIFR-SACSCODE.                */
		/* MOVE T5645-SACSTYPE-01      TO LIFR-SACSTYP.                 */
		/* MOVE T5645-CNTTOT-01        TO LIFR-CONTOT.                  */
		/* MOVE T5645-GLMAP-01         TO LIFR-GLCODE.                  */
		/* MOVE T5645-SIGN-01          TO LIFR-GLSIGN.                  */
		lifrtrnrec.sacscode.set(t5645rec.sacscode03);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype03);
		lifrtrnrec.contot.set(t5645rec.cnttot03);
		lifrtrnrec.glcode.set(t5645rec.glmap03);
		lifrtrnrec.glsign.set(t5645rec.sign03);
		lifrtrnrec.rcamt.set(rdocIO.getOrigamt());
		lifrtrnrec.frcdate.set(rdocIO.getTrandate());
	}

protected void setupFields023030()
	{
		/*    Fields associated with Posting '02' only.*/
		lifrtrnrec.function.set("PSTW");
		lifrtrnrec.jrnseq.add(1);
		lifrtrnrec.sacscode.set(t5645rec.sacscode02);
		lifrtrnrec.sacstyp.set(t5645rec.sacstype02);
		lifrtrnrec.contot.set(t5645rec.cnttot02);
		lifrtrnrec.glcode.set(t5645rec.glmap02);
		lifrtrnrec.glsign.set(t5645rec.sign02);
		lifrtrnrec.rcamt.set(ZERO);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
	}

protected void lifrtrnUpdte3100()
	{
		setupComnFields3110();
		updteLifrtrn3120();
	}

protected void setupComnFields3110()
	{
		/*    Fields associated with BOTH Posting '01' and '02'.*/
		lifrtrnrec.batckey.set(wsspcomn.batchkey);
		lifrtrnrec.rdocnum.set(sv.receiptno);
		lifrtrnrec.tranno.set(ZERO);
		lifrtrnrec.rldgcoy.set(rdocIO.getRldgcoy());
		lifrtrnrec.rldgacct.set(chdrlnbIO.getChdrnum());
		lifrtrnrec.origcurr.set(rdocIO.getOrigccy());
		lifrtrnrec.origamt.set(rdocIO.getOrigamt());
		lifrtrnrec.tranref.set(chdrlnbIO.getChdrnum());
		lifrtrnrec.trandesc.set(rdocIO.getTrandesc());
		lifrtrnrec.crate.set(rdocIO.getCrate());
		lifrtrnrec.acctamt.set(rdocIO.getAcctamt());
		lifrtrnrec.genlcoy.set(rdocIO.getGenlcoy());
		/* MOVE RDOC-GENLCUR           TO LIFR-GENLCUR.                 */
		/*    MOVE RDOC-ACCTCCY           TO LIFR-GENLCUR.            <025>*/
		lifrtrnrec.genlcur.set(rdocIO.getGenlcur());
		lifrtrnrec.postyear.set(ZERO);
		lifrtrnrec.postmonth.set(ZERO);
		lifrtrnrec.effdate.set(rdocIO.getEffdate());
		lifrtrnrec.transactionDate.set(varcom.vrcmDate);
		lifrtrnrec.transactionTime.set(varcom.vrcmTime);
		lifrtrnrec.user.set(varcom.vrcmUser);
		lifrtrnrec.termid.set(varcom.vrcmTermid);
		lifrtrnrec.substituteCode[1].set(chdrlnbIO.getCnttype());
	}

protected void updteLifrtrn3120()
	{
		/*    Call to LIFRTRN I/O module to UPDATE the Record.*/
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(lifrtrnrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void nextProgram4010()
	{
		/*    If ROLLUP requested then Re-display the screen with the*/
		/*    necessary details.*/
		/*    Else exit to next Program.*/
		if (isEQ(scrnparams.statuz, "ROLU")) {
			redisplayScreen4030();
			wsspcomn.nextprog.set(scrnparams.scrname);
		}
		else {
			wsspcomn.nextprog.set(wsaaProg);
			wsspcomn.programPtr.add(1);
		}
		goTo(GotoLabel.exit4090);
	}

protected void redisplayScreen4030()
	{
		/*    If one of the Roll keys has been pressed then re-calculate*/
		/*    the Contract Suspense Amount,  Move blanks  to the Receipt*/
		/*    Number and Amount and re-display the screen.*/
		wsaaRcptamt.set(sv.rcptamt);
		if (isEQ(t5645rec.sign02, "-")) {
			/*   SUBTRACT WSAA-RCPTAMT    FROM SACSLNB-SACSCURBAL*/
			setPrecision(acblIO.getSacscurbal(), 2);
			acblIO.setSacscurbal(sub(acblIO.getSacscurbal(), wsaaRcptamt));
		}
		else {
			setPrecision(acblIO.getSacscurbal(), 2);
			acblIO.setSacscurbal(add(acblIO.getSacscurbal(), wsaaRcptamt));
		}
		/*   ADD WSAA-RCPTAMT         TO SACSLNB-SACSCURBAL.*/
		if (isEQ(t3695rec.sign, "-")) {
			/*   MULTIPLY SACSLNB-SACSCURBAL BY -1*/
			compute(sv.cntsusp, 2).set(mult(acblIO.getSacscurbal(), -1));
		}
		else {
			sv.cntsusp.set(acblIO.getSacscurbal());
		}
		/*   MOVE SACSLNB-SACSCURBAL TO S5009-CNTSUSP.*/
		sv.receiptno.set(SPACES);
		sv.rcptamt.set(ZERO);
	}

protected void checkCalcCompTax7000()
	{
		start7010();
	}

protected void start7010()
	{
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		if (isEQ(covtlnbIO.getSingp(), ZERO)
		&& isEQ(covtlnbIO.getInstprem(), ZERO)) {
			return ;
		}
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set(covtlnbIO.getCrtable());
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
		/* Call TR52D tax subroutine                                       */
		if (isEQ(tr52erec.taxind01, "Y")) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("CALC");
			txcalcrec.statuz.set(varcom.oK);
			txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
			txcalcrec.life.set(covtlnbIO.getLife());
			txcalcrec.coverage.set(covtlnbIO.getCoverage());
			txcalcrec.rider.set(covtlnbIO.getRider());
			txcalcrec.planSuffix.set(ZERO);
			txcalcrec.crtable.set(covtlnbIO.getCrtable());
			txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
			txcalcrec.register.set(chdrlnbIO.getRegister());
			txcalcrec.taxrule.set(wsaaTr52eKey);
			wsaaRateItem.set(SPACES);
			txcalcrec.ccy.set(chdrlnbIO.getCntcurr());
			wsaaCntCurr.set(chdrlnbIO.getCntcurr());
			wsaaTxitem.set(tr52erec.txitem);
			txcalcrec.rateItem.set(wsaaRateItem);
			txcalcrec.transType.set("PREM");
			txcalcrec.taxType[1].set(SPACES);
			txcalcrec.taxType[2].set(SPACES);
			txcalcrec.taxAmt[1].set(ZERO);
			txcalcrec.taxAmt[2].set(ZERO);
			txcalcrec.taxAbsorb[1].set(SPACES);
			txcalcrec.taxAbsorb[2].set(SPACES);
			txcalcrec.effdate.set(chdrlnbIO.getOccdate());
		}
		if (isNE(covtlnbIO.getInstprem(), ZERO)) {
			if (isEQ(tr52erec.zbastyp, "Y")) {
				txcalcrec.amountIn.set(covtlnbIO.getZbinstprem());
			}
			else {
				txcalcrec.amountIn.set(covtlnbIO.getInstprem());
			}
			if (isEQ(tr52erec.taxind01, "Y")) {
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
				}
			}			
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaBillingInformationInner.wsaaRpTax[wsaaSub.toInt()].add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaBillingInformationInner.wsaaRpTax[wsaaSub.toInt()].add(txcalcrec.taxAmt[2]);
				}
			}
		}
		if (isNE(covtlnbIO.getSingp(), ZERO)) {
			txcalcrec.amountIn.set(covtlnbIO.getSingp());
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, varcom.oK)) {
				syserrrec.params.set(txcalcrec.linkRec);
				syserrrec.statuz.set(txcalcrec.statuz);
				fatalError600();
			}
			if (isGT(txcalcrec.taxAmt[1], ZERO)
			|| isGT(txcalcrec.taxAmt[2], ZERO)) {
				if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
					wsaaBillingInformationInner.wsaaSpTax[wsaaSub.toInt()].add(txcalcrec.taxAmt[1]);
				}
				if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
					wsaaBillingInformationInner.wsaaSpTax[wsaaSub.toInt()].add(txcalcrec.taxAmt[2]);
				}
			}
		}
	}

protected void checkCalcContTax7100()
	{
		start7110();
	}

protected void start7110()
	{
		wsaaTax.set(0);
		if (isEQ(mgfeelrec.mgfee, ZERO)) {
			return ;
			/****     GO TO 7090-EXIT                                   <S19FIX>*/
		}
		if (isEQ(tr52drec.txcode, SPACES)) {
			return ;
		}
		/* Read table TR52E                                                */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(wsspcomn.company);
		itemIO.setItemtabl(tr52e);
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlnbIO.getCnttype());
		wsaaTr52eCrtable.set("****");
		itemIO.setItemitem(wsaaTr52eKey);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(wsspcomn.company);
			itemIO.setItemtabl(tr52e);
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			itemIO.setItemitem(wsaaTr52eKey);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				syserrrec.params.set(itemIO.getParams());
				syserrrec.statuz.set(itemIO.getStatuz());
				fatalError600();
			}
		}
		tr52erec.tr52eRec.set(itemIO.getGenarea());
		if (isNE(tr52erec.taxind02, "Y")) {
			return ;
		}
		/* Call TR52D tax subroutine                                       */
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(varcom.oK);
		txcalcrec.chdrcoy.set(chdrlnbIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlnbIO.getChdrnum());
		txcalcrec.life.set(SPACES);
		txcalcrec.coverage.set(SPACES);
		txcalcrec.rider.set(SPACES);
		txcalcrec.crtable.set(SPACES);
		txcalcrec.planSuffix.set(ZERO);
		txcalcrec.cnttype.set(chdrlnbIO.getCnttype());
		txcalcrec.register.set(chdrlnbIO.getRegister());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		wsaaCntCurr.set(chdrlnbIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		txcalcrec.taxType[1].set(SPACES);
		txcalcrec.taxType[2].set(SPACES);
		txcalcrec.taxAmt[1].set(ZERO);
		txcalcrec.taxAmt[2].set(ZERO);
		txcalcrec.taxAbsorb[1].set(SPACES);
		txcalcrec.taxAbsorb[2].set(SPACES);
		txcalcrec.amountIn.set(mgfeelrec.mgfee);
		txcalcrec.transType.set("CNTF");
		txcalcrec.effdate.set(chdrlnbIO.getOccdate());
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, varcom.oK)) {
			syserrrec.params.set(txcalcrec.linkRec);
			syserrrec.statuz.set(txcalcrec.statuz);
			fatalError600();
		}
		if (isGT(txcalcrec.taxAmt[1], ZERO)
		|| isGT(txcalcrec.taxAmt[2], ZERO)) {
			if (isNE(txcalcrec.taxAbsorb[1], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[1]);
			}
			if (isNE(txcalcrec.taxAbsorb[2], "Y")) {
				wsaaTax.add(txcalcrec.taxAmt[2]);
			}
		}
	}

protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(wsspcomn.company);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(chdrlnbIO.getCntcurr());
		zrdecplrec.batctrcde.set(wsaaBatckey.batcBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-BILLING-INFORMATION--INNER
 */
private static final class WsaaBillingInformationInner {

		/* WSAA-BILLING-INFORMATION */
	private FixedLengthStringData[] wsaaBillingDetails = FLSInittedArray (9, 94);
	private ZonedDecimalData[] wsaaIncomeSeqNo = ZDArrayPartOfArrayStructure(2, 0, wsaaBillingDetails, 0, UNSIGNED_TRUE);
	private FixedLengthStringData[] wsaaBillfreq = FLSDArrayPartOfArrayStructure(2, wsaaBillingDetails, 2);
	private PackedDecimalData[] wsaaBillcd = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 4);
	private PackedDecimalData[] wsaaBtdate = PDArrayPartOfArrayStructure(8, 0, wsaaBillingDetails, 9);
	private FixedLengthStringData[] wsaaClntcoy = FLSDArrayPartOfArrayStructure(1, wsaaBillingDetails, 14);
	private FixedLengthStringData[] wsaaClntnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 15);
	private PackedDecimalData[] wsaaRegprem = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 23);
	private PackedDecimalData[] wsaaSingp = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 32);
	private PackedDecimalData[] wsaaSpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 41);
	private PackedDecimalData[] wsaaRpTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 50);
	private PackedDecimalData[] wsaaInstprm = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 59);
	private PackedDecimalData[] wsaaPremTax = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 68);
	private PackedDecimalData[] wsaaTaxrelamt = PDArrayPartOfArrayStructure(17, 2, wsaaBillingDetails, 77);
	private FixedLengthStringData[] wsaaInrevnum = FLSDArrayPartOfArrayStructure(8, wsaaBillingDetails, 86);
}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData acmvrec = new FixedLengthStringData(10).init("ACMVREC");
	private FixedLengthStringData rtrnrec = new FixedLengthStringData(10).init("RTRNREC");
	private FixedLengthStringData ttrcrec = new FixedLengthStringData(10).init("TTRCREC");
}
}
