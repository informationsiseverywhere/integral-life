package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: LextafiTableDAM.java
 * Date: Sun, 30 Aug 2009 03:42:21
 * Class transformed from LEXTAFI.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class LextafiTableDAM extends LextpfTableDAM {

	public LextafiTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("LEXTAFI");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", SEQNBR"
		             + ", TRANNO";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "SEQNBR, " +
		            "VALIDFLAG, " +
		            "TRANNO, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "OPCDA, " +
		            "OPPC, " +
		            "INSPRM, " +
		            "AGERATE, " +
		            "JLIFE, " +
		            "SBSTDL, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "EXTCD, " +
		            "ECESTRM, " +
		            "ECESDTE, " +
		            "REASIND, " +
		            "ZNADJPERC, " +
					"PREMADJ, " +
		            "ZMORTPCT, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "SEQNBR ASC, " +
		            "TRANNO ASC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "SEQNBR DESC, " +
		            "TRANNO DESC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               seqnbr,
                               validflag,
                               tranno,
                               currfrom,
                               currto,
                               opcda,
                               oppc,
                               insprm,
                               agerate,
                               jlife,
                               sbstdl,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               extCommDate,
                               extCessTerm,
                               extCessDate,
                               reasind,
                               znadjperc,
                               premadj,
                               zmortpct,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(44);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getSeqnbr().toInternal()
					+ getTranno().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, seqnbr);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller8 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller5.setInternal(rider.toInternal());
	nonKeyFiller6.setInternal(seqnbr.toInternal());
	nonKeyFiller8.setInternal(tranno.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(140);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ getValidflag().toInternal()
					+ nonKeyFiller8.toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getOpcda().toInternal()
					+ getOppc().toInternal()
					+ getInsprm().toInternal()
					+ getAgerate().toInternal()
					+ getJlife().toInternal()
					+ getSbstdl().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getExtCommDate().toInternal()
					+ getExtCessTerm().toInternal()
					+ getExtCessDate().toInternal()
					+ getReasind().toInternal()
					+ getZnadjperc().toInternal()
					+ getPremadj().toInternal()
					+ getZmortpct().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, nonKeyFiller8);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, opcda);
			what = ExternalData.chop(what, oppc);
			what = ExternalData.chop(what, insprm);
			what = ExternalData.chop(what, agerate);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, sbstdl);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, extCommDate);
			what = ExternalData.chop(what, extCessTerm);
			what = ExternalData.chop(what, extCessDate);
			what = ExternalData.chop(what, reasind);
			what = ExternalData.chop(what, znadjperc);
			what = ExternalData.chop(what, zmortpct);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getSeqnbr() {
		return seqnbr;
	}
	public void setSeqnbr(Object what) {
		setSeqnbr(what, false);
	}
	public void setSeqnbr(Object what, boolean rounded) {
		if (rounded)
			seqnbr.setRounded(what);
		else
			seqnbr.set(what);
	}
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public FixedLengthStringData getOpcda() {
		return opcda;
	}
	public void setOpcda(Object what) {
		opcda.set(what);
	}	
	public PackedDecimalData getOppc() {
		return oppc;
	}
	public void setOppc(Object what) {
		setOppc(what, false);
	}
	public void setOppc(Object what, boolean rounded) {
		if (rounded)
			oppc.setRounded(what);
		else
			oppc.set(what);
	}	
	public PackedDecimalData getInsprm() {
		return insprm;
	}
	public void setInsprm(Object what) {
		setInsprm(what, false);
	}
	public void setInsprm(Object what, boolean rounded) {
		if (rounded)
			insprm.setRounded(what);
		else
			insprm.set(what);
	}	
	public PackedDecimalData getAgerate() {
		return agerate;
	}
	public void setAgerate(Object what) {
		setAgerate(what, false);
	}
	public void setAgerate(Object what, boolean rounded) {
		if (rounded)
			agerate.setRounded(what);
		else
			agerate.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getSbstdl() {
		return sbstdl;
	}
	public void setSbstdl(Object what) {
		sbstdl.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public PackedDecimalData getExtCommDate() {
		return extCommDate;
	}
	public void setExtCommDate(Object what) {
		setExtCommDate(what, false);
	}
	public void setExtCommDate(Object what, boolean rounded) {
		if (rounded)
			extCommDate.setRounded(what);
		else
			extCommDate.set(what);
	}	
	public PackedDecimalData getExtCessTerm() {
		return extCessTerm;
	}
	public void setExtCessTerm(Object what) {
		setExtCessTerm(what, false);
	}
	public void setExtCessTerm(Object what, boolean rounded) {
		if (rounded)
			extCessTerm.setRounded(what);
		else
			extCessTerm.set(what);
	}	
	public PackedDecimalData getExtCessDate() {
		return extCessDate;
	}
	public void setExtCessDate(Object what) {
		setExtCessDate(what, false);
	}
	public void setExtCessDate(Object what, boolean rounded) {
		if (rounded)
			extCessDate.setRounded(what);
		else
			extCessDate.set(what);
	}	
	public FixedLengthStringData getReasind() {
		return reasind;
	}
	public void setReasind(Object what) {
		reasind.set(what);
	}	
	public PackedDecimalData getZnadjperc() {
		return znadjperc;
	}
	public void setZnadjperc(Object what) {
		setZnadjperc(what, false);
	}
	
	public void setZnadjperc(Object what, boolean rounded) {
		if (rounded)
			znadjperc.setRounded(what);
		else
			znadjperc.set(what);
	}	
	public PackedDecimalData getPremadj() {
		return premadj;
	}
	public void setPremadj(Object what) {
		setPremadj(what, false);
	}
	public void setPremadj(Object what, boolean rounded) {
		if (rounded)
			premadj.setRounded(what);
		else
			premadj.set(what);
	}
	public PackedDecimalData getZmortpct() {
		return zmortpct;
	}
	public void setZmortpct(Object what) {
		setZmortpct(what, false);
	}
	public void setZmortpct(Object what, boolean rounded) {
		if (rounded)
			zmortpct.setRounded(what);
		else
			zmortpct.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		seqnbr.clear();
		tranno.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		validflag.clear();
		nonKeyFiller8.clear();
		currfrom.clear();
		currto.clear();
		opcda.clear();
		oppc.clear();
		insprm.clear();
		agerate.clear();
		jlife.clear();
		sbstdl.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		extCommDate.clear();
		extCessTerm.clear();
		extCessDate.clear();
		reasind.clear();
		znadjperc.clear();
		zmortpct.clear();
		premadj.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}