package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * @author gsaluja2
 *
 */
public class Sjl47ScreenVars extends SmartVarModel{
	private static final long serialVersionUID = 1L;
	public FixedLengthStringData dataArea = new FixedLengthStringData(236);
	public FixedLengthStringData dataFields = new FixedLengthStringData(60).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData lincsubr = DD.lincsubr.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,46);
	public FixedLengthStringData rectypes = new FixedLengthStringData(6).isAPartOf(dataFields, 51);
	public FixedLengthStringData[] rectype = FLSArrayPartOfStructure(3, 2, rectypes, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(rectypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData rectype01 = DD.lincrctyp.copy().isAPartOf(filler1,0);
	public FixedLengthStringData rectype02 = DD.lincrctyp.copy().isAPartOf(filler1,2);
	public FixedLengthStringData rectype03 = DD.lincrctyp.copy().isAPartOf(filler1,4);
	public FixedLengthStringData chgtypes = new FixedLengthStringData(3).isAPartOf(dataFields, 57);
	public FixedLengthStringData[] chgtype = FLSArrayPartOfStructure(3, 1, chgtypes, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(3).isAPartOf(chgtypes, 0, FILLER_REDEFINE);
	public FixedLengthStringData chgtype01 = DD.lincchgtyp.copy().isAPartOf(filler2,0);
	public FixedLengthStringData chgtype02 = DD.lincchgtyp.copy().isAPartOf(filler2,1);
	public FixedLengthStringData chgtype03 = DD.lincchgtyp.copy().isAPartOf(filler2,2);
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(44).isAPartOf(dataArea, 60);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData lincsubrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData rectypesErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData[] rectypeErr = FLSArrayPartOfStructure(3, 4, rectypesErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(rectypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rectype01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData rectype02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData rectype03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData chgtypesErr = new FixedLengthStringData(12).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData[] chgtypeErr = FLSArrayPartOfStructure(3, 4, chgtypesErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(12).isAPartOf(chgtypesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData chgtype01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData chgtype02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData chgtype03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(132).isAPartOf(dataArea, 104);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] lincsubrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData rectypesOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 60);
	public FixedLengthStringData[] rectypeOut = FLSArrayPartOfStructure(3, 12, rectypesOut, 0);
	public FixedLengthStringData[][] rectypeO = FLSDArrayPartOfArrayStructure(12, 1, rectypeOut, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(36).isAPartOf(rectypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rectype01Out = FLSArrayPartOfStructure(12, 1, filler5, 0);
	public FixedLengthStringData[] rectype02Out = FLSArrayPartOfStructure(12, 1, filler5, 12);
	public FixedLengthStringData[] rectype03Out = FLSArrayPartOfStructure(12, 1, filler5, 24);
	public FixedLengthStringData chgtypesOut = new FixedLengthStringData(36).isAPartOf(outputIndicators, 96);
	public FixedLengthStringData[] chgtypeOut = FLSArrayPartOfStructure(3, 12, chgtypesOut, 0);
	public FixedLengthStringData[][] chgtypeO = FLSDArrayPartOfArrayStructure(12, 1, chgtypeOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(36).isAPartOf(chgtypesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] chgtype01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] chgtype02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] chgtype03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sjl47screenWritten = new LongData(0);
	public LongData Sjl47protectWritten = new LongData(0);

	@Override
	public boolean hasSubfile() {
		return false;
	}

	public Sjl47ScreenVars() {
		super();
		initialiseScreenVars();
	}

	@Override
	protected void initialiseScreenVars() {
		fieldIndMap.put(lincsubrOut, new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, lincsubr, rectype01, rectype02, rectype03, chgtype01,
									   chgtype02, chgtype03};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, lincsubrOut, rectype01Out, 
											rectype02Out, rectype03Out, chgtype01Out, chgtype02Out, chgtype03Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, lincsubrErr, rectype01Err, rectype02Err,
										  rectype03Err, chgtype01Err, chgtype02Err, chgtype03Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sjl47screen.class;
		protectRecord = Sjl47protect.class;
	}
}
