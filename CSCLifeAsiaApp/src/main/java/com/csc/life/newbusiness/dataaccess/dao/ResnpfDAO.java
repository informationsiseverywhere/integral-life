package com.csc.life.newbusiness.dataaccess.dao;

import java.util.Map;

import com.csc.life.newbusiness.dataaccess.model.Resnpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ResnpfDAO extends BaseDAO<Resnpf>{
	public void insertResnpf(Resnpf resnpf);
	public Resnpf readResnpf(String chdrcoy,String chdrnum,String trancde);
	public boolean deleteResnpf(Resnpf resnpf) ;
	public int updateResnpf(Resnpf resnpf);//ILB-1057
	public void updtOrInstResnpf(Resnpf resnpf);//ILB-1057
	public Map<String, Resnpf> searchResnpfMap(String coy, String chdrnum);
}
