package com.csc.life.newbusiness.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class HpavpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 91;
	public FixedLengthStringData hpavrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData hpavpfRecord = hpavrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(hpavrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(hpavrec);
    public FixedLengthStringData linsname =  DD.linsname.copy().isAPartOf(hpavrec);
	public PackedDecimalData hoissdte = DD.hoissdte.copy().isAPartOf(hpavrec);
	public PackedDecimalData anetpre = DD.anetpre.copy().isAPartOf(hpavrec);
  	public PackedDecimalData asptdty = DD.astpdty.copy().isAPartOf(hpavrec);
  	public FixedLengthStringData curr = DD.curr.copy().isAPartOf(hpavrec);
  	
  	
  	public HpavpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for HpavpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public HpavpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for HpavpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpavpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for HpavpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public HpavpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("HPAVPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LINSNAME, " +
							"HOISSDTE, " +
						    "ANETPRE, " + 
						    "ASPTDTY, " +
							"CURR, " +
							"UNIQUE_NUMBER";
	}
	
	public void setColumns() {
		
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     linsname,
                                     hoissdte,
                                     anetpre,
                                     asptdty,
                                     curr,
                                     unique_number
		                 };
	}
	
	

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	
	
	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		hoissdte.clear();
  	    linsname.clear();
  	    anetpre.clear();
  	    asptdty.clear();
  	    curr.clear();
	}
	
	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getHpadrec() {
  		return hpavrec;
	}

	public FixedLengthStringData getHpadpfRecord() {
  		return hpavpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setHpadrec(what);
	}

	public void setHpadrec(Object what) {
  		this.hpavrec.set(what);
	}

	public void setHpadpfRecord(Object what) {
  		this.hpavpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(hpavrec.getLength());
		result.set(hpavrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}
