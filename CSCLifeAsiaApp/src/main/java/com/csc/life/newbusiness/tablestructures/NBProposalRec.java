package com.csc.life.newbusiness.tablestructures;

import java.util.ArrayList;
import java.util.List;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

public class NBProposalRec extends ExternalData {

	private String cnttype;
	private String screeId;
	private List<String> fieldName = new ArrayList<String>();
	private List<String> fieldValue = new ArrayList<String>();
	private int noField;
	private List<String> outputFieldName = new ArrayList<String>();
	private List<String> ouputErrorCode = new ArrayList<String>();
	private String crtable;
	

	public String getCnttype() {
		return cnttype;
	}

	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}

	public String getScreeId() {
		return screeId;
	}

	public void setScreeId(String screeId) {
		this.screeId = screeId;
	}

	public List<String> getFieldName() {
		return fieldName;
	}

	public void setFieldName(List<String> fieldName) {
		this.fieldName = fieldName;
	}

	public List<String> getFieldValue() {
		return fieldValue;
	}

	public void setFieldValue(List<String> fieldValue) {
		this.fieldValue = fieldValue;
	}

	public int getNoField() {
		return noField;
	}

	public void setNoField(int noField) {
		this.noField = noField;
	}

	public List<String> getOutputFieldName() {
		return outputFieldName;
	}

	public void setOutputFieldName(List<String> outputFieldName) {
		this.outputFieldName = outputFieldName;
	}

	public List<String> getOuputErrorCode() {
		return ouputErrorCode;
	}

	public void setOuputErrorCode(List<String> ouputErrorCode) {
		this.ouputErrorCode = ouputErrorCode;
	}

	
	public String getCrtable() {
		return crtable;
	}

	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}

	@Override
	public void initialize() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}
	
	

}
