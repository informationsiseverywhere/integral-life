/*
 * File: Screen Variables for Sr5bc
 * Date: 29 Nov 2016 6:45:10
 * Author: pmujavadiya
 * 
 * 
 * Copyright (2013) CSC Asia, all rights reserved.
 */

package com.csc.life.newbusiness.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;

public class Sr5bcScreenVars extends SmartVarModel{
	public FixedLengthStringData dataArea = new FixedLengthStringData(442);
	public FixedLengthStringData dataFields = new FixedLengthStringData(202).isAPartOf(dataArea, 0);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData mbrAccNum = DD.mbrAccNum.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData USIFndNum = DD.USIFndNum.copy().isAPartOf(dataFields,29);
	public ZonedDecimalData startdte = DD.datefrm.copyToZonedDecimal().isAPartOf(dataFields,40);
	public ZonedDecimalData taxFreeCompAmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,48);
	public ZonedDecimalData kiwiCompAmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,65);
	public ZonedDecimalData taxAmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,82);
	public ZonedDecimalData nonTaxAmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,99);
	public ZonedDecimalData presAmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,116);
	public ZonedDecimalData kiwiPresAmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,133);
	public ZonedDecimalData unRestrictAmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,150);
	public ZonedDecimalData restrictAmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,167);
	public ZonedDecimalData totTranAmt = DD.acntamt.copyToZonedDecimal().isAPartOf(dataFields,184);
	public FixedLengthStringData moneyInd = DD.keya.copy().isAPartOf(dataFields,201);

	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(60).isAPartOf(dataArea, 202);
	public FixedLengthStringData chdrnumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData chdrcoyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData mbrAccNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData USIFndNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData startdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData taxFreeCompAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData kiwiCompAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData taxAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData nonTaxAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData presAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData kiwiPresAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData unRestrictAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData restrictAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData totTranAmtErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData moneyIndErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(180).isAPartOf(dataArea,262);
	public FixedLengthStringData[] chdrnumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);	
	public FixedLengthStringData[] chdrcoyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);	
	public FixedLengthStringData[] mbrAccNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] USIFndNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] startdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] taxFreeCompAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] kiwiCompAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);	
	public FixedLengthStringData[] taxAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] nonTaxAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);	
	public FixedLengthStringData[] presAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);	
	public FixedLengthStringData[] kiwiPresAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);	
	public FixedLengthStringData[] unRestrictAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);	
	public FixedLengthStringData[] restrictAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);	
	public FixedLengthStringData[] totTranAmtOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);	
	public FixedLengthStringData[] moneyIndOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);	
		

	
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	
	public FixedLengthStringData startdteDisp = new FixedLengthStringData(10);
	
	public LongData Sr5bcscreenWritten = new LongData(0);
	public LongData Sr5bcprotectWritten = new LongData(0);

	public boolean hasSubfile() { 
		return false;
	}


	public Sr5bcScreenVars() {
		super();
		initialiseScreenVars();
	}

	
	protected void initialiseScreenVars() {
		
	//fieldIndMap.put(totamntOut,new String[] {"26","27","-26","28",null, null, null, null, null, null, null, null});
    fieldIndMap.put(totTranAmtOut,new String[] {null,"10",null, null, null, null, null, null, null, null, null, null, null});//ILIFE-7805
	screenFields = new BaseData[] {chdrnum,  chdrcoy,mbrAccNum ,USIFndNum ,startdte,taxFreeCompAmt,kiwiCompAmt ,taxAmt ,nonTaxAmt ,presAmt ,kiwiPresAmt ,unRestrictAmt,restrictAmt ,totTranAmt ,moneyInd};
	screenOutFields = new BaseData[][] {chdrnumOut,  chdrcoyOut,mbrAccNumOut,USIFndNumOut,startdteOut,taxFreeCompAmtOut,kiwiCompAmtOut,taxAmtOut,nonTaxAmtOut,presAmtOut,kiwiPresAmtOut,unRestrictAmtOut,restrictAmtOut,totTranAmtOut,moneyIndOut};
	screenErrFields = new BaseData[] {chdrnumErr, chdrcoyErr,mbrAccNumErr,USIFndNumErr,startdteErr,taxFreeCompAmtErr,kiwiCompAmtErr,taxAmtErr,nonTaxAmtErr,presAmtErr,kiwiPresAmtErr,unRestrictAmtErr,restrictAmtErr,totTranAmtErr,moneyIndErr};
	
	screenDateFields = new BaseData[] {startdte};
	screenDateErrFields = new BaseData[] {startdteErr};
	screenDateDispFields = new BaseData[] {startdteDisp};

	screenDataArea = dataArea;
	errorInds = errorIndicators;
	screenRecord = Sr5bcscreen.class;
	protectRecord = Sr5bcprotect.class;

	}

 


}
