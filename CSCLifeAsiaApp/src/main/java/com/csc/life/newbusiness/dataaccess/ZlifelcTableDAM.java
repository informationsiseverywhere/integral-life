package com.csc.life.newbusiness.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: ZlifelcTableDAM.java
 * Date: Sun, 30 Aug 2009 03:53:13
 * Class transformed from ZLIFELC.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class ZlifelcTableDAM extends LifepfTableDAM {

	public ZlifelcTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("ZLIFELC");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", LIFCNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFCNUM, " +
		            "VALIDFLAG, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "STATCODE, " +
		            "TRANNO, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "LCDTE, " +
		            "LIFEREL, " +
		            "CLTDOB, " +
		            "CLTSEX, " +
		            "ANBCCD, " +
		            "AGEADM, " +
		            "SELECTION, " +
		            "SMOKING, " +
		            "OCCUP, " +
		            "PURSUIT01, " +
		            "PURSUIT02, " +
		            "MARTAL, " +
		            "SBSTDL, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "LIFCNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "LIFCNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               lifcnum,
                               validflag,
                               life,
                               jlife,
                               statcode,
                               tranno,
                               currfrom,
                               currto,
                               lifeCommDate,
                               liferel,
                               cltdob,
                               cltsex,
                               anbAtCcd,
                               ageadm,
                               selection,
                               smoking,
                               occup,
                               pursuit01,
                               pursuit02,
                               maritalState,
                               sbstdl,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getLongHeader();
	}
	
	public FixedLengthStringData setHeader(Object what) {
		return setLongHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(247);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getLifcnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, lifcnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller30.setInternal(lifcnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(94);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ getChdrnum().toInternal()
					+ nonKeyFiller30.toInternal()
					+ getValidflag().toInternal()
					+ getLife().toInternal()
					+ getJlife().toInternal()
					+ getStatcode().toInternal()
					+ getTranno().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getLifeCommDate().toInternal()
					+ getLiferel().toInternal()
					+ getCltdob().toInternal()
					+ getCltsex().toInternal()
					+ getAnbAtCcd().toInternal()
					+ getAgeadm().toInternal()
					+ getSelection().toInternal()
					+ getSmoking().toInternal()
					+ getOccup().toInternal()
					+ getPursuit01().toInternal()
					+ getPursuit02().toInternal()
					+ getMaritalState().toInternal()
					+ getSbstdl().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, lifeCommDate);
			what = ExternalData.chop(what, liferel);
			what = ExternalData.chop(what, cltdob);
			what = ExternalData.chop(what, cltsex);
			what = ExternalData.chop(what, anbAtCcd);
			what = ExternalData.chop(what, ageadm);
			what = ExternalData.chop(what, selection);
			what = ExternalData.chop(what, smoking);
			what = ExternalData.chop(what, occup);
			what = ExternalData.chop(what, pursuit01);
			what = ExternalData.chop(what, pursuit02);
			what = ExternalData.chop(what, maritalState);
			what = ExternalData.chop(what, sbstdl);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);	
			what = ExternalData.chop(what, datime);	
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getLifcnum() {
		return lifcnum;
	}
	public void setLifcnum(Object what) {
		lifcnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getLifeCommDate() {
		return lifeCommDate;
	}
	public void setLifeCommDate(Object what) {
		setLifeCommDate(what, false);
	}
	public void setLifeCommDate(Object what, boolean rounded) {
		if (rounded)
			lifeCommDate.setRounded(what);
		else
			lifeCommDate.set(what);
	}	
	public FixedLengthStringData getLiferel() {
		return liferel;
	}
	public void setLiferel(Object what) {
		liferel.set(what);
	}	
	public PackedDecimalData getCltdob() {
		return cltdob;
	}
	public void setCltdob(Object what) {
		setCltdob(what, false);
	}
	public void setCltdob(Object what, boolean rounded) {
		if (rounded)
			cltdob.setRounded(what);
		else
			cltdob.set(what);
	}	
	public FixedLengthStringData getCltsex() {
		return cltsex;
	}
	public void setCltsex(Object what) {
		cltsex.set(what);
	}	
	public PackedDecimalData getAnbAtCcd() {
		return anbAtCcd;
	}
	public void setAnbAtCcd(Object what) {
		setAnbAtCcd(what, false);
	}
	public void setAnbAtCcd(Object what, boolean rounded) {
		if (rounded)
			anbAtCcd.setRounded(what);
		else
			anbAtCcd.set(what);
	}	
	public FixedLengthStringData getAgeadm() {
		return ageadm;
	}
	public void setAgeadm(Object what) {
		ageadm.set(what);
	}	
	public FixedLengthStringData getSelection() {
		return selection;
	}
	public void setSelection(Object what) {
		selection.set(what);
	}	
	public FixedLengthStringData getSmoking() {
		return smoking;
	}
	public void setSmoking(Object what) {
		smoking.set(what);
	}	
	public FixedLengthStringData getOccup() {
		return occup;
	}
	public void setOccup(Object what) {
		occup.set(what);
	}	
	public FixedLengthStringData getPursuit01() {
		return pursuit01;
	}
	public void setPursuit01(Object what) {
		pursuit01.set(what);
	}	
	public FixedLengthStringData getPursuit02() {
		return pursuit02;
	}
	public void setPursuit02(Object what) {
		pursuit02.set(what);
	}	
	public FixedLengthStringData getMaritalState() {
		return maritalState;
	}
	public void setMaritalState(Object what) {
		maritalState.set(what);
	}	
	public FixedLengthStringData getSbstdl() {
		return sbstdl;
	}
	public void setSbstdl(Object what) {
		sbstdl.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getPursuits() {
		return new FixedLengthStringData(pursuit01.toInternal()
										+ pursuit02.toInternal());
	}
	public void setPursuits(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getPursuits().getLength()).init(obj);
	
		what = ExternalData.chop(what, pursuit01);
		what = ExternalData.chop(what, pursuit02);
	}
	public FixedLengthStringData getPursuit(BaseData indx) {
		return getPursuit(indx.toInt());
	}
	public FixedLengthStringData getPursuit(int indx) {

		switch (indx) {
			case 1 : return pursuit01;
			case 2 : return pursuit02;
			default: return null; // Throw error instead?
		}
	
	}
	public void setPursuit(BaseData indx, Object what) {
		setPursuit(indx.toInt(), what);
	}
	public void setPursuit(int indx, Object what) {

		switch (indx) {
			case 1 : setPursuit01(what);
					 break;
			case 2 : setPursuit02(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		lifcnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		chdrnum.clear();
		nonKeyFiller30.clear();
		validflag.clear();
		life.clear();
		jlife.clear();
		statcode.clear();
		tranno.clear();
		currfrom.clear();
		currto.clear();
		lifeCommDate.clear();
		liferel.clear();
		cltdob.clear();
		cltsex.clear();
		anbAtCcd.clear();
		ageadm.clear();
		selection.clear();
		smoking.clear();
		occup.clear();
		pursuit01.clear();
		pursuit02.clear();
		maritalState.clear();
		sbstdl.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		datime.clear();
	}


}