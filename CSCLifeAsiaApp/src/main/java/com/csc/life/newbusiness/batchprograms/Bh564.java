/*
 * File: Bh564.java
 * Date: 29 August 2009 21:35:09
 * Author: Quipoz Limited
 * 
 * Class transformed from BH564.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon6;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon6rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.newbusiness.dataaccess.ZstrpfTableDAM;
import com.csc.life.newbusiness.recordstructures.Ph564par;
import com.csc.life.newbusiness.reports.Rh564Report;
import com.csc.life.newbusiness.tablestructures.Th565rec;
import com.csc.life.newbusiness.tablestructures.Th566rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   Monthly New Business Comparison Report
*
* This program produce a report on Monthy Basis to compare the
* figures (annualized Premium) of New Business Production on
* last 12 months follow this hierarchy
*
*  A Prompt Screen is Required.
*
*****************************************************************
* </pre>
*/
public class Bh564 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlzstrpf1rs = null;
	private java.sql.PreparedStatement sqlzstrpf1ps = null;
	private java.sql.Connection sqlzstrpf1conn = null;
	private String sqlzstrpf1 = "";
	private Rh564Report printerFile = new Rh564Report();
	private SortFileDAM sortFile = new SortFileDAM("RH564SRT");
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BH564");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private static final int tableSize = 350;
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(350);

	private FixedLengthStringData testCondition = new FixedLengthStringData(350).init("*", true);
	private FixedLengthStringData[] testChar = FLSArrayPartOfStructure(350, 1, testCondition, 0);

	private FixedLengthStringData filler1 = new FixedLengthStringData(350).isAPartOf(testCondition, 0, FILLER_REDEFINE);
	private FixedLengthStringData testCompany = new FixedLengthStringData(50).isAPartOf(filler1, 0);
	private FixedLengthStringData testDate = new FixedLengthStringData(50).isAPartOf(filler1, 50);
	private FixedLengthStringData testChannel = new FixedLengthStringData(50).isAPartOf(filler1, 100);
	private FixedLengthStringData testDistrict = new FixedLengthStringData(50).isAPartOf(filler1, 150);
	private FixedLengthStringData testProduct = new FixedLengthStringData(50).isAPartOf(filler1, 300);
	private FixedLengthStringData ap = new FixedLengthStringData(1).init("'");
	private static final String parenthesiso = "(";
	private static final String parenthesisc = ")";
	private static final String wsaaPercentage = "%";

		/* WSAA-PRODUCT-ARRAY */
	private FixedLengthStringData[] wsaaProductTotals = FLSInittedArray (13, 20);
	private PackedDecimalData[] wsaaProdAnnprem = PDArrayPartOfArrayStructure(11, 2, wsaaProductTotals, 0);
	private PackedDecimalData[] wsaaProdSingp = PDArrayPartOfArrayStructure(11, 2, wsaaProductTotals, 6);
	private PackedDecimalData[] wsaaProdAnnCnt = PDArrayPartOfArrayStructure(6, 0, wsaaProductTotals, 12);
	private PackedDecimalData[] wsaaProdSingCnt = PDArrayPartOfArrayStructure(6, 0, wsaaProductTotals, 16);

		/* WSAA-BRANCH-ARRAY */
	private FixedLengthStringData[] wsaaBranchTotals = FLSInittedArray (13, 20);
	private PackedDecimalData[] wsaaBrnAnnprem = PDArrayPartOfArrayStructure(11, 2, wsaaBranchTotals, 0);
	private PackedDecimalData[] wsaaBrnSingp = PDArrayPartOfArrayStructure(11, 2, wsaaBranchTotals, 6);
	private PackedDecimalData[] wsaaBrnAnnCnt = PDArrayPartOfArrayStructure(6, 0, wsaaBranchTotals, 12);
	private PackedDecimalData[] wsaaBrnSingCnt = PDArrayPartOfArrayStructure(6, 0, wsaaBranchTotals, 16);

		/* WSAA-DIVISION-ARRAY */
	private FixedLengthStringData[] wsaaDivisionTotals = FLSInittedArray (13, 20);
	private PackedDecimalData[] wsaaDivAnnprem = PDArrayPartOfArrayStructure(11, 2, wsaaDivisionTotals, 0);
	private PackedDecimalData[] wsaaDivSingp = PDArrayPartOfArrayStructure(11, 2, wsaaDivisionTotals, 6);
	private PackedDecimalData[] wsaaDivAnnCnt = PDArrayPartOfArrayStructure(6, 0, wsaaDivisionTotals, 12);
	private PackedDecimalData[] wsaaDivSingCnt = PDArrayPartOfArrayStructure(6, 0, wsaaDivisionTotals, 16);

		/* WSAA-DISTRICT-ARRAY */
	private FixedLengthStringData[] wsaaDistrictTotals = FLSInittedArray (13, 20);
	private PackedDecimalData[] wsaaDstAnnprem = PDArrayPartOfArrayStructure(11, 2, wsaaDistrictTotals, 0);
	private PackedDecimalData[] wsaaDstSingp = PDArrayPartOfArrayStructure(11, 2, wsaaDistrictTotals, 6);
	private PackedDecimalData[] wsaaDstAnnCnt = PDArrayPartOfArrayStructure(6, 0, wsaaDistrictTotals, 12);
	private PackedDecimalData[] wsaaDstSingCnt = PDArrayPartOfArrayStructure(6, 0, wsaaDistrictTotals, 16);

		/* WSAA-CHANNEL-ARRAY */
	private FixedLengthStringData[] wsaaChannelTotals = FLSInittedArray (13, 20);
	private PackedDecimalData[] wsaaChnlAnnprem = PDArrayPartOfArrayStructure(11, 2, wsaaChannelTotals, 0);
	private PackedDecimalData[] wsaaChnlSingp = PDArrayPartOfArrayStructure(11, 2, wsaaChannelTotals, 6);
	private PackedDecimalData[] wsaaChnlAnnCnt = PDArrayPartOfArrayStructure(6, 0, wsaaChannelTotals, 12);
	private PackedDecimalData[] wsaaChnlSingCnt = PDArrayPartOfArrayStructure(6, 0, wsaaChannelTotals, 16);

		/* WSAA-COMPANY-ARRAY */
	private FixedLengthStringData[] wsaaCompanyTotals = FLSInittedArray (13, 20);
	private PackedDecimalData[] wsaaCmpyAnnprem = PDArrayPartOfArrayStructure(11, 2, wsaaCompanyTotals, 0);
	private PackedDecimalData[] wsaaCmpySingp = PDArrayPartOfArrayStructure(11, 2, wsaaCompanyTotals, 6);
	private PackedDecimalData[] wsaaCmpyAnnCnt = PDArrayPartOfArrayStructure(6, 0, wsaaCompanyTotals, 12);
	private PackedDecimalData[] wsaaCmpySingCnt = PDArrayPartOfArrayStructure(6, 0, wsaaCompanyTotals, 16);
		/* WSAA-QTR-AMOUNTS */
	private PackedDecimalData wsaaQtrAnn = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaQtrAnnCnt = new ZonedDecimalData(6, 0);
	private PackedDecimalData wsaaQtrSingp = new PackedDecimalData(17, 2);
	private ZonedDecimalData wsaaQtrSingCnt = new ZonedDecimalData(6, 0);
		/* WSAA-TOTAL-PERCENT */
	private ZonedDecimalData wsaaAnnPrcnt = new ZonedDecimalData(5, 2);
	private ZonedDecimalData wsaaSingPrcnt = new ZonedDecimalData(5, 2);
	private ZonedDecimalData wsaaQtrAnnPct = new ZonedDecimalData(5, 2);
	private ZonedDecimalData wsaaQtrSingPct = new ZonedDecimalData(5, 2);

		/* WSAA-MONTH-ARRAY */
	private FixedLengthStringData[] wsaaMonths = FLSInittedArray (12, 22);
	private FixedLengthStringData[] wsaaMonthDesc = FLSDArrayPartOfArrayStructure(16, wsaaMonths, 0);
	private ZonedDecimalData[] wsaaMonth = ZDArrayPartOfArrayStructure(2, 0, wsaaMonths, 16, UNSIGNED_TRUE);
	private ZonedDecimalData[] wsaaYear = ZDArrayPartOfArrayStructure(4, 0, wsaaMonths, 18, UNSIGNED_TRUE);

	private FixedLengthStringData wsaaDatetext = new FixedLengthStringData(19);
	private FixedLengthStringData wsaaMonthYear = new FixedLengthStringData(14).isAPartOf(wsaaDatetext, 5);

	private FixedLengthStringData wsaaDatetexc = new FixedLengthStringData(22);
	private FixedLengthStringData wsaaHanYrMth = new FixedLengthStringData(16).isAPartOf(wsaaDatetexc, 0);
	private FixedLengthStringData wsaaHanDay = new FixedLengthStringData(6).isAPartOf(wsaaDatetexc, 16);

	private FixedLengthStringData wsaaDaysInMonths = new FixedLengthStringData(24).init("312831303130313130313031");
	private ZonedDecimalData[] wsaaDays = ZDArrayPartOfStructure(12, 2, 0, wsaaDaysInMonths, 0, UNSIGNED_TRUE);
	private ZonedDecimalData wsaaEndDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(wsaaEndDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaEndYear = new ZonedDecimalData(4, 0).isAPartOf(filler3, 0).setUnsigned();
	private ZonedDecimalData wsaaEndMonth = new ZonedDecimalData(2, 0).isAPartOf(filler3, 4).setUnsigned();
	private ZonedDecimalData wsaaEndDay = new ZonedDecimalData(2, 0).isAPartOf(filler3, 6).setUnsigned();
	private ZonedDecimalData wsaaStartDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaStartDate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaStartDay = new ZonedDecimalData(2, 0).isAPartOf(filler4, 6).setUnsigned();
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(wsaaEffdate, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaEffYear = new ZonedDecimalData(4, 0).isAPartOf(filler5, 0).setUnsigned();
	private ZonedDecimalData wsaaEffMonth = new ZonedDecimalData(2, 0).isAPartOf(filler5, 4).setUnsigned();
	private ZonedDecimalData wsaaDateArray = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler6 = new FixedLengthStringData(8).isAPartOf(wsaaDateArray, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaDateYear = new ZonedDecimalData(4, 0).isAPartOf(filler6, 0).setUnsigned();
	private ZonedDecimalData wsaaDateMonth = new ZonedDecimalData(2, 0).isAPartOf(filler6, 4).setUnsigned();
		/* WSAA-AMOUNTS */
	private PackedDecimalData wsaaSingp = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(11, 2);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(11, 2);

	private FixedLengthStringData wsaaSqlBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaSqlBillfreq, 0).setUnsigned();
		/* WSAA-LEAP-YEAR-CHECKS */
	private ZonedDecimalData wsaaChkYear = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaQuotient = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaRemainder = new ZonedDecimalData(4, 0).setUnsigned();
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaIy = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaLeapYear = new FixedLengthStringData(1).init("N");
	private Validator leapYear = new Validator(wsaaLeapYear, "Y");
		/* WSAA-PREVIOUS-KEYS */
	private FixedLengthStringData wsaaPrevCnttype = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaPrevAracde = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaPrevBranch = new FixedLengthStringData(2);
		/* WSAA-OUTPUT-FIELDS */
	private FixedLengthStringData wsaaCompanynm = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaCurrencynm = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaSdate = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaRepdate = new FixedLengthStringData(22);
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
	private static final String t5688 = "T5688";
	private static final String t5696 = "T5696";
	private static final String th565 = "TH565";
	private static final String th566 = "TH566";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler8 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler8, 5);

		/*  Control indicators                                             */
	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");
	private String wsaaEndOfSort = "N";
	private String wsaaFirstRec = "";
	private String wsaaOpenedFile = "";
	private ZonedDecimalData wsaaCtrlbrk = new ZonedDecimalData(1, 0).setUnsigned();

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon6rec datcon6rec = new Datcon6rec();
	private Ph564par ph564par = new Ph564par();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private Th565rec th565rec = new Th565rec();
	private Th566rec th566rec = new Th566rec();
	private PrinterRecInner printerRecInner = new PrinterRecInner();
	private SortRecInner sortRecInner = new SortRecInner();
	private SqlZstrpfInner sqlZstrpfInner = new SqlZstrpfInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		endOfFile2118, 
		exit2119
	}

	public Bh564() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		syserrrec.syserrStatuz.set(sqlStatuz);
		fatalError600();
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Place any additional restart processing in here.                */
		/*EXIT*/
	}

protected void initialise1000()
	{
		try {
			initialise1010();
			setUpHeadingCompany1020();
			setUpHeadingDates1040();
			readTh5651050();
			setUpHeadingCurrency1051();
			initialiseWsaa1052();
			reportDates1053();
			testConditions1055();
			defineCursor1060();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void initialise1010()
	{
		wsaaOpenedFile = "N";
		ph564par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(ph564par.ctrlbrk01, "N")
		&& isEQ(ph564par.ctrlbrk02, "N")
		&& isEQ(ph564par.ctrlbrk03, "N")) {
			wsspEdterror.set(varcom.endp);
			goTo(GotoLabel.exit1090);
		}
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		wsaaFirstRec = "Y";
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaCompanynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaSdate.set(datcon1rec.extDate);
	}

protected void readTh5651050()
	{
		/*  Read TH565 for valid transaction codes, base currency and      */
		/*  periods.                                                       */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th565);
		itemIO.setItemitem(bprdIO.getSystemParam01());
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th565rec.th565Rec.set(itemIO.getGenarea());
	}

protected void setUpHeadingCurrency1051()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(th565rec.currcode);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		wsaaCurrencynm.set(descIO.getLongdesc());
	}

protected void initialiseWsaa1052()
	{
		/*  Initialise working storage fields                              */
		for (wsaaIx.set(1); !(isGT(wsaaIx, 13)); wsaaIx.add(1)){
			wsaaProdAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaProdSingp[wsaaIx.toInt()].set(ZERO);
			wsaaProdAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaProdSingCnt[wsaaIx.toInt()].set(ZERO);
			wsaaBrnAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaBrnSingp[wsaaIx.toInt()].set(ZERO);
			wsaaBrnAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaBrnSingCnt[wsaaIx.toInt()].set(ZERO);
			wsaaDivAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaDivSingp[wsaaIx.toInt()].set(ZERO);
			wsaaDivAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaDivSingCnt[wsaaIx.toInt()].set(ZERO);
			wsaaDstAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaDstSingp[wsaaIx.toInt()].set(ZERO);
			wsaaDstAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaDstSingCnt[wsaaIx.toInt()].set(ZERO);
			wsaaChnlAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaChnlSingp[wsaaIx.toInt()].set(ZERO);
			wsaaChnlAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaChnlSingCnt[wsaaIx.toInt()].set(ZERO);
			wsaaCmpyAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaCmpySingp[wsaaIx.toInt()].set(ZERO);
			wsaaCmpyAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaCmpySingCnt[wsaaIx.toInt()].set(ZERO);
		}
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			wsaaMonth[wsaaIx.toInt()].set(ZERO);
			wsaaYear[wsaaIx.toInt()].set(ZERO);
		}
		wsaaEndDate.set(ZERO);
		wsaaStartDate.set(ZERO);
		wsaaQuotient.set(ZERO);
		wsaaRemainder.set(ZERO);
		wsaaChkYear.set(ZERO);
		wsaaIx.set(ZERO);
		wsaaIy.set(ZERO);
		wsaaSingp.set(ZERO);
		wsaaInstprem.set(ZERO);
		wsaaSumins.set(ZERO);
		wsaaQtrAnn.set(ZERO);
		wsaaQtrAnnCnt.set(ZERO);
		wsaaQtrSingp.set(ZERO);
		wsaaQtrSingCnt.set(ZERO);
		wsaaAnnPrcnt.set(ZERO);
		wsaaSingPrcnt.set(ZERO);
		wsaaQtrAnnPct.set(ZERO);
		wsaaQtrSingPct.set(ZERO);
		wsaaCtrlbrk.set(ZERO);
	}

protected void reportDates1053()
	{
		/*  Work out the date of report, this is the last day of the       */
		/*  previous month (from BSSC-EFFECTIVE-DATE).  First, we call     */
		/*  DATCON2 to get subtract BSSC-EFFECTIVE-DATE by one month,      */
		/*  next we obtain the date of report by replacing the day         */
		/*  portion by the number of days in that month.                   */
		/*  Report date should now be the last day of the current          */
		/*  month.                                                         */
		wsaaEndDate.set(bsscIO.getEffectiveDate());
		/*    If WSAA-END-DATE falls on a February, we need to check if    */
		/*    this is a leap year.                                         */
		if (isEQ(wsaaEndMonth, 2)) {
			wsaaChkYear.set(wsaaEndYear);
			checkLeapYear1100();
			if (leapYear.isTrue()) {
				wsaaDays[2].set(29);
			}
		}
		wsaaEndDay.set(wsaaDays[wsaaEndMonth.toInt()]);
		/*  Format date of report using DATCON6.                           */
		datcon6rec.language.set(bsscIO.getLanguage());
		datcon6rec.company.set(bsprIO.getCompany());
		datcon6rec.intDate1.set(wsaaEndDate);
		callProgram(Datcon6.class, datcon6rec.datcon6Rec);
		wsaaRepdate.set(datcon6rec.intDate2);
		/*  Obtain the start date of the report by adding one year to      */
		/*  the report date and replace the day part by 01.                */
		/*  Add 1 month from the end-date.                                 */
		wsaaStartDate.set(wsaaEndDate);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaStartDate);
		datcon2rec.frequency.set("12");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaStartDate.set(datcon2rec.intDate2);
		/*  Set to first day of the month.                                 */
		wsaaStartDay.set(1);
		/*  Subtract 1 year from this date.                                */
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(wsaaStartDate);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaStartDate.set(datcon2rec.intDate2);
		/*  Load the report month array.                                   */
		for (wsaaIx.set(12); !(isEQ(wsaaIx, 0)); wsaaIx.add(-1)){
			datcon6rec.language.set(bsscIO.getLanguage());
			datcon6rec.company.set(bsprIO.getCompany());
			datcon6rec.intDate1.set(datcon2rec.intDate2);
			wsaaDateArray.set(datcon2rec.intDate2);
			callProgram(Datcon6.class, datcon6rec.datcon6Rec);
			if (isEQ(datcon6rec.language, "S")
			|| isEQ(datcon6rec.language, "C")) {
				/* for Chinese - use Year Month format                             */
				wsaaDatetexc.set(datcon6rec.intDate2);
				wsaaMonthDesc[wsaaIx.toInt()].set(wsaaHanYrMth);
			}
			else {
				wsaaDatetext.set(datcon6rec.intDate2);
				wsaaMonthDesc[wsaaIx.toInt()].set(wsaaMonthYear);
			}
			wsaaMonth[wsaaIx.toInt()].set(wsaaDateMonth);
			wsaaYear[wsaaIx.toInt()].set(wsaaDateYear);
			datcon2rec.intDate1.set(datcon2rec.intDate2);
			datcon2rec.frequency.set("12");
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		}
	}

protected void testConditions1055()
	{
		/*  Set up SQL test condition.                                     */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("CHDRCOY = ");
		stringVariable1.addExpression(ap);
		stringVariable1.addExpression(bsprIO.getCompany());
		stringVariable1.addExpression(ap);
		stringVariable1.setStringInto(testCompany);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(" AND ");
		stringVariable2.addExpression("EFFDATE BETWEEN ");
		stringVariable2.addExpression(wsaaStartDate);
		stringVariable2.addExpression(" AND ");
		stringVariable2.addExpression(wsaaEndDate);
		stringVariable2.setStringInto(testDate);
		if (isNE(ph564par.branch, SPACES)) {
			StringUtil stringVariable3 = new StringUtil();
			stringVariable3.addExpression(" AND ");
			stringVariable3.addExpression("CNTBRANCH = ");
			stringVariable3.addExpression(ap);
			stringVariable3.addExpression(ph564par.branch);
			stringVariable3.addExpression(ap);
			stringVariable3.setStringInto(testChannel);
		}
		if (isNE(ph564par.aracde, SPACES)) {
			StringUtil stringVariable4 = new StringUtil();
			stringVariable4.addExpression(" AND ");
			stringVariable4.addExpression("ARACDE = ");
			stringVariable4.addExpression(ap);
			stringVariable4.addExpression(ph564par.aracde);
			stringVariable4.addExpression(ap);
			stringVariable4.setStringInto(testDistrict);
		}
		if (isNE(ph564par.chdrtype, SPACES)) {
			StringUtil stringVariable5 = new StringUtil();
			stringVariable5.addExpression(" AND ");
			stringVariable5.addExpression("CNTTYPE = ");
			stringVariable5.addExpression(ap);
			stringVariable5.addExpression(ph564par.chdrtype);
			stringVariable5.addExpression(ap);
			stringVariable5.setStringInto(testProduct);
		}
		/*  Remove all '*' from the test condition.                        */
		wsaaIy.set(1);
		for (wsaaIx.set(1); !(isGT(wsaaIx, tableSize)); wsaaIx.add(1)){
			if (isNE(testChar[wsaaIx.toInt()], "*")) {
				testChar[wsaaIy.toInt()].set(testChar[wsaaIx.toInt()]);
				wsaaIy.add(1);
			}
		}
		for (wsaaIx.set(wsaaIy); !(isGT(wsaaIx, tableSize)); wsaaIx.add(1)){
			testChar[wsaaIx.toInt()].set("*");
		}
		StringUtil stringVariable6 = new StringUtil();
		stringVariable6.addExpression("SELECT CNTBRANCH, ");
		stringVariable6.addExpression("ARACDE, ");
		stringVariable6.addExpression("CNTTYPE, ");
		stringVariable6.addExpression("CHDRNUM, ");
		stringVariable6.addExpression("CNTCURR, ");
		stringVariable6.addExpression("SINGP, ");
		stringVariable6.addExpression("INSTPREM, ");
		stringVariable6.addExpression("SUMINS, ");
		stringVariable6.addExpression("EFFDATE, ");
		stringVariable6.addExpression("BILLFREQ, ");
		stringVariable6.addExpression("BATCTRCDE, ");
		stringVariable6.addExpression("CNTFEE, ");
		stringVariable6.addExpression("OCCDATE FROM ZSTRPF ");
		stringVariable6.addExpression("WHERE ");
		stringVariable6.addExpression(testCondition, "*");
		stringVariable6.setStringInto(wsaaQcmdexc);
	}

protected void defineCursor1060()
	{
		/*  Prepare cursor for query                                       */
		/*  Define the query required by declaring a cursor                */
		sqlzstrpf1 = wsaaQcmdexc.toString();
		/*   Open the cursor (this runs the query)                         */
		sqlerrorflag = false;
		try {
			sqlzstrpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new ZstrpfTableDAM());
			sqlzstrpf1ps = getAppVars().prepareStatementEmbeded(sqlzstrpf1conn, sqlzstrpf1);
			sqlzstrpf1rs = getAppVars().executeQuery(sqlzstrpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		wsaaOpenedFile = "Y";
	}

protected void checkLeapYear1100()
	{
		chkLeapYr1100();
	}

protected void chkLeapYr1100()
	{
		wsaaLeapYear.set("N");
		compute(wsaaQuotient, 0).setDivide(wsaaChkYear, (4));
		wsaaRemainder.setRemainder(wsaaQuotient);
		if (isEQ(wsaaRemainder, 0)) {
			compute(wsaaQuotient, 0).setDivide(wsaaChkYear, (400));
			wsaaRemainder.setRemainder(wsaaQuotient);
			if (isEQ(wsaaRemainder, 0)) {
				wsaaLeapYear.set("Y");
			}
			else {
				compute(wsaaQuotient, 0).setDivide(wsaaChkYear, (100));
				wsaaRemainder.setRemainder(wsaaQuotient);
				if (isEQ(wsaaRemainder, 0)) {
					wsaaLeapYear.set("N");
				}
				else {
					wsaaLeapYear.set("Y");
				}
			}
		}
		else {
			wsaaLeapYear.set("N");
		}
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		sortFile.openOutput();
		releaseData2100();
		sortFile.close();
		FileSort fs1 = new FileSort(sortFile);
		fs1.addSortKey(sortRecInner.sortCntbranch, true);
		fs1.addSortKey(sortRecInner.sortAracde, true);
		fs1.addSortKey(sortRecInner.sortCnttype, true);
		fs1.sort();
		sortFile.openInput();
		retrieveData2200();
		sortFile.close();
		wsspEdterror.set(varcom.endp);
		/*EXIT*/
	}

protected void releaseData2100()
	{
		/*RELEASE*/
		wsaaEndOfSort = "N";
		while ( !(isEQ(wsaaEndOfSort, "Y"))) {
			releaseSort2110();
		}
		
		/*EXIT*/
	}

protected void releaseSort2110()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					relSort2110();
				case endOfFile2118: 
					endOfFile2118();
				case exit2119: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void relSort2110()
	{
		/*   Fetch record                                                  */
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlzstrpf1rs)) {
				getAppVars().getDBObject(sqlzstrpf1rs, 1, sqlZstrpfInner.sqlCntbranch);
				getAppVars().getDBObject(sqlzstrpf1rs, 2, sqlZstrpfInner.sqlAracde);
				getAppVars().getDBObject(sqlzstrpf1rs, 3, sqlZstrpfInner.sqlCnttype);
				getAppVars().getDBObject(sqlzstrpf1rs, 4, sqlZstrpfInner.sqlChdrnum);
				getAppVars().getDBObject(sqlzstrpf1rs, 5, sqlZstrpfInner.sqlCntcurr);
				getAppVars().getDBObject(sqlzstrpf1rs, 6, sqlZstrpfInner.sqlSingp);
				getAppVars().getDBObject(sqlzstrpf1rs, 7, sqlZstrpfInner.sqlInstprem);
				getAppVars().getDBObject(sqlzstrpf1rs, 8, sqlZstrpfInner.sqlSumins);
				getAppVars().getDBObject(sqlzstrpf1rs, 9, sqlZstrpfInner.sqlEffdate);
				getAppVars().getDBObject(sqlzstrpf1rs, 10, sqlZstrpfInner.sqlBillfreq);
				getAppVars().getDBObject(sqlzstrpf1rs, 11, sqlZstrpfInner.sqlBatctrcde);
				getAppVars().getDBObject(sqlzstrpf1rs, 12, sqlZstrpfInner.sqlCntfee);
				getAppVars().getDBObject(sqlzstrpf1rs, 13, sqlZstrpfInner.sqlOccdate);
			}
			else {
				goTo(GotoLabel.endOfFile2118);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  Convert all amounts to TH565 currency.                         */
		wsaaFirstRec = "N";
		if (isNE(sqlZstrpfInner.sqlCntcurr, th565rec.currcode)) {
			initialize(conlinkrec.clnk002Rec);
			conlinkrec.currIn.set(sqlZstrpfInner.sqlCntcurr);
			if (isNE(sqlZstrpfInner.sqlSingp, ZERO)) {
				compute(conlinkrec.amountIn, 2).set(add(sqlZstrpfInner.sqlSingp, sqlZstrpfInner.sqlCntfee));
				currencyConversion2120();
				wsaaSingp.set(conlinkrec.amountOut);
			}
			else {
				wsaaSingp.set(ZERO);
			}
			if (isNE(sqlZstrpfInner.sqlInstprem, ZERO)) {
				compute(conlinkrec.amountIn, 2).set(add(sqlZstrpfInner.sqlInstprem, sqlZstrpfInner.sqlCntfee));
				currencyConversion2120();
				wsaaInstprem.set(conlinkrec.amountOut);
			}
			else {
				wsaaInstprem.set(ZERO);
			}
			if (isNE(sqlZstrpfInner.sqlSumins, ZERO)) {
				conlinkrec.amountIn.set(sqlZstrpfInner.sqlSumins);
				currencyConversion2120();
				wsaaSumins.set(conlinkrec.amountOut);
			}
			else {
				wsaaInstprem.set(ZERO);
			}
		}
		else {
			if (isNE(sqlZstrpfInner.sqlSingp, ZERO)) {
				compute(wsaaSingp, 2).set(add(sqlZstrpfInner.sqlSingp, sqlZstrpfInner.sqlCntfee));
			}
			else {
				wsaaSingp.set(ZERO);
			}
			if (isNE(sqlZstrpfInner.sqlInstprem, ZERO)) {
				compute(wsaaInstprem, 2).set(add(sqlZstrpfInner.sqlInstprem, sqlZstrpfInner.sqlCntfee));
			}
			else {
				wsaaInstprem.set(ZERO);
			}
			if (isNE(sqlZstrpfInner.sqlSumins, ZERO)) {
				wsaaSumins.set(sqlZstrpfInner.sqlSumins);
			}
			else {
				wsaaSumins.set(ZERO);
			}
		}
		/*  Read TH566 to obtain the policy count.                         */
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItemitem(sqlZstrpfInner.sqlBatctrcde);
		readTh5662400();
		wsaaSqlBillfreq.set(sqlZstrpfInner.sqlBillfreq);
		compute(sortRecInner.sortAnnprem, 3).setRounded(mult(wsaaInstprem, wsaaBillfreq));
		wsaaEffdate.set(sqlZstrpfInner.sqlEffdate);
		sortRecInner.sortYear.set(wsaaEffYear);
		sortRecInner.sortMnth.set(wsaaEffMonth);
		/*  Add company total for the correct month/year slot              */
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(wsaaMonth[wsaaIx.toInt()], sortRecInner.sortMnth)
			&& isEQ(wsaaYear[wsaaIx.toInt()], sortRecInner.sortYear)) {
				wsaaCmpySingp[wsaaIx.toInt()].add(wsaaSingp);
				wsaaCmpyAnnprem[wsaaIx.toInt()].add(sortRecInner.sortAnnprem);
				if (isEQ(sqlZstrpfInner.sqlBatctrcde, th565rec.ztrcde01)) {
					if (isEQ(sqlZstrpfInner.sqlBillfreq, "00")) {
						wsaaCmpySingCnt[wsaaIx.toInt()].add(th566rec.cnt);
					}
					else {
						wsaaCmpyAnnCnt[wsaaIx.toInt()].add(th566rec.cnt);
					}
				}
				wsaaIx.set(13);
			}
		}
		sortRecInner.sortCntbranch.set(sqlZstrpfInner.sqlCntbranch);
		sortRecInner.sortAracde.set(sqlZstrpfInner.sqlAracde);
		sortRecInner.sortCnttype.set(sqlZstrpfInner.sqlCnttype);
		sortRecInner.sortChdrnum.set(sqlZstrpfInner.sqlChdrnum);
		sortRecInner.sortBatctrcde.set(sqlZstrpfInner.sqlBatctrcde);
		sortRecInner.sortSingp.set(wsaaSingp);
		sortRecInner.sortSumins.set(wsaaSumins);
		sortRecInner.sortBillfreq.set(sqlZstrpfInner.sqlBillfreq);
		sortFile.write(sortRecInner.sortRec);
		goTo(GotoLabel.exit2119);
	}

protected void endOfFile2118()
	{
		wsaaEndOfSort = "Y";
	}

protected void currencyConversion2120()
	{
		currConvert2120();
	}

protected void currConvert2120()
	{
		conlinkrec.function.set("CVRT");
		conlinkrec.currOut.set(th565rec.currcode);
		conlinkrec.cashdate.set(sqlZstrpfInner.sqlOccdate);
		conlinkrec.company.set(bsprIO.getCompany());
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(conlinkrec.statuz);
			syserrrec.params.set(conlinkrec.clnk002Rec);
			fatalError600();
		}
		/* MOVE CLNK-AMOUNT-OUT        TO ZRDP-AMOUNT-IN.               */
		/* PERFORM A000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT        TO CLNK-AMOUNT-OUT.              */
		if (isNE(conlinkrec.amountOut, 0)) {
			zrdecplrec.amountIn.set(conlinkrec.amountOut);
			a000CallRounding();
			conlinkrec.amountOut.set(zrdecplrec.amountOut);
		}
	}

protected void retrieveData2200()
	{
		/*RETRV-DATA*/
		if (isEQ(wsaaFirstRec, "Y")
		&& isEQ(wsaaEndOfSort, "Y")) {
			return ;
		}
		wsaaFirstRec = "Y";
		wsaaEndOfSort = "N";
		while ( !(isEQ(wsaaEndOfSort, "Y"))) {
			returnSort2210();
		}
		
		/*EXIT*/
	}

protected void returnSort2210()
	{
		retSort2210();
	}

protected void retSort2210()
	{
		sortFile.read(sortRecInner.sortRec);
		if (sortFile.isAtEnd()) {
			wsaaEndOfSort = "Y";
		}
		if (isEQ(wsaaEndOfSort, "Y")) {
			sortRecInner.sortCntbranch.set(SPACES);
		}
		if (isEQ(wsaaFirstRec, "Y")) {
			wsaaPrevCnttype.set(sortRecInner.sortCnttype);
			wsaaPrevAracde.set(sortRecInner.sortAracde);
			wsaaPrevBranch.set(sortRecInner.sortCntbranch);
		}
		if (isNE(sortRecInner.sortCntbranch, wsaaPrevBranch)
		&& isEQ(wsaaFirstRec, "N")) {
			if (isEQ(ph564par.ctrlbrk03, "Y")) {
				printProductTotals2300();
			}
			if (isEQ(ph564par.ctrlbrk02, "Y")) {
				printDistrictTotals2330();
			}
			if (isEQ(ph564par.ctrlbrk01, "Y")) {
				printChannelTotals2340();
			}
			if (isEQ(wsaaEndOfSort, "Y")) {
				printCompanyTotals2350();
			}
			wsaaPrevCnttype.set(sortRecInner.sortCnttype);
			wsaaPrevAracde.set(sortRecInner.sortAracde);
			wsaaPrevBranch.set(sortRecInner.sortCntbranch);
		}
		else {
			if (isNE(sortRecInner.sortAracde, wsaaPrevAracde)
			&& isEQ(wsaaFirstRec, "N")) {
				if (isEQ(ph564par.ctrlbrk03, "Y")) {
					printProductTotals2300();
				}
				if (isEQ(ph564par.ctrlbrk02, "Y")) {
					printDistrictTotals2330();
				}
				wsaaPrevCnttype.set(sortRecInner.sortCnttype);
				wsaaPrevAracde.set(sortRecInner.sortAracde);
			}
			else {
				if (isEQ(ph564par.ctrlbrk03, "Y")
				&& isEQ(wsaaFirstRec, "N")
				&& isNE(sortRecInner.sortCnttype, wsaaPrevCnttype)) {
					printProductTotals2300();
					wsaaPrevCnttype.set(sortRecInner.sortCnttype);
				}
			}
		}
		wsaaFirstRec = "N";
		/*  Read TH566 to obtain the policy count.                         */
		itemIO.setDataKey(SPACES);
		itemIO.setStatuz(varcom.oK);
		itemIO.setItemitem(sortRecInner.sortBatctrcde);
		readTh5662400();
		/*  Add premiums to the correct month/year slot                    */
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(wsaaMonth[wsaaIx.toInt()], sortRecInner.sortMnth)
			&& isEQ(wsaaYear[wsaaIx.toInt()], sortRecInner.sortYear)) {
				wsaaProdSingp[wsaaIx.toInt()].add(sortRecInner.sortSingp);
				wsaaBrnSingp[wsaaIx.toInt()].add(sortRecInner.sortSingp);
				wsaaDivSingp[wsaaIx.toInt()].add(sortRecInner.sortSingp);
				wsaaDstSingp[wsaaIx.toInt()].add(sortRecInner.sortSingp);
				wsaaChnlSingp[wsaaIx.toInt()].add(sortRecInner.sortSingp);
				wsaaProdAnnprem[wsaaIx.toInt()].add(sortRecInner.sortAnnprem);
				wsaaBrnAnnprem[wsaaIx.toInt()].add(sortRecInner.sortAnnprem);
				wsaaDivAnnprem[wsaaIx.toInt()].add(sortRecInner.sortAnnprem);
				wsaaDstAnnprem[wsaaIx.toInt()].add(sortRecInner.sortAnnprem);
				wsaaChnlAnnprem[wsaaIx.toInt()].add(sortRecInner.sortAnnprem);
				if (isEQ(sortRecInner.sortBatctrcde, th565rec.ztrcde01)) {
					if (isEQ(sortRecInner.sortBillfreq, "00")) {
						wsaaProdSingCnt[wsaaIx.toInt()].add(th566rec.cnt);
						wsaaBrnSingCnt[wsaaIx.toInt()].add(th566rec.cnt);
						wsaaDivSingCnt[wsaaIx.toInt()].add(th566rec.cnt);
						wsaaDstSingCnt[wsaaIx.toInt()].add(th566rec.cnt);
						wsaaChnlSingCnt[wsaaIx.toInt()].add(th566rec.cnt);
					}
					else {
						wsaaProdAnnCnt[wsaaIx.toInt()].add(th566rec.cnt);
						wsaaBrnAnnCnt[wsaaIx.toInt()].add(th566rec.cnt);
						wsaaDivAnnCnt[wsaaIx.toInt()].add(th566rec.cnt);
						wsaaDstAnnCnt[wsaaIx.toInt()].add(th566rec.cnt);
						wsaaChnlAnnCnt[wsaaIx.toInt()].add(th566rec.cnt);
					}
				}
				wsaaIx.set(13);
			}
		}
	}

protected void printProductTotals2300()
	{
		printProdTot2300();
	}

protected void printProdTot2300()
	{
		/*  Print page heading.                                            */
		wsaaCtrlbrk.set(5);
		pageHeading2480();
		/*  Write detail, checking for page overflow                       */
		wsaaAnnPrcnt.set(ZERO);
		wsaaSingPrcnt.set(ZERO);
		wsaaQtrAnnPct.set(ZERO);
		wsaaQtrSingPct.set(ZERO);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			printerRecInner.printerRec.set(SPACES);
			printerRecInner.instprem01.set(ZERO);
			printerRecInner.instprem02.set(ZERO);
			printerRecInner.singp01.set(ZERO);
			printerRecInner.singp02.set(ZERO);
			printerRecInner.tgtpcnt01.set(ZERO);
			printerRecInner.tgtpcnt02.set(ZERO);
			printerRecInner.tgtpcnt03.set(ZERO);
			printerRecInner.tgtpcnt04.set(ZERO);
			printerRecInner.cntcount01.set(ZERO);
			printerRecInner.cntcount02.set(ZERO);
			printerRecInner.cntcount03.set(ZERO);
			printerRecInner.cntcount04.set(ZERO);
			printerRecInner.datetext01.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.instprem01.set(wsaaProdAnnprem[wsaaIx.toInt()]);
			compute(printerRecInner.tgtpcnt01, 3).setRounded(div((mult(wsaaProdAnnprem[wsaaIx.toInt()], 100)), wsaaCmpyAnnprem[wsaaIx.toInt()]));
			if (isNE(printerRecInner.tgtpcnt01, ZERO)) {
				printerRecInner.pcamtind01.set(wsaaPercentage);
			}
			printerRecInner.cntcount01.set(wsaaProdAnnCnt[wsaaIx.toInt()]);
			if (isNE(wsaaProdSingp[wsaaIx.toInt()], ZERO)) {
				printerRecInner.zparnths01.set(parenthesiso);
				printerRecInner.singp01.set(wsaaProdSingp[wsaaIx.toInt()]);
				printerRecInner.zparnths02.set(parenthesisc);
				compute(printerRecInner.tgtpcnt03, 3).setRounded(div((mult(wsaaProdSingp[wsaaIx.toInt()], 100)), wsaaCmpySingp[wsaaIx.toInt()]));
				if (isNE(printerRecInner.tgtpcnt03, ZERO)) {
					printerRecInner.pcamtind03.set(wsaaPercentage);
				}
				printerRecInner.cntcount03.set(wsaaProdSingCnt[wsaaIx.toInt()]);
			}
			wsaaQtrAnn.add(wsaaProdAnnprem[wsaaIx.toInt()]);
			wsaaProdAnnprem[13].add(wsaaProdAnnprem[wsaaIx.toInt()]);
			wsaaQtrAnnCnt.add(wsaaProdAnnCnt[wsaaIx.toInt()]);
			wsaaProdAnnCnt[13].add(wsaaProdAnnCnt[wsaaIx.toInt()]);
			wsaaQtrSingp.add(wsaaProdSingp[wsaaIx.toInt()]);
			wsaaProdSingp[13].add(wsaaProdSingp[wsaaIx.toInt()]);
			wsaaQtrSingCnt.add(wsaaProdSingCnt[wsaaIx.toInt()]);
			wsaaProdSingCnt[13].add(wsaaProdSingCnt[wsaaIx.toInt()]);
			wsaaAnnPrcnt.add(printerRecInner.tgtpcnt01);
			wsaaQtrAnnPct.add(printerRecInner.tgtpcnt01);
			wsaaSingPrcnt.add(printerRecInner.tgtpcnt03);
			wsaaQtrSingPct.add(printerRecInner.tgtpcnt03);
			if (isEQ(wsaaIx, 3)
			|| isEQ(wsaaIx, 6)
			|| isEQ(wsaaIx, 9)
			|| isEQ(wsaaIx, 12)) {
				compute(wsaaIy, 0).set(sub(wsaaIx, 2));
				printerRecInner.datetext02.set(wsaaMonthDesc[wsaaIy.toInt()]);
				printerRecInner.instprem02.set(wsaaQtrAnn);
				printerRecInner.tgtpcnt02.set(wsaaQtrAnnPct);
				if (isNE(printerRecInner.tgtpcnt02, ZERO)) {
					printerRecInner.pcamtind02.set(wsaaPercentage);
				}
				printerRecInner.cntcount02.set(wsaaQtrAnnCnt);
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("- ");
				stringVariable1.addExpression(wsaaIx);
				stringVariable1.setStringInto(printerRecInner.datetext03);
				if (isNE(wsaaQtrSingp, ZERO)) {
					printerRecInner.zparnths03.set(parenthesiso);
					printerRecInner.singp02.set(wsaaQtrSingp);
					printerRecInner.zparnths04.set(parenthesisc);
					printerRecInner.tgtpcnt04.set(wsaaQtrSingPct);
					if (isNE(printerRecInner.tgtpcnt04, ZERO)) {
						printerRecInner.pcamtind04.set(wsaaPercentage);
					}
					printerRecInner.cntcount04.set(wsaaQtrSingCnt);
				}
				wsaaQtrAnn.set(ZERO);
				wsaaQtrAnnCnt.set(ZERO);
				wsaaQtrSingp.set(ZERO);
				wsaaQtrSingCnt.set(ZERO);
				wsaaQtrAnnPct.set(ZERO);
				wsaaQtrSingPct.set(ZERO);
			}
			printerFile.printRh564d01(printerRecInner.printerRec, indicArea);
		}
		/*  Print total line.                                              */
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem.set(ZERO);
		printerRecInner.singp.set(ZERO);
		printerRecInner.tgtpcnt011.set(ZERO);
		printerRecInner.tgtpcnt021.set(ZERO);
		printerRecInner.cntcount011.set(ZERO);
		printerRecInner.cntcount021.set(ZERO);
		printerRecInner.instprem.set(wsaaProdAnnprem[13]);
		printerRecInner.tgtpcnt011.set(wsaaAnnPrcnt);
		if (isNE(printerRecInner.tgtpcnt011, ZERO)) {
			printerRecInner.pcamtind011.set(wsaaPercentage);
		}
		printerRecInner.cntcount011.set(wsaaProdAnnCnt[13]);
		if (isNE(wsaaProdSingp[13], ZERO)) {
			printerRecInner.zparnths011.set(parenthesiso);
			printerRecInner.singp.set(wsaaProdSingp[13]);
			printerRecInner.zparnths021.set(parenthesisc);
			printerRecInner.tgtpcnt021.set(wsaaSingPrcnt);
			if (isNE(printerRecInner.tgtpcnt021, ZERO)) {
				printerRecInner.pcamtind021.set(wsaaPercentage);
			}
			printerRecInner.cntcount021.set(wsaaProdSingCnt[13]);
		}
		printerFile.printRh564t01(printerRecInner.printerRec);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 13)); wsaaIx.add(1)){
			wsaaProdAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaProdSingp[wsaaIx.toInt()].set(ZERO);
			wsaaProdAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaProdSingCnt[wsaaIx.toInt()].set(ZERO);
		}
	}

protected void printBranchTotals2310()
	{
		printBranch2310();
	}

protected void printBranch2310()
	{
		/*  Print page heading.                                            */
		wsaaCtrlbrk.set(4);
		pageHeading2480();
		/*  Write detail, checking for page overflow                       */
		wsaaAnnPrcnt.set(ZERO);
		wsaaSingPrcnt.set(ZERO);
		wsaaQtrAnnPct.set(ZERO);
		wsaaQtrSingPct.set(ZERO);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			printerRecInner.printerRec.set(SPACES);
			printerRecInner.instprem01.set(ZERO);
			printerRecInner.instprem02.set(ZERO);
			printerRecInner.singp01.set(ZERO);
			printerRecInner.singp02.set(ZERO);
			printerRecInner.tgtpcnt01.set(ZERO);
			printerRecInner.tgtpcnt02.set(ZERO);
			printerRecInner.tgtpcnt03.set(ZERO);
			printerRecInner.tgtpcnt04.set(ZERO);
			printerRecInner.cntcount01.set(ZERO);
			printerRecInner.cntcount02.set(ZERO);
			printerRecInner.cntcount03.set(ZERO);
			printerRecInner.cntcount04.set(ZERO);
			printerRecInner.datetext01.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.instprem01.set(wsaaBrnAnnprem[wsaaIx.toInt()]);
			compute(printerRecInner.tgtpcnt01, 3).setRounded(div((mult(wsaaBrnAnnprem[wsaaIx.toInt()], 100)), wsaaCmpyAnnprem[wsaaIx.toInt()]));
			if (isNE(printerRecInner.tgtpcnt01, ZERO)) {
				printerRecInner.pcamtind01.set(wsaaPercentage);
			}
			printerRecInner.cntcount01.set(wsaaBrnAnnCnt[wsaaIx.toInt()]);
			if (isNE(wsaaBrnSingp[wsaaIx.toInt()], ZERO)) {
				printerRecInner.zparnths01.set(parenthesiso);
				printerRecInner.singp01.set(wsaaBrnSingp[wsaaIx.toInt()]);
				printerRecInner.zparnths02.set(parenthesisc);
				compute(printerRecInner.tgtpcnt03, 3).setRounded(div((mult(wsaaBrnSingp[wsaaIx.toInt()], 100)), wsaaCmpySingp[wsaaIx.toInt()]));
				if (isNE(printerRecInner.tgtpcnt03, ZERO)) {
					printerRecInner.pcamtind03.set(wsaaPercentage);
				}
				printerRecInner.cntcount03.set(wsaaBrnSingCnt[wsaaIx.toInt()]);
			}
			wsaaQtrAnn.add(wsaaBrnAnnprem[wsaaIx.toInt()]);
			wsaaBrnAnnprem[13].add(wsaaBrnAnnprem[wsaaIx.toInt()]);
			wsaaQtrAnnCnt.add(wsaaBrnAnnCnt[wsaaIx.toInt()]);
			wsaaBrnAnnCnt[13].add(wsaaBrnAnnCnt[wsaaIx.toInt()]);
			wsaaQtrSingp.add(wsaaBrnSingp[wsaaIx.toInt()]);
			wsaaBrnSingp[13].add(wsaaBrnSingp[wsaaIx.toInt()]);
			wsaaQtrSingCnt.add(wsaaBrnSingCnt[wsaaIx.toInt()]);
			wsaaBrnSingCnt[13].add(wsaaBrnSingCnt[wsaaIx.toInt()]);
			wsaaAnnPrcnt.add(printerRecInner.tgtpcnt01);
			wsaaQtrAnnPct.add(printerRecInner.tgtpcnt01);
			wsaaSingPrcnt.add(printerRecInner.tgtpcnt03);
			wsaaQtrSingPct.add(printerRecInner.tgtpcnt03);
			if (isEQ(wsaaIx, 3)
			|| isEQ(wsaaIx, 6)
			|| isEQ(wsaaIx, 9)
			|| isEQ(wsaaIx, 12)) {
				compute(wsaaIy, 0).set(sub(wsaaIx, 2));
				printerRecInner.datetext02.set(wsaaMonthDesc[wsaaIy.toInt()]);
				printerRecInner.instprem02.set(wsaaQtrAnn);
				printerRecInner.tgtpcnt02.set(wsaaQtrAnnPct);
				if (isNE(printerRecInner.tgtpcnt02, ZERO)) {
					printerRecInner.pcamtind02.set(wsaaPercentage);
				}
				printerRecInner.cntcount02.set(wsaaQtrAnnCnt);
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("- ");
				stringVariable1.addExpression(wsaaIx);
				stringVariable1.setStringInto(printerRecInner.datetext03);
				if (isNE(wsaaQtrSingp, ZERO)) {
					printerRecInner.zparnths03.set(parenthesiso);
					printerRecInner.singp02.set(wsaaQtrSingp);
					printerRecInner.zparnths04.set(parenthesisc);
					printerRecInner.tgtpcnt04.set(wsaaQtrSingPct);
					if (isNE(printerRecInner.tgtpcnt04, ZERO)) {
						printerRecInner.pcamtind04.set(wsaaPercentage);
					}
					printerRecInner.cntcount04.set(wsaaQtrSingCnt);
				}
				wsaaQtrAnn.set(ZERO);
				wsaaQtrAnnCnt.set(ZERO);
				wsaaQtrSingp.set(ZERO);
				wsaaQtrSingCnt.set(ZERO);
				wsaaQtrAnnPct.set(ZERO);
				wsaaQtrSingPct.set(ZERO);
			}
			printerFile.printRh564d01(printerRecInner.printerRec, indicArea);
		}
		/*  Print total line.                                              */
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem.set(ZERO);
		printerRecInner.singp.set(ZERO);
		printerRecInner.tgtpcnt011.set(ZERO);
		printerRecInner.tgtpcnt021.set(ZERO);
		printerRecInner.cntcount011.set(ZERO);
		printerRecInner.cntcount021.set(ZERO);
		printerRecInner.instprem.set(wsaaBrnAnnprem[13]);
		printerRecInner.tgtpcnt011.set(wsaaAnnPrcnt);
		if (isNE(printerRecInner.tgtpcnt011, ZERO)) {
			printerRecInner.pcamtind011.set(wsaaPercentage);
		}
		printerRecInner.cntcount011.set(wsaaBrnAnnCnt[13]);
		if (isNE(wsaaBrnSingp[13], ZERO)) {
			printerRecInner.zparnths011.set(parenthesiso);
			printerRecInner.singp.set(wsaaBrnSingp[13]);
			printerRecInner.zparnths021.set(parenthesisc);
			printerRecInner.tgtpcnt021.set(wsaaSingPrcnt);
			if (isNE(printerRecInner.tgtpcnt021, ZERO)) {
				printerRecInner.pcamtind021.set(wsaaPercentage);
			}
			printerRecInner.cntcount021.set(wsaaBrnSingCnt[13]);
		}
		printerFile.printRh564t01(printerRecInner.printerRec);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 13)); wsaaIx.add(1)){
			wsaaBrnAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaBrnSingp[wsaaIx.toInt()].set(ZERO);
			wsaaBrnAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaBrnSingCnt[wsaaIx.toInt()].set(ZERO);
		}
	}

protected void printDivisionTotals2320()
	{
		printDivTotals2320();
	}

protected void printDivTotals2320()
	{
		/*  Print page heading.                                            */
		wsaaCtrlbrk.set(3);
		pageHeading2480();
		/*  Write detail, checking for page overflow                       */
		wsaaAnnPrcnt.set(ZERO);
		wsaaSingPrcnt.set(ZERO);
		wsaaQtrAnnPct.set(ZERO);
		wsaaQtrSingPct.set(ZERO);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			printerRecInner.printerRec.set(SPACES);
			printerRecInner.instprem01.set(ZERO);
			printerRecInner.instprem02.set(ZERO);
			printerRecInner.singp01.set(ZERO);
			printerRecInner.singp02.set(ZERO);
			printerRecInner.tgtpcnt01.set(ZERO);
			printerRecInner.tgtpcnt02.set(ZERO);
			printerRecInner.tgtpcnt03.set(ZERO);
			printerRecInner.tgtpcnt04.set(ZERO);
			printerRecInner.cntcount01.set(ZERO);
			printerRecInner.cntcount02.set(ZERO);
			printerRecInner.cntcount03.set(ZERO);
			printerRecInner.cntcount04.set(ZERO);
			printerRecInner.datetext01.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.instprem01.set(wsaaDivAnnprem[wsaaIx.toInt()]);
			compute(printerRecInner.tgtpcnt01, 3).setRounded(div((mult(wsaaDivAnnprem[wsaaIx.toInt()], 100)), wsaaCmpyAnnprem[wsaaIx.toInt()]));
			if (isNE(printerRecInner.tgtpcnt01, ZERO)) {
				printerRecInner.pcamtind01.set(wsaaPercentage);
			}
			printerRecInner.cntcount01.set(wsaaDivAnnCnt[wsaaIx.toInt()]);
			if (isNE(wsaaDivSingp[wsaaIx.toInt()], ZERO)) {
				printerRecInner.zparnths01.set(parenthesiso);
				printerRecInner.singp01.set(wsaaDivSingp[wsaaIx.toInt()]);
				printerRecInner.zparnths02.set(parenthesisc);
				compute(printerRecInner.tgtpcnt03, 3).setRounded(div((mult(wsaaDivSingp[wsaaIx.toInt()], 100)), wsaaCmpySingp[wsaaIx.toInt()]));
				if (isNE(printerRecInner.tgtpcnt03, ZERO)) {
					printerRecInner.pcamtind03.set(wsaaPercentage);
				}
				printerRecInner.cntcount03.set(wsaaDivSingCnt[wsaaIx.toInt()]);
			}
			wsaaQtrAnn.add(wsaaDivAnnprem[wsaaIx.toInt()]);
			wsaaDivAnnprem[13].add(wsaaDivAnnprem[wsaaIx.toInt()]);
			wsaaQtrAnnCnt.add(wsaaDivAnnCnt[wsaaIx.toInt()]);
			wsaaDivAnnCnt[13].add(wsaaDivAnnCnt[wsaaIx.toInt()]);
			wsaaQtrSingp.add(wsaaDivSingp[wsaaIx.toInt()]);
			wsaaDivSingp[13].add(wsaaDivSingp[wsaaIx.toInt()]);
			wsaaQtrSingCnt.add(wsaaDivSingCnt[wsaaIx.toInt()]);
			wsaaDivSingCnt[13].add(wsaaDivSingCnt[wsaaIx.toInt()]);
			wsaaAnnPrcnt.add(printerRecInner.tgtpcnt01);
			wsaaQtrAnnPct.add(printerRecInner.tgtpcnt01);
			wsaaSingPrcnt.add(printerRecInner.tgtpcnt03);
			wsaaQtrSingPct.add(printerRecInner.tgtpcnt03);
			if (isEQ(wsaaIx, 3)
			|| isEQ(wsaaIx, 6)
			|| isEQ(wsaaIx, 9)
			|| isEQ(wsaaIx, 12)) {
				compute(wsaaIy, 0).set(sub(wsaaIx, 2));
				printerRecInner.datetext02.set(wsaaMonthDesc[wsaaIy.toInt()]);
				printerRecInner.instprem02.set(wsaaQtrAnn);
				printerRecInner.tgtpcnt02.set(wsaaQtrAnnPct);
				if (isNE(printerRecInner.tgtpcnt02, ZERO)) {
					printerRecInner.pcamtind02.set(wsaaPercentage);
				}
				printerRecInner.cntcount02.set(wsaaQtrAnnCnt);
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("- ");
				stringVariable1.addExpression(wsaaIx);
				stringVariable1.setStringInto(printerRecInner.datetext03);
				if (isNE(wsaaQtrSingp, ZERO)) {
					printerRecInner.zparnths03.set(parenthesiso);
					printerRecInner.singp02.set(wsaaQtrSingp);
					printerRecInner.zparnths04.set(parenthesisc);
					printerRecInner.tgtpcnt04.set(wsaaQtrSingPct);
					if (isNE(printerRecInner.tgtpcnt04, ZERO)) {
						printerRecInner.pcamtind04.set(wsaaPercentage);
					}
					printerRecInner.cntcount04.set(wsaaQtrSingCnt);
				}
				wsaaQtrAnn.set(ZERO);
				wsaaQtrAnnCnt.set(ZERO);
				wsaaQtrSingp.set(ZERO);
				wsaaQtrSingCnt.set(ZERO);
				wsaaQtrAnnPct.set(ZERO);
				wsaaQtrSingPct.set(ZERO);
			}
			printerFile.printRh564d01(printerRecInner.printerRec, indicArea);
		}
		/*  Print total line.                                              */
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem.set(ZERO);
		printerRecInner.singp.set(ZERO);
		printerRecInner.tgtpcnt011.set(ZERO);
		printerRecInner.tgtpcnt021.set(ZERO);
		printerRecInner.cntcount011.set(ZERO);
		printerRecInner.cntcount021.set(ZERO);
		printerRecInner.instprem.set(wsaaDivAnnprem[13]);
		printerRecInner.tgtpcnt011.set(wsaaAnnPrcnt);
		if (isNE(printerRecInner.tgtpcnt011, ZERO)) {
			printerRecInner.pcamtind011.set(wsaaPercentage);
		}
		printerRecInner.cntcount011.set(wsaaDivAnnCnt[13]);
		if (isNE(wsaaDivSingp[13], ZERO)) {
			printerRecInner.zparnths011.set(parenthesiso);
			printerRecInner.singp.set(wsaaDivSingp[13]);
			printerRecInner.zparnths021.set(parenthesisc);
			printerRecInner.tgtpcnt021.set(wsaaSingPrcnt);
			if (isNE(printerRecInner.tgtpcnt021, ZERO)) {
				printerRecInner.pcamtind021.set(wsaaPercentage);
			}
			printerRecInner.cntcount021.set(wsaaDivSingCnt[13]);
		}
		printerFile.printRh564t01(printerRecInner.printerRec);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 13)); wsaaIx.add(1)){
			wsaaDivAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaDivSingp[wsaaIx.toInt()].set(ZERO);
			wsaaDivAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaDivSingCnt[wsaaIx.toInt()].set(ZERO);
		}
	}

protected void printDistrictTotals2330()
	{
		printDstTotals2330();
	}

protected void printDstTotals2330()
	{
		/*  Print page heading.                                            */
		wsaaCtrlbrk.set(2);
		pageHeading2480();
		/*  Write detail, checking for page overflow                       */
		wsaaAnnPrcnt.set(ZERO);
		wsaaSingPrcnt.set(ZERO);
		wsaaQtrAnnPct.set(ZERO);
		wsaaQtrSingPct.set(ZERO);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			printerRecInner.printerRec.set(SPACES);
			printerRecInner.instprem01.set(ZERO);
			printerRecInner.instprem02.set(ZERO);
			printerRecInner.singp01.set(ZERO);
			printerRecInner.singp02.set(ZERO);
			printerRecInner.tgtpcnt01.set(ZERO);
			printerRecInner.tgtpcnt02.set(ZERO);
			printerRecInner.tgtpcnt03.set(ZERO);
			printerRecInner.tgtpcnt04.set(ZERO);
			printerRecInner.cntcount01.set(ZERO);
			printerRecInner.cntcount02.set(ZERO);
			printerRecInner.cntcount03.set(ZERO);
			printerRecInner.cntcount04.set(ZERO);
			printerRecInner.datetext01.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.instprem01.set(wsaaDstAnnprem[wsaaIx.toInt()]);
			compute(printerRecInner.tgtpcnt01, 3).setRounded(div((mult(wsaaDstAnnprem[wsaaIx.toInt()], 100)), wsaaCmpyAnnprem[wsaaIx.toInt()]));
			if (isNE(printerRecInner.tgtpcnt01, ZERO)) {
				printerRecInner.pcamtind01.set(wsaaPercentage);
			}
			printerRecInner.cntcount01.set(wsaaDstAnnCnt[wsaaIx.toInt()]);
			if (isNE(wsaaDstSingp[wsaaIx.toInt()], ZERO)) {
				printerRecInner.zparnths01.set(parenthesiso);
				printerRecInner.singp01.set(wsaaDstSingp[wsaaIx.toInt()]);
				printerRecInner.zparnths02.set(parenthesisc);
				compute(printerRecInner.tgtpcnt03, 3).setRounded(div((mult(wsaaDstSingp[wsaaIx.toInt()], 100)), wsaaCmpySingp[wsaaIx.toInt()]));
				if (isNE(printerRecInner.tgtpcnt03, ZERO)) {
					printerRecInner.pcamtind03.set(wsaaPercentage);
				}
				printerRecInner.cntcount03.set(wsaaDstSingCnt[wsaaIx.toInt()]);
			}
			wsaaQtrAnn.add(wsaaDstAnnprem[wsaaIx.toInt()]);
			wsaaDstAnnprem[13].add(wsaaDstAnnprem[wsaaIx.toInt()]);
			wsaaQtrAnnCnt.add(wsaaDstAnnCnt[wsaaIx.toInt()]);
			wsaaDstAnnCnt[13].add(wsaaDstAnnCnt[wsaaIx.toInt()]);
			wsaaQtrSingp.add(wsaaDstSingp[wsaaIx.toInt()]);
			wsaaDstSingp[13].add(wsaaDstSingp[wsaaIx.toInt()]);
			wsaaQtrSingCnt.add(wsaaDstSingCnt[wsaaIx.toInt()]);
			wsaaDstSingCnt[13].add(wsaaDstSingCnt[wsaaIx.toInt()]);
			wsaaAnnPrcnt.add(printerRecInner.tgtpcnt01);
			wsaaQtrAnnPct.add(printerRecInner.tgtpcnt01);
			wsaaSingPrcnt.add(printerRecInner.tgtpcnt03);
			wsaaQtrSingPct.add(printerRecInner.tgtpcnt03);
			if (isEQ(wsaaIx, 3)
			|| isEQ(wsaaIx, 6)
			|| isEQ(wsaaIx, 9)
			|| isEQ(wsaaIx, 12)) {
				compute(wsaaIy, 0).set(sub(wsaaIx, 2));
				printerRecInner.datetext02.set(wsaaMonthDesc[wsaaIy.toInt()]);
				printerRecInner.instprem02.set(wsaaQtrAnn);
				printerRecInner.tgtpcnt02.set(wsaaQtrAnnPct);
				if (isNE(printerRecInner.tgtpcnt02, ZERO)) {
					printerRecInner.pcamtind02.set(wsaaPercentage);
				}
				printerRecInner.cntcount02.set(wsaaQtrAnnCnt);
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("- ");
				stringVariable1.addExpression(wsaaIx);
				stringVariable1.setStringInto(printerRecInner.datetext03);
				if (isNE(wsaaQtrSingp, ZERO)) {
					printerRecInner.zparnths03.set(parenthesiso);
					printerRecInner.singp02.set(wsaaQtrSingp);
					printerRecInner.zparnths04.set(parenthesisc);
					printerRecInner.tgtpcnt04.set(wsaaQtrSingPct);
					if (isNE(printerRecInner.tgtpcnt04, ZERO)) {
						printerRecInner.pcamtind04.set(wsaaPercentage);
					}
					printerRecInner.cntcount04.set(wsaaQtrSingCnt);
				}
				wsaaQtrAnn.set(ZERO);
				wsaaQtrAnnCnt.set(ZERO);
				wsaaQtrSingp.set(ZERO);
				wsaaQtrSingCnt.set(ZERO);
				wsaaQtrAnnPct.set(ZERO);
				wsaaQtrSingPct.set(ZERO);
			}
			printerFile.printRh564d01(printerRecInner.printerRec, indicArea);
		}
		/*  Print total line.                                              */
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem.set(ZERO);
		printerRecInner.singp.set(ZERO);
		printerRecInner.tgtpcnt011.set(ZERO);
		printerRecInner.tgtpcnt021.set(ZERO);
		printerRecInner.cntcount011.set(ZERO);
		printerRecInner.cntcount021.set(ZERO);
		printerRecInner.instprem.set(wsaaDstAnnprem[13]);
		printerRecInner.tgtpcnt011.set(wsaaAnnPrcnt);
		if (isNE(printerRecInner.tgtpcnt011, ZERO)) {
			printerRecInner.pcamtind011.set(wsaaPercentage);
		}
		printerRecInner.cntcount011.set(wsaaDstAnnCnt[13]);
		if (isNE(wsaaDstSingp[13], ZERO)) {
			printerRecInner.zparnths011.set(parenthesiso);
			printerRecInner.singp.set(wsaaDstSingp[13]);
			printerRecInner.zparnths021.set(parenthesisc);
			printerRecInner.tgtpcnt021.set(wsaaSingPrcnt);
			if (isNE(printerRecInner.tgtpcnt021, ZERO)) {
				printerRecInner.pcamtind021.set(wsaaPercentage);
			}
			printerRecInner.cntcount021.set(wsaaDstSingCnt[13]);
		}
		printerFile.printRh564t01(printerRecInner.printerRec);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 13)); wsaaIx.add(1)){
			wsaaDstAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaDstSingp[wsaaIx.toInt()].set(ZERO);
			wsaaDstAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaDstSingCnt[wsaaIx.toInt()].set(ZERO);
		}
	}

protected void printChannelTotals2340()
	{
		printChnlTotals2340();
	}

protected void printChnlTotals2340()
	{
		/*  Print page heading.                                            */
		wsaaCtrlbrk.set(1);
		pageHeading2480();
		/*  Write detail, checking for page overflow                       */
		wsaaAnnPrcnt.set(ZERO);
		wsaaSingPrcnt.set(ZERO);
		wsaaQtrAnnPct.set(ZERO);
		wsaaQtrSingPct.set(ZERO);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			printerRecInner.printerRec.set(SPACES);
			printerRecInner.instprem01.set(ZERO);
			printerRecInner.instprem02.set(ZERO);
			printerRecInner.singp01.set(ZERO);
			printerRecInner.singp02.set(ZERO);
			printerRecInner.tgtpcnt01.set(ZERO);
			printerRecInner.tgtpcnt02.set(ZERO);
			printerRecInner.tgtpcnt03.set(ZERO);
			printerRecInner.tgtpcnt04.set(ZERO);
			printerRecInner.cntcount01.set(ZERO);
			printerRecInner.cntcount02.set(ZERO);
			printerRecInner.cntcount03.set(ZERO);
			printerRecInner.cntcount04.set(ZERO);
			printerRecInner.datetext01.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.instprem01.set(wsaaChnlAnnprem[wsaaIx.toInt()]);
			compute(printerRecInner.tgtpcnt01, 3).setRounded(div((mult(wsaaChnlAnnprem[wsaaIx.toInt()], 100)), wsaaCmpyAnnprem[wsaaIx.toInt()]));
			if (isNE(printerRecInner.tgtpcnt01, ZERO)) {
				printerRecInner.pcamtind01.set(wsaaPercentage);
			}
			printerRecInner.cntcount01.set(wsaaChnlAnnCnt[wsaaIx.toInt()]);
			if (isNE(wsaaChnlSingp[wsaaIx.toInt()], ZERO)) {
				printerRecInner.zparnths01.set(parenthesiso);
				printerRecInner.singp01.set(wsaaChnlSingp[wsaaIx.toInt()]);
				printerRecInner.zparnths02.set(parenthesisc);
				compute(printerRecInner.tgtpcnt03, 3).setRounded(div((mult(wsaaChnlSingp[wsaaIx.toInt()], 100)), wsaaCmpySingp[wsaaIx.toInt()]));
				if (isNE(printerRecInner.tgtpcnt03, ZERO)) {
					printerRecInner.pcamtind03.set(wsaaPercentage);
				}
				printerRecInner.cntcount03.set(wsaaChnlSingCnt[wsaaIx.toInt()]);
			}
			wsaaQtrAnn.add(wsaaChnlAnnprem[wsaaIx.toInt()]);
			wsaaChnlAnnprem[13].add(wsaaChnlAnnprem[wsaaIx.toInt()]);
			wsaaQtrAnnCnt.add(wsaaChnlAnnCnt[wsaaIx.toInt()]);
			wsaaChnlAnnCnt[13].add(wsaaChnlAnnCnt[wsaaIx.toInt()]);
			wsaaQtrSingp.add(wsaaChnlSingp[wsaaIx.toInt()]);
			wsaaChnlSingp[13].add(wsaaChnlSingp[wsaaIx.toInt()]);
			wsaaQtrSingCnt.add(wsaaChnlSingCnt[wsaaIx.toInt()]);
			wsaaChnlSingCnt[13].add(wsaaChnlSingCnt[wsaaIx.toInt()]);
			wsaaAnnPrcnt.add(printerRecInner.tgtpcnt01);
			wsaaQtrAnnPct.add(printerRecInner.tgtpcnt01);
			wsaaSingPrcnt.add(printerRecInner.tgtpcnt03);
			wsaaQtrSingPct.add(printerRecInner.tgtpcnt03);
			if (isEQ(wsaaIx, 3)
			|| isEQ(wsaaIx, 6)
			|| isEQ(wsaaIx, 9)
			|| isEQ(wsaaIx, 12)) {
				compute(wsaaIy, 0).set(sub(wsaaIx, 2));
				printerRecInner.datetext02.set(wsaaMonthDesc[wsaaIy.toInt()]);
				printerRecInner.instprem02.set(wsaaQtrAnn);
				printerRecInner.tgtpcnt02.set(wsaaQtrAnnPct);
				if (isNE(printerRecInner.tgtpcnt02, ZERO)) {
					printerRecInner.pcamtind02.set(wsaaPercentage);
				}
				printerRecInner.cntcount02.set(wsaaQtrAnnCnt);
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("- ");
				stringVariable1.addExpression(wsaaIx);
				stringVariable1.setStringInto(printerRecInner.datetext03);
				if (isNE(wsaaQtrSingp, ZERO)) {
					printerRecInner.zparnths03.set(parenthesiso);
					printerRecInner.singp02.set(wsaaQtrSingp);
					printerRecInner.zparnths04.set(parenthesisc);
					printerRecInner.tgtpcnt04.set(wsaaQtrSingPct);
					if (isNE(printerRecInner.tgtpcnt04, ZERO)) {
						printerRecInner.pcamtind04.set(wsaaPercentage);
					}
					printerRecInner.cntcount04.set(wsaaQtrSingCnt);
				}
				wsaaQtrAnn.set(ZERO);
				wsaaQtrAnnCnt.set(ZERO);
				wsaaQtrSingp.set(ZERO);
				wsaaQtrSingCnt.set(ZERO);
				wsaaQtrAnnPct.set(ZERO);
				wsaaQtrSingPct.set(ZERO);
			}
			printerFile.printRh564d01(printerRecInner.printerRec, indicArea);
		}
		/*  Print total line.                                              */
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem.set(ZERO);
		printerRecInner.singp.set(ZERO);
		printerRecInner.tgtpcnt011.set(ZERO);
		printerRecInner.tgtpcnt021.set(ZERO);
		printerRecInner.cntcount011.set(ZERO);
		printerRecInner.cntcount021.set(ZERO);
		printerRecInner.instprem.set(wsaaChnlAnnprem[13]);
		printerRecInner.tgtpcnt011.set(wsaaAnnPrcnt);
		if (isNE(printerRecInner.tgtpcnt011, ZERO)) {
			printerRecInner.pcamtind011.set(wsaaPercentage);
		}
		printerRecInner.cntcount011.set(wsaaChnlAnnCnt[13]);
		if (isNE(wsaaChnlSingp[13], ZERO)) {
			printerRecInner.zparnths011.set(parenthesiso);
			printerRecInner.singp.set(wsaaChnlSingp[13]);
			printerRecInner.zparnths021.set(parenthesisc);
			printerRecInner.tgtpcnt021.set(wsaaSingPrcnt);
			if (isNE(printerRecInner.tgtpcnt021, ZERO)) {
				printerRecInner.pcamtind021.set(wsaaPercentage);
			}
			printerRecInner.cntcount021.set(wsaaChnlSingCnt[13]);
		}
		printerFile.printRh564t01(printerRecInner.printerRec);
		for (wsaaIx.set(1); !(isGT(wsaaIx, 13)); wsaaIx.add(1)){
			wsaaChnlAnnprem[wsaaIx.toInt()].set(ZERO);
			wsaaChnlSingp[wsaaIx.toInt()].set(ZERO);
			wsaaChnlAnnCnt[wsaaIx.toInt()].set(ZERO);
			wsaaChnlSingCnt[wsaaIx.toInt()].set(ZERO);
		}
	}

protected void printCompanyTotals2350()
	{
		printCmpyTotals2350();
	}

protected void printCmpyTotals2350()
	{
		/*  Print page heading.                                            */
		wsaaCtrlbrk.set(0);
		pageHeading2480();
		/*  Write detail, checking for page overflow                       */
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			printerRecInner.printerRec.set(SPACES);
			printerRecInner.instprem01.set(ZERO);
			printerRecInner.instprem02.set(ZERO);
			printerRecInner.singp01.set(ZERO);
			printerRecInner.singp02.set(ZERO);
			printerRecInner.tgtpcnt01.set(ZERO);
			printerRecInner.tgtpcnt02.set(ZERO);
			printerRecInner.tgtpcnt03.set(ZERO);
			printerRecInner.tgtpcnt04.set(ZERO);
			printerRecInner.cntcount01.set(ZERO);
			printerRecInner.cntcount02.set(ZERO);
			printerRecInner.cntcount03.set(ZERO);
			printerRecInner.cntcount04.set(ZERO);
			printerRecInner.datetext01.set(wsaaMonthDesc[wsaaIx.toInt()]);
			printerRecInner.instprem01.set(wsaaCmpyAnnprem[wsaaIx.toInt()]);
			if (isNE(wsaaCmpyAnnprem[wsaaIx.toInt()], ZERO)) {
				printerRecInner.tgtpcnt01.set(100);
				printerRecInner.pcamtind01.set(wsaaPercentage);
			}
			printerRecInner.cntcount01.set(wsaaCmpyAnnCnt[wsaaIx.toInt()]);
			if (isNE(wsaaCmpySingp[wsaaIx.toInt()], ZERO)) {
				printerRecInner.zparnths01.set(parenthesiso);
				printerRecInner.singp01.set(wsaaCmpySingp[wsaaIx.toInt()]);
				printerRecInner.zparnths02.set(parenthesisc);
				if (isNE(wsaaCmpySingp[wsaaIx.toInt()], ZERO)) {
					printerRecInner.tgtpcnt03.set(100);
					printerRecInner.pcamtind03.set(wsaaPercentage);
				}
				printerRecInner.cntcount03.set(wsaaCmpySingCnt[wsaaIx.toInt()]);
			}
			wsaaQtrAnn.add(wsaaCmpyAnnprem[wsaaIx.toInt()]);
			wsaaCmpyAnnprem[13].add(wsaaCmpyAnnprem[wsaaIx.toInt()]);
			wsaaQtrAnnCnt.add(wsaaCmpyAnnCnt[wsaaIx.toInt()]);
			wsaaCmpyAnnCnt[13].add(wsaaCmpyAnnCnt[wsaaIx.toInt()]);
			wsaaQtrSingp.add(wsaaCmpySingp[wsaaIx.toInt()]);
			wsaaCmpySingp[13].add(wsaaCmpySingp[wsaaIx.toInt()]);
			wsaaQtrSingCnt.add(wsaaCmpySingCnt[wsaaIx.toInt()]);
			wsaaCmpySingCnt[13].add(wsaaCmpySingCnt[wsaaIx.toInt()]);
			if (isEQ(wsaaIx, 3)
			|| isEQ(wsaaIx, 6)
			|| isEQ(wsaaIx, 9)
			|| isEQ(wsaaIx, 12)) {
				compute(wsaaIy, 0).set(sub(wsaaIx, 2));
				printerRecInner.datetext02.set(wsaaMonthDesc[wsaaIy.toInt()]);
				printerRecInner.instprem02.set(wsaaQtrAnn);
				if (isNE(wsaaQtrAnn, ZERO)) {
					printerRecInner.tgtpcnt02.set(100);
					printerRecInner.pcamtind02.set(wsaaPercentage);
				}
				printerRecInner.cntcount02.set(wsaaQtrAnnCnt);
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression("- ");
				stringVariable1.addExpression(wsaaIx);
				stringVariable1.setStringInto(printerRecInner.datetext03);
				if (isNE(wsaaQtrSingp, ZERO)) {
					printerRecInner.zparnths03.set(parenthesiso);
					printerRecInner.singp02.set(wsaaQtrSingp);
					printerRecInner.zparnths04.set(parenthesisc);
					if (isNE(wsaaQtrSingp, ZERO)) {
						printerRecInner.tgtpcnt04.set(100);
						printerRecInner.pcamtind04.set(wsaaPercentage);
					}
					printerRecInner.cntcount04.set(wsaaQtrSingCnt);
				}
				wsaaQtrAnn.set(ZERO);
				wsaaQtrAnnCnt.set(ZERO);
				wsaaQtrSingp.set(ZERO);
				wsaaQtrSingCnt.set(ZERO);
			}
			printerFile.printRh564d01(printerRecInner.printerRec, indicArea);
		}
		/*  Print total line.                                              */
		printerRecInner.printerRec.set(SPACES);
		printerRecInner.instprem.set(ZERO);
		printerRecInner.singp.set(ZERO);
		printerRecInner.tgtpcnt011.set(ZERO);
		printerRecInner.tgtpcnt021.set(ZERO);
		printerRecInner.cntcount011.set(ZERO);
		printerRecInner.cntcount021.set(ZERO);
		printerRecInner.instprem.set(wsaaCmpyAnnprem[13]);
		if (isNE(wsaaCmpyAnnprem[13], ZERO)) {
			printerRecInner.tgtpcnt011.set(100);
			printerRecInner.pcamtind011.set(wsaaPercentage);
		}
		printerRecInner.cntcount011.set(wsaaCmpyAnnCnt[13]);
		if (isNE(wsaaCmpySingp[13], ZERO)) {
			printerRecInner.zparnths011.set(parenthesiso);
			printerRecInner.singp.set(wsaaCmpySingp[13]);
			printerRecInner.zparnths021.set(parenthesisc);
			if (isNE(wsaaCmpySingp[13], ZERO)) {
				printerRecInner.tgtpcnt021.set(100);
				printerRecInner.pcamtind021.set(wsaaPercentage);
			}
			printerRecInner.cntcount021.set(wsaaCmpySingCnt[13]);
		}
		printerFile.printRh564t01(printerRecInner.printerRec);
	}

protected void readTh5662400()
	{
		th5662400();
	}

protected void th5662400()
	{
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(th566);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(itemIO.getStatuz());
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		th566rec.th566Rec.set(itemIO.getGenarea());
	}

protected void pageHeading2480()
	{
		heading2480();
	}

protected void heading2480()
	{
		printerRecInner.printerRec.set(SPACES);
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*  RH564H01.                                                      */
		printerRecInner.sdate.set(wsaaSdate);
		/* MOVE WSAA-REPDATE        TO DATETEXT   OF RH564H01-O.<MLS002>*/
		printerRecInner.datetexc.set(wsaaRepdate);
		printerRecInner.currcode.set(th565rec.currcode);
		printerRecInner.currencynm.set(wsaaCurrencynm);
		printerRecInner.company.set(bsprIO.getCompany());
		printerRecInner.companynm.set(wsaaCompanynm);
		printerFile.printRh564h01(printerRecInner.printerRec);
		/*  RH564H02.                                                      */
		if (isGT(wsaaCtrlbrk, 0)) {
			printerRecInner.printerRec.set(SPACES);
			printerRecInner.branch.set(wsaaPrevBranch);
			descIO.setDescitem(wsaaPrevBranch);
			descIO.setDesctabl(t1692);
			getDescription2490();
			printerRecInner.branchnm.set(descIO.getLongdesc());
			printerFile.printRh564h02(printerRecInner.printerRec);
		}
		/*  RH564H03.                                                      */
		if (isGT(wsaaCtrlbrk, 1)) {
			printerRecInner.printerRec.set(SPACES);
			printerRecInner.aracde.set(wsaaPrevAracde);
			descIO.setDescitem(wsaaPrevAracde);
			descIO.setDesctabl(t5696);
			getDescription2490();
			printerRecInner.aradesc.set(descIO.getLongdesc());
			printerFile.printRh564h03(printerRecInner.printerRec);
		}
		/*  RH564H06.                                                      */
		if (isGT(wsaaCtrlbrk, 4)) {
			printerRecInner.printerRec.set(SPACES);
			printerRecInner.cnttype.set(wsaaPrevCnttype);
			descIO.setDescitem(wsaaPrevCnttype);
			descIO.setDesctabl(t5688);
			getDescription2490();
			printerRecInner.cntdesc.set(descIO.getLongdesc());
			printerFile.printRh564h06(printerRecInner.printerRec);
		}
		/*  RH564H07                                                       */
		printerRecInner.printerRec.set(SPACES);
		printerFile.printRh564h07(printerRecInner.printerRec);
	}

protected void getDescription2490()
	{
		/*GETDESC*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		/** Check record is required for processing.                        */
		/** Softlock the record if it is to be updated.                     */
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/**  Update database records.                                       */
		/*WRITE-DETAIL*/
		/** If first page/overflow - write standard headings                */
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/** Place any additional commitment processing in here.             */
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.               */
		/*EXIT*/
	}

protected void close4000()
	{
		closeFiles4010();
		resetStatuz4080();
	}

protected void closeFiles4010()
	{
		if (isNE(wsaaOpenedFile, "Y")) {
			return ;
		}
		/*   Close the cursor                                              */
		getAppVars().freeDBConnectionIgnoreErr(sqlzstrpf1conn, sqlzstrpf1ps, sqlzstrpf1rs);
		/*  Close any open files.                                          */
		printerFile.close();
	}

protected void resetStatuz4080()
	{
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void a000CallRounding()
	{
		/*A100-CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.currency.set(th565rec.currcode);
		zrdecplrec.batctrcde.set(bprdIO.getAuthCode());
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*A900-EXIT*/
	}
/*
 * Class transformed  from Data Structure PRINTER-REC--INNER
 */
private static final class PrinterRecInner { 

	private FixedLengthStringData printerRec = new FixedLengthStringData(177);
	private FixedLengthStringData rh564Record = new FixedLengthStringData(177).isAPartOf(printerRec, 0);
	private FixedLengthStringData rh564h01O = new FixedLengthStringData(96).isAPartOf(rh564Record, 0, REDEFINE);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(rh564h01O, 0);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22).isAPartOf(rh564h01O, 10);
	private FixedLengthStringData currcode = new FixedLengthStringData(3).isAPartOf(rh564h01O, 32);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30).isAPartOf(rh564h01O, 35);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rh564h01O, 65);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rh564h01O, 66);
	private FixedLengthStringData rh564h02O = new FixedLengthStringData(32).isAPartOf(rh564Record, 0, REDEFINE);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(rh564h02O, 0);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(rh564h02O, 2);
	private FixedLengthStringData rh564h03O = new FixedLengthStringData(33).isAPartOf(rh564Record, 0, REDEFINE);
	private FixedLengthStringData aracde = new FixedLengthStringData(3).isAPartOf(rh564h03O, 0);
	private FixedLengthStringData aradesc = new FixedLengthStringData(30).isAPartOf(rh564h03O, 3);
	private FixedLengthStringData rh564h06O = new FixedLengthStringData(33).isAPartOf(rh564Record, 0, REDEFINE);
	private FixedLengthStringData cnttype = new FixedLengthStringData(3).isAPartOf(rh564h06O, 0);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(rh564h06O, 3);
	private FixedLengthStringData rh564d01O = new FixedLengthStringData(177).isAPartOf(rh564Record, 0, REDEFINE);
	private FixedLengthStringData datetext01 = new FixedLengthStringData(19).isAPartOf(rh564d01O, 0);
	private ZonedDecimalData instprem01 = new ZonedDecimalData(17, 2).isAPartOf(rh564d01O, 19);
	private ZonedDecimalData tgtpcnt01 = new ZonedDecimalData(5, 2).isAPartOf(rh564d01O, 36);
	private FixedLengthStringData pcamtind01 = new FixedLengthStringData(1).isAPartOf(rh564d01O, 41);
	private ZonedDecimalData cntcount01 = new ZonedDecimalData(6, 0).isAPartOf(rh564d01O, 42);
	private FixedLengthStringData datetext02 = new FixedLengthStringData(19).isAPartOf(rh564d01O, 48);
	private ZonedDecimalData instprem02 = new ZonedDecimalData(17, 2).isAPartOf(rh564d01O, 67);
	private ZonedDecimalData tgtpcnt02 = new ZonedDecimalData(5, 2).isAPartOf(rh564d01O, 84);
	private FixedLengthStringData pcamtind02 = new FixedLengthStringData(1).isAPartOf(rh564d01O, 89);
	private ZonedDecimalData cntcount02 = new ZonedDecimalData(6, 0).isAPartOf(rh564d01O, 90);
	private FixedLengthStringData zparnths01 = new FixedLengthStringData(1).isAPartOf(rh564d01O, 96);
	private ZonedDecimalData singp01 = new ZonedDecimalData(17, 2).isAPartOf(rh564d01O, 97);
	private FixedLengthStringData zparnths02 = new FixedLengthStringData(1).isAPartOf(rh564d01O, 114);
	private ZonedDecimalData tgtpcnt03 = new ZonedDecimalData(5, 2).isAPartOf(rh564d01O, 115);
	private FixedLengthStringData pcamtind03 = new FixedLengthStringData(1).isAPartOf(rh564d01O, 120);
	private ZonedDecimalData cntcount03 = new ZonedDecimalData(6, 0).isAPartOf(rh564d01O, 121);
	private FixedLengthStringData datetext03 = new FixedLengthStringData(19).isAPartOf(rh564d01O, 127);
	private FixedLengthStringData zparnths03 = new FixedLengthStringData(1).isAPartOf(rh564d01O, 146);
	private ZonedDecimalData singp02 = new ZonedDecimalData(17, 2).isAPartOf(rh564d01O, 147);
	private FixedLengthStringData zparnths04 = new FixedLengthStringData(1).isAPartOf(rh564d01O, 164);
	private ZonedDecimalData tgtpcnt04 = new ZonedDecimalData(5, 2).isAPartOf(rh564d01O, 165);
	private FixedLengthStringData pcamtind04 = new FixedLengthStringData(1).isAPartOf(rh564d01O, 170);
	private ZonedDecimalData cntcount04 = new ZonedDecimalData(6, 0).isAPartOf(rh564d01O, 171);
	private FixedLengthStringData rh564t01O = new FixedLengthStringData(60).isAPartOf(rh564Record, 0, REDEFINE);
	private ZonedDecimalData instprem = new ZonedDecimalData(17, 2).isAPartOf(rh564t01O, 0);
	private ZonedDecimalData tgtpcnt011 = new ZonedDecimalData(5, 2).isAPartOf(rh564t01O, 17);
	private FixedLengthStringData pcamtind011 = new FixedLengthStringData(1).isAPartOf(rh564t01O, 22);
	private ZonedDecimalData cntcount011 = new ZonedDecimalData(6, 0).isAPartOf(rh564t01O, 23);
	private FixedLengthStringData zparnths011 = new FixedLengthStringData(1).isAPartOf(rh564t01O, 29);
	private ZonedDecimalData singp = new ZonedDecimalData(17, 2).isAPartOf(rh564t01O, 30);
	private FixedLengthStringData zparnths021 = new FixedLengthStringData(1).isAPartOf(rh564t01O, 47);
	private ZonedDecimalData tgtpcnt021 = new ZonedDecimalData(5, 2).isAPartOf(rh564t01O, 48);
	private FixedLengthStringData pcamtind021 = new FixedLengthStringData(1).isAPartOf(rh564t01O, 53);
	private ZonedDecimalData cntcount021 = new ZonedDecimalData(6, 0).isAPartOf(rh564t01O, 54);
}
/*
 * Class transformed  from Data Structure SORT-REC--INNER
 */
private static final class SortRecInner { 

	private FixedLengthStringData sortRec = new FixedLengthStringData(55);
	private FixedLengthStringData sortCntbranch = new FixedLengthStringData(2).isAPartOf(sortRec, 0);
	private FixedLengthStringData sortAracde = new FixedLengthStringData(3).isAPartOf(sortRec, 2);
	private FixedLengthStringData sortCnttype = new FixedLengthStringData(3).isAPartOf(sortRec, 5);
	private FixedLengthStringData sortChdrnum = new FixedLengthStringData(8).isAPartOf(sortRec, 8);
	private FixedLengthStringData sortBatctrcde = new FixedLengthStringData(4).isAPartOf(sortRec, 16);
	private PackedDecimalData sortSingp = new PackedDecimalData(17, 2).isAPartOf(sortRec, 20);
	private PackedDecimalData sortAnnprem = new PackedDecimalData(17, 2).isAPartOf(sortRec, 29);
	private PackedDecimalData sortSumins = new PackedDecimalData(17, 2).isAPartOf(sortRec, 38);
	private FixedLengthStringData sortBillfreq = new FixedLengthStringData(2).isAPartOf(sortRec, 47);
	private ZonedDecimalData sortMnth = new ZonedDecimalData(2, 0).isAPartOf(sortRec, 49).setUnsigned();
	private ZonedDecimalData sortYear = new ZonedDecimalData(4, 0).isAPartOf(sortRec, 51).setUnsigned();
}
/*
 * Class transformed  from Data Structure SQL-ZSTRPF--INNER
 */
private static final class SqlZstrpfInner { 

		/* SQL-ZSTRPF */
	private FixedLengthStringData sqlZstrrec = new FixedLengthStringData(71);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlZstrrec, 0);
	private FixedLengthStringData sqlAracde = new FixedLengthStringData(3).isAPartOf(sqlZstrrec, 2);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlZstrrec, 5);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlZstrrec, 8);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlZstrrec, 16);
	private PackedDecimalData sqlSingp = new PackedDecimalData(17, 2).isAPartOf(sqlZstrrec, 19);
	private PackedDecimalData sqlInstprem = new PackedDecimalData(17, 2).isAPartOf(sqlZstrrec, 28);
	private PackedDecimalData sqlSumins = new PackedDecimalData(17, 2).isAPartOf(sqlZstrrec, 37);
	private PackedDecimalData sqlEffdate = new PackedDecimalData(8, 0).isAPartOf(sqlZstrrec, 46);
	private FixedLengthStringData sqlBillfreq = new FixedLengthStringData(2).isAPartOf(sqlZstrrec, 51);
	private FixedLengthStringData sqlBatctrcde = new FixedLengthStringData(4).isAPartOf(sqlZstrrec, 53);
	private PackedDecimalData sqlCntfee = new PackedDecimalData(17, 2).isAPartOf(sqlZstrrec, 57);
	private PackedDecimalData sqlOccdate = new PackedDecimalData(8, 0).isAPartOf(sqlZstrrec, 66);
}
}
