package com.csc.life.newbusiness.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:24
 * Description:
 * Copybook name: TH554REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Th554rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData th554Rec = new FixedLengthStringData(500);
  	public ZonedDecimalData zmaxunits = new ZonedDecimalData(2, 0).isAPartOf(th554Rec, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(498).isAPartOf(th554Rec, 2, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(th554Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		th554Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}