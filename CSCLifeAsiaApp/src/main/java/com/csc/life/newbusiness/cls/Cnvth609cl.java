/*
 * File: Cnvth609cl.java
 * Date: 30 August 2009 2:59:04
 * Author: $Id$
 * 
 * Class transformed from CNVTH609CL.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.cls;

import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.newbusiness.procedures.Cnvth609;
import com.csc.life.newbusiness.procedures.Cnvth609a;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class Cnvth609cl extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData dtalib = new FixedLengthStringData(10);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData statuz = new FixedLengthStringData(4);

	public Cnvth609cl() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		company = convertAndSetParam(company, parmArray, 1);
		dtalib = convertAndSetParam(dtalib, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					/* CPYTH609*/
					appVars.startCommitControl();
					callProgram(Cnvth609a.class, new Object[] {company, statuz});
					if (isNE(statuz,"****")) {
						appVars.sendMessageToQueue("AN ERROR OCCURED IN CNVTH609A ERROR STATUS IS "+statuz+".", "*");
						rollback();
						qState = returnVar;
						break;
					}
					callProgram(Cnvth609.class, new Object[] {company, statuz});
					if (isNE(statuz,"****")) {
						appVars.sendMessageToQueue("AN ERROR OCCURED IN CNVTH609 ERROR STATUS IS "+statuz+".", "*");
						rollback();
						qState = returnVar;
						break;
					}
					appVars.endCommitControl();
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.sendMessageToQueue("UNEXPECTED ERRORS IN CNVTH609CL", "*");
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("LBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
