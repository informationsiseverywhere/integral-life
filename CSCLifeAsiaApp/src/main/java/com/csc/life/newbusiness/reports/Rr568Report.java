package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RR568.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:52
 * @author Quipoz
 */
public class Rr568Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData date01 = new FixedLengthStringData(10);
	private FixedLengthStringData date02 = new FixedLengthStringData(10);
	private ZonedDecimalData instprem = new ZonedDecimalData(17, 2);
	private FixedLengthStringData mlinsopt = new FixedLengthStringData(2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private ZonedDecimalData sacscurbal = new ZonedDecimalData(17, 2);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rr568Report() {
		super();
	}


	/**
	 * Print the XML for Rr568d01
	 */
	public void printRr568d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		date01.setFieldName("date01");
		date01.setInternal(subString(recordData, 9, 10));
		mlinsopt.setFieldName("mlinsopt");
		mlinsopt.setInternal(subString(recordData, 19, 2));
		instprem.setFieldName("instprem");
		instprem.setInternal(subString(recordData, 21, 17));
		sacscurbal.setFieldName("sacscurbal");
		sacscurbal.setInternal(subString(recordData, 38, 17));
		date02.setFieldName("date02");
		date02.setInternal(subString(recordData, 55, 10));
		printLayout("Rr568d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				date01,
				mlinsopt,
				instprem,
				sacscurbal,
				date02
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rr568d02
	 */
	public void printRr568d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		instprem.setFieldName("instprem");
		instprem.setInternal(subString(recordData, 1, 17));
		sacscurbal.setFieldName("sacscurbal");
		sacscurbal.setInternal(subString(recordData, 18, 17));
		printLayout("Rr568d02",			// Record name
			new BaseData[]{			// Fields:
				instprem,
				sacscurbal
			}
		);

	}

	/**
	 * Print the XML for Rr568h01
	 */
	public void printRr568h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 1, 10));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 11, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 12, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 42, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 52, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 54, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("Rr568h01",			// Record name
			new BaseData[]{			// Fields:
				repdate,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time,
				pagnbr
			}
		);

		currentPrintLine.set(13);
	}


}
