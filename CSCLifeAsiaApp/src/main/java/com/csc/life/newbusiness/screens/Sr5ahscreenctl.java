package com.csc.life.newbusiness.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.TableModel.ScreenRecord.RecInfo;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

public class Sr5ahscreenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sr5ahscreensfl";
		lrec.subfileClass = Sr5ahscreensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.sizeSubfile = 8;
		lrec.pageSubfile = 8;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 14, 1, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sr5ahScreenVars sv = (Sr5ahScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sr5ahscreenctlWritten, sv.Sr5ahscreensflWritten, av, sv.sr5ahscreensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sr5ahScreenVars screenVars = (Sr5ahScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.chdrnum.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.crtabdesc.setClassString("");
		screenVars.optdsc01.setClassString("");
		screenVars.optdsc02.setClassString("");
		screenVars.optdsc03.setClassString("");
		screenVars.optdsc04.setClassString("");
		screenVars.optdsc05.setClassString("");
		
	}

/**
 * Clear all the variables in Sr5ahscreenctl
 */
	public static void clear(VarModel pv) {
		Sr5ahScreenVars screenVars = (Sr5ahScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.chdrnum.clear();
		screenVars.crtable.clear();
		screenVars.crtabdesc.clear();
		screenVars.optdsc01.clear();
		screenVars.optdsc02.clear();
		screenVars.optdsc03.clear();
		screenVars.optdsc04.clear();
		screenVars.optdsc05.clear();
	}
}
