/*
 * File: Ph538.java
 * Date: 30 August 2009 1:06:37
 * Author: Quipoz Limited
 * 
 * Class transformed from PH538.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.newbusiness.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.dataaccess.CltsTableDAM;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmnaTableDAM;
import com.csc.life.newbusiness.dataaccess.LifelnbTableDAM;
import com.csc.life.newbusiness.screens.Sh538ScreenVars;
import com.csc.life.productdefinition.recordstructures.Wssplife;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Atreq;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Atreqrec;
import com.csc.smart.recordstructures.Batckey;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Sftlockrec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*                    SCHEDULE REPRINT
*
*This is a submission screen requesting for schedule reprint.
*
* Initialise
* ----------
*
* Read CHDRMNA (RETRV)  in  order to obtain the contract header
* information.
*
* Read  the 'DESCIO' in  order to obtain the description of the
* risk and premium statuses.
*
* In order to format  the  required  names  the  client details
* record is read and  the  relevant copybook for formatting the
* names must be included in the program.
*
* Read  the  agent  details (AGNTLNB) and format the name using
* the copybooks.
*
* Validation
* ----------
*
* If KILL is requested, then skip the validation and exit.
*
* Check the  returned  screen  for  any  errors,  if errors are
* present, then highlight.
*
* Only validation.
*
*      - schedule print flag either 'Y' or 'N'.
*
*Updating
*--------
*
* Call 'LETRQST' to make a request via ATREQ.
*
* Next Program
* ------------
*
* Add 1 to the program pointer and exit.
*                                                                     *
*****************************************************************
* </pre>
*/
public class Ph538 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PH538");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaTransactionRec = new FixedLengthStringData(200);
	private PackedDecimalData wsaaTotamnt = new PackedDecimalData(17, 2).isAPartOf(wsaaTransactionRec, 0);
	private FixedLengthStringData wsaaFsuCoy = new FixedLengthStringData(1).isAPartOf(wsaaTransactionRec, 9);
	private PackedDecimalData wsaaTransactionDate = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 10);
	private PackedDecimalData wsaaTransactionTime = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 14);
	private PackedDecimalData wsaaUser = new PackedDecimalData(6, 0).isAPartOf(wsaaTransactionRec, 18);
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4).isAPartOf(wsaaTransactionRec, 22);
	private PackedDecimalData wsaaDteeff = new PackedDecimalData(8, 0).isAPartOf(wsaaTransactionRec, 26);
	private FixedLengthStringData filler = new FixedLengthStringData(169).isAPartOf(wsaaTransactionRec, 31, FILLER).init(SPACES);

	private FixedLengthStringData wsaaPrimaryKey = new FixedLengthStringData(27);
	private FixedLengthStringData wsaaPrimkeyReqcoy = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 0);
	private FixedLengthStringData wsaaPrimkeyLettype = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 1);
	private FixedLengthStringData wsaaPrimkeyClntcoy = new FixedLengthStringData(1).isAPartOf(wsaaPrimaryKey, 9);
	private FixedLengthStringData wsaaPrimkeyRdocnum = new FixedLengthStringData(9).isAPartOf(wsaaPrimaryKey, 10);
	private FixedLengthStringData wsaaPrimkeyClntnum = new FixedLengthStringData(8).isAPartOf(wsaaPrimaryKey, 19);
		/* ERRORS */
	private static final String e304 = "E304";
	private static final String f910 = "F910";
	private static final String hl29 = "HL29";
		/* TABLES */
	private static final String t3623 = "T3623";
	private static final String t3588 = "T3588";
	private static final String t5688 = "T5688";
	private static final String tr384 = "TR384";
	private static final String chdrmnarec = "CHDRMNAREC";
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0);

	private FixedLengthStringData wsaaOtherKeys = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaOtherKeys, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaOtherKeys, 1);
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private ChdrmnaTableDAM chdrmnaIO = new ChdrmnaTableDAM();
	private CltsTableDAM cltsIO = new CltsTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifelnbTableDAM lifelnbIO = new LifelnbTableDAM();
	private Batckey wsaaBatcKey = new Batckey();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Sftlockrec sftlockrec = new Sftlockrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Atreqrec atreqrec = new Atreqrec();
	private Tr384rec tr384rec = new Tr384rec();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Wssplife wssplife = new Wssplife();
	private Sh538ScreenVars sv = ScreenProgram.getScreenVars( Sh538ScreenVars.class);
	//ILJ-49 Starts
	private boolean cntDteFlag = false;
	private String cntDteFeature = "NBPRP113";
	//ILJ-49 End 

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit3090
	}

	public Ph538() {
		super();
		screenVars = sv;
		new ScreenModel("Sh538", AppVars.getInstance(), sv);
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wssplife.userArea = convertAndSetParam(wssplife.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);

		try {
			processBoMainline(sv, sv.dataArea, parmArray);
		} catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
}


	/**
	* <pre>
	*      INITIALISE FIELDS FOR SHOWING ON SCREEN
	* </pre>
	*/
protected void initialise1000()
	{
		initialise1010();
		blankOutFields1020();
	}

protected void initialise1010()
	{
		wsaaBatcKey.set(wsspcomn.batchkey);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
	}

protected void blankOutFields1020()
	{
		sv.dataArea.set(SPACES);
		//ILJ-49 Starts
				cntDteFlag = FeaConfg.isFeatureExist(wsspcomn.company.toString(), cntDteFeature, appVars, "IT");
						if(!cntDteFlag) {
							sv.occdateOut[varcom.nd.toInt()].set("Y");
							}
		//ILJ-49 End
		sv.prtshd.set("B");
		/* Retrieve contract fields from I/O module*/
		chdrmnaIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		sv.chdrnum.set(chdrmnaIO.getChdrnum());
		sv.occdate.set(chdrmnaIO.getOccdate());
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t5688);
		descIO.setDescitem(chdrmnaIO.getCnttype());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.ctypdesc.set(descIO.getLongdesc());
		}
		else {
			sv.ctypdesc.fill("?");
		}
		/*  Load all fields from the Contract Header to the Screen*/
		sv.cownnum.set(chdrmnaIO.getCownnum());
		sv.jownnum.set(chdrmnaIO.getJownnum());
		sv.ptdate.set(chdrmnaIO.getPtdate());
		sv.billfreq.set(chdrmnaIO.getBillfreq());
		sv.mop.set(chdrmnaIO.getBillchnl());
		sv.agntnum.set(chdrmnaIO.getAgntnum());
		sv.cnttype.set(chdrmnaIO.getCnttype()); 
		/*  Look up premium status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3588);
		descIO.setDescitem(chdrmnaIO.getPstatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.pstate.set(descIO.getShortdesc());
		}
		else {
			sv.pstate.fill("?");
		}
		/*  Look up Risk status*/
		descIO.setDataKey(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsspcomn.company);
		descIO.setDesctabl(t3623);
		descIO.setDescitem(chdrmnaIO.getStatcode());
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(), varcom.oK)) {
			sv.rstate.set(descIO.getShortdesc());
		}
		else {
			sv.rstate.fill("?");
		}
		/*   Look up all other descriptions*/
		/*       - owner name*/
		/*       - joint owner name*/
		/*       - agent name*/
		if (isNE(sv.cownnum, SPACES)) {
			formatClientName1700();
		}
		if (isNE(sv.jownnum, SPACES)) {
			formatJointOwner1800();
		}
		if (isNE(sv.agntnum, SPACES)) {
			formatAgentName1900();
		}
		sv.instPrem.set(chdrmnaIO.getInsttot06());
		sv.cntcurr.set(chdrmnaIO.getCntcurr());
	}

	/**
	* <pre>
	*      RETRIEVE CLIENT / JOIN OWNER DETAIL AND SET SCREEN
	* </pre>
	*/
protected void formatClientName1700()
	{
		readClientRecord1710();
	}

protected void readClientRecord1710()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.cownnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.cownnumErr.set(e304);
			sv.ownername.set(SPACES);
		}
		else {
			plainname();
			sv.ownername.set(wsspcomn.longconfname);
		}
	}

protected void formatJointOwner1800()
	{
		readClientRecord1810();
	}

protected void readClientRecord1810()
	{
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(sv.jownnum);
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)
		&& isNE(cltsIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		if (isEQ(cltsIO.getStatuz(), varcom.mrnf)
		|| isNE(cltsIO.getValidflag(), 1)) {
			sv.jownnumErr.set(e304);
			sv.jownername.set(SPACES);
		}
		else {
			plainname();
			sv.jownername.set(wsspcomn.longconfname);
		}
	}

protected void formatAgentName1900()
	{
		readAgent1910();
	}

protected void readAgent1910()
	{
		aglflnbIO.setDataKey(SPACES);
		aglflnbIO.setAgntcoy(wsspcomn.company);
		aglflnbIO.setAgntnum(sv.agntnum);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)
		&& isNE(aglflnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		sv.agentname.set(SPACES);
		cltsIO.setDataArea(SPACES);
		cltsIO.setClntcoy(wsspcomn.fsuco);
		cltsIO.setClntpfx("CN");
		cltsIO.setClntnum(aglflnbIO.getClntnum());
		cltsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cltsIO);
		if (isNE(cltsIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(cltsIO.getParams());
			fatalError600();
		}
		plainname();
		sv.agentname.set(wsspcomn.longconfname);
	}

protected void largename()
	{
		/*LGNM-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		wsspcomn.longconfname.set(cltsIO.getSurname());
		/*LGNM-EXIT*/
	}

protected void plainname()
	{
		/*PLAIN-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isNE(cltsIO.getGivname(), SPACES)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(", ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
		}
		else {
			wsspcomn.longconfname.set(cltsIO.getSurname());
		}
		/*PLAIN-EXIT*/
	}

protected void payeename()
	{
		/*PAYEE-100*/
		wsspcomn.longconfname.set(SPACES);
		if (isEQ(cltsIO.getClttype(), "C")) {
			corpname();
			return ;
		}
		if (isEQ(cltsIO.getEthorig(), "1")) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(cltsIO.getSalutl(), "  ");
			stringVariable1.addExpression(". ");
			stringVariable1.addExpression(cltsIO.getSurname(), "  ");
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(cltsIO.getGivname(), "  ");
			stringVariable1.setStringInto(wsspcomn.longconfname);
			return ;
		}
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(cltsIO.getSalutl(), "  ");
		stringVariable2.addExpression(". ");
		stringVariable2.addExpression(cltsIO.getGivname(), "  ");
		stringVariable2.addExpression(" ");
		stringVariable2.addExpression(cltsIO.getSurname(), "  ");
		stringVariable2.setStringInto(wsspcomn.longconfname);
		/*PAYEE-EXIT*/
	}

protected void corpname()
	{
		/*PAYEE-1001*/
		wsspcomn.longconfname.set(SPACES);
		/* STRING CLTS-SURNAME         DELIMITED SIZE                   */
		/*        CLTS-GIVNAME         DELIMITED '  '                   */
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(cltsIO.getLsurname(), "  ");
		stringVariable1.addExpression(" ");
		stringVariable1.addExpression(cltsIO.getLgivname(), "  ");
		stringVariable1.setStringInto(wsspcomn.longconfname);
		/*CORP-EXIT*/
	}

	/**
	* <pre>
	*  END OF CONFNAME **********************************************
	*     RETRIEVE SCREEN FIELDS AND EDIT
	* </pre>
	*/
protected void preScreenEdit()
	{
		/*PRE-START*/
		wsspcomn.edterror.set(varcom.oK);
		return ;
		/*PRE-EXIT*/
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		/*    CALL 'SH538IO' USING SCRN-SCREEN-PARAMS                      */
		/*                          SH538-DATA-AREA.                       */
		/* Screen errors are now handled in the calling program.           */
		/*    PERFORM 200-SCREEN-ERRORS.                                   */
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isNE(sv.prtshd, "N")
		&& isNE(sv.prtshd, "O")
		&& isNE(sv.prtshd, "B")) {
			sv.prtshdErr.set(hl29);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*     UPDATE DATABASE IF REQUIRED AND LOG TRANSACTION
	* </pre>
	*/
protected void update3000()
	{
		try {
			updateDatabase3010();
			toatSftlock3070();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void updateDatabase3010()
	{
		varcom.vrcmTranid.set(wsspcomn.tranid);
		varcom.vrcmCompTermid.set(varcom.vrcmTermid);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		chdrmnaIO.setTranid(varcom.vrcmCompTranid);
		/*  Update contract header fields as follows*/
		chdrmnaIO.setFunction(varcom.keeps);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		chdrmnaIO.setFunction(varcom.writs);
		chdrmnaIO.setFormat(chdrmnarec);
		SmartFileCode.execute(appVars, chdrmnaIO);
		if (isNE(chdrmnaIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(chdrmnaIO.getParams());
			fatalError600();
		}
		if (isEQ(sv.prtshd, "B")) {
			writeLetter6000();
		}
		if (isEQ(sv.prtshd, "N")
		|| isEQ(sv.prtshd, "B")) {
			/* Release the soft lock on the contract.*/
			sftlockrec.sftlockRec.set(SPACES);
			sftlockrec.company.set(wsspcomn.company);
			sftlockrec.entity.set(chdrmnaIO.getChdrnum());
			sftlockrec.enttyp.set("CH");
			sftlockrec.user.set(varcom.vrcmUser);
			sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
			sftlockrec.statuz.set(SPACES);
			sftlockrec.function.set("UNLK");
			callProgram(Sftlock.class, sftlockrec.sftlockRec);
			if (isNE(sftlockrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		writeLetter6000();
		if (isEQ(sv.prtshd, "O")) {
			sftlockrec.sftlockRec.set(SPACES);
			sftlockrec.company.set(wsspcomn.company);
			sftlockrec.entity.set(chdrmnaIO.getChdrnum());
			sftlockrec.enttyp.set("CH");
			sftlockrec.user.set(varcom.vrcmUser);
			sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
			sftlockrec.statuz.set(SPACES);
			sftlockrec.function.set("UNLK");
			callProgram(Sftlock.class, sftlockrec.sftlockRec);
			if (isNE(sftlockrec.statuz, varcom.oK)) {
				syserrrec.statuz.set(sftlockrec.statuz);
				fatalError600();
			}
			goTo(GotoLabel.exit3090);
		}
		if (isNE(letrqstrec.statuz, varcom.oK)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void toatSftlock3070()
	{
		/* Soft lock on the contract awaiting for AT.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(wsspcomn.company);
		sftlockrec.entity.set(chdrmnaIO.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(wsaaBatcKey.batcBatctrcde);
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("TOAT");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if ((isNE(sftlockrec.statuz, varcom.oK))
		&& (isNE(sftlockrec.statuz, "LOCK"))) {
			syserrrec.params.set(sftlockrec.sftlockRec);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			sv.chdrnumErr.set(f910);
			wsspcomn.edterror.set("Y");
			return ;
		}
		/*  call the at module ATREQ*/
		atreqrec.atreqRec.set(SPACES);
		atreqrec.acctYear.set(ZERO);
		atreqrec.acctMonth.set(ZERO);
		atreqrec.module.set("PH538AT");
		atreqrec.batchKey.set(wsspcomn.batchkey);
		atreqrec.reqProg.set(wsaaProg);
		atreqrec.reqUser.set(varcom.vrcmUser);
		atreqrec.reqTerm.set(varcom.vrcmTermid);
		atreqrec.reqDate.set(wsaaToday);
		atreqrec.reqTime.set(varcom.vrcmTime);
		atreqrec.language.set(wsspcomn.language);
		/* MOVE LETRQST-LETCKEY      TO  ATRT-PRIMARY-KEY.              */
		wsaaPrimkeyReqcoy.set(chdrmnaIO.getChdrcoy());
		wsaaPrimkeyLettype.set(tr384rec.letterType);
		wsaaPrimkeyClntcoy.set(chdrmnaIO.getCowncoy());
		wsaaPrimkeyRdocnum.set(chdrmnaIO.getChdrnum());
		wsaaPrimkeyClntnum.set(chdrmnaIO.getCownnum());
		atreqrec.primaryKey.set(wsaaPrimaryKey);
		wsaaFsuCoy.set(wsspcomn.fsuco);
		wsaaTransactionDate.set(varcom.vrcmDate);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(varcom.vrcmUser);
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaTotamnt.set(sv.instPrem);
		wsaaDteeff.set(wsaaToday);
		atreqrec.transArea.set(wsaaTransactionRec);
		atreqrec.statuz.set(varcom.oK);
		callProgram(Atreq.class, atreqrec.atreqRec);
		if (isNE(atreqrec.statuz, varcom.oK)) {
			syserrrec.params.set(atreqrec.atreqRec);
			fatalError600();
		}
	}

	/**
	* <pre>
	*     DECIDE WHICH TRANSACTION PROGRAM IS NEXT
	* </pre>
	*/
protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}

protected void writeLetter6000()
	{
		readT66346010();
	}

protected void readT66346010()
	{
		/*  Get the Letter type from T6634.*/
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrmnaIO.getChdrcoy());
		/* MOVE T6634                      TO ITEM-ITEMTABL.            */
		itemIO.setItemtabl(tr384);
		/*  Build key to T6634 from contract type & transaction code.*/
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(chdrmnaIO.getCnttype(), SPACES);
		stringVariable1.addExpression(wsaaBatcKey.batcBatctrcde, SPACES);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression("***", SPACES);
			stringVariable2.addExpression(wsaaBatcKey.batcBatctrcde, SPACES);
			stringVariable2.setStringInto(itemIO.getItemitem());
			SmartFileCode.execute(appVars, itemIO);
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		/* MOVE ITEM-GENAREA               TO T6634-T6634-REC.          */
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF  T6634-LETTER-TYPE           = SPACES                     */
		if (isEQ(tr384rec.letterType, SPACES)) {
			return ;
		}
		/*  Write LETC via LETRQST and update the seqno by 1*/
		letrqstrec.statuz.set(SPACES);
		letrqstrec.requestCompany.set(chdrmnaIO.getChdrcoy());
		/* MOVE T6634-LETTER-TYPE      TO LETRQST-LETTER-TYPE.          */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.clntcoy.set(chdrmnaIO.getCowncoy());
		letrqstrec.clntnum.set(chdrmnaIO.getCownnum());
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.rdocpfx.set(fsupfxcpy.chdr);
		letrqstrec.rdoccoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrmnaIO.getChdrnum());
		letrqstrec.tranno.set(chdrmnaIO.getTranno());
		letrqstrec.trcde.set(wsaaBatcKey.batcBatctrcde);
		wsaaOtherKeys.set(SPACES);
		wsaaChdrnum.set(chdrmnaIO.getChdrnum());
		wsaaLanguage.set(wsspcomn.language);
		letrqstrec.otherKeys.set(wsaaOtherKeys);
		letrqstrec.branch.set(chdrmnaIO.getCntbranch());
		letrqstrec.chdrcoy.set(chdrmnaIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrmnaIO.getChdrnum());
		if (isEQ(sv.prtshd, "O")) {
			letrqstrec.crtind.set("I");
		}
		else {
			letrqstrec.crtind.set(SPACES);
		}
		letrqstrec.function.set("ADD");
		/* IF  T6634-LETTER-TYPE       NOT = SPACES                     */
		if (isNE(tr384rec.letterType, SPACES)) {
			/*     CALL 'LETRQST' USING LETRQST-PARAMS                      */
			/*     CALL 'HLETRQS' USING LETRQST-PARAMS              <PCPPRT>*/
			callProgram(Letrqst.class, letrqstrec.params);
			if (isNE(letrqstrec.statuz, varcom.oK)) {
				syserrrec.params.set(letrqstrec.params);
				syserrrec.statuz.set(letrqstrec.statuz);
				fatalError600();
			}
		}
	}
}
