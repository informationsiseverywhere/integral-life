package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RH560.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rh560Report extends SMARTReportLayout { 

	private FixedLengthStringData agntnum = new FixedLengthStringData(8);
	private FixedLengthStringData aracde = new FixedLengthStringData(3);
	private FixedLengthStringData aradesc = new FixedLengthStringData(30);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData clntsname = new FixedLengthStringData(30);
	private FixedLengthStringData cltname = new FixedLengthStringData(47);
	private FixedLengthStringData cntbranch = new FixedLengthStringData(2);
	private FixedLengthStringData cntcurr = new FixedLengthStringData(3);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private ZonedDecimalData crlimit = new ZonedDecimalData(7, 0);
	private FixedLengthStringData datetexc = new FixedLengthStringData(22);
	private FixedLengthStringData descr = new FixedLengthStringData(30);
	private ZonedDecimalData origross = new ZonedDecimalData(18, 2);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totprem = new ZonedDecimalData(11, 2);
	private FixedLengthStringData zhlpol = new FixedLengthStringData(12);
	private FixedLengthStringData zxidno = new FixedLengthStringData(10);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rh560Report() {
		super();
	}


	/**
	 * Print the XML for Rh560d01
	 */
	public void printRh560d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(31).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		zhlpol.setFieldName("zhlpol");
		zhlpol.setInternal(subString(recordData, 1, 12));
		origross.setFieldName("origross");
		origross.setInternal(subString(recordData, 13, 18));
		descr.setFieldName("descr");
		descr.setInternal(subString(recordData, 31, 30));
		clntsname.setFieldName("clntsname");
		clntsname.setInternal(subString(recordData, 61, 30));
		zxidno.setFieldName("zxidno");
		zxidno.setInternal(subString(recordData, 91, 10));
		cntcurr.setFieldName("cntcurr");
		cntcurr.setInternal(subString(recordData, 101, 3));
		crlimit.setFieldName("crlimit");
		crlimit.setInternal(subString(recordData, 104, 7));
		totprem.setFieldName("totprem");
		totprem.setInternal(subString(recordData, 111, 11));
		printLayout("Rh560d01",			// Record name
			new BaseData[]{			// Fields:
				zhlpol,
				origross,
				descr,
				clntsname,
				zxidno,
				cntcurr,
				crlimit,
				totprem
			}
			, new Object[] {			// indicators
				new Object[]{"ind31", indicArea.charAt(31)}
			}
		);

		currentPrintLine.add(5);
	}

	/**
	 * Print the XML for Rh560f01
	 */
	public void printRh560f01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(3);

		printLayout("Rh560f01",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rh560h01
	 */
	public void printRh560h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		datetexc.setFieldName("datetexc");
		datetexc.setInternal(subString(recordData, 32, 22));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 54, 10));
		cntbranch.setFieldName("cntbranch");
		cntbranch.setInternal(subString(recordData, 64, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 66, 30));
		time.setFieldName("time");
		time.set(getTime());
		aracde.setFieldName("aracde");
		aracde.setInternal(subString(recordData, 96, 3));
		aradesc.setFieldName("aradesc");
		aradesc.setInternal(subString(recordData, 99, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		agntnum.setFieldName("agntnum");
		agntnum.setInternal(subString(recordData, 129, 8));
		cltname.setFieldName("cltname");
		cltname.setInternal(subString(recordData, 137, 47));
		printLayout("Rh560h01",			// Record name
			new BaseData[]{			// Fields:
				company,
				companynm,
				datetexc,
				sdate,
				cntbranch,
				branchnm,
				time,
				aracde,
				aradesc,
				pagnbr,
				agntnum,
				cltname
			}
		);

		currentPrintLine.set(10);
	}


}
