package com.csc.life.newbusiness.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:23:25
 * Description:
 * Copybook name: ZSTRAGSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Zstragskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData zstragsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData zstragsKey = new FixedLengthStringData(64).isAPartOf(zstragsFileKey, 0, REDEFINE);
  	public FixedLengthStringData zstragsChdrcoy = new FixedLengthStringData(1).isAPartOf(zstragsKey, 0);
  	public FixedLengthStringData zstragsCntbranch = new FixedLengthStringData(2).isAPartOf(zstragsKey, 1);
  	public FixedLengthStringData zstragsAracde = new FixedLengthStringData(3).isAPartOf(zstragsKey, 3);
  	public FixedLengthStringData zstragsAgntnum = new FixedLengthStringData(8).isAPartOf(zstragsKey, 6);
  	public FixedLengthStringData zstragsCnttype = new FixedLengthStringData(3).isAPartOf(zstragsKey, 14);
  	public PackedDecimalData zstragsEffdate = new PackedDecimalData(8, 0).isAPartOf(zstragsKey, 17);
  	public PackedDecimalData zstragsSingp = new PackedDecimalData(17, 2).isAPartOf(zstragsKey, 22);
  	public FixedLengthStringData filler = new FixedLengthStringData(33).isAPartOf(zstragsKey, 31, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(zstragsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		zstragsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}