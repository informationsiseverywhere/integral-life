package com.csc.life.newbusiness.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGDateData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * This class will generate the XML file for the report.
 * @version 1.0
 * 
 * @author vhukumagrawa
 *
 */
public class R5222Report extends SMARTReportLayout{

	private RPGDateData dateReportVariable = new RPGDateData();
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData errorcode = new FixedLengthStringData(4);
	private FixedLengthStringData errormsg = new FixedLengthStringData(30);
	private FixedLengthStringData screen = new FixedLengthStringData(5);
	private FixedLengthStringData addndtl = new FixedLengthStringData(30);
	private FixedLengthStringData dummy = new FixedLengthStringData(1);
	
	/**
	 * Constructors
	 */
	public R5222Report(){
		super();
	}
	
	/**
	 * Print the XML for R5222d01
	 */
	public void printR5222d01(FixedLengthStringData... printData) {
		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}
		
		currentPrintLine.add(1);
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		errorcode.setFieldName("errorcode");
		errorcode.setInternal(subString(recordData, 9, 4));
		errormsg.setFieldName("errormsg");
		errormsg.setInternal(subString(recordData, 13, 30));
		screen.setFieldName("screen");
		screen.setInternal(subString(recordData, 43, 5));
		addndtl.setFieldName("addndtl");
		addndtl.setInternal(subString(recordData, 48, 30));
		printLayout("R5222d01",
						new BaseData[]{
							chdrnum,
							errorcode,
							errormsg,
							screen,
							addndtl
						});
	}
	
	/**
	 * Print the XML for R5222h01
	 */
	public void printR5222h01(FixedLengthStringData... printData) {
		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}
		
		currentPrintLine.add(1);
		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.set(getDate());
		company.setFieldName("company");
		company.setInternal(subString(recordData, 1, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 2, 30));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R5222h01",			// Record name
				new BaseData[]{			// Fields:
					dateReportVariable,
					company,
					companynm,
					time,
					pagnbr
				}
			);
		
		currentPrintLine.set(9);
	}
	
	/**
	 * Print the XML for R5222e01
	 */
	public void printR5222e01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(5);

		dummy.setFieldName("dummy");
		dummy.setInternal(subString(recordData, 1, 1));
		printLayout("R5222e01",			// Record name
			new BaseData[]{			// Fields:
				dummy
			}
		);

	}
}
