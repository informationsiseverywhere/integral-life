/*
 * File: Bj528.java
 * Date: 29 August 2009 21:42:58
 * Author: Quipoz Limited
 *
 * Class transformed from BJ528.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.statistics.dataaccess.GoveTableDAM;
import com.csc.life.statistics.dataaccess.GvacstsTableDAM;
import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj528Report;
import com.csc.life.statistics.tablestructures.Tj675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                  Policy Movement Report
*                  ======================
*   This report will list the Policy Movement for the specified
*   period.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - Select data from GOVE where STATCAT = 'NB', 'RI', 'IN',  AR
*       'DE', 'DH', 'RD', 'RP', 'SU', 'LA', 'CF'                 AR
*
*    Read
*     - read SQL File Until end of file
*
*      Edit
*       - set WSSP-EDTERROR to ENDP when EOF.
*
*      Update
*       - if new page, write headings
*       - if there is a change in company, accounting cuurency
*         or source of business print the reassurance details
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*     02  -  Number of records read
*     03  -  Total policy no
*     04  -  Total sum assure
*     05  -  Total annuity pa
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Bj528 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgovepfAllrs = null;
	private java.sql.PreparedStatement sqlgovepfAllps = null;
	private java.sql.Connection sqlgovepfAllconn = null;
	private String sqlgovepfAll = "";
	private java.sql.ResultSet sqlgovepfrs = null;
	private java.sql.PreparedStatement sqlgovepfps = null;
	private java.sql.Connection sqlgovepfconn = null;
	private String sqlgovepf = "";
	private Rj528Report printerFile = new Rj528Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(2250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ528");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private ZonedDecimalData wsaaScrate = new ZonedDecimalData(12, 7);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData mth = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaSelectAll = new FixedLengthStringData(1);
	private Validator selectAll = new Validator(wsaaSelectAll, "Y");
	private FixedLengthStringData wsaaTj675Stsect01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect02 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect03 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect04 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect05 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect06 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect07 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect08 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect09 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect10 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStssect = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaRegister = new FixedLengthStringData(3);
	private String wsaaFrecord = "";
	private String wsaaFlag = "";

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaProgram = new FixedLengthStringData(5).isAPartOf(wsaaItem, 1);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String wsaaNb = "NB";
	private String wsaaRi = "RI";
	private String wsaaIn = "IN";
	private String wsaaDe = "DE";
	private String wsaaRd = "RD";
	private String wsaaDh = "DH";
	private String wsaaMa = "MA";
	private String wsaaEx = "EX";
	private String wsaaSu = "SU";
	private String wsaaLa = "LA";
	private String wsaaLc = "LC";
	private String wsaaCf = "CF";
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaAmount = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStcamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStiamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthAddTotal = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthAddTotal = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthAddTotal = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthDelTotal = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthDelTotal = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthDelTotal = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBonusAlloTotal = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBonusSurrTotal = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthNb = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthNb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthNb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthRi = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthRi = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthRi = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthIn = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthIn = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthIn = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBonusAllotted = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthDth = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthDth = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthDth = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthMa = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthMa = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthMa = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthEx = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthEx = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthEx = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthSu = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthSu = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthSu = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBonusSurrender = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStcmthDe = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthDe = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthDe = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthCf = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthCf = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthCf = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcmthLa = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStimthLa = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthLa = new PackedDecimalData(18, 2);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);

		/* SQL-GOVEPF */
	private FixedLengthStringData sqlGoverec = new FixedLengthStringData(335);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 0);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 1);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGoverec, 3);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 6);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 7);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 9);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 12);
	private FixedLengthStringData sqlCrpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 14);
	private FixedLengthStringData sqlCnpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 16);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 18);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 21);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 25);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 28);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 31);
	private PackedDecimalData sqlStcmth01 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 35);
	private PackedDecimalData sqlStcmth02 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 40);
	private PackedDecimalData sqlStcmth03 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 45);
	private PackedDecimalData sqlStcmth04 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 50);
	private PackedDecimalData sqlStcmth05 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 55);
	private PackedDecimalData sqlStcmth06 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 60);
	private PackedDecimalData sqlStcmth07 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 65);
	private PackedDecimalData sqlStcmth08 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 70);
	private PackedDecimalData sqlStcmth09 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 75);
	private PackedDecimalData sqlStcmth10 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 80);
	private PackedDecimalData sqlStcmth11 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 85);
	private PackedDecimalData sqlStcmth12 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 90);
	private PackedDecimalData sqlStimth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 95);
	private PackedDecimalData sqlStimth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 105);
	private PackedDecimalData sqlStimth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 115);
	private PackedDecimalData sqlStimth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 125);
	private PackedDecimalData sqlStimth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 135);
	private PackedDecimalData sqlStimth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 145);
	private PackedDecimalData sqlStimth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 155);
	private PackedDecimalData sqlStimth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 165);
	private PackedDecimalData sqlStimth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 175);
	private PackedDecimalData sqlStimth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 185);
	private PackedDecimalData sqlStimth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 195);
	private PackedDecimalData sqlStimth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 205);
	private PackedDecimalData sqlStamth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 215);
	private PackedDecimalData sqlStamth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 225);
	private PackedDecimalData sqlStamth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 235);
	private PackedDecimalData sqlStamth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 245);
	private PackedDecimalData sqlStamth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 255);
	private PackedDecimalData sqlStamth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 265);
	private PackedDecimalData sqlStamth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 275);
	private PackedDecimalData sqlStamth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 285);
	private PackedDecimalData sqlStamth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 295);
	private PackedDecimalData sqlStamth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 305);
	private PackedDecimalData sqlStamth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 315);
	private PackedDecimalData sqlStamth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 325);

	private FixedLengthStringData tmpGoverec = new FixedLengthStringData(335);
	private FixedLengthStringData tmpChdrcoy = new FixedLengthStringData(1).isAPartOf(tmpGoverec, 0);
	private FixedLengthStringData tmpStatcat = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 1);
	private PackedDecimalData tmpAcctyr = new PackedDecimalData(4, 0).isAPartOf(tmpGoverec, 3);
	private FixedLengthStringData tmpStfund = new FixedLengthStringData(1).isAPartOf(tmpGoverec, 6);
	private FixedLengthStringData tmpStsect = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 7);
	private FixedLengthStringData tmpRegister = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 9);
	private FixedLengthStringData tmpCntbranch = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 12);
	private FixedLengthStringData tmpCrpstat = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 14);
	private FixedLengthStringData tmpCnpstat = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 16);
	private FixedLengthStringData tmpCnttype = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 18);
	private FixedLengthStringData tmpCrtable = new FixedLengthStringData(4).isAPartOf(tmpGoverec, 21);
	private FixedLengthStringData tmpAcctccy = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 25);
	private FixedLengthStringData tmpCntcurr = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 28);
	private FixedLengthStringData tmpStssect = new FixedLengthStringData(4).isAPartOf(tmpGoverec, 31);
	private FixedLengthStringData tmpStcmths = new FixedLengthStringData(60).isAPartOf(tmpGoverec, 35);
	private PackedDecimalData[] tmpStcmth = PDArrayPartOfStructure(12, 9, 0, tmpStcmths, 0);
	private FixedLengthStringData tmpStimths = new FixedLengthStringData(120).isAPartOf(tmpGoverec, 95);
	private PackedDecimalData[] tmpStimth = PDArrayPartOfStructure(12, 18, 2, tmpStimths, 0);
	private FixedLengthStringData tmpStamths = new FixedLengthStringData(120).isAPartOf(tmpGoverec, 215);
	private PackedDecimalData[] tmpStamth = PDArrayPartOfStructure(12, 18, 2, tmpStamths, 0);
		/* ERRORS */
	private String g418 = "G418";
	private String esql = "ESQL";
	private String itemrec = "ITEMREC";
	private String descrec = "DESCREC";
	private String gvacstsrec = "GVACSTSREC";
		/* TABLES */
	private String t5685 = "T5685";
	private String t3589 = "T3589";
	private String t3629 = "T3629";
	private String t1693 = "T1693";
	private String t5684 = "T5684";
	private String tj675 = "TJ675";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;

	private FixedLengthStringData rj528H01 = new FixedLengthStringData(179);
	private FixedLengthStringData rj528h01O = new FixedLengthStringData(179).isAPartOf(rj528H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj528h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj528h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj528h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj528h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj528h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj528h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj528h01O, 50);
	private FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(rj528h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj528h01O, 83);
	private FixedLengthStringData stsect = new FixedLengthStringData(2).isAPartOf(rj528h01O, 113);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30).isAPartOf(rj528h01O, 115);
	private FixedLengthStringData stssect = new FixedLengthStringData(4).isAPartOf(rj528h01O, 145);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rj528h01O, 149);

	private FixedLengthStringData rj528D01 = new FixedLengthStringData(54);
	private FixedLengthStringData rj528d01O = new FixedLengthStringData(54).isAPartOf(rj528D01, 0);
	private ZonedDecimalData stcmth = new ZonedDecimalData(9, 0).isAPartOf(rj528d01O, 0);
	private ZonedDecimalData repamt01 = new ZonedDecimalData(15, 0).isAPartOf(rj528d01O, 9);
	private ZonedDecimalData repamt02 = new ZonedDecimalData(15, 0).isAPartOf(rj528d01O, 24);
	private ZonedDecimalData repamt03 = new ZonedDecimalData(15, 0).isAPartOf(rj528d01O, 39);

	private FixedLengthStringData rj528D02 = new FixedLengthStringData(2);

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Additional Govr Statistics Accumulation*/
	private GoveTableDAM goveIO = new GoveTableDAM();
		/*Statistic Accumulation for policy admin*/
	private GvacstsTableDAM gvacstsIO = new GvacstsTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Pj517par pj517par = new Pj517par();
	private T3629rec t3629rec = new T3629rec();
	private Tj675rec tj675rec = new Tj675rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		h320CallItemio,
		h520Read,
		h590Exit
	}

	public Bj528() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		printerFile.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsaaProgram.set(wsaaProg);
		wsaaLanguage.set(bsscIO.getLanguage());
		wsaaAcctyr.set(pj517par.acctyr);
		acctyear.set(pj517par.acctyr);
		acctmonth.set(pj517par.acctmnth);
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaStcmthNb.set(ZERO);
		wsaaStimthNb.set(ZERO);
		wsaaStamthNb.set(ZERO);
		wsaaStcmthRi.set(ZERO);
		wsaaStimthRi.set(ZERO);
		wsaaStamthRi.set(ZERO);
		wsaaStcmthIn.set(ZERO);
		wsaaStimthIn.set(ZERO);
		wsaaStamthIn.set(ZERO);
		wsaaStcmthDth.set(ZERO);
		wsaaStimthDth.set(ZERO);
		wsaaStamthDth.set(ZERO);
		wsaaStcmthMa.set(ZERO);
		wsaaStimthMa.set(ZERO);
		wsaaStamthMa.set(ZERO);
		wsaaStcmthEx.set(ZERO);
		wsaaStimthEx.set(ZERO);
		wsaaStamthEx.set(ZERO);
		wsaaStcmthSu.set(ZERO);
		wsaaStimthSu.set(ZERO);
		wsaaStamthSu.set(ZERO);
		wsaaStcmthDe.set(ZERO);
		wsaaStimthDe.set(ZERO);
		wsaaStamthDe.set(ZERO);
		wsaaStcmthCf.set(ZERO);
		wsaaStimthCf.set(ZERO);
		wsaaStamthCf.set(ZERO);
		wsaaStcmthLa.set(ZERO);
		wsaaStimthLa.set(ZERO);
		wsaaStamthLa.set(ZERO);
		wsaaBonusSurrender.set(ZERO);
		wsaaBonusAllotted.set(ZERO);
		wsaaStcmthAddTotal.set(ZERO);
		wsaaStimthAddTotal.set(ZERO);
		wsaaStamthAddTotal.set(ZERO);
		wsaaStcmthDelTotal.set(ZERO);
		wsaaStimthDelTotal.set(ZERO);
		wsaaStamthDelTotal.set(ZERO);
		wsaaBonusAlloTotal.set(ZERO);
		wsaaBonusSurrTotal.set(ZERO);
		wsaaStcamt.set(ZERO);
		wsaaStiamt.set(ZERO);
		wsaaStaamt.set(ZERO);
		wsaaStsect.set(SPACES);
		wsaaStssect.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaRegister.set(SPACES);
		wsaaFrecord = "Y";
		readTj6751300();
		/*EXIT*/
	}

protected void readTj6751300()
	{
		para1310();
	}

protected void para1310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tj675);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		wsaaSelectAll.set("Y");
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			selectAll1400();
		}
		else {
			wsaaSelectAll.set("N");
			tj675rec.tj675Rec.set(itemIO.getGenarea());
			selectSpecified1500();
		}
	}

protected void selectAll1400()
	{
		/*PARA*/
		sqlgovepfAll = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, STSSECT, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE ACCTYR = ?" +
" AND STATCAT IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfAllconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfAllps = appVars.prepareStatementEmbeded(sqlgovepfAllconn, sqlgovepfAll, "GOVEPF");
			appVars.setDBDouble(sqlgovepfAllps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgovepfAllps, 2, wsaaNb);
			appVars.setDBString(sqlgovepfAllps, 3, wsaaRi);
			appVars.setDBString(sqlgovepfAllps, 4, wsaaIn);
			appVars.setDBString(sqlgovepfAllps, 5, wsaaDe);
			appVars.setDBString(sqlgovepfAllps, 6, wsaaRd);
			appVars.setDBString(sqlgovepfAllps, 7, wsaaDh);
			appVars.setDBString(sqlgovepfAllps, 8, wsaaMa);
			appVars.setDBString(sqlgovepfAllps, 9, wsaaEx);
			appVars.setDBString(sqlgovepfAllps, 10, wsaaSu);
			appVars.setDBString(sqlgovepfAllps, 11, wsaaLa);
			appVars.setDBString(sqlgovepfAllps, 12, wsaaLc);
			appVars.setDBString(sqlgovepfAllps, 13, wsaaCf);
			sqlgovepfAllrs = appVars.executeQuery(sqlgovepfAllps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void selectSpecified1500()
	{
		para1510();
	}

protected void para1510()
	{
		wsaaTj675Stsect01.set(tj675rec.statSect[1]);
		wsaaTj675Stsect02.set(tj675rec.statSect[2]);
		wsaaTj675Stsect03.set(tj675rec.statSect[3]);
		wsaaTj675Stsect04.set(tj675rec.statSect[4]);
		wsaaTj675Stsect05.set(tj675rec.statSect[5]);
		wsaaTj675Stsect06.set(tj675rec.statSect[6]);
		wsaaTj675Stsect07.set(tj675rec.statSect[7]);
		wsaaTj675Stsect08.set(tj675rec.statSect[8]);
		wsaaTj675Stsect09.set(tj675rec.statSect[9]);
		wsaaTj675Stsect10.set(tj675rec.statSect[10]);
		sqlgovepf = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, STSSECT, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE ACCTYR = ?" +
" AND (STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?)" +
" AND STATCAT IN (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfps = appVars.prepareStatementEmbeded(sqlgovepfconn, sqlgovepf, "GOVEPF");
			appVars.setDBDouble(sqlgovepfps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgovepfps, 2, wsaaTj675Stsect01.toString());
			appVars.setDBString(sqlgovepfps, 3, wsaaTj675Stsect02.toString());
			appVars.setDBString(sqlgovepfps, 4, wsaaTj675Stsect03.toString());
			appVars.setDBString(sqlgovepfps, 5, wsaaTj675Stsect04.toString());
			appVars.setDBString(sqlgovepfps, 6, wsaaTj675Stsect05.toString());
			appVars.setDBString(sqlgovepfps, 7, wsaaTj675Stsect06.toString());
			appVars.setDBString(sqlgovepfps, 8, wsaaTj675Stsect07.toString());
			appVars.setDBString(sqlgovepfps, 9, wsaaTj675Stsect08.toString());
			appVars.setDBString(sqlgovepfps, 10, wsaaTj675Stsect09.toString());
			appVars.setDBString(sqlgovepfps, 11, wsaaTj675Stsect10.toString());
			appVars.setDBString(sqlgovepfps, 12, wsaaNb);
			appVars.setDBString(sqlgovepfps, 13, wsaaRi);
			appVars.setDBString(sqlgovepfps, 14, wsaaIn);
			appVars.setDBString(sqlgovepfps, 15, wsaaDe);
			appVars.setDBString(sqlgovepfps, 16, wsaaRd);
			appVars.setDBString(sqlgovepfps, 17, wsaaDh);
			appVars.setDBString(sqlgovepfps, 18, wsaaMa);
			appVars.setDBString(sqlgovepfps, 19, wsaaEx);
			appVars.setDBString(sqlgovepfps, 20, wsaaSu);
			appVars.setDBString(sqlgovepfps, 21, wsaaLa);
			appVars.setDBString(sqlgovepfps, 22, wsaaLc);
			appVars.setDBString(sqlgovepfps, 23, wsaaCf);
			sqlgovepfrs = appVars.executeQuery(sqlgovepfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		if (selectAll.isTrue()) {
			sqlerrorflag = false;
			try {
				if (sqlgovepfAllrs.next()) {
					appVars.getDBObject(sqlgovepfAllrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfAllrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfAllrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfAllrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfAllrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfAllrs, 6, sqlRegister);
					appVars.getDBObject(sqlgovepfAllrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgovepfAllrs, 8, sqlCrpstat);
					appVars.getDBObject(sqlgovepfAllrs, 9, sqlCnpstat);
					appVars.getDBObject(sqlgovepfAllrs, 10, sqlCnttype);
					appVars.getDBObject(sqlgovepfAllrs, 11, sqlCrtable);
					appVars.getDBObject(sqlgovepfAllrs, 12, sqlAcctccy);
					appVars.getDBObject(sqlgovepfAllrs, 13, sqlCntcurr);
					appVars.getDBObject(sqlgovepfAllrs, 14, sqlStssect);
					appVars.getDBObject(sqlgovepfAllrs, 15, sqlStcmth01);
					appVars.getDBObject(sqlgovepfAllrs, 16, sqlStcmth02);
					appVars.getDBObject(sqlgovepfAllrs, 17, sqlStcmth03);
					appVars.getDBObject(sqlgovepfAllrs, 18, sqlStcmth04);
					appVars.getDBObject(sqlgovepfAllrs, 19, sqlStcmth05);
					appVars.getDBObject(sqlgovepfAllrs, 20, sqlStcmth06);
					appVars.getDBObject(sqlgovepfAllrs, 21, sqlStcmth07);
					appVars.getDBObject(sqlgovepfAllrs, 22, sqlStcmth08);
					appVars.getDBObject(sqlgovepfAllrs, 23, sqlStcmth09);
					appVars.getDBObject(sqlgovepfAllrs, 24, sqlStcmth10);
					appVars.getDBObject(sqlgovepfAllrs, 25, sqlStcmth11);
					appVars.getDBObject(sqlgovepfAllrs, 26, sqlStcmth12);
					appVars.getDBObject(sqlgovepfAllrs, 27, sqlStimth01);
					appVars.getDBObject(sqlgovepfAllrs, 28, sqlStimth02);
					appVars.getDBObject(sqlgovepfAllrs, 29, sqlStimth03);
					appVars.getDBObject(sqlgovepfAllrs, 30, sqlStimth04);
					appVars.getDBObject(sqlgovepfAllrs, 31, sqlStimth05);
					appVars.getDBObject(sqlgovepfAllrs, 32, sqlStimth06);
					appVars.getDBObject(sqlgovepfAllrs, 33, sqlStimth07);
					appVars.getDBObject(sqlgovepfAllrs, 34, sqlStimth08);
					appVars.getDBObject(sqlgovepfAllrs, 35, sqlStimth09);
					appVars.getDBObject(sqlgovepfAllrs, 36, sqlStimth10);
					appVars.getDBObject(sqlgovepfAllrs, 37, sqlStimth11);
					appVars.getDBObject(sqlgovepfAllrs, 38, sqlStimth12);
					appVars.getDBObject(sqlgovepfAllrs, 39, sqlStamth01);
					appVars.getDBObject(sqlgovepfAllrs, 40, sqlStamth02);
					appVars.getDBObject(sqlgovepfAllrs, 41, sqlStamth03);
					appVars.getDBObject(sqlgovepfAllrs, 42, sqlStamth04);
					appVars.getDBObject(sqlgovepfAllrs, 43, sqlStamth05);
					appVars.getDBObject(sqlgovepfAllrs, 44, sqlStamth06);
					appVars.getDBObject(sqlgovepfAllrs, 45, sqlStamth07);
					appVars.getDBObject(sqlgovepfAllrs, 46, sqlStamth08);
					appVars.getDBObject(sqlgovepfAllrs, 47, sqlStamth09);
					appVars.getDBObject(sqlgovepfAllrs, 48, sqlStamth10);
					appVars.getDBObject(sqlgovepfAllrs, 49, sqlStamth11);
					appVars.getDBObject(sqlgovepfAllrs, 50, sqlStamth12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		else {
			sqlerrorflag = false;
			try {
				if (sqlgovepfrs.next()) {
					appVars.getDBObject(sqlgovepfrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfrs, 6, sqlRegister);
					appVars.getDBObject(sqlgovepfrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgovepfrs, 8, sqlCrpstat);
					appVars.getDBObject(sqlgovepfrs, 9, sqlCnpstat);
					appVars.getDBObject(sqlgovepfrs, 10, sqlCnttype);
					appVars.getDBObject(sqlgovepfrs, 11, sqlCrtable);
					appVars.getDBObject(sqlgovepfrs, 12, sqlAcctccy);
					appVars.getDBObject(sqlgovepfrs, 13, sqlCntcurr);
					appVars.getDBObject(sqlgovepfrs, 14, sqlStssect);
					appVars.getDBObject(sqlgovepfrs, 15, sqlStcmth01);
					appVars.getDBObject(sqlgovepfrs, 16, sqlStcmth02);
					appVars.getDBObject(sqlgovepfrs, 17, sqlStcmth03);
					appVars.getDBObject(sqlgovepfrs, 18, sqlStcmth04);
					appVars.getDBObject(sqlgovepfrs, 19, sqlStcmth05);
					appVars.getDBObject(sqlgovepfrs, 20, sqlStcmth06);
					appVars.getDBObject(sqlgovepfrs, 21, sqlStcmth07);
					appVars.getDBObject(sqlgovepfrs, 22, sqlStcmth08);
					appVars.getDBObject(sqlgovepfrs, 23, sqlStcmth09);
					appVars.getDBObject(sqlgovepfrs, 24, sqlStcmth10);
					appVars.getDBObject(sqlgovepfrs, 25, sqlStcmth11);
					appVars.getDBObject(sqlgovepfrs, 26, sqlStcmth12);
					appVars.getDBObject(sqlgovepfrs, 27, sqlStimth01);
					appVars.getDBObject(sqlgovepfrs, 28, sqlStimth02);
					appVars.getDBObject(sqlgovepfrs, 29, sqlStimth03);
					appVars.getDBObject(sqlgovepfrs, 30, sqlStimth04);
					appVars.getDBObject(sqlgovepfrs, 31, sqlStimth05);
					appVars.getDBObject(sqlgovepfrs, 32, sqlStimth06);
					appVars.getDBObject(sqlgovepfrs, 33, sqlStimth07);
					appVars.getDBObject(sqlgovepfrs, 34, sqlStimth08);
					appVars.getDBObject(sqlgovepfrs, 35, sqlStimth09);
					appVars.getDBObject(sqlgovepfrs, 36, sqlStimth10);
					appVars.getDBObject(sqlgovepfrs, 37, sqlStimth11);
					appVars.getDBObject(sqlgovepfrs, 38, sqlStimth12);
					appVars.getDBObject(sqlgovepfrs, 39, sqlStamth01);
					appVars.getDBObject(sqlgovepfrs, 40, sqlStamth02);
					appVars.getDBObject(sqlgovepfrs, 41, sqlStamth03);
					appVars.getDBObject(sqlgovepfrs, 42, sqlStamth04);
					appVars.getDBObject(sqlgovepfrs, 43, sqlStamth05);
					appVars.getDBObject(sqlgovepfrs, 44, sqlStamth06);
					appVars.getDBObject(sqlgovepfrs, 45, sqlStamth07);
					appVars.getDBObject(sqlgovepfrs, 46, sqlStamth08);
					appVars.getDBObject(sqlgovepfrs, 47, sqlStamth09);
					appVars.getDBObject(sqlgovepfrs, 48, sqlStamth10);
					appVars.getDBObject(sqlgovepfrs, 49, sqlStamth11);
					appVars.getDBObject(sqlgovepfrs, 50, sqlStamth12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		tmpGoverec.set(sqlGoverec);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
		if (isNE(wsaaFrecord,"Y")) {
			h500ReadGvacsts();
			h600SetHeading();
			h100NewPage();
			h200WriteDetail();
		}
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
		writeDetail3080();
	}

protected void update3010()
	{
		for (mth.set(1); !(isGT(mth,pj517par.acctmnth)); mth.add(1)){
			wsaaStcamt.add(tmpStcmth[mth.toInt()]);
			wsaaStiamt.add(tmpStimth[mth.toInt()]);
			wsaaStaamt.add(tmpStamth[mth.toInt()]);
		}
		if (isNE(sqlAcctccy,sqlCntcurr)) {
			h300CheckRate();
		}
	}

protected void writeDetail3080()
	{
		if (isEQ(wsaaFrecord,"Y")) {
			wsaaFrecord = "N";
		}
		else {
			if (isNE(wsaaRegister,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaCompany,sqlChdrcoy)
			|| isNE(wsaaStsect,sqlStsect)
			|| isNE(wsaaStssect,sqlStssect)
			|| isNE(wsaaBranch,sqlCntbranch)) {
				h500ReadGvacsts();
				h600SetHeading();
				h100NewPage();
				h200WriteDetail();
			}
		}
		if (isEQ(sqlStatcat,wsaaNb)) {
			wsaaStcmthNb.add(wsaaStcamt);
			wsaaStcmthAddTotal.add(wsaaStcamt);
			wsaaStimthNb.add(wsaaStiamt);
			wsaaStimthAddTotal.add(wsaaStiamt);
			wsaaStamthNb.add(wsaaStaamt);
			wsaaStamthAddTotal.add(wsaaStaamt);
		}
		else {
			if (isEQ(sqlStatcat,wsaaRi)) {
				wsaaStcmthRi.add(wsaaStcamt);
				wsaaStcmthLa.add(wsaaStcamt);
				wsaaStcmthAddTotal.add(wsaaStcamt);
				wsaaStcmthDelTotal.add(wsaaStcamt);
				wsaaStimthRi.add(wsaaStiamt);
				wsaaStimthLa.add(wsaaStiamt);
				wsaaStimthAddTotal.add(wsaaStiamt);
				wsaaStimthDelTotal.add(wsaaStiamt);
				wsaaStamthRi.add(wsaaStaamt);
				wsaaStamthLa.add(wsaaStaamt);
				wsaaStamthAddTotal.add(wsaaStaamt);
				wsaaStamthDelTotal.add(wsaaStaamt);
			}
			else {
				if (isEQ(sqlStatcat,wsaaIn)) {
					wsaaStcmthIn.add(wsaaStcamt);
					wsaaStcmthAddTotal.add(wsaaStcamt);
					wsaaStimthIn.add(wsaaStiamt);
					wsaaStimthAddTotal.add(wsaaStiamt);
					wsaaStamthIn.add(wsaaStaamt);
					wsaaStamthAddTotal.add(wsaaStaamt);
				}
				else {
					if (isEQ(sqlStatcat,wsaaRd)
					|| isEQ(sqlStatcat,wsaaDh)) {
						wsaaStcmthDth.add(wsaaStcamt);
						wsaaStcmthDelTotal.add(wsaaStcamt);
						wsaaStimthDth.add(wsaaStiamt);
						wsaaStimthDelTotal.add(wsaaStiamt);
						wsaaStamthDth.add(wsaaStaamt);
						wsaaStamthDelTotal.add(wsaaStaamt);
					}
					else {
						if (isEQ(sqlStatcat,wsaaMa)) {
							wsaaStcmthMa.add(wsaaStcamt);
							wsaaStcmthDelTotal.add(wsaaStcamt);
							wsaaStimthMa.add(wsaaStiamt);
							wsaaStimthDelTotal.add(wsaaStiamt);
							wsaaStamthMa.add(wsaaStaamt);
							wsaaStamthDelTotal.add(wsaaStaamt);
						}
						else {
							if (isEQ(sqlStatcat,wsaaEx)) {
								wsaaStcmthEx.add(wsaaStcamt);
								wsaaStcmthDelTotal.add(wsaaStcamt);
								wsaaStimthEx.add(wsaaStiamt);
								wsaaStimthDelTotal.add(wsaaStiamt);
								wsaaStamthEx.add(wsaaStaamt);
								wsaaStamthDelTotal.add(wsaaStaamt);
							}
							else {
								if (isEQ(sqlStatcat,wsaaSu)) {
									wsaaStcmthSu.add(wsaaStcamt);
									wsaaStcmthDelTotal.add(wsaaStcamt);
									wsaaStimthSu.add(wsaaStiamt);
									wsaaStimthDelTotal.add(wsaaStiamt);
									wsaaStamthSu.add(wsaaStaamt);
									wsaaStamthDelTotal.add(wsaaStaamt);
								}
								else {
									if (isEQ(sqlStatcat,wsaaDe)
									|| isEQ(sqlStatcat,wsaaLc)) {
										wsaaStcmthDe.add(wsaaStcamt);
										wsaaStcmthDelTotal.add(wsaaStcamt);
										wsaaStimthDe.add(wsaaStiamt);
										wsaaStimthDelTotal.add(wsaaStiamt);
										wsaaStamthDe.add(wsaaStaamt);
										wsaaStamthDelTotal.add(wsaaStaamt);
									}
									else {
										if (isEQ(sqlStatcat,wsaaLa)) {
											wsaaStcmthLa.add(wsaaStcamt);
											wsaaStcmthDelTotal.add(wsaaStcamt);
											wsaaStimthLa.add(wsaaStiamt);
											wsaaStimthDelTotal.add(wsaaStiamt);
											wsaaStamthLa.add(wsaaStaamt);
											wsaaStamthDelTotal.add(wsaaStaamt);
										}
										else {
											if (isEQ(sqlStatcat,wsaaCf)) {
												wsaaStcmthCf.add(wsaaStcamt);
												wsaaStcmthDelTotal.add(wsaaStcamt);
												wsaaStimthCf.add(wsaaStiamt);
												wsaaStimthDelTotal.add(wsaaStiamt);
												wsaaStamthCf.add(wsaaStaamt);
												wsaaStamthDelTotal.add(wsaaStaamt);
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		compute(wsaaAmount, 0).set(add(wsaaStcmthAddTotal,wsaaStcmthDelTotal));
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaAmount);
		callContot001();
		compute(wsaaAmount, 2).set(add(wsaaStimthAddTotal,wsaaStimthDelTotal));
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaAmount);
		callContot001();
		compute(wsaaAmount, 2).set(add(wsaaStamthAddTotal,wsaaStamthDelTotal));
		contotrec.totno.set(ct05);
		contotrec.totval.set(wsaaAmount);
		callContot001();
		wsaaRegister.set(sqlRegister);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaCompany.set(sqlChdrcoy);
		wsaaBranch.set(sqlCntbranch);
		wsaaStssect.set(sqlStssect);
		wsaaStsect.set(sqlStsect);
		wsaaStcamt.set(ZERO);
		wsaaStiamt.set(ZERO);
		wsaaStaamt.set(ZERO);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (selectAll.isTrue()) {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfAllconn, sqlgovepfAllps, sqlgovepfAllrs);
		}
		else {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfconn, sqlgovepfps, sqlgovepfrs);
		}
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void h100NewPage()
	{
		/*H110-START*/
		printerFile.printRj528h01(rj528H01);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*H190-EXIT*/
	}

protected void h200WriteDetail()
	{
		h210Start();
	}

protected void h210Start()
	{
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(11);
		stcmth.set(ZERO);
		repamt01.set(ZERO);
		repamt02.set(ZERO);
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(12);
		stcmth.set(wsaaStcmthNb);
		compute(repamt01, 3).setRounded(div(wsaaStimthNb,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthNb,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(13);
		stcmth.set(wsaaStcmthRi);
		compute(repamt01, 3).setRounded(div(wsaaStimthRi,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthRi,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(14);
		stcmth.set(wsaaStcmthIn);
		compute(repamt01, 3).setRounded(div(wsaaStimthIn,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthIn,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(15);
		stcmth.set(ZERO);
		repamt01.set(ZERO);
		repamt02.set(ZERO);
		compute(repamt03, 3).setRounded(div(wsaaBonusAllotted,1000));
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(16);
		stcmth.set(wsaaStcmthAddTotal);
		compute(repamt01, 3).setRounded(div(wsaaStimthAddTotal,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthAddTotal,1000));
		compute(repamt03, 3).setRounded(div(wsaaBonusAlloTotal,1000));
		printerFile.printRj528d01(rj528D01);
		printerFile.printRj528d02(rj528D02);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(17);
		stcmth.set(ZERO);
		repamt01.set(ZERO);
		repamt02.set(ZERO);
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(18);
		stcmth.set(wsaaStcmthDth);
		compute(repamt01, 3).setRounded(div(wsaaStimthDth,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthDth,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(19);
		stcmth.set(wsaaStcmthMa);
		compute(repamt01, 3).setRounded(div(wsaaStimthMa,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthMa,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(20);
		stcmth.set(wsaaStcmthEx);
		compute(repamt01, 3).setRounded(div(wsaaStimthEx,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthEx,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(21);
		stcmth.set(wsaaStcmthSu);
		compute(repamt01, 3).setRounded(div(wsaaStimthSu,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthSu,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(22);
		stcmth.set(ZERO);
		repamt01.set(ZERO);
		repamt02.set(ZERO);
		compute(repamt03, 1).setRounded(div(wsaaBonusSurrender,1000));
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(23);
		stcmth.set(wsaaStcmthDe);
		compute(repamt01, 3).setRounded(div(wsaaStimthDe,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthDe,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(24);
		stcmth.set(wsaaStcmthCf);
		compute(repamt01, 3).setRounded(div(wsaaStimthCf,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthCf,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(25);
		stcmth.set(wsaaStcmthLa);
		compute(repamt01, 3).setRounded(div(wsaaStimthLa,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthLa,1000));
		repamt03.set(ZERO);
		printerFile.printRj528d01(rj528D01);
		for (wsaaSub.set(1); !(isGT(wsaaSub,25)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		indOn.setTrue(16);
		stcmth.set(wsaaStcmthDelTotal);
		compute(repamt01, 3).setRounded(div(wsaaStimthDelTotal,1000));
		compute(repamt02, 3).setRounded(div(wsaaStamthDelTotal,1000));
		compute(repamt03, 3).setRounded(div(wsaaBonusSurrTotal,1000));
		printerFile.printRj528d01(rj528D01);
		wsaaOverflow.set("Y");
		wsaaStcmthNb.set(ZERO);
		wsaaStimthNb.set(ZERO);
		wsaaStamthNb.set(ZERO);
		wsaaStcmthRi.set(ZERO);
		wsaaStimthRi.set(ZERO);
		wsaaStamthRi.set(ZERO);
		wsaaStcmthIn.set(ZERO);
		wsaaStimthIn.set(ZERO);
		wsaaStamthIn.set(ZERO);
		wsaaStcmthDth.set(ZERO);
		wsaaStimthDth.set(ZERO);
		wsaaStamthDth.set(ZERO);
		wsaaStcmthMa.set(ZERO);
		wsaaStimthMa.set(ZERO);
		wsaaStamthMa.set(ZERO);
		wsaaStcmthEx.set(ZERO);
		wsaaStimthEx.set(ZERO);
		wsaaStamthEx.set(ZERO);
		wsaaStcmthSu.set(ZERO);
		wsaaStimthSu.set(ZERO);
		wsaaStamthSu.set(ZERO);
		wsaaStcmthDe.set(ZERO);
		wsaaStimthDe.set(ZERO);
		wsaaStamthDe.set(ZERO);
		wsaaStcmthCf.set(ZERO);
		wsaaStimthCf.set(ZERO);
		wsaaStamthCf.set(ZERO);
		wsaaStcmthLa.set(ZERO);
		wsaaStimthLa.set(ZERO);
		wsaaStamthLa.set(ZERO);
		wsaaBonusSurrender.set(ZERO);
		wsaaBonusAllotted.set(ZERO);
		wsaaStcmthAddTotal.set(ZERO);
		wsaaStimthAddTotal.set(ZERO);
		wsaaStamthAddTotal.set(ZERO);
		wsaaStcmthDelTotal.set(ZERO);
		wsaaStimthDelTotal.set(ZERO);
		wsaaStamthDelTotal.set(ZERO);
		wsaaBonusAlloTotal.set(ZERO);
		wsaaBonusSurrTotal.set(ZERO);
	}

protected void h300CheckRate()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					h310Start();
				}
				case h320CallItemio: {
					h320CallItemio();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void h310Start()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemitem(sqlCntcurr);
		itemIO.setItemtabl(t3629);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
	}

protected void h320CallItemio()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaScrate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaScrate,ZERO)
		|| isGT(wsaaX,7))) {
			if (isGTE(datcon1rec.intDate,t3629rec.frmdate[wsaaX.toInt()])
			&& isLTE(datcon1rec.intDate,t3629rec.todate[wsaaX.toInt()])) {
				wsaaScrate.set(t3629rec.scrate[wsaaX.toInt()]);
			}
			else {
				wsaaX.add(1);
			}
		}

		if (isEQ(wsaaScrate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.h320CallItemio);
			}
			else {
				syserrrec.statuz.set(g418);
				fatalError600();
			}
		}
		compute(wsaaStiamt, 8).setRounded(mult(tmpStimth[pj517par.acctmnth.toInt()],wsaaScrate));
		compute(wsaaStaamt, 8).setRounded(mult(tmpStamth[pj517par.acctmnth.toInt()],wsaaScrate));
	}

protected void h500ReadGvacsts()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					h510Start();
				}
				case h520Read: {
					h520Read();
				}
				case h590Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void h510Start()
	{
		gvacstsIO.setParams(SPACES);
		gvacstsIO.setChdrcoy(wsaaCompany);
		gvacstsIO.setCntbranch(wsaaBranch);
		gvacstsIO.setAcctyr(pj517par.acctyr);
		gvacstsIO.setAcctccy(wsaaAcctccy);
		gvacstsIO.setRegister(wsaaRegister);
		gvacstsIO.setStatSect(wsaaStsect);
		gvacstsIO.setStatSubsect(wsaaStssect);
		gvacstsIO.setCnttype(SPACES);
		gvacstsIO.setCrtable(SPACES);
		gvacstsIO.setCovPremStat(SPACES);
		gvacstsIO.setCnPremStat(SPACES);
		gvacstsIO.setFunction(varcom.begn);
		gvacstsIO.setFormat(gvacstsrec);
	}

protected void h520Read()
	{


	//performance improvement --  atiwari23
    gvacstsIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
    gvacstsIO.setFitKeysSearch("CHDRCOY","ACCTYR","CNTBRANCH","ACCTCCY","REG","STSECT");

		SmartFileCode.execute(appVars, gvacstsIO);
		if (isNE(gvacstsIO.getStatuz(),varcom.oK)
		&& isNE(gvacstsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(gvacstsIO.getParams());
			fatalError600();
		}
		if (isEQ(gvacstsIO.getStatuz(),varcom.endp)
		|| isNE(wsaaAcctyr,gvacstsIO.getAcctyr())
		|| isNE(wsaaRegister,gvacstsIO.getRegister())
		|| isNE(wsaaAcctccy,gvacstsIO.getAcctccy())
		|| isNE(wsaaCompany,gvacstsIO.getChdrcoy())
		|| isNE(wsaaBranch,gvacstsIO.getCntbranch())
		|| isNE(wsaaStsect,gvacstsIO.getStatSect())
		|| isNE(wsaaStssect,gvacstsIO.getStatSubsect())) {
			goTo(GotoLabel.h590Exit);
		}
		for (mth.set(1); !(isGT(mth,pj517par.acctmnth)); mth.add(1)){
			wsaaBonusAllotted.add(gvacstsIO.getStaccrb(mth));
			wsaaBonusAlloTotal.add(gvacstsIO.getStaccrb(mth));
			wsaaBonusSurrender.add(gvacstsIO.getStaccbs(mth));
			wsaaBonusSurrTotal.add(gvacstsIO.getStaccbs(mth));
		}
		gvacstsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.h520Read);
	}

protected void h600SetHeading()
	{
		h610Start();
	}

protected void h610Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(wsaaCompany);
		rh01Companynm.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(wsaaAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		acctccy.set(wsaaAcctccy);
		currdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3589);
		descIO.setDescitem(wsaaRegister);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		register.set(wsaaRegister);
		descrip.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5685);
		descIO.setDescitem(wsaaStsect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stsect.set(wsaaStsect);
		itmdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5684);
		descIO.setDescitem(wsaaStssect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stssect.set(wsaaStssect);
		longdesc.set(descIO.getLongdesc());
	}
}
