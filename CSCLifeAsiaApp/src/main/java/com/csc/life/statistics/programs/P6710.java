/*
 * File: P6710.java
 * Date: 30 August 2009 0:54:35
 * Author: Quipoz Limited
 * 
 * Class transformed from P6710.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.statistics.dataaccess.GojnTableDAM;
import com.csc.life.statistics.dataaccess.GovrTableDAM;
import com.csc.life.statistics.screens.S6710ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*        CREATE GOVERNMENT STATISTICAL JOURNAL  -  SCREEN 1
*
* This program accepts the agent statistical accumulation key via
* screen S6710. A journal record is created in GOJN file for every
* transaction committed. The key (GOVR-DATA-KEY) entered will be
* kept and passed to the next program using KEEPS on GOVR and
* GOJN files.
*
* If the GOVR record is not found for the key entered, a new record
* with the new key would be created in GOVR file. The initialisation
* of the new record is done in this program before a KEEPS action is
* performed on GOVR file.
*
* Validation performed:
*
* - Accounting year and month must not be zeroes. It is mandatory
*   that user enter these fields.
*
* - It is possible to enter a journal record to update GOVR file
*   regardless of whether the accounting period is opened or closed.
*
* FUNCTION KEYS:
*               <EXIT>  Return to submenu.
*
*               <ENTER> Call the next update program P6713 to allow
*                       user to update the amount fields in GOVR file.
*
* FILES USED:
*               GOVRSKM
*               GOJNSKM
*
*
*****************************************************************
* </pre>
*/
public class P6710 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6710");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
		/* ERRORS */
	private String e186 = "E186";
		/* FORMATS */
	private String govrrec = "GOVRREC";
	private String gojnrec = "GOJNREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Government Statistical Journal*/
	private GojnTableDAM gojnIO = new GojnTableDAM();
		/*Government Statistical Accumulation*/
	private GovrTableDAM govrIO = new GovrTableDAM();
	private S6710ScreenVars sv = ScreenProgram.getScreenVars( S6710ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		preExit, 
		exit3090, 
		exit4090
	}

	public P6710() {
		super();
		screenVars = sv;
		new ScreenModel("S6710", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.acctyr.set(ZERO);
		sv.acmn.set(ZERO);
		sv.commyr.set(ZERO);
		sv.company.set(wsspcomn.company);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		/*SCREEN-IO*/
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo12010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo12010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(sv.acctyr,ZERO)) {
			sv.acctyrErr.set(e186);
		}
		if (isEQ(sv.acmn,ZERO)
		|| isGT(sv.acmn,12)) {
			sv.acmnErr.set(e186);
		}
		if (isEQ(sv.commyr,ZERO)) {
			sv.commyrErr.set(e186);
		}
		if (isEQ(sv.statcat,SPACES)) {
			sv.statcatErr.set(e186);
		}
		if (isEQ(sv.statFund,SPACES)) {
			sv.stfundErr.set(e186);
		}
		if (isEQ(sv.statSect,SPACES)) {
			sv.stsectErr.set(e186);
		}
		if (isEQ(sv.stsubsect,SPACES)) {
			sv.stsubsectErr.set(e186);
		}
		if (isEQ(sv.register,SPACES)) {
			sv.registerErr.set(e186);
		}
		if (isEQ(sv.cntbranch,SPACES)) {
			sv.cntbranchErr.set(e186);
		}
		if (isEQ(sv.pstatcd,SPACES)) {
			sv.pstatcdErr.set(e186);
		}
		if (isEQ(sv.cntcurr,SPACES)) {
			sv.cntcurrErr.set(e186);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			keepRecord2100();
		}
		/*EXIT*/
	}

protected void keepRecord2100()
	{
		moveScreenToFiles2110();
		keepGovrRecord2120();
		keepGojnRecord2120();
	}

protected void moveScreenToFiles2110()
	{
		govrIO.setChdrcoy(wsspcomn.company);
		gojnIO.setChdrcoy(wsspcomn.company);
		govrIO.setStatcat(sv.statcat);
		gojnIO.setStatcat(sv.statcat);
		govrIO.setAcctyr(sv.acctyr);
		gojnIO.setAcctyr(sv.acctyr);
		govrIO.setCommyr(sv.commyr);
		gojnIO.setCommyr(sv.commyr);
		govrIO.setStatFund(sv.statFund);
		gojnIO.setStatFund(sv.statFund);
		govrIO.setStatSect(sv.statSect);
		gojnIO.setStatSect(sv.statSect);
		govrIO.setStatSubsect(sv.stsubsect);
		gojnIO.setStatSubsect(sv.stsubsect);
		govrIO.setRegister(sv.register);
		gojnIO.setRegister(sv.register);
		govrIO.setCntbranch(sv.cntbranch);
		gojnIO.setCntbranch(sv.cntbranch);
		govrIO.setBandage(sv.bandage);
		gojnIO.setBandage(sv.bandage);
		govrIO.setBandsa(sv.bandsa);
		gojnIO.setBandsa(sv.bandsa);
		govrIO.setBandprm(sv.bandprm);
		gojnIO.setBandprm(sv.bandprm);
		govrIO.setBandtrm(sv.bandtrm);
		gojnIO.setBandtrm(sv.bandtrm);
		govrIO.setCntcurr(sv.cntcurr);
		gojnIO.setCntcurr(sv.cntcurr);
		govrIO.setPstatcode(sv.pstatcd);
		gojnIO.setPstatcode(sv.pstatcd);
		gojnIO.setAcctmonth(sv.acmn);
		gojnIO.setTransactionDate(ZERO);
		gojnIO.setTransactionTime(ZERO);
		gojnIO.setEffdate(ZERO);
		gojnIO.setUser(ZERO);
		gojnIO.setStcmth(ZERO);
		gojnIO.setStpmth(ZERO);
		gojnIO.setStsmth(ZERO);
		gojnIO.setStimth(ZERO);
	}

protected void keepGovrRecord2120()
	{
		govrIO.setFormat(govrrec);
		govrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)
		&& isNE(govrIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(govrIO.getParams());
			fatalError600();
		}
		if (isEQ(govrIO.getStatuz(),varcom.mrnf)) {
			wsaaI.set(1);
			govrIO.setBfwdc(ZERO);
			govrIO.setCfwdc(ZERO);
			govrIO.setBfwdp(ZERO);
			govrIO.setCfwdp(ZERO);
			govrIO.setBfwds(ZERO);
			govrIO.setCfwds(ZERO);
			govrIO.setBfwdi(ZERO);
			govrIO.setCfwdi(ZERO);
			for (wsaaI.set(1); !(isGT(wsaaI,12)); wsaaI.add(1)){
				initialiseGovrValues2200();
			}
		}
		govrIO.setFormat(govrrec);
		govrIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(govrIO.getParams());
			fatalError600();
		}
	}

protected void keepGojnRecord2120()
	{
		gojnIO.setFormat(gojnrec);
		gojnIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, gojnIO);
		if (isNE(gojnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gojnIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void initialiseGovrValues2200()
	{
		/*PARA*/
		govrIO.setStcmth(wsaaI, ZERO);
		govrIO.setStpmth(wsaaI, ZERO);
		govrIO.setStsmth(wsaaI, ZERO);
		govrIO.setStimth(wsaaI, ZERO);
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*bug #ILIFE-1070 start*/
		//wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		/*bug #ILIFE-1070 end*/
		wsspcomn.programPtr.add(1);
	}
}
