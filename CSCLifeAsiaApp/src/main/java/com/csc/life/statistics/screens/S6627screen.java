package com.csc.life.statistics.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6627screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6627ScreenVars sv = (S6627ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6627screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6627ScreenVars screenVars = (S6627ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.statband01.setClassString("");
		screenVars.statband02.setClassString("");
		screenVars.statband03.setClassString("");
		screenVars.statband04.setClassString("");
		screenVars.statband05.setClassString("");
		screenVars.statband06.setClassString("");
		screenVars.statband07.setClassString("");
		screenVars.statband08.setClassString("");
		screenVars.statband09.setClassString("");
		screenVars.statband10.setClassString("");
		screenVars.statband11.setClassString("");
		screenVars.statband12.setClassString("");
		screenVars.statband13.setClassString("");
		screenVars.statband14.setClassString("");
		screenVars.statband15.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.statfrom01.setClassString("");
		screenVars.statfrom02.setClassString("");
		screenVars.statfrom03.setClassString("");
		screenVars.statfrom04.setClassString("");
		screenVars.statfrom05.setClassString("");
		screenVars.statfrom06.setClassString("");
		screenVars.statfrom07.setClassString("");
		screenVars.statfrom08.setClassString("");
		screenVars.statfrom09.setClassString("");
		screenVars.statfrom10.setClassString("");
		screenVars.statfrom11.setClassString("");
		screenVars.statfrom12.setClassString("");
		screenVars.statfrom13.setClassString("");
		screenVars.statfrom14.setClassString("");
		screenVars.statfrom15.setClassString("");
		screenVars.statto01.setClassString("");
		screenVars.statto02.setClassString("");
		screenVars.statto03.setClassString("");
		screenVars.statto04.setClassString("");
		screenVars.statto05.setClassString("");
		screenVars.statto06.setClassString("");
		screenVars.statto07.setClassString("");
		screenVars.statto08.setClassString("");
		screenVars.statto09.setClassString("");
		screenVars.statto10.setClassString("");
		screenVars.statto11.setClassString("");
		screenVars.statto12.setClassString("");
		screenVars.statto13.setClassString("");
		screenVars.statto14.setClassString("");
		screenVars.statto15.setClassString("");
	}

/**
 * Clear all the variables in S6627screen
 */
	public static void clear(VarModel pv) {
		S6627ScreenVars screenVars = (S6627ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.statband01.clear();
		screenVars.statband02.clear();
		screenVars.statband03.clear();
		screenVars.statband04.clear();
		screenVars.statband05.clear();
		screenVars.statband06.clear();
		screenVars.statband07.clear();
		screenVars.statband08.clear();
		screenVars.statband09.clear();
		screenVars.statband10.clear();
		screenVars.statband11.clear();
		screenVars.statband12.clear();
		screenVars.statband13.clear();
		screenVars.statband14.clear();
		screenVars.statband15.clear();
		screenVars.longdesc.clear();
		screenVars.statfrom01.clear();
		screenVars.statfrom02.clear();
		screenVars.statfrom03.clear();
		screenVars.statfrom04.clear();
		screenVars.statfrom05.clear();
		screenVars.statfrom06.clear();
		screenVars.statfrom07.clear();
		screenVars.statfrom08.clear();
		screenVars.statfrom09.clear();
		screenVars.statfrom10.clear();
		screenVars.statfrom11.clear();
		screenVars.statfrom12.clear();
		screenVars.statfrom13.clear();
		screenVars.statfrom14.clear();
		screenVars.statfrom15.clear();
		screenVars.statto01.clear();
		screenVars.statto02.clear();
		screenVars.statto03.clear();
		screenVars.statto04.clear();
		screenVars.statto05.clear();
		screenVars.statto06.clear();
		screenVars.statto07.clear();
		screenVars.statto08.clear();
		screenVars.statto09.clear();
		screenVars.statto10.clear();
		screenVars.statto11.clear();
		screenVars.statto12.clear();
		screenVars.statto13.clear();
		screenVars.statto14.clear();
		screenVars.statto15.clear();
	}
}
