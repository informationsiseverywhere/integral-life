package com.csc.life.statistics.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.statistics.dataaccess.dao.SttrpfDAO;
import com.csc.life.statistics.dataaccess.model.Sttrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class SttrpfDAOImpl extends BaseDAOImpl<Sttrpf> implements SttrpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(SttrpfDAOImpl.class);

    public List<Sttrpf> searchSttrpfRecord(String chdrcoy, String chdrnum) {
        String sqlSelect = "SELECT * FROM STTRPF WHERE CHDRCOY = ? AND CHDRNUM = ? ORDER BY CHDRCOY ASC, CHDRNUM ASC, TRANNO DESC, UNIQUE_NUMBER ASC";
        PreparedStatement psSelect = getPrepareStatement(sqlSelect);
        ResultSet sqlprs = null;
        List<Sttrpf> sttrpfList = new ArrayList<>();
        try {
            psSelect.setString(1, chdrcoy);
            psSelect.setString(2, chdrnum);
            sqlprs = executeQuery(psSelect);
            while (sqlprs.next()) {
                Sttrpf s = new Sttrpf();
                s.setChdrpfx(sqlprs.getString("CHDRPFX"));
                s.setChdrcoy(sqlprs.getString("CHDRCOY"));
                s.setChdrnum(sqlprs.getString("CHDRNUM"));
                s.setTranno(sqlprs.getInt("TRANNO"));
                s.setBatccoy(sqlprs.getString("BATCCOY"));
                s.setBatcbrn(sqlprs.getString("BATCBRN"));
                s.setBatcactyr(sqlprs.getInt("BATCACTYR"));
                s.setBatcactmn(sqlprs.getInt("BATCACTMN"));
                s.setBatctrcde(sqlprs.getString("BATCTRCDE"));
                s.setBatcbatch(sqlprs.getString("BATCBATCH"));
                s.setRevtrcde(sqlprs.getString("REVTRCDE"));
                s.setBillfreq(sqlprs.getString("BILLFREQ"));
                s.setStatgov(sqlprs.getString("STATGOV"));
                s.setStatagt(sqlprs.getString("STATAGT"));
                s.setAgntnum(sqlprs.getString("AGNTNUM"));
                s.setAracde(sqlprs.getString("ARACDE"));
                s.setCntbranch(sqlprs.getString("CNTBRANCH"));
                s.setBandage(sqlprs.getString("BANDAGE"));
                s.setBandsa(sqlprs.getString("BANDSA"));
                s.setBandprm(sqlprs.getString("BANDPRM"));
                s.setBandtrm(sqlprs.getString("BANDTRM"));
                s.setCnttype(sqlprs.getString("CNTTYPE"));
                s.setCrtable(sqlprs.getString("CRTABLE"));
                s.setParind(sqlprs.getString("PARIND"));
                s.setCommyr(sqlprs.getInt("COMMYR"));
                s.setCnPremStat(sqlprs.getString("CNPSTAT"));
                s.setPstatcode(sqlprs.getString("PSTATCODE"));
                s.setCntcurr(sqlprs.getString("CNTCURR"));
                s.setRegister(sqlprs.getString("REG"));
                s.setStatFund(sqlprs.getString("STFUND"));
                s.setStatSect(sqlprs.getString("STSECT"));
                s.setStatSubsect(sqlprs.getString("STSSECT"));
                s.setBandageg(sqlprs.getString("BANDAGEG"));
                s.setBandsag(sqlprs.getString("BANDSAG"));
                s.setBandprmg(sqlprs.getString("BANDPRMG"));
                s.setBandtrmg(sqlprs.getString("BANDTRMG"));
                s.setSrcebus(sqlprs.getString("SRCEBUS"));
                s.setSex(sqlprs.getString("SEX"));
                s.setMortcls(sqlprs.getString("MORTCLS"));
                s.setReptcd01(sqlprs.getString("REPTCD01"));
                s.setReptcd02(sqlprs.getString("REPTCD02"));
                s.setReptcd03(sqlprs.getString("REPTCD03"));
                s.setReptcd04(sqlprs.getString("REPTCD04"));
                s.setReptcd05(sqlprs.getString("REPTCD05"));
                s.setReptcd06(sqlprs.getString("REPTCD06"));
                s.setChdrstcda(sqlprs.getString("STCA"));
                s.setChdrstcdb(sqlprs.getString("STCB"));
                s.setChdrstcdc(sqlprs.getString("STCC"));
                s.setChdrstcdd(sqlprs.getString("STCD"));
                s.setChdrstcde(sqlprs.getString("STCE"));
                s.setOvrdcat(sqlprs.getString("OVRDCAT"));
                s.setStatcat(sqlprs.getString("STATCAT"));
                s.setStcmth(sqlprs.getBigDecimal("STCMTH"));
                s.setStvmth(sqlprs.getBigDecimal("STVMTH"));
                s.setStpmth(sqlprs.getBigDecimal("STPMTH"));
                s.setStsmth(sqlprs.getBigDecimal("STSMTH"));
                s.setStmmth(sqlprs.getBigDecimal("STMMTH"));
                s.setStimth(sqlprs.getBigDecimal("STIMTH"));
                s.setStcmthg(sqlprs.getBigDecimal("STCMTHG"));
                s.setStpmthg(sqlprs.getBigDecimal("STPMTHG"));
                s.setStsmthg(sqlprs.getBigDecimal("STSMTHG"));
                s.setStumth(sqlprs.getBigDecimal("STUMTH"));
                s.setStnmth(sqlprs.getBigDecimal("STNMTH"));
                s.setTransactionDate(sqlprs.getInt("TRDT"));
                s.setTrannor(sqlprs.getInt("TRANNOR"));
                s.setLife(sqlprs.getString("LIFE"));
                s.setCoverage(sqlprs.getString("COVERAGE"));
                s.setRider(sqlprs.getString("RIDER"));
                s.setPlanSuffix(sqlprs.getInt("PLNSFX"));
                s.setAcctccy(sqlprs.getString("ACCTCCY"));
                s.setStlmth(sqlprs.getBigDecimal("STLMTH"));
                s.setStbmthg(sqlprs.getBigDecimal("STBMTHG"));
                s.setStldmthg(sqlprs.getBigDecimal("STLDMTHG"));
                s.setStraamt(sqlprs.getBigDecimal("STRAAMT"));
                s.setUniqueNumber(sqlprs.getLong("UNIQUE_NUMBER"));
                sttrpfList.add(s);
            }

        } catch (SQLException e) {
            LOGGER.error("searchSttrpfRecord()", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(psSelect, sqlprs);
        }
        return sttrpfList;

    }

    public void insertSttrpfRecord(List<Sttrpf> insertSttrpfList) {
        if (insertSttrpfList != null && !insertSttrpfList.isEmpty()) {
            StringBuilder insertSql = new StringBuilder();
            insertSql
                    .append(" INSERT INTO STTRPF (CHDRPFX,CHDRCOY,CHDRNUM,TRANNO,BATCCOY,BATCBRN,BATCACTYR,BATCACTMN,BATCTRCDE,BATCBATCH,REVTRCDE,BILLFREQ,STATGOV,STATAGT,AGNTNUM,ARACDE,CNTBRANCH,BANDAGE,BANDSA,BANDPRM,BANDTRM,CNTTYPE,CRTABLE,PARIND,COMMYR,CNPSTAT,PSTATCODE,CNTCURR,REG,STFUND,STSECT,STSSECT,BANDAGEG,BANDSAG,BANDPRMG,BANDTRMG,SRCEBUS,SEX,MORTCLS,REPTCD01,REPTCD02,REPTCD03,REPTCD04,REPTCD05,REPTCD06,STCA,STCB,STCC,STCD,STCE,OVRDCAT,STATCAT,STCMTH,STVMTH,STPMTH,STSMTH,STMMTH,STIMTH,STCMTHG,STPMTHG,STSMTHG,STUMTH,STNMTH,TRDT,TRANNOR,LIFE,COVERAGE,RIDER,PLNSFX,ACCTCCY,STLMTH,STBMTHG,STLDMTHG,STRAAMT)");
            insertSql
                    .append(" VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
            PreparedStatement ps = getPrepareStatement(insertSql.toString());
            try {
                int i;
                for (Sttrpf s : insertSttrpfList) {
                    i = 1;
                    ps.setString(i++, s.getChdrpfx());
                    ps.setString(i++, s.getChdrcoy());
                    ps.setString(i++, s.getChdrnum());
                    ps.setInt(i++, s.getTranno());
                    ps.setString(i++, s.getBatccoy());
                    ps.setString(i++, s.getBatcbrn());
                    ps.setInt(i++, s.getBatcactyr());
                    ps.setInt(i++, s.getBatcactmn());
                    ps.setString(i++, s.getBatctrcde());
                    ps.setString(i++, s.getBatcbatch());
                    ps.setString(i++, s.getRevtrcde());
                    ps.setString(i++, s.getBillfreq());
                    ps.setString(i++, s.getStatgov());
                    ps.setString(i++, s.getStatagt());
                    ps.setString(i++, s.getAgntnum());
                    ps.setString(i++, s.getAracde());
                    ps.setString(i++, s.getCntbranch());
                    ps.setString(i++, s.getBandage());
                    ps.setString(i++, s.getBandsa());
                    ps.setString(i++, s.getBandprm());
                    ps.setString(i++, s.getBandtrm());
                    ps.setString(i++, s.getCnttype());
                    ps.setString(i++, s.getCrtable());
                    ps.setString(i++, s.getParind());
                    ps.setInt(i++, s.getCommyr());
                    ps.setString(i++, s.getCnPremStat());
                    ps.setString(i++, s.getPstatcode());
                    ps.setString(i++, s.getCntcurr());
                    ps.setString(i++, s.getRegister());
                    ps.setString(i++, s.getStatFund());
                    ps.setString(i++, s.getStatSect());
                    ps.setString(i++, s.getStatSubsect());
                    ps.setString(i++, s.getBandageg());
                    ps.setString(i++, s.getBandsag());
                    ps.setString(i++, s.getBandprmg());
                    ps.setString(i++, s.getBandtrmg());
                    ps.setString(i++, s.getSrcebus());
                    ps.setString(i++, s.getSex());
                    ps.setString(i++, s.getMortcls());
                    ps.setString(i++, s.getReptcd01());
                    ps.setString(i++, s.getReptcd02());
                    ps.setString(i++, s.getReptcd03());
                    ps.setString(i++, s.getReptcd04());
                    ps.setString(i++, s.getReptcd05());
                    ps.setString(i++, s.getReptcd06());
                    ps.setString(i++, s.getChdrstcda());
                    ps.setString(i++, s.getChdrstcdb());
                    ps.setString(i++, s.getChdrstcdc());
                    ps.setString(i++, s.getChdrstcdd());
                    ps.setString(i++, s.getChdrstcde());
                    ps.setString(i++, s.getOvrdcat());
                    ps.setString(i++, s.getStatcat());
                    ps.setBigDecimal(i++, s.getStcmth());
                    ps.setBigDecimal(i++, s.getStvmth());
                    ps.setBigDecimal(i++, s.getStpmth());
                    ps.setBigDecimal(i++, s.getStsmth());
                    ps.setBigDecimal(i++, s.getStmmth());
                    ps.setBigDecimal(i++, s.getStimth());
                    ps.setBigDecimal(i++, s.getStcmthg());
                    ps.setBigDecimal(i++, s.getStpmthg());
                    ps.setBigDecimal(i++, s.getStsmthg());
                    ps.setBigDecimal(i++, s.getStumth());
                    ps.setBigDecimal(i++, s.getStnmth());
                    ps.setInt(i++, s.getTransactionDate());
                    ps.setInt(i++, s.getTrannor());
                    ps.setString(i++, s.getLife());
                    ps.setString(i++, s.getCoverage());
                    ps.setString(i++, s.getRider());
                    ps.setInt(i++, s.getPlanSuffix());
                    ps.setString(i++, s.getAcctccy());
                    ps.setBigDecimal(i++, s.getStlmth());
                    ps.setBigDecimal(i++, s.getStbmthg());
                    ps.setBigDecimal(i++, s.getStldmthg());
                    ps.setBigDecimal(i++, s.getStraamt());
                    ps.addBatch();
                }
                ps.executeBatch();

            } catch (SQLException e) {
                LOGGER.error("insertSttrpgfRecord", e);//IJTI-1561
                throw new SQLRuntimeException(e);
            } finally {
                close(ps, null);
            }
        }
    }
}
