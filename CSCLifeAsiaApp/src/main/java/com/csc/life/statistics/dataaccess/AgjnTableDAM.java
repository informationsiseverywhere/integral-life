package com.csc.life.statistics.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgjnTableDAM.java
 * Date: Sun, 30 Aug 2009 03:28:31
 * Class transformed from AGJN.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgjnTableDAM extends AgjnpfTableDAM {

	public AgjnTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGJN");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", AGNTNUM"
		             + ", STATCAT"
		             + ", ACCTYR"
		             + ", ACCTMONTH"
		             + ", ARACDE"
		             + ", CNTBRANCH"
		             + ", BANDAGE"
		             + ", BANDSA"
		             + ", BANDPRM"
		             + ", BANDTRM"
		             + ", CNTTYPE"
		             + ", CRTABLE"
		             + ", OVRDCAT"
		             + ", PSTATCODE"
		             + ", CNTCURR"
		             + ", EFFDATE"
		             + ", TRTM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "AGNTNUM, " +
		            "ARACDE, " +
		            "CNTBRANCH, " +
		            "BANDAGE, " +
		            "BANDSA, " +
		            "BANDPRM, " +
		            "BANDTRM, " +
		            "CNTTYPE, " +
		            "CRTABLE, " +
		            "OVRDCAT, " +
		            "STATCAT, " +
		            "PSTATCODE, " +
		            "CNTCURR, " +
		            "ACCTYR, " +
		            "ACCTMONTH, " +
		            "STCMTH, " +
		            "STVMTH, " +
		            "STPMTH, " +
		            "STSMTH, " +
		            "STMMTH, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "EFFDATE, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "AGNTNUM ASC, " +
		            "STATCAT ASC, " +
		            "ACCTYR ASC, " +
		            "ACCTMONTH ASC, " +
		            "ARACDE ASC, " +
		            "CNTBRANCH ASC, " +
		            "BANDAGE ASC, " +
		            "BANDSA ASC, " +
		            "BANDPRM ASC, " +
		            "BANDTRM ASC, " +
		            "CNTTYPE ASC, " +
		            "CRTABLE ASC, " +
		            "OVRDCAT ASC, " +
		            "PSTATCODE ASC, " +
		            "CNTCURR ASC, " +
		            "EFFDATE DESC, " +
		            "TRTM DESC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "AGNTNUM DESC, " +
		            "STATCAT DESC, " +
		            "ACCTYR DESC, " +
		            "ACCTMONTH DESC, " +
		            "ARACDE DESC, " +
		            "CNTBRANCH DESC, " +
		            "BANDAGE DESC, " +
		            "BANDSA DESC, " +
		            "BANDPRM DESC, " +
		            "BANDTRM DESC, " +
		            "CNTTYPE DESC, " +
		            "CRTABLE DESC, " +
		            "OVRDCAT DESC, " +
		            "PSTATCODE DESC, " +
		            "CNTCURR DESC, " +
		            "EFFDATE ASC, " +
		            "TRTM ASC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               agntnum,
                               aracde,
                               cntbranch,
                               bandage,
                               bandsa,
                               bandprm,
                               bandtrm,
                               cnttype,
                               crtable,
                               ovrdcat,
                               statcat,
                               pstatcode,
                               cntcurr,
                               acctyr,
                               acctmonth,
                               stcmth,
                               stvmth,
                               stpmth,
                               stsmth,
                               stmmth,
                               transactionDate,
                               transactionTime,
                               user,
                               effdate,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(13);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getStatcat().toInternal()
					+ getAcctyr().toInternal()
					+ getAcctmonth().toInternal()
					+ getAracde().toInternal()
					+ getCntbranch().toInternal()
					+ getBandage().toInternal()
					+ getBandsa().toInternal()
					+ getBandprm().toInternal()
					+ getBandtrm().toInternal()
					+ getCnttype().toInternal()
					+ getCrtable().toInternal()
					+ getOvrdcat().toInternal()
					+ getPstatcode().toInternal()
					+ getCntcurr().toInternal()
					+ getEffdate().toInternal()
					+ getTransactionTime().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, statcat);
			what = ExternalData.chop(what, acctyr);
			what = ExternalData.chop(what, acctmonth);
			what = ExternalData.chop(what, aracde);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, bandage);
			what = ExternalData.chop(what, bandsa);
			what = ExternalData.chop(what, bandprm);
			what = ExternalData.chop(what, bandtrm);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, ovrdcat);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller130 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller140 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller150 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller160 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller230 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller241 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(agntnum.toInternal());
	nonKeyFiller30.setInternal(aracde.toInternal());
	nonKeyFiller40.setInternal(cntbranch.toInternal());
	nonKeyFiller50.setInternal(bandage.toInternal());
	nonKeyFiller60.setInternal(bandsa.toInternal());
	nonKeyFiller70.setInternal(bandprm.toInternal());
	nonKeyFiller80.setInternal(bandtrm.toInternal());
	nonKeyFiller90.setInternal(cnttype.toInternal());
	nonKeyFiller100.setInternal(crtable.toInternal());
	nonKeyFiller110.setInternal(ovrdcat.toInternal());
	nonKeyFiller120.setInternal(statcat.toInternal());
	nonKeyFiller130.setInternal(pstatcode.toInternal());
	nonKeyFiller140.setInternal(cntcurr.toInternal());
	nonKeyFiller150.setInternal(acctyr.toInternal());
	nonKeyFiller160.setInternal(acctmonth.toInternal());
	nonKeyFiller230.setInternal(transactionTime.toInternal());
	nonKeyFiller241.setInternal(effdate.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(145);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ nonKeyFiller130.toInternal()
					+ nonKeyFiller140.toInternal()
					+ nonKeyFiller150.toInternal()
					+ nonKeyFiller160.toInternal()
					+ getStcmth().toInternal()
					+ getStvmth().toInternal()
					+ getStpmth().toInternal()
					+ getStsmth().toInternal()
					+ getStmmth().toInternal()
					+ getTransactionDate().toInternal()
					+ nonKeyFiller230.toInternal()
					+ getUser().toInternal()
					+ nonKeyFiller241.toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, nonKeyFiller130);
			what = ExternalData.chop(what, nonKeyFiller140);
			what = ExternalData.chop(what, nonKeyFiller150);
			what = ExternalData.chop(what, nonKeyFiller160);
			what = ExternalData.chop(what, stcmth);
			what = ExternalData.chop(what, stvmth);
			what = ExternalData.chop(what, stpmth);
			what = ExternalData.chop(what, stsmth);
			what = ExternalData.chop(what, stmmth);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, nonKeyFiller230);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, nonKeyFiller241);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	public FixedLengthStringData getStatcat() {
		return statcat;
	}
	public void setStatcat(Object what) {
		statcat.set(what);
	}
	public PackedDecimalData getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(Object what) {
		setAcctyr(what, false);
	}
	public void setAcctyr(Object what, boolean rounded) {
		if (rounded)
			acctyr.setRounded(what);
		else
			acctyr.set(what);
	}
	public PackedDecimalData getAcctmonth() {
		return acctmonth;
	}
	public void setAcctmonth(Object what) {
		setAcctmonth(what, false);
	}
	public void setAcctmonth(Object what, boolean rounded) {
		if (rounded)
			acctmonth.setRounded(what);
		else
			acctmonth.set(what);
	}
	public FixedLengthStringData getAracde() {
		return aracde;
	}
	public void setAracde(Object what) {
		aracde.set(what);
	}
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}
	public FixedLengthStringData getBandage() {
		return bandage;
	}
	public void setBandage(Object what) {
		bandage.set(what);
	}
	public FixedLengthStringData getBandsa() {
		return bandsa;
	}
	public void setBandsa(Object what) {
		bandsa.set(what);
	}
	public FixedLengthStringData getBandprm() {
		return bandprm;
	}
	public void setBandprm(Object what) {
		bandprm.set(what);
	}
	public FixedLengthStringData getBandtrm() {
		return bandtrm;
	}
	public void setBandtrm(Object what) {
		bandtrm.set(what);
	}
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}
	public FixedLengthStringData getOvrdcat() {
		return ovrdcat;
	}
	public void setOvrdcat(Object what) {
		ovrdcat.set(what);
	}
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getStcmth() {
		return stcmth;
	}
	public void setStcmth(Object what) {
		setStcmth(what, false);
	}
	public void setStcmth(Object what, boolean rounded) {
		if (rounded)
			stcmth.setRounded(what);
		else
			stcmth.set(what);
	}	
	public PackedDecimalData getStvmth() {
		return stvmth;
	}
	public void setStvmth(Object what) {
		setStvmth(what, false);
	}
	public void setStvmth(Object what, boolean rounded) {
		if (rounded)
			stvmth.setRounded(what);
		else
			stvmth.set(what);
	}	
	public PackedDecimalData getStpmth() {
		return stpmth;
	}
	public void setStpmth(Object what) {
		setStpmth(what, false);
	}
	public void setStpmth(Object what, boolean rounded) {
		if (rounded)
			stpmth.setRounded(what);
		else
			stpmth.set(what);
	}	
	public PackedDecimalData getStsmth() {
		return stsmth;
	}
	public void setStsmth(Object what) {
		setStsmth(what, false);
	}
	public void setStsmth(Object what, boolean rounded) {
		if (rounded)
			stsmth.setRounded(what);
		else
			stsmth.set(what);
	}	
	public PackedDecimalData getStmmth() {
		return stmmth;
	}
	public void setStmmth(Object what) {
		setStmmth(what, false);
	}
	public void setStmmth(Object what, boolean rounded) {
		if (rounded)
			stmmth.setRounded(what);
		else
			stmmth.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		agntnum.clear();
		statcat.clear();
		acctyr.clear();
		acctmonth.clear();
		aracde.clear();
		cntbranch.clear();
		bandage.clear();
		bandsa.clear();
		bandprm.clear();
		bandtrm.clear();
		cnttype.clear();
		crtable.clear();
		ovrdcat.clear();
		pstatcode.clear();
		cntcurr.clear();
		effdate.clear();
		transactionTime.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		nonKeyFiller130.clear();
		nonKeyFiller140.clear();
		nonKeyFiller150.clear();
		nonKeyFiller160.clear();
		stcmth.clear();
		stvmth.clear();
		stpmth.clear();
		stsmth.clear();
		stmmth.clear();
		transactionDate.clear();
		nonKeyFiller230.clear();
		user.clear();
		nonKeyFiller241.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}