/*
 * File: Bm511.java
 * Date: 29 August 2009 21:48:16
 * Author: Quipoz Limited
 *
 * Class transformed from BM511.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.clients.procedures.Namadrs;
import com.csc.fsu.clients.recordstructures.Namadrsrec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.AgntlagTableDAM;
import com.csc.life.agents.dataaccess.MacfeffTableDAM;
import com.csc.life.agents.dataaccess.MapreffTableDAM;
import com.csc.life.statistics.reports.Rm511Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.FileSort;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.SortFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*  AGENT PREMIUM INCOME SUMMARY.
*
*  Overview
*  ========
*
*  This program will process all the agents production record
*  which is not later than the specified effective month/year.
*  Consequently it will generate a summary report of agents'
*  career stati and their premium production history on a
*  monthlly basis.
*
*  The details are sorted and presented for the approriate
*  company under the following criteria:
*
*     Company
*        Region
*           Branch
*              Agency
*                 Agent
*                    Agent Type
*
*  And subtotals are presented for respective Branch, Region
*  and Company.
*
*  Processing
*  ==========
*
*  1000-INITIALISE.
*
*  - Prepare output file RM511
*  - Preload constantly reference tables into working storages:
*    they are: T1692, T1693.
*
*  2000-READ-FILE
*  - Prepare a sort file of an ascending sequence of following:
*       Company
*          Region
*             Branch
*                Agency
*                   Reporting Level 01 (Self)
*                      Reporting Level (immediate)
*                         Reporting Level (secondary)
*                            Agent
*                               Agent Type
*    2100-READ-MAPREFF (Sort Input Procedure)
*    3100-PRINT-REPORT (Sort Output Procedure)
*
*  2100-READ-MAPREFF
*    Repeat 2200-READ-MAPREFFIO
*                  until WSSP-EDTERROR set to ENDP
*
*  2200-READ-MAPREFFIO
*    Read a MAPR record with process defined company
*                UNTIL file end or different company
*
*    Those records with accounting year/month later than
*    the batch accounting year/month will be excluded.
*
*    Read a MACF record using MAPR values for agent movement
*    type(career status) of a SORT record.
*
*    Release a SORT record from the saved temporary working
*    variables and flush them afterwards, when encounter
*    changes against previous record such as agent, agent type,
*    reporting levels, otherwise,
*
*    accumulate for policy counts, sum insured and personal
*    production premium(s) - 1st to 5 yr(s) and 6 yrs or greater
*    for an agent of an identical position from MAPR values and
*    store them into temporary working variables of both YTD and
*    MTD figures respectively.
*    (N.B. MTD figures only need to store for the record of
*    batch accounting month.)
*
*    Read a AGLF record using MAPR values to determine his/her
*    branch, region, payment currency.
*
*    When the record is no longer the process defined company,
*    Release the last SORT record and setup ENDP for
*    WSSP-EDRTERROR.
*
*  3100-PRINT-REPORT
*    Return the sort file
*              until file end
*
*    When there is any change of the following against the
*    stored values
*         Region, Branch, Agency, reporting levels
*    then
*         When there is any change of reporting levels,
*              Print for the previous stored agency unit subtotal
*              Flush the corresponding stored subtotal
*         When there is any change of agency,
*              Print for the previous stored agency unit subtotal
*              Print for the previous stored agency subtotal
*              Flush the corresponding stored subtotals
*         When there is any change of branch,
*              Print for the previous stored agency unit subtotal
*              Print for the previous stored agency subtotal
*              Print for the previous stored branch subtotal
*              Flush the corresponding stored subtotals
*         When there is any change of region,
*              Print for the previous stored agency unit subtotal
*              Print for the previous stored agency subtotal
*              Print for the previous stored branch subtotal
*              Print for the previous stored region subtotal
*              Flush the corresponding stored subtotals
*         Store up the current region, branch, agency amd
*         reporting levels
*
*    For each SORT record
*         Print a detail line for each agent which
*               featuring agent's current career status, sum
*               insured, premium production figures from
*               the current SORT record.
*
*   (N.B. for agent's name read a AGNT record using SORT values
*         then Return agent name using a subroutine 'NAMADRS'
*         and AGNT values)
*
*    And accumulate values for subtotals (agency unit, agency,
*    branch, region and company).
*
*    Print for the last sub totals (including company subtotal)
*
*  4000-CLOSE
*    CLOSE printer file
*
*****************************************************************
* </pre>
*/
public class Bm511 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rm511Report printerFile = new Rm511Report();
	private SortFileDAM sortFile = new SortFileDAM("SORT");
	private FixedLengthStringData printerRec = new FixedLengthStringData(512);

	private FixedLengthStringData sortRecord = new FixedLengthStringData(211);
	private FixedLengthStringData sortRegion = new FixedLengthStringData(3).isAPartOf(sortRecord, 0);
	private FixedLengthStringData sortBranch = new FixedLengthStringData(2).isAPartOf(sortRecord, 3);
	private FixedLengthStringData sortAgency = new FixedLengthStringData(8).isAPartOf(sortRecord, 5);
	private FixedLengthStringData sortAgencyType = new FixedLengthStringData(2).isAPartOf(sortRecord, 13);
	private ZonedDecimalData sortEffdate = new ZonedDecimalData(8, 0).isAPartOf(sortRecord, 15).setUnsigned();
	private FixedLengthStringData sortLevel01 = new FixedLengthStringData(8).isAPartOf(sortRecord, 23);
	private FixedLengthStringData sortLevel02 = new FixedLengthStringData(8).isAPartOf(sortRecord, 31);
	private FixedLengthStringData sortLevel03 = new FixedLengthStringData(8).isAPartOf(sortRecord, 39);
	private FixedLengthStringData sortActivityCode = new FixedLengthStringData(1).isAPartOf(sortRecord, 47);
	private FixedLengthStringData sortAgent = new FixedLengthStringData(8).isAPartOf(sortRecord, 48);
	private FixedLengthStringData sortAgentType = new FixedLengthStringData(2).isAPartOf(sortRecord, 56);
	private FixedLengthStringData sortAgentCcy = new FixedLengthStringData(3).isAPartOf(sortRecord, 58);
	private PackedDecimalData sortAgentPolMtd = new PackedDecimalData(4, 0).isAPartOf(sortRecord, 61);
	private PackedDecimalData sortAgent1ypMtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 64);
	private PackedDecimalData sortAgent2ypMtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 73);
	private PackedDecimalData sortAgent3ypMtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 82);
	private PackedDecimalData sortAgent4ypMtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 91);
	private PackedDecimalData sortAgent5ypMtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 100);
	private PackedDecimalData sortAgentRypMtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 109);
	private PackedDecimalData sortAgentSiMtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 118);
	private PackedDecimalData sortAgentTotMtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 127);
	private PackedDecimalData sortAgentPolYtd = new PackedDecimalData(4, 0).isAPartOf(sortRecord, 136);
	private PackedDecimalData sortAgent1ypYtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 139);
	private PackedDecimalData sortAgent2ypYtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 148);
	private PackedDecimalData sortAgent3ypYtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 157);
	private PackedDecimalData sortAgent4ypYtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 166);
	private PackedDecimalData sortAgent5ypYtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 175);
	private PackedDecimalData sortAgentRypYtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 184);
	private PackedDecimalData sortAgentSiYtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 193);
	private PackedDecimalData sortAgentTotYtd = new PackedDecimalData(17, 2).isAPartOf(sortRecord, 202);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BM511");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaRegion = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAgency = new FixedLengthStringData(8);
	private String wsaaAgencyType = "";
	private FixedLengthStringData wsaaLevel1 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLevel2 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLevel3 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLevel01 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLevel02 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaLevel03 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaActivityCode = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaAgent = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAgentType = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAgentCcy = new FixedLengthStringData(3);
	private PackedDecimalData wsaaAgentPolMtd = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaAgent1ypMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgent2ypMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgent3ypMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgent4ypMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgent5ypMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgentRypMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgentSiMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgentTotMtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgentPolYtd = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaAgent1ypYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgent2ypYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgent3ypYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgent4ypYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgent5ypYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgentRypYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgentSiYtd = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgentTotYtd = new PackedDecimalData(17, 2);
	private String wsaaFirstTime = "Y";
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex1 = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaSortAgencySave = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaSortBranchSave = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaSortRegionSave = new FixedLengthStringData(3).init(SPACES);

	private FixedLengthStringData wsaaReportags = new FixedLengthStringData(40);
	private FixedLengthStringData[] wsaaReportag = FLSArrayPartOfStructure(5, 8, wsaaReportags, 0);

	private FixedLengthStringData wsaaSaveLevel = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaSaveLevel01 = new FixedLengthStringData(8).isAPartOf(wsaaSaveLevel, 0);
	private FixedLengthStringData wsaaSaveLevel02 = new FixedLengthStringData(8).isAPartOf(wsaaSaveLevel, 8);

	private FixedLengthStringData wsaaSortLevel = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaSortLev01 = new FixedLengthStringData(8).isAPartOf(wsaaSortLevel, 0);
	private FixedLengthStringData wsaaSortLev02 = new FixedLengthStringData(8).isAPartOf(wsaaSortLevel, 8);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaTotalsTable = new FixedLengthStringData(750);
	private FixedLengthStringData[] wsaaTotalTab = FLSArrayPartOfStructure(5, 150, wsaaTotalsTable, 0);
	private PackedDecimalData[] wsaaTotalPolMtd = PDArrayPartOfArrayStructure(4, 0, wsaaTotalTab, 0);
	private PackedDecimalData[] wsaaTotal1ypMtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 3);
	private PackedDecimalData[] wsaaTotal2ypMtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 12);
	private PackedDecimalData[] wsaaTotal3ypMtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 21);
	private PackedDecimalData[] wsaaTotal4ypMtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 30);
	private PackedDecimalData[] wsaaTotal5ypMtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 39);
	private PackedDecimalData[] wsaaTotalRypMtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 48);
	private PackedDecimalData[] wsaaTotalSiMtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 57);
	private PackedDecimalData[] wsaaTotalTotMtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 66);
	private PackedDecimalData[] wsaaTotalPolYtd = PDArrayPartOfArrayStructure(4, 0, wsaaTotalTab, 75);
	private PackedDecimalData[] wsaaTotal1ypYtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 78);
	private PackedDecimalData[] wsaaTotal2ypYtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 87);
	private PackedDecimalData[] wsaaTotal3ypYtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 96);
	private PackedDecimalData[] wsaaTotal4ypYtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 105);
	private PackedDecimalData[] wsaaTotal5ypYtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 114);
	private PackedDecimalData[] wsaaTotalRypYtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 123);
	private PackedDecimalData[] wsaaTotalSiYtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 132);
	private PackedDecimalData[] wsaaTotalTotYtd = PDArrayPartOfArrayStructure(17, 2, wsaaTotalTab, 141);

	private FixedLengthStringData wsaaFirstChar = new FixedLengthStringData(1);
	private Validator mainCoverage = new Validator(wsaaFirstChar, "U", "M", "W", "B", "E", "G", "F", "C", "Y");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String descrec = "DESCREC";
	private String agntlagrec = "AGNTLAGREC";
		/* TABLES */
	private String t1692 = "T1692";
	private String t1693 = "T1693";
	private String t5696 = "T5696";
		/* CONTROL-TOTALS */
	private int ct01 = 1;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rm511H01 = new FixedLengthStringData(151);
	private FixedLengthStringData rm511h01O = new FixedLengthStringData(151).isAPartOf(rm511H01, 0);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rm511h01O, 0);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rm511h01O, 30);
	private ZonedDecimalData rh01Mnth = new ZonedDecimalData(2, 0).isAPartOf(rm511h01O, 40);
	private ZonedDecimalData rh01Year = new ZonedDecimalData(4, 0).isAPartOf(rm511h01O, 42);
	private FixedLengthStringData rh01Region = new FixedLengthStringData(3).isAPartOf(rm511h01O, 46);
	private FixedLengthStringData rh01RegionDesc = new FixedLengthStringData(30).isAPartOf(rm511h01O, 49);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(rm511h01O, 79);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(rm511h01O, 81);
	private FixedLengthStringData rh01Agntnum = new FixedLengthStringData(8).isAPartOf(rm511h01O, 111);
	private FixedLengthStringData rh01Agntname = new FixedLengthStringData(30).isAPartOf(rm511h01O, 119);
	private FixedLengthStringData rh01Agnttype = new FixedLengthStringData(2).isAPartOf(rm511h01O, 149);

	private FixedLengthStringData rm511D01 = new FixedLengthStringData(53);
	private FixedLengthStringData rm511d01O = new FixedLengthStringData(53).isAPartOf(rm511D01, 0);
	private FixedLengthStringData rd01Agntnum = new FixedLengthStringData(8).isAPartOf(rm511d01O, 0);
	private FixedLengthStringData rd01Agntname = new FixedLengthStringData(30).isAPartOf(rm511d01O, 8);
	private FixedLengthStringData rd01Agtype = new FixedLengthStringData(2).isAPartOf(rm511d01O, 38);
	private FixedLengthStringData rd01Effdate = new FixedLengthStringData(10).isAPartOf(rm511d01O, 40);
	private FixedLengthStringData rd01Currency = new FixedLengthStringData(3).isAPartOf(rm511d01O, 50);
	/*Start ILIFE-1120*/
	/*
	private FixedLengthStringData rm511D02 = new FixedLengthStringData(134);
	private FixedLengthStringData rm511d02O = new FixedLengthStringData(134).isAPartOf(rm511D02, 0);
	*/
	private FixedLengthStringData rm511D02 = new FixedLengthStringData(150);
	private FixedLengthStringData rm511d02O = new FixedLengthStringData(150).isAPartOf(rm511D02, 0);
	
	private ZonedDecimalData rd02Numpols = new ZonedDecimalData(4, 0).isAPartOf(rm511d02O, 0);
	private ZonedDecimalData rd02Si = new ZonedDecimalData(17, 2).isAPartOf(rm511d02O, 4);
	private ZonedDecimalData rd021yp = new ZonedDecimalData(17, 2).isAPartOf(rm511d02O, 21);
	private ZonedDecimalData rd022yp = new ZonedDecimalData(17, 2).isAPartOf(rm511d02O, 38);
	private ZonedDecimalData rd023yp = new ZonedDecimalData(17, 2).isAPartOf(rm511d02O, 55);
	private ZonedDecimalData rd024yp = new ZonedDecimalData(17, 2).isAPartOf(rm511d02O, 72);
	private ZonedDecimalData rd025yp = new ZonedDecimalData(17, 2).isAPartOf(rm511d02O, 89);
	private ZonedDecimalData rd02Ryp = new ZonedDecimalData(17, 2).isAPartOf(rm511d02O, 106);
	private ZonedDecimalData rd02Totprem = new ZonedDecimalData(11, 2).isAPartOf(rm511d02O, 123);
	//ILIFE-1120
	private FixedLengthStringData leadtext = new FixedLengthStringData(16).isAPartOf(rm511d02O, 134);

	/*Start ILIFE-1120*/
	/*
	private FixedLengthStringData rm511Line = new FixedLengthStringData(190);
	private FixedLengthStringData rm511lineO = new FixedLengthStringData(190).isAPartOf(rm511Line, 0);
	private FixedLengthStringData cldb = new FixedLengthStringData(190).isAPartOf(rm511lineO, 0);
	 */
	private FixedLengthStringData rm511Line = new FixedLengthStringData(300);
	private FixedLengthStringData rm511lineO = new FixedLengthStringData(300).isAPartOf(rm511Line, 0);
	private FixedLengthStringData cldb = new FixedLengthStringData(300).isAPartOf(rm511lineO, 0);
	/*End ILIFE-1120*/
	
	private FixedLengthStringData rm511End = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Joind FSU and Life agent headers - new b*/
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
		/*Agent header - life*/
	private AgntlagTableDAM agntlagIO = new AgntlagTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Agent Career History by Effective Date*/
	private MacfeffTableDAM macfeffIO = new MacfeffTableDAM();
		/*Agency Production by Effective Date*/
	private MapreffTableDAM mapreffIO = new MapreffTableDAM();
	private Namadrsrec namadrsrec = new Namadrsrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr2200,
		exit2200,
		start3100,
		end3100
	}

	public Bm511() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingCompany1020();
		setUpHeadingBranch1030();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		wsaaQcmdexc.set("CHGPRTF FILE(RM511) PAGESIZE(*SAME 198) CPI(15)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		printerFile.openOutput();
		wsspEdterror.set(varcom.oK);
		wsaaEffdate.set(ZERO);
		sortEffdate.set(ZERO);
		sortAgentCcy.set(ZERO);
		sortAgentPolMtd.set(ZERO);
		sortAgent1ypMtd.set(ZERO);
		sortAgent2ypMtd.set(ZERO);
		sortAgent3ypMtd.set(ZERO);
		sortAgent4ypMtd.set(ZERO);
		sortAgent5ypMtd.set(ZERO);
		sortAgentRypMtd.set(ZERO);
		sortAgentSiMtd.set(ZERO);
		sortAgentTotMtd.set(ZERO);
		sortAgentPolYtd.set(ZERO);
		sortAgent1ypYtd.set(ZERO);
		sortAgent2ypYtd.set(ZERO);
		sortAgent3ypYtd.set(ZERO);
		sortAgent4ypYtd.set(ZERO);
		sortAgent5ypYtd.set(ZERO);
		sortAgentRypYtd.set(ZERO);
		sortAgentSiYtd.set(ZERO);
		sortAgentTotYtd.set(ZERO);
		initialize2900();
		for (wsaaIndex.set(1); !(isGT(wsaaIndex,5)); wsaaIndex.add(1)){
			wsaaTotalPolMtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal1ypMtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal2ypMtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal3ypMtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal4ypMtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal5ypMtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotalRypMtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotalSiMtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotalTotMtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotalPolYtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal1ypYtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal2ypYtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal3ypYtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal4ypYtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotal5ypYtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotalRypYtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotalSiYtd[wsaaIndex.toInt()].set(ZERO);
			wsaaTotalTotYtd[wsaaIndex.toInt()].set(ZERO);
		}
	}

protected void setUpHeadingCompany1020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(bsprIO.getCompany());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch1030()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(bprdIO.getDefaultBranch());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Branch.set(bprdIO.getDefaultBranch());
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		rh01Year.set(bsscIO.getAcctYear());
		rh01Mnth.set(bsscIO.getAcctMonth());
		/*EXIT*/
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		wsspEdterror.set(varcom.oK);
		sortFile.openOutput();
		readMapreff2100();
		sortFile.close();
		FileSort fs1 = new FileSort(sortFile);
		fs1.addSortKey(sortRegion, true);
		fs1.addSortKey(sortBranch, true);
		fs1.addSortKey(sortAgency, true);
		fs1.addSortKey(sortLevel01, true);
		fs1.addSortKey(sortLevel02, true);
		fs1.addSortKey(sortLevel03, true);
		fs1.addSortKey(sortAgent, true);
		fs1.addSortKey(sortAgentType, true);
		fs1.sort();
		sortFile.openInput();
		printReport3100();
		sortFile.close();
		/*EXIT*/
	}

protected void readMapreff2100()
	{
		start2100();
	}

protected void start2100()
	{
		mapreffIO.setParams(SPACES);
		mapreffIO.setAgntcoy(bsprIO.getCompany());
		mapreffIO.setAgntnum(SPACES);
		mapreffIO.setAcctyr(ZERO);
		mapreffIO.setMnth(ZERO);
		mapreffIO.setCnttype(SPACES);
		mapreffIO.setEffdate(ZERO);
		mapreffIO.setFunction(varcom.begn);
		while ( !(isEQ(mapreffIO.getStatuz(),varcom.endp))) {

        	//performance improvement --  atiwari23
            mapreffIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
            mapreffIO.setFitKeysSearch("AGNTCOY");

			readMapreffio2200();
		}

		if (isEQ(wsaaEof,"Y")) {
			wsspEdterror.set(varcom.endp);
		}
	}

protected void readMapreffio2200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start2200();
					cont2250();
				}
				case nextr2200: {
					nextr2200();
				}
				case exit2200: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2200()
	{
		SmartFileCode.execute(appVars, mapreffIO);
		if (isNE(mapreffIO.getStatuz(),varcom.oK)
		&& isNE(mapreffIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(mapreffIO.getStatuz());
			syserrrec.params.set(mapreffIO.getParams());
			fatalError600();
		}
		if (isEQ(mapreffIO.getStatuz(),varcom.endp)
		|| isNE(mapreffIO.getAgntcoy(),bsprIO.getCompany())) {
			wsaaEof.set("Y");
			sortRegion.set(wsaaRegion);
			sortBranch.set(wsaaBranch);
			sortAgency.set(wsaaAgency);
			sortAgencyType.set(wsaaAgencyType);
			sortLevel01.set(wsaaLevel01);
			sortLevel02.set(wsaaLevel02);
			sortLevel03.set(wsaaLevel03);
			sortActivityCode.set(wsaaActivityCode);
			sortAgent.set(wsaaAgent);
			if (isEQ(wsaaEffdate,ZERO)) {
				sortEffdate.set(varcom.vrcmMaxDate);
			}
			else {
				sortEffdate.set(wsaaEffdate);
			}
			sortAgentType.set(wsaaAgentType);
			sortAgentCcy.set(wsaaAgentCcy);
			sortAgentPolMtd.set(wsaaAgentPolMtd);
			sortAgent1ypMtd.set(wsaaAgent1ypMtd);
			sortAgent2ypMtd.set(wsaaAgent2ypMtd);
			sortAgent3ypMtd.set(wsaaAgent3ypMtd);
			sortAgent4ypMtd.set(wsaaAgent4ypMtd);
			sortAgent5ypMtd.set(wsaaAgent5ypMtd);
			sortAgentRypMtd.set(wsaaAgentRypMtd);
			sortAgentSiMtd.set(wsaaAgentSiMtd);
			sortAgentPolYtd.set(wsaaAgentPolYtd);
			sortAgent1ypYtd.set(wsaaAgent1ypYtd);
			sortAgent2ypYtd.set(wsaaAgent2ypYtd);
			sortAgent3ypYtd.set(wsaaAgent3ypYtd);
			sortAgent4ypYtd.set(wsaaAgent4ypYtd);
			sortAgent5ypYtd.set(wsaaAgent5ypYtd);
			sortAgentRypYtd.set(wsaaAgentRypYtd);
			sortAgentSiYtd.set(wsaaAgentSiYtd);
			compute(wsaaAgentTotMtd, 2).set(add(add(add(add(add(wsaaAgent1ypMtd,wsaaAgent2ypMtd),wsaaAgent3ypMtd),wsaaAgent4ypMtd),wsaaAgent5ypMtd),wsaaAgentRypMtd));
			sortAgentTotMtd.set(wsaaAgentTotMtd);
			compute(wsaaAgentTotYtd, 2).set(add(add(add(add(add(wsaaAgent1ypYtd,wsaaAgent2ypYtd),wsaaAgent3ypYtd),wsaaAgent4ypYtd),wsaaAgent5ypYtd),wsaaAgentRypYtd));
			sortAgentTotYtd.set(wsaaAgentTotYtd);
			sortFile.write(sortRecord);
			goTo(GotoLabel.exit2200);
		}
		if (isNE(mapreffIO.getAcctyr(),bsscIO.getAcctYear())
		|| isGT(mapreffIO.getMnth(),bsscIO.getAcctMonth())) {
			goTo(GotoLabel.nextr2200);
		}
		wsaaReportag[1].set(SPACES);
		wsaaReportag[2].set(SPACES);
		wsaaReportag[3].set(SPACES);
		macfeffIO.setAgntcoy(mapreffIO.getAgntcoy());
		macfeffIO.setAgntnum(mapreffIO.getAgntnum());
		macfeffIO.setEffdate(mapreffIO.getEffdate());
		macfeffIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, macfeffIO);
		if (isNE(macfeffIO.getStatuz(),varcom.oK)
		&& isNE(macfeffIO.getStatuz(),varcom.endp)
		|| (isNE(macfeffIO.getAgntcoy(),mapreffIO.getAgntcoy()))
		|| (isNE(macfeffIO.getAgntnum(),mapreffIO.getAgntnum()))
		|| (isGT(macfeffIO.getEffdate(),mapreffIO.getEffdate()))) {
			syserrrec.statuz.set(macfeffIO.getStatuz());
			syserrrec.params.set(macfeffIO.getParams());
			fatalError600();
		}
		wsaaActivityCode.set(macfeffIO.getAgmvty());
		wsaaReportag[1].set(macfeffIO.getAgntnum());
		wsaaReportag[2].set(macfeffIO.getZrptga());
		wsaaReportag[3].set(macfeffIO.getZrptgb());
		if (isEQ(wsaaAgent,SPACES)
		&& isEQ(wsaaAgentType,SPACES)) {
			wsaaAgent.set(mapreffIO.getAgntnum());
			wsaaEffdate.set(mapreffIO.getEffdate());
			wsaaAgentType.set(macfeffIO.getMlagttyp());
			wsaaLevel1.set(wsaaReportag[1]);
			wsaaLevel2.set(wsaaReportag[2]);
			wsaaLevel3.set(wsaaReportag[3]);
		}
		if (isNE(wsaaAgent,mapreffIO.getAgntnum())
		|| isNE(wsaaAgentType,macfeffIO.getMlagttyp())
		|| isNE(wsaaLevel1,wsaaReportag[1])
		|| isNE(wsaaLevel2,wsaaReportag[2])
		|| isNE(wsaaLevel3,wsaaReportag[3])) {
			sortRegion.set(wsaaRegion);
			sortBranch.set(wsaaBranch);
			sortAgency.set(wsaaAgency);
			sortAgencyType.set(wsaaAgencyType);
			if (isEQ(wsaaEffdate,ZERO)) {
				sortEffdate.set(varcom.vrcmMaxDate);
			}
			else {
				sortEffdate.set(wsaaEffdate);
			}
			sortLevel01.set(wsaaLevel01);
			sortLevel02.set(wsaaLevel02);
			sortLevel03.set(wsaaLevel03);
			sortActivityCode.set(wsaaActivityCode);
			sortAgent.set(wsaaAgent);
			sortAgentType.set(wsaaAgentType);
			sortAgentCcy.set(wsaaAgentCcy);
			sortAgentPolMtd.set(wsaaAgentPolMtd);
			sortAgent1ypMtd.set(wsaaAgent1ypMtd);
			sortAgent2ypMtd.set(wsaaAgent2ypMtd);
			sortAgent3ypMtd.set(wsaaAgent3ypMtd);
			sortAgent4ypMtd.set(wsaaAgent4ypMtd);
			sortAgent5ypMtd.set(wsaaAgent5ypMtd);
			sortAgentRypMtd.set(wsaaAgentRypMtd);
			sortAgentSiMtd.set(wsaaAgentSiMtd);
			sortAgentPolYtd.set(wsaaAgentPolYtd);
			sortAgent1ypYtd.set(wsaaAgent1ypYtd);
			sortAgent2ypYtd.set(wsaaAgent2ypYtd);
			sortAgent3ypYtd.set(wsaaAgent3ypYtd);
			sortAgent4ypYtd.set(wsaaAgent4ypYtd);
			sortAgent5ypYtd.set(wsaaAgent5ypYtd);
			sortAgentRypYtd.set(wsaaAgentRypYtd);
			sortAgentSiYtd.set(wsaaAgentSiYtd);
			compute(wsaaAgentTotMtd, 2).set(add(add(add(add(add(wsaaAgent1ypMtd,wsaaAgent2ypMtd),wsaaAgent3ypMtd),wsaaAgent4ypMtd),wsaaAgent5ypMtd),wsaaAgentRypMtd));
			sortAgentTotMtd.set(wsaaAgentTotMtd);
			compute(wsaaAgentTotYtd, 2).set(add(add(add(add(add(wsaaAgent1ypYtd,wsaaAgent2ypYtd),wsaaAgent3ypYtd),wsaaAgent4ypYtd),wsaaAgent5ypYtd),wsaaAgentRypYtd));
			sortAgentTotYtd.set(wsaaAgentTotYtd);
			sortFile.write(sortRecord);
			wsaaAgent.set(mapreffIO.getAgntnum());
			wsaaEffdate.set(mapreffIO.getEffdate());
			wsaaAgentType.set(macfeffIO.getMlagttyp());
			wsaaLevel1.set(wsaaReportag[1]);
			wsaaLevel2.set(wsaaReportag[2]);
			wsaaLevel3.set(wsaaReportag[3]);
			initialize2900();
		}
		if (isEQ(mapreffIO.getMnth(),bsscIO.getAcctMonth())) {
			wsaaAgent1ypMtd.add(mapreffIO.getMlperpp(1));
			wsaaAgent2ypMtd.add(mapreffIO.getMlperpp(2));
			wsaaAgent3ypMtd.add(mapreffIO.getMlperpp(3));
			wsaaAgent4ypMtd.add(mapreffIO.getMlperpp(4));
			wsaaAgent5ypMtd.add(mapreffIO.getMlperpp(5));
			compute(wsaaAgentRypMtd, 2).add(add(mapreffIO.getMlperpp(6),add(mapreffIO.getMlperpp(7),add(mapreffIO.getMlperpp(8),add(mapreffIO.getMlperpp(9),mapreffIO.getMlperpp(10))))));
			wsaaAgentSiMtd.add(mapreffIO.getSumins());
			wsaaAgentPolMtd.add(mapreffIO.getCntcount());
		}
		wsaaAgent1ypYtd.add(mapreffIO.getMlperpp(1));
		wsaaAgent2ypYtd.add(mapreffIO.getMlperpp(2));
		wsaaAgent3ypYtd.add(mapreffIO.getMlperpp(3));
		wsaaAgent4ypYtd.add(mapreffIO.getMlperpp(4));
		wsaaAgent5ypYtd.add(mapreffIO.getMlperpp(5));
		compute(wsaaAgentRypYtd, 2).add(add(mapreffIO.getMlperpp(6),add(mapreffIO.getMlperpp(7),add(mapreffIO.getMlperpp(8),add(mapreffIO.getMlperpp(9),mapreffIO.getMlperpp(10))))));
		wsaaAgentSiYtd.add(mapreffIO.getSumins());
		wsaaAgentPolYtd.add(mapreffIO.getCntcount());
	}

protected void cont2250()
	{
		aglfIO.setAgntcoy(mapreffIO.getAgntcoy());
		aglfIO.setAgntnum(mapreffIO.getAgntnum());
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)
		&& isNE(aglfIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglfIO.getStatuz());
			syserrrec.params.set(aglfIO.getParams());
			fatalError600();
		}
		wsaaAgentCcy.set(aglfIO.getCurrcode());
		aglflnbIO.setAgntcoy(mapreffIO.getAgntcoy());
		aglflnbIO.setAgntnum(mapreffIO.getAgntnum());
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(),varcom.oK)
		&& isNE(aglflnbIO.getStatuz(),varcom.mrnf)) {
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		wsaaRegion.set(aglflnbIO.getAracde());
		wsaaBranch.set(aglflnbIO.getAgntbr());
		wsaaLevel01.set(SPACES);
		wsaaLevel02.set(SPACES);
		wsaaLevel03.set(SPACES);
		wsaaIndex1.set(ZERO);
		for (wsaaIndex.set(3); !(isEQ(wsaaIndex,ZERO)); wsaaIndex.add(-1)){
			if (isNE(wsaaReportag[wsaaIndex.toInt()],SPACES)) {
				wsaaIndex1.add(1);
				if (isEQ(wsaaIndex1,1)) {
					wsaaLevel01.set(wsaaReportag[wsaaIndex.toInt()]);
				}
				if (isEQ(wsaaIndex1,2)) {
					wsaaLevel02.set(wsaaReportag[wsaaIndex.toInt()]);
				}
				if (isEQ(wsaaIndex1,3)) {
					wsaaLevel03.set(wsaaReportag[wsaaIndex.toInt()]);
				}
			}
		}
		if (isEQ(wsaaLevel03,SPACES)
		&& isNE(wsaaLevel02,SPACES)
		&& isEQ(macfeffIO.getMlagttyp(),"AG")) {
			wsaaLevel03.set(wsaaLevel02);
			wsaaLevel02.set(SPACES);
		}
		wsaaAgency.set(wsaaLevel01);
	}

protected void nextr2200()
	{
		mapreffIO.setFunction(varcom.nextr);
	}

protected void initialize2900()
	{
		/*START*/
		wsaaAgentCcy.set(ZERO);
		wsaaAgentPolMtd.set(ZERO);
		wsaaAgent1ypMtd.set(ZERO);
		wsaaAgent2ypMtd.set(ZERO);
		wsaaAgent3ypMtd.set(ZERO);
		wsaaAgent4ypMtd.set(ZERO);
		wsaaAgent5ypMtd.set(ZERO);
		wsaaAgentRypMtd.set(ZERO);
		wsaaAgentSiMtd.set(ZERO);
		wsaaAgentTotMtd.set(ZERO);
		wsaaAgentPolYtd.set(ZERO);
		wsaaAgent1ypYtd.set(ZERO);
		wsaaAgent2ypYtd.set(ZERO);
		wsaaAgent3ypYtd.set(ZERO);
		wsaaAgent4ypYtd.set(ZERO);
		wsaaAgent5ypYtd.set(ZERO);
		wsaaAgentRypYtd.set(ZERO);
		wsaaAgentTotYtd.set(ZERO);
		wsaaAgentSiYtd.set(ZERO);
		wsaaLevel01.set(SPACES);
		wsaaLevel02.set(SPACES);
		wsaaLevel03.set(SPACES);
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*EXIT*/
	}

protected void printReport3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				}
				case start3100: {
					start3100();
				}
				case end3100: {
					end3100();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3100()
	{
		sortFile.read(sortRecord);
		if (sortFile.isAtEnd()) {
			goTo(GotoLabel.end3100);
		}
		startPrint3200();
		goTo(GotoLabel.start3100);
	}

protected void end3100()
	{
		d4000SubBreak();
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,4)); wsaaCnt.add(1)){
			wsaaIndex.set(wsaaCnt);
			e1000MoveToPrintRec();
		}
		cldb.fill("-");
		printerFile.printRm511line(rm511Line, indicArea);
		printerFile.printRm511end(rm511End, indicArea);
		/*EXIT*/
	}

protected void startPrint3200()
	{
		start3200();
	}

protected void start3200()
	{
		if (isEQ(wsaaSortRegionSave,SPACES)
		&& isEQ(wsaaSortBranchSave,SPACES)
		&& isEQ(wsaaSortAgencySave,SPACES)) {
			wsaaSortRegionSave.set(sortRegion);
			wsaaSortBranchSave.set(sortBranch);
			wsaaSortAgencySave.set(sortAgency);
			wsaaSaveLevel01.set(sortLevel01);
			wsaaSaveLevel02.set(sortLevel02);
			c1000ResetAgency();
			c2000ResetBranch();
			c3000ResetRegion();
			wsaaOverflow.set("Y");
		}
		wsaaSortLev01.set(sortLevel01);
		wsaaSortLev02.set(sortLevel02);
		if (isNE(sortRegion,wsaaSortRegionSave)) {
			d4000SubBreak();
			d1000AgencyBreak();
			d2000BranchBreak();
			d3000RegionBreak();
			wsaaSortRegionSave.set(sortRegion);
			wsaaSortBranchSave.set(sortBranch);
			wsaaSortAgencySave.set(sortAgency);
			wsaaSaveLevel01.set(sortLevel01);
			wsaaSaveLevel02.set(sortLevel02);
			c1000ResetAgency();
			c2000ResetBranch();
			c3000ResetRegion();
			wsaaOverflow.set("Y");
		}
		else {
			if (isNE(sortBranch,wsaaSortBranchSave)) {
				d4000SubBreak();
				d1000AgencyBreak();
				d2000BranchBreak();
				wsaaSortBranchSave.set(sortBranch);
				wsaaSortAgencySave.set(sortAgency);
				wsaaSaveLevel01.set(sortLevel01);
				wsaaSaveLevel02.set(sortLevel02);
				c1000ResetAgency();
				c2000ResetBranch();
				wsaaOverflow.set("Y");
			}
			else {
				if (isNE(sortAgency,wsaaSortAgencySave)) {
					d4000SubBreak();
					d1000AgencyBreak();
					wsaaSortAgencySave.set(sortAgency);
					wsaaSaveLevel01.set(sortLevel01);
					wsaaSaveLevel02.set(sortLevel02);
					c1000ResetAgency();
					wsaaOverflow.set("Y");
				}
				else {
					if (isNE(wsaaSaveLevel,wsaaSortLevel)) {
						d4000SubBreak();
						wsaaSaveLevel01.set(sortLevel01);
						wsaaSaveLevel02.set(sortLevel02);
					}
				}
			}
		}
		b1300PrintDetailLine();
	}

protected void b1300PrintDetailLine()
	{
		b1300Begin();
	}

protected void b1300Begin()
	{
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			if (isEQ(wsaaFirstTime,"N")) {
				cldb.fill("-");
				printerFile.printRm511line(rm511Line, indicArea);
			}
			wsaaFirstTime = "N";
			printerFile.printRm511h01(rm511H01, indicArea);
			wsaaOverflow.set("N");
		}
		if (isEQ(sortActivityCode,"T")) {
			indOn.setTrue(11);
			indOff.setTrue(12);
			indOff.setTrue(13);
			indOff.setTrue(14);
			indOff.setTrue(15);
			indOff.setTrue(16);
		}
		else {
			if (isEQ(sortActivityCode,"A")) {
				indOn.setTrue(12);
				indOff.setTrue(11);
				indOff.setTrue(13);
				indOff.setTrue(14);
				indOff.setTrue(15);
				indOff.setTrue(16);
			}
			else {
				if (isEQ(sortActivityCode,"P")) {
					indOn.setTrue(13);
					indOff.setTrue(11);
					indOff.setTrue(12);
					indOff.setTrue(14);
					indOff.setTrue(15);
					indOff.setTrue(16);
				}
				else {
					if (isEQ(sortActivityCode,"D")) {
						indOn.setTrue(14);
						indOff.setTrue(11);
						indOff.setTrue(12);
						indOff.setTrue(13);
						indOff.setTrue(15);
						indOff.setTrue(16);
					}
					else {
						if (isEQ(sortActivityCode,"F")
						|| isEQ(sortActivityCode,"M")) {
							indOn.setTrue(15);
							indOff.setTrue(11);
							indOff.setTrue(12);
							indOff.setTrue(13);
							indOff.setTrue(14);
							indOff.setTrue(16);
						}
						else {
							indOn.setTrue(16);
							indOff.setTrue(11);
							indOff.setTrue(12);
							indOff.setTrue(13);
							indOff.setTrue(14);
							indOff.setTrue(15);
						}
					}
				}
			}
		}
		rd01Agntnum.set(sortAgent);
		rd01Agtype.set(sortAgentType);
		rd01Currency.set(sortAgentCcy);
		if (isEQ(sortEffdate,ZERO)) {
			rd01Effdate.set(varcom.vrcmMaxDate);
		}
		else {
			datcon1rec.function.set(varcom.conv);
			datcon1rec.intDate.set(sortEffdate);
			Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
			rd01Effdate.set(datcon1rec.extDate);
		}
		agntlagIO.setAgntcoy(bsprIO.getCompany());
		agntlagIO.setAgntnum(sortAgent);
		agntlagIO.setFunction(varcom.readr);
		agntlagIO.setFormat(agntlagrec);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(agntlagIO.getClntnum());
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(bsprIO.getFsuco());
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.function.set("LGNMS");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		rd01Agntname.set(namadrsrec.name);
		printerFile.printRm511d01(rm511D01, indicArea);
		indOn.setTrue(17);
		indOff.setTrue(1);
		indOff.setTrue(2);
		indOff.setTrue(3);
		indOff.setTrue(4);
		indOff.setTrue(5);
		indOff.setTrue(6);
		indOff.setTrue(18);
		rd021yp.set(sortAgent1ypMtd);
		rd022yp.set(sortAgent2ypMtd);
		rd023yp.set(sortAgent3ypMtd);
		rd024yp.set(sortAgent4ypMtd);
		rd025yp.set(sortAgent5ypMtd);
		rd02Ryp.set(sortAgentRypMtd);
		rd02Si.set(sortAgentSiMtd);
		rd02Totprem.set(sortAgentTotMtd);
		rd02Numpols.set(sortAgentPolMtd);
		//ILIFE-1120
		leadtext.set("Agent MTD");
		printerFile.printRm511d02(rm511D02, indicArea);
		indOn.setTrue(18);
		indOff.setTrue(1);
		indOff.setTrue(2);
		indOff.setTrue(3);
		indOff.setTrue(4);
		indOff.setTrue(5);
		indOff.setTrue(6);
		indOff.setTrue(17);
		rd021yp.set(sortAgent1ypYtd);
		rd022yp.set(sortAgent2ypYtd);
		rd023yp.set(sortAgent3ypYtd);
		rd024yp.set(sortAgent4ypYtd);
		rd025yp.set(sortAgent5ypYtd);
		rd02Ryp.set(sortAgentRypYtd);
		rd02Si.set(sortAgentSiYtd);
		rd02Totprem.set(sortAgentTotYtd);
		rd02Numpols.set(sortAgentPolYtd);
		//ILIFE-1120
		leadtext.set("YTD");
		printerFile.printRm511d02(rm511D02, indicArea);
		for (wsaaCnt.set(1); !(isGT(wsaaCnt,5)); wsaaCnt.add(1)){
			wsaaTotalPolMtd[wsaaCnt.toInt()].add(sortAgentPolMtd);
			wsaaTotal1ypMtd[wsaaCnt.toInt()].add(sortAgent1ypMtd);
			wsaaTotal2ypMtd[wsaaCnt.toInt()].add(sortAgent2ypMtd);
			wsaaTotal3ypMtd[wsaaCnt.toInt()].add(sortAgent3ypMtd);
			wsaaTotal4ypMtd[wsaaCnt.toInt()].add(sortAgent4ypMtd);
			wsaaTotal5ypMtd[wsaaCnt.toInt()].add(sortAgent5ypMtd);
			wsaaTotalRypMtd[wsaaCnt.toInt()].add(sortAgentRypMtd);
			wsaaTotalSiMtd[wsaaCnt.toInt()].add(sortAgentSiMtd);
			wsaaTotalTotMtd[wsaaCnt.toInt()].add(sortAgentTotMtd);
			wsaaTotalPolYtd[wsaaCnt.toInt()].add(sortAgentPolYtd);
			wsaaTotal1ypYtd[wsaaCnt.toInt()].add(sortAgent1ypYtd);
			wsaaTotal2ypYtd[wsaaCnt.toInt()].add(sortAgent2ypYtd);
			wsaaTotal3ypYtd[wsaaCnt.toInt()].add(sortAgent3ypYtd);
			wsaaTotal4ypYtd[wsaaCnt.toInt()].add(sortAgent4ypYtd);
			wsaaTotal5ypYtd[wsaaCnt.toInt()].add(sortAgent5ypYtd);
			wsaaTotalRypYtd[wsaaCnt.toInt()].add(sortAgentRypYtd);
			wsaaTotalSiYtd[wsaaCnt.toInt()].add(sortAgentSiYtd);
			wsaaTotalTotYtd[wsaaCnt.toInt()].add(sortAgentTotYtd);
		}
		f2000CummulateSub();
	}

protected void c1000ResetAgency()
	{
		c1000Begin();
	}

protected void c1000Begin()
	{
		rh01Agntnum.set(sortAgency);
		rh01Agnttype.set(sortAgencyType);
		agntlagIO.setAgntcoy(bsprIO.getCompany());
		agntlagIO.setAgntnum(sortAgency);
		agntlagIO.setFunction(varcom.readr);
		agntlagIO.setFormat(agntlagrec);
		SmartFileCode.execute(appVars, agntlagIO);
		if (isNE(agntlagIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agntlagIO.getParams());
			fatalError600();
		}
		namadrsrec.namadrsRec.set(SPACES);
		namadrsrec.clntNumber.set(agntlagIO.getClntnum());
		namadrsrec.clntPrefix.set("CN");
		namadrsrec.clntCompany.set(bsprIO.getFsuco());
		namadrsrec.language.set(bsscIO.getLanguage());
		namadrsrec.function.set("LGNMS");
		callProgram(Namadrs.class, namadrsrec.namadrsRec);
		if (isNE(namadrsrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(namadrsrec.statuz);
			fatalError600();
		}
		rh01Agntname.set(namadrsrec.name);
	}

protected void c2000ResetBranch()
	{
		c2000Begin();
	}

protected void c2000Begin()
	{
		rh01Branch.set(sortBranch);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t1692);
		descIO.setDescitem(sortBranch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("??????????????????????????????");
		}
		else {
			if (isNE(descIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
		}
		rh01Branchnm.set(descIO.getLongdesc());
	}

protected void c3000ResetRegion()
	{
		c3000Begin();
	}

protected void c3000Begin()
	{
		rh01Region.set(sortRegion);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5696);
		descIO.setDescitem(sortRegion);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("??????????????????????????????");
		}
		else {
			if (isNE(descIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(descIO.getParams());
				fatalError600();
			}
		}
		rh01RegionDesc.set(descIO.getLongdesc());
	}

protected void d1000AgencyBreak()
	{
		/*D1000-BEGIN*/
		wsaaIndex.set(1);
		e1000MoveToPrintRec();
		/*D1000-EXIT*/
	}

protected void d2000BranchBreak()
	{
		/*D2000-BEGIN*/
		wsaaIndex.set(2);
		e1000MoveToPrintRec();
		/*D2000-EXIT*/
	}

protected void d3000RegionBreak()
	{
		/*D3000-BEGIN*/
		wsaaIndex.set(3);
		e1000MoveToPrintRec();
		/*D3000-EXIT*/
	}

protected void d4000SubBreak()
	{
		/*D4000-BEGIN*/
		wsaaIndex.set(5);
		e1000MoveToPrintRec();
		/*D4000-EXIT*/
	}

protected void e1000MoveToPrintRec()
	{
		e1000Begin();
	}

protected void e1000Begin()
	{
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			cldb.fill("-");
			printerFile.printRm511line(rm511Line, indicArea);
			printerFile.printRm511h01(rm511H01, indicArea);
			wsaaOverflow.set("N");
		}
		cldb.set(SPACES);
		printerFile.printRm511line(rm511Line, indicArea);
		rd02Numpols.set(wsaaTotalPolMtd[wsaaIndex.toInt()]);
		rd021yp.set(wsaaTotal1ypMtd[wsaaIndex.toInt()]);
		rd022yp.set(wsaaTotal2ypMtd[wsaaIndex.toInt()]);
		rd023yp.set(wsaaTotal3ypMtd[wsaaIndex.toInt()]);
		rd024yp.set(wsaaTotal4ypMtd[wsaaIndex.toInt()]);
		rd025yp.set(wsaaTotal5ypMtd[wsaaIndex.toInt()]);
		rd02Ryp.set(wsaaTotalRypMtd[wsaaIndex.toInt()]);
		rd02Si.set(wsaaTotalSiMtd[wsaaIndex.toInt()]);
		rd02Totprem.set(wsaaTotalTotMtd[wsaaIndex.toInt()]);
		if (isEQ(wsaaIndex,1)){
			indOn.setTrue(1);
			indOff.setTrue(2);
			indOff.setTrue(3);
			indOff.setTrue(4);
			indOff.setTrue(5);
			indOff.setTrue(6);
			indOff.setTrue(17);
			indOff.setTrue(18);
			//ILIFE-1120
			leadtext.set("Agency MTD");
		}
		else if (isEQ(wsaaIndex,2)){
			indOn.setTrue(2);
			indOff.setTrue(1);
			indOff.setTrue(3);
			indOff.setTrue(4);
			indOff.setTrue(5);
			indOff.setTrue(6);
			indOff.setTrue(17);
			indOff.setTrue(18);		
			//ILIFE-1120
			leadtext.set("Branch MTD");
		}
		else if (isEQ(wsaaIndex,3)){
			indOn.setTrue(3);
			indOff.setTrue(1);
			indOff.setTrue(2);
			indOff.setTrue(4);
			indOff.setTrue(5);
			indOff.setTrue(6);
			indOff.setTrue(17);
			indOff.setTrue(18);
			//ILIFE-1120
			leadtext.set("Region MTD");
		}
		else if (isEQ(wsaaIndex,4)){
			indOn.setTrue(4);
			indOff.setTrue(1);
			indOff.setTrue(2);
			indOff.setTrue(3);
			indOff.setTrue(5);
			indOff.setTrue(6);
			indOff.setTrue(17);
			indOff.setTrue(18);
			//ILIFE-1120
			leadtext.set("Company MTD");
		}
		else if (isEQ(wsaaIndex,5)){
			indOn.setTrue(5);
			indOff.setTrue(1);
			indOff.setTrue(2);
			indOff.setTrue(3);
			indOff.setTrue(4);
			indOff.setTrue(6);
			indOff.setTrue(17);
			indOff.setTrue(18);
			//ILIFE-1120
			leadtext.set("Agency Unit MTD");
		}
		printerFile.printRm511d02(rm511D02, indicArea);
		rd02Numpols.set(wsaaTotalPolYtd[wsaaIndex.toInt()]);
		rd021yp.set(wsaaTotal1ypYtd[wsaaIndex.toInt()]);
		rd022yp.set(wsaaTotal2ypYtd[wsaaIndex.toInt()]);
		rd023yp.set(wsaaTotal3ypYtd[wsaaIndex.toInt()]);
		rd024yp.set(wsaaTotal4ypYtd[wsaaIndex.toInt()]);
		rd025yp.set(wsaaTotal5ypYtd[wsaaIndex.toInt()]);
		rd02Ryp.set(wsaaTotalRypYtd[wsaaIndex.toInt()]);
		rd02Si.set(wsaaTotalSiYtd[wsaaIndex.toInt()]);
		rd02Totprem.set(wsaaTotalTotYtd[wsaaIndex.toInt()]);
		if (isEQ(wsaaIndex,1)){
			indOn.setTrue(6);
			indOff.setTrue(1);
			indOff.setTrue(2);
			indOff.setTrue(3);
			indOff.setTrue(4);
			indOff.setTrue(5);
			indOff.setTrue(17);
			indOff.setTrue(18);
		}
		else if (isEQ(wsaaIndex,2)){
			indOn.setTrue(6);
			indOff.setTrue(1);
			indOff.setTrue(2);
			indOff.setTrue(3);
			indOff.setTrue(4);
			indOff.setTrue(5);
			indOff.setTrue(17);
			indOff.setTrue(18);
		}
		else if (isEQ(wsaaIndex,3)){
			indOn.setTrue(6);
			indOff.setTrue(1);
			indOff.setTrue(2);
			indOff.setTrue(3);
			indOff.setTrue(4);
			indOff.setTrue(5);
			indOff.setTrue(17);
			indOff.setTrue(18);
		}
		else if (isEQ(wsaaIndex,4)){
			indOn.setTrue(6);
			indOff.setTrue(1);
			indOff.setTrue(2);
			indOff.setTrue(3);
			indOff.setTrue(4);
			indOff.setTrue(5);
			indOff.setTrue(17);
			indOff.setTrue(18);
		}
		else if (isEQ(wsaaIndex,5)){
			indOn.setTrue(6);
			indOff.setTrue(1);
			indOff.setTrue(2);
			indOff.setTrue(3);
			indOff.setTrue(4);
			indOff.setTrue(5);
			indOff.setTrue(17);
			indOff.setTrue(18);
		}
		//ILIFE-1120
		leadtext.set("MTD");
		printerFile.printRm511d02(rm511D02, indicArea);
		wsaaTotal1ypMtd[wsaaIndex.toInt()].set(0);
		wsaaTotal2ypMtd[wsaaIndex.toInt()].set(0);
		wsaaTotal3ypMtd[wsaaIndex.toInt()].set(0);
		wsaaTotal4ypMtd[wsaaIndex.toInt()].set(0);
		wsaaTotal5ypMtd[wsaaIndex.toInt()].set(0);
		wsaaTotalRypMtd[wsaaIndex.toInt()].set(0);
		wsaaTotalSiMtd[wsaaIndex.toInt()].set(0);
		wsaaTotalTotMtd[wsaaIndex.toInt()].set(0);
		wsaaTotalPolMtd[wsaaIndex.toInt()].set(0);
		wsaaTotal1ypYtd[wsaaIndex.toInt()].set(0);
		wsaaTotal2ypYtd[wsaaIndex.toInt()].set(0);
		wsaaTotal3ypYtd[wsaaIndex.toInt()].set(0);
		wsaaTotal4ypYtd[wsaaIndex.toInt()].set(0);
		wsaaTotal5ypYtd[wsaaIndex.toInt()].set(0);
		wsaaTotalRypYtd[wsaaIndex.toInt()].set(0);
		wsaaTotalSiYtd[wsaaIndex.toInt()].set(0);
		wsaaTotalTotYtd[wsaaIndex.toInt()].set(0);
		wsaaTotalPolYtd[wsaaIndex.toInt()].set(0);
	}

protected void f1000InitializeSub()
	{
		/*F1000-START*/
		wsaaAgentPolMtd.set(ZERO);
		wsaaAgent1ypMtd.set(ZERO);
		wsaaAgent2ypMtd.set(ZERO);
		wsaaAgent3ypMtd.set(ZERO);
		wsaaAgent4ypMtd.set(ZERO);
		wsaaAgent5ypMtd.set(ZERO);
		wsaaAgentRypMtd.set(ZERO);
		wsaaAgentSiMtd.set(ZERO);
		wsaaAgentTotMtd.set(ZERO);
		wsaaAgentPolYtd.set(ZERO);
		wsaaAgent1ypYtd.set(ZERO);
		wsaaAgent2ypYtd.set(ZERO);
		wsaaAgent3ypYtd.set(ZERO);
		wsaaAgent4ypYtd.set(ZERO);
		wsaaAgent5ypYtd.set(ZERO);
		wsaaAgentRypYtd.set(ZERO);
		wsaaAgentTotYtd.set(ZERO);
		wsaaAgentSiYtd.set(ZERO);
		/*F1000-EXIT*/
	}

protected void f2000CummulateSub()
	{
		f2000Start();
	}

protected void f2000Start()
	{
		wsaaAgentPolMtd.add(sortAgentPolMtd);
		wsaaAgent1ypMtd.add(sortAgent1ypMtd);
		wsaaAgent2ypMtd.add(sortAgent2ypMtd);
		wsaaAgent3ypMtd.add(sortAgent3ypMtd);
		wsaaAgent4ypMtd.add(sortAgent4ypMtd);
		wsaaAgent5ypMtd.add(sortAgent5ypMtd);
		wsaaAgentRypMtd.add(sortAgentRypMtd);
		wsaaAgentSiMtd.add(sortAgentSiMtd);
		wsaaAgentTotMtd.add(sortAgentTotMtd);
		wsaaAgentPolYtd.add(sortAgentPolYtd);
		wsaaAgent1ypYtd.add(sortAgent1ypYtd);
		wsaaAgent2ypYtd.add(sortAgent2ypYtd);
		wsaaAgent3ypYtd.add(sortAgent3ypYtd);
		wsaaAgent4ypYtd.add(sortAgent4ypYtd);
		wsaaAgent5ypYtd.add(sortAgent5ypYtd);
		wsaaAgentRypYtd.add(sortAgentRypYtd);
		wsaaAgentSiYtd.add(sortAgentSiYtd);
		wsaaAgentTotYtd.add(sortAgentTotYtd);
	}

protected void f3000SubBreak()
	{
		f3000Start();
	}

protected void f3000Start()
	{
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			cldb.fill("-");
			printerFile.printRm511line(rm511Line, indicArea);
			printerFile.printRm511h01(rm511H01, indicArea);
			wsaaOverflow.set("N");
		}
		cldb.set(SPACES);
		printerFile.printRm511line(rm511Line, indicArea);
		rd02Numpols.set(wsaaAgentPolMtd);
		rd021yp.set(wsaaAgent1ypMtd);
		rd022yp.set(wsaaAgent2ypMtd);
		rd023yp.set(wsaaAgent3ypMtd);
		rd024yp.set(wsaaAgent4ypMtd);
		rd025yp.set(wsaaAgent5ypMtd);
		rd02Ryp.set(wsaaAgentRypMtd);
		rd02Si.set(wsaaAgentSiMtd);
		rd02Totprem.set(wsaaAgentTotMtd);
		indOff.setTrue(6);
		indOff.setTrue(1);
		indOff.setTrue(2);
		indOff.setTrue(3);
		indOff.setTrue(4);
		indOff.setTrue(5);
		indOff.setTrue(17);
		indOff.setTrue(18);
		printerFile.printRm511d02(rm511D02, indicArea);
		rd02Numpols.set(wsaaAgentPolYtd);
		rd021yp.set(wsaaAgent1ypYtd);
		rd022yp.set(wsaaAgent2ypYtd);
		rd023yp.set(wsaaAgent3ypYtd);
		rd024yp.set(wsaaAgent4ypYtd);
		rd025yp.set(wsaaAgent5ypYtd);
		rd02Ryp.set(wsaaAgentRypYtd);
		rd02Si.set(wsaaAgentSiYtd);
		rd02Totprem.set(wsaaAgentTotYtd);
		indOff.setTrue(6);
		indOff.setTrue(1);
		indOff.setTrue(2);
		indOff.setTrue(3);
		indOff.setTrue(4);
		indOff.setTrue(5);
		indOff.setTrue(17);
		indOff.setTrue(18);
		printerFile.printRm511d02(rm511D02, indicArea);
		f1000InitializeSub();
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
}
