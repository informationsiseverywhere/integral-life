/*
 * File: Bj531.java
 * Date: 29 August 2009 21:44:26
 * Author: Quipoz Limited
 *
 * Class transformed from BJ531.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj531Report;
import com.csc.life.statistics.tablestructures.Tj675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                  Total Inforce Business
*                  ======================
*
*   This report will list the Inforce business and identify those
*     that have been reassurance ceded for the specified period.
*     It will be segregated into regular premium contracts, single
*     premium contracts(includes immediate and deferred annuities)
*     and Others(fully paid and paid up contracts) at the end of the
*     accounting year.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   1000-INITIALISE section
*     Read table TR386 using BSSC-LANGUAGE and WSAA-PROG.Store
*       the information in the working storage array. This table
*       is an extra data multiple screen.
*
*     Read GOVE using SQL.Selet records where STATCAT = 'IF' and
*       ACCTYR=PJ517-ACCTYEAR.Order by company,accounting currency
*       register, statutory section, sub-satitory section, contract type
*       and coverage type.
*
*   2000-READ-FILE section.
*     Fetch the SQL record. If end-of-file, set WSSP-EDTERROR to ENDP.
*
*   2500-EDIT section.
*     Set WSSP-EDTERRIR to O-K.
*
*   3000-UPDATE section.
*     If there is a change in company, accounting currency or
*      register, Business Category or Division/Sub-Class/Gro     up
*      skip to a new page and print report heading and initialise
*      working storage.
*
*   For each SQL record read, do the following:
*     Read GVACSTS using function READR to get the reassurance amount
*       (STRAAMT) and premium ceded amount(STACCRIP).
*
*     Read T3629 using SQL-CNTCURR to get the currency rate. Amounts
*       coming from GOVE are in premium currency.
*
*     If Accounting currency is not equal to Premium currency,
*        Convert the amounts to accounting currency amount by
*        multiplying with T3629-SCRATE.
*
*     We need to check if it is a regular premium contract or a
*        single premium contract.
*
*   Control totals:
*     01  -  Totals of read records
*     02  -  Totals of pages printed
*     03  -  Totals of policy count (exclude Reassurance)
*     04  -  Totals of lives count  (exclude Reassurance)
*     05  -  Totals of sum assured  (exclude Reassurance)
*     06  -  Totals of annuity pa   (exclude Reassurance)
*     07  -  Totals of annual prem  (exclude Reassurance)
*     08  -  Totals of single prem  (exclude Reassurance)
*     09  -  Totals of reassurance
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*****************************************************************
* </pre>
*/
public class Bj531 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlgovepfAllrs = null;
	private java.sql.PreparedStatement sqlgovepfAllps = null;
	private java.sql.Connection sqlgovepfAllconn = null;
	private String sqlgovepfAll = "";
	private java.sql.ResultSet sqlgovepfrs = null;
	private java.sql.PreparedStatement sqlgovepfps = null;
	private java.sql.Connection sqlgovepfconn = null;
	private String sqlgovepf = "";
	private Rj531Report rj531 = new Rj531Report();
	private FixedLengthStringData rj531Rec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ531");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

		/*01  WSAA-TR386-AREA.                                             */
	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaProgram = new FixedLengthStringData(5).isAPartOf(wsaaItem, 1);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String wsaaFirstRecord = "";
	private FixedLengthStringData wsaaIf = new FixedLengthStringData(2).init("IF");
	private FixedLengthStringData wsaaIn = new FixedLengthStringData(2).init("IN");
	private FixedLengthStringData wsaaDe = new FixedLengthStringData(2).init("DE");
	private static final String wsaaFp = "FP";
	private static final String wsaaPu = "PU";
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);
	private ZonedDecimalData wsaaScrate = new ZonedDecimalData(12, 7);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData mth = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaSelectAll = new FixedLengthStringData(1);
	private Validator selectAll = new Validator(wsaaSelectAll, "Y");
	private FixedLengthStringData wsaaTj675Stsect01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect02 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect03 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect04 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect05 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect06 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect07 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect08 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect09 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect10 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStssect = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaSrcebus = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaReg = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaParind = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBillingFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreqN = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillingFreq, 0, REDEFINE).setUnsigned();
	private PackedDecimalData wsaaStcmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStlmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStimthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStsmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStbmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStraamtc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRiStbamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAnnlBasic = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAnnlLoaded = new PackedDecimalData(18, 2);
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String t1693 = "T1693";
	private static final String t3589 = "T3589";
	private static final String t3629 = "T3629";
	private static final String t5684 = "T5684";
	private static final String t5685 = "T5685";
	private static final String tj675 = "TJ675";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private static final int ct08 = 8;
	private static final int ct09 = 9;
		/* ERRORS */
	private static final String esql = "ESQL";
	private static final String g418 = "G418";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rj531H02 = new FixedLengthStringData(2);

		/*    COPY DD-RJ531H02-O OF RJ531.                                 */
	private FixedLengthStringData rj531D01 = new FixedLengthStringData(78);
	private FixedLengthStringData rj531d01O = new FixedLengthStringData(78).isAPartOf(rj531D01, 0);
	private ZonedDecimalData stcmthg = new ZonedDecimalData(9, 0).isAPartOf(rj531d01O, 0);
	private ZonedDecimalData stlmth = new ZonedDecimalData(9, 0).isAPartOf(rj531d01O, 9);
	private ZonedDecimalData repamt01 = new ZonedDecimalData(15, 0).isAPartOf(rj531d01O, 18);
	private ZonedDecimalData repamt02 = new ZonedDecimalData(15, 0).isAPartOf(rj531d01O, 33);
	private ZonedDecimalData repamt03 = new ZonedDecimalData(15, 0).isAPartOf(rj531d01O, 48);
	private ZonedDecimalData repamt04 = new ZonedDecimalData(15, 0).isAPartOf(rj531d01O, 63);

	private FixedLengthStringData rj531D02 = new FixedLengthStringData(2);

		/*    COPY DD-RJ531D02-O OF RJ531.                                 */
	private FixedLengthStringData rj531D03 = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Pj517par pj517par = new Pj517par();
	private T3629rec t3629rec = new T3629rec();
	private Tj675rec tj675rec = new Tj675rec();
	private Rj531H01Inner rj531H01Inner = new Rj531H01Inner();
	private SqlGovepfInner sqlGovepfInner = new SqlGovepfInner();
	private WsaaAmountAreasInner wsaaAmountAreasInner = new WsaaAmountAreasInner();
	private WsaaSqlKeepInner wsaaSqlKeepInner = new WsaaSqlKeepInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		a220CallItemio
	}

	public Bj531() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/* Open required files.*/
		rj531.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsaaAcctyr.set(pj517par.acctyr);
		wsspEdterror.set(varcom.oK);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rj531H01Inner.rh01Sdate.set(datcon1rec.extDate);
		wsaaProgram.set(wsaaProg);
		wsaaLanguage.set(bsscIO.getLanguage());
		rj531H01Inner.acctyear.set(pj517par.acctyr);
		rj531H01Inner.acctmonth.set(pj517par.acctmnth);
		/*    PERFORM 1100-READ-TR386.                                     */
		a000InitialWs();
		for (wsaaX.set(1); !(isGT(wsaaX,12)); wsaaX.add(1)){
			wsaaSqlKeepInner.wsaaStcmth[wsaaX.toInt()].set(0);
			wsaaSqlKeepInner.wsaaStlmth[wsaaX.toInt()].set(0);
			wsaaSqlKeepInner.wsaaStimth[wsaaX.toInt()].set(0);
			wsaaSqlKeepInner.wsaaStsmth[wsaaX.toInt()].set(0);
			wsaaSqlKeepInner.wsaaStraamt[wsaaX.toInt()].set(0);
			wsaaSqlKeepInner.wsaaStamth[wsaaX.toInt()].set(0);
			wsaaSqlKeepInner.wsaaStbmthg[wsaaX.toInt()].set(0);
			wsaaSqlKeepInner.wsaaStldmth[wsaaX.toInt()].set(0);
		}
		wsaaX.set(0);
		wsaaStsect.set(SPACES);
		wsaaStssect.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaReg.set(SPACES);
		wsaaFirstRecord = "Y";
		readTj6751200();
	}

protected void readTj6751200()
	{
		para1210();
	}

protected void para1210()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tj675);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		wsaaSelectAll.set("Y");
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			selectAll1300();
		}
		else {
			wsaaSelectAll.set("N");
			tj675rec.tj675Rec.set(itemIO.getGenarea());
			selectSpecified1400();
		}
	}

protected void selectAll1300()
	{
		/*PARA*/
		sqlgovepfAll = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, PARIND, BILLFREQ, BFWDC, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, BFWDL, STLMTH01, STLMTH02, STLMTH03, STLMTH04, STLMTH05, STLMTH06, STLMTH07, STLMTH08, STLMTH09, STLMTH10, STLMTH11, STLMTH12, BFWDI, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, BFWDA, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12, BFWDB, STBMTHG01, STBMTHG02, STBMTHG03, STBMTHG04, STBMTHG05, STBMTHG06, STBMTHG07, STBMTHG08, STBMTHG09, STBMTHG10, STBMTHG11, STBMTHG12, BFWDS, STSMTH01, STSMTH02, STSMTH03, STSMTH04, STSMTH05, STSMTH06, STSMTH07, STSMTH08, STSMTH09, STSMTH10, STSMTH11, STSMTH12, BFWDP, STPMTH01, STPMTH02, STPMTH03, STPMTH04, STPMTH05, STPMTH06, STPMTH07, STPMTH08, STPMTH09, STPMTH10, STPMTH11, STPMTH12, BFWDRA, STRAAMT01, STRAAMT02, STRAAMT03, STRAAMT04, STRAAMT05, STRAAMT06, STRAAMT07, STRAAMT08, STRAAMT09, STRAAMT10, STRAAMT11, STRAAMT12, BFWDLD, STLDMTHG01, STLDMTHG02, STLDMTHG03, STLDMTHG04, STLDMTHG05, STLDMTHG06, STLDMTHG07, STLDMTHG08, STLDMTHG09, STLDMTHG10, STLDMTHG11, STLDMTHG12" + //ILIFE-3496
" FROM   " + getAppVars().getTableNameOverriden("GOVEPF") + " " +
" WHERE STATCAT IN (?, ?, ?)" +
" AND ACCTYR = ?" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfAllconn = getAppVars().getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfAllps = getAppVars().prepareStatementEmbeded(sqlgovepfAllconn, sqlgovepfAll, "GOVEPF");
			getAppVars().setDBString(sqlgovepfAllps, 1, wsaaIf);
			getAppVars().setDBString(sqlgovepfAllps, 2, wsaaDe);
			getAppVars().setDBString(sqlgovepfAllps, 3, wsaaIn);
			getAppVars().setDBNumber(sqlgovepfAllps, 4, wsaaAcctyr);
			sqlgovepfAllrs = getAppVars().executeQuery(sqlgovepfAllps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void selectSpecified1400()
	{
		para1410();
	}

protected void para1410()
	{
		wsaaTj675Stsect01.set(tj675rec.statSect[1]);
		wsaaTj675Stsect02.set(tj675rec.statSect[2]);
		wsaaTj675Stsect03.set(tj675rec.statSect[3]);
		wsaaTj675Stsect04.set(tj675rec.statSect[4]);
		wsaaTj675Stsect05.set(tj675rec.statSect[5]);
		wsaaTj675Stsect06.set(tj675rec.statSect[6]);
		wsaaTj675Stsect07.set(tj675rec.statSect[7]);
		wsaaTj675Stsect08.set(tj675rec.statSect[8]);
		wsaaTj675Stsect09.set(tj675rec.statSect[9]);
		wsaaTj675Stsect10.set(tj675rec.statSect[10]);
		sqlgovepf = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, PARIND, BILLFREQ, BFWDC, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, BFWDL, STLMTH01, STLMTH02, STLMTH03, STLMTH04, STLMTH05, STLMTH06, STLMTH07, STLMTH08, STLMTH09, STLMTH10, STLMTH11, STLMTH12, BFWDI, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, BFWDA, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12, BFWDB, STBMTHG01, STBMTHG02, STBMTHG03, STBMTHG04, STBMTHG05, STBMTHG06, STBMTHG07, STBMTHG08, STBMTHG09, STBMTHG10, STBMTHG11, STBMTHG12, BFWDS, STSMTH01, STSMTH02, STSMTH03, STSMTH04, STSMTH05, STSMTH06, STSMTH07, STSMTH08, STSMTH09, STSMTH10, STSMTH11, STSMTH12, BFWDP, STPMTH01, STPMTH02, STPMTH03, STPMTH04, STPMTH05, STPMTH06, STPMTH07, STPMTH08, STPMTH09, STPMTH10, STPMTH11, STPMTH12, BFWDRA, STRAAMT01, STRAAMT02, STRAAMT03, STRAAMT04, STRAAMT05, STRAAMT06, STRAAMT07, STRAAMT08, STRAAMT09, STRAAMT10, STRAAMT11, STRAAMT12, BFWDLD, STLDMTHG01, STLDMTHG02, STLDMTHG03, STLDMTHG04, STLDMTHG05, STLDMTHG06, STLDMTHG07, STLDMTHG08, STLDMTHG09, STLDMTHG10, STLDMTHG11, STLDMTHG12" + //ILIFE-3496
" FROM   " + getAppVars().getTableNameOverriden("GOVEPF") + " " +
" WHERE STATCAT IN (?, ?, ?)" +
" AND ACCTYR = ?" +
" AND (STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?)" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfconn = getAppVars().getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfps = getAppVars().prepareStatementEmbeded(sqlgovepfconn, sqlgovepf, "GOVEPF");
			getAppVars().setDBString(sqlgovepfps, 1, wsaaIf);
			getAppVars().setDBString(sqlgovepfps, 2, wsaaDe);
			getAppVars().setDBString(sqlgovepfps, 3, wsaaIn);
			getAppVars().setDBNumber(sqlgovepfps, 4, wsaaAcctyr);
			getAppVars().setDBString(sqlgovepfps, 5, wsaaTj675Stsect01);
			getAppVars().setDBString(sqlgovepfps, 6, wsaaTj675Stsect02);
			getAppVars().setDBString(sqlgovepfps, 7, wsaaTj675Stsect03);
			getAppVars().setDBString(sqlgovepfps, 8, wsaaTj675Stsect04);
			getAppVars().setDBString(sqlgovepfps, 9, wsaaTj675Stsect05);
			getAppVars().setDBString(sqlgovepfps, 10, wsaaTj675Stsect06);
			getAppVars().setDBString(sqlgovepfps, 11, wsaaTj675Stsect07);
			getAppVars().setDBString(sqlgovepfps, 12, wsaaTj675Stsect08);
			getAppVars().setDBString(sqlgovepfps, 13, wsaaTj675Stsect09);
			getAppVars().setDBString(sqlgovepfps, 14, wsaaTj675Stsect10);
			sqlgovepfrs = getAppVars().executeQuery(sqlgovepfps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readFile2010();
				case eof2080:
					eof2080();
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		if (selectAll.isTrue()) {
			sqlerrorflag = false;
			try {
				if (getAppVars().fetchNext(sqlgovepfAllrs)) {
					getAppVars().getDBObject(sqlgovepfAllrs, 1, sqlGovepfInner.sqlChdrcoy);
					getAppVars().getDBObject(sqlgovepfAllrs, 2, sqlGovepfInner.sqlStatcat);
					getAppVars().getDBObject(sqlgovepfAllrs, 3, sqlGovepfInner.sqlAcctyr);
					getAppVars().getDBObject(sqlgovepfAllrs, 4, sqlGovepfInner.sqlStfund);
					getAppVars().getDBObject(sqlgovepfAllrs, 5, sqlGovepfInner.sqlStsect);
					getAppVars().getDBObject(sqlgovepfAllrs, 6, sqlGovepfInner.sqlStssect);
					getAppVars().getDBObject(sqlgovepfAllrs, 7, sqlGovepfInner.sqlRegister);
					getAppVars().getDBObject(sqlgovepfAllrs, 8, sqlGovepfInner.sqlCntbranch);
					getAppVars().getDBObject(sqlgovepfAllrs, 9, sqlGovepfInner.sqlCrpstat);
					getAppVars().getDBObject(sqlgovepfAllrs, 10, sqlGovepfInner.sqlCnpstat);
					getAppVars().getDBObject(sqlgovepfAllrs, 11, sqlGovepfInner.sqlCnttype);
					getAppVars().getDBObject(sqlgovepfAllrs, 12, sqlGovepfInner.sqlCrtable);
					getAppVars().getDBObject(sqlgovepfAllrs, 13, sqlGovepfInner.sqlAcctccy);
					getAppVars().getDBObject(sqlgovepfAllrs, 14, sqlGovepfInner.sqlCntcurr);
					getAppVars().getDBObject(sqlgovepfAllrs, 15, sqlGovepfInner.sqlParind);
					getAppVars().getDBObject(sqlgovepfAllrs, 16, sqlGovepfInner.sqlBillfreq);
					getAppVars().getDBObject(sqlgovepfAllrs, 17, sqlGovepfInner.sqlBfwdc);
					getAppVars().getDBObject(sqlgovepfAllrs, 18, sqlGovepfInner.sqlStcmth01);
					getAppVars().getDBObject(sqlgovepfAllrs, 19, sqlGovepfInner.sqlStcmth02);
					getAppVars().getDBObject(sqlgovepfAllrs, 20, sqlGovepfInner.sqlStcmth03);
					getAppVars().getDBObject(sqlgovepfAllrs, 21, sqlGovepfInner.sqlStcmth04);
					getAppVars().getDBObject(sqlgovepfAllrs, 22, sqlGovepfInner.sqlStcmth05);
					getAppVars().getDBObject(sqlgovepfAllrs, 23, sqlGovepfInner.sqlStcmth06);
					getAppVars().getDBObject(sqlgovepfAllrs, 24, sqlGovepfInner.sqlStcmth07);
					getAppVars().getDBObject(sqlgovepfAllrs, 25, sqlGovepfInner.sqlStcmth08);
					getAppVars().getDBObject(sqlgovepfAllrs, 26, sqlGovepfInner.sqlStcmth09);
					getAppVars().getDBObject(sqlgovepfAllrs, 27, sqlGovepfInner.sqlStcmth10);
					getAppVars().getDBObject(sqlgovepfAllrs, 28, sqlGovepfInner.sqlStcmth11);
					getAppVars().getDBObject(sqlgovepfAllrs, 29, sqlGovepfInner.sqlStcmth12);
					getAppVars().getDBObject(sqlgovepfAllrs, 30, sqlGovepfInner.sqlBfwdl);
					getAppVars().getDBObject(sqlgovepfAllrs, 31, sqlGovepfInner.sqlStlmth01);
					getAppVars().getDBObject(sqlgovepfAllrs, 32, sqlGovepfInner.sqlStlmth02);
					getAppVars().getDBObject(sqlgovepfAllrs, 33, sqlGovepfInner.sqlStlmth03);
					getAppVars().getDBObject(sqlgovepfAllrs, 34, sqlGovepfInner.sqlStlmth04);
					getAppVars().getDBObject(sqlgovepfAllrs, 35, sqlGovepfInner.sqlStlmth05);
					getAppVars().getDBObject(sqlgovepfAllrs, 36, sqlGovepfInner.sqlStlmth06);
					getAppVars().getDBObject(sqlgovepfAllrs, 37, sqlGovepfInner.sqlStlmth07);
					getAppVars().getDBObject(sqlgovepfAllrs, 38, sqlGovepfInner.sqlStlmth08);
					getAppVars().getDBObject(sqlgovepfAllrs, 39, sqlGovepfInner.sqlStlmth09);
					getAppVars().getDBObject(sqlgovepfAllrs, 40, sqlGovepfInner.sqlStlmth10);
					getAppVars().getDBObject(sqlgovepfAllrs, 41, sqlGovepfInner.sqlStlmth11);
					getAppVars().getDBObject(sqlgovepfAllrs, 42, sqlGovepfInner.sqlStlmth12);
					getAppVars().getDBObject(sqlgovepfAllrs, 43, sqlGovepfInner.sqlBfwdi);
					getAppVars().getDBObject(sqlgovepfAllrs, 44, sqlGovepfInner.sqlStimth01);
					getAppVars().getDBObject(sqlgovepfAllrs, 45, sqlGovepfInner.sqlStimth02);
					getAppVars().getDBObject(sqlgovepfAllrs, 46, sqlGovepfInner.sqlStimth03);
					getAppVars().getDBObject(sqlgovepfAllrs, 47, sqlGovepfInner.sqlStimth04);
					getAppVars().getDBObject(sqlgovepfAllrs, 48, sqlGovepfInner.sqlStimth05);
					getAppVars().getDBObject(sqlgovepfAllrs, 49, sqlGovepfInner.sqlStimth06);
					getAppVars().getDBObject(sqlgovepfAllrs, 50, sqlGovepfInner.sqlStimth07);
					getAppVars().getDBObject(sqlgovepfAllrs, 51, sqlGovepfInner.sqlStimth08);
					getAppVars().getDBObject(sqlgovepfAllrs, 52, sqlGovepfInner.sqlStimth09);
					getAppVars().getDBObject(sqlgovepfAllrs, 53, sqlGovepfInner.sqlStimth10);
					getAppVars().getDBObject(sqlgovepfAllrs, 54, sqlGovepfInner.sqlStimth11);
					getAppVars().getDBObject(sqlgovepfAllrs, 55, sqlGovepfInner.sqlStimth12);
					getAppVars().getDBObject(sqlgovepfAllrs, 56, sqlGovepfInner.sqlBfwda);
					getAppVars().getDBObject(sqlgovepfAllrs, 57, sqlGovepfInner.sqlStamth01);
					getAppVars().getDBObject(sqlgovepfAllrs, 58, sqlGovepfInner.sqlStamth02);
					getAppVars().getDBObject(sqlgovepfAllrs, 59, sqlGovepfInner.sqlStamth03);
					getAppVars().getDBObject(sqlgovepfAllrs, 60, sqlGovepfInner.sqlStamth04);
					getAppVars().getDBObject(sqlgovepfAllrs, 61, sqlGovepfInner.sqlStamth05);
					getAppVars().getDBObject(sqlgovepfAllrs, 62, sqlGovepfInner.sqlStamth06);
					getAppVars().getDBObject(sqlgovepfAllrs, 63, sqlGovepfInner.sqlStamth07);
					getAppVars().getDBObject(sqlgovepfAllrs, 64, sqlGovepfInner.sqlStamth08);
					getAppVars().getDBObject(sqlgovepfAllrs, 65, sqlGovepfInner.sqlStamth09);
					getAppVars().getDBObject(sqlgovepfAllrs, 66, sqlGovepfInner.sqlStamth10);
					getAppVars().getDBObject(sqlgovepfAllrs, 67, sqlGovepfInner.sqlStamth11);
					getAppVars().getDBObject(sqlgovepfAllrs, 68, sqlGovepfInner.sqlStamth12);
					getAppVars().getDBObject(sqlgovepfAllrs, 69, sqlGovepfInner.sqlBfwdb);
					getAppVars().getDBObject(sqlgovepfAllrs, 70, sqlGovepfInner.sqlStbmthg01);
					getAppVars().getDBObject(sqlgovepfAllrs, 71, sqlGovepfInner.sqlStbmthg02);
					getAppVars().getDBObject(sqlgovepfAllrs, 72, sqlGovepfInner.sqlStbmthg03);
					getAppVars().getDBObject(sqlgovepfAllrs, 73, sqlGovepfInner.sqlStbmthg04);
					getAppVars().getDBObject(sqlgovepfAllrs, 74, sqlGovepfInner.sqlStbmthg05);
					getAppVars().getDBObject(sqlgovepfAllrs, 75, sqlGovepfInner.sqlStbmthg06);
					getAppVars().getDBObject(sqlgovepfAllrs, 76, sqlGovepfInner.sqlStbmthg07);
					getAppVars().getDBObject(sqlgovepfAllrs, 77, sqlGovepfInner.sqlStbmthg08);
					getAppVars().getDBObject(sqlgovepfAllrs, 78, sqlGovepfInner.sqlStbmthg09);
					getAppVars().getDBObject(sqlgovepfAllrs, 79, sqlGovepfInner.sqlStbmthg10);
					getAppVars().getDBObject(sqlgovepfAllrs, 80, sqlGovepfInner.sqlStbmthg11);
					getAppVars().getDBObject(sqlgovepfAllrs, 81, sqlGovepfInner.sqlStbmthg12);
					getAppVars().getDBObject(sqlgovepfAllrs, 82, sqlGovepfInner.sqlBfwds);
					getAppVars().getDBObject(sqlgovepfAllrs, 83, sqlGovepfInner.sqlStsmth01);
					getAppVars().getDBObject(sqlgovepfAllrs, 84, sqlGovepfInner.sqlStsmth02);
					getAppVars().getDBObject(sqlgovepfAllrs, 85, sqlGovepfInner.sqlStsmth03);
					getAppVars().getDBObject(sqlgovepfAllrs, 86, sqlGovepfInner.sqlStsmth04);
					getAppVars().getDBObject(sqlgovepfAllrs, 87, sqlGovepfInner.sqlStsmth05);
					getAppVars().getDBObject(sqlgovepfAllrs, 88, sqlGovepfInner.sqlStsmth06);
					getAppVars().getDBObject(sqlgovepfAllrs, 89, sqlGovepfInner.sqlStsmth07);
					getAppVars().getDBObject(sqlgovepfAllrs, 90, sqlGovepfInner.sqlStsmth08);
					getAppVars().getDBObject(sqlgovepfAllrs, 91, sqlGovepfInner.sqlStsmth09);
					getAppVars().getDBObject(sqlgovepfAllrs, 92, sqlGovepfInner.sqlStsmth10);
					getAppVars().getDBObject(sqlgovepfAllrs, 93, sqlGovepfInner.sqlStsmth11);
					getAppVars().getDBObject(sqlgovepfAllrs, 94, sqlGovepfInner.sqlStsmth12);
					getAppVars().getDBObject(sqlgovepfAllrs, 95, sqlGovepfInner.sqlBfwdp);
					getAppVars().getDBObject(sqlgovepfAllrs, 96, sqlGovepfInner.sqlStpmth01);
					getAppVars().getDBObject(sqlgovepfAllrs, 97, sqlGovepfInner.sqlStpmth02);
					getAppVars().getDBObject(sqlgovepfAllrs, 98, sqlGovepfInner.sqlStpmth03);
					getAppVars().getDBObject(sqlgovepfAllrs, 99, sqlGovepfInner.sqlStpmth04);
					getAppVars().getDBObject(sqlgovepfAllrs, 100, sqlGovepfInner.sqlStpmth05);
					getAppVars().getDBObject(sqlgovepfAllrs, 101, sqlGovepfInner.sqlStpmth06);
					getAppVars().getDBObject(sqlgovepfAllrs, 102, sqlGovepfInner.sqlStpmth07);
					getAppVars().getDBObject(sqlgovepfAllrs, 103, sqlGovepfInner.sqlStpmth08);
					getAppVars().getDBObject(sqlgovepfAllrs, 104, sqlGovepfInner.sqlStpmth09);
					getAppVars().getDBObject(sqlgovepfAllrs, 105, sqlGovepfInner.sqlStpmth10);
					getAppVars().getDBObject(sqlgovepfAllrs, 106, sqlGovepfInner.sqlStpmth11);
					getAppVars().getDBObject(sqlgovepfAllrs, 107, sqlGovepfInner.sqlStpmth12);
					getAppVars().getDBObject(sqlgovepfAllrs, 108, sqlGovepfInner.sqlBfwdra);
					getAppVars().getDBObject(sqlgovepfAllrs, 109, sqlGovepfInner.sqlStraamt01);
					getAppVars().getDBObject(sqlgovepfAllrs, 110, sqlGovepfInner.sqlStraamt02);
					getAppVars().getDBObject(sqlgovepfAllrs, 111, sqlGovepfInner.sqlStraamt03);
					getAppVars().getDBObject(sqlgovepfAllrs, 112, sqlGovepfInner.sqlStraamt04);
					getAppVars().getDBObject(sqlgovepfAllrs, 113, sqlGovepfInner.sqlStraamt05);
					getAppVars().getDBObject(sqlgovepfAllrs, 114, sqlGovepfInner.sqlStraamt06);
					getAppVars().getDBObject(sqlgovepfAllrs, 115, sqlGovepfInner.sqlStraamt07);
					getAppVars().getDBObject(sqlgovepfAllrs, 116, sqlGovepfInner.sqlStraamt08);
					getAppVars().getDBObject(sqlgovepfAllrs, 117, sqlGovepfInner.sqlStraamt09);
					getAppVars().getDBObject(sqlgovepfAllrs, 118, sqlGovepfInner.sqlStraamt10);
					getAppVars().getDBObject(sqlgovepfAllrs, 119, sqlGovepfInner.sqlStraamt11);
					getAppVars().getDBObject(sqlgovepfAllrs, 120, sqlGovepfInner.sqlStraamt12);
					getAppVars().getDBObject(sqlgovepfAllrs, 121, sqlGovepfInner.sqlBfwdld);
					getAppVars().getDBObject(sqlgovepfAllrs, 122, sqlGovepfInner.sqlStldmthg01);
					getAppVars().getDBObject(sqlgovepfAllrs, 123, sqlGovepfInner.sqlStldmthg02);
					getAppVars().getDBObject(sqlgovepfAllrs, 124, sqlGovepfInner.sqlStldmthg03);
					getAppVars().getDBObject(sqlgovepfAllrs, 125, sqlGovepfInner.sqlStldmthg04);
					getAppVars().getDBObject(sqlgovepfAllrs, 126, sqlGovepfInner.sqlStldmthg05);
					getAppVars().getDBObject(sqlgovepfAllrs, 127, sqlGovepfInner.sqlStldmthg06);
					getAppVars().getDBObject(sqlgovepfAllrs, 128, sqlGovepfInner.sqlStldmthg07);
					getAppVars().getDBObject(sqlgovepfAllrs, 129, sqlGovepfInner.sqlStldmthg08);
					getAppVars().getDBObject(sqlgovepfAllrs, 130, sqlGovepfInner.sqlStldmthg09);
					getAppVars().getDBObject(sqlgovepfAllrs, 131, sqlGovepfInner.sqlStldmthg10);
					getAppVars().getDBObject(sqlgovepfAllrs, 132, sqlGovepfInner.sqlStldmthg11);
					getAppVars().getDBObject(sqlgovepfAllrs, 133, sqlGovepfInner.sqlStldmthg12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlerrorflag = true;
				getAppVars().setSqlErrorCode(ex);
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		else {
			sqlerrorflag = false;
			try {
				if (getAppVars().fetchNext(sqlgovepfrs)) {
					getAppVars().getDBObject(sqlgovepfrs, 1, sqlGovepfInner.sqlChdrcoy);
					getAppVars().getDBObject(sqlgovepfrs, 2, sqlGovepfInner.sqlStatcat);
					getAppVars().getDBObject(sqlgovepfrs, 3, sqlGovepfInner.sqlAcctyr);
					getAppVars().getDBObject(sqlgovepfrs, 4, sqlGovepfInner.sqlStfund);
					getAppVars().getDBObject(sqlgovepfrs, 5, sqlGovepfInner.sqlStsect);
					getAppVars().getDBObject(sqlgovepfrs, 6, sqlGovepfInner.sqlStssect);
					getAppVars().getDBObject(sqlgovepfrs, 7, sqlGovepfInner.sqlRegister);
					getAppVars().getDBObject(sqlgovepfrs, 8, sqlGovepfInner.sqlCntbranch);
					getAppVars().getDBObject(sqlgovepfrs, 9, sqlGovepfInner.sqlCrpstat);
					getAppVars().getDBObject(sqlgovepfrs, 10, sqlGovepfInner.sqlCnpstat);
					getAppVars().getDBObject(sqlgovepfrs, 11, sqlGovepfInner.sqlCnttype);
					getAppVars().getDBObject(sqlgovepfrs, 12, sqlGovepfInner.sqlCrtable);
					getAppVars().getDBObject(sqlgovepfrs, 13, sqlGovepfInner.sqlAcctccy);
					getAppVars().getDBObject(sqlgovepfrs, 14, sqlGovepfInner.sqlCntcurr);
					getAppVars().getDBObject(sqlgovepfrs, 15, sqlGovepfInner.sqlParind);
					getAppVars().getDBObject(sqlgovepfrs, 16, sqlGovepfInner.sqlBillfreq);
					getAppVars().getDBObject(sqlgovepfrs, 17, sqlGovepfInner.sqlBfwdc);
					getAppVars().getDBObject(sqlgovepfrs, 18, sqlGovepfInner.sqlStcmth01);
					getAppVars().getDBObject(sqlgovepfrs, 19, sqlGovepfInner.sqlStcmth02);
					getAppVars().getDBObject(sqlgovepfrs, 20, sqlGovepfInner.sqlStcmth03);
					getAppVars().getDBObject(sqlgovepfrs, 21, sqlGovepfInner.sqlStcmth04);
					getAppVars().getDBObject(sqlgovepfrs, 22, sqlGovepfInner.sqlStcmth05);
					getAppVars().getDBObject(sqlgovepfrs, 23, sqlGovepfInner.sqlStcmth06);
					getAppVars().getDBObject(sqlgovepfrs, 24, sqlGovepfInner.sqlStcmth07);
					getAppVars().getDBObject(sqlgovepfrs, 25, sqlGovepfInner.sqlStcmth08);
					getAppVars().getDBObject(sqlgovepfrs, 26, sqlGovepfInner.sqlStcmth09);
					getAppVars().getDBObject(sqlgovepfrs, 27, sqlGovepfInner.sqlStcmth10);
					getAppVars().getDBObject(sqlgovepfrs, 28, sqlGovepfInner.sqlStcmth11);
					getAppVars().getDBObject(sqlgovepfrs, 29, sqlGovepfInner.sqlStcmth12);
					getAppVars().getDBObject(sqlgovepfrs, 30, sqlGovepfInner.sqlBfwdl);
					getAppVars().getDBObject(sqlgovepfrs, 31, sqlGovepfInner.sqlStlmth01);
					getAppVars().getDBObject(sqlgovepfrs, 32, sqlGovepfInner.sqlStlmth02);
					getAppVars().getDBObject(sqlgovepfrs, 33, sqlGovepfInner.sqlStlmth03);
					getAppVars().getDBObject(sqlgovepfrs, 34, sqlGovepfInner.sqlStlmth04);
					getAppVars().getDBObject(sqlgovepfrs, 35, sqlGovepfInner.sqlStlmth05);
					getAppVars().getDBObject(sqlgovepfrs, 36, sqlGovepfInner.sqlStlmth06);
					getAppVars().getDBObject(sqlgovepfrs, 37, sqlGovepfInner.sqlStlmth07);
					getAppVars().getDBObject(sqlgovepfrs, 38, sqlGovepfInner.sqlStlmth08);
					getAppVars().getDBObject(sqlgovepfrs, 39, sqlGovepfInner.sqlStlmth09);
					getAppVars().getDBObject(sqlgovepfrs, 40, sqlGovepfInner.sqlStlmth10);
					getAppVars().getDBObject(sqlgovepfrs, 41, sqlGovepfInner.sqlStlmth11);
					getAppVars().getDBObject(sqlgovepfrs, 42, sqlGovepfInner.sqlStlmth12);
					getAppVars().getDBObject(sqlgovepfrs, 43, sqlGovepfInner.sqlBfwdi);
					getAppVars().getDBObject(sqlgovepfrs, 44, sqlGovepfInner.sqlStimth01);
					getAppVars().getDBObject(sqlgovepfrs, 45, sqlGovepfInner.sqlStimth02);
					getAppVars().getDBObject(sqlgovepfrs, 46, sqlGovepfInner.sqlStimth03);
					getAppVars().getDBObject(sqlgovepfrs, 47, sqlGovepfInner.sqlStimth04);
					getAppVars().getDBObject(sqlgovepfrs, 48, sqlGovepfInner.sqlStimth05);
					getAppVars().getDBObject(sqlgovepfrs, 49, sqlGovepfInner.sqlStimth06);
					getAppVars().getDBObject(sqlgovepfrs, 50, sqlGovepfInner.sqlStimth07);
					getAppVars().getDBObject(sqlgovepfrs, 51, sqlGovepfInner.sqlStimth08);
					getAppVars().getDBObject(sqlgovepfrs, 52, sqlGovepfInner.sqlStimth09);
					getAppVars().getDBObject(sqlgovepfrs, 53, sqlGovepfInner.sqlStimth10);
					getAppVars().getDBObject(sqlgovepfrs, 54, sqlGovepfInner.sqlStimth11);
					getAppVars().getDBObject(sqlgovepfrs, 55, sqlGovepfInner.sqlStimth12);
					getAppVars().getDBObject(sqlgovepfrs, 56, sqlGovepfInner.sqlBfwda);
					getAppVars().getDBObject(sqlgovepfrs, 57, sqlGovepfInner.sqlStamth01);
					getAppVars().getDBObject(sqlgovepfrs, 58, sqlGovepfInner.sqlStamth02);
					getAppVars().getDBObject(sqlgovepfrs, 59, sqlGovepfInner.sqlStamth03);
					getAppVars().getDBObject(sqlgovepfrs, 60, sqlGovepfInner.sqlStamth04);
					getAppVars().getDBObject(sqlgovepfrs, 61, sqlGovepfInner.sqlStamth05);
					getAppVars().getDBObject(sqlgovepfrs, 62, sqlGovepfInner.sqlStamth06);
					getAppVars().getDBObject(sqlgovepfrs, 63, sqlGovepfInner.sqlStamth07);
					getAppVars().getDBObject(sqlgovepfrs, 64, sqlGovepfInner.sqlStamth08);
					getAppVars().getDBObject(sqlgovepfrs, 65, sqlGovepfInner.sqlStamth09);
					getAppVars().getDBObject(sqlgovepfrs, 66, sqlGovepfInner.sqlStamth10);
					getAppVars().getDBObject(sqlgovepfrs, 67, sqlGovepfInner.sqlStamth11);
					getAppVars().getDBObject(sqlgovepfrs, 68, sqlGovepfInner.sqlStamth12);
					getAppVars().getDBObject(sqlgovepfrs, 69, sqlGovepfInner.sqlBfwdb);
					getAppVars().getDBObject(sqlgovepfrs, 70, sqlGovepfInner.sqlStbmthg01);
					getAppVars().getDBObject(sqlgovepfrs, 71, sqlGovepfInner.sqlStbmthg02);
					getAppVars().getDBObject(sqlgovepfrs, 72, sqlGovepfInner.sqlStbmthg03);
					getAppVars().getDBObject(sqlgovepfrs, 73, sqlGovepfInner.sqlStbmthg04);
					getAppVars().getDBObject(sqlgovepfrs, 74, sqlGovepfInner.sqlStbmthg05);
					getAppVars().getDBObject(sqlgovepfrs, 75, sqlGovepfInner.sqlStbmthg06);
					getAppVars().getDBObject(sqlgovepfrs, 76, sqlGovepfInner.sqlStbmthg07);
					getAppVars().getDBObject(sqlgovepfrs, 77, sqlGovepfInner.sqlStbmthg08);
					getAppVars().getDBObject(sqlgovepfrs, 78, sqlGovepfInner.sqlStbmthg09);
					getAppVars().getDBObject(sqlgovepfrs, 79, sqlGovepfInner.sqlStbmthg10);
					getAppVars().getDBObject(sqlgovepfrs, 80, sqlGovepfInner.sqlStbmthg11);
					getAppVars().getDBObject(sqlgovepfrs, 81, sqlGovepfInner.sqlStbmthg12);
					getAppVars().getDBObject(sqlgovepfrs, 82, sqlGovepfInner.sqlBfwds);
					getAppVars().getDBObject(sqlgovepfrs, 83, sqlGovepfInner.sqlStsmth01);
					getAppVars().getDBObject(sqlgovepfrs, 84, sqlGovepfInner.sqlStsmth02);
					getAppVars().getDBObject(sqlgovepfrs, 85, sqlGovepfInner.sqlStsmth03);
					getAppVars().getDBObject(sqlgovepfrs, 86, sqlGovepfInner.sqlStsmth04);
					getAppVars().getDBObject(sqlgovepfrs, 87, sqlGovepfInner.sqlStsmth05);
					getAppVars().getDBObject(sqlgovepfrs, 88, sqlGovepfInner.sqlStsmth06);
					getAppVars().getDBObject(sqlgovepfrs, 89, sqlGovepfInner.sqlStsmth07);
					getAppVars().getDBObject(sqlgovepfrs, 90, sqlGovepfInner.sqlStsmth08);
					getAppVars().getDBObject(sqlgovepfrs, 91, sqlGovepfInner.sqlStsmth09);
					getAppVars().getDBObject(sqlgovepfrs, 92, sqlGovepfInner.sqlStsmth10);
					getAppVars().getDBObject(sqlgovepfrs, 93, sqlGovepfInner.sqlStsmth11);
					getAppVars().getDBObject(sqlgovepfrs, 94, sqlGovepfInner.sqlStsmth12);
					getAppVars().getDBObject(sqlgovepfrs, 95, sqlGovepfInner.sqlBfwdp);
					getAppVars().getDBObject(sqlgovepfrs, 96, sqlGovepfInner.sqlStpmth01);
					getAppVars().getDBObject(sqlgovepfrs, 97, sqlGovepfInner.sqlStpmth02);
					getAppVars().getDBObject(sqlgovepfrs, 98, sqlGovepfInner.sqlStpmth03);
					getAppVars().getDBObject(sqlgovepfrs, 99, sqlGovepfInner.sqlStpmth04);
					getAppVars().getDBObject(sqlgovepfrs, 100, sqlGovepfInner.sqlStpmth05);
					getAppVars().getDBObject(sqlgovepfrs, 101, sqlGovepfInner.sqlStpmth06);
					getAppVars().getDBObject(sqlgovepfrs, 102, sqlGovepfInner.sqlStpmth07);
					getAppVars().getDBObject(sqlgovepfrs, 103, sqlGovepfInner.sqlStpmth08);
					getAppVars().getDBObject(sqlgovepfrs, 104, sqlGovepfInner.sqlStpmth09);
					getAppVars().getDBObject(sqlgovepfrs, 105, sqlGovepfInner.sqlStpmth10);
					getAppVars().getDBObject(sqlgovepfrs, 106, sqlGovepfInner.sqlStpmth11);
					getAppVars().getDBObject(sqlgovepfrs, 107, sqlGovepfInner.sqlStpmth12);
					getAppVars().getDBObject(sqlgovepfrs, 108, sqlGovepfInner.sqlBfwdra);
					getAppVars().getDBObject(sqlgovepfrs, 109, sqlGovepfInner.sqlStraamt01);
					getAppVars().getDBObject(sqlgovepfrs, 110, sqlGovepfInner.sqlStraamt02);
					getAppVars().getDBObject(sqlgovepfrs, 111, sqlGovepfInner.sqlStraamt03);
					getAppVars().getDBObject(sqlgovepfrs, 112, sqlGovepfInner.sqlStraamt04);
					getAppVars().getDBObject(sqlgovepfrs, 113, sqlGovepfInner.sqlStraamt05);
					getAppVars().getDBObject(sqlgovepfrs, 114, sqlGovepfInner.sqlStraamt06);
					getAppVars().getDBObject(sqlgovepfrs, 115, sqlGovepfInner.sqlStraamt07);
					getAppVars().getDBObject(sqlgovepfrs, 116, sqlGovepfInner.sqlStraamt08);
					getAppVars().getDBObject(sqlgovepfrs, 117, sqlGovepfInner.sqlStraamt09);
					getAppVars().getDBObject(sqlgovepfrs, 118, sqlGovepfInner.sqlStraamt10);
					getAppVars().getDBObject(sqlgovepfrs, 119, sqlGovepfInner.sqlStraamt11);
					getAppVars().getDBObject(sqlgovepfrs, 120, sqlGovepfInner.sqlStraamt12);
					getAppVars().getDBObject(sqlgovepfrs, 121, sqlGovepfInner.sqlBfwdld);
					getAppVars().getDBObject(sqlgovepfrs, 122, sqlGovepfInner.sqlStldmthg01);
					getAppVars().getDBObject(sqlgovepfrs, 123, sqlGovepfInner.sqlStldmthg02);
					getAppVars().getDBObject(sqlgovepfrs, 124, sqlGovepfInner.sqlStldmthg03);
					getAppVars().getDBObject(sqlgovepfrs, 125, sqlGovepfInner.sqlStldmthg04);
					getAppVars().getDBObject(sqlgovepfrs, 126, sqlGovepfInner.sqlStldmthg05);
					getAppVars().getDBObject(sqlgovepfrs, 127, sqlGovepfInner.sqlStldmthg06);
					getAppVars().getDBObject(sqlgovepfrs, 128, sqlGovepfInner.sqlStldmthg07);
					getAppVars().getDBObject(sqlgovepfrs, 129, sqlGovepfInner.sqlStldmthg08);
					getAppVars().getDBObject(sqlgovepfrs, 130, sqlGovepfInner.sqlStldmthg09);
					getAppVars().getDBObject(sqlgovepfrs, 131, sqlGovepfInner.sqlStldmthg10);
					getAppVars().getDBObject(sqlgovepfrs, 132, sqlGovepfInner.sqlStldmthg11);
					getAppVars().getDBObject(sqlgovepfrs, 133, sqlGovepfInner.sqlStldmthg12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlerrorflag = true;
				getAppVars().setSqlErrorCode(ex);
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		wsaaSqlKeepInner.wsaaSqlKeep.set(sqlGovepfInner.sqlGoverec);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		if (isNE(wsaaFirstRecord,"Y")) {
			getHeading5000();
			newPage6000();
			printBfReassurance7000();
			printReasaurance8000();
			printAfReassurance9000();
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (isEQ(wsaaFirstRecord,"Y")) {
			wsaaFirstRecord = "N";
		}
		else {
			if (isNE(wsaaStsect, sqlGovepfInner.sqlStsect)
			|| isNE(wsaaReg, sqlGovepfInner.sqlRegister)
			|| isNE(wsaaAcctccy, sqlGovepfInner.sqlAcctccy)
			|| isNE(wsaaCompany, sqlGovepfInner.sqlChdrcoy)
			|| isNE(wsaaStssect, sqlGovepfInner.sqlStssect)
			|| isNE(wsaaBranch, sqlGovepfInner.sqlCntbranch)) {
				getHeading5000();
				newPage6000();
				printBfReassurance7000();
				printReasaurance8000();
				printAfReassurance9000();
				a000InitialWs();
			}
		}
		wsaaBillingFreq.set(sqlGovepfInner.sqlBillfreq);
		for (mth.set(1); !(isGT(mth,pj517par.acctmnth)); mth.add(1)){
			if (isEQ(wsaaSqlKeepInner.wsaaSStatcat, "IF")) {
				wsaaStcmthc.add(wsaaSqlKeepInner.wsaaStcmth[mth.toInt()]);
				wsaaStlmthc.add(wsaaSqlKeepInner.wsaaStlmth[mth.toInt()]);
			}
			wsaaStimthc.add(wsaaSqlKeepInner.wsaaStimth[mth.toInt()]);
			wsaaStsmthc.add(wsaaSqlKeepInner.wsaaStsmth[mth.toInt()]);
			wsaaStamthc.add(wsaaSqlKeepInner.wsaaStamth[mth.toInt()]);
			compute(wsaaAnnlBasic, 3).setRounded(mult(wsaaSqlKeepInner.wsaaStbmthg[mth.toInt()], wsaaBillfreqN));
			compute(wsaaAnnlLoaded, 3).setRounded(mult(wsaaSqlKeepInner.wsaaStldmth[mth.toInt()], wsaaBillfreqN));
			compute(wsaaStbmthc, 2).add(add(wsaaAnnlBasic,wsaaAnnlLoaded));
			wsaaStraamtc.add(wsaaSqlKeepInner.wsaaStraamt[mth.toInt()]);
		}
		/* Add brought forward figures.*/
		if (isEQ(wsaaSqlKeepInner.wsaaSStatcat, "IF")) {
			wsaaStcmthc.add(wsaaSqlKeepInner.wsaaBfwdc);
			wsaaStlmthc.add(wsaaSqlKeepInner.wsaaBfwdl);
		}
		wsaaStimthc.add(wsaaSqlKeepInner.wsaaBfwdi);
		wsaaStsmthc.add(wsaaSqlKeepInner.wsaaBfwds);
		wsaaStamthc.add(wsaaSqlKeepInner.wsaaBfwda);
		wsaaStraamtc.add(wsaaSqlKeepInner.wsaaBfwdra);
		compute(wsaaAnnlBasic, 3).setRounded(mult(wsaaSqlKeepInner.wsaaBfwdb, wsaaBillfreqN));
		compute(wsaaAnnlLoaded, 3).setRounded(mult(wsaaSqlKeepInner.wsaaBfwdld, wsaaBillfreqN));
		compute(wsaaStbmthc, 2).add(add(wsaaAnnlBasic,wsaaAnnlLoaded));
		/* If CNTCURR not equal ACCTCCY we need to convert.*/
		if (isNE(sqlGovepfInner.sqlCntcurr, sqlGovepfInner.sqlAcctccy)) {
			a200ReadT3629();
			compute(wsaaStimthc, 8).setRounded(mult(wsaaStimthc,wsaaScrate));
			compute(wsaaStsmthc, 8).setRounded(mult(wsaaStsmthc,wsaaScrate));
			compute(wsaaStamthc, 8).setRounded(mult(wsaaStamthc,wsaaScrate));
			compute(wsaaStraamtc, 8).setRounded(mult(wsaaStraamtc,wsaaScrate));
			compute(wsaaStbmthc, 8).setRounded(mult(wsaaStbmthc,wsaaScrate));
		}
		/* We need to check if it is a Fully paid up or Reduced Paid up*/
		if (isEQ(sqlGovepfInner.sqlCnpstat, wsaaFp)
		|| isEQ(sqlGovepfInner.sqlCnpstat, wsaaPu)) {
			wsaaAmountAreasInner.wsaaOthNofpol.add(wsaaStcmthc);
			wsaaAmountAreasInner.wsaaOthNoflif.add(wsaaStlmthc);
			wsaaAmountAreasInner.wsaaOthSi.add(wsaaStimthc);
			wsaaAmountAreasInner.wsaaOthAnnuity.add(wsaaStamthc);
			wsaaAmountAreasInner.wsaaOthAprem.add(wsaaStbmthc);
			wsaaAmountAreasInner.wsaaOthSprem.add(wsaaStsmthc);
			if (isNE(wsaaStraamtc,ZERO)) {
				/*        ADD WSAA-STCMTH       TO WSAA-RI-OTH-NOFPOL            */
				/*        ADD WSAA-STLMTH       TO WSAA-RI-OTH-NOFLIF            */
				wsaaAmountAreasInner.wsaaRiOthNofpol.add(wsaaSqlKeepInner.wsaaStcmth[1]);
				wsaaAmountAreasInner.wsaaRiOthNoflif.add(wsaaSqlKeepInner.wsaaStlmth[1]);
				if (isNE(wsaaStimthc,ZERO)) {
					wsaaAmountAreasInner.wsaaRiOthSi.add(wsaaStraamtc);
					if (isNE(wsaaStbmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStimthc)),wsaaStbmthc));
						wsaaAmountAreasInner.wsaaRiOthAprem.add(wsaaRiStbamt);
					}
					if (isNE(wsaaStsmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStimthc)),wsaaStsmthc));
						wsaaAmountAreasInner.wsaaRiOthSprem.add(wsaaRiStbamt);
					}
				}
				else {
					wsaaAmountAreasInner.wsaaRiOthAnnuity.add(wsaaStraamtc);
					if (isNE(wsaaStbmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStamthc)),wsaaStbmthc));
						wsaaAmountAreasInner.wsaaRiOthAprem.add(wsaaRiStbamt);
					}
					if (isNE(wsaaStsmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStamthc)),wsaaStsmthc));
						wsaaAmountAreasInner.wsaaRiOthSprem.add(wsaaRiStbamt);
					}
				}
			}
		}
		else {
			/* Check the billing frequency to determine if its regular premium*/
			/* contract or single premium contract.*/
			/* For regular premium contract, billing freqency is <> '00'.*/
			/* For single premium contract, billing freqency is = '00'.*/
			if (isNE(sqlGovepfInner.sqlBillfreq, "00")) {
				wsaaAmountAreasInner.wsaaRegNofpol.add(wsaaStcmthc);
				wsaaAmountAreasInner.wsaaRegNoflif.add(wsaaStlmthc);
				wsaaAmountAreasInner.wsaaRegSi.add(wsaaStimthc);
				wsaaAmountAreasInner.wsaaRegAnnuity.add(wsaaStamthc);
				wsaaAmountAreasInner.wsaaRegAprem.add(wsaaStbmthc);
				wsaaAmountAreasInner.wsaaRegSprem.add(wsaaStsmthc);
				if (isNE(wsaaStraamtc,ZERO)) {
					wsaaAmountAreasInner.wsaaRiRegNofpol.add(wsaaStcmthc);
					wsaaAmountAreasInner.wsaaRiRegNoflif.add(wsaaStlmthc);
					/* Here, we need to determine if we should report the RI amount*/
					/* under sum assured or annuity.*/
					if (isNE(wsaaStimthc,ZERO)) {
						wsaaAmountAreasInner.wsaaRiRegSi.add(wsaaStraamtc);
						if (isNE(wsaaStbmthc,ZERO)) {
							compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStimthc)),wsaaStbmthc));
							wsaaAmountAreasInner.wsaaRiRegAprem.add(wsaaRiStbamt);
						}
						if (isNE(wsaaStsmthc,ZERO)) {
							compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStimthc)),wsaaStsmthc));
							wsaaAmountAreasInner.wsaaRiRegSprem.add(wsaaRiStbamt);
						}
					}
					else {
						wsaaAmountAreasInner.wsaaRiRegAnnuity.add(wsaaStraamtc);
						if (isNE(wsaaStbmthc,ZERO)) {
							compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStamthc)),wsaaStbmthc));
							wsaaAmountAreasInner.wsaaRiRegAprem.add(wsaaRiStbamt);
						}
						if (isNE(wsaaStsmthc,ZERO)) {
							compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStamthc)),wsaaStsmthc));
							wsaaAmountAreasInner.wsaaRiRegSprem.add(wsaaRiStbamt);
						}
					}
				}
			}
			else {
				/* Single Premium contract*/
				if (isEQ(sqlGovepfInner.sqlBillfreq, "00")) {
					wsaaAmountAreasInner.wsaaSglNofpol.add(wsaaStcmthc);
					wsaaAmountAreasInner.wsaaSglNoflif.add(wsaaStlmthc);
					wsaaAmountAreasInner.wsaaSglSi.add(wsaaStimthc);
					wsaaAmountAreasInner.wsaaSglAnnuity.add(wsaaStamthc);
					wsaaAmountAreasInner.wsaaSglAprem.add(wsaaStbmthc);
					wsaaAmountAreasInner.wsaaSglSprem.add(wsaaStsmthc);
					if (isNE(wsaaStraamtc,ZERO)) {
						wsaaAmountAreasInner.wsaaRiSglNofpol.add(wsaaStcmthc);
						wsaaAmountAreasInner.wsaaRiSglNoflif.add(wsaaStlmthc);
						if (isNE(wsaaStimthc,ZERO)) {
							wsaaAmountAreasInner.wsaaRiSglSi.add(wsaaStraamtc);
							if (isNE(wsaaStsmthc,ZERO)) {
								compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStimthc)),wsaaStsmthc));
								wsaaAmountAreasInner.wsaaRiSglSprem.add(wsaaRiStbamt);
							}
						}
						else {
							wsaaAmountAreasInner.wsaaRiSglAnnuity.add(wsaaStraamtc);
							if (isNE(wsaaStsmthc,ZERO)) {
								compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStamthc)),wsaaStsmthc));
								wsaaAmountAreasInner.wsaaRiSglSprem.add(wsaaRiStbamt);
							}
						}
					}
				}
			}
		}
		wsaaStsect.set(sqlGovepfInner.sqlStsect);
		wsaaStssect.set(sqlGovepfInner.sqlStssect);
		wsaaAcctccy.set(sqlGovepfInner.sqlAcctccy);
		wsaaCompany.set(sqlGovepfInner.sqlChdrcoy);
		wsaaBranch.set(sqlGovepfInner.sqlCntbranch);
		wsaaReg.set(sqlGovepfInner.sqlRegister);
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaStcmthc);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaStlmthc);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(wsaaStimthc);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(wsaaStamthc);
		callContot001();
		contotrec.totno.set(ct07);
		contotrec.totval.set(wsaaStbmthc);
		callContot001();
		contotrec.totno.set(ct08);
		contotrec.totval.set(wsaaStsmthc);
		callContot001();
		contotrec.totno.set(ct09);
		contotrec.totval.set(wsaaStraamtc);
		callContot001();
		wsaaStcmthc.set(ZERO);
		wsaaStlmthc.set(ZERO);
		wsaaStimthc.set(ZERO);
		wsaaStsmthc.set(ZERO);
		wsaaStamthc.set(ZERO);
		wsaaStbmthc.set(ZERO);
		wsaaStraamtc.set(ZERO);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close any open files.*/
		/* Close the cursor*/
		if (selectAll.isTrue()) {
			getAppVars().freeDBConnectionIgnoreErr(sqlgovepfAllconn, sqlgovepfAllps, sqlgovepfAllrs);
		}
		else {
			getAppVars().freeDBConnectionIgnoreErr(sqlgovepfconn, sqlgovepfps, sqlgovepfrs);
		}
		rj531.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void getHeading5000()
	{
		setUpHeadingCompany5020();
		setUpHeadingCurrency5050();
	}

protected void setUpHeadingCompany5020()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rj531H01Inner.rh01Company.set(wsaaCompany);
		rj531H01Inner.rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingCurrency5050()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(wsaaAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rj531H01Inner.acctccy.set(wsaaAcctccy);
		rj531H01Inner.currdesc.set(descIO.getLongdesc());
		/* Get Register*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3589);
		descIO.setDescitem(wsaaReg);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rj531H01Inner.register.set(wsaaReg);
		rj531H01Inner.descrip.set(descIO.getLongdesc());
		/* Business catergory*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5685);
		descIO.setDescitem(wsaaStsect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		rj531H01Inner.stsect.set(wsaaStsect);
		rj531H01Inner.itmdesc.set(descIO.getLongdesc());
		/* Division/Sub-Class/Group*/
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5684);
		descIO.setDescitem(wsaaStssect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		rj531H01Inner.stssect.set(wsaaStssect);
		rj531H01Inner.longdesc.set(descIO.getLongdesc());
	}

protected void newPage6000()
	{
		start6010();
	}

protected void start6010()
	{
		rj531.printRj531h01(rj531H01Inner.rj531H01, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		/*    MOVE WSAA-MESSAGE(09)       TO RNAME   OF RJ531H02-O.        */
		rj531.printRj531h02(rj531H02, indicArea);
		/*    MOVE WSAA-MESSAGE(01)       TO DITDSC  OF RJ531D02-O.        */
		indOn.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(13);
		rj531.printRj531d02(rj531D02, indicArea);
	}

protected void printBfReassurance7000()
	{
		start7010();
	}

protected void start7010()
	{
		/* Print Regular premium*/
		/*    MOVE WSAA-MESSAGE(04)       TO DITDSC  OF RJ531D01-O.        */
		indOn.setTrue(14);
		indOff.setTrue(15);
		indOff.setTrue(16);
		stcmthg.set(wsaaAmountAreasInner.wsaaRegNofpol);
		stlmth.set(wsaaAmountAreasInner.wsaaRegNoflif);
		compute(repamt01, 1).setRounded(div(wsaaAmountAreasInner.wsaaRegSi, 1000));
		compute(repamt02, 1).setRounded(div(wsaaAmountAreasInner.wsaaRegAnnuity, 1000));
		compute(repamt03, 1).setRounded(div(wsaaAmountAreasInner.wsaaRegAprem, 1000));
		compute(repamt04, 1).setRounded(div(wsaaAmountAreasInner.wsaaRegSprem, 1000));
		/* We are storing the reported figures so as to avoid rounding*/
		/* error when computing the figures after reassurance. The figures*/
		/* shown on the report should be used for computation.*/
		wsaaAmountAreasInner.wsaaRegSi.set(repamt01);
		wsaaAmountAreasInner.wsaaRegAnnuity.set(repamt02);
		wsaaAmountAreasInner.wsaaRegAprem.set(repamt03);
		wsaaAmountAreasInner.wsaaRegSprem.set(repamt04);
		rj531.printRj531d01(rj531D01, indicArea);
		/* Print Single premium*/
		/*    MOVE WSAA-MESSAGE(05)       TO DITDSC  OF RJ531D01-O.        */
		indOn.setTrue(15);
		indOff.setTrue(14);
		indOff.setTrue(16);
		stcmthg.set(wsaaAmountAreasInner.wsaaSglNofpol);
		stlmth.set(wsaaAmountAreasInner.wsaaSglNoflif);
		compute(repamt01, 1).setRounded(div(wsaaAmountAreasInner.wsaaSglSi, 1000));
		compute(repamt02, 1).setRounded(div(wsaaAmountAreasInner.wsaaSglAnnuity, 1000));
		compute(repamt03, 1).setRounded(div(wsaaAmountAreasInner.wsaaSglAprem, 1000));
		compute(repamt04, 1).setRounded(div(wsaaAmountAreasInner.wsaaSglSprem, 1000));
		wsaaAmountAreasInner.wsaaSglSi.set(repamt01);
		wsaaAmountAreasInner.wsaaSglAnnuity.set(repamt02);
		wsaaAmountAreasInner.wsaaSglAprem.set(repamt03);
		wsaaAmountAreasInner.wsaaSglSprem.set(repamt04);
		rj531.printRj531d01(rj531D01, indicArea);
		/* Print Other premium*/
		/*    MOVE WSAA-MESSAGE(06)       TO DITDSC  OF RJ531D01-O.        */
		indOn.setTrue(16);
		indOff.setTrue(15);
		indOff.setTrue(14);
		stcmthg.set(wsaaAmountAreasInner.wsaaOthNofpol);
		stlmth.set(wsaaAmountAreasInner.wsaaOthNoflif);
		compute(repamt01, 1).setRounded(div(wsaaAmountAreasInner.wsaaOthSi, 1000));
		compute(repamt02, 1).setRounded(div(wsaaAmountAreasInner.wsaaOthAnnuity, 1000));
		compute(repamt03, 1).setRounded(div(wsaaAmountAreasInner.wsaaOthAprem, 1000));
		compute(repamt04, 1).setRounded(div(wsaaAmountAreasInner.wsaaOthSprem, 1000));
		wsaaAmountAreasInner.wsaaOthSi.set(repamt01);
		wsaaAmountAreasInner.wsaaOthAnnuity.set(repamt02);
		wsaaAmountAreasInner.wsaaOthAprem.set(repamt03);
		wsaaAmountAreasInner.wsaaOthSprem.set(repamt04);
		rj531.printRj531d01(rj531D01, indicArea);
	}

protected void printReasaurance8000()
	{
		start8010();
	}

protected void start8010()
	{
		/* Print Description*/
		rj531.printRj531d03(rj531D03, indicArea);
		/*    MOVE WSAA-MESSAGE(02)       TO DITDSC  OF RJ531D02-O.        */
		indOn.setTrue(12);
		indOff.setTrue(11);
		indOff.setTrue(13);
		rj531.printRj531d02(rj531D02, indicArea);
		/* Print Regular premium*/
		/*    MOVE WSAA-MESSAGE(04)       TO DITDSC  OF RJ531D01-O.        */
		indOn.setTrue(14);
		indOff.setTrue(15);
		indOff.setTrue(16);
		stcmthg.set(wsaaAmountAreasInner.wsaaRiRegNofpol);
		stlmth.set(wsaaAmountAreasInner.wsaaRiRegNoflif);
		compute(repamt01, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiRegSi, 1000));
		compute(repamt02, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiRegAnnuity, 1000));
		compute(repamt03, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiRegAprem, 1000));
		compute(repamt04, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiRegSprem, 1000));
		wsaaAmountAreasInner.wsaaRiRegSi.set(repamt01);
		wsaaAmountAreasInner.wsaaRiRegAnnuity.set(repamt02);
		wsaaAmountAreasInner.wsaaRiRegAprem.set(repamt03);
		wsaaAmountAreasInner.wsaaRiRegSprem.set(repamt04);
		rj531.printRj531d01(rj531D01, indicArea);
		/* Print Single premium*/
		/*    MOVE WSAA-MESSAGE(05)       TO DITDSC  OF RJ531D01-O.        */
		indOn.setTrue(15);
		indOff.setTrue(14);
		indOff.setTrue(16);
		stcmthg.set(wsaaAmountAreasInner.wsaaRiSglNofpol);
		stlmth.set(wsaaAmountAreasInner.wsaaRiSglNoflif);
		compute(repamt01, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiSglSi, 1000));
		compute(repamt02, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiSglAnnuity, 1000));
		compute(repamt03, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiSglAprem, 1000));
		compute(repamt04, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiSglSprem, 1000));
		wsaaAmountAreasInner.wsaaRiSglSi.set(repamt01);
		wsaaAmountAreasInner.wsaaRiSglAnnuity.set(repamt02);
		wsaaAmountAreasInner.wsaaRiSglAprem.set(repamt03);
		wsaaAmountAreasInner.wsaaRiSglSprem.set(repamt04);
		rj531.printRj531d01(rj531D01, indicArea);
		/* Print Other premium*/
		/*    MOVE WSAA-MESSAGE(06)       TO DITDSC  OF RJ531D01-O.        */
		indOn.setTrue(16);
		indOff.setTrue(15);
		indOff.setTrue(14);
		stcmthg.set(wsaaAmountAreasInner.wsaaRiOthNofpol);
		stlmth.set(wsaaAmountAreasInner.wsaaRiOthNoflif);
		compute(repamt01, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiOthSi, 1000));
		compute(repamt02, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiOthAnnuity, 1000));
		compute(repamt03, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiOthAprem, 1000));
		compute(repamt04, 1).setRounded(div(wsaaAmountAreasInner.wsaaRiOthSprem, 1000));
		wsaaAmountAreasInner.wsaaRiOthSi.set(repamt01);
		wsaaAmountAreasInner.wsaaRiOthAnnuity.set(repamt02);
		wsaaAmountAreasInner.wsaaRiOthAprem.set(repamt03);
		wsaaAmountAreasInner.wsaaRiOthSprem.set(repamt04);
		rj531.printRj531d01(rj531D01, indicArea);
	}

protected void printAfReassurance9000()
	{
		start9010();
	}

protected void start9010()
	{
		/* Print Description*/
		rj531.printRj531d03(rj531D03, indicArea);
		/*    MOVE WSAA-MESSAGE(03)       TO DITDSC  OF RJ531D02-O.        */
		indOn.setTrue(13);
		indOff.setTrue(12);
		indOff.setTrue(11);
		rj531.printRj531d02(rj531D02, indicArea);
		stcmthg.set(0);
		stlmth.set(0);
		repamt01.set(0);
		repamt02.set(0);
		repamt03.set(0);
		repamt04.set(0);
		/*    MOVE WSAA-MESSAGE(04)       TO DITDSC  OF RJ531D01-O.        */
		indOn.setTrue(14);
		indOff.setTrue(15);
		indOff.setTrue(16);
		compute(stcmthg, 0).set(sub(wsaaAmountAreasInner.wsaaRegNofpol, wsaaAmountAreasInner.wsaaRiRegNofpol));
		compute(stlmth, 0).set(sub(wsaaAmountAreasInner.wsaaRegNoflif, wsaaAmountAreasInner.wsaaRiRegNoflif));
		compute(repamt01, 0).set(sub(wsaaAmountAreasInner.wsaaRegSi, wsaaAmountAreasInner.wsaaRiRegSi));
		compute(repamt02, 0).set(sub(wsaaAmountAreasInner.wsaaRegAnnuity, wsaaAmountAreasInner.wsaaRiRegAnnuity));
		compute(repamt03, 0).set(sub(wsaaAmountAreasInner.wsaaRegAprem, wsaaAmountAreasInner.wsaaRiRegAprem));
		compute(repamt04, 0).set(sub(wsaaAmountAreasInner.wsaaRegSprem, wsaaAmountAreasInner.wsaaRiRegSprem));
		rj531.printRj531d01(rj531D01, indicArea);
		/* Print Single premium*/
		stcmthg.set(0);
		stlmth.set(0);
		repamt01.set(0);
		repamt02.set(0);
		repamt03.set(0);
		repamt04.set(0);
		/*    MOVE WSAA-MESSAGE(05)       TO DITDSC  OF RJ531D01-O.        */
		indOn.setTrue(15);
		indOff.setTrue(14);
		indOff.setTrue(16);
		compute(stcmthg, 0).set(sub(wsaaAmountAreasInner.wsaaSglNofpol, wsaaAmountAreasInner.wsaaRiSglNofpol));
		compute(stlmth, 0).set(sub(wsaaAmountAreasInner.wsaaSglNoflif, wsaaAmountAreasInner.wsaaRiSglNoflif));
		compute(repamt01, 0).set(sub(wsaaAmountAreasInner.wsaaSglSi, wsaaAmountAreasInner.wsaaRiSglSi));
		compute(repamt02, 0).set(sub(wsaaAmountAreasInner.wsaaSglAnnuity, wsaaAmountAreasInner.wsaaRiSglAnnuity));
		compute(repamt03, 0).set(sub(wsaaAmountAreasInner.wsaaSglAprem, wsaaAmountAreasInner.wsaaRiSglAprem));
		compute(repamt04, 0).set(sub(wsaaAmountAreasInner.wsaaSglSprem, wsaaAmountAreasInner.wsaaRiSglSprem));
		rj531.printRj531d01(rj531D01, indicArea);
		/* Print Other premium*/
		stcmthg.set(0);
		stlmth.set(0);
		repamt01.set(0);
		repamt02.set(0);
		repamt03.set(0);
		repamt04.set(0);
		/*    MOVE WSAA-MESSAGE(06)       TO DITDSC  OF RJ531D01-O.        */
		indOn.setTrue(16);
		indOff.setTrue(15);
		indOff.setTrue(14);
		compute(stcmthg, 0).set(sub(wsaaAmountAreasInner.wsaaOthNofpol, wsaaAmountAreasInner.wsaaRiOthNofpol));
		compute(stlmth, 0).set(sub(wsaaAmountAreasInner.wsaaOthNoflif, wsaaAmountAreasInner.wsaaRiOthNoflif));
		compute(repamt01, 0).set(sub(wsaaAmountAreasInner.wsaaOthSi, wsaaAmountAreasInner.wsaaRiOthSi));
		compute(repamt02, 0).set(sub(wsaaAmountAreasInner.wsaaOthAnnuity, wsaaAmountAreasInner.wsaaRiOthAnnuity));
		compute(repamt03, 0).set(sub(wsaaAmountAreasInner.wsaaOthAprem, wsaaAmountAreasInner.wsaaRiOthAprem));
		compute(repamt04, 0).set(sub(wsaaAmountAreasInner.wsaaOthSprem, wsaaAmountAreasInner.wsaaRiOthSprem));
		rj531.printRj531d01(rj531D01, indicArea);
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void a000InitialWs()
	{
		/*A010-START*/
		wsaaAmountAreasInner.wsaaRegNofpol.set(0);
		wsaaAmountAreasInner.wsaaRegNoflif.set(0);
		wsaaAmountAreasInner.wsaaRegSi.set(0);
		wsaaAmountAreasInner.wsaaRegAnnuity.set(0);
		wsaaAmountAreasInner.wsaaRegAprem.set(0);
		wsaaAmountAreasInner.wsaaRegSprem.set(0);
		wsaaAmountAreasInner.wsaaRiRegNofpol.set(0);
		wsaaAmountAreasInner.wsaaRiRegNoflif.set(0);
		wsaaAmountAreasInner.wsaaRiRegSi.set(0);
		wsaaAmountAreasInner.wsaaRiRegAnnuity.set(0);
		wsaaAmountAreasInner.wsaaRiRegAprem.set(0);
		wsaaAmountAreasInner.wsaaRiRegSprem.set(0);
		wsaaAmountAreasInner.wsaaOthNofpol.set(0);
		wsaaAmountAreasInner.wsaaOthNoflif.set(0);
		wsaaAmountAreasInner.wsaaOthSi.set(0);
		wsaaAmountAreasInner.wsaaOthAnnuity.set(0);
		wsaaAmountAreasInner.wsaaOthAprem.set(0);
		wsaaAmountAreasInner.wsaaOthSprem.set(0);
		wsaaAmountAreasInner.wsaaRiOthNofpol.set(0);
		wsaaAmountAreasInner.wsaaRiOthNoflif.set(0);
		wsaaAmountAreasInner.wsaaRiOthSi.set(0);
		wsaaAmountAreasInner.wsaaRiOthAnnuity.set(0);
		wsaaAmountAreasInner.wsaaRiOthAprem.set(0);
		wsaaAmountAreasInner.wsaaRiOthSprem.set(0);
		wsaaAmountAreasInner.wsaaSglNofpol.set(0);
		wsaaAmountAreasInner.wsaaSglNoflif.set(0);
		wsaaAmountAreasInner.wsaaSglSi.set(0);
		wsaaAmountAreasInner.wsaaSglAnnuity.set(0);
		wsaaAmountAreasInner.wsaaSglAprem.set(0);
		wsaaAmountAreasInner.wsaaSglSprem.set(0);
		wsaaAmountAreasInner.wsaaRiSglNofpol.set(0);
		wsaaAmountAreasInner.wsaaRiSglNoflif.set(0);
		wsaaAmountAreasInner.wsaaRiSglSi.set(0);
		wsaaAmountAreasInner.wsaaRiSglAnnuity.set(0);
		wsaaAmountAreasInner.wsaaRiSglAprem.set(0);
		wsaaAmountAreasInner.wsaaRiSglSprem.set(0);
		wsaaStsmthc.set(ZERO);
		wsaaStimthc.set(ZERO);
		wsaaStamthc.set(ZERO);
		wsaaStcmthc.set(ZERO);
		wsaaStlmthc.set(ZERO);
		wsaaStraamtc.set(ZERO);
		wsaaAnnlBasic.set(ZERO);
		wsaaAnnlLoaded.set(ZERO);
		wsaaStbmthc.set(ZERO);
		/*A090-EXIT*/
	}

protected void a200ReadT3629()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					a210();
				case a220CallItemio:
					a220CallItemio();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a210()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemitem(sqlGovepfInner.sqlCntcurr);
		itemIO.setItemtabl(t3629);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
	}

protected void a220CallItemio()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaScrate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaScrate,ZERO)
		|| isGT(wsaaX,7))) {
			if (isGTE(datcon1rec.intDate,t3629rec.frmdate[wsaaX.toInt()])
			&& isLTE(datcon1rec.intDate,t3629rec.todate[wsaaX.toInt()])) {
				wsaaScrate.set(t3629rec.scrate[wsaaX.toInt()]);
			}
			else {
				wsaaX.add(1);
			}
		}

		if (isEQ(wsaaScrate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.a220CallItemio);
			}
			else {
				syserrrec.statuz.set(g418);
				fatalError600();
			}
		}
	}
/*
 * Class transformed  from Data Structure WSAA-SQL-KEEP--INNER
 */
private static final class WsaaSqlKeepInner {

		/* Keep SQL data*/
	private FixedLengthStringData wsaaSqlKeep = new FixedLengthStringData(1148);
	private FixedLengthStringData wsaaSChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 0);
	private FixedLengthStringData wsaaSStatcat = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 1);
	private PackedDecimalData wsaaSAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaSqlKeep, 3);
	private FixedLengthStringData wsaaSStfund = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 6);
	private FixedLengthStringData wsaaSStsect = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 7);
	private FixedLengthStringData wsaaSStssect = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 9);
	private FixedLengthStringData wsaaSRegister = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 13);
	private FixedLengthStringData wsaaSCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 16);
	private FixedLengthStringData wsaaSCrpstat = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 18);
	private FixedLengthStringData wsaaSCnpstat = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 20);
	private FixedLengthStringData wsaaSCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 22);
	private FixedLengthStringData wsaaSCrtable = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 25);
	private FixedLengthStringData wsaaSAcctccy = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 29);
	private FixedLengthStringData wsaaSCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 32);
	private FixedLengthStringData wsaaSParind = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 35);
	private FixedLengthStringData wsaaSBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 36);
	private PackedDecimalData wsaaBfwdc = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 38);
	private FixedLengthStringData wsaaStcmths = new FixedLengthStringData(60).isAPartOf(wsaaSqlKeep, 48);
	private PackedDecimalData[] wsaaStcmth = PDArrayPartOfStructure(12, 9, 0, wsaaStcmths, 0);
	private PackedDecimalData wsaaBfwdl = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 108);
	private FixedLengthStringData wsaaStlmths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 118);
	private PackedDecimalData[] wsaaStlmth = PDArrayPartOfStructure(12, 18, 2, wsaaStlmths, 0);
	private PackedDecimalData wsaaBfwdi = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 238);
	private FixedLengthStringData wsaaStimths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 248);
	private PackedDecimalData[] wsaaStimth = PDArrayPartOfStructure(12, 18, 2, wsaaStimths, 0);
	private PackedDecimalData wsaaBfwda = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 368);
	private FixedLengthStringData wsaaStamths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 378);
	private PackedDecimalData[] wsaaStamth = PDArrayPartOfStructure(12, 18, 2, wsaaStamths, 0);
	private PackedDecimalData wsaaBfwdb = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 498);
	private FixedLengthStringData wsaaStbmthgs = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 508);
	private PackedDecimalData[] wsaaStbmthg = PDArrayPartOfStructure(12, 18, 2, wsaaStbmthgs, 0);
	private PackedDecimalData wsaaBfwds = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 628);
	private FixedLengthStringData wsaaStsmths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 638);
	private PackedDecimalData[] wsaaStsmth = PDArrayPartOfStructure(12, 18, 2, wsaaStsmths, 0);
	private PackedDecimalData wsaaBfwdp = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 758);
	private FixedLengthStringData wsaaStpmths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 768);
	private PackedDecimalData[] wsaaStpmth = PDArrayPartOfStructure(12, 18, 2, wsaaStpmths, 0);
	private PackedDecimalData wsaaBfwdra = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 888);
	private FixedLengthStringData wsaaStraamts = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 898);
	private PackedDecimalData[] wsaaStraamt = PDArrayPartOfStructure(12, 18, 2, wsaaStraamts, 0);
	private PackedDecimalData wsaaBfwdld = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 1018);
	private FixedLengthStringData wsaaStldmths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 1028);
	private PackedDecimalData[] wsaaStldmth = PDArrayPartOfStructure(12, 18, 2, wsaaStldmths, 0);
}
/*
 * Class transformed  from Data Structure WSAA-AMOUNT-AREAS--INNER
 */
private static final class WsaaAmountAreasInner {

		/* WSAA-AMOUNT-AREAS */
	private FixedLengthStringData wsaaOtherPremium = new FixedLengthStringData(120);
	private PackedDecimalData wsaaOthNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 0).init(0);
	private PackedDecimalData wsaaOthNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 10).init(0);
	private PackedDecimalData wsaaOthSi = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 20).init(0);
	private PackedDecimalData wsaaOthAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 30).init(0);
	private PackedDecimalData wsaaOthAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 40).init(0);
	private PackedDecimalData wsaaOthSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 50).init(0);
	private PackedDecimalData wsaaRiOthNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 60).init(0);
	private PackedDecimalData wsaaRiOthNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 70).init(0);
	private PackedDecimalData wsaaRiOthSi = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 80).init(0);
	private PackedDecimalData wsaaRiOthAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 90).init(0);
	private PackedDecimalData wsaaRiOthAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 100).init(0);
	private PackedDecimalData wsaaRiOthSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaOtherPremium, 110).init(0);

	private FixedLengthStringData wsaaRegularPremium = new FixedLengthStringData(120);
	private PackedDecimalData wsaaRegNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 0).init(0);
	private PackedDecimalData wsaaRegNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 10).init(0);
	private PackedDecimalData wsaaRegSi = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 20).init(0);
	private PackedDecimalData wsaaRegAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 30).init(0);
	private PackedDecimalData wsaaRegAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 40).init(0);
	private PackedDecimalData wsaaRegSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 50).init(0);
	private PackedDecimalData wsaaRiRegNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 60).init(0);
	private PackedDecimalData wsaaRiRegNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 70).init(0);
	private PackedDecimalData wsaaRiRegSi = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 80).init(0);
	private PackedDecimalData wsaaRiRegAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 90).init(0);
	private PackedDecimalData wsaaRiRegAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 100).init(0);
	private PackedDecimalData wsaaRiRegSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 110).init(0);

	private FixedLengthStringData wsaaSinglePremium = new FixedLengthStringData(120);
	private PackedDecimalData wsaaSglNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 0).init(0);
	private PackedDecimalData wsaaSglNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 10).init(0);
	private PackedDecimalData wsaaSglSi = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 20).init(0);
	private PackedDecimalData wsaaSglAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 30).init(0);
	private PackedDecimalData wsaaSglAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 40).init(0);
	private PackedDecimalData wsaaSglSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 50).init(0);
	private PackedDecimalData wsaaRiSglNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 60).init(0);
	private PackedDecimalData wsaaRiSglNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 70).init(0);
	private PackedDecimalData wsaaRiSglSi = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 80).init(0);
	private PackedDecimalData wsaaRiSglAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 90).init(0);
	private PackedDecimalData wsaaRiSglAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 100).init(0);
	private PackedDecimalData wsaaRiSglSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 110).init(0);
}
/*
 * Class transformed  from Data Structure SQL-GOVEPF--INNER
 */
private static final class SqlGovepfInner {

		/* SQL-GOVEPF */
	private FixedLengthStringData sqlGoverec = new FixedLengthStringData(1148);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 0);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 1);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGoverec, 3);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 6);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 7);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 9);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 13);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 16);
	private FixedLengthStringData sqlCrpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 18);
	private FixedLengthStringData sqlCnpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 20);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 22);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 25);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 29);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 32);
	private FixedLengthStringData sqlParind = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 35);
	private FixedLengthStringData sqlBillfreq = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 36);
	private PackedDecimalData sqlBfwdc = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 38);
	private PackedDecimalData sqlStcmth01 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 48);
	private PackedDecimalData sqlStcmth02 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 53);
	private PackedDecimalData sqlStcmth03 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 58);
	private PackedDecimalData sqlStcmth04 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 63);
	private PackedDecimalData sqlStcmth05 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 68);
	private PackedDecimalData sqlStcmth06 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 73);
	private PackedDecimalData sqlStcmth07 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 78);
	private PackedDecimalData sqlStcmth08 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 83);
	private PackedDecimalData sqlStcmth09 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 88);
	private PackedDecimalData sqlStcmth10 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 93);
	private PackedDecimalData sqlStcmth11 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 98);
	private PackedDecimalData sqlStcmth12 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 103);
	private PackedDecimalData sqlBfwdl = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 108);
	private PackedDecimalData sqlStlmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 118);
	private PackedDecimalData sqlStlmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 128);
	private PackedDecimalData sqlStlmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 138);
	private PackedDecimalData sqlStlmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 148);
	private PackedDecimalData sqlStlmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 158);
	private PackedDecimalData sqlStlmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 168);
	private PackedDecimalData sqlStlmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 178);
	private PackedDecimalData sqlStlmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 188);
	private PackedDecimalData sqlStlmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 198);
	private PackedDecimalData sqlStlmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 208);
	private PackedDecimalData sqlStlmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 218);
	private PackedDecimalData sqlStlmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 228);
	private PackedDecimalData sqlBfwdi = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 238);
	private PackedDecimalData sqlStimth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 248);
	private PackedDecimalData sqlStimth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 258);
	private PackedDecimalData sqlStimth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 268);
	private PackedDecimalData sqlStimth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 278);
	private PackedDecimalData sqlStimth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 288);
	private PackedDecimalData sqlStimth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 298);
	private PackedDecimalData sqlStimth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 308);
	private PackedDecimalData sqlStimth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 318);
	private PackedDecimalData sqlStimth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 328);
	private PackedDecimalData sqlStimth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 338);
	private PackedDecimalData sqlStimth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 348);
	private PackedDecimalData sqlStimth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 358);
	private PackedDecimalData sqlBfwda = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 368);
	private PackedDecimalData sqlStamth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 378);
	private PackedDecimalData sqlStamth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 388);
	private PackedDecimalData sqlStamth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 398);
	private PackedDecimalData sqlStamth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 408);
	private PackedDecimalData sqlStamth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 418);
	private PackedDecimalData sqlStamth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 428);
	private PackedDecimalData sqlStamth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 438);
	private PackedDecimalData sqlStamth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 448);
	private PackedDecimalData sqlStamth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 458);
	private PackedDecimalData sqlStamth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 468);
	private PackedDecimalData sqlStamth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 478);
	private PackedDecimalData sqlStamth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 488);
	private PackedDecimalData sqlBfwdb = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 498);
	private PackedDecimalData sqlStbmthg01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 508);
	private PackedDecimalData sqlStbmthg02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 518);
	private PackedDecimalData sqlStbmthg03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 528);
	private PackedDecimalData sqlStbmthg04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 538);
	private PackedDecimalData sqlStbmthg05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 548);
	private PackedDecimalData sqlStbmthg06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 558);
	private PackedDecimalData sqlStbmthg07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 568);
	private PackedDecimalData sqlStbmthg08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 578);
	private PackedDecimalData sqlStbmthg09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 588);
	private PackedDecimalData sqlStbmthg10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 598);
	private PackedDecimalData sqlStbmthg11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 608);
	private PackedDecimalData sqlStbmthg12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 618);
	private PackedDecimalData sqlBfwds = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 628);
	private PackedDecimalData sqlStsmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 638);
	private PackedDecimalData sqlStsmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 648);
	private PackedDecimalData sqlStsmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 658);
	private PackedDecimalData sqlStsmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 668);
	private PackedDecimalData sqlStsmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 678);
	private PackedDecimalData sqlStsmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 688);
	private PackedDecimalData sqlStsmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 698);
	private PackedDecimalData sqlStsmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 708);
	private PackedDecimalData sqlStsmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 718);
	private PackedDecimalData sqlStsmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 728);
	private PackedDecimalData sqlStsmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 738);
	private PackedDecimalData sqlStsmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 748);
	private PackedDecimalData sqlBfwdp = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 758);
	private PackedDecimalData sqlStpmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 768);
	private PackedDecimalData sqlStpmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 778);
	private PackedDecimalData sqlStpmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 788);
	private PackedDecimalData sqlStpmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 798);
	private PackedDecimalData sqlStpmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 808);
	private PackedDecimalData sqlStpmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 818);
	private PackedDecimalData sqlStpmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 828);
	private PackedDecimalData sqlStpmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 838);
	private PackedDecimalData sqlStpmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 848);
	private PackedDecimalData sqlStpmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 858);
	private PackedDecimalData sqlStpmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 868);
	private PackedDecimalData sqlStpmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 878);
	private PackedDecimalData sqlBfwdra = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 888);
	private PackedDecimalData sqlStraamt01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 898);
	private PackedDecimalData sqlStraamt02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 908);
	private PackedDecimalData sqlStraamt03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 918);
	private PackedDecimalData sqlStraamt04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 928);
	private PackedDecimalData sqlStraamt05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 938);
	private PackedDecimalData sqlStraamt06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 948);
	private PackedDecimalData sqlStraamt07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 958);
	private PackedDecimalData sqlStraamt08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 968);
	private PackedDecimalData sqlStraamt09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 978);
	private PackedDecimalData sqlStraamt10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 988);
	private PackedDecimalData sqlStraamt11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 998);
	private PackedDecimalData sqlStraamt12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1008);
	private PackedDecimalData sqlBfwdld = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1018);
	private PackedDecimalData sqlStldmthg01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1028);
	private PackedDecimalData sqlStldmthg02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1038);
	private PackedDecimalData sqlStldmthg03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1048);
	private PackedDecimalData sqlStldmthg04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1058);
	private PackedDecimalData sqlStldmthg05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1068);
	private PackedDecimalData sqlStldmthg06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1078);
	private PackedDecimalData sqlStldmthg07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1088);
	private PackedDecimalData sqlStldmthg08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1098);
	private PackedDecimalData sqlStldmthg09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1108);
	private PackedDecimalData sqlStldmthg10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1118);
	private PackedDecimalData sqlStldmthg11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1128);
	private PackedDecimalData sqlStldmthg12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1138);
}
/*
 * Class transformed  from Data Structure RJ531-H01--INNER
 */
private static final class Rj531H01Inner {

	private FixedLengthStringData rj531H01 = new FixedLengthStringData(179);
	private FixedLengthStringData rj531h01O = new FixedLengthStringData(179).isAPartOf(rj531H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj531h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj531h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj531h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj531h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj531h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj531h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj531h01O, 50);
	private FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(rj531h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj531h01O, 83);
	private FixedLengthStringData stsect = new FixedLengthStringData(2).isAPartOf(rj531h01O, 113);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30).isAPartOf(rj531h01O, 115);
	private FixedLengthStringData stssect = new FixedLengthStringData(4).isAPartOf(rj531h01O, 145);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rj531h01O, 149);
}
}
