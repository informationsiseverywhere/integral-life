package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:21
 * Description:
 * Copybook name: GVACSTSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Gvacstskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData gvacstsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData gvacstsKey = new FixedLengthStringData(64).isAPartOf(gvacstsFileKey, 0, REDEFINE);
  	public PackedDecimalData gvacstsAcctyr = new PackedDecimalData(4, 0).isAPartOf(gvacstsKey, 0);
  	public FixedLengthStringData gvacstsChdrcoy = new FixedLengthStringData(1).isAPartOf(gvacstsKey, 3);
  	public FixedLengthStringData gvacstsCntbranch = new FixedLengthStringData(2).isAPartOf(gvacstsKey, 4);
  	public FixedLengthStringData gvacstsAcctccy = new FixedLengthStringData(3).isAPartOf(gvacstsKey, 6);
  	public FixedLengthStringData gvacstsRegister = new FixedLengthStringData(3).isAPartOf(gvacstsKey, 9);
  	public FixedLengthStringData gvacstsStatSect = new FixedLengthStringData(2).isAPartOf(gvacstsKey, 12);
  	public FixedLengthStringData gvacstsStatSubsect = new FixedLengthStringData(4).isAPartOf(gvacstsKey, 14);
  	public FixedLengthStringData gvacstsCnttype = new FixedLengthStringData(3).isAPartOf(gvacstsKey, 18);
  	public FixedLengthStringData gvacstsCrtable = new FixedLengthStringData(4).isAPartOf(gvacstsKey, 21);
  	public FixedLengthStringData gvacstsCnPremStat = new FixedLengthStringData(2).isAPartOf(gvacstsKey, 25);
  	public FixedLengthStringData gvacstsCovPremStat = new FixedLengthStringData(2).isAPartOf(gvacstsKey, 27);
  	public FixedLengthStringData filler = new FixedLengthStringData(35).isAPartOf(gvacstsKey, 29, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(gvacstsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		gvacstsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}