/*
 * File: P6706.java
 * Date: 30 August 2009 0:54:15
 * Author: Quipoz Limited
 * 
 * Class transformed from P6706.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.procedures.Acmdesc;
import com.csc.fsu.general.recordstructures.Acmdescrec;
import com.csc.life.statistics.dataaccess.AgjnTableDAM;
import com.csc.life.statistics.dataaccess.AgstTableDAM;
import com.csc.life.statistics.screens.S6706ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*          CREATE AGENT STATISTICAL JOURNAL - SCREEN 2
*
*
* This program retrieves AGST accumulation key passed from P6703
* as well as retrieving AGJN record. Journal values are accepted
* from this screen. If the transaction is committed, AGST file
* would be updated and the final balances shown under ADJUSTED
* BALANCE column.
*
* This program allows the journalling of any accounting periods. It
* starts reading of the AGST file for the first record of the same
* accumulation key except for accounting year.
*
* For previous year records of the same accumulation key, the last
* carried forward amount is retrieved and to be updated as the
* brought forward amount for journalled record. Adding the journal
* values under the ADJUSTMENT column to the brought forward values
* would give the carried forward values of the journalled record.
*
* For all forward records (ie. accounting years that are after the
* journalled record), a roll forward to add the adjustments to the
* brought forwards and carried forwards is  performed until the
* accumulation key changed, except for the accounting year.
*
*
* One journal record AGJN is created for every transaction commit-
* ted capturing the transaction date and time.
*
* VALIDATION:
*
*   - Ensure journal values are input (not zeroes).
*
*   - It is possible to enter journal records for any accounting
*     periods.
*
*
* FUNCTION KEYS:
*               <EXIT>  Return to sub-menu.
*
*               <CALC>  Calculation is performed with the adjustments.
*                       Display the adjusted accounted balances should
*                       the entered journal values be posted.
*
*               <KILL>  Return user to the select/previous screen
*                       to enter another accumulation key.
*
*               <ENTER> The first time this key is entered, a
*                       re-calculation of the journal values against
*                       AGST file for the final account balances is
*                       performed and displayed and the ADJUSTMENT
*                       column is protected.
*
*                       When in the protected mode, to commit the
*                       display information into the databases, the
*                       <ENTER> key is to be pressed for the second
*                       time.
*
*                       Should user decide to abandon the changes,
*                       <KILL> or <EXIT> should be pressed and the
*                       information displayed in S6706 would not be
*                       committed.
*
* FILES USED:
*               AGSTSKM - Agent Statistical Accumulation Master File
*               AGJNSKM - Agent Statistical Journal File
*               DESCSKM - To retrieve currency's description
*
/
*****************************************************************
* </pre>
*/
public class P6706 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6706");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaGetBfFlag = "";
	private String wsaaNewRecFlag = "";
	private ZonedDecimalData wsaaCounter = new ZonedDecimalData(1, 0).setUnsigned();

	private FixedLengthStringData wsaaScrAgntsel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaAgntsel = new FixedLengthStringData(8).isAPartOf(wsaaScrAgntsel, 0);
	private FixedLengthStringData wsaaFiller = new FixedLengthStringData(2).isAPartOf(wsaaScrAgntsel, 8);
	private ZonedDecimalData wsaaFullDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaEffdate = new FixedLengthStringData(8).isAPartOf(wsaaFullDate, 0, REDEFINE);
	private ZonedDecimalData wsaaCentury = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffdate, 0).setUnsigned();
	private FixedLengthStringData wsaaYymmdd = new FixedLengthStringData(6).isAPartOf(wsaaEffdate, 2);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(2, 0).isAPartOf(wsaaYymmdd, 0).setUnsigned();
	private ZonedDecimalData wsaaMmdd = new ZonedDecimalData(4, 0).isAPartOf(wsaaYymmdd, 2).setUnsigned();
	private PackedDecimalData wsaaBfwdc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdv = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwds = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdm = new PackedDecimalData(18, 2);
	private String t3629 = "T3629";
		/* FORMATS */
	private String agstrec = "AGSTREC";
	private String agjnrec = "AGJNREC";
	private String descrec = "DESCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Acmdescrec acmdescrec = new Acmdescrec();
		/*Agent Statistical Accum. Journal*/
	private AgjnTableDAM agjnIO = new AgjnTableDAM();
		/*Agent Statistical Accumulation File*/
	private AgstTableDAM agstIO = new AgstTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private S6706ScreenVars sv = ScreenProgram.getScreenVars( S6706ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2090, 
		exit3090, 
		exit3190
	}

	public P6706() {
		super();
		screenVars = sv;
		new ScreenModel("S6706", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialScreen1010();
		retrvAgjnFields1020();
		retrvAgstFields1030();
		setUpCurrencyDesc1040();
		setUpMonthDesc1050();
	}

protected void initialScreen1010()
	{
		wsaaCounter.set(ZERO);
		initialiseScreen1100();
	}

protected void retrvAgjnFields1020()
	{
		agjnIO.setFunction(varcom.retrv);
		agjnIO.setFormat(agjnrec);
		SmartFileCode.execute(appVars, agjnIO);
		if (isNE(agjnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agjnIO.getParams());
			fatalError600();
		}
		agjnIO.setFunction(varcom.rlse);
		agjnIO.setFormat(agjnrec);
		SmartFileCode.execute(appVars, agjnIO);
		if (isNE(agjnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agjnIO.getParams());
			fatalError600();
		}
		sv.acyr.set(agjnIO.getAcctyr());
		sv.acmn.set(agjnIO.getAcctmonth());
	}

protected void retrvAgstFields1030()
	{
		agstIO.setFunction(varcom.retrv);
		agstIO.setFormat(agstrec);
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agstIO.getParams());
			fatalError600();
		}
		agstIO.setFunction(varcom.rlse);
		agstIO.setFormat(agstrec);
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agstIO.getParams());
			fatalError600();
		}
		sv.agntsel.set(agstIO.getAgntnum());
		sv.statcat.set(agstIO.getStatcat());
		sv.acctyr.set(agstIO.getAcctyr());
		sv.acyr.set(agstIO.getAcctyr());
		sv.aracde.set(agstIO.getAracde());
		sv.cntbranch.set(agstIO.getCntbranch());
		sv.bandage.set(agstIO.getBandage());
		sv.bandsa.set(agstIO.getBandsa());
		sv.bandprm.set(agstIO.getBandprm());
		sv.bandtrm.set(agstIO.getBandtrm());
		sv.chdrtype.set(agstIO.getCnttype());
		sv.crtable.set(agstIO.getCrtable());
		sv.ovrdcat.set(agstIO.getOvrdcat());
		sv.pstatcd.set(agstIO.getPstatcode());
		sv.cntcurr.set(agstIO.getCntcurr());
		sv.stcmth.set(agstIO.getStcmth(agjnIO.getAcctmonth()));
		sv.stvmth.set(agstIO.getStvmth(agjnIO.getAcctmonth()));
		sv.stpmth.set(agstIO.getStpmth(agjnIO.getAcctmonth()));
		sv.stsmth.set(agstIO.getStsmth(agjnIO.getAcctmonth()));
		sv.stmmth.set(agstIO.getStmmth(agjnIO.getAcctmonth()));
	}

protected void setUpCurrencyDesc1040()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(agstIO.getChdrcoy());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(agstIO.getCntcurr());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.descrip.set(descIO.getLongdesc());
	}

protected void setUpMonthDesc1050()
	{
		setUpMonthDesc1200();
		/*EXIT*/
	}

protected void initialiseScreen1100()
	{
		initialise1110();
	}

protected void initialise1110()
	{
		sv.dataArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		sv.acctyr.set(ZERO);
		sv.acmn.set(ZERO);
		sv.acyr.set(ZERO);
		sv.stcmth.set(ZERO);
		sv.stcmthAdj.set(ZERO);
		sv.stcmtho.set(ZERO);
		sv.stmmth.set(ZERO);
		sv.stmmthAdj.set(ZERO);
		sv.stmmtho.set(ZERO);
		sv.stpmth.set(ZERO);
		sv.stpmthAdj.set(ZERO);
		sv.stpmtho.set(ZERO);
		sv.stsmth.set(ZERO);
		sv.stsmthAdj.set(ZERO);
		sv.stsmtho.set(ZERO);
		sv.stvmth.set(ZERO);
		sv.stvmthAdj.set(ZERO);
		sv.stvmtho.set(ZERO);
	}

protected void setUpMonthDesc1200()
	{
		/*PARA*/
		acmdescrec.company.set(agstIO.getChdrcoy());
		acmdescrec.language.set(wsspcomn.language);
		acmdescrec.branch.set(agstIO.getCntbranch());
		acmdescrec.function.set("GETD");
		callProgram(Acmdesc.class, acmdescrec.acmdescRec);
		if (isNE(acmdescrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(acmdescrec.statuz);
			fatalError600();
		}
		sv.mthldesc.set(acmdescrec.lngdesc[agjnIO.getAcctmonth().toInt()]);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			checkForCf092040();
			reconfirmPosting2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		sv.stcmthadjOut[varcom.pr.toInt()].set("N");
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE-AMOUNTS*/
		if (isEQ(sv.stcmthAdj,ZERO)
		&& isEQ(sv.stvmthAdj,ZERO)
		&& isEQ(sv.stpmthAdj,ZERO)
		&& isEQ(sv.stsmthAdj,ZERO)
		&& isEQ(sv.stmmthAdj,ZERO)) {
			/*bug #ILIFE-1070 start*/
			sv.stcmthadjErr.set("E207");
			/*bug #ILIFE-1070 end*/
			wsspcomn.edterror.set("Y");
			wsaaCounter.set(ZERO);
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForCf092040()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			calculateJournals2100();
			wsaaCounter.set(1);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-CF11*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
	}

protected void reconfirmPosting2060()
	{
		calculateJournals2100();
		wsaaCounter.add(1);
		sv.stcmthadjOut[varcom.pr.toInt()].set("Y");
		wsspcomn.edterror.set("Y");
		if (isEQ(wsaaCounter,2)) {
			wsspcomn.edterror.set(varcom.oK);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void calculateJournals2100()
	{
		/*PARA*/
		compute(sv.stcmtho, 0).set(add(sv.stcmth,sv.stcmthAdj));
		compute(sv.stvmtho, 0).set(add(sv.stvmth,sv.stvmthAdj));
		compute(sv.stpmtho, 2).set(add(sv.stpmth,sv.stpmthAdj));
		compute(sv.stsmtho, 2).set(add(sv.stsmth,sv.stsmthAdj));
		compute(sv.stmmtho, 2).set(add(sv.stmmth,sv.stmmthAdj));
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			checkFunctionKey3010();
			updateAgjnFile3020();
			updateAgstFile3030();
			checkCfBfAmounts3040();
		}
		catch (GOTOException e){
		}
	}

protected void checkFunctionKey3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updateAgjnFile3020()
	{
		agjnIO.setTransactionDate(varcom.vrcmDate);
		agjnIO.setTransactionTime(varcom.vrcmTime);
		agjnIO.setUser(varcom.vrcmUser);
		wsaaYymmdd.set(varcom.vrcmDate);
		if (isGT(wsaaYear,50)) {
			wsaaCentury.set(19);
		}
		else {
			wsaaCentury.set(20);
		}
		agjnIO.setEffdate(wsaaFullDate);
		agjnIO.setStcmth(sv.stcmthAdj);
		agjnIO.setStvmth(sv.stvmthAdj);
		agjnIO.setStpmth(sv.stpmthAdj);
		agjnIO.setStsmth(sv.stsmthAdj);
		agjnIO.setStmmth(sv.stmmthAdj);
		agjnIO.setFormat(agjnrec);
		agjnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agjnIO);
		if (isNE(agjnIO.getStatuz(),varcom.oK)
		&& isNE(agjnIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agjnIO.getParams());
			syserrrec.statuz.set(agjnIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agjnIO.getStatuz(),varcom.mrnf)) {
			agjnIO.setFunction(varcom.writr);
			SmartFileCode.execute(appVars, agjnIO);
			if (isNE(agjnIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(agjnIO.getParams());
				syserrrec.statuz.set(agjnIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void updateAgstFile3030()
	{
		agstIO.setStcmth(agjnIO.getAcctmonth(), sv.stcmtho);
		agstIO.setStvmth(agjnIO.getAcctmonth(), sv.stvmtho);
		agstIO.setStpmth(agjnIO.getAcctmonth(), sv.stpmtho);
		agstIO.setStsmth(agjnIO.getAcctmonth(), sv.stsmtho);
		agstIO.setStmmth(agjnIO.getAcctmonth(), sv.stmmtho);
		setPrecision(agstIO.getCfwdc(), 2);
		agstIO.setCfwdc(add(agstIO.getCfwdc(),sv.stcmthAdj));
		setPrecision(agstIO.getCfwdv(), 2);
		agstIO.setCfwdv(add(agstIO.getCfwdv(),sv.stvmthAdj));
		setPrecision(agstIO.getCfwdp(), 2);
		agstIO.setCfwdp(add(agstIO.getCfwdp(),sv.stpmthAdj));
		setPrecision(agstIO.getCfwds(), 2);
		agstIO.setCfwds(add(agstIO.getCfwds(),sv.stsmthAdj));
		setPrecision(agstIO.getCfwdm(), 2);
		agstIO.setCfwdm(add(agstIO.getCfwdm(),sv.stmmthAdj));
		wsaaNewRecFlag = "Y";
		agstIO.setFunction(varcom.writr);
		agstIO.setFormat(agstrec);
		SmartFileCode.execute(appVars, agstIO);
		if (isEQ(agstIO.getStatuz(),varcom.dupr)) {
			wsaaNewRecFlag = "N";
			agstIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, agstIO);
		}
		else {
			if (isNE(agstIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(agstIO.getParams());
				syserrrec.statuz.set(agstIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void checkCfBfAmounts3040()
	{
		wsaaBfwdc.set(ZERO);
		wsaaBfwdv.set(ZERO);
		wsaaBfwdp.set(ZERO);
		wsaaBfwds.set(ZERO);
		wsaaBfwdm.set(ZERO);
		agstIO.setAcctyr(ZERO);
		agstIO.setFunction(varcom.begnh);
		wsaaGetBfFlag = "N";
		while ( !(isEQ(wsaaGetBfFlag,"Y"))) {
			checkBalForwardValue3100();
		}
		
	}

protected void checkBalForwardValue3100()
	{
		try {
			para3110();
			readNextRecord3120();
		}
		catch (GOTOException e){
		}
	}

protected void para3110()
	{
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)
		&& isNE(agstIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agstIO.getStatuz());
			syserrrec.params.set(agstIO.getParams());
			fatalError600();
		}
		if (isEQ(agstIO.getStatuz(),varcom.endp)) {
			wsaaGetBfFlag = "Y";
			goTo(GotoLabel.exit3190);
		}
		wsaaScrAgntsel.set(sv.agntsel);
		if (isEQ(sv.company,agstIO.getChdrcoy())
		&& isEQ(wsaaAgntsel,agstIO.getAgntnum())
		&& isEQ(sv.statcat,agstIO.getStatcat())
		&& isEQ(sv.aracde,agstIO.getAracde())
		&& isEQ(sv.cntbranch,agstIO.getCntbranch())
		&& isEQ(sv.bandage,agstIO.getBandage())
		&& isEQ(sv.bandsa,agstIO.getBandsa())
		&& isEQ(sv.bandprm,agstIO.getBandprm())
		&& isEQ(sv.bandtrm,agstIO.getBandtrm())
		&& isEQ(sv.chdrtype,agstIO.getCnttype())
		&& isEQ(sv.crtable,agstIO.getCrtable())
		&& isEQ(sv.ovrdcat,agstIO.getOvrdcat())
		&& isEQ(sv.pstatcd,agstIO.getPstatcode())
		&& isEQ(sv.cntcurr,agstIO.getCntcurr())) {
			if (isLT(sv.acctyr,agstIO.getAcctyr())) {
				rollForwardUpdate3200();
			}
			else {
				if (isEQ(sv.acctyr,agstIO.getAcctyr())) {
					updateBfAmount3300();
				}
				else {
					wsaaBfwdc.set(agstIO.getCfwdc());
					wsaaBfwdv.set(agstIO.getCfwdv());
					wsaaBfwdp.set(agstIO.getCfwdp());
					wsaaBfwds.set(agstIO.getCfwds());
					wsaaBfwdm.set(agstIO.getCfwdm());
				}
			}
		}
		else {
			wsaaGetBfFlag = "Y";
			goTo(GotoLabel.exit3190);
		}
	}

protected void readNextRecord3120()
	{
		agstIO.setFunction(varcom.nextr);
	}

protected void rollForwardUpdate3200()
	{
		para3210();
	}

protected void para3210()
	{
		agstIO.setFormat(agstrec);
		agstIO.setFunction(varcom.rewrt);
		setPrecision(agstIO.getBfwdc(), 2);
		agstIO.setBfwdc(add(agstIO.getBfwdc(),sv.stcmthAdj));
		setPrecision(agstIO.getCfwdc(), 2);
		agstIO.setCfwdc(add(agstIO.getCfwdc(),sv.stcmthAdj));
		setPrecision(agstIO.getBfwdv(), 2);
		agstIO.setBfwdv(add(agstIO.getBfwdv(),sv.stvmthAdj));
		setPrecision(agstIO.getCfwdv(), 2);
		agstIO.setCfwdv(add(agstIO.getCfwdv(),sv.stvmthAdj));
		setPrecision(agstIO.getBfwdp(), 2);
		agstIO.setBfwdp(add(agstIO.getBfwdp(),sv.stpmthAdj));
		setPrecision(agstIO.getCfwdp(), 2);
		agstIO.setCfwdp(add(agstIO.getCfwdp(),sv.stpmthAdj));
		setPrecision(agstIO.getBfwds(), 2);
		agstIO.setBfwds(add(agstIO.getBfwds(),sv.stsmthAdj));
		setPrecision(agstIO.getCfwds(), 2);
		agstIO.setCfwds(add(agstIO.getCfwds(),sv.stsmthAdj));
		setPrecision(agstIO.getBfwdm(), 2);
		agstIO.setBfwdm(add(agstIO.getBfwdm(),sv.stmmthAdj));
		setPrecision(agstIO.getCfwdm(), 2);
		agstIO.setCfwdm(add(agstIO.getCfwdm(),sv.stmmthAdj));
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agstIO.getParams());
			fatalError600();
		}
	}

protected void updateBfAmount3300()
	{
		para3310();
	}

protected void para3310()
	{
		agstIO.setBfwdc(wsaaBfwdc);
		agstIO.setBfwdv(wsaaBfwdv);
		agstIO.setBfwdp(wsaaBfwdp);
		agstIO.setBfwds(wsaaBfwds);
		agstIO.setBfwdm(wsaaBfwdm);
		if (isEQ(wsaaNewRecFlag,"Y")) {
			setPrecision(agstIO.getCfwdc(), 2);
			agstIO.setCfwdc(add(agstIO.getCfwdc(),wsaaBfwdc));
			setPrecision(agstIO.getCfwdv(), 2);
			agstIO.setCfwdv(add(agstIO.getCfwdv(),wsaaBfwdv));
			setPrecision(agstIO.getCfwdp(), 2);
			agstIO.setCfwdp(add(agstIO.getCfwdp(),wsaaBfwdp));
			setPrecision(agstIO.getCfwds(), 2);
			agstIO.setCfwds(add(agstIO.getCfwds(),wsaaBfwds));
			setPrecision(agstIO.getCfwdm(), 2);
			agstIO.setCfwdm(add(agstIO.getCfwdm(),wsaaBfwdm));
		}
		agstIO.setFormat(agstrec);
		agstIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agstIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.secActn[1].set(SPACES);
		}
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
