package com.csc.life.statistics.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RJ529.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rj529Report extends SMARTReportLayout { 

	private FixedLengthStringData acctccy = new FixedLengthStringData(3);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private ZonedDecimalData dliaoa01 = new ZonedDecimalData(13, 0);
	private ZonedDecimalData dliaoa02 = new ZonedDecimalData(13, 0);
	private ZonedDecimalData dliaop01 = new ZonedDecimalData(13, 0);
	private ZonedDecimalData dliaop02 = new ZonedDecimalData(13, 0);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData register = new FixedLengthStringData(3);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData stcmth01 = new ZonedDecimalData(9, 0);
	private ZonedDecimalData stcmth02 = new ZonedDecimalData(9, 0);
	private FixedLengthStringData stsect = new FixedLengthStringData(2);
	private FixedLengthStringData stssect = new FixedLengthStringData(4);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rj529Report() {
		super();
	}


	/**
	 * Print the XML for Rj529d01
	 */
	public void printRj529d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stcmth01.setFieldName("stcmth01");
		stcmth01.setInternal(subString(recordData, 1, 9));
		stcmth02.setFieldName("stcmth02");
		stcmth02.setInternal(subString(recordData, 10, 9));
		dliaop01.setFieldName("dliaop01");
		dliaop01.setInternal(subString(recordData, 19, 13));
		dliaop02.setFieldName("dliaop02");
		dliaop02.setInternal(subString(recordData, 32, 13));
		dliaoa01.setFieldName("dliaoa01");
		dliaoa01.setInternal(subString(recordData, 45, 13));
		dliaoa02.setFieldName("dliaoa02");
		dliaoa02.setInternal(subString(recordData, 58, 13));
		printLayout("Rj529d01",			// Record name
			new BaseData[]{			// Fields:
				stcmth01,
				stcmth02,
				dliaop01,
				dliaop02,
				dliaoa01,
				dliaoa02
			}
		);

	}

	/**
	 * Print the XML for Rj529h01
	 */
	public void printRj529h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		acctmonth.setFieldName("acctmonth");
		acctmonth.setInternal(subString(recordData, 1, 2));
		acctyear.setFieldName("acctyear");
		acctyear.setInternal(subString(recordData, 3, 4));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 7, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 8, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 38, 10));
		acctccy.setFieldName("acctccy");
		acctccy.setInternal(subString(recordData, 48, 3));
		currdesc.setFieldName("currdesc");
		currdesc.setInternal(subString(recordData, 51, 30));
		time.setFieldName("time");
		time.set(getTime());
		register.setFieldName("register");
		register.setInternal(subString(recordData, 81, 3));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 84, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		stsect.setFieldName("stsect");
		stsect.setInternal(subString(recordData, 114, 2));
		itmdesc.setFieldName("itmdesc");
		itmdesc.setInternal(subString(recordData, 116, 30));
		stssect.setFieldName("stssect");
		stssect.setInternal(subString(recordData, 146, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 150, 30));
		printLayout("Rj529h01",			// Record name
			new BaseData[]{			// Fields:
				acctmonth,
				acctyear,
				company,
				companynm,
				sdate,
				acctccy,
				currdesc,
				time,
				register,
				descrip,
				pagnbr,
				stsect,
				itmdesc,
				stssect,
				longdesc
			}
		);

		currentPrintLine.set(13);
	}


}
