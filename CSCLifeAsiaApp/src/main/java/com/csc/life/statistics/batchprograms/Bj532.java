/*
 * File: Bj532.java
 * Date: 29 August 2009 21:44:50
 * Author: Quipoz Limited
 *
 * Class transformed from BJ532.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.statistics.dataaccess.GoveTableDAM;
import com.csc.life.statistics.dataaccess.GvacstsTableDAM;
import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj532Report;
import com.csc.life.statistics.tablestructures.Tj675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*         Claims Benefit Payable (Linked/Health Insurance)
*         ================================================
*
*   This report will list the following claim for the specific
*   period.
*  -Claims benefits payable on Insurance Products segregating
*   them into regular premium contracts, single premium contracts
*   (includes immediate and deferred annuities), fully paid      s
*   up contracts and paid up contracts
*  -Claims benefits payable on Rider Benefits.
*  -Loaded premiums on claimed policies.
*  -Reassurance ceded on claimed policies.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - Get SQL file from GOVE to accouting year pass from       AR
*
*    Read
*     - read SQL record
*
*    Perform    Until End of File
*
*      Edit
*       - set WSSP-EDTERROR to ENDP when EOF.
*
*      Update
*       - if new page, write headings
*       - if there is a change in company, accounting cuurency
*         or source of business or statutory
*         read GVACSTS key by SQL company, currency, source of
*         business and statutory. accumulated Regular Premium
*         contracts, Single Premium contracts Fully paid up and
*         Reduced Paid up by contract type.
*       - if there is a change in contract type write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*     02  -  Total records read
*     03  -  Total no of contract
*     04  -  Total not of lives
*     05  -  Total basic premium
*     06  -  Total death benefit pay
*     07  -  Total maturity benefit pay
*     08  -  Total other benefit pay
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the GVACSTS-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*                                                                     *
***********************************************************************
* </pre>
*/
public class Bj532 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgovepfAllrs = null;
	private java.sql.PreparedStatement sqlgovepfAllps = null;
	private java.sql.Connection sqlgovepfAllconn = null;
	private String sqlgovepfAll = "";
	private java.sql.ResultSet sqlgovepfrs = null;
	private java.sql.PreparedStatement sqlgovepfps = null;
	private java.sql.Connection sqlgovepfconn = null;
	private String sqlgovepf = "";
	private Rj532Report printerFile = new Rj532Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ532");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaProgram = new FixedLengthStringData(5).isAPartOf(wsaaItem, 1);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String wsaa00 = "00";
	private ZonedDecimalData wsaaScrate = new ZonedDecimalData(12, 7);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData mth = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaSelectAll = new FixedLengthStringData(1);
	private Validator selectAll = new Validator(wsaaSelectAll, "Y");
	private FixedLengthStringData wsaaTj675Stsect01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect02 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect03 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect04 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect05 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect06 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect07 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect08 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect09 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect10 = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaCount2 = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaStcamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStlamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStbamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStsamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStiamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStraamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRiStbamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBlRiStbamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaccpdb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaccmtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaccob = new PackedDecimalData(18, 2);
	private ZonedDecimalData wsaaRatio = new ZonedDecimalData(12, 9).setUnsigned();
		/* WSAA-STATCAT */
	private String wsaaStatcatRp = "RP";
	private String wsaaStatcatMa = "MA";
	private String wsaaStatcatDh = "DH";
	private String wsaaStatcatRd = "RD";

	private FixedLengthStringData wsaaBlNofpols = new FixedLengthStringData(60);
	private FixedLengthStringData[] wsaaStoreNofpol = FLSArrayPartOfStructure(12, 5, wsaaBlNofpols, 0);
	private PackedDecimalData[] wsaaBlNofpol = PDArrayPartOfArrayStructure(9, 0, wsaaStoreNofpol, 0);

	private FixedLengthStringData wsaaBlNoflifs = new FixedLengthStringData(60);
	private FixedLengthStringData[] wsaaStoreNoflif = FLSArrayPartOfStructure(12, 5, wsaaBlNoflifs, 0);
	private PackedDecimalData[] wsaaBlNoflif = PDArrayPartOfArrayStructure(9, 0, wsaaStoreNoflif, 0);

	private FixedLengthStringData wsaaBlStaccpdbs = new FixedLengthStringData(120);
	private FixedLengthStringData[] wsaaStoreStaccpdb = FLSArrayPartOfStructure(12, 10, wsaaBlStaccpdbs, 0);
	private PackedDecimalData[] wsaaBlStaccpdb = PDArrayPartOfArrayStructure(18, 2, wsaaStoreStaccpdb, 0);

	private FixedLengthStringData wsaaBlStaccmtbs = new FixedLengthStringData(120);
	private FixedLengthStringData[] wsaaStoreStaccmtb = FLSArrayPartOfStructure(12, 10, wsaaBlStaccmtbs, 0);
	private PackedDecimalData[] wsaaBlStaccmtb = PDArrayPartOfArrayStructure(18, 2, wsaaStoreStaccmtb, 0);

	private FixedLengthStringData wsaaBlStaccobs = new FixedLengthStringData(120);
	private FixedLengthStringData[] wsaaStoreStaccob = FLSArrayPartOfStructure(12, 10, wsaaBlStaccobs, 0);
	private PackedDecimalData[] wsaaBlStaccob = PDArrayPartOfArrayStructure(18, 2, wsaaStoreStaccob, 0);

	private FixedLengthStringData wsaaBlBprems = new FixedLengthStringData(120);
	private FixedLengthStringData[] wsaaStoreBprem = FLSArrayPartOfStructure(12, 10, wsaaBlBprems, 0);
	private PackedDecimalData[] wsaaBlBprem = PDArrayPartOfArrayStructure(18, 2, wsaaStoreBprem, 0);

	private FixedLengthStringData wsaaDbps = new FixedLengthStringData(60);
	private FixedLengthStringData[] wsaaStoreDbp = FLSArrayPartOfStructure(6, 10, wsaaDbps, 0);
	private PackedDecimalData[] wsaaDbp = PDArrayPartOfArrayStructure(18, 2, wsaaStoreDbp, 0);

	private FixedLengthStringData wsaaMtbs = new FixedLengthStringData(60);
	private FixedLengthStringData[] wsaaStoreMtb = FLSArrayPartOfStructure(6, 10, wsaaMtbs, 0);
	private PackedDecimalData[] wsaaMtb = PDArrayPartOfArrayStructure(18, 2, wsaaStoreMtb, 0);

	private FixedLengthStringData wsaaObs = new FixedLengthStringData(60);
	private FixedLengthStringData[] wsaaStoreOb = FLSArrayPartOfStructure(6, 10, wsaaObs, 0);
	private PackedDecimalData[] wsaaOb = PDArrayPartOfArrayStructure(18, 2, wsaaStoreOb, 0);
	private String wsaaFp = "FP";
	private String wsaaPu = "PU";
	private String wsaaP = "P";
	private String wsaaY = "Y";
	private FixedLengthStringData wsaaInd1 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaInd2 = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaStaccripInd = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaFpuNofpol = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaFpuNoflif = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaFpuBprem = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRpuNofpol = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaRpuNoflif = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaRpuBprem = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRegNofpol = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaRegNoflif = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaRegBprem = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaSglNofpol = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaSglNoflif = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaSglBprem = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPrdTotNofpol = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaPrdTotNoflif = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaPrdTotBprem = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRiNofpol = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaRiNoflif = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaRiBprem = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRitbprem = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPrdTarNofpol = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaPrdTarNoflif = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaPrdTarBprem = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaFpuDbp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaFpuMtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaFpuOb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRpuDbp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRpuMtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRpuOb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRegDbp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRegMtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRegOb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaSglDbp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaSglMtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaSglOb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPrdTotDbp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPrdTotMtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPrdTotOb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRiDbp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRiMtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRiOb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRitdbp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRitmtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRitob = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPrdTarDbp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPrdTarMtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPrdTarOb = new PackedDecimalData(18, 2);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStssect = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaParind = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaRegister = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3);
	private String wsaaFrecord = "";
	private String wsaaFlag = "";
	private FixedLengthStringData wsaaGvacstsCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaGvacstsCnttype = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaGvacstsCrpstat = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaGvacstsCnpstat = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-GOVEPF */
	private FixedLengthStringData sqlGoverec = new FixedLengthStringData(758);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 0);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 1);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGoverec, 3);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 6);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 7);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 9);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 12);
	private FixedLengthStringData sqlCrpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 14);
	private FixedLengthStringData sqlCnpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 16);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 18);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 21);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 25);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 28);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 31);
	private FixedLengthStringData sqlBillfreq = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 35);
	private FixedLengthStringData sqlParind = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 37);
	private PackedDecimalData sqlStcmth01 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 38);
	private PackedDecimalData sqlStcmth02 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 43);
	private PackedDecimalData sqlStcmth03 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 48);
	private PackedDecimalData sqlStcmth04 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 53);
	private PackedDecimalData sqlStcmth05 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 58);
	private PackedDecimalData sqlStcmth06 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 63);
	private PackedDecimalData sqlStcmth07 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 68);
	private PackedDecimalData sqlStcmth08 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 73);
	private PackedDecimalData sqlStcmth09 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 78);
	private PackedDecimalData sqlStcmth00 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 83);
	private PackedDecimalData sqlStcmth11 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 88);
	private PackedDecimalData sqlStcmth12 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 93);
	private PackedDecimalData sqlStlmth01 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 98);
	private PackedDecimalData sqlStlmth02 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 103);
	private PackedDecimalData sqlStlmth03 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 108);
	private PackedDecimalData sqlStlmth04 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 113);
	private PackedDecimalData sqlStlmth05 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 118);
	private PackedDecimalData sqlStlmth06 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 123);
	private PackedDecimalData sqlStlmth07 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 128);
	private PackedDecimalData sqlStlmth08 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 133);
	private PackedDecimalData sqlStlmth09 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 138);
	private PackedDecimalData sqlStlmth10 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 143);
	private PackedDecimalData sqlStlmth11 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 148);
	private PackedDecimalData sqlStlmth12 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 153);
	private PackedDecimalData sqlStbmthg01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 158);
	private PackedDecimalData sqlStbmthg02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 168);
	private PackedDecimalData sqlStbmthg03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 178);
	private PackedDecimalData sqlStbmthg04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 188);
	private PackedDecimalData sqlStbmthg05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 198);
	private PackedDecimalData sqlStbmthg06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 208);
	private PackedDecimalData sqlStbmthg07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 218);
	private PackedDecimalData sqlStbmthg08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 228);
	private PackedDecimalData sqlStbmthg09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 238);
	private PackedDecimalData sqlStbmthg10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 248);
	private PackedDecimalData sqlStbmthg11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 258);
	private PackedDecimalData sqlStbmthg12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 268);
	private PackedDecimalData sqlStamth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 278);
	private PackedDecimalData sqlStamth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 288);
	private PackedDecimalData sqlStamth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 298);
	private PackedDecimalData sqlStamth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 308);
	private PackedDecimalData sqlStamth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 318);
	private PackedDecimalData sqlStamth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 328);
	private PackedDecimalData sqlStamth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 338);
	private PackedDecimalData sqlStamth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 348);
	private PackedDecimalData sqlStamth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 358);
	private PackedDecimalData sqlStamth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 368);
	private PackedDecimalData sqlStamth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 378);
	private PackedDecimalData sqlStamth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 388);
	private PackedDecimalData sqlStraamt01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 398);
	private PackedDecimalData sqlStraamt02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 408);
	private PackedDecimalData sqlStraamt03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 418);
	private PackedDecimalData sqlStraamt04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 428);
	private PackedDecimalData sqlStraamt05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 438);
	private PackedDecimalData sqlStraamt06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 448);
	private PackedDecimalData sqlStraamt07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 458);
	private PackedDecimalData sqlStraamt08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 468);
	private PackedDecimalData sqlStraamt09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 478);
	private PackedDecimalData sqlStraamt10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 488);
	private PackedDecimalData sqlStraamt11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 498);
	private PackedDecimalData sqlStraamt12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 508);
	private PackedDecimalData sqlStimth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 518);
	private PackedDecimalData sqlStimth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 528);
	private PackedDecimalData sqlStimth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 538);
	private PackedDecimalData sqlStimth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 548);
	private PackedDecimalData sqlStimth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 558);
	private PackedDecimalData sqlStimth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 568);
	private PackedDecimalData sqlStimth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 578);
	private PackedDecimalData sqlStimth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 588);
	private PackedDecimalData sqlStimth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 598);
	private PackedDecimalData sqlStimth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 608);
	private PackedDecimalData sqlStimth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 618);
	private PackedDecimalData sqlStimth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 628);
	private PackedDecimalData sqlStsmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 638);
	private PackedDecimalData sqlStsmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 648);
	private PackedDecimalData sqlStsmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 658);
	private PackedDecimalData sqlStsmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 668);
	private PackedDecimalData sqlStsmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 678);
	private PackedDecimalData sqlStsmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 688);
	private PackedDecimalData sqlStsmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 698);
	private PackedDecimalData sqlStsmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 708);
	private PackedDecimalData sqlStsmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 718);
	private PackedDecimalData sqlStsmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 728);
	private PackedDecimalData sqlStsmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 738);
	private PackedDecimalData sqlStsmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 748);

	private FixedLengthStringData tmpGoverec = new FixedLengthStringData(758);
	private FixedLengthStringData tmpChdrcoy = new FixedLengthStringData(1).isAPartOf(tmpGoverec, 0);
	private FixedLengthStringData tmpStatcat = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 1);
	private PackedDecimalData tmpAcctyr = new PackedDecimalData(4, 0).isAPartOf(tmpGoverec, 3);
	private FixedLengthStringData tmpStfund = new FixedLengthStringData(1).isAPartOf(tmpGoverec, 6);
	private FixedLengthStringData tmpStsect = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 7);
	private FixedLengthStringData tmpRegister = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 9);
	private FixedLengthStringData tmpCntbranch = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 12);
	private FixedLengthStringData tmpCrpstat = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 14);
	private FixedLengthStringData tmpCnpstat = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 16);
	private FixedLengthStringData tmpCnttype = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 18);
	private FixedLengthStringData tmpCrtable = new FixedLengthStringData(4).isAPartOf(tmpGoverec, 21);
	private FixedLengthStringData tmpAcctccy = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 25);
	private FixedLengthStringData tmpCntcurr = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 28);
	private FixedLengthStringData tmpStssect = new FixedLengthStringData(4).isAPartOf(tmpGoverec, 31);
	private FixedLengthStringData tmpBillfreq = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 35);
	private FixedLengthStringData tmpParind = new FixedLengthStringData(1).isAPartOf(tmpGoverec, 37);
	private FixedLengthStringData tmpStcmths = new FixedLengthStringData(60).isAPartOf(tmpGoverec, 38);
	private PackedDecimalData[] tmpStcmth = PDArrayPartOfStructure(12, 9, 0, tmpStcmths, 0);
	private FixedLengthStringData tmpStlmths = new FixedLengthStringData(60).isAPartOf(tmpGoverec, 98);
	private PackedDecimalData[] tmpStlmth = PDArrayPartOfStructure(12, 9, 0, tmpStlmths, 0);
	private FixedLengthStringData tmpStbmthgs = new FixedLengthStringData(120).isAPartOf(tmpGoverec, 158);
	private PackedDecimalData[] tmpStbmthg = PDArrayPartOfStructure(12, 18, 2, tmpStbmthgs, 0);
	private FixedLengthStringData tmpStamths = new FixedLengthStringData(120).isAPartOf(tmpGoverec, 278);
	private PackedDecimalData[] tmpStamth = PDArrayPartOfStructure(12, 18, 2, tmpStamths, 0);
	private FixedLengthStringData tmpStraamts = new FixedLengthStringData(120).isAPartOf(tmpGoverec, 398);
	private PackedDecimalData[] tmpStraamt = PDArrayPartOfStructure(12, 18, 2, tmpStraamts, 0);
	private FixedLengthStringData tmpStimths = new FixedLengthStringData(120).isAPartOf(tmpGoverec, 518);
	private PackedDecimalData[] tmpStimth = PDArrayPartOfStructure(12, 18, 2, tmpStimths, 0);
	private FixedLengthStringData tmpStsmths = new FixedLengthStringData(120).isAPartOf(tmpGoverec, 638);
	private PackedDecimalData[] tmpStsmth = PDArrayPartOfStructure(12, 18, 2, tmpStsmths, 0);
		/* ERRORS */
	private String g418 = "G418";
	private String esql = "ESQL";
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String gvacstsrec = "GVACSTSREC";
		/* TABLES */
	private String t1693 = "T1693";
	private String t3629 = "T3629";
	private String t3589 = "T3589";
	private String t5688 = "T5688";
	private String t5685 = "T5685";
	private String t5684 = "T5684";
	private String t6697 = "T6697";
	private String t6625 = "T6625";
	private String tj675 = "TJ675";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private int ct08 = 8;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rj532H01 = new FixedLengthStringData(179);
	private FixedLengthStringData rj532h01O = new FixedLengthStringData(179).isAPartOf(rj532H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj532h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj532h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj532h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj532h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj532h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj532h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj532h01O, 50);
	private FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(rj532h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj532h01O, 83);
	private FixedLengthStringData stsect = new FixedLengthStringData(2).isAPartOf(rj532h01O, 113);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30).isAPartOf(rj532h01O, 115);
	private FixedLengthStringData stssect = new FixedLengthStringData(4).isAPartOf(rj532h01O, 145);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rj532h01O, 149);

	private FixedLengthStringData rj532H03 = new FixedLengthStringData(30);
	private FixedLengthStringData rj532h03O = new FixedLengthStringData(30).isAPartOf(rj532H03, 0);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(rj532h03O, 0);

	private FixedLengthStringData rj532D01 = new FixedLengthStringData(70);
	private FixedLengthStringData rj532d01O = new FixedLengthStringData(70).isAPartOf(rj532D01, 0);
	private ZonedDecimalData stcmthg = new ZonedDecimalData(9, 0).isAPartOf(rj532d01O, 0);
	private ZonedDecimalData stlmth = new ZonedDecimalData(9, 0).isAPartOf(rj532d01O, 9);
	private ZonedDecimalData dancar01 = new ZonedDecimalData(13, 0).isAPartOf(rj532d01O, 18);
	private ZonedDecimalData dancar02 = new ZonedDecimalData(13, 0).isAPartOf(rj532d01O, 31);
	private ZonedDecimalData dancar03 = new ZonedDecimalData(13, 0).isAPartOf(rj532d01O, 44);
	private ZonedDecimalData dancar04 = new ZonedDecimalData(13, 0).isAPartOf(rj532d01O, 57);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Additional Govr Statistics Accumulation*/
	private GoveTableDAM goveIO = new GoveTableDAM();
		/*Statistic Accumulation for policy admin*/
	private GvacstsTableDAM gvacstsIO = new GvacstsTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Pj517par pj517par = new Pj517par();
	private T3629rec t3629rec = new T3629rec();
	private Tj675rec tj675rec = new Tj675rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		h320CallItemio,
		h359Exit,
		h520Read,
		h590Exit
	}

	public Bj532() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaAcctyr.set(pj517par.acctyr);
		acctyear.set(pj517par.acctyr);
		acctmonth.set(pj517par.acctmnth);
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaFpuNofpol.set(ZERO);
		wsaaFpuNoflif.set(ZERO);
		wsaaFpuBprem.set(ZERO);
		wsaaRpuNofpol.set(ZERO);
		wsaaRpuNoflif.set(ZERO);
		wsaaRpuBprem.set(ZERO);
		wsaaRegNofpol.set(ZERO);
		wsaaRegNoflif.set(ZERO);
		wsaaRegBprem.set(ZERO);
		wsaaSglNofpol.set(ZERO);
		wsaaSglNoflif.set(ZERO);
		wsaaSglBprem.set(ZERO);
		wsaaRiNofpol.set(ZERO);
		wsaaRiNoflif.set(ZERO);
		wsaaRiBprem.set(ZERO);
		wsaaFpuDbp.set(ZERO);
		wsaaFpuMtb.set(ZERO);
		wsaaFpuOb.set(ZERO);
		wsaaRpuDbp.set(ZERO);
		wsaaRpuMtb.set(ZERO);
		wsaaRpuOb.set(ZERO);
		wsaaRegDbp.set(ZERO);
		wsaaRegMtb.set(ZERO);
		wsaaRegOb.set(ZERO);
		wsaaSglDbp.set(ZERO);
		wsaaSglMtb.set(ZERO);
		wsaaSglOb.set(ZERO);
		wsaaRiDbp.set(ZERO);
		wsaaRiMtb.set(ZERO);
		wsaaRiOb.set(ZERO);
		wsaaPrdTotNofpol.set(ZERO);
		wsaaPrdTotNoflif.set(ZERO);
		wsaaPrdTotBprem.set(ZERO);
		wsaaPrdTarNofpol.set(ZERO);
		wsaaPrdTarNoflif.set(ZERO);
		wsaaPrdTarBprem.set(ZERO);
		wsaaPrdTotDbp.set(ZERO);
		wsaaPrdTotMtb.set(ZERO);
		wsaaPrdTotOb.set(ZERO);
		wsaaPrdTarDbp.set(ZERO);
		wsaaPrdTarMtb.set(ZERO);
		wsaaPrdTarOb.set(ZERO);
		wsaaStcamt.set(ZERO);
		wsaaStlamt.set(ZERO);
		wsaaStiamt.set(ZERO);
		wsaaStbamt.set(ZERO);
		wsaaStsamt.set(ZERO);
		wsaaStraamt.set(ZERO);
		wsaaStaccpdb.set(ZERO);
		wsaaStaccmtb.set(ZERO);
		wsaaStaccob.set(ZERO);
		wsaaRiStbamt.set(ZERO);
		wsaaBlRiStbamt.set(ZERO);
		wsaaRatio.set(ZERO);
		wsaaStsect.set(SPACES);
		wsaaStssect.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaCnttype.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaRegister.set(SPACES);
		wsaaCount.set(1);
		while ( !(isGT(wsaaCount,12))) {
			wsaaBlNofpol[wsaaCount.toInt()].set(ZERO);
			wsaaBlNoflif[wsaaCount.toInt()].set(ZERO);
			wsaaBlBprem[wsaaCount.toInt()].set(ZERO);
			wsaaBlStaccmtb[wsaaCount.toInt()].set(ZERO);
			wsaaBlStaccob[wsaaCount.toInt()].set(ZERO);
			wsaaBlStaccpdb[wsaaCount.toInt()].set(ZERO);
			wsaaCount.add(1);
		}

		wsaaFrecord = "Y";
		initialize(wsaaItem);
		wsaaProgram.set(wsaaProg);
		wsaaLanguage.set(bsscIO.getLanguage());
		readTj6751300();
	}

protected void readTj6751300()
	{
		para1310();
	}

protected void para1310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tj675);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		wsaaSelectAll.set("Y");
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			selectAll1400();
		}
		else {
			wsaaSelectAll.set("N");
			tj675rec.tj675Rec.set(itemIO.getGenarea());
			selectSpecified1500();
		}
	}

protected void selectAll1400()
	{
		/*PARA*/
		sqlgovepfAll = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, STSSECT, BILLFREQ, PARIND, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, STLMTH01, STLMTH02, STLMTH03, STLMTH04, STLMTH05, STLMTH06, STLMTH07, STLMTH08, STLMTH09, STLMTH10, STLMTH11, STLMTH12, STBMTHG01, STBMTHG02, STBMTHG03, STBMTHG04, STBMTHG05, STBMTHG06, STBMTHG07, STBMTHG08, STBMTHG09, STBMTHG10, STBMTHG11, STBMTHG12, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12, STRAAMT01, STRAAMT02, STRAAMT03, STRAAMT04, STRAAMT05, STRAAMT06, STRAAMT07, STRAAMT08, STRAAMT09, STRAAMT10, STRAAMT11, STRAAMT12, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, STSMTH01, STSMTH02, STSMTH03, STSMTH04, STSMTH05, STSMTH06, STSMTH07, STSMTH08, STSMTH09, STSMTH10, STSMTH11, STSMTH12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE ACCTYR = ?" +
" AND STATCAT IN (?, ?, ?, ?)" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, PARIND"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfAllconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfAllps = appVars.prepareStatementEmbeded(sqlgovepfAllconn, sqlgovepfAll, "GOVEPF");
			appVars.setDBDouble(sqlgovepfAllps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgovepfAllps, 2, wsaaStatcatRp);
			appVars.setDBString(sqlgovepfAllps, 3, wsaaStatcatMa);
			appVars.setDBString(sqlgovepfAllps, 4, wsaaStatcatDh);
			appVars.setDBString(sqlgovepfAllps, 5, wsaaStatcatRd);
			sqlgovepfAllrs = appVars.executeQuery(sqlgovepfAllps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void selectSpecified1500()
	{
		para1510();
	}

protected void para1510()
	{
		wsaaTj675Stsect01.set(tj675rec.statSect[1]);
		wsaaTj675Stsect02.set(tj675rec.statSect[2]);
		wsaaTj675Stsect03.set(tj675rec.statSect[3]);
		wsaaTj675Stsect04.set(tj675rec.statSect[4]);
		wsaaTj675Stsect05.set(tj675rec.statSect[5]);
		wsaaTj675Stsect06.set(tj675rec.statSect[6]);
		wsaaTj675Stsect07.set(tj675rec.statSect[7]);
		wsaaTj675Stsect08.set(tj675rec.statSect[8]);
		wsaaTj675Stsect09.set(tj675rec.statSect[9]);
		wsaaTj675Stsect10.set(tj675rec.statSect[10]);
		sqlgovepf = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, STSSECT, BILLFREQ, PARIND, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, STLMTH01, STLMTH02, STLMTH03, STLMTH04, STLMTH05, STLMTH06, STLMTH07, STLMTH08, STLMTH09, STLMTH10, STLMTH11, STLMTH12, STBMTHG01, STBMTHG02, STBMTHG03, STBMTHG04, STBMTHG05, STBMTHG06, STBMTHG07, STBMTHG08, STBMTHG09, STBMTHG10, STBMTHG11, STBMTHG12, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12, STRAAMT01, STRAAMT02, STRAAMT03, STRAAMT04, STRAAMT05, STRAAMT06, STRAAMT07, STRAAMT08, STRAAMT09, STRAAMT10, STRAAMT11, STRAAMT12, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, STSMTH01, STSMTH02, STSMTH03, STSMTH04, STSMTH05, STSMTH06, STSMTH07, STSMTH08, STSMTH09, STSMTH10, STSMTH11, STSMTH12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE ACCTYR = ?" +
" AND STATCAT IN (?, ?, ?, ?)" +
" AND (STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?)" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, PARIND"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfps = appVars.prepareStatementEmbeded(sqlgovepfconn, sqlgovepf, "GOVEPF");
			appVars.setDBDouble(sqlgovepfps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgovepfps, 2, wsaaStatcatRp);
			appVars.setDBString(sqlgovepfps, 3, wsaaStatcatMa);
			appVars.setDBString(sqlgovepfps, 4, wsaaStatcatRd);
			appVars.setDBString(sqlgovepfps, 5, wsaaStatcatDh);
			appVars.setDBString(sqlgovepfps, 6, wsaaTj675Stsect01.toString());
			appVars.setDBString(sqlgovepfps, 7, wsaaTj675Stsect02.toString());
			appVars.setDBString(sqlgovepfps, 8, wsaaTj675Stsect03.toString());
			appVars.setDBString(sqlgovepfps, 9, wsaaTj675Stsect04.toString());
			appVars.setDBString(sqlgovepfps, 10, wsaaTj675Stsect05.toString());
			appVars.setDBString(sqlgovepfps, 11, wsaaTj675Stsect06.toString());
			appVars.setDBString(sqlgovepfps, 12, wsaaTj675Stsect07.toString());
			appVars.setDBString(sqlgovepfps, 13, wsaaTj675Stsect08.toString());
			appVars.setDBString(sqlgovepfps, 14, wsaaTj675Stsect09.toString());
			appVars.setDBString(sqlgovepfps, 15, wsaaTj675Stsect10.toString());
			sqlgovepfrs = appVars.executeQuery(sqlgovepfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		if (selectAll.isTrue()) {
			sqlerrorflag = false;
			try {
				if (sqlgovepfAllrs.next()) {
					appVars.getDBObject(sqlgovepfAllrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfAllrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfAllrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfAllrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfAllrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfAllrs, 6, sqlRegister);
					appVars.getDBObject(sqlgovepfAllrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgovepfAllrs, 8, sqlCrpstat);
					appVars.getDBObject(sqlgovepfAllrs, 9, sqlCnpstat);
					appVars.getDBObject(sqlgovepfAllrs, 10, sqlCnttype);
					appVars.getDBObject(sqlgovepfAllrs, 11, sqlCrtable);
					appVars.getDBObject(sqlgovepfAllrs, 12, sqlAcctccy);
					appVars.getDBObject(sqlgovepfAllrs, 13, sqlCntcurr);
					appVars.getDBObject(sqlgovepfAllrs, 14, sqlStssect);
					appVars.getDBObject(sqlgovepfAllrs, 15, sqlBillfreq);
					appVars.getDBObject(sqlgovepfAllrs, 16, sqlParind);
					appVars.getDBObject(sqlgovepfAllrs, 17, sqlStcmth01);
					appVars.getDBObject(sqlgovepfAllrs, 18, sqlStcmth02);
					appVars.getDBObject(sqlgovepfAllrs, 19, sqlStcmth03);
					appVars.getDBObject(sqlgovepfAllrs, 20, sqlStcmth04);
					appVars.getDBObject(sqlgovepfAllrs, 21, sqlStcmth05);
					appVars.getDBObject(sqlgovepfAllrs, 22, sqlStcmth06);
					appVars.getDBObject(sqlgovepfAllrs, 23, sqlStcmth07);
					appVars.getDBObject(sqlgovepfAllrs, 24, sqlStcmth08);
					appVars.getDBObject(sqlgovepfAllrs, 25, sqlStcmth09);
					appVars.getDBObject(sqlgovepfAllrs, 26, sqlStcmth00);
					appVars.getDBObject(sqlgovepfAllrs, 27, sqlStcmth11);
					appVars.getDBObject(sqlgovepfAllrs, 28, sqlStcmth12);
					appVars.getDBObject(sqlgovepfAllrs, 29, sqlStlmth01);
					appVars.getDBObject(sqlgovepfAllrs, 30, sqlStlmth02);
					appVars.getDBObject(sqlgovepfAllrs, 31, sqlStlmth03);
					appVars.getDBObject(sqlgovepfAllrs, 32, sqlStlmth04);
					appVars.getDBObject(sqlgovepfAllrs, 33, sqlStlmth05);
					appVars.getDBObject(sqlgovepfAllrs, 34, sqlStlmth06);
					appVars.getDBObject(sqlgovepfAllrs, 35, sqlStlmth07);
					appVars.getDBObject(sqlgovepfAllrs, 36, sqlStlmth08);
					appVars.getDBObject(sqlgovepfAllrs, 37, sqlStlmth09);
					appVars.getDBObject(sqlgovepfAllrs, 38, sqlStlmth10);
					appVars.getDBObject(sqlgovepfAllrs, 39, sqlStlmth11);
					appVars.getDBObject(sqlgovepfAllrs, 40, sqlStlmth12);
					appVars.getDBObject(sqlgovepfAllrs, 41, sqlStbmthg01);
					appVars.getDBObject(sqlgovepfAllrs, 42, sqlStbmthg02);
					appVars.getDBObject(sqlgovepfAllrs, 43, sqlStbmthg03);
					appVars.getDBObject(sqlgovepfAllrs, 44, sqlStbmthg04);
					appVars.getDBObject(sqlgovepfAllrs, 45, sqlStbmthg05);
					appVars.getDBObject(sqlgovepfAllrs, 46, sqlStbmthg06);
					appVars.getDBObject(sqlgovepfAllrs, 47, sqlStbmthg07);
					appVars.getDBObject(sqlgovepfAllrs, 48, sqlStbmthg08);
					appVars.getDBObject(sqlgovepfAllrs, 49, sqlStbmthg09);
					appVars.getDBObject(sqlgovepfAllrs, 50, sqlStbmthg10);
					appVars.getDBObject(sqlgovepfAllrs, 51, sqlStbmthg11);
					appVars.getDBObject(sqlgovepfAllrs, 52, sqlStbmthg12);
					appVars.getDBObject(sqlgovepfAllrs, 53, sqlStamth01);
					appVars.getDBObject(sqlgovepfAllrs, 54, sqlStamth02);
					appVars.getDBObject(sqlgovepfAllrs, 55, sqlStamth03);
					appVars.getDBObject(sqlgovepfAllrs, 56, sqlStamth04);
					appVars.getDBObject(sqlgovepfAllrs, 57, sqlStamth05);
					appVars.getDBObject(sqlgovepfAllrs, 58, sqlStamth06);
					appVars.getDBObject(sqlgovepfAllrs, 59, sqlStamth07);
					appVars.getDBObject(sqlgovepfAllrs, 60, sqlStamth08);
					appVars.getDBObject(sqlgovepfAllrs, 61, sqlStamth09);
					appVars.getDBObject(sqlgovepfAllrs, 62, sqlStamth10);
					appVars.getDBObject(sqlgovepfAllrs, 63, sqlStamth11);
					appVars.getDBObject(sqlgovepfAllrs, 64, sqlStamth12);
					appVars.getDBObject(sqlgovepfAllrs, 65, sqlStraamt01);
					appVars.getDBObject(sqlgovepfAllrs, 66, sqlStraamt02);
					appVars.getDBObject(sqlgovepfAllrs, 67, sqlStraamt03);
					appVars.getDBObject(sqlgovepfAllrs, 68, sqlStraamt04);
					appVars.getDBObject(sqlgovepfAllrs, 69, sqlStraamt05);
					appVars.getDBObject(sqlgovepfAllrs, 70, sqlStraamt06);
					appVars.getDBObject(sqlgovepfAllrs, 71, sqlStraamt07);
					appVars.getDBObject(sqlgovepfAllrs, 72, sqlStraamt08);
					appVars.getDBObject(sqlgovepfAllrs, 73, sqlStraamt09);
					appVars.getDBObject(sqlgovepfAllrs, 74, sqlStraamt10);
					appVars.getDBObject(sqlgovepfAllrs, 75, sqlStraamt11);
					appVars.getDBObject(sqlgovepfAllrs, 76, sqlStraamt12);
					appVars.getDBObject(sqlgovepfAllrs, 77, sqlStimth01);
					appVars.getDBObject(sqlgovepfAllrs, 78, sqlStimth02);
					appVars.getDBObject(sqlgovepfAllrs, 79, sqlStimth03);
					appVars.getDBObject(sqlgovepfAllrs, 80, sqlStimth04);
					appVars.getDBObject(sqlgovepfAllrs, 81, sqlStimth05);
					appVars.getDBObject(sqlgovepfAllrs, 82, sqlStimth06);
					appVars.getDBObject(sqlgovepfAllrs, 83, sqlStimth07);
					appVars.getDBObject(sqlgovepfAllrs, 84, sqlStimth08);
					appVars.getDBObject(sqlgovepfAllrs, 85, sqlStimth09);
					appVars.getDBObject(sqlgovepfAllrs, 86, sqlStimth10);
					appVars.getDBObject(sqlgovepfAllrs, 87, sqlStimth11);
					appVars.getDBObject(sqlgovepfAllrs, 88, sqlStimth12);
					appVars.getDBObject(sqlgovepfAllrs, 89, sqlStsmth01);
					appVars.getDBObject(sqlgovepfAllrs, 90, sqlStsmth02);
					appVars.getDBObject(sqlgovepfAllrs, 91, sqlStsmth03);
					appVars.getDBObject(sqlgovepfAllrs, 92, sqlStsmth04);
					appVars.getDBObject(sqlgovepfAllrs, 93, sqlStsmth05);
					appVars.getDBObject(sqlgovepfAllrs, 94, sqlStsmth06);
					appVars.getDBObject(sqlgovepfAllrs, 95, sqlStsmth07);
					appVars.getDBObject(sqlgovepfAllrs, 96, sqlStsmth08);
					appVars.getDBObject(sqlgovepfAllrs, 97, sqlStsmth09);
					appVars.getDBObject(sqlgovepfAllrs, 98, sqlStsmth10);
					appVars.getDBObject(sqlgovepfAllrs, 99, sqlStsmth11);
					appVars.getDBObject(sqlgovepfAllrs, 100, sqlStsmth12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		else {
			sqlerrorflag = false;
			try {
				if (sqlgovepfrs.next()) {
					appVars.getDBObject(sqlgovepfrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfrs, 6, sqlRegister);
					appVars.getDBObject(sqlgovepfrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgovepfrs, 8, sqlCrpstat);
					appVars.getDBObject(sqlgovepfrs, 9, sqlCnpstat);
					appVars.getDBObject(sqlgovepfrs, 10, sqlCnttype);
					appVars.getDBObject(sqlgovepfrs, 11, sqlCrtable);
					appVars.getDBObject(sqlgovepfrs, 12, sqlAcctccy);
					appVars.getDBObject(sqlgovepfrs, 13, sqlCntcurr);
					appVars.getDBObject(sqlgovepfrs, 14, sqlStssect);
					appVars.getDBObject(sqlgovepfrs, 15, sqlBillfreq);
					appVars.getDBObject(sqlgovepfrs, 16, sqlParind);
					appVars.getDBObject(sqlgovepfrs, 17, sqlStcmth01);
					appVars.getDBObject(sqlgovepfrs, 18, sqlStcmth02);
					appVars.getDBObject(sqlgovepfrs, 19, sqlStcmth03);
					appVars.getDBObject(sqlgovepfrs, 20, sqlStcmth04);
					appVars.getDBObject(sqlgovepfrs, 21, sqlStcmth05);
					appVars.getDBObject(sqlgovepfrs, 22, sqlStcmth06);
					appVars.getDBObject(sqlgovepfrs, 23, sqlStcmth07);
					appVars.getDBObject(sqlgovepfrs, 24, sqlStcmth08);
					appVars.getDBObject(sqlgovepfrs, 25, sqlStcmth09);
					appVars.getDBObject(sqlgovepfrs, 26, sqlStcmth00);
					appVars.getDBObject(sqlgovepfrs, 27, sqlStcmth11);
					appVars.getDBObject(sqlgovepfrs, 28, sqlStcmth12);
					appVars.getDBObject(sqlgovepfrs, 29, sqlStlmth01);
					appVars.getDBObject(sqlgovepfrs, 30, sqlStlmth02);
					appVars.getDBObject(sqlgovepfrs, 31, sqlStlmth03);
					appVars.getDBObject(sqlgovepfrs, 32, sqlStlmth04);
					appVars.getDBObject(sqlgovepfrs, 33, sqlStlmth05);
					appVars.getDBObject(sqlgovepfrs, 34, sqlStlmth06);
					appVars.getDBObject(sqlgovepfrs, 35, sqlStlmth07);
					appVars.getDBObject(sqlgovepfrs, 36, sqlStlmth08);
					appVars.getDBObject(sqlgovepfrs, 37, sqlStlmth09);
					appVars.getDBObject(sqlgovepfrs, 38, sqlStlmth10);
					appVars.getDBObject(sqlgovepfrs, 39, sqlStlmth11);
					appVars.getDBObject(sqlgovepfrs, 40, sqlStlmth12);
					appVars.getDBObject(sqlgovepfrs, 41, sqlStbmthg01);
					appVars.getDBObject(sqlgovepfrs, 42, sqlStbmthg02);
					appVars.getDBObject(sqlgovepfrs, 43, sqlStbmthg03);
					appVars.getDBObject(sqlgovepfrs, 44, sqlStbmthg04);
					appVars.getDBObject(sqlgovepfrs, 45, sqlStbmthg05);
					appVars.getDBObject(sqlgovepfrs, 46, sqlStbmthg06);
					appVars.getDBObject(sqlgovepfrs, 47, sqlStbmthg07);
					appVars.getDBObject(sqlgovepfrs, 48, sqlStbmthg08);
					appVars.getDBObject(sqlgovepfrs, 49, sqlStbmthg09);
					appVars.getDBObject(sqlgovepfrs, 50, sqlStbmthg10);
					appVars.getDBObject(sqlgovepfrs, 51, sqlStbmthg11);
					appVars.getDBObject(sqlgovepfrs, 52, sqlStbmthg12);
					appVars.getDBObject(sqlgovepfrs, 53, sqlStamth01);
					appVars.getDBObject(sqlgovepfrs, 54, sqlStamth02);
					appVars.getDBObject(sqlgovepfrs, 55, sqlStamth03);
					appVars.getDBObject(sqlgovepfrs, 56, sqlStamth04);
					appVars.getDBObject(sqlgovepfrs, 57, sqlStamth05);
					appVars.getDBObject(sqlgovepfrs, 58, sqlStamth06);
					appVars.getDBObject(sqlgovepfrs, 59, sqlStamth07);
					appVars.getDBObject(sqlgovepfrs, 60, sqlStamth08);
					appVars.getDBObject(sqlgovepfrs, 61, sqlStamth09);
					appVars.getDBObject(sqlgovepfrs, 62, sqlStamth10);
					appVars.getDBObject(sqlgovepfrs, 63, sqlStamth11);
					appVars.getDBObject(sqlgovepfrs, 64, sqlStamth12);
					appVars.getDBObject(sqlgovepfrs, 65, sqlStraamt01);
					appVars.getDBObject(sqlgovepfrs, 66, sqlStraamt02);
					appVars.getDBObject(sqlgovepfrs, 67, sqlStraamt03);
					appVars.getDBObject(sqlgovepfrs, 68, sqlStraamt04);
					appVars.getDBObject(sqlgovepfrs, 69, sqlStraamt05);
					appVars.getDBObject(sqlgovepfrs, 70, sqlStraamt06);
					appVars.getDBObject(sqlgovepfrs, 71, sqlStraamt07);
					appVars.getDBObject(sqlgovepfrs, 72, sqlStraamt08);
					appVars.getDBObject(sqlgovepfrs, 73, sqlStraamt09);
					appVars.getDBObject(sqlgovepfrs, 74, sqlStraamt10);
					appVars.getDBObject(sqlgovepfrs, 75, sqlStraamt11);
					appVars.getDBObject(sqlgovepfrs, 76, sqlStraamt12);
					appVars.getDBObject(sqlgovepfrs, 77, sqlStimth01);
					appVars.getDBObject(sqlgovepfrs, 78, sqlStimth02);
					appVars.getDBObject(sqlgovepfrs, 79, sqlStimth03);
					appVars.getDBObject(sqlgovepfrs, 80, sqlStimth04);
					appVars.getDBObject(sqlgovepfrs, 81, sqlStimth05);
					appVars.getDBObject(sqlgovepfrs, 82, sqlStimth06);
					appVars.getDBObject(sqlgovepfrs, 83, sqlStimth07);
					appVars.getDBObject(sqlgovepfrs, 84, sqlStimth08);
					appVars.getDBObject(sqlgovepfrs, 85, sqlStimth09);
					appVars.getDBObject(sqlgovepfrs, 86, sqlStimth10);
					appVars.getDBObject(sqlgovepfrs, 87, sqlStimth11);
					appVars.getDBObject(sqlgovepfrs, 88, sqlStimth12);
					appVars.getDBObject(sqlgovepfrs, 89, sqlStsmth01);
					appVars.getDBObject(sqlgovepfrs, 90, sqlStsmth02);
					appVars.getDBObject(sqlgovepfrs, 91, sqlStsmth03);
					appVars.getDBObject(sqlgovepfrs, 92, sqlStsmth04);
					appVars.getDBObject(sqlgovepfrs, 93, sqlStsmth05);
					appVars.getDBObject(sqlgovepfrs, 94, sqlStsmth06);
					appVars.getDBObject(sqlgovepfrs, 95, sqlStsmth07);
					appVars.getDBObject(sqlgovepfrs, 96, sqlStsmth08);
					appVars.getDBObject(sqlgovepfrs, 97, sqlStsmth09);
					appVars.getDBObject(sqlgovepfrs, 98, sqlStsmth10);
					appVars.getDBObject(sqlgovepfrs, 99, sqlStsmth11);
					appVars.getDBObject(sqlgovepfrs, 100, sqlStsmth12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		tmpGoverec.set(sqlGoverec);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
		if (isNE(wsaaFrecord,"Y")) {
			if (newPageReq.isTrue()) {
				indOn.setTrue(1);
				h100NewPage();
			}
			h200WriteDetail();
			h400WriteSummary();
		}
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
		writeDetail3080();
	}

protected void update3010()
	{
		for (mth.set(1); !(isGT(mth,pj517par.acctmnth)); mth.add(1)){
			wsaaStcamt.add(tmpStcmth[mth.toInt()]);
			wsaaStlamt.add(tmpStlmth[mth.toInt()]);
			wsaaStiamt.add(tmpStimth[mth.toInt()]);
			wsaaStbamt.add(tmpStbmthg[mth.toInt()]);
			wsaaStsamt.add(tmpStsmth[mth.toInt()]);
			wsaaStraamt.add(tmpStraamt[mth.toInt()]);
		}
		if (isNE(sqlAcctccy,sqlCntcurr)) {
			h300CheckRate();
			compute(wsaaStbamt, 8).setRounded(mult(wsaaStbamt,wsaaScrate));
			compute(wsaaStsamt, 8).setRounded(mult(wsaaStsamt,wsaaScrate));
			compute(wsaaStiamt, 8).setRounded(mult(wsaaStiamt,wsaaScrate));
			compute(wsaaStraamt, 8).setRounded(mult(wsaaStraamt,wsaaScrate));
		}
		h350CheckBl();
	}

protected void writeDetail3080()
	{
		if (isEQ(wsaaFrecord,"Y")) {
			wsaaFrecord = "N";
			h600SetHeading();
		}
		else {
			if (isNE(wsaaRegister,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaCompany,sqlChdrcoy)
			|| isNE(wsaaStsect,sqlStsect)
			|| isNE(wsaaStssect,sqlStssect)
			|| isNE(wsaaBranch,sqlCntbranch)) {
				if (newPageReq.isTrue()) {
					indOn.setTrue(1);
					h100NewPage();
				}
				h200WriteDetail();
				h400WriteSummary();
				h600SetHeading();
			}
			else {
				if (isNE(wsaaCnttype,sqlCnttype)) {
					if (newPageReq.isTrue()) {
						indOn.setTrue(1);
						h100NewPage();
					}
					h200WriteDetail();
				}
			}
		}
		if (isNE(wsaaGvacstsCnttype,sqlCnttype)
		|| isNE(wsaaGvacstsCrtable,sqlCrtable)
		|| isNE(wsaaGvacstsCnpstat,sqlCnpstat)
		|| isNE(wsaaGvacstsCrpstat,sqlCrpstat)) {
			h500ReadGvacsts();
			h550Accumulate();
		}
		if (isEQ(sqlCnpstat,wsaaFp)) {
			wsaaFpuNofpol.add(wsaaStcamt);
			wsaaFpuNoflif.add(wsaaStlamt);
			wsaaFpuBprem.add(wsaaStbamt);
			wsaaFpuBprem.add(wsaaStsamt);
		}
		else {
			if (isEQ(sqlCnpstat,wsaaPu)) {
				wsaaRpuNofpol.add(wsaaStcamt);
				wsaaRpuNoflif.add(wsaaStlamt);
				wsaaRpuBprem.add(wsaaStbamt);
				wsaaRpuBprem.add(wsaaStsamt);
			}
			else {
				if (isNE(sqlBillfreq,wsaa00)) {
					wsaaRegNofpol.add(wsaaStcamt);
					wsaaRegNoflif.add(wsaaStlamt);
					wsaaRegBprem.add(wsaaStbamt);
				}
				else {
					wsaaSglNofpol.add(wsaaStcamt);
					wsaaSglNoflif.add(wsaaStlamt);
					wsaaSglBprem.add(wsaaStsamt);
				}
			}
		}
		h700SumBr();
		if (isNE(wsaaStraamt,ZERO)) {
			wsaaRiNofpol.add(wsaaStcamt);
			wsaaRiNoflif.add(wsaaStlamt);
			compute(wsaaRatio, 9).set(div(wsaaStraamt,wsaaStiamt));
			if (isNE(wsaaStbamt,ZERO)) {
				compute(wsaaRiStbamt, 10).setRounded(mult(wsaaRatio,wsaaStbamt));
			}
			if (isNE(wsaaStsamt,ZERO)) {
				compute(wsaaRiStbamt, 10).setRounded(mult(wsaaRatio,wsaaStsamt));
			}
			wsaaRiBprem.add(wsaaRiStbamt);
			h750SumAr();
		}
		wsaaRegister.set(sqlRegister);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaCompany.set(sqlChdrcoy);
		wsaaBranch.set(sqlCntbranch);
		wsaaStssect.set(sqlStssect);
		wsaaStsect.set(sqlStsect);
		wsaaCnttype.set(sqlCnttype);
		wsaaStcamt.set(ZERO);
		wsaaStlamt.set(ZERO);
		wsaaStsamt.set(ZERO);
		wsaaStbamt.set(ZERO);
		wsaaStiamt.set(ZERO);
		wsaaStraamt.set(ZERO);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (selectAll.isTrue()) {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfAllconn, sqlgovepfAllps, sqlgovepfAllrs);
		}
		else {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfconn, sqlgovepfps, sqlgovepfrs);
		}
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void h100NewPage()
	{
		/*H110-START*/
		printerFile.printRj532h01(rj532H01);
		wsaaOverflow.set("N");
		printerFile.printRj532h02(printerRec, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*H190-EXIT*/
	}

protected void h200WriteDetail()
	{
		h210Start();
	}

protected void h210Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(wsaaCnttype);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		indOn.setTrue(17);
		indOff.setTrue(10);
		indOff.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(13);
		indOff.setTrue(14);
		cntdesc.set(descIO.getLongdesc());
		printerFile.printRj532h03(rj532H03);
		indOn.setTrue(2);
		stcmthg.set(wsaaRegNofpol);
		stlmth.set(wsaaRegNoflif);
		compute(dancar01, 3).setRounded(div(wsaaRegDbp,1000));
		compute(dancar02, 3).setRounded(div(wsaaRegMtb,1000));
		compute(dancar03, 3).setRounded(div(wsaaRegOb,1000));
		compute(dancar04, 3).setRounded(div(wsaaRegBprem,1000));
		printerFile.printRj532d01(rj532D01);
		indOn.setTrue(3);
		stcmthg.set(wsaaSglNofpol);
		stlmth.set(wsaaSglNoflif);
		compute(dancar01, 3).setRounded(div(wsaaSglDbp,1000));
		compute(dancar02, 3).setRounded(div(wsaaSglMtb,1000));
		compute(dancar03, 3).setRounded(div(wsaaSglOb,1000));
		compute(dancar04, 3).setRounded(div(wsaaSglBprem,1000));
		printerFile.printRj532d01(rj532D01);
		indOn.setTrue(4);
		stcmthg.set(wsaaFpuNofpol);
		stlmth.set(wsaaFpuNoflif);
		compute(dancar01, 3).setRounded(div(wsaaFpuDbp,1000));
		compute(dancar02, 3).setRounded(div(wsaaFpuMtb,1000));
		compute(dancar03, 3).setRounded(div(wsaaFpuOb,1000));
		compute(dancar04, 3).setRounded(div(wsaaFpuBprem,1000));
		printerFile.printRj532d01(rj532D01);
		indOn.setTrue(5);
		stcmthg.set(wsaaRpuNofpol);
		stlmth.set(wsaaRpuNoflif);
		compute(dancar01, 3).setRounded(div(wsaaRpuDbp,1000));
		compute(dancar02, 3).setRounded(div(wsaaRpuMtb,1000));
		compute(dancar03, 3).setRounded(div(wsaaRpuOb,1000));
		compute(dancar04, 3).setRounded(div(wsaaRpuBprem,1000));
		printerFile.printRj532d01(rj532D01);
		compute(wsaaPrdTotNofpol, 0).set(add(add(add(add(wsaaPrdTotNofpol,wsaaRegNofpol),wsaaSglNofpol),wsaaFpuNofpol),wsaaRpuNofpol));
		compute(wsaaPrdTotNoflif, 0).set(add(add(add(add(wsaaPrdTotNoflif,wsaaRegNoflif),wsaaSglNoflif),wsaaFpuNoflif),wsaaRpuNoflif));
		compute(wsaaPrdTotDbp, 2).set(add(add(add(add(wsaaPrdTotDbp,wsaaRegDbp),wsaaSglDbp),wsaaFpuDbp),wsaaRpuDbp));
		compute(wsaaPrdTotMtb, 2).set(add(add(add(add(wsaaPrdTotMtb,wsaaRegMtb),wsaaSglMtb),wsaaFpuMtb),wsaaRpuMtb));
		compute(wsaaPrdTotOb, 2).set(add(add(add(add(wsaaPrdTotOb,wsaaRegOb),wsaaSglOb),wsaaFpuOb),wsaaRpuOb));
		compute(wsaaPrdTotBprem, 2).set(add(add(add(add(wsaaPrdTotBprem,wsaaRegBprem),wsaaSglBprem),wsaaFpuBprem),wsaaRpuBprem));
		wsaaFpuNofpol.set(ZERO);
		wsaaFpuNoflif.set(ZERO);
		wsaaFpuBprem.set(ZERO);
		wsaaRpuNofpol.set(ZERO);
		wsaaRpuNoflif.set(ZERO);
		wsaaRpuBprem.set(ZERO);
		wsaaRegNofpol.set(ZERO);
		wsaaRegNoflif.set(ZERO);
		wsaaRegBprem.set(ZERO);
		wsaaSglNofpol.set(ZERO);
		wsaaSglNoflif.set(ZERO);
		wsaaSglBprem.set(ZERO);
		wsaaFpuDbp.set(ZERO);
		wsaaFpuMtb.set(ZERO);
		wsaaFpuOb.set(ZERO);
		wsaaRpuDbp.set(ZERO);
		wsaaRpuMtb.set(ZERO);
		wsaaRpuOb.set(ZERO);
		wsaaRegDbp.set(ZERO);
		wsaaRegMtb.set(ZERO);
		wsaaRegOb.set(ZERO);
		wsaaSglDbp.set(ZERO);
		wsaaSglMtb.set(ZERO);
		wsaaSglOb.set(ZERO);
		wsaaRiDbp.set(ZERO);
		wsaaRiMtb.set(ZERO);
		wsaaRiOb.set(ZERO);
		wsaaStaccpdb.set(ZERO);
		wsaaStaccmtb.set(ZERO);
		wsaaStaccob.set(ZERO);
	}

protected void h300CheckRate()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					h310Start();
				}
				case h320CallItemio: {
					h320CallItemio();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void h310Start()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemitem(sqlCntcurr);
		itemIO.setItemtabl(t3629);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
	}

protected void h320CallItemio()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaScrate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaScrate,ZERO)
		|| isGT(wsaaX,7))) {
			if (isGTE(datcon1rec.intDate,t3629rec.frmdate[wsaaX.toInt()])
			&& isLTE(datcon1rec.intDate,t3629rec.todate[wsaaX.toInt()])) {
				wsaaScrate.set(t3629rec.scrate[wsaaX.toInt()]);
			}
			else {
				wsaaX.add(1);
			}
		}

		if (isEQ(wsaaScrate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.h320CallItemio);
			}
			else {
				syserrrec.statuz.set(g418);
				fatalError600();
			}
		}
	}

protected void h350CheckBl()
	{
		try {
			h351Start();
		}
		catch (GOTOException e){
		}
	}

protected void h351Start()
	{
		wsaaInd1.set(SPACES);
		wsaaInd2.set(SPACES);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6697);
		itemIO.setItemseq(SPACES);
		itemIO.setItemitem(sqlCnttype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			wsaaInd1.set(wsaaY);
			goTo(GotoLabel.h359Exit);
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6625);
		itemIO.setItemseq(SPACES);
		itemIO.setItemitem(sqlCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			wsaaInd2.set(wsaaY);
			goTo(GotoLabel.h359Exit);
		}
	}

protected void h400WriteSummary()
	{
		h410Start();
	}

protected void h410Start()
	{
		indOn.setTrue(6);
		stcmthg.set(wsaaPrdTotNofpol);
		stlmth.set(wsaaPrdTotNoflif);
		compute(dancar01, 3).setRounded(div(wsaaPrdTotDbp,1000));
		compute(dancar02, 3).setRounded(div(wsaaPrdTotMtb,1000));
		compute(dancar03, 3).setRounded(div(wsaaPrdTotOb,1000));
		compute(dancar04, 3).setRounded(div(wsaaPrdTotBprem,1000));
		printerFile.printRj532d01(rj532D01);
		indOn.setTrue(7);
		stcmthg.set(wsaaRiNofpol);
		stlmth.set(wsaaRiNoflif);
		compute(dancar01, 3).setRounded(div(wsaaRiDbp,1000));
		compute(dancar02, 3).setRounded(div(wsaaRiMtb,1000));
		compute(dancar03, 3).setRounded(div(wsaaRiOb,1000));
		compute(dancar04, 3).setRounded(div(wsaaRiBprem,1000));
		wsaaRitdbp.set(dancar01);
		wsaaRitmtb.set(dancar02);
		wsaaRitob.set(dancar03);
		wsaaRiBprem.set(dancar04);
		printerFile.printRj532d01(rj532D01);
		compute(wsaaPrdTarNofpol, 0).set(sub(wsaaPrdTotNofpol,wsaaRiNofpol));
		compute(wsaaPrdTarNoflif, 0).set(sub(wsaaPrdTotNoflif,wsaaRiNoflif));
		compute(wsaaPrdTarDbp, 2).set(sub(wsaaPrdTotDbp,wsaaRitdbp));
		compute(wsaaPrdTarMtb, 2).set(sub(wsaaPrdTotMtb,wsaaRitmtb));
		compute(wsaaPrdTarOb, 2).set(sub(wsaaPrdTotOb,wsaaRitob));
		compute(wsaaPrdTarBprem, 2).set(sub(wsaaPrdTotBprem,wsaaRitbprem));
		indOn.setTrue(8);
		stcmthg.set(wsaaPrdTarNofpol);
		stlmth.set(wsaaPrdTarNoflif);
		compute(dancar01, 3).setRounded(div(wsaaPrdTarDbp,1000));
		compute(dancar02, 3).setRounded(div(wsaaPrdTarMtb,1000));
		compute(dancar03, 3).setRounded(div(wsaaPrdTarOb,1000));
		compute(dancar04, 3).setRounded(div(wsaaPrdTarBprem,1000));
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaPrdTarNofpol);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaPrdTarNoflif);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(wsaaPrdTarBprem);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(wsaaPrdTarDbp);
		callContot001();
		contotrec.totno.set(ct07);
		contotrec.totval.set(wsaaPrdTarMtb);
		callContot001();
		contotrec.totno.set(ct08);
		contotrec.totval.set(wsaaPrdTarOb);
		callContot001();
		printerFile.printRj532d01(rj532D01);
		indOn.setTrue(9);
		h100NewPage();
		h450Caltotar();
		wsaaCount.set(ZERO);
		while ( !(isGTE(wsaaCount,12))) {
			if (isEQ(wsaaCount,6)
			|| isEQ(wsaaCount,0)) {
				if (isEQ(wsaaCount,6)) {
					indOn.setTrue(14);
				}
				else {
					indOn.setTrue(14);
				}
				printerFile.printRj532h03(rj532H03);
			}
			if (isEQ(wsaaCount,2)
			|| isEQ(wsaaCount,8)) {
				indOn.setTrue(11);
			}
			if (isEQ(wsaaCount,4)
			|| isEQ(wsaaCount,10)) {
				indOn.setTrue(12);
			}
			if (isEQ(wsaaCount,6)
			|| isEQ(wsaaCount,0)) {
				indOn.setTrue(10);
			}
			printerFile.printRj532h03(rj532H03);
			wsaaCount.add(1);
			indOn.setTrue(15);
			h900WriteBl();
			wsaaCount.add(1);
			indOn.setTrue(16);
			h900WriteBl();
		}

		wsaaOverflow.set("Y");
		wsaaRiNofpol.set(ZERO);
		wsaaRiNoflif.set(ZERO);
		wsaaRiBprem.set(ZERO);
		wsaaRiDbp.set(ZERO);
		wsaaRiMtb.set(ZERO);
		wsaaRiOb.set(ZERO);
		wsaaPrdTotNofpol.set(ZERO);
		wsaaPrdTotNoflif.set(ZERO);
		wsaaPrdTotBprem.set(ZERO);
		wsaaPrdTarNofpol.set(ZERO);
		wsaaPrdTarNoflif.set(ZERO);
		wsaaPrdTarBprem.set(ZERO);
		wsaaPrdTotDbp.set(ZERO);
		wsaaPrdTotMtb.set(ZERO);
		wsaaPrdTotOb.set(ZERO);
		wsaaPrdTarDbp.set(ZERO);
		wsaaPrdTarMtb.set(ZERO);
		wsaaPrdTarOb.set(ZERO);
		wsaaCount.set(1);
		while ( !(isGT(wsaaCount,12))) {
			wsaaBlNofpol[wsaaCount.toInt()].set(ZERO);
			wsaaBlNoflif[wsaaCount.toInt()].set(ZERO);
			wsaaBlBprem[wsaaCount.toInt()].set(ZERO);
			wsaaBlStaccmtb[wsaaCount.toInt()].set(ZERO);
			wsaaBlStaccob[wsaaCount.toInt()].set(ZERO);
			wsaaBlStaccpdb[wsaaCount.toInt()].set(ZERO);
			wsaaCount.add(1);
		}

	}

protected void h450Caltotar()
	{
		h451Start();
	}

protected void h451Start()
	{
		wsaaCount.set(7);
		wsaaCount2.set(1);
		while ( !(isGT(wsaaCount2,6))) {
			compute(wsaaBlNofpol[wsaaCount.toInt()], 0).set(sub(wsaaBlNofpol[wsaaCount2.toInt()],wsaaBlNofpol[wsaaCount.toInt()]));
			compute(wsaaBlNoflif[wsaaCount.toInt()], 0).set(sub(wsaaBlNoflif[wsaaCount2.toInt()],wsaaBlNoflif[wsaaCount.toInt()]));
			compute(wsaaBlBprem[wsaaCount.toInt()], 2).set(sub(wsaaBlBprem[wsaaCount2.toInt()],wsaaBlBprem[wsaaCount.toInt()]));
			compute(wsaaBlStaccpdb[wsaaCount.toInt()], 2).set(sub(wsaaBlStaccpdb[wsaaCount2.toInt()],wsaaBlStaccpdb[wsaaCount.toInt()]));
			compute(wsaaBlStaccmtb[wsaaCount.toInt()], 2).set(sub(wsaaBlStaccmtb[wsaaCount2.toInt()],wsaaBlStaccmtb[wsaaCount.toInt()]));
			compute(wsaaBlStaccob[wsaaCount.toInt()], 2).set(sub(wsaaBlStaccob[wsaaCount2.toInt()],wsaaBlStaccob[wsaaCount.toInt()]));
			wsaaCount.add(1);
			wsaaCount2.add(1);
		}

	}

protected void h500ReadGvacsts()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					h510Start();
				}
				case h520Read: {
					h520Read();
				}
				case h590Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void h510Start()
	{
		wsaaStaccripInd.set(SPACES);
		wsaaCount.set(1);
		while ( !(isGT(wsaaCount,6))) {
			wsaaDbp[wsaaCount.toInt()].set(ZERO);
			wsaaMtb[wsaaCount.toInt()].set(ZERO);
			wsaaOb[wsaaCount.toInt()].set(ZERO);
			wsaaCount.add(1);
		}

		wsaaGvacstsCnttype.set(sqlCnttype);
		wsaaGvacstsCrtable.set(sqlCrtable);
		wsaaGvacstsCnpstat.set(sqlCnpstat);
		wsaaGvacstsCrpstat.set(sqlCrpstat);
		gvacstsIO.setParams(SPACES);
		gvacstsIO.setChdrcoy(sqlChdrcoy);
		gvacstsIO.setCntbranch(sqlCntbranch);
		gvacstsIO.setAcctyr(pj517par.acctyr);
		gvacstsIO.setAcctccy(sqlAcctccy);
		gvacstsIO.setRegister(sqlRegister);
		gvacstsIO.setStatSect(sqlStsect);
		gvacstsIO.setStatSubsect(sqlStssect);
		gvacstsIO.setCnttype(sqlCnttype);
		gvacstsIO.setCrtable(sqlCrtable);
		gvacstsIO.setCnPremStat(sqlCnpstat);
		gvacstsIO.setCovPremStat(sqlCrpstat);
		gvacstsIO.setFunction(varcom.begn);
		gvacstsIO.setFormat(gvacstsrec);
	}

protected void h520Read()
	{	//performance improvement --  atiwari23
	gvacstsIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);

		SmartFileCode.execute(appVars, gvacstsIO);
		if (isNE(gvacstsIO.getStatuz(),varcom.oK)
		&& isNE(gvacstsIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(gvacstsIO.getParams());
			fatalError600();
		}
		if (isEQ(gvacstsIO.getStatuz(),varcom.endp)
		|| isNE(sqlAcctyr,gvacstsIO.getAcctyr())
		|| isNE(sqlRegister,gvacstsIO.getRegister())
		|| isNE(sqlAcctccy,gvacstsIO.getAcctccy())
		|| isNE(sqlChdrcoy,gvacstsIO.getChdrcoy())
		|| isNE(sqlCnttype,gvacstsIO.getCnttype())
		|| isNE(sqlCrtable,gvacstsIO.getCrtable())
		|| isNE(sqlCntbranch,gvacstsIO.getCntbranch())
		|| isNE(sqlStsect,gvacstsIO.getStatSect())
		|| isNE(sqlStssect,gvacstsIO.getStatSubsect())
		|| isNE(sqlCnpstat,gvacstsIO.getCnPremStat())
		|| isNE(sqlCrpstat,gvacstsIO.getCovPremStat())) {
			gvacstsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.h590Exit);
		}
		if (isNE(gvacstsIO.getStaccrip(pj517par.acctmnth),ZERO)) {
			wsaaStaccripInd.set(wsaaY);
		}
		for (mth.set(1); !(isGT(mth,pj517par.acctmnth)); mth.add(1)){
			wsaaStaccpdb.add(gvacstsIO.getStaccpdb(mth));
			wsaaStaccmtb.add(gvacstsIO.getStaccmtb(mth));
			wsaaStaccob.add(gvacstsIO.getStaccob(mth));
		}
		if (isEQ(gvacstsIO.getCnPremStat(),wsaaFp)) {
			wsaaFpuDbp.add(wsaaStaccpdb);
			wsaaFpuMtb.add(wsaaStaccmtb);
			wsaaFpuOb.add(wsaaStaccob);
		}
		else {
			if (isEQ(gvacstsIO.getCnPremStat(),wsaaPu)) {
				wsaaRpuDbp.add(wsaaStaccpdb);
				wsaaRpuMtb.add(wsaaStaccmtb);
				wsaaRpuOb.add(wsaaStaccob);
			}
			else {
				if (isNE(sqlBillfreq,wsaa00)) {
					wsaaRegDbp.add(wsaaStaccpdb);
					wsaaRegMtb.add(wsaaStaccmtb);
					wsaaRegOb.add(wsaaStaccob);
				}
				else {
					wsaaSglDbp.add(wsaaStaccpdb);
					wsaaSglMtb.add(wsaaStaccmtb);
					wsaaSglOb.add(wsaaStaccob);
				}
			}
		}
		if (isNE(tmpStraamt[pj517par.acctmnth.toInt()],ZERO)) {
			wsaaRiDbp.add(wsaaStaccpdb);
			wsaaRiMtb.add(wsaaStaccmtb);
			wsaaRiOb.add(wsaaStaccob);
		}
		if (isNE(sqlParind,wsaaP)) {
			if (isEQ(wsaaInd1,wsaaY)) {
				wsaaDbp[5].add(wsaaStaccpdb);
				wsaaMtb[5].add(wsaaStaccmtb);
				wsaaOb[5].add(wsaaStaccob);
			}
			else {
				if (isEQ(wsaaInd2,wsaaY)) {
					wsaaDbp[3].add(wsaaStaccpdb);
					wsaaMtb[3].add(wsaaStaccmtb);
					wsaaOb[3].add(wsaaStaccob);
				}
				else {
					wsaaDbp[1].add(wsaaStaccpdb);
					wsaaMtb[1].add(wsaaStaccmtb);
					wsaaOb[1].add(wsaaStaccob);
				}
			}
		}
		if (isEQ(sqlParind,wsaaP)) {
			if (isEQ(wsaaInd1,wsaaY)) {
				wsaaDbp[6].add(wsaaStaccpdb);
				wsaaMtb[6].add(wsaaStaccmtb);
				wsaaOb[6].add(wsaaStaccob);
			}
			else {
				if (isEQ(wsaaInd2,wsaaY)) {
					wsaaDbp[4].add(wsaaStaccpdb);
					wsaaMtb[4].add(wsaaStaccmtb);
					wsaaOb[4].add(wsaaStaccob);
				}
				else {
					wsaaDbp[2].add(wsaaStaccpdb);
					wsaaMtb[2].add(wsaaStaccmtb);
					wsaaOb[2].add(wsaaStaccob);
				}
			}
		}
		gvacstsIO.setFunction(varcom.nextr);
		wsaaStaccpdb.set(ZERO);
		wsaaStaccmtb.set(ZERO);
		wsaaStaccob.set(ZERO);
		goTo(GotoLabel.h520Read);
	}

protected void h550Accumulate()
	{
		/*H551-START*/
		if (isEQ(wsaaStaccripInd,wsaaY)) {
			wsaaCount.set(7);
		}
		else {
			wsaaCount.set(1);
		}
		wsaaCount2.set(1);
		while ( !(isGT(wsaaCount2,6))) {
			wsaaBlStaccpdb[wsaaCount.toInt()].add(wsaaDbp[wsaaCount2.toInt()]);
			wsaaBlStaccmtb[wsaaCount.toInt()].add(wsaaMtb[wsaaCount2.toInt()]);
			wsaaBlStaccob[wsaaCount.toInt()].add(wsaaOb[wsaaCount2.toInt()]);
			wsaaCount.add(1);
			wsaaCount2.add(1);
		}

		/*H559-EXIT*/
	}

protected void h600SetHeading()
	{
		h610Start();
	}

protected void h610Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(sqlChdrcoy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(sqlChdrcoy);
		rh01Companynm.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sqlAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		acctccy.set(sqlAcctccy);
		currdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3589);
		descIO.setDescitem(sqlRegister);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		register.set(sqlRegister);
		descrip.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5685);
		descIO.setDescitem(sqlStsect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stsect.set(sqlStsect);
		itmdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5684);
		descIO.setDescitem(sqlStssect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stssect.set(sqlStssect);
		longdesc.set(descIO.getLongdesc());
	}

protected void h700SumBr()
	{
		h710Start();
	}

protected void h710Start()
	{
		if (isNE(sqlParind,wsaaP)) {
			if (isEQ(wsaaInd1,wsaaY)) {
				wsaaBlNofpol[5].add(wsaaStcamt);
				wsaaBlNoflif[5].add(wsaaStlamt);
				if (isNE(sqlBillfreq,wsaa00)) {
					wsaaBlBprem[5].add(wsaaStbamt);
				}
				else {
					wsaaBlBprem[5].add(wsaaStsamt);
				}
			}
			else {
				if (isEQ(wsaaInd2,wsaaY)) {
					wsaaBlNofpol[3].add(wsaaStcamt);
					wsaaBlNoflif[3].add(wsaaStlamt);
					if (isNE(sqlBillfreq,wsaa00)) {
						wsaaBlBprem[3].add(wsaaStbamt);
					}
					else {
						wsaaBlBprem[3].add(wsaaStsamt);
					}
				}
				else {
					wsaaBlNofpol[1].add(wsaaStcamt);
					wsaaBlNoflif[1].add(wsaaStlamt);
					if (isNE(sqlBillfreq,wsaa00)) {
						wsaaBlBprem[1].add(wsaaStbamt);
					}
					else {
						wsaaBlBprem[1].add(wsaaStsamt);
					}
				}
			}
		}
		if (isEQ(sqlParind,wsaaP)) {
			if (isEQ(wsaaInd1,wsaaY)) {
				wsaaBlNofpol[6].add(wsaaStcamt);
				wsaaBlNoflif[6].add(wsaaStlamt);
				if (isNE(sqlBillfreq,wsaa00)) {
					wsaaBlBprem[6].add(wsaaStbamt);
				}
				else {
					wsaaBlBprem[6].add(wsaaStsamt);
				}
			}
			else {
				if (isEQ(wsaaInd2,wsaaY)) {
					wsaaBlNofpol[4].add(wsaaStcamt);
					wsaaBlNoflif[4].add(wsaaStlamt);
					if (isNE(sqlBillfreq,wsaa00)) {
						wsaaBlBprem[4].add(wsaaStbamt);
					}
					else {
						wsaaBlBprem[4].add(wsaaStsamt);
					}
				}
				else {
					wsaaBlNofpol[2].add(wsaaStcamt);
					wsaaBlNoflif[2].add(wsaaStlamt);
					if (isNE(sqlBillfreq,wsaa00)) {
						wsaaBlBprem[2].add(wsaaStbamt);
					}
					else {
						wsaaBlBprem[2].add(wsaaStsamt);
					}
				}
			}
		}
	}

protected void h750SumAr()
	{
		h751Start();
	}

protected void h751Start()
	{
		compute(wsaaBlRiStbamt, 3).setRounded(div(wsaaRiStbamt,1000));
		if (isNE(sqlParind,wsaaP)) {
			if (isEQ(wsaaInd1,wsaaY)) {
				wsaaBlNofpol[11].add(wsaaStcamt);
				wsaaBlNoflif[11].add(wsaaStlamt);
				wsaaBlBprem[11].add(wsaaStbamt);
				wsaaBlBprem[11].add(wsaaStsamt);
				wsaaBlBprem[11].add(wsaaBlRiStbamt);
			}
			else {
				if (isEQ(wsaaInd2,wsaaY)) {
					wsaaBlNofpol[9].add(wsaaStcamt);
					wsaaBlNoflif[9].add(wsaaStlamt);
					wsaaBlBprem[9].add(wsaaBlRiStbamt);
				}
				else {
					wsaaBlNofpol[7].add(wsaaStcamt);
					wsaaBlNoflif[7].add(wsaaStlamt);
					wsaaBlBprem[7].add(wsaaBlRiStbamt);
				}
			}
		}
		if (isEQ(sqlParind,wsaaP)) {
			if (isEQ(wsaaInd1,wsaaY)) {
				wsaaBlNofpol[12].add(wsaaStcamt);
				wsaaBlNoflif[12].add(wsaaStlamt);
				wsaaBlBprem[12].add(wsaaBlRiStbamt);
			}
			else {
				if (isEQ(wsaaInd2,wsaaY)) {
					wsaaBlNofpol[10].add(wsaaStcamt);
					wsaaBlNoflif[10].add(wsaaStlamt);
					wsaaBlBprem[10].add(wsaaBlRiStbamt);
				}
				else {
					wsaaBlNofpol[8].add(wsaaStcamt);
					wsaaBlNoflif[8].add(wsaaStlamt);
					wsaaBlBprem[8].add(wsaaBlRiStbamt);
				}
			}
		}
	}

protected void h900WriteBl()
	{
		/*H910-START*/
		stcmthg.set(wsaaBlNofpol[wsaaCount.toInt()]);
		stlmth.set(wsaaBlNoflif[wsaaCount.toInt()]);
		compute(dancar01, 3).setRounded(div(wsaaBlStaccpdb[wsaaCount.toInt()],1000));
		compute(dancar02, 3).setRounded(div(wsaaBlStaccmtb[wsaaCount.toInt()],1000));
		compute(dancar03, 3).setRounded(div(wsaaBlStaccob[wsaaCount.toInt()],1000));
		compute(dancar04, 3).setRounded(div(wsaaBlBprem[wsaaCount.toInt()],1000));
		printerFile.printRj532d01(rj532D01);
		/*H990-EXIT*/
	}
}
