/*********************  */
/*Author  :Liwei		   */
/*Purpose :curd foe Agprpf*/
/*Date    :2018.11.24	   */
package com.csc.life.statistics.dataaccess.dao;

import com.csc.life.statistics.dataaccess.model.Agprpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface AgprpfDAO extends BaseDAO<Agprpf>{
	public void insertAgprpfRecord(Agprpf agprpf);
}
