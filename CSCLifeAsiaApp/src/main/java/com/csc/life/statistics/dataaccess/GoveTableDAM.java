package com.csc.life.statistics.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GoveTableDAM.java
 * Date: Sun, 30 Aug 2009 03:38:43
 * Class transformed from GOVE.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GoveTableDAM extends GovepfTableDAM {

	public GoveTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("GOVE");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CNTBRANCH"
		             + ", REG"
		             + ", SRCEBUS"
		             + ", STATCAT"
		             + ", STFUND"
		             + ", STSECT"
		             + ", STSSECT"
		             + ", CNPSTAT"
		             + ", CRPSTAT"
		             + ", ACCTCCY"
		             + ", CNTCURR"
		             + ", CNTTYPE"
		             + ", CRTABLE"
		             + ", PARIND"
		             + ", BILLFREQ"
		             + ", ACCTYR";
		
		QUALIFIEDCOLUMNS = 
		            "REG, " +
		            "CHDRCOY, " +
		            "CNTBRANCH, " +
		            "STFUND, " +
		            "STSECT, " +
		            "STSSECT, " +
		            "STATCAT, " +
		            "CNPSTAT, " +
		            "CRPSTAT, " +
		            "PARIND, " +
		            "CNTTYPE, " +
		            "CRTABLE, " +
		            "ACCTCCY, " +
		            "CNTCURR, " +
		            "ACCTYR, " +
		            "SRCEBUS, " +
		            "BILLFREQ, " +
		            "BFWDC, " +
		            "STCMTH01, " +
		            "STCMTH02, " +
		            "STCMTH03, " +
		            "STCMTH04, " +
		            "STCMTH05, " +
		            "STCMTH06, " +
		            "STCMTH07, " +
		            "STCMTH08, " +
		            "STCMTH09, " +
		            "STCMTH10, " +
		            "STCMTH11, " +
		            "STCMTH12, " +
		            "CFWDC, " +
		            "BFWDP, " +
		            "STPMTH01, " +
		            "STPMTH02, " +
		            "STPMTH03, " +
		            "STPMTH04, " +
		            "STPMTH05, " +
		            "STPMTH06, " +
		            "STPMTH07, " +
		            "STPMTH08, " +
		            "STPMTH09, " +
		            "STPMTH10, " +
		            "STPMTH11, " +
		            "STPMTH12, " +
		            "CFWDP, " +
		            "BFWDS, " +
		            "STSMTH01, " +
		            "STSMTH02, " +
		            "STSMTH03, " +
		            "STSMTH04, " +
		            "STSMTH05, " +
		            "STSMTH06, " +
		            "STSMTH07, " +
		            "STSMTH08, " +
		            "STSMTH09, " +
		            "STSMTH10, " +
		            "STSMTH11, " +
		            "STSMTH12, " +
		            "CFWDS, " +
		            "BFWDI, " +
		            "STIMTH01, " +
		            "STIMTH02, " +
		            "STIMTH03, " +
		            "STIMTH04, " +
		            "STIMTH05, " +
		            "STIMTH06, " +
		            "STIMTH07, " +
		            "STIMTH08, " +
		            "STIMTH09, " +
		            "STIMTH10, " +
		            "STIMTH11, " +
		            "STIMTH12, " +
		            "CFWDI, " +
		            "BFWDL, " +
		            "STLMTH01, " +
		            "STLMTH02, " +
		            "STLMTH03, " +
		            "STLMTH04, " +
		            "STLMTH05, " +
		            "STLMTH06, " +
		            "STLMTH07, " +
		            "STLMTH08, " +
		            "STLMTH09, " +
		            "STLMTH10, " +
		            "STLMTH11, " +
		            "STLMTH12, " +
		            "CFWDL, " +
		            "BFWDA, " +
		            "STAMTH01, " +
		            "STAMTH02, " +
		            "STAMTH03, " +
		            "STAMTH04, " +
		            "STAMTH05, " +
		            "STAMTH06, " +
		            "STAMTH07, " +
		            "STAMTH08, " +
		            "STAMTH09, " +
		            "STAMTH10, " +
		            "STAMTH11, " +
		            "STAMTH12, " +
		            "CFWDA, " +
		            "BFWDB, " +
		            "STBMTHG01, " +
		            "STBMTHG02, " +
		            "STBMTHG03, " +
		            "STBMTHG04, " +
		            "STBMTHG05, " +
		            "STBMTHG06, " +
		            "STBMTHG07, " +
		            "STBMTHG08, " +
		            "STBMTHG09, " +
		            "STBMTHG10, " +
		            "STBMTHG11, " +
		            "STBMTHG12, " +
		            "CFWDB, " +
		            "BFWDLD, " +
		            "STLDMTHG01, " +
		            "STLDMTHG02, " +
		            "STLDMTHG03, " +
		            "STLDMTHG04, " +
		            "STLDMTHG05, " +
		            "STLDMTHG06, " +
		            "STLDMTHG07, " +
		            "STLDMTHG08, " +
		            "STLDMTHG09, " +
		            "STLDMTHG10, " +
		            "STLDMTHG11, " +
		            "STLDMTHG12, " +
		            "CFWDLD, " +
		            "BFWDRA, " +
		            "STRAAMT01, " +
		            "STRAAMT02, " +
		            "STRAAMT03, " +
		            "STRAAMT04, " +
		            "STRAAMT05, " +
		            "STRAAMT06, " +
		            "STRAAMT07, " +
		            "STRAAMT08, " +
		            "STRAAMT09, " +
		            "STRAAMT10, " +
		            "STRAAMT11, " +
		            "STRAAMT12, " +
		            "CFWDRA, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CNTBRANCH ASC, " +
		            "REG ASC, " +
		            "SRCEBUS ASC, " +
		            "STATCAT ASC, " +
		            "STFUND ASC, " +
		            "STSECT ASC, " +
		            "STSSECT ASC, " +
		            "CNPSTAT ASC, " +
		            "CRPSTAT ASC, " +
		            "ACCTCCY ASC, " +
		            "CNTCURR ASC, " +
		            "CNTTYPE ASC, " +
		            "CRTABLE ASC, " +
		            "PARIND ASC, " +
		            "BILLFREQ ASC, " +
		            "ACCTYR ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CNTBRANCH DESC, " +
		            "REG DESC, " +
		            "SRCEBUS DESC, " +
		            "STATCAT DESC, " +
		            "STFUND DESC, " +
		            "STSECT DESC, " +
		            "STSSECT DESC, " +
		            "CNPSTAT DESC, " +
		            "CRPSTAT DESC, " +
		            "ACCTCCY DESC, " +
		            "CNTCURR DESC, " +
		            "CNTTYPE DESC, " +
		            "CRTABLE DESC, " +
		            "PARIND DESC, " +
		            "BILLFREQ DESC, " +
		            "ACCTYR DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               register,
                               chdrcoy,
                               cntbranch,
                               statFund,
                               statSect,
                               statSubsect,
                               statcat,
                               cnPremStat,
                               covPremStat,
                               parind,
                               cnttype,
                               crtable,
                               acctccy,
                               cntcurr,
                               acctyr,
                               srcebus,
                               billfreq,
                               bfwdc,
                               stcmth01,
                               stcmth02,
                               stcmth03,
                               stcmth04,
                               stcmth05,
                               stcmth06,
                               stcmth07,
                               stcmth08,
                               stcmth09,
                               stcmth10,
                               stcmth11,
                               stcmth12,
                               cfwdc,
                               bfwdp,
                               stpmth01,
                               stpmth02,
                               stpmth03,
                               stpmth04,
                               stpmth05,
                               stpmth06,
                               stpmth07,
                               stpmth08,
                               stpmth09,
                               stpmth10,
                               stpmth11,
                               stpmth12,
                               cfwdp,
                               bfwds,
                               stsmth01,
                               stsmth02,
                               stsmth03,
                               stsmth04,
                               stsmth05,
                               stsmth06,
                               stsmth07,
                               stsmth08,
                               stsmth09,
                               stsmth10,
                               stsmth11,
                               stsmth12,
                               cfwds,
                               bfwdi,
                               stimth01,
                               stimth02,
                               stimth03,
                               stimth04,
                               stimth05,
                               stimth06,
                               stimth07,
                               stimth08,
                               stimth09,
                               stimth10,
                               stimth11,
                               stimth12,
                               cfwdi,
                               bfwdl,
                               stlmth01,
                               stlmth02,
                               stlmth03,
                               stlmth04,
                               stlmth05,
                               stlmth06,
                               stlmth07,
                               stlmth08,
                               stlmth09,
                               stlmth10,
                               stlmth11,
                               stlmth12,
                               cfwdl,
                               bfwda,
                               stamth01,
                               stamth02,
                               stamth03,
                               stamth04,
                               stamth05,
                               stamth06,
                               stamth07,
                               stamth08,
                               stamth09,
                               stamth10,
                               stamth11,
                               stamth12,
                               cfwda,
                               bfwdb,
                               stbmthg01,
                               stbmthg02,
                               stbmthg03,
                               stbmthg04,
                               stbmthg05,
                               stbmthg06,
                               stbmthg07,
                               stbmthg08,
                               stbmthg09,
                               stbmthg10,
                               stbmthg11,
                               stbmthg12,
                               cfwdb,
                               bfwdld,
                               stldmthg01,
                               stldmthg02,
                               stldmthg03,
                               stldmthg04,
                               stldmthg05,
                               stldmthg06,
                               stldmthg07,
                               stldmthg08,
                               stldmthg09,
                               stldmthg10,
                               stldmthg11,
                               stldmthg12,
                               cfwdld,
                               bfwdra,
                               straamt01,
                               straamt02,
                               straamt03,
                               straamt04,
                               straamt05,
                               straamt06,
                               straamt07,
                               straamt08,
                               straamt09,
                               straamt10,
                               straamt11,
                               straamt12,
                               cfwdra,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(24);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getCntbranch().toInternal()
					+ getRegister().toInternal()
					+ getSrcebus().toInternal()
					+ getStatcat().toInternal()
					+ getStatFund().toInternal()
					+ getStatSect().toInternal()
					+ getStatSubsect().toInternal()
					+ getCnPremStat().toInternal()
					+ getCovPremStat().toInternal()
					+ getAcctccy().toInternal()
					+ getCntcurr().toInternal()
					+ getCnttype().toInternal()
					+ getCrtable().toInternal()
					+ getParind().toInternal()
					+ getBillfreq().toInternal()
					+ getAcctyr().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, statcat);
			what = ExternalData.chop(what, statFund);
			what = ExternalData.chop(what, statSect);
			what = ExternalData.chop(what, statSubsect);
			what = ExternalData.chop(what, cnPremStat);
			what = ExternalData.chop(what, covPremStat);
			what = ExternalData.chop(what, acctccy);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, parind);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, acctyr);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller130 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller140 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller150 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller160 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller170 = new FixedLengthStringData(2);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(register.toInternal());
	nonKeyFiller30.setInternal(cntbranch.toInternal());
	nonKeyFiller40.setInternal(statFund.toInternal());
	nonKeyFiller50.setInternal(statSect.toInternal());
	nonKeyFiller60.setInternal(statSubsect.toInternal());
	nonKeyFiller70.setInternal(statcat.toInternal());
	nonKeyFiller80.setInternal(cnPremStat.toInternal());
	nonKeyFiller90.setInternal(covPremStat.toInternal());
	nonKeyFiller100.setInternal(parind.toInternal());
	nonKeyFiller110.setInternal(cnttype.toInternal());
	nonKeyFiller120.setInternal(crtable.toInternal());
	nonKeyFiller130.setInternal(acctccy.toInternal());
	nonKeyFiller140.setInternal(cntcurr.toInternal());
	nonKeyFiller150.setInternal(acctyr.toInternal());
	nonKeyFiller160.setInternal(srcebus.toInternal());
	nonKeyFiller170.setInternal(billfreq.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(1226);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ nonKeyFiller130.toInternal()
					+ nonKeyFiller140.toInternal()
					+ nonKeyFiller150.toInternal()
					+ nonKeyFiller160.toInternal()
					+ nonKeyFiller170.toInternal()
					+ getBfwdc().toInternal()
					+ getStcmth01().toInternal()
					+ getStcmth02().toInternal()
					+ getStcmth03().toInternal()
					+ getStcmth04().toInternal()
					+ getStcmth05().toInternal()
					+ getStcmth06().toInternal()
					+ getStcmth07().toInternal()
					+ getStcmth08().toInternal()
					+ getStcmth09().toInternal()
					+ getStcmth10().toInternal()
					+ getStcmth11().toInternal()
					+ getStcmth12().toInternal()
					+ getCfwdc().toInternal()
					+ getBfwdp().toInternal()
					+ getStpmth01().toInternal()
					+ getStpmth02().toInternal()
					+ getStpmth03().toInternal()
					+ getStpmth04().toInternal()
					+ getStpmth05().toInternal()
					+ getStpmth06().toInternal()
					+ getStpmth07().toInternal()
					+ getStpmth08().toInternal()
					+ getStpmth09().toInternal()
					+ getStpmth10().toInternal()
					+ getStpmth11().toInternal()
					+ getStpmth12().toInternal()
					+ getCfwdp().toInternal()
					+ getBfwds().toInternal()
					+ getStsmth01().toInternal()
					+ getStsmth02().toInternal()
					+ getStsmth03().toInternal()
					+ getStsmth04().toInternal()
					+ getStsmth05().toInternal()
					+ getStsmth06().toInternal()
					+ getStsmth07().toInternal()
					+ getStsmth08().toInternal()
					+ getStsmth09().toInternal()
					+ getStsmth10().toInternal()
					+ getStsmth11().toInternal()
					+ getStsmth12().toInternal()
					+ getCfwds().toInternal()
					+ getBfwdi().toInternal()
					+ getStimth01().toInternal()
					+ getStimth02().toInternal()
					+ getStimth03().toInternal()
					+ getStimth04().toInternal()
					+ getStimth05().toInternal()
					+ getStimth06().toInternal()
					+ getStimth07().toInternal()
					+ getStimth08().toInternal()
					+ getStimth09().toInternal()
					+ getStimth10().toInternal()
					+ getStimth11().toInternal()
					+ getStimth12().toInternal()
					+ getCfwdi().toInternal()
					+ getBfwdl().toInternal()
					+ getStlmth01().toInternal()
					+ getStlmth02().toInternal()
					+ getStlmth03().toInternal()
					+ getStlmth04().toInternal()
					+ getStlmth05().toInternal()
					+ getStlmth06().toInternal()
					+ getStlmth07().toInternal()
					+ getStlmth08().toInternal()
					+ getStlmth09().toInternal()
					+ getStlmth10().toInternal()
					+ getStlmth11().toInternal()
					+ getStlmth12().toInternal()
					+ getCfwdl().toInternal()
					+ getBfwda().toInternal()
					+ getStamth01().toInternal()
					+ getStamth02().toInternal()
					+ getStamth03().toInternal()
					+ getStamth04().toInternal()
					+ getStamth05().toInternal()
					+ getStamth06().toInternal()
					+ getStamth07().toInternal()
					+ getStamth08().toInternal()
					+ getStamth09().toInternal()
					+ getStamth10().toInternal()
					+ getStamth11().toInternal()
					+ getStamth12().toInternal()
					+ getCfwda().toInternal()
					+ getBfwdb().toInternal()
					+ getStbmthg01().toInternal()
					+ getStbmthg02().toInternal()
					+ getStbmthg03().toInternal()
					+ getStbmthg04().toInternal()
					+ getStbmthg05().toInternal()
					+ getStbmthg06().toInternal()
					+ getStbmthg07().toInternal()
					+ getStbmthg08().toInternal()
					+ getStbmthg09().toInternal()
					+ getStbmthg10().toInternal()
					+ getStbmthg11().toInternal()
					+ getStbmthg12().toInternal()
					+ getCfwdb().toInternal()
					+ getBfwdld().toInternal()
					+ getStldmthg01().toInternal()
					+ getStldmthg02().toInternal()
					+ getStldmthg03().toInternal()
					+ getStldmthg04().toInternal()
					+ getStldmthg05().toInternal()
					+ getStldmthg06().toInternal()
					+ getStldmthg07().toInternal()
					+ getStldmthg08().toInternal()
					+ getStldmthg09().toInternal()
					+ getStldmthg10().toInternal()
					+ getStldmthg11().toInternal()
					+ getStldmthg12().toInternal()
					+ getCfwdld().toInternal()
					+ getBfwdra().toInternal()
					+ getStraamt01().toInternal()
					+ getStraamt02().toInternal()
					+ getStraamt03().toInternal()
					+ getStraamt04().toInternal()
					+ getStraamt05().toInternal()
					+ getStraamt06().toInternal()
					+ getStraamt07().toInternal()
					+ getStraamt08().toInternal()
					+ getStraamt09().toInternal()
					+ getStraamt10().toInternal()
					+ getStraamt11().toInternal()
					+ getStraamt12().toInternal()
					+ getCfwdra().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, nonKeyFiller130);
			what = ExternalData.chop(what, nonKeyFiller140);
			what = ExternalData.chop(what, nonKeyFiller150);
			what = ExternalData.chop(what, nonKeyFiller160);
			what = ExternalData.chop(what, nonKeyFiller170);
			what = ExternalData.chop(what, bfwdc);
			what = ExternalData.chop(what, stcmth01);
			what = ExternalData.chop(what, stcmth02);
			what = ExternalData.chop(what, stcmth03);
			what = ExternalData.chop(what, stcmth04);
			what = ExternalData.chop(what, stcmth05);
			what = ExternalData.chop(what, stcmth06);
			what = ExternalData.chop(what, stcmth07);
			what = ExternalData.chop(what, stcmth08);
			what = ExternalData.chop(what, stcmth09);
			what = ExternalData.chop(what, stcmth10);
			what = ExternalData.chop(what, stcmth11);
			what = ExternalData.chop(what, stcmth12);
			what = ExternalData.chop(what, cfwdc);
			what = ExternalData.chop(what, bfwdp);
			what = ExternalData.chop(what, stpmth01);
			what = ExternalData.chop(what, stpmth02);
			what = ExternalData.chop(what, stpmth03);
			what = ExternalData.chop(what, stpmth04);
			what = ExternalData.chop(what, stpmth05);
			what = ExternalData.chop(what, stpmth06);
			what = ExternalData.chop(what, stpmth07);
			what = ExternalData.chop(what, stpmth08);
			what = ExternalData.chop(what, stpmth09);
			what = ExternalData.chop(what, stpmth10);
			what = ExternalData.chop(what, stpmth11);
			what = ExternalData.chop(what, stpmth12);
			what = ExternalData.chop(what, cfwdp);
			what = ExternalData.chop(what, bfwds);
			what = ExternalData.chop(what, stsmth01);
			what = ExternalData.chop(what, stsmth02);
			what = ExternalData.chop(what, stsmth03);
			what = ExternalData.chop(what, stsmth04);
			what = ExternalData.chop(what, stsmth05);
			what = ExternalData.chop(what, stsmth06);
			what = ExternalData.chop(what, stsmth07);
			what = ExternalData.chop(what, stsmth08);
			what = ExternalData.chop(what, stsmth09);
			what = ExternalData.chop(what, stsmth10);
			what = ExternalData.chop(what, stsmth11);
			what = ExternalData.chop(what, stsmth12);
			what = ExternalData.chop(what, cfwds);
			what = ExternalData.chop(what, bfwdi);
			what = ExternalData.chop(what, stimth01);
			what = ExternalData.chop(what, stimth02);
			what = ExternalData.chop(what, stimth03);
			what = ExternalData.chop(what, stimth04);
			what = ExternalData.chop(what, stimth05);
			what = ExternalData.chop(what, stimth06);
			what = ExternalData.chop(what, stimth07);
			what = ExternalData.chop(what, stimth08);
			what = ExternalData.chop(what, stimth09);
			what = ExternalData.chop(what, stimth10);
			what = ExternalData.chop(what, stimth11);
			what = ExternalData.chop(what, stimth12);
			what = ExternalData.chop(what, cfwdi);
			what = ExternalData.chop(what, bfwdl);
			what = ExternalData.chop(what, stlmth01);
			what = ExternalData.chop(what, stlmth02);
			what = ExternalData.chop(what, stlmth03);
			what = ExternalData.chop(what, stlmth04);
			what = ExternalData.chop(what, stlmth05);
			what = ExternalData.chop(what, stlmth06);
			what = ExternalData.chop(what, stlmth07);
			what = ExternalData.chop(what, stlmth08);
			what = ExternalData.chop(what, stlmth09);
			what = ExternalData.chop(what, stlmth10);
			what = ExternalData.chop(what, stlmth11);
			what = ExternalData.chop(what, stlmth12);
			what = ExternalData.chop(what, cfwdl);
			what = ExternalData.chop(what, bfwda);
			what = ExternalData.chop(what, stamth01);
			what = ExternalData.chop(what, stamth02);
			what = ExternalData.chop(what, stamth03);
			what = ExternalData.chop(what, stamth04);
			what = ExternalData.chop(what, stamth05);
			what = ExternalData.chop(what, stamth06);
			what = ExternalData.chop(what, stamth07);
			what = ExternalData.chop(what, stamth08);
			what = ExternalData.chop(what, stamth09);
			what = ExternalData.chop(what, stamth10);
			what = ExternalData.chop(what, stamth11);
			what = ExternalData.chop(what, stamth12);
			what = ExternalData.chop(what, cfwda);
			what = ExternalData.chop(what, bfwdb);
			what = ExternalData.chop(what, stbmthg01);
			what = ExternalData.chop(what, stbmthg02);
			what = ExternalData.chop(what, stbmthg03);
			what = ExternalData.chop(what, stbmthg04);
			what = ExternalData.chop(what, stbmthg05);
			what = ExternalData.chop(what, stbmthg06);
			what = ExternalData.chop(what, stbmthg07);
			what = ExternalData.chop(what, stbmthg08);
			what = ExternalData.chop(what, stbmthg09);
			what = ExternalData.chop(what, stbmthg10);
			what = ExternalData.chop(what, stbmthg11);
			what = ExternalData.chop(what, stbmthg12);
			what = ExternalData.chop(what, cfwdb);
			what = ExternalData.chop(what, bfwdld);
			what = ExternalData.chop(what, stldmthg01);
			what = ExternalData.chop(what, stldmthg02);
			what = ExternalData.chop(what, stldmthg03);
			what = ExternalData.chop(what, stldmthg04);
			what = ExternalData.chop(what, stldmthg05);
			what = ExternalData.chop(what, stldmthg06);
			what = ExternalData.chop(what, stldmthg07);
			what = ExternalData.chop(what, stldmthg08);
			what = ExternalData.chop(what, stldmthg09);
			what = ExternalData.chop(what, stldmthg10);
			what = ExternalData.chop(what, stldmthg11);
			what = ExternalData.chop(what, stldmthg12);
			what = ExternalData.chop(what, cfwdld);
			what = ExternalData.chop(what, bfwdra);
			what = ExternalData.chop(what, straamt01);
			what = ExternalData.chop(what, straamt02);
			what = ExternalData.chop(what, straamt03);
			what = ExternalData.chop(what, straamt04);
			what = ExternalData.chop(what, straamt05);
			what = ExternalData.chop(what, straamt06);
			what = ExternalData.chop(what, straamt07);
			what = ExternalData.chop(what, straamt08);
			what = ExternalData.chop(what, straamt09);
			what = ExternalData.chop(what, straamt10);
			what = ExternalData.chop(what, straamt11);
			what = ExternalData.chop(what, straamt12);
			what = ExternalData.chop(what, cfwdra);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}
	public FixedLengthStringData getStatcat() {
		return statcat;
	}
	public void setStatcat(Object what) {
		statcat.set(what);
	}
	public FixedLengthStringData getStatFund() {
		return statFund;
	}
	public void setStatFund(Object what) {
		statFund.set(what);
	}
	public FixedLengthStringData getStatSect() {
		return statSect;
	}
	public void setStatSect(Object what) {
		statSect.set(what);
	}
	public FixedLengthStringData getStatSubsect() {
		return statSubsect;
	}
	public void setStatSubsect(Object what) {
		statSubsect.set(what);
	}
	public FixedLengthStringData getCnPremStat() {
		return cnPremStat;
	}
	public void setCnPremStat(Object what) {
		cnPremStat.set(what);
	}
	public FixedLengthStringData getCovPremStat() {
		return covPremStat;
	}
	public void setCovPremStat(Object what) {
		covPremStat.set(what);
	}
	public FixedLengthStringData getAcctccy() {
		return acctccy;
	}
	public void setAcctccy(Object what) {
		acctccy.set(what);
	}
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}
	public FixedLengthStringData getParind() {
		return parind;
	}
	public void setParind(Object what) {
		parind.set(what);
	}
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}
	public PackedDecimalData getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(Object what) {
		setAcctyr(what, false);
	}
	public void setAcctyr(Object what, boolean rounded) {
		if (rounded)
			acctyr.setRounded(what);
		else
			acctyr.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getBfwdc() {
		return bfwdc;
	}
	public void setBfwdc(Object what) {
		setBfwdc(what, false);
	}
	public void setBfwdc(Object what, boolean rounded) {
		if (rounded)
			bfwdc.setRounded(what);
		else
			bfwdc.set(what);
	}	
	public PackedDecimalData getStcmth01() {
		return stcmth01;
	}
	public void setStcmth01(Object what) {
		setStcmth01(what, false);
	}
	public void setStcmth01(Object what, boolean rounded) {
		if (rounded)
			stcmth01.setRounded(what);
		else
			stcmth01.set(what);
	}	
	public PackedDecimalData getStcmth02() {
		return stcmth02;
	}
	public void setStcmth02(Object what) {
		setStcmth02(what, false);
	}
	public void setStcmth02(Object what, boolean rounded) {
		if (rounded)
			stcmth02.setRounded(what);
		else
			stcmth02.set(what);
	}	
	public PackedDecimalData getStcmth03() {
		return stcmth03;
	}
	public void setStcmth03(Object what) {
		setStcmth03(what, false);
	}
	public void setStcmth03(Object what, boolean rounded) {
		if (rounded)
			stcmth03.setRounded(what);
		else
			stcmth03.set(what);
	}	
	public PackedDecimalData getStcmth04() {
		return stcmth04;
	}
	public void setStcmth04(Object what) {
		setStcmth04(what, false);
	}
	public void setStcmth04(Object what, boolean rounded) {
		if (rounded)
			stcmth04.setRounded(what);
		else
			stcmth04.set(what);
	}	
	public PackedDecimalData getStcmth05() {
		return stcmth05;
	}
	public void setStcmth05(Object what) {
		setStcmth05(what, false);
	}
	public void setStcmth05(Object what, boolean rounded) {
		if (rounded)
			stcmth05.setRounded(what);
		else
			stcmth05.set(what);
	}	
	public PackedDecimalData getStcmth06() {
		return stcmth06;
	}
	public void setStcmth06(Object what) {
		setStcmth06(what, false);
	}
	public void setStcmth06(Object what, boolean rounded) {
		if (rounded)
			stcmth06.setRounded(what);
		else
			stcmth06.set(what);
	}	
	public PackedDecimalData getStcmth07() {
		return stcmth07;
	}
	public void setStcmth07(Object what) {
		setStcmth07(what, false);
	}
	public void setStcmth07(Object what, boolean rounded) {
		if (rounded)
			stcmth07.setRounded(what);
		else
			stcmth07.set(what);
	}	
	public PackedDecimalData getStcmth08() {
		return stcmth08;
	}
	public void setStcmth08(Object what) {
		setStcmth08(what, false);
	}
	public void setStcmth08(Object what, boolean rounded) {
		if (rounded)
			stcmth08.setRounded(what);
		else
			stcmth08.set(what);
	}	
	public PackedDecimalData getStcmth09() {
		return stcmth09;
	}
	public void setStcmth09(Object what) {
		setStcmth09(what, false);
	}
	public void setStcmth09(Object what, boolean rounded) {
		if (rounded)
			stcmth09.setRounded(what);
		else
			stcmth09.set(what);
	}	
	public PackedDecimalData getStcmth10() {
		return stcmth10;
	}
	public void setStcmth10(Object what) {
		setStcmth10(what, false);
	}
	public void setStcmth10(Object what, boolean rounded) {
		if (rounded)
			stcmth10.setRounded(what);
		else
			stcmth10.set(what);
	}	
	public PackedDecimalData getStcmth11() {
		return stcmth11;
	}
	public void setStcmth11(Object what) {
		setStcmth11(what, false);
	}
	public void setStcmth11(Object what, boolean rounded) {
		if (rounded)
			stcmth11.setRounded(what);
		else
			stcmth11.set(what);
	}	
	public PackedDecimalData getStcmth12() {
		return stcmth12;
	}
	public void setStcmth12(Object what) {
		setStcmth12(what, false);
	}
	public void setStcmth12(Object what, boolean rounded) {
		if (rounded)
			stcmth12.setRounded(what);
		else
			stcmth12.set(what);
	}	
	public PackedDecimalData getCfwdc() {
		return cfwdc;
	}
	public void setCfwdc(Object what) {
		setCfwdc(what, false);
	}
	public void setCfwdc(Object what, boolean rounded) {
		if (rounded)
			cfwdc.setRounded(what);
		else
			cfwdc.set(what);
	}	
	public PackedDecimalData getBfwdp() {
		return bfwdp;
	}
	public void setBfwdp(Object what) {
		setBfwdp(what, false);
	}
	public void setBfwdp(Object what, boolean rounded) {
		if (rounded)
			bfwdp.setRounded(what);
		else
			bfwdp.set(what);
	}	
	public PackedDecimalData getStpmth01() {
		return stpmth01;
	}
	public void setStpmth01(Object what) {
		setStpmth01(what, false);
	}
	public void setStpmth01(Object what, boolean rounded) {
		if (rounded)
			stpmth01.setRounded(what);
		else
			stpmth01.set(what);
	}	
	public PackedDecimalData getStpmth02() {
		return stpmth02;
	}
	public void setStpmth02(Object what) {
		setStpmth02(what, false);
	}
	public void setStpmth02(Object what, boolean rounded) {
		if (rounded)
			stpmth02.setRounded(what);
		else
			stpmth02.set(what);
	}	
	public PackedDecimalData getStpmth03() {
		return stpmth03;
	}
	public void setStpmth03(Object what) {
		setStpmth03(what, false);
	}
	public void setStpmth03(Object what, boolean rounded) {
		if (rounded)
			stpmth03.setRounded(what);
		else
			stpmth03.set(what);
	}	
	public PackedDecimalData getStpmth04() {
		return stpmth04;
	}
	public void setStpmth04(Object what) {
		setStpmth04(what, false);
	}
	public void setStpmth04(Object what, boolean rounded) {
		if (rounded)
			stpmth04.setRounded(what);
		else
			stpmth04.set(what);
	}	
	public PackedDecimalData getStpmth05() {
		return stpmth05;
	}
	public void setStpmth05(Object what) {
		setStpmth05(what, false);
	}
	public void setStpmth05(Object what, boolean rounded) {
		if (rounded)
			stpmth05.setRounded(what);
		else
			stpmth05.set(what);
	}	
	public PackedDecimalData getStpmth06() {
		return stpmth06;
	}
	public void setStpmth06(Object what) {
		setStpmth06(what, false);
	}
	public void setStpmth06(Object what, boolean rounded) {
		if (rounded)
			stpmth06.setRounded(what);
		else
			stpmth06.set(what);
	}	
	public PackedDecimalData getStpmth07() {
		return stpmth07;
	}
	public void setStpmth07(Object what) {
		setStpmth07(what, false);
	}
	public void setStpmth07(Object what, boolean rounded) {
		if (rounded)
			stpmth07.setRounded(what);
		else
			stpmth07.set(what);
	}	
	public PackedDecimalData getStpmth08() {
		return stpmth08;
	}
	public void setStpmth08(Object what) {
		setStpmth08(what, false);
	}
	public void setStpmth08(Object what, boolean rounded) {
		if (rounded)
			stpmth08.setRounded(what);
		else
			stpmth08.set(what);
	}	
	public PackedDecimalData getStpmth09() {
		return stpmth09;
	}
	public void setStpmth09(Object what) {
		setStpmth09(what, false);
	}
	public void setStpmth09(Object what, boolean rounded) {
		if (rounded)
			stpmth09.setRounded(what);
		else
			stpmth09.set(what);
	}	
	public PackedDecimalData getStpmth10() {
		return stpmth10;
	}
	public void setStpmth10(Object what) {
		setStpmth10(what, false);
	}
	public void setStpmth10(Object what, boolean rounded) {
		if (rounded)
			stpmth10.setRounded(what);
		else
			stpmth10.set(what);
	}	
	public PackedDecimalData getStpmth11() {
		return stpmth11;
	}
	public void setStpmth11(Object what) {
		setStpmth11(what, false);
	}
	public void setStpmth11(Object what, boolean rounded) {
		if (rounded)
			stpmth11.setRounded(what);
		else
			stpmth11.set(what);
	}	
	public PackedDecimalData getStpmth12() {
		return stpmth12;
	}
	public void setStpmth12(Object what) {
		setStpmth12(what, false);
	}
	public void setStpmth12(Object what, boolean rounded) {
		if (rounded)
			stpmth12.setRounded(what);
		else
			stpmth12.set(what);
	}	
	public PackedDecimalData getCfwdp() {
		return cfwdp;
	}
	public void setCfwdp(Object what) {
		setCfwdp(what, false);
	}
	public void setCfwdp(Object what, boolean rounded) {
		if (rounded)
			cfwdp.setRounded(what);
		else
			cfwdp.set(what);
	}	
	public PackedDecimalData getBfwds() {
		return bfwds;
	}
	public void setBfwds(Object what) {
		setBfwds(what, false);
	}
	public void setBfwds(Object what, boolean rounded) {
		if (rounded)
			bfwds.setRounded(what);
		else
			bfwds.set(what);
	}	
	public PackedDecimalData getStsmth01() {
		return stsmth01;
	}
	public void setStsmth01(Object what) {
		setStsmth01(what, false);
	}
	public void setStsmth01(Object what, boolean rounded) {
		if (rounded)
			stsmth01.setRounded(what);
		else
			stsmth01.set(what);
	}	
	public PackedDecimalData getStsmth02() {
		return stsmth02;
	}
	public void setStsmth02(Object what) {
		setStsmth02(what, false);
	}
	public void setStsmth02(Object what, boolean rounded) {
		if (rounded)
			stsmth02.setRounded(what);
		else
			stsmth02.set(what);
	}	
	public PackedDecimalData getStsmth03() {
		return stsmth03;
	}
	public void setStsmth03(Object what) {
		setStsmth03(what, false);
	}
	public void setStsmth03(Object what, boolean rounded) {
		if (rounded)
			stsmth03.setRounded(what);
		else
			stsmth03.set(what);
	}	
	public PackedDecimalData getStsmth04() {
		return stsmth04;
	}
	public void setStsmth04(Object what) {
		setStsmth04(what, false);
	}
	public void setStsmth04(Object what, boolean rounded) {
		if (rounded)
			stsmth04.setRounded(what);
		else
			stsmth04.set(what);
	}	
	public PackedDecimalData getStsmth05() {
		return stsmth05;
	}
	public void setStsmth05(Object what) {
		setStsmth05(what, false);
	}
	public void setStsmth05(Object what, boolean rounded) {
		if (rounded)
			stsmth05.setRounded(what);
		else
			stsmth05.set(what);
	}	
	public PackedDecimalData getStsmth06() {
		return stsmth06;
	}
	public void setStsmth06(Object what) {
		setStsmth06(what, false);
	}
	public void setStsmth06(Object what, boolean rounded) {
		if (rounded)
			stsmth06.setRounded(what);
		else
			stsmth06.set(what);
	}	
	public PackedDecimalData getStsmth07() {
		return stsmth07;
	}
	public void setStsmth07(Object what) {
		setStsmth07(what, false);
	}
	public void setStsmth07(Object what, boolean rounded) {
		if (rounded)
			stsmth07.setRounded(what);
		else
			stsmth07.set(what);
	}	
	public PackedDecimalData getStsmth08() {
		return stsmth08;
	}
	public void setStsmth08(Object what) {
		setStsmth08(what, false);
	}
	public void setStsmth08(Object what, boolean rounded) {
		if (rounded)
			stsmth08.setRounded(what);
		else
			stsmth08.set(what);
	}	
	public PackedDecimalData getStsmth09() {
		return stsmth09;
	}
	public void setStsmth09(Object what) {
		setStsmth09(what, false);
	}
	public void setStsmth09(Object what, boolean rounded) {
		if (rounded)
			stsmth09.setRounded(what);
		else
			stsmth09.set(what);
	}	
	public PackedDecimalData getStsmth10() {
		return stsmth10;
	}
	public void setStsmth10(Object what) {
		setStsmth10(what, false);
	}
	public void setStsmth10(Object what, boolean rounded) {
		if (rounded)
			stsmth10.setRounded(what);
		else
			stsmth10.set(what);
	}	
	public PackedDecimalData getStsmth11() {
		return stsmth11;
	}
	public void setStsmth11(Object what) {
		setStsmth11(what, false);
	}
	public void setStsmth11(Object what, boolean rounded) {
		if (rounded)
			stsmth11.setRounded(what);
		else
			stsmth11.set(what);
	}	
	public PackedDecimalData getStsmth12() {
		return stsmth12;
	}
	public void setStsmth12(Object what) {
		setStsmth12(what, false);
	}
	public void setStsmth12(Object what, boolean rounded) {
		if (rounded)
			stsmth12.setRounded(what);
		else
			stsmth12.set(what);
	}	
	public PackedDecimalData getCfwds() {
		return cfwds;
	}
	public void setCfwds(Object what) {
		setCfwds(what, false);
	}
	public void setCfwds(Object what, boolean rounded) {
		if (rounded)
			cfwds.setRounded(what);
		else
			cfwds.set(what);
	}	
	public PackedDecimalData getBfwdi() {
		return bfwdi;
	}
	public void setBfwdi(Object what) {
		setBfwdi(what, false);
	}
	public void setBfwdi(Object what, boolean rounded) {
		if (rounded)
			bfwdi.setRounded(what);
		else
			bfwdi.set(what);
	}	
	public PackedDecimalData getStimth01() {
		return stimth01;
	}
	public void setStimth01(Object what) {
		setStimth01(what, false);
	}
	public void setStimth01(Object what, boolean rounded) {
		if (rounded)
			stimth01.setRounded(what);
		else
			stimth01.set(what);
	}	
	public PackedDecimalData getStimth02() {
		return stimth02;
	}
	public void setStimth02(Object what) {
		setStimth02(what, false);
	}
	public void setStimth02(Object what, boolean rounded) {
		if (rounded)
			stimth02.setRounded(what);
		else
			stimth02.set(what);
	}	
	public PackedDecimalData getStimth03() {
		return stimth03;
	}
	public void setStimth03(Object what) {
		setStimth03(what, false);
	}
	public void setStimth03(Object what, boolean rounded) {
		if (rounded)
			stimth03.setRounded(what);
		else
			stimth03.set(what);
	}	
	public PackedDecimalData getStimth04() {
		return stimth04;
	}
	public void setStimth04(Object what) {
		setStimth04(what, false);
	}
	public void setStimth04(Object what, boolean rounded) {
		if (rounded)
			stimth04.setRounded(what);
		else
			stimth04.set(what);
	}	
	public PackedDecimalData getStimth05() {
		return stimth05;
	}
	public void setStimth05(Object what) {
		setStimth05(what, false);
	}
	public void setStimth05(Object what, boolean rounded) {
		if (rounded)
			stimth05.setRounded(what);
		else
			stimth05.set(what);
	}	
	public PackedDecimalData getStimth06() {
		return stimth06;
	}
	public void setStimth06(Object what) {
		setStimth06(what, false);
	}
	public void setStimth06(Object what, boolean rounded) {
		if (rounded)
			stimth06.setRounded(what);
		else
			stimth06.set(what);
	}	
	public PackedDecimalData getStimth07() {
		return stimth07;
	}
	public void setStimth07(Object what) {
		setStimth07(what, false);
	}
	public void setStimth07(Object what, boolean rounded) {
		if (rounded)
			stimth07.setRounded(what);
		else
			stimth07.set(what);
	}	
	public PackedDecimalData getStimth08() {
		return stimth08;
	}
	public void setStimth08(Object what) {
		setStimth08(what, false);
	}
	public void setStimth08(Object what, boolean rounded) {
		if (rounded)
			stimth08.setRounded(what);
		else
			stimth08.set(what);
	}	
	public PackedDecimalData getStimth09() {
		return stimth09;
	}
	public void setStimth09(Object what) {
		setStimth09(what, false);
	}
	public void setStimth09(Object what, boolean rounded) {
		if (rounded)
			stimth09.setRounded(what);
		else
			stimth09.set(what);
	}	
	public PackedDecimalData getStimth10() {
		return stimth10;
	}
	public void setStimth10(Object what) {
		setStimth10(what, false);
	}
	public void setStimth10(Object what, boolean rounded) {
		if (rounded)
			stimth10.setRounded(what);
		else
			stimth10.set(what);
	}	
	public PackedDecimalData getStimth11() {
		return stimth11;
	}
	public void setStimth11(Object what) {
		setStimth11(what, false);
	}
	public void setStimth11(Object what, boolean rounded) {
		if (rounded)
			stimth11.setRounded(what);
		else
			stimth11.set(what);
	}	
	public PackedDecimalData getStimth12() {
		return stimth12;
	}
	public void setStimth12(Object what) {
		setStimth12(what, false);
	}
	public void setStimth12(Object what, boolean rounded) {
		if (rounded)
			stimth12.setRounded(what);
		else
			stimth12.set(what);
	}	
	public PackedDecimalData getCfwdi() {
		return cfwdi;
	}
	public void setCfwdi(Object what) {
		setCfwdi(what, false);
	}
	public void setCfwdi(Object what, boolean rounded) {
		if (rounded)
			cfwdi.setRounded(what);
		else
			cfwdi.set(what);
	}	
	public PackedDecimalData getBfwdl() {
		return bfwdl;
	}
	public void setBfwdl(Object what) {
		setBfwdl(what, false);
	}
	public void setBfwdl(Object what, boolean rounded) {
		if (rounded)
			bfwdl.setRounded(what);
		else
			bfwdl.set(what);
	}	
	public PackedDecimalData getStlmth01() {
		return stlmth01;
	}
	public void setStlmth01(Object what) {
		setStlmth01(what, false);
	}
	public void setStlmth01(Object what, boolean rounded) {
		if (rounded)
			stlmth01.setRounded(what);
		else
			stlmth01.set(what);
	}	
	public PackedDecimalData getStlmth02() {
		return stlmth02;
	}
	public void setStlmth02(Object what) {
		setStlmth02(what, false);
	}
	public void setStlmth02(Object what, boolean rounded) {
		if (rounded)
			stlmth02.setRounded(what);
		else
			stlmth02.set(what);
	}	
	public PackedDecimalData getStlmth03() {
		return stlmth03;
	}
	public void setStlmth03(Object what) {
		setStlmth03(what, false);
	}
	public void setStlmth03(Object what, boolean rounded) {
		if (rounded)
			stlmth03.setRounded(what);
		else
			stlmth03.set(what);
	}	
	public PackedDecimalData getStlmth04() {
		return stlmth04;
	}
	public void setStlmth04(Object what) {
		setStlmth04(what, false);
	}
	public void setStlmth04(Object what, boolean rounded) {
		if (rounded)
			stlmth04.setRounded(what);
		else
			stlmth04.set(what);
	}	
	public PackedDecimalData getStlmth05() {
		return stlmth05;
	}
	public void setStlmth05(Object what) {
		setStlmth05(what, false);
	}
	public void setStlmth05(Object what, boolean rounded) {
		if (rounded)
			stlmth05.setRounded(what);
		else
			stlmth05.set(what);
	}	
	public PackedDecimalData getStlmth06() {
		return stlmth06;
	}
	public void setStlmth06(Object what) {
		setStlmth06(what, false);
	}
	public void setStlmth06(Object what, boolean rounded) {
		if (rounded)
			stlmth06.setRounded(what);
		else
			stlmth06.set(what);
	}	
	public PackedDecimalData getStlmth07() {
		return stlmth07;
	}
	public void setStlmth07(Object what) {
		setStlmth07(what, false);
	}
	public void setStlmth07(Object what, boolean rounded) {
		if (rounded)
			stlmth07.setRounded(what);
		else
			stlmth07.set(what);
	}	
	public PackedDecimalData getStlmth08() {
		return stlmth08;
	}
	public void setStlmth08(Object what) {
		setStlmth08(what, false);
	}
	public void setStlmth08(Object what, boolean rounded) {
		if (rounded)
			stlmth08.setRounded(what);
		else
			stlmth08.set(what);
	}	
	public PackedDecimalData getStlmth09() {
		return stlmth09;
	}
	public void setStlmth09(Object what) {
		setStlmth09(what, false);
	}
	public void setStlmth09(Object what, boolean rounded) {
		if (rounded)
			stlmth09.setRounded(what);
		else
			stlmth09.set(what);
	}	
	public PackedDecimalData getStlmth10() {
		return stlmth10;
	}
	public void setStlmth10(Object what) {
		setStlmth10(what, false);
	}
	public void setStlmth10(Object what, boolean rounded) {
		if (rounded)
			stlmth10.setRounded(what);
		else
			stlmth10.set(what);
	}	
	public PackedDecimalData getStlmth11() {
		return stlmth11;
	}
	public void setStlmth11(Object what) {
		setStlmth11(what, false);
	}
	public void setStlmth11(Object what, boolean rounded) {
		if (rounded)
			stlmth11.setRounded(what);
		else
			stlmth11.set(what);
	}	
	public PackedDecimalData getStlmth12() {
		return stlmth12;
	}
	public void setStlmth12(Object what) {
		setStlmth12(what, false);
	}
	public void setStlmth12(Object what, boolean rounded) {
		if (rounded)
			stlmth12.setRounded(what);
		else
			stlmth12.set(what);
	}	
	public PackedDecimalData getCfwdl() {
		return cfwdl;
	}
	public void setCfwdl(Object what) {
		setCfwdl(what, false);
	}
	public void setCfwdl(Object what, boolean rounded) {
		if (rounded)
			cfwdl.setRounded(what);
		else
			cfwdl.set(what);
	}	
	public PackedDecimalData getBfwda() {
		return bfwda;
	}
	public void setBfwda(Object what) {
		setBfwda(what, false);
	}
	public void setBfwda(Object what, boolean rounded) {
		if (rounded)
			bfwda.setRounded(what);
		else
			bfwda.set(what);
	}	
	public PackedDecimalData getStamth01() {
		return stamth01;
	}
	public void setStamth01(Object what) {
		setStamth01(what, false);
	}
	public void setStamth01(Object what, boolean rounded) {
		if (rounded)
			stamth01.setRounded(what);
		else
			stamth01.set(what);
	}	
	public PackedDecimalData getStamth02() {
		return stamth02;
	}
	public void setStamth02(Object what) {
		setStamth02(what, false);
	}
	public void setStamth02(Object what, boolean rounded) {
		if (rounded)
			stamth02.setRounded(what);
		else
			stamth02.set(what);
	}	
	public PackedDecimalData getStamth03() {
		return stamth03;
	}
	public void setStamth03(Object what) {
		setStamth03(what, false);
	}
	public void setStamth03(Object what, boolean rounded) {
		if (rounded)
			stamth03.setRounded(what);
		else
			stamth03.set(what);
	}	
	public PackedDecimalData getStamth04() {
		return stamth04;
	}
	public void setStamth04(Object what) {
		setStamth04(what, false);
	}
	public void setStamth04(Object what, boolean rounded) {
		if (rounded)
			stamth04.setRounded(what);
		else
			stamth04.set(what);
	}	
	public PackedDecimalData getStamth05() {
		return stamth05;
	}
	public void setStamth05(Object what) {
		setStamth05(what, false);
	}
	public void setStamth05(Object what, boolean rounded) {
		if (rounded)
			stamth05.setRounded(what);
		else
			stamth05.set(what);
	}	
	public PackedDecimalData getStamth06() {
		return stamth06;
	}
	public void setStamth06(Object what) {
		setStamth06(what, false);
	}
	public void setStamth06(Object what, boolean rounded) {
		if (rounded)
			stamth06.setRounded(what);
		else
			stamth06.set(what);
	}	
	public PackedDecimalData getStamth07() {
		return stamth07;
	}
	public void setStamth07(Object what) {
		setStamth07(what, false);
	}
	public void setStamth07(Object what, boolean rounded) {
		if (rounded)
			stamth07.setRounded(what);
		else
			stamth07.set(what);
	}	
	public PackedDecimalData getStamth08() {
		return stamth08;
	}
	public void setStamth08(Object what) {
		setStamth08(what, false);
	}
	public void setStamth08(Object what, boolean rounded) {
		if (rounded)
			stamth08.setRounded(what);
		else
			stamth08.set(what);
	}	
	public PackedDecimalData getStamth09() {
		return stamth09;
	}
	public void setStamth09(Object what) {
		setStamth09(what, false);
	}
	public void setStamth09(Object what, boolean rounded) {
		if (rounded)
			stamth09.setRounded(what);
		else
			stamth09.set(what);
	}	
	public PackedDecimalData getStamth10() {
		return stamth10;
	}
	public void setStamth10(Object what) {
		setStamth10(what, false);
	}
	public void setStamth10(Object what, boolean rounded) {
		if (rounded)
			stamth10.setRounded(what);
		else
			stamth10.set(what);
	}	
	public PackedDecimalData getStamth11() {
		return stamth11;
	}
	public void setStamth11(Object what) {
		setStamth11(what, false);
	}
	public void setStamth11(Object what, boolean rounded) {
		if (rounded)
			stamth11.setRounded(what);
		else
			stamth11.set(what);
	}	
	public PackedDecimalData getStamth12() {
		return stamth12;
	}
	public void setStamth12(Object what) {
		setStamth12(what, false);
	}
	public void setStamth12(Object what, boolean rounded) {
		if (rounded)
			stamth12.setRounded(what);
		else
			stamth12.set(what);
	}	
	public PackedDecimalData getCfwda() {
		return cfwda;
	}
	public void setCfwda(Object what) {
		setCfwda(what, false);
	}
	public void setCfwda(Object what, boolean rounded) {
		if (rounded)
			cfwda.setRounded(what);
		else
			cfwda.set(what);
	}	
	public PackedDecimalData getBfwdb() {
		return bfwdb;
	}
	public void setBfwdb(Object what) {
		setBfwdb(what, false);
	}
	public void setBfwdb(Object what, boolean rounded) {
		if (rounded)
			bfwdb.setRounded(what);
		else
			bfwdb.set(what);
	}	
	public PackedDecimalData getStbmthg01() {
		return stbmthg01;
	}
	public void setStbmthg01(Object what) {
		setStbmthg01(what, false);
	}
	public void setStbmthg01(Object what, boolean rounded) {
		if (rounded)
			stbmthg01.setRounded(what);
		else
			stbmthg01.set(what);
	}	
	public PackedDecimalData getStbmthg02() {
		return stbmthg02;
	}
	public void setStbmthg02(Object what) {
		setStbmthg02(what, false);
	}
	public void setStbmthg02(Object what, boolean rounded) {
		if (rounded)
			stbmthg02.setRounded(what);
		else
			stbmthg02.set(what);
	}	
	public PackedDecimalData getStbmthg03() {
		return stbmthg03;
	}
	public void setStbmthg03(Object what) {
		setStbmthg03(what, false);
	}
	public void setStbmthg03(Object what, boolean rounded) {
		if (rounded)
			stbmthg03.setRounded(what);
		else
			stbmthg03.set(what);
	}	
	public PackedDecimalData getStbmthg04() {
		return stbmthg04;
	}
	public void setStbmthg04(Object what) {
		setStbmthg04(what, false);
	}
	public void setStbmthg04(Object what, boolean rounded) {
		if (rounded)
			stbmthg04.setRounded(what);
		else
			stbmthg04.set(what);
	}	
	public PackedDecimalData getStbmthg05() {
		return stbmthg05;
	}
	public void setStbmthg05(Object what) {
		setStbmthg05(what, false);
	}
	public void setStbmthg05(Object what, boolean rounded) {
		if (rounded)
			stbmthg05.setRounded(what);
		else
			stbmthg05.set(what);
	}	
	public PackedDecimalData getStbmthg06() {
		return stbmthg06;
	}
	public void setStbmthg06(Object what) {
		setStbmthg06(what, false);
	}
	public void setStbmthg06(Object what, boolean rounded) {
		if (rounded)
			stbmthg06.setRounded(what);
		else
			stbmthg06.set(what);
	}	
	public PackedDecimalData getStbmthg07() {
		return stbmthg07;
	}
	public void setStbmthg07(Object what) {
		setStbmthg07(what, false);
	}
	public void setStbmthg07(Object what, boolean rounded) {
		if (rounded)
			stbmthg07.setRounded(what);
		else
			stbmthg07.set(what);
	}	
	public PackedDecimalData getStbmthg08() {
		return stbmthg08;
	}
	public void setStbmthg08(Object what) {
		setStbmthg08(what, false);
	}
	public void setStbmthg08(Object what, boolean rounded) {
		if (rounded)
			stbmthg08.setRounded(what);
		else
			stbmthg08.set(what);
	}	
	public PackedDecimalData getStbmthg09() {
		return stbmthg09;
	}
	public void setStbmthg09(Object what) {
		setStbmthg09(what, false);
	}
	public void setStbmthg09(Object what, boolean rounded) {
		if (rounded)
			stbmthg09.setRounded(what);
		else
			stbmthg09.set(what);
	}	
	public PackedDecimalData getStbmthg10() {
		return stbmthg10;
	}
	public void setStbmthg10(Object what) {
		setStbmthg10(what, false);
	}
	public void setStbmthg10(Object what, boolean rounded) {
		if (rounded)
			stbmthg10.setRounded(what);
		else
			stbmthg10.set(what);
	}	
	public PackedDecimalData getStbmthg11() {
		return stbmthg11;
	}
	public void setStbmthg11(Object what) {
		setStbmthg11(what, false);
	}
	public void setStbmthg11(Object what, boolean rounded) {
		if (rounded)
			stbmthg11.setRounded(what);
		else
			stbmthg11.set(what);
	}	
	public PackedDecimalData getStbmthg12() {
		return stbmthg12;
	}
	public void setStbmthg12(Object what) {
		setStbmthg12(what, false);
	}
	public void setStbmthg12(Object what, boolean rounded) {
		if (rounded)
			stbmthg12.setRounded(what);
		else
			stbmthg12.set(what);
	}	
	public PackedDecimalData getCfwdb() {
		return cfwdb;
	}
	public void setCfwdb(Object what) {
		setCfwdb(what, false);
	}
	public void setCfwdb(Object what, boolean rounded) {
		if (rounded)
			cfwdb.setRounded(what);
		else
			cfwdb.set(what);
	}	
	public PackedDecimalData getBfwdld() {
		return bfwdld;
	}
	public void setBfwdld(Object what) {
		setBfwdld(what, false);
	}
	public void setBfwdld(Object what, boolean rounded) {
		if (rounded)
			bfwdld.setRounded(what);
		else
			bfwdld.set(what);
	}	
	public PackedDecimalData getStldmthg01() {
		return stldmthg01;
	}
	public void setStldmthg01(Object what) {
		setStldmthg01(what, false);
	}
	public void setStldmthg01(Object what, boolean rounded) {
		if (rounded)
			stldmthg01.setRounded(what);
		else
			stldmthg01.set(what);
	}	
	public PackedDecimalData getStldmthg02() {
		return stldmthg02;
	}
	public void setStldmthg02(Object what) {
		setStldmthg02(what, false);
	}
	public void setStldmthg02(Object what, boolean rounded) {
		if (rounded)
			stldmthg02.setRounded(what);
		else
			stldmthg02.set(what);
	}	
	public PackedDecimalData getStldmthg03() {
		return stldmthg03;
	}
	public void setStldmthg03(Object what) {
		setStldmthg03(what, false);
	}
	public void setStldmthg03(Object what, boolean rounded) {
		if (rounded)
			stldmthg03.setRounded(what);
		else
			stldmthg03.set(what);
	}	
	public PackedDecimalData getStldmthg04() {
		return stldmthg04;
	}
	public void setStldmthg04(Object what) {
		setStldmthg04(what, false);
	}
	public void setStldmthg04(Object what, boolean rounded) {
		if (rounded)
			stldmthg04.setRounded(what);
		else
			stldmthg04.set(what);
	}	
	public PackedDecimalData getStldmthg05() {
		return stldmthg05;
	}
	public void setStldmthg05(Object what) {
		setStldmthg05(what, false);
	}
	public void setStldmthg05(Object what, boolean rounded) {
		if (rounded)
			stldmthg05.setRounded(what);
		else
			stldmthg05.set(what);
	}	
	public PackedDecimalData getStldmthg06() {
		return stldmthg06;
	}
	public void setStldmthg06(Object what) {
		setStldmthg06(what, false);
	}
	public void setStldmthg06(Object what, boolean rounded) {
		if (rounded)
			stldmthg06.setRounded(what);
		else
			stldmthg06.set(what);
	}	
	public PackedDecimalData getStldmthg07() {
		return stldmthg07;
	}
	public void setStldmthg07(Object what) {
		setStldmthg07(what, false);
	}
	public void setStldmthg07(Object what, boolean rounded) {
		if (rounded)
			stldmthg07.setRounded(what);
		else
			stldmthg07.set(what);
	}	
	public PackedDecimalData getStldmthg08() {
		return stldmthg08;
	}
	public void setStldmthg08(Object what) {
		setStldmthg08(what, false);
	}
	public void setStldmthg08(Object what, boolean rounded) {
		if (rounded)
			stldmthg08.setRounded(what);
		else
			stldmthg08.set(what);
	}	
	public PackedDecimalData getStldmthg09() {
		return stldmthg09;
	}
	public void setStldmthg09(Object what) {
		setStldmthg09(what, false);
	}
	public void setStldmthg09(Object what, boolean rounded) {
		if (rounded)
			stldmthg09.setRounded(what);
		else
			stldmthg09.set(what);
	}	
	public PackedDecimalData getStldmthg10() {
		return stldmthg10;
	}
	public void setStldmthg10(Object what) {
		setStldmthg10(what, false);
	}
	public void setStldmthg10(Object what, boolean rounded) {
		if (rounded)
			stldmthg10.setRounded(what);
		else
			stldmthg10.set(what);
	}	
	public PackedDecimalData getStldmthg11() {
		return stldmthg11;
	}
	public void setStldmthg11(Object what) {
		setStldmthg11(what, false);
	}
	public void setStldmthg11(Object what, boolean rounded) {
		if (rounded)
			stldmthg11.setRounded(what);
		else
			stldmthg11.set(what);
	}	
	public PackedDecimalData getStldmthg12() {
		return stldmthg12;
	}
	public void setStldmthg12(Object what) {
		setStldmthg12(what, false);
	}
	public void setStldmthg12(Object what, boolean rounded) {
		if (rounded)
			stldmthg12.setRounded(what);
		else
			stldmthg12.set(what);
	}	
	public PackedDecimalData getCfwdld() {
		return cfwdld;
	}
	public void setCfwdld(Object what) {
		setCfwdld(what, false);
	}
	public void setCfwdld(Object what, boolean rounded) {
		if (rounded)
			cfwdld.setRounded(what);
		else
			cfwdld.set(what);
	}	
	public PackedDecimalData getBfwdra() {
		return bfwdra;
	}
	public void setBfwdra(Object what) {
		setBfwdra(what, false);
	}
	public void setBfwdra(Object what, boolean rounded) {
		if (rounded)
			bfwdra.setRounded(what);
		else
			bfwdra.set(what);
	}	
	public PackedDecimalData getStraamt01() {
		return straamt01;
	}
	public void setStraamt01(Object what) {
		setStraamt01(what, false);
	}
	public void setStraamt01(Object what, boolean rounded) {
		if (rounded)
			straamt01.setRounded(what);
		else
			straamt01.set(what);
	}	
	public PackedDecimalData getStraamt02() {
		return straamt02;
	}
	public void setStraamt02(Object what) {
		setStraamt02(what, false);
	}
	public void setStraamt02(Object what, boolean rounded) {
		if (rounded)
			straamt02.setRounded(what);
		else
			straamt02.set(what);
	}	
	public PackedDecimalData getStraamt03() {
		return straamt03;
	}
	public void setStraamt03(Object what) {
		setStraamt03(what, false);
	}
	public void setStraamt03(Object what, boolean rounded) {
		if (rounded)
			straamt03.setRounded(what);
		else
			straamt03.set(what);
	}	
	public PackedDecimalData getStraamt04() {
		return straamt04;
	}
	public void setStraamt04(Object what) {
		setStraamt04(what, false);
	}
	public void setStraamt04(Object what, boolean rounded) {
		if (rounded)
			straamt04.setRounded(what);
		else
			straamt04.set(what);
	}	
	public PackedDecimalData getStraamt05() {
		return straamt05;
	}
	public void setStraamt05(Object what) {
		setStraamt05(what, false);
	}
	public void setStraamt05(Object what, boolean rounded) {
		if (rounded)
			straamt05.setRounded(what);
		else
			straamt05.set(what);
	}	
	public PackedDecimalData getStraamt06() {
		return straamt06;
	}
	public void setStraamt06(Object what) {
		setStraamt06(what, false);
	}
	public void setStraamt06(Object what, boolean rounded) {
		if (rounded)
			straamt06.setRounded(what);
		else
			straamt06.set(what);
	}	
	public PackedDecimalData getStraamt07() {
		return straamt07;
	}
	public void setStraamt07(Object what) {
		setStraamt07(what, false);
	}
	public void setStraamt07(Object what, boolean rounded) {
		if (rounded)
			straamt07.setRounded(what);
		else
			straamt07.set(what);
	}	
	public PackedDecimalData getStraamt08() {
		return straamt08;
	}
	public void setStraamt08(Object what) {
		setStraamt08(what, false);
	}
	public void setStraamt08(Object what, boolean rounded) {
		if (rounded)
			straamt08.setRounded(what);
		else
			straamt08.set(what);
	}	
	public PackedDecimalData getStraamt09() {
		return straamt09;
	}
	public void setStraamt09(Object what) {
		setStraamt09(what, false);
	}
	public void setStraamt09(Object what, boolean rounded) {
		if (rounded)
			straamt09.setRounded(what);
		else
			straamt09.set(what);
	}	
	public PackedDecimalData getStraamt10() {
		return straamt10;
	}
	public void setStraamt10(Object what) {
		setStraamt10(what, false);
	}
	public void setStraamt10(Object what, boolean rounded) {
		if (rounded)
			straamt10.setRounded(what);
		else
			straamt10.set(what);
	}	
	public PackedDecimalData getStraamt11() {
		return straamt11;
	}
	public void setStraamt11(Object what) {
		setStraamt11(what, false);
	}
	public void setStraamt11(Object what, boolean rounded) {
		if (rounded)
			straamt11.setRounded(what);
		else
			straamt11.set(what);
	}	
	public PackedDecimalData getStraamt12() {
		return straamt12;
	}
	public void setStraamt12(Object what) {
		setStraamt12(what, false);
	}
	public void setStraamt12(Object what, boolean rounded) {
		if (rounded)
			straamt12.setRounded(what);
		else
			straamt12.set(what);
	}	
	public PackedDecimalData getCfwdra() {
		return cfwdra;
	}
	public void setCfwdra(Object what) {
		setCfwdra(what, false);
	}
	public void setCfwdra(Object what, boolean rounded) {
		if (rounded)
			cfwdra.setRounded(what);
		else
			cfwdra.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getStsmths() {
		return new FixedLengthStringData(stsmth01.toInternal()
										+ stsmth02.toInternal()
										+ stsmth03.toInternal()
										+ stsmth04.toInternal()
										+ stsmth05.toInternal()
										+ stsmth06.toInternal()
										+ stsmth07.toInternal()
										+ stsmth08.toInternal()
										+ stsmth09.toInternal()
										+ stsmth10.toInternal()
										+ stsmth11.toInternal()
										+ stsmth12.toInternal());
	}
	public void setStsmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStsmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stsmth01);
		what = ExternalData.chop(what, stsmth02);
		what = ExternalData.chop(what, stsmth03);
		what = ExternalData.chop(what, stsmth04);
		what = ExternalData.chop(what, stsmth05);
		what = ExternalData.chop(what, stsmth06);
		what = ExternalData.chop(what, stsmth07);
		what = ExternalData.chop(what, stsmth08);
		what = ExternalData.chop(what, stsmth09);
		what = ExternalData.chop(what, stsmth10);
		what = ExternalData.chop(what, stsmth11);
		what = ExternalData.chop(what, stsmth12);
	}
	public PackedDecimalData getStsmth(BaseData indx) {
		return getStsmth(indx.toInt());
	}
	public PackedDecimalData getStsmth(int indx) {

		switch (indx) {
			case 1 : return stsmth01;
			case 2 : return stsmth02;
			case 3 : return stsmth03;
			case 4 : return stsmth04;
			case 5 : return stsmth05;
			case 6 : return stsmth06;
			case 7 : return stsmth07;
			case 8 : return stsmth08;
			case 9 : return stsmth09;
			case 10 : return stsmth10;
			case 11 : return stsmth11;
			case 12 : return stsmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStsmth(BaseData indx, Object what) {
		setStsmth(indx, what, false);
	}
	public void setStsmth(BaseData indx, Object what, boolean rounded) {
		setStsmth(indx.toInt(), what, rounded);
	}
	public void setStsmth(int indx, Object what) {
		setStsmth(indx, what, false);
	}
	public void setStsmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStsmth01(what, rounded);
					 break;
			case 2 : setStsmth02(what, rounded);
					 break;
			case 3 : setStsmth03(what, rounded);
					 break;
			case 4 : setStsmth04(what, rounded);
					 break;
			case 5 : setStsmth05(what, rounded);
					 break;
			case 6 : setStsmth06(what, rounded);
					 break;
			case 7 : setStsmth07(what, rounded);
					 break;
			case 8 : setStsmth08(what, rounded);
					 break;
			case 9 : setStsmth09(what, rounded);
					 break;
			case 10 : setStsmth10(what, rounded);
					 break;
			case 11 : setStsmth11(what, rounded);
					 break;
			case 12 : setStsmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStraamts() {
		return new FixedLengthStringData(straamt01.toInternal()
										+ straamt02.toInternal()
										+ straamt03.toInternal()
										+ straamt04.toInternal()
										+ straamt05.toInternal()
										+ straamt06.toInternal()
										+ straamt07.toInternal()
										+ straamt08.toInternal()
										+ straamt09.toInternal()
										+ straamt10.toInternal()
										+ straamt11.toInternal()
										+ straamt12.toInternal());
	}
	public void setStraamts(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStraamts().getLength()).init(obj);
	
		what = ExternalData.chop(what, straamt01);
		what = ExternalData.chop(what, straamt02);
		what = ExternalData.chop(what, straamt03);
		what = ExternalData.chop(what, straamt04);
		what = ExternalData.chop(what, straamt05);
		what = ExternalData.chop(what, straamt06);
		what = ExternalData.chop(what, straamt07);
		what = ExternalData.chop(what, straamt08);
		what = ExternalData.chop(what, straamt09);
		what = ExternalData.chop(what, straamt10);
		what = ExternalData.chop(what, straamt11);
		what = ExternalData.chop(what, straamt12);
	}
	public PackedDecimalData getStraamt(BaseData indx) {
		return getStraamt(indx.toInt());
	}
	public PackedDecimalData getStraamt(int indx) {

		switch (indx) {
			case 1 : return straamt01;
			case 2 : return straamt02;
			case 3 : return straamt03;
			case 4 : return straamt04;
			case 5 : return straamt05;
			case 6 : return straamt06;
			case 7 : return straamt07;
			case 8 : return straamt08;
			case 9 : return straamt09;
			case 10 : return straamt10;
			case 11 : return straamt11;
			case 12 : return straamt12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStraamt(BaseData indx, Object what) {
		setStraamt(indx, what, false);
	}
	public void setStraamt(BaseData indx, Object what, boolean rounded) {
		setStraamt(indx.toInt(), what, rounded);
	}
	public void setStraamt(int indx, Object what) {
		setStraamt(indx, what, false);
	}
	public void setStraamt(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStraamt01(what, rounded);
					 break;
			case 2 : setStraamt02(what, rounded);
					 break;
			case 3 : setStraamt03(what, rounded);
					 break;
			case 4 : setStraamt04(what, rounded);
					 break;
			case 5 : setStraamt05(what, rounded);
					 break;
			case 6 : setStraamt06(what, rounded);
					 break;
			case 7 : setStraamt07(what, rounded);
					 break;
			case 8 : setStraamt08(what, rounded);
					 break;
			case 9 : setStraamt09(what, rounded);
					 break;
			case 10 : setStraamt10(what, rounded);
					 break;
			case 11 : setStraamt11(what, rounded);
					 break;
			case 12 : setStraamt12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStpmths() {
		return new FixedLengthStringData(stpmth01.toInternal()
										+ stpmth02.toInternal()
										+ stpmth03.toInternal()
										+ stpmth04.toInternal()
										+ stpmth05.toInternal()
										+ stpmth06.toInternal()
										+ stpmth07.toInternal()
										+ stpmth08.toInternal()
										+ stpmth09.toInternal()
										+ stpmth10.toInternal()
										+ stpmth11.toInternal()
										+ stpmth12.toInternal());
	}
	public void setStpmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStpmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stpmth01);
		what = ExternalData.chop(what, stpmth02);
		what = ExternalData.chop(what, stpmth03);
		what = ExternalData.chop(what, stpmth04);
		what = ExternalData.chop(what, stpmth05);
		what = ExternalData.chop(what, stpmth06);
		what = ExternalData.chop(what, stpmth07);
		what = ExternalData.chop(what, stpmth08);
		what = ExternalData.chop(what, stpmth09);
		what = ExternalData.chop(what, stpmth10);
		what = ExternalData.chop(what, stpmth11);
		what = ExternalData.chop(what, stpmth12);
	}
	public PackedDecimalData getStpmth(BaseData indx) {
		return getStpmth(indx.toInt());
	}
	public PackedDecimalData getStpmth(int indx) {

		switch (indx) {
			case 1 : return stpmth01;
			case 2 : return stpmth02;
			case 3 : return stpmth03;
			case 4 : return stpmth04;
			case 5 : return stpmth05;
			case 6 : return stpmth06;
			case 7 : return stpmth07;
			case 8 : return stpmth08;
			case 9 : return stpmth09;
			case 10 : return stpmth10;
			case 11 : return stpmth11;
			case 12 : return stpmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStpmth(BaseData indx, Object what) {
		setStpmth(indx, what, false);
	}
	public void setStpmth(BaseData indx, Object what, boolean rounded) {
		setStpmth(indx.toInt(), what, rounded);
	}
	public void setStpmth(int indx, Object what) {
		setStpmth(indx, what, false);
	}
	public void setStpmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStpmth01(what, rounded);
					 break;
			case 2 : setStpmth02(what, rounded);
					 break;
			case 3 : setStpmth03(what, rounded);
					 break;
			case 4 : setStpmth04(what, rounded);
					 break;
			case 5 : setStpmth05(what, rounded);
					 break;
			case 6 : setStpmth06(what, rounded);
					 break;
			case 7 : setStpmth07(what, rounded);
					 break;
			case 8 : setStpmth08(what, rounded);
					 break;
			case 9 : setStpmth09(what, rounded);
					 break;
			case 10 : setStpmth10(what, rounded);
					 break;
			case 11 : setStpmth11(what, rounded);
					 break;
			case 12 : setStpmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStlmths() {
		return new FixedLengthStringData(stlmth01.toInternal()
										+ stlmth02.toInternal()
										+ stlmth03.toInternal()
										+ stlmth04.toInternal()
										+ stlmth05.toInternal()
										+ stlmth06.toInternal()
										+ stlmth07.toInternal()
										+ stlmth08.toInternal()
										+ stlmth09.toInternal()
										+ stlmth10.toInternal()
										+ stlmth11.toInternal()
										+ stlmth12.toInternal());
	}
	public void setStlmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStlmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stlmth01);
		what = ExternalData.chop(what, stlmth02);
		what = ExternalData.chop(what, stlmth03);
		what = ExternalData.chop(what, stlmth04);
		what = ExternalData.chop(what, stlmth05);
		what = ExternalData.chop(what, stlmth06);
		what = ExternalData.chop(what, stlmth07);
		what = ExternalData.chop(what, stlmth08);
		what = ExternalData.chop(what, stlmth09);
		what = ExternalData.chop(what, stlmth10);
		what = ExternalData.chop(what, stlmth11);
		what = ExternalData.chop(what, stlmth12);
	}
	public PackedDecimalData getStlmth(BaseData indx) {
		return getStlmth(indx.toInt());
	}
	public PackedDecimalData getStlmth(int indx) {

		switch (indx) {
			case 1 : return stlmth01;
			case 2 : return stlmth02;
			case 3 : return stlmth03;
			case 4 : return stlmth04;
			case 5 : return stlmth05;
			case 6 : return stlmth06;
			case 7 : return stlmth07;
			case 8 : return stlmth08;
			case 9 : return stlmth09;
			case 10 : return stlmth10;
			case 11 : return stlmth11;
			case 12 : return stlmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStlmth(BaseData indx, Object what) {
		setStlmth(indx, what, false);
	}
	public void setStlmth(BaseData indx, Object what, boolean rounded) {
		setStlmth(indx.toInt(), what, rounded);
	}
	public void setStlmth(int indx, Object what) {
		setStlmth(indx, what, false);
	}
	public void setStlmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStlmth01(what, rounded);
					 break;
			case 2 : setStlmth02(what, rounded);
					 break;
			case 3 : setStlmth03(what, rounded);
					 break;
			case 4 : setStlmth04(what, rounded);
					 break;
			case 5 : setStlmth05(what, rounded);
					 break;
			case 6 : setStlmth06(what, rounded);
					 break;
			case 7 : setStlmth07(what, rounded);
					 break;
			case 8 : setStlmth08(what, rounded);
					 break;
			case 9 : setStlmth09(what, rounded);
					 break;
			case 10 : setStlmth10(what, rounded);
					 break;
			case 11 : setStlmth11(what, rounded);
					 break;
			case 12 : setStlmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStldmthgs() {
		return new FixedLengthStringData(stldmthg01.toInternal()
										+ stldmthg02.toInternal()
										+ stldmthg03.toInternal()
										+ stldmthg04.toInternal()
										+ stldmthg05.toInternal()
										+ stldmthg06.toInternal()
										+ stldmthg07.toInternal()
										+ stldmthg08.toInternal()
										+ stldmthg09.toInternal()
										+ stldmthg10.toInternal()
										+ stldmthg11.toInternal()
										+ stldmthg12.toInternal());
	}
	public void setStldmthgs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStldmthgs().getLength()).init(obj);
	
		what = ExternalData.chop(what, stldmthg01);
		what = ExternalData.chop(what, stldmthg02);
		what = ExternalData.chop(what, stldmthg03);
		what = ExternalData.chop(what, stldmthg04);
		what = ExternalData.chop(what, stldmthg05);
		what = ExternalData.chop(what, stldmthg06);
		what = ExternalData.chop(what, stldmthg07);
		what = ExternalData.chop(what, stldmthg08);
		what = ExternalData.chop(what, stldmthg09);
		what = ExternalData.chop(what, stldmthg10);
		what = ExternalData.chop(what, stldmthg11);
		what = ExternalData.chop(what, stldmthg12);
	}
	public PackedDecimalData getStldmthg(BaseData indx) {
		return getStldmthg(indx.toInt());
	}
	public PackedDecimalData getStldmthg(int indx) {

		switch (indx) {
			case 1 : return stldmthg01;
			case 2 : return stldmthg02;
			case 3 : return stldmthg03;
			case 4 : return stldmthg04;
			case 5 : return stldmthg05;
			case 6 : return stldmthg06;
			case 7 : return stldmthg07;
			case 8 : return stldmthg08;
			case 9 : return stldmthg09;
			case 10 : return stldmthg10;
			case 11 : return stldmthg11;
			case 12 : return stldmthg12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStldmthg(BaseData indx, Object what) {
		setStldmthg(indx, what, false);
	}
	public void setStldmthg(BaseData indx, Object what, boolean rounded) {
		setStldmthg(indx.toInt(), what, rounded);
	}
	public void setStldmthg(int indx, Object what) {
		setStldmthg(indx, what, false);
	}
	public void setStldmthg(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStldmthg01(what, rounded);
					 break;
			case 2 : setStldmthg02(what, rounded);
					 break;
			case 3 : setStldmthg03(what, rounded);
					 break;
			case 4 : setStldmthg04(what, rounded);
					 break;
			case 5 : setStldmthg05(what, rounded);
					 break;
			case 6 : setStldmthg06(what, rounded);
					 break;
			case 7 : setStldmthg07(what, rounded);
					 break;
			case 8 : setStldmthg08(what, rounded);
					 break;
			case 9 : setStldmthg09(what, rounded);
					 break;
			case 10 : setStldmthg10(what, rounded);
					 break;
			case 11 : setStldmthg11(what, rounded);
					 break;
			case 12 : setStldmthg12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStimths() {
		return new FixedLengthStringData(stimth01.toInternal()
										+ stimth02.toInternal()
										+ stimth03.toInternal()
										+ stimth04.toInternal()
										+ stimth05.toInternal()
										+ stimth06.toInternal()
										+ stimth07.toInternal()
										+ stimth08.toInternal()
										+ stimth09.toInternal()
										+ stimth10.toInternal()
										+ stimth11.toInternal()
										+ stimth12.toInternal());
	}
	public void setStimths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStimths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stimth01);
		what = ExternalData.chop(what, stimth02);
		what = ExternalData.chop(what, stimth03);
		what = ExternalData.chop(what, stimth04);
		what = ExternalData.chop(what, stimth05);
		what = ExternalData.chop(what, stimth06);
		what = ExternalData.chop(what, stimth07);
		what = ExternalData.chop(what, stimth08);
		what = ExternalData.chop(what, stimth09);
		what = ExternalData.chop(what, stimth10);
		what = ExternalData.chop(what, stimth11);
		what = ExternalData.chop(what, stimth12);
	}
	public PackedDecimalData getStimth(BaseData indx) {
		return getStimth(indx.toInt());
	}
	public PackedDecimalData getStimth(int indx) {

		switch (indx) {
			case 1 : return stimth01;
			case 2 : return stimth02;
			case 3 : return stimth03;
			case 4 : return stimth04;
			case 5 : return stimth05;
			case 6 : return stimth06;
			case 7 : return stimth07;
			case 8 : return stimth08;
			case 9 : return stimth09;
			case 10 : return stimth10;
			case 11 : return stimth11;
			case 12 : return stimth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStimth(BaseData indx, Object what) {
		setStimth(indx, what, false);
	}
	public void setStimth(BaseData indx, Object what, boolean rounded) {
		setStimth(indx.toInt(), what, rounded);
	}
	public void setStimth(int indx, Object what) {
		setStimth(indx, what, false);
	}
	public void setStimth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStimth01(what, rounded);
					 break;
			case 2 : setStimth02(what, rounded);
					 break;
			case 3 : setStimth03(what, rounded);
					 break;
			case 4 : setStimth04(what, rounded);
					 break;
			case 5 : setStimth05(what, rounded);
					 break;
			case 6 : setStimth06(what, rounded);
					 break;
			case 7 : setStimth07(what, rounded);
					 break;
			case 8 : setStimth08(what, rounded);
					 break;
			case 9 : setStimth09(what, rounded);
					 break;
			case 10 : setStimth10(what, rounded);
					 break;
			case 11 : setStimth11(what, rounded);
					 break;
			case 12 : setStimth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStcmths() {
		return new FixedLengthStringData(stcmth01.toInternal()
										+ stcmth02.toInternal()
										+ stcmth03.toInternal()
										+ stcmth04.toInternal()
										+ stcmth05.toInternal()
										+ stcmth06.toInternal()
										+ stcmth07.toInternal()
										+ stcmth08.toInternal()
										+ stcmth09.toInternal()
										+ stcmth10.toInternal()
										+ stcmth11.toInternal()
										+ stcmth12.toInternal());
	}
	public void setStcmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStcmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stcmth01);
		what = ExternalData.chop(what, stcmth02);
		what = ExternalData.chop(what, stcmth03);
		what = ExternalData.chop(what, stcmth04);
		what = ExternalData.chop(what, stcmth05);
		what = ExternalData.chop(what, stcmth06);
		what = ExternalData.chop(what, stcmth07);
		what = ExternalData.chop(what, stcmth08);
		what = ExternalData.chop(what, stcmth09);
		what = ExternalData.chop(what, stcmth10);
		what = ExternalData.chop(what, stcmth11);
		what = ExternalData.chop(what, stcmth12);
	}
	public PackedDecimalData getStcmth(BaseData indx) {
		return getStcmth(indx.toInt());
	}
	public PackedDecimalData getStcmth(int indx) {

		switch (indx) {
			case 1 : return stcmth01;
			case 2 : return stcmth02;
			case 3 : return stcmth03;
			case 4 : return stcmth04;
			case 5 : return stcmth05;
			case 6 : return stcmth06;
			case 7 : return stcmth07;
			case 8 : return stcmth08;
			case 9 : return stcmth09;
			case 10 : return stcmth10;
			case 11 : return stcmth11;
			case 12 : return stcmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStcmth(BaseData indx, Object what) {
		setStcmth(indx, what, false);
	}
	public void setStcmth(BaseData indx, Object what, boolean rounded) {
		setStcmth(indx.toInt(), what, rounded);
	}
	public void setStcmth(int indx, Object what) {
		setStcmth(indx, what, false);
	}
	public void setStcmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStcmth01(what, rounded);
					 break;
			case 2 : setStcmth02(what, rounded);
					 break;
			case 3 : setStcmth03(what, rounded);
					 break;
			case 4 : setStcmth04(what, rounded);
					 break;
			case 5 : setStcmth05(what, rounded);
					 break;
			case 6 : setStcmth06(what, rounded);
					 break;
			case 7 : setStcmth07(what, rounded);
					 break;
			case 8 : setStcmth08(what, rounded);
					 break;
			case 9 : setStcmth09(what, rounded);
					 break;
			case 10 : setStcmth10(what, rounded);
					 break;
			case 11 : setStcmth11(what, rounded);
					 break;
			case 12 : setStcmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStbmthgs() {
		return new FixedLengthStringData(stbmthg01.toInternal()
										+ stbmthg02.toInternal()
										+ stbmthg03.toInternal()
										+ stbmthg04.toInternal()
										+ stbmthg05.toInternal()
										+ stbmthg06.toInternal()
										+ stbmthg07.toInternal()
										+ stbmthg08.toInternal()
										+ stbmthg09.toInternal()
										+ stbmthg10.toInternal()
										+ stbmthg11.toInternal()
										+ stbmthg12.toInternal());
	}
	public void setStbmthgs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStbmthgs().getLength()).init(obj);
	
		what = ExternalData.chop(what, stbmthg01);
		what = ExternalData.chop(what, stbmthg02);
		what = ExternalData.chop(what, stbmthg03);
		what = ExternalData.chop(what, stbmthg04);
		what = ExternalData.chop(what, stbmthg05);
		what = ExternalData.chop(what, stbmthg06);
		what = ExternalData.chop(what, stbmthg07);
		what = ExternalData.chop(what, stbmthg08);
		what = ExternalData.chop(what, stbmthg09);
		what = ExternalData.chop(what, stbmthg10);
		what = ExternalData.chop(what, stbmthg11);
		what = ExternalData.chop(what, stbmthg12);
	}
	public PackedDecimalData getStbmthg(BaseData indx) {
		return getStbmthg(indx.toInt());
	}
	public PackedDecimalData getStbmthg(int indx) {

		switch (indx) {
			case 1 : return stbmthg01;
			case 2 : return stbmthg02;
			case 3 : return stbmthg03;
			case 4 : return stbmthg04;
			case 5 : return stbmthg05;
			case 6 : return stbmthg06;
			case 7 : return stbmthg07;
			case 8 : return stbmthg08;
			case 9 : return stbmthg09;
			case 10 : return stbmthg10;
			case 11 : return stbmthg11;
			case 12 : return stbmthg12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStbmthg(BaseData indx, Object what) {
		setStbmthg(indx, what, false);
	}
	public void setStbmthg(BaseData indx, Object what, boolean rounded) {
		setStbmthg(indx.toInt(), what, rounded);
	}
	public void setStbmthg(int indx, Object what) {
		setStbmthg(indx, what, false);
	}
	public void setStbmthg(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStbmthg01(what, rounded);
					 break;
			case 2 : setStbmthg02(what, rounded);
					 break;
			case 3 : setStbmthg03(what, rounded);
					 break;
			case 4 : setStbmthg04(what, rounded);
					 break;
			case 5 : setStbmthg05(what, rounded);
					 break;
			case 6 : setStbmthg06(what, rounded);
					 break;
			case 7 : setStbmthg07(what, rounded);
					 break;
			case 8 : setStbmthg08(what, rounded);
					 break;
			case 9 : setStbmthg09(what, rounded);
					 break;
			case 10 : setStbmthg10(what, rounded);
					 break;
			case 11 : setStbmthg11(what, rounded);
					 break;
			case 12 : setStbmthg12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStamths() {
		return new FixedLengthStringData(stamth01.toInternal()
										+ stamth02.toInternal()
										+ stamth03.toInternal()
										+ stamth04.toInternal()
										+ stamth05.toInternal()
										+ stamth06.toInternal()
										+ stamth07.toInternal()
										+ stamth08.toInternal()
										+ stamth09.toInternal()
										+ stamth10.toInternal()
										+ stamth11.toInternal()
										+ stamth12.toInternal());
	}
	public void setStamths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStamths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stamth01);
		what = ExternalData.chop(what, stamth02);
		what = ExternalData.chop(what, stamth03);
		what = ExternalData.chop(what, stamth04);
		what = ExternalData.chop(what, stamth05);
		what = ExternalData.chop(what, stamth06);
		what = ExternalData.chop(what, stamth07);
		what = ExternalData.chop(what, stamth08);
		what = ExternalData.chop(what, stamth09);
		what = ExternalData.chop(what, stamth10);
		what = ExternalData.chop(what, stamth11);
		what = ExternalData.chop(what, stamth12);
	}
	public PackedDecimalData getStamth(BaseData indx) {
		return getStamth(indx.toInt());
	}
	public PackedDecimalData getStamth(int indx) {

		switch (indx) {
			case 1 : return stamth01;
			case 2 : return stamth02;
			case 3 : return stamth03;
			case 4 : return stamth04;
			case 5 : return stamth05;
			case 6 : return stamth06;
			case 7 : return stamth07;
			case 8 : return stamth08;
			case 9 : return stamth09;
			case 10 : return stamth10;
			case 11 : return stamth11;
			case 12 : return stamth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStamth(BaseData indx, Object what) {
		setStamth(indx, what, false);
	}
	public void setStamth(BaseData indx, Object what, boolean rounded) {
		setStamth(indx.toInt(), what, rounded);
	}
	public void setStamth(int indx, Object what) {
		setStamth(indx, what, false);
	}
	public void setStamth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStamth01(what, rounded);
					 break;
			case 2 : setStamth02(what, rounded);
					 break;
			case 3 : setStamth03(what, rounded);
					 break;
			case 4 : setStamth04(what, rounded);
					 break;
			case 5 : setStamth05(what, rounded);
					 break;
			case 6 : setStamth06(what, rounded);
					 break;
			case 7 : setStamth07(what, rounded);
					 break;
			case 8 : setStamth08(what, rounded);
					 break;
			case 9 : setStamth09(what, rounded);
					 break;
			case 10 : setStamth10(what, rounded);
					 break;
			case 11 : setStamth11(what, rounded);
					 break;
			case 12 : setStamth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		cntbranch.clear();
		register.clear();
		srcebus.clear();
		statcat.clear();
		statFund.clear();
		statSect.clear();
		statSubsect.clear();
		cnPremStat.clear();
		covPremStat.clear();
		acctccy.clear();
		cntcurr.clear();
		cnttype.clear();
		crtable.clear();
		parind.clear();
		billfreq.clear();
		acctyr.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		nonKeyFiller130.clear();
		nonKeyFiller140.clear();
		nonKeyFiller150.clear();
		nonKeyFiller160.clear();
		nonKeyFiller170.clear();
		bfwdc.clear();
		stcmth01.clear();
		stcmth02.clear();
		stcmth03.clear();
		stcmth04.clear();
		stcmth05.clear();
		stcmth06.clear();
		stcmth07.clear();
		stcmth08.clear();
		stcmth09.clear();
		stcmth10.clear();
		stcmth11.clear();
		stcmth12.clear();
		cfwdc.clear();
		bfwdp.clear();
		stpmth01.clear();
		stpmth02.clear();
		stpmth03.clear();
		stpmth04.clear();
		stpmth05.clear();
		stpmth06.clear();
		stpmth07.clear();
		stpmth08.clear();
		stpmth09.clear();
		stpmth10.clear();
		stpmth11.clear();
		stpmth12.clear();
		cfwdp.clear();
		bfwds.clear();
		stsmth01.clear();
		stsmth02.clear();
		stsmth03.clear();
		stsmth04.clear();
		stsmth05.clear();
		stsmth06.clear();
		stsmth07.clear();
		stsmth08.clear();
		stsmth09.clear();
		stsmth10.clear();
		stsmth11.clear();
		stsmth12.clear();
		cfwds.clear();
		bfwdi.clear();
		stimth01.clear();
		stimth02.clear();
		stimth03.clear();
		stimth04.clear();
		stimth05.clear();
		stimth06.clear();
		stimth07.clear();
		stimth08.clear();
		stimth09.clear();
		stimth10.clear();
		stimth11.clear();
		stimth12.clear();
		cfwdi.clear();
		bfwdl.clear();
		stlmth01.clear();
		stlmth02.clear();
		stlmth03.clear();
		stlmth04.clear();
		stlmth05.clear();
		stlmth06.clear();
		stlmth07.clear();
		stlmth08.clear();
		stlmth09.clear();
		stlmth10.clear();
		stlmth11.clear();
		stlmth12.clear();
		cfwdl.clear();
		bfwda.clear();
		stamth01.clear();
		stamth02.clear();
		stamth03.clear();
		stamth04.clear();
		stamth05.clear();
		stamth06.clear();
		stamth07.clear();
		stamth08.clear();
		stamth09.clear();
		stamth10.clear();
		stamth11.clear();
		stamth12.clear();
		cfwda.clear();
		bfwdb.clear();
		stbmthg01.clear();
		stbmthg02.clear();
		stbmthg03.clear();
		stbmthg04.clear();
		stbmthg05.clear();
		stbmthg06.clear();
		stbmthg07.clear();
		stbmthg08.clear();
		stbmthg09.clear();
		stbmthg10.clear();
		stbmthg11.clear();
		stbmthg12.clear();
		cfwdb.clear();
		bfwdld.clear();
		stldmthg01.clear();
		stldmthg02.clear();
		stldmthg03.clear();
		stldmthg04.clear();
		stldmthg05.clear();
		stldmthg06.clear();
		stldmthg07.clear();
		stldmthg08.clear();
		stldmthg09.clear();
		stldmthg10.clear();
		stldmthg11.clear();
		stldmthg12.clear();
		cfwdld.clear();
		bfwdra.clear();
		straamt01.clear();
		straamt02.clear();
		straamt03.clear();
		straamt04.clear();
		straamt05.clear();
		straamt06.clear();
		straamt07.clear();
		straamt08.clear();
		straamt09.clear();
		straamt10.clear();
		straamt11.clear();
		straamt12.clear();
		cfwdra.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}