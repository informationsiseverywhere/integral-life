package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:14
 * Description:
 * Copybook name: GOJNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Gojnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData gojnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData gojnKey = new FixedLengthStringData(64).isAPartOf(gojnFileKey, 0, REDEFINE);
  	public FixedLengthStringData gojnChdrcoy = new FixedLengthStringData(1).isAPartOf(gojnKey, 0);
  	public FixedLengthStringData gojnStatcat = new FixedLengthStringData(2).isAPartOf(gojnKey, 1);
  	public PackedDecimalData gojnAcctyr = new PackedDecimalData(4, 0).isAPartOf(gojnKey, 3);
  	public PackedDecimalData gojnAcctmonth = new PackedDecimalData(2, 0).isAPartOf(gojnKey, 6);
  	public FixedLengthStringData gojnStatFund = new FixedLengthStringData(1).isAPartOf(gojnKey, 8);
  	public FixedLengthStringData gojnStatSect = new FixedLengthStringData(2).isAPartOf(gojnKey, 9);
  	public FixedLengthStringData gojnStatSubsect = new FixedLengthStringData(4).isAPartOf(gojnKey, 11);
  	public FixedLengthStringData gojnRegister = new FixedLengthStringData(3).isAPartOf(gojnKey, 15);
  	public FixedLengthStringData gojnCntbranch = new FixedLengthStringData(2).isAPartOf(gojnKey, 18);
  	public FixedLengthStringData gojnBandage = new FixedLengthStringData(2).isAPartOf(gojnKey, 20);
  	public FixedLengthStringData gojnBandsa = new FixedLengthStringData(2).isAPartOf(gojnKey, 22);
  	public FixedLengthStringData gojnBandprm = new FixedLengthStringData(2).isAPartOf(gojnKey, 24);
  	public FixedLengthStringData gojnBandtrm = new FixedLengthStringData(2).isAPartOf(gojnKey, 26);
  	public PackedDecimalData gojnCommyr = new PackedDecimalData(4, 0).isAPartOf(gojnKey, 28);
  	public FixedLengthStringData gojnPstatcode = new FixedLengthStringData(2).isAPartOf(gojnKey, 31);
  	public FixedLengthStringData gojnCntcurr = new FixedLengthStringData(3).isAPartOf(gojnKey, 33);
  	public PackedDecimalData gojnEffdate = new PackedDecimalData(8, 0).isAPartOf(gojnKey, 36);
  	public PackedDecimalData gojnTransactionTime = new PackedDecimalData(6, 0).isAPartOf(gojnKey, 41);
  	public FixedLengthStringData filler = new FixedLengthStringData(19).isAPartOf(gojnKey, 45, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(gojnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		gojnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}