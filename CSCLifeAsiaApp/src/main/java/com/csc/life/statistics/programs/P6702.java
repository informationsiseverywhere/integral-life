/*
 * File: P6702.java
 * Date: 30 August 2009 0:53:56
 * Author: Quipoz Limited
 * 
 * Class transformed from P6702.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Acmdesc;
import com.csc.fsu.general.recordstructures.Acmdescrec;
import com.csc.life.statistics.dataaccess.AgstTableDAM;
import com.csc.life.statistics.screens.S6702ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*             INQUIRE ON AGENT STATISTICAL BALANCES
*
* It accepts the enquiry key from user and displays the corresponding
* record from AGST file. If the required record is not found, the next
* record retrieved when the file is read will be displayed.
*
* The AGST records will be arranged in the following ascending
* sequence:
*
*          Agency
*          Category
*          Account year
*          Area code
*          Servicing branch
*          Age band
*          Sum insured band
*          Premium band
*          Term band
*          Contract type
*          Coverage code
*          Overriding commission category
*          Premium statistical code
*          Currency
*
* The  statistical  accumulation  key  will be used to search the
* AGST file.  The AGST record  returned must match on all entered
* screen fields.  Screen fields left blank can contain any value.
*
*
*
* FUNCTION KEYS:
*               <KILL>  Return to sub-menu.
*
*             <CANCEL>  Allow user to select another accumulation
*                       inquiry key. It clears the screen S6702
*                       and prompts user to enter a new inquiry key.
*
*               <ENTER> Display the accounts for the accumulation key
*                       if it has been changed, else display the
*                       accumulation account for the next month.
*                       Where inquiry months are exhausted
*                       (i.e. S6702-ACMN = 12), the first account
*                       month of the next AGST record will be
*                       displayed.
*                       If no inquiry key is entered, the first record
*                       in the file is displayed.
*
*
* FILES USED:
*               AGSTSKM
*               DESCSKM
*
*****************************************************************
* </pre>
*/
public class P6702 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6702");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaAgntselPrev = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaScrAgntsel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaAgntsel = new FixedLengthStringData(8).isAPartOf(wsaaScrAgntsel, 0);
	private FixedLengthStringData wsaaFiller = new FixedLengthStringData(2).isAPartOf(wsaaScrAgntsel, 8);

	private FixedLengthStringData wsaaScreenKey = new FixedLengthStringData(41);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaScreenKey, 0);
	private FixedLengthStringData wsaaStatcat = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 8);
	private PackedDecimalData wsaaAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaScreenKey, 10);
	private FixedLengthStringData wsaaAracde = new FixedLengthStringData(3).isAPartOf(wsaaScreenKey, 13);
	private FixedLengthStringData wsaaCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 16);
	private FixedLengthStringData wsaaBandage = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 18);
	private FixedLengthStringData wsaaBandsa = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 20);
	private FixedLengthStringData wsaaBandprm = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 22);
	private FixedLengthStringData wsaaBandtrm = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 24);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaScreenKey, 26);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaScreenKey, 29);
	private FixedLengthStringData wsaaOvrdcat = new FixedLengthStringData(1).isAPartOf(wsaaScreenKey, 33);
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 34);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaScreenKey, 36);
	private ZonedDecimalData wsaaAcctmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaScreenKey, 39).setUnsigned();

		/* WSAA-KEY-FIELDS-ARRAY */
	private FixedLengthStringData wsaaFields = new FixedLengthStringData(135);
	private FixedLengthStringData[] wsaaKeyField = FLSArrayPartOfStructure(15, 8, wsaaFields, 0);
	private FixedLengthStringData[] wsaaFound = FLSArrayPartOfStructure(15, 1, wsaaFields, 120);

		/* WSAA-AGST-FIELDS-ARRAY */
	private FixedLengthStringData wsaaAgstFields = new FixedLengthStringData(120);
	private FixedLengthStringData[] wsaaAgstField = FLSArrayPartOfStructure(15, 8, wsaaAgstFields, 0);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaAgstFound = "";
	private String e382 = "E382";
	private String e015 = "E015";
		/* TABLES */
	private String t3629 = "T3629";
		/* FORMATS */
	private String agstrec = "AGSTREC";
	private String descrec = "DESCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Acmdescrec acmdescrec = new Acmdescrec();
		/*Agent Statistical Accumulation File*/
	private AgstTableDAM agstIO = new AgstTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private S6702ScreenVars sv = ScreenProgram.getScreenVars( S6702ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		checkForErrors2080, 
		exit2090, 
		exit2190, 
		exit2290, 
		exit2649
	}

	public P6702() {
		super();
		screenVars = sv;
		new ScreenModel("S6702", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsaaScreenKey.set(SPACES);
		wsaaI.set(ZERO);
		wsaaAcctmn.set(ZERO);
		wsaaAcctyr.set(9999);
		initialiseScreen1100();
		sv.stcmthOut[varcom.nd.toInt()].set("Y");
		sv.company.set(wsspcomn.company);
		/*EXIT*/
	}

protected void initialiseScreen1100()
	{
		/*PARA*/
		sv.dataArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		sv.acctyr.set(ZERO);
		sv.acmn.set(ZERO);
		sv.acyr.set(ZERO);
		sv.stcmth.set(ZERO);
		sv.stmmth.set(ZERO);
		sv.stpmth.set(ZERO);
		sv.stsmth.set(ZERO);
		sv.stvmth.set(ZERO);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2030();
					displayAgstRecord2040();
					setUpMonthDesc2050();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsaaScrAgntsel.set(sv.agntsel);
		if (isEQ(wsaaAgntsel,wsaaAgntselPrev)
		&& isEQ(sv.statcat,wsaaStatcat)
		&& isEQ(sv.acctyr,wsaaAcctyr)
		&& isEQ(sv.aracde,wsaaAracde)
		&& isEQ(sv.cntbranch,wsaaCntbranch)
		&& isEQ(sv.bandage,wsaaBandage)
		&& isEQ(sv.bandsa,wsaaBandsa)
		&& isEQ(sv.bandprm,wsaaBandprm)
		&& isEQ(sv.bandtrm,wsaaBandtrm)
		&& isEQ(sv.chdrtype,wsaaCnttype)
		&& isEQ(sv.crtable,wsaaCrtable)
		&& isEQ(sv.ovrdcat,wsaaOvrdcat)
		&& isEQ(sv.acctyr,wsaaAcctyr)
		&& isEQ(sv.pstatcd,wsaaPstatcode)
		&& isEQ(sv.cntcurr,wsaaCntcurr)) {
			getNextDetail2300();
		}
		else {
			begnDisplay2400();
		}
		if (isEQ(wsaaI,ZERO)
		|| isGT(wsaaI,12)) {
			wsaaI.set(1);
			wsaaAcctmn.set(1);
		}
	}

protected void validate2030()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			initialiseScreen1100();
			wsaaI.set(ZERO);
			wsspcomn.edterror.set("Y");
			sv.stcmthOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(agstIO.getStatuz(),varcom.endp)
		|| isNE(agstIO.getChdrcoy(),wsspcomn.company)) {
			initialiseScreen1100();
			wsaaI.set(ZERO);
			scrnparams.errorCode.set(e382);
			wsspcomn.edterror.set("Y");
			sv.stcmthOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.acmn,12)) {
			sv.acmnErr.set(e015);
			goTo(GotoLabel.checkForErrors2080);
		}
	}

protected void displayAgstRecord2040()
	{
		sv.stcmthOut[varcom.nd.toInt()].set("N");
		sv.acmn.set(wsaaI);
		sv.acyr.set(agstIO.getAcctyr());
		sv.acctyr.set(agstIO.getAcctyr());
		wsaaAgntsel.set(agstIO.getAgntnum());
		if (isEQ(agstIO.getAgntnum(),ZERO)) {
			wsaaAgntsel.set(SPACES);
		}
		wsaaFiller.set(SPACES);
		sv.agntsel.set(wsaaScrAgntsel);
		sv.statcat.set(agstIO.getStatcat());
		sv.aracde.set(agstIO.getAracde());
		sv.cntbranch.set(agstIO.getCntbranch());
		sv.bandage.set(agstIO.getBandage());
		sv.bandprm.set(agstIO.getBandprm());
		sv.bandsa.set(agstIO.getBandsa());
		sv.bandtrm.set(agstIO.getBandtrm());
		sv.chdrtype.set(agstIO.getCnttype());
		sv.crtable.set(agstIO.getCrtable());
		sv.ovrdcat.set(agstIO.getOvrdcat());
		sv.pstatcd.set(agstIO.getPstatcode());
		sv.cntcurr.set(agstIO.getCntcurr());
		sv.stcmth.set(agstIO.getStcmth(wsaaI));
		sv.stmmth.set(agstIO.getStmmth(wsaaI));
		sv.stpmth.set(agstIO.getStpmth(wsaaI));
		sv.stsmth.set(agstIO.getStsmth(wsaaI));
		sv.stvmth.set(agstIO.getStvmth(wsaaI));
	}

protected void setUpMonthDesc2050()
	{
		acmdescrec.company.set(agstIO.getChdrcoy());
		acmdescrec.language.set(wsspcomn.language);
		acmdescrec.branch.set(agstIO.getCntbranch());
		acmdescrec.function.set("GETD");
		callProgram(Acmdesc.class, acmdescrec.acmdescRec);
		if (isNE(acmdescrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(acmdescrec.statuz);
			fatalError600();
		}
		if (isEQ(wsaaI,ZERO)) {
			wsaaI.set(1);
		}
		sv.mthldesc.set(acmdescrec.lngdesc[wsaaI.toInt()]);
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readAgstRecord2100()
	{
		try {
			para2110();
			setUpCurrencyDesc2120();
		}
		catch (GOTOException e){
		}
	}

protected void para2110()
	{
		agstIO.setChdrcoy(wsspcomn.company);
		wsaaScrAgntsel.set(sv.agntsel);
		agstIO.setAgntnum(wsaaAgntsel);
		agstIO.setStatcat(sv.statcat);
		agstIO.setAcctyr(sv.acctyr);
		agstIO.setAracde(sv.aracde);
		agstIO.setCntbranch(sv.cntbranch);
		agstIO.setBandage(sv.bandage);
		agstIO.setBandsa(sv.bandsa);
		agstIO.setBandprm(sv.bandprm);
		agstIO.setBandtrm(sv.bandtrm);
		agstIO.setCnttype(sv.chdrtype);
		agstIO.setCrtable(sv.crtable);
		agstIO.setOvrdcat(sv.ovrdcat);
		agstIO.setPstatcode(sv.pstatcd);
		agstIO.setCntcurr(sv.cntcurr);
		agstIO.setFormat(agstrec);
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)
		&& isNE(agstIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agstIO.getParams());
			syserrrec.statuz.set(agstIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agstIO.getStatuz(),varcom.endp)
		|| isNE(agstIO.getChdrcoy(),wsspcomn.company)) {
			goTo(GotoLabel.exit2190);
		}
		if (isNE(agstIO.getFunction(),varcom.nextr)) {
			searchForAgst2500();
		}
		if (isEQ(agstIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
	}

protected void setUpCurrencyDesc2120()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(agstIO.getChdrcoy());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(agstIO.getCntcurr());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.descrip.set(descIO.getLongdesc());
	}

protected void moveCurrentToWsaa2200()
	{
		try {
			para2210();
		}
		catch (GOTOException e){
		}
	}

protected void para2210()
	{
		if (isEQ(agstIO.getStatuz(),varcom.endp)
		|| isNE(agstIO.getChdrcoy(),wsspcomn.company)) {
			goTo(GotoLabel.exit2290);
		}
		wsaaAgntselPrev.set(agstIO.getAgntnum());
		wsaaAcctmn.set(wsaaI);
		wsaaStatcat.set(agstIO.getStatcat());
		wsaaAcctyr.set(agstIO.getAcctyr());
		wsaaAracde.set(agstIO.getAracde());
		wsaaCntbranch.set(agstIO.getCntbranch());
		wsaaBandage.set(agstIO.getBandage());
		wsaaBandsa.set(agstIO.getBandsa());
		wsaaBandprm.set(agstIO.getBandprm());
		wsaaBandtrm.set(agstIO.getBandtrm());
		wsaaCnttype.set(agstIO.getCnttype());
		wsaaCrtable.set(agstIO.getCrtable());
		wsaaOvrdcat.set(agstIO.getOvrdcat());
		wsaaPstatcode.set(agstIO.getPstatcode());
		wsaaCntcurr.set(agstIO.getCntcurr());
	}

protected void getNextDetail2300()
	{
		/*PARA*/
		if (isEQ(wsaaI,12)) {
			agstIO.setFunction(varcom.nextr);
			wsaaI.set(1);
			wsaaAcctmn.set(1);
			readAgstRecord2100();
			moveCurrentToWsaa2200();
		}
		else {
			if (isEQ(sv.acmn,wsaaAcctmn)
			|| isEQ(sv.acmn,ZERO)) {
				wsaaI.add(1);
				wsaaAcctmn.set(wsaaI);
			}
			else {
				wsaaI.set(sv.acmn);
				wsaaAcctmn.set(sv.acmn);
			}
		}
		/*EXIT*/
	}

protected void begnDisplay2400()
	{
		/*PARA*/
		if (isEQ(sv.acmn,ZERO)
		|| isGT(sv.acmn,12)) {
			wsaaI.add(1);
			wsaaAcctmn.set(wsaaI);
		}
		else {
			wsaaI.set(sv.acmn);
			wsaaAcctmn.set(sv.acmn);
		}
		agstIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		agstIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		agstIO.setFitKeysSearch("CHDRCOY");
		readAgstRecord2100();
		moveCurrentToWsaa2200();
		/*EXIT*/
	}

protected void searchForAgst2500()
	{
		search2510();
	}

protected void search2510()
	{
		wsaaKeyField[1].set(sv.company);
		wsaaKeyField[2].set(sv.agntsel);
		wsaaKeyField[3].set(sv.statcat);
		if (isNE(sv.acctyr,ZERO)) {
			wsaaKeyField[4].set(sv.acctyr);
		}
		else {
			wsaaKeyField[4].set(SPACES);
		}
		wsaaKeyField[5].set(sv.aracde);
		wsaaKeyField[6].set(sv.cntbranch);
		wsaaKeyField[7].set(sv.bandage);
		wsaaKeyField[8].set(sv.bandsa);
		wsaaKeyField[9].set(sv.bandprm);
		wsaaKeyField[10].set(sv.bandtrm);
		wsaaKeyField[11].set(sv.chdrtype);
		wsaaKeyField[12].set(sv.crtable);
		wsaaKeyField[13].set(sv.ovrdcat);
		wsaaKeyField[14].set(sv.pstatcd);
		wsaaKeyField[15].set(sv.cntcurr);
		wsaaAgstFound = "N";
		while ( !(isEQ(wsaaAgstFound,"Y")
		|| isEQ(agstIO.getStatuz(),varcom.endp))) {
			checkAgst2600();
		}
		
	}

protected void checkAgst2600()
	{
		try {
			check2610();
		}
		catch (GOTOException e){
		}
	}

protected void check2610()
	{
		wsaaAgstField[1].set(agstIO.getChdrcoy());
		wsaaAgstField[2].set(agstIO.getAgntnum());
		wsaaAgstField[3].set(agstIO.getStatcat());
		wsaaAgstField[4].set(agstIO.getAcctyr());
		wsaaAgstField[5].set(agstIO.getAracde());
		wsaaAgstField[6].set(agstIO.getCntbranch());
		wsaaAgstField[7].set(agstIO.getBandage());
		wsaaAgstField[8].set(agstIO.getBandsa());
		wsaaAgstField[9].set(agstIO.getBandprm());
		wsaaAgstField[10].set(agstIO.getBandtrm());
		wsaaAgstField[11].set(agstIO.getCnttype());
		wsaaAgstField[12].set(agstIO.getCrtable());
		wsaaAgstField[13].set(agstIO.getOvrdcat());
		wsaaAgstField[14].set(agstIO.getPstatcode());
		wsaaAgstField[15].set(agstIO.getCntcurr());
		for (wsaaX.set(1); !(isGT(wsaaX,15)); wsaaX.add(1)){
			wsaaFound[wsaaX.toInt()].set("N");
		}
		for (wsaaX.set(1); !(isGT(wsaaX,15)
		|| isEQ(agstIO.getStatuz(),varcom.endp)); wsaaX.add(1)){
			check2650();
		}
		if (isEQ(agstIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2649);
		}
		wsaaAgstFound = "Y";
		for (wsaaX.set(1); !(isGT(wsaaX,5)
		|| isEQ(wsaaAgstFound,"N")); wsaaX.add(1)){
			if (isNE(wsaaKeyField[wsaaX.toInt()],SPACES)
			&& isNE(wsaaFound[wsaaX.toInt()],"Y")) {
				wsaaAgstFound = "N";
			}
		}
		if (isEQ(wsaaAgstFound,"Y")) {
			goTo(GotoLabel.exit2649);
		}
		agstIO.setFunction(varcom.nextr);
		agstIO.setFormat(agstrec);
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)
		&& isNE(agstIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(agstIO.getStatuz());
			syserrrec.params.set(agstIO.getParams());
			fatalError600();
		}
	}

protected void check2650()
	{
		/*CHECK*/
		if (isNE(wsaaKeyField[wsaaX.toInt()],wsaaAgstField[wsaaX.toInt()])) {
			if (isEQ(wsaaFound[wsaaX.toInt()],"Y")) {
				agstIO.setStatuz(varcom.endp);
			}
		}
		else {
			wsaaFound[wsaaX.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
