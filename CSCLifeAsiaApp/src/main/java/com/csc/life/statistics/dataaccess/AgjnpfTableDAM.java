package com.csc.life.statistics.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgjnpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:42
 * Class transformed from AGJNPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgjnpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 145;
	public FixedLengthStringData agjnrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agjnpfRecord = agjnrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(agjnrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agjnrec);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(agjnrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(agjnrec);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(agjnrec);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(agjnrec);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(agjnrec);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(agjnrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(agjnrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(agjnrec);
	public FixedLengthStringData ovrdcat = DD.ovrdcat.copy().isAPartOf(agjnrec);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(agjnrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(agjnrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(agjnrec);
	public PackedDecimalData acctyr = DD.acctyr.copy().isAPartOf(agjnrec);
	public PackedDecimalData acctmonth = DD.acctmonth.copy().isAPartOf(agjnrec);
	public PackedDecimalData stcmth = DD.stcmth.copy().isAPartOf(agjnrec);
	public PackedDecimalData stvmth = DD.stvmth.copy().isAPartOf(agjnrec);
	public PackedDecimalData stpmth = DD.stpmth.copy().isAPartOf(agjnrec);
	public PackedDecimalData stsmth = DD.stsmth.copy().isAPartOf(agjnrec);
	public PackedDecimalData stmmth = DD.stmmth.copy().isAPartOf(agjnrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(agjnrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(agjnrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(agjnrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(agjnrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agjnrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agjnrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agjnrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgjnpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgjnpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgjnpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgjnpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgjnpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgjnpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgjnpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGJNPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"AGNTNUM, " +
							"ARACDE, " +
							"CNTBRANCH, " +
							"BANDAGE, " +
							"BANDSA, " +
							"BANDPRM, " +
							"BANDTRM, " +
							"CNTTYPE, " +
							"CRTABLE, " +
							"OVRDCAT, " +
							"STATCAT, " +
							"PSTATCODE, " +
							"CNTCURR, " +
							"ACCTYR, " +
							"ACCTMONTH, " +
							"STCMTH, " +
							"STVMTH, " +
							"STPMTH, " +
							"STSMTH, " +
							"STMMTH, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"EFFDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     agntnum,
                                     aracde,
                                     cntbranch,
                                     bandage,
                                     bandsa,
                                     bandprm,
                                     bandtrm,
                                     cnttype,
                                     crtable,
                                     ovrdcat,
                                     statcat,
                                     pstatcode,
                                     cntcurr,
                                     acctyr,
                                     acctmonth,
                                     stcmth,
                                     stvmth,
                                     stpmth,
                                     stsmth,
                                     stmmth,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     effdate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		agntnum.clear();
  		aracde.clear();
  		cntbranch.clear();
  		bandage.clear();
  		bandsa.clear();
  		bandprm.clear();
  		bandtrm.clear();
  		cnttype.clear();
  		crtable.clear();
  		ovrdcat.clear();
  		statcat.clear();
  		pstatcode.clear();
  		cntcurr.clear();
  		acctyr.clear();
  		acctmonth.clear();
  		stcmth.clear();
  		stvmth.clear();
  		stpmth.clear();
  		stsmth.clear();
  		stmmth.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		effdate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgjnrec() {
  		return agjnrec;
	}

	public FixedLengthStringData getAgjnpfRecord() {
  		return agjnpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgjnrec(what);
	}

	public void setAgjnrec(Object what) {
  		this.agjnrec.set(what);
	}

	public void setAgjnpfRecord(Object what) {
  		this.agjnpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agjnrec.getLength());
		result.set(agjnrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}