/*
 * File: Bj527.java
 * Date: 29 August 2009 21:42:36
 * Author: Quipoz Limited
 *
 * Class transformed from BJ527.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj527Report;
import com.csc.life.statistics.tablestructures.Tj675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                 New Business Transacted
*                 =======================
*
*   This report will list the New Business transacted during the
*     year segregating them into regular premium contracts, single
*     premium contracts(includes immediate and defered annuities).
*
*   This refers to proposals created during the specified accounting
*     year regardless of the proposal status at the end of the
*     accounting year.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   1000-INITIALISE section
*     Read table TR386 using BSSC-LANGUAGE and WSAA-PROG.Store
*       the information in the working storage array. This table
*       is an extra data multiple screen.
*
*     Read GOVE using SQL.Selet records where STATCAT = 'NB' and
*       ACCTYR=PJ517-ACCTYEAR.Order by company,accounting currency
*       source of business, statutory section, statutory subsection,
*       contract type and coverage type.
*
*   2000-READ-FILE section.
*     Fetch the SQL record. If end-of-file, set WSSP-EDTERROR to ENDP.
*
*   2500-EDIT section.
*     Set WSSP-EDTERRIR to O-K.
*
*   3000-UPDATE section.
*     If there is a change in company, accounting currency or source
*      of business or Business Category or Division/Sub-Class/Group
*      skip to a new page and print report heading and initialise
*      working storage.
*
*   For each SQL record read, do the following:
*
*     Read T3629 using SQL-CNTCURR to get the currency rate. Amounts
*       coming from GOVE are in premium currency.
*
*     If Accounting currency is not equal to Premium currency,
*        Convert the amounts to accounting currency amount by
*        multiplying with T3629-SCRATE.
*
*     We need to check if it is a regular premium contract or a
*        single premium contract.
*
*   Control totals:
*     01  -  Totals of read records
*     02  -  Totals of pages printed
*     03  -  Totals of policy count (exclude Reassurance)
*     04  -  Totals of lives count  (exclude Reassurance)
*     05  -  Totals of sum assured  (exclude Reassurance)
*     06  -  Totals of annuity pa   (exclude Reassurance)
*     07  -  Totals of annual prem  (exclude Reassurance)
*     08  -  Totals of single prem  (exclude Reassurance)
*     09  -  Totals of reassurance
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
* </pre>
*/
public class Bj527 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgovepfAllrs = null;
	private java.sql.PreparedStatement sqlgovepfAllps = null;
	private java.sql.Connection sqlgovepfAllconn = null;
	private String sqlgovepfAll = "";
	private java.sql.ResultSet sqlgovepfrs = null;
	private java.sql.PreparedStatement sqlgovepfps = null;
	private java.sql.Connection sqlgovepfconn = null;
	private String sqlgovepf = "";
	private Rj527Report rj527 = new Rj527Report();
	private FixedLengthStringData rj527Rec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ527");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaProgram = new FixedLengthStringData(5).isAPartOf(wsaaItem, 1);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String wsaaFirstRecord = "";
	private String wsaaNb = "NB";
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);
	private ZonedDecimalData wsaaScrate = new ZonedDecimalData(12, 7);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData mth = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaSelectAll = new FixedLengthStringData(1);
	private Validator selectAll = new Validator(wsaaSelectAll, "Y");
	private FixedLengthStringData wsaaTj675Stsect01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect02 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect03 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect04 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect05 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect06 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect07 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect08 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect09 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect10 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBillingFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreqN = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillingFreq, 0, REDEFINE).setUnsigned();
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStssect = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaSrcebus = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaReg = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaParind = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private PackedDecimalData wsaaStcmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStlmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStimthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStsmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStbmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStraamtc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRiStbamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAnnlBasic = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAnnlLoaded = new PackedDecimalData(18, 2);

	private FixedLengthStringData wsaaSqlKeep = new FixedLengthStringData(1058);
	private FixedLengthStringData wsaaSChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 0);
	private FixedLengthStringData wsaaSStatcat = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 1);
	private PackedDecimalData wsaaSAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaSqlKeep, 3);
	private FixedLengthStringData wsaaSStfund = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 6);
	private FixedLengthStringData wsaaSStsect = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 7);
	private FixedLengthStringData wsaaSStssect = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 9);
	private FixedLengthStringData wsaaSRegister = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 13);
	private FixedLengthStringData wsaaSCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 16);
	private FixedLengthStringData wsaaSCrpstat = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 18);
	private FixedLengthStringData wsaaSCnpstat = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 20);
	private FixedLengthStringData wsaaSCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 22);
	private FixedLengthStringData wsaaSCrtable = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 25);
	private FixedLengthStringData wsaaSAcctccy = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 29);
	private FixedLengthStringData wsaaSCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 32);
	private FixedLengthStringData wsaaSParind = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 35);
	private FixedLengthStringData wsaaSBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 36);
	private PackedDecimalData[] wsaaStcmth = PDArrayPartOfStructure(12, 9, 0, wsaaSqlKeep, 38);
	private PackedDecimalData[] wsaaStlmth = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 98);
	private PackedDecimalData[] wsaaStimth = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 218);
	private PackedDecimalData[] wsaaStamth = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 338);
	private PackedDecimalData[] wsaaStbmthg = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 458);
	private PackedDecimalData[] wsaaStsmth = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 578);
	private PackedDecimalData[] wsaaStpmth = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 698);
	private PackedDecimalData[] wsaaStraamt = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 818);
	private PackedDecimalData[] wsaaStldmth = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 938);

		/* WSAA-AMOUNT-AREAS */
	private FixedLengthStringData wsaaRegularPremium = new FixedLengthStringData(120);
	private PackedDecimalData wsaaRegNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 0).init(0);
	private PackedDecimalData wsaaRegNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 10).init(0);
	private PackedDecimalData wsaaRegSi = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 20).init(0);
	private PackedDecimalData wsaaRegAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 30).init(0);
	private PackedDecimalData wsaaRegAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 40).init(0);
	private PackedDecimalData wsaaRegSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 50).init(0);
	private PackedDecimalData wsaaRiRegNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 60).init(0);
	private PackedDecimalData wsaaRiRegNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 70).init(0);
	private PackedDecimalData wsaaRiRegSi = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 80).init(0);
	private PackedDecimalData wsaaRiRegAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 90).init(0);
	private PackedDecimalData wsaaRiRegAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 100).init(0);
	private PackedDecimalData wsaaRiRegSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 110).init(0);

	private FixedLengthStringData wsaaSinglePremium = new FixedLengthStringData(120);
	private PackedDecimalData wsaaSglNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 0).init(0);
	private PackedDecimalData wsaaSglNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 10).init(0);
	private PackedDecimalData wsaaSglSi = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 20).init(0);
	private PackedDecimalData wsaaSglAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 30).init(0);
	private PackedDecimalData wsaaSglAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 40).init(0);
	private PackedDecimalData wsaaSglSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 50).init(0);
	private PackedDecimalData wsaaRiSglNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 60).init(0);
	private PackedDecimalData wsaaRiSglNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 70).init(0);
	private PackedDecimalData wsaaRiSglSi = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 80).init(0);
	private PackedDecimalData wsaaRiSglAnnuity = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 90).init(0);
	private PackedDecimalData wsaaRiSglAprem = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 100).init(0);
	private PackedDecimalData wsaaRiSglSprem = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 110).init(0);
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String t1693 = "T1693";
	private String t3589 = "T3589";
	private String t3629 = "T3629";
	private String t5684 = "T5684";
	private String t5685 = "T5685";
	private String tj675 = "TJ675";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private int ct08 = 8;
	private int ct09 = 9;
		/* ERRORS */
	private String esql = "ESQL";
	private String g418 = "G418";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-GOVEPF */
	private FixedLengthStringData sqlGoverec = new FixedLengthStringData(1058);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 0);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 1);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGoverec, 3);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 6);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 7);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 9);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 13);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 16);
	private FixedLengthStringData sqlCrpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 18);
	private FixedLengthStringData sqlCnpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 20);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 22);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 25);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 29);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 32);
	private FixedLengthStringData sqlParind = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 35);
	private FixedLengthStringData sqlBillfreq = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 36);
	private PackedDecimalData sqlStcmth01 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 38);
	private PackedDecimalData sqlStcmth02 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 43);
	private PackedDecimalData sqlStcmth03 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 48);
	private PackedDecimalData sqlStcmth04 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 53);
	private PackedDecimalData sqlStcmth05 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 58);
	private PackedDecimalData sqlStcmth06 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 63);
	private PackedDecimalData sqlStcmth07 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 68);
	private PackedDecimalData sqlStcmth08 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 73);
	private PackedDecimalData sqlStcmth09 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 78);
	private PackedDecimalData sqlStcmth10 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 83);
	private PackedDecimalData sqlStcmth11 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 88);
	private PackedDecimalData sqlStcmth12 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 93);
	private PackedDecimalData sqlStlmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 98);
	private PackedDecimalData sqlStlmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 108);
	private PackedDecimalData sqlStlmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 118);
	private PackedDecimalData sqlStlmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 128);
	private PackedDecimalData sqlStlmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 138);
	private PackedDecimalData sqlStlmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 148);
	private PackedDecimalData sqlStlmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 158);
	private PackedDecimalData sqlStlmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 168);
	private PackedDecimalData sqlStlmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 178);
	private PackedDecimalData sqlStlmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 188);
	private PackedDecimalData sqlStlmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 198);
	private PackedDecimalData sqlStlmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 208);
	private PackedDecimalData sqlStimth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 218);
	private PackedDecimalData sqlStimth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 228);
	private PackedDecimalData sqlStimth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 238);
	private PackedDecimalData sqlStimth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 248);
	private PackedDecimalData sqlStimth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 258);
	private PackedDecimalData sqlStimth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 268);
	private PackedDecimalData sqlStimth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 278);
	private PackedDecimalData sqlStimth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 288);
	private PackedDecimalData sqlStimth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 298);
	private PackedDecimalData sqlStimth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 308);
	private PackedDecimalData sqlStimth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 318);
	private PackedDecimalData sqlStimth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 328);
	private PackedDecimalData sqlStamth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 338);
	private PackedDecimalData sqlStamth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 348);
	private PackedDecimalData sqlStamth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 358);
	private PackedDecimalData sqlStamth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 368);
	private PackedDecimalData sqlStamth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 378);
	private PackedDecimalData sqlStamth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 388);
	private PackedDecimalData sqlStamth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 398);
	private PackedDecimalData sqlStamth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 408);
	private PackedDecimalData sqlStamth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 418);
	private PackedDecimalData sqlStamth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 428);
	private PackedDecimalData sqlStamth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 438);
	private PackedDecimalData sqlStamth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 448);
	private PackedDecimalData sqlStbmthg01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 458);
	private PackedDecimalData sqlStbmthg02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 468);
	private PackedDecimalData sqlStbmthg03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 478);
	private PackedDecimalData sqlStbmthg04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 488);
	private PackedDecimalData sqlStbmthg05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 498);
	private PackedDecimalData sqlStbmthg06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 508);
	private PackedDecimalData sqlStbmthg07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 518);
	private PackedDecimalData sqlStbmthg08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 528);
	private PackedDecimalData sqlStbmthg09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 538);
	private PackedDecimalData sqlStbmthg10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 548);
	private PackedDecimalData sqlStbmthg11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 558);
	private PackedDecimalData sqlStbmthg12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 568);
	private PackedDecimalData sqlStsmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 578);
	private PackedDecimalData sqlStsmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 588);
	private PackedDecimalData sqlStsmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 598);
	private PackedDecimalData sqlStsmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 608);
	private PackedDecimalData sqlStsmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 618);
	private PackedDecimalData sqlStsmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 628);
	private PackedDecimalData sqlStsmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 638);
	private PackedDecimalData sqlStsmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 648);
	private PackedDecimalData sqlStsmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 658);
	private PackedDecimalData sqlStsmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 668);
	private PackedDecimalData sqlStsmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 678);
	private PackedDecimalData sqlStsmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 688);
	private PackedDecimalData sqlStpmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 698);
	private PackedDecimalData sqlStpmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 708);
	private PackedDecimalData sqlStpmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 718);
	private PackedDecimalData sqlStpmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 728);
	private PackedDecimalData sqlStpmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 738);
	private PackedDecimalData sqlStpmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 748);
	private PackedDecimalData sqlStpmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 758);
	private PackedDecimalData sqlStpmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 768);
	private PackedDecimalData sqlStpmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 778);
	private PackedDecimalData sqlStpmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 788);
	private PackedDecimalData sqlStpmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 798);
	private PackedDecimalData sqlStpmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 808);
	private PackedDecimalData sqlStraamt01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 818);
	private PackedDecimalData sqlStraamt02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 828);
	private PackedDecimalData sqlStraamt03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 838);
	private PackedDecimalData sqlStraamt04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 848);
	private PackedDecimalData sqlStraamt05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 858);
	private PackedDecimalData sqlStraamt06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 868);
	private PackedDecimalData sqlStraamt07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 878);
	private PackedDecimalData sqlStraamt08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 888);
	private PackedDecimalData sqlStraamt09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 898);
	private PackedDecimalData sqlStraamt10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 908);
	private PackedDecimalData sqlStraamt11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 918);
	private PackedDecimalData sqlStraamt12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 928);
	private PackedDecimalData sqlStldmthg01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 938);
	private PackedDecimalData sqlStldmthg02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 948);
	private PackedDecimalData sqlStldmthg03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 958);
	private PackedDecimalData sqlStldmthg04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 968);
	private PackedDecimalData sqlStldmthg05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 978);
	private PackedDecimalData sqlStldmthg06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 988);
	private PackedDecimalData sqlStldmthg07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 998);
	private PackedDecimalData sqlStldmthg08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1008);
	private PackedDecimalData sqlStldmthg09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1018);
	private PackedDecimalData sqlStldmthg10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1028);
	private PackedDecimalData sqlStldmthg11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1038);
	private PackedDecimalData sqlStldmthg12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1048);

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rj527H01 = new FixedLengthStringData(179);
	private FixedLengthStringData rj527h01O = new FixedLengthStringData(179).isAPartOf(rj527H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj527h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj527h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj527h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj527h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj527h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj527h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj527h01O, 50);
	private FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(rj527h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj527h01O, 83);
	private FixedLengthStringData stsect = new FixedLengthStringData(2).isAPartOf(rj527h01O, 113);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30).isAPartOf(rj527h01O, 115);
	private FixedLengthStringData stssect = new FixedLengthStringData(4).isAPartOf(rj527h01O, 145);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rj527h01O, 149);

	private FixedLengthStringData rj527H02 = new FixedLengthStringData(2);

	private FixedLengthStringData rj527H03 = new FixedLengthStringData(2);

	private FixedLengthStringData rj527D01 = new FixedLengthStringData(78);
	private FixedLengthStringData rj527d01O = new FixedLengthStringData(78).isAPartOf(rj527D01, 0);
	private ZonedDecimalData stcmthg = new ZonedDecimalData(9, 0).isAPartOf(rj527d01O, 0);
	private ZonedDecimalData stlmth = new ZonedDecimalData(9, 0).isAPartOf(rj527d01O, 9);
	private ZonedDecimalData repamt01 = new ZonedDecimalData(15, 0).isAPartOf(rj527d01O, 18);
	private ZonedDecimalData repamt02 = new ZonedDecimalData(15, 0).isAPartOf(rj527d01O, 33);
	private ZonedDecimalData repamt03 = new ZonedDecimalData(15, 0).isAPartOf(rj527d01O, 48);
	private ZonedDecimalData repamt04 = new ZonedDecimalData(15, 0).isAPartOf(rj527d01O, 63);

	private FixedLengthStringData rj527D02 = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Pj517par pj517par = new Pj517par();
	private T3629rec t3629rec = new T3629rec();
	private Tj675rec tj675rec = new Tj675rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		a220CallItemio
	}

	public Bj527() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		rj527.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsaaAcctyr.set(pj517par.acctyr);
		wsspEdterror.set(varcom.oK);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaProgram.set(wsaaProg);
		wsaaLanguage.set(bsscIO.getLanguage());
		acctyear.set(pj517par.acctyr);
		acctmonth.set(pj517par.acctmnth);
		a000InitialWs();
		for (wsaaX.set(1); !(isGT(wsaaX,12)); wsaaX.add(1)){
			wsaaStcmth[wsaaX.toInt()].set(0);
			wsaaStlmth[wsaaX.toInt()].set(0);
			wsaaStimth[wsaaX.toInt()].set(0);
			wsaaStsmth[wsaaX.toInt()].set(0);
			wsaaStamth[wsaaX.toInt()].set(0);
			wsaaStbmthg[wsaaX.toInt()].set(0);
			wsaaStraamt[wsaaX.toInt()].set(0);
			wsaaStldmth[wsaaX.toInt()].set(0);
		}
		wsaaX.set(0);
		wsaaStcmthc.set(0);
		wsaaStlmthc.set(0);
		wsaaStimthc.set(0);
		wsaaStsmthc.set(0);
		wsaaStamthc.set(0);
		wsaaStbmthc.set(0);
		wsaaStraamtc.set(0);
		wsaaAnnlBasic.set(0);
		wsaaAnnlLoaded.set(0);
		wsaaStsect.set(SPACES);
		wsaaStssect.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaReg.set(SPACES);
		wsaaFirstRecord = "Y";
		readTj6751200();
	}

protected void readTj6751200()
	{
		para1210();
	}

protected void para1210()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tj675);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		wsaaSelectAll.set("Y");
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			selectAll1300();
		}
		else {
			wsaaSelectAll.set("N");
			tj675rec.tj675Rec.set(itemIO.getGenarea());
			selectSpecified1400();
		}
	}

protected void selectAll1300()
	{
		/*PARA*/
		sqlgovepfAll = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, PARIND, BILLFREQ, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, STLMTH01, STLMTH02, STLMTH03, STLMTH04, STLMTH05, STLMTH06, STLMTH07, STLMTH08, STLMTH09, STLMTH10, STLMTH11, STLMTH12, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12, STBMTHG01, STBMTHG02, STBMTHG03, STBMTHG04, STBMTHG05, STBMTHG06, STBMTHG07, STBMTHG08, STBMTHG09, STBMTHG10, STBMTHG11, STBMTHG12, STSMTH01, STSMTH02, STSMTH03, STSMTH04, STSMTH05, STSMTH06, STSMTH07, STSMTH08, STSMTH09, STSMTH10, STSMTH11, STSMTH12, STPMTH01, STPMTH02, STPMTH03, STPMTH04, STPMTH05, STPMTH06, STPMTH07, STPMTH08, STPMTH09, STPMTH10, STPMTH11, STPMTH12, STRAAMT01, STRAAMT02, STRAAMT03, STRAAMT04, STRAAMT05, STRAAMT06, STRAAMT07, STRAAMT08, STRAAMT09, STRAAMT10, STRAAMT11, STRAAMT12, STLDMTHG01, STLDMTHG02, STLDMTHG03, STLDMTHG04, STLDMTHG05, STLDMTHG06, STLDMTHG07, STLDMTHG08, STLDMTHG09, STLDMTHG10, STLDMTHG11, STLDMTHG12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE STATCAT = ?" +
" AND ACCTYR = ?" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfAllconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfAllps = appVars.prepareStatementEmbeded(sqlgovepfAllconn, sqlgovepfAll, "GOVEPF");
			appVars.setDBString(sqlgovepfAllps, 1, wsaaNb);
			appVars.setDBDouble(sqlgovepfAllps, 2, wsaaAcctyr.toDouble());
			sqlgovepfAllrs = appVars.executeQuery(sqlgovepfAllps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void selectSpecified1400()
	{
		para1410();
	}

protected void para1410()
	{
		wsaaTj675Stsect01.set(tj675rec.statSect[1]);
		wsaaTj675Stsect02.set(tj675rec.statSect[2]);
		wsaaTj675Stsect03.set(tj675rec.statSect[3]);
		wsaaTj675Stsect04.set(tj675rec.statSect[4]);
		wsaaTj675Stsect05.set(tj675rec.statSect[5]);
		wsaaTj675Stsect06.set(tj675rec.statSect[6]);
		wsaaTj675Stsect07.set(tj675rec.statSect[7]);
		wsaaTj675Stsect08.set(tj675rec.statSect[8]);
		wsaaTj675Stsect09.set(tj675rec.statSect[9]);
		wsaaTj675Stsect10.set(tj675rec.statSect[10]);
		sqlgovepf = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, PARIND, BILLFREQ, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, STLMTH01, STLMTH02, STLMTH03, STLMTH04, STLMTH05, STLMTH06, STLMTH07, STLMTH08, STLMTH09, STLMTH10, STLMTH11, STLMTH12, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12, STBMTHG01, STBMTHG02, STBMTHG03, STBMTHG04, STBMTHG05, STBMTHG06, STBMTHG07, STBMTHG08, STBMTHG09, STBMTHG10, STBMTHG11, STBMTHG12, STSMTH01, STSMTH02, STSMTH03, STSMTH04, STSMTH05, STSMTH06, STSMTH07, STSMTH08, STSMTH09, STSMTH10, STSMTH11, STSMTH12, STPMTH01, STPMTH02, STPMTH03, STPMTH04, STPMTH05, STPMTH06, STPMTH07, STPMTH08, STPMTH09, STPMTH10, STPMTH11, STPMTH12, STRAAMT01, STRAAMT02, STRAAMT03, STRAAMT04, STRAAMT05, STRAAMT06, STRAAMT07, STRAAMT08, STRAAMT09, STRAAMT10, STRAAMT11, STRAAMT12, STLDMTHG01, STLDMTHG02, STLDMTHG03, STLDMTHG04, STLDMTHG05, STLDMTHG06, STLDMTHG07, STLDMTHG08, STLDMTHG09, STLDMTHG10, STLDMTHG11, STLDMTHG12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE STATCAT = ?" +
" AND ACCTYR = ?" +
" AND (STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?)" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfps = appVars.prepareStatementEmbeded(sqlgovepfconn, sqlgovepf, "GOVEPF");
			appVars.setDBString(sqlgovepfps, 1, wsaaNb);
			appVars.setDBDouble(sqlgovepfps, 2, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgovepfps, 3, wsaaTj675Stsect01.toString());
			appVars.setDBString(sqlgovepfps, 4, wsaaTj675Stsect02.toString());
			appVars.setDBString(sqlgovepfps, 5, wsaaTj675Stsect03.toString());
			appVars.setDBString(sqlgovepfps, 6, wsaaTj675Stsect04.toString());
			appVars.setDBString(sqlgovepfps, 7, wsaaTj675Stsect05.toString());
			appVars.setDBString(sqlgovepfps, 8, wsaaTj675Stsect06.toString());
			appVars.setDBString(sqlgovepfps, 9, wsaaTj675Stsect07.toString());
			appVars.setDBString(sqlgovepfps, 10, wsaaTj675Stsect08.toString());
			appVars.setDBString(sqlgovepfps, 11, wsaaTj675Stsect09.toString());
			appVars.setDBString(sqlgovepfps, 12, wsaaTj675Stsect10.toString());
			sqlgovepfrs = appVars.executeQuery(sqlgovepfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		if (selectAll.isTrue()) {
			sqlerrorflag = false;
			try {
				if (sqlgovepfAllrs.next()) {
					appVars.getDBObject(sqlgovepfAllrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfAllrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfAllrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfAllrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfAllrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfAllrs, 6, sqlStssect);
					appVars.getDBObject(sqlgovepfAllrs, 7, sqlRegister);
					appVars.getDBObject(sqlgovepfAllrs, 8, sqlCntbranch);
					appVars.getDBObject(sqlgovepfAllrs, 9, sqlCrpstat);
					appVars.getDBObject(sqlgovepfAllrs, 10, sqlCnpstat);
					appVars.getDBObject(sqlgovepfAllrs, 11, sqlCnttype);
					appVars.getDBObject(sqlgovepfAllrs, 12, sqlCrtable);
					appVars.getDBObject(sqlgovepfAllrs, 13, sqlAcctccy);
					appVars.getDBObject(sqlgovepfAllrs, 14, sqlCntcurr);
					appVars.getDBObject(sqlgovepfAllrs, 15, sqlParind);
					appVars.getDBObject(sqlgovepfAllrs, 16, sqlBillfreq);
					appVars.getDBObject(sqlgovepfAllrs, 17, sqlStcmth01);
					appVars.getDBObject(sqlgovepfAllrs, 18, sqlStcmth02);
					appVars.getDBObject(sqlgovepfAllrs, 19, sqlStcmth03);
					appVars.getDBObject(sqlgovepfAllrs, 20, sqlStcmth04);
					appVars.getDBObject(sqlgovepfAllrs, 21, sqlStcmth05);
					appVars.getDBObject(sqlgovepfAllrs, 22, sqlStcmth06);
					appVars.getDBObject(sqlgovepfAllrs, 23, sqlStcmth07);
					appVars.getDBObject(sqlgovepfAllrs, 24, sqlStcmth08);
					appVars.getDBObject(sqlgovepfAllrs, 25, sqlStcmth09);
					appVars.getDBObject(sqlgovepfAllrs, 26, sqlStcmth10);
					appVars.getDBObject(sqlgovepfAllrs, 27, sqlStcmth11);
					appVars.getDBObject(sqlgovepfAllrs, 28, sqlStcmth12);
					appVars.getDBObject(sqlgovepfAllrs, 29, sqlStlmth01);
					appVars.getDBObject(sqlgovepfAllrs, 30, sqlStlmth02);
					appVars.getDBObject(sqlgovepfAllrs, 31, sqlStlmth03);
					appVars.getDBObject(sqlgovepfAllrs, 32, sqlStlmth04);
					appVars.getDBObject(sqlgovepfAllrs, 33, sqlStlmth05);
					appVars.getDBObject(sqlgovepfAllrs, 34, sqlStlmth06);
					appVars.getDBObject(sqlgovepfAllrs, 35, sqlStlmth07);
					appVars.getDBObject(sqlgovepfAllrs, 36, sqlStlmth08);
					appVars.getDBObject(sqlgovepfAllrs, 37, sqlStlmth09);
					appVars.getDBObject(sqlgovepfAllrs, 38, sqlStlmth10);
					appVars.getDBObject(sqlgovepfAllrs, 39, sqlStlmth11);
					appVars.getDBObject(sqlgovepfAllrs, 40, sqlStlmth12);
					appVars.getDBObject(sqlgovepfAllrs, 41, sqlStimth01);
					appVars.getDBObject(sqlgovepfAllrs, 42, sqlStimth02);
					appVars.getDBObject(sqlgovepfAllrs, 43, sqlStimth03);
					appVars.getDBObject(sqlgovepfAllrs, 44, sqlStimth04);
					appVars.getDBObject(sqlgovepfAllrs, 45, sqlStimth05);
					appVars.getDBObject(sqlgovepfAllrs, 46, sqlStimth06);
					appVars.getDBObject(sqlgovepfAllrs, 47, sqlStimth07);
					appVars.getDBObject(sqlgovepfAllrs, 48, sqlStimth08);
					appVars.getDBObject(sqlgovepfAllrs, 49, sqlStimth09);
					appVars.getDBObject(sqlgovepfAllrs, 50, sqlStimth10);
					appVars.getDBObject(sqlgovepfAllrs, 51, sqlStimth11);
					appVars.getDBObject(sqlgovepfAllrs, 52, sqlStimth12);
					appVars.getDBObject(sqlgovepfAllrs, 53, sqlStamth01);
					appVars.getDBObject(sqlgovepfAllrs, 54, sqlStamth02);
					appVars.getDBObject(sqlgovepfAllrs, 55, sqlStamth03);
					appVars.getDBObject(sqlgovepfAllrs, 56, sqlStamth04);
					appVars.getDBObject(sqlgovepfAllrs, 57, sqlStamth05);
					appVars.getDBObject(sqlgovepfAllrs, 58, sqlStamth06);
					appVars.getDBObject(sqlgovepfAllrs, 59, sqlStamth07);
					appVars.getDBObject(sqlgovepfAllrs, 60, sqlStamth08);
					appVars.getDBObject(sqlgovepfAllrs, 61, sqlStamth09);
					appVars.getDBObject(sqlgovepfAllrs, 62, sqlStamth10);
					appVars.getDBObject(sqlgovepfAllrs, 63, sqlStamth11);
					appVars.getDBObject(sqlgovepfAllrs, 64, sqlStamth12);
					appVars.getDBObject(sqlgovepfAllrs, 65, sqlStbmthg01);
					appVars.getDBObject(sqlgovepfAllrs, 66, sqlStbmthg02);
					appVars.getDBObject(sqlgovepfAllrs, 67, sqlStbmthg03);
					appVars.getDBObject(sqlgovepfAllrs, 68, sqlStbmthg04);
					appVars.getDBObject(sqlgovepfAllrs, 69, sqlStbmthg05);
					appVars.getDBObject(sqlgovepfAllrs, 70, sqlStbmthg06);
					appVars.getDBObject(sqlgovepfAllrs, 71, sqlStbmthg07);
					appVars.getDBObject(sqlgovepfAllrs, 72, sqlStbmthg08);
					appVars.getDBObject(sqlgovepfAllrs, 73, sqlStbmthg09);
					appVars.getDBObject(sqlgovepfAllrs, 74, sqlStbmthg10);
					appVars.getDBObject(sqlgovepfAllrs, 75, sqlStbmthg11);
					appVars.getDBObject(sqlgovepfAllrs, 76, sqlStbmthg12);
					appVars.getDBObject(sqlgovepfAllrs, 77, sqlStsmth01);
					appVars.getDBObject(sqlgovepfAllrs, 78, sqlStsmth02);
					appVars.getDBObject(sqlgovepfAllrs, 79, sqlStsmth03);
					appVars.getDBObject(sqlgovepfAllrs, 80, sqlStsmth04);
					appVars.getDBObject(sqlgovepfAllrs, 81, sqlStsmth05);
					appVars.getDBObject(sqlgovepfAllrs, 82, sqlStsmth06);
					appVars.getDBObject(sqlgovepfAllrs, 83, sqlStsmth07);
					appVars.getDBObject(sqlgovepfAllrs, 84, sqlStsmth08);
					appVars.getDBObject(sqlgovepfAllrs, 85, sqlStsmth09);
					appVars.getDBObject(sqlgovepfAllrs, 86, sqlStsmth10);
					appVars.getDBObject(sqlgovepfAllrs, 87, sqlStsmth11);
					appVars.getDBObject(sqlgovepfAllrs, 88, sqlStsmth12);
					appVars.getDBObject(sqlgovepfAllrs, 89, sqlStpmth01);
					appVars.getDBObject(sqlgovepfAllrs, 90, sqlStpmth02);
					appVars.getDBObject(sqlgovepfAllrs, 91, sqlStpmth03);
					appVars.getDBObject(sqlgovepfAllrs, 92, sqlStpmth04);
					appVars.getDBObject(sqlgovepfAllrs, 93, sqlStpmth05);
					appVars.getDBObject(sqlgovepfAllrs, 94, sqlStpmth06);
					appVars.getDBObject(sqlgovepfAllrs, 95, sqlStpmth07);
					appVars.getDBObject(sqlgovepfAllrs, 96, sqlStpmth08);
					appVars.getDBObject(sqlgovepfAllrs, 97, sqlStpmth09);
					appVars.getDBObject(sqlgovepfAllrs, 98, sqlStpmth10);
					appVars.getDBObject(sqlgovepfAllrs, 99, sqlStpmth11);
					appVars.getDBObject(sqlgovepfAllrs, 100, sqlStpmth12);
					appVars.getDBObject(sqlgovepfAllrs, 101, sqlStraamt01);
					appVars.getDBObject(sqlgovepfAllrs, 102, sqlStraamt02);
					appVars.getDBObject(sqlgovepfAllrs, 103, sqlStraamt03);
					appVars.getDBObject(sqlgovepfAllrs, 104, sqlStraamt04);
					appVars.getDBObject(sqlgovepfAllrs, 105, sqlStraamt05);
					appVars.getDBObject(sqlgovepfAllrs, 106, sqlStraamt06);
					appVars.getDBObject(sqlgovepfAllrs, 107, sqlStraamt07);
					appVars.getDBObject(sqlgovepfAllrs, 108, sqlStraamt08);
					appVars.getDBObject(sqlgovepfAllrs, 109, sqlStraamt09);
					appVars.getDBObject(sqlgovepfAllrs, 110, sqlStraamt10);
					appVars.getDBObject(sqlgovepfAllrs, 111, sqlStraamt11);
					appVars.getDBObject(sqlgovepfAllrs, 112, sqlStraamt12);
					appVars.getDBObject(sqlgovepfAllrs, 113, sqlStldmthg01);
					appVars.getDBObject(sqlgovepfAllrs, 114, sqlStldmthg02);
					appVars.getDBObject(sqlgovepfAllrs, 115, sqlStldmthg03);
					appVars.getDBObject(sqlgovepfAllrs, 116, sqlStldmthg04);
					appVars.getDBObject(sqlgovepfAllrs, 117, sqlStldmthg05);
					appVars.getDBObject(sqlgovepfAllrs, 118, sqlStldmthg06);
					appVars.getDBObject(sqlgovepfAllrs, 119, sqlStldmthg07);
					appVars.getDBObject(sqlgovepfAllrs, 120, sqlStldmthg08);
					appVars.getDBObject(sqlgovepfAllrs, 121, sqlStldmthg09);
					appVars.getDBObject(sqlgovepfAllrs, 122, sqlStldmthg10);
					appVars.getDBObject(sqlgovepfAllrs, 123, sqlStldmthg11);
					appVars.getDBObject(sqlgovepfAllrs, 124, sqlStldmthg12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		else {
			sqlerrorflag = false;
			try {
				if (sqlgovepfrs.next()) {
					appVars.getDBObject(sqlgovepfrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfrs, 6, sqlStssect);
					appVars.getDBObject(sqlgovepfrs, 7, sqlRegister);
					appVars.getDBObject(sqlgovepfrs, 8, sqlCntbranch);
					appVars.getDBObject(sqlgovepfrs, 9, sqlCrpstat);
					appVars.getDBObject(sqlgovepfrs, 10, sqlCnpstat);
					appVars.getDBObject(sqlgovepfrs, 11, sqlCnttype);
					appVars.getDBObject(sqlgovepfrs, 12, sqlCrtable);
					appVars.getDBObject(sqlgovepfrs, 13, sqlAcctccy);
					appVars.getDBObject(sqlgovepfrs, 14, sqlCntcurr);
					appVars.getDBObject(sqlgovepfrs, 15, sqlParind);
					appVars.getDBObject(sqlgovepfrs, 16, sqlBillfreq);
					appVars.getDBObject(sqlgovepfrs, 17, sqlStcmth01);
					appVars.getDBObject(sqlgovepfrs, 18, sqlStcmth02);
					appVars.getDBObject(sqlgovepfrs, 19, sqlStcmth03);
					appVars.getDBObject(sqlgovepfrs, 20, sqlStcmth04);
					appVars.getDBObject(sqlgovepfrs, 21, sqlStcmth05);
					appVars.getDBObject(sqlgovepfrs, 22, sqlStcmth06);
					appVars.getDBObject(sqlgovepfrs, 23, sqlStcmth07);
					appVars.getDBObject(sqlgovepfrs, 24, sqlStcmth08);
					appVars.getDBObject(sqlgovepfrs, 25, sqlStcmth09);
					appVars.getDBObject(sqlgovepfrs, 26, sqlStcmth10);
					appVars.getDBObject(sqlgovepfrs, 27, sqlStcmth11);
					appVars.getDBObject(sqlgovepfrs, 28, sqlStcmth12);
					appVars.getDBObject(sqlgovepfrs, 29, sqlStlmth01);
					appVars.getDBObject(sqlgovepfrs, 30, sqlStlmth02);
					appVars.getDBObject(sqlgovepfrs, 31, sqlStlmth03);
					appVars.getDBObject(sqlgovepfrs, 32, sqlStlmth04);
					appVars.getDBObject(sqlgovepfrs, 33, sqlStlmth05);
					appVars.getDBObject(sqlgovepfrs, 34, sqlStlmth06);
					appVars.getDBObject(sqlgovepfrs, 35, sqlStlmth07);
					appVars.getDBObject(sqlgovepfrs, 36, sqlStlmth08);
					appVars.getDBObject(sqlgovepfrs, 37, sqlStlmth09);
					appVars.getDBObject(sqlgovepfrs, 38, sqlStlmth10);
					appVars.getDBObject(sqlgovepfrs, 39, sqlStlmth11);
					appVars.getDBObject(sqlgovepfrs, 40, sqlStlmth12);
					appVars.getDBObject(sqlgovepfrs, 41, sqlStimth01);
					appVars.getDBObject(sqlgovepfrs, 42, sqlStimth02);
					appVars.getDBObject(sqlgovepfrs, 43, sqlStimth03);
					appVars.getDBObject(sqlgovepfrs, 44, sqlStimth04);
					appVars.getDBObject(sqlgovepfrs, 45, sqlStimth05);
					appVars.getDBObject(sqlgovepfrs, 46, sqlStimth06);
					appVars.getDBObject(sqlgovepfrs, 47, sqlStimth07);
					appVars.getDBObject(sqlgovepfrs, 48, sqlStimth08);
					appVars.getDBObject(sqlgovepfrs, 49, sqlStimth09);
					appVars.getDBObject(sqlgovepfrs, 50, sqlStimth10);
					appVars.getDBObject(sqlgovepfrs, 51, sqlStimth11);
					appVars.getDBObject(sqlgovepfrs, 52, sqlStimth12);
					appVars.getDBObject(sqlgovepfrs, 53, sqlStamth01);
					appVars.getDBObject(sqlgovepfrs, 54, sqlStamth02);
					appVars.getDBObject(sqlgovepfrs, 55, sqlStamth03);
					appVars.getDBObject(sqlgovepfrs, 56, sqlStamth04);
					appVars.getDBObject(sqlgovepfrs, 57, sqlStamth05);
					appVars.getDBObject(sqlgovepfrs, 58, sqlStamth06);
					appVars.getDBObject(sqlgovepfrs, 59, sqlStamth07);
					appVars.getDBObject(sqlgovepfrs, 60, sqlStamth08);
					appVars.getDBObject(sqlgovepfrs, 61, sqlStamth09);
					appVars.getDBObject(sqlgovepfrs, 62, sqlStamth10);
					appVars.getDBObject(sqlgovepfrs, 63, sqlStamth11);
					appVars.getDBObject(sqlgovepfrs, 64, sqlStamth12);
					appVars.getDBObject(sqlgovepfrs, 65, sqlStbmthg01);
					appVars.getDBObject(sqlgovepfrs, 66, sqlStbmthg02);
					appVars.getDBObject(sqlgovepfrs, 67, sqlStbmthg03);
					appVars.getDBObject(sqlgovepfrs, 68, sqlStbmthg04);
					appVars.getDBObject(sqlgovepfrs, 69, sqlStbmthg05);
					appVars.getDBObject(sqlgovepfrs, 70, sqlStbmthg06);
					appVars.getDBObject(sqlgovepfrs, 71, sqlStbmthg07);
					appVars.getDBObject(sqlgovepfrs, 72, sqlStbmthg08);
					appVars.getDBObject(sqlgovepfrs, 73, sqlStbmthg09);
					appVars.getDBObject(sqlgovepfrs, 74, sqlStbmthg10);
					appVars.getDBObject(sqlgovepfrs, 75, sqlStbmthg11);
					appVars.getDBObject(sqlgovepfrs, 76, sqlStbmthg12);
					appVars.getDBObject(sqlgovepfrs, 77, sqlStsmth01);
					appVars.getDBObject(sqlgovepfrs, 78, sqlStsmth02);
					appVars.getDBObject(sqlgovepfrs, 79, sqlStsmth03);
					appVars.getDBObject(sqlgovepfrs, 80, sqlStsmth04);
					appVars.getDBObject(sqlgovepfrs, 81, sqlStsmth05);
					appVars.getDBObject(sqlgovepfrs, 82, sqlStsmth06);
					appVars.getDBObject(sqlgovepfrs, 83, sqlStsmth07);
					appVars.getDBObject(sqlgovepfrs, 84, sqlStsmth08);
					appVars.getDBObject(sqlgovepfrs, 85, sqlStsmth09);
					appVars.getDBObject(sqlgovepfrs, 86, sqlStsmth10);
					appVars.getDBObject(sqlgovepfrs, 87, sqlStsmth11);
					appVars.getDBObject(sqlgovepfrs, 88, sqlStsmth12);
					appVars.getDBObject(sqlgovepfrs, 89, sqlStpmth01);
					appVars.getDBObject(sqlgovepfrs, 90, sqlStpmth02);
					appVars.getDBObject(sqlgovepfrs, 91, sqlStpmth03);
					appVars.getDBObject(sqlgovepfrs, 92, sqlStpmth04);
					appVars.getDBObject(sqlgovepfrs, 93, sqlStpmth05);
					appVars.getDBObject(sqlgovepfrs, 94, sqlStpmth06);
					appVars.getDBObject(sqlgovepfrs, 95, sqlStpmth07);
					appVars.getDBObject(sqlgovepfrs, 96, sqlStpmth08);
					appVars.getDBObject(sqlgovepfrs, 97, sqlStpmth09);
					appVars.getDBObject(sqlgovepfrs, 98, sqlStpmth10);
					appVars.getDBObject(sqlgovepfrs, 99, sqlStpmth11);
					appVars.getDBObject(sqlgovepfrs, 100, sqlStpmth12);
					appVars.getDBObject(sqlgovepfrs, 101, sqlStraamt01);
					appVars.getDBObject(sqlgovepfrs, 102, sqlStraamt02);
					appVars.getDBObject(sqlgovepfrs, 103, sqlStraamt03);
					appVars.getDBObject(sqlgovepfrs, 104, sqlStraamt04);
					appVars.getDBObject(sqlgovepfrs, 105, sqlStraamt05);
					appVars.getDBObject(sqlgovepfrs, 106, sqlStraamt06);
					appVars.getDBObject(sqlgovepfrs, 107, sqlStraamt07);
					appVars.getDBObject(sqlgovepfrs, 108, sqlStraamt08);
					appVars.getDBObject(sqlgovepfrs, 109, sqlStraamt09);
					appVars.getDBObject(sqlgovepfrs, 110, sqlStraamt10);
					appVars.getDBObject(sqlgovepfrs, 111, sqlStraamt11);
					appVars.getDBObject(sqlgovepfrs, 112, sqlStraamt12);
					appVars.getDBObject(sqlgovepfrs, 113, sqlStldmthg01);
					appVars.getDBObject(sqlgovepfrs, 114, sqlStldmthg02);
					appVars.getDBObject(sqlgovepfrs, 115, sqlStldmthg03);
					appVars.getDBObject(sqlgovepfrs, 116, sqlStldmthg04);
					appVars.getDBObject(sqlgovepfrs, 117, sqlStldmthg05);
					appVars.getDBObject(sqlgovepfrs, 118, sqlStldmthg06);
					appVars.getDBObject(sqlgovepfrs, 119, sqlStldmthg07);
					appVars.getDBObject(sqlgovepfrs, 120, sqlStldmthg08);
					appVars.getDBObject(sqlgovepfrs, 121, sqlStldmthg09);
					appVars.getDBObject(sqlgovepfrs, 122, sqlStldmthg10);
					appVars.getDBObject(sqlgovepfrs, 123, sqlStldmthg11);
					appVars.getDBObject(sqlgovepfrs, 124, sqlStldmthg12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		wsaaSqlKeep.set(sqlGoverec);
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		if (isNE(wsaaFirstRecord,"Y")) {
			getHeading5000();
			newPage6000();
			printBfReassurance7000();
			printReasaurance8000();
			printAfReassurance9000();
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (isEQ(wsaaFirstRecord,"Y")) {
			wsaaFirstRecord = "N";
		}
		else {
			if (isNE(wsaaStsect,sqlStsect)
			|| isNE(wsaaReg,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaCompany,sqlChdrcoy)
			|| isNE(wsaaStssect,sqlStssect)
			|| isNE(wsaaBranch,sqlCntbranch)) {
				getHeading5000();
				newPage6000();
				printBfReassurance7000();
				printReasaurance8000();
				printAfReassurance9000();
				a000InitialWs();
			}
		}
		wsaaBillingFreq.set(sqlBillfreq);
		for (mth.set(1); !(isGT(mth,pj517par.acctmnth)); mth.add(1)){
			wsaaStcmthc.add(wsaaStcmth[mth.toInt()]);
			wsaaStlmthc.add(wsaaStlmth[mth.toInt()]);
			wsaaStimthc.add(wsaaStimth[mth.toInt()]);
			wsaaStsmthc.add(wsaaStsmth[mth.toInt()]);
			wsaaStamthc.add(wsaaStamth[mth.toInt()]);
			compute(wsaaAnnlBasic, 3).setRounded(mult(wsaaStbmthg[mth.toInt()],wsaaBillfreqN));
			compute(wsaaAnnlLoaded, 3).setRounded(mult(wsaaStldmth[mth.toInt()],wsaaBillfreqN));
			compute(wsaaStbmthc, 2).add(add(wsaaAnnlBasic,wsaaAnnlLoaded));
			wsaaStraamtc.add(wsaaStraamt[mth.toInt()]);
		}
		if (isNE(sqlCntcurr,sqlAcctccy)) {
			a200ReadT3629();
			compute(wsaaStimthc, 8).setRounded(mult(wsaaStimthc,wsaaScrate));
			compute(wsaaStsmthc, 8).setRounded(mult(wsaaStsmthc,wsaaScrate));
			compute(wsaaStamthc, 8).setRounded(mult(wsaaStamthc,wsaaScrate));
			compute(wsaaStraamtc, 8).setRounded(mult(wsaaStraamtc,wsaaScrate));
			compute(wsaaStbmthc, 8).setRounded(mult(wsaaStbmthc,wsaaScrate));
		}
		if (isNE(sqlBillfreq,"00")) {
			wsaaRegNofpol.add(wsaaStcmthc);
			wsaaRegNoflif.add(wsaaStlmthc);
			wsaaRegSi.add(wsaaStimthc);
			wsaaRegAnnuity.add(wsaaStamthc);
			wsaaRegAprem.add(wsaaStbmthc);
			wsaaRegSprem.add(wsaaStsmthc);
			if (isNE(wsaaStraamtc,ZERO)) {
				wsaaRiRegNofpol.add(wsaaStcmthc);
				wsaaRiRegNoflif.add(wsaaStlmthc);
				if (isNE(wsaaStimthc,ZERO)) {
					wsaaRiRegSi.add(wsaaStraamtc);
					if (isNE(wsaaStbmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStimthc)),wsaaStbmthc));
						wsaaRiRegAprem.add(wsaaRiStbamt);
					}
					if (isNE(wsaaStsmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStimthc)),wsaaStsmthc));
						wsaaRiRegSprem.add(wsaaRiStbamt);
					}
				}
				else {
					wsaaRiRegAnnuity.add(wsaaStraamtc);
					if (isNE(wsaaStbmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStamthc)),wsaaStbmthc));
						wsaaRiRegAprem.add(wsaaRiStbamt);
					}
					if (isNE(wsaaStsmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStamthc)),wsaaStsmthc));
						wsaaRiRegSprem.add(wsaaRiStbamt);
					}
				}
			}
		}
		if (isEQ(sqlBillfreq,"00")) {
			wsaaSglNofpol.add(wsaaStcmthc);
			wsaaSglNoflif.add(wsaaStlmthc);
			wsaaSglSi.add(wsaaStimthc);
			wsaaSglAnnuity.add(wsaaStamthc);
			wsaaSglAprem.add(wsaaStbmthc);
			wsaaSglSprem.add(wsaaStsmthc);
			if (isNE(wsaaStraamtc,ZERO)) {
				wsaaRiSglNofpol.add(wsaaStcmthc);
				wsaaRiSglNoflif.add(wsaaStlmthc);
				if (isNE(wsaaStimthc,ZERO)) {
					wsaaRiSglSi.add(wsaaStraamtc);
					if (isNE(wsaaStsmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStimthc)),wsaaStsmthc));
						wsaaRiSglSprem.add(wsaaRiStbamt);
					}
				}
				else {
					wsaaRiSglAnnuity.add(wsaaStraamtc);
					if (isNE(wsaaStsmthc,ZERO)) {
						compute(wsaaRiStbamt, 3).setRounded(mult((div(wsaaStraamtc,wsaaStamthc)),wsaaStsmthc));
						wsaaRiSglSprem.add(wsaaRiStbamt);
					}
				}
			}
		}
		wsaaStsect.set(sqlStsect);
		wsaaStssect.set(sqlStssect);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaCompany.set(sqlChdrcoy);
		wsaaBranch.set(sqlCntbranch);
		wsaaReg.set(sqlRegister);
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaStcmthc);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaStlmthc);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(wsaaStimthc);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(wsaaStamthc);
		callContot001();
		contotrec.totno.set(ct07);
		contotrec.totval.set(wsaaStbmthc);
		callContot001();
		contotrec.totno.set(ct08);
		contotrec.totval.set(wsaaStsmthc);
		callContot001();
		contotrec.totno.set(ct09);
		contotrec.totval.set(wsaaStraamtc);
		callContot001();
		wsaaStcmthc.set(ZERO);
		wsaaStlmthc.set(ZERO);
		wsaaStimthc.set(ZERO);
		wsaaStsmthc.set(ZERO);
		wsaaStamthc.set(ZERO);
		wsaaStbmthc.set(ZERO);
		wsaaStraamtc.set(ZERO);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		if (selectAll.isTrue()) {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfAllconn, sqlgovepfAllps, sqlgovepfAllrs);
		}
		else {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfconn, sqlgovepfps, sqlgovepfrs);
		}
		rj527.close();
		/*EXIT*/
	}

protected void getHeading5000()
	{
		setUpHeadingCompany5010();
		setUpHeadingCurrency5050();
	}

protected void setUpHeadingCompany5010()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(wsaaCompany);
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingCurrency5050()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(wsaaAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		acctccy.set(wsaaAcctccy);
		currdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3589);
		descIO.setDescitem(wsaaReg);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		register.set(wsaaReg);
		descrip.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5685);
		descIO.setDescitem(wsaaStsect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stsect.set(wsaaStsect);
		itmdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5684);
		descIO.setDescitem(wsaaStssect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stssect.set(wsaaStssect);
		longdesc.set(descIO.getLongdesc());
	}

protected void newPage6000()
	{
		start6010();
	}

protected void start6010()
	{
		rj527.printRj527h01(rj527H01, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		rj527.printRj527h02(rj527H02, indicArea);
		rj527.printRj527d02(rj527D02, indicArea);
		indOn.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(13);
		rj527.printRj527h03(rj527H03, indicArea);
	}

protected void printBfReassurance7000()
	{
		start7010();
	}

protected void start7010()
	{
		indOn.setTrue(14);
		indOff.setTrue(15);
		stcmthg.set(wsaaRegNofpol);
		stlmth.set(wsaaRegNoflif);
		compute(repamt01, 3).setRounded(div(wsaaRegSi,1000));
		compute(repamt02, 3).setRounded(div(wsaaRegAnnuity,1000));
		compute(repamt03, 3).setRounded(div(wsaaRegAprem,1000));
		compute(repamt04, 3).setRounded(div(wsaaRegSprem,1000));
		wsaaRegSi.set(repamt01);
		wsaaRegAnnuity.set(repamt02);
		wsaaRegAprem.set(repamt03);
		wsaaRegSprem.set(repamt04);
		rj527.printRj527d01(rj527D01, indicArea);
		indOn.setTrue(15);
		indOff.setTrue(14);
		stcmthg.set(wsaaSglNofpol);
		stlmth.set(wsaaSglNoflif);
		compute(repamt01, 3).setRounded(div(wsaaSglSi,1000));
		compute(repamt02, 3).setRounded(div(wsaaSglAnnuity,1000));
		compute(repamt03, 3).setRounded(div(wsaaSglAprem,1000));
		compute(repamt04, 3).setRounded(div(wsaaSglSprem,1000));
		wsaaSglSi.set(repamt01);
		wsaaSglAnnuity.set(repamt02);
		wsaaSglAprem.set(repamt03);
		wsaaSglSprem.set(repamt04);
		rj527.printRj527d01(rj527D01, indicArea);
	}

protected void printReasaurance8000()
	{
		start8010();
	}

protected void start8010()
	{
		rj527.printRj527d02(rj527D02, indicArea);
		indOn.setTrue(12);
		indOff.setTrue(11);
		indOff.setTrue(13);
		rj527.printRj527h03(rj527H03, indicArea);
		indOn.setTrue(14);
		indOff.setTrue(15);
		stcmthg.set(wsaaRiRegNofpol);
		stlmth.set(wsaaRiRegNoflif);
		compute(repamt01, 3).setRounded(div(wsaaRiRegSi,1000));
		compute(repamt02, 3).setRounded(div(wsaaRiRegAnnuity,1000));
		compute(repamt03, 3).setRounded(div(wsaaRiRegAprem,1000));
		compute(repamt04, 3).setRounded(div(wsaaRiRegSprem,1000));
		wsaaRiRegSi.set(repamt01);
		wsaaRiRegAnnuity.set(repamt02);
		wsaaRiRegAprem.set(repamt03);
		wsaaRiRegSprem.set(repamt04);
		rj527.printRj527d01(rj527D01, indicArea);
		indOn.setTrue(15);
		indOff.setTrue(14);
		stcmthg.set(wsaaRiSglNofpol);
		stlmth.set(wsaaRiSglNoflif);
		compute(repamt01, 3).setRounded(div(wsaaRiSglSi,1000));
		compute(repamt02, 3).setRounded(div(wsaaRiSglAnnuity,1000));
		compute(repamt03, 3).setRounded(div(wsaaRiSglAprem,1000));
		compute(repamt04, 3).setRounded(div(wsaaRiSglSprem,1000));
		wsaaRiSglSi.set(repamt01);
		wsaaRiSglAnnuity.set(repamt02);
		wsaaRiSglAprem.set(repamt03);
		wsaaRiSglSprem.set(repamt04);
		rj527.printRj527d01(rj527D01, indicArea);
	}

protected void printAfReassurance9000()
	{
		start9010();
	}

protected void start9010()
	{
		rj527.printRj527d02(rj527D02, indicArea);
		indOn.setTrue(13);
		indOff.setTrue(11);
		indOff.setTrue(12);
		rj527.printRj527h03(rj527H03, indicArea);
		stcmthg.set(0);
		stlmth.set(0);
		repamt01.set(0);
		repamt02.set(0);
		repamt03.set(0);
		repamt04.set(0);
		indOn.setTrue(14);
		indOff.setTrue(15);
		compute(stcmthg, 2).set(sub(wsaaRegNofpol,wsaaRiRegNofpol));
		compute(stlmth, 2).set(sub(wsaaRegNoflif,wsaaRiRegNoflif));
		compute(repamt01, 2).set(sub(wsaaRegSi,wsaaRiRegSi));
		compute(repamt02, 2).set(sub(wsaaRegAnnuity,wsaaRiRegAnnuity));
		compute(repamt03, 2).set(sub(wsaaRegAprem,wsaaRiRegAprem));
		compute(repamt04, 2).set(sub(wsaaRegSprem,wsaaRiRegSprem));
		rj527.printRj527d01(rj527D01, indicArea);
		stcmthg.set(0);
		stlmth.set(0);
		repamt01.set(0);
		repamt02.set(0);
		repamt03.set(0);
		repamt04.set(0);
		indOn.setTrue(15);
		indOff.setTrue(14);
		compute(stcmthg, 2).set(sub(wsaaSglNofpol,wsaaRiSglNofpol));
		compute(stlmth, 2).set(sub(wsaaSglNoflif,wsaaRiSglNoflif));
		compute(repamt01, 2).set(sub(wsaaSglSi,wsaaRiSglSi));
		compute(repamt02, 2).set(sub(wsaaSglAnnuity,wsaaRiSglAnnuity));
		compute(repamt03, 2).set(sub(wsaaSglAprem,wsaaRiSglAprem));
		compute(repamt04, 2).set(sub(wsaaSglSprem,wsaaRiSglSprem));
		rj527.printRj527d01(rj527D01, indicArea);
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void a000InitialWs()
	{
		/*A010-START*/
		wsaaRegNofpol.set(0);
		wsaaRegNoflif.set(0);
		wsaaRegSi.set(0);
		wsaaRegAnnuity.set(0);
		wsaaRegAprem.set(0);
		wsaaRegSprem.set(0);
		wsaaRiRegNofpol.set(0);
		wsaaRiRegNoflif.set(0);
		wsaaRiRegSi.set(0);
		wsaaRiRegAnnuity.set(0);
		wsaaRiRegAprem.set(0);
		wsaaRiRegSprem.set(0);
		wsaaSglNofpol.set(0);
		wsaaSglNoflif.set(0);
		wsaaSglSi.set(0);
		wsaaSglAnnuity.set(0);
		wsaaSglAprem.set(0);
		wsaaSglSprem.set(0);
		wsaaRiSglNofpol.set(0);
		wsaaRiSglNoflif.set(0);
		wsaaRiSglSi.set(0);
		wsaaRiSglAnnuity.set(0);
		wsaaRiSglAprem.set(0);
		wsaaRiSglSprem.set(0);
		/*A090-EXIT*/
	}

protected void a200ReadT3629()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a210();
				}
				case a220CallItemio: {
					a220CallItemio();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a210()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemitem(sqlCntcurr);
		itemIO.setItemtabl(t3629);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
	}

protected void a220CallItemio()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaScrate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaScrate,ZERO)
		|| isGT(wsaaX,7))) {
			if (isGTE(datcon1rec.intDate,t3629rec.frmdate[wsaaX.toInt()])
			&& isLTE(datcon1rec.intDate,t3629rec.todate[wsaaX.toInt()])) {
				wsaaScrate.set(t3629rec.scrate[wsaaX.toInt()]);
			}
			else {
				wsaaX.add(1);
			}
		}

		if (isEQ(wsaaScrate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.a220CallItemio);
			}
			else {
				syserrrec.statuz.set(g418);
				fatalError600();
			}
		}
	}
}
