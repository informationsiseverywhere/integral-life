package com.csc.life.statistics.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GovrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:06
 * Class transformed from GOVRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GovrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 580;
	public FixedLengthStringData govrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData govrpfRecord = govrrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(govrrec);
	public FixedLengthStringData register = DD.reg.copy().isAPartOf(govrrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(govrrec);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(govrrec);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(govrrec);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(govrrec);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(govrrec);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(govrrec);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(govrrec);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(govrrec);
	public PackedDecimalData commyr = DD.commyr.copy().isAPartOf(govrrec);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(govrrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(govrrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(govrrec);
	public PackedDecimalData acctyr = DD.acctyr.copy().isAPartOf(govrrec);
	public PackedDecimalData bfwdc = DD.bfwdc.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth01 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth02 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth03 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth04 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth05 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth06 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth07 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth08 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth09 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth10 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth11 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stcmth12 = DD.stcmth.copy().isAPartOf(govrrec);
	public PackedDecimalData cfwdc = DD.cfwdc.copy().isAPartOf(govrrec);
	public PackedDecimalData bfwdp = DD.bfwdp.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth01 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth02 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth03 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth04 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth05 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth06 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth07 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth08 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth09 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth10 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth11 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stpmth12 = DD.stpmth.copy().isAPartOf(govrrec);
	public PackedDecimalData cfwdp = DD.cfwdp.copy().isAPartOf(govrrec);
	public PackedDecimalData bfwds = DD.bfwds.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth01 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth02 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth03 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth04 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth05 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth06 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth07 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth08 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth09 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth10 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth11 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData stsmth12 = DD.stsmth.copy().isAPartOf(govrrec);
	public PackedDecimalData cfwds = DD.cfwds.copy().isAPartOf(govrrec);
	public PackedDecimalData bfwdi = DD.bfwdi.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth01 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth02 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth03 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth04 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth05 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth06 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth07 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth08 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth09 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth10 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth11 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData stimth12 = DD.stimth.copy().isAPartOf(govrrec);
	public PackedDecimalData cfwdi = DD.cfwdi.copy().isAPartOf(govrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(govrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(govrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(govrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public GovrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for GovrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public GovrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for GovrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public GovrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for GovrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public GovrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("GOVRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"REG, " +
							"CNTBRANCH, " +
							"STFUND, " +
							"STSECT, " +
							"STSSECT, " +
							"BANDAGE, " +
							"BANDSA, " +
							"BANDPRM, " +
							"BANDTRM, " +
							"COMMYR, " +
							"STATCAT, " +
							"PSTATCODE, " +
							"CNTCURR, " +
							"ACCTYR, " +
							"BFWDC, " +
							"STCMTH01, " +
							"STCMTH02, " +
							"STCMTH03, " +
							"STCMTH04, " +
							"STCMTH05, " +
							"STCMTH06, " +
							"STCMTH07, " +
							"STCMTH08, " +
							"STCMTH09, " +
							"STCMTH10, " +
							"STCMTH11, " +
							"STCMTH12, " +
							"CFWDC, " +
							"BFWDP, " +
							"STPMTH01, " +
							"STPMTH02, " +
							"STPMTH03, " +
							"STPMTH04, " +
							"STPMTH05, " +
							"STPMTH06, " +
							"STPMTH07, " +
							"STPMTH08, " +
							"STPMTH09, " +
							"STPMTH10, " +
							"STPMTH11, " +
							"STPMTH12, " +
							"CFWDP, " +
							"BFWDS, " +
							"STSMTH01, " +
							"STSMTH02, " +
							"STSMTH03, " +
							"STSMTH04, " +
							"STSMTH05, " +
							"STSMTH06, " +
							"STSMTH07, " +
							"STSMTH08, " +
							"STSMTH09, " +
							"STSMTH10, " +
							"STSMTH11, " +
							"STSMTH12, " +
							"CFWDS, " +
							"BFWDI, " +
							"STIMTH01, " +
							"STIMTH02, " +
							"STIMTH03, " +
							"STIMTH04, " +
							"STIMTH05, " +
							"STIMTH06, " +
							"STIMTH07, " +
							"STIMTH08, " +
							"STIMTH09, " +
							"STIMTH10, " +
							"STIMTH11, " +
							"STIMTH12, " +
							"CFWDI, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     register,
                                     cntbranch,
                                     statFund,
                                     statSect,
                                     statSubsect,
                                     bandage,
                                     bandsa,
                                     bandprm,
                                     bandtrm,
                                     commyr,
                                     statcat,
                                     pstatcode,
                                     cntcurr,
                                     acctyr,
                                     bfwdc,
                                     stcmth01,
                                     stcmth02,
                                     stcmth03,
                                     stcmth04,
                                     stcmth05,
                                     stcmth06,
                                     stcmth07,
                                     stcmth08,
                                     stcmth09,
                                     stcmth10,
                                     stcmth11,
                                     stcmth12,
                                     cfwdc,
                                     bfwdp,
                                     stpmth01,
                                     stpmth02,
                                     stpmth03,
                                     stpmth04,
                                     stpmth05,
                                     stpmth06,
                                     stpmth07,
                                     stpmth08,
                                     stpmth09,
                                     stpmth10,
                                     stpmth11,
                                     stpmth12,
                                     cfwdp,
                                     bfwds,
                                     stsmth01,
                                     stsmth02,
                                     stsmth03,
                                     stsmth04,
                                     stsmth05,
                                     stsmth06,
                                     stsmth07,
                                     stsmth08,
                                     stsmth09,
                                     stsmth10,
                                     stsmth11,
                                     stsmth12,
                                     cfwds,
                                     bfwdi,
                                     stimth01,
                                     stimth02,
                                     stimth03,
                                     stimth04,
                                     stimth05,
                                     stimth06,
                                     stimth07,
                                     stimth08,
                                     stimth09,
                                     stimth10,
                                     stimth11,
                                     stimth12,
                                     cfwdi,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		register.clear();
  		cntbranch.clear();
  		statFund.clear();
  		statSect.clear();
  		statSubsect.clear();
  		bandage.clear();
  		bandsa.clear();
  		bandprm.clear();
  		bandtrm.clear();
  		commyr.clear();
  		statcat.clear();
  		pstatcode.clear();
  		cntcurr.clear();
  		acctyr.clear();
  		bfwdc.clear();
  		stcmth01.clear();
  		stcmth02.clear();
  		stcmth03.clear();
  		stcmth04.clear();
  		stcmth05.clear();
  		stcmth06.clear();
  		stcmth07.clear();
  		stcmth08.clear();
  		stcmth09.clear();
  		stcmth10.clear();
  		stcmth11.clear();
  		stcmth12.clear();
  		cfwdc.clear();
  		bfwdp.clear();
  		stpmth01.clear();
  		stpmth02.clear();
  		stpmth03.clear();
  		stpmth04.clear();
  		stpmth05.clear();
  		stpmth06.clear();
  		stpmth07.clear();
  		stpmth08.clear();
  		stpmth09.clear();
  		stpmth10.clear();
  		stpmth11.clear();
  		stpmth12.clear();
  		cfwdp.clear();
  		bfwds.clear();
  		stsmth01.clear();
  		stsmth02.clear();
  		stsmth03.clear();
  		stsmth04.clear();
  		stsmth05.clear();
  		stsmth06.clear();
  		stsmth07.clear();
  		stsmth08.clear();
  		stsmth09.clear();
  		stsmth10.clear();
  		stsmth11.clear();
  		stsmth12.clear();
  		cfwds.clear();
  		bfwdi.clear();
  		stimth01.clear();
  		stimth02.clear();
  		stimth03.clear();
  		stimth04.clear();
  		stimth05.clear();
  		stimth06.clear();
  		stimth07.clear();
  		stimth08.clear();
  		stimth09.clear();
  		stimth10.clear();
  		stimth11.clear();
  		stimth12.clear();
  		cfwdi.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getGovrrec() {
  		return govrrec;
	}

	public FixedLengthStringData getGovrpfRecord() {
  		return govrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setGovrrec(what);
	}

	public void setGovrrec(Object what) {
  		this.govrrec.set(what);
	}

	public void setGovrpfRecord(Object what) {
  		this.govrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(govrrec.getLength());
		result.set(govrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}