package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:27
 * Description:
 * Copybook name: STTRREVKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Sttrrevkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData sttrrevFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData sttrrevKey = new FixedLengthStringData(64).isAPartOf(sttrrevFileKey, 0, REDEFINE);
  	public FixedLengthStringData sttrrevChdrcoy = new FixedLengthStringData(1).isAPartOf(sttrrevKey, 0);
  	public FixedLengthStringData sttrrevChdrnum = new FixedLengthStringData(8).isAPartOf(sttrrevKey, 1);
  	public PackedDecimalData sttrrevTranno = new PackedDecimalData(5, 0).isAPartOf(sttrrevKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(52).isAPartOf(sttrrevKey, 12, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(sttrrevFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		sttrrevFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}