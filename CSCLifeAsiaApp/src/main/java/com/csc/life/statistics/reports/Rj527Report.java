package com.csc.life.statistics.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RJ527.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rj527Report extends SMARTReportLayout { 

	private FixedLengthStringData acctccy = new FixedLengthStringData(3);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData register = new FixedLengthStringData(3);
	private ZonedDecimalData repamt01 = new ZonedDecimalData(15, 0);
	private ZonedDecimalData repamt02 = new ZonedDecimalData(15, 0);
	private ZonedDecimalData repamt03 = new ZonedDecimalData(15, 0);
	private ZonedDecimalData repamt04 = new ZonedDecimalData(15, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData stcmthg = new ZonedDecimalData(9, 0);
	private ZonedDecimalData stlmth = new ZonedDecimalData(9, 0);
	private FixedLengthStringData stsect = new FixedLengthStringData(2);
	private FixedLengthStringData stssect = new FixedLengthStringData(4);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rj527Report() {
		super();
	}


	/**
	 * Print the XML for Rj527d01
	 */
	public void printRj527d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(15).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stcmthg.setFieldName("stcmthg");
		stcmthg.setInternal(subString(recordData, 1, 9));
		stlmth.setFieldName("stlmth");
		stlmth.setInternal(subString(recordData, 10, 9));
		repamt01.setFieldName("repamt01");
		repamt01.setInternal(subString(recordData, 19, 15));
		repamt02.setFieldName("repamt02");
		repamt02.setInternal(subString(recordData, 34, 15));
		repamt03.setFieldName("repamt03");
		repamt03.setInternal(subString(recordData, 49, 15));
		repamt04.setFieldName("repamt04");
		repamt04.setInternal(subString(recordData, 64, 15));
		printLayout("Rj527d01",			// Record name
			new BaseData[]{			// Fields:
				stcmthg,
				stlmth,
				repamt01,
				repamt02,
				repamt03,
				repamt04
			}
			, new Object[] {			// indicators
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind15", indicArea.charAt(15)}
			}
		);

	}

	/**
	 * Print the XML for Rj527d02
	 */
	public void printRj527d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rj527d02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rj527h01
	 */
	public void printRj527h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		acctmonth.setFieldName("acctmonth");
		acctmonth.setInternal(subString(recordData, 1, 2));
		acctyear.setFieldName("acctyear");
		acctyear.setInternal(subString(recordData, 3, 4));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 7, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 8, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 38, 10));
		acctccy.setFieldName("acctccy");
		acctccy.setInternal(subString(recordData, 48, 3));
		currdesc.setFieldName("currdesc");
		currdesc.setInternal(subString(recordData, 51, 30));
		time.setFieldName("time");
		time.set(getTime());
		register.setFieldName("register");
		register.setInternal(subString(recordData, 81, 3));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 84, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		stsect.setFieldName("stsect");
		stsect.setInternal(subString(recordData, 114, 2));
		itmdesc.setFieldName("itmdesc");
		itmdesc.setInternal(subString(recordData, 116, 30));
		stssect.setFieldName("stssect");
		stssect.setInternal(subString(recordData, 146, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 150, 30));
		printLayout("Rj527h01",			// Record name
			new BaseData[]{			// Fields:
				acctmonth,
				acctyear,
				company,
				companynm,
				sdate,
				acctccy,
				currdesc,
				time,
				register,
				descrip,
				pagnbr,
				stsect,
				itmdesc,
				stssect,
				longdesc
			}
		);

		currentPrintLine.set(11);
	}

	/**
	 * Print the XML for Rj527h02
	 */
	public void printRj527h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rj527h02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

		currentPrintLine.add(2);
	}

	/**
	 * Print the XML for Rj527h03
	 */
	public void printRj527h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(13).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);
		printLayout("Rj527h03",			// Record name
			new BaseData[]{			// Fields:
			}
			, new Object[] {			// indicators
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind12", indicArea.charAt(12)},
				new Object[]{"ind13", indicArea.charAt(13)}
			}
		);

	}


}
