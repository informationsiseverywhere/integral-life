/*
 * File: Bj518.java
 * Date: 29 August 2009 21:40:07
 * Author: Quipoz Limited
 *
 * Class transformed from BJ518.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.sql.SQLException;

import com.csc.fsu.general.dataaccess.AcmvTableDAM;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.tablestructures.T6640rec;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovtTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.statistics.dataaccess.GvstTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*        Additional Government Statistics Extraction
*        ===========================================
*
*   This batch program is activated by L2NEWSTEXT. It will read
*   BAKXPF and for each BAKX it will read all its ACMV records.
*   For each ACMV record read, it will check T5645 to see if the
*   subaccount code and type is specified. If this is the
*   required record, then a GVST record will be created. GVST is
*   a new statistical file which will be used later for updating
*   to the new accumulation files, GVAH and GVAC. These two new files
*   are mainly used for the accounting related statutory reports.
*
*   Initialise
*     Read T5645 using BJ518.
*     Override BAKXPF and use SQL to read BAKXPF.
*     Select all records that falls within the accounting year
*     and accounting month specified in the parameter prompt.
*
*    Perform    Until End of File
*
*      Edit
*       - Fetch the SQL record
*
*      Update
*       - Read ACMV using the key values from BAKX
*       - For each ACMV record, search T5645 for the subaccount
*         code and type. If not found, read next ACMV.
*       - For each valid ACMV record, read contract header and
*         coverage file for required information. Then, update
*         GVST.
*
*      Read next primary file record
*
*    End Perform
*
* Multi-Thread Batch Environment Notes:
*
*   1.  This program must follow B0321 not B0236.
*   2.  The BAKX file read has the name 'BAKX' + XX + 9999
*       where 'XX' is the first 2 chars of BSSC-SYSTEM-PARAM04
*       and 9999 is the last four digits of the schedule number.
*       This naming convention is used to allow this program
*       to be used in multiple processes across multiple
*       companies in a single schedule. Regular archiving of
*       these files will avoid conflict with the truncation
*       of the schedule number and unique use of system param
*       will avoid conflict across companies and/or processes.
*
*   Control totals:
*     01  -  No. of BAKX records read
*     02  -  No. of ACMV read
*     03  -  Total of first year prem
*     04  -  Total of renenwal prem
*     05  -  Total of single prem
*     06  -  Total of initial comm
*     07  -  Total of renewal comm
*     08  -  Total of SP comm
*     09  -  Total of RI ceded Prem
*     10  -  Total of RI ceded Comm
*     11  -  Total of Terminl bonus
*     12  -  Total of Rev bonus
*     13  -  Total of Extra bonus
*     14  -  Total of Interim bonus
*     15  -  Total of Other benefits
*     16  -  Total of Pending Death benefit
*     17  -  Total of Maturity benefit
*     18  -  Total of Bonus Surrender
*     19  -  Total of Clm Recovery
*     20  -  Total of Advance Premium
*     21  -  Total of Dividend Allocated
*     22  -  Total of Dividend Interest
*     23  -  Total of Approved Death benefit
*     24  -  Total of Death benefit adjustment
*     25  -  Total of Death benefit interest
*
***********************************************************************
*                                                                     *
* </pre>
*/
public class Bj518 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlbakxCursorrs = null;
	private java.sql.PreparedStatement sqlbakxCursorps = null;
	private java.sql.Connection sqlbakxCursorconn = null;
	private String sqlbakxCursor = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ518");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaAcctmnth = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaAcctyear = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRdocnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaRiskCommDate = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaStatFund = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaStatSect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStatSubsect = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBonusInd = new FixedLengthStringData(1);
		/* WSAA-CRRCD */
	private ZonedDecimalData wsaaCommYear = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaCommYr = new FixedLengthStringData(8).isAPartOf(wsaaCommYear, 0, REDEFINE);
	private ZonedDecimalData wsaaCommyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaCommYr, 0).setUnsigned();

	private FixedLengthStringData wsaaRecFound = new FixedLengthStringData(1).init("Y");
	private Validator recFound = new Validator(wsaaRecFound, "Y");

	private FixedLengthStringData wsaaAcctLevel = new FixedLengthStringData(1);
	private Validator compLevelAcc = new Validator(wsaaAcctLevel, "Y");

	private FixedLengthStringData wsaaTransaction = new FixedLengthStringData(4);
	private Validator maturity = new Validator(wsaaTransaction, "T542");
	private Validator maturityReversal = new Validator(wsaaTransaction, "T543");
	private Validator deathClaimAdjustment = new Validator(wsaaTransaction, "T671");
	private Validator pendingDeathClaim = new Validator(wsaaTransaction, "T668");
	private Validator approvedDeathClaim = new Validator(wsaaTransaction, "T669");
	private Validator premiumCollection = new Validator(wsaaTransaction, "B522");

	private FixedLengthStringData wsaaCoverageKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaCoverageKey, 0);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaCoverageKey, 8);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaCoverageKey, 10);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaCoverageKey, 12);
	private FixedLengthStringData wsaaPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaCoverageKey, 14);

	private FixedLengthStringData wsaaPrevCoverageKey = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaPrevCoverageKey, 0);
	private FixedLengthStringData wsaaPrevLife = new FixedLengthStringData(2).isAPartOf(wsaaPrevCoverageKey, 8);
	private FixedLengthStringData wsaaPrevCoverage = new FixedLengthStringData(2).isAPartOf(wsaaPrevCoverageKey, 10);
	private FixedLengthStringData wsaaPrevRider = new FixedLengthStringData(2).isAPartOf(wsaaPrevCoverageKey, 12);
	private FixedLengthStringData wsaaPrevPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaPrevCoverageKey, 14);
	private PackedDecimalData wsaaT5645Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5645Ix = new PackedDecimalData(5, 0);
	//private int wsaaT5645Size = 99;
	//ILIFE-2628 fixed--Array size increased
	private int wsaaT5645Size = 1000;

	private FixedLengthStringData wsaaT5645 = new FixedLengthStringData(4000);
	private FixedLengthStringData[] wsaaT5645Rec = FLSArrayPartOfStructure(1000, 4, wsaaT5645, 0);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Rec, 0);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Rec, 2);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaBakxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaBakxFn, 0, FILLER).init("BAKX");
	private FixedLengthStringData wsaaBakxRunid = new FixedLengthStringData(2).isAPartOf(wsaaBakxFn, 4);
	private ZonedDecimalData wsaaBakxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaBakxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler2 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler4 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler6 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler6, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler6, 8);

		/* SQL-BAKXPF */
	private FixedLengthStringData sqlBakxrec = new FixedLengthStringData(17);
	private FixedLengthStringData sqlBatccoy = new FixedLengthStringData(1).isAPartOf(sqlBakxrec, 0);
	private FixedLengthStringData sqlBatcbrn = new FixedLengthStringData(2).isAPartOf(sqlBakxrec, 1);
	private PackedDecimalData sqlBatcactyr = new PackedDecimalData(4, 0).isAPartOf(sqlBakxrec, 3);
	private PackedDecimalData sqlBatcactmn = new PackedDecimalData(2, 0).isAPartOf(sqlBakxrec, 6);
	private FixedLengthStringData sqlBatctrcde = new FixedLengthStringData(4).isAPartOf(sqlBakxrec, 8);
	private FixedLengthStringData sqlBatcbatch = new FixedLengthStringData(5).isAPartOf(sqlBakxrec, 12);
		/* ERRORS */
	private String esql = "ESQL";
	private String f294 = "F294";
	private String h791 = "H791";
	private String acmvrec = "ACMVREC";
	private String chdrlnbrec = "CHDRLNBREC";
	private String covrrec = "COVRREC";
	private String covtrec = "COVTREC";
	private String gvstrec = "GVSTREC";
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String t5645 = "T5645";
	private String t5687 = "T5687";
	private String t5540 = "T5540";
	private String t6640 = "T6640";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private int ct08 = 8;
	private int ct09 = 9;
	private int ct10 = 10;
	private int ct11 = 11;
	private int ct12 = 12;
	private int ct13 = 13;
	private int ct14 = 14;
	private int ct15 = 15;
	private int ct16 = 16;
	private int ct17 = 17;
	private int ct18 = 18;
	private int ct19 = 19;
	private int ct20 = 20;
	private int ct21 = 21;
	private int ct22 = 22;
	private int ct23 = 23;
	private int ct24 = 24;
	private int ct25 = 25;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Subsidiary account movement be batch key*/
	private AcmvTableDAM acmvIO = new AcmvTableDAM();
		/*Contract header - life new business*/
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
		/*Component (Coverage/Rider) Record*/
	private CovrTableDAM covrIO = new CovrTableDAM();
		/*COVERAGE VIEW*/
	private CovtTableDAM covtIO = new CovtTableDAM();
	private Datcon3rec datcon3rec = new Datcon3rec();
		/*Additional Government Statistical File*/
	private GvstTableDAM gvstIO = new GvstTableDAM();
		/*Table items, date - maintenance view*/
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5645rec t5645rec = new T5645rec();
	private T5687rec t5687rec = new T5687rec();
	private T6640rec t6640rec = new T6640rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit1199,
		eof2080,
		exit2090,
		nextr5080,
		exit5090,
		exit5290,
		exit5590,
		exit5790
	}

	public Bj518() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		readT56451020();
		overrideFile1030();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaCompany.set(bsprIO.getCompany());
		wsaaBranch.set(bsprIO.getDefaultBranch());
		wsaaAcctmnth.set(bsscIO.getAcctMonth());
		wsaaAcctyear.set(bsscIO.getAcctYear());
	}

protected void readT56451020()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemtabl(t5645);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.begn);
		wsaaT5645Sub.set(1);
		wsaaT5645Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23
			itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itemIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");


			loadT56451100();
		}

	}

protected void overrideFile1030()
	{
		wsaaBakxRunid.set(bprdIO.getSystemParam04());
		wsaaBakxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(BAKXPF) TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaBakxFn.toString());
		stringVariable1.append(") ");
		stringVariable1.append("MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		sqlbakxCursor = " SELECT  BATCCOY, BATCBRN, BATCACTYR, BATCACTMN, BATCTRCDE, BATCBATCH" +
" FROM   " + appVars.getTableNameOverriden("BAKXPF") + " " +
" WHERE BATCCOY = ?" +
" AND BATCBRN = ?" +
" AND BATCACTYR = ?" +
" AND BATCACTMN = ?";
		sqlerrorflag = false;
		try {
			sqlbakxCursorconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.smart.dataaccess.BakxpfTableDAM());
			sqlbakxCursorps = appVars.prepareStatementEmbeded(sqlbakxCursorconn, sqlbakxCursor, "BAKXPF");
			appVars.setDBString(sqlbakxCursorps, 1, wsaaCompany.toString());
			appVars.setDBString(sqlbakxCursorps, 2, wsaaBranch.toString());
			appVars.setDBDouble(sqlbakxCursorps, 3, wsaaAcctyear.toDouble());
			appVars.setDBDouble(sqlbakxCursorps, 4, wsaaAcctmnth.toDouble());
			sqlbakxCursorrs = appVars.executeQuery(sqlbakxCursorps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void loadT56451100()
	{
		try {
			start1110();
		}
		catch (GOTOException e){
		}
	}

protected void start1110()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),t5645)
		|| isNE(itemIO.getItemitem(),wsaaProg)
		|| isNE(itemIO.getStatuz(),varcom.oK)) {
			if (isEQ(itemIO.getFunction(),varcom.begn)) {
				syserrrec.params.set(itemIO.getParams());
				fatalError600();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				goTo(GotoLabel.exit1199);
			}
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaT5645Sub.set(1);
		if (isGT(wsaaT5645Sub,wsaaT5645Size)) {
			syserrrec.statuz.set(h791);
			syserrrec.params.set(t5645);
			fatalError600();
		}
		while ( !(isGT(wsaaT5645Sub,15))) {
			wsaaT5645Sacscode[wsaaT5645Ix.toInt()].set(t5645rec.sacscode[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacstype[wsaaT5645Ix.toInt()].set(t5645rec.sacstype[wsaaT5645Sub.toInt()]);
			wsaaT5645Sub.add(1);
			wsaaT5645Ix.add(1);
		}

		itemIO.setFunction(varcom.nextr);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlbakxCursorrs.next()) {
				appVars.getDBObject(sqlbakxCursorrs, 1, sqlBatccoy);
				appVars.getDBObject(sqlbakxCursorrs, 2, sqlBatcbrn);
				appVars.getDBObject(sqlbakxCursorrs, 3, sqlBatcactyr);
				appVars.getDBObject(sqlbakxCursorrs, 4, sqlBatcactmn);
				appVars.getDBObject(sqlbakxCursorrs, 5, sqlBatctrcde);
				appVars.getDBObject(sqlbakxCursorrs, 6, sqlBatcbatch);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		wsaaRdocnum.set(SPACES);
		wsaaPrevCoverageKey.set(SPACES);
		acmvIO.setParams(SPACES);
		acmvIO.setBatccoy(sqlBatccoy);
		acmvIO.setBatcbrn(sqlBatcbrn);
		acmvIO.setBatcactyr(sqlBatcactyr);
		acmvIO.setBatcactmn(sqlBatcactmn);
		acmvIO.setBatctrcde(sqlBatctrcde);
		acmvIO.setBatcbatch(sqlBatcbatch);
		acmvIO.setFunction(varcom.begn);

		acmvIO.setFormat(acmvrec);
		while ( !(isEQ(acmvIO.getStatuz(),varcom.endp))) {
			//performance improvement --  atiwari23
			acmvIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);



			processAcmv5000();
		}

	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlbakxCursorconn, sqlbakxCursorps, sqlbakxCursorrs);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void processAcmv5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para5010();
				}
				case nextr5080: {
					nextr5080();
				}
				case exit5090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para5010()
	{
		SmartFileCode.execute(appVars, acmvIO);
		if (isNE(acmvIO.getStatuz(),varcom.endp)
		&& isNE(acmvIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(acmvIO.getParams());
			fatalError600();
		}
		if (isNE(acmvIO.getBatccoy(),sqlBatccoy)
		|| isNE(acmvIO.getBatcbrn(),sqlBatcbrn)
		|| isNE(acmvIO.getBatcactyr(),sqlBatcactyr)
		|| isNE(acmvIO.getBatcactmn(),sqlBatcactmn)
		|| isNE(acmvIO.getBatctrcde(),sqlBatctrcde)
		|| isNE(acmvIO.getBatcbatch(),sqlBatcbatch)
		|| isEQ(acmvIO.getStatuz(),varcom.endp)) {
			acmvIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5090);
		}
		wsaaTransaction.set(acmvIO.getBatctrcde());
		if (((maturity.isTrue()
		|| maturityReversal.isTrue())
		&& isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[9]))
		|| ((deathClaimAdjustment.isTrue()
		|| pendingDeathClaim.isTrue())
		&& isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[47]))
		|| (premiumCollection.isTrue()
		&& isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[49]))) {
			goTo(GotoLabel.nextr5080);
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		wsaaCoverageKey.set(SPACES);
		wsaaRecFound.set("N");
		for (wsaaT5645Ix.set(1); !(isGT(wsaaT5645Ix,99)
		|| recFound.isTrue()); wsaaT5645Ix.add(1)){
			if (isEQ(acmvIO.getSacscode(),wsaaT5645Sacscode[wsaaT5645Ix.toInt()])
			&& isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[wsaaT5645Ix.toInt()])) {
				wsaaRecFound.set("Y");
			}
		}
		if (!recFound.isTrue()) {
			goTo(GotoLabel.nextr5080);
		}
		if (isNE(acmvIO.getRdocnum(),wsaaRdocnum)) {
			readChdr5100();
			wsaaRdocnum.set(acmvIO.getRdocnum());
		}
		wsaaAcctLevel.set("Y");
		if (isEQ(acmvIO.getSacscode(),wsaaT5645Sacscode[3])) {
			wsaaCoverageKey.set(acmvIO.getTranref());
		}
		else {
			if (isEQ(acmvIO.getSacscode(),wsaaT5645Sacscode[1])) {
				wsaaCoverageKey.set(acmvIO.getRldgacct());
			}
		}
		if (isEQ(wsaaCoverage,SPACES)) {
			wsaaChdrnum.set(acmvIO.getRdocnum());
			wsaaLife.set("01");
			wsaaCoverage.set("01");
			wsaaRider.set("00");
			wsaaPlanSuffix.set(ZERO);
			wsaaAcctLevel.set("N");
		}
		if (isNE(wsaaCoverageKey,wsaaPrevCoverageKey)) {
			readCovr5200();
			if (isEQ(covrIO.getStatuz(),varcom.mrnf)
			&& isEQ(covtIO.getStatuz(),varcom.mrnf)) {
				goTo(GotoLabel.nextr5080);
			}
		}
		wsaaPrevCoverageKey.set(wsaaCoverageKey);
		gvstIO.setParams(SPACES);
		gvstIO.setBatccoy(sqlBatccoy);
		gvstIO.setBatcbrn(sqlBatcbrn);
		gvstIO.setBatcactyr(sqlBatcactyr);
		gvstIO.setBatcactmn(sqlBatcactmn);
		gvstIO.setBatctrcde(sqlBatctrcde);
		gvstIO.setBatcbatch(sqlBatcbatch);
		gvstIO.setAcctccy(acmvIO.getGenlcur());
		gvstIO.setChdrnum(wsaaChdrnum);
		gvstIO.setLife(wsaaLife);
		gvstIO.setCoverage(wsaaCoverage);
		gvstIO.setRider(wsaaRider);
		gvstIO.setPlanSuffix(wsaaPlanSuffix);
		gvstIO.setFunction(varcom.readr);
		gvstIO.setFormat(gvstrec);
		SmartFileCode.execute(appVars, gvstIO);
		if (isEQ(gvstIO.getStatuz(),varcom.mrnf)) {
			initialiseGvst5300();
			gvstIO.setFunction(varcom.writr);
		}
		else {
			gvstIO.setFunction(varcom.updat);
		}
		updateGvst5400();
	}

protected void nextr5080()
	{
		acmvIO.setFunction(varcom.nextr);
	}

protected void readChdr5100()
	{
		/*PARA*/
		chdrlnbIO.setChdrcoy(sqlBatccoy);
		chdrlnbIO.setChdrnum(acmvIO.getRdocnum());
		chdrlnbIO.setFunction(varcom.readr);
		chdrlnbIO.setFormat(chdrlnbrec);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)
				&& isNE(chdrlnbIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void readCovr5200()
	{
		try {
			para5210();
		}
		catch (GOTOException e){
		}
	}

protected void para5210()
	{
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(sqlBatccoy);
		covrIO.setChdrnum(wsaaChdrnum);
		covrIO.setLife(wsaaLife);
		covrIO.setCoverage(wsaaCoverage);
		covrIO.setRider(wsaaRider);
		covrIO.setPlanSuffix(wsaaPlanSuffix);
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isEQ(covrIO.getStatuz(),varcom.mrnf)) {
			readCovt5500();
			goTo(GotoLabel.exit5290);
		}
		wsaaRiskCommDate.set(covrIO.getCrrcd());
		wsaaStatFund.set(covrIO.getStatFund());
		wsaaStatSect.set(covrIO.getStatSect());
		wsaaStatSubsect.set(covrIO.getStatSubsect());
		wsaaCrtable.set(covrIO.getCrtable());
		wsaaPstatcode.set(covrIO.getPstatcode());
		wsaaBonusInd.set(covrIO.getBonusInd());
	}

protected void initialiseGvst5300()
	{
		para5310();
	}

protected void para5310()
	{
		gvstIO.setParams(SPACES);
		gvstIO.setBatccoy(sqlBatccoy);
		gvstIO.setBatcbrn(sqlBatcbrn);
		gvstIO.setBatcactyr(sqlBatcactyr);
		gvstIO.setBatcactmn(sqlBatcactmn);
		gvstIO.setBatctrcde(sqlBatctrcde);
		gvstIO.setBatcbatch(sqlBatcbatch);
		gvstIO.setChdrcoy(chdrlnbIO.getChdrcoy());
		gvstIO.setChdrnum(wsaaChdrnum);
		gvstIO.setLife(wsaaLife);
		gvstIO.setCoverage(wsaaCoverage);
		gvstIO.setRider(wsaaRider);
		gvstIO.setPlanSuffix(wsaaPlanSuffix);
		gvstIO.setAcctccy(acmvIO.getGenlcur());
		gvstIO.setBillfreq(chdrlnbIO.getBillfreq());
		wsaaCommYear.set(wsaaRiskCommDate);
		gvstIO.setStatFund(wsaaStatFund);
		gvstIO.setStatSect(wsaaStatSect);
		gvstIO.setStatSubsect(wsaaStatSubsect);
		gvstIO.setCrtable(wsaaCrtable);
		gvstIO.setCovPremStat(wsaaPstatcode);
		if (isEQ(wsaaBonusInd,SPACES)) {
			readT55405800();
			if (isNE(itemIO.getStatuz(),varcom.mrnf)) {
				gvstIO.setParind("P");
			}
			else {
				gvstIO.setParind("N");
			}
		}
		else {
			gvstIO.setParind("P");
		}
		gvstIO.setCommyr(wsaaCommyr);
		gvstIO.setCntbranch(chdrlnbIO.getCntbranch());
		gvstIO.setSrcebus(chdrlnbIO.getSrcebus());
		gvstIO.setRegister(chdrlnbIO.getRegister());
		gvstIO.setCnttype(chdrlnbIO.getCnttype());
		gvstIO.setCnPremStat(chdrlnbIO.getPstatcode());
		gvstIO.setStatcode(chdrlnbIO.getStatcode());
		gvstIO.setStaccfyp(ZERO);
		gvstIO.setStaccrnp(ZERO);
		gvstIO.setStaccspd(ZERO);
		gvstIO.setStaccfyc(ZERO);
		gvstIO.setStaccrlc(ZERO);
		gvstIO.setStaccspc(ZERO);
		gvstIO.setStaccrip(ZERO);
		gvstIO.setStaccric(ZERO);
		gvstIO.setStacctb(ZERO);
		gvstIO.setStaccrb(ZERO);
		gvstIO.setStaccxb(ZERO);
		gvstIO.setStaccib(ZERO);
		gvstIO.setStaccob(ZERO);
		gvstIO.setStaccmtb(ZERO);
		gvstIO.setStaccbs(ZERO);
		gvstIO.setStaccclr(ZERO);
		gvstIO.setStaccdd(ZERO);
		gvstIO.setStaccdi(ZERO);
		gvstIO.setStaccpdb(ZERO);
		gvstIO.setStaccadb(ZERO);
		gvstIO.setStaccadj(ZERO);
		gvstIO.setStaccint(ZERO);
		gvstIO.setStaccadv(ZERO);
	}

protected void updateGvst5400()
	{
		para5410();
	}

protected void para5410()
	{
		if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[1])) {
			datcon3rec.datcon3Rec.set(SPACES);
			datcon3rec.intDate1.set(wsaaRiskCommDate);
			datcon3rec.intDate2.set(acmvIO.getEffdate());
			datcon3rec.frequency.set("01");
			callProgram(Datcon3.class, datcon3rec.datcon3Rec);
			if (isNE(datcon3rec.statuz,varcom.oK)) {
				syserrrec.params.set(datcon3rec.datcon3Rec);
				fatalError600();
			}
			if (isGTE(datcon3rec.freqFactor,1)) {
				setPrecision(gvstIO.getStaccrnp(), 2);
				gvstIO.setStaccrnp(add(gvstIO.getStaccrnp(),acmvIO.getAcctamt()));
				contotrec.totno.set(ct03);
				contotrec.totval.set(acmvIO.getAcctamt());
				callContot001();
			}
			else {
				setPrecision(gvstIO.getStaccfyp(), 2);
				gvstIO.setStaccfyp(add(gvstIO.getStaccfyp(),acmvIO.getAcctamt()));
				contotrec.totno.set(ct04);
				contotrec.totval.set(acmvIO.getAcctamt());
				callContot001();
			}
			if (isGT(acmvIO.getEffdate(),bsscIO.getEffectiveDate())) {
				setPrecision(gvstIO.getStaccadv(), 2);
				gvstIO.setStaccadv(add(gvstIO.getStaccadv(),acmvIO.getAcctamt()));
				contotrec.totno.set(ct20);
				contotrec.totval.set(acmvIO.getAcctamt());
				callContot001();
			}
		}
		else {
			if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[2])) {
				setPrecision(gvstIO.getStaccspd(), 2);
				gvstIO.setStaccspd(add(gvstIO.getStaccspd(),acmvIO.getAcctamt()));
				contotrec.totno.set(ct05);
				contotrec.totval.set(acmvIO.getAcctamt());
				callContot001();
			}
			else {
				if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[3])) {
					setPrecision(gvstIO.getStaccfyc(), 2);
					gvstIO.setStaccfyc(add(gvstIO.getStaccfyc(),acmvIO.getAcctamt()));
					contotrec.totno.set(ct06);
					contotrec.totval.set(acmvIO.getAcctamt());
					callContot001();
				}
				else {
					if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[4])) {
						setPrecision(gvstIO.getStaccrlc(), 2);
						gvstIO.setStaccrlc(add(gvstIO.getStaccrlc(),acmvIO.getAcctamt()));
						contotrec.totno.set(ct07);
						contotrec.totval.set(acmvIO.getAcctamt());
						callContot001();
					}
					else {
						if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[5])) {
							setPrecision(gvstIO.getStaccspc(), 2);
							gvstIO.setStaccspc(add(gvstIO.getStaccspc(),acmvIO.getAcctamt()));
							contotrec.totno.set(ct08);
							contotrec.totval.set(acmvIO.getAcctamt());
							callContot001();
						}
						else {
							if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[6])) {
								if (isEQ(acmvIO.getGlsign(),"-")) {
									setPrecision(acmvIO.getAcctamt(), 2);
									acmvIO.setAcctamt(mult(acmvIO.getAcctamt(),-1));
								}
								setPrecision(gvstIO.getStaccrip(), 2);
								gvstIO.setStaccrip(add(gvstIO.getStaccrip(),acmvIO.getAcctamt()));
								contotrec.totno.set(ct09);
								contotrec.totval.set(acmvIO.getAcctamt());
								callContot001();
							}
							else {
								if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[7])) {
									if (isEQ(acmvIO.getGlsign(),"+")) {
										setPrecision(acmvIO.getAcctamt(), 2);
										acmvIO.setAcctamt(mult(acmvIO.getAcctamt(),-1));
									}
									setPrecision(gvstIO.getStaccric(), 2);
									gvstIO.setStaccric(add(gvstIO.getStaccric(),acmvIO.getAcctamt()));
									contotrec.totno.set(ct10);
									contotrec.totval.set(acmvIO.getAcctamt());
									callContot001();
								}
								else {
									if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[8])) {
										setPrecision(gvstIO.getStacctb(), 2);
										gvstIO.setStacctb(add(gvstIO.getStacctb(),acmvIO.getAcctamt()));
										setPrecision(gvstIO.getStaccmtb(), 2);
										gvstIO.setStaccmtb(add(gvstIO.getStaccmtb(),acmvIO.getAcctamt()));
										contotrec.totno.set(ct11);
										contotrec.totval.set(acmvIO.getAcctamt());
										callContot001();
										contotrec.totno.set(ct17);
										contotrec.totval.set(acmvIO.getAcctamt());
										callContot001();
									}
									else {
										if ((isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[9])
										|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[21]))) {
											if (isEQ(acmvIO.getGlsign(),"-")) {
												setPrecision(acmvIO.getAcctamt(), 2);
												acmvIO.setAcctamt(mult(acmvIO.getAcctamt(),-1));
											}
											setPrecision(gvstIO.getStaccrb(), 2);
											gvstIO.setStaccrb(add(gvstIO.getStaccrb(),acmvIO.getAcctamt()));
											contotrec.totno.set(ct12);
											contotrec.totval.set(acmvIO.getAcctamt());
											callContot001();
										}
										else {
											if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[10])) {
												setPrecision(gvstIO.getStaccxb(), 2);
												gvstIO.setStaccxb(add(gvstIO.getStaccxb(),acmvIO.getAcctamt()));
												setPrecision(gvstIO.getStaccmtb(), 2);
												gvstIO.setStaccmtb(add(gvstIO.getStaccmtb(),acmvIO.getAcctamt()));
												contotrec.totno.set(ct13);
												contotrec.totval.set(acmvIO.getAcctamt());
												callContot001();
												contotrec.totno.set(ct17);
												contotrec.totval.set(acmvIO.getAcctamt());
												callContot001();
											}
											else {
												if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[11])) {
													setPrecision(gvstIO.getStaccib(), 2);
													gvstIO.setStaccib(add(gvstIO.getStaccib(),acmvIO.getAcctamt()));
													setPrecision(gvstIO.getStaccmtb(), 2);
													gvstIO.setStaccmtb(add(gvstIO.getStaccmtb(),acmvIO.getAcctamt()));
													contotrec.totno.set(ct14);
													contotrec.totval.set(acmvIO.getAcctamt());
													callContot001();
													contotrec.totno.set(ct17);
													contotrec.totval.set(acmvIO.getAcctamt());
													callContot001();
												}
												else {
													if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[12])) {
														if (isEQ(acmvIO.getGlsign(),"+")) {
															setPrecision(gvstIO.getStaccob(), 2);
															gvstIO.setStaccob(add(gvstIO.getStaccob(),acmvIO.getAcctamt()));
															contotrec.totno.set(ct15);
															contotrec.totval.set(acmvIO.getAcctamt());
															callContot001();
														}
													}
													else {
														if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[46])
														|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[49])) {
															setPrecision(gvstIO.getStaccob(), 2);
															gvstIO.setStaccob(add(gvstIO.getStaccob(),acmvIO.getAcctamt()));
															contotrec.totno.set(ct15);
															contotrec.totval.set(acmvIO.getAcctamt());
															callContot001();
														}
														else {
															if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[13])) {
																if (approvedDeathClaim.isTrue()) {
																	if (isEQ(acmvIO.getGlsign(),"-")) {
																		setPrecision(acmvIO.getAcctamt(), 2);
																		acmvIO.setAcctamt(mult(acmvIO.getAcctamt(),-1));
																	}
																	setPrecision(gvstIO.getStaccadb(), 2);
																	gvstIO.setStaccadb(add(gvstIO.getStaccadb(),acmvIO.getAcctamt()));
																	contotrec.totno.set(ct23);
																	contotrec.totval.set(acmvIO.getAcctamt());
																	callContot001();
																}
																else {
																	if (isEQ(acmvIO.getGlsign(),"+")) {
																		setPrecision(acmvIO.getAcctamt(), 2);
																		acmvIO.setAcctamt(mult(acmvIO.getAcctamt(),-1));
																	}
																	setPrecision(gvstIO.getStaccpdb(), 2);
																	gvstIO.setStaccpdb(add(gvstIO.getStaccpdb(),acmvIO.getAcctamt()));
																	contotrec.totno.set(ct16);
																	contotrec.totval.set(acmvIO.getAcctamt());
																	callContot001();
																}
															}
															else {
																if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[48])) {
																	setPrecision(gvstIO.getStaccint(), 2);
																	gvstIO.setStaccint(add(gvstIO.getStaccint(),acmvIO.getAcctamt()));
																	contotrec.totno.set(ct25);
																	contotrec.totval.set(acmvIO.getAcctamt());
																	callContot001();
																}
																else {
																	if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[47])) {
																		if (isEQ(acmvIO.getGlsign(),"-")) {
																			setPrecision(acmvIO.getAcctamt(), 2);
																			acmvIO.setAcctamt(mult(acmvIO.getAcctamt(),-1));
																		}
																		setPrecision(gvstIO.getStaccadj(), 2);
																		gvstIO.setStaccadj(add(gvstIO.getStaccadj(),acmvIO.getAcctamt()));
																		contotrec.totno.set(ct24);
																		contotrec.totval.set(acmvIO.getAcctamt());
																		callContot001();
																	}
																	else {
																		if ((isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[14])
																		|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[15])
																		|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[16])
																		|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[41])
																		|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[50])
																		|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[51])
																		|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[52])
																		|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[53]))) {
																			if (isEQ(acmvIO.getGlsign(),"-")) {
																				setPrecision(acmvIO.getAcctamt(), 2);
																				acmvIO.setAcctamt(mult(acmvIO.getAcctamt(),-1));
																			}
																			setPrecision(gvstIO.getStaccmtb(), 2);
																			gvstIO.setStaccmtb(add(gvstIO.getStaccmtb(),acmvIO.getAcctamt()));
																			contotrec.totno.set(ct17);
																			contotrec.totval.set(acmvIO.getAcctamt());
																			callContot001();
																		}
																		else {
																			if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[17])) {
																				setPrecision(gvstIO.getStaccbs(), 2);
																				gvstIO.setStaccbs(add(gvstIO.getStaccbs(),acmvIO.getAcctamt()));
																				contotrec.totno.set(ct18);
																				contotrec.totval.set(acmvIO.getAcctamt());
																				callContot001();
																			}
																			else {
																				if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[20])) {
																					setPrecision(gvstIO.getStaccclr(), 2);
																					gvstIO.setStaccclr(add(gvstIO.getStaccclr(),acmvIO.getAcctamt()));
																					contotrec.totno.set(ct19);
																					contotrec.totval.set(acmvIO.getAcctamt());
																					callContot001();
																				}
																				else {
																					if ((isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[23])
																					|| isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[24]))) {
																						if (isEQ(acmvIO.getGlsign(),"+")) {
																							setPrecision(acmvIO.getAcctamt(), 2);
																							acmvIO.setAcctamt(mult(acmvIO.getAcctamt(),-1));
																						}
																						setPrecision(gvstIO.getStaccadv(), 2);
																						gvstIO.setStaccadv(add(gvstIO.getStaccadv(),acmvIO.getAcctamt()));
																						contotrec.totno.set(ct20);
																						contotrec.totval.set(acmvIO.getAcctamt());
																						callContot001();
																					}
																					else {
																						if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[42])) {
																							setPrecision(gvstIO.getStaccdd(), 2);
																							gvstIO.setStaccdd(add(gvstIO.getStaccdd(),acmvIO.getAcctamt()));
																							contotrec.totno.set(ct21);
																							contotrec.totval.set(acmvIO.getAcctamt());
																							callContot001();
																						}
																						else {
																							if (isEQ(acmvIO.getSacstyp(),wsaaT5645Sacstype[43])) {
																								setPrecision(gvstIO.getStaccdi(), 2);
																								gvstIO.setStaccdi(add(gvstIO.getStaccdi(),acmvIO.getAcctamt()));
																								contotrec.totno.set(ct22);
																								contotrec.totval.set(acmvIO.getAcctamt());
																								callContot001();
																							}
																						}
																					}
																				}
																			}
																		}
																	}
																}
															}
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		gvstIO.setFormat(gvstrec);
		SmartFileCode.execute(appVars, gvstIO);
		if (isNE(gvstIO.getStatuz(),varcom.oK)
		&& isNE(gvstIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(gvstIO.getParams());
			fatalError600();
		}
	}

protected void readCovt5500()
	{
		try {
			para5510();
		}
		catch (GOTOException e){
		}
	}

protected void para5510()
	{
		covtIO.setParams(SPACES);
		covtIO.setChdrcoy(sqlBatccoy);
		covtIO.setChdrnum(wsaaChdrnum);
		covtIO.setLife(wsaaLife);
		covtIO.setCoverage(wsaaCoverage);
		covtIO.setRider(wsaaRider);
		covtIO.setPlanSuffix(wsaaPlanSuffix);
		covtIO.setFormat(covtrec);
		covtIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covtIO);
		if (isEQ(covtIO.getStatuz(),varcom.mrnf)) {
			goTo(GotoLabel.exit5590);
		}
		wsaaRiskCommDate.set(covtIO.getEffdate());
		wsaaCrtable.set(covtIO.getCrtable());
		wsaaPstatcode.set(SPACES);
		readT56875600();
		wsaaStatFund.set(t5687rec.statFund);
		wsaaStatSect.set(t5687rec.statSect);
		wsaaStatSubsect.set(t5687rec.statSubSect);
		readT66405700();
		wsaaBonusInd.set(t6640rec.zbondivalc);
	}

protected void readT56875600()
	{
		para5610();
	}

protected void para5610()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covtIO.getCrtable());
		itdmIO.setItmfrm(covtIO.getEffdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t5687)
		|| isNE(itdmIO.getItemitem(),covtIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(covtIO.getCrtable());
			syserrrec.statuz.set(f294);
			fatalError600();
		}
		t5687rec.t5687Rec.set(itdmIO.getGenarea());
	}

protected void readT66405700()
	{
		try {
			para5710();
		}
		catch (GOTOException e){
		}
	}

protected void para5710()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(bsprIO.getCompany());
		itdmIO.setItemtabl(t6640);
		itdmIO.setItemitem(covtIO.getCrtable());
		itdmIO.setItmfrm(covtIO.getEffdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);

		//performance improvement --  atiwari23
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");


		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(),varcom.oK)
		&& isNE(itdmIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isNE(itdmIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itdmIO.getItemtabl(),t6640)
		|| isNE(itdmIO.getItemitem(),covtIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(),varcom.endp)) {
			t6640rec.t6640Rec.set(SPACES);
			goTo(GotoLabel.exit5790);
		}
		t6640rec.t6640Rec.set(itdmIO.getGenarea());
	}

protected void readT55405800()
	{
		/*PARA*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t5540);
		itemIO.setItemitem(wsaaCrtable);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
