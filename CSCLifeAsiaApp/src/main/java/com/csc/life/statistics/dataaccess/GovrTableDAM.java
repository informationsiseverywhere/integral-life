package com.csc.life.statistics.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GovrTableDAM.java
 * Date: Sun, 30 Aug 2009 03:38:48
 * Class transformed from GOVR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GovrTableDAM extends GovrpfTableDAM {

	public GovrTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("GOVR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", STATCAT"
		             + ", ACCTYR"
		             + ", STFUND"
		             + ", STSECT"
		             + ", STSSECT"
		             + ", REG"
		             + ", CNTBRANCH"
		             + ", BANDAGE"
		             + ", BANDSA"
		             + ", BANDPRM"
		             + ", BANDTRM"
		             + ", COMMYR"
		             + ", PSTATCODE"
		             + ", CNTCURR";
		
		QUALIFIEDCOLUMNS = 
		            "REG, " +
		            "CHDRCOY, " +
		            "CNTBRANCH, " +
		            "STFUND, " +
		            "STSECT, " +
		            "STSSECT, " +
		            "BANDAGE, " +
		            "BANDSA, " +
		            "BANDPRM, " +
		            "BANDTRM, " +
		            "COMMYR, " +
		            "STATCAT, " +
		            "PSTATCODE, " +
		            "CNTCURR, " +
		            "ACCTYR, " +
		            "BFWDC, " +
		            "STCMTH01, " +
		            "STCMTH02, " +
		            "STCMTH03, " +
		            "STCMTH04, " +
		            "STCMTH05, " +
		            "STCMTH06, " +
		            "STCMTH07, " +
		            "STCMTH08, " +
		            "STCMTH09, " +
		            "STCMTH10, " +
		            "STCMTH11, " +
		            "STCMTH12, " +
		            "CFWDC, " +
		            "BFWDP, " +
		            "STPMTH01, " +
		            "STPMTH02, " +
		            "STPMTH03, " +
		            "STPMTH04, " +
		            "STPMTH05, " +
		            "STPMTH06, " +
		            "STPMTH07, " +
		            "STPMTH08, " +
		            "STPMTH09, " +
		            "STPMTH10, " +
		            "STPMTH11, " +
		            "STPMTH12, " +
		            "CFWDP, " +
		            "BFWDS, " +
		            "STSMTH01, " +
		            "STSMTH02, " +
		            "STSMTH03, " +
		            "STSMTH04, " +
		            "STSMTH05, " +
		            "STSMTH06, " +
		            "STSMTH07, " +
		            "STSMTH08, " +
		            "STSMTH09, " +
		            "STSMTH10, " +
		            "STSMTH11, " +
		            "STSMTH12, " +
		            "CFWDS, " +
		            "BFWDI, " +
		            "STIMTH01, " +
		            "STIMTH02, " +
		            "STIMTH03, " +
		            "STIMTH04, " +
		            "STIMTH05, " +
		            "STIMTH06, " +
		            "STIMTH07, " +
		            "STIMTH08, " +
		            "STIMTH09, " +
		            "STIMTH10, " +
		            "STIMTH11, " +
		            "STIMTH12, " +
		            "CFWDI, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "STATCAT ASC, " +
		            "ACCTYR ASC, " +
		            "STFUND ASC, " +
		            "STSECT ASC, " +
		            "STSSECT ASC, " +
		            "REG ASC, " +
		            "CNTBRANCH ASC, " +
		            "BANDAGE ASC, " +
		            "BANDSA ASC, " +
		            "BANDPRM ASC, " +
		            "BANDTRM ASC, " +
		            "COMMYR ASC, " +
		            "PSTATCODE ASC, " +
		            "CNTCURR ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "STATCAT DESC, " +
		            "ACCTYR DESC, " +
		            "STFUND DESC, " +
		            "STSECT DESC, " +
		            "STSSECT DESC, " +
		            "REG DESC, " +
		            "CNTBRANCH DESC, " +
		            "BANDAGE DESC, " +
		            "BANDSA DESC, " +
		            "BANDPRM DESC, " +
		            "BANDTRM DESC, " +
		            "COMMYR DESC, " +
		            "PSTATCODE DESC, " +
		            "CNTCURR DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               register,
                               chdrcoy,
                               cntbranch,
                               statFund,
                               statSect,
                               statSubsect,
                               bandage,
                               bandsa,
                               bandprm,
                               bandtrm,
                               commyr,
                               statcat,
                               pstatcode,
                               cntcurr,
                               acctyr,
                               bfwdc,
                               stcmth01,
                               stcmth02,
                               stcmth03,
                               stcmth04,
                               stcmth05,
                               stcmth06,
                               stcmth07,
                               stcmth08,
                               stcmth09,
                               stcmth10,
                               stcmth11,
                               stcmth12,
                               cfwdc,
                               bfwdp,
                               stpmth01,
                               stpmth02,
                               stpmth03,
                               stpmth04,
                               stpmth05,
                               stpmth06,
                               stpmth07,
                               stpmth08,
                               stpmth09,
                               stpmth10,
                               stpmth11,
                               stpmth12,
                               cfwdp,
                               bfwds,
                               stsmth01,
                               stsmth02,
                               stsmth03,
                               stsmth04,
                               stsmth05,
                               stsmth06,
                               stsmth07,
                               stsmth08,
                               stsmth09,
                               stsmth10,
                               stsmth11,
                               stsmth12,
                               cfwds,
                               bfwdi,
                               stimth01,
                               stimth02,
                               stimth03,
                               stimth04,
                               stimth05,
                               stimth06,
                               stimth07,
                               stimth08,
                               stimth09,
                               stimth10,
                               stimth11,
                               stimth12,
                               cfwdi,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(222);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getStatcat().toInternal()
					+ getAcctyr().toInternal()
					+ getStatFund().toInternal()
					+ getStatSect().toInternal()
					+ getStatSubsect().toInternal()
					+ getRegister().toInternal()
					+ getCntbranch().toInternal()
					+ getBandage().toInternal()
					+ getBandsa().toInternal()
					+ getBandprm().toInternal()
					+ getBandtrm().toInternal()
					+ getCommyr().toInternal()
					+ getPstatcode().toInternal()
					+ getCntcurr().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, statcat);
			what = ExternalData.chop(what, acctyr);
			what = ExternalData.chop(what, statFund);
			what = ExternalData.chop(what, statSect);
			what = ExternalData.chop(what, statSubsect);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, bandage);
			what = ExternalData.chop(what, bandsa);
			what = ExternalData.chop(what, bandprm);
			what = ExternalData.chop(what, bandtrm);
			what = ExternalData.chop(what, commyr);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller130 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller140 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller150 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(register.toInternal());
	nonKeyFiller30.setInternal(cntbranch.toInternal());
	nonKeyFiller40.setInternal(statFund.toInternal());
	nonKeyFiller50.setInternal(statSect.toInternal());
	nonKeyFiller60.setInternal(statSubsect.toInternal());
	nonKeyFiller70.setInternal(bandage.toInternal());
	nonKeyFiller80.setInternal(bandsa.toInternal());
	nonKeyFiller90.setInternal(bandprm.toInternal());
	nonKeyFiller100.setInternal(bandtrm.toInternal());
	nonKeyFiller110.setInternal(commyr.toInternal());
	nonKeyFiller120.setInternal(statcat.toInternal());
	nonKeyFiller130.setInternal(pstatcode.toInternal());
	nonKeyFiller140.setInternal(cntcurr.toInternal());
	nonKeyFiller150.setInternal(acctyr.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(580);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ nonKeyFiller130.toInternal()
					+ nonKeyFiller140.toInternal()
					+ nonKeyFiller150.toInternal()
					+ getBfwdc().toInternal()
					+ getStcmth01().toInternal()
					+ getStcmth02().toInternal()
					+ getStcmth03().toInternal()
					+ getStcmth04().toInternal()
					+ getStcmth05().toInternal()
					+ getStcmth06().toInternal()
					+ getStcmth07().toInternal()
					+ getStcmth08().toInternal()
					+ getStcmth09().toInternal()
					+ getStcmth10().toInternal()
					+ getStcmth11().toInternal()
					+ getStcmth12().toInternal()
					+ getCfwdc().toInternal()
					+ getBfwdp().toInternal()
					+ getStpmth01().toInternal()
					+ getStpmth02().toInternal()
					+ getStpmth03().toInternal()
					+ getStpmth04().toInternal()
					+ getStpmth05().toInternal()
					+ getStpmth06().toInternal()
					+ getStpmth07().toInternal()
					+ getStpmth08().toInternal()
					+ getStpmth09().toInternal()
					+ getStpmth10().toInternal()
					+ getStpmth11().toInternal()
					+ getStpmth12().toInternal()
					+ getCfwdp().toInternal()
					+ getBfwds().toInternal()
					+ getStsmth01().toInternal()
					+ getStsmth02().toInternal()
					+ getStsmth03().toInternal()
					+ getStsmth04().toInternal()
					+ getStsmth05().toInternal()
					+ getStsmth06().toInternal()
					+ getStsmth07().toInternal()
					+ getStsmth08().toInternal()
					+ getStsmth09().toInternal()
					+ getStsmth10().toInternal()
					+ getStsmth11().toInternal()
					+ getStsmth12().toInternal()
					+ getCfwds().toInternal()
					+ getBfwdi().toInternal()
					+ getStimth01().toInternal()
					+ getStimth02().toInternal()
					+ getStimth03().toInternal()
					+ getStimth04().toInternal()
					+ getStimth05().toInternal()
					+ getStimth06().toInternal()
					+ getStimth07().toInternal()
					+ getStimth08().toInternal()
					+ getStimth09().toInternal()
					+ getStimth10().toInternal()
					+ getStimth11().toInternal()
					+ getStimth12().toInternal()
					+ getCfwdi().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, nonKeyFiller130);
			what = ExternalData.chop(what, nonKeyFiller140);
			what = ExternalData.chop(what, nonKeyFiller150);
			what = ExternalData.chop(what, bfwdc);
			what = ExternalData.chop(what, stcmth01);
			what = ExternalData.chop(what, stcmth02);
			what = ExternalData.chop(what, stcmth03);
			what = ExternalData.chop(what, stcmth04);
			what = ExternalData.chop(what, stcmth05);
			what = ExternalData.chop(what, stcmth06);
			what = ExternalData.chop(what, stcmth07);
			what = ExternalData.chop(what, stcmth08);
			what = ExternalData.chop(what, stcmth09);
			what = ExternalData.chop(what, stcmth10);
			what = ExternalData.chop(what, stcmth11);
			what = ExternalData.chop(what, stcmth12);
			what = ExternalData.chop(what, cfwdc);
			what = ExternalData.chop(what, bfwdp);
			what = ExternalData.chop(what, stpmth01);
			what = ExternalData.chop(what, stpmth02);
			what = ExternalData.chop(what, stpmth03);
			what = ExternalData.chop(what, stpmth04);
			what = ExternalData.chop(what, stpmth05);
			what = ExternalData.chop(what, stpmth06);
			what = ExternalData.chop(what, stpmth07);
			what = ExternalData.chop(what, stpmth08);
			what = ExternalData.chop(what, stpmth09);
			what = ExternalData.chop(what, stpmth10);
			what = ExternalData.chop(what, stpmth11);
			what = ExternalData.chop(what, stpmth12);
			what = ExternalData.chop(what, cfwdp);
			what = ExternalData.chop(what, bfwds);
			what = ExternalData.chop(what, stsmth01);
			what = ExternalData.chop(what, stsmth02);
			what = ExternalData.chop(what, stsmth03);
			what = ExternalData.chop(what, stsmth04);
			what = ExternalData.chop(what, stsmth05);
			what = ExternalData.chop(what, stsmth06);
			what = ExternalData.chop(what, stsmth07);
			what = ExternalData.chop(what, stsmth08);
			what = ExternalData.chop(what, stsmth09);
			what = ExternalData.chop(what, stsmth10);
			what = ExternalData.chop(what, stsmth11);
			what = ExternalData.chop(what, stsmth12);
			what = ExternalData.chop(what, cfwds);
			what = ExternalData.chop(what, bfwdi);
			what = ExternalData.chop(what, stimth01);
			what = ExternalData.chop(what, stimth02);
			what = ExternalData.chop(what, stimth03);
			what = ExternalData.chop(what, stimth04);
			what = ExternalData.chop(what, stimth05);
			what = ExternalData.chop(what, stimth06);
			what = ExternalData.chop(what, stimth07);
			what = ExternalData.chop(what, stimth08);
			what = ExternalData.chop(what, stimth09);
			what = ExternalData.chop(what, stimth10);
			what = ExternalData.chop(what, stimth11);
			what = ExternalData.chop(what, stimth12);
			what = ExternalData.chop(what, cfwdi);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getStatcat() {
		return statcat;
	}
	public void setStatcat(Object what) {
		statcat.set(what);
	}
	public PackedDecimalData getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(Object what) {
		setAcctyr(what, false);
	}
	public void setAcctyr(Object what, boolean rounded) {
		if (rounded)
			acctyr.setRounded(what);
		else
			acctyr.set(what);
	}
	public FixedLengthStringData getStatFund() {
		return statFund;
	}
	public void setStatFund(Object what) {
		statFund.set(what);
	}
	public FixedLengthStringData getStatSect() {
		return statSect;
	}
	public void setStatSect(Object what) {
		statSect.set(what);
	}
	public FixedLengthStringData getStatSubsect() {
		return statSubsect;
	}
	public void setStatSubsect(Object what) {
		statSubsect.set(what);
	}
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}
	public FixedLengthStringData getBandage() {
		return bandage;
	}
	public void setBandage(Object what) {
		bandage.set(what);
	}
	public FixedLengthStringData getBandsa() {
		return bandsa;
	}
	public void setBandsa(Object what) {
		bandsa.set(what);
	}
	public FixedLengthStringData getBandprm() {
		return bandprm;
	}
	public void setBandprm(Object what) {
		bandprm.set(what);
	}
	public FixedLengthStringData getBandtrm() {
		return bandtrm;
	}
	public void setBandtrm(Object what) {
		bandtrm.set(what);
	}
	public PackedDecimalData getCommyr() {
		return commyr;
	}
	public void setCommyr(Object what) {
		setCommyr(what, false);
	}
	public void setCommyr(Object what, boolean rounded) {
		if (rounded)
			commyr.setRounded(what);
		else
			commyr.set(what);
	}
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getBfwdc() {
		return bfwdc;
	}
	public void setBfwdc(Object what) {
		setBfwdc(what, false);
	}
	public void setBfwdc(Object what, boolean rounded) {
		if (rounded)
			bfwdc.setRounded(what);
		else
			bfwdc.set(what);
	}	
	public PackedDecimalData getStcmth01() {
		return stcmth01;
	}
	public void setStcmth01(Object what) {
		setStcmth01(what, false);
	}
	public void setStcmth01(Object what, boolean rounded) {
		if (rounded)
			stcmth01.setRounded(what);
		else
			stcmth01.set(what);
	}	
	public PackedDecimalData getStcmth02() {
		return stcmth02;
	}
	public void setStcmth02(Object what) {
		setStcmth02(what, false);
	}
	public void setStcmth02(Object what, boolean rounded) {
		if (rounded)
			stcmth02.setRounded(what);
		else
			stcmth02.set(what);
	}	
	public PackedDecimalData getStcmth03() {
		return stcmth03;
	}
	public void setStcmth03(Object what) {
		setStcmth03(what, false);
	}
	public void setStcmth03(Object what, boolean rounded) {
		if (rounded)
			stcmth03.setRounded(what);
		else
			stcmth03.set(what);
	}	
	public PackedDecimalData getStcmth04() {
		return stcmth04;
	}
	public void setStcmth04(Object what) {
		setStcmth04(what, false);
	}
	public void setStcmth04(Object what, boolean rounded) {
		if (rounded)
			stcmth04.setRounded(what);
		else
			stcmth04.set(what);
	}	
	public PackedDecimalData getStcmth05() {
		return stcmth05;
	}
	public void setStcmth05(Object what) {
		setStcmth05(what, false);
	}
	public void setStcmth05(Object what, boolean rounded) {
		if (rounded)
			stcmth05.setRounded(what);
		else
			stcmth05.set(what);
	}	
	public PackedDecimalData getStcmth06() {
		return stcmth06;
	}
	public void setStcmth06(Object what) {
		setStcmth06(what, false);
	}
	public void setStcmth06(Object what, boolean rounded) {
		if (rounded)
			stcmth06.setRounded(what);
		else
			stcmth06.set(what);
	}	
	public PackedDecimalData getStcmth07() {
		return stcmth07;
	}
	public void setStcmth07(Object what) {
		setStcmth07(what, false);
	}
	public void setStcmth07(Object what, boolean rounded) {
		if (rounded)
			stcmth07.setRounded(what);
		else
			stcmth07.set(what);
	}	
	public PackedDecimalData getStcmth08() {
		return stcmth08;
	}
	public void setStcmth08(Object what) {
		setStcmth08(what, false);
	}
	public void setStcmth08(Object what, boolean rounded) {
		if (rounded)
			stcmth08.setRounded(what);
		else
			stcmth08.set(what);
	}	
	public PackedDecimalData getStcmth09() {
		return stcmth09;
	}
	public void setStcmth09(Object what) {
		setStcmth09(what, false);
	}
	public void setStcmth09(Object what, boolean rounded) {
		if (rounded)
			stcmth09.setRounded(what);
		else
			stcmth09.set(what);
	}	
	public PackedDecimalData getStcmth10() {
		return stcmth10;
	}
	public void setStcmth10(Object what) {
		setStcmth10(what, false);
	}
	public void setStcmth10(Object what, boolean rounded) {
		if (rounded)
			stcmth10.setRounded(what);
		else
			stcmth10.set(what);
	}	
	public PackedDecimalData getStcmth11() {
		return stcmth11;
	}
	public void setStcmth11(Object what) {
		setStcmth11(what, false);
	}
	public void setStcmth11(Object what, boolean rounded) {
		if (rounded)
			stcmth11.setRounded(what);
		else
			stcmth11.set(what);
	}	
	public PackedDecimalData getStcmth12() {
		return stcmth12;
	}
	public void setStcmth12(Object what) {
		setStcmth12(what, false);
	}
	public void setStcmth12(Object what, boolean rounded) {
		if (rounded)
			stcmth12.setRounded(what);
		else
			stcmth12.set(what);
	}	
	public PackedDecimalData getCfwdc() {
		return cfwdc;
	}
	public void setCfwdc(Object what) {
		setCfwdc(what, false);
	}
	public void setCfwdc(Object what, boolean rounded) {
		if (rounded)
			cfwdc.setRounded(what);
		else
			cfwdc.set(what);
	}	
	public PackedDecimalData getBfwdp() {
		return bfwdp;
	}
	public void setBfwdp(Object what) {
		setBfwdp(what, false);
	}
	public void setBfwdp(Object what, boolean rounded) {
		if (rounded)
			bfwdp.setRounded(what);
		else
			bfwdp.set(what);
	}	
	public PackedDecimalData getStpmth01() {
		return stpmth01;
	}
	public void setStpmth01(Object what) {
		setStpmth01(what, false);
	}
	public void setStpmth01(Object what, boolean rounded) {
		if (rounded)
			stpmth01.setRounded(what);
		else
			stpmth01.set(what);
	}	
	public PackedDecimalData getStpmth02() {
		return stpmth02;
	}
	public void setStpmth02(Object what) {
		setStpmth02(what, false);
	}
	public void setStpmth02(Object what, boolean rounded) {
		if (rounded)
			stpmth02.setRounded(what);
		else
			stpmth02.set(what);
	}	
	public PackedDecimalData getStpmth03() {
		return stpmth03;
	}
	public void setStpmth03(Object what) {
		setStpmth03(what, false);
	}
	public void setStpmth03(Object what, boolean rounded) {
		if (rounded)
			stpmth03.setRounded(what);
		else
			stpmth03.set(what);
	}	
	public PackedDecimalData getStpmth04() {
		return stpmth04;
	}
	public void setStpmth04(Object what) {
		setStpmth04(what, false);
	}
	public void setStpmth04(Object what, boolean rounded) {
		if (rounded)
			stpmth04.setRounded(what);
		else
			stpmth04.set(what);
	}	
	public PackedDecimalData getStpmth05() {
		return stpmth05;
	}
	public void setStpmth05(Object what) {
		setStpmth05(what, false);
	}
	public void setStpmth05(Object what, boolean rounded) {
		if (rounded)
			stpmth05.setRounded(what);
		else
			stpmth05.set(what);
	}	
	public PackedDecimalData getStpmth06() {
		return stpmth06;
	}
	public void setStpmth06(Object what) {
		setStpmth06(what, false);
	}
	public void setStpmth06(Object what, boolean rounded) {
		if (rounded)
			stpmth06.setRounded(what);
		else
			stpmth06.set(what);
	}	
	public PackedDecimalData getStpmth07() {
		return stpmth07;
	}
	public void setStpmth07(Object what) {
		setStpmth07(what, false);
	}
	public void setStpmth07(Object what, boolean rounded) {
		if (rounded)
			stpmth07.setRounded(what);
		else
			stpmth07.set(what);
	}	
	public PackedDecimalData getStpmth08() {
		return stpmth08;
	}
	public void setStpmth08(Object what) {
		setStpmth08(what, false);
	}
	public void setStpmth08(Object what, boolean rounded) {
		if (rounded)
			stpmth08.setRounded(what);
		else
			stpmth08.set(what);
	}	
	public PackedDecimalData getStpmth09() {
		return stpmth09;
	}
	public void setStpmth09(Object what) {
		setStpmth09(what, false);
	}
	public void setStpmth09(Object what, boolean rounded) {
		if (rounded)
			stpmth09.setRounded(what);
		else
			stpmth09.set(what);
	}	
	public PackedDecimalData getStpmth10() {
		return stpmth10;
	}
	public void setStpmth10(Object what) {
		setStpmth10(what, false);
	}
	public void setStpmth10(Object what, boolean rounded) {
		if (rounded)
			stpmth10.setRounded(what);
		else
			stpmth10.set(what);
	}	
	public PackedDecimalData getStpmth11() {
		return stpmth11;
	}
	public void setStpmth11(Object what) {
		setStpmth11(what, false);
	}
	public void setStpmth11(Object what, boolean rounded) {
		if (rounded)
			stpmth11.setRounded(what);
		else
			stpmth11.set(what);
	}	
	public PackedDecimalData getStpmth12() {
		return stpmth12;
	}
	public void setStpmth12(Object what) {
		setStpmth12(what, false);
	}
	public void setStpmth12(Object what, boolean rounded) {
		if (rounded)
			stpmth12.setRounded(what);
		else
			stpmth12.set(what);
	}	
	public PackedDecimalData getCfwdp() {
		return cfwdp;
	}
	public void setCfwdp(Object what) {
		setCfwdp(what, false);
	}
	public void setCfwdp(Object what, boolean rounded) {
		if (rounded)
			cfwdp.setRounded(what);
		else
			cfwdp.set(what);
	}	
	public PackedDecimalData getBfwds() {
		return bfwds;
	}
	public void setBfwds(Object what) {
		setBfwds(what, false);
	}
	public void setBfwds(Object what, boolean rounded) {
		if (rounded)
			bfwds.setRounded(what);
		else
			bfwds.set(what);
	}	
	public PackedDecimalData getStsmth01() {
		return stsmth01;
	}
	public void setStsmth01(Object what) {
		setStsmth01(what, false);
	}
	public void setStsmth01(Object what, boolean rounded) {
		if (rounded)
			stsmth01.setRounded(what);
		else
			stsmth01.set(what);
	}	
	public PackedDecimalData getStsmth02() {
		return stsmth02;
	}
	public void setStsmth02(Object what) {
		setStsmth02(what, false);
	}
	public void setStsmth02(Object what, boolean rounded) {
		if (rounded)
			stsmth02.setRounded(what);
		else
			stsmth02.set(what);
	}	
	public PackedDecimalData getStsmth03() {
		return stsmth03;
	}
	public void setStsmth03(Object what) {
		setStsmth03(what, false);
	}
	public void setStsmth03(Object what, boolean rounded) {
		if (rounded)
			stsmth03.setRounded(what);
		else
			stsmth03.set(what);
	}	
	public PackedDecimalData getStsmth04() {
		return stsmth04;
	}
	public void setStsmth04(Object what) {
		setStsmth04(what, false);
	}
	public void setStsmth04(Object what, boolean rounded) {
		if (rounded)
			stsmth04.setRounded(what);
		else
			stsmth04.set(what);
	}	
	public PackedDecimalData getStsmth05() {
		return stsmth05;
	}
	public void setStsmth05(Object what) {
		setStsmth05(what, false);
	}
	public void setStsmth05(Object what, boolean rounded) {
		if (rounded)
			stsmth05.setRounded(what);
		else
			stsmth05.set(what);
	}	
	public PackedDecimalData getStsmth06() {
		return stsmth06;
	}
	public void setStsmth06(Object what) {
		setStsmth06(what, false);
	}
	public void setStsmth06(Object what, boolean rounded) {
		if (rounded)
			stsmth06.setRounded(what);
		else
			stsmth06.set(what);
	}	
	public PackedDecimalData getStsmth07() {
		return stsmth07;
	}
	public void setStsmth07(Object what) {
		setStsmth07(what, false);
	}
	public void setStsmth07(Object what, boolean rounded) {
		if (rounded)
			stsmth07.setRounded(what);
		else
			stsmth07.set(what);
	}	
	public PackedDecimalData getStsmth08() {
		return stsmth08;
	}
	public void setStsmth08(Object what) {
		setStsmth08(what, false);
	}
	public void setStsmth08(Object what, boolean rounded) {
		if (rounded)
			stsmth08.setRounded(what);
		else
			stsmth08.set(what);
	}	
	public PackedDecimalData getStsmth09() {
		return stsmth09;
	}
	public void setStsmth09(Object what) {
		setStsmth09(what, false);
	}
	public void setStsmth09(Object what, boolean rounded) {
		if (rounded)
			stsmth09.setRounded(what);
		else
			stsmth09.set(what);
	}	
	public PackedDecimalData getStsmth10() {
		return stsmth10;
	}
	public void setStsmth10(Object what) {
		setStsmth10(what, false);
	}
	public void setStsmth10(Object what, boolean rounded) {
		if (rounded)
			stsmth10.setRounded(what);
		else
			stsmth10.set(what);
	}	
	public PackedDecimalData getStsmth11() {
		return stsmth11;
	}
	public void setStsmth11(Object what) {
		setStsmth11(what, false);
	}
	public void setStsmth11(Object what, boolean rounded) {
		if (rounded)
			stsmth11.setRounded(what);
		else
			stsmth11.set(what);
	}	
	public PackedDecimalData getStsmth12() {
		return stsmth12;
	}
	public void setStsmth12(Object what) {
		setStsmth12(what, false);
	}
	public void setStsmth12(Object what, boolean rounded) {
		if (rounded)
			stsmth12.setRounded(what);
		else
			stsmth12.set(what);
	}	
	public PackedDecimalData getCfwds() {
		return cfwds;
	}
	public void setCfwds(Object what) {
		setCfwds(what, false);
	}
	public void setCfwds(Object what, boolean rounded) {
		if (rounded)
			cfwds.setRounded(what);
		else
			cfwds.set(what);
	}	
	public PackedDecimalData getBfwdi() {
		return bfwdi;
	}
	public void setBfwdi(Object what) {
		setBfwdi(what, false);
	}
	public void setBfwdi(Object what, boolean rounded) {
		if (rounded)
			bfwdi.setRounded(what);
		else
			bfwdi.set(what);
	}	
	public PackedDecimalData getStimth01() {
		return stimth01;
	}
	public void setStimth01(Object what) {
		setStimth01(what, false);
	}
	public void setStimth01(Object what, boolean rounded) {
		if (rounded)
			stimth01.setRounded(what);
		else
			stimth01.set(what);
	}	
	public PackedDecimalData getStimth02() {
		return stimth02;
	}
	public void setStimth02(Object what) {
		setStimth02(what, false);
	}
	public void setStimth02(Object what, boolean rounded) {
		if (rounded)
			stimth02.setRounded(what);
		else
			stimth02.set(what);
	}	
	public PackedDecimalData getStimth03() {
		return stimth03;
	}
	public void setStimth03(Object what) {
		setStimth03(what, false);
	}
	public void setStimth03(Object what, boolean rounded) {
		if (rounded)
			stimth03.setRounded(what);
		else
			stimth03.set(what);
	}	
	public PackedDecimalData getStimth04() {
		return stimth04;
	}
	public void setStimth04(Object what) {
		setStimth04(what, false);
	}
	public void setStimth04(Object what, boolean rounded) {
		if (rounded)
			stimth04.setRounded(what);
		else
			stimth04.set(what);
	}	
	public PackedDecimalData getStimth05() {
		return stimth05;
	}
	public void setStimth05(Object what) {
		setStimth05(what, false);
	}
	public void setStimth05(Object what, boolean rounded) {
		if (rounded)
			stimth05.setRounded(what);
		else
			stimth05.set(what);
	}	
	public PackedDecimalData getStimth06() {
		return stimth06;
	}
	public void setStimth06(Object what) {
		setStimth06(what, false);
	}
	public void setStimth06(Object what, boolean rounded) {
		if (rounded)
			stimth06.setRounded(what);
		else
			stimth06.set(what);
	}	
	public PackedDecimalData getStimth07() {
		return stimth07;
	}
	public void setStimth07(Object what) {
		setStimth07(what, false);
	}
	public void setStimth07(Object what, boolean rounded) {
		if (rounded)
			stimth07.setRounded(what);
		else
			stimth07.set(what);
	}	
	public PackedDecimalData getStimth08() {
		return stimth08;
	}
	public void setStimth08(Object what) {
		setStimth08(what, false);
	}
	public void setStimth08(Object what, boolean rounded) {
		if (rounded)
			stimth08.setRounded(what);
		else
			stimth08.set(what);
	}	
	public PackedDecimalData getStimth09() {
		return stimth09;
	}
	public void setStimth09(Object what) {
		setStimth09(what, false);
	}
	public void setStimth09(Object what, boolean rounded) {
		if (rounded)
			stimth09.setRounded(what);
		else
			stimth09.set(what);
	}	
	public PackedDecimalData getStimth10() {
		return stimth10;
	}
	public void setStimth10(Object what) {
		setStimth10(what, false);
	}
	public void setStimth10(Object what, boolean rounded) {
		if (rounded)
			stimth10.setRounded(what);
		else
			stimth10.set(what);
	}	
	public PackedDecimalData getStimth11() {
		return stimth11;
	}
	public void setStimth11(Object what) {
		setStimth11(what, false);
	}
	public void setStimth11(Object what, boolean rounded) {
		if (rounded)
			stimth11.setRounded(what);
		else
			stimth11.set(what);
	}	
	public PackedDecimalData getStimth12() {
		return stimth12;
	}
	public void setStimth12(Object what) {
		setStimth12(what, false);
	}
	public void setStimth12(Object what, boolean rounded) {
		if (rounded)
			stimth12.setRounded(what);
		else
			stimth12.set(what);
	}	
	public PackedDecimalData getCfwdi() {
		return cfwdi;
	}
	public void setCfwdi(Object what) {
		setCfwdi(what, false);
	}
	public void setCfwdi(Object what, boolean rounded) {
		if (rounded)
			cfwdi.setRounded(what);
		else
			cfwdi.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getStsmths() {
		return new FixedLengthStringData(stsmth01.toInternal()
										+ stsmth02.toInternal()
										+ stsmth03.toInternal()
										+ stsmth04.toInternal()
										+ stsmth05.toInternal()
										+ stsmth06.toInternal()
										+ stsmth07.toInternal()
										+ stsmth08.toInternal()
										+ stsmth09.toInternal()
										+ stsmth10.toInternal()
										+ stsmth11.toInternal()
										+ stsmth12.toInternal());
	}
	public void setStsmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStsmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stsmth01);
		what = ExternalData.chop(what, stsmth02);
		what = ExternalData.chop(what, stsmth03);
		what = ExternalData.chop(what, stsmth04);
		what = ExternalData.chop(what, stsmth05);
		what = ExternalData.chop(what, stsmth06);
		what = ExternalData.chop(what, stsmth07);
		what = ExternalData.chop(what, stsmth08);
		what = ExternalData.chop(what, stsmth09);
		what = ExternalData.chop(what, stsmth10);
		what = ExternalData.chop(what, stsmth11);
		what = ExternalData.chop(what, stsmth12);
	}
	public PackedDecimalData getStsmth(BaseData indx) {
		return getStsmth(indx.toInt());
	}
	public PackedDecimalData getStsmth(int indx) {

		switch (indx) {
			case 1 : return stsmth01;
			case 2 : return stsmth02;
			case 3 : return stsmth03;
			case 4 : return stsmth04;
			case 5 : return stsmth05;
			case 6 : return stsmth06;
			case 7 : return stsmth07;
			case 8 : return stsmth08;
			case 9 : return stsmth09;
			case 10 : return stsmth10;
			case 11 : return stsmth11;
			case 12 : return stsmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStsmth(BaseData indx, Object what) {
		setStsmth(indx, what, false);
	}
	public void setStsmth(BaseData indx, Object what, boolean rounded) {
		setStsmth(indx.toInt(), what, rounded);
	}
	public void setStsmth(int indx, Object what) {
		setStsmth(indx, what, false);
	}
	public void setStsmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStsmth01(what, rounded);
					 break;
			case 2 : setStsmth02(what, rounded);
					 break;
			case 3 : setStsmth03(what, rounded);
					 break;
			case 4 : setStsmth04(what, rounded);
					 break;
			case 5 : setStsmth05(what, rounded);
					 break;
			case 6 : setStsmth06(what, rounded);
					 break;
			case 7 : setStsmth07(what, rounded);
					 break;
			case 8 : setStsmth08(what, rounded);
					 break;
			case 9 : setStsmth09(what, rounded);
					 break;
			case 10 : setStsmth10(what, rounded);
					 break;
			case 11 : setStsmth11(what, rounded);
					 break;
			case 12 : setStsmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStpmths() {
		return new FixedLengthStringData(stpmth01.toInternal()
										+ stpmth02.toInternal()
										+ stpmth03.toInternal()
										+ stpmth04.toInternal()
										+ stpmth05.toInternal()
										+ stpmth06.toInternal()
										+ stpmth07.toInternal()
										+ stpmth08.toInternal()
										+ stpmth09.toInternal()
										+ stpmth10.toInternal()
										+ stpmth11.toInternal()
										+ stpmth12.toInternal());
	}
	public void setStpmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStpmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stpmth01);
		what = ExternalData.chop(what, stpmth02);
		what = ExternalData.chop(what, stpmth03);
		what = ExternalData.chop(what, stpmth04);
		what = ExternalData.chop(what, stpmth05);
		what = ExternalData.chop(what, stpmth06);
		what = ExternalData.chop(what, stpmth07);
		what = ExternalData.chop(what, stpmth08);
		what = ExternalData.chop(what, stpmth09);
		what = ExternalData.chop(what, stpmth10);
		what = ExternalData.chop(what, stpmth11);
		what = ExternalData.chop(what, stpmth12);
	}
	public PackedDecimalData getStpmth(BaseData indx) {
		return getStpmth(indx.toInt());
	}
	public PackedDecimalData getStpmth(int indx) {

		switch (indx) {
			case 1 : return stpmth01;
			case 2 : return stpmth02;
			case 3 : return stpmth03;
			case 4 : return stpmth04;
			case 5 : return stpmth05;
			case 6 : return stpmth06;
			case 7 : return stpmth07;
			case 8 : return stpmth08;
			case 9 : return stpmth09;
			case 10 : return stpmth10;
			case 11 : return stpmth11;
			case 12 : return stpmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStpmth(BaseData indx, Object what) {
		setStpmth(indx, what, false);
	}
	public void setStpmth(BaseData indx, Object what, boolean rounded) {
		setStpmth(indx.toInt(), what, rounded);
	}
	public void setStpmth(int indx, Object what) {
		setStpmth(indx, what, false);
	}
	public void setStpmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStpmth01(what, rounded);
					 break;
			case 2 : setStpmth02(what, rounded);
					 break;
			case 3 : setStpmth03(what, rounded);
					 break;
			case 4 : setStpmth04(what, rounded);
					 break;
			case 5 : setStpmth05(what, rounded);
					 break;
			case 6 : setStpmth06(what, rounded);
					 break;
			case 7 : setStpmth07(what, rounded);
					 break;
			case 8 : setStpmth08(what, rounded);
					 break;
			case 9 : setStpmth09(what, rounded);
					 break;
			case 10 : setStpmth10(what, rounded);
					 break;
			case 11 : setStpmth11(what, rounded);
					 break;
			case 12 : setStpmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStimths() {
		return new FixedLengthStringData(stimth01.toInternal()
										+ stimth02.toInternal()
										+ stimth03.toInternal()
										+ stimth04.toInternal()
										+ stimth05.toInternal()
										+ stimth06.toInternal()
										+ stimth07.toInternal()
										+ stimth08.toInternal()
										+ stimth09.toInternal()
										+ stimth10.toInternal()
										+ stimth11.toInternal()
										+ stimth12.toInternal());
	}
	public void setStimths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStimths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stimth01);
		what = ExternalData.chop(what, stimth02);
		what = ExternalData.chop(what, stimth03);
		what = ExternalData.chop(what, stimth04);
		what = ExternalData.chop(what, stimth05);
		what = ExternalData.chop(what, stimth06);
		what = ExternalData.chop(what, stimth07);
		what = ExternalData.chop(what, stimth08);
		what = ExternalData.chop(what, stimth09);
		what = ExternalData.chop(what, stimth10);
		what = ExternalData.chop(what, stimth11);
		what = ExternalData.chop(what, stimth12);
	}
	public PackedDecimalData getStimth(BaseData indx) {
		return getStimth(indx.toInt());
	}
	public PackedDecimalData getStimth(int indx) {

		switch (indx) {
			case 1 : return stimth01;
			case 2 : return stimth02;
			case 3 : return stimth03;
			case 4 : return stimth04;
			case 5 : return stimth05;
			case 6 : return stimth06;
			case 7 : return stimth07;
			case 8 : return stimth08;
			case 9 : return stimth09;
			case 10 : return stimth10;
			case 11 : return stimth11;
			case 12 : return stimth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStimth(BaseData indx, Object what) {
		setStimth(indx, what, false);
	}
	public void setStimth(BaseData indx, Object what, boolean rounded) {
		setStimth(indx.toInt(), what, rounded);
	}
	public void setStimth(int indx, Object what) {
		setStimth(indx, what, false);
	}
	public void setStimth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStimth01(what, rounded);
					 break;
			case 2 : setStimth02(what, rounded);
					 break;
			case 3 : setStimth03(what, rounded);
					 break;
			case 4 : setStimth04(what, rounded);
					 break;
			case 5 : setStimth05(what, rounded);
					 break;
			case 6 : setStimth06(what, rounded);
					 break;
			case 7 : setStimth07(what, rounded);
					 break;
			case 8 : setStimth08(what, rounded);
					 break;
			case 9 : setStimth09(what, rounded);
					 break;
			case 10 : setStimth10(what, rounded);
					 break;
			case 11 : setStimth11(what, rounded);
					 break;
			case 12 : setStimth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStcmths() {
		return new FixedLengthStringData(stcmth01.toInternal()
										+ stcmth02.toInternal()
										+ stcmth03.toInternal()
										+ stcmth04.toInternal()
										+ stcmth05.toInternal()
										+ stcmth06.toInternal()
										+ stcmth07.toInternal()
										+ stcmth08.toInternal()
										+ stcmth09.toInternal()
										+ stcmth10.toInternal()
										+ stcmth11.toInternal()
										+ stcmth12.toInternal());
	}
	public void setStcmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStcmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stcmth01);
		what = ExternalData.chop(what, stcmth02);
		what = ExternalData.chop(what, stcmth03);
		what = ExternalData.chop(what, stcmth04);
		what = ExternalData.chop(what, stcmth05);
		what = ExternalData.chop(what, stcmth06);
		what = ExternalData.chop(what, stcmth07);
		what = ExternalData.chop(what, stcmth08);
		what = ExternalData.chop(what, stcmth09);
		what = ExternalData.chop(what, stcmth10);
		what = ExternalData.chop(what, stcmth11);
		what = ExternalData.chop(what, stcmth12);
	}
	public PackedDecimalData getStcmth(BaseData indx) {
		return getStcmth(indx.toInt());
	}
	public PackedDecimalData getStcmth(int indx) {

		switch (indx) {
			case 1 : return stcmth01;
			case 2 : return stcmth02;
			case 3 : return stcmth03;
			case 4 : return stcmth04;
			case 5 : return stcmth05;
			case 6 : return stcmth06;
			case 7 : return stcmth07;
			case 8 : return stcmth08;
			case 9 : return stcmth09;
			case 10 : return stcmth10;
			case 11 : return stcmth11;
			case 12 : return stcmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStcmth(BaseData indx, Object what) {
		setStcmth(indx, what, false);
	}
	public void setStcmth(BaseData indx, Object what, boolean rounded) {
		setStcmth(indx.toInt(), what, rounded);
	}
	public void setStcmth(int indx, Object what) {
		setStcmth(indx, what, false);
	}
	public void setStcmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStcmth01(what, rounded);
					 break;
			case 2 : setStcmth02(what, rounded);
					 break;
			case 3 : setStcmth03(what, rounded);
					 break;
			case 4 : setStcmth04(what, rounded);
					 break;
			case 5 : setStcmth05(what, rounded);
					 break;
			case 6 : setStcmth06(what, rounded);
					 break;
			case 7 : setStcmth07(what, rounded);
					 break;
			case 8 : setStcmth08(what, rounded);
					 break;
			case 9 : setStcmth09(what, rounded);
					 break;
			case 10 : setStcmth10(what, rounded);
					 break;
			case 11 : setStcmth11(what, rounded);
					 break;
			case 12 : setStcmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		statcat.clear();
		acctyr.clear();
		statFund.clear();
		statSect.clear();
		statSubsect.clear();
		register.clear();
		cntbranch.clear();
		bandage.clear();
		bandsa.clear();
		bandprm.clear();
		bandtrm.clear();
		commyr.clear();
		pstatcode.clear();
		cntcurr.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		nonKeyFiller130.clear();
		nonKeyFiller140.clear();
		nonKeyFiller150.clear();
		bfwdc.clear();
		stcmth01.clear();
		stcmth02.clear();
		stcmth03.clear();
		stcmth04.clear();
		stcmth05.clear();
		stcmth06.clear();
		stcmth07.clear();
		stcmth08.clear();
		stcmth09.clear();
		stcmth10.clear();
		stcmth11.clear();
		stcmth12.clear();
		cfwdc.clear();
		bfwdp.clear();
		stpmth01.clear();
		stpmth02.clear();
		stpmth03.clear();
		stpmth04.clear();
		stpmth05.clear();
		stpmth06.clear();
		stpmth07.clear();
		stpmth08.clear();
		stpmth09.clear();
		stpmth10.clear();
		stpmth11.clear();
		stpmth12.clear();
		cfwdp.clear();
		bfwds.clear();
		stsmth01.clear();
		stsmth02.clear();
		stsmth03.clear();
		stsmth04.clear();
		stsmth05.clear();
		stsmth06.clear();
		stsmth07.clear();
		stsmth08.clear();
		stsmth09.clear();
		stsmth10.clear();
		stsmth11.clear();
		stsmth12.clear();
		cfwds.clear();
		bfwdi.clear();
		stimth01.clear();
		stimth02.clear();
		stimth03.clear();
		stimth04.clear();
		stimth05.clear();
		stimth06.clear();
		stimth07.clear();
		stimth08.clear();
		stimth09.clear();
		stimth10.clear();
		stimth11.clear();
		stimth12.clear();
		cfwdi.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}