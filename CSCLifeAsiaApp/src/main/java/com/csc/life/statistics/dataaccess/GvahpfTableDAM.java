package com.csc.life.statistics.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GvahpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:20
 * Class transformed from GVAHPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GvahpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 3287;
	public FixedLengthStringData gvahrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData gvahpfRecord = gvahrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(gvahrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(gvahrec);
	public FixedLengthStringData register = DD.reg.copy().isAPartOf(gvahrec);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(gvahrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(gvahrec);
	public FixedLengthStringData cnPremStat = DD.cnpstat.copy().isAPartOf(gvahrec);
	public FixedLengthStringData acctccy = DD.acctccy.copy().isAPartOf(gvahrec);
	public PackedDecimalData acctyr = DD.acctyr.copy().isAPartOf(gvahrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdfyp = DD.bfwdfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp01 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp02 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp03 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp04 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp05 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp06 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp07 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp08 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp09 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp10 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp11 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyp12 = DD.staccfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdfyp = DD.cfwdfyp.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdrnp = DD.bfwdrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp01 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp02 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp03 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp04 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp05 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp06 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp07 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp08 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp09 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp10 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp11 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrnp12 = DD.staccrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdrnp = DD.cfwdrnp.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdspd = DD.bfwdspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd01 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd02 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd03 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd04 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd05 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd06 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd07 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd08 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd09 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd10 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd11 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspd12 = DD.staccspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdspd = DD.cfwdspd.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdfyc = DD.bfwdfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc01 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc02 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc03 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc04 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc05 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc06 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc07 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc08 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc09 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc10 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc11 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccfyc12 = DD.staccfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdfyc = DD.cfwdfyc.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdrlc = DD.bfwdrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc01 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc02 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc03 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc04 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc05 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc06 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc07 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc08 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc09 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc10 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc11 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrlc12 = DD.staccrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdrlc = DD.cfwdrlc.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdspc = DD.bfwdspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc01 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc02 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc03 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc04 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc05 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc06 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc07 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc08 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc09 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc10 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc11 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccspc12 = DD.staccspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdspc = DD.cfwdspc.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdrb = DD.bfwdrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb01 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb02 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb03 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb04 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb05 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb06 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb07 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb08 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb09 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb10 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb11 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrb12 = DD.staccrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdrb = DD.cfwdrb.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdxb = DD.bfwdxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb01 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb02 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb03 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb04 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb05 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb06 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb07 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb08 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb09 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb10 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb11 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccxb12 = DD.staccxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdxb = DD.cfwdxb.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdtb = DD.bfwdtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb01 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb02 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb03 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb04 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb05 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb06 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb07 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb08 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb09 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb10 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb11 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData stacctb12 = DD.stacctb.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdtb = DD.cfwdtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdib = DD.bfwdib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib01 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib02 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib03 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib04 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib05 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib06 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib07 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib08 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib09 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib10 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib11 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccib12 = DD.staccib.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdib = DD.cfwdib.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwddd = DD.bfwddd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd01 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd02 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd03 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd04 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd05 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd06 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd07 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd08 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd09 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd10 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd11 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdd12 = DD.staccdd.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwddd = DD.cfwddd.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwddi = DD.bfwddi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi01 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi02 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi03 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi04 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi05 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi06 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi07 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi08 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi09 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi10 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi11 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccdi12 = DD.staccdi.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwddi = DD.cfwddi.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdbs = DD.bfwdbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs01 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs02 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs03 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs04 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs05 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs06 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs07 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs08 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs09 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs10 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs11 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccbs12 = DD.staccbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdbs = DD.cfwdbs.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdmtb = DD.bfwdmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb01 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb02 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb03 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb04 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb05 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb06 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb07 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb08 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb09 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb10 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb11 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccmtb12 = DD.staccmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdmtb = DD.cfwdmtb.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdob = DD.bfwdob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob01 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob02 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob03 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob04 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob05 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob06 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob07 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob08 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob09 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob10 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob11 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccob12 = DD.staccob.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdob = DD.cfwdob.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdclr = DD.bfwdclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr01 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr02 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr03 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr04 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr05 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr06 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr07 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr08 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr09 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr10 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr11 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccclr12 = DD.staccclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdclr = DD.cfwdclr.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdrip = DD.bfwdrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip01 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip02 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip03 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip04 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip05 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip06 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip07 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip08 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip09 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip10 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip11 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccrip12 = DD.staccrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdrip = DD.cfwdrip.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdric = DD.bfwdric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric01 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric02 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric03 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric04 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric05 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric06 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric07 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric08 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric09 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric10 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric11 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccric12 = DD.staccric.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdric = DD.cfwdric.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdadv = DD.bfwdadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv01 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv02 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv03 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv04 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv05 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv06 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv07 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv08 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv09 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv10 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv11 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadv12 = DD.staccadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdadv = DD.cfwdadv.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdpdb = DD.bfwdpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb01 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb02 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb03 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb04 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb05 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb06 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb07 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb08 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb09 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb10 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb11 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccpdb12 = DD.staccpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdpdb = DD.cfwdpdb.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdadb = DD.bfwdadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb01 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb02 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb03 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb04 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb05 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb06 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb07 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb08 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb09 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb10 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb11 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadb12 = DD.staccadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdadb = DD.cfwdadb.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdadj = DD.bfwdadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj01 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj02 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj03 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj04 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj05 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj06 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj07 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj08 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj09 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj10 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj11 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccadj12 = DD.staccadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdadj = DD.cfwdadj.copy().isAPartOf(gvahrec);
	public PackedDecimalData bfwdint = DD.bfwdint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint01 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint02 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint03 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint04 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint05 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint06 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint07 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint08 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint09 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint10 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint11 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData staccint12 = DD.staccint.copy().isAPartOf(gvahrec);
	public PackedDecimalData cfwdint = DD.cfwdint.copy().isAPartOf(gvahrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(gvahrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(gvahrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(gvahrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public GvahpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for GvahpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public GvahpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for GvahpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public GvahpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for GvahpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public GvahpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("GVAHPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CNTBRANCH, " +
							"REG, " +
							"SRCEBUS, " +
							"STATCODE, " +
							"CNPSTAT, " +
							"ACCTCCY, " +
							"ACCTYR, " +
							"CNTTYPE, " +
							"BFWDFYP, " +
							"STACCFYP01, " +
							"STACCFYP02, " +
							"STACCFYP03, " +
							"STACCFYP04, " +
							"STACCFYP05, " +
							"STACCFYP06, " +
							"STACCFYP07, " +
							"STACCFYP08, " +
							"STACCFYP09, " +
							"STACCFYP10, " +
							"STACCFYP11, " +
							"STACCFYP12, " +
							"CFWDFYP, " +
							"BFWDRNP, " +
							"STACCRNP01, " +
							"STACCRNP02, " +
							"STACCRNP03, " +
							"STACCRNP04, " +
							"STACCRNP05, " +
							"STACCRNP06, " +
							"STACCRNP07, " +
							"STACCRNP08, " +
							"STACCRNP09, " +
							"STACCRNP10, " +
							"STACCRNP11, " +
							"STACCRNP12, " +
							"CFWDRNP, " +
							"BFWDSPD, " +
							"STACCSPD01, " +
							"STACCSPD02, " +
							"STACCSPD03, " +
							"STACCSPD04, " +
							"STACCSPD05, " +
							"STACCSPD06, " +
							"STACCSPD07, " +
							"STACCSPD08, " +
							"STACCSPD09, " +
							"STACCSPD10, " +
							"STACCSPD11, " +
							"STACCSPD12, " +
							"CFWDSPD, " +
							"BFWDFYC, " +
							"STACCFYC01, " +
							"STACCFYC02, " +
							"STACCFYC03, " +
							"STACCFYC04, " +
							"STACCFYC05, " +
							"STACCFYC06, " +
							"STACCFYC07, " +
							"STACCFYC08, " +
							"STACCFYC09, " +
							"STACCFYC10, " +
							"STACCFYC11, " +
							"STACCFYC12, " +
							"CFWDFYC, " +
							"BFWDRLC, " +
							"STACCRLC01, " +
							"STACCRLC02, " +
							"STACCRLC03, " +
							"STACCRLC04, " +
							"STACCRLC05, " +
							"STACCRLC06, " +
							"STACCRLC07, " +
							"STACCRLC08, " +
							"STACCRLC09, " +
							"STACCRLC10, " +
							"STACCRLC11, " +
							"STACCRLC12, " +
							"CFWDRLC, " +
							"BFWDSPC, " +
							"STACCSPC01, " +
							"STACCSPC02, " +
							"STACCSPC03, " +
							"STACCSPC04, " +
							"STACCSPC05, " +
							"STACCSPC06, " +
							"STACCSPC07, " +
							"STACCSPC08, " +
							"STACCSPC09, " +
							"STACCSPC10, " +
							"STACCSPC11, " +
							"STACCSPC12, " +
							"CFWDSPC, " +
							"BFWDRB, " +
							"STACCRB01, " +
							"STACCRB02, " +
							"STACCRB03, " +
							"STACCRB04, " +
							"STACCRB05, " +
							"STACCRB06, " +
							"STACCRB07, " +
							"STACCRB08, " +
							"STACCRB09, " +
							"STACCRB10, " +
							"STACCRB11, " +
							"STACCRB12, " +
							"CFWDRB, " +
							"BFWDXB, " +
							"STACCXB01, " +
							"STACCXB02, " +
							"STACCXB03, " +
							"STACCXB04, " +
							"STACCXB05, " +
							"STACCXB06, " +
							"STACCXB07, " +
							"STACCXB08, " +
							"STACCXB09, " +
							"STACCXB10, " +
							"STACCXB11, " +
							"STACCXB12, " +
							"CFWDXB, " +
							"BFWDTB, " +
							"STACCTB01, " +
							"STACCTB02, " +
							"STACCTB03, " +
							"STACCTB04, " +
							"STACCTB05, " +
							"STACCTB06, " +
							"STACCTB07, " +
							"STACCTB08, " +
							"STACCTB09, " +
							"STACCTB10, " +
							"STACCTB11, " +
							"STACCTB12, " +
							"CFWDTB, " +
							"BFWDIB, " +
							"STACCIB01, " +
							"STACCIB02, " +
							"STACCIB03, " +
							"STACCIB04, " +
							"STACCIB05, " +
							"STACCIB06, " +
							"STACCIB07, " +
							"STACCIB08, " +
							"STACCIB09, " +
							"STACCIB10, " +
							"STACCIB11, " +
							"STACCIB12, " +
							"CFWDIB, " +
							"BFWDDD, " +
							"STACCDD01, " +
							"STACCDD02, " +
							"STACCDD03, " +
							"STACCDD04, " +
							"STACCDD05, " +
							"STACCDD06, " +
							"STACCDD07, " +
							"STACCDD08, " +
							"STACCDD09, " +
							"STACCDD10, " +
							"STACCDD11, " +
							"STACCDD12, " +
							"CFWDDD, " +
							"BFWDDI, " +
							"STACCDI01, " +
							"STACCDI02, " +
							"STACCDI03, " +
							"STACCDI04, " +
							"STACCDI05, " +
							"STACCDI06, " +
							"STACCDI07, " +
							"STACCDI08, " +
							"STACCDI09, " +
							"STACCDI10, " +
							"STACCDI11, " +
							"STACCDI12, " +
							"CFWDDI, " +
							"BFWDBS, " +
							"STACCBS01, " +
							"STACCBS02, " +
							"STACCBS03, " +
							"STACCBS04, " +
							"STACCBS05, " +
							"STACCBS06, " +
							"STACCBS07, " +
							"STACCBS08, " +
							"STACCBS09, " +
							"STACCBS10, " +
							"STACCBS11, " +
							"STACCBS12, " +
							"CFWDBS, " +
							"BFWDMTB, " +
							"STACCMTB01, " +
							"STACCMTB02, " +
							"STACCMTB03, " +
							"STACCMTB04, " +
							"STACCMTB05, " +
							"STACCMTB06, " +
							"STACCMTB07, " +
							"STACCMTB08, " +
							"STACCMTB09, " +
							"STACCMTB10, " +
							"STACCMTB11, " +
							"STACCMTB12, " +
							"CFWDMTB, " +
							"BFWDOB, " +
							"STACCOB01, " +
							"STACCOB02, " +
							"STACCOB03, " +
							"STACCOB04, " +
							"STACCOB05, " +
							"STACCOB06, " +
							"STACCOB07, " +
							"STACCOB08, " +
							"STACCOB09, " +
							"STACCOB10, " +
							"STACCOB11, " +
							"STACCOB12, " +
							"CFWDOB, " +
							"BFWDCLR, " +
							"STACCCLR01, " +
							"STACCCLR02, " +
							"STACCCLR03, " +
							"STACCCLR04, " +
							"STACCCLR05, " +
							"STACCCLR06, " +
							"STACCCLR07, " +
							"STACCCLR08, " +
							"STACCCLR09, " +
							"STACCCLR10, " +
							"STACCCLR11, " +
							"STACCCLR12, " +
							"CFWDCLR, " +
							"BFWDRIP, " +
							"STACCRIP01, " +
							"STACCRIP02, " +
							"STACCRIP03, " +
							"STACCRIP04, " +
							"STACCRIP05, " +
							"STACCRIP06, " +
							"STACCRIP07, " +
							"STACCRIP08, " +
							"STACCRIP09, " +
							"STACCRIP10, " +
							"STACCRIP11, " +
							"STACCRIP12, " +
							"CFWDRIP, " +
							"BFWDRIC, " +
							"STACCRIC01, " +
							"STACCRIC02, " +
							"STACCRIC03, " +
							"STACCRIC04, " +
							"STACCRIC05, " +
							"STACCRIC06, " +
							"STACCRIC07, " +
							"STACCRIC08, " +
							"STACCRIC09, " +
							"STACCRIC10, " +
							"STACCRIC11, " +
							"STACCRIC12, " +
							"CFWDRIC, " +
							"BFWDADV, " +
							"STACCADV01, " +
							"STACCADV02, " +
							"STACCADV03, " +
							"STACCADV04, " +
							"STACCADV05, " +
							"STACCADV06, " +
							"STACCADV07, " +
							"STACCADV08, " +
							"STACCADV09, " +
							"STACCADV10, " +
							"STACCADV11, " +
							"STACCADV12, " +
							"CFWDADV, " +
							"BFWDPDB, " +
							"STACCPDB01, " +
							"STACCPDB02, " +
							"STACCPDB03, " +
							"STACCPDB04, " +
							"STACCPDB05, " +
							"STACCPDB06, " +
							"STACCPDB07, " +
							"STACCPDB08, " +
							"STACCPDB09, " +
							"STACCPDB10, " +
							"STACCPDB11, " +
							"STACCPDB12, " +
							"CFWDPDB, " +
							"BFWDADB, " +
							"STACCADB01, " +
							"STACCADB02, " +
							"STACCADB03, " +
							"STACCADB04, " +
							"STACCADB05, " +
							"STACCADB06, " +
							"STACCADB07, " +
							"STACCADB08, " +
							"STACCADB09, " +
							"STACCADB10, " +
							"STACCADB11, " +
							"STACCADB12, " +
							"CFWDADB, " +
							"BFWDADJ, " +
							"STACCADJ01, " +
							"STACCADJ02, " +
							"STACCADJ03, " +
							"STACCADJ04, " +
							"STACCADJ05, " +
							"STACCADJ06, " +
							"STACCADJ07, " +
							"STACCADJ08, " +
							"STACCADJ09, " +
							"STACCADJ10, " +
							"STACCADJ11, " +
							"STACCADJ12, " +
							"CFWDADJ, " +
							"BFWDINT, " +
							"STACCINT01, " +
							"STACCINT02, " +
							"STACCINT03, " +
							"STACCINT04, " +
							"STACCINT05, " +
							"STACCINT06, " +
							"STACCINT07, " +
							"STACCINT08, " +
							"STACCINT09, " +
							"STACCINT10, " +
							"STACCINT11, " +
							"STACCINT12, " +
							"CFWDINT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     cntbranch,
                                     register,
                                     srcebus,
                                     statcode,
                                     cnPremStat,
                                     acctccy,
                                     acctyr,
                                     cnttype,
                                     bfwdfyp,
                                     staccfyp01,
                                     staccfyp02,
                                     staccfyp03,
                                     staccfyp04,
                                     staccfyp05,
                                     staccfyp06,
                                     staccfyp07,
                                     staccfyp08,
                                     staccfyp09,
                                     staccfyp10,
                                     staccfyp11,
                                     staccfyp12,
                                     cfwdfyp,
                                     bfwdrnp,
                                     staccrnp01,
                                     staccrnp02,
                                     staccrnp03,
                                     staccrnp04,
                                     staccrnp05,
                                     staccrnp06,
                                     staccrnp07,
                                     staccrnp08,
                                     staccrnp09,
                                     staccrnp10,
                                     staccrnp11,
                                     staccrnp12,
                                     cfwdrnp,
                                     bfwdspd,
                                     staccspd01,
                                     staccspd02,
                                     staccspd03,
                                     staccspd04,
                                     staccspd05,
                                     staccspd06,
                                     staccspd07,
                                     staccspd08,
                                     staccspd09,
                                     staccspd10,
                                     staccspd11,
                                     staccspd12,
                                     cfwdspd,
                                     bfwdfyc,
                                     staccfyc01,
                                     staccfyc02,
                                     staccfyc03,
                                     staccfyc04,
                                     staccfyc05,
                                     staccfyc06,
                                     staccfyc07,
                                     staccfyc08,
                                     staccfyc09,
                                     staccfyc10,
                                     staccfyc11,
                                     staccfyc12,
                                     cfwdfyc,
                                     bfwdrlc,
                                     staccrlc01,
                                     staccrlc02,
                                     staccrlc03,
                                     staccrlc04,
                                     staccrlc05,
                                     staccrlc06,
                                     staccrlc07,
                                     staccrlc08,
                                     staccrlc09,
                                     staccrlc10,
                                     staccrlc11,
                                     staccrlc12,
                                     cfwdrlc,
                                     bfwdspc,
                                     staccspc01,
                                     staccspc02,
                                     staccspc03,
                                     staccspc04,
                                     staccspc05,
                                     staccspc06,
                                     staccspc07,
                                     staccspc08,
                                     staccspc09,
                                     staccspc10,
                                     staccspc11,
                                     staccspc12,
                                     cfwdspc,
                                     bfwdrb,
                                     staccrb01,
                                     staccrb02,
                                     staccrb03,
                                     staccrb04,
                                     staccrb05,
                                     staccrb06,
                                     staccrb07,
                                     staccrb08,
                                     staccrb09,
                                     staccrb10,
                                     staccrb11,
                                     staccrb12,
                                     cfwdrb,
                                     bfwdxb,
                                     staccxb01,
                                     staccxb02,
                                     staccxb03,
                                     staccxb04,
                                     staccxb05,
                                     staccxb06,
                                     staccxb07,
                                     staccxb08,
                                     staccxb09,
                                     staccxb10,
                                     staccxb11,
                                     staccxb12,
                                     cfwdxb,
                                     bfwdtb,
                                     stacctb01,
                                     stacctb02,
                                     stacctb03,
                                     stacctb04,
                                     stacctb05,
                                     stacctb06,
                                     stacctb07,
                                     stacctb08,
                                     stacctb09,
                                     stacctb10,
                                     stacctb11,
                                     stacctb12,
                                     cfwdtb,
                                     bfwdib,
                                     staccib01,
                                     staccib02,
                                     staccib03,
                                     staccib04,
                                     staccib05,
                                     staccib06,
                                     staccib07,
                                     staccib08,
                                     staccib09,
                                     staccib10,
                                     staccib11,
                                     staccib12,
                                     cfwdib,
                                     bfwddd,
                                     staccdd01,
                                     staccdd02,
                                     staccdd03,
                                     staccdd04,
                                     staccdd05,
                                     staccdd06,
                                     staccdd07,
                                     staccdd08,
                                     staccdd09,
                                     staccdd10,
                                     staccdd11,
                                     staccdd12,
                                     cfwddd,
                                     bfwddi,
                                     staccdi01,
                                     staccdi02,
                                     staccdi03,
                                     staccdi04,
                                     staccdi05,
                                     staccdi06,
                                     staccdi07,
                                     staccdi08,
                                     staccdi09,
                                     staccdi10,
                                     staccdi11,
                                     staccdi12,
                                     cfwddi,
                                     bfwdbs,
                                     staccbs01,
                                     staccbs02,
                                     staccbs03,
                                     staccbs04,
                                     staccbs05,
                                     staccbs06,
                                     staccbs07,
                                     staccbs08,
                                     staccbs09,
                                     staccbs10,
                                     staccbs11,
                                     staccbs12,
                                     cfwdbs,
                                     bfwdmtb,
                                     staccmtb01,
                                     staccmtb02,
                                     staccmtb03,
                                     staccmtb04,
                                     staccmtb05,
                                     staccmtb06,
                                     staccmtb07,
                                     staccmtb08,
                                     staccmtb09,
                                     staccmtb10,
                                     staccmtb11,
                                     staccmtb12,
                                     cfwdmtb,
                                     bfwdob,
                                     staccob01,
                                     staccob02,
                                     staccob03,
                                     staccob04,
                                     staccob05,
                                     staccob06,
                                     staccob07,
                                     staccob08,
                                     staccob09,
                                     staccob10,
                                     staccob11,
                                     staccob12,
                                     cfwdob,
                                     bfwdclr,
                                     staccclr01,
                                     staccclr02,
                                     staccclr03,
                                     staccclr04,
                                     staccclr05,
                                     staccclr06,
                                     staccclr07,
                                     staccclr08,
                                     staccclr09,
                                     staccclr10,
                                     staccclr11,
                                     staccclr12,
                                     cfwdclr,
                                     bfwdrip,
                                     staccrip01,
                                     staccrip02,
                                     staccrip03,
                                     staccrip04,
                                     staccrip05,
                                     staccrip06,
                                     staccrip07,
                                     staccrip08,
                                     staccrip09,
                                     staccrip10,
                                     staccrip11,
                                     staccrip12,
                                     cfwdrip,
                                     bfwdric,
                                     staccric01,
                                     staccric02,
                                     staccric03,
                                     staccric04,
                                     staccric05,
                                     staccric06,
                                     staccric07,
                                     staccric08,
                                     staccric09,
                                     staccric10,
                                     staccric11,
                                     staccric12,
                                     cfwdric,
                                     bfwdadv,
                                     staccadv01,
                                     staccadv02,
                                     staccadv03,
                                     staccadv04,
                                     staccadv05,
                                     staccadv06,
                                     staccadv07,
                                     staccadv08,
                                     staccadv09,
                                     staccadv10,
                                     staccadv11,
                                     staccadv12,
                                     cfwdadv,
                                     bfwdpdb,
                                     staccpdb01,
                                     staccpdb02,
                                     staccpdb03,
                                     staccpdb04,
                                     staccpdb05,
                                     staccpdb06,
                                     staccpdb07,
                                     staccpdb08,
                                     staccpdb09,
                                     staccpdb10,
                                     staccpdb11,
                                     staccpdb12,
                                     cfwdpdb,
                                     bfwdadb,
                                     staccadb01,
                                     staccadb02,
                                     staccadb03,
                                     staccadb04,
                                     staccadb05,
                                     staccadb06,
                                     staccadb07,
                                     staccadb08,
                                     staccadb09,
                                     staccadb10,
                                     staccadb11,
                                     staccadb12,
                                     cfwdadb,
                                     bfwdadj,
                                     staccadj01,
                                     staccadj02,
                                     staccadj03,
                                     staccadj04,
                                     staccadj05,
                                     staccadj06,
                                     staccadj07,
                                     staccadj08,
                                     staccadj09,
                                     staccadj10,
                                     staccadj11,
                                     staccadj12,
                                     cfwdadj,
                                     bfwdint,
                                     staccint01,
                                     staccint02,
                                     staccint03,
                                     staccint04,
                                     staccint05,
                                     staccint06,
                                     staccint07,
                                     staccint08,
                                     staccint09,
                                     staccint10,
                                     staccint11,
                                     staccint12,
                                     cfwdint,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		cntbranch.clear();
  		register.clear();
  		srcebus.clear();
  		statcode.clear();
  		cnPremStat.clear();
  		acctccy.clear();
  		acctyr.clear();
  		cnttype.clear();
  		bfwdfyp.clear();
  		staccfyp01.clear();
  		staccfyp02.clear();
  		staccfyp03.clear();
  		staccfyp04.clear();
  		staccfyp05.clear();
  		staccfyp06.clear();
  		staccfyp07.clear();
  		staccfyp08.clear();
  		staccfyp09.clear();
  		staccfyp10.clear();
  		staccfyp11.clear();
  		staccfyp12.clear();
  		cfwdfyp.clear();
  		bfwdrnp.clear();
  		staccrnp01.clear();
  		staccrnp02.clear();
  		staccrnp03.clear();
  		staccrnp04.clear();
  		staccrnp05.clear();
  		staccrnp06.clear();
  		staccrnp07.clear();
  		staccrnp08.clear();
  		staccrnp09.clear();
  		staccrnp10.clear();
  		staccrnp11.clear();
  		staccrnp12.clear();
  		cfwdrnp.clear();
  		bfwdspd.clear();
  		staccspd01.clear();
  		staccspd02.clear();
  		staccspd03.clear();
  		staccspd04.clear();
  		staccspd05.clear();
  		staccspd06.clear();
  		staccspd07.clear();
  		staccspd08.clear();
  		staccspd09.clear();
  		staccspd10.clear();
  		staccspd11.clear();
  		staccspd12.clear();
  		cfwdspd.clear();
  		bfwdfyc.clear();
  		staccfyc01.clear();
  		staccfyc02.clear();
  		staccfyc03.clear();
  		staccfyc04.clear();
  		staccfyc05.clear();
  		staccfyc06.clear();
  		staccfyc07.clear();
  		staccfyc08.clear();
  		staccfyc09.clear();
  		staccfyc10.clear();
  		staccfyc11.clear();
  		staccfyc12.clear();
  		cfwdfyc.clear();
  		bfwdrlc.clear();
  		staccrlc01.clear();
  		staccrlc02.clear();
  		staccrlc03.clear();
  		staccrlc04.clear();
  		staccrlc05.clear();
  		staccrlc06.clear();
  		staccrlc07.clear();
  		staccrlc08.clear();
  		staccrlc09.clear();
  		staccrlc10.clear();
  		staccrlc11.clear();
  		staccrlc12.clear();
  		cfwdrlc.clear();
  		bfwdspc.clear();
  		staccspc01.clear();
  		staccspc02.clear();
  		staccspc03.clear();
  		staccspc04.clear();
  		staccspc05.clear();
  		staccspc06.clear();
  		staccspc07.clear();
  		staccspc08.clear();
  		staccspc09.clear();
  		staccspc10.clear();
  		staccspc11.clear();
  		staccspc12.clear();
  		cfwdspc.clear();
  		bfwdrb.clear();
  		staccrb01.clear();
  		staccrb02.clear();
  		staccrb03.clear();
  		staccrb04.clear();
  		staccrb05.clear();
  		staccrb06.clear();
  		staccrb07.clear();
  		staccrb08.clear();
  		staccrb09.clear();
  		staccrb10.clear();
  		staccrb11.clear();
  		staccrb12.clear();
  		cfwdrb.clear();
  		bfwdxb.clear();
  		staccxb01.clear();
  		staccxb02.clear();
  		staccxb03.clear();
  		staccxb04.clear();
  		staccxb05.clear();
  		staccxb06.clear();
  		staccxb07.clear();
  		staccxb08.clear();
  		staccxb09.clear();
  		staccxb10.clear();
  		staccxb11.clear();
  		staccxb12.clear();
  		cfwdxb.clear();
  		bfwdtb.clear();
  		stacctb01.clear();
  		stacctb02.clear();
  		stacctb03.clear();
  		stacctb04.clear();
  		stacctb05.clear();
  		stacctb06.clear();
  		stacctb07.clear();
  		stacctb08.clear();
  		stacctb09.clear();
  		stacctb10.clear();
  		stacctb11.clear();
  		stacctb12.clear();
  		cfwdtb.clear();
  		bfwdib.clear();
  		staccib01.clear();
  		staccib02.clear();
  		staccib03.clear();
  		staccib04.clear();
  		staccib05.clear();
  		staccib06.clear();
  		staccib07.clear();
  		staccib08.clear();
  		staccib09.clear();
  		staccib10.clear();
  		staccib11.clear();
  		staccib12.clear();
  		cfwdib.clear();
  		bfwddd.clear();
  		staccdd01.clear();
  		staccdd02.clear();
  		staccdd03.clear();
  		staccdd04.clear();
  		staccdd05.clear();
  		staccdd06.clear();
  		staccdd07.clear();
  		staccdd08.clear();
  		staccdd09.clear();
  		staccdd10.clear();
  		staccdd11.clear();
  		staccdd12.clear();
  		cfwddd.clear();
  		bfwddi.clear();
  		staccdi01.clear();
  		staccdi02.clear();
  		staccdi03.clear();
  		staccdi04.clear();
  		staccdi05.clear();
  		staccdi06.clear();
  		staccdi07.clear();
  		staccdi08.clear();
  		staccdi09.clear();
  		staccdi10.clear();
  		staccdi11.clear();
  		staccdi12.clear();
  		cfwddi.clear();
  		bfwdbs.clear();
  		staccbs01.clear();
  		staccbs02.clear();
  		staccbs03.clear();
  		staccbs04.clear();
  		staccbs05.clear();
  		staccbs06.clear();
  		staccbs07.clear();
  		staccbs08.clear();
  		staccbs09.clear();
  		staccbs10.clear();
  		staccbs11.clear();
  		staccbs12.clear();
  		cfwdbs.clear();
  		bfwdmtb.clear();
  		staccmtb01.clear();
  		staccmtb02.clear();
  		staccmtb03.clear();
  		staccmtb04.clear();
  		staccmtb05.clear();
  		staccmtb06.clear();
  		staccmtb07.clear();
  		staccmtb08.clear();
  		staccmtb09.clear();
  		staccmtb10.clear();
  		staccmtb11.clear();
  		staccmtb12.clear();
  		cfwdmtb.clear();
  		bfwdob.clear();
  		staccob01.clear();
  		staccob02.clear();
  		staccob03.clear();
  		staccob04.clear();
  		staccob05.clear();
  		staccob06.clear();
  		staccob07.clear();
  		staccob08.clear();
  		staccob09.clear();
  		staccob10.clear();
  		staccob11.clear();
  		staccob12.clear();
  		cfwdob.clear();
  		bfwdclr.clear();
  		staccclr01.clear();
  		staccclr02.clear();
  		staccclr03.clear();
  		staccclr04.clear();
  		staccclr05.clear();
  		staccclr06.clear();
  		staccclr07.clear();
  		staccclr08.clear();
  		staccclr09.clear();
  		staccclr10.clear();
  		staccclr11.clear();
  		staccclr12.clear();
  		cfwdclr.clear();
  		bfwdrip.clear();
  		staccrip01.clear();
  		staccrip02.clear();
  		staccrip03.clear();
  		staccrip04.clear();
  		staccrip05.clear();
  		staccrip06.clear();
  		staccrip07.clear();
  		staccrip08.clear();
  		staccrip09.clear();
  		staccrip10.clear();
  		staccrip11.clear();
  		staccrip12.clear();
  		cfwdrip.clear();
  		bfwdric.clear();
  		staccric01.clear();
  		staccric02.clear();
  		staccric03.clear();
  		staccric04.clear();
  		staccric05.clear();
  		staccric06.clear();
  		staccric07.clear();
  		staccric08.clear();
  		staccric09.clear();
  		staccric10.clear();
  		staccric11.clear();
  		staccric12.clear();
  		cfwdric.clear();
  		bfwdadv.clear();
  		staccadv01.clear();
  		staccadv02.clear();
  		staccadv03.clear();
  		staccadv04.clear();
  		staccadv05.clear();
  		staccadv06.clear();
  		staccadv07.clear();
  		staccadv08.clear();
  		staccadv09.clear();
  		staccadv10.clear();
  		staccadv11.clear();
  		staccadv12.clear();
  		cfwdadv.clear();
  		bfwdpdb.clear();
  		staccpdb01.clear();
  		staccpdb02.clear();
  		staccpdb03.clear();
  		staccpdb04.clear();
  		staccpdb05.clear();
  		staccpdb06.clear();
  		staccpdb07.clear();
  		staccpdb08.clear();
  		staccpdb09.clear();
  		staccpdb10.clear();
  		staccpdb11.clear();
  		staccpdb12.clear();
  		cfwdpdb.clear();
  		bfwdadb.clear();
  		staccadb01.clear();
  		staccadb02.clear();
  		staccadb03.clear();
  		staccadb04.clear();
  		staccadb05.clear();
  		staccadb06.clear();
  		staccadb07.clear();
  		staccadb08.clear();
  		staccadb09.clear();
  		staccadb10.clear();
  		staccadb11.clear();
  		staccadb12.clear();
  		cfwdadb.clear();
  		bfwdadj.clear();
  		staccadj01.clear();
  		staccadj02.clear();
  		staccadj03.clear();
  		staccadj04.clear();
  		staccadj05.clear();
  		staccadj06.clear();
  		staccadj07.clear();
  		staccadj08.clear();
  		staccadj09.clear();
  		staccadj10.clear();
  		staccadj11.clear();
  		staccadj12.clear();
  		cfwdadj.clear();
  		bfwdint.clear();
  		staccint01.clear();
  		staccint02.clear();
  		staccint03.clear();
  		staccint04.clear();
  		staccint05.clear();
  		staccint06.clear();
  		staccint07.clear();
  		staccint08.clear();
  		staccint09.clear();
  		staccint10.clear();
  		staccint11.clear();
  		staccint12.clear();
  		cfwdint.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getGvahrec() {
  		return gvahrec;
	}

	public FixedLengthStringData getGvahpfRecord() {
  		return gvahpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setGvahrec(what);
	}

	public void setGvahrec(Object what) {
  		this.gvahrec.set(what);
	}

	public void setGvahpfRecord(Object what) {
  		this.gvahpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(gvahrec.getLength());
		result.set(gvahrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}