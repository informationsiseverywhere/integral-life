package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:10
 * Description:
 * Copybook name: LIFSTTRREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Lifsttrrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData lifsttrRec = new FixedLengthStringData(60);
  	public FixedLengthStringData function = new FixedLengthStringData(5).isAPartOf(lifsttrRec, 0);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(lifsttrRec, 5);
  	public FixedLengthStringData batckey = new FixedLengthStringData(19).isAPartOf(lifsttrRec, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(batckey, 0, FILLER);
  	public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batckey, 2);
  	public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batckey, 3);
  	public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batckey, 5);
  	public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batckey, 8);
  	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batckey, 10);
  	public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batckey, 14);
  	public FixedLengthStringData chdrcoy = new FixedLengthStringData(1).isAPartOf(lifsttrRec, 28);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(lifsttrRec, 29);
  	public FixedLengthStringData agntnum = new FixedLengthStringData(8).isAPartOf(lifsttrRec, 37);
  	public FixedLengthStringData oldAgntnum = new FixedLengthStringData(8).isAPartOf(lifsttrRec, 45);
  	public FixedLengthStringData minorChg = new FixedLengthStringData(1).isAPartOf(lifsttrRec, 53);
  	public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(lifsttrRec, 54);
  	public PackedDecimalData trannor = new PackedDecimalData(5, 0).isAPartOf(lifsttrRec, 57);


	public void initialize() {
		COBOLFunctions.initialize(lifsttrRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		lifsttrRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}