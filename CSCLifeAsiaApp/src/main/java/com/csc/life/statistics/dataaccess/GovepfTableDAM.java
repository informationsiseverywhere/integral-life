package com.csc.life.statistics.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GovepfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:05
 * Class transformed from GOVEPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GovepfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 1226;
	public FixedLengthStringData goverec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData govepfRecord = goverec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(goverec);
	public FixedLengthStringData register = DD.reg.copy().isAPartOf(goverec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(goverec);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(goverec);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(goverec);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(goverec);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(goverec);
	public FixedLengthStringData cnPremStat = DD.cnpstat.copy().isAPartOf(goverec);
	public FixedLengthStringData covPremStat = DD.crpstat.copy().isAPartOf(goverec);
	public FixedLengthStringData parind = DD.parind.copy().isAPartOf(goverec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(goverec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(goverec);
	public FixedLengthStringData acctccy = DD.acctccy.copy().isAPartOf(goverec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(goverec);
	public PackedDecimalData acctyr = DD.acctyr.copy().isAPartOf(goverec);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(goverec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(goverec);
	public PackedDecimalData bfwdc = DD.bfwdc.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth01 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth02 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth03 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth04 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth05 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth06 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth07 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth08 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth09 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth10 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth11 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData stcmth12 = DD.stcmth.copy().isAPartOf(goverec);
	public PackedDecimalData cfwdc = DD.cfwdc.copy().isAPartOf(goverec);
	public PackedDecimalData bfwdp = DD.bfwdp.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth01 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth02 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth03 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth04 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth05 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth06 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth07 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth08 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth09 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth10 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth11 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData stpmth12 = DD.stpmth.copy().isAPartOf(goverec);
	public PackedDecimalData cfwdp = DD.cfwdp.copy().isAPartOf(goverec);
	public PackedDecimalData bfwds = DD.bfwds.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth01 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth02 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth03 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth04 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth05 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth06 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth07 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth08 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth09 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth10 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth11 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData stsmth12 = DD.stsmth.copy().isAPartOf(goverec);
	public PackedDecimalData cfwds = DD.cfwds.copy().isAPartOf(goverec);
	public PackedDecimalData bfwdi = DD.bfwdi.copy().isAPartOf(goverec);
	public PackedDecimalData stimth01 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth02 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth03 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth04 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth05 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth06 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth07 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth08 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth09 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth10 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth11 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData stimth12 = DD.stimth.copy().isAPartOf(goverec);
	public PackedDecimalData cfwdi = DD.cfwdi.copy().isAPartOf(goverec);
	public PackedDecimalData bfwdl = DD.bfwdl.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth01 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth02 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth03 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth04 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth05 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth06 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth07 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth08 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth09 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth10 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth11 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData stlmth12 = DD.stlmth.copy().isAPartOf(goverec);
	public PackedDecimalData cfwdl = DD.cfwdl.copy().isAPartOf(goverec);
	public PackedDecimalData bfwda = DD.bfwda.copy().isAPartOf(goverec);
	public PackedDecimalData stamth01 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth02 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth03 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth04 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth05 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth06 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth07 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth08 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth09 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth10 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth11 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData stamth12 = DD.stamth.copy().isAPartOf(goverec);
	public PackedDecimalData cfwda = DD.cfwda.copy().isAPartOf(goverec);
	public PackedDecimalData bfwdb = DD.bfwdb.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg01 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg02 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg03 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg04 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg05 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg06 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg07 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg08 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg09 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg10 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg11 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stbmthg12 = DD.stbmthg.copy().isAPartOf(goverec);
	public PackedDecimalData cfwdb = DD.cfwdb.copy().isAPartOf(goverec);
	public PackedDecimalData bfwdld = DD.bfwdld.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg01 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg02 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg03 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg04 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg05 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg06 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg07 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg08 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg09 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg10 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg11 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData stldmthg12 = DD.stldmthg.copy().isAPartOf(goverec);
	public PackedDecimalData cfwdld = DD.cfwdld.copy().isAPartOf(goverec);
	public PackedDecimalData bfwdra = DD.bfwdra.copy().isAPartOf(goverec);
	public PackedDecimalData straamt01 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt02 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt03 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt04 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt05 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt06 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt07 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt08 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt09 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt10 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt11 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData straamt12 = DD.straamt.copy().isAPartOf(goverec);
	public PackedDecimalData cfwdra = DD.cfwdra.copy().isAPartOf(goverec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(goverec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(goverec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(goverec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public GovepfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for GovepfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public GovepfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for GovepfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public GovepfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for GovepfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public GovepfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("GOVEPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"REG, " +
							"CNTBRANCH, " +
							"STFUND, " +
							"STSECT, " +
							"STSSECT, " +
							"STATCAT, " +
							"CNPSTAT, " +
							"CRPSTAT, " +
							"PARIND, " +
							"CNTTYPE, " +
							"CRTABLE, " +
							"ACCTCCY, " +
							"CNTCURR, " +
							"ACCTYR, " +
							"SRCEBUS, " +
							"BILLFREQ, " +
							"BFWDC, " +
							"STCMTH01, " +
							"STCMTH02, " +
							"STCMTH03, " +
							"STCMTH04, " +
							"STCMTH05, " +
							"STCMTH06, " +
							"STCMTH07, " +
							"STCMTH08, " +
							"STCMTH09, " +
							"STCMTH10, " +
							"STCMTH11, " +
							"STCMTH12, " +
							"CFWDC, " +
							"BFWDP, " +
							"STPMTH01, " +
							"STPMTH02, " +
							"STPMTH03, " +
							"STPMTH04, " +
							"STPMTH05, " +
							"STPMTH06, " +
							"STPMTH07, " +
							"STPMTH08, " +
							"STPMTH09, " +
							"STPMTH10, " +
							"STPMTH11, " +
							"STPMTH12, " +
							"CFWDP, " +
							"BFWDS, " +
							"STSMTH01, " +
							"STSMTH02, " +
							"STSMTH03, " +
							"STSMTH04, " +
							"STSMTH05, " +
							"STSMTH06, " +
							"STSMTH07, " +
							"STSMTH08, " +
							"STSMTH09, " +
							"STSMTH10, " +
							"STSMTH11, " +
							"STSMTH12, " +
							"CFWDS, " +
							"BFWDI, " +
							"STIMTH01, " +
							"STIMTH02, " +
							"STIMTH03, " +
							"STIMTH04, " +
							"STIMTH05, " +
							"STIMTH06, " +
							"STIMTH07, " +
							"STIMTH08, " +
							"STIMTH09, " +
							"STIMTH10, " +
							"STIMTH11, " +
							"STIMTH12, " +
							"CFWDI, " +
							"BFWDL, " +
							"STLMTH01, " +
							"STLMTH02, " +
							"STLMTH03, " +
							"STLMTH04, " +
							"STLMTH05, " +
							"STLMTH06, " +
							"STLMTH07, " +
							"STLMTH08, " +
							"STLMTH09, " +
							"STLMTH10, " +
							"STLMTH11, " +
							"STLMTH12, " +
							"CFWDL, " +
							"BFWDA, " +
							"STAMTH01, " +
							"STAMTH02, " +
							"STAMTH03, " +
							"STAMTH04, " +
							"STAMTH05, " +
							"STAMTH06, " +
							"STAMTH07, " +
							"STAMTH08, " +
							"STAMTH09, " +
							"STAMTH10, " +
							"STAMTH11, " +
							"STAMTH12, " +
							"CFWDA, " +
							"BFWDB, " +
							"STBMTHG01, " +
							"STBMTHG02, " +
							"STBMTHG03, " +
							"STBMTHG04, " +
							"STBMTHG05, " +
							"STBMTHG06, " +
							"STBMTHG07, " +
							"STBMTHG08, " +
							"STBMTHG09, " +
							"STBMTHG10, " +
							"STBMTHG11, " +
							"STBMTHG12, " +
							"CFWDB, " +
							"BFWDLD, " +
							"STLDMTHG01, " +
							"STLDMTHG02, " +
							"STLDMTHG03, " +
							"STLDMTHG04, " +
							"STLDMTHG05, " +
							"STLDMTHG06, " +
							"STLDMTHG07, " +
							"STLDMTHG08, " +
							"STLDMTHG09, " +
							"STLDMTHG10, " +
							"STLDMTHG11, " +
							"STLDMTHG12, " +
							"CFWDLD, " +
							"BFWDRA, " +
							"STRAAMT01, " +
							"STRAAMT02, " +
							"STRAAMT03, " +
							"STRAAMT04, " +
							"STRAAMT05, " +
							"STRAAMT06, " +
							"STRAAMT07, " +
							"STRAAMT08, " +
							"STRAAMT09, " +
							"STRAAMT10, " +
							"STRAAMT11, " +
							"STRAAMT12, " +
							"CFWDRA, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     register,
                                     cntbranch,
                                     statFund,
                                     statSect,
                                     statSubsect,
                                     statcat,
                                     cnPremStat,
                                     covPremStat,
                                     parind,
                                     cnttype,
                                     crtable,
                                     acctccy,
                                     cntcurr,
                                     acctyr,
                                     srcebus,
                                     billfreq,
                                     bfwdc,
                                     stcmth01,
                                     stcmth02,
                                     stcmth03,
                                     stcmth04,
                                     stcmth05,
                                     stcmth06,
                                     stcmth07,
                                     stcmth08,
                                     stcmth09,
                                     stcmth10,
                                     stcmth11,
                                     stcmth12,
                                     cfwdc,
                                     bfwdp,
                                     stpmth01,
                                     stpmth02,
                                     stpmth03,
                                     stpmth04,
                                     stpmth05,
                                     stpmth06,
                                     stpmth07,
                                     stpmth08,
                                     stpmth09,
                                     stpmth10,
                                     stpmth11,
                                     stpmth12,
                                     cfwdp,
                                     bfwds,
                                     stsmth01,
                                     stsmth02,
                                     stsmth03,
                                     stsmth04,
                                     stsmth05,
                                     stsmth06,
                                     stsmth07,
                                     stsmth08,
                                     stsmth09,
                                     stsmth10,
                                     stsmth11,
                                     stsmth12,
                                     cfwds,
                                     bfwdi,
                                     stimth01,
                                     stimth02,
                                     stimth03,
                                     stimth04,
                                     stimth05,
                                     stimth06,
                                     stimth07,
                                     stimth08,
                                     stimth09,
                                     stimth10,
                                     stimth11,
                                     stimth12,
                                     cfwdi,
                                     bfwdl,
                                     stlmth01,
                                     stlmth02,
                                     stlmth03,
                                     stlmth04,
                                     stlmth05,
                                     stlmth06,
                                     stlmth07,
                                     stlmth08,
                                     stlmth09,
                                     stlmth10,
                                     stlmth11,
                                     stlmth12,
                                     cfwdl,
                                     bfwda,
                                     stamth01,
                                     stamth02,
                                     stamth03,
                                     stamth04,
                                     stamth05,
                                     stamth06,
                                     stamth07,
                                     stamth08,
                                     stamth09,
                                     stamth10,
                                     stamth11,
                                     stamth12,
                                     cfwda,
                                     bfwdb,
                                     stbmthg01,
                                     stbmthg02,
                                     stbmthg03,
                                     stbmthg04,
                                     stbmthg05,
                                     stbmthg06,
                                     stbmthg07,
                                     stbmthg08,
                                     stbmthg09,
                                     stbmthg10,
                                     stbmthg11,
                                     stbmthg12,
                                     cfwdb,
                                     bfwdld,
                                     stldmthg01,
                                     stldmthg02,
                                     stldmthg03,
                                     stldmthg04,
                                     stldmthg05,
                                     stldmthg06,
                                     stldmthg07,
                                     stldmthg08,
                                     stldmthg09,
                                     stldmthg10,
                                     stldmthg11,
                                     stldmthg12,
                                     cfwdld,
                                     bfwdra,
                                     straamt01,
                                     straamt02,
                                     straamt03,
                                     straamt04,
                                     straamt05,
                                     straamt06,
                                     straamt07,
                                     straamt08,
                                     straamt09,
                                     straamt10,
                                     straamt11,
                                     straamt12,
                                     cfwdra,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		register.clear();
  		cntbranch.clear();
  		statFund.clear();
  		statSect.clear();
  		statSubsect.clear();
  		statcat.clear();
  		cnPremStat.clear();
  		covPremStat.clear();
  		parind.clear();
  		cnttype.clear();
  		crtable.clear();
  		acctccy.clear();
  		cntcurr.clear();
  		acctyr.clear();
  		srcebus.clear();
  		billfreq.clear();
  		bfwdc.clear();
  		stcmth01.clear();
  		stcmth02.clear();
  		stcmth03.clear();
  		stcmth04.clear();
  		stcmth05.clear();
  		stcmth06.clear();
  		stcmth07.clear();
  		stcmth08.clear();
  		stcmth09.clear();
  		stcmth10.clear();
  		stcmth11.clear();
  		stcmth12.clear();
  		cfwdc.clear();
  		bfwdp.clear();
  		stpmth01.clear();
  		stpmth02.clear();
  		stpmth03.clear();
  		stpmth04.clear();
  		stpmth05.clear();
  		stpmth06.clear();
  		stpmth07.clear();
  		stpmth08.clear();
  		stpmth09.clear();
  		stpmth10.clear();
  		stpmth11.clear();
  		stpmth12.clear();
  		cfwdp.clear();
  		bfwds.clear();
  		stsmth01.clear();
  		stsmth02.clear();
  		stsmth03.clear();
  		stsmth04.clear();
  		stsmth05.clear();
  		stsmth06.clear();
  		stsmth07.clear();
  		stsmth08.clear();
  		stsmth09.clear();
  		stsmth10.clear();
  		stsmth11.clear();
  		stsmth12.clear();
  		cfwds.clear();
  		bfwdi.clear();
  		stimth01.clear();
  		stimth02.clear();
  		stimth03.clear();
  		stimth04.clear();
  		stimth05.clear();
  		stimth06.clear();
  		stimth07.clear();
  		stimth08.clear();
  		stimth09.clear();
  		stimth10.clear();
  		stimth11.clear();
  		stimth12.clear();
  		cfwdi.clear();
  		bfwdl.clear();
  		stlmth01.clear();
  		stlmth02.clear();
  		stlmth03.clear();
  		stlmth04.clear();
  		stlmth05.clear();
  		stlmth06.clear();
  		stlmth07.clear();
  		stlmth08.clear();
  		stlmth09.clear();
  		stlmth10.clear();
  		stlmth11.clear();
  		stlmth12.clear();
  		cfwdl.clear();
  		bfwda.clear();
  		stamth01.clear();
  		stamth02.clear();
  		stamth03.clear();
  		stamth04.clear();
  		stamth05.clear();
  		stamth06.clear();
  		stamth07.clear();
  		stamth08.clear();
  		stamth09.clear();
  		stamth10.clear();
  		stamth11.clear();
  		stamth12.clear();
  		cfwda.clear();
  		bfwdb.clear();
  		stbmthg01.clear();
  		stbmthg02.clear();
  		stbmthg03.clear();
  		stbmthg04.clear();
  		stbmthg05.clear();
  		stbmthg06.clear();
  		stbmthg07.clear();
  		stbmthg08.clear();
  		stbmthg09.clear();
  		stbmthg10.clear();
  		stbmthg11.clear();
  		stbmthg12.clear();
  		cfwdb.clear();
  		bfwdld.clear();
  		stldmthg01.clear();
  		stldmthg02.clear();
  		stldmthg03.clear();
  		stldmthg04.clear();
  		stldmthg05.clear();
  		stldmthg06.clear();
  		stldmthg07.clear();
  		stldmthg08.clear();
  		stldmthg09.clear();
  		stldmthg10.clear();
  		stldmthg11.clear();
  		stldmthg12.clear();
  		cfwdld.clear();
  		bfwdra.clear();
  		straamt01.clear();
  		straamt02.clear();
  		straamt03.clear();
  		straamt04.clear();
  		straamt05.clear();
  		straamt06.clear();
  		straamt07.clear();
  		straamt08.clear();
  		straamt09.clear();
  		straamt10.clear();
  		straamt11.clear();
  		straamt12.clear();
  		cfwdra.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getGoverec() {
  		return goverec;
	}

	public FixedLengthStringData getGovepfRecord() {
  		return govepfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setGoverec(what);
	}

	public void setGoverec(Object what) {
  		this.goverec.set(what);
	}

	public void setGovepfRecord(Object what) {
  		this.govepfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(goverec.getLength());
		result.set(goverec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}