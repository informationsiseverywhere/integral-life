package com.csc.life.statistics.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: SttrpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:26:30
 * Class transformed from STTRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class SttrpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 321;
	public FixedLengthStringData sttrrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData sttrpfRecord = sttrrec;
	
	public FixedLengthStringData chdrpfx = DD.chdrpfx.copy().isAPartOf(sttrrec);
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(sttrrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(sttrrec);
	public PackedDecimalData tranno = DD.tranno.copy().isAPartOf(sttrrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(sttrrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(sttrrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(sttrrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(sttrrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(sttrrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(sttrrec);
	public FixedLengthStringData revtrcde = DD.revtrcde.copy().isAPartOf(sttrrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(sttrrec);
	public FixedLengthStringData statgov = DD.statgov.copy().isAPartOf(sttrrec);
	public FixedLengthStringData statagt = DD.statagt.copy().isAPartOf(sttrrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(sttrrec);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(sttrrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(sttrrec);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(sttrrec);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(sttrrec);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(sttrrec);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(sttrrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(sttrrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(sttrrec);
	public FixedLengthStringData parind = DD.parind.copy().isAPartOf(sttrrec);
	public PackedDecimalData commyr = DD.commyr.copy().isAPartOf(sttrrec);
	public FixedLengthStringData cnPremStat = DD.cnpstat.copy().isAPartOf(sttrrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(sttrrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(sttrrec);
	public FixedLengthStringData register = DD.reg.copy().isAPartOf(sttrrec);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(sttrrec);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(sttrrec);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(sttrrec);
	public FixedLengthStringData bandageg = DD.bandageg.copy().isAPartOf(sttrrec);
	public FixedLengthStringData bandsag = DD.bandsag.copy().isAPartOf(sttrrec);
	public FixedLengthStringData bandprmg = DD.bandprmg.copy().isAPartOf(sttrrec);
	public FixedLengthStringData bandtrmg = DD.bandtrmg.copy().isAPartOf(sttrrec);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(sttrrec);
	public FixedLengthStringData sex = DD.sex.copy().isAPartOf(sttrrec);
	public FixedLengthStringData mortcls = DD.mortcls.copy().isAPartOf(sttrrec);
	public FixedLengthStringData reptcd01 = DD.reptcd.copy().isAPartOf(sttrrec);
	public FixedLengthStringData reptcd02 = DD.reptcd.copy().isAPartOf(sttrrec);
	public FixedLengthStringData reptcd03 = DD.reptcd.copy().isAPartOf(sttrrec);
	public FixedLengthStringData reptcd04 = DD.reptcd.copy().isAPartOf(sttrrec);
	public FixedLengthStringData reptcd05 = DD.reptcd.copy().isAPartOf(sttrrec);
	public FixedLengthStringData reptcd06 = DD.reptcd.copy().isAPartOf(sttrrec);
	public FixedLengthStringData chdrstcda = DD.stca.copy().isAPartOf(sttrrec);
	public FixedLengthStringData chdrstcdb = DD.stcb.copy().isAPartOf(sttrrec);
	public FixedLengthStringData chdrstcdc = DD.stcc.copy().isAPartOf(sttrrec);
	public FixedLengthStringData chdrstcdd = DD.stcd.copy().isAPartOf(sttrrec);
	public FixedLengthStringData chdrstcde = DD.stce.copy().isAPartOf(sttrrec);
	public FixedLengthStringData ovrdcat = DD.ovrdcat.copy().isAPartOf(sttrrec);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(sttrrec);
	public PackedDecimalData stcmth = DD.stcmth.copy().isAPartOf(sttrrec);
	public PackedDecimalData stvmth = DD.stvmth.copy().isAPartOf(sttrrec);
	public PackedDecimalData stpmth = DD.stpmth.copy().isAPartOf(sttrrec);
	public PackedDecimalData stsmth = DD.stsmth.copy().isAPartOf(sttrrec);
	public PackedDecimalData stmmth = DD.stmmth.copy().isAPartOf(sttrrec);
	public PackedDecimalData stimth = DD.stimth.copy().isAPartOf(sttrrec);
	public PackedDecimalData stcmthg = DD.stcmthg.copy().isAPartOf(sttrrec);
	public PackedDecimalData stpmthg = DD.stpmthg.copy().isAPartOf(sttrrec);
	public PackedDecimalData stsmthg = DD.stsmthg.copy().isAPartOf(sttrrec);
	public PackedDecimalData stumth = DD.stumth.copy().isAPartOf(sttrrec);
	public PackedDecimalData stnmth = DD.stnmth.copy().isAPartOf(sttrrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(sttrrec);
	public PackedDecimalData trannor = DD.trannor.copy().isAPartOf(sttrrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(sttrrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(sttrrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(sttrrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(sttrrec);
	public FixedLengthStringData acctccy = DD.acctccy.copy().isAPartOf(sttrrec);
	public PackedDecimalData stlmth = DD.stlmth.copy().isAPartOf(sttrrec);
	public PackedDecimalData stbmthg = DD.stbmthg.copy().isAPartOf(sttrrec);
	public PackedDecimalData stldmthg = DD.stldmthg.copy().isAPartOf(sttrrec);
	public PackedDecimalData straamt = DD.straamt.copy().isAPartOf(sttrrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(sttrrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(sttrrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(sttrrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public SttrpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for SttrpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public SttrpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for SttrpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public SttrpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for SttrpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public SttrpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("STTRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRPFX, " +
							"CHDRCOY, " +
							"CHDRNUM, " +
							"TRANNO, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"REVTRCDE, " +
							"BILLFREQ, " +
							"STATGOV, " +
							"STATAGT, " +
							"AGNTNUM, " +
							"ARACDE, " +
							"CNTBRANCH, " +
							"BANDAGE, " +
							"BANDSA, " +
							"BANDPRM, " +
							"BANDTRM, " +
							"CNTTYPE, " +
							"CRTABLE, " +
							"PARIND, " +
							"COMMYR, " +
							"CNPSTAT, " +
							"PSTATCODE, " +
							"CNTCURR, " +
							"REG, " +
							"STFUND, " +
							"STSECT, " +
							"STSSECT, " +
							"BANDAGEG, " +
							"BANDSAG, " +
							"BANDPRMG, " +
							"BANDTRMG, " +
							"SRCEBUS, " +
							"SEX, " +
							"MORTCLS, " +
							"REPTCD01, " +
							"REPTCD02, " +
							"REPTCD03, " +
							"REPTCD04, " +
							"REPTCD05, " +
							"REPTCD06, " +
							"STCA, " +
							"STCB, " +
							"STCC, " +
							"STCD, " +
							"STCE, " +
							"OVRDCAT, " +
							"STATCAT, " +
							"STCMTH, " +
							"STVMTH, " +
							"STPMTH, " +
							"STSMTH, " +
							"STMMTH, " +
							"STIMTH, " +
							"STCMTHG, " +
							"STPMTHG, " +
							"STSMTHG, " +
							"STUMTH, " +
							"STNMTH, " +
							"TRDT, " +
							"TRANNOR, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"ACCTCCY, " +
							"STLMTH, " +
							"STBMTHG, " +
							"STLDMTHG, " +
							"STRAAMT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrpfx,
                                     chdrcoy,
                                     chdrnum,
                                     tranno,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     revtrcde,
                                     billfreq,
                                     statgov,
                                     statagt,
                                     agntnum,
                                     aracde,
                                     cntbranch,
                                     bandage,
                                     bandsa,
                                     bandprm,
                                     bandtrm,
                                     cnttype,
                                     crtable,
                                     parind,
                                     commyr,
                                     cnPremStat,
                                     pstatcode,
                                     cntcurr,
                                     register,
                                     statFund,
                                     statSect,
                                     statSubsect,
                                     bandageg,
                                     bandsag,
                                     bandprmg,
                                     bandtrmg,
                                     srcebus,
                                     sex,
                                     mortcls,
                                     reptcd01,
                                     reptcd02,
                                     reptcd03,
                                     reptcd04,
                                     reptcd05,
                                     reptcd06,
                                     chdrstcda,
                                     chdrstcdb,
                                     chdrstcdc,
                                     chdrstcdd,
                                     chdrstcde,
                                     ovrdcat,
                                     statcat,
                                     stcmth,
                                     stvmth,
                                     stpmth,
                                     stsmth,
                                     stmmth,
                                     stimth,
                                     stcmthg,
                                     stpmthg,
                                     stsmthg,
                                     stumth,
                                     stnmth,
                                     transactionDate,
                                     trannor,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     acctccy,
                                     stlmth,
                                     stbmthg,
                                     stldmthg,
                                     straamt,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrpfx.clear();
  		chdrcoy.clear();
  		chdrnum.clear();
  		tranno.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		revtrcde.clear();
  		billfreq.clear();
  		statgov.clear();
  		statagt.clear();
  		agntnum.clear();
  		aracde.clear();
  		cntbranch.clear();
  		bandage.clear();
  		bandsa.clear();
  		bandprm.clear();
  		bandtrm.clear();
  		cnttype.clear();
  		crtable.clear();
  		parind.clear();
  		commyr.clear();
  		cnPremStat.clear();
  		pstatcode.clear();
  		cntcurr.clear();
  		register.clear();
  		statFund.clear();
  		statSect.clear();
  		statSubsect.clear();
  		bandageg.clear();
  		bandsag.clear();
  		bandprmg.clear();
  		bandtrmg.clear();
  		srcebus.clear();
  		sex.clear();
  		mortcls.clear();
  		reptcd01.clear();
  		reptcd02.clear();
  		reptcd03.clear();
  		reptcd04.clear();
  		reptcd05.clear();
  		reptcd06.clear();
  		chdrstcda.clear();
  		chdrstcdb.clear();
  		chdrstcdc.clear();
  		chdrstcdd.clear();
  		chdrstcde.clear();
  		ovrdcat.clear();
  		statcat.clear();
  		stcmth.clear();
  		stvmth.clear();
  		stpmth.clear();
  		stsmth.clear();
  		stmmth.clear();
  		stimth.clear();
  		stcmthg.clear();
  		stpmthg.clear();
  		stsmthg.clear();
  		stumth.clear();
  		stnmth.clear();
  		transactionDate.clear();
  		trannor.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		acctccy.clear();
  		stlmth.clear();
  		stbmthg.clear();
  		stldmthg.clear();
  		straamt.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getSttrrec() {
  		return sttrrec;
	}

	public FixedLengthStringData getSttrpfRecord() {
  		return sttrpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setSttrrec(what);
	}

	public void setSttrrec(Object what) {
  		this.sttrrec.set(what);
	}

	public void setSttrpfRecord(Object what) {
  		this.sttrpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(sttrrec.getLength());
		result.set(sttrrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}