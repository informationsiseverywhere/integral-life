package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6704
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6704ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(613);
	public FixedLengthStringData dataFields = new FixedLengthStringData(181).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctyr = DD.acctyr.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acmn = DD.acmn.copyToZonedDecimal().isAPartOf(dataFields,4);
	public ZonedDecimalData acyr = DD.acyr.copyToZonedDecimal().isAPartOf(dataFields,6);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData chdrtype = DD.chdrtype.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,44);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,74);
	public FixedLengthStringData mthldesc = DD.mthldesc.copy().isAPartOf(dataFields,82);
	public FixedLengthStringData ovrdcat = DD.ovrdcat.copy().isAPartOf(dataFields,92);
	public FixedLengthStringData pstatcd = DD.pstatcd.copy().isAPartOf(dataFields,93);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(dataFields,95);
	public ZonedDecimalData stcmth = DD.stcmth.copyToZonedDecimal().isAPartOf(dataFields,97);
	public ZonedDecimalData stmmth = DD.stmmth.copyToZonedDecimal().isAPartOf(dataFields,106);
	public ZonedDecimalData stpmth = DD.stpmth.copyToZonedDecimal().isAPartOf(dataFields,124);
	public ZonedDecimalData stsmth = DD.stsmth.copyToZonedDecimal().isAPartOf(dataFields,142);
	public ZonedDecimalData stvmth = DD.stvmth.copyToZonedDecimal().isAPartOf(dataFields,160);
	public ZonedDecimalData transactionTime = DD.trtm.copyToZonedDecimal().isAPartOf(dataFields,169);
	public ZonedDecimalData user = DD.user.copyToZonedDecimal().isAPartOf(dataFields,175);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(108).isAPartOf(dataArea, 181);
	public FixedLengthStringData acctyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acmnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData acyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bandageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bandprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData bandsaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData bandtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData chdrtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData mthldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData ovrdcatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData pstatcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData statcatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData stcmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData stmmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData stpmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData stsmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData stvmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData trtmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData userErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(324).isAPartOf(dataArea, 289);
	public FixedLengthStringData[] acctyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acmnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] acyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bandageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bandprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] bandsaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] bandtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] mthldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] ovrdcatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] pstatcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] statcatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] stcmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] stmmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] stpmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] stsmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] stvmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] trtmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] userOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S6704screenWritten = new LongData(0);
	public LongData S6704protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6704ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(acctyrOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acyrOut,new String[] {null, null, null, "17",null, null, null, null, null, null, null, null});
		fieldIndMap.put(acmnOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(descripOut,new String[] {null, null, null, "17",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, agntsel, statcat, acctyr, aracde, cntbranch, bandage, bandsa, bandprm, bandtrm, chdrtype, crtable, cntcurr, acyr, acmn, pstatcd, effdate, transactionTime, user, mthldesc, ovrdcat, descrip, stcmth, stvmth, stpmth, stsmth, stmmth};
		screenOutFields = new BaseData[][] {companyOut, agntselOut, statcatOut, acctyrOut, aracdeOut, cntbranchOut, bandageOut, bandsaOut, bandprmOut, bandtrmOut, chdrtypeOut, crtableOut, cntcurrOut, acyrOut, acmnOut, pstatcdOut, effdateOut, trtmOut, userOut, mthldescOut, ovrdcatOut, descripOut, stcmthOut, stvmthOut, stpmthOut, stsmthOut, stmmthOut};
		screenErrFields = new BaseData[] {companyErr, agntselErr, statcatErr, acctyrErr, aracdeErr, cntbranchErr, bandageErr, bandsaErr, bandprmErr, bandtrmErr, chdrtypeErr, crtableErr, cntcurrErr, acyrErr, acmnErr, pstatcdErr, effdateErr, trtmErr, userErr, mthldescErr, ovrdcatErr, descripErr, stcmthErr, stvmthErr, stpmthErr, stsmthErr, stmmthErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6704screen.class;
		protectRecord = S6704protect.class;
	}

}
