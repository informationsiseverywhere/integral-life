package com.csc.life.statistics.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GojnTableDAM.java
 * Date: Sun, 30 Aug 2009 03:38:38
 * Class transformed from GOJN.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GojnTableDAM extends GojnpfTableDAM {

	public GojnTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("GOJN");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", STATCAT"
		             + ", ACCTYR"
		             + ", ACCTMONTH"
		             + ", STFUND"
		             + ", STSECT"
		             + ", STSSECT"
		             + ", REG"
		             + ", CNTBRANCH"
		             + ", BANDAGE"
		             + ", BANDSA"
		             + ", BANDPRM"
		             + ", BANDTRM"
		             + ", COMMYR"
		             + ", PSTATCODE"
		             + ", CNTCURR"
		             + ", EFFDATE"
		             + ", TRTM";
		
		QUALIFIEDCOLUMNS = 
		            "REG, " +
		            "CHDRCOY, " +
		            "CNTBRANCH, " +
		            "STFUND, " +
		            "STSECT, " +
		            "STSSECT, " +
		            "BANDAGE, " +
		            "BANDSA, " +
		            "BANDPRM, " +
		            "BANDTRM, " +
		            "COMMYR, " +
		            "STATCAT, " +
		            "PSTATCODE, " +
		            "CNTCURR, " +
		            "ACCTYR, " +
		            "ACCTMONTH, " +
		            "STCMTH, " +
		            "STPMTH, " +
		            "STSMTH, " +
		            "STIMTH, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "EFFDATE, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "STATCAT ASC, " +
		            "ACCTYR ASC, " +
		            "ACCTMONTH ASC, " +
		            "STFUND ASC, " +
		            "STSECT ASC, " +
		            "STSSECT ASC, " +
		            "REG ASC, " +
		            "CNTBRANCH ASC, " +
		            "BANDAGE ASC, " +
		            "BANDSA ASC, " +
		            "BANDPRM ASC, " +
		            "BANDTRM ASC, " +
		            "COMMYR ASC, " +
		            "PSTATCODE ASC, " +
		            "CNTCURR ASC, " +
		            "EFFDATE DESC, " +
		            "TRTM DESC, " +
					"UNIQUE_NUMBER ASC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "STATCAT DESC, " +
		            "ACCTYR DESC, " +
		            "ACCTMONTH DESC, " +
		            "STFUND DESC, " +
		            "STSECT DESC, " +
		            "STSSECT DESC, " +
		            "REG DESC, " +
		            "CNTBRANCH DESC, " +
		            "BANDAGE DESC, " +
		            "BANDSA DESC, " +
		            "BANDPRM DESC, " +
		            "BANDTRM DESC, " +
		            "COMMYR DESC, " +
		            "PSTATCODE DESC, " +
		            "CNTCURR DESC, " +
		            "EFFDATE ASC, " +
		            "TRTM ASC, " +
					"UNIQUE_NUMBER DESC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               register,
                               chdrcoy,
                               cntbranch,
                               statFund,
                               statSect,
                               statSubsect,
                               bandage,
                               bandsa,
                               bandprm,
                               bandtrm,
                               commyr,
                               statcat,
                               pstatcode,
                               cntcurr,
                               acctyr,
                               acctmonth,
                               stcmth,
                               stpmth,
                               stsmth,
                               stimth,
                               transactionDate,
                               transactionTime,
                               user,
                               effdate,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(19);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getStatcat().toInternal()
					+ getAcctyr().toInternal()
					+ getAcctmonth().toInternal()
					+ getStatFund().toInternal()
					+ getStatSect().toInternal()
					+ getStatSubsect().toInternal()
					+ getRegister().toInternal()
					+ getCntbranch().toInternal()
					+ getBandage().toInternal()
					+ getBandsa().toInternal()
					+ getBandprm().toInternal()
					+ getBandtrm().toInternal()
					+ getCommyr().toInternal()
					+ getPstatcode().toInternal()
					+ getCntcurr().toInternal()
					+ getEffdate().toInternal()
					+ getTransactionTime().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, statcat);
			what = ExternalData.chop(what, acctyr);
			what = ExternalData.chop(what, acctmonth);
			what = ExternalData.chop(what, statFund);
			what = ExternalData.chop(what, statSect);
			what = ExternalData.chop(what, statSubsect);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, bandage);
			what = ExternalData.chop(what, bandsa);
			what = ExternalData.chop(what, bandprm);
			what = ExternalData.chop(what, bandtrm);
			what = ExternalData.chop(what, commyr);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller130 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller140 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller150 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller160 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller220 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller240 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(register.toInternal());
	nonKeyFiller30.setInternal(cntbranch.toInternal());
	nonKeyFiller40.setInternal(statFund.toInternal());
	nonKeyFiller50.setInternal(statSect.toInternal());
	nonKeyFiller60.setInternal(statSubsect.toInternal());
	nonKeyFiller70.setInternal(bandage.toInternal());
	nonKeyFiller80.setInternal(bandsa.toInternal());
	nonKeyFiller90.setInternal(bandprm.toInternal());
	nonKeyFiller100.setInternal(bandtrm.toInternal());
	nonKeyFiller110.setInternal(commyr.toInternal());
	nonKeyFiller120.setInternal(statcat.toInternal());
	nonKeyFiller130.setInternal(pstatcode.toInternal());
	nonKeyFiller140.setInternal(cntcurr.toInternal());
	nonKeyFiller150.setInternal(acctyr.toInternal());
	nonKeyFiller160.setInternal(acctmonth.toInternal());
	nonKeyFiller220.setInternal(transactionTime.toInternal());
	nonKeyFiller240.setInternal(effdate.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(134);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ nonKeyFiller130.toInternal()
					+ nonKeyFiller140.toInternal()
					+ nonKeyFiller150.toInternal()
					+ nonKeyFiller160.toInternal()
					+ getStcmth().toInternal()
					+ getStpmth().toInternal()
					+ getStsmth().toInternal()
					+ getStimth().toInternal()
					+ getTransactionDate().toInternal()
					+ nonKeyFiller220.toInternal()
					+ getUser().toInternal()
					+ nonKeyFiller240.toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, nonKeyFiller130);
			what = ExternalData.chop(what, nonKeyFiller140);
			what = ExternalData.chop(what, nonKeyFiller150);
			what = ExternalData.chop(what, nonKeyFiller160);
			what = ExternalData.chop(what, stcmth);
			what = ExternalData.chop(what, stpmth);
			what = ExternalData.chop(what, stsmth);
			what = ExternalData.chop(what, stimth);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, nonKeyFiller220);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, nonKeyFiller240);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getStatcat() {
		return statcat;
	}
	public void setStatcat(Object what) {
		statcat.set(what);
	}
	public PackedDecimalData getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(Object what) {
		setAcctyr(what, false);
	}
	public void setAcctyr(Object what, boolean rounded) {
		if (rounded)
			acctyr.setRounded(what);
		else
			acctyr.set(what);
	}
	public PackedDecimalData getAcctmonth() {
		return acctmonth;
	}
	public void setAcctmonth(Object what) {
		setAcctmonth(what, false);
	}
	public void setAcctmonth(Object what, boolean rounded) {
		if (rounded)
			acctmonth.setRounded(what);
		else
			acctmonth.set(what);
	}
	public FixedLengthStringData getStatFund() {
		return statFund;
	}
	public void setStatFund(Object what) {
		statFund.set(what);
	}
	public FixedLengthStringData getStatSect() {
		return statSect;
	}
	public void setStatSect(Object what) {
		statSect.set(what);
	}
	public FixedLengthStringData getStatSubsect() {
		return statSubsect;
	}
	public void setStatSubsect(Object what) {
		statSubsect.set(what);
	}
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}
	public FixedLengthStringData getBandage() {
		return bandage;
	}
	public void setBandage(Object what) {
		bandage.set(what);
	}
	public FixedLengthStringData getBandsa() {
		return bandsa;
	}
	public void setBandsa(Object what) {
		bandsa.set(what);
	}
	public FixedLengthStringData getBandprm() {
		return bandprm;
	}
	public void setBandprm(Object what) {
		bandprm.set(what);
	}
	public FixedLengthStringData getBandtrm() {
		return bandtrm;
	}
	public void setBandtrm(Object what) {
		bandtrm.set(what);
	}
	public PackedDecimalData getCommyr() {
		return commyr;
	}
	public void setCommyr(Object what) {
		setCommyr(what, false);
	}
	public void setCommyr(Object what, boolean rounded) {
		if (rounded)
			commyr.setRounded(what);
		else
			commyr.set(what);
	}
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getStcmth() {
		return stcmth;
	}
	public void setStcmth(Object what) {
		setStcmth(what, false);
	}
	public void setStcmth(Object what, boolean rounded) {
		if (rounded)
			stcmth.setRounded(what);
		else
			stcmth.set(what);
	}	
	public PackedDecimalData getStpmth() {
		return stpmth;
	}
	public void setStpmth(Object what) {
		setStpmth(what, false);
	}
	public void setStpmth(Object what, boolean rounded) {
		if (rounded)
			stpmth.setRounded(what);
		else
			stpmth.set(what);
	}	
	public PackedDecimalData getStsmth() {
		return stsmth;
	}
	public void setStsmth(Object what) {
		setStsmth(what, false);
	}
	public void setStsmth(Object what, boolean rounded) {
		if (rounded)
			stsmth.setRounded(what);
		else
			stsmth.set(what);
	}	
	public PackedDecimalData getStimth() {
		return stimth;
	}
	public void setStimth(Object what) {
		setStimth(what, false);
	}
	public void setStimth(Object what, boolean rounded) {
		if (rounded)
			stimth.setRounded(what);
		else
			stimth.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		statcat.clear();
		acctyr.clear();
		acctmonth.clear();
		statFund.clear();
		statSect.clear();
		statSubsect.clear();
		register.clear();
		cntbranch.clear();
		bandage.clear();
		bandsa.clear();
		bandprm.clear();
		bandtrm.clear();
		commyr.clear();
		pstatcode.clear();
		cntcurr.clear();
		effdate.clear();
		transactionTime.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		nonKeyFiller130.clear();
		nonKeyFiller140.clear();
		nonKeyFiller150.clear();
		nonKeyFiller160.clear();
		stcmth.clear();
		stpmth.clear();
		stsmth.clear();
		stimth.clear();
		transactionDate.clear();
		nonKeyFiller220.clear();
		user.clear();
		nonKeyFiller240.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}