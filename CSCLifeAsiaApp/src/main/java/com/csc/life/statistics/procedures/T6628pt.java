/*
 * File: T6628pt.java
 * Date: 30 August 2009 2:27:47
 * Author: Quipoz Limited
 * 
 * Class transformed from T6628PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.statistics.tablestructures.T6628rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6628.
*
*
*****************************************************************
* </pre>
*/
public class T6628pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(22).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(54).isAPartOf(wsaaPrtLine001, 22, FILLER).init("Statistical Category By Transaction              S6628");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(73);
	private FixedLengthStringData filler3 = new FixedLengthStringData(12).isAPartOf(wsaaPrtLine002, 0, FILLER).init("   Company");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 12);
	private FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 13, FILLER).init("  Table");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 21);
	private FixedLengthStringData filler5 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine002, 26, FILLER).init("  Item");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 33);
	private FixedLengthStringData filler6 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine002, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 43);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(57);
	private FixedLengthStringData filler7 = new FixedLengthStringData(57).isAPartOf(wsaaPrtLine003, 0, FILLER).init("   Details                         Statistical Categories");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(77);
	private FixedLengthStringData filler8 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine004, 0, FILLER).init("   Previous              Subtract");
	private FixedLengthStringData fieldNo005 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 35);
	private FixedLengthStringData filler9 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo006 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 39);
	private FixedLengthStringData filler10 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 43);
	private FixedLengthStringData filler11 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo008 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 47);
	private FixedLengthStringData filler12 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo009 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 51);
	private FixedLengthStringData filler13 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 55);
	private FixedLengthStringData filler14 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo011 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 59);
	private FixedLengthStringData filler15 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo012 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 63);
	private FixedLengthStringData filler16 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 67);
	private FixedLengthStringData filler17 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo014 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 71);
	private FixedLengthStringData filler18 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 73, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo015 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 75);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(77);
	private FixedLengthStringData filler19 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler20 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine005, 25, FILLER).init("Add");
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 35);
	private FixedLengthStringData filler21 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo017 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 39);
	private FixedLengthStringData filler22 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo018 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 43);
	private FixedLengthStringData filler23 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 47);
	private FixedLengthStringData filler24 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo020 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 51);
	private FixedLengthStringData filler25 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo021 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 55);
	private FixedLengthStringData filler26 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 59);
	private FixedLengthStringData filler27 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo023 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 63);
	private FixedLengthStringData filler28 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo024 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 67);
	private FixedLengthStringData filler29 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 71);
	private FixedLengthStringData filler30 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 73, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo026 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 75);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(77);
	private FixedLengthStringData filler31 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine006, 0, FILLER).init("   Current               Subtract");
	private FixedLengthStringData fieldNo027 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 35);
	private FixedLengthStringData filler32 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 39);
	private FixedLengthStringData filler33 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo029 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 43);
	private FixedLengthStringData filler34 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo030 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 47);
	private FixedLengthStringData filler35 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 51);
	private FixedLengthStringData filler36 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo032 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 55);
	private FixedLengthStringData filler37 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo033 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 59);
	private FixedLengthStringData filler38 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 63);
	private FixedLengthStringData filler39 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo035 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 67);
	private FixedLengthStringData filler40 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo036 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 71);
	private FixedLengthStringData filler41 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 73, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 75);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(77);
	private FixedLengthStringData filler42 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler43 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine007, 25, FILLER).init("Add");
	private FixedLengthStringData fieldNo038 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 35);
	private FixedLengthStringData filler44 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo039 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 39);
	private FixedLengthStringData filler45 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 43);
	private FixedLengthStringData filler46 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo041 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 47);
	private FixedLengthStringData filler47 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo042 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 51);
	private FixedLengthStringData filler48 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 55);
	private FixedLengthStringData filler49 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo044 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 59);
	private FixedLengthStringData filler50 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo045 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 63);
	private FixedLengthStringData filler51 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 67);
	private FixedLengthStringData filler52 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo047 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 71);
	private FixedLengthStringData filler53 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 73, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo048 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 75);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(77);
	private FixedLengthStringData filler54 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine008, 0, FILLER).init("   Difference  increase  Subtract");
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 35);
	private FixedLengthStringData filler55 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo050 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 39);
	private FixedLengthStringData filler56 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo051 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 43);
	private FixedLengthStringData filler57 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo052 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 47);
	private FixedLengthStringData filler58 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo053 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 51);
	private FixedLengthStringData filler59 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo054 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 55);
	private FixedLengthStringData filler60 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo055 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 59);
	private FixedLengthStringData filler61 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo056 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 63);
	private FixedLengthStringData filler62 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo057 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 67);
	private FixedLengthStringData filler63 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo058 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 71);
	private FixedLengthStringData filler64 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 73, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo059 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 75);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(77);
	private FixedLengthStringData filler65 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler66 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine009, 25, FILLER).init("Add");
	private FixedLengthStringData fieldNo060 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 35);
	private FixedLengthStringData filler67 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo061 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 39);
	private FixedLengthStringData filler68 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo062 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 43);
	private FixedLengthStringData filler69 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo063 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 47);
	private FixedLengthStringData filler70 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo064 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 51);
	private FixedLengthStringData filler71 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo065 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 55);
	private FixedLengthStringData filler72 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo066 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 59);
	private FixedLengthStringData filler73 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo067 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 63);
	private FixedLengthStringData filler74 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo068 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 67);
	private FixedLengthStringData filler75 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo069 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 71);
	private FixedLengthStringData filler76 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 73, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo070 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 75);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(77);
	private FixedLengthStringData filler77 = new FixedLengthStringData(35).isAPartOf(wsaaPrtLine010, 0, FILLER).init("   Difference  decrease  Subtract");
	private FixedLengthStringData fieldNo071 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 35);
	private FixedLengthStringData filler78 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo072 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 39);
	private FixedLengthStringData filler79 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo073 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 43);
	private FixedLengthStringData filler80 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo074 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 47);
	private FixedLengthStringData filler81 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo075 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 51);
	private FixedLengthStringData filler82 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo076 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 55);
	private FixedLengthStringData filler83 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo077 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 59);
	private FixedLengthStringData filler84 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo078 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 63);
	private FixedLengthStringData filler85 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo079 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 67);
	private FixedLengthStringData filler86 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo080 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 71);
	private FixedLengthStringData filler87 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 73, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo081 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 75);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(77);
	private FixedLengthStringData filler88 = new FixedLengthStringData(25).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler89 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine011, 25, FILLER).init("Add");
	private FixedLengthStringData fieldNo082 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 35);
	private FixedLengthStringData filler90 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 37, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo083 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 39);
	private FixedLengthStringData filler91 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 41, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo084 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 43);
	private FixedLengthStringData filler92 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 45, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo085 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 47);
	private FixedLengthStringData filler93 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 49, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo086 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 51);
	private FixedLengthStringData filler94 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 53, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo087 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 55);
	private FixedLengthStringData filler95 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 57, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo088 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 59);
	private FixedLengthStringData filler96 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 61, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo089 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 63);
	private FixedLengthStringData filler97 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 65, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo090 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 67);
	private FixedLengthStringData filler98 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 69, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo091 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 71);
	private FixedLengthStringData filler99 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 73, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo092 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 75);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6628rec t6628rec = new T6628rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6628pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6628rec.t6628Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo005.set(t6628rec.statcatg01);
		fieldNo006.set(t6628rec.statcatg02);
		fieldNo007.set(t6628rec.statcatg03);
		fieldNo008.set(t6628rec.statcatg04);
		fieldNo009.set(t6628rec.statcatg05);
		fieldNo010.set(t6628rec.statcatg06);
		fieldNo011.set(t6628rec.statcatg07);
		fieldNo012.set(t6628rec.statcatg08);
		fieldNo013.set(t6628rec.statcatg09);
		fieldNo014.set(t6628rec.statcatg10);
		fieldNo015.set(t6628rec.statcatg11);
		fieldNo016.set(t6628rec.statcatg12);
		fieldNo017.set(t6628rec.statcatg13);
		fieldNo018.set(t6628rec.statcatg14);
		fieldNo019.set(t6628rec.statcatg15);
		fieldNo020.set(t6628rec.statcatg16);
		fieldNo021.set(t6628rec.statcatg17);
		fieldNo022.set(t6628rec.statcatg18);
		fieldNo023.set(t6628rec.statcatg19);
		fieldNo024.set(t6628rec.statcatg20);
		fieldNo025.set(t6628rec.statcatg21);
		fieldNo026.set(t6628rec.statcatg22);
		fieldNo027.set(t6628rec.statcatg23);
		fieldNo028.set(t6628rec.statcatg24);
		fieldNo029.set(t6628rec.statcatg25);
		fieldNo030.set(t6628rec.statcatg26);
		fieldNo031.set(t6628rec.statcatg27);
		fieldNo032.set(t6628rec.statcatg28);
		fieldNo033.set(t6628rec.statcatg29);
		fieldNo034.set(t6628rec.statcatg30);
		fieldNo035.set(t6628rec.statcatg31);
		fieldNo036.set(t6628rec.statcatg32);
		fieldNo037.set(t6628rec.statcatg33);
		fieldNo038.set(t6628rec.statcatg34);
		fieldNo039.set(t6628rec.statcatg35);
		fieldNo040.set(t6628rec.statcatg36);
		fieldNo041.set(t6628rec.statcatg37);
		fieldNo042.set(t6628rec.statcatg38);
		fieldNo043.set(t6628rec.statcatg39);
		fieldNo044.set(t6628rec.statcatg40);
		fieldNo045.set(t6628rec.statcatg41);
		fieldNo046.set(t6628rec.statcatg42);
		fieldNo047.set(t6628rec.statcatg43);
		fieldNo048.set(t6628rec.statcatg44);
		fieldNo049.set(t6628rec.statcatg45);
		fieldNo050.set(t6628rec.statcatg46);
		fieldNo051.set(t6628rec.statcatg47);
		fieldNo052.set(t6628rec.statcatg48);
		fieldNo053.set(t6628rec.statcatg49);
		fieldNo054.set(t6628rec.statcatg50);
		fieldNo055.set(t6628rec.statcatg51);
		fieldNo056.set(t6628rec.statcatg52);
		fieldNo057.set(t6628rec.statcatg53);
		fieldNo058.set(t6628rec.statcatg54);
		fieldNo059.set(t6628rec.statcatg55);
		fieldNo060.set(t6628rec.statcatg56);
		fieldNo061.set(t6628rec.statcatg57);
		fieldNo062.set(t6628rec.statcatg58);
		fieldNo063.set(t6628rec.statcatg59);
		fieldNo064.set(t6628rec.statcatg60);
		fieldNo065.set(t6628rec.statcatg61);
		fieldNo066.set(t6628rec.statcatg62);
		fieldNo067.set(t6628rec.statcatg63);
		fieldNo068.set(t6628rec.statcatg64);
		fieldNo069.set(t6628rec.statcatg65);
		fieldNo070.set(t6628rec.statcatg66);
		fieldNo071.set(t6628rec.statcatg67);
		fieldNo072.set(t6628rec.statcatg68);
		fieldNo073.set(t6628rec.statcatg69);
		fieldNo074.set(t6628rec.statcatg70);
		fieldNo075.set(t6628rec.statcatg71);
		fieldNo076.set(t6628rec.statcatg72);
		fieldNo077.set(t6628rec.statcatg73);
		fieldNo078.set(t6628rec.statcatg74);
		fieldNo079.set(t6628rec.statcatg75);
		fieldNo080.set(t6628rec.statcatg76);
		fieldNo081.set(t6628rec.statcatg77);
		fieldNo082.set(t6628rec.statcatg78);
		fieldNo083.set(t6628rec.statcatg79);
		fieldNo084.set(t6628rec.statcatg80);
		fieldNo085.set(t6628rec.statcatg81);
		fieldNo086.set(t6628rec.statcatg82);
		fieldNo087.set(t6628rec.statcatg83);
		fieldNo088.set(t6628rec.statcatg84);
		fieldNo089.set(t6628rec.statcatg85);
		fieldNo090.set(t6628rec.statcatg86);
		fieldNo091.set(t6628rec.statcatg87);
		fieldNo092.set(t6628rec.statcatg88);
		fieldNo004.set(tablistrec.longdesc);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
