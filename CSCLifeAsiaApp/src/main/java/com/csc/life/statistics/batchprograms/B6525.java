/*
 * File: B6525.java
 * Date: 29 August 2009 21:22:23
 * Author: Quipoz Limited
 *
 * Class transformed from B6525.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.sql.SQLException;

import com.csc.life.statistics.dataaccess.GovrTableDAM;
import com.csc.life.statistics.dataaccess.SttrTableDAM;
import com.csc.smart.dataaccess.BakyTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*           GOVERNMENT STATISTICAL MOVEMENT POSTING
*
*
* This  program  updates  the  government statistical  accumulation
* accounts with details from  STTR statistical  movement records
* that have been selected to update the GOVR agent accumulation
* accounts. Selection is by the batch extract and agent details
* identifier(STATGOV = 'Y').
*
*
* 400-SECTION
* -----------
*
* SELECT ALL STATISTICAL RECORDS FROM STTR FILE THAT ARE REQUIRED TO BE
* POSTED (BASED ON A PARTICULAR ACCOUNTING PERIOD MM/YY) INTO AN
* SQL TEMPORARY FILE CALLED SQL-POSTPF1
*
*
* An SQL statement is used to select all STTR records that have
* a corresponding Batchkey record in BAKY file. This baky-data-key is
* created in a prior jobstep created by program B0236.
*
* The selection criteria for posting to GOVR file on the following
* items where:
*
*
* Description     BAKY              PARM-JOB parameters
* -----------     ----              -------------------
*
* Job stream      BAKY-JCTLJBN   =  PARM-JOBNUM         AND
* batch           BAKY-JCTLCOY   =  PARM-COMPANY        AND
*                 BAKY-JCTLJN    =  PARM-JOBNAME        AND
*                 BAKY-JCTLJBN   =  PARM-JOBNUM         AND
*                 BAKY-JCTLACTYR =  PARM-ACCTYEAR       AND
*                 BAKY-JCTLACTMN =  PARM-ACCTMN         AND
*                 BAKY-JOBSTEP   =  WSAA-PARM-JS        AND
*
* Extracted       BAKY-BATCCOY   =  STTR-BATCCOY        AND
* Baky keys &     BAKY-BATCBRN   =  STTR-BATCBRN        AND
* STTR batch      BAKY-BATCACTYR =  STTR-BATCACTYR      AND
* details         BAKY-BATCACTMN =  STTR-BATCACTMN      AND
*                 BAKY-BATCTRCDE =  STTR-BATCTRCDE      AND
*                 BAKY-BATCBATCH =  STTR-BATCBATCH      AND
*
*                 STTR-STATGOV   =  'Y'.
*
*
*
* The selected STTR  records  will be arranged in the following
* sequence which the same as the GOVR-DATA-KEY
*
*          Category
*          Statutory fund
*          Section
*          Sub-section
*          Register
*          Servicing branch
*          Age band
*          Sum insured band
*          Premium band
*          Term band
*          Commencement year
*          Premium statistical code
*          Currency
*          Account year
*
*
*
*
*  POST EACH GROUP OF STTR RECORDS WHICH HAVE THE SAME ACCUMULATION
*  DATA KEY INTO GOVR FILE IN SUMMARISED FORM AS EXTRACTED IN THE
*  PREVIOUS SECTION
*
*
*
*  For each SQL record created for each statistical accumulation
*  key, it perform the following sections:
*
*
*     1000-SECTION
*     ------------
*     READH GOVR file to check if it exists.
*
*     IF no accumulation key exists in GOVR,
*        Set flag WSAA-NEW-REC-FLAG to 'Y'
*
*     ELSE
*        Set flag WSAA-NEW-REC-FLAG to 'N'
*
*     END-IF.
*
*
*     2000-SECTION
*     ------------
*
*     IF WSAA-NEW-REC-FLAG to 'Y'
*
*        Initialise the record area for GOVR record in GOVR file
*        Move WRITR function to GOVR-FUNCTION
*
*     ELSE
*        Move REWRT function to GOVR-FUNCTION
*
*     END-IF.
*
*     Add statistical amounts to GOVR record, including the
*     carried forward amounts.
*
*     Update accumulation record on GOVR file.
*
*
*
*     3000-SECTION
*     ------------
*
*     - if it is a new accumulation key in GOVR file, a new record
*       would be created in GOVR file. The carried forward amount
*       of the previous year would be read to update as the brought
*       forward figure of this new record. The carried forward amount
*       of this new record would be updated to all 'future' records
*       that already exists in the system. This is because this program
*       allows the posting of previous years records.
*
*
*
*     For each SQL record fetched,
*
*       Initialise WSAA-BFWDC    WSAA-BFWDS
*                  WSAA-BFWDP    WSAA-BFWDI.
*
*       Move zeroes to GOVR-ACCTYR.
*
*       The following logic would loop until the SQL accumulation key
*       changes with GOVR accumulation key read except for the
*       accounting period concerned:
*
*           Start reading GOVR file on the first accumulation record of
*             the first accounting year.
*
*             - IF accounting year of the GOVR record read is less than
*                  the SQL-ACCTYR (accounting year):
*
*               Roll forward the balance brought forward (BF) and
*                    carried forward (CF) amounts by
*                    adding the posted amounts to the brought foward
*                    and carried forward amounts.
*
*
*             - IF accounting year of the GOVR record read is equal to
*                  the SQL-ACCTYR (accounting year):
*
*                Move WSAA-BFWDC
*                     WSAA-BFWDP
*                     WSAA-BFWDS
*                     WSAA-BFWDI
*                     to their corresponding balance brought amounts
*                     in the GOVR record.
*
*           - IF accounting year of the GOVR record read is more than
*                the SQL-ACCTYR (accounting year):
*
*                Move GOVR-CFWDC  to WSAA-BFWDC
*                     GOVR-CFWDP  to WSAA-BFWDP
*                     GOVR-CFWDS  to WSAA-BFWDS
*                     GOVR-CFWDI  to WSAA-BFWDI
*                     to their corresponding balance brought amounts in
*                     the working storage.
*
*                This is done to retrieve the last carried foward
*                     amounts of the previous accounting year to be
*                     updated as the brought forward amounts for the
*                     posted record.
*
*
*
*           Rewrite GOVR record with the adjusted BF and CF amounts.
*
*           Read for the next record in GOVR record (NEXTR).
*
*
*
*
*
*   Batch control totals will be output:
*
*     01  -  Number of BAKYs selected
*     02  -  Number of GOVRs written
*     03  -  Total no. of contract counts
*     04  -  Total amount of annual premium
*     05  -  Total amount of single premium
*     06  -  Total amount of sum insured
*
*
*
*****************************************************************
* </pre>
*/
public class B6525 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlpostpf1rs = null;
	private java.sql.PreparedStatement sqlpostpf1ps = null;
	private java.sql.Connection sqlpostpf1conn = null;
	private String sqlpostpf1 = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6525");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaParmCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaParmJobname = new FixedLengthStringData(8);
	private PackedDecimalData wsaaParmAcctyear = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaParmAcctmn = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaParmJobnum = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaRunparm1 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaParmCp = new FixedLengthStringData(5).isAPartOf(wsaaRunparm1, 0);
	private FixedLengthStringData wsaaParmJs = new FixedLengthStringData(3).isAPartOf(wsaaRunparm1, 5);
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaStatgovYes = new FixedLengthStringData(1).init("Y");
	private String wsaaGetBfFlag = "";
	private String wsaaNewRecFlag = "";
	private PackedDecimalData wsaaStcmthg = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStpmthg = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStsmthg = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStimth = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwds = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdi = new PackedDecimalData(18, 2);
	private static final String govrrec = "GOVRREC";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler2 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler2, 5);

		/*  Control indicators*/
	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

		/* INDIC-AREA */
	private Indicator[] indicTable = IndicatorInittedArray(99, 1);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
	private BakyTableDAM bakyIO = new BakyTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private GovrTableDAM govrIO = new GovrTableDAM();
	private SttrTableDAM sttrIO = new SttrTableDAM();
	private Varcom varcom = new Varcom();
	private SqlaPostpf1Inner sqlaPostpf1Inner = new SqlaPostpf1Inner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit190,
		eof580,
		exit590,
		exit3190
	}

	public B6525() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainline100()
	{
		try {
			main110();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void main110()
	{
		initialise200();
		readFirstRecord300();
		while ( !(endOfFile.isTrue())) {
			updateGovrRecords400();
		}

		finished900();
		goTo(GotoLabel.exit190);
	}

protected void sqlError180()
	{
		sqlError();
	}

protected void initialise200()
	{
		/*INITIALISE*/
		govrIO.setDataArea(SPACES);
		wsaaBfwdc.set(ZERO);
		wsaaBfwdp.set(ZERO);
		wsaaBfwds.set(ZERO);
		wsaaBfwdi.set(ZERO);
		wsaaEof.set("N");
		wsaaGetBfFlag = "N";
		wsaaParmCompany.set(runparmrec.company);
		wsaaParmJobname.set(runparmrec.jobname);
		wsaaParmAcctyear.set(runparmrec.acctyear);
		wsaaParmAcctmn.set(runparmrec.acctmonth);
		wsaaParmJobnum.set(runparmrec.jobnum);
		wsaaRunparm1.set(runparmrec.runparm1);
		/*EXIT*/
	}

protected void readFirstRecord300()
	{
		/*BEGIN-READING*/
		/*  Define the query required by declaring a cursor*/
		sqlpostpf1 = " SELECT  ST.CHDRCOY, ST.STATCAT, ST.BATCACTYR, ST.BATCACTMN, ST.STFUND, ST.STSECT, ST.STSSECT, ST.REG, ST.CNTBRANCH, ST.BANDAGEG, ST.BANDSAG, ST.BANDPRMG, ST.BANDTRMG, ST.COMMYR, ST.PSTATCODE, ST.CNTCURR, SUM(ST.STCMTHG), SUM(ST.STPMTHG), SUM(ST.STSMTHG), SUM(ST.STIMTH)" +
" FROM   " + getAppVars().getTableNameOverriden("BAKYPF") + "  BK,  " + getAppVars().getTableNameOverriden("STTRPF") + "  ST" +
" WHERE BK.JCTLJBN = ?" +
" AND BK.JCTLCOY = ?" +
" AND BK.JCTLJN = ?" +
" AND BK.JCTLJBN = ?" +
" AND BK.JCTLACTYR = ?" +
" AND BK.JCTLACTMN = ?" +
" AND BK.BATCCOY = ST.BATCCOY" +
" AND BK.BATCBRN = ST.BATCBRN" +
" AND BK.BATCACTYR = ST.BATCACTYR" +
" AND BK.BATCACTMN = ST.BATCACTMN" +
" AND BK.BATCTRCDE = ST.BATCTRCDE" +
" AND BK.BATCBATCH = ST.BATCBATCH" +
" AND BK.JOBSTEP = ?" +
" AND ST.STATGOV = ?" +
" GROUP BY ST.CHDRCOY, ST.STATCAT, ST.BATCACTYR, ST.BATCACTMN, ST.STFUND, ST.STSECT, ST.STSSECT, ST.REG, ST.CNTBRANCH, ST.BANDAGEG, ST.BANDSAG, ST.BANDPRMG, ST.BANDTRMG, ST.COMMYR, ST.PSTATCODE, ST.CNTCURR" +
" ORDER BY ST.CHDRCOY, ST.STATCAT, ST.STFUND, ST.STSECT, ST.STSSECT, ST.REG, ST.CNTBRANCH, ST.BANDAGEG, ST.BANDSAG, ST.BANDPRMG, ST.BANDTRMG, ST.COMMYR, ST.PSTATCODE, ST.CNTCURR, ST.BATCACTYR, ST.BATCACTMN";
		/*   Open the cursor (this runs the query)*/
		sqlerrorflag = false;
		try {
			sqlpostpf1conn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.smart.dataaccess.BakypfTableDAM(), new com.csc.life.statistics.dataaccess.SttrpfTableDAM()});
			sqlpostpf1ps = getAppVars().prepareStatementEmbeded(sqlpostpf1conn, sqlpostpf1);
			getAppVars().setDBNumber(sqlpostpf1ps, 1, wsaaParmJobnum);
			getAppVars().setDBString(sqlpostpf1ps, 2, wsaaParmCompany);
			getAppVars().setDBString(sqlpostpf1ps, 3, wsaaParmJobname);
			getAppVars().setDBNumber(sqlpostpf1ps, 4, wsaaParmJobnum);
			getAppVars().setDBNumber(sqlpostpf1ps, 5, wsaaParmAcctyear);
			getAppVars().setDBNumber(sqlpostpf1ps, 6, wsaaParmAcctmn);
			getAppVars().setDBString(sqlpostpf1ps, 7, wsaaParmJs);
			getAppVars().setDBString(sqlpostpf1ps, 8, wsaaStatgovYes);
			sqlpostpf1rs = getAppVars().executeQuery(sqlpostpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError180();
			throw new RuntimeException("B6525: ERROR (1) - Unexpected return from a transformed GO TO statement: 180-SQL-ERROR");
		}
		/*   Fetch first record*/
		fetchPrimary500();
		/*EXIT*/
	}

protected void updateGovrRecords400()
	{
		/*READ-REFERRED-TO-RECORDS*/
		readGovrFile1000();
		updatePostedRecord2000();
		updateGovrFile3000();
		/*READ-NEXT-RECORD*/
		fetchPrimary500();
		/*EXIT*/
	}

protected void fetchPrimary500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					fetchRecord510();
					updateControls520();
				case eof580:
					eof580();
				case exit590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fetchRecord510()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlpostpf1rs)) {
				getAppVars().getDBObject(sqlpostpf1rs, 1, sqlaPostpf1Inner.sqlChdrcoy);
				getAppVars().getDBObject(sqlpostpf1rs, 2, sqlaPostpf1Inner.sqlStatcat);
				getAppVars().getDBObject(sqlpostpf1rs, 3, sqlaPostpf1Inner.sqlAcctyr);
				getAppVars().getDBObject(sqlpostpf1rs, 4, sqlaPostpf1Inner.sqlAcctmn);
				getAppVars().getDBObject(sqlpostpf1rs, 5, sqlaPostpf1Inner.sqlStfund);
				getAppVars().getDBObject(sqlpostpf1rs, 6, sqlaPostpf1Inner.sqlStsect);
				getAppVars().getDBObject(sqlpostpf1rs, 7, sqlaPostpf1Inner.sqlStssect);
				getAppVars().getDBObject(sqlpostpf1rs, 8, sqlaPostpf1Inner.sqlReg);
				getAppVars().getDBObject(sqlpostpf1rs, 9, sqlaPostpf1Inner.sqlCntbranch);
				getAppVars().getDBObject(sqlpostpf1rs, 10, sqlaPostpf1Inner.sqlBandageg);
				getAppVars().getDBObject(sqlpostpf1rs, 11, sqlaPostpf1Inner.sqlBandsag);
				getAppVars().getDBObject(sqlpostpf1rs, 12, sqlaPostpf1Inner.sqlBandprmg);
				getAppVars().getDBObject(sqlpostpf1rs, 13, sqlaPostpf1Inner.sqlBandtrmg);
				getAppVars().getDBObject(sqlpostpf1rs, 14, sqlaPostpf1Inner.sqlCommyr);
				getAppVars().getDBObject(sqlpostpf1rs, 15, sqlaPostpf1Inner.sqlPstatcode);
				getAppVars().getDBObject(sqlpostpf1rs, 16, sqlaPostpf1Inner.sqlCntcurr);
				getAppVars().getDBObject(sqlpostpf1rs, 17, sqlaPostpf1Inner.sqlStcmthg);
				getAppVars().getDBObject(sqlpostpf1rs, 18, sqlaPostpf1Inner.sqlStpmthg);
				getAppVars().getDBObject(sqlpostpf1rs, 19, sqlaPostpf1Inner.sqlStsmthg);
				getAppVars().getDBObject(sqlpostpf1rs, 20, sqlaPostpf1Inner.sqlStimth);
			}
			else {
				goTo(GotoLabel.eof580);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError180();
			throw new RuntimeException("B6525: ERROR (2) - Unexpected return from a transformed GO TO statement: 180-SQL-ERROR");
		}
	}

protected void updateControls520()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStcmthg);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStpmthg);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStsmthg);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStimth);
		callContot001();
		goTo(GotoLabel.exit590);
	}

protected void eof580()
	{
		wsaaEof.set("Y");
	}

protected void readGovrFile1000()
	{
		para1010();
	}

protected void para1010()
	{
		govrIO.setFormat(govrrec);
		govrIO.setFunction(varcom.readh);
		govrIO.setChdrcoy(sqlaPostpf1Inner.sqlChdrcoy);
		govrIO.setStatcat(sqlaPostpf1Inner.sqlStatcat);
		govrIO.setAcctyr(sqlaPostpf1Inner.sqlAcctyr);
		govrIO.setStatFund(sqlaPostpf1Inner.sqlStfund);
		govrIO.setStatSect(sqlaPostpf1Inner.sqlStsect);
		govrIO.setStatSubsect(sqlaPostpf1Inner.sqlStssect);
		govrIO.setRegister(sqlaPostpf1Inner.sqlReg);
		govrIO.setCntbranch(sqlaPostpf1Inner.sqlCntbranch);
		govrIO.setBandage(sqlaPostpf1Inner.sqlBandageg);
		govrIO.setBandsa(sqlaPostpf1Inner.sqlBandsag);
		govrIO.setBandprm(sqlaPostpf1Inner.sqlBandprmg);
		govrIO.setBandtrm(sqlaPostpf1Inner.sqlBandtrmg);
		govrIO.setCommyr(sqlaPostpf1Inner.sqlCommyr);
		govrIO.setCntcurr(sqlaPostpf1Inner.sqlCntcurr);
		govrIO.setPstatcode(sqlaPostpf1Inner.sqlPstatcode);
		SmartFileCode.execute(appVars, govrIO);
		if (isEQ(govrIO.getStatuz(), varcom.mrnf)) {
			wsaaNewRecFlag = "Y";
			return ;
		}
		else {
			wsaaNewRecFlag = "N";
		}
		if (isNE(govrIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(govrIO.getParams());
			conerrrec.statuz.set(govrIO.getStatuz());
			databaseError006();
		}
	}

protected void updatePostedRecord2000()
	{
		para2010();
	}

protected void para2010()
	{
		if (isEQ(wsaaNewRecFlag,"Y")) {
			govrIO.setFunction(varcom.writr);
			govrIO.setBfwdc(ZERO);
			govrIO.setCfwdc(ZERO);
			govrIO.setBfwdp(ZERO);
			govrIO.setCfwdp(ZERO);
			govrIO.setBfwds(ZERO);
			govrIO.setCfwds(ZERO);
			govrIO.setBfwdi(ZERO);
			govrIO.setCfwdi(ZERO);
			wsaaI.set(1);
			for (wsaaI.set(1); !(isGT(wsaaI,12)); wsaaI.add(1)){
				initializeGovrValues2100();
			}
		}
		else {
			govrIO.setFunction(varcom.rewrt);
		}
		setPrecision(govrIO.getStcmth(sqlaPostpf1Inner.sqlAcctmn), 0);
		govrIO.setStcmth(sqlaPostpf1Inner.sqlAcctmn, add(govrIO.getStcmth(sqlaPostpf1Inner.sqlAcctmn), sqlaPostpf1Inner.sqlStcmthg));
		setPrecision(govrIO.getCfwdc(), 2);
		govrIO.setCfwdc(add(govrIO.getCfwdc(), sqlaPostpf1Inner.sqlStcmthg));
		setPrecision(govrIO.getStpmth(sqlaPostpf1Inner.sqlAcctmn), 0);
		govrIO.setStpmth(sqlaPostpf1Inner.sqlAcctmn, add(govrIO.getStpmth(sqlaPostpf1Inner.sqlAcctmn), sqlaPostpf1Inner.sqlStpmthg));
		setPrecision(govrIO.getCfwdp(), 2);
		govrIO.setCfwdp(add(govrIO.getCfwdp(), sqlaPostpf1Inner.sqlStpmthg));
		setPrecision(govrIO.getStsmth(sqlaPostpf1Inner.sqlAcctmn), 0);
		govrIO.setStsmth(sqlaPostpf1Inner.sqlAcctmn, add(govrIO.getStsmth(sqlaPostpf1Inner.sqlAcctmn), sqlaPostpf1Inner.sqlStsmthg));
		setPrecision(govrIO.getCfwds(), 2);
		govrIO.setCfwds(add(govrIO.getCfwds(), sqlaPostpf1Inner.sqlStsmthg));
		setPrecision(govrIO.getStimth(sqlaPostpf1Inner.sqlAcctmn), 0);
		govrIO.setStimth(sqlaPostpf1Inner.sqlAcctmn, add(govrIO.getStimth(sqlaPostpf1Inner.sqlAcctmn), sqlaPostpf1Inner.sqlStimth));
		setPrecision(govrIO.getCfwdi(), 2);
		govrIO.setCfwdi(add(govrIO.getCfwdi(), sqlaPostpf1Inner.sqlStimth));
		govrIO.setFormat(govrrec);
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(govrIO.getParams());
			databaseError006();
		}
	}

protected void initializeGovrValues2100()
	{
		/*PARA*/
		govrIO.setStcmth(wsaaI, ZERO);
		govrIO.setStpmth(wsaaI, ZERO);
		govrIO.setStsmth(wsaaI, ZERO);
		govrIO.setStimth(wsaaI, ZERO);
		/*EXIT*/
	}

protected void updateGovrFile3000()
	{
		/*PARA*/
		wsaaBfwdc.set(ZERO);
		wsaaBfwdp.set(ZERO);
		wsaaBfwds.set(ZERO);
		wsaaBfwdi.set(ZERO);
		govrIO.setAcctyr(ZERO);
		govrIO.setFormat(govrrec);
		govrIO.setFunction(varcom.begnh);
		wsaaGetBfFlag = "N";
		while ( !(isEQ(wsaaGetBfFlag,"Y"))) {
			checkBalForwardValue3100();
		}

		/*EXIT*/
	}

protected void checkBalForwardValue3100()
	{
		try {
			para3110();
			readNextRecord3120();
		}
		catch (GOTOException e){
		}
	}

protected void para3110()
	{
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)
		&& isNE(govrIO.getStatuz(),varcom.endp)) {
			conerrrec.statuz.set(govrIO.getStatuz());
			conerrrec.params.set(govrIO.getParams());
			databaseError006();
		}
		if (isEQ(govrIO.getStatuz(), varcom.endp)) {
			wsaaGetBfFlag = "Y";
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(sqlaPostpf1Inner.sqlChdrcoy, govrIO.getChdrcoy())
		&& isEQ(sqlaPostpf1Inner.sqlStatcat, govrIO.getStatcat())
		&& isEQ(sqlaPostpf1Inner.sqlStfund, govrIO.getStatFund())
		&& isEQ(sqlaPostpf1Inner.sqlStsect, govrIO.getStatSect())
		&& isEQ(sqlaPostpf1Inner.sqlStssect, govrIO.getStatSubsect())
		&& isEQ(sqlaPostpf1Inner.sqlReg, govrIO.getRegister())
		&& isEQ(sqlaPostpf1Inner.sqlCntbranch, govrIO.getCntbranch())
		&& isEQ(sqlaPostpf1Inner.sqlBandageg, govrIO.getBandage())
		&& isEQ(sqlaPostpf1Inner.sqlBandsag, govrIO.getBandsa())
		&& isEQ(sqlaPostpf1Inner.sqlBandprmg, govrIO.getBandprm())
		&& isEQ(sqlaPostpf1Inner.sqlBandtrmg, govrIO.getBandtrm())
		&& isEQ(sqlaPostpf1Inner.sqlCommyr, govrIO.getCommyr())
		&& isEQ(sqlaPostpf1Inner.sqlPstatcode, govrIO.getPstatcode())
		&& isEQ(sqlaPostpf1Inner.sqlCntcurr, govrIO.getCntcurr())) {
			if (isLT(sqlaPostpf1Inner.sqlAcctyr, govrIO.getAcctyr())) {
				rollForwardUpdate3200();
			}
			else {
				if (isEQ(sqlaPostpf1Inner.sqlAcctyr, govrIO.getAcctyr())) {
					updateBfAmount3300();
				}
				else {
					wsaaBfwdc.set(govrIO.getCfwdc());
					wsaaBfwdp.set(govrIO.getCfwdp());
					wsaaBfwds.set(govrIO.getCfwds());
					wsaaBfwdi.set(govrIO.getCfwdi());
				}
			}
		}
		else {
			wsaaGetBfFlag = "Y";
			goTo(GotoLabel.exit3190);
		}
	}

protected void readNextRecord3120()
	{
		govrIO.setFunction(varcom.nextr);
	}

protected void rollForwardUpdate3200()
	{
		/*PARA*/
		govrIO.setFormat(govrrec);
		govrIO.setFunction(varcom.rewrt);
		setPrecision(govrIO.getBfwdc(), 2);
		govrIO.setBfwdc(add(govrIO.getBfwdc(), sqlaPostpf1Inner.sqlStcmthg));
		setPrecision(govrIO.getCfwdc(), 2);
		govrIO.setCfwdc(add(govrIO.getCfwdc(), sqlaPostpf1Inner.sqlStcmthg));
		setPrecision(govrIO.getBfwdp(), 2);
		govrIO.setBfwdp(add(govrIO.getBfwdp(), sqlaPostpf1Inner.sqlStpmthg));
		setPrecision(govrIO.getCfwdp(), 2);
		govrIO.setCfwdp(add(govrIO.getCfwdp(), sqlaPostpf1Inner.sqlStpmthg));
		setPrecision(govrIO.getBfwds(), 2);
		govrIO.setBfwds(add(govrIO.getBfwds(), sqlaPostpf1Inner.sqlStsmthg));
		setPrecision(govrIO.getCfwds(), 2);
		govrIO.setCfwds(add(govrIO.getCfwds(), sqlaPostpf1Inner.sqlStsmthg));
		setPrecision(govrIO.getBfwdi(), 2);
		govrIO.setBfwdi(add(govrIO.getBfwdi(), sqlaPostpf1Inner.sqlStimth));
		setPrecision(govrIO.getCfwdi(), 2);
		govrIO.setCfwdi(add(govrIO.getCfwdi(), sqlaPostpf1Inner.sqlStimth));
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(), varcom.oK)) {
			conerrrec.params.set(govrIO.getParams());
			databaseError006();
		}
		/*EXIT*/
	}

protected void updateBfAmount3300()
	{
		para3310();
	}

protected void para3310()
	{
		govrIO.setBfwdc(wsaaBfwdc);
		govrIO.setBfwdp(wsaaBfwdp);
		govrIO.setBfwds(wsaaBfwds);
		govrIO.setBfwdi(wsaaBfwdi);
		if (isEQ(wsaaNewRecFlag,"Y")) {
			setPrecision(govrIO.getCfwdc(), 2);
			govrIO.setCfwdc(add(govrIO.getCfwdc(),wsaaBfwdc));
			setPrecision(govrIO.getCfwdp(), 2);
			govrIO.setCfwdp(add(govrIO.getCfwdp(),wsaaBfwdp));
			setPrecision(govrIO.getCfwds(), 2);
			govrIO.setCfwds(add(govrIO.getCfwds(),wsaaBfwds));
			setPrecision(govrIO.getCfwdi(), 2);
			govrIO.setCfwdi(add(govrIO.getCfwdi(),wsaaBfwdi));
		}
		govrIO.setFormat(govrrec);
		govrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(govrIO.getParams());
			conerrrec.params.set(govrIO.getStatuz());
			databaseError006();
		}
	}

protected void finished900()
	{
		/*CLOSE-FILES*/
		/*   Close the cursor*/
		getAppVars().freeDBConnectionIgnoreErr(sqlpostpf1conn, sqlpostpf1ps, sqlpostpf1rs);
		/*EXIT*/
	}

protected void sqlError()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		conerrrec.syserror.set(sqlStatuz);
		systemError005();
		/*EXIT-SQL-ERROR*/
	}
/*
 * Class transformed  from Data Structure SQLA-POSTPF1--INNER
 */
private static final class SqlaPostpf1Inner {

		/* SQLA-POSTPF1 */
	private FixedLengthStringData sqlPostrec = new FixedLengthStringData(71);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 0);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 1);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlPostrec, 3);
	private PackedDecimalData sqlAcctmn = new PackedDecimalData(2, 0).isAPartOf(sqlPostrec, 6);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 8);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 9);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlPostrec, 11);
	private FixedLengthStringData sqlReg = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 15);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 18);
	private FixedLengthStringData sqlBandageg = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 20);
	private FixedLengthStringData sqlBandsag = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 22);
	private FixedLengthStringData sqlBandprmg = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 24);
	private FixedLengthStringData sqlBandtrmg = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 26);
	private PackedDecimalData sqlCommyr = new PackedDecimalData(4, 0).isAPartOf(sqlPostrec, 28);
	private FixedLengthStringData sqlPstatcode = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 31);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 33);
	private PackedDecimalData sqlStcmthg = new PackedDecimalData(9, 0).isAPartOf(sqlPostrec, 36);
	private PackedDecimalData sqlStpmthg = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 41);
	private PackedDecimalData sqlStsmthg = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 51);
	private PackedDecimalData sqlStimth = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 61);
}
}
