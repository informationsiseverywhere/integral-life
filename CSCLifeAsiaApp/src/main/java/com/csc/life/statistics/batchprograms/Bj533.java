/*
 * File: Bj533.java
 * Date: 29 August 2009 21:45:34
 * Author: Quipoz Limited
 * 
 * Class transformed from BJ533.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.sql.SQLException;

import com.csc.life.statistics.dataaccess.GoveTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*      Additional Government Statistics Posting from STTR
*      ==================================================
*
*   This batch program is activated by L2PASTAPST. It will read
*   BAKXPF and for each BAKX it will read all its STTR records.
*   The new accumulation files, GOVE will be created. This file is
*   used for producing the policy admin related statutory reports.
*
*   This is similar to the existing B6525 and GOVR except with a
*   different accumulation key. So as to minimise the changes, this
*   new posting program is created. This is an addition to the existing
*   GOVR. The online government statistics enquiry and journal
*   functionality is not changed. There is no online enquiry or journal
*   provided for the new statistical file.
*
* Multi-Thread Batch Environment Notes:
*
*   1.  This program must follow B0321 not B0236.
*   2.  The BAKX file read has the name 'BAKX' + XX + 9999
*       where 'XX' is the first 2 chars of BSSC-SYSTEM-PARAM04
*       and 9999 is the last four digits of the schedule number.
*       This naming convention is used to allow this program
*       to be used in multiple processes across multiple
*       companies in a single schedule. Regular archiving of
*       these files will avoid conflict with the truncation
*       of the schedule number and unique use of system param
*       will avoid conflict across companies and/or processes.
*
*   Control totals:
*     01  -  No. of BAKX records read
*     02  -  No. GOVE records written
*     03  -  Number of Contracts
*     04  -  Total of annual premiums
*     05  -  Total of single premiums
*     06  -  Total of sums assured
*     07  -  Total of annuity pa
*     08  -  Number of Lives
*     09  -  Total of basic premium
*     10  -  Total of loaded premium
*     10  -  Total of sum reassured
*
*****************************************************************
* </pre>
*/
public class Bj533 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlsttrpfrs = null;
	private java.sql.PreparedStatement sqlsttrpfps = null;
	private java.sql.Connection sqlsttrpfconn = null;
	private String sqlsttrpf = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ533");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaNewGove = "";
	private String wsaaGetGoveBfFlag = "";
	private String wsaaStatgovYes = "Y";
	private String wsaaAnnuity = "";
	//ILIFE-2628 fixed--Array size increased
	private FixedLengthStringData wsaaT6625Table = new FixedLengthStringData(4000);
	private FixedLengthStringData[] wsaaT6625Entries = FLSArrayPartOfStructure(1000, 4, wsaaT6625Table, 0);
	private FixedLengthStringData[] wsaaT6625Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT6625Entries, 0, HIVALUES);
	private PackedDecimalData wsaaBfwdc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwds = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdi = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdl = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwda = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdld = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdra = new PackedDecimalData(18, 2);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaBakxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaBakxFn, 0, FILLER).init("BAKX");
	private FixedLengthStringData wsaaBakxRunid = new FixedLengthStringData(2).isAPartOf(wsaaBakxFn, 4);
	private ZonedDecimalData wsaaBakxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaBakxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);

		/* SQL-STTRPF */
	private FixedLengthStringData sqlSttrrec = new FixedLengthStringData(112);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlSttrrec, 0);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlSttrrec, 1);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlSttrrec, 3);
	private PackedDecimalData sqlAcctmn = new PackedDecimalData(2, 0).isAPartOf(sqlSttrrec, 6);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlSttrrec, 8);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlSttrrec, 9);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlSttrrec, 11);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlSttrrec, 15);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlSttrrec, 18);
	private FixedLengthStringData sqlCrpstat = new FixedLengthStringData(2).isAPartOf(sqlSttrrec, 20);
	private FixedLengthStringData sqlCnpstat = new FixedLengthStringData(2).isAPartOf(sqlSttrrec, 22);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlSttrrec, 24);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlSttrrec, 27);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlSttrrec, 31);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlSttrrec, 34);
	private FixedLengthStringData sqlSrcebus = new FixedLengthStringData(2).isAPartOf(sqlSttrrec, 37);
	private FixedLengthStringData sqlParind = new FixedLengthStringData(1).isAPartOf(sqlSttrrec, 39);
	private FixedLengthStringData sqlBillfreq = new FixedLengthStringData(2).isAPartOf(sqlSttrrec, 40);
	private PackedDecimalData sqlStcmthg = new PackedDecimalData(9, 0).isAPartOf(sqlSttrrec, 42);
	private PackedDecimalData sqlStpmthg = new PackedDecimalData(18, 2).isAPartOf(sqlSttrrec, 47);
	private PackedDecimalData sqlStsmthg = new PackedDecimalData(18, 2).isAPartOf(sqlSttrrec, 57);
	private PackedDecimalData sqlStbmthg = new PackedDecimalData(18, 2).isAPartOf(sqlSttrrec, 67);
	private PackedDecimalData sqlStldmthg = new PackedDecimalData(18, 2).isAPartOf(sqlSttrrec, 77);
	private PackedDecimalData sqlStimth = new PackedDecimalData(18, 2).isAPartOf(sqlSttrrec, 87);
	private PackedDecimalData sqlStraamt = new PackedDecimalData(18, 2).isAPartOf(sqlSttrrec, 97);
	private PackedDecimalData sqlStlmth = new PackedDecimalData(9, 0).isAPartOf(sqlSttrrec, 107);
		/* ERRORS */
	private String esql = "ESQL";
	private String h791 = "H791";
	private String goverec = "GOVEREC";
	private String itemrec = "ITEMREC";
		/* TABLES */
	private String t6625 = "T6625";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private int ct08 = 8;
	private int ct09 = 9;
	private int ct10 = 10;
	private int ct11 = 11;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData ix = new IntegerData();
		/*Additional Govr Statistics Accumulation*/
	private GoveTableDAM goveIO = new GoveTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1190, 
		eof2080, 
		exit2090, 
		exit7190
	}

	public Bj533() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		ix.set(1);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6625);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		itemIO.setFormat(itemrec);
		while ( !(isEQ(itemIO.getStatuz(),varcom.endp))) {
			
			//performance improvement --  atiwari23 
			itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			itemIO.setFitKeysSearch("ITEMCOY","ITEMTABL");
		

			loadT66251100();
		}
		
		wsaaBakxRunid.set(bprdIO.getSystemParam04());
		wsaaBakxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(BAKXPF) TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaBakxFn.toString());
		stringVariable1.append(") ");
		stringVariable1.append("MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		sqlsttrpf = " SELECT  ST.CHDRCOY, ST.STATCAT, ST.BATCACTYR, ST.BATCACTMN, ST.STFUND, ST.STSECT, ST.STSSECT, ST.REG, ST.CNTBRANCH, ST.PSTATCODE, ST.CNPSTAT, ST.CNTTYPE, ST.CRTABLE, ST.ACCTCCY, ST.CNTCURR, ST.SRCEBUS, ST.PARIND, ST.BILLFREQ, SUM(ST.STCMTHG), SUM(ST.STPMTHG), SUM(ST.STSMTHG), SUM(ST.STBMTHG), SUM(ST.STLDMTHG), SUM(ST.STIMTH), SUM(ST.STRAAMT), SUM(ST.STLMTH)" +
" FROM   " + appVars.getTableNameOverriden("BAKXPF") + "  BK,  " + appVars.getTableNameOverriden("STTRPF") + "  ST" +
" WHERE BK.BATCCOY = ST.BATCCOY" +
" AND BK.BATCBRN = ST.BATCBRN" +
" AND BK.BATCACTYR = ST.BATCACTYR" +
" AND BK.BATCACTMN = ST.BATCACTMN" +
" AND BK.BATCTRCDE = ST.BATCTRCDE" +
" AND BK.BATCBATCH = ST.BATCBATCH" +
" AND ST.STATGOV = ?" +
" GROUP BY ST.CHDRCOY, ST.CNTBRANCH, ST.BATCACTYR, ST.BATCACTMN, ST.STATCAT, ST.ACCTCCY, ST.CNTCURR, ST.SRCEBUS, ST.REG, ST.STFUND, ST.STSECT, ST.STSSECT, ST.CNTTYPE, ST.CRTABLE, ST.PARIND, ST.CNPSTAT, ST.PSTATCODE, ST.BILLFREQ" +
" ORDER BY ST.CHDRCOY, ST.CNTBRANCH, ST.BATCACTYR, ST.BATCACTMN, ST.STATCAT, ST.ACCTCCY, ST.CNTCURR, ST.SRCEBUS, ST.REG, ST.STFUND, ST.STSECT, ST.STSSECT, ST.CNTTYPE, ST.CRTABLE, ST.PARIND, ST.CNPSTAT, ST.PSTATCODE, ST.BILLFREQ";
		sqlerrorflag = false;
		try {
			sqlsttrpfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.SttrpfTableDAM());
			sqlsttrpfps = appVars.prepareStatementEmbeded(sqlsttrpfconn, sqlsttrpf);
			appVars.setDBString(sqlsttrpfps, 1, wsaaStatgovYes);
			sqlsttrpfrs = appVars.executeQuery(sqlsttrpfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void loadT66251100()
	{
		try {
			loadT66251110();
		}
		catch (GOTOException e){
		}
	}

protected void loadT66251110()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		if (isEQ(itemIO.getStatuz(),varcom.endp) 
		|| isNE(itemIO.getItemcoy(),bsprIO.getCompany())
		|| isNE(itemIO.getItemtabl(),t6625)) {
			itemIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		if (isGT(ix,1000)) { //ILIFE-2628 fixed--Array size increased
			syserrrec.statuz.set(h791);
			fatalError600();
		}
		wsaaT6625Crtable[ix.toInt()].set(itemIO.getItemitem());
		ix.add(1);
		itemIO.setFunction(varcom.nextr);
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlsttrpfrs.next()) {
				appVars.getDBObject(sqlsttrpfrs, 1, sqlChdrcoy);
				appVars.getDBObject(sqlsttrpfrs, 2, sqlStatcat);
				appVars.getDBObject(sqlsttrpfrs, 3, sqlAcctyr);
				appVars.getDBObject(sqlsttrpfrs, 4, sqlAcctmn);
				appVars.getDBObject(sqlsttrpfrs, 5, sqlStfund);
				appVars.getDBObject(sqlsttrpfrs, 6, sqlStsect);
				appVars.getDBObject(sqlsttrpfrs, 7, sqlStssect);
				appVars.getDBObject(sqlsttrpfrs, 8, sqlRegister);
				appVars.getDBObject(sqlsttrpfrs, 9, sqlCntbranch);
				appVars.getDBObject(sqlsttrpfrs, 10, sqlCrpstat);
				appVars.getDBObject(sqlsttrpfrs, 11, sqlCnpstat);
				appVars.getDBObject(sqlsttrpfrs, 12, sqlCnttype);
				appVars.getDBObject(sqlsttrpfrs, 13, sqlCrtable);
				appVars.getDBObject(sqlsttrpfrs, 14, sqlAcctccy);
				appVars.getDBObject(sqlsttrpfrs, 15, sqlCntcurr);
				appVars.getDBObject(sqlsttrpfrs, 16, sqlSrcebus);
				appVars.getDBObject(sqlsttrpfrs, 17, sqlParind);
				appVars.getDBObject(sqlsttrpfrs, 18, sqlBillfreq);
				appVars.getDBObject(sqlsttrpfrs, 19, sqlStcmthg);
				appVars.getDBObject(sqlsttrpfrs, 20, sqlStpmthg);
				appVars.getDBObject(sqlsttrpfrs, 21, sqlStsmthg);
				appVars.getDBObject(sqlsttrpfrs, 22, sqlStbmthg);
				appVars.getDBObject(sqlsttrpfrs, 23, sqlStldmthg);
				appVars.getDBObject(sqlsttrpfrs, 24, sqlStimth);
				appVars.getDBObject(sqlsttrpfrs, 25, sqlStraamt);
				appVars.getDBObject(sqlsttrpfrs, 26, sqlStlmth);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		ArraySearch as1 = ArraySearch.getInstance(wsaaT6625Entries);
		as1.setIndices(ix);
		as1.addSearchKey(wsaaT6625Crtable, sqlCrtable, true);
		if (as1.binarySearch()) {
			wsaaAnnuity = "Y";
		}
		else {
			wsaaAnnuity = "N";
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		readGove5000();
		updatePostedRecord6000();
		updateGove7000();
		controlTotals8000();
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlsttrpfconn, sqlsttrpfps, sqlsttrpfrs);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void readGove5000()
	{
		para5010();
	}

protected void para5010()
	{
		goveIO.setParams(SPACES);
		goveIO.setChdrcoy(sqlChdrcoy);
		goveIO.setCntbranch(sqlCntbranch);
		goveIO.setAcctccy(sqlAcctccy);
		goveIO.setCntcurr(sqlCntcurr);
		goveIO.setAcctyr(sqlAcctyr);
		goveIO.setSrcebus(sqlSrcebus);
		goveIO.setRegister(sqlRegister);
		goveIO.setStatFund(sqlStfund);
		goveIO.setStatSect(sqlStsect);
		goveIO.setStatSubsect(sqlStssect);
		goveIO.setCnttype(sqlCnttype);
		goveIO.setCrtable(sqlCrtable);
		goveIO.setCnPremStat(sqlCnpstat);
		goveIO.setCovPremStat(sqlCrpstat);
		goveIO.setParind(sqlParind);
		goveIO.setStatcat(sqlStatcat);
		goveIO.setBillfreq(sqlBillfreq);
		goveIO.setFormat(goverec);
		goveIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, goveIO);
		if (isEQ(goveIO.getStatuz(),varcom.mrnf)) {
			wsaaNewGove = "Y";
		}
		else {
			wsaaNewGove = "N";
		}
	}

protected void updatePostedRecord6000()
	{
		para6010();
	}

protected void para6010()
	{
		if (isEQ(wsaaNewGove,"Y")) {
			goveIO.setFunction(varcom.writr);
			goveIO.setBfwdc(ZERO);
			goveIO.setCfwdc(ZERO);
			goveIO.setBfwdp(ZERO);
			goveIO.setCfwdp(ZERO);
			goveIO.setBfwds(ZERO);
			goveIO.setCfwds(ZERO);
			goveIO.setBfwdi(ZERO);
			goveIO.setCfwdi(ZERO);
			goveIO.setBfwda(ZERO);
			goveIO.setCfwda(ZERO);
			goveIO.setBfwdb(ZERO);
			goveIO.setCfwdb(ZERO);
			goveIO.setBfwdld(ZERO);
			goveIO.setCfwdld(ZERO);
			goveIO.setBfwdl(ZERO);
			goveIO.setCfwdl(ZERO);
			goveIO.setBfwdra(ZERO);
			goveIO.setCfwdra(ZERO);
			for (wsaaI.set(1); !(isGT(wsaaI,12)); wsaaI.add(1)){
				initializeGoveValues6100();
			}
		}
		else {
			goveIO.setFunction(varcom.updat);
		}
		setPrecision(goveIO.getStcmth(sqlAcctmn), 0);
		goveIO.setStcmth(sqlAcctmn, add(goveIO.getStcmth(sqlAcctmn),sqlStcmthg));
		setPrecision(goveIO.getCfwdc(), 2);
		goveIO.setCfwdc(add(goveIO.getCfwdc(),sqlStcmthg));
		setPrecision(goveIO.getStpmth(sqlAcctmn), 2);
		goveIO.setStpmth(sqlAcctmn, add(goveIO.getStpmth(sqlAcctmn),sqlStpmthg));
		setPrecision(goveIO.getCfwdp(), 2);
		goveIO.setCfwdp(add(goveIO.getCfwdp(),sqlStpmthg));
		setPrecision(goveIO.getStsmth(sqlAcctmn), 2);
		goveIO.setStsmth(sqlAcctmn, add(goveIO.getStsmth(sqlAcctmn),sqlStsmthg));
		setPrecision(goveIO.getCfwds(), 2);
		goveIO.setCfwds(add(goveIO.getCfwds(),sqlStsmthg));
		if (isEQ(wsaaAnnuity,"Y")) {
			setPrecision(goveIO.getStamth(sqlAcctmn), 2);
			goveIO.setStamth(sqlAcctmn, add(goveIO.getStamth(sqlAcctmn),sqlStimth));
			setPrecision(goveIO.getCfwda(), 2);
			goveIO.setCfwda(add(goveIO.getCfwda(),sqlStimth));
		}
		else {
			setPrecision(goveIO.getStimth(sqlAcctmn), 2);
			goveIO.setStimth(sqlAcctmn, add(goveIO.getStimth(sqlAcctmn),sqlStimth));
			setPrecision(goveIO.getCfwdi(), 2);
			goveIO.setCfwdi(add(goveIO.getCfwdi(),sqlStimth));
		}
		setPrecision(goveIO.getStlmth(sqlAcctmn), 0);
		goveIO.setStlmth(sqlAcctmn, add(goveIO.getStlmth(sqlAcctmn),sqlStlmth));
		setPrecision(goveIO.getCfwdl(), 2);
		goveIO.setCfwdl(add(goveIO.getCfwdl(),sqlStlmth));
		setPrecision(goveIO.getStbmthg(sqlAcctmn), 2);
		goveIO.setStbmthg(sqlAcctmn, add(goveIO.getStbmthg(sqlAcctmn),sqlStbmthg));
		setPrecision(goveIO.getCfwdb(), 2);
		goveIO.setCfwdb(add(goveIO.getCfwdb(),sqlStbmthg));
		setPrecision(goveIO.getStldmthg(sqlAcctmn), 2);
		goveIO.setStldmthg(sqlAcctmn, add(goveIO.getStldmthg(sqlAcctmn),sqlStldmthg));
		setPrecision(goveIO.getCfwdld(), 2);
		goveIO.setCfwdld(add(goveIO.getCfwdld(),sqlStldmthg));
		setPrecision(goveIO.getStraamt(sqlAcctmn), 2);
		goveIO.setStraamt(sqlAcctmn, add(goveIO.getStraamt(sqlAcctmn),sqlStraamt));
		setPrecision(goveIO.getCfwdra(), 2);
		goveIO.setCfwdra(add(goveIO.getCfwdra(),sqlStraamt));
		goveIO.setFormat(goverec);
		SmartFileCode.execute(appVars, goveIO);
		if (isNE(goveIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(goveIO.getParams());
			fatalError600();
		}
	}

protected void initializeGoveValues6100()
	{
		/*PARA*/
		goveIO.setStcmth(wsaaI, ZERO);
		goveIO.setStpmth(wsaaI, ZERO);
		goveIO.setStsmth(wsaaI, ZERO);
		goveIO.setStlmth(wsaaI, ZERO);
		goveIO.setStamth(wsaaI, ZERO);
		goveIO.setStbmthg(wsaaI, ZERO);
		goveIO.setStraamt(wsaaI, ZERO);
		goveIO.setStldmthg(wsaaI, ZERO);
		goveIO.setStimth(wsaaI, ZERO);
		/*EXIT*/
	}

protected void updateGove7000()
	{
		/*PARA*/
		wsaaBfwdc.set(ZERO);
		wsaaBfwdp.set(ZERO);
		wsaaBfwds.set(ZERO);
		wsaaBfwdl.set(ZERO);
		wsaaBfwda.set(ZERO);
		wsaaBfwdb.set(ZERO);
		wsaaBfwdld.set(ZERO);
		wsaaBfwdra.set(ZERO);
		wsaaBfwdi.set(ZERO);
		goveIO.setAcctyr(ZERO);
		goveIO.setFormat(goverec);
		goveIO.setFunction(varcom.begn);
		wsaaGetGoveBfFlag = "N";
		while ( !(isEQ(wsaaGetGoveBfFlag,"Y"))) {
			//performance improvement --  atiwari23 
			goveIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			goveIO.setFitKeysSearch("CHDRCOY", "CNTBRANCH", "REG", "SRCEBUS", "STATCAT", "STFUND", 
					"STSECT", "STSSECT", "CNPSTAT", "CRPSTAT", "ACCTCCY", "CNTCURR", "CNTTYPE", 
					"CRTABLE", "PARIND", "BILLFREQ");
		
			checkBalForwardValue7100();
		}
		
		/*EXIT*/
	}

protected void checkBalForwardValue7100()
	{
		try {
			para7110();
			readNextRecord7120();
		}
		catch (GOTOException e){
		}
	}

protected void para7110()
	{
		SmartFileCode.execute(appVars, goveIO);
		if (isNE(goveIO.getStatuz(),varcom.endp)
		&& isNE(goveIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(goveIO.getParams());
			fatalError600();
		}
		if (isEQ(goveIO.getStatuz(),varcom.endp)) {
			wsaaGetGoveBfFlag = "Y";
			goTo(GotoLabel.exit7190);
		}
		if (isEQ(sqlChdrcoy,goveIO.getChdrcoy())
		&& isEQ(sqlStfund,goveIO.getStatFund())
		&& isEQ(sqlStsect,goveIO.getStatSect())
		&& isEQ(sqlStssect,goveIO.getStatSubsect())
		&& isEQ(sqlRegister,goveIO.getRegister())
		&& isEQ(sqlSrcebus,goveIO.getSrcebus())
		&& isEQ(sqlCntbranch,goveIO.getCntbranch())
		&& isEQ(sqlCnpstat,goveIO.getCnPremStat())
		&& isEQ(sqlCrpstat,goveIO.getCovPremStat())
		&& isEQ(sqlCnttype,goveIO.getCnttype())
		&& isEQ(sqlCrtable,goveIO.getCrtable())
		&& isEQ(sqlAcctccy,goveIO.getAcctccy())
		&& isEQ(sqlCntcurr,goveIO.getCntcurr())
		&& isEQ(sqlParind,goveIO.getParind())
		&& isEQ(sqlStatcat,goveIO.getStatcat())
		&& isEQ(sqlBillfreq,goveIO.getBillfreq())) {
			if (isLT(sqlAcctyr,goveIO.getAcctyr())) {
				rollForwardUpdate7200();
			}
			else {
				if (isEQ(sqlAcctyr,goveIO.getAcctyr())) {
					updateBfAmount7300();
				}
				else {
					wsaaBfwdc.set(goveIO.getCfwdc());
					wsaaBfwdp.set(goveIO.getCfwdp());
					wsaaBfwds.set(goveIO.getCfwds());
					wsaaBfwdi.set(goveIO.getCfwdi());
					wsaaBfwdl.set(goveIO.getCfwdl());
					wsaaBfwda.set(goveIO.getCfwda());
					wsaaBfwdb.set(goveIO.getCfwdb());
					wsaaBfwdld.set(goveIO.getCfwdld());
					wsaaBfwdra.set(goveIO.getCfwdra());
				}
			}
		}
		else {
			wsaaGetGoveBfFlag = "Y";
			goTo(GotoLabel.exit7190);
		}
	}

protected void readNextRecord7120()
	{
		goveIO.setFunction(varcom.nextr);
	}

protected void rollForwardUpdate7200()
	{
		para7210();
	}

protected void para7210()
	{
		setPrecision(goveIO.getBfwdc(), 2);
		goveIO.setBfwdc(add(goveIO.getBfwdc(),sqlStcmthg));
		setPrecision(goveIO.getCfwdc(), 2);
		goveIO.setCfwdc(add(goveIO.getCfwdc(),sqlStcmthg));
		setPrecision(goveIO.getBfwdp(), 2);
		goveIO.setBfwdp(add(goveIO.getBfwdp(),sqlStpmthg));
		setPrecision(goveIO.getCfwdp(), 2);
		goveIO.setCfwdp(add(goveIO.getCfwdp(),sqlStpmthg));
		setPrecision(goveIO.getBfwds(), 2);
		goveIO.setBfwds(add(goveIO.getBfwds(),sqlStsmthg));
		setPrecision(goveIO.getCfwds(), 2);
		goveIO.setCfwds(add(goveIO.getCfwds(),sqlStsmthg));
		if (isEQ(wsaaAnnuity,"Y")) {
			setPrecision(goveIO.getBfwda(), 2);
			goveIO.setBfwda(add(goveIO.getBfwda(),sqlStimth));
			setPrecision(goveIO.getCfwda(), 2);
			goveIO.setCfwda(add(goveIO.getCfwda(),sqlStimth));
		}
		else {
			setPrecision(goveIO.getBfwdi(), 2);
			goveIO.setBfwdi(add(goveIO.getBfwdi(),sqlStimth));
			setPrecision(goveIO.getCfwdi(), 2);
			goveIO.setCfwdi(add(goveIO.getCfwdi(),sqlStimth));
		}
		setPrecision(goveIO.getBfwdb(), 2);
		goveIO.setBfwdb(add(goveIO.getBfwdb(),sqlStbmthg));
		setPrecision(goveIO.getCfwdb(), 2);
		goveIO.setCfwdb(add(goveIO.getCfwdb(),sqlStbmthg));
		setPrecision(goveIO.getBfwdld(), 2);
		goveIO.setBfwdld(add(goveIO.getBfwdld(),sqlStldmthg));
		setPrecision(goveIO.getCfwdld(), 2);
		goveIO.setCfwdld(add(goveIO.getCfwdld(),sqlStldmthg));
		setPrecision(goveIO.getBfwdl(), 2);
		goveIO.setBfwdl(add(goveIO.getBfwdl(),sqlStlmth));
		setPrecision(goveIO.getCfwdl(), 2);
		goveIO.setCfwdl(add(goveIO.getCfwdl(),sqlStlmth));
		setPrecision(goveIO.getBfwdra(), 2);
		goveIO.setBfwdra(add(goveIO.getBfwdra(),sqlStraamt));
		setPrecision(goveIO.getCfwdra(), 2);
		goveIO.setCfwdra(add(goveIO.getCfwdra(),sqlStraamt));
		goveIO.setFormat(goverec);
		goveIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, goveIO);
		if (isNE(goveIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(goveIO.getParams());
			fatalError600();
		}
	}

protected void updateBfAmount7300()
	{
		para7310();
	}

protected void para7310()
	{
		goveIO.setBfwdc(wsaaBfwdc);
		goveIO.setBfwdp(wsaaBfwdp);
		goveIO.setBfwds(wsaaBfwds);
		goveIO.setBfwdi(wsaaBfwdi);
		goveIO.setBfwda(wsaaBfwda);
		goveIO.setBfwdb(wsaaBfwdb);
		goveIO.setBfwdl(wsaaBfwdl);
		goveIO.setBfwdld(wsaaBfwdld);
		goveIO.setBfwdra(wsaaBfwdra);
		if (isEQ(wsaaNewGove,"Y")) {
			setPrecision(goveIO.getCfwdc(), 2);
			goveIO.setCfwdc(add(goveIO.getCfwdc(),wsaaBfwdc));
			setPrecision(goveIO.getCfwdp(), 2);
			goveIO.setCfwdp(add(goveIO.getCfwdp(),wsaaBfwdp));
			setPrecision(goveIO.getCfwds(), 2);
			goveIO.setCfwds(add(goveIO.getCfwds(),wsaaBfwds));
			setPrecision(goveIO.getCfwdb(), 2);
			goveIO.setCfwdb(add(goveIO.getCfwdb(),wsaaBfwdb));
			setPrecision(goveIO.getCfwdld(), 2);
			goveIO.setCfwdld(add(goveIO.getCfwdld(),wsaaBfwdld));
			setPrecision(goveIO.getCfwdra(), 2);
			goveIO.setCfwdra(add(goveIO.getCfwdra(),wsaaBfwdra));
			setPrecision(goveIO.getCfwda(), 2);
			goveIO.setCfwda(add(goveIO.getCfwda(),wsaaBfwda));
			setPrecision(goveIO.getCfwdl(), 2);
			goveIO.setCfwdl(add(goveIO.getCfwdl(),wsaaBfwdl));
			setPrecision(goveIO.getCfwdi(), 2);
			goveIO.setCfwdi(add(goveIO.getCfwdi(),wsaaBfwdi));
		}
		goveIO.setFormat(goverec);
		goveIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, goveIO);
		if (isNE(goveIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(goveIO.getParams());
			fatalError600();
		}
	}

protected void controlTotals8000()
	{
		para8010();
	}

protected void para8010()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(sqlStcmthg);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(sqlStpmthg);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(sqlStsmthg);
		callContot001();
		if (isEQ(wsaaAnnuity,"Y")) {
			contotrec.totno.set(ct07);
			contotrec.totval.set(sqlStimth);
			callContot001();
		}
		else {
			contotrec.totno.set(ct06);
			contotrec.totval.set(sqlStimth);
			callContot001();
		}
		contotrec.totno.set(ct08);
		contotrec.totval.set(sqlStlmth);
		callContot001();
		contotrec.totno.set(ct09);
		contotrec.totval.set(sqlStbmthg);
		callContot001();
		contotrec.totno.set(ct10);
		contotrec.totval.set(sqlStldmthg);
		callContot001();
		contotrec.totno.set(ct11);
		contotrec.totval.set(sqlStraamt);
		callContot001();
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
