package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:21
 * Description:
 * Copybook name: GVAHKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Gvahkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData gvahFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData gvahKey = new FixedLengthStringData(64).isAPartOf(gvahFileKey, 0, REDEFINE);
  	public FixedLengthStringData gvahChdrcoy = new FixedLengthStringData(1).isAPartOf(gvahKey, 0);
  	public FixedLengthStringData gvahCntbranch = new FixedLengthStringData(2).isAPartOf(gvahKey, 1);
  	public FixedLengthStringData gvahRegister = new FixedLengthStringData(3).isAPartOf(gvahKey, 3);
  	public FixedLengthStringData gvahSrcebus = new FixedLengthStringData(2).isAPartOf(gvahKey, 6);
  	public FixedLengthStringData gvahStatcode = new FixedLengthStringData(2).isAPartOf(gvahKey, 8);
  	public FixedLengthStringData gvahCnPremStat = new FixedLengthStringData(2).isAPartOf(gvahKey, 10);
  	public FixedLengthStringData gvahAcctccy = new FixedLengthStringData(3).isAPartOf(gvahKey, 12);
  	public FixedLengthStringData gvahCnttype = new FixedLengthStringData(3).isAPartOf(gvahKey, 15);
  	public PackedDecimalData gvahAcctyr = new PackedDecimalData(4, 0).isAPartOf(gvahKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(gvahKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(gvahFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		gvahFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}