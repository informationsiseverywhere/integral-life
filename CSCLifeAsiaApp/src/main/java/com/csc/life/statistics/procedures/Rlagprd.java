/*
 * File: Rlagprd.java
 * Date: 30 August 2009 2:12:46
 * Author: Quipoz Limited
 * 
 * Class transformed from RLAGPRD.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.agents.dataaccess.AglflnbTableDAM;
import com.csc.life.agents.dataaccess.MacfenqTableDAM;
import com.csc.life.agents.dataaccess.MacfprdTableDAM;
import com.csc.life.agents.dataaccess.MapreffTableDAM;
import com.csc.life.agents.recordstructures.Rlagprdrec;
import com.csc.life.agents.tablestructures.Tm603rec;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.HpadTableDAM;
import com.csc.life.statistics.dataaccess.AgprTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   This subroutine calculates and update the agency production
*     figures in Agency Production file MAPR.
*
*   It is called from LIFACMV if T3695 for the premium or
*   commission production flag is on for the sub-account type.
*
*   These files are read for:
*     HPAD - first issue date
*     AGLF - agent type (used if no MACF)
*     MACF - agent type
*
*   These tables are read for:
*     TM603 - production group to determine the production
*             production categories and identify direct or
*             group production updates
*
*****************************************************************
* </pre>
*/
public class Rlagprd extends SMARTCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private final String wsaaSubr = "RLAGPRD";
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaIndex = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaIndex1 = new ZonedDecimalData(1, 0).setUnsigned();
	private FixedLengthStringData wsaaTeamFlg = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOvrFlg = new FixedLengthStringData(1);
	private String wsaaExist = "";

	private FixedLengthStringData wsaaTm603Key = new FixedLengthStringData(5);
	private FixedLengthStringData wsaaTm603Agtype = new FixedLengthStringData(2).isAPartOf(wsaaTm603Key, 0);
	private FixedLengthStringData wsaaTm603Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaTm603Key, 2);
	private ZonedDecimalData wsaaInteger = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaEffdate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaCurrfrom = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaMlperpc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMlperpp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMlgrppc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMlgrppp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMldirpc = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaMldirpp = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaTotSumins = new PackedDecimalData(17, 2);
	private String wsaaValidCoverage = "";
	private PackedDecimalData wsaaPolicyCount = new PackedDecimalData(7, 0);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(4);

	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaRider, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaRider1 = new FixedLengthStringData(1).isAPartOf(filler, 0);

	private FixedLengthStringData wsaaReportags = new FixedLengthStringData(32);
	private FixedLengthStringData[] wsaaReportag = FLSArrayPartOfStructure(4, 8, wsaaReportags, 0);
	private static final String e031 = "E031";
		/* TABLES */
	private static final String tm603 = "TM603";
	private static final String macfprdrec = "MACFPRDREC";
	private static final String macfenqrec = "MACFENQREC";
	private static final String mapreffrec = "MAPREFFREC";
	private static final String covrlnbrec = "COVRLNBREC";
	private static final String hpadrec = "HPADREC";
	private static final String agprrec = "AGPRREC";
	private AglflnbTableDAM aglflnbIO = new AglflnbTableDAM();
	private AgprTableDAM agprIO = new AgprTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private HpadTableDAM hpadIO = new HpadTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private MacfenqTableDAM macfenqIO = new MacfenqTableDAM();
	private MacfprdTableDAM macfprdIO = new MacfprdTableDAM();
	private MapreffTableDAM mapreffIO = new MapreffTableDAM();
	private Varcom varcom = new Varcom();
	private Syserrrec syserrrec = new Syserrrec();
	private Tm603rec tm603rec = new Tm603rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Rlagprdrec rlagprdrec = new Rlagprdrec();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr800, 
		exit800
	}

	public Rlagprd() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		rlagprdrec.rlagprdRec = convertAndSetParam(rlagprdrec.rlagprdRec, parmArray, 0);
		try {
			startSubr010();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startSubr010()
	{
		para010();
		exit090();
	}

protected void para010()
	{
		/* Initialise all working storage fields*/
		initialize(wsaaTeamFlg);
		initialize(wsaaOvrFlg);
		initialize(wsaaIndex);
		initialize(wsaaIndex1);
		initialize(wsaaEffdate);
		initialize(wsaaCurrfrom);
		initialize(wsaaAgntnum);
		wsaaMlgrppc.set(ZERO);
		wsaaMlgrppp.set(ZERO);
		wsaaMldirpc.set(ZERO);
		wsaaMldirpp.set(ZERO);
		wsaaMlperpc.set(ZERO);
		wsaaMlperpp.set(ZERO);
		wsaaPolicyCount.set(ZERO);
		wsaaTotSumins.set(ZERO);
		syserrrec.subrname.set(wsaaSubr);
		datcon3rec.intDate1.set(rlagprdrec.occdate);
		datcon3rec.intDate2.set(rlagprdrec.effdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			rlagprdrec.statuz.set(datcon3rec.statuz);
			fatalError600();
		}
		wsaaInteger.set(datcon3rec.freqFactor);
		wsaaInteger.add(1);
		hpadIO.setChdrcoy(rlagprdrec.chdrcoy);
		hpadIO.setChdrnum(rlagprdrec.chdrnum);
		hpadIO.setFormat(hpadrec);
		hpadIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hpadIO);
		if (isNE(hpadIO.getStatuz(), varcom.oK)
		&& isNE(hpadIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(hpadIO.getStatuz());
			fatalError600();
		}
		wsaaCurrfrom.set(hpadIO.getHissdte());
		/* If the following transaction codes not matched, then select*/
		/* the CURRFROM from AGCMOVR file.*/
		/* Contract Issue = 'T642'*/
		/*Renewals       = 'B522'*/
		/*PTD Advance    = 'T536'*/
		/*    IF RLAG-BATCTRCDE           NOT = 'T642'*/
		/*   OR RLAG-BATCTRCDE           NOT = 'B522'*/
		/*   OR RLAG-BATCTRCDE           NOT = 'T536'*/
		/*       MOVE RLAG-CHDRCOY           TO AGCMOVR-CHDRCOY*/
		/*       MOVE RLAG-CHDRNUM           TO AGCMOVR-CHDRNUM*/
		/*       MOVE '01'                   TO AGCMOVR-LIFE*/
		/*       MOVE '01'                   TO AGCMOVR-COVERAGE*/
		/*       MOVE '00'                   TO AGCMOVR-RIDER*/
		/*       MOVE 'B'                    TO AGCMOVR-OVRDCAT*/
		/*       MOVE READR                  TO AGCMOVR-FUNCTION*/
		/*       CALL 'AGCMOVRIO'         USING AGCMOVR-PARAMS*/
		/*       IF AGCMOVR-STATUZ        NOT = O-K AND MRNF*/
		/*          MOVE AGCMOVR-STATUZ      TO SYSR-STATUZ*/
		/*          MOVE AGCMOVR-PARAMS      TO SYSR-PARAMS*/
		/*          PERFORM 600-FATAL-ERROR*/
		/*       END-IF*/
		/*       IF AGCMOVR-STATUZ            = O-K*/
		/*          MOVE AGCMOVR-CURRFROM    TO WSAA-CURRFROM*/
		/*       END-IF*/
		/*    END-IF.*/
		aglflnbIO.setAgntnum(rlagprdrec.agntnum);
		aglflnbIO.setAgntcoy(rlagprdrec.agntcoy);
		aglflnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglflnbIO);
		if (isNE(aglflnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(aglflnbIO.getStatuz());
			syserrrec.params.set(aglflnbIO.getParams());
			fatalError600();
		}
		macfprdIO.setAgntcoy(rlagprdrec.agntcoy);
		macfprdIO.setAgntnum(rlagprdrec.agntnum);
		/*    MOVE HPAD-HISSDTE            TO MACFPRD-EFFDATE.*/
		/*    MOVE AGCMOVR-CURRFROM        TO MACFPRD-EFFDATE.*/
		macfprdIO.setEffdate(wsaaCurrfrom);
		macfprdIO.setAgmvty(SPACES);
		macfprdIO.setStatuz(varcom.oK);
		macfprdIO.setFormat(macfprdrec);
		macfprdIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		macfprdIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfprdIO.setFitKeysSearch("AGNTCOY","AGNTNUM");

		SmartFileCode.execute(appVars, macfprdIO);
		if (isNE(macfprdIO.getStatuz(), varcom.oK)
		&& isNE(macfprdIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfprdIO.getStatuz());
			syserrrec.params.set(macfprdIO.getParams());
			fatalError600();
		}
		if (isNE(rlagprdrec.agntnum, macfprdIO.getAgntnum())
		|| isNE(rlagprdrec.agntcoy, macfprdIO.getAgntcoy())
		|| isEQ(macfprdIO.getStatuz(), varcom.endp)) {
			/*      MOVE MACFPRD-STATUZ        TO RLAG-STATUZ*/
			/*      GO TO 090-EXIT*/
			macfprdIO.setParams(SPACES);
			macfprdIO.setAgntcoy(rlagprdrec.agntcoy);
			macfprdIO.setAgntnum(rlagprdrec.agntnum);
			macfprdIO.setEffdate(ZERO);
			macfprdIO.setFunction(varcom.endr);
			SmartFileCode.execute(appVars, macfprdIO);
			if (isNE(macfprdIO.getStatuz(), varcom.oK)
			&& isNE(macfprdIO.getStatuz(), varcom.endp)) {
				syserrrec.statuz.set(macfprdIO.getStatuz());
				syserrrec.params.set(macfprdIO.getParams());
				fatalError600();
			}
			if (isNE(rlagprdrec.agntnum, macfprdIO.getAgntnum())
			|| isNE(rlagprdrec.agntcoy, macfprdIO.getAgntcoy())
			|| isEQ(macfprdIO.getStatuz(), varcom.endp)) {
				syserrrec.statuz.set(macfprdIO.getStatuz());
				syserrrec.params.set(macfprdIO.getParams());
				fatalError600();
			}
		}
		readTm603100();
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			rlagprdrec.statuz.set(varcom.endp);
			return ;
		}
		if (isNE(tm603rec.active01, "Y")
		&& isNE(tm603rec.active02, "Y")
		&& isNE(tm603rec.active03, "Y")) {
			return ;
		}
		personalProduction400();
		wsaaIndex1.set(1);
		wsaaExist = "N";
		wsaaReportags.set(SPACES);
		wsaaReportag[1].set(macfprdIO.getZrptga());
		wsaaReportag[2].set(macfprdIO.getZrptgb());
		wsaaReportag[3].set(macfprdIO.getZrptgc());
		wsaaReportag[4].set(macfprdIO.getZrptgd());
		/*    PERFORM 300-UPDATE-PRODUCTION                                */
		/*         UNTIL WSAA-INDEX1 > 5                                   */
		/*          OR  MACFPRD-REPORTAG(WSAA-INDEX1) = SPACES.            */
		/*-LA1174 ---> There are only 4 report tag, since we haven't       */
		/* encountered a situation of having five levels, if happened      */
		/* to be there, might have  ended in MCH0603 subscript error.      */
		while ( !(isGT(wsaaIndex1, 4)
		|| isEQ(wsaaReportag[wsaaIndex1.toInt()], SPACES))) {
			updateProduction300();
		}
		
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readTm603100()
	{
		start100();
	}

protected void start100()
	{
		tm603rec.tm603Rec.set(SPACES);
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(rlagprdrec.agntcoy);
		itdmIO.setItemtabl(tm603);
		if (isEQ(macfprdIO.getMlagttyp(), SPACES)) {
			wsaaTm603Agtype.set(aglflnbIO.getAgtype());
		}
		else {
			wsaaTm603Agtype.set(macfprdIO.getMlagttyp());
		}
		wsaaTm603Cnttype.set(rlagprdrec.cnttype);
		itdmIO.setItemitem(wsaaTm603Key);
		itdmIO.setItmfrm(rlagprdrec.occdate);
		itdmIO.setFunction(varcom.begn);
		
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	

		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)
		&& isEQ(itdmIO.getItemcoy(), rlagprdrec.agntcoy)
		&& isEQ(itdmIO.getItemtabl(), tm603)
		&& isEQ(itdmIO.getItemitem(), wsaaTm603Key)) {
			tm603rec.tm603Rec.set(itdmIO.getGenarea());
			return ;
		}
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(rlagprdrec.agntcoy);
		itdmIO.setItemtabl(tm603);
		if (isEQ(macfprdIO.getMlagttyp(), SPACES)) {
			wsaaTm603Agtype.set(aglflnbIO.getAgtype());
		}
		else {
			wsaaTm603Agtype.set(macfprdIO.getMlagttyp());
		}
		wsaaTm603Cnttype.set("***");
		itdmIO.setItemitem(wsaaTm603Key);
		itdmIO.setItmfrm(rlagprdrec.occdate);
		itdmIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY","ITEMTABL","ITEMITEM");
	
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(itdmIO.getStatuz());
			syserrrec.params.set(itdmIO.getParams());
			fatalError600();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.oK)
		&& isEQ(itdmIO.getItemcoy(), rlagprdrec.agntcoy)
		&& isEQ(itdmIO.getItemtabl(), tm603)
		&& isEQ(itdmIO.getItemitem(), wsaaTm603Key)) {
			tm603rec.tm603Rec.set(itdmIO.getGenarea());
		}
		else {
			/*     MOVE ITEM-STATUZ        TO SYSR-STATUZ                   */
			/*     MOVE ITEM-PARAMS        TO SYSR-PARAMS                   */
			itdmIO.setItemitem(wsaaTm603Key);
			itdmIO.setItmfrm(rlagprdrec.occdate);
			syserrrec.params.set(itdmIO.getParams());
			syserrrec.statuz.set(e031);
			fatalError600();
		}
	}

protected void updateProduction300()
	{
		start300();
	}

protected void start300()
	{
		macfenqIO.setAgntcoy(rlagprdrec.agntcoy);
		/*    MOVE MACFPRD-REPORTAG(WSAA-INDEX1) TO MACFENQ-AGNTNUM.       */
		macfenqIO.setAgntnum(wsaaReportag[wsaaIndex1.toInt()]);
		/*    MOVE HPAD-HISSDTE            TO MACFENQ-EFFDATE.*/
		/*    MOVE AGCMOVR-CURRFROM        TO MACFENQ-EFFDATE.*/
		macfenqIO.setEffdate(wsaaCurrfrom);
		macfenqIO.setAgmvty(SPACES);
		macfenqIO.setStatuz(varcom.oK);
		macfenqIO.setFormat(macfenqrec);
		macfenqIO.setFunction(varcom.begn);
		//performance improvement --  atiwari23 
		macfenqIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		macfenqIO.setFitKeysSearch("AGNTCOY","AGNTNUM");
		SmartFileCode.execute(appVars, macfenqIO);
		if (isNE(macfenqIO.getStatuz(), varcom.oK)
		&& isNE(macfenqIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(macfenqIO.getStatuz());
			syserrrec.params.set(macfenqIO.getParams());
			fatalError600();
		}
		/*    IF MACFPRD-REPORTAG(WSAA-INDEX1) NOT = MACFENQ-AGNTNUM       */
		if (isNE(wsaaReportag[wsaaIndex1.toInt()], macfenqIO.getAgntnum())
		|| isNE(rlagprdrec.agntcoy, macfenqIO.getAgntcoy())
		|| isEQ(macfenqIO.getStatuz(), varcom.endp)) {
			macfenqIO.setParams(SPACES);
			macfenqIO.setAgntcoy(rlagprdrec.agntcoy);
			/*      MOVE MACFPRD-REPORTAG(WSAA-INDEX1) TO MACFENQ-AGNTNUM      */
			macfenqIO.setAgntnum(wsaaReportag[wsaaIndex1.toInt()]);
			macfenqIO.setEffdate(ZERO);
			macfenqIO.setFunction(varcom.endr);
			SmartFileCode.execute(appVars, macfenqIO);
			if (isNE(macfenqIO.getStatuz(), varcom.oK)
			&& isNE(macfenqIO.getStatuz(), varcom.endp)) {
				syserrrec.statuz.set(macfenqIO.getStatuz());
				syserrrec.params.set(macfenqIO.getParams());
				fatalError600();
			}
			/*      IF MACFPRD-REPORTAG(WSAA-INDEX1) NOT = MACFENQ-AGNTNUM     */
			if (isNE(wsaaReportag[wsaaIndex1.toInt()], macfenqIO.getAgntnum())
			|| isNE(rlagprdrec.agntcoy, macfenqIO.getAgntcoy())
			|| isEQ(macfenqIO.getStatuz(), varcom.endp)) {
				syserrrec.statuz.set(macfenqIO.getStatuz());
				syserrrec.params.set(macfenqIO.getParams());
				fatalError600();
			}
		}
		wsaaTeamFlg.set("N");
		wsaaOvrFlg.set("N");
		wsaaMlgrppc.set(ZERO);
		wsaaMlgrppp.set(ZERO);
		wsaaMldirpc.set(ZERO);
		wsaaMldirpp.set(ZERO);
		wsaaMlperpc.set(ZERO);
		wsaaMlperpp.set(ZERO);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 10)
		|| isEQ(wsaaTeamFlg, "Y")); wsaaIndex.add(1)){
			if (isEQ(macfenqIO.getMlagttyp(), tm603rec.mlagttyp[wsaaIndex.toInt()])) {
				wsaaTeamFlg.set("Y");
			}
		}
		if (isEQ(wsaaTeamFlg, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMlgrppc.set(rlagprdrec.origamt);
			}
			else {
				wsaaMlgrppp.set(rlagprdrec.origamt);
			}
		}
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 10)
		|| isEQ(wsaaOvrFlg, "Y")
		|| isEQ(wsaaExist, "Y")); wsaaIndex.add(1)){
			if (isEQ(macfenqIO.getMlagttyp(), tm603rec.agtype[wsaaIndex.toInt()])) {
				wsaaOvrFlg.set("Y");
				wsaaExist = "Y";
			}
		}
		if (isEQ(wsaaOvrFlg, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMldirpc.set(rlagprdrec.origamt);
			}
			else {
				wsaaMldirpp.set(rlagprdrec.origamt);
			}
		}
		/*    MOVE MACFPRD-REPORTAG(WSAA-INDEX1)  TO WSAA-AGNTNUM.         */
		wsaaAgntnum.set(wsaaReportag[wsaaIndex1.toInt()]);
		wsaaPolicyCount.set(ZERO);
		wsaaTotSumins.set(ZERO);
		wsaaEffdate.set(macfenqIO.getEffdate());
		if (isEQ(subString(rlagprdrec.batctrcde, 1, 1), "B")) {
			deferredMaprUpdate900();
		}
		else {
			writeMapr500();
		}
		wsaaIndex1.add(1);
	}

protected void personalProduction400()
	{
		start400();
	}

protected void start400()
	{
		if (isEQ(tm603rec.active03, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMlperpc.add(rlagprdrec.origamt);
			}
			else {
				wsaaMlperpp.add(rlagprdrec.origamt);
			}
		}
		if (isEQ(tm603rec.active01, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMldirpc.add(rlagprdrec.origamt);
			}
			else {
				wsaaMldirpp.add(rlagprdrec.origamt);
			}
		}
		if (isEQ(tm603rec.active02, "Y")) {
			if (isEQ(rlagprdrec.prdflg, "C")) {
				wsaaMlgrppc.add(rlagprdrec.origamt);
			}
			else {
				wsaaMlgrppp.add(rlagprdrec.origamt);
			}
		}
		wsaaAgntnum.set(rlagprdrec.agntnum);
		wsaaEffdate.set(macfprdIO.getEffdate());
		if ((isEQ(rlagprdrec.batctrcde, "T642")
		|| isEQ(rlagprdrec.batctrcde, "T607"))
		&& isEQ(rlagprdrec.prdflg, "P")) {
			getSumAssured700();
		}
		if (isEQ(rlagprdrec.batctrcde, "T642")
		&& isEQ(rlagprdrec.prdflg, "P")) {
			wsaaPolicyCount.add(1);
		}
		if (isEQ(rlagprdrec.batctrcde, "T607")
		&& isEQ(rlagprdrec.prdflg, "P")) {
			wsaaPolicyCount.add(-1);
			compute(wsaaTotSumins, 2).set(mult(wsaaTotSumins, -1));
		}
		if (isEQ(subString(rlagprdrec.batctrcde, 1, 1), "B")) {
			deferredMaprUpdate900();
		}
		else {
			writeMapr500();
		}
	}

protected void writeMapr500()
	{
		start500();
	}

protected void start500()
	{
		mapreffIO.setDataArea(SPACES);
		initialize(mapreffIO.getMlperpps());
		initialize(mapreffIO.getMlperpcs());
		initialize(mapreffIO.getMlgrppps());
		initialize(mapreffIO.getMlgrppcs());
		initialize(mapreffIO.getMldirpps());
		initialize(mapreffIO.getMldirpcs());
		initialize(mapreffIO.getCntcount());
		initialize(mapreffIO.getSumins());
		mapreffIO.setAgntcoy(rlagprdrec.agntcoy);
		mapreffIO.setAgntnum(wsaaAgntnum);
		mapreffIO.setAcctyr(rlagprdrec.actyear);
		mapreffIO.setMnth(rlagprdrec.actmnth);
		mapreffIO.setCnttype(rlagprdrec.cnttype);
		mapreffIO.setEffdate(wsaaEffdate);
		mapreffIO.setFormat(mapreffrec);
		mapreffIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, mapreffIO);
		if (isNE(mapreffIO.getStatuz(), varcom.oK)
		&& isNE(mapreffIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(mapreffIO.getStatuz());
			syserrrec.params.set(mapreffIO.getParams());
			fatalError600();
		}
		if (isGT(wsaaInteger, 10)) {
			setPrecision(mapreffIO.getMlperpc(10), 2);
			mapreffIO.setMlperpc(10, add(mapreffIO.getMlperpc(10), wsaaMlperpc));
			setPrecision(mapreffIO.getMlperpp(10), 2);
			mapreffIO.setMlperpp(10, add(mapreffIO.getMlperpp(10), wsaaMlperpp));
			setPrecision(mapreffIO.getMlgrppc(10), 2);
			mapreffIO.setMlgrppc(10, add(mapreffIO.getMlgrppc(10), wsaaMlgrppc));
			setPrecision(mapreffIO.getMlgrppp(10), 2);
			mapreffIO.setMlgrppp(10, add(mapreffIO.getMlgrppp(10), wsaaMlgrppp));
			setPrecision(mapreffIO.getMldirpc(10), 2);
			mapreffIO.setMldirpc(10, add(mapreffIO.getMldirpc(10), wsaaMldirpc));
			setPrecision(mapreffIO.getMldirpp(10), 2);
			mapreffIO.setMldirpp(10, add(mapreffIO.getMldirpp(10), wsaaMldirpp));
		}
		else {
			setPrecision(mapreffIO.getMlperpc(wsaaInteger), 2);
			mapreffIO.setMlperpc(wsaaInteger, add(mapreffIO.getMlperpc(wsaaInteger), wsaaMlperpc));
			setPrecision(mapreffIO.getMlperpp(wsaaInteger), 2);
			mapreffIO.setMlperpp(wsaaInteger, add(mapreffIO.getMlperpp(wsaaInteger), wsaaMlperpp));
			setPrecision(mapreffIO.getMlgrppc(wsaaInteger), 2);
			mapreffIO.setMlgrppc(wsaaInteger, add(mapreffIO.getMlgrppc(wsaaInteger), wsaaMlgrppc));
			setPrecision(mapreffIO.getMlgrppp(wsaaInteger), 2);
			mapreffIO.setMlgrppp(wsaaInteger, add(mapreffIO.getMlgrppp(wsaaInteger), wsaaMlgrppp));
			setPrecision(mapreffIO.getMldirpc(wsaaInteger), 2);
			mapreffIO.setMldirpc(wsaaInteger, add(mapreffIO.getMldirpc(wsaaInteger), wsaaMldirpc));
			setPrecision(mapreffIO.getMldirpp(wsaaInteger), 2);
			mapreffIO.setMldirpp(wsaaInteger, add(mapreffIO.getMldirpp(wsaaInteger), wsaaMldirpp));
		}
		setPrecision(mapreffIO.getCntcount(), 0);
		mapreffIO.setCntcount(add(mapreffIO.getCntcount(), wsaaPolicyCount));
		setPrecision(mapreffIO.getSumins(), 2);
		mapreffIO.setSumins(add(mapreffIO.getSumins(), wsaaTotSumins));
		if (isEQ(mapreffIO.getStatuz(), varcom.oK)) {
			mapreffIO.setFunction(varcom.rewrt);
		}
		else {
			mapreffIO.setFunction(varcom.writr);
		}
		SmartFileCode.execute(appVars, mapreffIO);
		if (isNE(mapreffIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(mapreffIO.getStatuz());
			syserrrec.params.set(mapreffIO.getParams());
			fatalError600();
		}
	}

protected void fatalError600()
	{
		/*START*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		callProgram(Syserr.class, syserrrec.syserrRec);
		rlagprdrec.statuz.set("BOMB");
		/*EXIT*/
	}

protected void getSumAssured700()
	{
		/*START*/
		wsaaTotSumins.set(ZERO);
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(rlagprdrec.chdrcoy);
		covrlnbIO.setChdrnum(rlagprdrec.chdrnum);
		covrlnbIO.setLife(SPACES);
		covrlnbIO.setCoverage(SPACES);
		covrlnbIO.setRider(SPACES);
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setFormat(covrlnbrec);
		covrlnbIO.setFunction(varcom.begn);
		while ( !(isEQ(covrlnbIO.getStatuz(), varcom.endp))) {

			//performance improvement --  atiwari23 
			covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			covrlnbIO.setFitKeysSearch("CHDRCOY","CHDRNUM");
			readCoverage800();
		}
		
		/*EXIT*/
	}

protected void readCoverage800()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start800();
				case nextr800: 
					nextr800();
				case exit800: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start800()
	{
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)
		&& isNE(covrlnbIO.getStatuz(), varcom.endp)) {
			syserrrec.statuz.set(covrlnbIO.getStatuz());
			syserrrec.params.set(covrlnbIO.getParams());
			fatalError600();
		}
		if (isNE(covrlnbIO.getChdrnum(), rlagprdrec.chdrnum)
		|| isNE(covrlnbIO.getChdrcoy(), rlagprdrec.chdrcoy)) {
			covrlnbIO.setStatuz(varcom.endp);
		}
		if (isEQ(covrlnbIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit800);
		}
		if (isNE(covrlnbIO.getValidflag(), "1")) {
			goTo(GotoLabel.nextr800);
		}
		wsaaValidCoverage = "N";
		wsaaRider.set(covrlnbIO.getCrtable());
		/*    PERFORM VARYING WSAA-INDEX FROM 1 BY 1 UNTIL*/
		/*                               WSAA-INDEX > 12 OR*/
		/*                               VALID-COVERAGE*/
		/*       IF T5679-COV-RISK-STAT(WSAA-INDEX) = COVRLNB-STATCODE*/
		/*         IF COVRLNB-RIDER       = '00'*/
		/*              MOVE 'Y'         TO WSAA-VALID-COVERAGE*/
		/*         ELSE*/
		/*           IF WSAA-RIDER-1      = 'D'*/
		/*            OR WSAA-RIDER-1     = 'P'*/
		/*             OR WSAA-RIDER-1    = 'R'*/
		/*              MOVE 'Y'         TO WSAA-VALID-COVERAGE*/
		/*           END-IF*/
		/*         END-IF*/
		/*       END-IF*/
		/*    END-PERFORM.*/
		if (isEQ(covrlnbIO.getRider(), "00")) {
			wsaaValidCoverage = "Y";
		}
		else {
			if (isEQ(wsaaRider1, "D")
			|| isEQ(wsaaRider1, "P")
			|| isEQ(wsaaRider1, "R")) {
				wsaaValidCoverage = "Y";
			}
		}
		if (isEQ(wsaaValidCoverage, "Y")) {
			compute(wsaaTotSumins, 2).set(add(wsaaTotSumins, covrlnbIO.getSumins()));
		}
	}

protected void nextr800()
	{
		covrlnbIO.setFunction(varcom.nextr);
	}

protected void deferredMaprUpdate900()
	{
		start910();
	}

protected void start910()
	{
		agprIO.setDataArea(SPACES);
		for (wsaaIndex.set(1); !(isGT(wsaaIndex, 10)); wsaaIndex.add(1)){
			agprIO.setMlperpp(wsaaIndex, ZERO);
			agprIO.setMlperpc(wsaaIndex, ZERO);
			agprIO.setMlgrppp(wsaaIndex, ZERO);
			agprIO.setMlgrppc(wsaaIndex, ZERO);
			agprIO.setMldirpp(wsaaIndex, ZERO);
			agprIO.setMldirpc(wsaaIndex, ZERO);
		}
		agprIO.setCntcount(ZERO);
		agprIO.setSumins(ZERO);
		agprIO.setAgntcoy(rlagprdrec.agntcoy);
		agprIO.setAgntnum(wsaaAgntnum);
		agprIO.setAcctyr(rlagprdrec.actyear);
		agprIO.setMnth(rlagprdrec.actmnth);
		agprIO.setCnttype(rlagprdrec.cnttype);
		agprIO.setChdrnum(rlagprdrec.chdrnum);
		agprIO.setEffdate(wsaaEffdate);
		agprIO.setFormat(agprrec);
		if (isGT(wsaaInteger, 10)) {
			setPrecision(agprIO.getMlperpc(10), 2);
			agprIO.setMlperpc(10, add(agprIO.getMlperpc(10), wsaaMlperpc));
			setPrecision(agprIO.getMlperpp(10), 2);
			agprIO.setMlperpp(10, add(agprIO.getMlperpp(10), wsaaMlperpp));
			setPrecision(agprIO.getMlgrppc(10), 2);
			agprIO.setMlgrppc(10, add(agprIO.getMlgrppc(10), wsaaMlgrppc));
			setPrecision(agprIO.getMlgrppp(10), 2);
			agprIO.setMlgrppp(10, add(agprIO.getMlgrppp(10), wsaaMlgrppp));
			setPrecision(agprIO.getMldirpc(10), 2);
			agprIO.setMldirpc(10, add(agprIO.getMldirpc(10), wsaaMldirpc));
			setPrecision(agprIO.getMldirpp(10), 2);
			agprIO.setMldirpp(10, add(agprIO.getMldirpp(10), wsaaMldirpp));
		}
		else {
			setPrecision(agprIO.getMlperpc(wsaaInteger), 2);
			agprIO.setMlperpc(wsaaInteger, add(agprIO.getMlperpc(wsaaInteger), wsaaMlperpc));
			setPrecision(agprIO.getMlperpp(wsaaInteger), 2);
			agprIO.setMlperpp(wsaaInteger, add(agprIO.getMlperpp(wsaaInteger), wsaaMlperpp));
			setPrecision(agprIO.getMlgrppc(wsaaInteger), 2);
			agprIO.setMlgrppc(wsaaInteger, add(agprIO.getMlgrppc(wsaaInteger), wsaaMlgrppc));
			setPrecision(agprIO.getMlgrppp(wsaaInteger), 2);
			agprIO.setMlgrppp(wsaaInteger, add(agprIO.getMlgrppp(wsaaInteger), wsaaMlgrppp));
			setPrecision(agprIO.getMldirpc(wsaaInteger), 2);
			agprIO.setMldirpc(wsaaInteger, add(agprIO.getMldirpc(wsaaInteger), wsaaMldirpc));
			setPrecision(agprIO.getMldirpp(wsaaInteger), 2);
			agprIO.setMldirpp(wsaaInteger, add(agprIO.getMldirpp(wsaaInteger), wsaaMldirpp));
		}
		agprIO.setCntcount(ZERO);
		agprIO.setSumins(wsaaTotSumins);
		agprIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agprIO);
		if (isNE(agprIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(agprIO.getStatuz());
			syserrrec.params.set(agprIO.getParams());
			fatalError600();
		}
	}
}
