/*
 * File: P6709.java
 * Date: 30 August 2009 0:54:29
 * Author: Quipoz Limited
 * 
 * Class transformed from P6709.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Acmdesc;
import com.csc.fsu.general.recordstructures.Acmdescrec;
import com.csc.life.statistics.dataaccess.GovrTableDAM;
import com.csc.life.statistics.screens.S6709ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*            ENQUIRE ON GOVERNMENT STATISTICAL BALANCES
*
* This program accepts the enquiry key and displays the corresponding
* record from GOVR file. If the required record is not found, the next
* record retrieved when the file is read will be displayed.
*
* The GOVR records will be arranged in the following ascending
* sequence:
*
*          Category
*          Account year
*          Statutory fund
*          Section
*          Sub-section
*          Register
*          Servicing branch
*          Age band
*          Sum insured band
*          Premium band
*          Term band
*          Commencement year
*          Premium statistical code
*          Currency
*
* The  statistical  accumulation  key  will be used to search the
* GOVR file.  The GOVR record  returned must match on all entered
* screen fields.  Screen fields left blank can contain any value.
*
* FUNCTION KEYS:
*               <EXIT>  Return to sub-menu.
*
*               <KILL>  Allow user to select another accumulation
*                       enquiry key. It clears the screen S6709
*                       and prompts user to enter a new enquiry key.
*
*               <ENTER> Display the accounts for the accumulation key
*                       if it has been changed, else display the
*                       accumulation account for the next month.
*                       Where enquiry months are exhausted
*                       (i.e. S6709-ACMN = 12), the first account
*                       month of the next GOVR record will be
*                       displayed.
*                       If no enquiry key is entered, the first record
*                       in the file is displayed.
*
* FILES USED:
*               GOVRSKM
*               DESCSKM
*
*
*****************************************************************
* </pre>
*/
public class P6709 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6709");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
		/* WSAA-SCREEN-KEY */
	private FixedLengthStringData wsaaStatcat = new FixedLengthStringData(2);
	private PackedDecimalData wsaaAcctyr = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaStfund = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStsubsect = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRegister = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaCntbranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBandage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBandsa = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBandprm = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBandtrm = new FixedLengthStringData(2);
	private PackedDecimalData wsaaCommyr = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3);
	private ZonedDecimalData wsaaAcctmn = new ZonedDecimalData(2, 0).setUnsigned();

		/* WSAA-KEY-FIELDS-ARRAY */
	private FixedLengthStringData wsaaFields = new FixedLengthStringData(75);
	private FixedLengthStringData[] wsaaKeyField = FLSArrayPartOfStructure(15, 4, wsaaFields, 0);
	private FixedLengthStringData[] wsaaFound = FLSArrayPartOfStructure(15, 1, wsaaFields, 60);

		/* WSAA-GOVR-FIELDS-ARRAY */
	private FixedLengthStringData wsaaGovrFields = new FixedLengthStringData(60);
	private FixedLengthStringData[] wsaaGovrField = FLSArrayPartOfStructure(15, 4, wsaaGovrFields, 0);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaGovrFound = "";
	private String e015 = "E015";
	private String e382 = "E382";
		/* TABLES */
	private String t3629 = "T3629";
		/* FORMATS */
	private String govrrec = "GOVRREC";
	private String descrec = "DESCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Acmdescrec acmdescrec = new Acmdescrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Government Statistical Accumulation*/
	private GovrTableDAM govrIO = new GovrTableDAM();
	private S6709ScreenVars sv = ScreenProgram.getScreenVars( S6709ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		checkForErrors2080, 
		exit2090, 
		exit2190, 
		exit2290, 
		exit2649
	}

	public P6709() {
		super();
		screenVars = sv;
		new ScreenModel("S6709", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsaaI.set(ZERO);
		wsaaAcctmn.set(ZERO);
		wsaaCommyr.set(ZERO);
		wsaaAcctyr.set(9999);
		initialiseScreen1100();
		/*EXIT*/
	}

protected void initialiseScreen1100()
	{
		para1100();
	}

protected void para1100()
	{
		sv.dataArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		sv.stcmthOut[varcom.nd.toInt()].set("Y");
		sv.acctyr.set(ZERO);
		sv.acmn.set(ZERO);
		sv.acyr.set(ZERO);
		sv.commyr.set(ZERO);
		sv.stcmth.set(ZERO);
		sv.stimth.set(ZERO);
		sv.stpmth.set(ZERO);
		sv.stsmth.set(ZERO);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					screenIo2010();
					validate2020();
					displayGovrRecord2040();
					setUpMonthDesc2050();
				}
				case checkForErrors2080: {
					checkForErrors2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
		if (isEQ(sv.statcat,wsaaStatcat)
		&& isEQ(sv.acctyr,wsaaAcctyr)
		&& isEQ(sv.statFund,wsaaStfund)
		&& isEQ(sv.statSect,wsaaStsect)
		&& isEQ(sv.stsubsect,wsaaStsubsect)
		&& isEQ(sv.register,wsaaRegister)
		&& isEQ(sv.cntbranch,wsaaCntbranch)
		&& isEQ(sv.bandage,wsaaBandage)
		&& isEQ(sv.bandsa,wsaaBandsa)
		&& isEQ(sv.bandprm,wsaaBandprm)
		&& isEQ(sv.bandtrm,wsaaBandtrm)
		&& isEQ(sv.commyr,wsaaCommyr)
		&& isEQ(sv.pstatcd,wsaaPstatcode)
		&& isEQ(sv.cntcurr,wsaaCntcurr)) {
			getNextDetail2300();
		}
		else {
			begnDisplay2400();
		}
		if (isEQ(wsaaI,ZERO)
		|| isGT(wsaaI,12)) {
			wsaaI.set(1);
			wsaaAcctmn.set(1);
		}
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			initialiseScreen1100();
			wsaaI.set(ZERO);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(govrIO.getStatuz(),varcom.endp)
		|| isNE(govrIO.getChdrcoy(),wsspcomn.company)) {
			initialiseScreen1100();
			wsaaI.set(ZERO);
			scrnparams.errorCode.set(e382);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isGT(sv.acmn,12)) {
			sv.acmnErr.set(e015);
			goTo(GotoLabel.checkForErrors2080);
		}
	}

protected void displayGovrRecord2040()
	{
		sv.stcmthOut[varcom.nd.toInt()].set("N");
		sv.acmn.set(wsaaI);
		sv.acyr.set(govrIO.getAcctyr());
		sv.acctyr.set(govrIO.getAcctyr());
		sv.statcat.set(govrIO.getStatcat());
		sv.statFund.set(govrIO.getStatFund());
		sv.statSect.set(govrIO.getStatSect());
		sv.stsubsect.set(govrIO.getStatSubsect());
		sv.register.set(govrIO.getRegister());
		sv.cntbranch.set(govrIO.getCntbranch());
		sv.bandage.set(govrIO.getBandage());
		sv.bandsa.set(govrIO.getBandsa());
		sv.bandprm.set(govrIO.getBandprm());
		sv.bandtrm.set(govrIO.getBandtrm());
		sv.commyr.set(govrIO.getCommyr());
		sv.pstatcd.set(govrIO.getPstatcode());
		sv.cntcurr.set(govrIO.getCntcurr());
		sv.stcmth.set(govrIO.getStcmth(wsaaI));
		sv.stpmth.set(govrIO.getStpmth(wsaaI));
		sv.stsmth.set(govrIO.getStsmth(wsaaI));
		sv.stimth.set(govrIO.getStimth(wsaaI));
	}

protected void setUpMonthDesc2050()
	{
		acmdescrec.company.set(govrIO.getChdrcoy());
		acmdescrec.language.set(wsspcomn.language);
		acmdescrec.branch.set(govrIO.getCntbranch());
		acmdescrec.function.set("GETD");
		callProgram(Acmdesc.class, acmdescrec.acmdescRec);
		if (isNE(acmdescrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(acmdescrec.statuz);
			fatalError600();
		}
		sv.mthldesc.set(acmdescrec.lngdesc[wsaaI.toInt()]);
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readGovrRecord2100()
	{
		try {
			para2110();
			setUpCurrencyDesc2120();
		}
		catch (GOTOException e){
		}
	}

protected void para2110()
	{
		govrIO.setChdrcoy(wsspcomn.company);
		govrIO.setStatcat(sv.statcat);
		govrIO.setAcctyr(sv.acctyr);
		govrIO.setStatFund(sv.statFund);
		govrIO.setStatSect(sv.statSect);
		govrIO.setStatSubsect(sv.stsubsect);
		govrIO.setRegister(sv.register);
		govrIO.setCntbranch(sv.cntbranch);
		govrIO.setBandage(sv.bandage);
		govrIO.setBandsa(sv.bandsa);
		govrIO.setBandprm(sv.bandprm);
		govrIO.setBandtrm(sv.bandtrm);
		govrIO.setCommyr(sv.commyr);
		govrIO.setPstatcode(sv.pstatcd);
		govrIO.setCntcurr(sv.cntcurr);
		govrIO.setFormat(govrrec);
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)
		&& isNE(govrIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(govrIO.getParams());
			syserrrec.statuz.set(govrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(govrIO.getStatuz(),varcom.endp)
		|| isNE(govrIO.getChdrcoy(),wsspcomn.company)) {
			goTo(GotoLabel.exit2190);
		}
		if (isNE(govrIO.getFunction(),varcom.nextr)) {
			searchForGovr2500();
		}
		if (isEQ(govrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
	}

protected void setUpCurrencyDesc2120()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(govrIO.getChdrcoy());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(govrIO.getCntcurr());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.descrip.set(descIO.getLongdesc());
	}

protected void moveCurrentToWsaa2200()
	{
		try {
			para2210();
		}
		catch (GOTOException e){
		}
	}

protected void para2210()
	{
		if (isEQ(govrIO.getStatuz(),varcom.endp)
		|| isNE(govrIO.getChdrcoy(),wsspcomn.company)) {
			goTo(GotoLabel.exit2290);
		}
		wsaaAcctmn.set(wsaaI);
		wsaaStatcat.set(govrIO.getStatcat());
		wsaaAcctyr.set(govrIO.getAcctyr());
		wsaaStfund.set(govrIO.getStatFund());
		wsaaStsect.set(govrIO.getStatSect());
		wsaaStsubsect.set(govrIO.getStatSubsect());
		wsaaRegister.set(govrIO.getRegister());
		wsaaCntbranch.set(govrIO.getCntbranch());
		wsaaBandage.set(govrIO.getBandage());
		wsaaBandsa.set(govrIO.getBandsa());
		wsaaBandprm.set(govrIO.getBandprm());
		wsaaBandtrm.set(govrIO.getBandtrm());
		wsaaCommyr.set(govrIO.getCommyr());
		wsaaPstatcode.set(govrIO.getPstatcode());
		wsaaCntcurr.set(govrIO.getCntcurr());
	}

protected void getNextDetail2300()
	{
		/*PARA*/
		if (isEQ(wsaaI,12)) {
			govrIO.setFunction(varcom.nextr);
			wsaaI.set(1);
			wsaaAcctmn.set(1);
			readGovrRecord2100();
			moveCurrentToWsaa2200();
		}
		else {
			if (isEQ(sv.acmn,wsaaAcctmn)
			|| isEQ(sv.acmn,ZERO)) {
				wsaaI.add(1);
				wsaaAcctmn.set(wsaaI);
			}
			else {
				wsaaI.set(sv.acmn);
				wsaaAcctmn.set(sv.acmn);
			}
		}
		/*EXIT*/
	}

protected void begnDisplay2400()
	{
		/*PARA*/
		if (isEQ(sv.acmn,ZERO)
		|| isGT(sv.acmn,12)) {
			wsaaI.add(1);
			wsaaAcctmn.set(wsaaI);
		}
		else {
			wsaaI.set(sv.acmn);
			wsaaAcctmn.set(sv.acmn);
		}
		govrIO.setFunction(varcom.begn);
		//performance improvement -- Anjali
		govrIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		govrIO.setFitKeysSearch("CHDRCOY");
		readGovrRecord2100();
		moveCurrentToWsaa2200();
		/*EXIT*/
	}

protected void searchForGovr2500()
	{
		search2510();
	}

protected void search2510()
	{
		wsaaKeyField[1].set(sv.company);
		wsaaKeyField[2].set(sv.statcat);
		if (isNE(sv.acctyr,ZERO)) {
			wsaaKeyField[3].set(sv.acctyr);
		}
		else {
			wsaaKeyField[3].set(SPACES);
		}
		wsaaKeyField[4].set(sv.statFund);
		wsaaKeyField[5].set(sv.statSect);
		wsaaKeyField[6].set(sv.stsubsect);
		wsaaKeyField[7].set(sv.register);
		wsaaKeyField[8].set(sv.cntbranch);
		wsaaKeyField[9].set(sv.bandage);
		wsaaKeyField[10].set(sv.bandsa);
		wsaaKeyField[11].set(sv.bandprm);
		wsaaKeyField[12].set(sv.bandtrm);
		if (isNE(sv.commyr,ZERO)) {
			wsaaKeyField[13].set(sv.commyr);
		}
		else {
			wsaaKeyField[13].set(SPACES);
		}
		wsaaKeyField[14].set(sv.pstatcd);
		wsaaKeyField[15].set(sv.cntcurr);
		wsaaGovrFound = "N";
		while ( !(isEQ(wsaaGovrFound,"Y")
		|| isEQ(govrIO.getStatuz(),varcom.endp))) {
			checkGovr2600();
		}
		
	}

protected void checkGovr2600()
	{
		try {
			check2610();
		}
		catch (GOTOException e){
		}
	}

protected void check2610()
	{
		wsaaGovrField[1].set(govrIO.getChdrcoy());
		wsaaGovrField[2].set(govrIO.getStatcat());
		wsaaGovrField[3].set(govrIO.getAcctyr());
		wsaaGovrField[4].set(govrIO.getStatFund());
		wsaaGovrField[5].set(govrIO.getStatSect());
		wsaaGovrField[6].set(govrIO.getStatSubsect());
		wsaaGovrField[7].set(govrIO.getRegister());
		wsaaGovrField[8].set(govrIO.getCntbranch());
		wsaaGovrField[9].set(govrIO.getBandage());
		wsaaGovrField[10].set(govrIO.getBandsa());
		wsaaGovrField[11].set(govrIO.getBandprm());
		wsaaGovrField[12].set(govrIO.getBandtrm());
		wsaaGovrField[13].set(govrIO.getCommyr());
		wsaaGovrField[14].set(govrIO.getPstatcode());
		wsaaGovrField[15].set(govrIO.getCntcurr());
		for (wsaaX.set(1); !(isGT(wsaaX,15)); wsaaX.add(1)){
			wsaaFound[wsaaX.toInt()].set("N");
		}
		for (wsaaX.set(1); !(isGT(wsaaX,15)
		|| isEQ(govrIO.getStatuz(),varcom.endp)); wsaaX.add(1)){
			check2650();
		}
		if (isEQ(govrIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2649);
		}
		wsaaGovrFound = "Y";
		for (wsaaX.set(1); !(isGT(wsaaX,15)
		|| isEQ(wsaaGovrFound,"N")); wsaaX.add(1)){
			if (isNE(wsaaKeyField[wsaaX.toInt()],SPACES)
			&& isNE(wsaaFound[wsaaX.toInt()],"Y")) {
				wsaaGovrFound = "N";
			}
		}
		if (isEQ(wsaaGovrFound,"Y")) {
			goTo(GotoLabel.exit2649);
		}
		govrIO.setFunction(varcom.nextr);
		govrIO.setFormat(govrrec);
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)
		&& isNE(govrIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(govrIO.getStatuz());
			syserrrec.params.set(govrIO.getParams());
			fatalError600();
		}
	}

protected void check2650()
	{
		/*CHECK*/
		if (isNE(wsaaKeyField[wsaaX.toInt()],wsaaGovrField[wsaaX.toInt()])) {
			if (isEQ(wsaaFound[wsaaX.toInt()],"Y")) {
				govrIO.setStatuz(varcom.endp);
			}
		}
		else {
			wsaaFound[wsaaX.toInt()].set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
