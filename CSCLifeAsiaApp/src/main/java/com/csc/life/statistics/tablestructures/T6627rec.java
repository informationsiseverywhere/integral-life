package com.csc.life.statistics.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:28
 * Description:
 * Copybook name: T6627REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6627rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6627Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData statbands = new FixedLengthStringData(30).isAPartOf(t6627Rec, 0);
  	public FixedLengthStringData[] statband = FLSArrayPartOfStructure(15, 2, statbands, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(statbands, 0, FILLER_REDEFINE);
  	public FixedLengthStringData statband01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData statband02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData statband03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData statband04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData statband05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData statband06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData statband07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData statband08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData statband09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData statband10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData statband11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData statband12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData statband13 = new FixedLengthStringData(2).isAPartOf(filler, 24);
  	public FixedLengthStringData statband14 = new FixedLengthStringData(2).isAPartOf(filler, 26);
  	public FixedLengthStringData statband15 = new FixedLengthStringData(2).isAPartOf(filler, 28);
  	public FixedLengthStringData statfroms = new FixedLengthStringData(225).isAPartOf(t6627Rec, 30);
  	public ZonedDecimalData[] statfrom = ZDArrayPartOfStructure(15, 15, 0, statfroms, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(225).isAPartOf(statfroms, 0, FILLER_REDEFINE);
  	public ZonedDecimalData statfrom01 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 0);
  	public ZonedDecimalData statfrom02 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 15);
  	public ZonedDecimalData statfrom03 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 30);
  	public ZonedDecimalData statfrom04 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 45);
  	public ZonedDecimalData statfrom05 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 60);
  	public ZonedDecimalData statfrom06 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 75);
  	public ZonedDecimalData statfrom07 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 90);
  	public ZonedDecimalData statfrom08 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 105);
  	public ZonedDecimalData statfrom09 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 120);
  	public ZonedDecimalData statfrom10 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 135);
  	public ZonedDecimalData statfrom11 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 150);
  	public ZonedDecimalData statfrom12 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 165);
  	public ZonedDecimalData statfrom13 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 180);
  	public ZonedDecimalData statfrom14 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 195);
  	public ZonedDecimalData statfrom15 = new ZonedDecimalData(15, 0).isAPartOf(filler1, 210);
  	public FixedLengthStringData stattos = new FixedLengthStringData(225).isAPartOf(t6627Rec, 255);
  	public ZonedDecimalData[] statto = ZDArrayPartOfStructure(15, 15, 0, stattos, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(225).isAPartOf(stattos, 0, FILLER_REDEFINE);
  	public ZonedDecimalData statto01 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 0);
  	public ZonedDecimalData statto02 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 15);
  	public ZonedDecimalData statto03 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 30);
  	public ZonedDecimalData statto04 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 45);
  	public ZonedDecimalData statto05 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 60);
  	public ZonedDecimalData statto06 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 75);
  	public ZonedDecimalData statto07 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 90);
  	public ZonedDecimalData statto08 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 105);
  	public ZonedDecimalData statto09 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 120);
  	public ZonedDecimalData statto10 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 135);
  	public ZonedDecimalData statto11 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 150);
  	public ZonedDecimalData statto12 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 165);
  	public ZonedDecimalData statto13 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 180);
  	public ZonedDecimalData statto14 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 195);
  	public ZonedDecimalData statto15 = new ZonedDecimalData(15, 0).isAPartOf(filler2, 210);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(20).isAPartOf(t6627Rec, 480, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6627Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6627Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}