package com.csc.life.statistics.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: SttrTableDAM.java
 * Date: Sun, 30 Aug 2009 03:48:24
 * Class transformed from STTR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class SttrTableDAM extends SttrpfTableDAM {

	public SttrTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("STTR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "BATCCOY"
		             + ", BATCBRN"
		             + ", BATCACTYR"
		             + ", BATCACTMN"
		             + ", BATCTRCDE"
		             + ", BATCBATCH"
		             + ", CNTCURR";
		
		QUALIFIEDCOLUMNS = 
		            "REG, " +
		            "SEX, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "REVTRCDE, " +
		            "CHDRPFX, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "TRANNO, " +
		            "TRANNOR, " +
		            "STATGOV, " +
		            "STATAGT, " +
		            "AGNTNUM, " +
		            "ARACDE, " +
		            "CNTBRANCH, " +
		            "BANDAGE, " +
		            "BANDSA, " +
		            "BANDPRM, " +
		            "BANDTRM, " +
		            "CNTTYPE, " +
		            "CRTABLE, " +
		            "PARIND, " +
		            "COMMYR, " +
		            "CNPSTAT, " +
		            "PSTATCODE, " +
		            "CNTCURR, " +
		            "ACCTCCY, " +
		            "BILLFREQ, " +
		            "STFUND, " +
		            "STSECT, " +
		            "STSSECT, " +
		            "BANDAGEG, " +
		            "BANDSAG, " +
		            "BANDPRMG, " +
		            "BANDTRMG, " +
		            "SRCEBUS, " +
		            "MORTCLS, " +
		            "REPTCD01, " +
		            "REPTCD02, " +
		            "REPTCD03, " +
		            "REPTCD04, " +
		            "REPTCD05, " +
		            "REPTCD06, " +
		            "STCA, " +
		            "STCB, " +
		            "STCC, " +
		            "STCD, " +
		            "STCE, " +
		            "OVRDCAT, " +
		            "STATCAT, " +
		            "STCMTH, " +
		            "STVMTH, " +
		            "STPMTH, " +
		            "STSMTH, " +
		            "STMMTH, " +
		            "STIMTH, " +
		            "STCMTHG, " +
		            "STPMTHG, " +
		            "STSMTHG, " +
		            "STUMTH, " +
		            "STNMTH, " +
		            "STLMTH, " +
		            "STBMTHG, " +
		            "STLDMTHG, " +
		            "STRAAMT, " +
		            "TRDT, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "BATCCOY ASC, " +
		            "BATCBRN ASC, " +
		            "BATCACTYR ASC, " +
		            "BATCACTMN ASC, " +
		            "BATCTRCDE ASC, " +
		            "BATCBATCH ASC, " +
		            "CNTCURR ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "BATCCOY DESC, " +
		            "BATCBRN DESC, " +
		            "BATCACTYR DESC, " +
		            "BATCACTMN DESC, " +
		            "BATCTRCDE DESC, " +
		            "BATCBATCH DESC, " +
		            "CNTCURR DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               register,
                               sex,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               revtrcde,
                               chdrpfx,
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               tranno,
                               trannor,
                               statgov,
                               statagt,
                               agntnum,
                               aracde,
                               cntbranch,
                               bandage,
                               bandsa,
                               bandprm,
                               bandtrm,
                               cnttype,
                               crtable,
                               parind,
                               commyr,
                               cnPremStat,
                               pstatcode,
                               cntcurr,
                               acctccy,
                               billfreq,
                               statFund,
                               statSect,
                               statSubsect,
                               bandageg,
                               bandsag,
                               bandprmg,
                               bandtrmg,
                               srcebus,
                               mortcls,
                               reptcd01,
                               reptcd02,
                               reptcd03,
                               reptcd04,
                               reptcd05,
                               reptcd06,
                               chdrstcda,
                               chdrstcdb,
                               chdrstcdc,
                               chdrstcdd,
                               chdrstcde,
                               ovrdcat,
                               statcat,
                               stcmth,
                               stvmth,
                               stpmth,
                               stsmth,
                               stmmth,
                               stimth,
                               stcmthg,
                               stpmthg,
                               stsmthg,
                               stumth,
                               stnmth,
                               stlmth,
                               stbmthg,
                               stldmthg,
                               straamt,
                               transactionDate,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(44);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getCntcurr().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller320 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(batccoy.toInternal());
	nonKeyFiller20.setInternal(batcbrn.toInternal());
	nonKeyFiller30.setInternal(batcactyr.toInternal());
	nonKeyFiller40.setInternal(batcactmn.toInternal());
	nonKeyFiller50.setInternal(batctrcde.toInternal());
	nonKeyFiller60.setInternal(batcbatch.toInternal());
	nonKeyFiller320.setInternal(cntcurr.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(321);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ getRevtrcde().toInternal()
					+ getChdrpfx().toInternal()
					+ getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getTranno().toInternal()
					+ getTrannor().toInternal()
					+ getStatgov().toInternal()
					+ getStatagt().toInternal()
					+ getAgntnum().toInternal()
					+ getAracde().toInternal()
					+ getCntbranch().toInternal()
					+ getBandage().toInternal()
					+ getBandsa().toInternal()
					+ getBandprm().toInternal()
					+ getBandtrm().toInternal()
					+ getCnttype().toInternal()
					+ getCrtable().toInternal()
					+ getParind().toInternal()
					+ getCommyr().toInternal()
					+ getCnPremStat().toInternal()
					+ getPstatcode().toInternal()
					+ nonKeyFiller320.toInternal()
					+ getAcctccy().toInternal()
					+ getBillfreq().toInternal()
					+ getRegister().toInternal()
					+ getStatFund().toInternal()
					+ getStatSect().toInternal()
					+ getStatSubsect().toInternal()
					+ getBandageg().toInternal()
					+ getBandsag().toInternal()
					+ getBandprmg().toInternal()
					+ getBandtrmg().toInternal()
					+ getSrcebus().toInternal()
					+ getSex().toInternal()
					+ getMortcls().toInternal()
					+ getReptcd01().toInternal()
					+ getReptcd02().toInternal()
					+ getReptcd03().toInternal()
					+ getReptcd04().toInternal()
					+ getReptcd05().toInternal()
					+ getReptcd06().toInternal()
					+ getChdrstcda().toInternal()
					+ getChdrstcdb().toInternal()
					+ getChdrstcdc().toInternal()
					+ getChdrstcdd().toInternal()
					+ getChdrstcde().toInternal()
					+ getOvrdcat().toInternal()
					+ getStatcat().toInternal()
					+ getStcmth().toInternal()
					+ getStvmth().toInternal()
					+ getStpmth().toInternal()
					+ getStsmth().toInternal()
					+ getStmmth().toInternal()
					+ getStimth().toInternal()
					+ getStcmthg().toInternal()
					+ getStpmthg().toInternal()
					+ getStsmthg().toInternal()
					+ getStumth().toInternal()
					+ getStnmth().toInternal()
					+ getStlmth().toInternal()
					+ getStbmthg().toInternal()
					+ getStldmthg().toInternal()
					+ getStraamt().toInternal()
					+ getTransactionDate().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, revtrcde);
			what = ExternalData.chop(what, chdrpfx);
			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, trannor);
			what = ExternalData.chop(what, statgov);
			what = ExternalData.chop(what, statagt);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, aracde);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, bandage);
			what = ExternalData.chop(what, bandsa);
			what = ExternalData.chop(what, bandprm);
			what = ExternalData.chop(what, bandtrm);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, parind);
			what = ExternalData.chop(what, commyr);
			what = ExternalData.chop(what, cnPremStat);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, nonKeyFiller320);
			what = ExternalData.chop(what, acctccy);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, statFund);
			what = ExternalData.chop(what, statSect);
			what = ExternalData.chop(what, statSubsect);
			what = ExternalData.chop(what, bandageg);
			what = ExternalData.chop(what, bandsag);
			what = ExternalData.chop(what, bandprmg);
			what = ExternalData.chop(what, bandtrmg);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, sex);
			what = ExternalData.chop(what, mortcls);
			what = ExternalData.chop(what, reptcd01);
			what = ExternalData.chop(what, reptcd02);
			what = ExternalData.chop(what, reptcd03);
			what = ExternalData.chop(what, reptcd04);
			what = ExternalData.chop(what, reptcd05);
			what = ExternalData.chop(what, reptcd06);
			what = ExternalData.chop(what, chdrstcda);
			what = ExternalData.chop(what, chdrstcdb);
			what = ExternalData.chop(what, chdrstcdc);
			what = ExternalData.chop(what, chdrstcdd);
			what = ExternalData.chop(what, chdrstcde);
			what = ExternalData.chop(what, ovrdcat);
			what = ExternalData.chop(what, statcat);
			what = ExternalData.chop(what, stcmth);
			what = ExternalData.chop(what, stvmth);
			what = ExternalData.chop(what, stpmth);
			what = ExternalData.chop(what, stsmth);
			what = ExternalData.chop(what, stmmth);
			what = ExternalData.chop(what, stimth);
			what = ExternalData.chop(what, stcmthg);
			what = ExternalData.chop(what, stpmthg);
			what = ExternalData.chop(what, stsmthg);
			what = ExternalData.chop(what, stumth);
			what = ExternalData.chop(what, stnmth);
			what = ExternalData.chop(what, stlmth);
			what = ExternalData.chop(what, stbmthg);
			what = ExternalData.chop(what, stldmthg);
			what = ExternalData.chop(what, straamt);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getRevtrcde() {
		return revtrcde;
	}
	public void setRevtrcde(Object what) {
		revtrcde.set(what);
	}	
	public FixedLengthStringData getChdrpfx() {
		return chdrpfx;
	}
	public void setChdrpfx(Object what) {
		chdrpfx.set(what);
	}	
	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}	
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}	
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}	
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}	
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}	
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getTrannor() {
		return trannor;
	}
	public void setTrannor(Object what) {
		setTrannor(what, false);
	}
	public void setTrannor(Object what, boolean rounded) {
		if (rounded)
			trannor.setRounded(what);
		else
			trannor.set(what);
	}	
	public FixedLengthStringData getStatgov() {
		return statgov;
	}
	public void setStatgov(Object what) {
		statgov.set(what);
	}	
	public FixedLengthStringData getStatagt() {
		return statagt;
	}
	public void setStatagt(Object what) {
		statagt.set(what);
	}	
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}	
	public FixedLengthStringData getAracde() {
		return aracde;
	}
	public void setAracde(Object what) {
		aracde.set(what);
	}	
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}	
	public FixedLengthStringData getBandage() {
		return bandage;
	}
	public void setBandage(Object what) {
		bandage.set(what);
	}	
	public FixedLengthStringData getBandsa() {
		return bandsa;
	}
	public void setBandsa(Object what) {
		bandsa.set(what);
	}	
	public FixedLengthStringData getBandprm() {
		return bandprm;
	}
	public void setBandprm(Object what) {
		bandprm.set(what);
	}	
	public FixedLengthStringData getBandtrm() {
		return bandtrm;
	}
	public void setBandtrm(Object what) {
		bandtrm.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getParind() {
		return parind;
	}
	public void setParind(Object what) {
		parind.set(what);
	}	
	public PackedDecimalData getCommyr() {
		return commyr;
	}
	public void setCommyr(Object what) {
		setCommyr(what, false);
	}
	public void setCommyr(Object what, boolean rounded) {
		if (rounded)
			commyr.setRounded(what);
		else
			commyr.set(what);
	}	
	public FixedLengthStringData getCnPremStat() {
		return cnPremStat;
	}
	public void setCnPremStat(Object what) {
		cnPremStat.set(what);
	}	
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}	
	public FixedLengthStringData getAcctccy() {
		return acctccy;
	}
	public void setAcctccy(Object what) {
		acctccy.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}	
	public FixedLengthStringData getStatFund() {
		return statFund;
	}
	public void setStatFund(Object what) {
		statFund.set(what);
	}	
	public FixedLengthStringData getStatSect() {
		return statSect;
	}
	public void setStatSect(Object what) {
		statSect.set(what);
	}	
	public FixedLengthStringData getStatSubsect() {
		return statSubsect;
	}
	public void setStatSubsect(Object what) {
		statSubsect.set(what);
	}	
	public FixedLengthStringData getBandageg() {
		return bandageg;
	}
	public void setBandageg(Object what) {
		bandageg.set(what);
	}	
	public FixedLengthStringData getBandsag() {
		return bandsag;
	}
	public void setBandsag(Object what) {
		bandsag.set(what);
	}	
	public FixedLengthStringData getBandprmg() {
		return bandprmg;
	}
	public void setBandprmg(Object what) {
		bandprmg.set(what);
	}	
	public FixedLengthStringData getBandtrmg() {
		return bandtrmg;
	}
	public void setBandtrmg(Object what) {
		bandtrmg.set(what);
	}	
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}	
	public FixedLengthStringData getSex() {
		return sex;
	}
	public void setSex(Object what) {
		sex.set(what);
	}	
	public FixedLengthStringData getMortcls() {
		return mortcls;
	}
	public void setMortcls(Object what) {
		mortcls.set(what);
	}	
	public FixedLengthStringData getReptcd01() {
		return reptcd01;
	}
	public void setReptcd01(Object what) {
		reptcd01.set(what);
	}	
	public FixedLengthStringData getReptcd02() {
		return reptcd02;
	}
	public void setReptcd02(Object what) {
		reptcd02.set(what);
	}	
	public FixedLengthStringData getReptcd03() {
		return reptcd03;
	}
	public void setReptcd03(Object what) {
		reptcd03.set(what);
	}	
	public FixedLengthStringData getReptcd04() {
		return reptcd04;
	}
	public void setReptcd04(Object what) {
		reptcd04.set(what);
	}	
	public FixedLengthStringData getReptcd05() {
		return reptcd05;
	}
	public void setReptcd05(Object what) {
		reptcd05.set(what);
	}	
	public FixedLengthStringData getReptcd06() {
		return reptcd06;
	}
	public void setReptcd06(Object what) {
		reptcd06.set(what);
	}	
	public FixedLengthStringData getChdrstcda() {
		return chdrstcda;
	}
	public void setChdrstcda(Object what) {
		chdrstcda.set(what);
	}	
	public FixedLengthStringData getChdrstcdb() {
		return chdrstcdb;
	}
	public void setChdrstcdb(Object what) {
		chdrstcdb.set(what);
	}	
	public FixedLengthStringData getChdrstcdc() {
		return chdrstcdc;
	}
	public void setChdrstcdc(Object what) {
		chdrstcdc.set(what);
	}	
	public FixedLengthStringData getChdrstcdd() {
		return chdrstcdd;
	}
	public void setChdrstcdd(Object what) {
		chdrstcdd.set(what);
	}	
	public FixedLengthStringData getChdrstcde() {
		return chdrstcde;
	}
	public void setChdrstcde(Object what) {
		chdrstcde.set(what);
	}	
	public FixedLengthStringData getOvrdcat() {
		return ovrdcat;
	}
	public void setOvrdcat(Object what) {
		ovrdcat.set(what);
	}	
	public FixedLengthStringData getStatcat() {
		return statcat;
	}
	public void setStatcat(Object what) {
		statcat.set(what);
	}	
	public PackedDecimalData getStcmth() {
		return stcmth;
	}
	public void setStcmth(Object what) {
		setStcmth(what, false);
	}
	public void setStcmth(Object what, boolean rounded) {
		if (rounded)
			stcmth.setRounded(what);
		else
			stcmth.set(what);
	}	
	public PackedDecimalData getStvmth() {
		return stvmth;
	}
	public void setStvmth(Object what) {
		setStvmth(what, false);
	}
	public void setStvmth(Object what, boolean rounded) {
		if (rounded)
			stvmth.setRounded(what);
		else
			stvmth.set(what);
	}	
	public PackedDecimalData getStpmth() {
		return stpmth;
	}
	public void setStpmth(Object what) {
		setStpmth(what, false);
	}
	public void setStpmth(Object what, boolean rounded) {
		if (rounded)
			stpmth.setRounded(what);
		else
			stpmth.set(what);
	}	
	public PackedDecimalData getStsmth() {
		return stsmth;
	}
	public void setStsmth(Object what) {
		setStsmth(what, false);
	}
	public void setStsmth(Object what, boolean rounded) {
		if (rounded)
			stsmth.setRounded(what);
		else
			stsmth.set(what);
	}	
	public PackedDecimalData getStmmth() {
		return stmmth;
	}
	public void setStmmth(Object what) {
		setStmmth(what, false);
	}
	public void setStmmth(Object what, boolean rounded) {
		if (rounded)
			stmmth.setRounded(what);
		else
			stmmth.set(what);
	}	
	public PackedDecimalData getStimth() {
		return stimth;
	}
	public void setStimth(Object what) {
		setStimth(what, false);
	}
	public void setStimth(Object what, boolean rounded) {
		if (rounded)
			stimth.setRounded(what);
		else
			stimth.set(what);
	}	
	public PackedDecimalData getStcmthg() {
		return stcmthg;
	}
	public void setStcmthg(Object what) {
		setStcmthg(what, false);
	}
	public void setStcmthg(Object what, boolean rounded) {
		if (rounded)
			stcmthg.setRounded(what);
		else
			stcmthg.set(what);
	}	
	public PackedDecimalData getStpmthg() {
		return stpmthg;
	}
	public void setStpmthg(Object what) {
		setStpmthg(what, false);
	}
	public void setStpmthg(Object what, boolean rounded) {
		if (rounded)
			stpmthg.setRounded(what);
		else
			stpmthg.set(what);
	}	
	public PackedDecimalData getStsmthg() {
		return stsmthg;
	}
	public void setStsmthg(Object what) {
		setStsmthg(what, false);
	}
	public void setStsmthg(Object what, boolean rounded) {
		if (rounded)
			stsmthg.setRounded(what);
		else
			stsmthg.set(what);
	}	
	public PackedDecimalData getStumth() {
		return stumth;
	}
	public void setStumth(Object what) {
		setStumth(what, false);
	}
	public void setStumth(Object what, boolean rounded) {
		if (rounded)
			stumth.setRounded(what);
		else
			stumth.set(what);
	}	
	public PackedDecimalData getStnmth() {
		return stnmth;
	}
	public void setStnmth(Object what) {
		setStnmth(what, false);
	}
	public void setStnmth(Object what, boolean rounded) {
		if (rounded)
			stnmth.setRounded(what);
		else
			stnmth.set(what);
	}	
	public PackedDecimalData getStlmth() {
		return stlmth;
	}
	public void setStlmth(Object what) {
		setStlmth(what, false);
	}
	public void setStlmth(Object what, boolean rounded) {
		if (rounded)
			stlmth.setRounded(what);
		else
			stlmth.set(what);
	}	
	public PackedDecimalData getStbmthg() {
		return stbmthg;
	}
	public void setStbmthg(Object what) {
		setStbmthg(what, false);
	}
	public void setStbmthg(Object what, boolean rounded) {
		if (rounded)
			stbmthg.setRounded(what);
		else
			stbmthg.set(what);
	}	
	public PackedDecimalData getStldmthg() {
		return stldmthg;
	}
	public void setStldmthg(Object what) {
		setStldmthg(what, false);
	}
	public void setStldmthg(Object what, boolean rounded) {
		if (rounded)
			stldmthg.setRounded(what);
		else
			stldmthg.set(what);
	}	
	public PackedDecimalData getStraamt() {
		return straamt;
	}
	public void setStraamt(Object what) {
		setStraamt(what, false);
	}
	public void setStraamt(Object what, boolean rounded) {
		if (rounded)
			straamt.setRounded(what);
		else
			straamt.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getReptcds() {
		return new FixedLengthStringData(reptcd01.toInternal()
										+ reptcd02.toInternal()
										+ reptcd03.toInternal()
										+ reptcd04.toInternal()
										+ reptcd05.toInternal()
										+ reptcd06.toInternal());
	}
	public void setReptcds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getReptcds().getLength()).init(obj);
	
		what = ExternalData.chop(what, reptcd01);
		what = ExternalData.chop(what, reptcd02);
		what = ExternalData.chop(what, reptcd03);
		what = ExternalData.chop(what, reptcd04);
		what = ExternalData.chop(what, reptcd05);
		what = ExternalData.chop(what, reptcd06);
	}
	public FixedLengthStringData getReptcd(BaseData indx) {
		return getReptcd(indx.toInt());
	}
	public FixedLengthStringData getReptcd(int indx) {

		switch (indx) {
			case 1 : return reptcd01;
			case 2 : return reptcd02;
			case 3 : return reptcd03;
			case 4 : return reptcd04;
			case 5 : return reptcd05;
			case 6 : return reptcd06;
			default: return null; // Throw error instead?
		}
	
	}
	public void setReptcd(BaseData indx, Object what) {
		setReptcd(indx.toInt(), what);
	}
	public void setReptcd(int indx, Object what) {

		switch (indx) {
			case 1 : setReptcd01(what);
					 break;
			case 2 : setReptcd02(what);
					 break;
			case 3 : setReptcd03(what);
					 break;
			case 4 : setReptcd04(what);
					 break;
			case 5 : setReptcd05(what);
					 break;
			case 6 : setReptcd06(what);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		cntcurr.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		revtrcde.clear();
		chdrpfx.clear();
		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		tranno.clear();
		trannor.clear();
		statgov.clear();
		statagt.clear();
		agntnum.clear();
		aracde.clear();
		cntbranch.clear();
		bandage.clear();
		bandsa.clear();
		bandprm.clear();
		bandtrm.clear();
		cnttype.clear();
		crtable.clear();
		parind.clear();
		commyr.clear();
		cnPremStat.clear();
		pstatcode.clear();
		nonKeyFiller320.clear();
		acctccy.clear();
		billfreq.clear();
		register.clear();
		statFund.clear();
		statSect.clear();
		statSubsect.clear();
		bandageg.clear();
		bandsag.clear();
		bandprmg.clear();
		bandtrmg.clear();
		srcebus.clear();
		sex.clear();
		mortcls.clear();
		reptcd01.clear();
		reptcd02.clear();
		reptcd03.clear();
		reptcd04.clear();
		reptcd05.clear();
		reptcd06.clear();
		chdrstcda.clear();
		chdrstcdb.clear();
		chdrstcdc.clear();
		chdrstcdd.clear();
		chdrstcde.clear();
		ovrdcat.clear();
		statcat.clear();
		stcmth.clear();
		stvmth.clear();
		stpmth.clear();
		stsmth.clear();
		stmmth.clear();
		stimth.clear();
		stcmthg.clear();
		stpmthg.clear();
		stsmthg.clear();
		stumth.clear();
		stnmth.clear();
		stlmth.clear();
		stbmthg.clear();
		stldmthg.clear();
		straamt.clear();
		transactionDate.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}