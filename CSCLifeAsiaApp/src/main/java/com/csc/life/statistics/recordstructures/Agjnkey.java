package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:05
 * Description:
 * Copybook name: AGJNKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agjnkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agjnFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agjnKey = new FixedLengthStringData(64).isAPartOf(agjnFileKey, 0, REDEFINE);
  	public FixedLengthStringData agjnChdrcoy = new FixedLengthStringData(1).isAPartOf(agjnKey, 0);
  	public FixedLengthStringData agjnAgntnum = new FixedLengthStringData(8).isAPartOf(agjnKey, 1);
  	public FixedLengthStringData agjnStatcat = new FixedLengthStringData(2).isAPartOf(agjnKey, 9);
  	public PackedDecimalData agjnAcctyr = new PackedDecimalData(4, 0).isAPartOf(agjnKey, 11);
  	public PackedDecimalData agjnAcctmonth = new PackedDecimalData(2, 0).isAPartOf(agjnKey, 14);
  	public FixedLengthStringData agjnAracde = new FixedLengthStringData(3).isAPartOf(agjnKey, 16);
  	public FixedLengthStringData agjnCntbranch = new FixedLengthStringData(2).isAPartOf(agjnKey, 19);
  	public FixedLengthStringData agjnBandage = new FixedLengthStringData(2).isAPartOf(agjnKey, 21);
  	public FixedLengthStringData agjnBandsa = new FixedLengthStringData(2).isAPartOf(agjnKey, 23);
  	public FixedLengthStringData agjnBandprm = new FixedLengthStringData(2).isAPartOf(agjnKey, 25);
  	public FixedLengthStringData agjnBandtrm = new FixedLengthStringData(2).isAPartOf(agjnKey, 27);
  	public FixedLengthStringData agjnCnttype = new FixedLengthStringData(3).isAPartOf(agjnKey, 29);
  	public FixedLengthStringData agjnCrtable = new FixedLengthStringData(4).isAPartOf(agjnKey, 32);
  	public FixedLengthStringData agjnOvrdcat = new FixedLengthStringData(1).isAPartOf(agjnKey, 36);
  	public FixedLengthStringData agjnPstatcode = new FixedLengthStringData(2).isAPartOf(agjnKey, 37);
  	public FixedLengthStringData agjnCntcurr = new FixedLengthStringData(3).isAPartOf(agjnKey, 39);
  	public PackedDecimalData agjnEffdate = new PackedDecimalData(8, 0).isAPartOf(agjnKey, 42);
  	public PackedDecimalData agjnTransactionTime = new PackedDecimalData(6, 0).isAPartOf(agjnKey, 47);
  	public FixedLengthStringData filler = new FixedLengthStringData(13).isAPartOf(agjnKey, 51, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agjnFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agjnFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}