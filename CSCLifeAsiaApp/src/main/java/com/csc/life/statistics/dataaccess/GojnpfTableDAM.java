package com.csc.life.statistics.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GojnpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:03
 * Class transformed from GOJNPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GojnpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 134;
	public FixedLengthStringData gojnrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData gojnpfRecord = gojnrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(gojnrec);
	public FixedLengthStringData register = DD.reg.copy().isAPartOf(gojnrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(gojnrec);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(gojnrec);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(gojnrec);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(gojnrec);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(gojnrec);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(gojnrec);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(gojnrec);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(gojnrec);
	public PackedDecimalData commyr = DD.commyr.copy().isAPartOf(gojnrec);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(gojnrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(gojnrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(gojnrec);
	public PackedDecimalData acctyr = DD.acctyr.copy().isAPartOf(gojnrec);
	public PackedDecimalData acctmonth = DD.acctmonth.copy().isAPartOf(gojnrec);
	public PackedDecimalData stcmth = DD.stcmth.copy().isAPartOf(gojnrec);
	public PackedDecimalData stpmth = DD.stpmth.copy().isAPartOf(gojnrec);
	public PackedDecimalData stsmth = DD.stsmth.copy().isAPartOf(gojnrec);
	public PackedDecimalData stimth = DD.stimth.copy().isAPartOf(gojnrec);
	public PackedDecimalData transactionDate = DD.trdt.copy().isAPartOf(gojnrec);
	public PackedDecimalData transactionTime = DD.trtm.copy().isAPartOf(gojnrec);
	public PackedDecimalData user = DD.user.copy().isAPartOf(gojnrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(gojnrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(gojnrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(gojnrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(gojnrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public GojnpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for GojnpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public GojnpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for GojnpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public GojnpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for GojnpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public GojnpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("GOJNPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"REG, " +
							"CNTBRANCH, " +
							"STFUND, " +
							"STSECT, " +
							"STSSECT, " +
							"BANDAGE, " +
							"BANDSA, " +
							"BANDPRM, " +
							"BANDTRM, " +
							"COMMYR, " +
							"STATCAT, " +
							"PSTATCODE, " +
							"CNTCURR, " +
							"ACCTYR, " +
							"ACCTMONTH, " +
							"STCMTH, " +
							"STPMTH, " +
							"STSMTH, " +
							"STIMTH, " +
							"TRDT, " +
							"TRTM, " +
							"USER_T, " +
							"EFFDATE, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     register,
                                     cntbranch,
                                     statFund,
                                     statSect,
                                     statSubsect,
                                     bandage,
                                     bandsa,
                                     bandprm,
                                     bandtrm,
                                     commyr,
                                     statcat,
                                     pstatcode,
                                     cntcurr,
                                     acctyr,
                                     acctmonth,
                                     stcmth,
                                     stpmth,
                                     stsmth,
                                     stimth,
                                     transactionDate,
                                     transactionTime,
                                     user,
                                     effdate,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		register.clear();
  		cntbranch.clear();
  		statFund.clear();
  		statSect.clear();
  		statSubsect.clear();
  		bandage.clear();
  		bandsa.clear();
  		bandprm.clear();
  		bandtrm.clear();
  		commyr.clear();
  		statcat.clear();
  		pstatcode.clear();
  		cntcurr.clear();
  		acctyr.clear();
  		acctmonth.clear();
  		stcmth.clear();
  		stpmth.clear();
  		stsmth.clear();
  		stimth.clear();
  		transactionDate.clear();
  		transactionTime.clear();
  		user.clear();
  		effdate.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getGojnrec() {
  		return gojnrec;
	}

	public FixedLengthStringData getGojnpfRecord() {
  		return gojnpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setGojnrec(what);
	}

	public void setGojnrec(Object what) {
  		this.gojnrec.set(what);
	}

	public void setGojnpfRecord(Object what) {
  		this.gojnpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(gojnrec.getLength());
		result.set(gojnrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}