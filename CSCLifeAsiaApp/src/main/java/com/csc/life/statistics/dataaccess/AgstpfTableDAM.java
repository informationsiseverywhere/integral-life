package com.csc.life.statistics.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgstpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:50
 * Class transformed from AGSTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgstpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 666;
	public FixedLengthStringData agstrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agstpfRecord = agstrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(agstrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agstrec);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(agstrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(agstrec);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(agstrec);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(agstrec);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(agstrec);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(agstrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(agstrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(agstrec);
	public FixedLengthStringData ovrdcat = DD.ovrdcat.copy().isAPartOf(agstrec);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(agstrec);
	public FixedLengthStringData pstatcode = DD.pstatcode.copy().isAPartOf(agstrec);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(agstrec);
	public PackedDecimalData acctyr = DD.acctyr.copy().isAPartOf(agstrec);
	public PackedDecimalData bfwdc = DD.bfwdc.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth01 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth02 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth03 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth04 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth05 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth06 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth07 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth08 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth09 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth10 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth11 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stcmth12 = DD.stcmth.copy().isAPartOf(agstrec);
	public PackedDecimalData cfwdc = DD.cfwdc.copy().isAPartOf(agstrec);
	public PackedDecimalData bfwdv = DD.bfwdv.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth01 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth02 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth03 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth04 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth05 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth06 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth07 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth08 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth09 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth10 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth11 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stvmth12 = DD.stvmth.copy().isAPartOf(agstrec);
	public PackedDecimalData cfwdv = DD.cfwdv.copy().isAPartOf(agstrec);
	public PackedDecimalData bfwdp = DD.bfwdp.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth01 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth02 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth03 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth04 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth05 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth06 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth07 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth08 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth09 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth10 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth11 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stpmth12 = DD.stpmth.copy().isAPartOf(agstrec);
	public PackedDecimalData cfwdp = DD.cfwdp.copy().isAPartOf(agstrec);
	public PackedDecimalData bfwds = DD.bfwds.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth01 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth02 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth03 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth04 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth05 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth06 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth07 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth08 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth09 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth10 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth11 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stsmth12 = DD.stsmth.copy().isAPartOf(agstrec);
	public PackedDecimalData cfwds = DD.cfwds.copy().isAPartOf(agstrec);
	public PackedDecimalData bfwdm = DD.bfwdm.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth01 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth02 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth03 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth04 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth05 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth06 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth07 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth08 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth09 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth10 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth11 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData stmmth12 = DD.stmmth.copy().isAPartOf(agstrec);
	public PackedDecimalData cfwdm = DD.cfwdm.copy().isAPartOf(agstrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agstrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agstrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agstrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgstpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgstpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgstpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgstpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgstpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgstpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgstpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGSTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"AGNTNUM, " +
							"ARACDE, " +
							"CNTBRANCH, " +
							"BANDAGE, " +
							"BANDSA, " +
							"BANDPRM, " +
							"BANDTRM, " +
							"CNTTYPE, " +
							"CRTABLE, " +
							"OVRDCAT, " +
							"STATCAT, " +
							"PSTATCODE, " +
							"CNTCURR, " +
							"ACCTYR, " +
							"BFWDC, " +
							"STCMTH01, " +
							"STCMTH02, " +
							"STCMTH03, " +
							"STCMTH04, " +
							"STCMTH05, " +
							"STCMTH06, " +
							"STCMTH07, " +
							"STCMTH08, " +
							"STCMTH09, " +
							"STCMTH10, " +
							"STCMTH11, " +
							"STCMTH12, " +
							"CFWDC, " +
							"BFWDV, " +
							"STVMTH01, " +
							"STVMTH02, " +
							"STVMTH03, " +
							"STVMTH04, " +
							"STVMTH05, " +
							"STVMTH06, " +
							"STVMTH07, " +
							"STVMTH08, " +
							"STVMTH09, " +
							"STVMTH10, " +
							"STVMTH11, " +
							"STVMTH12, " +
							"CFWDV, " +
							"BFWDP, " +
							"STPMTH01, " +
							"STPMTH02, " +
							"STPMTH03, " +
							"STPMTH04, " +
							"STPMTH05, " +
							"STPMTH06, " +
							"STPMTH07, " +
							"STPMTH08, " +
							"STPMTH09, " +
							"STPMTH10, " +
							"STPMTH11, " +
							"STPMTH12, " +
							"CFWDP, " +
							"BFWDS, " +
							"STSMTH01, " +
							"STSMTH02, " +
							"STSMTH03, " +
							"STSMTH04, " +
							"STSMTH05, " +
							"STSMTH06, " +
							"STSMTH07, " +
							"STSMTH08, " +
							"STSMTH09, " +
							"STSMTH10, " +
							"STSMTH11, " +
							"STSMTH12, " +
							"CFWDS, " +
							"BFWDM, " +
							"STMMTH01, " +
							"STMMTH02, " +
							"STMMTH03, " +
							"STMMTH04, " +
							"STMMTH05, " +
							"STMMTH06, " +
							"STMMTH07, " +
							"STMMTH08, " +
							"STMMTH09, " +
							"STMMTH10, " +
							"STMMTH11, " +
							"STMMTH12, " +
							"CFWDM, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     agntnum,
                                     aracde,
                                     cntbranch,
                                     bandage,
                                     bandsa,
                                     bandprm,
                                     bandtrm,
                                     cnttype,
                                     crtable,
                                     ovrdcat,
                                     statcat,
                                     pstatcode,
                                     cntcurr,
                                     acctyr,
                                     bfwdc,
                                     stcmth01,
                                     stcmth02,
                                     stcmth03,
                                     stcmth04,
                                     stcmth05,
                                     stcmth06,
                                     stcmth07,
                                     stcmth08,
                                     stcmth09,
                                     stcmth10,
                                     stcmth11,
                                     stcmth12,
                                     cfwdc,
                                     bfwdv,
                                     stvmth01,
                                     stvmth02,
                                     stvmth03,
                                     stvmth04,
                                     stvmth05,
                                     stvmth06,
                                     stvmth07,
                                     stvmth08,
                                     stvmth09,
                                     stvmth10,
                                     stvmth11,
                                     stvmth12,
                                     cfwdv,
                                     bfwdp,
                                     stpmth01,
                                     stpmth02,
                                     stpmth03,
                                     stpmth04,
                                     stpmth05,
                                     stpmth06,
                                     stpmth07,
                                     stpmth08,
                                     stpmth09,
                                     stpmth10,
                                     stpmth11,
                                     stpmth12,
                                     cfwdp,
                                     bfwds,
                                     stsmth01,
                                     stsmth02,
                                     stsmth03,
                                     stsmth04,
                                     stsmth05,
                                     stsmth06,
                                     stsmth07,
                                     stsmth08,
                                     stsmth09,
                                     stsmth10,
                                     stsmth11,
                                     stsmth12,
                                     cfwds,
                                     bfwdm,
                                     stmmth01,
                                     stmmth02,
                                     stmmth03,
                                     stmmth04,
                                     stmmth05,
                                     stmmth06,
                                     stmmth07,
                                     stmmth08,
                                     stmmth09,
                                     stmmth10,
                                     stmmth11,
                                     stmmth12,
                                     cfwdm,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		agntnum.clear();
  		aracde.clear();
  		cntbranch.clear();
  		bandage.clear();
  		bandsa.clear();
  		bandprm.clear();
  		bandtrm.clear();
  		cnttype.clear();
  		crtable.clear();
  		ovrdcat.clear();
  		statcat.clear();
  		pstatcode.clear();
  		cntcurr.clear();
  		acctyr.clear();
  		bfwdc.clear();
  		stcmth01.clear();
  		stcmth02.clear();
  		stcmth03.clear();
  		stcmth04.clear();
  		stcmth05.clear();
  		stcmth06.clear();
  		stcmth07.clear();
  		stcmth08.clear();
  		stcmth09.clear();
  		stcmth10.clear();
  		stcmth11.clear();
  		stcmth12.clear();
  		cfwdc.clear();
  		bfwdv.clear();
  		stvmth01.clear();
  		stvmth02.clear();
  		stvmth03.clear();
  		stvmth04.clear();
  		stvmth05.clear();
  		stvmth06.clear();
  		stvmth07.clear();
  		stvmth08.clear();
  		stvmth09.clear();
  		stvmth10.clear();
  		stvmth11.clear();
  		stvmth12.clear();
  		cfwdv.clear();
  		bfwdp.clear();
  		stpmth01.clear();
  		stpmth02.clear();
  		stpmth03.clear();
  		stpmth04.clear();
  		stpmth05.clear();
  		stpmth06.clear();
  		stpmth07.clear();
  		stpmth08.clear();
  		stpmth09.clear();
  		stpmth10.clear();
  		stpmth11.clear();
  		stpmth12.clear();
  		cfwdp.clear();
  		bfwds.clear();
  		stsmth01.clear();
  		stsmth02.clear();
  		stsmth03.clear();
  		stsmth04.clear();
  		stsmth05.clear();
  		stsmth06.clear();
  		stsmth07.clear();
  		stsmth08.clear();
  		stsmth09.clear();
  		stsmth10.clear();
  		stsmth11.clear();
  		stsmth12.clear();
  		cfwds.clear();
  		bfwdm.clear();
  		stmmth01.clear();
  		stmmth02.clear();
  		stmmth03.clear();
  		stmmth04.clear();
  		stmmth05.clear();
  		stmmth06.clear();
  		stmmth07.clear();
  		stmmth08.clear();
  		stmmth09.clear();
  		stmmth10.clear();
  		stmmth11.clear();
  		stmmth12.clear();
  		cfwdm.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgstrec() {
  		return agstrec;
	}

	public FixedLengthStringData getAgstpfRecord() {
  		return agstpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgstrec(what);
	}

	public void setAgstrec(Object what) {
  		this.agstrec.set(what);
	}

	public void setAgstpfRecord(Object what) {
  		this.agstpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agstrec.getLength());
		result.set(agstrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}