/*
 * File: Bj557.java
 * Date: 29 August 2009 21:45:53
 * Author: Quipoz Limited
 *
 * Class transformed from BJ557.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.sql.SQLException;

import com.csc.life.statistics.dataaccess.GoveTableDAM;
import com.csc.life.statistics.dataaccess.GvacTableDAM;
import com.csc.life.statistics.dataaccess.GvahTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*               Statistical Year End Rollover
*               =============================
*   Basically this program will read through GOVE, GVAH and GVAC
*   for the specified year and create a new record for the
*   corresponding year with the brought forward figures.
*
*****************************************************************
* </pre>
*/
public class Bj557 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgovepfrs = null;
	private java.sql.PreparedStatement sqlgovepfps = null;
	private java.sql.Connection sqlgovepfconn = null;
	private String sqlgovepf = "";
	private java.sql.ResultSet sqlgvahpfrs = null;
	private java.sql.PreparedStatement sqlgvahpfps = null;
	private java.sql.Connection sqlgvahpfconn = null;
	private String sqlgvahpf = "";
	private java.sql.ResultSet sqlgvacpfrs = null;
	private java.sql.PreparedStatement sqlgvacpfps = null;
	private java.sql.Connection sqlgvacpfconn = null;
	private String sqlgvacpf = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ557");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData ix = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaAcctyr = new PackedDecimalData(4, 0);
		/* ERRORS */
	private String esql = "ESQL";
	private String goverec = "GOVEREC";
	private String gvahrec = "GVAHREC";
	private String gvacrec = "GVACREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-GOVEPF */
	private FixedLengthStringData sqlGoverec = new FixedLengthStringData(132);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 0);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 1);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 3);
	private FixedLengthStringData sqlSrcebus = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 6);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 8);
	private FixedLengthStringData sqlStatFund = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 10);
	private FixedLengthStringData sqlStatSect = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 11);
	private FixedLengthStringData sqlStatSubsect = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 15);
	private FixedLengthStringData sqlCnPremStat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 19);
	private FixedLengthStringData sqlCovPremStat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 21);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 23);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 26);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 29);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 32);
	private FixedLengthStringData sqlParind = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 36);
	private FixedLengthStringData sqlBillfreq = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 37);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGoverec, 39);
	private PackedDecimalData sqlCfwdc = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 42);
	private PackedDecimalData sqlCfwdp = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 52);
	private PackedDecimalData sqlCfwds = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 62);
	private PackedDecimalData sqlCfwdi = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 72);
	private PackedDecimalData sqlCfwdl = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 82);
	private PackedDecimalData sqlCfwda = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 92);
	private PackedDecimalData sqlCfwdb = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 102);
	private PackedDecimalData sqlCfwdld = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 112);
	private PackedDecimalData sqlCfwdra = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 122);

		/* SQL-GVAHPF */
	private FixedLengthStringData sqlGvahrec = new FixedLengthStringData(251);
	private FixedLengthStringData sqlGvahChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGvahrec, 0);
	private FixedLengthStringData sqlGvahCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGvahrec, 1);
	private FixedLengthStringData sqlGvahRegister = new FixedLengthStringData(3).isAPartOf(sqlGvahrec, 3);
	private FixedLengthStringData sqlGvahSrcebus = new FixedLengthStringData(2).isAPartOf(sqlGvahrec, 6);
	private FixedLengthStringData sqlGvahStatcode = new FixedLengthStringData(2).isAPartOf(sqlGvahrec, 8);
	private FixedLengthStringData sqlGvahCnpstat = new FixedLengthStringData(2).isAPartOf(sqlGvahrec, 10);
	private FixedLengthStringData sqlGvahAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGvahrec, 12);
	private FixedLengthStringData sqlGvahCnttype = new FixedLengthStringData(3).isAPartOf(sqlGvahrec, 15);
	private PackedDecimalData sqlGvahAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGvahrec, 18);
	private PackedDecimalData sqlGvahCfwdfyp = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 21);
	private PackedDecimalData sqlGvahCfwdrnp = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 31);
	private PackedDecimalData sqlGvahCfwdspd = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 41);
	private PackedDecimalData sqlGvahCfwdfyc = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 51);
	private PackedDecimalData sqlGvahCfwdrlc = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 61);
	private PackedDecimalData sqlGvahCfwdspc = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 71);
	private PackedDecimalData sqlGvahCfwdrb = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 81);
	private PackedDecimalData sqlGvahCfwdxb = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 91);
	private PackedDecimalData sqlGvahCfwdtb = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 101);
	private PackedDecimalData sqlGvahCfwdib = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 111);
	private PackedDecimalData sqlGvahCfwddd = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 121);
	private PackedDecimalData sqlGvahCfwddi = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 131);
	private PackedDecimalData sqlGvahCfwdbs = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 141);
	private PackedDecimalData sqlGvahCfwdmtb = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 151);
	private PackedDecimalData sqlGvahCfwdob = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 161);
	private PackedDecimalData sqlGvahCfwdclr = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 171);
	private PackedDecimalData sqlGvahCfwdrip = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 181);
	private PackedDecimalData sqlGvahCfwdric = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 191);
	private PackedDecimalData sqlGvahCfwdadv = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 201);
	private PackedDecimalData sqlGvahCfwdpdb = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 211);
	private PackedDecimalData sqlGvahCfwdadb = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 221);
	private PackedDecimalData sqlGvahCfwdadj = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 231);
	private PackedDecimalData sqlGvahCfwdint = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 241);

		/* SQL-GVACPF */
	private FixedLengthStringData sqlGvacrec = new FixedLengthStringData(265);
	private FixedLengthStringData sqlGvacChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 0);
	private FixedLengthStringData sqlGvacCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 1);
	private FixedLengthStringData sqlGvacRegister = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 3);
	private FixedLengthStringData sqlGvacSrcebus = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 6);
	private FixedLengthStringData sqlGvacStfund = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 8);
	private FixedLengthStringData sqlGvacStsect = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 9);
	private FixedLengthStringData sqlGvacStssect = new FixedLengthStringData(4).isAPartOf(sqlGvacrec, 11);
	private FixedLengthStringData sqlGvacStatcode = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 15);
	private FixedLengthStringData sqlGvacCnpstat = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 17);
	private FixedLengthStringData sqlGvacCrpstat = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 19);
	private FixedLengthStringData sqlGvacAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 21);
	private FixedLengthStringData sqlGvacCnttype = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 24);
	private FixedLengthStringData sqlGvacCrtable = new FixedLengthStringData(4).isAPartOf(sqlGvacrec, 27);
	private FixedLengthStringData sqlGvacParind = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 31);
	private PackedDecimalData sqlGvacAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGvacrec, 32);
	private PackedDecimalData sqlGvacCfwdfyp = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 35);
	private PackedDecimalData sqlGvacCfwdrnp = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 45);
	private PackedDecimalData sqlGvacCfwdspd = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 55);
	private PackedDecimalData sqlGvacCfwdfyc = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 65);
	private PackedDecimalData sqlGvacCfwdrlc = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 75);
	private PackedDecimalData sqlGvacCfwdspc = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 85);
	private PackedDecimalData sqlGvacCfwdrb = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 95);
	private PackedDecimalData sqlGvacCfwdxb = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 105);
	private PackedDecimalData sqlGvacCfwdtb = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 115);
	private PackedDecimalData sqlGvacCfwdib = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 125);
	private PackedDecimalData sqlGvacCfwddd = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 135);
	private PackedDecimalData sqlGvacCfwddi = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 145);
	private PackedDecimalData sqlGvacCfwdbs = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 155);
	private PackedDecimalData sqlGvacCfwdmtb = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 165);
	private PackedDecimalData sqlGvacCfwdob = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 175);
	private PackedDecimalData sqlGvacCfwdclr = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 185);
	private PackedDecimalData sqlGvacCfwdrip = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 195);
	private PackedDecimalData sqlGvacCfwdric = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 205);
	private PackedDecimalData sqlGvacCfwdadv = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 215);
	private PackedDecimalData sqlGvacCfwdpdb = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 225);
	private PackedDecimalData sqlGvacCfwdadb = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 235);
	private PackedDecimalData sqlGvacCfwdadj = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 245);
	private PackedDecimalData sqlGvacCfwdint = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 255);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Additional Govr Statistics Accumulation*/
	private GoveTableDAM goveIO = new GoveTableDAM();
		/*Government Accumulation File by Coverage*/
	private GvacTableDAM gvacIO = new GvacTableDAM();
		/*Government Accumulation File by Contract*/
	private GvahTableDAM gvahIO = new GvahTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		getRecord1120,
		exit1190,
		getRecord1220,
		exit1290,
		getRecord1320,
		exit1390
	}

	public Bj557() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsspEdterror.set(varcom.oK);
		wsaaAcctyr.set(bsscIO.getAcctYear());
		processGovepf1100();
		processGvahpf1200();
		processGvacpf1300();
		/*EXIT*/
	}

protected void processGovepf1100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1110();
				}
				case getRecord1120: {
					getRecord1120();
				}
				case exit1190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1110()
	{
		//but 977 tom du
//		sqlgovepf = " SELECT  CHDRCOY, CNTBRANCH, REGISTER, SRCEBUS, STATCAT, STFUND, STSECT, STSSECT, CNPSTAT, CRPSTAT, ACCTCCY, CNTCURR, CNTTYPE, CRTABLE, PARIND, BILLFREQ, ACCTYR, CFWDC, CFWDP, CFWDS, CFWDI, CFWDL, CFWDA, CFWDB, CFWDLD, CFWDRA" +
		sqlgovepf = " SELECT  CHDRCOY, CNTBRANCH, REG, SRCEBUS, STATCAT, STFUND, STSECT, STSSECT, CNPSTAT, CRPSTAT, ACCTCCY, CNTCURR, CNTTYPE, CRTABLE, PARIND, BILLFREQ, ACCTYR, CFWDC, CFWDP, CFWDS, CFWDI, CFWDL, CFWDA, CFWDB, CFWDLD, CFWDRA" +
		//
		" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE ACCTYR = ?";
		sqlerrorflag = false;
		try {
			sqlgovepfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfps = appVars.prepareStatementEmbeded(sqlgovepfconn, sqlgovepf, "GOVEPF");
			appVars.setDBDouble(sqlgovepfps, 1, wsaaAcctyr.toDouble());
			sqlgovepfrs = appVars.executeQuery(sqlgovepfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void getRecord1120()
	{
		sqlerrorflag = false;
		try {
			if (sqlgovepfrs.next()) {
				appVars.getDBObject(sqlgovepfrs, 1, sqlChdrcoy);
				appVars.getDBObject(sqlgovepfrs, 2, sqlCntbranch);
				appVars.getDBObject(sqlgovepfrs, 3, sqlRegister);
				appVars.getDBObject(sqlgovepfrs, 4, sqlSrcebus);
				appVars.getDBObject(sqlgovepfrs, 5, sqlStatcat);
				appVars.getDBObject(sqlgovepfrs, 6, sqlStatFund);
				appVars.getDBObject(sqlgovepfrs, 7, sqlStatSect);
				appVars.getDBObject(sqlgovepfrs, 8, sqlStatSubsect);
				appVars.getDBObject(sqlgovepfrs, 9, sqlCnPremStat);
				appVars.getDBObject(sqlgovepfrs, 10, sqlCovPremStat);
				appVars.getDBObject(sqlgovepfrs, 11, sqlAcctccy);
				appVars.getDBObject(sqlgovepfrs, 12, sqlCntcurr);
				appVars.getDBObject(sqlgovepfrs, 13, sqlCnttype);
				appVars.getDBObject(sqlgovepfrs, 14, sqlCrtable);
				appVars.getDBObject(sqlgovepfrs, 15, sqlParind);
				appVars.getDBObject(sqlgovepfrs, 16, sqlBillfreq);
				appVars.getDBObject(sqlgovepfrs, 17, sqlAcctyr);
				appVars.getDBObject(sqlgovepfrs, 18, sqlCfwdc);
				appVars.getDBObject(sqlgovepfrs, 19, sqlCfwdp);
				appVars.getDBObject(sqlgovepfrs, 20, sqlCfwds);
				appVars.getDBObject(sqlgovepfrs, 21, sqlCfwdi);
				appVars.getDBObject(sqlgovepfrs, 22, sqlCfwdl);
				appVars.getDBObject(sqlgovepfrs, 23, sqlCfwda);
				appVars.getDBObject(sqlgovepfrs, 24, sqlCfwdb);
				appVars.getDBObject(sqlgovepfrs, 25, sqlCfwdld);
				appVars.getDBObject(sqlgovepfrs, 26, sqlCfwdra);
			}
			else {
				goTo(GotoLabel.exit1190);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		writeGovepf5000();
		goTo(GotoLabel.getRecord1120);
	}

protected void processGvahpf1200()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1210();
				}
				case getRecord1220: {
					getRecord1220();
				}
				case exit1290: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1210()
	{
		//bug 977 tom du
//		sqlgvahpf = " SELECT  CHDRCOY, CNTBRANCH, REGISTER, SRCEBUS, STATCODE, CNPSTAT, ACCTCCY, CNTTYPE, ACCTYR, CFWDFYP, CFWDRNP, CFWDSPD, CFWDFYC, CFWDRLC, CFWDSPC, CFWDRB, CFWDXB, CFWDTB, CFWDIB, CFWDDD, CFWDDI, CFWDBS, CFWDMTB, CFWDOB, CFWDCLR, CFWDRIP, CFWDRIC, CFWDADV, CFWDPDB, CFWDADB, CFWDADJ, CFWDINT" +
		sqlgvahpf = " SELECT  CHDRCOY, CNTBRANCH, REG, SRCEBUS, STATCODE, CNPSTAT, ACCTCCY, CNTTYPE, ACCTYR, CFWDFYP, CFWDRNP, CFWDSPD, CFWDFYC, CFWDRLC, CFWDSPC, CFWDRB, CFWDXB, CFWDTB, CFWDIB, CFWDDD, CFWDDI, CFWDBS, CFWDMTB, CFWDOB, CFWDCLR, CFWDRIP, CFWDRIC, CFWDADV, CFWDPDB, CFWDADB, CFWDADJ, CFWDINT" +
		//
		" FROM   " + appVars.getTableNameOverriden("GVAHPF") + " " +
" WHERE ACCTYR = ?";
		sqlerrorflag = false;
		try {
			sqlgvahpfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvahpfTableDAM());
			sqlgvahpfps = appVars.prepareStatementEmbeded(sqlgvahpfconn, sqlgvahpf, "GVAHPF");
			appVars.setDBDouble(sqlgvahpfps, 1, wsaaAcctyr.toDouble());
			sqlgvahpfrs = appVars.executeQuery(sqlgvahpfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void getRecord1220()
	{
		sqlerrorflag = false;
		try {
			if (sqlgvahpfrs.next()) {
				appVars.getDBObject(sqlgvahpfrs, 1, sqlGvahChdrcoy);
				appVars.getDBObject(sqlgvahpfrs, 2, sqlGvahCntbranch);
				appVars.getDBObject(sqlgvahpfrs, 3, sqlGvahRegister);
				appVars.getDBObject(sqlgvahpfrs, 4, sqlGvahSrcebus);
				appVars.getDBObject(sqlgvahpfrs, 5, sqlGvahStatcode);
				appVars.getDBObject(sqlgvahpfrs, 6, sqlGvahCnpstat);
				appVars.getDBObject(sqlgvahpfrs, 7, sqlGvahAcctccy);
				appVars.getDBObject(sqlgvahpfrs, 8, sqlGvahCnttype);
				appVars.getDBObject(sqlgvahpfrs, 9, sqlGvahAcctyr);
				appVars.getDBObject(sqlgvahpfrs, 10, sqlGvahCfwdfyp);
				appVars.getDBObject(sqlgvahpfrs, 11, sqlGvahCfwdrnp);
				appVars.getDBObject(sqlgvahpfrs, 12, sqlGvahCfwdspd);
				appVars.getDBObject(sqlgvahpfrs, 13, sqlGvahCfwdfyc);
				appVars.getDBObject(sqlgvahpfrs, 14, sqlGvahCfwdrlc);
				appVars.getDBObject(sqlgvahpfrs, 15, sqlGvahCfwdspc);
				appVars.getDBObject(sqlgvahpfrs, 16, sqlGvahCfwdrb);
				appVars.getDBObject(sqlgvahpfrs, 17, sqlGvahCfwdxb);
				appVars.getDBObject(sqlgvahpfrs, 18, sqlGvahCfwdtb);
				appVars.getDBObject(sqlgvahpfrs, 19, sqlGvahCfwdib);
				appVars.getDBObject(sqlgvahpfrs, 20, sqlGvahCfwddd);
				appVars.getDBObject(sqlgvahpfrs, 21, sqlGvahCfwddi);
				appVars.getDBObject(sqlgvahpfrs, 22, sqlGvahCfwdbs);
				appVars.getDBObject(sqlgvahpfrs, 23, sqlGvahCfwdmtb);
				appVars.getDBObject(sqlgvahpfrs, 24, sqlGvahCfwdob);
				appVars.getDBObject(sqlgvahpfrs, 25, sqlGvahCfwdclr);
				appVars.getDBObject(sqlgvahpfrs, 26, sqlGvahCfwdrip);
				appVars.getDBObject(sqlgvahpfrs, 27, sqlGvahCfwdric);
				appVars.getDBObject(sqlgvahpfrs, 28, sqlGvahCfwdadv);
				appVars.getDBObject(sqlgvahpfrs, 29, sqlGvahCfwdpdb);
				appVars.getDBObject(sqlgvahpfrs, 30, sqlGvahCfwdadb);
				appVars.getDBObject(sqlgvahpfrs, 31, sqlGvahCfwdadj);
				appVars.getDBObject(sqlgvahpfrs, 32, sqlGvahCfwdint);
			}
			else {
				goTo(GotoLabel.exit1290);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		writeGvahpf6000();
		goTo(GotoLabel.getRecord1220);
	}

protected void processGvacpf1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					para1310();
				}
				case getRecord1320: {
					getRecord1320();
				}
				case exit1390: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void para1310()
	{
		//bug 977 tom du
//		sqlgvacpf = " SELECT  CHDRCOY, CNTBRANCH, REGISTER, SRCEBUS, STFUND, STSECT, STSSECT, STATCODE, CNPSTAT, CRPSTAT, ACCTCCY, CNTTYPE, CRTABLE, PARIND, ACCTYR, CFWDFYP, CFWDRNP, CFWDSPD, CFWDFYC, CFWDRLC, CFWDSPC, CFWDRB, CFWDXB, CFWDTB, CFWDIB, CFWDDD, CFWDDI, CFWDBS, CFWDMTB, CFWDOB, CFWDCLR, CFWDRIP, CFWDRIC, CFWDADV, CFWDPDB, CFWDADB, CFWDADJ, CFWDINT" +
		sqlgvacpf = " SELECT  CHDRCOY, CNTBRANCH, REG, SRCEBUS, STFUND, STSECT, STSSECT, STATCODE, CNPSTAT, CRPSTAT, ACCTCCY, CNTTYPE, CRTABLE, PARIND, ACCTYR, CFWDFYP, CFWDRNP, CFWDSPD, CFWDFYC, CFWDRLC, CFWDSPC, CFWDRB, CFWDXB, CFWDTB, CFWDIB, CFWDDD, CFWDDI, CFWDBS, CFWDMTB, CFWDOB, CFWDCLR, CFWDRIP, CFWDRIC, CFWDADV, CFWDPDB, CFWDADB, CFWDADJ, CFWDINT" +
		//
		" FROM   " + appVars.getTableNameOverriden("GVACPF") + " " +
" WHERE ACCTYR = ?";
		sqlerrorflag = false;
		try {
			sqlgvacpfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvacpfTableDAM());
			sqlgvacpfps = appVars.prepareStatementEmbeded(sqlgvacpfconn, sqlgvacpf, "GVACPF");
			appVars.setDBDouble(sqlgvacpfps, 1, wsaaAcctyr.toDouble());
			sqlgvacpfrs = appVars.executeQuery(sqlgvacpfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void getRecord1320()
	{
		sqlerrorflag = false;
		try {
			if (sqlgvacpfrs.next()) {
				appVars.getDBObject(sqlgvacpfrs, 1, sqlGvacChdrcoy);
				appVars.getDBObject(sqlgvacpfrs, 2, sqlGvacCntbranch);
				appVars.getDBObject(sqlgvacpfrs, 3, sqlGvacRegister);
				appVars.getDBObject(sqlgvacpfrs, 4, sqlGvacSrcebus);
				appVars.getDBObject(sqlgvacpfrs, 5, sqlGvacStfund);
				appVars.getDBObject(sqlgvacpfrs, 6, sqlGvacStsect);
				appVars.getDBObject(sqlgvacpfrs, 7, sqlGvacStssect);
				appVars.getDBObject(sqlgvacpfrs, 8, sqlGvacStatcode);
				appVars.getDBObject(sqlgvacpfrs, 9, sqlGvacCnpstat);
				appVars.getDBObject(sqlgvacpfrs, 10, sqlGvacCrpstat);
				appVars.getDBObject(sqlgvacpfrs, 11, sqlGvacAcctccy);
				appVars.getDBObject(sqlgvacpfrs, 12, sqlGvacCnttype);
				appVars.getDBObject(sqlgvacpfrs, 13, sqlGvacCrtable);
				appVars.getDBObject(sqlgvacpfrs, 14, sqlGvacParind);
				appVars.getDBObject(sqlgvacpfrs, 15, sqlGvacAcctyr);
				appVars.getDBObject(sqlgvacpfrs, 16, sqlGvacCfwdfyp);
				appVars.getDBObject(sqlgvacpfrs, 17, sqlGvacCfwdrnp);
				appVars.getDBObject(sqlgvacpfrs, 18, sqlGvacCfwdspd);
				appVars.getDBObject(sqlgvacpfrs, 19, sqlGvacCfwdfyc);
				appVars.getDBObject(sqlgvacpfrs, 20, sqlGvacCfwdrlc);
				appVars.getDBObject(sqlgvacpfrs, 21, sqlGvacCfwdspc);
				appVars.getDBObject(sqlgvacpfrs, 22, sqlGvacCfwdrb);
				appVars.getDBObject(sqlgvacpfrs, 23, sqlGvacCfwdxb);
				appVars.getDBObject(sqlgvacpfrs, 24, sqlGvacCfwdtb);
				appVars.getDBObject(sqlgvacpfrs, 25, sqlGvacCfwdib);
				appVars.getDBObject(sqlgvacpfrs, 26, sqlGvacCfwddd);
				appVars.getDBObject(sqlgvacpfrs, 27, sqlGvacCfwddi);
				appVars.getDBObject(sqlgvacpfrs, 28, sqlGvacCfwdbs);
				appVars.getDBObject(sqlgvacpfrs, 29, sqlGvacCfwdmtb);
				appVars.getDBObject(sqlgvacpfrs, 30, sqlGvacCfwdob);
				appVars.getDBObject(sqlgvacpfrs, 31, sqlGvacCfwdclr);
				appVars.getDBObject(sqlgvacpfrs, 32, sqlGvacCfwdrip);
				appVars.getDBObject(sqlgvacpfrs, 33, sqlGvacCfwdric);
				appVars.getDBObject(sqlgvacpfrs, 34, sqlGvacCfwdadv);
				appVars.getDBObject(sqlgvacpfrs, 35, sqlGvacCfwdpdb);
				appVars.getDBObject(sqlgvacpfrs, 36, sqlGvacCfwdadb);
				appVars.getDBObject(sqlgvacpfrs, 37, sqlGvacCfwdadj);
				appVars.getDBObject(sqlgvacpfrs, 38, sqlGvacCfwdint);
			}
			else {
				goTo(GotoLabel.exit1390);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct05);
		contotrec.totval.set(1);
		callContot001();
		writeGvacpf7000();
		goTo(GotoLabel.getRecord1320);
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		wsspEdterror.set(varcom.endp);
		/*EXIT*/
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		/*EXIT*/
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlgovepfconn, sqlgovepfps, sqlgovepfrs);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void writeGovepf5000()
	{
		para5010();
	}

protected void para5010()
	{
		goveIO.setChdrcoy(sqlChdrcoy);
		goveIO.setCntbranch(sqlCntbranch);
		goveIO.setRegister(sqlRegister);
		goveIO.setSrcebus(sqlSrcebus);
		goveIO.setStatcat(sqlStatcat);
		goveIO.setStatFund(sqlStatFund);
		goveIO.setStatSect(sqlStatSect);
		goveIO.setStatSubsect(sqlStatSubsect);
		goveIO.setCnPremStat(sqlCnPremStat);
		goveIO.setCovPremStat(sqlCovPremStat);
		goveIO.setAcctccy(sqlAcctccy);
		goveIO.setCntcurr(sqlCntcurr);
		goveIO.setCnttype(sqlCnttype);
		goveIO.setCrtable(sqlCrtable);
		goveIO.setParind(sqlParind);
		goveIO.setBillfreq(sqlBillfreq);
		setPrecision(goveIO.getAcctyr(), 0);
		goveIO.setAcctyr(add(bsscIO.getAcctYear(),1));
		goveIO.setBfwdc(sqlCfwdc);
		goveIO.setCfwdc(sqlCfwdc);
		goveIO.setBfwdp(sqlCfwdp);
		goveIO.setCfwdp(sqlCfwdp);
		goveIO.setBfwds(sqlCfwds);
		goveIO.setCfwds(sqlCfwds);
		goveIO.setBfwdi(sqlCfwdi);
		goveIO.setCfwdi(sqlCfwdi);
		goveIO.setBfwdl(sqlCfwdl);
		goveIO.setCfwdl(sqlCfwdl);
		goveIO.setBfwda(sqlCfwda);
		goveIO.setCfwda(sqlCfwda);
		goveIO.setBfwdb(sqlCfwdb);
		goveIO.setCfwdb(sqlCfwdb);
		goveIO.setBfwdld(sqlCfwdld);
		goveIO.setCfwdld(sqlCfwdld);
		goveIO.setBfwdra(sqlCfwdra);
		goveIO.setCfwdra(sqlCfwdra);
		for (ix.set(1); !(isGT(ix,12)); ix.add(1)){
			goveIO.setStcmth(ix, ZERO);
			goveIO.setStpmth(ix, ZERO);
			goveIO.setStsmth(ix, ZERO);
			goveIO.setStimth(ix, ZERO);
			goveIO.setStlmth(ix, ZERO);
			goveIO.setStamth(ix, ZERO);
			goveIO.setStbmthg(ix, ZERO);
			goveIO.setStldmthg(ix, ZERO);
			goveIO.setStraamt(ix, ZERO);
		}
		goveIO.setFunction(varcom.writr);
		goveIO.setFormat(goverec);
		SmartFileCode.execute(appVars, goveIO);
		if (isNE(goveIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(goveIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void writeGvahpf6000()
	{
		para6010();
	}

protected void para6010()
	{
		gvahIO.setChdrcoy(sqlGvahChdrcoy);
		gvahIO.setCntbranch(sqlGvahCntbranch);
		gvahIO.setRegister(sqlGvahRegister);
		gvahIO.setSrcebus(sqlGvahSrcebus);
		gvahIO.setStatcode(sqlGvahStatcode);
		gvahIO.setCnPremStat(sqlGvahCnpstat);
		gvahIO.setAcctccy(sqlGvahAcctccy);
		gvahIO.setCnttype(sqlGvahCnttype);
		setPrecision(gvahIO.getAcctyr(), 0);
		gvahIO.setAcctyr(add(bsscIO.getAcctYear(),1));
		gvahIO.setBfwdfyp(sqlGvahCfwdfyp);
		gvahIO.setCfwdfyp(sqlGvahCfwdfyp);
		gvahIO.setBfwdrnp(sqlGvahCfwdrnp);
		gvahIO.setCfwdrnp(sqlGvahCfwdrnp);
		gvahIO.setBfwdspd(sqlGvahCfwdspd);
		gvahIO.setCfwdspd(sqlGvahCfwdspd);
		gvahIO.setBfwdfyc(sqlGvahCfwdfyc);
		gvahIO.setCfwdfyc(sqlGvahCfwdfyc);
		gvahIO.setBfwdrlc(sqlGvahCfwdrlc);
		gvahIO.setCfwdrlc(sqlGvahCfwdrlc);
		gvahIO.setBfwdspc(sqlGvahCfwdspc);
		gvahIO.setCfwdspc(sqlGvahCfwdspc);
		gvahIO.setBfwdrb(sqlGvahCfwdrb);
		gvahIO.setCfwdrb(sqlGvahCfwdrb);
		gvahIO.setBfwdxb(sqlGvahCfwdxb);
		gvahIO.setCfwdxb(sqlGvahCfwdxb);
		gvahIO.setBfwdtb(sqlGvahCfwdtb);
		gvahIO.setCfwdtb(sqlGvahCfwdtb);
		gvahIO.setBfwdib(sqlGvahCfwdib);
		gvahIO.setCfwdib(sqlGvahCfwdib);
		gvahIO.setBfwddd(sqlGvahCfwddd);
		gvahIO.setCfwddd(sqlGvahCfwddd);
		gvahIO.setBfwddi(sqlGvahCfwddi);
		gvahIO.setCfwddi(sqlGvahCfwddi);
		gvahIO.setBfwdbs(sqlGvahCfwdbs);
		gvahIO.setCfwdbs(sqlGvahCfwdbs);
		gvahIO.setBfwdmtb(sqlGvahCfwdmtb);
		gvahIO.setCfwdmtb(sqlGvahCfwdmtb);
		gvahIO.setBfwdob(sqlGvahCfwdob);
		gvahIO.setCfwdob(sqlGvahCfwdob);
		gvahIO.setBfwdclr(sqlGvahCfwdclr);
		gvahIO.setCfwdclr(sqlGvahCfwdclr);
		gvahIO.setBfwdrip(sqlGvahCfwdrip);
		gvahIO.setCfwdrip(sqlGvahCfwdrip);
		gvahIO.setBfwdric(sqlGvahCfwdric);
		gvahIO.setCfwdric(sqlGvahCfwdric);
		gvahIO.setBfwdadv(sqlGvahCfwdadv);
		gvahIO.setCfwdadv(sqlGvahCfwdadv);
		gvahIO.setBfwdpdb(sqlGvahCfwdpdb);
		gvahIO.setCfwdpdb(sqlGvahCfwdpdb);
		gvahIO.setBfwdadb(sqlGvahCfwdadb);
		gvahIO.setCfwdadb(sqlGvahCfwdadb);
		gvahIO.setBfwdadj(sqlGvahCfwdadj);
		gvahIO.setCfwdadj(sqlGvahCfwdadj);
		gvahIO.setBfwdint(sqlGvahCfwdint);
		gvahIO.setCfwdint(sqlGvahCfwdint);
		for (ix.set(1); !(isGT(ix,12)); ix.add(1)){
			gvahIO.setStaccfyp(ix, ZERO);
			gvahIO.setStaccrnp(ix, ZERO);
			gvahIO.setStaccspd(ix, ZERO);
			gvahIO.setStaccfyc(ix, ZERO);
			gvahIO.setStaccrlc(ix, ZERO);
			gvahIO.setStaccspc(ix, ZERO);
			gvahIO.setStaccrb(ix, ZERO);
			gvahIO.setStaccxb(ix, ZERO);
			gvahIO.setStacctb(ix, ZERO);
			gvahIO.setStaccib(ix, ZERO);
			gvahIO.setStaccdd(ix, ZERO);
			gvahIO.setStaccdi(ix, ZERO);
			gvahIO.setStaccbs(ix, ZERO);
			gvahIO.setStaccmtb(ix, ZERO);
			gvahIO.setStaccob(ix, ZERO);
			gvahIO.setStaccclr(ix, ZERO);
			gvahIO.setStaccrip(ix, ZERO);
			gvahIO.setStaccric(ix, ZERO);
			gvahIO.setStaccadv(ix, ZERO);
			gvahIO.setStaccpdb(ix, ZERO);
			gvahIO.setStaccadb(ix, ZERO);
			gvahIO.setStaccadj(ix, ZERO);
			gvahIO.setStaccint(ix, ZERO);
		}
		gvahIO.setFunction(varcom.writr);
		gvahIO.setFormat(gvahrec);
		SmartFileCode.execute(appVars, gvahIO);
		if (isNE(gvahIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvahIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct04);
		contotrec.totval.set(1);
		callContot001();
	}

protected void writeGvacpf7000()
	{
		para7010();
	}

protected void para7010()
	{
		gvacIO.setChdrcoy(sqlGvacChdrcoy);
		gvacIO.setCntbranch(sqlGvacCntbranch);
		gvacIO.setRegister(sqlGvacRegister);
		gvacIO.setSrcebus(sqlGvacSrcebus);
		gvacIO.setStatcode(sqlGvacStatcode);
		gvacIO.setCnPremStat(sqlGvacCnpstat);
		gvacIO.setCovPremStat(sqlGvacCrpstat);
		gvacIO.setAcctccy(sqlGvacAcctccy);
		gvacIO.setCnttype(sqlGvacCnttype);
		gvacIO.setCrtable(sqlGvacCrtable);
		gvacIO.setStatFund(sqlGvacStfund);
		gvacIO.setStatSect(sqlGvacStsect);
		gvacIO.setStatSubsect(sqlGvacStssect);
		gvacIO.setParind(sqlGvacParind);
		setPrecision(gvacIO.getAcctyr(), 0);
		gvacIO.setAcctyr(add(bsscIO.getAcctYear(),1));
		gvacIO.setBfwdfyp(sqlGvacCfwdfyp);
		gvacIO.setCfwdfyp(sqlGvacCfwdfyp);
		gvacIO.setBfwdrnp(sqlGvacCfwdrnp);
		gvacIO.setCfwdrnp(sqlGvacCfwdrnp);
		gvacIO.setBfwdspd(sqlGvacCfwdspd);
		gvacIO.setCfwdspd(sqlGvacCfwdspd);
		gvacIO.setBfwdfyc(sqlGvacCfwdfyc);
		gvacIO.setCfwdfyc(sqlGvacCfwdfyc);
		gvacIO.setBfwdrlc(sqlGvacCfwdrlc);
		gvacIO.setCfwdrlc(sqlGvacCfwdrlc);
		gvacIO.setBfwdspc(sqlGvacCfwdspc);
		gvacIO.setCfwdspc(sqlGvacCfwdspc);
		gvacIO.setBfwdrb(sqlGvacCfwdrb);
		gvacIO.setCfwdrb(sqlGvacCfwdrb);
		gvacIO.setBfwdxb(sqlGvacCfwdxb);
		gvacIO.setCfwdxb(sqlGvacCfwdxb);
		gvacIO.setBfwdtb(sqlGvacCfwdtb);
		gvacIO.setCfwdtb(sqlGvacCfwdtb);
		gvacIO.setBfwdib(sqlGvacCfwdib);
		gvacIO.setCfwdib(sqlGvacCfwdib);
		gvacIO.setBfwddd(sqlGvacCfwddd);
		gvacIO.setCfwddd(sqlGvacCfwddd);
		gvacIO.setBfwddi(sqlGvacCfwddi);
		gvacIO.setCfwddi(sqlGvacCfwddi);
		gvacIO.setBfwdbs(sqlGvacCfwdbs);
		gvacIO.setCfwdbs(sqlGvacCfwdbs);
		gvacIO.setBfwdmtb(sqlGvacCfwdmtb);
		gvacIO.setCfwdmtb(sqlGvacCfwdmtb);
		gvacIO.setBfwdob(sqlGvacCfwdob);
		gvacIO.setCfwdob(sqlGvacCfwdob);
		gvacIO.setBfwdclr(sqlGvacCfwdclr);
		gvacIO.setCfwdclr(sqlGvacCfwdclr);
		gvacIO.setBfwdrip(sqlGvacCfwdrip);
		gvacIO.setCfwdrip(sqlGvacCfwdrip);
		gvacIO.setBfwdric(sqlGvacCfwdric);
		gvacIO.setCfwdric(sqlGvacCfwdric);
		gvacIO.setBfwdadv(sqlGvacCfwdadv);
		gvacIO.setCfwdadv(sqlGvacCfwdadv);
		gvacIO.setBfwdpdb(sqlGvacCfwdpdb);
		gvacIO.setCfwdpdb(sqlGvacCfwdpdb);
		gvacIO.setBfwdadb(sqlGvacCfwdadb);
		gvacIO.setCfwdadb(sqlGvacCfwdadb);
		gvacIO.setBfwdadj(sqlGvacCfwdadj);
		gvacIO.setCfwdadj(sqlGvacCfwdadj);
		gvacIO.setBfwdint(sqlGvacCfwdint);
		gvacIO.setCfwdint(sqlGvacCfwdint);
		for (ix.set(1); !(isGT(ix,12)); ix.add(1)){
			gvacIO.setStaccfyp(ix, ZERO);
			gvacIO.setStaccrnp(ix, ZERO);
			gvacIO.setStaccspd(ix, ZERO);
			gvacIO.setStaccfyc(ix, ZERO);
			gvacIO.setStaccrlc(ix, ZERO);
			gvacIO.setStaccspc(ix, ZERO);
			gvacIO.setStaccrb(ix, ZERO);
			gvacIO.setStaccxb(ix, ZERO);
			gvacIO.setStacctb(ix, ZERO);
			gvacIO.setStaccib(ix, ZERO);
			gvacIO.setStaccdd(ix, ZERO);
			gvacIO.setStaccdi(ix, ZERO);
			gvacIO.setStaccbs(ix, ZERO);
			gvacIO.setStaccmtb(ix, ZERO);
			gvacIO.setStaccob(ix, ZERO);
			gvacIO.setStaccclr(ix, ZERO);
			gvacIO.setStaccrip(ix, ZERO);
			gvacIO.setStaccric(ix, ZERO);
			gvacIO.setStaccadv(ix, ZERO);
			gvacIO.setStaccpdb(ix, ZERO);
			gvacIO.setStaccadb(ix, ZERO);
			gvacIO.setStaccadj(ix, ZERO);
			gvacIO.setStaccint(ix, ZERO);
		}
		gvacIO.setFunction(varcom.writr);
		gvacIO.setFormat(gvacrec);
		SmartFileCode.execute(appVars, gvacIO);
		if (isNE(gvacIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvacIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct06);
		contotrec.totval.set(1);
		callContot001();
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
