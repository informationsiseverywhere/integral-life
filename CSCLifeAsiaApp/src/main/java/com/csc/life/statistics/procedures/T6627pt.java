/*
 * File: T6627pt.java
 * Date: 30 August 2009 2:27:40
 * Author: Quipoz Limited
 * 
 * Class transformed from T6627PT.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;

import com.csc.life.statistics.tablestructures.T6627rec;
import com.csc.smart.procedures.Tabuff;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.tablestructures.Tablistrec;
import com.csc.smart.tablestructures.Tabuffrec;
import com.csc.smart400framework.utility.Datcon1;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*REMARKS.
*
*        PRINT SUBROUTINE FOR T6627.
*
*
*****************************************************************
* </pre>
*/
public class T6627pt extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private String oK = "****";

	private FixedLengthStringData wsaaPrtLine001 = new FixedLengthStringData(76);
	private FixedLengthStringData filler1 = new FixedLengthStringData(28).isAPartOf(wsaaPrtLine001, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler2 = new FixedLengthStringData(48).isAPartOf(wsaaPrtLine001, 28, FILLER).init("Statistics Accumulation by Band            S6627");

	private FixedLengthStringData wsaaPrtLine002 = new FixedLengthStringData(80);
	private FixedLengthStringData filler3 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine002, 0, FILLER).init("   Company:");
	private FixedLengthStringData fieldNo001 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine002, 13);
	private FixedLengthStringData filler4 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 14, FILLER).init("  Table:");
	private FixedLengthStringData fieldNo002 = new FixedLengthStringData(5).isAPartOf(wsaaPrtLine002, 24);
	private FixedLengthStringData filler5 = new FixedLengthStringData(10).isAPartOf(wsaaPrtLine002, 29, FILLER).init("   Item:");
	private FixedLengthStringData fieldNo003 = new FixedLengthStringData(8).isAPartOf(wsaaPrtLine002, 39);
	private FixedLengthStringData filler6 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine002, 47, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo004 = new FixedLengthStringData(30).isAPartOf(wsaaPrtLine002, 50);

	private FixedLengthStringData wsaaPrtLine003 = new FixedLengthStringData(61);
	private FixedLengthStringData filler7 = new FixedLengthStringData(23).isAPartOf(wsaaPrtLine003, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler8 = new FixedLengthStringData(38).isAPartOf(wsaaPrtLine003, 23, FILLER).init("From              To        Band Label");

	private FixedLengthStringData wsaaPrtLine004 = new FixedLengthStringData(56);
	private FixedLengthStringData filler9 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine004, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler10 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine004, 13, FILLER).init("1");
	private ZonedDecimalData fieldNo005 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine004, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler11 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine004, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo006 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine004, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler12 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine004, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo007 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine004, 54);

	private FixedLengthStringData wsaaPrtLine005 = new FixedLengthStringData(56);
	private FixedLengthStringData filler13 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine005, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler14 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine005, 13, FILLER).init("2");
	private ZonedDecimalData fieldNo008 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine005, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler15 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine005, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo009 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine005, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler16 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine005, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo010 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine005, 54);

	private FixedLengthStringData wsaaPrtLine006 = new FixedLengthStringData(56);
	private FixedLengthStringData filler17 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine006, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler18 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine006, 13, FILLER).init("3");
	private ZonedDecimalData fieldNo011 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine006, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler19 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine006, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo012 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine006, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler20 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine006, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo013 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine006, 54);

	private FixedLengthStringData wsaaPrtLine007 = new FixedLengthStringData(56);
	private FixedLengthStringData filler21 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine007, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler22 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine007, 13, FILLER).init("4");
	private ZonedDecimalData fieldNo014 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine007, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler23 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine007, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo015 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine007, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler24 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine007, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo016 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine007, 54);

	private FixedLengthStringData wsaaPrtLine008 = new FixedLengthStringData(56);
	private FixedLengthStringData filler25 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine008, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler26 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine008, 13, FILLER).init("5");
	private ZonedDecimalData fieldNo017 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine008, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler27 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine008, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo018 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine008, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler28 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine008, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo019 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine008, 54);

	private FixedLengthStringData wsaaPrtLine009 = new FixedLengthStringData(56);
	private FixedLengthStringData filler29 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine009, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler30 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine009, 13, FILLER).init("6");
	private ZonedDecimalData fieldNo020 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine009, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler31 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine009, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo021 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine009, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler32 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine009, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo022 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine009, 54);

	private FixedLengthStringData wsaaPrtLine010 = new FixedLengthStringData(56);
	private FixedLengthStringData filler33 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine010, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler34 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine010, 13, FILLER).init("7");
	private ZonedDecimalData fieldNo023 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine010, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler35 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine010, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo024 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine010, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler36 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine010, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo025 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine010, 54);

	private FixedLengthStringData wsaaPrtLine011 = new FixedLengthStringData(56);
	private FixedLengthStringData filler37 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine011, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler38 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine011, 13, FILLER).init("8");
	private ZonedDecimalData fieldNo026 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine011, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler39 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine011, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo027 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine011, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler40 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine011, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo028 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine011, 54);

	private FixedLengthStringData wsaaPrtLine012 = new FixedLengthStringData(56);
	private FixedLengthStringData filler41 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine012, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler42 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine012, 13, FILLER).init("9");
	private ZonedDecimalData fieldNo029 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine012, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler43 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine012, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo030 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine012, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler44 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine012, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo031 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine012, 54);

	private FixedLengthStringData wsaaPrtLine013 = new FixedLengthStringData(56);
	private FixedLengthStringData filler45 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine013, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler46 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine013, 13, FILLER).init("10");
	private ZonedDecimalData fieldNo032 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine013, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler47 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine013, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo033 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine013, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler48 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine013, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo034 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine013, 54);

	private FixedLengthStringData wsaaPrtLine014 = new FixedLengthStringData(56);
	private FixedLengthStringData filler49 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine014, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler50 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine014, 13, FILLER).init("11");
	private ZonedDecimalData fieldNo035 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine014, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler51 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine014, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo036 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine014, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler52 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine014, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo037 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine014, 54);

	private FixedLengthStringData wsaaPrtLine015 = new FixedLengthStringData(56);
	private FixedLengthStringData filler53 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine015, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler54 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine015, 13, FILLER).init("12");
	private ZonedDecimalData fieldNo038 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine015, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler55 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine015, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo039 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine015, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler56 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine015, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo040 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine015, 54);

	private FixedLengthStringData wsaaPrtLine016 = new FixedLengthStringData(56);
	private FixedLengthStringData filler57 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine016, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler58 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine016, 13, FILLER).init("13");
	private ZonedDecimalData fieldNo041 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine016, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler59 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine016, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo042 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine016, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler60 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine016, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo043 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine016, 54);

	private FixedLengthStringData wsaaPrtLine017 = new FixedLengthStringData(56);
	private FixedLengthStringData filler61 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine017, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler62 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine017, 13, FILLER).init("14");
	private ZonedDecimalData fieldNo044 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine017, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler63 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine017, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo045 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine017, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler64 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine017, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo046 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine017, 54);

	private FixedLengthStringData wsaaPrtLine018 = new FixedLengthStringData(56);
	private FixedLengthStringData filler65 = new FixedLengthStringData(13).isAPartOf(wsaaPrtLine018, 0, FILLER).init(SPACES);
	private FixedLengthStringData filler66 = new FixedLengthStringData(7).isAPartOf(wsaaPrtLine018, 13, FILLER).init("15");
	private ZonedDecimalData fieldNo047 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine018, 20).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler67 = new FixedLengthStringData(1).isAPartOf(wsaaPrtLine018, 35, FILLER).init(SPACES);
	private ZonedDecimalData fieldNo048 = new ZonedDecimalData(15, 0).isAPartOf(wsaaPrtLine018, 36).setPattern("ZZZZZZZZZZZZZZZ");
	private FixedLengthStringData filler68 = new FixedLengthStringData(3).isAPartOf(wsaaPrtLine018, 51, FILLER).init(SPACES);
	private FixedLengthStringData fieldNo049 = new FixedLengthStringData(2).isAPartOf(wsaaPrtLine018, 54);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T6627rec t6627rec = new T6627rec();
	private Tablistrec tablistrec = new Tablistrec();
	private Tabuffrec tabuffrec = new Tabuffrec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		endUp090
	}

	public T6627pt() {
		super();
	}

public void mainline(Object... parmArray)
	{
		tablistrec.tablistRec = convertAndSetParam(tablistrec.tablistRec, parmArray, 0);
		try {
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline000()
	{
		try {
			mainline010();
			moveFields020();
			writeOutputLines030();
		}
		catch (GOTOException e){
		}
		finally{
			endUp090();
		}
	}

protected void mainline010()
	{
		tabuffrec.function.set("ADD");
		tablistrec.statuz.set(oK);
		datcon1rec.function.set("CONV");
	}

protected void moveFields020()
	{
		t6627rec.t6627Rec.set(tablistrec.generalArea);
		fieldNo001.set(tablistrec.company);
		fieldNo002.set(tablistrec.tabl);
		fieldNo003.set(tablistrec.item);
		fieldNo007.set(t6627rec.statband01);
		fieldNo010.set(t6627rec.statband02);
		fieldNo013.set(t6627rec.statband03);
		fieldNo016.set(t6627rec.statband04);
		fieldNo019.set(t6627rec.statband05);
		fieldNo022.set(t6627rec.statband06);
		fieldNo025.set(t6627rec.statband07);
		fieldNo028.set(t6627rec.statband08);
		fieldNo031.set(t6627rec.statband09);
		fieldNo034.set(t6627rec.statband10);
		fieldNo037.set(t6627rec.statband11);
		fieldNo040.set(t6627rec.statband12);
		fieldNo043.set(t6627rec.statband13);
		fieldNo046.set(t6627rec.statband14);
		fieldNo049.set(t6627rec.statband15);
		fieldNo004.set(tablistrec.longdesc);
		fieldNo005.set(t6627rec.statfrom01);
		fieldNo008.set(t6627rec.statfrom02);
		fieldNo011.set(t6627rec.statfrom03);
		fieldNo014.set(t6627rec.statfrom04);
		fieldNo017.set(t6627rec.statfrom05);
		fieldNo020.set(t6627rec.statfrom06);
		fieldNo023.set(t6627rec.statfrom07);
		fieldNo026.set(t6627rec.statfrom08);
		fieldNo029.set(t6627rec.statfrom09);
		fieldNo032.set(t6627rec.statfrom10);
		fieldNo035.set(t6627rec.statfrom11);
		fieldNo038.set(t6627rec.statfrom12);
		fieldNo041.set(t6627rec.statfrom13);
		fieldNo044.set(t6627rec.statfrom14);
		fieldNo047.set(t6627rec.statfrom15);
		fieldNo006.set(t6627rec.statto01);
		fieldNo009.set(t6627rec.statto02);
		fieldNo012.set(t6627rec.statto03);
		fieldNo015.set(t6627rec.statto04);
		fieldNo018.set(t6627rec.statto05);
		fieldNo021.set(t6627rec.statto06);
		fieldNo024.set(t6627rec.statto07);
		fieldNo027.set(t6627rec.statto08);
		fieldNo030.set(t6627rec.statto09);
		fieldNo033.set(t6627rec.statto10);
		fieldNo036.set(t6627rec.statto11);
		fieldNo039.set(t6627rec.statto12);
		fieldNo042.set(t6627rec.statto13);
		fieldNo045.set(t6627rec.statto14);
		fieldNo048.set(t6627rec.statto15);
	}

protected void writeOutputLines030()
	{
		tabuffrec.bufferLine.set(wsaaPrtLine001);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine002);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine003);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine004);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine005);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine006);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine007);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine008);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine009);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine010);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine011);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine012);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine013);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine014);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine015);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine016);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine017);
		writeRecord040();
		tabuffrec.bufferLine.set(wsaaPrtLine018);
		writeRecord040();
		goTo(GotoLabel.endUp090);
	}

protected void writeRecord040()
	{
		callProgram(Tabuff.class, tabuffrec.tabuffRec);
	}

protected void endUp090()
	{
		exitProgram();
	}

protected void callDatcon1100()
	{
		/*CALL-DATCON1*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/*EXIT*/
	}
}
