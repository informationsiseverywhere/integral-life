package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6702
 * @version 1.0 generated on 30/08/09 06:58
 * @author Quipoz
 */
public class S6702ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(545);
	public FixedLengthStringData dataFields = new FixedLengthStringData(161).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctyr = DD.acctyr.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acmn = DD.acmn.copyToZonedDecimal().isAPartOf(dataFields,4);
	public ZonedDecimalData acyr = DD.acyr.copyToZonedDecimal().isAPartOf(dataFields,6);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,20);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(dataFields,29);
	public FixedLengthStringData chdrtype = DD.chdrtype.copy().isAPartOf(dataFields,31);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,34);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,44);
	public FixedLengthStringData mthldesc = DD.mthldesc.copy().isAPartOf(dataFields,74);
	public FixedLengthStringData ovrdcat = DD.ovrdcat.copy().isAPartOf(dataFields,84);
	public FixedLengthStringData pstatcd = DD.pstatcd.copy().isAPartOf(dataFields,85);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(dataFields,87);
	public ZonedDecimalData stcmth = DD.stcmth.copyToZonedDecimal().isAPartOf(dataFields,89);
	public ZonedDecimalData stmmth = DD.stmmth.copyToZonedDecimal().isAPartOf(dataFields,98);
	public ZonedDecimalData stpmth = DD.stpmth.copyToZonedDecimal().isAPartOf(dataFields,116);
	public ZonedDecimalData stsmth = DD.stsmth.copyToZonedDecimal().isAPartOf(dataFields,134);
	public ZonedDecimalData stvmth = DD.stvmth.copyToZonedDecimal().isAPartOf(dataFields,152);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(96).isAPartOf(dataArea, 161);
	public FixedLengthStringData acctyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acmnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData acyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bandageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bandprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData bandsaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData bandtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData chdrtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData mthldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData ovrdcatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData pstatcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData statcatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData stcmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData stmmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData stpmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData stsmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData stvmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(288).isAPartOf(dataArea, 257);
	public FixedLengthStringData[] acctyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acmnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] acyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bandageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bandprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] bandsaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] bandtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] mthldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] ovrdcatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] pstatcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] statcatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] stcmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] stmmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] stpmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] stsmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] stvmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6702screenWritten = new LongData(0);
	public LongData S6702protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6702ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(acctyrOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(descripOut,new String[] {null, null, null, "14",null, null, null, null, null, null, null, null});
		fieldIndMap.put(acmnOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acyrOut,new String[] {null, null, null, "14",null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, agntsel, statcat, acctyr, aracde, cntbranch, bandage, bandsa, bandprm, bandtrm, chdrtype, crtable, cntcurr, ovrdcat, descrip, acmn, pstatcd, mthldesc, acyr, stcmth, stvmth, stpmth, stsmth, stmmth};
		screenOutFields = new BaseData[][] {companyOut, agntselOut, statcatOut, acctyrOut, aracdeOut, cntbranchOut, bandageOut, bandsaOut, bandprmOut, bandtrmOut, chdrtypeOut, crtableOut, cntcurrOut, ovrdcatOut, descripOut, acmnOut, pstatcdOut, mthldescOut, acyrOut, stcmthOut, stvmthOut, stpmthOut, stsmthOut, stmmthOut};
		screenErrFields = new BaseData[] {companyErr, agntselErr, statcatErr, acctyrErr, aracdeErr, cntbranchErr, bandageErr, bandsaErr, bandprmErr, bandtrmErr, chdrtypeErr, crtableErr, cntcurrErr, ovrdcatErr, descripErr, acmnErr, pstatcdErr, mthldescErr, acyrErr, stcmthErr, stvmthErr, stpmthErr, stsmthErr, stmmthErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6702screen.class;
		protectRecord = S6702protect.class;
	}

}
