/*********************  */
/*Author  :Liwei		   */
/*Purpose :Model foe Agprpf*/
/*Date    :2018.11.24	   */
package com.csc.life.statistics.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.life.statistics.dataaccess.dao.AgprpfDAO;
import com.csc.life.statistics.dataaccess.model.Agprpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class AgprpfDAOImpl extends BaseDAOImpl<Agprpf> implements AgprpfDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(AgprpfDAOImpl.class);
    public void insertAgprpfRecord(Agprpf agprpf){
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO AGPRPF(AGNTCOY,AGNTNUM,ACCTYR,MNTH,CHDRNUM,CNTTYPE,EFFDATE,");
        sb.append(" MLPERPP01,MLPERPP02,MLPERPP03,MLPERPP04,MLPERPP05,MLPERPP06,MLPERPP07,MLPERPP08,MLPERPP09,MLPERPP10");
        sb.append(",MLPERPC01,MLPERPC02,MLPERPC03,MLPERPC04,MLPERPC05,MLPERPC06,MLPERPC07,MLPERPC08,MLPERPC09,MLPERPC10");
        sb.append(",MLDIRPP01,MLDIRPP02,MLDIRPP03,MLDIRPP04,MLDIRPP05,MLDIRPP06,MLDIRPP07,MLDIRPP08,MLDIRPP09,MLDIRPP10");
        sb.append(",MLDIRPC01,MLDIRPC02,MLDIRPC03,MLDIRPC04,MLDIRPC05,MLDIRPC06,MLDIRPC07,MLDIRPC08,MLDIRPC09,MLDIRPC10");
        sb.append(",MLGRPPP01,MLGRPPP02,MLGRPPP03,MLGRPPP04,MLGRPPP05,MLGRPPP06,MLGRPPP07,MLGRPPP08,MLGRPPP09,MLGRPPP10");
        sb.append(",MLGRPPC01,MLGRPPC02,MLGRPPC03,MLGRPPC04,MLGRPPC05,MLGRPPC06,MLGRPPC07,MLGRPPC08,MLGRPPC09,MLGRPPC10");
        sb.append(",CNTCOUNT,SUMINS,USRPRF,JOBNM,DATIME)");
        sb.append(" values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        PreparedStatement ps = getPrepareStatement(sb.toString());
        try {
            ps.setString(1, agprpf.getAgntcoy());
            ps.setString(2, agprpf.getAgntnum());
            ps.setInt(3, agprpf.getAcctyr());
            ps.setInt(4, agprpf.getMnth());
            ps.setString(5, agprpf.getChdrnum());
            ps.setString(6, agprpf.getCnttype());
            ps.setInt(7, agprpf.getEffdate());
            ps.setFloat(8, agprpf.getMlperpp01());
            ps.setFloat(9, agprpf.getMlperpp02());
            ps.setFloat(10, agprpf.getMlperpp03());
            ps.setFloat(11, agprpf.getMlperpp04());
            ps.setFloat(12, agprpf.getMlperpp05());
            ps.setFloat(13, agprpf.getMlperpp06());
            ps.setFloat(14, agprpf.getMlperpp07());
            ps.setFloat(15, agprpf.getMlperpp08());
            ps.setFloat(16, agprpf.getMlperpp09());
            ps.setFloat(17, agprpf.getMlperpp10());
            ps.setFloat(18, agprpf.getMlperpc01());
            ps.setFloat(19, agprpf.getMlperpc02());
            ps.setFloat(20, agprpf.getMlperpc03());
            ps.setFloat(21, agprpf.getMlperpc04());
            ps.setFloat(22, agprpf.getMlperpc05());
            ps.setFloat(23, agprpf.getMlperpc06());
            ps.setFloat(24, agprpf.getMlperpc07());
            ps.setFloat(25, agprpf.getMlperpc08());
            ps.setFloat(26, agprpf.getMlperpc09());
            ps.setFloat(27, agprpf.getMlperpc10());
            ps.setFloat(28, agprpf.getMldirpp01());
            ps.setFloat(29, agprpf.getMldirpp02());
            ps.setFloat(30, agprpf.getMldirpp03());
            ps.setFloat(31, agprpf.getMldirpp04());
            ps.setFloat(32, agprpf.getMldirpp05());
            ps.setFloat(33, agprpf.getMldirpp06());
            ps.setFloat(34, agprpf.getMldirpp07());
            ps.setFloat(35, agprpf.getMldirpp08());
            ps.setFloat(36, agprpf.getMldirpp09());
            ps.setFloat(37, agprpf.getMldirpp10());
            ps.setFloat(38, agprpf.getMldirpc01());
            ps.setFloat(39, agprpf.getMldirpc02());
            ps.setFloat(40, agprpf.getMldirpc03());
            ps.setFloat(41, agprpf.getMldirpc04());
            ps.setFloat(42, agprpf.getMldirpc05());
            ps.setFloat(43, agprpf.getMldirpc06());
            ps.setFloat(44, agprpf.getMldirpc07());
            ps.setFloat(45, agprpf.getMldirpc08());
            ps.setFloat(46, agprpf.getMldirpc09());
            ps.setFloat(47, agprpf.getMldirpc10());
            ps.setFloat(48, agprpf.getMlgrppp01());
            ps.setFloat(49, agprpf.getMlgrppp02());
            ps.setFloat(50, agprpf.getMlgrppp03());
            ps.setFloat(51, agprpf.getMlgrppp04());
            ps.setFloat(52, agprpf.getMlgrppp05());
            ps.setFloat(53, agprpf.getMlgrppp06());
            ps.setFloat(54, agprpf.getMlgrppp07());
            ps.setFloat(55, agprpf.getMlgrppp08());
            ps.setFloat(56, agprpf.getMlgrppp09());
            ps.setFloat(57, agprpf.getMlgrppp10());
            ps.setFloat(58, agprpf.getMlgrppc01());
            ps.setFloat(59, agprpf.getMlgrppc02());
            ps.setFloat(60, agprpf.getMlgrppc03());
            ps.setFloat(61, agprpf.getMlgrppc04());
            ps.setFloat(62, agprpf.getMlgrppc05());
            ps.setFloat(63, agprpf.getMlgrppc06());
            ps.setFloat(64, agprpf.getMlgrppc07());
            ps.setFloat(65, agprpf.getMlgrppc08());
            ps.setFloat(66, agprpf.getMlgrppc09());
            ps.setFloat(67, agprpf.getMlgrppc10());
	        ps.setInt(68, agprpf.getCntcount());
	        ps.setFloat(69, agprpf.getSumins());
	        ps.setString(70, getUsrprf());
			ps.setString(71, getJobnm());
			ps.setTimestamp(72, getDatime()); 
			executeUpdate(ps);
        }catch (SQLException e) {
        	LOGGER.error("insertAgprpfRecord", e);//IJTI-1561
            throw new SQLRuntimeException(e);
        } finally {
            close(ps, null);
        }
    }
}
