package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6708
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6708ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(17);
	public FixedLengthStringData dataFields = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	public FixedLengthStringData action = DD.action.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(4).isAPartOf(dataArea, 1);
	public FixedLengthStringData actionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(12).isAPartOf(dataArea, 5);
	public FixedLengthStringData[] actionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6708screenWritten = new LongData(0);
	public LongData S6708protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6708ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(actionOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {action};
		screenOutFields = new BaseData[][] {actionOut};
		screenErrFields = new BaseData[] {actionErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenActionVar = action;
		screenRecord = S6708screen.class;
		protectRecord = S6708protect.class;
	}

}
