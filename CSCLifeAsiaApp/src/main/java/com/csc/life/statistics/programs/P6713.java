/*
 * File: P6713.java
 * Date: 30 August 2009 0:54:46
 * Author: Quipoz Limited
 * 
 * Class transformed from P6713.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.fsu.general.procedures.Acmdesc;
import com.csc.fsu.general.recordstructures.Acmdescrec;
import com.csc.life.statistics.dataaccess.GojnTableDAM;
import com.csc.life.statistics.dataaccess.GovrTableDAM;
import com.csc.life.statistics.screens.S6713ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*         CREATE GOVERNMENT STATISTICAL JOURNAL - SCREEN 2
*
* This program retrieves GOVR accumulation key passed from P6710
* as well as retrieving GOJN record. Journal values are accepted
* from this screen. If the transaction is committed, GOVR file
* would be updated and the final balances shown under ADJUSTED
* BALANCE column.
*
* This program allows the journalling of any accounting periods. It
* starts reading of the GOVR file for the first record of the same
* accumulation key except for accounting year.
*
* For previous year records of the same accumulation key, the last
* carried forward amount is retrieved and to be updated as the
* brought forward amount for journalled record. Adding the journal
* values under the ADJUSTMENT column to the brought forward values
* would give the carried forward values of the journalled record.
*
* For all forward records (ie. accounting years that are after the
* journalled record), a roll forward to add the adjustments to the
* brought forwards and carried forwards is  performed until the
* accumulation key changed, except for the accounting year.
*
* One journal record GOJN is created for every transaction committed,
* capturing the transaction date and time.
*
* VALIDATION:
*
*   - Ensure journal values are input (not zeroes).
*
*   - It is possible to enter journal records for any accounting
*     periods.
*
* FUNCTION KEYS:
*               <EXIT>  Return to sub-menu.
*
*               <CALC>  Calculation is performed with the adjustments.
*                       Display the adjusted accounted balances should
*                       the entered journal values be posted.
*
*               <KILL>  Return user to the select/previous screen
*                       to enter another accumulation key.
*
*               <ENTER> The first time this key is entered, a
*                       re-calculation of the journal values against
*                       GOVR file for the final account balances is
*                       performed and displayed and the ADJUSTMENT
*                       column is protected.
*
*                       When in the protected mode, to commit the
*                       display information into the databases, the
*                       <ENTER> key is to be pressed for the second
*                       time.
*
*                       Should user decide to abandon the changes,
*                       <KILL> or <EXIT> should be pressed and the
*                       information displayed in S6713 would not be
*                       committed.
*
* FILES USED:
*               GOVRSKM - Agent Statistical Accumulation Master File
*               GOJNSKM - Agent Statistical Journal File
*               DESCSKM - To retrieve currency's description
*
*
*****************************************************************
* </pre>
*/
public class P6713 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6713");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaGetBfFlag = "";
	private String wsaaNewRecFlag = "";
	private String wsaaUpdateBf = "";
	private ZonedDecimalData wsaaCounter = new ZonedDecimalData(1, 0).setUnsigned();
	private ZonedDecimalData wsaaFullDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaEffdate = new FixedLengthStringData(8).isAPartOf(wsaaFullDate, 0, REDEFINE);
	private ZonedDecimalData wsaaCentury = new ZonedDecimalData(2, 0).isAPartOf(wsaaEffdate, 0).setUnsigned();
	private FixedLengthStringData wsaaYymmdd = new FixedLengthStringData(6).isAPartOf(wsaaEffdate, 2);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(2, 0).isAPartOf(wsaaYymmdd, 0).setUnsigned();
	private ZonedDecimalData wsaaMmdd = new ZonedDecimalData(4, 0).isAPartOf(wsaaYymmdd, 2).setUnsigned();
	private PackedDecimalData wsaaBfwdc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwds = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdi = new PackedDecimalData(18, 2);
		/* ERRORS */
	private String e186 = "E186";
	private String t3629 = "T3629";
		/* FORMATS */
	private String govrrec = "GOVRREC";
	private String gojnrec = "GOJNREC";
	private String descrec = "DESCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Acmdescrec acmdescrec = new Acmdescrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Government Statistical Journal*/
	private GojnTableDAM gojnIO = new GojnTableDAM();
		/*Government Statistical Accumulation*/
	private GovrTableDAM govrIO = new GovrTableDAM();
	private S6713ScreenVars sv = ScreenProgram.getScreenVars( S6713ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2090, 
		exit3090, 
		exit3190
	}

	public P6713() {
		super();
		screenVars = sv;
		new ScreenModel("S6713", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
		retrvGojnFields1020();
		retrvGovrFields1030();
		setUpCurrencyDesc1040();
		setUpMonthDesc1050();
	}

protected void initialise1010()
	{
		wsaaCounter.set(ZERO);
		initialiseScreen1100();
	}

protected void retrvGojnFields1020()
	{
		gojnIO.setFunction(varcom.retrv);
		gojnIO.setFormat(gojnrec);
		SmartFileCode.execute(appVars, gojnIO);
		if (isNE(gojnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gojnIO.getParams());
			fatalError600();
		}
		gojnIO.setFunction(varcom.rlse);
		gojnIO.setFormat(gojnrec);
		SmartFileCode.execute(appVars, gojnIO);
		if (isNE(gojnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gojnIO.getParams());
			fatalError600();
		}
		sv.acyr.set(gojnIO.getAcctyr());
		sv.acmn.set(gojnIO.getAcctmonth());
	}

protected void retrvGovrFields1030()
	{
		govrIO.setFunction(varcom.retrv);
		govrIO.setFormat(govrrec);
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(govrIO.getParams());
			fatalError600();
		}
		govrIO.setFunction(varcom.rlse);
		govrIO.setFormat(govrrec);
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(govrIO.getParams());
			fatalError600();
		}
		sv.statcat.set(govrIO.getStatcat());
		sv.acctyr.set(govrIO.getAcctyr());
		sv.acyr.set(govrIO.getAcctyr());
		sv.statFund.set(govrIO.getStatFund());
		sv.statSect.set(govrIO.getStatSect());
		sv.stsubsect.set(govrIO.getStatSubsect());
		sv.register.set(govrIO.getRegister());
		sv.cntbranch.set(govrIO.getCntbranch());
		sv.bandage.set(govrIO.getBandage());
		sv.bandsa.set(govrIO.getBandsa());
		sv.bandprm.set(govrIO.getBandprm());
		sv.bandtrm.set(govrIO.getBandtrm());
		sv.commyr.set(govrIO.getCommyr());
		sv.pstatcd.set(govrIO.getPstatcode());
		sv.cntcurr.set(govrIO.getCntcurr());
		sv.stcmth.set(govrIO.getStcmth(gojnIO.getAcctmonth()));
		sv.stpmth.set(govrIO.getStpmth(gojnIO.getAcctmonth()));
		sv.stsmth.set(govrIO.getStsmth(gojnIO.getAcctmonth()));
		sv.stimth.set(govrIO.getStimth(gojnIO.getAcctmonth()));
	}

protected void setUpCurrencyDesc1040()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(govrIO.getChdrcoy());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(govrIO.getCntcurr());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.descrip.set(descIO.getLongdesc());
	}

protected void setUpMonthDesc1050()
	{
		setUpMonthDesc1200();
		/*EXIT*/
	}

protected void initialiseScreen1100()
	{
		/*INITIALISE*/
		sv.dataArea.set(SPACES);
		wsaaCounter.set(ZERO);
		sv.stcmthadjOut[varcom.pr.toInt()].set("N");
		sv.company.set(wsspcomn.company);
		sv.acctyr.set(ZERO);
		sv.acmn.set(ZERO);
		sv.acyr.set(ZERO);
		sv.commyr.set(ZERO);
		sv.stcmth.set(ZERO);
		sv.stcmthAdj.set(ZERO);
		sv.stcmtho.set(ZERO);
		sv.stimth.set(ZERO);
		sv.stimthAdj.set(ZERO);
		sv.stimtho.set(ZERO);
		sv.stpmth.set(ZERO);
		sv.stpmthAdj.set(ZERO);
		sv.stpmtho.set(ZERO);
		sv.stsmth.set(ZERO);
		sv.stsmthAdj.set(ZERO);
		sv.stsmtho.set(ZERO);
	}

protected void setUpMonthDesc1200()
	{
		/*PARA*/
		acmdescrec.company.set(govrIO.getChdrcoy());
		acmdescrec.language.set(wsspcomn.language);
		acmdescrec.branch.set(govrIO.getCntbranch());
		acmdescrec.function.set("GETD");
		callProgram(Acmdesc.class, acmdescrec.acmdescRec);
		if (isNE(acmdescrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(acmdescrec.statuz);
			fatalError600();
		}
		sv.mthldesc.set(acmdescrec.lngdesc[gojnIO.getAcctmonth().toInt()]);
		/*EXIT*/
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			screenIo2010();
			checkForCf092040();
			reconfirmPosting2060();
		}
		catch (GOTOException e){
		}
	}

protected void screenIo2010()
	{
		sv.stcmthadjOut[varcom.pr.toInt()].set("N");
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		if (isEQ(sv.stcmthAdj,ZERO)
		&& isEQ(sv.stpmthAdj,ZERO)
		&& isEQ(sv.stsmthAdj,ZERO)
		&& isEQ(sv.stimthAdj,ZERO)) {
			/*bug #ILIFE-1070 start*/
			sv.stcmthadjErr.set("E207");
			/*bug #ILIFE-1070 END*/
			wsspcomn.edterror.set("Y");
			wsaaCounter.set(ZERO);
			goTo(GotoLabel.exit2090);
		}
	}

protected void checkForCf092040()
	{
		if (isEQ(scrnparams.statuz,varcom.calc)) {
			calculateJournals2100();
			wsaaCounter.set(1);
			wsspcomn.edterror.set("Y");
			goTo(GotoLabel.exit2090);
		}
		/*CHECK-FOR-CF11*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.edterror.set(varcom.oK);
			goTo(GotoLabel.exit2090);
		}
	}

protected void reconfirmPosting2060()
	{
		calculateJournals2100();
		wsaaCounter.add(1);
		sv.stcmthadjOut[varcom.pr.toInt()].set("Y");
		wsspcomn.edterror.set("Y");
		if (isEQ(wsaaCounter,2)) {
			wsspcomn.edterror.set(varcom.oK);
		}
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void calculateJournals2100()
	{
		/*PARA*/
		compute(sv.stcmtho, 0).set(add(sv.stcmth,sv.stcmthAdj));
		compute(sv.stpmtho, 2).set(add(sv.stpmth,sv.stpmthAdj));
		compute(sv.stsmtho, 2).set(add(sv.stsmth,sv.stsmthAdj));
		compute(sv.stimtho, 2).set(add(sv.stimth,sv.stimthAdj));
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
			updateGojnFile3020();
			updateGovrFile3030();
			checkCfBfAmounts3040();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		/*CHECK-FUNCTION-KEY*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void updateGojnFile3020()
	{
		gojnIO.setTransactionDate(varcom.vrcmDate);
		gojnIO.setTransactionTime(varcom.vrcmTime);
		gojnIO.setUser(varcom.vrcmUser);
		wsaaYymmdd.set(varcom.vrcmDate);
		if (isGT(wsaaYear,50)) {
			wsaaCentury.set(19);
		}
		else {
			wsaaCentury.set(20);
		}
		gojnIO.setEffdate(wsaaFullDate);
		gojnIO.setStcmth(sv.stcmthAdj);
		gojnIO.setStpmth(sv.stpmthAdj);
		gojnIO.setStsmth(sv.stsmthAdj);
		gojnIO.setStimth(sv.stimthAdj);
		gojnIO.setFormat(gojnrec);
		gojnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, gojnIO);
		if (isNE(gojnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gojnIO.getParams());
			syserrrec.statuz.set(gojnIO.getStatuz());
			fatalError600();
		}
	}

protected void updateGovrFile3030()
	{
		govrIO.setStcmth(gojnIO.getAcctmonth(), sv.stcmtho);
		govrIO.setStpmth(gojnIO.getAcctmonth(), sv.stpmtho);
		govrIO.setStsmth(gojnIO.getAcctmonth(), sv.stsmtho);
		govrIO.setStimth(gojnIO.getAcctmonth(), sv.stimtho);
		setPrecision(govrIO.getCfwdc(), 2);
		govrIO.setCfwdc(add(govrIO.getCfwdc(),sv.stcmthAdj));
		setPrecision(govrIO.getCfwdp(), 2);
		govrIO.setCfwdp(add(govrIO.getCfwdp(),sv.stpmthAdj));
		setPrecision(govrIO.getCfwds(), 2);
		govrIO.setCfwds(add(govrIO.getCfwds(),sv.stsmthAdj));
		setPrecision(govrIO.getCfwdi(), 2);
		govrIO.setCfwdi(add(govrIO.getCfwdi(),sv.stimthAdj));
		wsaaNewRecFlag = "Y";
		govrIO.setFormat(govrrec);
		govrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, govrIO);
		if (isEQ(govrIO.getStatuz(),varcom.dupr)) {
			wsaaNewRecFlag = "N";
			govrIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, govrIO);
		}
		else {
			if (isNE(govrIO.getStatuz(),varcom.oK)) {
				syserrrec.params.set(govrIO.getParams());
				syserrrec.statuz.set(govrIO.getStatuz());
				fatalError600();
			}
		}
	}

protected void checkCfBfAmounts3040()
	{
		wsaaBfwdc.set(ZERO);
		wsaaBfwdp.set(ZERO);
		wsaaBfwds.set(ZERO);
		wsaaBfwdi.set(ZERO);
		govrIO.setAcctyr(ZERO);
		govrIO.setFunction(varcom.begnh);
		wsaaGetBfFlag = "N";
		while ( !(isEQ(wsaaGetBfFlag,"Y"))) {
			checkBalForwardValue3100();
		}
		
	}

protected void checkBalForwardValue3100()
	{
		try {
			para3110();
			readNextRecord3120();
		}
		catch (GOTOException e){
		}
	}

protected void para3110()
	{
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)
		&& isNE(govrIO.getStatuz(),varcom.endp)) {
			syserrrec.statuz.set(govrIO.getStatuz());
			syserrrec.params.set(govrIO.getParams());
			fatalError600();
		}
		if (isEQ(govrIO.getStatuz(),varcom.endp)) {
			wsaaGetBfFlag = "Y";
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(sv.company,govrIO.getChdrcoy())
		&& isEQ(sv.statcat,govrIO.getStatcat())
		&& isEQ(sv.statFund,govrIO.getStatFund())
		&& isEQ(sv.statSect,govrIO.getStatSect())
		&& isEQ(sv.stsubsect,govrIO.getStatSubsect())
		&& isEQ(sv.register,govrIO.getRegister())
		&& isEQ(sv.cntbranch,govrIO.getCntbranch())
		&& isEQ(sv.bandage,govrIO.getBandage())
		&& isEQ(sv.bandsa,govrIO.getBandsa())
		&& isEQ(sv.bandprm,govrIO.getBandprm())
		&& isEQ(sv.bandtrm,govrIO.getBandtrm())
		&& isEQ(sv.commyr,govrIO.getCommyr())
		&& isEQ(sv.pstatcd,govrIO.getPstatcode())
		&& isEQ(sv.cntcurr,govrIO.getCntcurr())) {
			if (isLT(sv.acctyr,govrIO.getAcctyr())) {
				rollForwardUpdate3200();
			}
			else {
				if (isEQ(sv.acctyr,govrIO.getAcctyr())) {
					updateBfAmount3300();
				}
				else {
					wsaaBfwdc.set(govrIO.getCfwdc());
					wsaaBfwdp.set(govrIO.getCfwdp());
					wsaaBfwds.set(govrIO.getCfwds());
					wsaaBfwdi.set(govrIO.getCfwdi());
				}
			}
		}
		else {
			wsaaGetBfFlag = "Y";
			goTo(GotoLabel.exit3190);
		}
	}

protected void readNextRecord3120()
	{
		govrIO.setFunction(varcom.nextr);
	}

protected void rollForwardUpdate3200()
	{
		/*PARA*/
		govrIO.setFormat(govrrec);
		govrIO.setFunction(varcom.rewrt);
		setPrecision(govrIO.getBfwdc(), 2);
		govrIO.setBfwdc(add(govrIO.getBfwdc(),sv.stcmthAdj));
		setPrecision(govrIO.getCfwdc(), 2);
		govrIO.setCfwdc(add(govrIO.getCfwdc(),sv.stcmthAdj));
		setPrecision(govrIO.getBfwdp(), 2);
		govrIO.setBfwdp(add(govrIO.getBfwdp(),sv.stpmthAdj));
		setPrecision(govrIO.getCfwdp(), 2);
		govrIO.setCfwdp(add(govrIO.getCfwdp(),sv.stpmthAdj));
		setPrecision(govrIO.getBfwds(), 2);
		govrIO.setBfwds(add(govrIO.getBfwds(),sv.stsmthAdj));
		setPrecision(govrIO.getCfwds(), 2);
		govrIO.setCfwds(add(govrIO.getCfwds(),sv.stsmthAdj));
		setPrecision(govrIO.getBfwdi(), 2);
		govrIO.setBfwdi(add(govrIO.getBfwdi(),sv.stimthAdj));
		setPrecision(govrIO.getCfwdi(), 2);
		govrIO.setCfwdi(add(govrIO.getCfwdi(),sv.stimthAdj));
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(govrIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void updateBfAmount3300()
	{
		para3310();
	}

protected void para3310()
	{
		govrIO.setBfwdc(wsaaBfwdc);
		govrIO.setBfwdp(wsaaBfwdp);
		govrIO.setBfwds(wsaaBfwds);
		govrIO.setBfwdi(wsaaBfwdi);
		if (isEQ(wsaaNewRecFlag,"Y")) {
			setPrecision(govrIO.getCfwdc(), 2);
			govrIO.setCfwdc(add(govrIO.getCfwdc(),wsaaBfwdc));
			setPrecision(govrIO.getCfwdp(), 2);
			govrIO.setCfwdp(add(govrIO.getCfwdp(),wsaaBfwdp));
			setPrecision(govrIO.getCfwds(), 2);
			govrIO.setCfwds(add(govrIO.getCfwds(),wsaaBfwds));
			setPrecision(govrIO.getCfwdi(), 2);
			govrIO.setCfwdi(add(govrIO.getCfwdi(),wsaaBfwdi));
		}
		govrIO.setFormat(govrrec);
		govrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, govrIO);
		if (isNE(govrIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(govrIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			wsspcomn.secActn[1].set(SPACES);
		}
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
