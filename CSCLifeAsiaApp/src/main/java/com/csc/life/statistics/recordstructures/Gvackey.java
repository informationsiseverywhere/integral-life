package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:21
 * Description:
 * Copybook name: GVACKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Gvackey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData gvacFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData gvacKey = new FixedLengthStringData(64).isAPartOf(gvacFileKey, 0, REDEFINE);
  	public FixedLengthStringData gvacChdrcoy = new FixedLengthStringData(1).isAPartOf(gvacKey, 0);
  	public FixedLengthStringData gvacCntbranch = new FixedLengthStringData(2).isAPartOf(gvacKey, 1);
  	public FixedLengthStringData gvacRegister = new FixedLengthStringData(3).isAPartOf(gvacKey, 3);
  	public FixedLengthStringData gvacSrcebus = new FixedLengthStringData(2).isAPartOf(gvacKey, 6);
  	public FixedLengthStringData gvacStatFund = new FixedLengthStringData(1).isAPartOf(gvacKey, 8);
  	public FixedLengthStringData gvacStatSect = new FixedLengthStringData(2).isAPartOf(gvacKey, 9);
  	public FixedLengthStringData gvacStatSubsect = new FixedLengthStringData(4).isAPartOf(gvacKey, 11);
  	public FixedLengthStringData gvacStatcode = new FixedLengthStringData(2).isAPartOf(gvacKey, 15);
  	public FixedLengthStringData gvacCnPremStat = new FixedLengthStringData(2).isAPartOf(gvacKey, 17);
  	public FixedLengthStringData gvacCovPremStat = new FixedLengthStringData(2).isAPartOf(gvacKey, 19);
  	public FixedLengthStringData gvacAcctccy = new FixedLengthStringData(3).isAPartOf(gvacKey, 21);
  	public FixedLengthStringData gvacCnttype = new FixedLengthStringData(3).isAPartOf(gvacKey, 24);
  	public FixedLengthStringData gvacCrtable = new FixedLengthStringData(4).isAPartOf(gvacKey, 27);
  	public FixedLengthStringData gvacParind = new FixedLengthStringData(1).isAPartOf(gvacKey, 31);
  	public PackedDecimalData gvacAcctyr = new PackedDecimalData(4, 0).isAPartOf(gvacKey, 32);
  	public FixedLengthStringData filler = new FixedLengthStringData(29).isAPartOf(gvacKey, 35, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(gvacFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		gvacFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}