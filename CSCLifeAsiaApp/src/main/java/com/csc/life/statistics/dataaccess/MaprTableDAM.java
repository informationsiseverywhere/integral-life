package com.csc.life.statistics.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: MaprTableDAM.java
 * Date: Sun, 30 Aug 2009 03:43:17
 * Class transformed from MAPR.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class MaprTableDAM extends MaprpfTableDAM {

	public MaprTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("MAPR");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "AGNTCOY"
		             + ", AGNTNUM"
		             + ", ACCTYR"
		             + ", MNTH"
		             + ", CNTTYPE"
		             + ", EFFDATE";
		
		QUALIFIEDCOLUMNS = 
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
		            "AGNTCOY, " +
		            "AGNTNUM, " +
		            "ACCTYR, " +
		            "MNTH, " +
		            "CNTTYPE, " +
		            "EFFDATE, " +
		            "MLPERPP01, " +
		            "MLPERPP02, " +
		            "MLPERPP03, " +
		            "MLPERPP04, " +
		            "MLPERPP05, " +
		            "MLPERPP06, " +
		            "MLPERPP07, " +
		            "MLPERPP08, " +
		            "MLPERPP09, " +
		            "MLPERPP10, " +
		            "MLPERPC01, " +
		            "MLPERPC02, " +
		            "MLPERPC03, " +
		            "MLPERPC04, " +
		            "MLPERPC05, " +
		            "MLPERPC06, " +
		            "MLPERPC07, " +
		            "MLPERPC08, " +
		            "MLPERPC09, " +
		            "MLPERPC10, " +
		            "MLDIRPP01, " +
		            "MLDIRPP02, " +
		            "MLDIRPP03, " +
		            "MLDIRPP04, " +
		            "MLDIRPP05, " +
		            "MLDIRPP06, " +
		            "MLDIRPP07, " +
		            "MLDIRPP08, " +
		            "MLDIRPP09, " +
		            "MLDIRPP10, " +
		            "MLDIRPC01, " +
		            "MLDIRPC02, " +
		            "MLDIRPC03, " +
		            "MLDIRPC04, " +
		            "MLDIRPC05, " +
		            "MLDIRPC06, " +
		            "MLDIRPC07, " +
		            "MLDIRPC08, " +
		            "MLDIRPC09, " +
		            "MLDIRPC10, " +
		            "MLGRPPP01, " +
		            "MLGRPPP02, " +
		            "MLGRPPP03, " +
		            "MLGRPPP04, " +
		            "MLGRPPP05, " +
		            "MLGRPPP06, " +
		            "MLGRPPP07, " +
		            "MLGRPPP08, " +
		            "MLGRPPP09, " +
		            "MLGRPPP10, " +
		            "MLGRPPC01, " +
		            "MLGRPPC02, " +
		            "MLGRPPC03, " +
		            "MLGRPPC04, " +
		            "MLGRPPC05, " +
		            "MLGRPPC06, " +
		            "MLGRPPC07, " +
		            "MLGRPPC08, " +
		            "MLGRPPC09, " +
		            "MLGRPPC10, " +
		            "CNTCOUNT, " +
		            "SUMINS, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "AGNTCOY ASC, " +
		            "AGNTNUM ASC, " +
		            "ACCTYR ASC, " +
		            "MNTH ASC, " +
		            "CNTTYPE ASC, " +
		            "EFFDATE ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "AGNTCOY DESC, " +
		            "AGNTNUM DESC, " +
		            "ACCTYR DESC, " +
		            "MNTH DESC, " +
		            "CNTTYPE DESC, " +
		            "EFFDATE DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               jobName,
                               userProfile,
                               datime,
                               agntcoy,
                               agntnum,
                               acctyr,
                               mnth,
                               cnttype,
                               effdate,
                               mlperpp01,
                               mlperpp02,
                               mlperpp03,
                               mlperpp04,
                               mlperpp05,
                               mlperpp06,
                               mlperpp07,
                               mlperpp08,
                               mlperpp09,
                               mlperpp10,
                               mlperpc01,
                               mlperpc02,
                               mlperpc03,
                               mlperpc04,
                               mlperpc05,
                               mlperpc06,
                               mlperpc07,
                               mlperpc08,
                               mlperpc09,
                               mlperpc10,
                               mldirpp01,
                               mldirpp02,
                               mldirpp03,
                               mldirpp04,
                               mldirpp05,
                               mldirpp06,
                               mldirpp07,
                               mldirpp08,
                               mldirpp09,
                               mldirpp10,
                               mldirpc01,
                               mldirpc02,
                               mldirpc03,
                               mldirpc04,
                               mldirpc05,
                               mldirpc06,
                               mldirpc07,
                               mldirpc08,
                               mldirpc09,
                               mldirpc10,
                               mlgrppp01,
                               mlgrppp02,
                               mlgrppp03,
                               mlgrppp04,
                               mlgrppp05,
                               mlgrppp06,
                               mlgrppp07,
                               mlgrppp08,
                               mlgrppp09,
                               mlgrppp10,
                               mlgrppc01,
                               mlgrppc02,
                               mlgrppc03,
                               mlgrppc04,
                               mlgrppc05,
                               mlgrppc06,
                               mlgrppc07,
                               mlgrppc08,
                               mlgrppc09,
                               mlgrppc10,
                               cntcount,
                               sumins,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(42);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getAgntcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getAcctyr().toInternal()
					+ getMnth().toInternal()
					+ getCnttype().toInternal()
					+ getEffdate().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, agntcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, acctyr);
			what = ExternalData.chop(what, mnth);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(5);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller40.setInternal(agntcoy.toInternal());
	nonKeyFiller50.setInternal(agntnum.toInternal());
	nonKeyFiller60.setInternal(acctyr.toInternal());
	nonKeyFiller70.setInternal(mnth.toInternal());
	nonKeyFiller80.setInternal(cnttype.toInternal());
	nonKeyFiller90.setInternal(effdate.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(621);
		
		nonKeyData.set(
					getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ getMlperpp01().toInternal()
					+ getMlperpp02().toInternal()
					+ getMlperpp03().toInternal()
					+ getMlperpp04().toInternal()
					+ getMlperpp05().toInternal()
					+ getMlperpp06().toInternal()
					+ getMlperpp07().toInternal()
					+ getMlperpp08().toInternal()
					+ getMlperpp09().toInternal()
					+ getMlperpp10().toInternal()
					+ getMlperpc01().toInternal()
					+ getMlperpc02().toInternal()
					+ getMlperpc03().toInternal()
					+ getMlperpc04().toInternal()
					+ getMlperpc05().toInternal()
					+ getMlperpc06().toInternal()
					+ getMlperpc07().toInternal()
					+ getMlperpc08().toInternal()
					+ getMlperpc09().toInternal()
					+ getMlperpc10().toInternal()
					+ getMldirpp01().toInternal()
					+ getMldirpp02().toInternal()
					+ getMldirpp03().toInternal()
					+ getMldirpp04().toInternal()
					+ getMldirpp05().toInternal()
					+ getMldirpp06().toInternal()
					+ getMldirpp07().toInternal()
					+ getMldirpp08().toInternal()
					+ getMldirpp09().toInternal()
					+ getMldirpp10().toInternal()
					+ getMldirpc01().toInternal()
					+ getMldirpc02().toInternal()
					+ getMldirpc03().toInternal()
					+ getMldirpc04().toInternal()
					+ getMldirpc05().toInternal()
					+ getMldirpc06().toInternal()
					+ getMldirpc07().toInternal()
					+ getMldirpc08().toInternal()
					+ getMldirpc09().toInternal()
					+ getMldirpc10().toInternal()
					+ getMlgrppp01().toInternal()
					+ getMlgrppp02().toInternal()
					+ getMlgrppp03().toInternal()
					+ getMlgrppp04().toInternal()
					+ getMlgrppp05().toInternal()
					+ getMlgrppp06().toInternal()
					+ getMlgrppp07().toInternal()
					+ getMlgrppp08().toInternal()
					+ getMlgrppp09().toInternal()
					+ getMlgrppp10().toInternal()
					+ getMlgrppc01().toInternal()
					+ getMlgrppc02().toInternal()
					+ getMlgrppc03().toInternal()
					+ getMlgrppc04().toInternal()
					+ getMlgrppc05().toInternal()
					+ getMlgrppc06().toInternal()
					+ getMlgrppc07().toInternal()
					+ getMlgrppc08().toInternal()
					+ getMlgrppc09().toInternal()
					+ getMlgrppc10().toInternal()
					+ getCntcount().toInternal()
					+ getSumins().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, mlperpp01);
			what = ExternalData.chop(what, mlperpp02);
			what = ExternalData.chop(what, mlperpp03);
			what = ExternalData.chop(what, mlperpp04);
			what = ExternalData.chop(what, mlperpp05);
			what = ExternalData.chop(what, mlperpp06);
			what = ExternalData.chop(what, mlperpp07);
			what = ExternalData.chop(what, mlperpp08);
			what = ExternalData.chop(what, mlperpp09);
			what = ExternalData.chop(what, mlperpp10);
			what = ExternalData.chop(what, mlperpc01);
			what = ExternalData.chop(what, mlperpc02);
			what = ExternalData.chop(what, mlperpc03);
			what = ExternalData.chop(what, mlperpc04);
			what = ExternalData.chop(what, mlperpc05);
			what = ExternalData.chop(what, mlperpc06);
			what = ExternalData.chop(what, mlperpc07);
			what = ExternalData.chop(what, mlperpc08);
			what = ExternalData.chop(what, mlperpc09);
			what = ExternalData.chop(what, mlperpc10);
			what = ExternalData.chop(what, mldirpp01);
			what = ExternalData.chop(what, mldirpp02);
			what = ExternalData.chop(what, mldirpp03);
			what = ExternalData.chop(what, mldirpp04);
			what = ExternalData.chop(what, mldirpp05);
			what = ExternalData.chop(what, mldirpp06);
			what = ExternalData.chop(what, mldirpp07);
			what = ExternalData.chop(what, mldirpp08);
			what = ExternalData.chop(what, mldirpp09);
			what = ExternalData.chop(what, mldirpp10);
			what = ExternalData.chop(what, mldirpc01);
			what = ExternalData.chop(what, mldirpc02);
			what = ExternalData.chop(what, mldirpc03);
			what = ExternalData.chop(what, mldirpc04);
			what = ExternalData.chop(what, mldirpc05);
			what = ExternalData.chop(what, mldirpc06);
			what = ExternalData.chop(what, mldirpc07);
			what = ExternalData.chop(what, mldirpc08);
			what = ExternalData.chop(what, mldirpc09);
			what = ExternalData.chop(what, mldirpc10);
			what = ExternalData.chop(what, mlgrppp01);
			what = ExternalData.chop(what, mlgrppp02);
			what = ExternalData.chop(what, mlgrppp03);
			what = ExternalData.chop(what, mlgrppp04);
			what = ExternalData.chop(what, mlgrppp05);
			what = ExternalData.chop(what, mlgrppp06);
			what = ExternalData.chop(what, mlgrppp07);
			what = ExternalData.chop(what, mlgrppp08);
			what = ExternalData.chop(what, mlgrppp09);
			what = ExternalData.chop(what, mlgrppp10);
			what = ExternalData.chop(what, mlgrppc01);
			what = ExternalData.chop(what, mlgrppc02);
			what = ExternalData.chop(what, mlgrppc03);
			what = ExternalData.chop(what, mlgrppc04);
			what = ExternalData.chop(what, mlgrppc05);
			what = ExternalData.chop(what, mlgrppc06);
			what = ExternalData.chop(what, mlgrppc07);
			what = ExternalData.chop(what, mlgrppc08);
			what = ExternalData.chop(what, mlgrppc09);
			what = ExternalData.chop(what, mlgrppc10);
			what = ExternalData.chop(what, cntcount);
			what = ExternalData.chop(what, sumins);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(Object what) {
		agntcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	public PackedDecimalData getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(Object what) {
		setAcctyr(what, false);
	}
	public void setAcctyr(Object what, boolean rounded) {
		if (rounded)
			acctyr.setRounded(what);
		else
			acctyr.set(what);
	}
	public PackedDecimalData getMnth() {
		return mnth;
	}
	public void setMnth(Object what) {
		setMnth(what, false);
	}
	public void setMnth(Object what, boolean rounded) {
		if (rounded)
			mnth.setRounded(what);
		else
			mnth.set(what);
	}
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	
	public PackedDecimalData getMlperpp01() {
		return mlperpp01;
	}
	public void setMlperpp01(Object what) {
		setMlperpp01(what, false);
	}
	public void setMlperpp01(Object what, boolean rounded) {
		if (rounded)
			mlperpp01.setRounded(what);
		else
			mlperpp01.set(what);
	}	
	public PackedDecimalData getMlperpp02() {
		return mlperpp02;
	}
	public void setMlperpp02(Object what) {
		setMlperpp02(what, false);
	}
	public void setMlperpp02(Object what, boolean rounded) {
		if (rounded)
			mlperpp02.setRounded(what);
		else
			mlperpp02.set(what);
	}	
	public PackedDecimalData getMlperpp03() {
		return mlperpp03;
	}
	public void setMlperpp03(Object what) {
		setMlperpp03(what, false);
	}
	public void setMlperpp03(Object what, boolean rounded) {
		if (rounded)
			mlperpp03.setRounded(what);
		else
			mlperpp03.set(what);
	}	
	public PackedDecimalData getMlperpp04() {
		return mlperpp04;
	}
	public void setMlperpp04(Object what) {
		setMlperpp04(what, false);
	}
	public void setMlperpp04(Object what, boolean rounded) {
		if (rounded)
			mlperpp04.setRounded(what);
		else
			mlperpp04.set(what);
	}	
	public PackedDecimalData getMlperpp05() {
		return mlperpp05;
	}
	public void setMlperpp05(Object what) {
		setMlperpp05(what, false);
	}
	public void setMlperpp05(Object what, boolean rounded) {
		if (rounded)
			mlperpp05.setRounded(what);
		else
			mlperpp05.set(what);
	}	
	public PackedDecimalData getMlperpp06() {
		return mlperpp06;
	}
	public void setMlperpp06(Object what) {
		setMlperpp06(what, false);
	}
	public void setMlperpp06(Object what, boolean rounded) {
		if (rounded)
			mlperpp06.setRounded(what);
		else
			mlperpp06.set(what);
	}	
	public PackedDecimalData getMlperpp07() {
		return mlperpp07;
	}
	public void setMlperpp07(Object what) {
		setMlperpp07(what, false);
	}
	public void setMlperpp07(Object what, boolean rounded) {
		if (rounded)
			mlperpp07.setRounded(what);
		else
			mlperpp07.set(what);
	}	
	public PackedDecimalData getMlperpp08() {
		return mlperpp08;
	}
	public void setMlperpp08(Object what) {
		setMlperpp08(what, false);
	}
	public void setMlperpp08(Object what, boolean rounded) {
		if (rounded)
			mlperpp08.setRounded(what);
		else
			mlperpp08.set(what);
	}	
	public PackedDecimalData getMlperpp09() {
		return mlperpp09;
	}
	public void setMlperpp09(Object what) {
		setMlperpp09(what, false);
	}
	public void setMlperpp09(Object what, boolean rounded) {
		if (rounded)
			mlperpp09.setRounded(what);
		else
			mlperpp09.set(what);
	}	
	public PackedDecimalData getMlperpp10() {
		return mlperpp10;
	}
	public void setMlperpp10(Object what) {
		setMlperpp10(what, false);
	}
	public void setMlperpp10(Object what, boolean rounded) {
		if (rounded)
			mlperpp10.setRounded(what);
		else
			mlperpp10.set(what);
	}	
	public PackedDecimalData getMlperpc01() {
		return mlperpc01;
	}
	public void setMlperpc01(Object what) {
		setMlperpc01(what, false);
	}
	public void setMlperpc01(Object what, boolean rounded) {
		if (rounded)
			mlperpc01.setRounded(what);
		else
			mlperpc01.set(what);
	}	
	public PackedDecimalData getMlperpc02() {
		return mlperpc02;
	}
	public void setMlperpc02(Object what) {
		setMlperpc02(what, false);
	}
	public void setMlperpc02(Object what, boolean rounded) {
		if (rounded)
			mlperpc02.setRounded(what);
		else
			mlperpc02.set(what);
	}	
	public PackedDecimalData getMlperpc03() {
		return mlperpc03;
	}
	public void setMlperpc03(Object what) {
		setMlperpc03(what, false);
	}
	public void setMlperpc03(Object what, boolean rounded) {
		if (rounded)
			mlperpc03.setRounded(what);
		else
			mlperpc03.set(what);
	}	
	public PackedDecimalData getMlperpc04() {
		return mlperpc04;
	}
	public void setMlperpc04(Object what) {
		setMlperpc04(what, false);
	}
	public void setMlperpc04(Object what, boolean rounded) {
		if (rounded)
			mlperpc04.setRounded(what);
		else
			mlperpc04.set(what);
	}	
	public PackedDecimalData getMlperpc05() {
		return mlperpc05;
	}
	public void setMlperpc05(Object what) {
		setMlperpc05(what, false);
	}
	public void setMlperpc05(Object what, boolean rounded) {
		if (rounded)
			mlperpc05.setRounded(what);
		else
			mlperpc05.set(what);
	}	
	public PackedDecimalData getMlperpc06() {
		return mlperpc06;
	}
	public void setMlperpc06(Object what) {
		setMlperpc06(what, false);
	}
	public void setMlperpc06(Object what, boolean rounded) {
		if (rounded)
			mlperpc06.setRounded(what);
		else
			mlperpc06.set(what);
	}	
	public PackedDecimalData getMlperpc07() {
		return mlperpc07;
	}
	public void setMlperpc07(Object what) {
		setMlperpc07(what, false);
	}
	public void setMlperpc07(Object what, boolean rounded) {
		if (rounded)
			mlperpc07.setRounded(what);
		else
			mlperpc07.set(what);
	}	
	public PackedDecimalData getMlperpc08() {
		return mlperpc08;
	}
	public void setMlperpc08(Object what) {
		setMlperpc08(what, false);
	}
	public void setMlperpc08(Object what, boolean rounded) {
		if (rounded)
			mlperpc08.setRounded(what);
		else
			mlperpc08.set(what);
	}	
	public PackedDecimalData getMlperpc09() {
		return mlperpc09;
	}
	public void setMlperpc09(Object what) {
		setMlperpc09(what, false);
	}
	public void setMlperpc09(Object what, boolean rounded) {
		if (rounded)
			mlperpc09.setRounded(what);
		else
			mlperpc09.set(what);
	}	
	public PackedDecimalData getMlperpc10() {
		return mlperpc10;
	}
	public void setMlperpc10(Object what) {
		setMlperpc10(what, false);
	}
	public void setMlperpc10(Object what, boolean rounded) {
		if (rounded)
			mlperpc10.setRounded(what);
		else
			mlperpc10.set(what);
	}	
	public PackedDecimalData getMldirpp01() {
		return mldirpp01;
	}
	public void setMldirpp01(Object what) {
		setMldirpp01(what, false);
	}
	public void setMldirpp01(Object what, boolean rounded) {
		if (rounded)
			mldirpp01.setRounded(what);
		else
			mldirpp01.set(what);
	}	
	public PackedDecimalData getMldirpp02() {
		return mldirpp02;
	}
	public void setMldirpp02(Object what) {
		setMldirpp02(what, false);
	}
	public void setMldirpp02(Object what, boolean rounded) {
		if (rounded)
			mldirpp02.setRounded(what);
		else
			mldirpp02.set(what);
	}	
	public PackedDecimalData getMldirpp03() {
		return mldirpp03;
	}
	public void setMldirpp03(Object what) {
		setMldirpp03(what, false);
	}
	public void setMldirpp03(Object what, boolean rounded) {
		if (rounded)
			mldirpp03.setRounded(what);
		else
			mldirpp03.set(what);
	}	
	public PackedDecimalData getMldirpp04() {
		return mldirpp04;
	}
	public void setMldirpp04(Object what) {
		setMldirpp04(what, false);
	}
	public void setMldirpp04(Object what, boolean rounded) {
		if (rounded)
			mldirpp04.setRounded(what);
		else
			mldirpp04.set(what);
	}	
	public PackedDecimalData getMldirpp05() {
		return mldirpp05;
	}
	public void setMldirpp05(Object what) {
		setMldirpp05(what, false);
	}
	public void setMldirpp05(Object what, boolean rounded) {
		if (rounded)
			mldirpp05.setRounded(what);
		else
			mldirpp05.set(what);
	}	
	public PackedDecimalData getMldirpp06() {
		return mldirpp06;
	}
	public void setMldirpp06(Object what) {
		setMldirpp06(what, false);
	}
	public void setMldirpp06(Object what, boolean rounded) {
		if (rounded)
			mldirpp06.setRounded(what);
		else
			mldirpp06.set(what);
	}	
	public PackedDecimalData getMldirpp07() {
		return mldirpp07;
	}
	public void setMldirpp07(Object what) {
		setMldirpp07(what, false);
	}
	public void setMldirpp07(Object what, boolean rounded) {
		if (rounded)
			mldirpp07.setRounded(what);
		else
			mldirpp07.set(what);
	}	
	public PackedDecimalData getMldirpp08() {
		return mldirpp08;
	}
	public void setMldirpp08(Object what) {
		setMldirpp08(what, false);
	}
	public void setMldirpp08(Object what, boolean rounded) {
		if (rounded)
			mldirpp08.setRounded(what);
		else
			mldirpp08.set(what);
	}	
	public PackedDecimalData getMldirpp09() {
		return mldirpp09;
	}
	public void setMldirpp09(Object what) {
		setMldirpp09(what, false);
	}
	public void setMldirpp09(Object what, boolean rounded) {
		if (rounded)
			mldirpp09.setRounded(what);
		else
			mldirpp09.set(what);
	}	
	public PackedDecimalData getMldirpp10() {
		return mldirpp10;
	}
	public void setMldirpp10(Object what) {
		setMldirpp10(what, false);
	}
	public void setMldirpp10(Object what, boolean rounded) {
		if (rounded)
			mldirpp10.setRounded(what);
		else
			mldirpp10.set(what);
	}	
	public PackedDecimalData getMldirpc01() {
		return mldirpc01;
	}
	public void setMldirpc01(Object what) {
		setMldirpc01(what, false);
	}
	public void setMldirpc01(Object what, boolean rounded) {
		if (rounded)
			mldirpc01.setRounded(what);
		else
			mldirpc01.set(what);
	}	
	public PackedDecimalData getMldirpc02() {
		return mldirpc02;
	}
	public void setMldirpc02(Object what) {
		setMldirpc02(what, false);
	}
	public void setMldirpc02(Object what, boolean rounded) {
		if (rounded)
			mldirpc02.setRounded(what);
		else
			mldirpc02.set(what);
	}	
	public PackedDecimalData getMldirpc03() {
		return mldirpc03;
	}
	public void setMldirpc03(Object what) {
		setMldirpc03(what, false);
	}
	public void setMldirpc03(Object what, boolean rounded) {
		if (rounded)
			mldirpc03.setRounded(what);
		else
			mldirpc03.set(what);
	}	
	public PackedDecimalData getMldirpc04() {
		return mldirpc04;
	}
	public void setMldirpc04(Object what) {
		setMldirpc04(what, false);
	}
	public void setMldirpc04(Object what, boolean rounded) {
		if (rounded)
			mldirpc04.setRounded(what);
		else
			mldirpc04.set(what);
	}	
	public PackedDecimalData getMldirpc05() {
		return mldirpc05;
	}
	public void setMldirpc05(Object what) {
		setMldirpc05(what, false);
	}
	public void setMldirpc05(Object what, boolean rounded) {
		if (rounded)
			mldirpc05.setRounded(what);
		else
			mldirpc05.set(what);
	}	
	public PackedDecimalData getMldirpc06() {
		return mldirpc06;
	}
	public void setMldirpc06(Object what) {
		setMldirpc06(what, false);
	}
	public void setMldirpc06(Object what, boolean rounded) {
		if (rounded)
			mldirpc06.setRounded(what);
		else
			mldirpc06.set(what);
	}	
	public PackedDecimalData getMldirpc07() {
		return mldirpc07;
	}
	public void setMldirpc07(Object what) {
		setMldirpc07(what, false);
	}
	public void setMldirpc07(Object what, boolean rounded) {
		if (rounded)
			mldirpc07.setRounded(what);
		else
			mldirpc07.set(what);
	}	
	public PackedDecimalData getMldirpc08() {
		return mldirpc08;
	}
	public void setMldirpc08(Object what) {
		setMldirpc08(what, false);
	}
	public void setMldirpc08(Object what, boolean rounded) {
		if (rounded)
			mldirpc08.setRounded(what);
		else
			mldirpc08.set(what);
	}	
	public PackedDecimalData getMldirpc09() {
		return mldirpc09;
	}
	public void setMldirpc09(Object what) {
		setMldirpc09(what, false);
	}
	public void setMldirpc09(Object what, boolean rounded) {
		if (rounded)
			mldirpc09.setRounded(what);
		else
			mldirpc09.set(what);
	}	
	public PackedDecimalData getMldirpc10() {
		return mldirpc10;
	}
	public void setMldirpc10(Object what) {
		setMldirpc10(what, false);
	}
	public void setMldirpc10(Object what, boolean rounded) {
		if (rounded)
			mldirpc10.setRounded(what);
		else
			mldirpc10.set(what);
	}	
	public PackedDecimalData getMlgrppp01() {
		return mlgrppp01;
	}
	public void setMlgrppp01(Object what) {
		setMlgrppp01(what, false);
	}
	public void setMlgrppp01(Object what, boolean rounded) {
		if (rounded)
			mlgrppp01.setRounded(what);
		else
			mlgrppp01.set(what);
	}	
	public PackedDecimalData getMlgrppp02() {
		return mlgrppp02;
	}
	public void setMlgrppp02(Object what) {
		setMlgrppp02(what, false);
	}
	public void setMlgrppp02(Object what, boolean rounded) {
		if (rounded)
			mlgrppp02.setRounded(what);
		else
			mlgrppp02.set(what);
	}	
	public PackedDecimalData getMlgrppp03() {
		return mlgrppp03;
	}
	public void setMlgrppp03(Object what) {
		setMlgrppp03(what, false);
	}
	public void setMlgrppp03(Object what, boolean rounded) {
		if (rounded)
			mlgrppp03.setRounded(what);
		else
			mlgrppp03.set(what);
	}	
	public PackedDecimalData getMlgrppp04() {
		return mlgrppp04;
	}
	public void setMlgrppp04(Object what) {
		setMlgrppp04(what, false);
	}
	public void setMlgrppp04(Object what, boolean rounded) {
		if (rounded)
			mlgrppp04.setRounded(what);
		else
			mlgrppp04.set(what);
	}	
	public PackedDecimalData getMlgrppp05() {
		return mlgrppp05;
	}
	public void setMlgrppp05(Object what) {
		setMlgrppp05(what, false);
	}
	public void setMlgrppp05(Object what, boolean rounded) {
		if (rounded)
			mlgrppp05.setRounded(what);
		else
			mlgrppp05.set(what);
	}	
	public PackedDecimalData getMlgrppp06() {
		return mlgrppp06;
	}
	public void setMlgrppp06(Object what) {
		setMlgrppp06(what, false);
	}
	public void setMlgrppp06(Object what, boolean rounded) {
		if (rounded)
			mlgrppp06.setRounded(what);
		else
			mlgrppp06.set(what);
	}	
	public PackedDecimalData getMlgrppp07() {
		return mlgrppp07;
	}
	public void setMlgrppp07(Object what) {
		setMlgrppp07(what, false);
	}
	public void setMlgrppp07(Object what, boolean rounded) {
		if (rounded)
			mlgrppp07.setRounded(what);
		else
			mlgrppp07.set(what);
	}	
	public PackedDecimalData getMlgrppp08() {
		return mlgrppp08;
	}
	public void setMlgrppp08(Object what) {
		setMlgrppp08(what, false);
	}
	public void setMlgrppp08(Object what, boolean rounded) {
		if (rounded)
			mlgrppp08.setRounded(what);
		else
			mlgrppp08.set(what);
	}	
	public PackedDecimalData getMlgrppp09() {
		return mlgrppp09;
	}
	public void setMlgrppp09(Object what) {
		setMlgrppp09(what, false);
	}
	public void setMlgrppp09(Object what, boolean rounded) {
		if (rounded)
			mlgrppp09.setRounded(what);
		else
			mlgrppp09.set(what);
	}	
	public PackedDecimalData getMlgrppp10() {
		return mlgrppp10;
	}
	public void setMlgrppp10(Object what) {
		setMlgrppp10(what, false);
	}
	public void setMlgrppp10(Object what, boolean rounded) {
		if (rounded)
			mlgrppp10.setRounded(what);
		else
			mlgrppp10.set(what);
	}	
	public PackedDecimalData getMlgrppc01() {
		return mlgrppc01;
	}
	public void setMlgrppc01(Object what) {
		setMlgrppc01(what, false);
	}
	public void setMlgrppc01(Object what, boolean rounded) {
		if (rounded)
			mlgrppc01.setRounded(what);
		else
			mlgrppc01.set(what);
	}	
	public PackedDecimalData getMlgrppc02() {
		return mlgrppc02;
	}
	public void setMlgrppc02(Object what) {
		setMlgrppc02(what, false);
	}
	public void setMlgrppc02(Object what, boolean rounded) {
		if (rounded)
			mlgrppc02.setRounded(what);
		else
			mlgrppc02.set(what);
	}	
	public PackedDecimalData getMlgrppc03() {
		return mlgrppc03;
	}
	public void setMlgrppc03(Object what) {
		setMlgrppc03(what, false);
	}
	public void setMlgrppc03(Object what, boolean rounded) {
		if (rounded)
			mlgrppc03.setRounded(what);
		else
			mlgrppc03.set(what);
	}	
	public PackedDecimalData getMlgrppc04() {
		return mlgrppc04;
	}
	public void setMlgrppc04(Object what) {
		setMlgrppc04(what, false);
	}
	public void setMlgrppc04(Object what, boolean rounded) {
		if (rounded)
			mlgrppc04.setRounded(what);
		else
			mlgrppc04.set(what);
	}	
	public PackedDecimalData getMlgrppc05() {
		return mlgrppc05;
	}
	public void setMlgrppc05(Object what) {
		setMlgrppc05(what, false);
	}
	public void setMlgrppc05(Object what, boolean rounded) {
		if (rounded)
			mlgrppc05.setRounded(what);
		else
			mlgrppc05.set(what);
	}	
	public PackedDecimalData getMlgrppc06() {
		return mlgrppc06;
	}
	public void setMlgrppc06(Object what) {
		setMlgrppc06(what, false);
	}
	public void setMlgrppc06(Object what, boolean rounded) {
		if (rounded)
			mlgrppc06.setRounded(what);
		else
			mlgrppc06.set(what);
	}	
	public PackedDecimalData getMlgrppc07() {
		return mlgrppc07;
	}
	public void setMlgrppc07(Object what) {
		setMlgrppc07(what, false);
	}
	public void setMlgrppc07(Object what, boolean rounded) {
		if (rounded)
			mlgrppc07.setRounded(what);
		else
			mlgrppc07.set(what);
	}	
	public PackedDecimalData getMlgrppc08() {
		return mlgrppc08;
	}
	public void setMlgrppc08(Object what) {
		setMlgrppc08(what, false);
	}
	public void setMlgrppc08(Object what, boolean rounded) {
		if (rounded)
			mlgrppc08.setRounded(what);
		else
			mlgrppc08.set(what);
	}	
	public PackedDecimalData getMlgrppc09() {
		return mlgrppc09;
	}
	public void setMlgrppc09(Object what) {
		setMlgrppc09(what, false);
	}
	public void setMlgrppc09(Object what, boolean rounded) {
		if (rounded)
			mlgrppc09.setRounded(what);
		else
			mlgrppc09.set(what);
	}	
	public PackedDecimalData getMlgrppc10() {
		return mlgrppc10;
	}
	public void setMlgrppc10(Object what) {
		setMlgrppc10(what, false);
	}
	public void setMlgrppc10(Object what, boolean rounded) {
		if (rounded)
			mlgrppc10.setRounded(what);
		else
			mlgrppc10.set(what);
	}	
	public PackedDecimalData getCntcount() {
		return cntcount;
	}
	public void setCntcount(Object what) {
		setCntcount(what, false);
	}
	public void setCntcount(Object what, boolean rounded) {
		if (rounded)
			cntcount.setRounded(what);
		else
			cntcount.set(what);
	}	
	public PackedDecimalData getSumins() {
		return sumins;
	}
	public void setSumins(Object what) {
		setSumins(what, false);
	}
	public void setSumins(Object what, boolean rounded) {
		if (rounded)
			sumins.setRounded(what);
		else
			sumins.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getMlperpps() {
		return new FixedLengthStringData(mlperpp01.toInternal()
										+ mlperpp02.toInternal()
										+ mlperpp03.toInternal()
										+ mlperpp04.toInternal()
										+ mlperpp05.toInternal()
										+ mlperpp06.toInternal()
										+ mlperpp07.toInternal()
										+ mlperpp08.toInternal()
										+ mlperpp09.toInternal()
										+ mlperpp10.toInternal());
	}
	public void setMlperpps(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMlperpps().getLength()).init(obj);
	
		what = ExternalData.chop(what, mlperpp01);
		what = ExternalData.chop(what, mlperpp02);
		what = ExternalData.chop(what, mlperpp03);
		what = ExternalData.chop(what, mlperpp04);
		what = ExternalData.chop(what, mlperpp05);
		what = ExternalData.chop(what, mlperpp06);
		what = ExternalData.chop(what, mlperpp07);
		what = ExternalData.chop(what, mlperpp08);
		what = ExternalData.chop(what, mlperpp09);
		what = ExternalData.chop(what, mlperpp10);
	}
	public PackedDecimalData getMlperpp(BaseData indx) {
		return getMlperpp(indx.toInt());
	}
	public PackedDecimalData getMlperpp(int indx) {

		switch (indx) {
			case 1 : return mlperpp01;
			case 2 : return mlperpp02;
			case 3 : return mlperpp03;
			case 4 : return mlperpp04;
			case 5 : return mlperpp05;
			case 6 : return mlperpp06;
			case 7 : return mlperpp07;
			case 8 : return mlperpp08;
			case 9 : return mlperpp09;
			case 10 : return mlperpp10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMlperpp(BaseData indx, Object what) {
		setMlperpp(indx, what, false);
	}
	public void setMlperpp(BaseData indx, Object what, boolean rounded) {
		setMlperpp(indx.toInt(), what, rounded);
	}
	public void setMlperpp(int indx, Object what) {
		setMlperpp(indx, what, false);
	}
	public void setMlperpp(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setMlperpp01(what, rounded);
					 break;
			case 2 : setMlperpp02(what, rounded);
					 break;
			case 3 : setMlperpp03(what, rounded);
					 break;
			case 4 : setMlperpp04(what, rounded);
					 break;
			case 5 : setMlperpp05(what, rounded);
					 break;
			case 6 : setMlperpp06(what, rounded);
					 break;
			case 7 : setMlperpp07(what, rounded);
					 break;
			case 8 : setMlperpp08(what, rounded);
					 break;
			case 9 : setMlperpp09(what, rounded);
					 break;
			case 10 : setMlperpp10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getMlperpcs() {
		return new FixedLengthStringData(mlperpc01.toInternal()
										+ mlperpc02.toInternal()
										+ mlperpc03.toInternal()
										+ mlperpc04.toInternal()
										+ mlperpc05.toInternal()
										+ mlperpc06.toInternal()
										+ mlperpc07.toInternal()
										+ mlperpc08.toInternal()
										+ mlperpc09.toInternal()
										+ mlperpc10.toInternal());
	}
	public void setMlperpcs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMlperpcs().getLength()).init(obj);
	
		what = ExternalData.chop(what, mlperpc01);
		what = ExternalData.chop(what, mlperpc02);
		what = ExternalData.chop(what, mlperpc03);
		what = ExternalData.chop(what, mlperpc04);
		what = ExternalData.chop(what, mlperpc05);
		what = ExternalData.chop(what, mlperpc06);
		what = ExternalData.chop(what, mlperpc07);
		what = ExternalData.chop(what, mlperpc08);
		what = ExternalData.chop(what, mlperpc09);
		what = ExternalData.chop(what, mlperpc10);
	}
	public PackedDecimalData getMlperpc(BaseData indx) {
		return getMlperpc(indx.toInt());
	}
	public PackedDecimalData getMlperpc(int indx) {

		switch (indx) {
			case 1 : return mlperpc01;
			case 2 : return mlperpc02;
			case 3 : return mlperpc03;
			case 4 : return mlperpc04;
			case 5 : return mlperpc05;
			case 6 : return mlperpc06;
			case 7 : return mlperpc07;
			case 8 : return mlperpc08;
			case 9 : return mlperpc09;
			case 10 : return mlperpc10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMlperpc(BaseData indx, Object what) {
		setMlperpc(indx, what, false);
	}
	public void setMlperpc(BaseData indx, Object what, boolean rounded) {
		setMlperpc(indx.toInt(), what, rounded);
	}
	public void setMlperpc(int indx, Object what) {
		setMlperpc(indx, what, false);
	}
	public void setMlperpc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setMlperpc01(what, rounded);
					 break;
			case 2 : setMlperpc02(what, rounded);
					 break;
			case 3 : setMlperpc03(what, rounded);
					 break;
			case 4 : setMlperpc04(what, rounded);
					 break;
			case 5 : setMlperpc05(what, rounded);
					 break;
			case 6 : setMlperpc06(what, rounded);
					 break;
			case 7 : setMlperpc07(what, rounded);
					 break;
			case 8 : setMlperpc08(what, rounded);
					 break;
			case 9 : setMlperpc09(what, rounded);
					 break;
			case 10 : setMlperpc10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getMlgrppps() {
		return new FixedLengthStringData(mlgrppp01.toInternal()
										+ mlgrppp02.toInternal()
										+ mlgrppp03.toInternal()
										+ mlgrppp04.toInternal()
										+ mlgrppp05.toInternal()
										+ mlgrppp06.toInternal()
										+ mlgrppp07.toInternal()
										+ mlgrppp08.toInternal()
										+ mlgrppp09.toInternal()
										+ mlgrppp10.toInternal());
	}
	public void setMlgrppps(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMlgrppps().getLength()).init(obj);
	
		what = ExternalData.chop(what, mlgrppp01);
		what = ExternalData.chop(what, mlgrppp02);
		what = ExternalData.chop(what, mlgrppp03);
		what = ExternalData.chop(what, mlgrppp04);
		what = ExternalData.chop(what, mlgrppp05);
		what = ExternalData.chop(what, mlgrppp06);
		what = ExternalData.chop(what, mlgrppp07);
		what = ExternalData.chop(what, mlgrppp08);
		what = ExternalData.chop(what, mlgrppp09);
		what = ExternalData.chop(what, mlgrppp10);
	}
	public PackedDecimalData getMlgrppp(BaseData indx) {
		return getMlgrppp(indx.toInt());
	}
	public PackedDecimalData getMlgrppp(int indx) {

		switch (indx) {
			case 1 : return mlgrppp01;
			case 2 : return mlgrppp02;
			case 3 : return mlgrppp03;
			case 4 : return mlgrppp04;
			case 5 : return mlgrppp05;
			case 6 : return mlgrppp06;
			case 7 : return mlgrppp07;
			case 8 : return mlgrppp08;
			case 9 : return mlgrppp09;
			case 10 : return mlgrppp10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMlgrppp(BaseData indx, Object what) {
		setMlgrppp(indx, what, false);
	}
	public void setMlgrppp(BaseData indx, Object what, boolean rounded) {
		setMlgrppp(indx.toInt(), what, rounded);
	}
	public void setMlgrppp(int indx, Object what) {
		setMlgrppp(indx, what, false);
	}
	public void setMlgrppp(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setMlgrppp01(what, rounded);
					 break;
			case 2 : setMlgrppp02(what, rounded);
					 break;
			case 3 : setMlgrppp03(what, rounded);
					 break;
			case 4 : setMlgrppp04(what, rounded);
					 break;
			case 5 : setMlgrppp05(what, rounded);
					 break;
			case 6 : setMlgrppp06(what, rounded);
					 break;
			case 7 : setMlgrppp07(what, rounded);
					 break;
			case 8 : setMlgrppp08(what, rounded);
					 break;
			case 9 : setMlgrppp09(what, rounded);
					 break;
			case 10 : setMlgrppp10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getMlgrppcs() {
		return new FixedLengthStringData(mlgrppc01.toInternal()
										+ mlgrppc02.toInternal()
										+ mlgrppc03.toInternal()
										+ mlgrppc04.toInternal()
										+ mlgrppc05.toInternal()
										+ mlgrppc06.toInternal()
										+ mlgrppc07.toInternal()
										+ mlgrppc08.toInternal()
										+ mlgrppc09.toInternal()
										+ mlgrppc10.toInternal());
	}
	public void setMlgrppcs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMlgrppcs().getLength()).init(obj);
	
		what = ExternalData.chop(what, mlgrppc01);
		what = ExternalData.chop(what, mlgrppc02);
		what = ExternalData.chop(what, mlgrppc03);
		what = ExternalData.chop(what, mlgrppc04);
		what = ExternalData.chop(what, mlgrppc05);
		what = ExternalData.chop(what, mlgrppc06);
		what = ExternalData.chop(what, mlgrppc07);
		what = ExternalData.chop(what, mlgrppc08);
		what = ExternalData.chop(what, mlgrppc09);
		what = ExternalData.chop(what, mlgrppc10);
	}
	public PackedDecimalData getMlgrppc(BaseData indx) {
		return getMlgrppc(indx.toInt());
	}
	public PackedDecimalData getMlgrppc(int indx) {

		switch (indx) {
			case 1 : return mlgrppc01;
			case 2 : return mlgrppc02;
			case 3 : return mlgrppc03;
			case 4 : return mlgrppc04;
			case 5 : return mlgrppc05;
			case 6 : return mlgrppc06;
			case 7 : return mlgrppc07;
			case 8 : return mlgrppc08;
			case 9 : return mlgrppc09;
			case 10 : return mlgrppc10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMlgrppc(BaseData indx, Object what) {
		setMlgrppc(indx, what, false);
	}
	public void setMlgrppc(BaseData indx, Object what, boolean rounded) {
		setMlgrppc(indx.toInt(), what, rounded);
	}
	public void setMlgrppc(int indx, Object what) {
		setMlgrppc(indx, what, false);
	}
	public void setMlgrppc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setMlgrppc01(what, rounded);
					 break;
			case 2 : setMlgrppc02(what, rounded);
					 break;
			case 3 : setMlgrppc03(what, rounded);
					 break;
			case 4 : setMlgrppc04(what, rounded);
					 break;
			case 5 : setMlgrppc05(what, rounded);
					 break;
			case 6 : setMlgrppc06(what, rounded);
					 break;
			case 7 : setMlgrppc07(what, rounded);
					 break;
			case 8 : setMlgrppc08(what, rounded);
					 break;
			case 9 : setMlgrppc09(what, rounded);
					 break;
			case 10 : setMlgrppc10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getMldirpps() {
		return new FixedLengthStringData(mldirpp01.toInternal()
										+ mldirpp02.toInternal()
										+ mldirpp03.toInternal()
										+ mldirpp04.toInternal()
										+ mldirpp05.toInternal()
										+ mldirpp06.toInternal()
										+ mldirpp07.toInternal()
										+ mldirpp08.toInternal()
										+ mldirpp09.toInternal()
										+ mldirpp10.toInternal());
	}
	public void setMldirpps(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMldirpps().getLength()).init(obj);
	
		what = ExternalData.chop(what, mldirpp01);
		what = ExternalData.chop(what, mldirpp02);
		what = ExternalData.chop(what, mldirpp03);
		what = ExternalData.chop(what, mldirpp04);
		what = ExternalData.chop(what, mldirpp05);
		what = ExternalData.chop(what, mldirpp06);
		what = ExternalData.chop(what, mldirpp07);
		what = ExternalData.chop(what, mldirpp08);
		what = ExternalData.chop(what, mldirpp09);
		what = ExternalData.chop(what, mldirpp10);
	}
	public PackedDecimalData getMldirpp(BaseData indx) {
		return getMldirpp(indx.toInt());
	}
	public PackedDecimalData getMldirpp(int indx) {

		switch (indx) {
			case 1 : return mldirpp01;
			case 2 : return mldirpp02;
			case 3 : return mldirpp03;
			case 4 : return mldirpp04;
			case 5 : return mldirpp05;
			case 6 : return mldirpp06;
			case 7 : return mldirpp07;
			case 8 : return mldirpp08;
			case 9 : return mldirpp09;
			case 10 : return mldirpp10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMldirpp(BaseData indx, Object what) {
		setMldirpp(indx, what, false);
	}
	public void setMldirpp(BaseData indx, Object what, boolean rounded) {
		setMldirpp(indx.toInt(), what, rounded);
	}
	public void setMldirpp(int indx, Object what) {
		setMldirpp(indx, what, false);
	}
	public void setMldirpp(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setMldirpp01(what, rounded);
					 break;
			case 2 : setMldirpp02(what, rounded);
					 break;
			case 3 : setMldirpp03(what, rounded);
					 break;
			case 4 : setMldirpp04(what, rounded);
					 break;
			case 5 : setMldirpp05(what, rounded);
					 break;
			case 6 : setMldirpp06(what, rounded);
					 break;
			case 7 : setMldirpp07(what, rounded);
					 break;
			case 8 : setMldirpp08(what, rounded);
					 break;
			case 9 : setMldirpp09(what, rounded);
					 break;
			case 10 : setMldirpp10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getMldirpcs() {
		return new FixedLengthStringData(mldirpc01.toInternal()
										+ mldirpc02.toInternal()
										+ mldirpc03.toInternal()
										+ mldirpc04.toInternal()
										+ mldirpc05.toInternal()
										+ mldirpc06.toInternal()
										+ mldirpc07.toInternal()
										+ mldirpc08.toInternal()
										+ mldirpc09.toInternal()
										+ mldirpc10.toInternal());
	}
	public void setMldirpcs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getMldirpcs().getLength()).init(obj);
	
		what = ExternalData.chop(what, mldirpc01);
		what = ExternalData.chop(what, mldirpc02);
		what = ExternalData.chop(what, mldirpc03);
		what = ExternalData.chop(what, mldirpc04);
		what = ExternalData.chop(what, mldirpc05);
		what = ExternalData.chop(what, mldirpc06);
		what = ExternalData.chop(what, mldirpc07);
		what = ExternalData.chop(what, mldirpc08);
		what = ExternalData.chop(what, mldirpc09);
		what = ExternalData.chop(what, mldirpc10);
	}
	public PackedDecimalData getMldirpc(BaseData indx) {
		return getMldirpc(indx.toInt());
	}
	public PackedDecimalData getMldirpc(int indx) {

		switch (indx) {
			case 1 : return mldirpc01;
			case 2 : return mldirpc02;
			case 3 : return mldirpc03;
			case 4 : return mldirpc04;
			case 5 : return mldirpc05;
			case 6 : return mldirpc06;
			case 7 : return mldirpc07;
			case 8 : return mldirpc08;
			case 9 : return mldirpc09;
			case 10 : return mldirpc10;
			default: return null; // Throw error instead?
		}
	
	}
	public void setMldirpc(BaseData indx, Object what) {
		setMldirpc(indx, what, false);
	}
	public void setMldirpc(BaseData indx, Object what, boolean rounded) {
		setMldirpc(indx.toInt(), what, rounded);
	}
	public void setMldirpc(int indx, Object what) {
		setMldirpc(indx, what, false);
	}
	public void setMldirpc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setMldirpc01(what, rounded);
					 break;
			case 2 : setMldirpc02(what, rounded);
					 break;
			case 3 : setMldirpc03(what, rounded);
					 break;
			case 4 : setMldirpc04(what, rounded);
					 break;
			case 5 : setMldirpc05(what, rounded);
					 break;
			case 6 : setMldirpc06(what, rounded);
					 break;
			case 7 : setMldirpc07(what, rounded);
					 break;
			case 8 : setMldirpc08(what, rounded);
					 break;
			case 9 : setMldirpc09(what, rounded);
					 break;
			case 10 : setMldirpc10(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		agntcoy.clear();
		agntnum.clear();
		acctyr.clear();
		mnth.clear();
		cnttype.clear();
		effdate.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		jobName.clear();
		userProfile.clear();
		datime.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		mlperpp01.clear();
		mlperpp02.clear();
		mlperpp03.clear();
		mlperpp04.clear();
		mlperpp05.clear();
		mlperpp06.clear();
		mlperpp07.clear();
		mlperpp08.clear();
		mlperpp09.clear();
		mlperpp10.clear();
		mlperpc01.clear();
		mlperpc02.clear();
		mlperpc03.clear();
		mlperpc04.clear();
		mlperpc05.clear();
		mlperpc06.clear();
		mlperpc07.clear();
		mlperpc08.clear();
		mlperpc09.clear();
		mlperpc10.clear();
		mldirpp01.clear();
		mldirpp02.clear();
		mldirpp03.clear();
		mldirpp04.clear();
		mldirpp05.clear();
		mldirpp06.clear();
		mldirpp07.clear();
		mldirpp08.clear();
		mldirpp09.clear();
		mldirpp10.clear();
		mldirpc01.clear();
		mldirpc02.clear();
		mldirpc03.clear();
		mldirpc04.clear();
		mldirpc05.clear();
		mldirpc06.clear();
		mldirpc07.clear();
		mldirpc08.clear();
		mldirpc09.clear();
		mldirpc10.clear();
		mlgrppp01.clear();
		mlgrppp02.clear();
		mlgrppp03.clear();
		mlgrppp04.clear();
		mlgrppp05.clear();
		mlgrppp06.clear();
		mlgrppp07.clear();
		mlgrppp08.clear();
		mlgrppp09.clear();
		mlgrppp10.clear();
		mlgrppc01.clear();
		mlgrppc02.clear();
		mlgrppc03.clear();
		mlgrppc04.clear();
		mlgrppc05.clear();
		mlgrppc06.clear();
		mlgrppc07.clear();
		mlgrppc08.clear();
		mlgrppc09.clear();
		mlgrppc10.clear();
		cntcount.clear();
		sumins.clear();		
	}


}