/*
 * File: P6703.java
 * Date: 30 August 2009 0:54:03
 * Author: Quipoz Limited
 * 
 * Class transformed from P6703.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.statistics.dataaccess.AgjnTableDAM;
import com.csc.life.statistics.dataaccess.AgstTableDAM;
import com.csc.life.statistics.screens.S6703ScreenVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*          CREATE AGENT STATISTICAL JOURNAL  -  SCREEN 1
*
* This program accepts the agent statistical accumulation key via
* screen S6703. A journal record is created in AGJN file for every
* transaction committed. The key (AGST-DATA-KEY) entered will be
* kept and passed to the next program using KEEPS on AGST and
* AGJN files.
*
* If the AGST record is not found for the key entered, a new record
* with the new key would be created in AGST file. The initialisation
* of the new record is done in this program before a KEEPS action is
* performed on AGST file.
*
* Validation performed:
*
* Skip this section  if  returning  from  an optional selection
* (current stack position action flag = '*').
*
* - Accounting year and month must not be zeroes. It is mandatory
*   that the user enter these fields.
*
* - Overriding category (i.e. S6703-OVRDCAT) must be only 'B' or
*   'O'.
*
* - It is possible to enter a journal record to update AGST file
*   regardless of whether the accounting period is opened or closed.
*
* FUNCTION KEYS:
*               <EXIT>  Return to submenu.
*
*               <ENTER> Call the next update program P6706 to allow
*                       user to update the amount fields in AGST file.
*
* FILES USED:
*               AGSTSKM
*               AGJNSKM
*
*****************************************************************
* </pre>
*/
public class P6703 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6703");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaScrAgntsel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaAgntsel = new FixedLengthStringData(8).isAPartOf(wsaaScrAgntsel, 0);
	private FixedLengthStringData wsaaFiller = new FixedLengthStringData(2).isAPartOf(wsaaScrAgntsel, 8);
		/* ERRORS */
	private String e186 = "E186";
	private String t039 = "T039";
		/* FORMATS */
	private String agstrec = "AGSTREC";
	private String agjnrec = "AGJNREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
		/*Agent Statistical Accum. Journal*/
	private AgjnTableDAM agjnIO = new AgjnTableDAM();
		/*Agent Statistical Accumulation File*/
	private AgstTableDAM agstIO = new AgstTableDAM();
	private S6703ScreenVars sv = ScreenProgram.getScreenVars( S6703ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit1090, 
		preExit, 
		exit3090, 
		exit4090
	}

	public P6703() {
		super();
		screenVars = sv;
		new ScreenModel("S6703", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		try {
			initialise1010();
		}
		catch (GOTOException e){
		}
	}

protected void initialise1010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit1090);
		}
		sv.dataArea.set(SPACES);
		sv.acctyr.set(ZERO);
		sv.acmn.set(ZERO);
		sv.company.set(wsspcomn.company);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.edterror.set(varcom.oK);
			wsspcomn.sectionno.set("3000");
			goTo(GotoLabel.preExit);
		}
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		screenIo2010();
		validate2020();
		checkForErrors2080();
	}

protected void screenIo2010()
	{
		wsspcomn.edterror.set(varcom.oK);
	}

protected void validate2020()
	{
		if (isEQ(sv.acctyr,ZERO)) {
			sv.acctyrErr.set(e186);
		}
		if (isEQ(sv.acmn,ZERO)
		|| isGT(sv.acmn,12)) {
			sv.acmnErr.set(e186);
		}
		if (isNE(sv.ovrdcat,"B")
		&& isNE(sv.ovrdcat,"O")) {
			sv.ovrdcatErr.set(t039);
		}
		wsaaScrAgntsel.set(sv.agntsel);
		if (isEQ(sv.agntsel,SPACES)) {
			sv.agntselErr.set(e186);
		}
		if (isEQ(sv.statcat,SPACES)) {
			sv.statcatErr.set(e186);
		}
		if (isEQ(sv.aracde,SPACES)) {
			sv.aracdeErr.set(e186);
		}
		if (isEQ(sv.cntbranch,SPACES)) {
			sv.cntbranchErr.set(e186);
		}
		if (isEQ(sv.chdrtype,SPACES)) {
			sv.chdrtypeErr.set(e186);
		}
		if (isEQ(sv.crtable,SPACES)) {
			sv.crtableErr.set(e186);
		}
		if (isEQ(sv.ovrdcat,SPACES)) {
			sv.ovrdcatErr.set(e186);
		}
		if (isEQ(sv.pstatcd,SPACES)) {
			sv.pstatcdErr.set(e186);
		}
		if (isEQ(sv.cntcurr,SPACES)) {
			sv.cntcurrErr.set(e186);
		}
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		else {
			keepRecord2100();
		}
		/*EXIT*/
	}

protected void keepRecord2100()
	{
		moveScreenToFiles2110();
		keepAgstRecord2120();
		keepAgjnRecord2120();
	}

protected void moveScreenToFiles2110()
	{
		agstIO.setChdrcoy(wsspcomn.company);
		agjnIO.setChdrcoy(wsspcomn.company);
		agstIO.setAgntnum(wsaaAgntsel);
		agjnIO.setAgntnum(wsaaAgntsel);
		agstIO.setStatcat(sv.statcat);
		agjnIO.setStatcat(sv.statcat);
		agstIO.setAcctyr(sv.acctyr);
		agjnIO.setAcctyr(sv.acctyr);
		agstIO.setAracde(sv.aracde);
		agjnIO.setAracde(sv.aracde);
		agstIO.setCntbranch(sv.cntbranch);
		agjnIO.setCntbranch(sv.cntbranch);
		agstIO.setBandage(sv.bandage);
		agjnIO.setBandage(sv.bandage);
		agstIO.setBandsa(sv.bandsa);
		agjnIO.setBandsa(sv.bandsa);
		agstIO.setBandprm(sv.bandprm);
		agjnIO.setBandprm(sv.bandprm);
		agstIO.setBandtrm(sv.bandtrm);
		agjnIO.setBandtrm(sv.bandtrm);
		agstIO.setCnttype(sv.chdrtype);
		agjnIO.setCnttype(sv.chdrtype);
		agstIO.setCrtable(sv.crtable);
		agjnIO.setCrtable(sv.crtable);
		agstIO.setOvrdcat(sv.ovrdcat);
		agjnIO.setOvrdcat(sv.ovrdcat);
		agstIO.setCntcurr(sv.cntcurr);
		agjnIO.setCntcurr(sv.cntcurr);
		agstIO.setPstatcode(sv.pstatcd);
		agjnIO.setPstatcode(sv.pstatcd);
		agjnIO.setAcctmonth(sv.acmn);
		agjnIO.setTransactionDate(ZERO);
		agjnIO.setTransactionTime(ZERO);
		agjnIO.setEffdate(ZERO);
		agjnIO.setUser(ZERO);
		agjnIO.setStcmth(ZERO);
		agjnIO.setStvmth(ZERO);
		agjnIO.setStpmth(ZERO);
		agjnIO.setStsmth(ZERO);
		agjnIO.setStmmth(ZERO);
	}

protected void keepAgstRecord2120()
	{
		agstIO.setFormat(agstrec);
		agstIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)
		&& isNE(agstIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(agstIO.getParams());
			fatalError600();
		}
		if (isEQ(agstIO.getStatuz(),varcom.mrnf)) {
			wsaaI.set(1);
			agstIO.setBfwdc(ZERO);
			agstIO.setCfwdc(ZERO);
			agstIO.setBfwdv(ZERO);
			agstIO.setCfwdv(ZERO);
			agstIO.setBfwdp(ZERO);
			agstIO.setCfwdp(ZERO);
			agstIO.setBfwds(ZERO);
			agstIO.setCfwds(ZERO);
			agstIO.setBfwdm(ZERO);
			agstIO.setCfwdm(ZERO);
			for (wsaaI.set(1); !(isGT(wsaaI,12)); wsaaI.add(1)){
				initialiseAgstValues2200();
			}
		}
		agstIO.setFormat(agstrec);
		agstIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agstIO.getParams());
			fatalError600();
		}
	}

protected void keepAgjnRecord2120()
	{
		agjnIO.setFormat(agjnrec);
		agjnIO.setFunction(varcom.keeps);
		SmartFileCode.execute(appVars, agjnIO);
		if (isNE(agjnIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(agjnIO.getParams());
			fatalError600();
		}
		/*EXIT*/
	}

protected void initialiseAgstValues2200()
	{
		/*PARA*/
		agstIO.setStcmth(wsaaI, ZERO);
		agstIO.setStvmth(wsaaI, ZERO);
		agstIO.setStpmth(wsaaI, ZERO);
		agstIO.setStsmth(wsaaI, ZERO);
		agstIO.setStmmth(wsaaI, ZERO);
		/*EXIT*/
	}

protected void update3000()
	{
		try {
			updateDatabase3010();
		}
		catch (GOTOException e){
		}
	}

protected void updateDatabase3010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			goTo(GotoLabel.exit3090);
		}
	}

protected void whereNext4000()
	{
		try {
			nextProgram4010();
		}
		catch (GOTOException e){
		}
	}

protected void nextProgram4010()
	{
		if (isEQ(wsspcomn.secActn[wsspcomn.programPtr.toInt()],"*")) {
			wsspcomn.secActn[wsspcomn.programPtr.toInt()].set(SPACES);
			goTo(GotoLabel.exit4090);
		}
		/*bug #ILIFE-1070 start*/
		//wsspcomn.secActn[wsspcomn.programPtr.toInt()].set("*");
		/*bug #ILIFE-1070 end*/
		wsspcomn.programPtr.add(1);
	}
}
