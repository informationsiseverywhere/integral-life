package com.csc.life.statistics.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6713screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6713ScreenVars sv = (S6713ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6713screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6713ScreenVars screenVars = (S6713ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.stcmth.setClassString("");
		screenVars.stpmth.setClassString("");
		screenVars.stsmth.setClassString("");
		screenVars.stimth.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.acyr.setClassString("");
		screenVars.acmn.setClassString("");
		screenVars.stcmthAdj.setClassString("");
		screenVars.stpmthAdj.setClassString("");
		screenVars.stsmthAdj.setClassString("");
		screenVars.stimthAdj.setClassString("");
		screenVars.stcmtho.setClassString("");
		screenVars.stpmtho.setClassString("");
		screenVars.stsmtho.setClassString("");
		screenVars.stimtho.setClassString("");
		screenVars.statcat.setClassString("");
		screenVars.acctyr.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.stsubsect.setClassString("");
		screenVars.register.setClassString("");
		screenVars.cntbranch.setClassString("");
		screenVars.bandage.setClassString("");
		screenVars.bandsa.setClassString("");
		screenVars.bandprm.setClassString("");
		screenVars.bandtrm.setClassString("");
		screenVars.pstatcd.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.mthldesc.setClassString("");
		screenVars.commyr.setClassString("");
	}

/**
 * Clear all the variables in S6713screen
 */
	public static void clear(VarModel pv) {
		S6713ScreenVars screenVars = (S6713ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.stcmth.clear();
		screenVars.stpmth.clear();
		screenVars.stsmth.clear();
		screenVars.stimth.clear();
		screenVars.descrip.clear();
		screenVars.acyr.clear();
		screenVars.acmn.clear();
		screenVars.stcmthAdj.clear();
		screenVars.stpmthAdj.clear();
		screenVars.stsmthAdj.clear();
		screenVars.stimthAdj.clear();
		screenVars.stcmtho.clear();
		screenVars.stpmtho.clear();
		screenVars.stsmtho.clear();
		screenVars.stimtho.clear();
		screenVars.statcat.clear();
		screenVars.acctyr.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.stsubsect.clear();
		screenVars.register.clear();
		screenVars.cntbranch.clear();
		screenVars.bandage.clear();
		screenVars.bandsa.clear();
		screenVars.bandprm.clear();
		screenVars.bandtrm.clear();
		screenVars.pstatcd.clear();
		screenVars.cntcurr.clear();
		screenVars.mthldesc.clear();
		screenVars.commyr.clear();
	}
}
