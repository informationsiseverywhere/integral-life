package com.csc.life.statistics.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RJ530.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rj530Report extends SMARTReportLayout { 

	private FixedLengthStringData acctccy = new FixedLengthStringData(3);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData newtotsi01 = new ZonedDecimalData(12, 0);
	private ZonedDecimalData newtotsi02 = new ZonedDecimalData(12, 0);
	private ZonedDecimalData newtotsi03 = new ZonedDecimalData(12, 0);
	private ZonedDecimalData newtotsi04 = new ZonedDecimalData(12, 0);
	private ZonedDecimalData newtotsi05 = new ZonedDecimalData(12, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData register = new FixedLengthStringData(3);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData stcmthg = new ZonedDecimalData(9, 0);
	private ZonedDecimalData stlmth = new ZonedDecimalData(9, 0);
	private FixedLengthStringData stsect = new FixedLengthStringData(2);
	private FixedLengthStringData stssect = new FixedLengthStringData(4);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rj530Report() {
		super();
	}


	/**
	 * Print the XML for Rj530d01
	 */
	public void printRj530d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(17).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		cntdesc.setFieldName("cntdesc");
		cntdesc.setInternal(subString(recordData, 1, 30));
		printLayout("Rj530d01",			// Record name
			new BaseData[]{			// Fields:
				cntdesc
			}
			, new Object[] {			// indicators
				new Object[]{"ind16", indicArea.charAt(16)},
				new Object[]{"ind17", indicArea.charAt(17)}
			}
		);

	}

	/**
	 * Print the XML for Rj530d02
	 */
	public void printRj530d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(24).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stcmthg.setFieldName("stcmthg");
		stcmthg.setInternal(subString(recordData, 1, 9));
		stlmth.setFieldName("stlmth");
		stlmth.setInternal(subString(recordData, 10, 9));
		newtotsi01.setFieldName("newtotsi01");
		newtotsi01.setInternal(subString(recordData, 19, 12));
		newtotsi02.setFieldName("newtotsi02");
		newtotsi02.setInternal(subString(recordData, 31, 12));
		newtotsi03.setFieldName("newtotsi03");
		newtotsi03.setInternal(subString(recordData, 43, 12));
		newtotsi04.setFieldName("newtotsi04");
		newtotsi04.setInternal(subString(recordData, 55, 12));
		newtotsi05.setFieldName("newtotsi05");
		newtotsi05.setInternal(subString(recordData, 67, 12));
		printLayout("Rj530d02",			// Record name
			new BaseData[]{			// Fields:
				stcmthg,
				stlmth,
				newtotsi01,
				newtotsi02,
				newtotsi03,
				newtotsi04,
				newtotsi05
			}
			, new Object[] {			// indicators
				new Object[]{"ind13", indicArea.charAt(13)},
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind15", indicArea.charAt(15)},
				new Object[]{"ind18", indicArea.charAt(18)},
				new Object[]{"ind19", indicArea.charAt(19)},
				new Object[]{"ind20", indicArea.charAt(20)},
				new Object[]{"ind21", indicArea.charAt(21)},
				new Object[]{"ind22", indicArea.charAt(22)},
				new Object[]{"ind23", indicArea.charAt(23)},
				new Object[]{"ind24", indicArea.charAt(24)}
			}
		);

		currentPrintLine.add(1);
	}

	/**
	 * Print the XML for Rj530d03
	 */
	public void printRj530d03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rj530d03",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rj530h01
	 */
	public void printRj530h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		acctmonth.setFieldName("acctmonth");
		acctmonth.setInternal(subString(recordData, 1, 2));
		acctyear.setFieldName("acctyear");
		acctyear.setInternal(subString(recordData, 3, 4));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 7, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 8, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 38, 10));
		acctccy.setFieldName("acctccy");
		acctccy.setInternal(subString(recordData, 48, 3));
		currdesc.setFieldName("currdesc");
		currdesc.setInternal(subString(recordData, 51, 30));
		time.setFieldName("time");
		time.set(getTime());
		register.setFieldName("register");
		register.setInternal(subString(recordData, 81, 3));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 84, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		stsect.setFieldName("stsect");
		stsect.setInternal(subString(recordData, 114, 2));
		itmdesc.setFieldName("itmdesc");
		itmdesc.setInternal(subString(recordData, 116, 30));
		stssect.setFieldName("stssect");
		stssect.setInternal(subString(recordData, 146, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 150, 30));
		printLayout("Rj530h01",			// Record name
			new BaseData[]{			// Fields:
				acctmonth,
				acctyear,
				company,
				companynm,
				sdate,
				acctccy,
				currdesc,
				time,
				register,
				descrip,
				pagnbr,
				stsect,
				itmdesc,
				stssect,
				longdesc
			}
		);

		currentPrintLine.set(11);
	}

	/**
	 * Print the XML for Rj530h02
	 */
	public void printRj530h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(12).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);
		printLayout("Rj530h02",			// Record name
			new BaseData[]{			// Fields:
			}
			, new Object[] {			// indicators
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind12", indicArea.charAt(12)}
			}
		);
		currentPrintLine.add(2);
	}


}
