package com.csc.life.statistics.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R6522.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class R6522Report extends SMARTReportLayout { 

	private FixedLengthStringData accumkey = new FixedLengthStringData(41);
	private FixedLengthStringData batchkey = new FixedLengthStringData(22);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private ZonedDecimalData contractc = new ZonedDecimalData(2, 0);
	private ZonedDecimalData coverc = new ZonedDecimalData(2, 0);
	private FixedLengthStringData currcode = new FixedLengthStringData(3);
	private FixedLengthStringData currencynm = new FixedLengthStringData(30);
	private FixedLengthStringData jobname = new FixedLengthStringData(8);
	private ZonedDecimalData jobnum = new ZonedDecimalData(8, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData stmmth = new ZonedDecimalData(18, 2);
	private FixedLengthStringData stperiod = new FixedLengthStringData(7);
	private ZonedDecimalData stpmth = new ZonedDecimalData(18, 2);
	private ZonedDecimalData stsmth = new ZonedDecimalData(18, 2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData totaprem = new ZonedDecimalData(18, 2);
	private ZonedDecimalData totccount = new ZonedDecimalData(9, 0);
	private ZonedDecimalData totcommis = new ZonedDecimalData(18, 2);
	private ZonedDecimalData totsprem = new ZonedDecimalData(18, 2);
	private ZonedDecimalData totvcount = new ZonedDecimalData(9, 0);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R6522Report() {
		super();
	}


	/**
	 * Print the XML for R6522d01
	 */
	public void printR6522d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		batchkey.setFieldName("batchkey");
		batchkey.setInternal(subString(recordData, 1, 22));
		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 23, 8));
		printLayout("R6522d01",			// Record name
			new BaseData[]{			// Fields:
				batchkey,
				chdrnum
			}
		);

	}

	/**
	 * Print the XML for R6522d02
	 */
	public void printR6522d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		accumkey.setFieldName("accumkey");
		accumkey.setInternal(subString(recordData, 1, 41));
		contractc.setFieldName("contractc");
		contractc.setInternal(subString(recordData, 42, 2));
		coverc.setFieldName("coverc");
		coverc.setInternal(subString(recordData, 44, 2));
		stpmth.setFieldName("stpmth");
		stpmth.setInternal(subString(recordData, 46, 18));
		stsmth.setFieldName("stsmth");
		stsmth.setInternal(subString(recordData, 64, 18));
		stmmth.setFieldName("stmmth");
		stmmth.setInternal(subString(recordData, 82, 18));
		printLayout("R6522d02",			// Record name
			new BaseData[]{			// Fields:
				accumkey,
				contractc,
				coverc,
				stpmth,
				stsmth,
				stmmth
			}
		);

	}

	/**
	 * Print the XML for R6522d03
	 */
	public void printR6522d03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		totccount.setFieldName("totccount");
		totccount.setInternal(subString(recordData, 1, 9));
		totvcount.setFieldName("totvcount");
		totvcount.setInternal(subString(recordData, 10, 9));
		totaprem.setFieldName("totaprem");
		totaprem.setInternal(subString(recordData, 19, 18));
		totsprem.setFieldName("totsprem");
		totsprem.setInternal(subString(recordData, 37, 18));
		totcommis.setFieldName("totcommis");
		totcommis.setInternal(subString(recordData, 55, 18));
		printLayout("R6522d03",			// Record name
			new BaseData[]{			// Fields:
				totccount,
				totvcount,
				totaprem,
				totsprem,
				totcommis
			}
		);

	}

	/**
	 * Print the XML for R6522h01
	 */
	public void printR6522h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stperiod.setFieldName("stperiod");
		stperiod.setInternal(subString(recordData, 1, 7));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 8, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 9, 30));
		jobname.setFieldName("jobname");
		jobname.setInternal(subString(recordData, 39, 8));
		jobnum.setFieldName("jobnum");
		jobnum.setInternal(subString(recordData, 47, 8));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 55, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 65, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 67, 30));
		time.setFieldName("time");
		time.set(getTime());
		currcode.setFieldName("currcode");
		currcode.setInternal(subString(recordData, 97, 3));
		currencynm.setFieldName("currencynm");
		currencynm.setInternal(subString(recordData, 100, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		printLayout("R6522h01",			// Record name
			new BaseData[]{			// Fields:
				stperiod,
				company,
				companynm,
				jobname,
				jobnum,
				sdate,
				branch,
				branchnm,
				time,
				currcode,
				currencynm,
				pagnbr
			}
		);

		currentPrintLine.set(14);
	}


}
