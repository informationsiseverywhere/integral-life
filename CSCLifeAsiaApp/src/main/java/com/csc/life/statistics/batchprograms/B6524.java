/*
 * File: B6524.java
 * Date: 29 August 2009 21:22:16
 * Author: Quipoz Limited
 *
 * Class transformed from B6524.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.life.statistics.dataaccess.SttrTableDAM;
import com.csc.life.statistics.reports.R6524Report;
import com.csc.smart.dataaccess.BakyTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*
*        GOVERNMENT STATISTICAL MOVEMENT POSTING AUDIT REPORT
*
*
*   This program prints Govt. Statistical Movement Posting records
*   that are extracted by the batch extraction program B0236.
*
*   The basic procedure division logic is for reading via SQL and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*
*     - retrieve and set up standard report headings.
*
*     - The selected STTR records will be arranged in the following
*       sequence (major to minor) through an SQL SELECT
*       statement:
*
*                        From STTR file
*                        --------------
*
*      Batch key:        BATCCOY
*                        BATCBRN
*                        BATCACTYR
*                        BATCACTMN
*                        BATCTRCDE
*                        BATCBATCH
*
*      Currency:         CNTCURR
*
*      Contract number:  CHDRNUM
*
*      STTR information: STATCAT
*                        STFUND
*                        STSECT
*                        STSSECT
*                        REG
*                        CNTBRANCH
*                        BANDAGEG
*                        BANDSAG
*                        BANDPRMG
*                        BANDTRMG
*                        COMMYR
*                        PSTATCODE
*
*
*
*    Write details to report while not primary file EOF (BAKY)
*     - fetch a record from SQL-POSTPF1 for output details
*
*     - if new page, write page headings (R6522-H01)
*     - batch key and contract number are group indicate items
*       that is, print detail line 1 (R6522-D01) only if
*       batch-key and contract number changes from the previous
*       record.
*
*
*       WSAA-BATCKEY contains the current record batch details - STTR
*       WSAA-CHDRNUM contains the current contract number      - STTR
*
*
*       WSAA-PREV-BATCKEY contains the previous record's batch
*                         information.
*
*       WSAA-PREV-CHDRNUM contains the previous record's contract
*                          number information.
*
*     - print detail line 2 for every STTR record selected (R6522-D02).
*
*     - print sub-total line for R6522-D03 if the batch details and
*       contract number changes.
*
*     - read next primary file record
*
*
*
*
*   Control totals:
*     01  -  Number of pages printed
*     02  -  Number of STTRs extracted
*     03  -  Total no. of contract count
*     04  -  Total amount of annual premium
*     05  -  Total amount of single premium
*     06  -  Total amount of sum insured
*
*
*
*****************************************************************
* </pre>
*/
public class B6524 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlpostpf1rs = null;
	private java.sql.PreparedStatement sqlpostpf1ps = null;
	private java.sql.Connection sqlpostpf1conn = null;
	private String sqlpostpf1 = "";
	private R6524Report printerFile = new R6524Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6524");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaPrevCurr = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaPrevBatckey = new FixedLengthStringData(18);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaStatgovYes = new FixedLengthStringData(1).init("Y");
	private String wsaaFirstTime = "";
	private ZonedDecimalData wsaaTotccount = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaTotaprem = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaTotsprem = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaTotsumins = new ZonedDecimalData(18, 2);

	private FixedLengthStringData wsaaAcctperiod = new FixedLengthStringData(7);
	private ZonedDecimalData wsaaPerMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaAcctperiod, 0).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaAcctperiod, 2, FILLER).init("/");
	private ZonedDecimalData wsaaPerYy = new ZonedDecimalData(4, 0).isAPartOf(wsaaAcctperiod, 3).setUnsigned();
	private FixedLengthStringData wsaaParmCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaParmJobname = new FixedLengthStringData(8);
	private PackedDecimalData wsaaParmAcctyear = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaParmAcctmonth = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaParmJobnum = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaRunparm1 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaParmCp = new FixedLengthStringData(5).isAPartOf(wsaaRunparm1, 0);
	private FixedLengthStringData wsaaParmJs = new FixedLengthStringData(3).isAPartOf(wsaaRunparm1, 5);

	private FixedLengthStringData wsaaBatckey = new FixedLengthStringData(18);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaBatckey, 0);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 1);
	private FixedLengthStringData wsaaBatcactyr = new FixedLengthStringData(4).isAPartOf(wsaaBatckey, 3);
	private FixedLengthStringData wsaaBatcactmn = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 7);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatckey, 9);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaBatckey, 13);

		/* FORMATS */
	private static final String descrec = "DESCREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler3, 5);

		/* SQLA-POSTPF1 */

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/*  Detail line - add as many detail and total lines as required.
		              - use redefines to save WS space where applicable.*/

	private FixedLengthStringData r6524D01 = new FixedLengthStringData(30);
	private FixedLengthStringData r6524d01O = new FixedLengthStringData(30).isAPartOf(r6524D01, 0);
	private FixedLengthStringData rd01Batchkey = new FixedLengthStringData(22).isAPartOf(r6524d01O, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r6524d01O, 22);

	private FixedLengthStringData r6524D02 = new FixedLengthStringData(97);
	private FixedLengthStringData r6524d02O = new FixedLengthStringData(97).isAPartOf(r6524D02, 0);
	private FixedLengthStringData rd02Accumkey = new FixedLengthStringData(41).isAPartOf(r6524d02O, 0);
	private ZonedDecimalData rd02Contractc = new ZonedDecimalData(2, 0).isAPartOf(r6524d02O, 41);
	private ZonedDecimalData rd02Stpmth = new ZonedDecimalData(18, 2).isAPartOf(r6524d02O, 43);
	private ZonedDecimalData rd02Stsmth = new ZonedDecimalData(18, 2).isAPartOf(r6524d02O, 61);
	private ZonedDecimalData rd02Stimth = new ZonedDecimalData(18, 2).isAPartOf(r6524d02O, 79);

	private FixedLengthStringData r6524D03 = new FixedLengthStringData(63);
	private FixedLengthStringData r6524d03O = new FixedLengthStringData(63).isAPartOf(r6524D03, 0);
	private ZonedDecimalData rd03Totccount = new ZonedDecimalData(9, 0).isAPartOf(r6524d03O, 0);
	private ZonedDecimalData rd03Totaprem = new ZonedDecimalData(18, 2).isAPartOf(r6524d03O, 9);
	private ZonedDecimalData rd03Totsprem = new ZonedDecimalData(18, 2).isAPartOf(r6524d03O, 27);
	private ZonedDecimalData rd03Totsumins = new ZonedDecimalData(18, 2).isAPartOf(r6524d03O, 45);
		/*Logical File: Extracted Batch Keys*/
	private BakyTableDAM bakyIO = new BakyTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Agent Statistics Movement Logical File*/
	private SttrTableDAM sttrIO = new SttrTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private R6524H01Inner r6524H01Inner = new R6524H01Inner();
	private SqlaPostpf1Inner sqlaPostpf1Inner = new SqlaPostpf1Inner();
	private WsaaAccumulationKeyInner wsaaAccumulationKeyInner = new WsaaAccumulationKeyInner();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit190,
		eof580,
		exit590
	}

	public B6524() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		try {
			main110();
		}
		catch (GOTOException e){
		}
	}
protected void main110()
	{
		initialise200();
		readFirstRecord300();
		while ( !(endOfFile.isTrue())) {
			printReport400();
		}

		finished900();
		goTo(GotoLabel.exit190);
	}
protected void sqlError180()
	{
		sqlError();
	}

protected void initialise200()
	{
		initialise210();
		setUpHeadingCompany210();
		setUpHeadingBranch220();
		setUpHeadingDates240();
	}

protected void initialise210()
	{
		wsaaBatckey.set(SPACES);
		wsaaAccumulationKeyInner.wsaaAccumulationKey.set(SPACES);
		wsaaPrevCurr.set(SPACES);
		wsaaPrevBatckey.set(SPACES);
		wsaaPrevChdrnum.set(SPACES);
		wsaaFirstTime = "Y";
		wsaaParmCompany.set(runparmrec.company);
		wsaaParmJobname.set(runparmrec.jobname);
		wsaaParmAcctyear.set(runparmrec.acctyear);
		wsaaParmAcctmonth.set(runparmrec.acctmonth);
		wsaaParmJobnum.set(runparmrec.jobnum);
		wsaaRunparm1.set(runparmrec.runparm1);
		wsaaTotccount.set(ZERO);
		wsaaTotaprem.set(ZERO);
		wsaaTotsprem.set(ZERO);
		wsaaTotsumins.set(ZERO);
		printerFile.openOutput();
	}

protected void setUpHeadingCompany210()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(runparmrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		r6524H01Inner.rh01Company.set(runparmrec.company);
		r6524H01Inner.rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch220()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(runparmrec.branch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		r6524H01Inner.rh01Branch.set(runparmrec.branch);
		r6524H01Inner.rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates240()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r6524H01Inner.rh01Sdate.set(datcon1rec.extDate);
		/*SET-UP-JOB-DETAILS*/
		wsaaPerMm.set(runparmrec.acctmonth);
		wsaaPerYy.set(runparmrec.acctyear);
		r6524H01Inner.rh01Stperiod.set(wsaaAcctperiod);
		r6524H01Inner.rh01Jobname.set(runparmrec.jobname);
		r6524H01Inner.rh01Jobnum.set(runparmrec.jobnum);
	}

protected void readFirstRecord300()
	{
		/*BEGIN-READING*/
		/* This statement selects all records STTR records that have a batc*/
		/* record created in BAKY file.*/
		/*  Define the query required by declaring a cursor*/

		sqlpostpf1 = " SELECT  ST.BATCCOY, ST.BATCBRN, BK.BATCACTYR, ST.BATCACTMN, ST.BATCTRCDE, ST.BATCBATCH, ST.CHDRCOY, ST.STATCAT, ST.BATCACTYR, ST.STFUND, ST.STSECT, ST.STSSECT, ST.REG, ST.CNTBRANCH, ST.BANDAGEG, ST.BANDSAG, ST.BANDPRMG, ST.BANDTRMG, ST.COMMYR, ST.PSTATCODE, ST.CNTCURR, ST.CHDRNUM, ST.STCMTHG, ST.STPMTHG, ST.STSMTHG, ST.STIMTH" +
" FROM   " + getAppVars().getTableNameOverriden("BAKYPF") + "  BK,  " + getAppVars().getTableNameOverriden("STTRPF") + "  ST" +
" WHERE BK.JCTLJBN = ?" +
" AND BK.JCTLCOY = ?" +
" AND BK.JCTLJN = ?" +
" AND BK.JCTLACTYR = ?" +
" AND BK.JCTLACTMN = ?" +
" AND BK.BATCCOY = ST.BATCCOY" +
" AND BK.BATCBRN = ST.BATCBRN" +
" AND BK.BATCACTYR = ST.BATCACTYR" +
" AND BK.BATCACTMN = ST.BATCACTMN" +
" AND BK.BATCTRCDE = ST.BATCTRCDE" +
" AND BK.BATCBATCH = ST.BATCBATCH" +
" AND BK.JOBSTEP = ?" +
" AND ST.STATGOV = ?" +
" ORDER BY ST.BATCCOY, ST.BATCBRN, ST.BATCACTYR, ST.BATCACTMN, ST.BATCTRCDE, ST.BATCBATCH, ST.CNTCURR, ST.CHDRNUM, ST.STATCAT, ST.STFUND, ST.STSECT, ST.STSSECT, ST.REG, ST.CNTBRANCH, ST.BANDAGEG, ST.BANDSAG, ST.BANDPRMG, ST.BANDTRMG, ST.COMMYR, ST.PSTATCODE";
		sqlerrorflag = false;
		try {
			sqlpostpf1conn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.smart.dataaccess.BakypfTableDAM(), new com.csc.life.statistics.dataaccess.SttrpfTableDAM()});
			sqlpostpf1ps = getAppVars().prepareStatementEmbeded(sqlpostpf1conn, sqlpostpf1);
			getAppVars().setDBNumber(sqlpostpf1ps, 1, wsaaParmJobnum);
			getAppVars().setDBString(sqlpostpf1ps, 2, wsaaParmCompany);
			getAppVars().setDBString(sqlpostpf1ps, 3, wsaaParmJobname);
			getAppVars().setDBNumber(sqlpostpf1ps, 4, wsaaParmAcctyear);
			getAppVars().setDBNumber(sqlpostpf1ps, 5, wsaaParmAcctmonth);
			getAppVars().setDBString(sqlpostpf1ps, 6, wsaaParmJs);
			getAppVars().setDBString(sqlpostpf1ps, 7, wsaaStatgovYes);
			sqlpostpf1rs = getAppVars().executeQuery(sqlpostpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError180();
			throw new RuntimeException("B6524: ERROR (1) - Unexpected return from a transformed GO TO statement: 180-SQL-ERROR");
		}
		fetchPrimary500();
		if (endOfFile.isTrue()) {
			return ;
		}
	}

protected void printReport400()
	{
		writeDetail450();
		updateTotalAmounts460();
	}

protected void writeDetail450()
	{
		if (isNE(sqlaPostpf1Inner.sqlCntcurr, wsaaPrevCurr)) {
			wsaaPrevCurr.set(sqlaPostpf1Inner.sqlCntcurr);
			setUpHeadingCurrency600();
			wsaaOverflow.set("Y");
		}
		checkPageHeading800();
		wsaaBatccoy.set(sqlaPostpf1Inner.sqlBatccoy);
		wsaaBatcbrn.set(sqlaPostpf1Inner.sqlBatcbrn);
		wsaaBatcactyr.set(sqlaPostpf1Inner.sqlBatcactyr);
		wsaaBatcactmn.set(sqlaPostpf1Inner.sqlBatcactmn);
		wsaaBatctrcde.set(sqlaPostpf1Inner.sqlBatctrcde);
		wsaaBatcbatch.set(sqlaPostpf1Inner.sqlBatcbatch);
		if (isNE(wsaaBatckey,wsaaPrevBatckey)
		|| isNE(sqlaPostpf1Inner.sqlChdrnum, wsaaPrevChdrnum)) {
			wsaaPrevBatckey.set(wsaaBatckey);
			wsaaPrevChdrnum.set(sqlaPostpf1Inner.sqlChdrnum);
			printGrandTotals700();
			wsaaTotccount.set(ZERO);
			wsaaTotaprem.set(ZERO);
			wsaaTotsprem.set(ZERO);
			wsaaTotsumins.set(ZERO);
			rd01Batchkey.set(wsaaBatckey);
			rd01Chdrnum.set(sqlaPostpf1Inner.sqlChdrnum);
			printerFile.printR6524d01(r6524D01, indicArea);
		}
		checkPageHeading800();
		/* print detail line 2:*/
		wsaaAccumulationKeyInner.wsaaChdrcoy.set(sqlaPostpf1Inner.sqlChdrcoy);
		wsaaAccumulationKeyInner.wsaaStatcat.set(sqlaPostpf1Inner.sqlStatcat);
		wsaaAccumulationKeyInner.wsaaAcctyr.set(sqlaPostpf1Inner.sqlAcctyr);
		wsaaAccumulationKeyInner.wsaaStfund.set(sqlaPostpf1Inner.sqlStfund);
		wsaaAccumulationKeyInner.wsaaStsect.set(sqlaPostpf1Inner.sqlStsect);
		wsaaAccumulationKeyInner.wsaaStssect.set(sqlaPostpf1Inner.sqlStssect);
		wsaaAccumulationKeyInner.wsaaReg.set(sqlaPostpf1Inner.sqlReg);
		wsaaAccumulationKeyInner.wsaaCntbranch.set(sqlaPostpf1Inner.sqlCntbranch);
		wsaaAccumulationKeyInner.wsaaBandageg.set(sqlaPostpf1Inner.sqlBandageg);
		wsaaAccumulationKeyInner.wsaaBandsag.set(sqlaPostpf1Inner.sqlBandsag);
		wsaaAccumulationKeyInner.wsaaBandprmg.set(sqlaPostpf1Inner.sqlBandprmg);
		wsaaAccumulationKeyInner.wsaaBandtrmg.set(sqlaPostpf1Inner.sqlBandtrmg);
		wsaaAccumulationKeyInner.wsaaCommyr.set(sqlaPostpf1Inner.sqlCommyr);
		wsaaAccumulationKeyInner.wsaaPstatcode.set(sqlaPostpf1Inner.sqlPstatcode);
		wsaaAccumulationKeyInner.wsaaCntcurr.set(sqlaPostpf1Inner.sqlCntcurr);
		rd02Accumkey.set(wsaaAccumulationKeyInner.wsaaAccumulationKey);
		rd02Contractc.set(sqlaPostpf1Inner.sqlStcmthg);
		rd02Stpmth.set(sqlaPostpf1Inner.sqlStpmthg);
		rd02Stsmth.set(sqlaPostpf1Inner.sqlStsmthg);
		rd02Stimth.set(sqlaPostpf1Inner.sqlStimth);
		printerFile.printR6524d02(r6524D02, indicArea);
		checkPageHeading800();
	}

protected void updateTotalAmounts460()
	{
		wsaaTotccount.add(sqlaPostpf1Inner.sqlStcmthg);
		wsaaTotaprem.add(sqlaPostpf1Inner.sqlStpmthg);
		wsaaTotsprem.add(sqlaPostpf1Inner.sqlStsmthg);
		wsaaTotsumins.add(sqlaPostpf1Inner.sqlStimth);
		/*READ-NEXT-RECORD*/
		fetchPrimary500();
		if (endOfFile.isTrue()) {
			printGrandTotals700();
		}
		/*EXIT*/
	}

protected void fetchPrimary500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					fetchRecord510();
					updateControls520();
				case eof580:
					eof580();
				case exit590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fetchRecord510()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlpostpf1rs)) {
				getAppVars().getDBObject(sqlpostpf1rs, 1, sqlaPostpf1Inner.sqlBatccoy);
				getAppVars().getDBObject(sqlpostpf1rs, 2, sqlaPostpf1Inner.sqlBatcbrn);
				getAppVars().getDBObject(sqlpostpf1rs, 3, sqlaPostpf1Inner.sqlBatcactyr);
				getAppVars().getDBObject(sqlpostpf1rs, 4, sqlaPostpf1Inner.sqlBatcactmn);
				getAppVars().getDBObject(sqlpostpf1rs, 5, sqlaPostpf1Inner.sqlBatctrcde);
				getAppVars().getDBObject(sqlpostpf1rs, 6, sqlaPostpf1Inner.sqlBatcbatch);
				getAppVars().getDBObject(sqlpostpf1rs, 7, sqlaPostpf1Inner.sqlChdrcoy);
				getAppVars().getDBObject(sqlpostpf1rs, 8, sqlaPostpf1Inner.sqlStatcat);
				getAppVars().getDBObject(sqlpostpf1rs, 9, sqlaPostpf1Inner.sqlAcctyr);
				getAppVars().getDBObject(sqlpostpf1rs, 10, sqlaPostpf1Inner.sqlStfund);
				getAppVars().getDBObject(sqlpostpf1rs, 11, sqlaPostpf1Inner.sqlStsect);
				getAppVars().getDBObject(sqlpostpf1rs, 12, sqlaPostpf1Inner.sqlStssect);
				getAppVars().getDBObject(sqlpostpf1rs, 13, sqlaPostpf1Inner.sqlReg);
				getAppVars().getDBObject(sqlpostpf1rs, 14, sqlaPostpf1Inner.sqlCntbranch);
				getAppVars().getDBObject(sqlpostpf1rs, 15, sqlaPostpf1Inner.sqlBandageg);
				getAppVars().getDBObject(sqlpostpf1rs, 16, sqlaPostpf1Inner.sqlBandsag);
				getAppVars().getDBObject(sqlpostpf1rs, 17, sqlaPostpf1Inner.sqlBandprmg);
				getAppVars().getDBObject(sqlpostpf1rs, 18, sqlaPostpf1Inner.sqlBandtrmg);
				getAppVars().getDBObject(sqlpostpf1rs, 19, sqlaPostpf1Inner.sqlCommyr);
				getAppVars().getDBObject(sqlpostpf1rs, 20, sqlaPostpf1Inner.sqlPstatcode);
				getAppVars().getDBObject(sqlpostpf1rs, 21, sqlaPostpf1Inner.sqlCntcurr);
				getAppVars().getDBObject(sqlpostpf1rs, 22, sqlaPostpf1Inner.sqlChdrnum);
				getAppVars().getDBObject(sqlpostpf1rs, 23, sqlaPostpf1Inner.sqlStcmthg);
				getAppVars().getDBObject(sqlpostpf1rs, 24, sqlaPostpf1Inner.sqlStpmthg);
				getAppVars().getDBObject(sqlpostpf1rs, 25, sqlaPostpf1Inner.sqlStsmthg);
				getAppVars().getDBObject(sqlpostpf1rs, 26, sqlaPostpf1Inner.sqlStimth);
			}
			else {
				goTo(GotoLabel.eof580);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError180();
			throw new RuntimeException("B6524: ERROR (2) - Unexpected return from a transformed GO TO statement: 180-SQL-ERROR");
		}
	}

protected void updateControls520()
	{
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStcmthg);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStpmthg);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStsmthg);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStimth);
		callContot001();
		goTo(GotoLabel.exit590);
	}

protected void eof580()
	{
		wsaaEof.set("Y");
	}

protected void setUpHeadingCurrency600()
	{
		setUpHeadingCurrency610();
	}

protected void setUpHeadingCurrency610()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sqlaPostpf1Inner.sqlCntcurr);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		r6524H01Inner.rh01Currcode.set(sqlaPostpf1Inner.sqlCntcurr);
		r6524H01Inner.rh01Currencynm.set(descIO.getLongdesc());
	}

protected void printGrandTotals700()
	{
			para710();
	}

protected void para710()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			wsaaFirstTime = "N";
			return ;
		}
		rd03Totccount.set(wsaaTotccount);
		rd03Totaprem.set(wsaaTotaprem);
		rd03Totsprem.set(wsaaTotsprem);
		rd03Totsumins.set(wsaaTotsumins);
		printerFile.printR6524d03(r6524D03, indicArea);
		checkPageHeading800();
	}

protected void checkPageHeading800()
	{
		/*PARA*/
		/* If first page/overflow - write standard headings*/

		if (endOfFile.isTrue()) {
			return ;
		}
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printR6524h01(r6524H01Inner.r6524H01, indicArea);
			wsaaOverflow.set("N");
		}
	}

protected void finished900()
	{
		/*CLOSE-FILES*/
		getAppVars().freeDBConnectionIgnoreErr(sqlpostpf1conn, sqlpostpf1ps, sqlpostpf1rs);
		printerFile.close();
		/*EXIT*/
	}

protected void sqlError()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		conerrrec.syserror.set(sqlStatuz);
		systemError005();
		/*EXIT-SQL-ERROR*/
	}
private static final class WsaaAccumulationKeyInner {
	private FixedLengthStringData wsaaAccumulationKey = new FixedLengthStringData(36);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaAccumulationKey, 0);
	private FixedLengthStringData wsaaStatcat = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 1);
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaAccumulationKey, 3).setUnsigned();
	private FixedLengthStringData wsaaStfund = new FixedLengthStringData(1).isAPartOf(wsaaAccumulationKey, 7);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 8);
	private FixedLengthStringData wsaaStssect = new FixedLengthStringData(4).isAPartOf(wsaaAccumulationKey, 10);
	private FixedLengthStringData wsaaReg = new FixedLengthStringData(3).isAPartOf(wsaaAccumulationKey, 14);
	private FixedLengthStringData wsaaCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 17);
	private FixedLengthStringData wsaaBandageg = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 19);
	private FixedLengthStringData wsaaBandsag = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 21);
	private FixedLengthStringData wsaaBandprmg = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 23);
	private FixedLengthStringData wsaaBandtrmg = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 25);
	private ZonedDecimalData wsaaCommyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaAccumulationKey, 27).setUnsigned();
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 31);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaAccumulationKey, 33);
}
private static final class SqlaPostpf1Inner {
	private FixedLengthStringData sqlPostrec = new FixedLengthStringData(94);
	private FixedLengthStringData sqlBatccoy = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 0);
	private FixedLengthStringData sqlBatcbrn = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 1);
	private PackedDecimalData sqlBatcactyr = new PackedDecimalData(4, 0).isAPartOf(sqlPostrec, 3);
	private PackedDecimalData sqlBatcactmn = new PackedDecimalData(2, 0).isAPartOf(sqlPostrec, 6);
	private FixedLengthStringData sqlBatctrcde = new FixedLengthStringData(4).isAPartOf(sqlPostrec, 8);
	private FixedLengthStringData sqlBatcbatch = new FixedLengthStringData(5).isAPartOf(sqlPostrec, 12);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 17);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 18);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlPostrec, 20);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 23);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 24);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlPostrec, 26);
	private FixedLengthStringData sqlReg = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 30);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 33);
	private FixedLengthStringData sqlBandageg = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 35);
	private FixedLengthStringData sqlBandsag = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 37);
	private FixedLengthStringData sqlBandprmg = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 39);
	private FixedLengthStringData sqlBandtrmg = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 41);
	private PackedDecimalData sqlCommyr = new PackedDecimalData(4, 0).isAPartOf(sqlPostrec, 43);
	private FixedLengthStringData sqlPstatcode = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 46);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 48);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlPostrec, 51);
	private PackedDecimalData sqlStcmthg = new PackedDecimalData(9, 0).isAPartOf(sqlPostrec, 59);
	private PackedDecimalData sqlStpmthg = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 64);
	private PackedDecimalData sqlStsmthg = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 74);
	private PackedDecimalData sqlStimth = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 84);
}
private static final class R6524H01Inner {
	private FixedLengthStringData r6524H01 = new FixedLengthStringData(129);
	private FixedLengthStringData r6524h01O = new FixedLengthStringData(129).isAPartOf(r6524H01, 0);
	private FixedLengthStringData rh01Stperiod = new FixedLengthStringData(7).isAPartOf(r6524h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r6524h01O, 7);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r6524h01O, 8);
	private FixedLengthStringData rh01Jobname = new FixedLengthStringData(8).isAPartOf(r6524h01O, 38);
	private ZonedDecimalData rh01Jobnum = new ZonedDecimalData(8, 0).isAPartOf(r6524h01O, 46);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r6524h01O, 54);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(r6524h01O, 64);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(r6524h01O, 66);
	private FixedLengthStringData rh01Currcode = new FixedLengthStringData(3).isAPartOf(r6524h01O, 96);
	private FixedLengthStringData rh01Currencynm = new FixedLengthStringData(30).isAPartOf(r6524h01O, 99);
}
}
