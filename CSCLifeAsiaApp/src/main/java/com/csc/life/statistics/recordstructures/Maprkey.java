package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:06:36
 * Description:
 * Copybook name: MAPRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Maprkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData maprFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData maprKey = new FixedLengthStringData(64).isAPartOf(maprFileKey, 0, REDEFINE);
  	public FixedLengthStringData maprAgntcoy = new FixedLengthStringData(1).isAPartOf(maprKey, 0);
  	public FixedLengthStringData maprAgntnum = new FixedLengthStringData(8).isAPartOf(maprKey, 1);
  	public PackedDecimalData maprAcctyr = new PackedDecimalData(4, 0).isAPartOf(maprKey, 9);
  	public PackedDecimalData maprMnth = new PackedDecimalData(2, 0).isAPartOf(maprKey, 12);
  	public FixedLengthStringData maprCnttype = new FixedLengthStringData(3).isAPartOf(maprKey, 14);
  	public PackedDecimalData maprEffdate = new PackedDecimalData(8, 0).isAPartOf(maprKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(42).isAPartOf(maprKey, 22, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(maprFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		maprFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}