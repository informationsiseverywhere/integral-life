/*********************  */
/*Author  :Liwei					*/
/*Purpose :Model for Rlagprd		*/
/*Date    :2018.10.26				*/
package com.csc.life.statistics.procedures;

import java.math.BigDecimal;

public class RlagprdPojo {
  	private String agntcoy;
  	private String agntnum;
  	private int actyear;
  	private int actmnth;
  	private String agtype;
  	private String cnttype;
  	private int occdate;
  	private int effdate;
  	private BigDecimal origamt;
  	private String prdflg;
  	private String statuz;
  	private String chdrcoy;
  	private String chdrnum;
  	private String batctrcde;
  	
	public RlagprdPojo() {
		agntcoy = "";
		agntnum = "";
		actyear = 0;
		actmnth = 0;
		agtype = "";
		cnttype = "";
		occdate = 0;
		effdate = 0;
		origamt = BigDecimal.ZERO;
		prdflg = "";
		statuz = "";
		chdrcoy = "";
		chdrnum = "";
		batctrcde = "";
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public int getActyear() {
		return actyear;
	}
	public void setActyear(int actyear) {
		this.actyear = actyear;
	}
	public int getActmnth() {
		return actmnth;
	}
	public void setActmnth(int actmnth) {
		this.actmnth = actmnth;
	}
	public String getAgtype() {
		return agtype;
	}
	public void setAgtype(String agtype) {
		this.agtype = agtype;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public int getOccdate() {
		return occdate;
	}
	public void setOccdate(int occdate) {
		this.occdate = occdate;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public BigDecimal getOrigamt() {
		return origamt;
	}
	public void setOrigamt(BigDecimal origamt) {
		this.origamt = origamt;
	}
	public String getPrdflg() {
		return prdflg;
	}
	public void setPrdflg(String prdflg) {
		this.prdflg = prdflg;
	}
	public String getStatuz() {
		return statuz;
	}
	public void setStatuz(String statuz) {
		this.statuz = statuz;
	}
	public String getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(String batctrcde) {
		this.batctrcde = batctrcde;
	}
  	
  	
}
