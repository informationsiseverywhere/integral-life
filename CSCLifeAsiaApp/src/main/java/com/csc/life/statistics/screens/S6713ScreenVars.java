package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6713
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6713ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(767);
	public FixedLengthStringData dataFields = new FixedLengthStringData(271).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctyr = DD.acctyr.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acmn = DD.acmn.copyToZonedDecimal().isAPartOf(dataFields,4);
	public ZonedDecimalData acyr = DD.acyr.copyToZonedDecimal().isAPartOf(dataFields,6);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,20);
	public ZonedDecimalData commyr = DD.commyr.copyToZonedDecimal().isAPartOf(dataFields,23);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData mthldesc = DD.mthldesc.copy().isAPartOf(dataFields,58);
	public FixedLengthStringData pstatcd = DD.pstatcd.copy().isAPartOf(dataFields,68);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,70);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(dataFields,73);
	public ZonedDecimalData stcmth = DD.stcmth.copyToZonedDecimal().isAPartOf(dataFields,75);
	public ZonedDecimalData stcmthAdj = DD.stcmthadj.copyToZonedDecimal().isAPartOf(dataFields,84);
	public ZonedDecimalData stcmtho = DD.stcmtho.copyToZonedDecimal().isAPartOf(dataFields,93);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,102);
	public ZonedDecimalData stimth = DD.stimth.copyToZonedDecimal().isAPartOf(dataFields,103);
	public ZonedDecimalData stimthAdj = DD.stimthadj.copyToZonedDecimal().isAPartOf(dataFields,121);
	public ZonedDecimalData stimtho = DD.stimtho.copyToZonedDecimal().isAPartOf(dataFields,139);
	public ZonedDecimalData stpmth = DD.stpmth.copyToZonedDecimal().isAPartOf(dataFields,157);
	public ZonedDecimalData stpmthAdj = DD.stpmthadj.copyToZonedDecimal().isAPartOf(dataFields,175);
	public ZonedDecimalData stpmtho = DD.stpmtho.copyToZonedDecimal().isAPartOf(dataFields,193);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,211);
	public ZonedDecimalData stsmth = DD.stsmth.copyToZonedDecimal().isAPartOf(dataFields,213);
	public ZonedDecimalData stsmthAdj = DD.stsmthadj.copyToZonedDecimal().isAPartOf(dataFields,231);
	public ZonedDecimalData stsmtho = DD.stsmtho.copyToZonedDecimal().isAPartOf(dataFields,249);
	public FixedLengthStringData stsubsect = DD.stsubsect.copy().isAPartOf(dataFields,267);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(124).isAPartOf(dataArea, 271);
	public FixedLengthStringData acctyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acmnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData acyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bandageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bandprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bandsaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bandtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData commyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData mthldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData pstatcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData statcatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData stcmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData stcmthadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData stcmthoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData stimthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData stimthadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData stimthoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData stpmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData stpmthadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData stpmthoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 104);
	public FixedLengthStringData stsmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 108);
	public FixedLengthStringData stsmthadjErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 112);
	public FixedLengthStringData stsmthoErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 116);
	public FixedLengthStringData stsubsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 120);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(372).isAPartOf(dataArea, 395);
	public FixedLengthStringData[] acctyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acmnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] acyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bandageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bandprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bandsaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bandtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] commyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] mthldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] pstatcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] statcatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] stcmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] stcmthadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] stcmthoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] stimthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] stimthadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] stimthoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] stpmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] stpmthadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] stpmthoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 312);
	public FixedLengthStringData[] stsmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 324);
	public FixedLengthStringData[] stsmthadjOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 336);
	public FixedLengthStringData[] stsmthoOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 348);
	public FixedLengthStringData[] stsubsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 360);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6713screenWritten = new LongData(0);
	public LongData S6713protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6713ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(stcmthadjOut,new String[] {"02", "01","-02", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stpmthadjOut,new String[] {"02", "01","-02", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsmthadjOut,new String[] {"02", "01","-02", null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stimthadjOut,new String[] {"02", "01","-02", null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, stcmth, stpmth, stsmth, stimth, descrip, acyr, acmn, stcmthAdj, stpmthAdj, stsmthAdj, stimthAdj, stcmtho, stpmtho, stsmtho, stimtho, statcat, acctyr, statFund, statSect, stsubsect, register, cntbranch, bandage, bandsa, bandprm, bandtrm, pstatcd, cntcurr, mthldesc, commyr};
		screenOutFields = new BaseData[][] {companyOut, stcmthOut, stpmthOut, stsmthOut, stimthOut, descripOut, acyrOut, acmnOut, stcmthadjOut, stpmthadjOut, stsmthadjOut, stimthadjOut, stcmthoOut, stpmthoOut, stsmthoOut, stimthoOut, statcatOut, acctyrOut, stfundOut, stsectOut, stsubsectOut, registerOut, cntbranchOut, bandageOut, bandsaOut, bandprmOut, bandtrmOut, pstatcdOut, cntcurrOut, mthldescOut, commyrOut};
		screenErrFields = new BaseData[] {companyErr, stcmthErr, stpmthErr, stsmthErr, stimthErr, descripErr, acyrErr, acmnErr, stcmthadjErr, stpmthadjErr, stsmthadjErr, stimthadjErr, stcmthoErr, stpmthoErr, stsmthoErr, stimthoErr, statcatErr, acctyrErr, stfundErr, stsectErr, stsubsectErr, registerErr, cntbranchErr, bandageErr, bandsaErr, bandprmErr, bandtrmErr, pstatcdErr, cntcurrErr, mthldescErr, commyrErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6713screen.class;
		protectRecord = S6713protect.class;
	}

}
