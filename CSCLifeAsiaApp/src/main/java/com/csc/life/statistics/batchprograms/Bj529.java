/*
 * File: Bj529.java
 * Date: 29 August 2009 21:43:29
 * Author: Quipoz Limited
 *
 * Class transformed from BJ529.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.statistics.dataaccess.GoveTableDAM;
import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj529Report;
import com.csc.life.statistics.tablestructures.Tj675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                  Persistency Report
*                  ==================
*
*   This report will list the number of policies that lapsed and
*   reinstated and its corresponding sum assured and annuity pa
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*     - Select data from GOVE where STATCAT = 'LA', 'RI'         AR
*
*    Read
*     - read SQL File Until end of file
*
*      Edit
*       - set WSSP-EDTERROR to ENDP when EOF.
*
*      Update
*       - if new page, write headings
*       - if there is a change in company, accounting cuurency
*         or source of business print the reassurance details
*       - write details
*
*      Read next primary file record
*
*    End Perform
*
*   Control totals:
*     01  -  Number of pages printed
*     02  -  Number of records read
*     03  -  Total policy no
*     04  -  Total sum assure
*     05  -  Total annuity pa
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the SQL-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
***********************************************************************
* </pre>
*/
public class Bj529 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgovepfAllrs = null;
	private java.sql.PreparedStatement sqlgovepfAllps = null;
	private java.sql.Connection sqlgovepfAllconn = null;
	private String sqlgovepfAll = "";
	private java.sql.ResultSet sqlgovepfrs = null;
	private java.sql.PreparedStatement sqlgovepfps = null;
	private java.sql.Connection sqlgovepfconn = null;
	private String sqlgovepf = "";
	private Rj529Report printerFile = new Rj529Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(2250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ529");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");
	private ZonedDecimalData wsaaScrate = new ZonedDecimalData(12, 7);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData mth = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaSelectAll = new FixedLengthStringData(1);
	private Validator selectAll = new Validator(wsaaSelectAll, "Y");
	private FixedLengthStringData wsaaTj675Stsect01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect02 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect03 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect04 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect05 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect06 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect07 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect08 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect09 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect10 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStssect = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaRegister = new FixedLengthStringData(3);
	private String wsaaFrecord = "";
	private String wsaaFlag = "";

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String wsaaRi = "RI";
	private String wsaaLa = "LA";
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);
	private ZonedDecimalData wsaaCount = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaAmount = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStcamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStiamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaamt = new PackedDecimalData(18, 2);

	private FixedLengthStringData wsaaStcmths = new FixedLengthStringData(10);
	private FixedLengthStringData[] wsaaStoreStcmth = FLSArrayPartOfStructure(2, 5, wsaaStcmths, 0);
	private PackedDecimalData[] wsaaStcmth = PDArrayPartOfArrayStructure(9, 0, wsaaStoreStcmth, 0);

	private FixedLengthStringData wsaaStimths = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaStoreStimth = FLSArrayPartOfStructure(2, 10, wsaaStimths, 0);
	private PackedDecimalData[] wsaaStimth = PDArrayPartOfArrayStructure(18, 2, wsaaStoreStimth, 0);

	private FixedLengthStringData wsaaStamths = new FixedLengthStringData(20);
	private FixedLengthStringData[] wsaaStoreStamth = FLSArrayPartOfStructure(2, 10, wsaaStamths, 0);
	private PackedDecimalData[] wsaaStamth = PDArrayPartOfArrayStructure(18, 2, wsaaStoreStamth, 0);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);

		/* SQL-GOVEPF */
	private FixedLengthStringData sqlGoverec = new FixedLengthStringData(335);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 0);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 1);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGoverec, 3);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 6);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 7);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 9);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 12);
	private FixedLengthStringData sqlCrpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 14);
	private FixedLengthStringData sqlCnpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 16);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 18);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 21);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 25);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 28);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 31);
	private PackedDecimalData sqlStcmth01 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 35);
	private PackedDecimalData sqlStcmth02 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 40);
	private PackedDecimalData sqlStcmth03 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 45);
	private PackedDecimalData sqlStcmth04 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 50);
	private PackedDecimalData sqlStcmth05 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 55);
	private PackedDecimalData sqlStcmth06 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 60);
	private PackedDecimalData sqlStcmth07 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 65);
	private PackedDecimalData sqlStcmth08 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 70);
	private PackedDecimalData sqlStcmth09 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 75);
	private PackedDecimalData sqlStcmth10 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 80);
	private PackedDecimalData sqlStcmth11 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 85);
	private PackedDecimalData sqlStcmth12 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 90);
	private PackedDecimalData sqlStimth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 95);
	private PackedDecimalData sqlStimth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 105);
	private PackedDecimalData sqlStimth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 115);
	private PackedDecimalData sqlStimth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 125);
	private PackedDecimalData sqlStimth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 135);
	private PackedDecimalData sqlStimth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 145);
	private PackedDecimalData sqlStimth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 155);
	private PackedDecimalData sqlStimth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 165);
	private PackedDecimalData sqlStimth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 175);
	private PackedDecimalData sqlStimth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 185);
	private PackedDecimalData sqlStimth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 195);
	private PackedDecimalData sqlStimth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 205);
	private PackedDecimalData sqlStamth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 215);
	private PackedDecimalData sqlStamth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 225);
	private PackedDecimalData sqlStamth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 235);
	private PackedDecimalData sqlStamth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 245);
	private PackedDecimalData sqlStamth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 255);
	private PackedDecimalData sqlStamth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 265);
	private PackedDecimalData sqlStamth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 275);
	private PackedDecimalData sqlStamth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 285);
	private PackedDecimalData sqlStamth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 295);
	private PackedDecimalData sqlStamth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 305);
	private PackedDecimalData sqlStamth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 315);
	private PackedDecimalData sqlStamth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 325);

	private FixedLengthStringData tmpGoverec = new FixedLengthStringData(335);
	private FixedLengthStringData tmpChdrcoy = new FixedLengthStringData(1).isAPartOf(tmpGoverec, 0);
	private FixedLengthStringData tmpStatcat = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 1);
	private PackedDecimalData tmpAcctyr = new PackedDecimalData(4, 0).isAPartOf(tmpGoverec, 3);
	private FixedLengthStringData tmpStfund = new FixedLengthStringData(1).isAPartOf(tmpGoverec, 6);
	private FixedLengthStringData tmpStsect = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 7);
	private FixedLengthStringData tmpRegister = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 9);
	private FixedLengthStringData tmpCntbranch = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 12);
	private FixedLengthStringData tmpCrpstat = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 14);
	private FixedLengthStringData tmpCnpstat = new FixedLengthStringData(2).isAPartOf(tmpGoverec, 16);
	private FixedLengthStringData tmpCnttype = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 18);
	private FixedLengthStringData tmpCrtable = new FixedLengthStringData(4).isAPartOf(tmpGoverec, 21);
	private FixedLengthStringData tmpAcctccy = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 25);
	private FixedLengthStringData tmpCntcurr = new FixedLengthStringData(3).isAPartOf(tmpGoverec, 28);
	private FixedLengthStringData tmpStssect = new FixedLengthStringData(4).isAPartOf(tmpGoverec, 31);
	private FixedLengthStringData tmpStcmths = new FixedLengthStringData(60).isAPartOf(tmpGoverec, 35);
	private PackedDecimalData[] tmpStcmth = PDArrayPartOfStructure(12, 9, 0, tmpStcmths, 0);
	private FixedLengthStringData tmpStimths = new FixedLengthStringData(120).isAPartOf(tmpGoverec, 95);
	private PackedDecimalData[] tmpStimth = PDArrayPartOfStructure(12, 18, 2, tmpStimths, 0);
	private FixedLengthStringData tmpStamths = new FixedLengthStringData(120).isAPartOf(tmpGoverec, 215);
	private PackedDecimalData[] tmpStamth = PDArrayPartOfStructure(12, 18, 2, tmpStamths, 0);
		/* ERRORS */
	private String g418 = "G418";
	private String esql = "ESQL";
	private String itemrec = "ITEMREC";
	private String descrec = "DESCREC";
		/* TABLES */
	private String t5685 = "T5685";
	private String t5684 = "T5684";
	private String t3589 = "T3589";
	private String t3629 = "T3629";
	private String t1693 = "T1693";
	private String tj675 = "TJ675";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;

	private FixedLengthStringData rj529H01 = new FixedLengthStringData(179);
	private FixedLengthStringData rj529h01O = new FixedLengthStringData(179).isAPartOf(rj529H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj529h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj529h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj529h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj529h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj529h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj529h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj529h01O, 50);
	private FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(rj529h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj529h01O, 83);
	private FixedLengthStringData stsect = new FixedLengthStringData(2).isAPartOf(rj529h01O, 113);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30).isAPartOf(rj529h01O, 115);
	private FixedLengthStringData stssect = new FixedLengthStringData(4).isAPartOf(rj529h01O, 145);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rj529h01O, 149);

	private FixedLengthStringData rj529D01 = new FixedLengthStringData(70);
	private FixedLengthStringData rj529d01O = new FixedLengthStringData(70).isAPartOf(rj529D01, 0);
	private ZonedDecimalData stcmth01 = new ZonedDecimalData(9, 0).isAPartOf(rj529d01O, 0);
	private ZonedDecimalData stcmth02 = new ZonedDecimalData(9, 0).isAPartOf(rj529d01O, 9);
	private ZonedDecimalData dliaop01 = new ZonedDecimalData(13, 0).isAPartOf(rj529d01O, 18);
	private ZonedDecimalData dliaop02 = new ZonedDecimalData(13, 0).isAPartOf(rj529d01O, 31);
	private ZonedDecimalData dliaoa01 = new ZonedDecimalData(13, 0).isAPartOf(rj529d01O, 44);
	private ZonedDecimalData dliaoa02 = new ZonedDecimalData(13, 0).isAPartOf(rj529d01O, 57);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Additional Govr Statistics Accumulation*/
	private GoveTableDAM goveIO = new GoveTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Pj517par pj517par = new Pj517par();
	private T3629rec t3629rec = new T3629rec();
	private Tj675rec tj675rec = new Tj675rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		h320CallItemio
	}

	public Bj529() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		printerFile.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsaaAcctyr.set(pj517par.acctyr);
		acctyear.set(pj517par.acctyr);
		acctmonth.set(pj517par.acctmnth);
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaCount.set(1);
		while ( !(isGT(wsaaCount,2))) {
			wsaaStcmth[wsaaCount.toInt()].set(ZERO);
			wsaaStimth[wsaaCount.toInt()].set(ZERO);
			wsaaStamth[wsaaCount.toInt()].set(ZERO);
			wsaaCount.add(1);
		}

		wsaaCount.set(1);
		wsaaStsect.set(SPACES);
		wsaaStssect.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaRegister.set(SPACES);
		wsaaFrecord = "Y";
		readTj6751300();
	}

protected void readTj6751300()
	{
		para1310();
	}

protected void para1310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tj675);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		wsaaSelectAll.set("Y");
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			selectAll1400();
		}
		else {
			wsaaSelectAll.set("N");
			tj675rec.tj675Rec.set(itemIO.getGenarea());
			selectSpecified1500();
		}
	}

protected void selectAll1400()
	{
		/*PARA*/
		sqlgovepfAll = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, STSSECT, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE ACCTYR = ?" +
" AND STATCAT IN (?, ?)" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfAllconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfAllps = appVars.prepareStatementEmbeded(sqlgovepfAllconn, sqlgovepfAll, "GOVEPF");
			appVars.setDBDouble(sqlgovepfAllps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgovepfAllps, 2, wsaaLa);
			appVars.setDBString(sqlgovepfAllps, 3, wsaaRi);
			sqlgovepfAllrs = appVars.executeQuery(sqlgovepfAllps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void selectSpecified1500()
	{
		para1510();
	}

protected void para1510()
	{
		wsaaTj675Stsect01.set(tj675rec.statSect[1]);
		wsaaTj675Stsect02.set(tj675rec.statSect[2]);
		wsaaTj675Stsect03.set(tj675rec.statSect[3]);
		wsaaTj675Stsect04.set(tj675rec.statSect[4]);
		wsaaTj675Stsect05.set(tj675rec.statSect[5]);
		wsaaTj675Stsect06.set(tj675rec.statSect[6]);
		wsaaTj675Stsect07.set(tj675rec.statSect[7]);
		wsaaTj675Stsect08.set(tj675rec.statSect[8]);
		wsaaTj675Stsect09.set(tj675rec.statSect[9]);
		wsaaTj675Stsect10.set(tj675rec.statSect[10]);
		sqlgovepf = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, STSSECT, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE ACCTYR = ?" +
" AND (STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?)" +
" AND STATCAT IN (?, ?)" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfps = appVars.prepareStatementEmbeded(sqlgovepfconn, sqlgovepf, "GOVEPF");
			appVars.setDBDouble(sqlgovepfps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgovepfps, 2, wsaaTj675Stsect01.toString());
			appVars.setDBString(sqlgovepfps, 3, wsaaTj675Stsect02.toString());
			appVars.setDBString(sqlgovepfps, 4, wsaaTj675Stsect03.toString());
			appVars.setDBString(sqlgovepfps, 5, wsaaTj675Stsect04.toString());
			appVars.setDBString(sqlgovepfps, 6, wsaaTj675Stsect05.toString());
			appVars.setDBString(sqlgovepfps, 7, wsaaTj675Stsect06.toString());
			appVars.setDBString(sqlgovepfps, 8, wsaaTj675Stsect07.toString());
			appVars.setDBString(sqlgovepfps, 9, wsaaTj675Stsect08.toString());
			appVars.setDBString(sqlgovepfps, 10, wsaaTj675Stsect09.toString());
			appVars.setDBString(sqlgovepfps, 11, wsaaTj675Stsect10.toString());
			appVars.setDBString(sqlgovepfps, 12, wsaaLa);
			appVars.setDBString(sqlgovepfps, 13, wsaaRi);
			sqlgovepfrs = appVars.executeQuery(sqlgovepfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		if (selectAll.isTrue()) {
			sqlerrorflag = false;
			try {
				if (sqlgovepfAllrs.next()) {
					appVars.getDBObject(sqlgovepfAllrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfAllrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfAllrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfAllrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfAllrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfAllrs, 6, sqlRegister);
					appVars.getDBObject(sqlgovepfAllrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgovepfAllrs, 8, sqlCrpstat);
					appVars.getDBObject(sqlgovepfAllrs, 9, sqlCnpstat);
					appVars.getDBObject(sqlgovepfAllrs, 10, sqlCnttype);
					appVars.getDBObject(sqlgovepfAllrs, 11, sqlCrtable);
					appVars.getDBObject(sqlgovepfAllrs, 12, sqlAcctccy);
					appVars.getDBObject(sqlgovepfAllrs, 13, sqlCntcurr);
					appVars.getDBObject(sqlgovepfAllrs, 14, sqlStssect);
					appVars.getDBObject(sqlgovepfAllrs, 15, sqlStcmth01);
					appVars.getDBObject(sqlgovepfAllrs, 16, sqlStcmth02);
					appVars.getDBObject(sqlgovepfAllrs, 17, sqlStcmth03);
					appVars.getDBObject(sqlgovepfAllrs, 18, sqlStcmth04);
					appVars.getDBObject(sqlgovepfAllrs, 19, sqlStcmth05);
					appVars.getDBObject(sqlgovepfAllrs, 20, sqlStcmth06);
					appVars.getDBObject(sqlgovepfAllrs, 21, sqlStcmth07);
					appVars.getDBObject(sqlgovepfAllrs, 22, sqlStcmth08);
					appVars.getDBObject(sqlgovepfAllrs, 23, sqlStcmth09);
					appVars.getDBObject(sqlgovepfAllrs, 24, sqlStcmth10);
					appVars.getDBObject(sqlgovepfAllrs, 25, sqlStcmth11);
					appVars.getDBObject(sqlgovepfAllrs, 26, sqlStcmth12);
					appVars.getDBObject(sqlgovepfAllrs, 27, sqlStimth01);
					appVars.getDBObject(sqlgovepfAllrs, 28, sqlStimth02);
					appVars.getDBObject(sqlgovepfAllrs, 29, sqlStimth03);
					appVars.getDBObject(sqlgovepfAllrs, 30, sqlStimth04);
					appVars.getDBObject(sqlgovepfAllrs, 31, sqlStimth05);
					appVars.getDBObject(sqlgovepfAllrs, 32, sqlStimth06);
					appVars.getDBObject(sqlgovepfAllrs, 33, sqlStimth07);
					appVars.getDBObject(sqlgovepfAllrs, 34, sqlStimth08);
					appVars.getDBObject(sqlgovepfAllrs, 35, sqlStimth09);
					appVars.getDBObject(sqlgovepfAllrs, 36, sqlStimth10);
					appVars.getDBObject(sqlgovepfAllrs, 37, sqlStimth11);
					appVars.getDBObject(sqlgovepfAllrs, 38, sqlStimth12);
					appVars.getDBObject(sqlgovepfAllrs, 39, sqlStamth01);
					appVars.getDBObject(sqlgovepfAllrs, 40, sqlStamth02);
					appVars.getDBObject(sqlgovepfAllrs, 41, sqlStamth03);
					appVars.getDBObject(sqlgovepfAllrs, 42, sqlStamth04);
					appVars.getDBObject(sqlgovepfAllrs, 43, sqlStamth05);
					appVars.getDBObject(sqlgovepfAllrs, 44, sqlStamth06);
					appVars.getDBObject(sqlgovepfAllrs, 45, sqlStamth07);
					appVars.getDBObject(sqlgovepfAllrs, 46, sqlStamth08);
					appVars.getDBObject(sqlgovepfAllrs, 47, sqlStamth09);
					appVars.getDBObject(sqlgovepfAllrs, 48, sqlStamth10);
					appVars.getDBObject(sqlgovepfAllrs, 49, sqlStamth11);
					appVars.getDBObject(sqlgovepfAllrs, 50, sqlStamth12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		else {
			sqlerrorflag = false;
			try {
				if (sqlgovepfrs.next()) {
					appVars.getDBObject(sqlgovepfrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfrs, 6, sqlRegister);
					appVars.getDBObject(sqlgovepfrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgovepfrs, 8, sqlCrpstat);
					appVars.getDBObject(sqlgovepfrs, 9, sqlCnpstat);
					appVars.getDBObject(sqlgovepfrs, 10, sqlCnttype);
					appVars.getDBObject(sqlgovepfrs, 11, sqlCrtable);
					appVars.getDBObject(sqlgovepfrs, 12, sqlAcctccy);
					appVars.getDBObject(sqlgovepfrs, 13, sqlCntcurr);
					appVars.getDBObject(sqlgovepfrs, 14, sqlStssect);
					appVars.getDBObject(sqlgovepfrs, 15, sqlStcmth01);
					appVars.getDBObject(sqlgovepfrs, 16, sqlStcmth02);
					appVars.getDBObject(sqlgovepfrs, 17, sqlStcmth03);
					appVars.getDBObject(sqlgovepfrs, 18, sqlStcmth04);
					appVars.getDBObject(sqlgovepfrs, 19, sqlStcmth05);
					appVars.getDBObject(sqlgovepfrs, 20, sqlStcmth06);
					appVars.getDBObject(sqlgovepfrs, 21, sqlStcmth07);
					appVars.getDBObject(sqlgovepfrs, 22, sqlStcmth08);
					appVars.getDBObject(sqlgovepfrs, 23, sqlStcmth09);
					appVars.getDBObject(sqlgovepfrs, 24, sqlStcmth10);
					appVars.getDBObject(sqlgovepfrs, 25, sqlStcmth11);
					appVars.getDBObject(sqlgovepfrs, 26, sqlStcmth12);
					appVars.getDBObject(sqlgovepfrs, 27, sqlStimth01);
					appVars.getDBObject(sqlgovepfrs, 28, sqlStimth02);
					appVars.getDBObject(sqlgovepfrs, 29, sqlStimth03);
					appVars.getDBObject(sqlgovepfrs, 30, sqlStimth04);
					appVars.getDBObject(sqlgovepfrs, 31, sqlStimth05);
					appVars.getDBObject(sqlgovepfrs, 32, sqlStimth06);
					appVars.getDBObject(sqlgovepfrs, 33, sqlStimth07);
					appVars.getDBObject(sqlgovepfrs, 34, sqlStimth08);
					appVars.getDBObject(sqlgovepfrs, 35, sqlStimth09);
					appVars.getDBObject(sqlgovepfrs, 36, sqlStimth10);
					appVars.getDBObject(sqlgovepfrs, 37, sqlStimth11);
					appVars.getDBObject(sqlgovepfrs, 38, sqlStimth12);
					appVars.getDBObject(sqlgovepfrs, 39, sqlStamth01);
					appVars.getDBObject(sqlgovepfrs, 40, sqlStamth02);
					appVars.getDBObject(sqlgovepfrs, 41, sqlStamth03);
					appVars.getDBObject(sqlgovepfrs, 42, sqlStamth04);
					appVars.getDBObject(sqlgovepfrs, 43, sqlStamth05);
					appVars.getDBObject(sqlgovepfrs, 44, sqlStamth06);
					appVars.getDBObject(sqlgovepfrs, 45, sqlStamth07);
					appVars.getDBObject(sqlgovepfrs, 46, sqlStamth08);
					appVars.getDBObject(sqlgovepfrs, 47, sqlStamth09);
					appVars.getDBObject(sqlgovepfrs, 48, sqlStamth10);
					appVars.getDBObject(sqlgovepfrs, 49, sqlStamth11);
					appVars.getDBObject(sqlgovepfrs, 50, sqlStamth12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		tmpGoverec.set(sqlGoverec);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		wsspEdterror.set(varcom.endp);
		if (isNE(wsaaFrecord,"Y")) {
			h600SetHeading();
			h100NewPage();
			h200WriteDetail();
		}
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
		writeDetail3080();
	}

protected void update3010()
	{
		for (mth.set(1); !(isGT(mth,pj517par.acctmnth)); mth.add(1)){
			wsaaStcamt.add(tmpStcmth[mth.toInt()]);
			wsaaStiamt.add(tmpStimth[mth.toInt()]);
			wsaaStaamt.add(tmpStamth[mth.toInt()]);
		}
		if (isNE(sqlAcctccy,sqlCntcurr)) {
			h300CheckRate();
		}
	}

protected void writeDetail3080()
	{
		if (isEQ(wsaaFrecord,"Y")) {
			wsaaFrecord = "N";
		}
		else {
			if (isNE(wsaaRegister,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaCompany,sqlChdrcoy)
			|| isNE(wsaaStsect,sqlStsect)
			|| isNE(wsaaStssect,sqlStssect)
			|| isNE(wsaaBranch,sqlCntbranch)) {
				h600SetHeading();
				h100NewPage();
				h200WriteDetail();
			}
		}
		if (isEQ(sqlStatcat,wsaaLa)
		|| isEQ(sqlStatcat,wsaaRi)) {
			wsaaStcmth[1].add(wsaaStcamt);
			wsaaStimth[1].add(wsaaStiamt);
			wsaaStamth[1].add(wsaaStaamt);
		}
		if (isEQ(sqlStatcat,wsaaRi)) {
			wsaaStcmth[2].add(wsaaStcamt);
			wsaaStimth[2].add(wsaaStiamt);
			wsaaStamth[2].add(wsaaStaamt);
		}
		wsaaRegister.set(sqlRegister);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaCompany.set(sqlChdrcoy);
		wsaaBranch.set(sqlCntbranch);
		wsaaStssect.set(sqlStssect);
		wsaaStsect.set(sqlStsect);
		wsaaStcamt.set(ZERO);
		wsaaStiamt.set(ZERO);
		wsaaStaamt.set(ZERO);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (selectAll.isTrue()) {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfAllconn, sqlgovepfAllps, sqlgovepfAllrs);
		}
		else {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfconn, sqlgovepfps, sqlgovepfrs);
		}
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void h100NewPage()
	{
		/*H110-START*/
		printerFile.printRj529h01(rj529H01);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*H190-EXIT*/
	}

protected void h200WriteDetail()
	{
		h210Start();
	}

protected void h210Start()
	{
		stcmth01.set(wsaaStcmth[1]);
		stcmth02.set(wsaaStcmth[2]);
		compute(wsaaAmount, 2).set(add(wsaaStcmth[1],wsaaStcmth[2]));
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaAmount);
		callContot001();
		wsaaCount.set(1);
		while ( !(isGT(wsaaCount,2))) {
			compute(wsaaStimth[wsaaCount.toInt()], 3).setRounded(div(wsaaStimth[wsaaCount.toInt()],1000));
			compute(wsaaStamth[wsaaCount.toInt()], 3).setRounded(div(wsaaStamth[wsaaCount.toInt()],1000));
			wsaaCount.add(1);
		}

		compute(wsaaAmount, 2).set(add(wsaaStimth[1],wsaaStimth[2]));
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaAmount);
		callContot001();
		compute(wsaaAmount, 2).set(add(wsaaStamth[1],wsaaStamth[2]));
		contotrec.totno.set(ct05);
		contotrec.totval.set(wsaaAmount);
		callContot001();
		dliaop01.setRounded(wsaaStimth[1]);
		dliaop02.setRounded(wsaaStimth[2]);
		dliaoa01.setRounded(wsaaStamth[1]);
		dliaoa02.setRounded(wsaaStamth[2]);
		printerFile.printRj529d01(rj529D01);
		wsaaOverflow.set("Y");
		wsaaCount.set(1);
		while ( !(isGT(wsaaCount,2))) {
			wsaaStcmth[wsaaCount.toInt()].set(ZERO);
			wsaaStimth[wsaaCount.toInt()].set(ZERO);
			wsaaStamth[wsaaCount.toInt()].set(ZERO);
			wsaaCount.add(1);
		}

	}

protected void h300CheckRate()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					h310Start();
				}
				case h320CallItemio: {
					h320CallItemio();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void h310Start()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemitem(sqlCntcurr);
		itemIO.setItemtabl(t3629);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
	}

protected void h320CallItemio()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaScrate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaScrate,ZERO)
		|| isGT(wsaaX,7))) {
			if (isGTE(datcon1rec.intDate,t3629rec.frmdate[wsaaX.toInt()])
			&& isLTE(datcon1rec.intDate,t3629rec.todate[wsaaX.toInt()])) {
				wsaaScrate.set(t3629rec.scrate[wsaaX.toInt()]);
			}
			else {
				wsaaX.add(1);
			}
		}

		if (isEQ(wsaaScrate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.h320CallItemio);
			}
			else {
				syserrrec.statuz.set(g418);
				fatalError600();
			}
		}
		compute(wsaaStiamt, 8).setRounded(mult(tmpStimth[pj517par.acctmnth.toInt()],wsaaScrate));
		compute(wsaaStaamt, 8).setRounded(mult(tmpStamth[pj517par.acctmnth.toInt()],wsaaScrate));
	}

protected void h600SetHeading()
	{
		h610Start();
	}

protected void h610Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(wsaaCompany);
		rh01Companynm.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(wsaaAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		acctccy.set(wsaaAcctccy);
		currdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3589);
		descIO.setDescitem(wsaaRegister);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		register.set(wsaaRegister);
		descrip.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5685);
		descIO.setDescitem(wsaaStsect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stsect.set(wsaaStsect);
		itmdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5684);
		descIO.setDescitem(wsaaStssect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stssect.set(wsaaStssect);
		longdesc.set(descIO.getLongdesc());
	}
}
