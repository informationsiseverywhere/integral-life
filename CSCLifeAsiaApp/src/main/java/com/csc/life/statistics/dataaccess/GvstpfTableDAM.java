package com.csc.life.statistics.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GvstpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:25:23
 * Class transformed from GVSTPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GvstpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 347;
	public FixedLengthStringData gvstrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData gvstpfRecord = gvstrec;
	
	public FixedLengthStringData chdrcoy = DD.chdrcoy.copy().isAPartOf(gvstrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(gvstrec);
	public FixedLengthStringData life = DD.life.copy().isAPartOf(gvstrec);
	public FixedLengthStringData coverage = DD.coverage.copy().isAPartOf(gvstrec);
	public FixedLengthStringData rider = DD.rider.copy().isAPartOf(gvstrec);
	public PackedDecimalData planSuffix = DD.plnsfx.copy().isAPartOf(gvstrec);
	public FixedLengthStringData batccoy = DD.batccoy.copy().isAPartOf(gvstrec);
	public FixedLengthStringData batcbrn = DD.batcbrn.copy().isAPartOf(gvstrec);
	public PackedDecimalData batcactyr = DD.batcactyr.copy().isAPartOf(gvstrec);
	public PackedDecimalData batcactmn = DD.batcactmn.copy().isAPartOf(gvstrec);
	public FixedLengthStringData batctrcde = DD.batctrcde.copy().isAPartOf(gvstrec);
	public FixedLengthStringData batcbatch = DD.batcbatch.copy().isAPartOf(gvstrec);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(gvstrec);
	public FixedLengthStringData billfreq = DD.billfreq.copy().isAPartOf(gvstrec);
	public FixedLengthStringData acctccy = DD.acctccy.copy().isAPartOf(gvstrec);
	public PackedDecimalData commyr = DD.commyr.copy().isAPartOf(gvstrec);
	public FixedLengthStringData register = DD.reg.copy().isAPartOf(gvstrec);
	public FixedLengthStringData srcebus = DD.srcebus.copy().isAPartOf(gvstrec);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(gvstrec);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(gvstrec);
	public FixedLengthStringData statSubsect = DD.stssect.copy().isAPartOf(gvstrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(gvstrec);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(gvstrec);
	public FixedLengthStringData parind = DD.parind.copy().isAPartOf(gvstrec);
	public FixedLengthStringData statcode = DD.statcode.copy().isAPartOf(gvstrec);
	public FixedLengthStringData cnPremStat = DD.cnpstat.copy().isAPartOf(gvstrec);
	public FixedLengthStringData covPremStat = DD.crpstat.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccrb = DD.staccrb.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccxb = DD.staccxb.copy().isAPartOf(gvstrec);
	public PackedDecimalData stacctb = DD.stacctb.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccib = DD.staccib.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccdd = DD.staccdd.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccdi = DD.staccdi.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccbs = DD.staccbs.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccmtb = DD.staccmtb.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccob = DD.staccob.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccspd = DD.staccspd.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccfyp = DD.staccfyp.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccrnp = DD.staccrnp.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccadv = DD.staccadv.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccfyc = DD.staccfyc.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccrlc = DD.staccrlc.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccspc = DD.staccspc.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccrip = DD.staccrip.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccric = DD.staccric.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccclr = DD.staccclr.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccpdb = DD.staccpdb.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccadb = DD.staccadb.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccadj = DD.staccadj.copy().isAPartOf(gvstrec);
	public PackedDecimalData staccint = DD.staccint.copy().isAPartOf(gvstrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(gvstrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(gvstrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(gvstrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public GvstpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for GvstpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public GvstpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for GvstpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public GvstpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for GvstpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public GvstpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("GVSTPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"CHDRCOY, " +
							"CHDRNUM, " +
							"LIFE, " +
							"COVERAGE, " +
							"RIDER, " +
							"PLNSFX, " +
							"BATCCOY, " +
							"BATCBRN, " +
							"BATCACTYR, " +
							"BATCACTMN, " +
							"BATCTRCDE, " +
							"BATCBATCH, " +
							"CNTBRANCH, " +
							"BILLFREQ, " +
							"ACCTCCY, " +
							"COMMYR, " +
							"REG, " +
							"SRCEBUS, " +
							"STFUND, " +
							"STSECT, " +
							"STSSECT, " +
							"CNTTYPE, " +
							"CRTABLE, " +
							"PARIND, " +
							"STATCODE, " +
							"CNPSTAT, " +
							"CRPSTAT, " +
							"STACCRB, " +
							"STACCXB, " +
							"STACCTB, " +
							"STACCIB, " +
							"STACCDD, " +
							"STACCDI, " +
							"STACCBS, " +
							"STACCMTB, " +
							"STACCOB, " +
							"STACCSPD, " +
							"STACCFYP, " +
							"STACCRNP, " +
							"STACCADV, " +
							"STACCFYC, " +
							"STACCRLC, " +
							"STACCSPC, " +
							"STACCRIP, " +
							"STACCRIC, " +
							"STACCCLR, " +
							"STACCPDB, " +
							"STACCADB, " +
							"STACCADJ, " +
							"STACCINT, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     chdrcoy,
                                     chdrnum,
                                     life,
                                     coverage,
                                     rider,
                                     planSuffix,
                                     batccoy,
                                     batcbrn,
                                     batcactyr,
                                     batcactmn,
                                     batctrcde,
                                     batcbatch,
                                     cntbranch,
                                     billfreq,
                                     acctccy,
                                     commyr,
                                     register,
                                     srcebus,
                                     statFund,
                                     statSect,
                                     statSubsect,
                                     cnttype,
                                     crtable,
                                     parind,
                                     statcode,
                                     cnPremStat,
                                     covPremStat,
                                     staccrb,
                                     staccxb,
                                     stacctb,
                                     staccib,
                                     staccdd,
                                     staccdi,
                                     staccbs,
                                     staccmtb,
                                     staccob,
                                     staccspd,
                                     staccfyp,
                                     staccrnp,
                                     staccadv,
                                     staccfyc,
                                     staccrlc,
                                     staccspc,
                                     staccrip,
                                     staccric,
                                     staccclr,
                                     staccpdb,
                                     staccadb,
                                     staccadj,
                                     staccint,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		chdrcoy.clear();
  		chdrnum.clear();
  		life.clear();
  		coverage.clear();
  		rider.clear();
  		planSuffix.clear();
  		batccoy.clear();
  		batcbrn.clear();
  		batcactyr.clear();
  		batcactmn.clear();
  		batctrcde.clear();
  		batcbatch.clear();
  		cntbranch.clear();
  		billfreq.clear();
  		acctccy.clear();
  		commyr.clear();
  		register.clear();
  		srcebus.clear();
  		statFund.clear();
  		statSect.clear();
  		statSubsect.clear();
  		cnttype.clear();
  		crtable.clear();
  		parind.clear();
  		statcode.clear();
  		cnPremStat.clear();
  		covPremStat.clear();
  		staccrb.clear();
  		staccxb.clear();
  		stacctb.clear();
  		staccib.clear();
  		staccdd.clear();
  		staccdi.clear();
  		staccbs.clear();
  		staccmtb.clear();
  		staccob.clear();
  		staccspd.clear();
  		staccfyp.clear();
  		staccrnp.clear();
  		staccadv.clear();
  		staccfyc.clear();
  		staccrlc.clear();
  		staccspc.clear();
  		staccrip.clear();
  		staccric.clear();
  		staccclr.clear();
  		staccpdb.clear();
  		staccadb.clear();
  		staccadj.clear();
  		staccint.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getGvstrec() {
  		return gvstrec;
	}

	public FixedLengthStringData getGvstpfRecord() {
  		return gvstpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setGvstrec(what);
	}

	public void setGvstrec(Object what) {
  		this.gvstrec.set(what);
	}

	public void setGvstpfRecord(Object what) {
  		this.gvstpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(gvstrec.getLength());
		result.set(gvstrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}