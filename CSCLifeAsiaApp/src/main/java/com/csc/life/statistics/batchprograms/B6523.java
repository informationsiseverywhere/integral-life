/*
 * File: B6523.java
 * Date: 29 August 2009 21:22:09
 * Author: Quipoz Limited
 *
 * Class transformed from B6523.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.sql.SQLException;

import com.csc.life.statistics.dataaccess.AgstTableDAM;
import com.csc.life.statistics.dataaccess.SttrTableDAM;
import com.csc.smart.dataaccess.BakyTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*            AGENT STATISTICAL MOVEMENT POSTING
*
* This  program  updates  the  agent  statistical  accumulation
* accounts with details from  STTR statistical  movement records
* that have been selected to update the AGST agent accumulation
* accounts. Selection is by the batch extract and agent details
* identifier(STATAGT = 'Y').
*
*
*
*
* 400-SECTION
* -----------
*
* SELECT ALL STATISTICAL RECORDS FROM STTR FILE THAT ARE REQUIRED TO BE
* POSTED (BASED ON A PARTICULAR ACCOUNTING PERIOD MM/YY) INTO AN
* SQL TEMPORARY FILE CALLED SQL-POSTPF1
*
*
* An SQL statement is used to select all STTR records that have
* a corresponding Batchkey record in BAKY file. This baky-data-key is
* created in a prior jobstep created by program B0236.
*
* The selection criteria for posting to AGST file on the following
* items where:
*
*
* Description     BAKY              PARM-JOB parameters
* -----------     ----              -------------------
*
* Job stream      BAKY-JCTLJBN   =  PARM-JOBNUM         AND
* batch           BAKY-JCTLCOY   =  PARM-COMPANY        AND
*                 BAKY-JCTLJN    =  PARM-JOBNAME        AND
*                 BAKY-JCTLJBN   =  PARM-JOBNUM         AND
*                 BAKY-JCTLACTYR =  PARM-ACCTYEAR       AND
*                 BAKY-JCTLACTMN =  PARM-ACCTMN         AND
*                 BAKY-JOBSTEP   =  WSAA-PARM-JS        AND
*
* Extracted       BAKY-BATCCOY   =  STTR-BATCCOY        AND
* Baky keys &     BAKY-BATCBRN   =  STTR-BATCBRN        AND
* STTR batch      BAKY-BATCACTYR =  STTR-BATCACTYR      AND
* details         BAKY-BATCACTMN =  STTR-BATCACTMN      AND
*                 BAKY-BATCTRCDE =  STTR-BATCTRCDE      AND
*                 BAKY-BATCBATCH =  STTR-BATCBATCH      AND
*
*                 STTR-STATAGT   =  'Y'.
*
*
*
* The selected STTR  records  will be arranged in the following
* sequence which the same as the AGST-DATA-KEY
*
*          Agency
*          Category
*          Area code
*          Servicing branch
*          Age band
*          Sum insured band
*          Premium band
*          Term band
*          Contract type
*          Coverage table
*          Overriding commission cat
*          Premium statistical code
*          Currency
*          Account year
*
*
*
*
*  POST EACH GROUP OF STTR RECORDS WHICH HAVE THE SAME ACCUMULATION
*  DATA KEY INTO AGST FILE IN SUMMARISED FORM AS EXTRACTED IN THE
*  PREVIOUS SECTION
*
*
*
*  For each SQL record created for each statistical accumulation
*  key, it performs the following sections:
*
*
*     1000-SECTION
*     ------------
*     READH AGST file to check if it exists.
*
*     IF no accumulation key exists in AGST,
*        Set flag WSAA-NEW-REC-FLAG to 'Y'
*
*     ELSE
*        Set flag WSAA-NEW-REC-FLAG to 'N'
*
*     END-IF.
*
*
*     2000-SECTION
*     ------------
*
*     IF WSAA-NEW-REC-FLAG to 'Y'
*
*        Initialise the record area for AGST record in AGST file
*        Move WRITR function to AGST-FUNCTION
*
*     ELSE
*        Move REWRT function to AGST-FUNCTION
*
*     END-IF.
*
*     Add statistical amounts to AGST record, including the
*     carried forward amounts.
*
*     Update accumulation record on AGST file.
*
*
*
*     3000-SECTION
*     ------------
*
*     - if it is a new accumulation key in AGST file, a new record
*       would be created in AGST file. The carried forward amount
*       of the previous year would be read to update as the brought
*       forward figure of this new record. The carried forward amount
*       of this new record would be updated to all 'future' records
*       that already exists in the system. This is because this program
*       allows the posting of previous years records.
*
*
*
*     For each SQL record fetched,
*
*       Initialise WSAA-BFWDC    WSAA-BFWDP     WSAA-BFWDM.
*                  WSAA-BFWDV    WSAA-BFWDS
*
*       Move zeroes to AGST-ACCTYR.
*
*       The following logic would loop until the SQL accumulation key
*       changes with AGST accumulation key read except for the
*       accounting period concerned:
*
*           Start reading AGST file on the first accumulation record of
*             the first accounting year.
*
*             - IF accounting year of the AGST record read is less than
*                  the SQL-ACCTYR (accounting year):
*
*               Roll forward the balance brought forward (BF) and
*                    carried forward (CF) amounts by
*                    adding the posted amounts to the brought foward
*                    and carried forward amounts.
*
*
*             - IF accounting year of the AGST record read is equal to
*                  the SQL-ACCTYR (accounting year):
*
*                Move WSAA-BFWDC
*                     WSAA-BFWDv
*                     WSAA-BFWDP
*                     WSAA-BFWDS
*                     WSAA-BFWDM
*                     to their corresponding balance brought amounts
*                     in the AGST record.
*
*           - IF accounting year of the AGST record read is more than
*                the SQL-ACCTYR (accounting year):
*
*                Move AGST-CFWDC  to WSAA-BFWDC
*                     AGST-CFWDV  to WSAA-BFWDV
*                     AGST-CFWDP  to WSAA-BFWDP
*                     AGST-CFWDS  to WSAA-BFWDS
*                     AGST-CFWDM  to WSAA-BFWDM
*                     to their corresponding balance brought amounts in
*                     the working storage.
*
*                This is done to retrieve the last carried foward
*                     amounts of the previous accounting year to be
*                     updated as the brought forward amounts for the
*                     posted record.
*
*
*
*           Rewrite AGST record with the adjusted BF and CF amounts.
*
*           Read for the next record in AGST record (NEXTR).
*
*
*
*
*
*
*
*   Batch control totals used:
*
*     01  -  Number of BAKYs selected
*     02  -  Number of AGSTs written
*     03  -  Total no. of contract count
*     04  -  Total no. of cover count
*     05  -  Total amount of annual premium
*     06  -  Total amount of single premium
*     07  -  Total amount of commission
*
*
/
*****************************************************************
* </pre>
*/
public class B6523 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlpostpf1rs = null;
	private java.sql.PreparedStatement sqlpostpf1ps = null;
	private java.sql.Connection sqlpostpf1conn = null;
	private String sqlpostpf1 = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6523");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaParmCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaParmJobname = new FixedLengthStringData(8);
	private PackedDecimalData wsaaParmAcctyear = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaParmAcctmn = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaParmJobnum = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaRunparm1 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaParmCp = new FixedLengthStringData(5).isAPartOf(wsaaRunparm1, 0);
	private FixedLengthStringData wsaaParmJs = new FixedLengthStringData(3).isAPartOf(wsaaRunparm1, 5);
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaStatagtYes = "Y";
	private String wsaaGetBfFlag = "";
	private String wsaaNewRecFlag = "";
	private PackedDecimalData wsaaBfwdc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdv = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwds = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaBfwdm = new PackedDecimalData(18, 2);
	private String agstrec = "AGSTREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler2 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler2, 5);

		/* SQLA-POSTPF1 */
	private FixedLengthStringData sqlPostrec = new FixedLengthStringData(82);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 0);
	private FixedLengthStringData sqlAgntnum = new FixedLengthStringData(8).isAPartOf(sqlPostrec, 1);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 9);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlPostrec, 11);
	private PackedDecimalData sqlAcctmn = new PackedDecimalData(2, 0).isAPartOf(sqlPostrec, 14);
	private FixedLengthStringData sqlAracde = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 16);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 19);
	private FixedLengthStringData sqlBandage = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 21);
	private FixedLengthStringData sqlBandsa = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 23);
	private FixedLengthStringData sqlBandprm = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 25);
	private FixedLengthStringData sqlBandtrm = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 27);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 29);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlPostrec, 32);
	private FixedLengthStringData sqlOvrdcat = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 36);
	private FixedLengthStringData sqlPstatcode = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 37);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 39);
	private PackedDecimalData sqlStcmth = new PackedDecimalData(9, 0).isAPartOf(sqlPostrec, 42);
	private PackedDecimalData sqlStvmth = new PackedDecimalData(9, 0).isAPartOf(sqlPostrec, 47);
	private PackedDecimalData sqlStpmth = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 52);
	private PackedDecimalData sqlStsmth = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 62);
	private PackedDecimalData sqlStmmth = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 72);

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");
		/*Agent Statistical Accumulation File*/
	private AgstTableDAM agstIO = new AgstTableDAM();
		/*Logical File: Extracted Batch Keys*/
	private BakyTableDAM bakyIO = new BakyTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Agent Statistics Movement Logical File*/
	private SttrTableDAM sttrIO = new SttrTableDAM();
	private Varcom varcom = new Varcom();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof580,
		exit590,
		exit1090,
		exit13190
	}

	public B6523() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		/*MAIN*/
		initialise200();
		readFirstRecord300();
		while ( !(endOfFile.isTrue())) {
			updateAgstRecords400();
		}

		finished900();
		/*EXIT*/
	}

protected void initialise200()
	{
		/*INITIALISE*/
		agstIO.setDataArea(SPACES);
		wsaaBfwdc.set(ZERO);
		wsaaBfwdv.set(ZERO);
		wsaaBfwdp.set(ZERO);
		wsaaBfwds.set(ZERO);
		wsaaBfwdm.set(ZERO);
		wsaaEof.set("N");
		wsaaGetBfFlag = "N";
		wsaaParmCompany.set(runparmrec.company);
		wsaaParmJobname.set(runparmrec.jobname);
		wsaaParmAcctyear.set(runparmrec.acctyear);
		wsaaParmAcctmn.set(runparmrec.acctmonth);
		wsaaParmJobnum.set(runparmrec.jobnum);
		wsaaRunparm1.set(runparmrec.runparm1);
		/*EXIT*/
	}

protected void readFirstRecord300()
	{
		/*BEGIN-READING*/
		sqlpostpf1 = " SELECT  ST.CHDRCOY, ST.AGNTNUM, ST.STATCAT, ST.BATCACTYR, ST.BATCACTMN, ST.ARACDE, ST.CNTBRANCH, ST.BANDAGE, ST.BANDSA, ST.BANDPRM, ST.BANDTRM, ST.CNTTYPE, ST.CRTABLE, ST.OVRDCAT, ST.PSTATCODE, ST.CNTCURR, SUM(ST.STCMTH), SUM(ST.STVMTH), SUM(ST.STPMTH), SUM(ST.STSMTH), SUM(ST.STMMTH)" +
" FROM   " + appVars.getTableNameOverriden("BAKYPF") + "  BK,  " + appVars.getTableNameOverriden("STTRPF") + "  ST" +
" WHERE BK.JCTLJBN = ?" +
" AND BK.JCTLCOY = ?" +
" AND BK.JCTLJN = ?" +
" AND BK.JCTLACTYR = ?" +
" AND BK.JCTLACTMN = ?" +
" AND BK.BATCCOY = ST.BATCCOY" +
" AND BK.BATCBRN = ST.BATCBRN" +
" AND BK.BATCACTYR = ST.BATCACTYR" +
" AND BK.BATCACTMN = ST.BATCACTMN" +
" AND BK.BATCTRCDE = ST.BATCTRCDE" +
" AND BK.BATCBATCH = ST.BATCBATCH" +
" AND BK.JOBSTEP = ?" +
" AND ST.STATAGT = ?" +
" GROUP BY ST.CHDRCOY, ST.AGNTNUM, ST.STATCAT, ST.BATCACTYR, ST.BATCACTMN, ST.ARACDE, ST.CNTBRANCH, ST.BANDAGE, ST.BANDSA, ST.BANDPRM, ST.BANDTRM, ST.CNTTYPE, ST.CRTABLE, ST.OVRDCAT, ST.PSTATCODE, ST.CNTCURR" +
" ORDER BY ST.CHDRCOY, ST.AGNTNUM, ST.STATCAT, ST.ARACDE, ST.CNTBRANCH, ST.BANDAGE, ST.BANDSA, ST.BANDPRM, ST.BANDTRM, ST.CNTTYPE, ST.CRTABLE, ST.OVRDCAT, ST.PSTATCODE, ST.CNTCURR, ST.BATCACTYR, ST.BATCACTMN";
		sqlerrorflag = false;
		try {
			sqlpostpf1conn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.smart.dataaccess.BakypfTableDAM(), new com.csc.life.statistics.dataaccess.SttrpfTableDAM()});
			sqlpostpf1ps = appVars.prepareStatementEmbeded(sqlpostpf1conn, sqlpostpf1);
			appVars.setDBDouble(sqlpostpf1ps, 1, wsaaParmJobnum.toDouble());
			appVars.setDBString(sqlpostpf1ps, 2, wsaaParmCompany.toString());
			appVars.setDBString(sqlpostpf1ps, 3, wsaaParmJobname.toString());
			appVars.setDBDouble(sqlpostpf1ps, 4, wsaaParmAcctyear.toDouble());
			appVars.setDBDouble(sqlpostpf1ps, 5, wsaaParmAcctmn.toDouble());
			appVars.setDBString(sqlpostpf1ps, 6, wsaaParmJs.toString());
			appVars.setDBString(sqlpostpf1ps, 7, wsaaStatagtYes);
			sqlpostpf1rs = appVars.executeQuery(sqlpostpf1ps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError();
			throw new RuntimeException("B6523: ERROR (1) - Unexpected return from a transformed GO TO statement: SQL-ERROR");
		}
		fetchPrimary500();
		/*EXIT*/
	}

protected void updateAgstRecords400()
	{
		/*READ-REFERRED-TO-RECORDS*/
		readAgstFile1000();
		updatePostedRecord2000();
		updateAgstFile3000();
		/*READ-NEXT-RECORD*/
		fetchPrimary500();
		/*EXIT*/
	}

protected void fetchPrimary500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					fetchRecord510();
					updateControls520();
				}
				case eof580: {
					eof580();
				}
				case exit590: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fetchRecord510()
	{
		sqlerrorflag = false;
		try {
			if (sqlpostpf1rs.next()) {
				appVars.getDBObject(sqlpostpf1rs, 1, sqlChdrcoy);
				appVars.getDBObject(sqlpostpf1rs, 2, sqlAgntnum);
				appVars.getDBObject(sqlpostpf1rs, 3, sqlStatcat);
				appVars.getDBObject(sqlpostpf1rs, 4, sqlAcctyr);
				appVars.getDBObject(sqlpostpf1rs, 5, sqlAcctmn);
				appVars.getDBObject(sqlpostpf1rs, 6, sqlAracde);
				appVars.getDBObject(sqlpostpf1rs, 7, sqlCntbranch);
				appVars.getDBObject(sqlpostpf1rs, 8, sqlBandage);
				appVars.getDBObject(sqlpostpf1rs, 9, sqlBandsa);
				appVars.getDBObject(sqlpostpf1rs, 10, sqlBandprm);
				appVars.getDBObject(sqlpostpf1rs, 11, sqlBandtrm);
				appVars.getDBObject(sqlpostpf1rs, 12, sqlCnttype);
				appVars.getDBObject(sqlpostpf1rs, 13, sqlCrtable);
				appVars.getDBObject(sqlpostpf1rs, 14, sqlOvrdcat);
				appVars.getDBObject(sqlpostpf1rs, 15, sqlPstatcode);
				appVars.getDBObject(sqlpostpf1rs, 16, sqlCntcurr);
				appVars.getDBObject(sqlpostpf1rs, 17, sqlStcmth);
				appVars.getDBObject(sqlpostpf1rs, 18, sqlStvmth);
				appVars.getDBObject(sqlpostpf1rs, 19, sqlStpmth);
				appVars.getDBObject(sqlpostpf1rs, 20, sqlStsmth);
				appVars.getDBObject(sqlpostpf1rs, 21, sqlStmmth);
			}
			else {
				goTo(GotoLabel.eof580);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError();
			throw new RuntimeException("B6523: ERROR (2) - Unexpected return from a transformed GO TO statement: SQL-ERROR");
		}
	}

protected void updateControls520()
	{
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(sqlStcmth);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(sqlStvmth);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(sqlStpmth);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(sqlStsmth);
		callContot001();
		contotrec.totno.set(ct07);
		contotrec.totval.set(sqlStmmth);
		callContot001();
		goTo(GotoLabel.exit590);
	}

protected void eof580()
	{
		wsaaEof.set("Y");
	}

protected void readAgstFile1000()
	{
		try {
			para1000();
		}
		catch (GOTOException e){
		}
	}

protected void para1000()
	{
		agstIO.setFormat(agstrec);
		agstIO.setFunction(varcom.readh);
		agstIO.setChdrcoy(sqlChdrcoy);
		agstIO.setAgntnum(sqlAgntnum);
		agstIO.setStatcat(sqlStatcat);
		agstIO.setAcctyr(sqlAcctyr);
		agstIO.setAracde(sqlAracde);
		agstIO.setCntbranch(sqlCntbranch);
		agstIO.setBandage(sqlBandage);
		agstIO.setBandsa(sqlBandsa);
		agstIO.setBandprm(sqlBandprm);
		agstIO.setBandtrm(sqlBandtrm);
		agstIO.setCnttype(sqlCnttype);
		agstIO.setCrtable(sqlCrtable);
		agstIO.setOvrdcat(sqlOvrdcat);
		agstIO.setCntcurr(sqlCntcurr);
		agstIO.setPstatcode(sqlPstatcode);
		SmartFileCode.execute(appVars, agstIO);
		if (isEQ(agstIO.getStatuz(),varcom.mrnf)) {
			wsaaNewRecFlag = "Y";
			goTo(GotoLabel.exit1090);
		}
		else {
			wsaaNewRecFlag = "N";
		}
		if (isNE(agstIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(agstIO.getParams());
			databaseError006();
		}
	}

protected void updatePostedRecord2000()
	{
		para2010();
	}

protected void para2010()
	{
		if (isEQ(wsaaNewRecFlag,"Y")) {
			agstIO.setFunction(varcom.writr);
			agstIO.setBfwdc(ZERO);
			agstIO.setCfwdc(ZERO);
			agstIO.setBfwdv(ZERO);
			agstIO.setCfwdv(ZERO);
			agstIO.setBfwdp(ZERO);
			agstIO.setCfwdp(ZERO);
			agstIO.setBfwds(ZERO);
			agstIO.setCfwds(ZERO);
			agstIO.setBfwdm(ZERO);
			agstIO.setCfwdm(ZERO);
			wsaaI.set(1);
			for (wsaaI.set(1); !(isGT(wsaaI,12)); wsaaI.add(1)){
				initializeAgstValues2100();
			}
		}
		else {
			agstIO.setFunction(varcom.rewrt);
		}
		setPrecision(agstIO.getStcmth(sqlAcctmn), 0);
		agstIO.setStcmth(sqlAcctmn, add(agstIO.getStcmth(sqlAcctmn),sqlStcmth));
		setPrecision(agstIO.getCfwdc(), 2);
		agstIO.setCfwdc(add(agstIO.getCfwdc(),sqlStcmth));
		setPrecision(agstIO.getStvmth(sqlAcctmn), 0);
		agstIO.setStvmth(sqlAcctmn, add(agstIO.getStvmth(sqlAcctmn),sqlStvmth));
		setPrecision(agstIO.getCfwdv(), 2);
		agstIO.setCfwdv(add(agstIO.getCfwdv(),sqlStvmth));
		setPrecision(agstIO.getStpmth(sqlAcctmn), 2);
		agstIO.setStpmth(sqlAcctmn, add(agstIO.getStpmth(sqlAcctmn),sqlStpmth));
		setPrecision(agstIO.getCfwdp(), 2);
		agstIO.setCfwdp(add(agstIO.getCfwdp(),sqlStpmth));
		setPrecision(agstIO.getStsmth(sqlAcctmn), 2);
		agstIO.setStsmth(sqlAcctmn, add(agstIO.getStsmth(sqlAcctmn),sqlStsmth));
		setPrecision(agstIO.getCfwds(), 2);
		agstIO.setCfwds(add(agstIO.getCfwds(),sqlStsmth));
		setPrecision(agstIO.getStmmth(sqlAcctmn), 2);
		agstIO.setStmmth(sqlAcctmn, add(agstIO.getStmmth(sqlAcctmn),sqlStmmth));
		setPrecision(agstIO.getCfwdm(), 2);
		agstIO.setCfwdm(add(agstIO.getCfwdm(),sqlStmmth));
		agstIO.setFormat(agstrec);
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(agstIO.getParams());
			databaseError006();
		}
	}

protected void initializeAgstValues2100()
	{
		/*PARA*/
		agstIO.setStcmth(wsaaI, ZERO);
		agstIO.setStvmth(wsaaI, ZERO);
		agstIO.setStpmth(wsaaI, ZERO);
		agstIO.setStsmth(wsaaI, ZERO);
		agstIO.setStmmth(wsaaI, ZERO);
		/*EXIT*/
	}

protected void updateAgstFile3000()
	{
		/*PARA*/
		wsaaBfwdc.set(ZERO);
		wsaaBfwdv.set(ZERO);
		wsaaBfwdp.set(ZERO);
		wsaaBfwds.set(ZERO);
		wsaaBfwdm.set(ZERO);
		agstIO.setAcctyr(ZERO);
		agstIO.setFormat(agstrec);
		agstIO.setFunction(varcom.begnh);
		wsaaGetBfFlag = "N";
		while ( !(isEQ(wsaaGetBfFlag,"Y"))) {
			checkBalForwardValue3100();
		}

		/*EXIT*/
	}

protected void checkBalForwardValue3100()
	{
		try {
			para3110();
			readNextRecord3120();
		}
		catch (GOTOException e){
		}
	}

protected void para3110()
	{
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)
		&& isNE(agstIO.getStatuz(),varcom.endp)) {
			conerrrec.statuz.set(agstIO.getStatuz());
			conerrrec.params.set(agstIO.getParams());
			databaseError006();
		}
		if (isEQ(agstIO.getStatuz(),varcom.endp)) {
			wsaaGetBfFlag = "Y";
			goTo(GotoLabel.exit13190);
		}
		if (isEQ(sqlChdrcoy,agstIO.getChdrcoy())
		&& isEQ(sqlAgntnum,agstIO.getAgntnum())
		&& isEQ(sqlStatcat,agstIO.getStatcat())
		&& isEQ(sqlAracde,agstIO.getAracde())
		&& isEQ(sqlCntbranch,agstIO.getCntbranch())
		&& isEQ(sqlBandage,agstIO.getBandage())
		&& isEQ(sqlBandsa,agstIO.getBandsa())
		&& isEQ(sqlBandprm,agstIO.getBandprm())
		&& isEQ(sqlBandtrm,agstIO.getBandtrm())
		&& isEQ(sqlCnttype,agstIO.getCnttype())
		&& isEQ(sqlCrtable,agstIO.getCrtable())
		&& isEQ(sqlOvrdcat,agstIO.getOvrdcat())
		&& isEQ(sqlPstatcode,agstIO.getPstatcode())
		&& isEQ(sqlCntcurr,agstIO.getCntcurr())) {
			if (isLT(sqlAcctyr,agstIO.getAcctyr())) {
				rollForwardUpdate3200();
			}
			else {
				if (isEQ(sqlAcctyr,agstIO.getAcctyr())) {
					updateBfAmount3300();
				}
				else {
					wsaaBfwdc.set(agstIO.getCfwdc());
					wsaaBfwdv.set(agstIO.getCfwdv());
					wsaaBfwdp.set(agstIO.getCfwdp());
					wsaaBfwds.set(agstIO.getCfwds());
					wsaaBfwdm.set(agstIO.getCfwdm());
				}
			}
		}
		else {
			wsaaGetBfFlag = "Y";
			goTo(GotoLabel.exit13190);
		}
	}

protected void readNextRecord3120()
	{
		agstIO.setFunction(varcom.nextr);
	}

protected void rollForwardUpdate3200()
	{
		para3210();
	}

protected void para3210()
	{
		agstIO.setFormat(agstrec);
		agstIO.setFunction(varcom.rewrt);
		setPrecision(agstIO.getBfwdc(), 2);
		agstIO.setBfwdc(add(agstIO.getBfwdc(),sqlStcmth));
		setPrecision(agstIO.getCfwdc(), 2);
		agstIO.setCfwdc(add(agstIO.getCfwdc(),sqlStcmth));
		setPrecision(agstIO.getBfwdv(), 2);
		agstIO.setBfwdv(add(agstIO.getBfwdv(),sqlStvmth));
		setPrecision(agstIO.getCfwdv(), 2);
		agstIO.setCfwdv(add(agstIO.getCfwdv(),sqlStvmth));
		setPrecision(agstIO.getBfwdp(), 2);
		agstIO.setBfwdp(add(agstIO.getBfwdp(),sqlStpmth));
		setPrecision(agstIO.getCfwdp(), 2);
		agstIO.setCfwdp(add(agstIO.getCfwdp(),sqlStpmth));
		setPrecision(agstIO.getBfwds(), 2);
		agstIO.setBfwds(add(agstIO.getBfwds(),sqlStsmth));
		setPrecision(agstIO.getCfwds(), 2);
		agstIO.setCfwds(add(agstIO.getCfwds(),sqlStsmth));
		setPrecision(agstIO.getBfwdm(), 2);
		agstIO.setBfwdm(add(agstIO.getBfwdm(),sqlStmmth));
		setPrecision(agstIO.getCfwdm(), 2);
		agstIO.setCfwdm(add(agstIO.getCfwdm(),sqlStmmth));
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(agstIO.getParams());
			databaseError006();
		}
	}

protected void updateBfAmount3300()
	{
		para3310();
	}

protected void para3310()
	{
		agstIO.setBfwdc(wsaaBfwdc);
		agstIO.setBfwdv(wsaaBfwdv);
		agstIO.setBfwdp(wsaaBfwdp);
		agstIO.setBfwds(wsaaBfwds);
		agstIO.setBfwdm(wsaaBfwdm);
		if (isEQ(wsaaNewRecFlag,"Y")) {
			setPrecision(agstIO.getCfwdc(), 2);
			agstIO.setCfwdc(add(agstIO.getCfwdc(),wsaaBfwdc));
			setPrecision(agstIO.getCfwdv(), 2);
			agstIO.setCfwdv(add(agstIO.getCfwdv(),wsaaBfwdv));
			setPrecision(agstIO.getCfwdp(), 2);
			agstIO.setCfwdp(add(agstIO.getCfwdp(),wsaaBfwdp));
			setPrecision(agstIO.getCfwds(), 2);
			agstIO.setCfwds(add(agstIO.getCfwds(),wsaaBfwds));
			setPrecision(agstIO.getCfwdm(), 2);
			agstIO.setCfwdm(add(agstIO.getCfwdm(),wsaaBfwdm));
		}
		agstIO.setFormat(agstrec);
		agstIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, agstIO);
		if (isNE(agstIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(agstIO.getParams());
			conerrrec.params.set(agstIO.getStatuz());
			databaseError006();
		}
	}

protected void finished900()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlpostpf1conn, sqlpostpf1ps, sqlpostpf1rs);
		/*EXIT*/
	}

protected void sqlError()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		conerrrec.syserror.set(sqlStatuz);
		systemError005();
		/*EXIT-SQL-ERROR*/
	}
}
