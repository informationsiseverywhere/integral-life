/*
 * File: Bj522.java
 * Date: 29 August 2009 21:41:10
 * Author: Quipoz Limited
 *
 * Class transformed from BJ522.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.sql.SQLException;

import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj52201Report;
import com.csc.life.statistics.reports.Rj52202Report;
import com.csc.life.statistics.tablestructures.Tj675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                   Premium Details
*                   ===============
*
*   This report will list all premium due during the specified
*   accounting year and month, segregating them into
*   first year commission, renewal commission and single premium
*   commission. It will be reported at coverage level. An
*   accumulated figure will be printed for each source of business
*
*   Control totals:
*     01  -   TOTAL OF PRINTED PAGES
*     02  -   TOTAL OF SUMMARY PAGES
*     03  -   TOTAL OF 1ST YR PREMIUM
*     04  -   TOTAL OF RENEWAL PREMIUM
*     05  -   TOTAL OF SP PREMIUM
*
*****************************************************************
* </pre>
*/
public class Bj522 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgvacpfAllrs = null;
	private java.sql.PreparedStatement sqlgvacpfAllps = null;
	private java.sql.Connection sqlgvacpfAllconn = null;
	private String sqlgvacpfAll = "";
	private java.sql.ResultSet sqlgvacpfrs = null;
	private java.sql.PreparedStatement sqlgvacpfps = null;
	private java.sql.Connection sqlgvacpfconn = null;
	private String sqlgvacpf = "";
	private Rj52201Report rj52201 = new Rj52201Report();
	private Rj52202Report rj52202 = new Rj52202Report();
	private FixedLengthStringData rj52201Rec = new FixedLengthStringData(250);
	private FixedLengthStringData rj52202Rec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ522");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaProgram = new FixedLengthStringData(5).isAPartOf(wsaaItem, 1);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaTotadv = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaccfyp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaccrnp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaccspd = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTotal01 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTotal02 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTotal03 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt01 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt02 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt03 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTreg01 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTreg02 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTreg03 = new PackedDecimalData(18, 2);
	private FixedLengthStringData wsaaRegDesp = new FixedLengthStringData(30);
	private PackedDecimalData wsaaTotamt01 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTotamt02 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaInstamt06 = new PackedDecimalData(18, 3);
	private ZonedDecimalData wsaaScrate = new ZonedDecimalData(12, 7);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaPrevBillcurr = new FixedLengthStringData(3);

	private FixedLengthStringData wsaaSelectAll = new FixedLengthStringData(1);
	private Validator selectAll = new Validator(wsaaSelectAll, "Y");
	private FixedLengthStringData wsaaTj675Stsect01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect02 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect03 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect04 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect05 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect06 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect07 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect08 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect09 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect10 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStssect = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaSrcebus = new FixedLengthStringData(2);
	private String wsaaFrecord = "";
	private FixedLengthStringData wsaaParind = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaReg = new FixedLengthStringData(3);
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler1 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler3, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler3, 8);

		/* SQL-GVACPF */
	private FixedLengthStringData sqlGvacrec = new FixedLengthStringData(509);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 0);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGvacrec, 1);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 4);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 5);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGvacrec, 7);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 11);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 14);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 16);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGvacrec, 19);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 23);
	private FixedLengthStringData sqlParind = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 26);
	private FixedLengthStringData sqlSrcebus = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 27);
	private PackedDecimalData sqlStaccfyp01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 29);
	private PackedDecimalData sqlStaccfyp02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 39);
	private PackedDecimalData sqlStaccfyp03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 49);
	private PackedDecimalData sqlStaccfyp04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 59);
	private PackedDecimalData sqlStaccfyp05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 69);
	private PackedDecimalData sqlStaccfyp06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 79);
	private PackedDecimalData sqlStaccfyp07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 89);
	private PackedDecimalData sqlStaccfyp08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 99);
	private PackedDecimalData sqlStaccfyp09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 109);
	private PackedDecimalData sqlStaccfyp10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 119);
	private PackedDecimalData sqlStaccfyp11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 129);
	private PackedDecimalData sqlStaccfyp12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 139);
	private PackedDecimalData sqlStaccrnp01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 149);
	private PackedDecimalData sqlStaccrnp02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 159);
	private PackedDecimalData sqlStaccrnp03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 169);
	private PackedDecimalData sqlStaccrnp04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 179);
	private PackedDecimalData sqlStaccrnp05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 189);
	private PackedDecimalData sqlStaccrnp06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 199);
	private PackedDecimalData sqlStaccrnp07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 209);
	private PackedDecimalData sqlStaccrnp08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 219);
	private PackedDecimalData sqlStaccrnp09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 229);
	private PackedDecimalData sqlStaccrnp10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 239);
	private PackedDecimalData sqlStaccrnp11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 249);
	private PackedDecimalData sqlStaccrnp12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 259);
	private PackedDecimalData sqlStaccspd01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 269);
	private PackedDecimalData sqlStaccspd02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 279);
	private PackedDecimalData sqlStaccspd03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 289);
	private PackedDecimalData sqlStaccspd04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 299);
	private PackedDecimalData sqlStaccspd05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 309);
	private PackedDecimalData sqlStaccspd06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 319);
	private PackedDecimalData sqlStaccspd07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 329);
	private PackedDecimalData sqlStaccspd08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 339);
	private PackedDecimalData sqlStaccspd09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 349);
	private PackedDecimalData sqlStaccspd10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 359);
	private PackedDecimalData sqlStaccspd11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 369);
	private PackedDecimalData sqlStaccspd12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 379);
	private PackedDecimalData sqlStaccadv01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 389);
	private PackedDecimalData sqlStaccadv02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 399);
	private PackedDecimalData sqlStaccadv03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 409);
	private PackedDecimalData sqlStaccadv04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 419);
	private PackedDecimalData sqlStaccadv05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 429);
	private PackedDecimalData sqlStaccadv06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 439);
	private PackedDecimalData sqlStaccadv07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 449);
	private PackedDecimalData sqlStaccadv08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 459);
	private PackedDecimalData sqlStaccadv09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 469);
	private PackedDecimalData sqlStaccadv10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 479);
	private PackedDecimalData sqlStaccadv11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 489);
	private PackedDecimalData sqlStaccadv12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 499);

	private FixedLengthStringData wsaaSqlKeep = new FixedLengthStringData(509);
	private FixedLengthStringData wsaaSChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 0);
	private PackedDecimalData wsaaSAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaSqlKeep, 1);
	private FixedLengthStringData wsaaSStfund = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 4);
	private FixedLengthStringData wsaaSStsect = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 5);
	private FixedLengthStringData wsaaSStssect = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 7);
	private FixedLengthStringData wsaaSRegister = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 11);
	private FixedLengthStringData wsaaSCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 14);
	private FixedLengthStringData wsaaSCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 16);
	private FixedLengthStringData wsaaSCrtable = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 19);
	private FixedLengthStringData wsaaSAcctccy = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 23);
	private FixedLengthStringData wsaaSParind = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 26);
	private FixedLengthStringData wsaaSSrcebus = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 27);
	private PackedDecimalData[] wsaaSStaccfyp = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 29);
	private PackedDecimalData[] wsaaSStaccrnp = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 149);
	private PackedDecimalData[] wsaaSStaccspd = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 269);
	private PackedDecimalData[] wsaaSStaccadv = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 389);
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String linsrnlrec = "LINSRNLREC";
		/* ERRORS */
	private String esql = "ESQL";
	private String g418 = "G418";
		/* TABLES */
	private String t1693 = "T1693";
	private String t3629 = "T3629";
	private String t3589 = "T3589";
	private String t5685 = "T5685";
	private String t5687 = "T5687";
	private String tj675 = "TJ675";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rj52201H01 = new FixedLengthStringData(144);
	private FixedLengthStringData rj52201h01O = new FixedLengthStringData(144).isAPartOf(rj52201H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj52201h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj52201h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj52201h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj52201h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj52201h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj52201h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj52201h01O, 50);
	private FixedLengthStringData srcebus = new FixedLengthStringData(2).isAPartOf(rj52201h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj52201h01O, 82);
	private FixedLengthStringData stsect = new FixedLengthStringData(2).isAPartOf(rj52201h01O, 112);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30).isAPartOf(rj52201h01O, 114);

	private FixedLengthStringData rj52201H02 = new FixedLengthStringData(50);
	private FixedLengthStringData rj52201h02O = new FixedLengthStringData(50).isAPartOf(rj52201H02, 0);
	private FixedLengthStringData adsc = new FixedLengthStringData(50).isAPartOf(rj52201h02O, 0);

	private FixedLengthStringData rj52201H03 = new FixedLengthStringData(2);

	private FixedLengthStringData rj52201D01 = new FixedLengthStringData(88);
	private FixedLengthStringData rj52201d01O = new FixedLengthStringData(88).isAPartOf(rj52201D01, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rj52201d01O, 0);
	private FixedLengthStringData crtabled = new FixedLengthStringData(30).isAPartOf(rj52201d01O, 4);
	private ZonedDecimalData staccfyp = new ZonedDecimalData(18, 2).isAPartOf(rj52201d01O, 34);
	private ZonedDecimalData staccrnp = new ZonedDecimalData(18, 2).isAPartOf(rj52201d01O, 52);
	private ZonedDecimalData staccspd = new ZonedDecimalData(18, 2).isAPartOf(rj52201d01O, 70);

	private FixedLengthStringData rj52201D02 = new FixedLengthStringData(2);

	private FixedLengthStringData rj52201T01 = new FixedLengthStringData(54);
	private FixedLengthStringData rj52201t01O = new FixedLengthStringData(54).isAPartOf(rj52201T01, 0);
	private ZonedDecimalData total01 = new ZonedDecimalData(18, 2).isAPartOf(rj52201t01O, 0);
	private ZonedDecimalData total02 = new ZonedDecimalData(18, 2).isAPartOf(rj52201t01O, 18);
	private ZonedDecimalData total03 = new ZonedDecimalData(18, 2).isAPartOf(rj52201t01O, 36);

	private FixedLengthStringData rj52201T02 = new FixedLengthStringData(54);
	private FixedLengthStringData rj52201t02O = new FixedLengthStringData(54).isAPartOf(rj52201T02, 0);
	private ZonedDecimalData totalamt01 = new ZonedDecimalData(18, 2).isAPartOf(rj52201t02O, 0);
	private ZonedDecimalData totalamt02 = new ZonedDecimalData(18, 2).isAPartOf(rj52201t02O, 18);
	private ZonedDecimalData totalamt03 = new ZonedDecimalData(18, 2).isAPartOf(rj52201t02O, 36);

	private FixedLengthStringData rj52202H01 = new FixedLengthStringData(80);
	private FixedLengthStringData rj52202h01O = new FixedLengthStringData(80).isAPartOf(rj52202H01, 0);
	private ZonedDecimalData acctmonth1 = new ZonedDecimalData(2, 0).isAPartOf(rj52202h01O, 0);
	private ZonedDecimalData acctyear1 = new ZonedDecimalData(4, 0).isAPartOf(rj52202h01O, 2);
	private FixedLengthStringData rh01Company1 = new FixedLengthStringData(1).isAPartOf(rj52202h01O, 6);
	private FixedLengthStringData rh01Companynm1 = new FixedLengthStringData(30).isAPartOf(rj52202h01O, 7);
	private FixedLengthStringData rh01Sdate1 = new FixedLengthStringData(10).isAPartOf(rj52202h01O, 37);
	private FixedLengthStringData acctccy1 = new FixedLengthStringData(3).isAPartOf(rj52202h01O, 47);
	private FixedLengthStringData currdesc1 = new FixedLengthStringData(30).isAPartOf(rj52202h01O, 50);

	private FixedLengthStringData rj52202D01 = new FixedLengthStringData(48);
	private FixedLengthStringData rj52202d01O = new FixedLengthStringData(48).isAPartOf(rj52202D01, 0);
	private FixedLengthStringData descrip1 = new FixedLengthStringData(30).isAPartOf(rj52202d01O, 0);
	private ZonedDecimalData tamt = new ZonedDecimalData(18, 2).isAPartOf(rj52202d01O, 30);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life renewals - instalments billed*/
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	private Pj517par pj517par = new Pj517par();
	private T3629rec t3629rec = new T3629rec();
	private Tj675rec tj675rec = new Tj675rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		nextr5080,
		exit5090,
		a120CallItemio
	}

	public Bj522() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		rj52201.openOutput();
		rj52202.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaAcctyr.set(pj517par.acctyr);
		acctyear.set(pj517par.acctyr);
		acctmonth.set(pj517par.acctmnth);
		acctyear1.set(pj517par.acctyr);
		acctmonth1.set(pj517par.acctmnth);
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		rh01Sdate1.set(datcon1rec.extDate);
		wsaaTotal01.set(ZERO);
		wsaaTotal02.set(ZERO);
		wsaaTotal03.set(ZERO);
		wsaaTreg01.set(ZERO);
		wsaaTreg02.set(ZERO);
		wsaaTreg02.set(ZERO);
		wsaaTamt01.set(ZERO);
		wsaaTamt02.set(ZERO);
		wsaaTamt03.set(ZERO);
		wsaaStaccfyp.set(ZERO);
		wsaaStaccrnp.set(ZERO);
		wsaaTotadv.set(ZERO);
		wsaaInstamt06.set(ZERO);
		wsaaX.set(ZERO);
		wsaaStaccspd.set(ZERO);
		wsaaTotamt01.set(ZERO);
		wsaaStsect.set(SPACES);
		wsaaCrtable.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaParind.set(SPACES);
		wsaaReg.set(SPACES);
		wsaaPrevBillcurr.set(SPACES);
		wsaaFrecord = "Y";
		wsaaProgram.set(wsaaProg);
		wsaaLanguage.set(bsscIO.getLanguage());
		readTj6751100();
	}

protected void readTj6751100()
	{
		para1110();
	}

protected void para1110()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tj675);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		wsaaSelectAll.set("Y");
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			selectAll1200();
		}
		else {
			wsaaSelectAll.set("N");
			tj675rec.tj675Rec.set(itemIO.getGenarea());
			selectSpecified1300();
		}
	}

protected void selectAll1200()
	{
		/*PARA*/
		//ILIFE-1149
		//sqlgvacpfAll = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REGISTER, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STACCFYP01, STACCFYP02, STACCFYP03, STACCFYP04, STACCFYP05, STACCFYP06, STACCFYP07, STACCFYP08, STACCFYP09, STACCFYP10, STACCFYP11, STACCFYP12, STACCRNP01, STACCRNP02, STACCRNP03, STACCRNP04, STACCRNP05, STACCRNP06, STACCRNP07, STACCRNP08, STACCRNP09, STACCRNP10, STACCRNP11, STACCRNP12, STACCSPD01, STACCSPD02, STACCSPD03, STACCSPD04, STACCSPD05, STACCSPD06, STACCSPD07, STACCSPD08, STACCSPD09, STACCSPD10, STACCSPD11, STACCSPD12, STACCADV01, STACCADV02, STACCADV03, STACCADV04, STACCADV05, STACCADV06, STACCADV07, STACCADV08, STACCADV09, STACCADV10, STACCADV11, STACCADV12" +
		sqlgvacpfAll = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STACCFYP01, STACCFYP02, STACCFYP03, STACCFYP04, STACCFYP05, STACCFYP06, STACCFYP07, STACCFYP08, STACCFYP09, STACCFYP10, STACCFYP11, STACCFYP12, STACCRNP01, STACCRNP02, STACCRNP03, STACCRNP04, STACCRNP05, STACCRNP06, STACCRNP07, STACCRNP08, STACCRNP09, STACCRNP10, STACCRNP11, STACCRNP12, STACCSPD01, STACCSPD02, STACCSPD03, STACCSPD04, STACCSPD05, STACCSPD06, STACCSPD07, STACCSPD08, STACCSPD09, STACCSPD10, STACCSPD11, STACCSPD12, STACCADV01, STACCADV02, STACCADV03, STACCADV04, STACCADV05, STACCADV06, STACCADV07, STACCADV08, STACCADV09, STACCADV10, STACCADV11, STACCADV12" +
" FROM   " + appVars.getTableNameOverriden("GVACPF") + " " +
" WHERE ACCTYR = ?" +
//ILIFE-1149
//" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REGISTER, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REG, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
		sqlerrorflag = false;
		try {
			sqlgvacpfAllconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvacpfTableDAM());
			sqlgvacpfAllps = appVars.prepareStatementEmbeded(sqlgvacpfAllconn, sqlgvacpfAll, "GVACPF");
			appVars.setDBDouble(sqlgvacpfAllps, 1, wsaaAcctyr.toDouble());
			sqlgvacpfAllrs = appVars.executeQuery(sqlgvacpfAllps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		/*EXIT*/
	}

protected void selectSpecified1300()
	{
		para1310();
	}

protected void para1310()
	{
		wsaaTj675Stsect01.set(tj675rec.statSect[1]);
		wsaaTj675Stsect02.set(tj675rec.statSect[2]);
		wsaaTj675Stsect03.set(tj675rec.statSect[3]);
		wsaaTj675Stsect04.set(tj675rec.statSect[4]);
		wsaaTj675Stsect05.set(tj675rec.statSect[5]);
		wsaaTj675Stsect06.set(tj675rec.statSect[6]);
		wsaaTj675Stsect07.set(tj675rec.statSect[7]);
		wsaaTj675Stsect08.set(tj675rec.statSect[8]);
		wsaaTj675Stsect09.set(tj675rec.statSect[9]);
		wsaaTj675Stsect10.set(tj675rec.statSect[10]);
		//ILIFE-1149
		//sqlgvacpf = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REGISTER, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STACCFYP01, STACCFYP02, STACCFYP03, STACCFYP04, STACCFYP05, STACCFYP06, STACCFYP07, STACCFYP08, STACCFYP09, STACCFYP10, STACCFYP11, STACCFYP12, STACCRNP01, STACCRNP02, STACCRNP03, STACCRNP04, STACCRNP05, STACCRNP06, STACCRNP07, STACCRNP08, STACCRNP09, STACCRNP10, STACCRNP11, STACCRNP12, STACCSPD01, STACCSPD02, STACCSPD03, STACCSPD04, STACCSPD05, STACCSPD06, STACCSPD07, STACCSPD08, STACCSPD09, STACCSPD10, STACCSPD11, STACCSPD12, STACCADV01, STACCADV02, STACCADV03, STACCADV04, STACCADV05, STACCADV06, STACCADV07, STACCADV08, STACCADV09, STACCADV10, STACCADV11, STACCADV12" +
		sqlgvacpf = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STACCFYP01, STACCFYP02, STACCFYP03, STACCFYP04, STACCFYP05, STACCFYP06, STACCFYP07, STACCFYP08, STACCFYP09, STACCFYP10, STACCFYP11, STACCFYP12, STACCRNP01, STACCRNP02, STACCRNP03, STACCRNP04, STACCRNP05, STACCRNP06, STACCRNP07, STACCRNP08, STACCRNP09, STACCRNP10, STACCRNP11, STACCRNP12, STACCSPD01, STACCSPD02, STACCSPD03, STACCSPD04, STACCSPD05, STACCSPD06, STACCSPD07, STACCSPD08, STACCSPD09, STACCSPD10, STACCSPD11, STACCSPD12, STACCADV01, STACCADV02, STACCADV03, STACCADV04, STACCADV05, STACCADV06, STACCADV07, STACCADV08, STACCADV09, STACCADV10, STACCADV11, STACCADV12" +
" FROM   " + appVars.getTableNameOverriden("GVACPF") + " " +
" WHERE ACCTYR = ?" +
" AND (STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?)" +
//ILIFE-1149
//" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REGISTER, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REG, STSECT, PARIND DESC, CNTTYPE, CRTABLE";		
		sqlerrorflag = false;
		try {
			sqlgvacpfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvacpfTableDAM());
			sqlgvacpfps = appVars.prepareStatementEmbeded(sqlgvacpfconn, sqlgvacpf, "GVACPF");
			appVars.setDBDouble(sqlgvacpfps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgvacpfps, 2, wsaaTj675Stsect01.toString());
			appVars.setDBString(sqlgvacpfps, 3, wsaaTj675Stsect02.toString());
			appVars.setDBString(sqlgvacpfps, 4, wsaaTj675Stsect03.toString());
			appVars.setDBString(sqlgvacpfps, 5, wsaaTj675Stsect04.toString());
			appVars.setDBString(sqlgvacpfps, 6, wsaaTj675Stsect05.toString());
			appVars.setDBString(sqlgvacpfps, 7, wsaaTj675Stsect06.toString());
			appVars.setDBString(sqlgvacpfps, 8, wsaaTj675Stsect07.toString());
			appVars.setDBString(sqlgvacpfps, 9, wsaaTj675Stsect08.toString());
			appVars.setDBString(sqlgvacpfps, 10, wsaaTj675Stsect09.toString());
			appVars.setDBString(sqlgvacpfps, 11, wsaaTj675Stsect10.toString());
			sqlgvacpfrs = appVars.executeQuery(sqlgvacpfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		if (selectAll.isTrue()) {
			sqlerrorflag = false;
			try {
				if (sqlgvacpfAllrs.next()) {
					appVars.getDBObject(sqlgvacpfAllrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgvacpfAllrs, 2, sqlAcctyr);
					appVars.getDBObject(sqlgvacpfAllrs, 3, sqlStfund);
					appVars.getDBObject(sqlgvacpfAllrs, 4, sqlStsect);
					appVars.getDBObject(sqlgvacpfAllrs, 5, sqlStssect);
					appVars.getDBObject(sqlgvacpfAllrs, 6, sqlRegister);
					appVars.getDBObject(sqlgvacpfAllrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgvacpfAllrs, 8, sqlCnttype);
					appVars.getDBObject(sqlgvacpfAllrs, 9, sqlCrtable);
					appVars.getDBObject(sqlgvacpfAllrs, 10, sqlAcctccy);
					appVars.getDBObject(sqlgvacpfAllrs, 11, sqlParind);
					appVars.getDBObject(sqlgvacpfAllrs, 12, sqlSrcebus);
					appVars.getDBObject(sqlgvacpfAllrs, 13, sqlStaccfyp01);
					appVars.getDBObject(sqlgvacpfAllrs, 14, sqlStaccfyp02);
					appVars.getDBObject(sqlgvacpfAllrs, 15, sqlStaccfyp03);
					appVars.getDBObject(sqlgvacpfAllrs, 16, sqlStaccfyp04);
					appVars.getDBObject(sqlgvacpfAllrs, 17, sqlStaccfyp05);
					appVars.getDBObject(sqlgvacpfAllrs, 18, sqlStaccfyp06);
					appVars.getDBObject(sqlgvacpfAllrs, 19, sqlStaccfyp07);
					appVars.getDBObject(sqlgvacpfAllrs, 20, sqlStaccfyp08);
					appVars.getDBObject(sqlgvacpfAllrs, 21, sqlStaccfyp09);
					appVars.getDBObject(sqlgvacpfAllrs, 22, sqlStaccfyp10);
					appVars.getDBObject(sqlgvacpfAllrs, 23, sqlStaccfyp11);
					appVars.getDBObject(sqlgvacpfAllrs, 24, sqlStaccfyp12);
					appVars.getDBObject(sqlgvacpfAllrs, 25, sqlStaccrnp01);
					appVars.getDBObject(sqlgvacpfAllrs, 26, sqlStaccrnp02);
					appVars.getDBObject(sqlgvacpfAllrs, 27, sqlStaccrnp03);
					appVars.getDBObject(sqlgvacpfAllrs, 28, sqlStaccrnp04);
					appVars.getDBObject(sqlgvacpfAllrs, 29, sqlStaccrnp05);
					appVars.getDBObject(sqlgvacpfAllrs, 30, sqlStaccrnp06);
					appVars.getDBObject(sqlgvacpfAllrs, 31, sqlStaccrnp07);
					appVars.getDBObject(sqlgvacpfAllrs, 32, sqlStaccrnp08);
					appVars.getDBObject(sqlgvacpfAllrs, 33, sqlStaccrnp09);
					appVars.getDBObject(sqlgvacpfAllrs, 34, sqlStaccrnp10);
					appVars.getDBObject(sqlgvacpfAllrs, 35, sqlStaccrnp11);
					appVars.getDBObject(sqlgvacpfAllrs, 36, sqlStaccrnp12);
					appVars.getDBObject(sqlgvacpfAllrs, 37, sqlStaccspd01);
					appVars.getDBObject(sqlgvacpfAllrs, 38, sqlStaccspd02);
					appVars.getDBObject(sqlgvacpfAllrs, 39, sqlStaccspd03);
					appVars.getDBObject(sqlgvacpfAllrs, 40, sqlStaccspd04);
					appVars.getDBObject(sqlgvacpfAllrs, 41, sqlStaccspd05);
					appVars.getDBObject(sqlgvacpfAllrs, 42, sqlStaccspd06);
					appVars.getDBObject(sqlgvacpfAllrs, 43, sqlStaccspd07);
					appVars.getDBObject(sqlgvacpfAllrs, 44, sqlStaccspd08);
					appVars.getDBObject(sqlgvacpfAllrs, 45, sqlStaccspd09);
					appVars.getDBObject(sqlgvacpfAllrs, 46, sqlStaccspd10);
					appVars.getDBObject(sqlgvacpfAllrs, 47, sqlStaccspd11);
					appVars.getDBObject(sqlgvacpfAllrs, 48, sqlStaccspd12);
					appVars.getDBObject(sqlgvacpfAllrs, 49, sqlStaccadv01);
					appVars.getDBObject(sqlgvacpfAllrs, 50, sqlStaccadv02);
					appVars.getDBObject(sqlgvacpfAllrs, 51, sqlStaccadv03);
					appVars.getDBObject(sqlgvacpfAllrs, 52, sqlStaccadv04);
					appVars.getDBObject(sqlgvacpfAllrs, 53, sqlStaccadv05);
					appVars.getDBObject(sqlgvacpfAllrs, 54, sqlStaccadv06);
					appVars.getDBObject(sqlgvacpfAllrs, 55, sqlStaccadv07);
					appVars.getDBObject(sqlgvacpfAllrs, 56, sqlStaccadv08);
					appVars.getDBObject(sqlgvacpfAllrs, 57, sqlStaccadv09);
					appVars.getDBObject(sqlgvacpfAllrs, 58, sqlStaccadv10);
					appVars.getDBObject(sqlgvacpfAllrs, 59, sqlStaccadv11);
					appVars.getDBObject(sqlgvacpfAllrs, 60, sqlStaccadv12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
		}
		else {
			sqlerrorflag = false;
			try {
				if (sqlgvacpfrs.next()) {
					appVars.getDBObject(sqlgvacpfrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgvacpfrs, 2, sqlAcctyr);
					appVars.getDBObject(sqlgvacpfrs, 3, sqlStfund);
					appVars.getDBObject(sqlgvacpfrs, 4, sqlStsect);
					appVars.getDBObject(sqlgvacpfrs, 5, sqlStssect);
					appVars.getDBObject(sqlgvacpfrs, 6, sqlRegister);
					appVars.getDBObject(sqlgvacpfrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgvacpfrs, 8, sqlCnttype);
					appVars.getDBObject(sqlgvacpfrs, 9, sqlCrtable);
					appVars.getDBObject(sqlgvacpfrs, 10, sqlAcctccy);
					appVars.getDBObject(sqlgvacpfrs, 11, sqlParind);
					appVars.getDBObject(sqlgvacpfrs, 12, sqlSrcebus);
					appVars.getDBObject(sqlgvacpfrs, 13, sqlStaccfyp01);
					appVars.getDBObject(sqlgvacpfrs, 14, sqlStaccfyp02);
					appVars.getDBObject(sqlgvacpfrs, 15, sqlStaccfyp03);
					appVars.getDBObject(sqlgvacpfrs, 16, sqlStaccfyp04);
					appVars.getDBObject(sqlgvacpfrs, 17, sqlStaccfyp05);
					appVars.getDBObject(sqlgvacpfrs, 18, sqlStaccfyp06);
					appVars.getDBObject(sqlgvacpfrs, 19, sqlStaccfyp07);
					appVars.getDBObject(sqlgvacpfrs, 20, sqlStaccfyp08);
					appVars.getDBObject(sqlgvacpfrs, 21, sqlStaccfyp09);
					appVars.getDBObject(sqlgvacpfrs, 22, sqlStaccfyp10);
					appVars.getDBObject(sqlgvacpfrs, 23, sqlStaccfyp11);
					appVars.getDBObject(sqlgvacpfrs, 24, sqlStaccfyp12);
					appVars.getDBObject(sqlgvacpfrs, 25, sqlStaccrnp01);
					appVars.getDBObject(sqlgvacpfrs, 26, sqlStaccrnp02);
					appVars.getDBObject(sqlgvacpfrs, 27, sqlStaccrnp03);
					appVars.getDBObject(sqlgvacpfrs, 28, sqlStaccrnp04);
					appVars.getDBObject(sqlgvacpfrs, 29, sqlStaccrnp05);
					appVars.getDBObject(sqlgvacpfrs, 30, sqlStaccrnp06);
					appVars.getDBObject(sqlgvacpfrs, 31, sqlStaccrnp07);
					appVars.getDBObject(sqlgvacpfrs, 32, sqlStaccrnp08);
					appVars.getDBObject(sqlgvacpfrs, 33, sqlStaccrnp09);
					appVars.getDBObject(sqlgvacpfrs, 34, sqlStaccrnp10);
					appVars.getDBObject(sqlgvacpfrs, 35, sqlStaccrnp11);
					appVars.getDBObject(sqlgvacpfrs, 36, sqlStaccrnp12);
					appVars.getDBObject(sqlgvacpfrs, 37, sqlStaccspd01);
					appVars.getDBObject(sqlgvacpfrs, 38, sqlStaccspd02);
					appVars.getDBObject(sqlgvacpfrs, 39, sqlStaccspd03);
					appVars.getDBObject(sqlgvacpfrs, 40, sqlStaccspd04);
					appVars.getDBObject(sqlgvacpfrs, 41, sqlStaccspd05);
					appVars.getDBObject(sqlgvacpfrs, 42, sqlStaccspd06);
					appVars.getDBObject(sqlgvacpfrs, 43, sqlStaccspd07);
					appVars.getDBObject(sqlgvacpfrs, 44, sqlStaccspd08);
					appVars.getDBObject(sqlgvacpfrs, 45, sqlStaccspd09);
					appVars.getDBObject(sqlgvacpfrs, 46, sqlStaccspd10);
					appVars.getDBObject(sqlgvacpfrs, 47, sqlStaccspd11);
					appVars.getDBObject(sqlgvacpfrs, 48, sqlStaccspd12);
					appVars.getDBObject(sqlgvacpfrs, 49, sqlStaccadv01);
					appVars.getDBObject(sqlgvacpfrs, 50, sqlStaccadv02);
					appVars.getDBObject(sqlgvacpfrs, 51, sqlStaccadv03);
					appVars.getDBObject(sqlgvacpfrs, 52, sqlStaccadv04);
					appVars.getDBObject(sqlgvacpfrs, 53, sqlStaccadv05);
					appVars.getDBObject(sqlgvacpfrs, 54, sqlStaccadv06);
					appVars.getDBObject(sqlgvacpfrs, 55, sqlStaccadv07);
					appVars.getDBObject(sqlgvacpfrs, 56, sqlStaccadv08);
					appVars.getDBObject(sqlgvacpfrs, 57, sqlStaccadv09);
					appVars.getDBObject(sqlgvacpfrs, 58, sqlStaccadv10);
					appVars.getDBObject(sqlgvacpfrs, 59, sqlStaccadv11);
					appVars.getDBObject(sqlgvacpfrs, 60, sqlStaccadv12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
		}
		wsaaSqlKeep.set(sqlGvacrec);
		if (isNE(wsaaFrecord,"Y")) {
			if (isNE(wsaaStsect,sqlStsect)
			|| isNE(wsaaReg,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaCompany,sqlChdrcoy)
			|| isNE(wsaaBranch,sqlCntbranch)) {
				wsaaOverflow.set("Y");
				h200PrintTotal();
			}
			else {
				if (isNE(wsaaParind,sqlParind)) {
					h200PrintSubtotal();
				}
			}
		}
		if (isNE(wsaaFrecord,"Y")) {
			if (isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaCompany,sqlChdrcoy)
			|| isNE(wsaaBranch,sqlCntbranch)) {
				h800WriteSummary();
				h100NewPage02();
			}
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		h500WriteEnd();
		h800WriteSummary();
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		writeDetail3080();
	}

protected void writeDetail3080()
	{
		if (isEQ(wsaaFrecord,"Y")) {
			wsaaFrecord = "N";
			h600SetHeading();
			h100NewPage01();
			h100NewPage02();
		}
		else {
			if (isNE(wsaaCrtable,sqlCrtable)) {
				staccfyp.set(wsaaStaccfyp);
				staccrnp.set(wsaaStaccrnp);
				staccspd.set(wsaaStaccspd);
				if (newPageReq.isTrue()) {
					h100NewPage01();
				}
				rj52201.printRj52201d01(rj52201D01, indicArea);
				wsaaStaccfyp.set(ZERO);
				wsaaStaccrnp.set(ZERO);
				wsaaStaccspd.set(ZERO);
			}
		}
		h400ReadT5687();
		crtabled.set(descIO.getLongdesc());
		crtable.set(sqlCrtable);
		wsaaStaccfyp.add(wsaaSStaccfyp[pj517par.acctmnth.toInt()]);
		wsaaStaccrnp.add(wsaaSStaccrnp[pj517par.acctmnth.toInt()]);
		wsaaStaccspd.add(wsaaSStaccspd[pj517par.acctmnth.toInt()]);
		wsaaTotadv.add(wsaaSStaccadv[pj517par.acctmnth.toInt()]);
		wsaaTotal01.add(wsaaSStaccfyp[pj517par.acctmnth.toInt()]);
		wsaaTamt01.add(wsaaSStaccfyp[pj517par.acctmnth.toInt()]);
		wsaaTreg01.add(wsaaSStaccfyp[pj517par.acctmnth.toInt()]);
		wsaaTotal02.add(wsaaSStaccrnp[pj517par.acctmnth.toInt()]);
		wsaaTamt02.add(wsaaSStaccrnp[pj517par.acctmnth.toInt()]);
		wsaaTreg02.add(wsaaSStaccrnp[pj517par.acctmnth.toInt()]);
		wsaaTotal03.add(wsaaSStaccspd[pj517par.acctmnth.toInt()]);
		wsaaTamt03.add(wsaaSStaccspd[pj517par.acctmnth.toInt()]);
		wsaaTreg02.add(wsaaSStaccspd[pj517par.acctmnth.toInt()]);
		wsaaStsect.set(sqlStsect);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaCompany.set(sqlChdrcoy);
		wsaaBranch.set(sqlCntbranch);
		wsaaCrtable.set(sqlCrtable);
		wsaaReg.set(sqlRegister);
		wsaaParind.set(sqlParind);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (selectAll.isTrue()) {
			appVars.freeDBConnectionIgnoreErr(sqlgvacpfAllconn, sqlgvacpfAllps, sqlgvacpfAllrs);
		}
		else {
			appVars.freeDBConnectionIgnoreErr(sqlgvacpfconn, sqlgvacpfps, sqlgvacpfrs);
		}
		rj52201.close();
		rj52202.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void readLins5000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile5010();
				}
				case nextr5080: {
					nextr5080();
				}
				case exit5090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile5010()
	{
		SmartFileCode.execute(appVars, linsrnlIO);
		if (isNE(linsrnlIO.getStatuz(),varcom.oK)
		&& isNE(linsrnlIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(linsrnlIO.getParams());
			fatalError600();
		}
		if (isEQ(linsrnlIO.getStatuz(),varcom.endp)
		|| isNE(linsrnlIO.getChdrcoy(),wsaaCompany)) {
			linsrnlIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit5090);
		}
		if (isNE(linsrnlIO.getBranch(),wsaaBranch)) {
			goTo(GotoLabel.nextr5080);
		}
		if (isNE(linsrnlIO.getBillcurr(),wsaaPrevBillcurr)) {
			wsaaPrevBillcurr.set(linsrnlIO.getBillcurr());
			a100CheckRate();
		}
		if (isEQ(wsaaAcctccy,t3629rec.ledgcurr)) {
			compute(wsaaInstamt06, 8).setRounded(mult(linsrnlIO.getInstamt06(),wsaaScrate));
			wsaaTotamt01.add(wsaaInstamt06);
		}
	}

protected void nextr5080()
	{
		linsrnlIO.setFunction(varcom.nextr);
	}

protected void a100CheckRate()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a110Para();
				}
				case a120CallItemio: {
					a120CallItemio();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Para()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemitem(linsrnlIO.getBillcurr());
		itemIO.setItemtabl(t3629);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
	}

protected void a120CallItemio()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaScrate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaScrate,ZERO)
		|| isGT(wsaaX,7))) {
			if (isGTE(datcon1rec.intDate,t3629rec.frmdate[wsaaX.toInt()])
			&& isLTE(datcon1rec.intDate,t3629rec.todate[wsaaX.toInt()])) {
				wsaaScrate.set(t3629rec.scrate[wsaaX.toInt()]);
			}
			else {
				wsaaX.add(1);
			}
		}

		if (isEQ(wsaaScrate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.a120CallItemio);
			}
			else {
				syserrrec.statuz.set(g418);
				fatalError600();
			}
		}
	}

protected void h100NewPage01()
	{
		h110Start();
	}

protected void h110Start()
	{
		rj52201.printRj52201h01(rj52201H01, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		if (isEQ(sqlParind,"P")) {
			indOn.setTrue(11);
			indOff.setTrue(12);
		}
		else {
			indOn.setTrue(12);
			indOff.setTrue(11);
		}
		rj52201.printRj52201h02(rj52201H02, indicArea);
		rj52201.printRj52201h03(rj52201H03);
	}

protected void h100NewPage02()
	{
		/*H111-START*/
		rj52202.printRj52202h01(rj52202H01, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		/*H191-EXIT*/
	}

protected void h200PrintTotal()
	{
		h210Start();
	}

protected void h210Start()
	{
		staccfyp.set(wsaaStaccfyp);
		staccrnp.set(wsaaStaccrnp);
		staccspd.set(wsaaStaccspd);
		rj52201.printRj52201d01(rj52201D01, indicArea);
		wsaaStaccfyp.set(ZERO);
		wsaaStaccrnp.set(ZERO);
		wsaaStaccspd.set(ZERO);
		wsaaCrtable.set(sqlCrtable);
		total01.set(wsaaTotal01);
		total02.set(wsaaTotal02);
		total03.set(wsaaTotal03);
		rj52201.printRj52201t01(rj52201T01, indicArea);
		totalamt01.set(wsaaTamt01);
		totalamt02.set(wsaaTamt02);
		totalamt03.set(wsaaTamt03);
		rj52201.printRj52201t02(rj52201T02, indicArea);
		if (isNE(wsaaReg,sqlRegister)
		|| isNE(wsaaAcctccy,sqlAcctccy)) {
			descrip1.set(wsaaRegDesp);
			indOff.setTrue(13);
			indOff.setTrue(14);
			compute(tamt, 2).set(add(add(wsaaTreg01,wsaaTreg02),wsaaTreg03));
			rj52202.printRj52202d01(rj52202D01, indicArea);
			wsaaTreg01.set(0);
			wsaaTreg02.set(0);
			wsaaTreg03.set(0);
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaTamt01);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaTamt02);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(wsaaTamt03);
		callContot001();
		h600SetHeading();
		h100NewPage01();
		wsaaTotal01.set(ZERO);
		wsaaTotal02.set(ZERO);
		wsaaTotal03.set(ZERO);
		wsaaTamt01.set(ZERO);
		wsaaTamt02.set(ZERO);
		wsaaTamt03.set(ZERO);
	}

protected void h200PrintSubtotal()
	{
		h211Start();
	}

protected void h211Start()
	{
		staccfyp.set(wsaaStaccfyp);
		staccrnp.set(wsaaStaccrnp);
		staccspd.set(wsaaStaccspd);
		rj52201.printRj52201d01(rj52201D01, indicArea);
		wsaaStaccfyp.set(ZERO);
		wsaaStaccrnp.set(ZERO);
		wsaaStaccspd.set(ZERO);
		wsaaCrtable.set(sqlCrtable);
		total01.set(wsaaTotal01);
		total02.set(wsaaTotal02);
		total03.set(wsaaTotal03);
		rj52201.printRj52201t01(rj52201T01, indicArea);
		if (isEQ(sqlParind,"P")) {
			indOn.setTrue(11);
			indOff.setTrue(12);
		}
		else {
			indOn.setTrue(12);
			indOff.setTrue(11);
		}
		rj52201.printRj52201h02(rj52201H02, indicArea);
		rj52201.printRj52201d02(rj52201D02);
		wsaaTotal01.set(ZERO);
		wsaaTotal02.set(ZERO);
		wsaaTotal03.set(ZERO);
	}

protected void h400ReadT5687()
	{
		h410Start();
	}

protected void h410Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(sqlCrtable);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("?????");
		}
	}

protected void h500WriteEnd()
	{
		h510Start();
	}

protected void h510Start()
	{
		staccfyp.set(wsaaStaccfyp);
		staccrnp.set(wsaaStaccrnp);
		staccspd.set(wsaaStaccspd);
		rj52201.printRj52201d01(rj52201D01, indicArea);
		total01.set(wsaaTotal01);
		total02.set(wsaaTotal02);
		total03.set(wsaaTotal03);
		rj52201.printRj52201t01(rj52201T01, indicArea);
		totalamt01.set(wsaaTamt01);
		totalamt02.set(wsaaTamt02);
		totalamt03.set(wsaaTamt03);
		rj52201.printRj52201t02(rj52201T02, indicArea);
		wsaaOverflow.set("N");
		descrip1.set(wsaaRegDesp);
		indOff.setTrue(13);
		indOff.setTrue(14);
		compute(tamt, 2).set(add(add(wsaaTreg01,wsaaTreg02),wsaaTreg03));
		rj52202.printRj52202d01(rj52202D01, indicArea);
	}

protected void h600SetHeading()
	{
		h610Start();
	}

protected void h610Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(sqlChdrcoy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(sqlChdrcoy);
		rh01Companynm.set(descIO.getLongdesc());
		rh01Company1.set(sqlChdrcoy);
		rh01Companynm1.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sqlAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		acctccy.set(sqlAcctccy);
		currdesc.set(descIO.getLongdesc());
		acctccy1.set(sqlAcctccy);
		currdesc1.set(descIO.getLongdesc());
		//ILIFE-6360 starts
		if(isNE(sqlRegister,SPACES)){
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3589);
		descIO.setDescitem(sqlRegister);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		srcebus.set(sqlRegister);
		descrip.set(descIO.getLongdesc());
		wsaaRegDesp.set(descIO.getLongdesc());
		}
		
		else{
			descrip.set(SPACES);
			wsaaRegDesp.set(SPACES);
			srcebus.set(SPACES);
		}//ILIFE-6360 ends
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5685);
		descIO.setDescitem(sqlStsect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stsect.set(sqlStsect);
		itmdesc.set(descIO.getLongdesc());
	}

protected void h800WriteSummary()
	{
		h810Start();
	}

protected void h810Start()
	{
		linsrnlIO.setStatuz(SPACES);
		linsrnlIO.setChdrcoy(bsprIO.getCompany());
		linsrnlIO.setChdrnum(SPACES);
		linsrnlIO.setInstfrom(ZERO);
		linsrnlIO.setFunction(varcom.begn);
		linsrnlIO.setFormat(linsrnlrec);
		while ( !(isEQ(linsrnlIO.getStatuz(),varcom.endp))) {


			//performance improvement --  atiwari23
			linsrnlIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			linsrnlIO.setFitKeysSearch("CHDRCOY");

			readLins5000();
		}

		indOn.setTrue(13);
		indOff.setTrue(14);
		tamt.set(wsaaTotamt01);
		rj52202.printRj52202d01(rj52202D01, indicArea);
		indOn.setTrue(14);
		indOff.setTrue(13);
		tamt.set(wsaaTotadv);
		rj52202.printRj52202d01(rj52202D01, indicArea);
		wsaaTotamt01.set(ZERO);
		wsaaTotadv.set(ZERO);
	}
}
