/*
 * File: Bj525.java
 * Date: 29 August 2009 21:42:02
 * Author: Quipoz Limited
 *
 * Class transformed from BJ525.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj52501Report;
import com.csc.life.statistics.reports.Rj52502Report;
import com.csc.life.statistics.tablestructures.Tj675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*               Benefits Payment Details
*               ========================
*
*   This report will list all benefits payment during the specify
*   accouting year and month
*
*****************************************************************
* </pre>
*/
public class Bj525 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgvacpfAllrs = null;
	private java.sql.PreparedStatement sqlgvacpfAllps = null;
	private java.sql.Connection sqlgvacpfAllconn = null;
	private String sqlgvacpfAll = "";
	private java.sql.ResultSet sqlgvacpfrs = null;
	private java.sql.PreparedStatement sqlgvacpfps = null;
	private java.sql.Connection sqlgvacpfconn = null;
	private String sqlgvacpf = "";
	private Rj52501Report rj52501 = new Rj52501Report();
	private Rj52502Report rj52502 = new Rj52502Report();
	private FixedLengthStringData rj52501Rec = new FixedLengthStringData(250);
	private FixedLengthStringData rj52502Rec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ525");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);

	private FixedLengthStringData wsaaSelectAll = new FixedLengthStringData(1);
	private Validator selectAll = new Validator(wsaaSelectAll, "Y");
	private FixedLengthStringData wsaaTj675Stsect01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect02 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect03 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect04 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect05 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect06 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect07 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect08 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect09 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect10 = new FixedLengthStringData(2);
	private PackedDecimalData wsaaTotal = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTotamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt01 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt02 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt03 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt04 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAsAtPndDthclm = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAsAtAprDthclm = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAsAtRiclm = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPndDthclm = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaPndRiclm = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAprDthclm = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAprRiclm = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAprAdjust = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAprInterest = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaMatClm = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaMatRiclm = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAnnuity = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaAnnuityRi = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRegpay = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRegpayRi = new PackedDecimalData(18, 2);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaParind = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaRegister = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaCompanyS = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranchS = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccyS = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaRegisterS = new FixedLengthStringData(3);
	private String wsaaFrecord = "";
	private String wsaaFlag = "";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-GVACPF */
	private FixedLengthStringData sqlGvacrec = new FixedLengthStringData(901);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 0);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGvacrec, 1);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 4);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 5);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGvacrec, 7);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 11);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 14);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 16);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGvacrec, 19);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 23);
	private FixedLengthStringData sqlParind = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 26);
	private FixedLengthStringData sqlSrcebus = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 27);
	private FixedLengthStringData sqlStatcode = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 29);
	private PackedDecimalData sqlStaccmtb01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 31);
	private PackedDecimalData sqlStaccmtb02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 41);
	private PackedDecimalData sqlStaccmtb03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 51);
	private PackedDecimalData sqlStaccmtb04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 61);
	private PackedDecimalData sqlStaccmtb05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 71);
	private PackedDecimalData sqlStaccmtb06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 81);
	private PackedDecimalData sqlStaccmtb07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 91);
	private PackedDecimalData sqlStaccmtb08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 101);
	private PackedDecimalData sqlStaccmtb09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 111);
	private PackedDecimalData sqlStaccmtb10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 121);
	private PackedDecimalData sqlStaccmtb11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 131);
	private PackedDecimalData sqlStaccmtb12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 141);
	private PackedDecimalData sqlStaccpdb01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 151);
	private PackedDecimalData sqlStaccpdb02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 161);
	private PackedDecimalData sqlStaccpdb03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 171);
	private PackedDecimalData sqlStaccpdb04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 181);
	private PackedDecimalData sqlStaccpdb05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 191);
	private PackedDecimalData sqlStaccpdb06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 201);
	private PackedDecimalData sqlStaccpdb07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 211);
	private PackedDecimalData sqlStaccpdb08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 221);
	private PackedDecimalData sqlStaccpdb09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 231);
	private PackedDecimalData sqlStaccpdb10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 241);
	private PackedDecimalData sqlStaccpdb11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 251);
	private PackedDecimalData sqlStaccpdb12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 261);
	private PackedDecimalData sqlStaccadb01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 271);
	private PackedDecimalData sqlStaccadb02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 281);
	private PackedDecimalData sqlStaccadb03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 291);
	private PackedDecimalData sqlStaccadb04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 301);
	private PackedDecimalData sqlStaccadb05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 311);
	private PackedDecimalData sqlStaccadb06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 321);
	private PackedDecimalData sqlStaccadb07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 331);
	private PackedDecimalData sqlStaccadb08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 341);
	private PackedDecimalData sqlStaccadb09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 351);
	private PackedDecimalData sqlStaccadb10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 361);
	private PackedDecimalData sqlStaccadb11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 371);
	private PackedDecimalData sqlStaccadb12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 381);
	private PackedDecimalData sqlStaccclr01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 391);
	private PackedDecimalData sqlStaccclr02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 401);
	private PackedDecimalData sqlStaccclr03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 411);
	private PackedDecimalData sqlStaccclr04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 421);
	private PackedDecimalData sqlStaccclr05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 431);
	private PackedDecimalData sqlStaccclr06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 441);
	private PackedDecimalData sqlStaccclr07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 451);
	private PackedDecimalData sqlStaccclr08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 461);
	private PackedDecimalData sqlStaccclr09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 471);
	private PackedDecimalData sqlStaccclr10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 481);
	private PackedDecimalData sqlStaccclr11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 491);
	private PackedDecimalData sqlStaccclr12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 501);
	private PackedDecimalData sqlStaccadj01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 511);
	private PackedDecimalData sqlStaccadj02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 521);
	private PackedDecimalData sqlStaccadj03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 531);
	private PackedDecimalData sqlStaccadj04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 541);
	private PackedDecimalData sqlStaccadj05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 551);
	private PackedDecimalData sqlStaccadj06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 561);
	private PackedDecimalData sqlStaccadj07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 571);
	private PackedDecimalData sqlStaccadj08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 581);
	private PackedDecimalData sqlStaccadj09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 591);
	private PackedDecimalData sqlStaccadj10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 601);
	private PackedDecimalData sqlStaccadj11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 611);
	private PackedDecimalData sqlStaccadj12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 621);
	private PackedDecimalData sqlStaccint01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 631);
	private PackedDecimalData sqlStaccint02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 641);
	private PackedDecimalData sqlStaccint03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 651);
	private PackedDecimalData sqlStaccint04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 661);
	private PackedDecimalData sqlStaccint05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 671);
	private PackedDecimalData sqlStaccint06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 681);
	private PackedDecimalData sqlStaccint07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 691);
	private PackedDecimalData sqlStaccint08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 701);
	private PackedDecimalData sqlStaccint09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 711);
	private PackedDecimalData sqlStaccint10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 721);
	private PackedDecimalData sqlStaccint11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 731);
	private PackedDecimalData sqlStaccint12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 741);
	private PackedDecimalData sqlStaccob01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 751);
	private PackedDecimalData sqlStaccob02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 761);
	private PackedDecimalData sqlStaccob03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 771);
	private PackedDecimalData sqlStaccob04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 781);
	private PackedDecimalData sqlStaccob05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 791);
	private PackedDecimalData sqlStaccob06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 801);
	private PackedDecimalData sqlStaccob07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 811);
	private PackedDecimalData sqlStaccob08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 821);
	private PackedDecimalData sqlStaccob09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 831);
	private PackedDecimalData sqlStaccob10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 841);
	private PackedDecimalData sqlStaccob11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 851);
	private PackedDecimalData sqlStaccob12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 861);
	private PackedDecimalData sqlBfwdpdb = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 871);
	private PackedDecimalData sqlBfwdadb = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 881);
	private PackedDecimalData sqlBfwdclr = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 891);

	private FixedLengthStringData wsaaSqlKeep = new FixedLengthStringData(901);
	private FixedLengthStringData wsaaSChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 0);
	private PackedDecimalData wsaaSAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaSqlKeep, 1);
	private FixedLengthStringData wsaaSStfund = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 4);
	private FixedLengthStringData wsaaSStsect = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 5);
	private FixedLengthStringData wsaaSStssect = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 7);
	private FixedLengthStringData wsaaSRegister = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 11);
	private FixedLengthStringData wsaaSCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 14);
	private FixedLengthStringData wsaaSCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 16);
	private FixedLengthStringData wsaaSCrtable = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 19);
	private FixedLengthStringData wsaaSAcctccy = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 23);
	private FixedLengthStringData wsaaSParind = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 26);
	private FixedLengthStringData wsaaSSrcebus = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 27);
	private FixedLengthStringData wsaaSStatcode = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 29);
	private PackedDecimalData[] wsaaSStaccmtb = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 31);
	private PackedDecimalData[] wsaaSStaccpdb = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 151);
	private PackedDecimalData[] wsaaSStaccadb = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 271);
	private PackedDecimalData[] wsaaSStaccclr = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 391);
	private PackedDecimalData[] wsaaSStaccadj = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 511);
	private PackedDecimalData[] wsaaSStaccint = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 631);
	private PackedDecimalData[] wsaaSStaccob = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 751);
	private PackedDecimalData wsaaSBfwdpdb = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 871);
	private PackedDecimalData wsaaSBfwdadb = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 881);
	private PackedDecimalData wsaaSBfwdclr = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 891);
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
		/* ERRORS */
	private String esql = "ESQL";
		/* TABLES */
	private String t1693 = "T1693";
	private String t3629 = "T3629";
	private String t3589 = "T3589";
	private String t5685 = "T5685";
	private String t6625 = "T6625";
	private String tj675 = "TJ675";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rj52501H01 = new FixedLengthStringData(145);
	private FixedLengthStringData rj52501h01O = new FixedLengthStringData(145).isAPartOf(rj52501H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj52501h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj52501h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj52501h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj52501h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj52501h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj52501h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj52501h01O, 50);
	private FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(rj52501h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj52501h01O, 83);
	private FixedLengthStringData stsect = new FixedLengthStringData(2).isAPartOf(rj52501h01O, 113);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30).isAPartOf(rj52501h01O, 115);

	private FixedLengthStringData rj52501D01 = new FixedLengthStringData(72);
	private FixedLengthStringData rj52501d01O = new FixedLengthStringData(72).isAPartOf(rj52501D01, 0);
	private ZonedDecimalData tamt01 = new ZonedDecimalData(18, 2).isAPartOf(rj52501d01O, 0);
	private ZonedDecimalData tamt02 = new ZonedDecimalData(18, 2).isAPartOf(rj52501d01O, 18);
	private ZonedDecimalData tamt03 = new ZonedDecimalData(18, 2).isAPartOf(rj52501d01O, 36);
	private ZonedDecimalData tamt04 = new ZonedDecimalData(18, 2).isAPartOf(rj52501d01O, 54);

	private FixedLengthStringData rj52501D02 = new FixedLengthStringData(2);

	private FixedLengthStringData rj52502H01 = new FixedLengthStringData(80);
	private FixedLengthStringData rj52502h01O = new FixedLengthStringData(80).isAPartOf(rj52502H01, 0);
	private ZonedDecimalData acctmonth1 = new ZonedDecimalData(2, 0).isAPartOf(rj52502h01O, 0);
	private ZonedDecimalData acctyear1 = new ZonedDecimalData(4, 0).isAPartOf(rj52502h01O, 2);
	private FixedLengthStringData rh01Company1 = new FixedLengthStringData(1).isAPartOf(rj52502h01O, 6);
	private FixedLengthStringData rh01Companynm1 = new FixedLengthStringData(30).isAPartOf(rj52502h01O, 7);
	private FixedLengthStringData rh01Sdate1 = new FixedLengthStringData(10).isAPartOf(rj52502h01O, 37);
	private FixedLengthStringData acctccy1 = new FixedLengthStringData(3).isAPartOf(rj52502h01O, 47);
	private FixedLengthStringData currdesc1 = new FixedLengthStringData(30).isAPartOf(rj52502h01O, 50);

	private FixedLengthStringData rj52502D01 = new FixedLengthStringData(48);
	private FixedLengthStringData rj52502d01O = new FixedLengthStringData(48).isAPartOf(rj52502D01, 0);
	private FixedLengthStringData ditdsc = new FixedLengthStringData(30).isAPartOf(rj52502d01O, 0);
	private ZonedDecimalData tamt = new ZonedDecimalData(18, 2).isAPartOf(rj52502d01O, 30);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Pj517par pj517par = new Pj517par();
	private Tj675rec tj675rec = new Tj675rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090
	}

	public Bj525() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		rj52501.openOutput();
		rj52502.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaAcctyr.set(pj517par.acctyr);
		acctyear.set(pj517par.acctyr);
		acctmonth.set(pj517par.acctmnth);
		acctyear1.set(pj517par.acctyr);
		acctmonth1.set(pj517par.acctmnth);
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		rh01Sdate1.set(datcon1rec.extDate);
		wsaaTotal.set(ZERO);
		wsaaTotamt.set(ZERO);
		wsaaTamt.set(ZERO);
		wsaaAsAtPndDthclm.set(ZERO);
		wsaaAsAtAprDthclm.set(ZERO);
		wsaaAsAtRiclm.set(ZERO);
		wsaaAprDthclm.set(ZERO);
		wsaaAprAdjust.set(ZERO);
		wsaaAprInterest.set(ZERO);
		wsaaMatClm.set(ZERO);
		wsaaMatRiclm.set(ZERO);
		wsaaAnnuity.set(ZERO);
		wsaaAnnuityRi.set(ZERO);
		wsaaRegpay.set(ZERO);
		wsaaRegpayRi.set(ZERO);
		wsaaStsect.set(SPACES);
		wsaaBranchS.set(SPACES);
		wsaaCompanyS.set(SPACES);
		wsaaAcctccyS.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaRegister.set(SPACES);
		wsaaFrecord = "Y";
		readTj6751300();
		/*EXIT*/
	}

protected void readTj6751300()
	{
		para1310();
	}

protected void para1310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tj675);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		wsaaSelectAll.set("Y");
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			selectAll1400();
		}
		else {
			wsaaSelectAll.set("N");
			tj675rec.tj675Rec.set(itemIO.getGenarea());
			selectSpecified1500();
		}
	}

protected void selectAll1400()
	{
		/*PARA*/
		//ILIFE-1149
		//sqlgvacpfAll = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REGISTER, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STATCODE, STACCMTB01, STACCMTB02, STACCMTB03, STACCMTB04, STACCMTB05, STACCMTB06, STACCMTB07, STACCMTB08, STACCMTB09, STACCMTB10, STACCMTB11, STACCMTB12, STACCPDB01, STACCPDB02, STACCPDB03, STACCPDB04, STACCPDB05, STACCPDB06, STACCPDB07, STACCPDB08, STACCPDB09, STACCPDB10, STACCPDB11, STACCPDB12, STACCADB01, STACCADB02, STACCADB03, STACCADB04, STACCADB05, STACCADB06, STACCADB07, STACCADB08, STACCADB09, STACCADB10, STACCADB11, STACCADB12, STACCCLR01, STACCCLR02, STACCCLR03, STACCCLR04, STACCCLR05, STACCCLR06, STACCCLR07, STACCCLR08, STACCCLR09, STACCCLR10, STACCCLR11, STACCCLR12, STACCADJ01, STACCADJ02, STACCADJ03, STACCADJ04, STACCADJ05, STACCADJ06, STACCADJ07, STACCADJ08, STACCADJ09, STACCADJ10, STACCADJ11, STACCADJ12, STACCINT01, STACCINT02, STACCINT03, STACCINT04, STACCINT05, STACCINT06, STACCINT07, STACCINT08, STACCINT09, STACCINT10, STACCINT11, STACCINT12, STACCOB01, STACCOB02, STACCOB03, STACCOB04, STACCOB05, STACCOB06, STACCOB07, STACCOB08, STACCOB09, STACCOB10, STACCOB11, STACCOB12, BFWDPDB, BFWDADB, BFWDCLR" +
		sqlgvacpfAll = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STATCODE, STACCMTB01, STACCMTB02, STACCMTB03, STACCMTB04, STACCMTB05, STACCMTB06, STACCMTB07, STACCMTB08, STACCMTB09, STACCMTB10, STACCMTB11, STACCMTB12, STACCPDB01, STACCPDB02, STACCPDB03, STACCPDB04, STACCPDB05, STACCPDB06, STACCPDB07, STACCPDB08, STACCPDB09, STACCPDB10, STACCPDB11, STACCPDB12, STACCADB01, STACCADB02, STACCADB03, STACCADB04, STACCADB05, STACCADB06, STACCADB07, STACCADB08, STACCADB09, STACCADB10, STACCADB11, STACCADB12, STACCCLR01, STACCCLR02, STACCCLR03, STACCCLR04, STACCCLR05, STACCCLR06, STACCCLR07, STACCCLR08, STACCCLR09, STACCCLR10, STACCCLR11, STACCCLR12, STACCADJ01, STACCADJ02, STACCADJ03, STACCADJ04, STACCADJ05, STACCADJ06, STACCADJ07, STACCADJ08, STACCADJ09, STACCADJ10, STACCADJ11, STACCADJ12, STACCINT01, STACCINT02, STACCINT03, STACCINT04, STACCINT05, STACCINT06, STACCINT07, STACCINT08, STACCINT09, STACCINT10, STACCINT11, STACCINT12, STACCOB01, STACCOB02, STACCOB03, STACCOB04, STACCOB05, STACCOB06, STACCOB07, STACCOB08, STACCOB09, STACCOB10, STACCOB11, STACCOB12, BFWDPDB, BFWDADB, BFWDCLR" +
" FROM   " + appVars.getTableNameOverriden("GVACPF") + " " +
" WHERE ACCTYR = ?" +
//ILIFE-1149
//" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REGISTER, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REG, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
		sqlerrorflag = false;
		try {
			sqlgvacpfAllconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvacpfTableDAM());
			sqlgvacpfAllps = appVars.prepareStatementEmbeded(sqlgvacpfAllconn, sqlgvacpfAll, "GVACPF");
			appVars.setDBDouble(sqlgvacpfAllps, 1, wsaaAcctyr.toDouble());
			sqlgvacpfAllrs = appVars.executeQuery(sqlgvacpfAllps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		/*EXIT*/
	}

protected void selectSpecified1500()
	{
		para1510();
	}

protected void para1510()
	{
		wsaaTj675Stsect01.set(tj675rec.statSect[1]);
		wsaaTj675Stsect02.set(tj675rec.statSect[2]);
		wsaaTj675Stsect03.set(tj675rec.statSect[3]);
		wsaaTj675Stsect04.set(tj675rec.statSect[4]);
		wsaaTj675Stsect05.set(tj675rec.statSect[5]);
		wsaaTj675Stsect06.set(tj675rec.statSect[6]);
		wsaaTj675Stsect07.set(tj675rec.statSect[7]);
		wsaaTj675Stsect08.set(tj675rec.statSect[8]);
		wsaaTj675Stsect09.set(tj675rec.statSect[9]);
		wsaaTj675Stsect10.set(tj675rec.statSect[10]);
		//ILIFE-1149
		//sqlgvacpf = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REGISTER, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STATCODE, STACCMTB01, STACCMTB02, STACCMTB03, STACCMTB04, STACCMTB05, STACCMTB06, STACCMTB07, STACCMTB08, STACCMTB09, STACCMTB10, STACCMTB11, STACCMTB12, STACCPDB01, STACCPDB02, STACCPDB03, STACCPDB04, STACCPDB05, STACCPDB06, STACCPDB07, STACCPDB08, STACCPDB09, STACCPDB10, STACCPDB11, STACCPDB12, STACCADB01, STACCADB02, STACCADB03, STACCADB04, STACCADB05, STACCADB06, STACCADB07, STACCADB08, STACCADB09, STACCADB10, STACCADB11, STACCADB12, STACCCLR01, STACCCLR02, STACCCLR03, STACCCLR04, STACCCLR05, STACCCLR06, STACCCLR07, STACCCLR08, STACCCLR09, STACCCLR10, STACCCLR11, STACCCLR12, STACCADJ01, STACCADJ02, STACCADJ03, STACCADJ04, STACCADJ05, STACCADJ06, STACCADJ07, STACCADJ08, STACCADJ09, STACCADJ10, STACCADJ11, STACCADJ12, STACCINT01, STACCINT02, STACCINT03, STACCINT04, STACCINT05, STACCINT06, STACCINT07, STACCINT08, STACCINT09, STACCINT10, STACCINT11, STACCINT12, STACCOB01, STACCOB02, STACCOB03, STACCOB04, STACCOB05, STACCOB06, STACCOB07, STACCOB08, STACCOB09, STACCOB10, STACCOB11, STACCOB12, BFWDPDB, BFWDADB, BFWDCLR" +
		sqlgvacpf = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STATCODE, STACCMTB01, STACCMTB02, STACCMTB03, STACCMTB04, STACCMTB05, STACCMTB06, STACCMTB07, STACCMTB08, STACCMTB09, STACCMTB10, STACCMTB11, STACCMTB12, STACCPDB01, STACCPDB02, STACCPDB03, STACCPDB04, STACCPDB05, STACCPDB06, STACCPDB07, STACCPDB08, STACCPDB09, STACCPDB10, STACCPDB11, STACCPDB12, STACCADB01, STACCADB02, STACCADB03, STACCADB04, STACCADB05, STACCADB06, STACCADB07, STACCADB08, STACCADB09, STACCADB10, STACCADB11, STACCADB12, STACCCLR01, STACCCLR02, STACCCLR03, STACCCLR04, STACCCLR05, STACCCLR06, STACCCLR07, STACCCLR08, STACCCLR09, STACCCLR10, STACCCLR11, STACCCLR12, STACCADJ01, STACCADJ02, STACCADJ03, STACCADJ04, STACCADJ05, STACCADJ06, STACCADJ07, STACCADJ08, STACCADJ09, STACCADJ10, STACCADJ11, STACCADJ12, STACCINT01, STACCINT02, STACCINT03, STACCINT04, STACCINT05, STACCINT06, STACCINT07, STACCINT08, STACCINT09, STACCINT10, STACCINT11, STACCINT12, STACCOB01, STACCOB02, STACCOB03, STACCOB04, STACCOB05, STACCOB06, STACCOB07, STACCOB08, STACCOB09, STACCOB10, STACCOB11, STACCOB12, BFWDPDB, BFWDADB, BFWDCLR" +
" FROM   " + appVars.getTableNameOverriden("GVACPF") + " " +
" WHERE ACCTYR = ?" +
" AND (STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?)" +
//ILIFE-1149
//" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REGISTER, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REG, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
		sqlerrorflag = false;
		try {
			sqlgvacpfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvacpfTableDAM());
			sqlgvacpfps = appVars.prepareStatementEmbeded(sqlgvacpfconn, sqlgvacpf, "GVACPF");
			appVars.setDBDouble(sqlgvacpfps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgvacpfps, 2, wsaaTj675Stsect01.toString());
			appVars.setDBString(sqlgvacpfps, 3, wsaaTj675Stsect02.toString());
			appVars.setDBString(sqlgvacpfps, 4, wsaaTj675Stsect03.toString());
			appVars.setDBString(sqlgvacpfps, 5, wsaaTj675Stsect04.toString());
			appVars.setDBString(sqlgvacpfps, 6, wsaaTj675Stsect05.toString());
			appVars.setDBString(sqlgvacpfps, 7, wsaaTj675Stsect06.toString());
			appVars.setDBString(sqlgvacpfps, 8, wsaaTj675Stsect07.toString());
			appVars.setDBString(sqlgvacpfps, 9, wsaaTj675Stsect08.toString());
			appVars.setDBString(sqlgvacpfps, 10, wsaaTj675Stsect09.toString());
			appVars.setDBString(sqlgvacpfps, 11, wsaaTj675Stsect10.toString());
			sqlgvacpfrs = appVars.executeQuery(sqlgvacpfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		if (selectAll.isTrue()) {
			sqlerrorflag = false;
			try {
				if (sqlgvacpfAllrs.next()) {
					appVars.getDBObject(sqlgvacpfAllrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgvacpfAllrs, 2, sqlAcctyr);
					appVars.getDBObject(sqlgvacpfAllrs, 3, sqlStfund);
					appVars.getDBObject(sqlgvacpfAllrs, 4, sqlStsect);
					appVars.getDBObject(sqlgvacpfAllrs, 5, sqlStssect);
					appVars.getDBObject(sqlgvacpfAllrs, 6, sqlRegister);
					appVars.getDBObject(sqlgvacpfAllrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgvacpfAllrs, 8, sqlCnttype);
					appVars.getDBObject(sqlgvacpfAllrs, 9, sqlCrtable);
					appVars.getDBObject(sqlgvacpfAllrs, 10, sqlAcctccy);
					appVars.getDBObject(sqlgvacpfAllrs, 11, sqlParind);
					appVars.getDBObject(sqlgvacpfAllrs, 12, sqlSrcebus);
					appVars.getDBObject(sqlgvacpfAllrs, 13, sqlStatcode);
					appVars.getDBObject(sqlgvacpfAllrs, 14, sqlStaccmtb01);
					appVars.getDBObject(sqlgvacpfAllrs, 15, sqlStaccmtb02);
					appVars.getDBObject(sqlgvacpfAllrs, 16, sqlStaccmtb03);
					appVars.getDBObject(sqlgvacpfAllrs, 17, sqlStaccmtb04);
					appVars.getDBObject(sqlgvacpfAllrs, 18, sqlStaccmtb05);
					appVars.getDBObject(sqlgvacpfAllrs, 19, sqlStaccmtb06);
					appVars.getDBObject(sqlgvacpfAllrs, 20, sqlStaccmtb07);
					appVars.getDBObject(sqlgvacpfAllrs, 21, sqlStaccmtb08);
					appVars.getDBObject(sqlgvacpfAllrs, 22, sqlStaccmtb09);
					appVars.getDBObject(sqlgvacpfAllrs, 23, sqlStaccmtb10);
					appVars.getDBObject(sqlgvacpfAllrs, 24, sqlStaccmtb11);
					appVars.getDBObject(sqlgvacpfAllrs, 25, sqlStaccmtb12);
					appVars.getDBObject(sqlgvacpfAllrs, 26, sqlStaccpdb01);
					appVars.getDBObject(sqlgvacpfAllrs, 27, sqlStaccpdb02);
					appVars.getDBObject(sqlgvacpfAllrs, 28, sqlStaccpdb03);
					appVars.getDBObject(sqlgvacpfAllrs, 29, sqlStaccpdb04);
					appVars.getDBObject(sqlgvacpfAllrs, 30, sqlStaccpdb05);
					appVars.getDBObject(sqlgvacpfAllrs, 31, sqlStaccpdb06);
					appVars.getDBObject(sqlgvacpfAllrs, 32, sqlStaccpdb07);
					appVars.getDBObject(sqlgvacpfAllrs, 33, sqlStaccpdb08);
					appVars.getDBObject(sqlgvacpfAllrs, 34, sqlStaccpdb09);
					appVars.getDBObject(sqlgvacpfAllrs, 35, sqlStaccpdb10);
					appVars.getDBObject(sqlgvacpfAllrs, 36, sqlStaccpdb11);
					appVars.getDBObject(sqlgvacpfAllrs, 37, sqlStaccpdb12);
					appVars.getDBObject(sqlgvacpfAllrs, 38, sqlStaccadb01);
					appVars.getDBObject(sqlgvacpfAllrs, 39, sqlStaccadb02);
					appVars.getDBObject(sqlgvacpfAllrs, 40, sqlStaccadb03);
					appVars.getDBObject(sqlgvacpfAllrs, 41, sqlStaccadb04);
					appVars.getDBObject(sqlgvacpfAllrs, 42, sqlStaccadb05);
					appVars.getDBObject(sqlgvacpfAllrs, 43, sqlStaccadb06);
					appVars.getDBObject(sqlgvacpfAllrs, 44, sqlStaccadb07);
					appVars.getDBObject(sqlgvacpfAllrs, 45, sqlStaccadb08);
					appVars.getDBObject(sqlgvacpfAllrs, 46, sqlStaccadb09);
					appVars.getDBObject(sqlgvacpfAllrs, 47, sqlStaccadb10);
					appVars.getDBObject(sqlgvacpfAllrs, 48, sqlStaccadb11);
					appVars.getDBObject(sqlgvacpfAllrs, 49, sqlStaccadb12);
					appVars.getDBObject(sqlgvacpfAllrs, 50, sqlStaccclr01);
					appVars.getDBObject(sqlgvacpfAllrs, 51, sqlStaccclr02);
					appVars.getDBObject(sqlgvacpfAllrs, 52, sqlStaccclr03);
					appVars.getDBObject(sqlgvacpfAllrs, 53, sqlStaccclr04);
					appVars.getDBObject(sqlgvacpfAllrs, 54, sqlStaccclr05);
					appVars.getDBObject(sqlgvacpfAllrs, 55, sqlStaccclr06);
					appVars.getDBObject(sqlgvacpfAllrs, 56, sqlStaccclr07);
					appVars.getDBObject(sqlgvacpfAllrs, 57, sqlStaccclr08);
					appVars.getDBObject(sqlgvacpfAllrs, 58, sqlStaccclr09);
					appVars.getDBObject(sqlgvacpfAllrs, 59, sqlStaccclr10);
					appVars.getDBObject(sqlgvacpfAllrs, 60, sqlStaccclr11);
					appVars.getDBObject(sqlgvacpfAllrs, 61, sqlStaccclr12);
					appVars.getDBObject(sqlgvacpfAllrs, 62, sqlStaccadj01);
					appVars.getDBObject(sqlgvacpfAllrs, 63, sqlStaccadj02);
					appVars.getDBObject(sqlgvacpfAllrs, 64, sqlStaccadj03);
					appVars.getDBObject(sqlgvacpfAllrs, 65, sqlStaccadj04);
					appVars.getDBObject(sqlgvacpfAllrs, 66, sqlStaccadj05);
					appVars.getDBObject(sqlgvacpfAllrs, 67, sqlStaccadj06);
					appVars.getDBObject(sqlgvacpfAllrs, 68, sqlStaccadj07);
					appVars.getDBObject(sqlgvacpfAllrs, 69, sqlStaccadj08);
					appVars.getDBObject(sqlgvacpfAllrs, 70, sqlStaccadj09);
					appVars.getDBObject(sqlgvacpfAllrs, 71, sqlStaccadj10);
					appVars.getDBObject(sqlgvacpfAllrs, 72, sqlStaccadj11);
					appVars.getDBObject(sqlgvacpfAllrs, 73, sqlStaccadj12);
					appVars.getDBObject(sqlgvacpfAllrs, 74, sqlStaccint01);
					appVars.getDBObject(sqlgvacpfAllrs, 75, sqlStaccint02);
					appVars.getDBObject(sqlgvacpfAllrs, 76, sqlStaccint03);
					appVars.getDBObject(sqlgvacpfAllrs, 77, sqlStaccint04);
					appVars.getDBObject(sqlgvacpfAllrs, 78, sqlStaccint05);
					appVars.getDBObject(sqlgvacpfAllrs, 79, sqlStaccint06);
					appVars.getDBObject(sqlgvacpfAllrs, 80, sqlStaccint07);
					appVars.getDBObject(sqlgvacpfAllrs, 81, sqlStaccint08);
					appVars.getDBObject(sqlgvacpfAllrs, 82, sqlStaccint09);
					appVars.getDBObject(sqlgvacpfAllrs, 83, sqlStaccint10);
					appVars.getDBObject(sqlgvacpfAllrs, 84, sqlStaccint11);
					appVars.getDBObject(sqlgvacpfAllrs, 85, sqlStaccint12);
					appVars.getDBObject(sqlgvacpfAllrs, 86, sqlStaccob01);
					appVars.getDBObject(sqlgvacpfAllrs, 87, sqlStaccob02);
					appVars.getDBObject(sqlgvacpfAllrs, 88, sqlStaccob03);
					appVars.getDBObject(sqlgvacpfAllrs, 89, sqlStaccob04);
					appVars.getDBObject(sqlgvacpfAllrs, 90, sqlStaccob05);
					appVars.getDBObject(sqlgvacpfAllrs, 91, sqlStaccob06);
					appVars.getDBObject(sqlgvacpfAllrs, 92, sqlStaccob07);
					appVars.getDBObject(sqlgvacpfAllrs, 93, sqlStaccob08);
					appVars.getDBObject(sqlgvacpfAllrs, 94, sqlStaccob09);
					appVars.getDBObject(sqlgvacpfAllrs, 95, sqlStaccob10);
					appVars.getDBObject(sqlgvacpfAllrs, 96, sqlStaccob11);
					appVars.getDBObject(sqlgvacpfAllrs, 97, sqlStaccob12);
					appVars.getDBObject(sqlgvacpfAllrs, 98, sqlBfwdpdb);
					appVars.getDBObject(sqlgvacpfAllrs, 99, sqlBfwdadb);
					appVars.getDBObject(sqlgvacpfAllrs, 100, sqlBfwdclr);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
		}
		else {
			sqlerrorflag = false;
			try {
				if (sqlgvacpfrs.next()) {
					appVars.getDBObject(sqlgvacpfrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgvacpfrs, 2, sqlAcctyr);
					appVars.getDBObject(sqlgvacpfrs, 3, sqlStfund);
					appVars.getDBObject(sqlgvacpfrs, 4, sqlStsect);
					appVars.getDBObject(sqlgvacpfrs, 5, sqlStssect);
					appVars.getDBObject(sqlgvacpfrs, 6, sqlRegister);
					appVars.getDBObject(sqlgvacpfrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgvacpfrs, 8, sqlCnttype);
					appVars.getDBObject(sqlgvacpfrs, 9, sqlCrtable);
					appVars.getDBObject(sqlgvacpfrs, 10, sqlAcctccy);
					appVars.getDBObject(sqlgvacpfrs, 11, sqlParind);
					appVars.getDBObject(sqlgvacpfrs, 12, sqlSrcebus);
					appVars.getDBObject(sqlgvacpfrs, 13, sqlStatcode);
					appVars.getDBObject(sqlgvacpfrs, 14, sqlStaccmtb01);
					appVars.getDBObject(sqlgvacpfrs, 15, sqlStaccmtb02);
					appVars.getDBObject(sqlgvacpfrs, 16, sqlStaccmtb03);
					appVars.getDBObject(sqlgvacpfrs, 17, sqlStaccmtb04);
					appVars.getDBObject(sqlgvacpfrs, 18, sqlStaccmtb05);
					appVars.getDBObject(sqlgvacpfrs, 19, sqlStaccmtb06);
					appVars.getDBObject(sqlgvacpfrs, 20, sqlStaccmtb07);
					appVars.getDBObject(sqlgvacpfrs, 21, sqlStaccmtb08);
					appVars.getDBObject(sqlgvacpfrs, 22, sqlStaccmtb09);
					appVars.getDBObject(sqlgvacpfrs, 23, sqlStaccmtb10);
					appVars.getDBObject(sqlgvacpfrs, 24, sqlStaccmtb11);
					appVars.getDBObject(sqlgvacpfrs, 25, sqlStaccmtb12);
					appVars.getDBObject(sqlgvacpfrs, 26, sqlStaccpdb01);
					appVars.getDBObject(sqlgvacpfrs, 27, sqlStaccpdb02);
					appVars.getDBObject(sqlgvacpfrs, 28, sqlStaccpdb03);
					appVars.getDBObject(sqlgvacpfrs, 29, sqlStaccpdb04);
					appVars.getDBObject(sqlgvacpfrs, 30, sqlStaccpdb05);
					appVars.getDBObject(sqlgvacpfrs, 31, sqlStaccpdb06);
					appVars.getDBObject(sqlgvacpfrs, 32, sqlStaccpdb07);
					appVars.getDBObject(sqlgvacpfrs, 33, sqlStaccpdb08);
					appVars.getDBObject(sqlgvacpfrs, 34, sqlStaccpdb09);
					appVars.getDBObject(sqlgvacpfrs, 35, sqlStaccpdb10);
					appVars.getDBObject(sqlgvacpfrs, 36, sqlStaccpdb11);
					appVars.getDBObject(sqlgvacpfrs, 37, sqlStaccpdb12);
					appVars.getDBObject(sqlgvacpfrs, 38, sqlStaccadb01);
					appVars.getDBObject(sqlgvacpfrs, 39, sqlStaccadb02);
					appVars.getDBObject(sqlgvacpfrs, 40, sqlStaccadb03);
					appVars.getDBObject(sqlgvacpfrs, 41, sqlStaccadb04);
					appVars.getDBObject(sqlgvacpfrs, 42, sqlStaccadb05);
					appVars.getDBObject(sqlgvacpfrs, 43, sqlStaccadb06);
					appVars.getDBObject(sqlgvacpfrs, 44, sqlStaccadb07);
					appVars.getDBObject(sqlgvacpfrs, 45, sqlStaccadb08);
					appVars.getDBObject(sqlgvacpfrs, 46, sqlStaccadb09);
					appVars.getDBObject(sqlgvacpfrs, 47, sqlStaccadb10);
					appVars.getDBObject(sqlgvacpfrs, 48, sqlStaccadb11);
					appVars.getDBObject(sqlgvacpfrs, 49, sqlStaccadb12);
					appVars.getDBObject(sqlgvacpfrs, 50, sqlStaccclr01);
					appVars.getDBObject(sqlgvacpfrs, 51, sqlStaccclr02);
					appVars.getDBObject(sqlgvacpfrs, 52, sqlStaccclr03);
					appVars.getDBObject(sqlgvacpfrs, 53, sqlStaccclr04);
					appVars.getDBObject(sqlgvacpfrs, 54, sqlStaccclr05);
					appVars.getDBObject(sqlgvacpfrs, 55, sqlStaccclr06);
					appVars.getDBObject(sqlgvacpfrs, 56, sqlStaccclr07);
					appVars.getDBObject(sqlgvacpfrs, 57, sqlStaccclr08);
					appVars.getDBObject(sqlgvacpfrs, 58, sqlStaccclr09);
					appVars.getDBObject(sqlgvacpfrs, 59, sqlStaccclr10);
					appVars.getDBObject(sqlgvacpfrs, 60, sqlStaccclr11);
					appVars.getDBObject(sqlgvacpfrs, 61, sqlStaccclr12);
					appVars.getDBObject(sqlgvacpfrs, 62, sqlStaccadj01);
					appVars.getDBObject(sqlgvacpfrs, 63, sqlStaccadj02);
					appVars.getDBObject(sqlgvacpfrs, 64, sqlStaccadj03);
					appVars.getDBObject(sqlgvacpfrs, 65, sqlStaccadj04);
					appVars.getDBObject(sqlgvacpfrs, 66, sqlStaccadj05);
					appVars.getDBObject(sqlgvacpfrs, 67, sqlStaccadj06);
					appVars.getDBObject(sqlgvacpfrs, 68, sqlStaccadj07);
					appVars.getDBObject(sqlgvacpfrs, 69, sqlStaccadj08);
					appVars.getDBObject(sqlgvacpfrs, 70, sqlStaccadj09);
					appVars.getDBObject(sqlgvacpfrs, 71, sqlStaccadj10);
					appVars.getDBObject(sqlgvacpfrs, 72, sqlStaccadj11);
					appVars.getDBObject(sqlgvacpfrs, 73, sqlStaccadj12);
					appVars.getDBObject(sqlgvacpfrs, 74, sqlStaccint01);
					appVars.getDBObject(sqlgvacpfrs, 75, sqlStaccint02);
					appVars.getDBObject(sqlgvacpfrs, 76, sqlStaccint03);
					appVars.getDBObject(sqlgvacpfrs, 77, sqlStaccint04);
					appVars.getDBObject(sqlgvacpfrs, 78, sqlStaccint05);
					appVars.getDBObject(sqlgvacpfrs, 79, sqlStaccint06);
					appVars.getDBObject(sqlgvacpfrs, 80, sqlStaccint07);
					appVars.getDBObject(sqlgvacpfrs, 81, sqlStaccint08);
					appVars.getDBObject(sqlgvacpfrs, 82, sqlStaccint09);
					appVars.getDBObject(sqlgvacpfrs, 83, sqlStaccint10);
					appVars.getDBObject(sqlgvacpfrs, 84, sqlStaccint11);
					appVars.getDBObject(sqlgvacpfrs, 85, sqlStaccint12);
					appVars.getDBObject(sqlgvacpfrs, 86, sqlStaccob01);
					appVars.getDBObject(sqlgvacpfrs, 87, sqlStaccob02);
					appVars.getDBObject(sqlgvacpfrs, 88, sqlStaccob03);
					appVars.getDBObject(sqlgvacpfrs, 89, sqlStaccob04);
					appVars.getDBObject(sqlgvacpfrs, 90, sqlStaccob05);
					appVars.getDBObject(sqlgvacpfrs, 91, sqlStaccob06);
					appVars.getDBObject(sqlgvacpfrs, 92, sqlStaccob07);
					appVars.getDBObject(sqlgvacpfrs, 93, sqlStaccob08);
					appVars.getDBObject(sqlgvacpfrs, 94, sqlStaccob09);
					appVars.getDBObject(sqlgvacpfrs, 95, sqlStaccob10);
					appVars.getDBObject(sqlgvacpfrs, 96, sqlStaccob11);
					appVars.getDBObject(sqlgvacpfrs, 97, sqlStaccob12);
					appVars.getDBObject(sqlgvacpfrs, 98, sqlBfwdpdb);
					appVars.getDBObject(sqlgvacpfrs, 99, sqlBfwdadb);
					appVars.getDBObject(sqlgvacpfrs, 100, sqlBfwdclr);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
		}
		wsaaSqlKeep.set(sqlGvacrec);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		if (isNE(wsaaFrecord,"Y")) {
			h500WriteEnd();
			if (isNE(sqlAcctccy,wsaaAcctccyS)) {
				rj52502.printRj52502h01(rj52502H01, indicArea);
			}
			tamt.set(wsaaTotal);
			wsaaTotamt.add(wsaaTotal);
			indOff.setTrue(16);
			rj52502.printRj52502d01(rj52502D01, indicArea);
			indOn.setTrue(16);
			tamt.set(wsaaTotamt);
			rj52502.printRj52502d01(rj52502D01, indicArea);
			contotrec.totno.set(ct02);
			contotrec.totval.set(wsaaTotamt);
			callContot001();
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		writeDetail3080();
	}

protected void writeDetail3080()
	{
		if (isEQ(wsaaFrecord,"Y")) {
			wsaaFrecord = "N";
		}
		else {
			if (isNE(wsaaStsect,sqlStsect)
			|| isNE(wsaaRegister,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaBranch,sqlCntbranch)
			|| isNE(wsaaCompany,sqlChdrcoy)) {
				h600SetHeading();
				h100NewPage();
				h200Details();
			}
			if (isNE(wsaaRegister,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaBranch,sqlCntbranch)
			|| isNE(wsaaCompany,sqlChdrcoy)) {
				h800WriteSummary();
			}
		}
		if (isEQ(sqlStatcode,"MA")) {
			wsaaMatClm.add(wsaaSStaccmtb[pj517par.acctmnth.toInt()]);
			wsaaMatRiclm.add(wsaaSStaccclr[pj517par.acctmnth.toInt()]);
		}
		else {
			if (isEQ(sqlStatcode,"RD")
			|| isEQ(sqlStatcode,"DH")) {
				for (iz.set(1); !(isGT(iz,pj517par.acctmnth)); iz.add(1)){
					wsaaAsAtPndDthclm.add(wsaaSStaccpdb[iz.toInt()]);
					wsaaAsAtAprDthclm.add(wsaaSStaccadb[iz.toInt()]);
					wsaaAsAtRiclm.add(wsaaSStaccclr[iz.toInt()]);
				}
				wsaaAsAtPndDthclm.add(wsaaSBfwdpdb);
				wsaaAsAtAprDthclm.add(wsaaSBfwdadb);
				wsaaAsAtRiclm.add(wsaaSBfwdclr);
				wsaaAprDthclm.add(wsaaSStaccadb[pj517par.acctmnth.toInt()]);
				wsaaAprAdjust.add(wsaaSStaccadj[pj517par.acctmnth.toInt()]);
				wsaaAprInterest.add(wsaaSStaccint[pj517par.acctmnth.toInt()]);
			}
			else {
				if (isNE(wsaaSStaccob[pj517par.acctmnth.toInt()],ZERO)) {
					h700ReadT6625();
					if (isEQ(itemIO.getStatuz(),varcom.oK)) {
						wsaaAnnuity.add(wsaaSStaccob[pj517par.acctmnth.toInt()]);
						wsaaAnnuityRi.add(wsaaSStaccclr[pj517par.acctmnth.toInt()]);
					}
					else {
						wsaaRegpay.add(wsaaSStaccob[pj517par.acctmnth.toInt()]);
						wsaaRegpayRi.add(wsaaSStaccclr[pj517par.acctmnth.toInt()]);
					}
				}
			}
		}
		wsaaStsect.set(sqlStsect);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaCompany.set(sqlChdrcoy);
		wsaaBranch.set(sqlCntbranch);
		wsaaRegister.set(sqlRegister);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (selectAll.isTrue()) {
			appVars.freeDBConnectionIgnoreErr(sqlgvacpfAllconn, sqlgvacpfAllps, sqlgvacpfAllrs);
		}
		else {
			appVars.freeDBConnectionIgnoreErr(sqlgvacpfconn, sqlgvacpfps, sqlgvacpfrs);
		}
		rj52501.close();
		rj52502.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void h100NewPage()
	{
		/*H110-START*/
		rj52501.printRj52501h01(rj52501H01, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*H190-EXIT*/
	}

protected void h200Details()
	{
		h210Start();
	}

protected void h210Start()
	{
		indOn.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(13);
		indOff.setTrue(14);
		indOff.setTrue(15);
		compute(tamt01, 2).set(add(add(wsaaAprDthclm,wsaaAprAdjust),wsaaAprInterest));
		tamt02.set(wsaaMatClm);
		tamt03.set(wsaaAnnuity);
		tamt04.set(wsaaRegpay);
		rj52501.printRj52501d01(rj52501D01, indicArea);
		indOn.setTrue(12);
		indOff.setTrue(11);
		indOff.setTrue(13);
		indOff.setTrue(14);
		indOff.setTrue(15);
		if (isEQ(wsaaAprDthclm,ZERO)) {
			wsaaAsAtRiclm.set(ZERO);
		}
		tamt01.set(wsaaAsAtRiclm);
		tamt02.set(wsaaMatRiclm);
		tamt03.set(wsaaAnnuityRi);
		tamt04.set(wsaaRegpayRi);
		rj52501.printRj52501d01(rj52501D01, indicArea);
		indOn.setTrue(13);
		indOff.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(14);
		indOff.setTrue(15);
		tamt01.set(ZERO);
		tamt02.set(ZERO);
		tamt03.set(ZERO);
		tamt04.set(ZERO);
		rj52501.printRj52501d01(rj52501D01, indicArea);
		indOn.setTrue(14);
		indOff.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(13);
		indOff.setTrue(15);
		compute(wsaaTamt, 2).set(sub(add(add(wsaaAprDthclm,wsaaAprAdjust),wsaaAprInterest),wsaaAsAtRiclm));
		tamt01.set(wsaaTamt);
		wsaaTotal.add(wsaaTamt);
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaTamt);
		callContot001();
		compute(wsaaTamt, 2).set(add(wsaaMatClm,wsaaMatRiclm));
		tamt02.set(wsaaTamt);
		wsaaTotal.add(wsaaTamt);
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaTamt);
		callContot001();
		compute(wsaaTamt, 2).set(add(wsaaAnnuity,wsaaAnnuityRi));
		tamt03.set(wsaaTamt);
		wsaaTotal.add(wsaaTamt);
		contotrec.totno.set(ct05);
		contotrec.totval.set(wsaaTamt);
		callContot001();
		compute(wsaaTamt, 2).set(add(wsaaRegpay,wsaaRegpayRi));
		tamt04.set(wsaaTamt);
		wsaaTotal.add(wsaaTamt);
		contotrec.totno.set(ct06);
		contotrec.totval.set(wsaaTamt);
		callContot001();
		rj52501.printRj52501d01(rj52501D01, indicArea);
		rj52501.printRj52501d02(rj52501D02, indicArea);
		indOn.setTrue(15);
		indOff.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(14);
		indOff.setTrue(15);
		compute(wsaaTamt, 2).set(sub(wsaaAsAtPndDthclm,wsaaAsAtAprDthclm));
		tamt01.set(wsaaTamt);
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaTamt);
		callContot001();
		tamt02.set(ZERO);
		tamt03.set(ZERO);
		tamt04.set(ZERO);
		rj52501.printRj52501d01(rj52501D01, indicArea);
		wsaaOverflow.set("Y");
		wsaaAsAtPndDthclm.set(ZERO);
		wsaaAsAtAprDthclm.set(ZERO);
		wsaaAsAtRiclm.set(ZERO);
		wsaaAprDthclm.set(ZERO);
		wsaaAprAdjust.set(ZERO);
		wsaaAprInterest.set(ZERO);
		wsaaMatClm.set(ZERO);
		wsaaMatRiclm.set(ZERO);
		wsaaAnnuity.set(ZERO);
		wsaaAnnuityRi.set(ZERO);
		wsaaRegpay.set(ZERO);
		wsaaRegpayRi.set(ZERO);
	}

protected void h500WriteEnd()
	{
		/*H510-START*/
		h600SetHeading();
		h100NewPage();
		h200Details();
		/*H590-EXIT*/
	}

protected void h600SetHeading()
	{
		h610Start();
	}

protected void h610Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(wsaaCompany);
		rh01Companynm.set(descIO.getLongdesc());
		rh01Company1.set(wsaaCompany);
		rh01Companynm1.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsaaCompany);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(wsaaAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		acctccy.set(wsaaAcctccy);
		currdesc.set(descIO.getLongdesc());
		acctccy1.set(wsaaAcctccy);
		currdesc1.set(descIO.getLongdesc());
		//ILIFE-6360 starts
		if(isNE(wsaaRegister,SPACES)){
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsaaCompany);
		descIO.setDesctabl(t3589);
		descIO.setDescitem(wsaaRegister);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		register.set(wsaaRegister);
		descrip.set(descIO.getLongdesc());
		ditdsc.set(descIO.getLongdesc());
		}
		else{
			register.set(SPACES);
			descrip.set(SPACES);
			ditdsc.set(SPACES);
		}//ILIFE-6360 ends
		
		//ILIFE-6360 starts
		if(isNE(wsaaStsect,SPACES)){
		descIO.setDescpfx("IT");
		descIO.setDesccoy(wsaaCompany);
		descIO.setDesctabl(t5685);
		descIO.setDescitem(wsaaStsect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		stsect.set(wsaaStsect);
		itmdesc.set(descIO.getLongdesc());
	}
		
		else{
			stsect.set(SPACES);
			itmdesc.set(SPACES);
			
		}//ILIFE-6360 ends
	}

protected void h700ReadT6625()
	{
		/*H710-START*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6625);
		itemIO.setItemitem(sqlCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		/*H790-EXIT*/
	}

protected void h800WriteSummary()
	{
		h810Start();
	}

protected void h810Start()
	{
		if (isNE(wsaaAcctccyS,wsaaAcctccy)
		|| isNE(wsaaBranchS,wsaaBranch)
		|| isNE(wsaaCompanyS,wsaaCompany)) {
			rj52502.printRj52502h01(rj52502H01, indicArea);
		}
		tamt.set(wsaaTotal);
		wsaaTotamt.add(wsaaTotal);
		indOff.setTrue(16);
		rj52502.printRj52502d01(rj52502D01, indicArea);
		if (isNE(wsaaAcctccy,sqlAcctccy)
		|| isNE(wsaaBranch,sqlCntbranch)
		|| isNE(wsaaCompany,sqlChdrcoy)) {
			indOn.setTrue(16);
			tamt.set(wsaaTotamt);
			rj52502.printRj52502d01(rj52502D01, indicArea);
			contotrec.totno.set(ct02);
			contotrec.totval.set(wsaaTotamt);
			callContot001();
			wsaaTotamt.set(ZERO);
		}
		wsaaTotal.set(ZERO);
		wsaaAcctccyS.set(wsaaAcctccy);
		wsaaCompanyS.set(wsaaCompany);
		wsaaBranchS.set(wsaaBranch);
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
