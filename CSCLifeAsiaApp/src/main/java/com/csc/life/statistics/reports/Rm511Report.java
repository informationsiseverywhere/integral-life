package com.csc.life.statistics.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RM511.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rm511Report extends SMARTReportLayout { 

	private FixedLengthStringData agntname01 = new FixedLengthStringData(30);
	private FixedLengthStringData agntname02 = new FixedLengthStringData(30);
	private FixedLengthStringData agntnum01 = new FixedLengthStringData(8);
	private FixedLengthStringData agntnum02 = new FixedLengthStringData(8);
	private FixedLengthStringData agtype01 = new FixedLengthStringData(2);
	private FixedLengthStringData agtype02 = new FixedLengthStringData(2);
	private ZonedDecimalData aplamt01 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData aplamt02 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData aplamt03 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData aplamt04 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData aplamt05 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData aplamt06 = new ZonedDecimalData(17, 2);
	private ZonedDecimalData aplamt07 = new ZonedDecimalData(17, 2);
	private FixedLengthStringData aracde = new FixedLengthStringData(3);
	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	//ILIFE-1120
	//private FixedLengthStringData cldb = new FixedLengthStringData(190);
	private FixedLengthStringData cldb = new FixedLengthStringData(300);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currency = new FixedLengthStringData(3);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private FixedLengthStringData effdate = new FixedLengthStringData(10);
	private ZonedDecimalData mnth = new ZonedDecimalData(2, 0);
	private ZonedDecimalData numpols01 = new ZonedDecimalData(4, 0);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData year = new ZonedDecimalData(4, 0);
	private ZonedDecimalData zbamt03 = new ZonedDecimalData(11, 2);
	
	//ILIFE-1120
	private FixedLengthStringData leadtext = new FixedLengthStringData(30);
	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rm511Report() {
		super();
	}


	/**
	 * Print the XML for Rm511d01
	 */
	public void printRm511d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(16).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		agntnum02.setFieldName("agntnum02");
		agntnum02.setInternal(subString(recordData, 1, 8));
		agntname02.setFieldName("agntname02");
		agntname02.setInternal(subString(recordData, 9, 30));
		agtype02.setFieldName("agtype02");
		agtype02.setInternal(subString(recordData, 39, 2));
		effdate.setFieldName("effdate");
		effdate.setInternal(subString(recordData, 41, 10));
		currency.setFieldName("currency");
		currency.setInternal(subString(recordData, 51, 3));
		printLayout("Rm511d01",			// Record name
			new BaseData[]{			// Fields:
				agntnum02,
				agntname02,
				agtype02,
				effdate,
				currency
			}
			, new Object[] {			// indicators
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind12", indicArea.charAt(12)},
				new Object[]{"ind13", indicArea.charAt(13)},
				new Object[]{"ind14", indicArea.charAt(14)},
				new Object[]{"ind15", indicArea.charAt(15)},
				new Object[]{"ind16", indicArea.charAt(16)}
			}
		);

	}

	/**
	 * Print the XML for Rm511d02
	 */
	public void printRm511d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(18).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);
		//Start ILIFE-1120
		leadtext.setFieldName("leadtext");
		leadtext.setInternal(subString(recordData, 135, 16));
		//End ILIFE-1120
		
		numpols01.setFieldName("numpols01");
		numpols01.setInternal(subString(recordData, 1, 4));
		aplamt07.setFieldName("aplamt07");
		aplamt07.setInternal(subString(recordData, 5, 17));
		aplamt01.setFieldName("aplamt01");
		aplamt01.setInternal(subString(recordData, 22, 17));
		aplamt02.setFieldName("aplamt02");
		aplamt02.setInternal(subString(recordData, 39, 17));
		aplamt03.setFieldName("aplamt03");
		aplamt03.setInternal(subString(recordData, 56, 17));
		aplamt04.setFieldName("aplamt04");
		aplamt04.setInternal(subString(recordData, 73, 17));
		aplamt05.setFieldName("aplamt05");
		aplamt05.setInternal(subString(recordData, 90, 17));
		aplamt06.setFieldName("aplamt06");
		aplamt06.setInternal(subString(recordData, 107, 17));
		zbamt03.setFieldName("zbamt03");
		zbamt03.setInternal(subString(recordData, 124, 11));
		printLayout("Rm511d02",			// Record name
			new BaseData[]{			// Fields:
				//ILIFE-1120
				leadtext,
				numpols01,
				aplamt07,
				aplamt01,
				aplamt02,
				aplamt03,
				aplamt04,
				aplamt05,
				aplamt06,
				zbamt03
			}
			, new Object[] {			// indicators
				new Object[]{"ind01", indicArea.charAt(1)},
				new Object[]{"ind02", indicArea.charAt(2)},
				new Object[]{"ind03", indicArea.charAt(3)},
				new Object[]{"ind04", indicArea.charAt(4)},
				new Object[]{"ind05", indicArea.charAt(5)},
				new Object[]{"ind06", indicArea.charAt(6)},
				new Object[]{"ind17", indicArea.charAt(17)},
				new Object[]{"ind18", indicArea.charAt(18)}
			}
		);

	}

	/**
	 * Print the XML for Rm511end
	 */
	public void printRm511end(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rm511end",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rm511h01
	 */
	public void printRm511h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 1, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 31, 10));
		mnth.setFieldName("mnth");
		mnth.setInternal(subString(recordData, 41, 2));
		year.setFieldName("year");
		year.setInternal(subString(recordData, 43, 4));
		time.setFieldName("time");
		time.set(getTime());
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		aracde.setFieldName("aracde");
		aracde.setInternal(subString(recordData, 47, 3));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 50, 30));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 80, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 82, 30));
		agntnum01.setFieldName("agntnum01");
		agntnum01.setInternal(subString(recordData, 112, 8));
		agntname01.setFieldName("agntname01");
		agntname01.setInternal(subString(recordData, 120, 30));
		agtype01.setFieldName("agtype01");
		agtype01.setInternal(subString(recordData, 150, 2));
		printLayout("Rm511h01",			// Record name
			new BaseData[]{			// Fields:
				companynm,
				sdate,
				mnth,
				year,
				time,
				pagnbr,
				aracde,
				descrip,
				branch,
				branchnm,
				agntnum01,
				agntname01,
				agtype01
			}
		);

		currentPrintLine.set(16);
	}

	/**
	 * Print the XML for Rm511line
	 */
	public void printRm511line(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		cldb.setFieldName("cldb");
		//ILIFE-1120
		cldb.setInternal(subString(recordData, 1, 300));
		printLayout("Rm511line",			// Record name
			new BaseData[]{			// Fields:
				cldb
			}
		);

	}


}
