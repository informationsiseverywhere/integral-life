package com.csc.life.statistics.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6709screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 20, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6709ScreenVars sv = (S6709ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6709screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6709ScreenVars screenVars = (S6709ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.statcat.setClassString("");
		screenVars.acctyr.setClassString("");
		screenVars.statFund.setClassString("");
		screenVars.statSect.setClassString("");
		screenVars.stsubsect.setClassString("");
		screenVars.register.setClassString("");
		screenVars.cntbranch.setClassString("");
		screenVars.bandage.setClassString("");
		screenVars.bandsa.setClassString("");
		screenVars.bandprm.setClassString("");
		screenVars.bandtrm.setClassString("");
		screenVars.pstatcd.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.acmn.setClassString("");
		screenVars.acyr.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.commyr.setClassString("");
		screenVars.mthldesc.setClassString("");
		screenVars.stimth.setClassString("");
		screenVars.stsmth.setClassString("");
		screenVars.stpmth.setClassString("");
		screenVars.stcmth.setClassString("");
	}

/**
 * Clear all the variables in S6709screen
 */
	public static void clear(VarModel pv) {
		S6709ScreenVars screenVars = (S6709ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.statcat.clear();
		screenVars.acctyr.clear();
		screenVars.statFund.clear();
		screenVars.statSect.clear();
		screenVars.stsubsect.clear();
		screenVars.register.clear();
		screenVars.cntbranch.clear();
		screenVars.bandage.clear();
		screenVars.bandsa.clear();
		screenVars.bandprm.clear();
		screenVars.bandtrm.clear();
		screenVars.pstatcd.clear();
		screenVars.cntcurr.clear();
		screenVars.acmn.clear();
		screenVars.acyr.clear();
		screenVars.descrip.clear();
		screenVars.commyr.clear();
		screenVars.mthldesc.clear();
		screenVars.stimth.clear();
		screenVars.stsmth.clear();
		screenVars.stpmth.clear();
		screenVars.stcmth.clear();
	}
}
