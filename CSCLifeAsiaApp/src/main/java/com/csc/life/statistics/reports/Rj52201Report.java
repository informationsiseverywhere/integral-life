package com.csc.life.statistics.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RJ52201.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:50
 * @author Quipoz
 */
public class Rj52201Report extends SMARTReportLayout { 

	private FixedLengthStringData acctccy = new FixedLengthStringData(3);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0);
	private FixedLengthStringData adsc = new FixedLengthStringData(50);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData crtable = new FixedLengthStringData(4);
	private FixedLengthStringData crtabled = new FixedLengthStringData(30);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData srcebus = new FixedLengthStringData(2);
	private ZonedDecimalData staccfyp = new ZonedDecimalData(18, 2);
	private ZonedDecimalData staccrnp = new ZonedDecimalData(18, 2);
	private ZonedDecimalData staccspd = new ZonedDecimalData(18, 2);
	private FixedLengthStringData stsect = new FixedLengthStringData(2);
	private RPGTimeData time = new RPGTimeData();
	private ZonedDecimalData total01 = new ZonedDecimalData(18, 2);
	private ZonedDecimalData total02 = new ZonedDecimalData(18, 2);
	private ZonedDecimalData total03 = new ZonedDecimalData(18, 2);
	private ZonedDecimalData totalamt01 = new ZonedDecimalData(18, 2);
	private ZonedDecimalData totalamt02 = new ZonedDecimalData(18, 2);
	private ZonedDecimalData totalamt03 = new ZonedDecimalData(18, 2);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rj52201Report() {
		super();
	}


	/**
	 * Print the XML for Rj52201d01
	 */
	public void printRj52201d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		crtable.setFieldName("crtable");
		crtable.setInternal(subString(recordData, 1, 4));
		crtabled.setFieldName("crtabled");
		crtabled.setInternal(subString(recordData, 5, 30));
		staccfyp.setFieldName("staccfyp");
		staccfyp.setInternal(subString(recordData, 35, 18));
		staccrnp.setFieldName("staccrnp");
		staccrnp.setInternal(subString(recordData, 53, 18));
		staccspd.setFieldName("staccspd");
		staccspd.setInternal(subString(recordData, 71, 18));
		printLayout("Rj52201d01",			// Record name
			new BaseData[]{			// Fields:
				crtable,
				crtabled,
				staccfyp,
				staccrnp,
				staccspd
			}
		);

	}

	/**
	 * Print the XML for Rj52201d02
	 */
	public void printRj52201d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rj52201d02",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rj52201h01
	 */
	public void printRj52201h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		acctmonth.setFieldName("acctmonth");
		acctmonth.setInternal(subString(recordData, 1, 2));
		acctyear.setFieldName("acctyear");
		acctyear.setInternal(subString(recordData, 3, 4));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 7, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 8, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 38, 10));
		acctccy.setFieldName("acctccy");
		acctccy.setInternal(subString(recordData, 48, 3));
		currdesc.setFieldName("currdesc");
		currdesc.setInternal(subString(recordData, 51, 30));
		time.setFieldName("time");
		time.set(getTime());
		srcebus.setFieldName("srcebus");
		srcebus.setInternal(subString(recordData, 81, 2));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 83, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		stsect.setFieldName("stsect");
		stsect.setInternal(subString(recordData, 113, 2));
		itmdesc.setFieldName("itmdesc");
		itmdesc.setInternal(subString(recordData, 115, 30));
		printLayout("Rj52201h01",			// Record name
			new BaseData[]{			// Fields:
				acctmonth,
				acctyear,
				company,
				companynm,
				sdate,
				acctccy,
				currdesc,
				time,
				srcebus,
				descrip,
				pagnbr,
				stsect,
				itmdesc
			}
		);

		currentPrintLine.set(10);
	}

	/**
	 * Print the XML for Rj52201h02
	 */
	public void printRj52201h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(12).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		adsc.setFieldName("adsc");
		adsc.setInternal(subString(recordData, 1, 50));
		printLayout("Rj52201h02",			// Record name
			new BaseData[]{			// Fields:
				adsc
			}
			, new Object[] {			// indicators
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind12", indicArea.charAt(12)}
			}
		);

		currentPrintLine.add(6);
	}

	/**
	 * Print the XML for Rj52201h03
	 */
	public void printRj52201h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		printLayout("Rj52201h03",			// Record name
			new BaseData[]{			// Fields:
			}
		);

	}

	/**
	 * Print the XML for Rj52201t01
	 */
	public void printRj52201t01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		total01.setFieldName("total01");
		total01.setInternal(subString(recordData, 1, 18));
		total02.setFieldName("total02");
		total02.setInternal(subString(recordData, 19, 18));
		total03.setFieldName("total03");
		total03.setInternal(subString(recordData, 37, 18));
		printLayout("Rj52201t01",			// Record name
			new BaseData[]{			// Fields:
				total01,
				total02,
				total03
			}
		);

	}

	/**
	 * Print the XML for Rj52201t02
	 */
	public void printRj52201t02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(2);

		totalamt01.setFieldName("totalamt01");
		totalamt01.setInternal(subString(recordData, 1, 18));
		totalamt02.setFieldName("totalamt02");
		totalamt02.setInternal(subString(recordData, 19, 18));
		totalamt03.setFieldName("totalamt03");
		totalamt03.setInternal(subString(recordData, 37, 18));
		printLayout("Rj52201t02",			// Record name
			new BaseData[]{			// Fields:
				totalamt01,
				totalamt02,
				totalamt03
			}
		);

	}


}
