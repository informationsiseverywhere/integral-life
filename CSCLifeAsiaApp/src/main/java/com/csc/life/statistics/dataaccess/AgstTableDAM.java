package com.csc.life.statistics.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgstTableDAM.java
 * Date: Sun, 30 Aug 2009 03:29:02
 * Class transformed from AGST.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgstTableDAM extends AgstpfTableDAM {

	public AgstTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGST");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", AGNTNUM"
		             + ", STATCAT"
		             + ", ACCTYR"
		             + ", ARACDE"
		             + ", CNTBRANCH"
		             + ", BANDAGE"
		             + ", BANDSA"
		             + ", BANDPRM"
		             + ", BANDTRM"
		             + ", CNTTYPE"
		             + ", CRTABLE"
		             + ", OVRDCAT"
		             + ", PSTATCODE"
		             + ", CNTCURR";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "AGNTNUM, " +
		            "ARACDE, " +
		            "CNTBRANCH, " +
		            "BANDAGE, " +
		            "BANDSA, " +
		            "BANDPRM, " +
		            "BANDTRM, " +
		            "CNTTYPE, " +
		            "CRTABLE, " +
		            "OVRDCAT, " +
		            "STATCAT, " +
		            "PSTATCODE, " +
		            "CNTCURR, " +
		            "ACCTYR, " +
		            "BFWDC, " +
		            "STCMTH01, " +
		            "STCMTH02, " +
		            "STCMTH03, " +
		            "STCMTH04, " +
		            "STCMTH05, " +
		            "STCMTH06, " +
		            "STCMTH07, " +
		            "STCMTH08, " +
		            "STCMTH09, " +
		            "STCMTH10, " +
		            "STCMTH11, " +
		            "STCMTH12, " +
		            "CFWDC, " +
		            "BFWDV, " +
		            "STVMTH01, " +
		            "STVMTH02, " +
		            "STVMTH03, " +
		            "STVMTH04, " +
		            "STVMTH05, " +
		            "STVMTH06, " +
		            "STVMTH07, " +
		            "STVMTH08, " +
		            "STVMTH09, " +
		            "STVMTH10, " +
		            "STVMTH11, " +
		            "STVMTH12, " +
		            "CFWDV, " +
		            "BFWDP, " +
		            "STPMTH01, " +
		            "STPMTH02, " +
		            "STPMTH03, " +
		            "STPMTH04, " +
		            "STPMTH05, " +
		            "STPMTH06, " +
		            "STPMTH07, " +
		            "STPMTH08, " +
		            "STPMTH09, " +
		            "STPMTH10, " +
		            "STPMTH11, " +
		            "STPMTH12, " +
		            "CFWDP, " +
		            "BFWDS, " +
		            "STSMTH01, " +
		            "STSMTH02, " +
		            "STSMTH03, " +
		            "STSMTH04, " +
		            "STSMTH05, " +
		            "STSMTH06, " +
		            "STSMTH07, " +
		            "STSMTH08, " +
		            "STSMTH09, " +
		            "STSMTH10, " +
		            "STSMTH11, " +
		            "STSMTH12, " +
		            "CFWDS, " +
		            "BFWDM, " +
		            "STMMTH01, " +
		            "STMMTH02, " +
		            "STMMTH03, " +
		            "STMMTH04, " +
		            "STMMTH05, " +
		            "STMMTH06, " +
		            "STMMTH07, " +
		            "STMMTH08, " +
		            "STMMTH09, " +
		            "STMMTH10, " +
		            "STMMTH11, " +
		            "STMMTH12, " +
		            "CFWDM, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "AGNTNUM ASC, " +
		            "STATCAT ASC, " +
		            "ACCTYR ASC, " +
		            "ARACDE ASC, " +
		            "CNTBRANCH ASC, " +
		            "BANDAGE ASC, " +
		            "BANDSA ASC, " +
		            "BANDPRM ASC, " +
		            "BANDTRM ASC, " +
		            "CNTTYPE ASC, " +
		            "CRTABLE ASC, " +
		            "OVRDCAT ASC, " +
		            "PSTATCODE ASC, " +
		            "CNTCURR ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "AGNTNUM DESC, " +
		            "STATCAT DESC, " +
		            "ACCTYR DESC, " +
		            "ARACDE DESC, " +
		            "CNTBRANCH DESC, " +
		            "BANDAGE DESC, " +
		            "BANDSA DESC, " +
		            "BANDPRM DESC, " +
		            "BANDTRM DESC, " +
		            "CNTTYPE DESC, " +
		            "CRTABLE DESC, " +
		            "OVRDCAT DESC, " +
		            "PSTATCODE DESC, " +
		            "CNTCURR DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               agntnum,
                               aracde,
                               cntbranch,
                               bandage,
                               bandsa,
                               bandprm,
                               bandtrm,
                               cnttype,
                               crtable,
                               ovrdcat,
                               statcat,
                               pstatcode,
                               cntcurr,
                               acctyr,
                               bfwdc,
                               stcmth01,
                               stcmth02,
                               stcmth03,
                               stcmth04,
                               stcmth05,
                               stcmth06,
                               stcmth07,
                               stcmth08,
                               stcmth09,
                               stcmth10,
                               stcmth11,
                               stcmth12,
                               cfwdc,
                               bfwdv,
                               stvmth01,
                               stvmth02,
                               stvmth03,
                               stvmth04,
                               stvmth05,
                               stvmth06,
                               stvmth07,
                               stvmth08,
                               stvmth09,
                               stvmth10,
                               stvmth11,
                               stvmth12,
                               cfwdv,
                               bfwdp,
                               stpmth01,
                               stpmth02,
                               stpmth03,
                               stpmth04,
                               stpmth05,
                               stpmth06,
                               stpmth07,
                               stpmth08,
                               stpmth09,
                               stpmth10,
                               stpmth11,
                               stpmth12,
                               cfwdp,
                               bfwds,
                               stsmth01,
                               stsmth02,
                               stsmth03,
                               stsmth04,
                               stsmth05,
                               stsmth06,
                               stsmth07,
                               stsmth08,
                               stsmth09,
                               stsmth10,
                               stsmth11,
                               stsmth12,
                               cfwds,
                               bfwdm,
                               stmmth01,
                               stmmth02,
                               stmmth03,
                               stmmth04,
                               stmmth05,
                               stmmth06,
                               stmmth07,
                               stmmth08,
                               stmmth09,
                               stmmth10,
                               stmmth11,
                               stmmth12,
                               cfwdm,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(24);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getAgntnum().toInternal()
					+ getStatcat().toInternal()
					+ getAcctyr().toInternal()
					+ getAracde().toInternal()
					+ getCntbranch().toInternal()
					+ getBandage().toInternal()
					+ getBandsa().toInternal()
					+ getBandprm().toInternal()
					+ getBandtrm().toInternal()
					+ getCnttype().toInternal()
					+ getCrtable().toInternal()
					+ getOvrdcat().toInternal()
					+ getPstatcode().toInternal()
					+ getCntcurr().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, statcat);
			what = ExternalData.chop(what, acctyr);
			what = ExternalData.chop(what, aracde);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, bandage);
			what = ExternalData.chop(what, bandsa);
			what = ExternalData.chop(what, bandprm);
			what = ExternalData.chop(what, bandtrm);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, ovrdcat);
			what = ExternalData.chop(what, pstatcode);
			what = ExternalData.chop(what, cntcurr);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller130 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller140 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller150 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(agntnum.toInternal());
	nonKeyFiller30.setInternal(aracde.toInternal());
	nonKeyFiller40.setInternal(cntbranch.toInternal());
	nonKeyFiller50.setInternal(bandage.toInternal());
	nonKeyFiller60.setInternal(bandsa.toInternal());
	nonKeyFiller70.setInternal(bandprm.toInternal());
	nonKeyFiller80.setInternal(bandtrm.toInternal());
	nonKeyFiller90.setInternal(cnttype.toInternal());
	nonKeyFiller100.setInternal(crtable.toInternal());
	nonKeyFiller110.setInternal(ovrdcat.toInternal());
	nonKeyFiller120.setInternal(statcat.toInternal());
	nonKeyFiller130.setInternal(pstatcode.toInternal());
	nonKeyFiller140.setInternal(cntcurr.toInternal());
	nonKeyFiller150.setInternal(acctyr.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(666);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ nonKeyFiller130.toInternal()
					+ nonKeyFiller140.toInternal()
					+ nonKeyFiller150.toInternal()
					+ getBfwdc().toInternal()
					+ getStcmth01().toInternal()
					+ getStcmth02().toInternal()
					+ getStcmth03().toInternal()
					+ getStcmth04().toInternal()
					+ getStcmth05().toInternal()
					+ getStcmth06().toInternal()
					+ getStcmth07().toInternal()
					+ getStcmth08().toInternal()
					+ getStcmth09().toInternal()
					+ getStcmth10().toInternal()
					+ getStcmth11().toInternal()
					+ getStcmth12().toInternal()
					+ getCfwdc().toInternal()
					+ getBfwdv().toInternal()
					+ getStvmth01().toInternal()
					+ getStvmth02().toInternal()
					+ getStvmth03().toInternal()
					+ getStvmth04().toInternal()
					+ getStvmth05().toInternal()
					+ getStvmth06().toInternal()
					+ getStvmth07().toInternal()
					+ getStvmth08().toInternal()
					+ getStvmth09().toInternal()
					+ getStvmth10().toInternal()
					+ getStvmth11().toInternal()
					+ getStvmth12().toInternal()
					+ getCfwdv().toInternal()
					+ getBfwdp().toInternal()
					+ getStpmth01().toInternal()
					+ getStpmth02().toInternal()
					+ getStpmth03().toInternal()
					+ getStpmth04().toInternal()
					+ getStpmth05().toInternal()
					+ getStpmth06().toInternal()
					+ getStpmth07().toInternal()
					+ getStpmth08().toInternal()
					+ getStpmth09().toInternal()
					+ getStpmth10().toInternal()
					+ getStpmth11().toInternal()
					+ getStpmth12().toInternal()
					+ getCfwdp().toInternal()
					+ getBfwds().toInternal()
					+ getStsmth01().toInternal()
					+ getStsmth02().toInternal()
					+ getStsmth03().toInternal()
					+ getStsmth04().toInternal()
					+ getStsmth05().toInternal()
					+ getStsmth06().toInternal()
					+ getStsmth07().toInternal()
					+ getStsmth08().toInternal()
					+ getStsmth09().toInternal()
					+ getStsmth10().toInternal()
					+ getStsmth11().toInternal()
					+ getStsmth12().toInternal()
					+ getCfwds().toInternal()
					+ getBfwdm().toInternal()
					+ getStmmth01().toInternal()
					+ getStmmth02().toInternal()
					+ getStmmth03().toInternal()
					+ getStmmth04().toInternal()
					+ getStmmth05().toInternal()
					+ getStmmth06().toInternal()
					+ getStmmth07().toInternal()
					+ getStmmth08().toInternal()
					+ getStmmth09().toInternal()
					+ getStmmth10().toInternal()
					+ getStmmth11().toInternal()
					+ getStmmth12().toInternal()
					+ getCfwdm().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, nonKeyFiller130);
			what = ExternalData.chop(what, nonKeyFiller140);
			what = ExternalData.chop(what, nonKeyFiller150);
			what = ExternalData.chop(what, bfwdc);
			what = ExternalData.chop(what, stcmth01);
			what = ExternalData.chop(what, stcmth02);
			what = ExternalData.chop(what, stcmth03);
			what = ExternalData.chop(what, stcmth04);
			what = ExternalData.chop(what, stcmth05);
			what = ExternalData.chop(what, stcmth06);
			what = ExternalData.chop(what, stcmth07);
			what = ExternalData.chop(what, stcmth08);
			what = ExternalData.chop(what, stcmth09);
			what = ExternalData.chop(what, stcmth10);
			what = ExternalData.chop(what, stcmth11);
			what = ExternalData.chop(what, stcmth12);
			what = ExternalData.chop(what, cfwdc);
			what = ExternalData.chop(what, bfwdv);
			what = ExternalData.chop(what, stvmth01);
			what = ExternalData.chop(what, stvmth02);
			what = ExternalData.chop(what, stvmth03);
			what = ExternalData.chop(what, stvmth04);
			what = ExternalData.chop(what, stvmth05);
			what = ExternalData.chop(what, stvmth06);
			what = ExternalData.chop(what, stvmth07);
			what = ExternalData.chop(what, stvmth08);
			what = ExternalData.chop(what, stvmth09);
			what = ExternalData.chop(what, stvmth10);
			what = ExternalData.chop(what, stvmth11);
			what = ExternalData.chop(what, stvmth12);
			what = ExternalData.chop(what, cfwdv);
			what = ExternalData.chop(what, bfwdp);
			what = ExternalData.chop(what, stpmth01);
			what = ExternalData.chop(what, stpmth02);
			what = ExternalData.chop(what, stpmth03);
			what = ExternalData.chop(what, stpmth04);
			what = ExternalData.chop(what, stpmth05);
			what = ExternalData.chop(what, stpmth06);
			what = ExternalData.chop(what, stpmth07);
			what = ExternalData.chop(what, stpmth08);
			what = ExternalData.chop(what, stpmth09);
			what = ExternalData.chop(what, stpmth10);
			what = ExternalData.chop(what, stpmth11);
			what = ExternalData.chop(what, stpmth12);
			what = ExternalData.chop(what, cfwdp);
			what = ExternalData.chop(what, bfwds);
			what = ExternalData.chop(what, stsmth01);
			what = ExternalData.chop(what, stsmth02);
			what = ExternalData.chop(what, stsmth03);
			what = ExternalData.chop(what, stsmth04);
			what = ExternalData.chop(what, stsmth05);
			what = ExternalData.chop(what, stsmth06);
			what = ExternalData.chop(what, stsmth07);
			what = ExternalData.chop(what, stsmth08);
			what = ExternalData.chop(what, stsmth09);
			what = ExternalData.chop(what, stsmth10);
			what = ExternalData.chop(what, stsmth11);
			what = ExternalData.chop(what, stsmth12);
			what = ExternalData.chop(what, cfwds);
			what = ExternalData.chop(what, bfwdm);
			what = ExternalData.chop(what, stmmth01);
			what = ExternalData.chop(what, stmmth02);
			what = ExternalData.chop(what, stmmth03);
			what = ExternalData.chop(what, stmmth04);
			what = ExternalData.chop(what, stmmth05);
			what = ExternalData.chop(what, stmmth06);
			what = ExternalData.chop(what, stmmth07);
			what = ExternalData.chop(what, stmmth08);
			what = ExternalData.chop(what, stmmth09);
			what = ExternalData.chop(what, stmmth10);
			what = ExternalData.chop(what, stmmth11);
			what = ExternalData.chop(what, stmmth12);
			what = ExternalData.chop(what, cfwdm);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	public FixedLengthStringData getStatcat() {
		return statcat;
	}
	public void setStatcat(Object what) {
		statcat.set(what);
	}
	public PackedDecimalData getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(Object what) {
		setAcctyr(what, false);
	}
	public void setAcctyr(Object what, boolean rounded) {
		if (rounded)
			acctyr.setRounded(what);
		else
			acctyr.set(what);
	}
	public FixedLengthStringData getAracde() {
		return aracde;
	}
	public void setAracde(Object what) {
		aracde.set(what);
	}
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}
	public FixedLengthStringData getBandage() {
		return bandage;
	}
	public void setBandage(Object what) {
		bandage.set(what);
	}
	public FixedLengthStringData getBandsa() {
		return bandsa;
	}
	public void setBandsa(Object what) {
		bandsa.set(what);
	}
	public FixedLengthStringData getBandprm() {
		return bandprm;
	}
	public void setBandprm(Object what) {
		bandprm.set(what);
	}
	public FixedLengthStringData getBandtrm() {
		return bandtrm;
	}
	public void setBandtrm(Object what) {
		bandtrm.set(what);
	}
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}
	public FixedLengthStringData getOvrdcat() {
		return ovrdcat;
	}
	public void setOvrdcat(Object what) {
		ovrdcat.set(what);
	}
	public FixedLengthStringData getPstatcode() {
		return pstatcode;
	}
	public void setPstatcode(Object what) {
		pstatcode.set(what);
	}
	public FixedLengthStringData getCntcurr() {
		return cntcurr;
	}
	public void setCntcurr(Object what) {
		cntcurr.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getBfwdc() {
		return bfwdc;
	}
	public void setBfwdc(Object what) {
		setBfwdc(what, false);
	}
	public void setBfwdc(Object what, boolean rounded) {
		if (rounded)
			bfwdc.setRounded(what);
		else
			bfwdc.set(what);
	}	
	public PackedDecimalData getStcmth01() {
		return stcmth01;
	}
	public void setStcmth01(Object what) {
		setStcmth01(what, false);
	}
	public void setStcmth01(Object what, boolean rounded) {
		if (rounded)
			stcmth01.setRounded(what);
		else
			stcmth01.set(what);
	}	
	public PackedDecimalData getStcmth02() {
		return stcmth02;
	}
	public void setStcmth02(Object what) {
		setStcmth02(what, false);
	}
	public void setStcmth02(Object what, boolean rounded) {
		if (rounded)
			stcmth02.setRounded(what);
		else
			stcmth02.set(what);
	}	
	public PackedDecimalData getStcmth03() {
		return stcmth03;
	}
	public void setStcmth03(Object what) {
		setStcmth03(what, false);
	}
	public void setStcmth03(Object what, boolean rounded) {
		if (rounded)
			stcmth03.setRounded(what);
		else
			stcmth03.set(what);
	}	
	public PackedDecimalData getStcmth04() {
		return stcmth04;
	}
	public void setStcmth04(Object what) {
		setStcmth04(what, false);
	}
	public void setStcmth04(Object what, boolean rounded) {
		if (rounded)
			stcmth04.setRounded(what);
		else
			stcmth04.set(what);
	}	
	public PackedDecimalData getStcmth05() {
		return stcmth05;
	}
	public void setStcmth05(Object what) {
		setStcmth05(what, false);
	}
	public void setStcmth05(Object what, boolean rounded) {
		if (rounded)
			stcmth05.setRounded(what);
		else
			stcmth05.set(what);
	}	
	public PackedDecimalData getStcmth06() {
		return stcmth06;
	}
	public void setStcmth06(Object what) {
		setStcmth06(what, false);
	}
	public void setStcmth06(Object what, boolean rounded) {
		if (rounded)
			stcmth06.setRounded(what);
		else
			stcmth06.set(what);
	}	
	public PackedDecimalData getStcmth07() {
		return stcmth07;
	}
	public void setStcmth07(Object what) {
		setStcmth07(what, false);
	}
	public void setStcmth07(Object what, boolean rounded) {
		if (rounded)
			stcmth07.setRounded(what);
		else
			stcmth07.set(what);
	}	
	public PackedDecimalData getStcmth08() {
		return stcmth08;
	}
	public void setStcmth08(Object what) {
		setStcmth08(what, false);
	}
	public void setStcmth08(Object what, boolean rounded) {
		if (rounded)
			stcmth08.setRounded(what);
		else
			stcmth08.set(what);
	}	
	public PackedDecimalData getStcmth09() {
		return stcmth09;
	}
	public void setStcmth09(Object what) {
		setStcmth09(what, false);
	}
	public void setStcmth09(Object what, boolean rounded) {
		if (rounded)
			stcmth09.setRounded(what);
		else
			stcmth09.set(what);
	}	
	public PackedDecimalData getStcmth10() {
		return stcmth10;
	}
	public void setStcmth10(Object what) {
		setStcmth10(what, false);
	}
	public void setStcmth10(Object what, boolean rounded) {
		if (rounded)
			stcmth10.setRounded(what);
		else
			stcmth10.set(what);
	}	
	public PackedDecimalData getStcmth11() {
		return stcmth11;
	}
	public void setStcmth11(Object what) {
		setStcmth11(what, false);
	}
	public void setStcmth11(Object what, boolean rounded) {
		if (rounded)
			stcmth11.setRounded(what);
		else
			stcmth11.set(what);
	}	
	public PackedDecimalData getStcmth12() {
		return stcmth12;
	}
	public void setStcmth12(Object what) {
		setStcmth12(what, false);
	}
	public void setStcmth12(Object what, boolean rounded) {
		if (rounded)
			stcmth12.setRounded(what);
		else
			stcmth12.set(what);
	}	
	public PackedDecimalData getCfwdc() {
		return cfwdc;
	}
	public void setCfwdc(Object what) {
		setCfwdc(what, false);
	}
	public void setCfwdc(Object what, boolean rounded) {
		if (rounded)
			cfwdc.setRounded(what);
		else
			cfwdc.set(what);
	}	
	public PackedDecimalData getBfwdv() {
		return bfwdv;
	}
	public void setBfwdv(Object what) {
		setBfwdv(what, false);
	}
	public void setBfwdv(Object what, boolean rounded) {
		if (rounded)
			bfwdv.setRounded(what);
		else
			bfwdv.set(what);
	}	
	public PackedDecimalData getStvmth01() {
		return stvmth01;
	}
	public void setStvmth01(Object what) {
		setStvmth01(what, false);
	}
	public void setStvmth01(Object what, boolean rounded) {
		if (rounded)
			stvmth01.setRounded(what);
		else
			stvmth01.set(what);
	}	
	public PackedDecimalData getStvmth02() {
		return stvmth02;
	}
	public void setStvmth02(Object what) {
		setStvmth02(what, false);
	}
	public void setStvmth02(Object what, boolean rounded) {
		if (rounded)
			stvmth02.setRounded(what);
		else
			stvmth02.set(what);
	}	
	public PackedDecimalData getStvmth03() {
		return stvmth03;
	}
	public void setStvmth03(Object what) {
		setStvmth03(what, false);
	}
	public void setStvmth03(Object what, boolean rounded) {
		if (rounded)
			stvmth03.setRounded(what);
		else
			stvmth03.set(what);
	}	
	public PackedDecimalData getStvmth04() {
		return stvmth04;
	}
	public void setStvmth04(Object what) {
		setStvmth04(what, false);
	}
	public void setStvmth04(Object what, boolean rounded) {
		if (rounded)
			stvmth04.setRounded(what);
		else
			stvmth04.set(what);
	}	
	public PackedDecimalData getStvmth05() {
		return stvmth05;
	}
	public void setStvmth05(Object what) {
		setStvmth05(what, false);
	}
	public void setStvmth05(Object what, boolean rounded) {
		if (rounded)
			stvmth05.setRounded(what);
		else
			stvmth05.set(what);
	}	
	public PackedDecimalData getStvmth06() {
		return stvmth06;
	}
	public void setStvmth06(Object what) {
		setStvmth06(what, false);
	}
	public void setStvmth06(Object what, boolean rounded) {
		if (rounded)
			stvmth06.setRounded(what);
		else
			stvmth06.set(what);
	}	
	public PackedDecimalData getStvmth07() {
		return stvmth07;
	}
	public void setStvmth07(Object what) {
		setStvmth07(what, false);
	}
	public void setStvmth07(Object what, boolean rounded) {
		if (rounded)
			stvmth07.setRounded(what);
		else
			stvmth07.set(what);
	}	
	public PackedDecimalData getStvmth08() {
		return stvmth08;
	}
	public void setStvmth08(Object what) {
		setStvmth08(what, false);
	}
	public void setStvmth08(Object what, boolean rounded) {
		if (rounded)
			stvmth08.setRounded(what);
		else
			stvmth08.set(what);
	}	
	public PackedDecimalData getStvmth09() {
		return stvmth09;
	}
	public void setStvmth09(Object what) {
		setStvmth09(what, false);
	}
	public void setStvmth09(Object what, boolean rounded) {
		if (rounded)
			stvmth09.setRounded(what);
		else
			stvmth09.set(what);
	}	
	public PackedDecimalData getStvmth10() {
		return stvmth10;
	}
	public void setStvmth10(Object what) {
		setStvmth10(what, false);
	}
	public void setStvmth10(Object what, boolean rounded) {
		if (rounded)
			stvmth10.setRounded(what);
		else
			stvmth10.set(what);
	}	
	public PackedDecimalData getStvmth11() {
		return stvmth11;
	}
	public void setStvmth11(Object what) {
		setStvmth11(what, false);
	}
	public void setStvmth11(Object what, boolean rounded) {
		if (rounded)
			stvmth11.setRounded(what);
		else
			stvmth11.set(what);
	}	
	public PackedDecimalData getStvmth12() {
		return stvmth12;
	}
	public void setStvmth12(Object what) {
		setStvmth12(what, false);
	}
	public void setStvmth12(Object what, boolean rounded) {
		if (rounded)
			stvmth12.setRounded(what);
		else
			stvmth12.set(what);
	}	
	public PackedDecimalData getCfwdv() {
		return cfwdv;
	}
	public void setCfwdv(Object what) {
		setCfwdv(what, false);
	}
	public void setCfwdv(Object what, boolean rounded) {
		if (rounded)
			cfwdv.setRounded(what);
		else
			cfwdv.set(what);
	}	
	public PackedDecimalData getBfwdp() {
		return bfwdp;
	}
	public void setBfwdp(Object what) {
		setBfwdp(what, false);
	}
	public void setBfwdp(Object what, boolean rounded) {
		if (rounded)
			bfwdp.setRounded(what);
		else
			bfwdp.set(what);
	}	
	public PackedDecimalData getStpmth01() {
		return stpmth01;
	}
	public void setStpmth01(Object what) {
		setStpmth01(what, false);
	}
	public void setStpmth01(Object what, boolean rounded) {
		if (rounded)
			stpmth01.setRounded(what);
		else
			stpmth01.set(what);
	}	
	public PackedDecimalData getStpmth02() {
		return stpmth02;
	}
	public void setStpmth02(Object what) {
		setStpmth02(what, false);
	}
	public void setStpmth02(Object what, boolean rounded) {
		if (rounded)
			stpmth02.setRounded(what);
		else
			stpmth02.set(what);
	}	
	public PackedDecimalData getStpmth03() {
		return stpmth03;
	}
	public void setStpmth03(Object what) {
		setStpmth03(what, false);
	}
	public void setStpmth03(Object what, boolean rounded) {
		if (rounded)
			stpmth03.setRounded(what);
		else
			stpmth03.set(what);
	}	
	public PackedDecimalData getStpmth04() {
		return stpmth04;
	}
	public void setStpmth04(Object what) {
		setStpmth04(what, false);
	}
	public void setStpmth04(Object what, boolean rounded) {
		if (rounded)
			stpmth04.setRounded(what);
		else
			stpmth04.set(what);
	}	
	public PackedDecimalData getStpmth05() {
		return stpmth05;
	}
	public void setStpmth05(Object what) {
		setStpmth05(what, false);
	}
	public void setStpmth05(Object what, boolean rounded) {
		if (rounded)
			stpmth05.setRounded(what);
		else
			stpmth05.set(what);
	}	
	public PackedDecimalData getStpmth06() {
		return stpmth06;
	}
	public void setStpmth06(Object what) {
		setStpmth06(what, false);
	}
	public void setStpmth06(Object what, boolean rounded) {
		if (rounded)
			stpmth06.setRounded(what);
		else
			stpmth06.set(what);
	}	
	public PackedDecimalData getStpmth07() {
		return stpmth07;
	}
	public void setStpmth07(Object what) {
		setStpmth07(what, false);
	}
	public void setStpmth07(Object what, boolean rounded) {
		if (rounded)
			stpmth07.setRounded(what);
		else
			stpmth07.set(what);
	}	
	public PackedDecimalData getStpmth08() {
		return stpmth08;
	}
	public void setStpmth08(Object what) {
		setStpmth08(what, false);
	}
	public void setStpmth08(Object what, boolean rounded) {
		if (rounded)
			stpmth08.setRounded(what);
		else
			stpmth08.set(what);
	}	
	public PackedDecimalData getStpmth09() {
		return stpmth09;
	}
	public void setStpmth09(Object what) {
		setStpmth09(what, false);
	}
	public void setStpmth09(Object what, boolean rounded) {
		if (rounded)
			stpmth09.setRounded(what);
		else
			stpmth09.set(what);
	}	
	public PackedDecimalData getStpmth10() {
		return stpmth10;
	}
	public void setStpmth10(Object what) {
		setStpmth10(what, false);
	}
	public void setStpmth10(Object what, boolean rounded) {
		if (rounded)
			stpmth10.setRounded(what);
		else
			stpmth10.set(what);
	}	
	public PackedDecimalData getStpmth11() {
		return stpmth11;
	}
	public void setStpmth11(Object what) {
		setStpmth11(what, false);
	}
	public void setStpmth11(Object what, boolean rounded) {
		if (rounded)
			stpmth11.setRounded(what);
		else
			stpmth11.set(what);
	}	
	public PackedDecimalData getStpmth12() {
		return stpmth12;
	}
	public void setStpmth12(Object what) {
		setStpmth12(what, false);
	}
	public void setStpmth12(Object what, boolean rounded) {
		if (rounded)
			stpmth12.setRounded(what);
		else
			stpmth12.set(what);
	}	
	public PackedDecimalData getCfwdp() {
		return cfwdp;
	}
	public void setCfwdp(Object what) {
		setCfwdp(what, false);
	}
	public void setCfwdp(Object what, boolean rounded) {
		if (rounded)
			cfwdp.setRounded(what);
		else
			cfwdp.set(what);
	}	
	public PackedDecimalData getBfwds() {
		return bfwds;
	}
	public void setBfwds(Object what) {
		setBfwds(what, false);
	}
	public void setBfwds(Object what, boolean rounded) {
		if (rounded)
			bfwds.setRounded(what);
		else
			bfwds.set(what);
	}	
	public PackedDecimalData getStsmth01() {
		return stsmth01;
	}
	public void setStsmth01(Object what) {
		setStsmth01(what, false);
	}
	public void setStsmth01(Object what, boolean rounded) {
		if (rounded)
			stsmth01.setRounded(what);
		else
			stsmth01.set(what);
	}	
	public PackedDecimalData getStsmth02() {
		return stsmth02;
	}
	public void setStsmth02(Object what) {
		setStsmth02(what, false);
	}
	public void setStsmth02(Object what, boolean rounded) {
		if (rounded)
			stsmth02.setRounded(what);
		else
			stsmth02.set(what);
	}	
	public PackedDecimalData getStsmth03() {
		return stsmth03;
	}
	public void setStsmth03(Object what) {
		setStsmth03(what, false);
	}
	public void setStsmth03(Object what, boolean rounded) {
		if (rounded)
			stsmth03.setRounded(what);
		else
			stsmth03.set(what);
	}	
	public PackedDecimalData getStsmth04() {
		return stsmth04;
	}
	public void setStsmth04(Object what) {
		setStsmth04(what, false);
	}
	public void setStsmth04(Object what, boolean rounded) {
		if (rounded)
			stsmth04.setRounded(what);
		else
			stsmth04.set(what);
	}	
	public PackedDecimalData getStsmth05() {
		return stsmth05;
	}
	public void setStsmth05(Object what) {
		setStsmth05(what, false);
	}
	public void setStsmth05(Object what, boolean rounded) {
		if (rounded)
			stsmth05.setRounded(what);
		else
			stsmth05.set(what);
	}	
	public PackedDecimalData getStsmth06() {
		return stsmth06;
	}
	public void setStsmth06(Object what) {
		setStsmth06(what, false);
	}
	public void setStsmth06(Object what, boolean rounded) {
		if (rounded)
			stsmth06.setRounded(what);
		else
			stsmth06.set(what);
	}	
	public PackedDecimalData getStsmth07() {
		return stsmth07;
	}
	public void setStsmth07(Object what) {
		setStsmth07(what, false);
	}
	public void setStsmth07(Object what, boolean rounded) {
		if (rounded)
			stsmth07.setRounded(what);
		else
			stsmth07.set(what);
	}	
	public PackedDecimalData getStsmth08() {
		return stsmth08;
	}
	public void setStsmth08(Object what) {
		setStsmth08(what, false);
	}
	public void setStsmth08(Object what, boolean rounded) {
		if (rounded)
			stsmth08.setRounded(what);
		else
			stsmth08.set(what);
	}	
	public PackedDecimalData getStsmth09() {
		return stsmth09;
	}
	public void setStsmth09(Object what) {
		setStsmth09(what, false);
	}
	public void setStsmth09(Object what, boolean rounded) {
		if (rounded)
			stsmth09.setRounded(what);
		else
			stsmth09.set(what);
	}	
	public PackedDecimalData getStsmth10() {
		return stsmth10;
	}
	public void setStsmth10(Object what) {
		setStsmth10(what, false);
	}
	public void setStsmth10(Object what, boolean rounded) {
		if (rounded)
			stsmth10.setRounded(what);
		else
			stsmth10.set(what);
	}	
	public PackedDecimalData getStsmth11() {
		return stsmth11;
	}
	public void setStsmth11(Object what) {
		setStsmth11(what, false);
	}
	public void setStsmth11(Object what, boolean rounded) {
		if (rounded)
			stsmth11.setRounded(what);
		else
			stsmth11.set(what);
	}	
	public PackedDecimalData getStsmth12() {
		return stsmth12;
	}
	public void setStsmth12(Object what) {
		setStsmth12(what, false);
	}
	public void setStsmth12(Object what, boolean rounded) {
		if (rounded)
			stsmth12.setRounded(what);
		else
			stsmth12.set(what);
	}	
	public PackedDecimalData getCfwds() {
		return cfwds;
	}
	public void setCfwds(Object what) {
		setCfwds(what, false);
	}
	public void setCfwds(Object what, boolean rounded) {
		if (rounded)
			cfwds.setRounded(what);
		else
			cfwds.set(what);
	}	
	public PackedDecimalData getBfwdm() {
		return bfwdm;
	}
	public void setBfwdm(Object what) {
		setBfwdm(what, false);
	}
	public void setBfwdm(Object what, boolean rounded) {
		if (rounded)
			bfwdm.setRounded(what);
		else
			bfwdm.set(what);
	}	
	public PackedDecimalData getStmmth01() {
		return stmmth01;
	}
	public void setStmmth01(Object what) {
		setStmmth01(what, false);
	}
	public void setStmmth01(Object what, boolean rounded) {
		if (rounded)
			stmmth01.setRounded(what);
		else
			stmmth01.set(what);
	}	
	public PackedDecimalData getStmmth02() {
		return stmmth02;
	}
	public void setStmmth02(Object what) {
		setStmmth02(what, false);
	}
	public void setStmmth02(Object what, boolean rounded) {
		if (rounded)
			stmmth02.setRounded(what);
		else
			stmmth02.set(what);
	}	
	public PackedDecimalData getStmmth03() {
		return stmmth03;
	}
	public void setStmmth03(Object what) {
		setStmmth03(what, false);
	}
	public void setStmmth03(Object what, boolean rounded) {
		if (rounded)
			stmmth03.setRounded(what);
		else
			stmmth03.set(what);
	}	
	public PackedDecimalData getStmmth04() {
		return stmmth04;
	}
	public void setStmmth04(Object what) {
		setStmmth04(what, false);
	}
	public void setStmmth04(Object what, boolean rounded) {
		if (rounded)
			stmmth04.setRounded(what);
		else
			stmmth04.set(what);
	}	
	public PackedDecimalData getStmmth05() {
		return stmmth05;
	}
	public void setStmmth05(Object what) {
		setStmmth05(what, false);
	}
	public void setStmmth05(Object what, boolean rounded) {
		if (rounded)
			stmmth05.setRounded(what);
		else
			stmmth05.set(what);
	}	
	public PackedDecimalData getStmmth06() {
		return stmmth06;
	}
	public void setStmmth06(Object what) {
		setStmmth06(what, false);
	}
	public void setStmmth06(Object what, boolean rounded) {
		if (rounded)
			stmmth06.setRounded(what);
		else
			stmmth06.set(what);
	}	
	public PackedDecimalData getStmmth07() {
		return stmmth07;
	}
	public void setStmmth07(Object what) {
		setStmmth07(what, false);
	}
	public void setStmmth07(Object what, boolean rounded) {
		if (rounded)
			stmmth07.setRounded(what);
		else
			stmmth07.set(what);
	}	
	public PackedDecimalData getStmmth08() {
		return stmmth08;
	}
	public void setStmmth08(Object what) {
		setStmmth08(what, false);
	}
	public void setStmmth08(Object what, boolean rounded) {
		if (rounded)
			stmmth08.setRounded(what);
		else
			stmmth08.set(what);
	}	
	public PackedDecimalData getStmmth09() {
		return stmmth09;
	}
	public void setStmmth09(Object what) {
		setStmmth09(what, false);
	}
	public void setStmmth09(Object what, boolean rounded) {
		if (rounded)
			stmmth09.setRounded(what);
		else
			stmmth09.set(what);
	}	
	public PackedDecimalData getStmmth10() {
		return stmmth10;
	}
	public void setStmmth10(Object what) {
		setStmmth10(what, false);
	}
	public void setStmmth10(Object what, boolean rounded) {
		if (rounded)
			stmmth10.setRounded(what);
		else
			stmmth10.set(what);
	}	
	public PackedDecimalData getStmmth11() {
		return stmmth11;
	}
	public void setStmmth11(Object what) {
		setStmmth11(what, false);
	}
	public void setStmmth11(Object what, boolean rounded) {
		if (rounded)
			stmmth11.setRounded(what);
		else
			stmmth11.set(what);
	}	
	public PackedDecimalData getStmmth12() {
		return stmmth12;
	}
	public void setStmmth12(Object what) {
		setStmmth12(what, false);
	}
	public void setStmmth12(Object what, boolean rounded) {
		if (rounded)
			stmmth12.setRounded(what);
		else
			stmmth12.set(what);
	}	
	public PackedDecimalData getCfwdm() {
		return cfwdm;
	}
	public void setCfwdm(Object what) {
		setCfwdm(what, false);
	}
	public void setCfwdm(Object what, boolean rounded) {
		if (rounded)
			cfwdm.setRounded(what);
		else
			cfwdm.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getStvmths() {
		return new FixedLengthStringData(stvmth01.toInternal()
										+ stvmth02.toInternal()
										+ stvmth03.toInternal()
										+ stvmth04.toInternal()
										+ stvmth05.toInternal()
										+ stvmth06.toInternal()
										+ stvmth07.toInternal()
										+ stvmth08.toInternal()
										+ stvmth09.toInternal()
										+ stvmth10.toInternal()
										+ stvmth11.toInternal()
										+ stvmth12.toInternal());
	}
	public void setStvmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStvmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stvmth01);
		what = ExternalData.chop(what, stvmth02);
		what = ExternalData.chop(what, stvmth03);
		what = ExternalData.chop(what, stvmth04);
		what = ExternalData.chop(what, stvmth05);
		what = ExternalData.chop(what, stvmth06);
		what = ExternalData.chop(what, stvmth07);
		what = ExternalData.chop(what, stvmth08);
		what = ExternalData.chop(what, stvmth09);
		what = ExternalData.chop(what, stvmth10);
		what = ExternalData.chop(what, stvmth11);
		what = ExternalData.chop(what, stvmth12);
	}
	public PackedDecimalData getStvmth(BaseData indx) {
		return getStvmth(indx.toInt());
	}
	public PackedDecimalData getStvmth(int indx) {

		switch (indx) {
			case 1 : return stvmth01;
			case 2 : return stvmth02;
			case 3 : return stvmth03;
			case 4 : return stvmth04;
			case 5 : return stvmth05;
			case 6 : return stvmth06;
			case 7 : return stvmth07;
			case 8 : return stvmth08;
			case 9 : return stvmth09;
			case 10 : return stvmth10;
			case 11 : return stvmth11;
			case 12 : return stvmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStvmth(BaseData indx, Object what) {
		setStvmth(indx, what, false);
	}
	public void setStvmth(BaseData indx, Object what, boolean rounded) {
		setStvmth(indx.toInt(), what, rounded);
	}
	public void setStvmth(int indx, Object what) {
		setStvmth(indx, what, false);
	}
	public void setStvmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStvmth01(what, rounded);
					 break;
			case 2 : setStvmth02(what, rounded);
					 break;
			case 3 : setStvmth03(what, rounded);
					 break;
			case 4 : setStvmth04(what, rounded);
					 break;
			case 5 : setStvmth05(what, rounded);
					 break;
			case 6 : setStvmth06(what, rounded);
					 break;
			case 7 : setStvmth07(what, rounded);
					 break;
			case 8 : setStvmth08(what, rounded);
					 break;
			case 9 : setStvmth09(what, rounded);
					 break;
			case 10 : setStvmth10(what, rounded);
					 break;
			case 11 : setStvmth11(what, rounded);
					 break;
			case 12 : setStvmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStsmths() {
		return new FixedLengthStringData(stsmth01.toInternal()
										+ stsmth02.toInternal()
										+ stsmth03.toInternal()
										+ stsmth04.toInternal()
										+ stsmth05.toInternal()
										+ stsmth06.toInternal()
										+ stsmth07.toInternal()
										+ stsmth08.toInternal()
										+ stsmth09.toInternal()
										+ stsmth10.toInternal()
										+ stsmth11.toInternal()
										+ stsmth12.toInternal());
	}
	public void setStsmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStsmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stsmth01);
		what = ExternalData.chop(what, stsmth02);
		what = ExternalData.chop(what, stsmth03);
		what = ExternalData.chop(what, stsmth04);
		what = ExternalData.chop(what, stsmth05);
		what = ExternalData.chop(what, stsmth06);
		what = ExternalData.chop(what, stsmth07);
		what = ExternalData.chop(what, stsmth08);
		what = ExternalData.chop(what, stsmth09);
		what = ExternalData.chop(what, stsmth10);
		what = ExternalData.chop(what, stsmth11);
		what = ExternalData.chop(what, stsmth12);
	}
	public PackedDecimalData getStsmth(BaseData indx) {
		return getStsmth(indx.toInt());
	}
	public PackedDecimalData getStsmth(int indx) {

		switch (indx) {
			case 1 : return stsmth01;
			case 2 : return stsmth02;
			case 3 : return stsmth03;
			case 4 : return stsmth04;
			case 5 : return stsmth05;
			case 6 : return stsmth06;
			case 7 : return stsmth07;
			case 8 : return stsmth08;
			case 9 : return stsmth09;
			case 10 : return stsmth10;
			case 11 : return stsmth11;
			case 12 : return stsmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStsmth(BaseData indx, Object what) {
		setStsmth(indx, what, false);
	}
	public void setStsmth(BaseData indx, Object what, boolean rounded) {
		setStsmth(indx.toInt(), what, rounded);
	}
	public void setStsmth(int indx, Object what) {
		setStsmth(indx, what, false);
	}
	public void setStsmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStsmth01(what, rounded);
					 break;
			case 2 : setStsmth02(what, rounded);
					 break;
			case 3 : setStsmth03(what, rounded);
					 break;
			case 4 : setStsmth04(what, rounded);
					 break;
			case 5 : setStsmth05(what, rounded);
					 break;
			case 6 : setStsmth06(what, rounded);
					 break;
			case 7 : setStsmth07(what, rounded);
					 break;
			case 8 : setStsmth08(what, rounded);
					 break;
			case 9 : setStsmth09(what, rounded);
					 break;
			case 10 : setStsmth10(what, rounded);
					 break;
			case 11 : setStsmth11(what, rounded);
					 break;
			case 12 : setStsmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStpmths() {
		return new FixedLengthStringData(stpmth01.toInternal()
										+ stpmth02.toInternal()
										+ stpmth03.toInternal()
										+ stpmth04.toInternal()
										+ stpmth05.toInternal()
										+ stpmth06.toInternal()
										+ stpmth07.toInternal()
										+ stpmth08.toInternal()
										+ stpmth09.toInternal()
										+ stpmth10.toInternal()
										+ stpmth11.toInternal()
										+ stpmth12.toInternal());
	}
	public void setStpmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStpmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stpmth01);
		what = ExternalData.chop(what, stpmth02);
		what = ExternalData.chop(what, stpmth03);
		what = ExternalData.chop(what, stpmth04);
		what = ExternalData.chop(what, stpmth05);
		what = ExternalData.chop(what, stpmth06);
		what = ExternalData.chop(what, stpmth07);
		what = ExternalData.chop(what, stpmth08);
		what = ExternalData.chop(what, stpmth09);
		what = ExternalData.chop(what, stpmth10);
		what = ExternalData.chop(what, stpmth11);
		what = ExternalData.chop(what, stpmth12);
	}
	public PackedDecimalData getStpmth(BaseData indx) {
		return getStpmth(indx.toInt());
	}
	public PackedDecimalData getStpmth(int indx) {

		switch (indx) {
			case 1 : return stpmth01;
			case 2 : return stpmth02;
			case 3 : return stpmth03;
			case 4 : return stpmth04;
			case 5 : return stpmth05;
			case 6 : return stpmth06;
			case 7 : return stpmth07;
			case 8 : return stpmth08;
			case 9 : return stpmth09;
			case 10 : return stpmth10;
			case 11 : return stpmth11;
			case 12 : return stpmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStpmth(BaseData indx, Object what) {
		setStpmth(indx, what, false);
	}
	public void setStpmth(BaseData indx, Object what, boolean rounded) {
		setStpmth(indx.toInt(), what, rounded);
	}
	public void setStpmth(int indx, Object what) {
		setStpmth(indx, what, false);
	}
	public void setStpmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStpmth01(what, rounded);
					 break;
			case 2 : setStpmth02(what, rounded);
					 break;
			case 3 : setStpmth03(what, rounded);
					 break;
			case 4 : setStpmth04(what, rounded);
					 break;
			case 5 : setStpmth05(what, rounded);
					 break;
			case 6 : setStpmth06(what, rounded);
					 break;
			case 7 : setStpmth07(what, rounded);
					 break;
			case 8 : setStpmth08(what, rounded);
					 break;
			case 9 : setStpmth09(what, rounded);
					 break;
			case 10 : setStpmth10(what, rounded);
					 break;
			case 11 : setStpmth11(what, rounded);
					 break;
			case 12 : setStpmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStmmths() {
		return new FixedLengthStringData(stmmth01.toInternal()
										+ stmmth02.toInternal()
										+ stmmth03.toInternal()
										+ stmmth04.toInternal()
										+ stmmth05.toInternal()
										+ stmmth06.toInternal()
										+ stmmth07.toInternal()
										+ stmmth08.toInternal()
										+ stmmth09.toInternal()
										+ stmmth10.toInternal()
										+ stmmth11.toInternal()
										+ stmmth12.toInternal());
	}
	public void setStmmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStmmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stmmth01);
		what = ExternalData.chop(what, stmmth02);
		what = ExternalData.chop(what, stmmth03);
		what = ExternalData.chop(what, stmmth04);
		what = ExternalData.chop(what, stmmth05);
		what = ExternalData.chop(what, stmmth06);
		what = ExternalData.chop(what, stmmth07);
		what = ExternalData.chop(what, stmmth08);
		what = ExternalData.chop(what, stmmth09);
		what = ExternalData.chop(what, stmmth10);
		what = ExternalData.chop(what, stmmth11);
		what = ExternalData.chop(what, stmmth12);
	}
	public PackedDecimalData getStmmth(BaseData indx) {
		return getStmmth(indx.toInt());
	}
	public PackedDecimalData getStmmth(int indx) {

		switch (indx) {
			case 1 : return stmmth01;
			case 2 : return stmmth02;
			case 3 : return stmmth03;
			case 4 : return stmmth04;
			case 5 : return stmmth05;
			case 6 : return stmmth06;
			case 7 : return stmmth07;
			case 8 : return stmmth08;
			case 9 : return stmmth09;
			case 10 : return stmmth10;
			case 11 : return stmmth11;
			case 12 : return stmmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStmmth(BaseData indx, Object what) {
		setStmmth(indx, what, false);
	}
	public void setStmmth(BaseData indx, Object what, boolean rounded) {
		setStmmth(indx.toInt(), what, rounded);
	}
	public void setStmmth(int indx, Object what) {
		setStmmth(indx, what, false);
	}
	public void setStmmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStmmth01(what, rounded);
					 break;
			case 2 : setStmmth02(what, rounded);
					 break;
			case 3 : setStmmth03(what, rounded);
					 break;
			case 4 : setStmmth04(what, rounded);
					 break;
			case 5 : setStmmth05(what, rounded);
					 break;
			case 6 : setStmmth06(what, rounded);
					 break;
			case 7 : setStmmth07(what, rounded);
					 break;
			case 8 : setStmmth08(what, rounded);
					 break;
			case 9 : setStmmth09(what, rounded);
					 break;
			case 10 : setStmmth10(what, rounded);
					 break;
			case 11 : setStmmth11(what, rounded);
					 break;
			case 12 : setStmmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStcmths() {
		return new FixedLengthStringData(stcmth01.toInternal()
										+ stcmth02.toInternal()
										+ stcmth03.toInternal()
										+ stcmth04.toInternal()
										+ stcmth05.toInternal()
										+ stcmth06.toInternal()
										+ stcmth07.toInternal()
										+ stcmth08.toInternal()
										+ stcmth09.toInternal()
										+ stcmth10.toInternal()
										+ stcmth11.toInternal()
										+ stcmth12.toInternal());
	}
	public void setStcmths(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStcmths().getLength()).init(obj);
	
		what = ExternalData.chop(what, stcmth01);
		what = ExternalData.chop(what, stcmth02);
		what = ExternalData.chop(what, stcmth03);
		what = ExternalData.chop(what, stcmth04);
		what = ExternalData.chop(what, stcmth05);
		what = ExternalData.chop(what, stcmth06);
		what = ExternalData.chop(what, stcmth07);
		what = ExternalData.chop(what, stcmth08);
		what = ExternalData.chop(what, stcmth09);
		what = ExternalData.chop(what, stcmth10);
		what = ExternalData.chop(what, stcmth11);
		what = ExternalData.chop(what, stcmth12);
	}
	public PackedDecimalData getStcmth(BaseData indx) {
		return getStcmth(indx.toInt());
	}
	public PackedDecimalData getStcmth(int indx) {

		switch (indx) {
			case 1 : return stcmth01;
			case 2 : return stcmth02;
			case 3 : return stcmth03;
			case 4 : return stcmth04;
			case 5 : return stcmth05;
			case 6 : return stcmth06;
			case 7 : return stcmth07;
			case 8 : return stcmth08;
			case 9 : return stcmth09;
			case 10 : return stcmth10;
			case 11 : return stcmth11;
			case 12 : return stcmth12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStcmth(BaseData indx, Object what) {
		setStcmth(indx, what, false);
	}
	public void setStcmth(BaseData indx, Object what, boolean rounded) {
		setStcmth(indx.toInt(), what, rounded);
	}
	public void setStcmth(int indx, Object what) {
		setStcmth(indx, what, false);
	}
	public void setStcmth(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStcmth01(what, rounded);
					 break;
			case 2 : setStcmth02(what, rounded);
					 break;
			case 3 : setStcmth03(what, rounded);
					 break;
			case 4 : setStcmth04(what, rounded);
					 break;
			case 5 : setStcmth05(what, rounded);
					 break;
			case 6 : setStcmth06(what, rounded);
					 break;
			case 7 : setStcmth07(what, rounded);
					 break;
			case 8 : setStcmth08(what, rounded);
					 break;
			case 9 : setStcmth09(what, rounded);
					 break;
			case 10 : setStcmth10(what, rounded);
					 break;
			case 11 : setStcmth11(what, rounded);
					 break;
			case 12 : setStcmth12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		agntnum.clear();
		statcat.clear();
		acctyr.clear();
		aracde.clear();
		cntbranch.clear();
		bandage.clear();
		bandsa.clear();
		bandprm.clear();
		bandtrm.clear();
		cnttype.clear();
		crtable.clear();
		ovrdcat.clear();
		pstatcode.clear();
		cntcurr.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		nonKeyFiller130.clear();
		nonKeyFiller140.clear();
		nonKeyFiller150.clear();
		bfwdc.clear();
		stcmth01.clear();
		stcmth02.clear();
		stcmth03.clear();
		stcmth04.clear();
		stcmth05.clear();
		stcmth06.clear();
		stcmth07.clear();
		stcmth08.clear();
		stcmth09.clear();
		stcmth10.clear();
		stcmth11.clear();
		stcmth12.clear();
		cfwdc.clear();
		bfwdv.clear();
		stvmth01.clear();
		stvmth02.clear();
		stvmth03.clear();
		stvmth04.clear();
		stvmth05.clear();
		stvmth06.clear();
		stvmth07.clear();
		stvmth08.clear();
		stvmth09.clear();
		stvmth10.clear();
		stvmth11.clear();
		stvmth12.clear();
		cfwdv.clear();
		bfwdp.clear();
		stpmth01.clear();
		stpmth02.clear();
		stpmth03.clear();
		stpmth04.clear();
		stpmth05.clear();
		stpmth06.clear();
		stpmth07.clear();
		stpmth08.clear();
		stpmth09.clear();
		stpmth10.clear();
		stpmth11.clear();
		stpmth12.clear();
		cfwdp.clear();
		bfwds.clear();
		stsmth01.clear();
		stsmth02.clear();
		stsmth03.clear();
		stsmth04.clear();
		stsmth05.clear();
		stsmth06.clear();
		stsmth07.clear();
		stsmth08.clear();
		stsmth09.clear();
		stsmth10.clear();
		stsmth11.clear();
		stsmth12.clear();
		cfwds.clear();
		bfwdm.clear();
		stmmth01.clear();
		stmmth02.clear();
		stmmth03.clear();
		stmmth04.clear();
		stmmth05.clear();
		stmmth06.clear();
		stmmth07.clear();
		stmmth08.clear();
		stmmth09.clear();
		stmmth10.clear();
		stmmth11.clear();
		stmmth12.clear();
		cfwdm.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}