package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:23
 * Description:
 * Copybook name: RACDSTAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdstakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdstaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdstaKey = new FixedLengthStringData(64).isAPartOf(racdstaFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdstaChdrcoy = new FixedLengthStringData(1).isAPartOf(racdstaKey, 0);
  	public FixedLengthStringData racdstaChdrnum = new FixedLengthStringData(8).isAPartOf(racdstaKey, 1);
  	public FixedLengthStringData racdstaLife = new FixedLengthStringData(2).isAPartOf(racdstaKey, 9);
  	public FixedLengthStringData racdstaCoverage = new FixedLengthStringData(2).isAPartOf(racdstaKey, 11);
  	public FixedLengthStringData racdstaRider = new FixedLengthStringData(2).isAPartOf(racdstaKey, 13);
  	public PackedDecimalData racdstaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdstaKey, 15);
  	public PackedDecimalData racdstaTranno = new PackedDecimalData(5, 0).isAPartOf(racdstaKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(racdstaKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdstaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdstaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}