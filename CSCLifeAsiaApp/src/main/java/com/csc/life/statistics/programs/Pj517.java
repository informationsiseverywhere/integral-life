/*
 * File: Pj517.java
 * Date: 30 August 2009 1:12:15
 * Author: Quipoz Limited
 *
 * Class transformed from PJ517.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.screens.Sj517ScreenVars;
import com.csc.smart.dataaccess.BppdTableDAM;
import com.csc.smart.dataaccess.BscdTableDAM;
import com.csc.smart.dataaccess.BsscTableDAM;
import com.csc.smart.dataaccess.BupaTableDAM;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters - SCRVER(02)            Do Not Delete|
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*            PJ517 MAINLINE.
*
*   Parameter prompt program
*
***********************************************************************
* </pre>
*/
public class Pj517 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("PJ517");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");
		/* FORMATS */
	private String bscdrec = "BSCDREC";
	private String bsscrec = "BSSCREC";
	private String buparec = "BUPAREC";
	private String bppdrec = "BPPDREC";
		/*Parameter Prompt Definition - Multi-thre*/
	private BppdTableDAM bppdIO = new BppdTableDAM();
		/*Schedule Definition - Multi Thread Batch*/
	private BscdTableDAM bscdIO = new BscdTableDAM();
		/*Schedule File - Multi-Thread Batch*/
	private BsscTableDAM bsscIO = new BsscTableDAM();
		/*User Parameter Area - Multi-Thread Batch*/
	private BupaTableDAM bupaIO = new BupaTableDAM();
	private Pj517par pj517par = new Pj517par();
	private Wsspsmart wsspsmart = new Wsspsmart();
	private Sj517ScreenVars sv = ScreenProgram.getScreenVars( Sj517ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		preExit
	}

	public Pj517() {
		super();
		screenVars = sv;
		new ScreenModel("Sj517", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		bsscIO.setFormat(bsscrec);
		bsscIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bsscIO);
		if (isNE(bsscIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bsscIO.getParams());
			fatalError600();
		}
		bscdIO.setFormat(bscdrec);
		bscdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bscdIO);
		if (isNE(bscdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bscdIO.getParams());
			fatalError600();
		}
		bppdIO.setFormat(bppdrec);
		bppdIO.setFunction(varcom.retrv);
		SmartFileCode.execute(appVars, bppdIO);
		if (isNE(bppdIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bppdIO.getParams());
			fatalError600();
		}
		sv.dataArea.set(SPACES);
		sv.acctmnth.set(bsscIO.getAcctMonth());
		sv.acctyr.set(bsscIO.getAcctYear());
		sv.scheduleName.set(bsscIO.getScheduleName());
		sv.scheduleNumber.set(bsscIO.getScheduleNumber());
		sv.effdate.set(bsscIO.getEffectiveDate());
		sv.acctmonth.set(bsscIO.getAcctMonth());
		sv.acctyear.set(bsscIO.getAcctYear());
		sv.jobq.set(bscdIO.getJobq());
		sv.bcompany.set(bppdIO.getCompany());
		sv.bbranch.set(bsscIO.getInitBranch());
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		/*SCREEN-IO*/
		wsspcomn.edterror.set(varcom.oK);
		/*VALIDATE*/
		/*CHECK-FOR-ERRORS*/
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		/*EXIT*/
	}

protected void update3000()
	{
		/*LOAD-FIELDS*/
		moveParameters3100();
		writeParamRecord3200();
		/*EXIT*/
	}

protected void moveParameters3100()
	{
		/*MOVE-TO-PARM-RECORD*/
		pj517par.acctmnth.set(sv.acctmnth);
		pj517par.acctyr.set(sv.acctyr);
		/*EXIT*/
	}

protected void writeParamRecord3200()
	{
		writBupa3210();
	}

protected void writBupa3210()
	{
		bupaIO.setParams(SPACES);
		bupaIO.setScheduleName(sv.scheduleName);
		bupaIO.setScheduleNumber(sv.scheduleNumber);
		bupaIO.setEffectiveDate(sv.effdate);
		bupaIO.setAcctMonth(sv.acctmonth);
		bupaIO.setAcctYear(sv.acctyear);
		bupaIO.setCompany(sv.bcompany);
		bupaIO.setBranch(sv.bbranch);
		bupaIO.setParmarea(pj517par.parmRecord);
		bupaIO.setParmPromptProg(bppdIO.getParmPromptProg());
		bupaIO.setFormat(buparec);
		bupaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, bupaIO);
		if (isNE(bupaIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(bupaIO.getParams());
			fatalError600();
		}
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
