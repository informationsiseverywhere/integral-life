/*
 * File: B6522.java
 * Date: 29 August 2009 21:22:02
 * Author: Quipoz Limited
 *
 * Class transformed from B6522.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.life.statistics.dataaccess.SttrTableDAM;
import com.csc.life.statistics.reports.R6522Report;
import com.csc.smart.dataaccess.BakyTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Bachmain;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
*
*           AGENT STATISTICAL MOVEMENT POSTING AUDIT REPORT
*
*
*   This program prints Agent Statistical Movement Posting records
*   that are extracted by the batch extraction program B0236.
*
*   The basic procedure division logic is for reading via SQL and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   Initialise
*
*     - retrieve and set up standard report headings.
*
*     - The selected STTR records will be arranged in the following
*       sequence (major to minor) through an SQL SELECT
*       statement:
*
*                        From STTR file
*                        --------------
*
*      Batch key:        BATCCOY
*                        BATCBRN
*                        BATCACTYR
*                        BATCACTMN
*                        BATCTRCDE
*                        BATCBATCH
*
*      Currency:         CNTCURR
*
*      Contract number:  CHDRNUM
*
*                        CHDRCOY
*                        ARACDE
*                        CNTBRANCH
*                        BANDAGE
*                        BANDSA
*                        BANDPRM
*                        BANDTRM
*                        CNTTYPE
*                        CRTABLE
*                        OVRDCAT
*                        PSTATCODE
*
*
*    Write details to report while not primary file EOF (BAKY)
*     - fetch a record from SQL-POSTPF1 for output details
*
*     - if new page, write page headings (R6522-H01)
*     - batch key and contract number are group indicate items
*       that is, print detail line 1 (R6522-D01) only if
*       batch-key and contract number changes from the previous
*       record.
*
*       WSAA-BATCKEY contains the current record batch details - STTR
*       WSAA-CHDRNUM contains the current contract number      - STTR
*
*
*       WSAA-PREV-BATCKEY contains the previous record's batch
*                         information.
*
*       WSAA-PREV-CHDRNUM contains the previous record's contract
*                          number information.
*
*     - print detail line 2 for every STTR record selected (R6522-D02).
*
*     - print sub-total line for R6522-D03 if the batch details and
*       contract number changes.
*
*     - read next primary file record
*
*
*   Control totals:
*     01  -  Number of pages printed
*     02  -  Number of STTRs extracted
*     03  -  Total no. of contract count
*     04  -  Total no. of cover count
*     05  -  Total amount of annual premium
*     06  -  Total amount of single premium
*     07  -  Total amount of commission
*
*
*
*****************************************************************
* </pre>
*/
public class B6522 extends Bachmain {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlpostpf1rs = null;
	private java.sql.PreparedStatement sqlpostpf1ps = null;
	private java.sql.Connection sqlpostpf1conn = null;
	private String sqlpostpf1 = "";
	private R6522Report printerFile = new R6522Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(127);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("B6522");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaStatagtYes = new FixedLengthStringData(1).init("Y");
	private String wsaaFirstTime = "";
	private ZonedDecimalData wsaaTotccount = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaTotvcount = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaTotaprem = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaTotsprem = new ZonedDecimalData(18, 2);
	private ZonedDecimalData wsaaTotcommis = new ZonedDecimalData(18, 2);

	private FixedLengthStringData wsaaAcctperiod = new FixedLengthStringData(7);
	private ZonedDecimalData wsaaPerMm = new ZonedDecimalData(2, 0).isAPartOf(wsaaAcctperiod, 0).setUnsigned();
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaAcctperiod, 2, FILLER).init("/");
	private ZonedDecimalData wsaaPerYy = new ZonedDecimalData(4, 0).isAPartOf(wsaaAcctperiod, 3).setUnsigned();
	private FixedLengthStringData wsaaParmCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaParmJobname = new FixedLengthStringData(8);
	private PackedDecimalData wsaaParmAcctyear = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaParmAcctmonth = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaParmJobnum = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaRunparm1 = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaParmCp = new FixedLengthStringData(5).isAPartOf(wsaaRunparm1, 0);
	private FixedLengthStringData wsaaParmJs = new FixedLengthStringData(3).isAPartOf(wsaaRunparm1, 5);
	private FixedLengthStringData wsaaPrevCurr = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaPrevBatckey = new FixedLengthStringData(18);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaBatckey = new FixedLengthStringData(18);
	private FixedLengthStringData wsaaBatccoy = new FixedLengthStringData(1).isAPartOf(wsaaBatckey, 0);
	private FixedLengthStringData wsaaBatcbrn = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 1);
	private FixedLengthStringData wsaaBatcactyr = new FixedLengthStringData(4).isAPartOf(wsaaBatckey, 3);
	private FixedLengthStringData wsaaBatcactmn = new FixedLengthStringData(2).isAPartOf(wsaaBatckey, 7);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaBatckey, 9);
	private FixedLengthStringData wsaaBatcbatch = new FixedLengthStringData(5).isAPartOf(wsaaBatckey, 13);

		/* FORMATS */
	private static final String descrec = "DESCREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t3629 = "T3629";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(9, 0);

	private FixedLengthStringData filler3 = new FixedLengthStringData(9).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(4).isAPartOf(filler3, 5);

		/* SQLA-POSTPF1 */

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

		/*  Detail line - add as many detail and total lines as required.
		              - use redefines to save WS space where applicable.*/

	private FixedLengthStringData r6522D01 = new FixedLengthStringData(30);
	private FixedLengthStringData r6522d01O = new FixedLengthStringData(30).isAPartOf(r6522D01, 0);
	private FixedLengthStringData rd01Batchkey = new FixedLengthStringData(22).isAPartOf(r6522d01O, 0);
	private FixedLengthStringData rd01Chdrnum = new FixedLengthStringData(8).isAPartOf(r6522d01O, 22);

	private FixedLengthStringData r6522D02 = new FixedLengthStringData(99);
	private FixedLengthStringData r6522d02O = new FixedLengthStringData(99).isAPartOf(r6522D02, 0);
	private FixedLengthStringData rd02Accumkey = new FixedLengthStringData(41).isAPartOf(r6522d02O, 0);
	private ZonedDecimalData rd02Contractc = new ZonedDecimalData(2, 0).isAPartOf(r6522d02O, 41);
	private ZonedDecimalData rd02Coverc = new ZonedDecimalData(2, 0).isAPartOf(r6522d02O, 43);
	private ZonedDecimalData rd02Stpmth = new ZonedDecimalData(18, 2).isAPartOf(r6522d02O, 45);
	private ZonedDecimalData rd02Stsmth = new ZonedDecimalData(18, 2).isAPartOf(r6522d02O, 63);
	private ZonedDecimalData rd02Stmmth = new ZonedDecimalData(18, 2).isAPartOf(r6522d02O, 81);

	private FixedLengthStringData r6522D03 = new FixedLengthStringData(72);
	private FixedLengthStringData r6522d03O = new FixedLengthStringData(72).isAPartOf(r6522D03, 0);
	private ZonedDecimalData rd03Totccount = new ZonedDecimalData(9, 0).isAPartOf(r6522d03O, 0);
	private ZonedDecimalData rd03Totvcount = new ZonedDecimalData(9, 0).isAPartOf(r6522d03O, 9);
	private ZonedDecimalData rd03Totaprem = new ZonedDecimalData(18, 2).isAPartOf(r6522d03O, 18);
	private ZonedDecimalData rd03Totsprem = new ZonedDecimalData(18, 2).isAPartOf(r6522d03O, 36);
	private ZonedDecimalData rd03Totcommis = new ZonedDecimalData(18, 2).isAPartOf(r6522d03O, 54);
		/*Logical File: Extracted Batch Keys*/
	private BakyTableDAM bakyIO = new BakyTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Agent Statistics Movement Logical File*/
	private SttrTableDAM sttrIO = new SttrTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private R6522H01Inner r6522H01Inner = new R6522H01Inner();
	private SqlaPostpf1Inner sqlaPostpf1Inner = new SqlaPostpf1Inner();
	private WsaaAccumulationKeyInner wsaaAccumulationKeyInner = new WsaaAccumulationKeyInner();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		exit190,
		eof580,
		exit590
	}

	public B6522() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

public void mainline(Object... parmArray)
	{
		runparmrec.runparmRec = convertAndSetParam(runparmrec.runparmRec, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void mainline100()
	{
		try {
			main110();
		}
		catch (GOTOException e){
		}
	}
protected void main110()
	{
		initialise200();
		readFirstRecord300();
		while ( !(endOfFile.isTrue())) {
			printReport400();
		}

		finished900();
		goTo(GotoLabel.exit190);
	}
protected void sqlError180()
	{
		sqlError();
	}

protected void initialise200()
	{
		initialise210();
		setUpHeadingCompany210();
		setUpHeadingBranch220();
		setUpHeadingDates240();
	}

protected void initialise210()
	{
		wsaaBatckey.set(SPACES);
		wsaaAccumulationKeyInner.wsaaAccumulationKey.set(SPACES);
		wsaaPrevCurr.set(SPACES);
		wsaaPrevBatckey.set(SPACES);
		wsaaPrevChdrnum.set(SPACES);
		wsaaFirstTime = "Y";
		wsaaParmCompany.set(runparmrec.company);
		wsaaParmJobname.set(runparmrec.jobname);
		wsaaParmAcctyear.set(runparmrec.acctyear);
		wsaaParmAcctmonth.set(runparmrec.acctmonth);
		wsaaParmJobnum.set(runparmrec.jobnum);
		wsaaRunparm1.set(runparmrec.runparm1);
		wsaaTotccount.set(ZERO);
		wsaaTotvcount.set(ZERO);
		wsaaTotaprem.set(ZERO);
		wsaaTotsprem.set(ZERO);
		wsaaTotcommis.set(ZERO);
		printerFile.openOutput();
	}

protected void setUpHeadingCompany210()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(runparmrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		r6522H01Inner.rh01Company.set(runparmrec.company);
		r6522H01Inner.rh01Companynm.set(descIO.getLongdesc());
	}

protected void setUpHeadingBranch220()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(runparmrec.branch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		r6522H01Inner.rh01Branch.set(runparmrec.branch);
		r6522H01Inner.rh01Branchnm.set(descIO.getLongdesc());
	}

protected void setUpHeadingDates240()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		r6522H01Inner.rh01Sdate.set(datcon1rec.extDate);
		/*SET-UP-JOB-DETAILS*/
		wsaaPerMm.set(runparmrec.acctmonth);
		wsaaPerYy.set(runparmrec.acctyear);
		r6522H01Inner.rh01Stperiod.set(wsaaAcctperiod);
		r6522H01Inner.rh01Jobname.set(runparmrec.jobname);
		r6522H01Inner.rh01Jobnum.set(runparmrec.jobnum);
	}

protected void readFirstRecord300()
	{
		/*BEGIN-READING*/
		sqlpostpf1 = " SELECT  ST.BATCCOY, ST.BATCBRN, BK.BATCACTYR, ST.BATCACTMN, ST.BATCTRCDE, ST.BATCBATCH, ST.CHDRCOY, ST.AGNTNUM, ST.STATCAT, ST.BATCACTYR, ST.ARACDE, ST.CNTBRANCH, ST.BANDAGE, ST.BANDSA, ST.BANDPRM, ST.BANDTRM, ST.CNTTYPE, ST.CRTABLE, ST.OVRDCAT, ST.PSTATCODE, ST.CNTCURR, ST.CHDRNUM, ST.TRDT, ST.STCMTH, ST.STVMTH, ST.STPMTH, ST.STSMTH, ST.STMMTH" +
" FROM   " + getAppVars().getTableNameOverriden("BAKYPF") + "  BK,  " + getAppVars().getTableNameOverriden("STTRPF") + "  ST" +
" WHERE BK.JCTLJBN = ?" +
" AND BK.JCTLCOY = ?" +
" AND BK.JCTLJN = ?" +
" AND BK.JCTLACTYR = ?" +
" AND BK.JCTLACTMN = ?" +
" AND BK.BATCCOY = ST.BATCCOY" +
" AND BK.BATCBRN = ST.BATCBRN" +
" AND BK.BATCACTYR = ST.BATCACTYR" +
" AND BK.BATCACTMN = ST.BATCACTMN" +
" AND BK.BATCTRCDE = ST.BATCTRCDE" +
" AND BK.BATCBATCH = ST.BATCBATCH" +
" AND BK.JOBSTEP = ?" +
" AND ST.STATAGT = ?" +
" ORDER BY ST.BATCCOY, ST.BATCBRN, ST.BATCACTYR, ST.BATCACTMN, ST.BATCTRCDE, ST.BATCBATCH, ST.CNTCURR, ST.CHDRNUM, ST.CHDRCOY, ST.ARACDE, ST.CNTBRANCH, ST.BANDAGE, ST.BANDSA, ST.BANDPRM, ST.BANDTRM, ST.CNTTYPE, ST.CRTABLE, ST.OVRDCAT, ST.PSTATCODE";
		sqlerrorflag = false;
		try {
			sqlpostpf1conn = getAppVars().getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.smart.dataaccess.BakypfTableDAM(), new com.csc.life.statistics.dataaccess.SttrpfTableDAM()});
			sqlpostpf1ps = getAppVars().prepareStatementEmbeded(sqlpostpf1conn, sqlpostpf1);
			getAppVars().setDBNumber(sqlpostpf1ps, 1, wsaaParmJobnum);
			getAppVars().setDBString(sqlpostpf1ps, 2, wsaaParmCompany);
			getAppVars().setDBString(sqlpostpf1ps, 3, wsaaParmJobname);
			getAppVars().setDBNumber(sqlpostpf1ps, 4, wsaaParmAcctyear);
			getAppVars().setDBNumber(sqlpostpf1ps, 5, wsaaParmAcctmonth);
			getAppVars().setDBString(sqlpostpf1ps, 6, wsaaParmJs);
			getAppVars().setDBString(sqlpostpf1ps, 7, wsaaStatagtYes);
			sqlpostpf1rs = getAppVars().executeQuery(sqlpostpf1ps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError180();
			throw new RuntimeException("B6522: ERROR (1) - Unexpected return from a transformed GO TO statement: 180-SQL-ERROR");
		}
		fetchPrimary500();
		/*EXIT*/
	}

protected void printReport400()
	{
		writeDetail450();
		updateTotalAmounts460();
	}

protected void writeDetail450()
	{
		if (isNE(sqlaPostpf1Inner.sqlCntcurr, wsaaPrevCurr)) {
			wsaaPrevCurr.set(sqlaPostpf1Inner.sqlCntcurr);
			setUpHeadingCurrency600();
			wsaaOverflow.set("Y");
		}
		checkPageHeading800();
		wsaaBatccoy.set(sqlaPostpf1Inner.sqlBatccoy);
		wsaaBatcbrn.set(sqlaPostpf1Inner.sqlBatcbrn);
		wsaaBatcactyr.set(sqlaPostpf1Inner.sqlBatcactyr);
		wsaaBatcactmn.set(sqlaPostpf1Inner.sqlBatcactmn);
		wsaaBatctrcde.set(sqlaPostpf1Inner.sqlBatctrcde);
		wsaaBatcbatch.set(sqlaPostpf1Inner.sqlBatcbatch);
		if (isNE(wsaaBatckey,wsaaPrevBatckey)
		|| isNE(sqlaPostpf1Inner.sqlChdrnum, wsaaPrevChdrnum)) {
			wsaaPrevBatckey.set(wsaaBatckey);
			wsaaPrevChdrnum.set(sqlaPostpf1Inner.sqlChdrnum);
			printGrandTotals700();
			rd01Batchkey.set(wsaaBatckey);
			rd01Chdrnum.set(sqlaPostpf1Inner.sqlChdrnum);
			printerFile.printR6522d01(r6522D01, indicArea);
		}
		checkPageHeading800();
		/* print detail line 2:*/
		wsaaAccumulationKeyInner.wsaaChdrcoy.set(sqlaPostpf1Inner.sqlChdrcoy);
		wsaaAccumulationKeyInner.wsaaAgntnum.set(sqlaPostpf1Inner.sqlAgntnum);
		wsaaAccumulationKeyInner.wsaaStatcat.set(sqlaPostpf1Inner.sqlStatcat);
		wsaaAccumulationKeyInner.wsaaAcctyr.set(sqlaPostpf1Inner.sqlAcctyr);
		wsaaAccumulationKeyInner.wsaaAracde.set(sqlaPostpf1Inner.sqlAracde);
		wsaaAccumulationKeyInner.wsaaCntbranch.set(sqlaPostpf1Inner.sqlCntbranch);
		wsaaAccumulationKeyInner.wsaaBandage.set(sqlaPostpf1Inner.sqlBandage);
		wsaaAccumulationKeyInner.wsaaBandsa.set(sqlaPostpf1Inner.sqlBandsa);
		wsaaAccumulationKeyInner.wsaaBandprm.set(sqlaPostpf1Inner.sqlBandprm);
		wsaaAccumulationKeyInner.wsaaBandtrm.set(sqlaPostpf1Inner.sqlBandtrm);
		wsaaAccumulationKeyInner.wsaaCnttype.set(sqlaPostpf1Inner.sqlCnttype);
		wsaaAccumulationKeyInner.wsaaCrtable.set(sqlaPostpf1Inner.sqlCrtable);
		wsaaAccumulationKeyInner.wsaaPstatcode.set(sqlaPostpf1Inner.sqlPstatcode);
		wsaaAccumulationKeyInner.wsaaCntcurr.set(sqlaPostpf1Inner.sqlCntcurr);
		wsaaAccumulationKeyInner.wsaaOvrdcat.set(sqlaPostpf1Inner.sqlOvrdcat);
		rd02Accumkey.set(wsaaAccumulationKeyInner.wsaaAccumulationKey);
		rd02Contractc.set(sqlaPostpf1Inner.sqlStcmth);
		rd02Coverc.set(sqlaPostpf1Inner.sqlStvmth);
		rd02Stpmth.set(sqlaPostpf1Inner.sqlStpmth);
		rd02Stsmth.set(sqlaPostpf1Inner.sqlStsmth);
		rd02Stmmth.set(sqlaPostpf1Inner.sqlStmmth);
		printerFile.printR6522d02(r6522D02, indicArea);
		checkPageHeading800();
	}

protected void updateTotalAmounts460()
	{
		wsaaTotccount.add(sqlaPostpf1Inner.sqlStcmth);
		wsaaTotvcount.add(sqlaPostpf1Inner.sqlStvmth);
		wsaaTotaprem.add(sqlaPostpf1Inner.sqlStpmth);
		wsaaTotsprem.add(sqlaPostpf1Inner.sqlStsmth);
		wsaaTotcommis.add(sqlaPostpf1Inner.sqlStmmth);
		/*READ-NEXT-RECORD*/
		fetchPrimary500();
		if (endOfFile.isTrue()) {
			printGrandTotals700();
		}
		/*EXIT*/
	}

protected void fetchPrimary500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					fetchRecord510();
					updateControls520();
				case eof580:
					eof580();
				case exit590:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void fetchRecord510()
	{
		sqlerrorflag = false;
		try {
			if (getAppVars().fetchNext(sqlpostpf1rs)) {
				getAppVars().getDBObject(sqlpostpf1rs, 1, sqlaPostpf1Inner.sqlBatccoy);
				getAppVars().getDBObject(sqlpostpf1rs, 2, sqlaPostpf1Inner.sqlBatcbrn);
				getAppVars().getDBObject(sqlpostpf1rs, 3, sqlaPostpf1Inner.sqlBatcactyr);
				getAppVars().getDBObject(sqlpostpf1rs, 4, sqlaPostpf1Inner.sqlBatcactmn);
				getAppVars().getDBObject(sqlpostpf1rs, 5, sqlaPostpf1Inner.sqlBatctrcde);
				getAppVars().getDBObject(sqlpostpf1rs, 6, sqlaPostpf1Inner.sqlBatcbatch);
				getAppVars().getDBObject(sqlpostpf1rs, 7, sqlaPostpf1Inner.sqlChdrcoy);
				getAppVars().getDBObject(sqlpostpf1rs, 8, sqlaPostpf1Inner.sqlAgntnum);
				getAppVars().getDBObject(sqlpostpf1rs, 9, sqlaPostpf1Inner.sqlStatcat);
				getAppVars().getDBObject(sqlpostpf1rs, 10, sqlaPostpf1Inner.sqlAcctyr);
				getAppVars().getDBObject(sqlpostpf1rs, 11, sqlaPostpf1Inner.sqlAracde);
				getAppVars().getDBObject(sqlpostpf1rs, 12, sqlaPostpf1Inner.sqlCntbranch);
				getAppVars().getDBObject(sqlpostpf1rs, 13, sqlaPostpf1Inner.sqlBandage);
				getAppVars().getDBObject(sqlpostpf1rs, 14, sqlaPostpf1Inner.sqlBandsa);
				getAppVars().getDBObject(sqlpostpf1rs, 15, sqlaPostpf1Inner.sqlBandprm);
				getAppVars().getDBObject(sqlpostpf1rs, 16, sqlaPostpf1Inner.sqlBandtrm);
				getAppVars().getDBObject(sqlpostpf1rs, 17, sqlaPostpf1Inner.sqlCnttype);
				getAppVars().getDBObject(sqlpostpf1rs, 18, sqlaPostpf1Inner.sqlCrtable);
				getAppVars().getDBObject(sqlpostpf1rs, 19, sqlaPostpf1Inner.sqlOvrdcat);
				getAppVars().getDBObject(sqlpostpf1rs, 20, sqlaPostpf1Inner.sqlPstatcode);
				getAppVars().getDBObject(sqlpostpf1rs, 21, sqlaPostpf1Inner.sqlCntcurr);
				getAppVars().getDBObject(sqlpostpf1rs, 22, sqlaPostpf1Inner.sqlChdrnum);
				getAppVars().getDBObject(sqlpostpf1rs, 23, sqlaPostpf1Inner.sqlTrdt);
				getAppVars().getDBObject(sqlpostpf1rs, 24, sqlaPostpf1Inner.sqlStcmth);
				getAppVars().getDBObject(sqlpostpf1rs, 25, sqlaPostpf1Inner.sqlStvmth);
				getAppVars().getDBObject(sqlpostpf1rs, 26, sqlaPostpf1Inner.sqlStpmth);
				getAppVars().getDBObject(sqlpostpf1rs, 27, sqlaPostpf1Inner.sqlStsmth);
				getAppVars().getDBObject(sqlpostpf1rs, 28, sqlaPostpf1Inner.sqlStmmth);
			}
			else {
				goTo(GotoLabel.eof580);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError180();
			throw new RuntimeException("B6522: ERROR (2) - Unexpected return from a transformed GO TO statement: 180-SQL-ERROR");
		}
	}

protected void updateControls520()
	{
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStcmth);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStvmth);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStpmth);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStsmth);
		callContot001();
		contotrec.totno.set(ct07);
		contotrec.totval.set(sqlaPostpf1Inner.sqlStmmth);
		callContot001();
		goTo(GotoLabel.exit590);
	}

protected void eof580()
	{
		wsaaEof.set("Y");
	}

protected void setUpHeadingCurrency600()
	{
		setUpHeadingCurrency610();
	}

protected void setUpHeadingCurrency610()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(runparmrec.company);
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sqlaPostpf1Inner.sqlCntcurr);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(runparmrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			conerrrec.params.set(descIO.getParams());
			databaseError006();
		}
		r6522H01Inner.rh01Currcode.set(sqlaPostpf1Inner.sqlCntcurr);
		r6522H01Inner.rh01Currencynm.set(descIO.getLongdesc());
	}

protected void printGrandTotals700()
	{
			para710();
	}

protected void para710()
	{
		if (isEQ(wsaaFirstTime,"Y")) {
			wsaaFirstTime = "N";
			return ;
		}
		rd03Totccount.set(wsaaTotccount);
		rd03Totvcount.set(wsaaTotvcount);
		rd03Totaprem.set(wsaaTotaprem);
		rd03Totsprem.set(wsaaTotsprem);
		rd03Totcommis.set(wsaaTotcommis);
		printerFile.printR6522d03(r6522D03, indicArea);
		checkPageHeading800();
		wsaaTotccount.set(ZERO);
		wsaaTotvcount.set(ZERO);
		wsaaTotaprem.set(ZERO);
		wsaaTotsprem.set(ZERO);
		wsaaTotcommis.set(ZERO);
	}

protected void checkPageHeading800()
	{
		/*PARA*/

		if (endOfFile.isTrue()) {
			return ;
		}
		if (newPageReq.isTrue()) {
			contotrec.totno.set(ct01);
			contotrec.totval.set(1);
			callContot001();
			printerFile.printR6522h01(r6522H01Inner.r6522H01, indicArea);
			wsaaOverflow.set("N");
		}
	}

protected void finished900()
	{
		/*CLOSE-FILES*/
		getAppVars().freeDBConnectionIgnoreErr(sqlpostpf1conn, sqlpostpf1ps, sqlpostpf1rs);
		printerFile.close();
		/*EXIT*/
	}

protected void sqlError()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		conerrrec.syserror.set(sqlStatuz);
		systemError005();
		/*EXIT-SQL-ERROR*/
	}
private static final class WsaaAccumulationKeyInner {
	private FixedLengthStringData wsaaAccumulationKey = new FixedLengthStringData(41);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaAccumulationKey, 0);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaAccumulationKey, 1);
	private FixedLengthStringData wsaaStatcat = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 9);
	private FixedLengthStringData wsaaAracde = new FixedLengthStringData(3).isAPartOf(wsaaAccumulationKey, 11);
	private FixedLengthStringData wsaaCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 14);
	private FixedLengthStringData wsaaBandage = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 16);
	private FixedLengthStringData wsaaBandsa = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 18);
	private FixedLengthStringData wsaaBandprm = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 20);
	private FixedLengthStringData wsaaBandtrm = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 22);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaAccumulationKey, 24);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaAccumulationKey, 27);
	private FixedLengthStringData wsaaOvrdcat = new FixedLengthStringData(1).isAPartOf(wsaaAccumulationKey, 31);
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2).isAPartOf(wsaaAccumulationKey, 32);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaAccumulationKey, 34);
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaAccumulationKey, 37).setUnsigned();
}
private static final class SqlaPostpf1Inner {
	private FixedLengthStringData sqlPostrec = new FixedLengthStringData(109);
	private FixedLengthStringData sqlBatccoy = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 0);
	private FixedLengthStringData sqlBatcbrn = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 1);
	private PackedDecimalData sqlBatcactyr = new PackedDecimalData(4, 0).isAPartOf(sqlPostrec, 3);
	private PackedDecimalData sqlBatcactmn = new PackedDecimalData(2, 0).isAPartOf(sqlPostrec, 6);
	private FixedLengthStringData sqlBatctrcde = new FixedLengthStringData(4).isAPartOf(sqlPostrec, 8);
	private FixedLengthStringData sqlBatcbatch = new FixedLengthStringData(5).isAPartOf(sqlPostrec, 12);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 17);
	private FixedLengthStringData sqlAgntnum = new FixedLengthStringData(8).isAPartOf(sqlPostrec, 18);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 26);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlPostrec, 28);
	private FixedLengthStringData sqlAracde = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 31);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 34);
	private FixedLengthStringData sqlBandage = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 36);
	private FixedLengthStringData sqlBandsa = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 38);
	private FixedLengthStringData sqlBandprm = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 40);
	private FixedLengthStringData sqlBandtrm = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 42);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 44);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlPostrec, 47);
	private FixedLengthStringData sqlOvrdcat = new FixedLengthStringData(1).isAPartOf(sqlPostrec, 51);
	private FixedLengthStringData sqlPstatcode = new FixedLengthStringData(2).isAPartOf(sqlPostrec, 52);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlPostrec, 54);
	private FixedLengthStringData sqlChdrnum = new FixedLengthStringData(8).isAPartOf(sqlPostrec, 57);
	private PackedDecimalData sqlTrdt = new PackedDecimalData(6, 0).isAPartOf(sqlPostrec, 65);
	private PackedDecimalData sqlStcmth = new PackedDecimalData(9, 0).isAPartOf(sqlPostrec, 69);
	private PackedDecimalData sqlStvmth = new PackedDecimalData(9, 0).isAPartOf(sqlPostrec, 74);
	private PackedDecimalData sqlStpmth = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 79);
	private PackedDecimalData sqlStsmth = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 89);
	private PackedDecimalData sqlStmmth = new PackedDecimalData(18, 2).isAPartOf(sqlPostrec, 99);
}
private static final class R6522H01Inner {
	private FixedLengthStringData r6522H01 = new FixedLengthStringData(129);
	private FixedLengthStringData r6522h01O = new FixedLengthStringData(129).isAPartOf(r6522H01, 0);
	private FixedLengthStringData rh01Stperiod = new FixedLengthStringData(7).isAPartOf(r6522h01O, 0);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(r6522h01O, 7);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(r6522h01O, 8);
	private FixedLengthStringData rh01Jobname = new FixedLengthStringData(8).isAPartOf(r6522h01O, 38);
	private ZonedDecimalData rh01Jobnum = new ZonedDecimalData(8, 0).isAPartOf(r6522h01O, 46);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(r6522h01O, 54);
	private FixedLengthStringData rh01Branch = new FixedLengthStringData(2).isAPartOf(r6522h01O, 64);
	private FixedLengthStringData rh01Branchnm = new FixedLengthStringData(30).isAPartOf(r6522h01O, 66);
	private FixedLengthStringData rh01Currcode = new FixedLengthStringData(3).isAPartOf(r6522h01O, 96);
	private FixedLengthStringData rh01Currencynm = new FixedLengthStringData(30).isAPartOf(r6522h01O, 99);
}
}
