package com.csc.life.statistics.dataaccess;

import com.csc.life.newbusiness.dataaccess.AgcmpfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgcmstsTableDAM.java
 * Date: Sun, 30 Aug 2009 03:28:26
 * Class transformed from AGCMSTS.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgcmstsTableDAM extends AgcmpfTableDAM {

	public AgcmstsTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("AGCMSTS");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX"
		             + ", AGNTNUM";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "TRANNO, " +
		            "AGNTNUM, " +
		            "EFDATE, " +
		            "ANNPREM, " +
		            "BASCMETH, " +
		            "INITCOM, " +
		            "BASCPY, " +
		            "COMPAY, " +
		            "COMERN, " +
		            "SRVCPY, " +
		            "SCMDUE, " +
		            "SCMEARN, " +
		            "RNWCPY, " +
		            "RNLCDUE, " +
		            "RNLCEARN, " +
		            "AGCLS, " +
		            "TERMID, " +
		            "TRDT, " +
		            "TRTM, " +
		            "USER_T, " +
		            "VALIDFLAG, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "PTDATE, " +
		            "SEQNO, " +
		            "CEDAGENT, " +
		            "OVRDCAT, " +
		            "DORMFLAG, " +
		            "USRPRF, " +
		            "JOBNM, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
		            "AGNTNUM ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
		            "AGNTNUM DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               tranno,
                               agntnum,
                               efdate,
                               annprem,
                               basicCommMeth,
                               initcom,
                               bascpy,
                               compay,
                               comern,
                               srvcpy,
                               scmdue,
                               scmearn,
                               rnwcpy,
                               rnlcdue,
                               rnlcearn,
                               agentClass,
                               termid,
                               transactionDate,
                               transactionTime,
                               user,
                               validflag,
                               currfrom,
                               currto,
                               ptdate,
                               seqno,
                               cedagent,
                               ovrdcat,
                               dormantFlag,
                               userProfile,
                               jobName,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(38);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ getAgntnum().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, agntnum);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller1 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller2 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller3 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller4 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller5 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller6 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller8 = new FixedLengthStringData(8);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller1.setInternal(chdrcoy.toInternal());
	nonKeyFiller2.setInternal(chdrnum.toInternal());
	nonKeyFiller3.setInternal(life.toInternal());
	nonKeyFiller4.setInternal(coverage.toInternal());
	nonKeyFiller5.setInternal(rider.toInternal());
	nonKeyFiller6.setInternal(planSuffix.toInternal());
	nonKeyFiller8.setInternal(agntnum.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(216);
		
		nonKeyData.set(
					nonKeyFiller1.toInternal()
					+ nonKeyFiller2.toInternal()
					+ nonKeyFiller3.toInternal()
					+ nonKeyFiller4.toInternal()
					+ nonKeyFiller5.toInternal()
					+ nonKeyFiller6.toInternal()
					+ getTranno().toInternal()
					+ nonKeyFiller8.toInternal()
					+ getEfdate().toInternal()
					+ getAnnprem().toInternal()
					+ getBasicCommMeth().toInternal()
					+ getInitcom().toInternal()
					+ getBascpy().toInternal()
					+ getCompay().toInternal()
					+ getComern().toInternal()
					+ getSrvcpy().toInternal()
					+ getScmdue().toInternal()
					+ getScmearn().toInternal()
					+ getRnwcpy().toInternal()
					+ getRnlcdue().toInternal()
					+ getRnlcearn().toInternal()
					+ getAgentClass().toInternal()
					+ getTermid().toInternal()
					+ getTransactionDate().toInternal()
					+ getTransactionTime().toInternal()
					+ getUser().toInternal()
					+ getValidflag().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getPtdate().toInternal()
					+ getSeqno().toInternal()
					+ getCedagent().toInternal()
					+ getOvrdcat().toInternal()
					+ getDormantFlag().toInternal()
					+ getUserProfile().toInternal()
					+ getJobName().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller1);
			what = ExternalData.chop(what, nonKeyFiller2);
			what = ExternalData.chop(what, nonKeyFiller3);
			what = ExternalData.chop(what, nonKeyFiller4);
			what = ExternalData.chop(what, nonKeyFiller5);
			what = ExternalData.chop(what, nonKeyFiller6);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, nonKeyFiller8);
			what = ExternalData.chop(what, efdate);
			what = ExternalData.chop(what, annprem);
			what = ExternalData.chop(what, basicCommMeth);
			what = ExternalData.chop(what, initcom);
			what = ExternalData.chop(what, bascpy);
			what = ExternalData.chop(what, compay);
			what = ExternalData.chop(what, comern);
			what = ExternalData.chop(what, srvcpy);
			what = ExternalData.chop(what, scmdue);
			what = ExternalData.chop(what, scmearn);
			what = ExternalData.chop(what, rnwcpy);
			what = ExternalData.chop(what, rnlcdue);
			what = ExternalData.chop(what, rnlcearn);
			what = ExternalData.chop(what, agentClass);
			what = ExternalData.chop(what, termid);
			what = ExternalData.chop(what, transactionDate);
			what = ExternalData.chop(what, transactionTime);
			what = ExternalData.chop(what, user);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, ptdate);
			what = ExternalData.chop(what, seqno);
			what = ExternalData.chop(what, cedagent);
			what = ExternalData.chop(what, ovrdcat);
			what = ExternalData.chop(what, dormantFlag);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	public FixedLengthStringData getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(Object what) {
		agntnum.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public PackedDecimalData getEfdate() {
		return efdate;
	}
	public void setEfdate(Object what) {
		setEfdate(what, false);
	}
	public void setEfdate(Object what, boolean rounded) {
		if (rounded)
			efdate.setRounded(what);
		else
			efdate.set(what);
	}	
	public PackedDecimalData getAnnprem() {
		return annprem;
	}
	public void setAnnprem(Object what) {
		setAnnprem(what, false);
	}
	public void setAnnprem(Object what, boolean rounded) {
		if (rounded)
			annprem.setRounded(what);
		else
			annprem.set(what);
	}	
	public FixedLengthStringData getBasicCommMeth() {
		return basicCommMeth;
	}
	public void setBasicCommMeth(Object what) {
		basicCommMeth.set(what);
	}	
	public PackedDecimalData getInitcom() {
		return initcom;
	}
	public void setInitcom(Object what) {
		setInitcom(what, false);
	}
	public void setInitcom(Object what, boolean rounded) {
		if (rounded)
			initcom.setRounded(what);
		else
			initcom.set(what);
	}	
	public FixedLengthStringData getBascpy() {
		return bascpy;
	}
	public void setBascpy(Object what) {
		bascpy.set(what);
	}	
	public PackedDecimalData getCompay() {
		return compay;
	}
	public void setCompay(Object what) {
		setCompay(what, false);
	}
	public void setCompay(Object what, boolean rounded) {
		if (rounded)
			compay.setRounded(what);
		else
			compay.set(what);
	}	
	public PackedDecimalData getComern() {
		return comern;
	}
	public void setComern(Object what) {
		setComern(what, false);
	}
	public void setComern(Object what, boolean rounded) {
		if (rounded)
			comern.setRounded(what);
		else
			comern.set(what);
	}	
	public FixedLengthStringData getSrvcpy() {
		return srvcpy;
	}
	public void setSrvcpy(Object what) {
		srvcpy.set(what);
	}	
	public PackedDecimalData getScmdue() {
		return scmdue;
	}
	public void setScmdue(Object what) {
		setScmdue(what, false);
	}
	public void setScmdue(Object what, boolean rounded) {
		if (rounded)
			scmdue.setRounded(what);
		else
			scmdue.set(what);
	}	
	public PackedDecimalData getScmearn() {
		return scmearn;
	}
	public void setScmearn(Object what) {
		setScmearn(what, false);
	}
	public void setScmearn(Object what, boolean rounded) {
		if (rounded)
			scmearn.setRounded(what);
		else
			scmearn.set(what);
	}	
	public FixedLengthStringData getRnwcpy() {
		return rnwcpy;
	}
	public void setRnwcpy(Object what) {
		rnwcpy.set(what);
	}	
	public PackedDecimalData getRnlcdue() {
		return rnlcdue;
	}
	public void setRnlcdue(Object what) {
		setRnlcdue(what, false);
	}
	public void setRnlcdue(Object what, boolean rounded) {
		if (rounded)
			rnlcdue.setRounded(what);
		else
			rnlcdue.set(what);
	}	
	public PackedDecimalData getRnlcearn() {
		return rnlcearn;
	}
	public void setRnlcearn(Object what) {
		setRnlcearn(what, false);
	}
	public void setRnlcearn(Object what, boolean rounded) {
		if (rounded)
			rnlcearn.setRounded(what);
		else
			rnlcearn.set(what);
	}	
	public FixedLengthStringData getAgentClass() {
		return agentClass;
	}
	public void setAgentClass(Object what) {
		agentClass.set(what);
	}	
	public FixedLengthStringData getTermid() {
		return termid;
	}
	public void setTermid(Object what) {
		termid.set(what);
	}	
	public PackedDecimalData getTransactionDate() {
		return transactionDate;
	}
	public void setTransactionDate(Object what) {
		setTransactionDate(what, false);
	}
	public void setTransactionDate(Object what, boolean rounded) {
		if (rounded)
			transactionDate.setRounded(what);
		else
			transactionDate.set(what);
	}	
	public PackedDecimalData getTransactionTime() {
		return transactionTime;
	}
	public void setTransactionTime(Object what) {
		setTransactionTime(what, false);
	}
	public void setTransactionTime(Object what, boolean rounded) {
		if (rounded)
			transactionTime.setRounded(what);
		else
			transactionTime.set(what);
	}	
	public PackedDecimalData getUser() {
		return user;
	}
	public void setUser(Object what) {
		setUser(what, false);
	}
	public void setUser(Object what, boolean rounded) {
		if (rounded)
			user.setRounded(what);
		else
			user.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getPtdate() {
		return ptdate;
	}
	public void setPtdate(Object what) {
		setPtdate(what, false);
	}
	public void setPtdate(Object what, boolean rounded) {
		if (rounded)
			ptdate.setRounded(what);
		else
			ptdate.set(what);
	}	
	public PackedDecimalData getSeqno() {
		return seqno;
	}
	public void setSeqno(Object what) {
		setSeqno(what, false);
	}
	public void setSeqno(Object what, boolean rounded) {
		if (rounded)
			seqno.setRounded(what);
		else
			seqno.set(what);
	}	
	public FixedLengthStringData getCedagent() {
		return cedagent;
	}
	public void setCedagent(Object what) {
		cedagent.set(what);
	}	
	public FixedLengthStringData getOvrdcat() {
		return ovrdcat;
	}
	public void setOvrdcat(Object what) {
		ovrdcat.set(what);
	}	
	public FixedLengthStringData getDormantFlag() {
		return dormantFlag;
	}
	public void setDormantFlag(Object what) {
		dormantFlag.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		agntnum.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller1.clear();
		nonKeyFiller2.clear();
		nonKeyFiller3.clear();
		nonKeyFiller4.clear();
		nonKeyFiller5.clear();
		nonKeyFiller6.clear();
		tranno.clear();
		nonKeyFiller8.clear();
		efdate.clear();
		annprem.clear();
		basicCommMeth.clear();
		initcom.clear();
		bascpy.clear();
		compay.clear();
		comern.clear();
		srvcpy.clear();
		scmdue.clear();
		scmearn.clear();
		rnwcpy.clear();
		rnlcdue.clear();
		rnlcearn.clear();
		agentClass.clear();
		termid.clear();
		transactionDate.clear();
		transactionTime.clear();
		user.clear();
		validflag.clear();
		currfrom.clear();
		currto.clear();
		ptdate.clear();
		seqno.clear();
		cedagent.clear();
		ovrdcat.clear();
		dormantFlag.clear();
		userProfile.clear();
		jobName.clear();
		datime.clear();		
	}


}