package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:11:27
 * Description:
 * Copybook name: STTRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Sttrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData sttrFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData sttrKey = new FixedLengthStringData(64).isAPartOf(sttrFileKey, 0, REDEFINE);
  	public FixedLengthStringData sttrBatccoy = new FixedLengthStringData(1).isAPartOf(sttrKey, 0);
  	public FixedLengthStringData sttrBatcbrn = new FixedLengthStringData(2).isAPartOf(sttrKey, 1);
  	public PackedDecimalData sttrBatcactyr = new PackedDecimalData(4, 0).isAPartOf(sttrKey, 3);
  	public PackedDecimalData sttrBatcactmn = new PackedDecimalData(2, 0).isAPartOf(sttrKey, 6);
  	public FixedLengthStringData sttrBatctrcde = new FixedLengthStringData(4).isAPartOf(sttrKey, 8);
  	public FixedLengthStringData sttrBatcbatch = new FixedLengthStringData(5).isAPartOf(sttrKey, 12);
  	public FixedLengthStringData sttrCntcurr = new FixedLengthStringData(3).isAPartOf(sttrKey, 17);
  	public FixedLengthStringData filler = new FixedLengthStringData(44).isAPartOf(sttrKey, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(sttrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		sttrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}