package com.csc.life.statistics.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sj675screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sj675ScreenVars sv = (Sj675ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sj675screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sj675ScreenVars screenVars = (Sj675ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.statSect01.setClassString("");
		screenVars.statSect02.setClassString("");
		screenVars.statSect03.setClassString("");
		screenVars.statSect04.setClassString("");
		screenVars.statSect05.setClassString("");
		screenVars.statSect06.setClassString("");
		screenVars.statSect07.setClassString("");
		screenVars.statSect08.setClassString("");
		screenVars.statSect09.setClassString("");
		screenVars.statSect10.setClassString("");
	}

/**
 * Clear all the variables in Sj675screen
 */
	public static void clear(VarModel pv) {
		Sj675ScreenVars screenVars = (Sj675ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.statSect01.clear();
		screenVars.statSect02.clear();
		screenVars.statSect03.clear();
		screenVars.statSect04.clear();
		screenVars.statSect05.clear();
		screenVars.statSect06.clear();
		screenVars.statSect07.clear();
		screenVars.statSect08.clear();
		screenVars.statSect09.clear();
		screenVars.statSect10.clear();
	}
}
