package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:23
 * Description:
 * Copybook name: RACDSTBKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdstbkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdstbFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdstbKey = new FixedLengthStringData(64).isAPartOf(racdstbFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdstbChdrcoy = new FixedLengthStringData(1).isAPartOf(racdstbKey, 0);
  	public FixedLengthStringData racdstbChdrnum = new FixedLengthStringData(8).isAPartOf(racdstbKey, 1);
  	public FixedLengthStringData racdstbLife = new FixedLengthStringData(2).isAPartOf(racdstbKey, 9);
  	public FixedLengthStringData racdstbCoverage = new FixedLengthStringData(2).isAPartOf(racdstbKey, 11);
  	public FixedLengthStringData racdstbRider = new FixedLengthStringData(2).isAPartOf(racdstbKey, 13);
  	public PackedDecimalData racdstbPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdstbKey, 15);
  	public FixedLengthStringData racdstbRasnum = new FixedLengthStringData(8).isAPartOf(racdstbKey, 18);
  	public PackedDecimalData racdstbTranno = new PackedDecimalData(5, 0).isAPartOf(racdstbKey, 26);
  	public FixedLengthStringData filler = new FixedLengthStringData(35).isAPartOf(racdstbKey, 29, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdstbFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdstbFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}