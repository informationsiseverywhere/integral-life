/*
 * File: Lifsttr.java
 * Date: 29 August 2009 22:58:18
 * Author: Quipoz Limited
 * 
 * Class transformed from LIFSTTR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.csc.fsu.general.dataaccess.ChdrTableDAM;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.agents.dataaccess.AglfTableDAM;
import com.csc.life.flexiblepremium.dataaccess.dao.AgcmpfDAO;
import com.csc.life.flexiblepremium.dataaccess.model.Agcmpf;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.reassurance.dataaccess.dao.RacdpfDAO;
import com.csc.life.reassurance.dataaccess.model.Racdpf;
import com.csc.life.statistics.dataaccess.dao.SttrpfDAO;
import com.csc.life.statistics.dataaccess.model.Sttrpf;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.statistics.tablestructures.T6627rec;
import com.csc.life.statistics.tablestructures.T6628rec;
import com.csc.life.statistics.tablestructures.T6629rec;
import com.csc.life.statistics.tablestructures.Tj688rec;
import com.csc.life.terminationclaims.dataaccess.RegpTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.csc.smart400framework.utility.Datcon1Util;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*REMARKS.
*
* AGENT/GOVERNMENT STATISTICS TRANSACTION SUBROUTINE.
*
* This subroutine has two basic functions;
* a) To reverse Statistical movement records.
* b) To create  Statistical movement records.
*
* The STTR file will hold all the relevant details of
* selected transactions which affect a contract and have
* to have a statistical record written.
*
* Two new coverage logicals have been created for the
* Statistical subsystem. These are COVRSTS and COVRSTA.
* COVRSTS allows only validflag 1 records, and COVRSTA
* allows only validflag 2 records.
* Another logical AGCMSTS has been set up for the agent's
* commission records. This allows only validflag 1 records
* to be read.
* The other logical AGCMSTA set up for the old agent's
* commission records, allows only validflag 2 records,
* dormant flag 'Y' to be read.
*
* This subroutine will be included in various ATs
* which affect contracts/coverages. It is designed to
* track a policy through its life and report on any
* changes to it. The statistical records produced form
* the basis for the Statistical subsystem.
*
* The three tables used for Statistics are;
*     T6627, T6628, T6629.
* T6628 has as its item name the transaction code and
* the contract status of a contract, e.g. T642IF. This
* table is read first to see if any statistical (STTR)
* records are required to be written. If not the program
* will finish without any processing needed. If the table
* has valid statistical categories, then these are used
* to access T6629. An item name for T6629 could be 'ZZ'.
* On T6629 there are two question fields which determine
* if agent and/or government details need accumulating.
*
* The four fields under each question are used to access
* T6627. These fields refer to Age, Sum insured, Risk term
* and Premium. T6627 has items which are used to check the
* value of these fields, e.g. the Age of the policy holder
* is 35. On T6627, the Age item has several values on
* To and From fields. These could be 0 - 20, 21 - 40 etc.
* The Age will be found to be within a certain band, and
* the band label for that range, e.g. AB, will be moved to
* the STTR record. The same principle applies for the
* other T6629 fields.
*
* T6628 splits the statistical categories into four
* areas; PREVIOUS, CURRENT, INCREASE and DECREASE.
* All previous categories refer to COVRSTA records, and
* current categories refer to COVRSTS records. AGCMSTS
* records can refer to both. On the coverage logicals,
* the INCREASE and DECREASE are for the premium, and
* on the AGCMSTS, these refer to the commission.
*
* STTR records are written for each valid T6628 category.
* So for a current record there could be several current
* categories, therefore the corresponding number of STTRs
* will be written. Also, if the premium has increased or
* decreased, a number of STTRs will be written. This process
* will be repeated for all the AGCMSTS records attached to
* that current record.
*
* When a reversal of STTRs is required, the linkage field
* LIFS-TRANNOR has the transaction number, which the STTRs
* have to be reversed back to. The LIFS-TRANNO has the
* current tranno for a particular coverage. All STTR records
* are reversed out up to and including the TRANNOR number.
*
*****************************************************************
* </pre>
*/
public class Lifsttr extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("LIFSTTR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaBandage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaBandsa = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaBandprm = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaBandtrm = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaAgntage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaAgntsa = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaAgntprm = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaAgnttrm = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaGovtage = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaGovtsa = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaGovtprm = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaGovttrm = new FixedLengthStringData(2).init(SPACES);

	private FixedLengthStringData wsaaBillfreqAlpha = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreqAlpha, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaBillfreqPrevAlpha = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaBillfreqPrev = new ZonedDecimalData(2, 0).isAPartOf(wsaaBillfreqPrevAlpha, 0).init(ZERO).setUnsigned();
	private String wsaaHdrChg = "";
	private FixedLengthStringData wsaaHdrAccum = new FixedLengthStringData(1);
	private String wsaaAgntstat = "";
	private String wsaaGovtstat = "";
	private FixedLengthStringData wsaaOvrdcat = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);

	private FixedLengthStringData wsaaSttrWritten = new FixedLengthStringData(1);
	private Validator sttrWritten = new Validator(wsaaSttrWritten, "Y");
	private ZonedDecimalData wsaaLives = new ZonedDecimalData(8, 0).setUnsigned();
	protected ZonedDecimalData wsaaIdx = new ZonedDecimalData(3, 0).setUnsigned();  //MLIL-539
	private FixedLengthStringData wsaaFound = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrPstatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCharBillfreq = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaBonusInd = new FixedLengthStringData(1);
	private PackedDecimalData wsaaLastTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaPrevCntcurr = new FixedLengthStringData(3);
	private PackedDecimalData wsaaRacdstaLastTranno = new PackedDecimalData(5, 0);
	private String wsaaRacdstaFirstTime = "";
	private FixedLengthStringData wsaaStatcat = new FixedLengthStringData(2).init(SPACES);
	private FixedLengthStringData wsaaT6628Transcode = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init(SPACES);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaPrevLife = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaUpdateFlag = new FixedLengthStringData(1).init(SPACES);
	private Validator updateRequired = new Validator(wsaaUpdateFlag, "Y");
	private FixedLengthStringData wsaaValidflag = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaSttrflag = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private ZonedDecimalData wsaaTableSub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private Validator subtractPrevious = new Validator(wsaaTableSub, "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11");
	private Validator addPrevious = new Validator(wsaaTableSub, "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22");
	private Validator subtractCurrent = new Validator(wsaaTableSub, "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33");
	private Validator addCurrent = new Validator(wsaaTableSub, "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44");
	private Validator subtractIncrease = new Validator(wsaaTableSub, "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55");
	private Validator addIncrease = new Validator(wsaaTableSub, "56", "57", "58", "59", "60", "61", "62", "63", "64", "65", "66");
	private Validator subtractDecrease = new Validator(wsaaTableSub, "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77");
	private Validator addDecrease = new Validator(wsaaTableSub, "78", "79", "80", "81", "82", "83", "84", "85", "86", "87", "88");

	private FixedLengthStringData wsaaT6628Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTransCode = new FixedLengthStringData(4).isAPartOf(wsaaT6628Item, 0);
	private FixedLengthStringData wsaaStatcd = new FixedLengthStringData(2).isAPartOf(wsaaT6628Item, 4);
	private PackedDecimalData wsaaIndex = new PackedDecimalData(3, 0).init(ZERO).setUnsigned();
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init(SPACES);
		/* WSAA-CRRCD */
	private ZonedDecimalData wsaaCommYear = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData wsaaCommYr = new FixedLengthStringData(8).isAPartOf(wsaaCommYear, 0, REDEFINE);
	private ZonedDecimalData wsaaCommyr = new ZonedDecimalData(4, 0).isAPartOf(wsaaCommYr, 0).setUnsigned();
	private PackedDecimalData wsaaCurrPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPrevPrem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaHdrPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(17, 2);
	
	private PackedDecimalData wsaaCommission = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaInstprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAnbAtCcd = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaRiskCessTerm = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaStcmth = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStcmthg = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStlmth = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStvmth = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStumth = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStnmth = new PackedDecimalData(9, 0);
	private PackedDecimalData wsaaStpmth = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStsmth = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStmmth = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStimth = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStpmthg = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStsmthg = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStbmthg = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStldmthg = new PackedDecimalData(18, 2);
	private FixedLengthStringData wsaaChdrStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrReg = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaChdrCntbranch = new FixedLengthStringData(2);
		/* WSAA-CHDR-STATS */
	private FixedLengthStringData wsaaStatA = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaStatB = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaStatC = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaStatD = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaStatE = new FixedLengthStringData(3);
	private PackedDecimalData wsaaChdrTranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaChdrAgntnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaSinstamt02 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinstamt03 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinstamt04 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaSinstamt06 = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaChdrpfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaSrcebus = new FixedLengthStringData(2);
		/* ERRORS */
	private String h205 = "H205";
		/* FORMATS */
	private String itemrec = "ITEMREC";
	private String aglfrec = "AGLFREC";
	private String chdrrec = "CHDRREC";
	private String regprec = "REGPREC";
		/* TABLES */
	private String t5679 = "T5679";
	private String tj688 = "TJ688";
		/*Agent commission logical*/
//	private AgcmstaTableDAM agcmstaIO = new AgcmstaTableDAM();
		/*Agent commission statistical layout*/
//	private AgcmstsTableDAM agcmstsIO = new AgcmstsTableDAM();
		/*Life Agent Header Logical File*/
	private AglfTableDAM aglfIO = new AglfTableDAM();
		/*Contract header file*/
	//private ChdrTableDAM chdrIO = new ChdrTableDAM();
		/*Statistical coverage file layout*/

		/*Statistical coverage file layout*/
//	private CovrstsTableDAM covrstsIO = new CovrstsTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
		/*Life record*/
//	private LifeTableDAM lifeIO = new LifeTableDAM();
	protected Lifsttrrec lifsttrrec = new Lifsttrrec();
		/*Statistical Reinsurance Cession Layout*/
//	private RacdstaTableDAM racdstaIO = new RacdstaTableDAM();
		/*Statistical Reinsurance Cession*/
//	private RacdstbTableDAM racdstbIO = new RacdstbTableDAM();
		/*Statistical Reinsurance Cession Layout*/
//	private RacdstsTableDAM racdstsIO = new RacdstsTableDAM();
		/*Regular Payments File*/
	private RegpTableDAM regpIO = new RegpTableDAM();
		/*Agent Statistics Movement Logical File*/
//	private SttrTableDAM sttrIO = new SttrTableDAM();
		/*Statistical reversal logical file*/
	//private SttrrevTableDAM sttrrevIO = new SttrrevTableDAM();
	private Syserrrec syserrrec = new Syserrrec();
	private T3629rec t3629rec = new T3629rec();
	private T5679rec t5679rec = new T5679rec();
	private T6627rec t6627rec = new T6627rec();
	protected T6628rec t6628rec = new T6628rec();   //MLIL-539
	protected T6629rec t6629rec = new T6629rec();  //MLIL-539
	private Tj688rec tj688rec = new Tj688rec();
	private Varcom varcom = new Varcom();
	
    private SttrpfDAO sttrpfDAO = getApplicationContext().getBean("sttrpfDAO", SttrpfDAO.class);
    private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
    private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
    private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO", AgcmpfDAO.class);
    private RacdpfDAO racdpfDAO = getApplicationContext().getBean("racdpfDAO", RacdpfDAO.class);
    private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
    
    private List<Sttrpf> sttrrevList;
    protected List<Sttrpf> insertSttrrevList;   //MLIL-539
    
    private List<Sttrpf> insertSttrpfList;
    private Iterator<Agcmpf> agcmstsIterator;
    private Iterator<Agcmpf> agcmstaIterator;
    private Iterator<Covrpf> covrstsIterator;
    private Agcmpf agcmsts = new Agcmpf();
    private Agcmpf agcmsta = new Agcmpf();
    private Sttrpf sttrIO = new Sttrpf();
    private Covrpf covrsts = new Covrpf();
    private BigDecimal wsaaAnnTotCurrent = BigDecimal.ZERO;
    private BigDecimal wsaaAnnTotPrem = BigDecimal.ZERO;
    private BigDecimal wsaaAnnpremCurr = BigDecimal.ZERO;
    private BigDecimal wsaaAnnCurrent = BigDecimal.ZERO;
    private BigDecimal wsaaAnnpremPrev = BigDecimal.ZERO;
    private BigDecimal wsaaAnnPrevious = BigDecimal.ZERO;
    private BigDecimal wsaaStraamt = BigDecimal.ZERO;
    
    protected Map<String, List<Itempf>> t6629Map = null;    //MLIL-539
    private Map<String, List<Itempf>> t6628Map = null;
    private Map<String, List<Itempf>> t6627Map = null;
    private Map<String, List<Itempf>> t3629Map = null;
    private Map<String, List<Itempf>> t5540Map = null;
    
    private Datcon1Util datcon1Util = new Datcon1Util();
    private Datcon1rec datcon1rec = new Datcon1rec();
	private Chdrpf chdrIO = new Chdrpf();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	
	private Covrpf covrsta = null; //ILIFE-8800
	private List<Covrpf> covrstaList = null; //ILIFE-8800


	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next170, 
		move1030, 
		loopWrite1040, 
		next1050, 
		exit1090, 
		exit2090, 
		exit2190, 
		covr3020, 
		continue3021, 
		read3030, 
		cont3050, 
		agent3055, 
		next3060, 
		exit3090, 
		loop3120, 
		next3130, 
		loop3140, 
		next3160, 
		exit3190, 
		govt3350, 
		exit3390, 
		loop3620, 
		next3650, 
		cont3660, 
		next3670, 
		exit3695, 
		write3850, 
		loop3920, 
		next3950, 
		cont3960, 
		next3970, 
		loop3977, 
		next3980, 
		write3985, 
		next3987, 
		exit3990, 
		check4420, 
		next4450, 
		exit4490, 
		loop5120, 
		next5150, 
		cont5160, 
		next5170, 
		exit5195, 
		loop5520, 
		next5550, 
		cont5560, 
		next5570, 
		loop5577, 
		next5580, 
		write5585, 
		next5587, 
		exit5590, 
		exit6090, 
		a105Next, 
		a109Exit, 
		a200Next, 
		a200Exit, 
		a520CallRacdstsio, 
		a590Exit, 
		a620CallRacdstaio, 
		a690Exit, 
		a820CallRacdstbio, 
		a890Exit
	}

	public Lifsttr() {
		super();
	}

	public void mainline(Object... parmArray)
	{
		lifsttrrec.lifsttrRec = convertAndSetParam(lifsttrrec.lifsttrRec, parmArray, 0);
		try {
			main100();
		}
		catch (COBOLExitProgramException e) {
		}
	}

	protected void main100()
	{
        if(main110()){
            next170();
        }
	}

protected boolean main110()
 {
        t6629Map = itemDAO.loadSmartTable("IT", lifsttrrec.batccoy.toString(), "T6629");
        t6628Map = itemDAO.loadSmartTable("IT", lifsttrrec.batccoy.toString(), "T6628");
        t6627Map = itemDAO.loadSmartTable("IT", lifsttrrec.batccoy.toString(), "T6627");
        t3629Map = itemDAO.loadSmartTable("IT", lifsttrrec.batccoy.toString(), "T3629");
        t5540Map = itemDAO.loadSmartTable("IT", lifsttrrec.batccoy.toString(), "T5540");
        if (isNE(lifsttrrec.trannor, 99999) && isLT(lifsttrrec.trannor, lifsttrrec.tranno)) {
            reverseSttr1000();
            return true;
        }
        initialise2000();
        componentSelect3000();
        checkTranscation6000();
        if (insertSttrrevList != null && !insertSttrrevList.isEmpty()) {
            sttrpfDAO.insertSttrpfRecord(insertSttrrevList);
        }
        return true;
    }

protected void next170()
	{
		lifsttrrec.statuz.set("****");
		/*EXIT*/
		exitProgram();
	}

protected void dbError580()
	{
		/*PARA*/
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("1");
		callProgram(Syserr.class, syserrrec.syserrRec);
		lifsttrrec.statuz.set("BOMB");
		/*EXIT*/
		exitProgram();
	}

protected void reverseSttr1000()
 {
        sttrrevList = sttrpfDAO.searchSttrpfRecord(lifsttrrec.chdrcoy.toString(), lifsttrrec.chdrnum.toString());
        wsaaLastTranno.set(0);
        wsaaFirstTime.set("Y");

        if (sttrrevList != null && !sttrrevList.isEmpty()) {
            for (Sttrpf sttrrevIO : sttrrevList) {
                if (firstTime.isTrue() && isEQ(sttrrevIO.getStatgov(), "Y") && isEQ(sttrrevIO.getRevtrcde(), SPACES)) {
                    wsaaStatcat.set(sttrrevIO.getStatcat());
                    wsaaFirstTime.set("N");
                }
                if (isLT(sttrrevIO.getTranno(), lifsttrrec.trannor) || isGTE(sttrrevIO.getTranno(), lifsttrrec.tranno)) {
                    break;
                }
                if (isNE(wsaaLastTranno, 0) && isNE(sttrrevIO.getTranno(), wsaaLastTranno)) {
                    break;
                }
                if (isNE(sttrrevIO.getRevtrcde(), SPACES)) {
                    continue;
                }
                move1030(sttrrevIO);

                if (isNE(sttrrevIO.getStatcat(), wsaaStatcat) || isNE(sttrrevIO.getStatgov(), "Y")) {
                    continue;
                }
                wsaaT6628Transcode.set(lifsttrrec.batctrcde);
                
                wsaaTransCode.set(wsaaT6628Transcode);
                wsaaStatcd.set("**");
                if (t6628Map != null && t6628Map.containsKey(wsaaT6628Item)) {
                    t6628rec.t6628Rec.set(StringUtil.rawToString(t6628Map.get(wsaaT6628Item).get(0).getGenarea()));
                }else{
                    t6628rec.t6628Rec.set(SPACES);
                    continue;
                }
                if (isEQ(t6628rec.statcatgs, SPACES)) {
                    continue;
                }
                wsaaIdx.set(0);
                loopWrite1040(sttrrevIO);
            }
        }
    }

protected void move1030(Sttrpf sttrrevIO)
	{
		sttrrevIO.setStcmth((mult(sttrrevIO.getStcmth(),-1)).getbigdata());
		sttrrevIO.setStvmth((mult(sttrrevIO.getStvmth(),-1)).getbigdata());
		sttrrevIO.setStpmth((mult(sttrrevIO.getStpmth(),-1)).getbigdata());
		sttrrevIO.setStsmth((mult(sttrrevIO.getStsmth(),-1)).getbigdata());
		sttrrevIO.setStmmth((mult(sttrrevIO.getStmmth(),-1)).getbigdata());
		sttrrevIO.setStimth((mult(sttrrevIO.getStimth(),-1)).getbigdata());
		sttrrevIO.setStcmthg((mult(sttrrevIO.getStcmthg(),-1)).getbigdata());
		sttrrevIO.setStpmthg((mult(sttrrevIO.getStpmthg(),-1)).getbigdata());
		sttrrevIO.setStsmthg((mult(sttrrevIO.getStsmthg(),-1)).getbigdata());
		sttrrevIO.setStumth((mult(sttrrevIO.getStumth(),-1)).getbigdata());
		sttrrevIO.setStnmth((mult(sttrrevIO.getStnmth(),-1)).getbigdata());
		sttrrevIO.setStlmth((mult(sttrrevIO.getStlmth(),-1)).getbigdata());
		sttrrevIO.setStbmthg((mult(sttrrevIO.getStbmthg(),-1)).getbigdata());
		sttrrevIO.setStldmthg((mult(sttrrevIO.getStldmthg(),-1)).getbigdata());
		sttrrevIO.setStraamt((mult(sttrrevIO.getStraamt(),-1)).getbigdata());
		wsaaLastTranno.set(sttrrevIO.getTranno());
		sttrrevIO.setTranno(lifsttrrec.tranno.toInt());
		sttrrevIO.setBatccoy(lifsttrrec.batccoy.toString());
		sttrrevIO.setBatcbrn(lifsttrrec.batcbrn.toString());
		sttrrevIO.setBatcactyr(lifsttrrec.batcactyr.toInt());
		sttrrevIO.setBatcactmn(lifsttrrec.batcactmn.toInt());
		sttrrevIO.setBatctrcde(lifsttrrec.batctrcde.toString());
		sttrrevIO.setBatcbatch(lifsttrrec.batcbatch.toString());
		if(insertSttrrevList == null){
		    insertSttrrevList = new ArrayList<>();
		}
		insertSttrrevList.add(sttrrevIO);
	}

protected void loopWrite1040(Sttrpf sttrrevIO)
 {
        wsaaIdx.add(1);
        do {
            if (isEQ(t6628rec.statcatg[wsaaIdx.toInt()], SPACES)) {
            	wsaaIdx.add(1);
                continue;
            }
            String itemKey = t6628rec.statcatg[wsaaIdx.toInt()].toString();
            if (t6629Map != null && t6629Map.containsKey(itemKey)) {
                t6629rec.t6629Rec.set(StringUtil.rawToString(t6629Map.get(itemKey).get(0).getGenarea()));
                if (isNE(t6629rec.govtstat, "Y")) {
                    continue;
                }
                sttrrevIO.setStatcat(t6628rec.statcatg[wsaaIdx.toInt()].toString());
                sttrrevIO.setStatgov(t6629rec.govtstat.toString());
                sttrrevIO.setStcmth((mult(sttrrevIO.getStcmth(), -1)).getbigdata());
                sttrrevIO.setStvmth((mult(sttrrevIO.getStvmth(), -1)).getbigdata());
                sttrrevIO.setStpmth((mult(sttrrevIO.getStpmth(), -1)).getbigdata());
                sttrrevIO.setStsmth((mult(sttrrevIO.getStsmth(), -1)).getbigdata());
                sttrrevIO.setStmmth((mult(sttrrevIO.getStmmth(), -1)).getbigdata());
                sttrrevIO.setStimth((mult(sttrrevIO.getStimth(), -1)).getbigdata());
                sttrrevIO.setStcmthg((mult(sttrrevIO.getStcmthg(), -1)).getbigdata());
                sttrrevIO.setStpmthg((mult(sttrrevIO.getStpmthg(), -1)).getbigdata());
                sttrrevIO.setStsmthg((mult(sttrrevIO.getStsmthg(), -1)).getbigdata());
                sttrrevIO.setStumth((mult(sttrrevIO.getStumth(), -1)).getbigdata());
                sttrrevIO.setStnmth((mult(sttrrevIO.getStnmth(), -1)).getbigdata());
                sttrrevIO.setStlmth((mult(sttrrevIO.getStlmth(), -1)).getbigdata());
                sttrrevIO.setStbmthg((mult(sttrrevIO.getStbmthg(), -1)).getbigdata());
                sttrrevIO.setStldmthg((mult(sttrrevIO.getStldmthg(), -1)).getbigdata());
                sttrrevIO.setStraamt((mult(sttrrevIO.getStraamt(), -1)).getbigdata());
                sttrrevIO.setTranno(lifsttrrec.tranno.toInt());
                sttrrevIO.setBatccoy(lifsttrrec.batccoy.toString());
                sttrrevIO.setBatcbrn(lifsttrrec.batcbrn.toString());
                sttrrevIO.setBatcactyr(lifsttrrec.batcactyr.toInt());
                sttrrevIO.setBatcactmn(lifsttrrec.batcactmn.toInt());
                sttrrevIO.setBatctrcde(lifsttrrec.batctrcde.toString());
                sttrrevIO.setBatcbatch(lifsttrrec.batcbatch.toString());
                sttrrevIO.setRevtrcde(SPACES.toString());
                if(insertSttrrevList == null){
                    insertSttrrevList = new ArrayList<>();
                }
                insertSttrrevList.add(sttrrevIO);
            }
            wsaaIdx.add(1);
        } while (isLTE(wsaaIdx, 88));
    }

protected void initialise2000()
	{
        sttrIO.setCommyr(0);
        sttrIO.setStcmth(BigDecimal.ZERO);
        sttrIO.setStvmth(BigDecimal.ZERO);
        sttrIO.setStpmth(BigDecimal.ZERO);
        sttrIO.setStsmth(BigDecimal.ZERO);
        sttrIO.setStmmth(BigDecimal.ZERO);
        sttrIO.setStimth(BigDecimal.ZERO);
        sttrIO.setStcmthg(BigDecimal.ZERO);
        sttrIO.setStpmthg(BigDecimal.ZERO);
        sttrIO.setStsmthg(BigDecimal.ZERO);
        sttrIO.setStumth(BigDecimal.ZERO);
        sttrIO.setStnmth(BigDecimal.ZERO);
        sttrIO.setTranno(0);
        sttrIO.setTrannor(0);
        sttrIO.setBatcactyr(0);
        sttrIO.setBatcactmn(0);
        sttrIO.setStlmth(BigDecimal.ZERO);
        sttrIO.setStbmthg(BigDecimal.ZERO);
        sttrIO.setStldmthg(BigDecimal.ZERO);
        sttrIO.setStraamt(BigDecimal.ZERO);
        sttrIO.setTransactionDate(0);
        wsaaStcmth.set(ZERO);
        wsaaStvmth.set(ZERO);
        wsaaStpmth.set(ZERO);
        wsaaStsmth.set(ZERO);
        wsaaStmmth.set(ZERO);
        wsaaStimth.set(ZERO);
        wsaaStcmthg.set(ZERO);
        wsaaStlmth.set(ZERO);
        wsaaStpmthg.set(ZERO);
        wsaaStsmthg.set(ZERO);
        wsaaStumth.set(ZERO);
        wsaaStnmth.set(ZERO);
        wsaaStraamt = BigDecimal.ZERO;
        wsaaStbmthg.set(ZERO);
        wsaaStldmthg.set(ZERO);
        wsaaHdrPremium.set(ZERO);
        datcon1rec = datcon1Util.callTday(datcon1rec);
        itemIO.setParams(SPACES);
        itemIO.setItempfx("IT");
        itemIO.setItemcoy(lifsttrrec.chdrcoy);
        itemIO.setItemtabl(t5679);
        itemIO.setItemitem(lifsttrrec.batctrcde);
        itemIO.setFormat(itemrec);
        itemIO.setFunction(varcom.readr);
        SmartFileCode.execute(appVars, itemIO);
        if (isNE(itemIO.getStatuz(),varcom.oK)) {
            syserrrec.params.set(itemIO.getParams());
            syserrrec.statuz.set(itemIO.getStatuz());
            dbError580();
        }
		t5679rec.t5679Rec.set(itemIO.getGenarea());

		chdrIO = chdrpfDAO.getChdrpf(lifsttrrec.chdrcoy.toString(),lifsttrrec.chdrnum.toString());
		if(null ==  chdrIO){
			syserrrec.params.set(lifsttrrec.chdrcoy.toString()+"   "+lifsttrrec.chdrnum.toString());
			dbError580();
		} 

		wsaaChdrTranno.set(chdrIO.getTranno());
		wsaaChdrAgntnum.set(chdrIO.getAgntnum());
		wsaaBillfreqAlpha.set(chdrIO.getBillfreq());
		wsaaBillfreqPrevAlpha.set(chdrIO.getBillfreq());
		wsaaSinstamt02.set(chdrIO.getSinstamt02());
		wsaaSinstamt03.set(chdrIO.getSinstamt03());
		wsaaSinstamt04.set(chdrIO.getSinstamt04());
		wsaaSinstamt06.set(chdrIO.getSinstamt06());
		wsaaChdrpfx.set(chdrIO.getChdrpfx());
		wsaaCnttype.set(chdrIO.getCnttype());
		wsaaSrcebus.set(chdrIO.getSrcebus());
		wsaaChdrStatcode.set(chdrIO.getStatcode());
		wsaaChdrPstatcode.set(chdrIO.getPstcde());
		wsaaChdrReg.set(chdrIO.getReg());
		wsaaChdrCntbranch.set(chdrIO.getCntbranch());
		wsaaCharBillfreq.set(chdrIO.getBillfreq());
		readAglfRecord3200();
		if (isEQ(lifsttrrec.minorChg,"Y")) {
			wsaaStvmth.set(ZERO);
		}
		else {
			wsaaStvmth.set(1);
		}
		wsaaChdrTranno.set(chdrIO.getTranno());
		if (isEQ(chdrIO.getTranno(),lifsttrrec.tranno)) {
			checkHeaderChange2100();
		}
		if (isEQ(wsaaHdrChg,"N")) {
			regpIO.setParams(SPACES);
			regpIO.setChdrcoy(lifsttrrec.chdrcoy);
			regpIO.setChdrnum(lifsttrrec.chdrnum);
			regpIO.setLife("01");
			regpIO.setCoverage("01");
			regpIO.setRider("00");
			regpIO.setRgpynum(1);
			regpIO.setFormat(regprec);
			regpIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, regpIO);
			if (isEQ(regpIO.getStatuz(),varcom.mrnf)) {
				return;
			}
			if (isEQ(regpIO.getTranno(),lifsttrrec.tranno)) {
				wsaaHdrChg = "Y";
			}
		}
	}

	protected void checkHeaderChange2100()
	{
		wsaaChdrStatcode.set(chdrIO.getStatcode());
		wsaaChdrReg.set(chdrIO.getReg());
		wsaaChdrCntbranch.set(chdrIO.getCntbranch());
		wsaaStatA.set(chdrIO.getStca());
		wsaaStatB.set(chdrIO.getStcb());
		wsaaStatC.set(chdrIO.getStcc());
		wsaaStatD.set(chdrIO.getStcd());
		wsaaStatE.set(chdrIO.getStce());


		List<Chdrpf> chdrpfList = chdrpfDAO.getChdrpfByChdrnumList("CH",lifsttrrec.chdrcoy.toString(),lifsttrrec.chdrnum.toString(),"2");
		
		if(chdrpfList!= null && chdrpfList.isEmpty()){
			wsaaHdrChg = "Y";
			wsaaStcmth.set(1);
			wsaaStcmthg.set(1);
			return;
		}
		else{
			chdrIO = chdrpfList.get(0);	
		}	
		if (isNE(wsaaChdrStatcode,chdrIO.getStatcode())
				|| isNE(wsaaChdrReg,chdrIO.getReg())
				|| isNE(wsaaChdrCntbranch,chdrIO.getCntbranch())
				|| isNE(wsaaStatA,chdrIO.getStca())
				|| isNE(wsaaStatB,chdrIO.getStcb())
				|| isNE(wsaaStatC,chdrIO.getStcc())
				|| isNE(wsaaStatD,chdrIO.getStcd())
				|| isNE(wsaaStatE,chdrIO.getStce())
				|| isNE(wsaaSinstamt02,chdrIO.getSinstamt02())
				|| isNE(wsaaSinstamt03,chdrIO.getSinstamt03())
				|| isNE(wsaaSinstamt04,chdrIO.getSinstamt04())) {
			wsaaHdrChg = "Y";
			wsaaStcmth.set(1);
			wsaaStcmthg.set(1);
		}
		else {
			wsaaHdrChg = "N";
			wsaaStcmth.set(ZERO);
            wsaaStcmthg.set(ZERO);
        }
        wsaaBillfreqPrevAlpha.set(chdrIO.getBillfreq());
	}

protected void componentSelect3000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
				    start3010();
					prev3015();
				}
				case covr3020: {
					covr3020();
				}
				case continue3021: {
					continue3021();
				}
				case read3030: {
					read3030();
					prev3040();
					
				}
				case cont3050: {
					cont3050();
				}
				case agent3055: {
					agent3055();
				}
				case next3060: {
					next3060();
				}
				case exit3090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3010()
 {
        wsaaPrevCntcurr.set(SPACES);
        wsaaSttrWritten.set("N");

        List<Covrpf> covrstsList = covrpfDAO.searchValidCovrpfRecord(lifsttrrec.chdrcoy.toString(), lifsttrrec.chdrnum.toString(), "01", "01", "00");

        if (covrstsList == null || covrstsList.isEmpty()) {
            goTo(GotoLabel.exit3090);
        }
        covrstsIterator = covrstsList.iterator();
        covrsts = covrstsIterator.next();
        wsaaPrevLife.set(covrsts.getLife());
        a100CheckNoflives();
    }

protected void prev3015()
	{
		//ILIFE-8800 start
		covrsta = new Covrpf();
		covrsta.setCurrfrom(covrsts.getCurrfrom());
		covrsta.setChdrcoy(covrsts.getChdrcoy());
		covrsta.setChdrnum(covrsts.getChdrnum());
		covrsta.setLife("01");
		covrsta.setCoverage("01");
		covrsta.setRider(ZERO.stringValue());
		covrsta.setPlanSuffix(9999);
		covrsta.setValidflag("2");
		covrstaList = covrpfDAO.selectCovrRecord(covrsta);
		if(covrstaList !=null && !covrstaList.isEmpty()) {
		covrsta = covrstaList.get(0);
		}
		//ILIFE-8800 end
		if (isNE(lifsttrrec.agntnum,SPACES)) {
			wsaaTransCode.set(lifsttrrec.batctrcde);
			wsaaStatcd.set(wsaaChdrStatcode);
			if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item)){
			    Itempf item = t6628Map.get(wsaaT6628Item).get(0);
			    t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
			}else{
				t6628rec.t6628Rec.set(SPACES);
			}
			if (isNE(t6628rec.statcatgs,SPACES)) {
				wsaaPrevPrem.set(ZERO);
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaBonusInd.set(covrsts.getBonusInd());
				wsaaTranno.set(lifsttrrec.tranno);
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum.set(wsaaChdrAgntnum);
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaStpmth.set(ZERO);
				wsaaStvmth.set(ZERO);
				wsaaStsmth.set(ZERO);
				wsaaStcmthg.set(ZERO);
				wsaaStlmth.set(ZERO);
				wsaaStmmth.set(ZERO);
				wsaaStcmth.set(1);
				wsaaHdrPremium.set(ZERO);
				wsaaHdrAccum.set("Y");
				agentChange3900();
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaHdrAccum.set(SPACES);
				wsaaAgntstat = "N";
			}
	        wsaaTransCode.set(lifsttrrec.batctrcde);
	        wsaaStatcd.set(covrsts.getStatcode());
			if(t6628Map != null && t6628Map.containsKey(wsaaT6628Item.toString())){
			    Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
			    t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
			}else{
	             t6628rec.t6628Rec.set(SPACES);
	             return;
			}
			if (isEQ(t6628rec.statcatgs,SPACES)) {
			    return;
			}
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum.set(lifsttrrec.agntnum);
			wsaaValidflag.set("1");
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			wsaaTranno.set(lifsttrrec.tranno);
			wsaaStvmth.set(1);
			wsaaStcmth.set(ZERO);
			wsaaStcmthg.set(ZERO);
			wsaaStlmth.set(ZERO);
			agentChange3900();
			wsaaAgntstat = "N";
			wsaaAgntnum.set(SPACES);
			wsaaOvrdcat.set(SPACES);
			return;
		}
		wsaaCurrPrem.set(wsaaSinstamt06);
		  if (covrstaList == null || covrstaList.isEmpty()){
			if (isEQ(wsaaHdrChg,"Y")) {
				if (isEQ(covrsts.getTranno(),lifsttrrec.tranno)) {
					wsaaTransCode.set(lifsttrrec.batctrcde);
					wsaaStatcd.set(wsaaChdrStatcode);
		            if(t6628Map != null && t6628Map.containsKey(wsaaT6628Item.toString())){
		                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
		                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
		            }else{
		                 t6628rec.t6628Rec.set(SPACES);
		            }
					if (isNE(t6628rec.statcatgs,SPACES)) {
						wsaaPrevPrem.set(ZERO);
						wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
						wsaaSumins.set(covrsts.getSumins());
						wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
						wsaaInstprem.set(covrsts.getInstprem());
						wsaaStbmthg.set(covrsts.getZbinstprem());
						wsaaStldmthg.set(covrsts.getZlinstprem());
						wsaaBonusInd.set(covrsts.getBonusInd());
						wsaaStlmth.set(wsaaLives);
						wsaaTranno.set(wsaaChdrTranno);
						wsaaGovtstat = "Y";
						wsaaAgntstat = "N";
						wsaaAgntnum.set(SPACES);
						wsaaOvrdcat.set(SPACES);
						wsaaStpmth.set(ZERO);
						wsaaStvmth.set(ZERO);
						wsaaStcmth.set(ZERO);
						wsaaStsmth.set(ZERO);
						wsaaStmmth.set(ZERO);
						wsaaHdrAccum.set("Y");
						wsaaValidflag.set("1");
						headerPremium3400();
						updateRecord3100();
						wsaaHdrPremium.set(ZERO);
						wsaaAgntstat = "Y";
						wsaaGovtstat = "N";
						wsaaAgntnum.set(wsaaChdrAgntnum);
						wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
						wsaaSumins.set(covrsts.getSumins());
						wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
						wsaaInstprem.set(covrsts.getInstprem());
						wsaaStbmthg.set(covrsts.getZbinstprem());
						wsaaStldmthg.set(covrsts.getZlinstprem());
						wsaaBonusInd.set(covrsts.getBonusInd());
						wsaaStvmth.set(ZERO);
						wsaaStcmth.set(1);
						processAgents3600();
						wsaaAgntnum.set(SPACES);
						wsaaOvrdcat.set(SPACES);
						wsaaHdrAccum.set(SPACES);
						wsaaAgntstat = "N";
					}
					wsaaTransCode.set(lifsttrrec.batctrcde);
					wsaaStatcd.set(covrsts.getStatcode());
                    if(t6628Map != null && t6628Map.containsKey(wsaaT6628Item.toString())){
                        Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                        t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
                    }else{
                        t6628rec.t6628Rec.set(SPACES);
                        return;
                    }
					if (isEQ(t6628rec.statcatgs,SPACES)) {
					    return;
					}
					wsaaValidflag.set("1");
					wsaaCurrPrem.set(wsaaSinstamt06);
					wsaaPrevPrem.set(ZERO);
					wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
					wsaaSumins.set(covrsts.getSumins());
					wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
					wsaaInstprem.set(covrsts.getInstprem());
					wsaaStbmthg.set(covrsts.getZbinstprem());
					wsaaStldmthg.set(covrsts.getZlinstprem());
					wsaaBonusInd.set(covrsts.getBonusInd());
					wsaaTranno.set(wsaaChdrTranno);
					wsaaGovtstat = "Y";
					wsaaAgntstat = "N";
					wsaaAgntnum.set(SPACES);
					wsaaOvrdcat.set(SPACES);
					wsaaStpmth.set(ZERO);
					wsaaStvmth.set(ZERO);
					wsaaStcmth.set(ZERO);
					wsaaStcmthg.set(ZERO);
					wsaaStlmth.set(ZERO);
					wsaaStsmth.set(ZERO);
					wsaaStmmth.set(ZERO);
					wsaaStraamt = BigDecimal.ZERO;
					wsaaLife.set(covrsts.getLife());
					wsaaCoverage.set(covrsts.getCoverage());
					wsaaRider.set(covrsts.getRider());
					wsaaPlanSuffix.set(covrsts.getPlanSuffix());
					a500ReadRacdsts();
					updateRecord3100();
					wsaaHdrPremium.set(ZERO);
					wsaaGovtstat = "N";
					wsaaAgntstat = "Y";
					wsaaGovtstat = "N";
					wsaaAgntnum.set(wsaaChdrAgntnum);
					wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
					wsaaSumins.set(covrsts.getSumins());
					wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
					wsaaInstprem.set(covrsts.getInstprem());
					wsaaStbmthg.set(covrsts.getZbinstprem());
					wsaaStldmthg.set(covrsts.getZlinstprem());
					wsaaBonusInd.set(covrsts.getBonusInd());
					wsaaStvmth.set(1);
					wsaaStcmth.set(ZERO);
					wsaaStcmthg.set(ZERO);
					wsaaStlmth.set(ZERO);
					processAgents3600();
					wsaaAgntnum.set(SPACES);
					wsaaOvrdcat.set(SPACES);
					wsaaAgntstat = "N";
					return;
				}
			}
		}
		if (covrstaList == null || covrstaList.isEmpty()){
			if (isEQ(wsaaHdrChg,"N")) {
				if (isEQ(covrsts.getTranno(),lifsttrrec.tranno)) {
                    wsaaTransCode.set(lifsttrrec.batctrcde);
                    wsaaStatcd.set(covrsts.getStatcode());
				    if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
				        Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
				        t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
				    }else{
                        t6628rec.t6628Rec.set(SPACES);
                        goTo(GotoLabel.covr3020);
				    }
					if (isEQ(t6628rec.statcatgs,SPACES)) {
						goTo(GotoLabel.covr3020);
					}
					wsaaValidflag.set("1");
					wsaaPrevPrem.set(ZERO);
					wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
					wsaaSumins.set(covrsts.getSumins());
					wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
					wsaaInstprem.set(covrsts.getInstprem());
					wsaaStbmthg.set(covrsts.getZbinstprem());
					wsaaStldmthg.set(covrsts.getZlinstprem());
					wsaaBonusInd.set(covrsts.getBonusInd());
					wsaaTranno.set(covrsts.getTranno());
					wsaaGovtstat = "Y";
					wsaaAgntstat = "N";
					wsaaAgntnum.set(SPACES);
					wsaaOvrdcat.set(SPACES);
					wsaaStpmth.set(ZERO);
					wsaaStvmth.set(ZERO);
					wsaaStcmth.set(ZERO);
					wsaaStcmthg.set(ZERO);
					wsaaStlmth.set(ZERO);
					wsaaStsmth.set(ZERO);
					wsaaStmmth.set(ZERO);
					wsaaStraamt = BigDecimal.ZERO;
					wsaaLife.set(covrsts.getLife());
					wsaaCoverage.set(covrsts.getCoverage());
					wsaaRider.set(covrsts.getRider());
					wsaaPlanSuffix.set(covrsts.getPlanSuffix());
					a500ReadRacdsts();
					updateRecord3100();
					wsaaGovtstat = "N";
					wsaaAgntstat = "Y";
					wsaaGovtstat = "N";
					wsaaAgntnum.set(wsaaChdrAgntnum);
					wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
					wsaaSumins.set(covrsts.getSumins());
					wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
					wsaaInstprem.set(covrsts.getInstprem());
					wsaaStbmthg.set(covrsts.getZbinstprem());
					wsaaStldmthg.set(covrsts.getZlinstprem());
					wsaaBonusInd.set(covrsts.getBonusInd());
					wsaaStvmth.set(1);
					wsaaStcmth.set(ZERO);
					wsaaStcmthg.set(ZERO);
					wsaaStlmth.set(ZERO);
					processAgents3600();
					wsaaAgntnum.set(SPACES);
					wsaaOvrdcat.set(SPACES);
					wsaaAgntstat = "N";
					goTo(GotoLabel.covr3020);
				}
			}
		}
		if (isEQ(lifsttrrec.minorChg,"Y")) {
	        wsaaTransCode.set(lifsttrrec.batctrcde);
	        wsaaStatcd.set(covrsts.getStatcode());
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
            }else{
                t6628rec.t6628Rec.set(SPACES);
                goTo(GotoLabel.covr3020);
            }
			if (isEQ(t6628rec.statcatgs,SPACES)) {
				goTo(GotoLabel.covr3020);
			}
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum.set(wsaaChdrAgntnum);
			wsaaTranno.set(lifsttrrec.tranno);
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			wsaaStvmth.set(1);
			wsaaStcmth.set(ZERO);
			wsaaStcmthg.set(ZERO);
			wsaaStlmth.set(ZERO);
			agentCommChange4400();
		}
		
		if((covrstaList == null || covrstaList.isEmpty())
		&& isEQ(wsaaHdrChg,"N")) {
			goTo(GotoLabel.covr3020);
		}
		if (isEQ(wsaaHdrChg,"Y")) {
			if (isEQ(wsaaBillfreq,wsaaBillfreqPrev)) {
				if (isNE(covrsts.getTranno(),lifsttrrec.tranno)) {
                    wsaaTransCode.set(lifsttrrec.batctrcde);
                    wsaaStatcd.set(wsaaChdrStatcode);
		            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
		                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
		                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
		            }else{
		                t6628rec.t6628Rec.set(SPACES);
		                goTo(GotoLabel.covr3020);
		            }
					if (isEQ(t6628rec.statcatgs,SPACES)) {
						goTo(GotoLabel.covr3020);
					}
					wsaaPrevPrem.set(ZERO);
					wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
					wsaaSumins.set(covrsts.getSumins());
					wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
					wsaaInstprem.set(covrsts.getInstprem());
					wsaaStbmthg.set(covrsts.getZbinstprem());
					wsaaStldmthg.set(covrsts.getZlinstprem());
					wsaaBonusInd.set(covrsts.getBonusInd());
					wsaaTranno.set(wsaaChdrTranno);
					wsaaGovtstat = "Y";
					wsaaAgntstat = "N";
					wsaaAgntnum.set(SPACES);
					wsaaOvrdcat.set(SPACES);
					wsaaStpmth.set(ZERO);
					wsaaStvmth.set(ZERO);
					wsaaStcmth.set(ZERO);
					wsaaStsmth.set(ZERO);
					wsaaStmmth.set(ZERO);
					wsaaStcmthg.set(1);
					wsaaStlmth.set(wsaaLives);
					wsaaHdrAccum.set("Y");
					wsaaValidflag.set("1");
					headerPremium3400();
					updateRecord3100();
					if (!(covrstaList.isEmpty()) || (covrstaList!=null)){
						wsaaPrevPrem.set(ZERO);
						wsaaAnbAtCcd.set(covrsta.getAnbAtCcd());
						wsaaSumins.set(covrsta.getSumins());
						wsaaRiskCessTerm.set(covrsta.getRiskCessTerm());
						wsaaInstprem.set(covrsta.getInstprem());
						wsaaStbmthg.set(covrsta.getZbinstprem());
						wsaaStldmthg.set(covrsta.getZlinstprem());
						wsaaBonusInd.set(covrsta.getBonusInd());
						wsaaTranno.set(wsaaChdrTranno);
						wsaaGovtstat = "Y";
						wsaaAgntstat = "N";
						wsaaAgntnum.set(SPACES);
						wsaaOvrdcat.set(SPACES);
						wsaaStpmth.set(ZERO);
						wsaaStvmth.set(ZERO);
						wsaaStcmth.set(ZERO);
						wsaaStsmth.set(ZERO);
						wsaaStmmth.set(ZERO);
						wsaaStcmthg.set(1);
						wsaaStlmth.set(wsaaLives);
						wsaaValidflag.set("2");
						previousHeaderPremium5400();
						updateRecord3100();
						wsaaHdrPremium.set(ZERO);
					}
					wsaaAgntstat = "Y";
					wsaaGovtstat = "N";
					wsaaAgntnum.set(wsaaChdrAgntnum);
					wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
					wsaaSumins.set(covrsts.getSumins());
					wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
					wsaaInstprem.set(covrsts.getInstprem());
					wsaaStbmthg.set(covrsts.getZbinstprem());
					wsaaStldmthg.set(covrsts.getZlinstprem());
					wsaaBonusInd.set(covrsts.getBonusInd());
					wsaaStvmth.set(ZERO);
					wsaaStcmthg.set(ZERO);
					wsaaStlmth.set(ZERO);
					wsaaStcmth.set(1);
					processAgents5500();
					wsaaAgntnum.set(SPACES);
					wsaaOvrdcat.set(SPACES);
					wsaaHdrAccum.set(SPACES);
					wsaaAgntstat = "N";
				
					if(covrstaList == null || covrstaList.isEmpty()){
						wsaaGovtstat = "Y";
						wsaaStraamt = BigDecimal.ZERO;
						wsaaLife.set(covrsts.getLife());
						wsaaCoverage.set(covrsts.getCoverage());
						wsaaRider.set(covrsts.getRider());
						wsaaPlanSuffix.set(covrsts.getPlanSuffix());
						a500ReadRacdsts();
						updateRecord3100();
					}
					goTo(GotoLabel.covr3020);
				}
			}
		}
		if (isEQ(wsaaHdrChg,"X")) {
            wsaaTransCode.set(lifsttrrec.batctrcde);
            wsaaStatcd.set(wsaaChdrStatcode);
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
            }else{
                t6628rec.t6628Rec.set(SPACES);
                goTo(GotoLabel.covr3020);
            }
			if (isEQ(t6628rec.statcatgs,SPACES)) {
				goTo(GotoLabel.covr3020);
			}
			wsaaValidStatus.set("N");
			wsaaStatcode.set(covrsts.getStatcode());
			wsaaPstatcode.set(covrsts.getPstatcode());
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
				validateCovrStatus3700();
			}
			if (isEQ(wsaaValidStatus,"N")) {
				syserrrec.params.set(covrsta.getChdrcoy().concat(covrsta.getChdrnum()));
				syserrrec.statuz.set(h205);
				dbError580();
			}
			wsaaValidflag.set("1");
			wsaaPrevPrem.set(chdrIO.getSinstamt06());
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			wsaaTranno.set(covrsts.getTranno());
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaAgntnum.set(SPACES);
			wsaaOvrdcat.set(SPACES);
			wsaaStpmth.set(ZERO);
			wsaaStsmth.set(ZERO);
			wsaaStmmth.set(ZERO);
			wsaaStcmth.set(ZERO);
			wsaaStvmth.set(ZERO);
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife.set(covrsts.getLife());
			wsaaCoverage.set(covrsts.getCoverage());
			wsaaRider.set(covrsts.getRider());
			wsaaPlanSuffix.set(covrsts.getPlanSuffix());
			a500ReadRacdsts();
			updateRecord3100();
			wsaaValidflag.set("2");
			wsaaAnbAtCcd.set(covrsta.getAnbAtCcd());
			wsaaSumins.set(covrsta.getSumins());
			wsaaRiskCessTerm.set(covrsta.getRiskCessTerm());
			wsaaInstprem.set(covrsta.getInstprem());
			wsaaStbmthg.set(covrsta.getZbinstprem());
			wsaaStldmthg.set(covrsta.getZlinstprem());
			wsaaBonusInd.set(covrsta.getBonusInd());
			wsaaTranno.set(covrsts.getTranno());
			wsaaStpmth.set(ZERO);
			wsaaStsmth.set(ZERO);
			wsaaStmmth.set(ZERO);
			wsaaStcmth.set(ZERO);
			wsaaStvmth.set(ZERO);
			wsaaStraamt = BigDecimal.ZERO;
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaLife.set(covrsta.getLife());
			wsaaCoverage.set(covrsta.getCoverage());
			wsaaRider.set(covrsta.getRider());
			wsaaPlanSuffix.set(covrsta.getPlanSuffix());
			a600ReadRacdsta();
			updateRecord3100();
			wsaaGovtstat = "N";
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum.set(wsaaChdrAgntnum);
			wsaaTranno.set(covrsts.getTranno());
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			wsaaStvmth.set(ZERO);
			wsaaStcmth.set(1);
			processAgents3600();
			wsaaAgntnum.set(SPACES);
			wsaaOvrdcat.set(SPACES);
			wsaaAgntstat = "N";
			goTo(GotoLabel.covr3020);
		}
		if (isNE(wsaaBillfreq,wsaaBillfreqPrev)) {
			if (isEQ(wsaaHdrChg,"Y")) {
                wsaaTransCode.set(lifsttrrec.batctrcde);
                wsaaStatcd.set(wsaaChdrStatcode);
                if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                    Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                    t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
                }else{
                    t6628rec.t6628Rec.set(SPACES);
                    goTo(GotoLabel.covr3020);
                }
				if (isEQ(t6628rec.statcatgs,SPACES)) {
					goTo(GotoLabel.covr3020);
				}
				wsaaPrevPrem.set(ZERO);
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaBonusInd.set(covrsts.getBonusInd());
				wsaaTranno.set(wsaaChdrTranno);
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaStpmth.set(ZERO);
				wsaaStvmth.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStsmth.set(ZERO);
				wsaaStmmth.set(ZERO);
				wsaaStcmthg.set(1);
				wsaaStlmth.set(wsaaLives);
				wsaaHdrAccum.set("Y");
				wsaaValidflag.set("1");
				headerPremium3400();
				updateRecord3100();
				wsaaPrevPrem.set(ZERO);
				wsaaAnbAtCcd.set(covrsta.getAnbAtCcd());
				wsaaSumins.set(covrsta.getSumins());
				wsaaRiskCessTerm.set(covrsta.getRiskCessTerm());
				wsaaInstprem.set(covrsta.getInstprem());
				wsaaStbmthg.set(covrsta.getZbinstprem());
				wsaaStldmthg.set(covrsta.getZlinstprem());
				wsaaBonusInd.set(covrsta.getBonusInd());
				wsaaTranno.set(wsaaChdrTranno);
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaStpmth.set(ZERO);
				wsaaStvmth.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStsmth.set(ZERO);
				wsaaStmmth.set(ZERO);
				wsaaStcmthg.set(1);
				wsaaStlmth.set(wsaaLives);
				wsaaValidflag.set("2");
				previousHeaderPremium5400();
				updateRecord3100();
				wsaaHdrPremium.set(ZERO);
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum.set(wsaaChdrAgntnum);
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaBonusInd.set(covrsts.getBonusInd());
				wsaaStvmth.set(ZERO);
				wsaaStcmthg.set(ZERO);
				wsaaStlmth.set(ZERO);
				wsaaStcmth.set(1);
				processAgents5500();
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaHdrAccum.set(SPACES);
				wsaaAgntstat = "N";
			}
			if (isNE(covrsts.getTranno(),lifsttrrec.tranno)) {
				goTo(GotoLabel.covr3020);
			}
            wsaaTransCode.set(lifsttrrec.batctrcde);
            wsaaStatcd.set(covrsts.getStatcode());
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
            }else{
                t6628rec.t6628Rec.set(SPACES);
                goTo(GotoLabel.covr3020);
            }
			if (isEQ(t6628rec.statcatgs,SPACES)) {
				goTo(GotoLabel.covr3020);
			}
			wsaaPrevPrem.set(chdrIO.getSinstamt06());
			wsaaValidflag.set("1");
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			wsaaTranno.set(covrsts.getTranno());
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaAgntnum.set(SPACES);
			wsaaOvrdcat.set(SPACES);
			wsaaStpmth.set(ZERO);
			wsaaStvmth.set(ZERO);
			wsaaStcmth.set(ZERO);
			wsaaStsmth.set(ZERO);
			wsaaStmmth.set(ZERO);
			wsaaStcmthg.set(ZERO);
			wsaaStlmth.set(ZERO);
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife.set(covrsts.getLife());
			wsaaCoverage.set(covrsts.getCoverage());
			wsaaRider.set(covrsts.getRider());
			wsaaPlanSuffix.set(covrsts.getPlanSuffix());
			a500ReadRacdsts();
			updateRecord3100();
			wsaaGovtstat = "N";
			wsaaValidflag.set("2");
			wsaaAnbAtCcd.set(covrsta.getAnbAtCcd());
			wsaaSumins.set(covrsta.getSumins());
			wsaaRiskCessTerm.set(covrsta.getRiskCessTerm());
			wsaaInstprem.set(covrsta.getInstprem());
			wsaaStbmthg.set(covrsta.getZbinstprem());
			wsaaStldmthg.set(covrsta.getZlinstprem());
			wsaaBonusInd.set(covrsta.getBonusInd());
			wsaaTranno.set(covrsts.getTranno());
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaStpmth.set(ZERO);
			wsaaStvmth.set(ZERO);
			wsaaStcmth.set(ZERO);
			wsaaStsmth.set(ZERO);
			wsaaStmmth.set(ZERO);
			wsaaStcmthg.set(ZERO);
			wsaaStlmth.set(ZERO);
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife.set(covrsta.getLife());
			wsaaCoverage.set(covrsta.getCoverage());
			wsaaRider.set(covrsta.getRider());
			wsaaPlanSuffix.set(covrsta.getPlanSuffix());
			a600ReadRacdsta();
			updateRecord3100();
			wsaaGovtstat = "N";
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum.set(wsaaChdrAgntnum);
			wsaaTranno.set(covrsts.getTranno());
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			if (isNE(wsaaHdrChg,"Y")) {
				wsaaStcmthg.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStlmth.set(ZERO);
			}
			wsaaStvmth.set(1);
			processAgents5500();
			wsaaAgntnum.set(SPACES);
			wsaaOvrdcat.set(SPACES);
			wsaaAgntstat = "N";
			goTo(GotoLabel.covr3020);
		}
		if (isGT(covrsts.getTranno(),covrsta.getTranno())
		&& isEQ(covrsts.getTranno(),lifsttrrec.tranno)) {
			if (isEQ(wsaaHdrChg,"Y")) {
	            wsaaTransCode.set(lifsttrrec.batctrcde);
	            wsaaStatcd.set(wsaaChdrStatcode);
	            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
	                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
	                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
	            }else{
	                t6628rec.t6628Rec.set(SPACES);
	                goTo(GotoLabel.covr3020);
	            }
				if (isEQ(t6628rec.statcatgs,SPACES)) {
					goTo(GotoLabel.covr3020);
				}
				wsaaPrevPrem.set(ZERO);
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaBonusInd.set(covrsts.getBonusInd());
				wsaaTranno.set(wsaaChdrTranno);
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaStpmth.set(ZERO);
				wsaaStvmth.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStsmth.set(ZERO);
				wsaaStmmth.set(ZERO);
				wsaaStcmthg.set(1);
				wsaaStlmth.set(wsaaLives);
				wsaaHdrAccum.set("Y");
				wsaaValidflag.set("1");
				headerPremium3400();
				updateRecord3100();
				wsaaPrevPrem.set(ZERO);
				wsaaAnbAtCcd.set(covrsta.getAnbAtCcd());
				wsaaSumins.set(covrsta.getSumins());
				wsaaRiskCessTerm.set(covrsta.getRiskCessTerm());
				wsaaInstprem.set(covrsta.getInstprem());
				wsaaStbmthg.set(covrsta.getZbinstprem());
				wsaaStldmthg.set(covrsta.getZlinstprem());
				wsaaBonusInd.set(covrsta.getBonusInd());
				wsaaTranno.set(wsaaChdrTranno);
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaStpmth.set(ZERO);
				wsaaStvmth.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStsmth.set(ZERO);
				wsaaStmmth.set(ZERO);
				wsaaStcmthg.set(1);
				wsaaStlmth.set(wsaaLives);
				wsaaValidflag.set("2");
				previousHeaderPremium5400();
				updateRecord3100();
				wsaaHdrPremium.set(ZERO);
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum.set(wsaaChdrAgntnum);
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaBonusInd.set(covrsts.getBonusInd());
				wsaaStvmth.set(ZERO);
				wsaaStcmthg.set(ZERO);
				wsaaStlmth.set(ZERO);
				wsaaStcmth.set(1);
				processAgents3600();
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaHdrAccum.set(SPACES);
				wsaaAgntstat = "N";
			}
			wsaaTransCode.set(lifsttrrec.batctrcde);
			wsaaStatcd.set(wsaaChdrStatcode);
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
            }else{
                t6628rec.t6628Rec.set(SPACES);
                goTo(GotoLabel.covr3020);
            }
			if (isEQ(t6628rec.statcatgs,SPACES)) {
				goTo(GotoLabel.covr3020);
			}
			wsaaPrevPrem.set(chdrIO.getSinstamt06());
			wsaaValidflag.set("1");
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			wsaaTranno.set(covrsts.getTranno());
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaAgntnum.set(SPACES);
			wsaaOvrdcat.set(SPACES);
			wsaaStpmth.set(ZERO);
			wsaaStvmth.set(ZERO);
			wsaaStcmth.set(ZERO);
			wsaaStsmth.set(ZERO);
			wsaaStmmth.set(ZERO);
			wsaaStcmthg.set(ZERO);
			wsaaStlmth.set(ZERO);
			wsaaStraamt = BigDecimal.ZERO;
			if (isEQ(wsaaHdrChg,"N")) {
				wsaaStcmthg.set(1);
				wsaaStlmth.set(wsaaLives);
			}
			wsaaLife.set(covrsts.getLife());
			wsaaCoverage.set(covrsts.getCoverage());
			wsaaRider.set(covrsts.getRider());
			wsaaPlanSuffix.set(covrsts.getPlanSuffix());
			a500ReadRacdsts();
			updateRecord3100();
			wsaaGovtstat = "N";
			wsaaValidflag.set("2");
			wsaaAnbAtCcd.set(covrsta.getAnbAtCcd());
			wsaaSumins.set(covrsta.getSumins());
			wsaaRiskCessTerm.set(covrsta.getRiskCessTerm());
			wsaaInstprem.set(covrsta.getInstprem());
			wsaaStbmthg.set(covrsta.getZbinstprem());
			wsaaStldmthg.set(covrsta.getZlinstprem());
			wsaaBonusInd.set(covrsta.getBonusInd());
			wsaaTranno.set(covrsts.getTranno());
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaStpmth.set(ZERO);
			wsaaStvmth.set(ZERO);
			wsaaStcmth.set(ZERO);
			wsaaStsmth.set(ZERO);
			wsaaStmmth.set(ZERO);
			wsaaStcmthg.set(ZERO);
			wsaaStlmth.set(ZERO);
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife.set(covrsta.getLife());
			wsaaCoverage.set(covrsta.getCoverage());
			wsaaRider.set(covrsta.getRider());
			wsaaPlanSuffix.set(covrsta.getPlanSuffix());
			a600ReadRacdsta();
			updateRecord3100();
			wsaaGovtstat = "N";
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum.set(wsaaChdrAgntnum);
			wsaaTranno.set(covrsts.getTranno());
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			if (isNE(wsaaHdrChg,"Y")) {
				wsaaStcmthg.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStlmth.set(ZERO);
			}
			wsaaStvmth.set(1);
			processAgents3600();
			wsaaAgntnum.set(SPACES);
			wsaaOvrdcat.set(SPACES);
			wsaaAgntstat = "N";
		}
	}

protected void covr3020()
	{
		if (!sttrWritten.isTrue()) {
			sttrIO.setStcmth(BigDecimal.ONE);
			sttrIO.setStcmthg(BigDecimal.ONE);
			wsaaStcmthg.set(1);
			wsaaStcmth.set(1);
			goTo(GotoLabel.continue3021);
		}
		sttrIO.setStcmth(BigDecimal.ZERO);
		sttrIO.setStcmthg(BigDecimal.ZERO);
		wsaaStcmthg.set(ZERO);
		wsaaStcmth.set(ZERO);
	}

protected void continue3021()
	{
		return;
	}

protected void read3030()
	{
        if(covrstsIterator == null || !covrstsIterator.hasNext()){
            goTo(GotoLabel.exit3090);
        }
        covrsts = covrstsIterator.next();
		if (sttrWritten.isTrue()) {
			if (isEQ(covrsts.getLife(),wsaaPrevLife)) {
				wsaaLives.set(ZERO);
			}
			else {
				a100CheckNoflives();
				wsaaPrevLife.set(covrsts.getLife());
			}
		}
	}

protected void prev3040()
	{
	 	//ILIFE-8800 start
		covrsta = new Covrpf();
		covrsta.setCurrfrom(covrsts.getCurrfrom());
        covrsta.setChdrcoy(covrsts.getChdrcoy());
        covrsta.setChdrnum(covrsts.getChdrnum());
        covrsta.setLife(covrsts.getLife());
        covrsta.setCoverage(covrsts.getCoverage());
        covrsta.setRider(covrsts.getRider());
        covrsta.setPlanSuffix(covrsts.getPlanSuffix());
        covrsta.setTranno(covrsts.getTranno());
        covrsta.setValidflag("2");
        covrstaList = covrpfDAO.selectCovrRecord(covrsta);
        if(covrstaList !=null && !covrstaList.isEmpty()) {
    		covrsta = covrstaList.get(0);
    		}
        else {
        	cont3050();
        }
        //ILIFE-8800 end
	}

protected void cont3050()
	{
		wsaaCurrPrem.set(wsaaSinstamt06);
		wsaaStvmth.set(1);
		if (covrstaList == null || covrstaList.isEmpty()){
			if (isEQ(wsaaHdrChg,"Y")) {
				if (isEQ(covrsts.getTranno(),lifsttrrec.tranno)) {
					wsaaTransCode.set(lifsttrrec.batctrcde);
					wsaaStatcd.set(covrsts.getStatcode());
	                if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
	                    Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
	                    t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
	                }else{
	                    t6628rec.t6628Rec.set(SPACES);
	                }
					if (isNE(t6628rec.statcatgs,SPACES)) {
						wsaaPrevPrem.set(ZERO);
						wsaaValidflag.set("1");
						wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
						wsaaSumins.set(covrsts.getSumins());
						wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
						wsaaInstprem.set(covrsts.getInstprem());
						wsaaStbmthg.set(covrsts.getZbinstprem());
						wsaaStldmthg.set(covrsts.getZlinstprem());
						wsaaBonusInd.set(covrsts.getBonusInd());
						wsaaTranno.set(wsaaChdrTranno);
						wsaaGovtstat = "Y";
						wsaaAgntstat = "N";
						wsaaAgntnum.set(SPACES);
						wsaaOvrdcat.set(SPACES);
						wsaaStlmth.set(wsaaLives);
						wsaaStpmth.set(ZERO);
						wsaaStsmth.set(ZERO);
						wsaaStmmth.set(ZERO);
						wsaaStcmth.set(ZERO);
						wsaaStvmth.set(ZERO);
						wsaaStraamt = BigDecimal.ZERO;
						wsaaLife.set(covrsts.getLife());
						wsaaCoverage.set(covrsts.getCoverage());
						wsaaRider.set(covrsts.getRider());
						wsaaPlanSuffix.set(covrsts.getPlanSuffix());
						a500ReadRacdsts();
						updateRecord3100();
						wsaaGovtstat = "N";
						wsaaAgntstat = "Y";
						wsaaGovtstat = "N";
						wsaaAgntnum.set(wsaaChdrAgntnum);
						wsaaTranno.set(covrsts.getTranno());
						wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
						wsaaSumins.set(covrsts.getSumins());
						wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
						wsaaInstprem.set(covrsts.getInstprem());
						wsaaStbmthg.set(covrsts.getZbinstprem());
						wsaaStldmthg.set(covrsts.getZlinstprem());
						wsaaBonusInd.set(covrsts.getBonusInd());
						wsaaStvmth.set(1);
						wsaaStcmth.set(ZERO);
						processAgents3600();
						wsaaAgntnum.set(SPACES);
						wsaaOvrdcat.set(SPACES);
						wsaaAgntstat = "N";
					}
					goTo(GotoLabel.next3060);
				}
			}
		}
		if (covrstaList == null || covrstaList.isEmpty()){
			if (isEQ(wsaaHdrChg,"N")) {
				if (isEQ(covrsts.getTranno(),lifsttrrec.tranno)) {
					wsaaTransCode.set(lifsttrrec.batctrcde);
					wsaaStatcd.set(covrsts.getStatcode());
                    if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                        Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                        t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
                    }else{
                        t6628rec.t6628Rec.set(SPACES);
                    }
					if (isNE(t6628rec.statcatgs,SPACES)) {
						wsaaValidflag.set("1");
						wsaaPrevPrem.set(ZERO);
						wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
						wsaaSumins.set(covrsts.getSumins());
						wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
						wsaaInstprem.set(covrsts.getInstprem());
						wsaaStbmthg.set(covrsts.getZbinstprem());
						wsaaStldmthg.set(covrsts.getZlinstprem());
						wsaaBonusInd.set(covrsts.getBonusInd());
						wsaaTranno.set(covrsts.getTranno());
						wsaaGovtstat = "Y";
						wsaaAgntstat = "N";
						wsaaAgntnum.set(SPACES);
						wsaaOvrdcat.set(SPACES);
						wsaaStpmth.set(ZERO);
						wsaaStsmth.set(ZERO);
						wsaaStmmth.set(ZERO);
						wsaaStcmth.set(ZERO);
						wsaaStvmth.set(ZERO);
						wsaaStraamt = BigDecimal.ZERO;
						wsaaStlmth.set(wsaaLives);
						wsaaLife.set(covrsts.getLife());
						wsaaCoverage.set(covrsts.getCoverage());
						wsaaRider.set(covrsts.getRider());
						wsaaPlanSuffix.set(covrsts.getPlanSuffix());
						a500ReadRacdsts();
						updateRecord3100();
						wsaaGovtstat = "N";
						wsaaAgntstat = "Y";
						wsaaGovtstat = "N";
						wsaaAgntnum.set(wsaaChdrAgntnum);
						wsaaTranno.set(covrsts.getTranno());
						wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
						wsaaSumins.set(covrsts.getSumins());
						wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
						wsaaInstprem.set(covrsts.getInstprem());
						wsaaStbmthg.set(covrsts.getZbinstprem());
						wsaaStldmthg.set(covrsts.getZlinstprem());
						wsaaBonusInd.set(covrsts.getBonusInd());
						wsaaStvmth.set(1);
						wsaaStcmth.set(ZERO);
						processAgents3600();
						wsaaAgntnum.set(SPACES);
						wsaaOvrdcat.set(SPACES);
						wsaaAgntstat = "N";
					}
					goTo(GotoLabel.next3060);
				}
			}
		}
		if (isEQ(lifsttrrec.minorChg,"Y")) {
			wsaaTransCode.set(lifsttrrec.batctrcde);
			wsaaStatcd.set(covrsts.getStatcode());
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
            }else{
                t6628rec.t6628Rec.set(SPACES);
            }
			if (isNE(t6628rec.statcatgs,SPACES)) {
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum.set(wsaaChdrAgntnum);
				wsaaTranno.set(lifsttrrec.tranno);
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaBonusInd.set(covrsts.getBonusInd());
				wsaaStcmthg.set(ZERO);
				wsaaStlmth.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStvmth.set(1);
				agentCommChange4400();
			}
		}
		if (covrstaList == null || covrstaList.isEmpty()){
			goTo(GotoLabel.agent3055);
		}
		if (isNE(wsaaBillfreq,wsaaBillfreqPrev)
		&& isEQ(covrsts.getTranno(),lifsttrrec.tranno)) {
			wsaaTransCode.set(lifsttrrec.batctrcde);
			wsaaStatcd.set(covrsts.getStatcode());
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
            }else{
                t6628rec.t6628Rec.set(SPACES);
                goTo(GotoLabel.next3060);
            }
			if (isEQ(t6628rec.statcatgs,SPACES)) {
				goTo(GotoLabel.next3060);
			}
			wsaaPrevPrem.set(chdrIO.getSinstamt06());
			wsaaValidflag.set("1");
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			wsaaTranno.set(covrsts.getTranno());
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaAgntnum.set(SPACES);
			wsaaOvrdcat.set(SPACES);
			wsaaStlmth.set(wsaaLives);
			wsaaStpmth.set(ZERO);
			wsaaStvmth.set(ZERO);
			wsaaStcmth.set(ZERO);
			wsaaStsmth.set(ZERO);
			wsaaStmmth.set(ZERO);
			wsaaStcmthg.set(ZERO);
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife.set(covrsts.getLife());
			wsaaCoverage.set(covrsts.getCoverage());
			wsaaRider.set(covrsts.getRider());
			wsaaPlanSuffix.set(covrsts.getPlanSuffix());
			a500ReadRacdsts();
			updateRecord3100();
			wsaaGovtstat = "N";
			wsaaValidflag.set("2");
			wsaaAnbAtCcd.set(covrsta.getAnbAtCcd());
			wsaaSumins.set(covrsta.getSumins());
			wsaaRiskCessTerm.set(covrsta.getRiskCessTerm());
			wsaaInstprem.set(covrsta.getInstprem());
			wsaaStbmthg.set(covrsta.getZbinstprem());
			wsaaStldmthg.set(covrsta.getZlinstprem());
			wsaaBonusInd.set(covrsta.getBonusInd());
			wsaaTranno.set(covrsts.getTranno());
			wsaaGovtstat = "Y";
			wsaaAgntstat = "N";
			wsaaStpmth.set(ZERO);
			wsaaStvmth.set(ZERO);
			wsaaStcmth.set(ZERO);
			wsaaStsmth.set(ZERO);
			wsaaStmmth.set(ZERO);
			wsaaStcmthg.set(ZERO);
			wsaaStlmth.set(ZERO);
			wsaaStraamt = BigDecimal.ZERO;
			wsaaLife.set(covrsta.getLife());
			wsaaCoverage.set(covrsta.getCoverage());
			wsaaRider.set(covrsta.getRider());
			wsaaPlanSuffix.set(covrsta.getPlanSuffix());
			a600ReadRacdsta();
			updateRecord3100();
			wsaaGovtstat = "N";
			wsaaAgntstat = "Y";
			wsaaGovtstat = "N";
			wsaaAgntnum.set(wsaaChdrAgntnum);
			wsaaTranno.set(covrsts.getTranno());
			wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
			wsaaSumins.set(covrsts.getSumins());
			wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
			wsaaInstprem.set(covrsts.getInstprem());
			wsaaStbmthg.set(covrsts.getZbinstprem());
			wsaaStldmthg.set(covrsts.getZlinstprem());
			wsaaBonusInd.set(covrsts.getBonusInd());
			if (isNE(wsaaHdrChg,"Y")) {
				wsaaStcmthg.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStlmth.set(ZERO);
			}
			wsaaStvmth.set(1);
			processAgents5500();
			wsaaAgntnum.set(SPACES);
			wsaaOvrdcat.set(SPACES);
			wsaaAgntstat = "N";
			goTo(GotoLabel.next3060);
		}
		if (isGT(covrsts.getTranno(),covrsta.getTranno())
		&& isEQ(covrsts.getTranno(),lifsttrrec.tranno)) {
			wsaaTransCode.set(lifsttrrec.batctrcde);
			wsaaStatcd.set(wsaaChdrStatcode);
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
            }else{
                t6628rec.t6628Rec.set(SPACES);
            }
			if (isNE(t6628rec.statcatgs,SPACES)) {
				wsaaValidflag.set("1");
				wsaaPrevPrem.set(chdrIO.getSinstamt06());
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaBonusInd.set(covrsts.getBonusInd());
				wsaaTranno.set(covrsts.getTranno());
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaStlmth.set(wsaaLives);
				wsaaStpmth.set(ZERO);
				wsaaStsmth.set(ZERO);
				wsaaStmmth.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStvmth.set(ZERO);
				wsaaStraamt = BigDecimal.ZERO;
				wsaaLife.set(covrsts.getLife());
				wsaaCoverage.set(covrsts.getCoverage());
				wsaaRider.set(covrsts.getRider());
				wsaaPlanSuffix.set(covrsts.getPlanSuffix());
				a500ReadRacdsts();
				updateRecord3100();
				wsaaValidflag.set("2");
				wsaaAnbAtCcd.set(covrsta.getAnbAtCcd());
				wsaaSumins.set(covrsta.getSumins());
				wsaaRiskCessTerm.set(covrsta.getRiskCessTerm());
				wsaaInstprem.set(covrsta.getInstprem());
				wsaaStbmthg.set(covrsta.getZbinstprem());
				wsaaStldmthg.set(covrsta.getZlinstprem());
				wsaaBonusInd.set(covrsta.getBonusInd());
				wsaaTranno.set(covrsts.getTranno());
				wsaaStpmth.set(ZERO);
				wsaaStsmth.set(ZERO);
				wsaaStmmth.set(ZERO);
				wsaaStcmth.set(ZERO);
				wsaaStvmth.set(ZERO);
				wsaaStraamt = BigDecimal.ZERO;
				wsaaGovtstat = "Y";
				wsaaAgntstat = "N";
				wsaaLife.set(covrsta.getLife());
				wsaaCoverage.set(covrsta.getCoverage());
				wsaaRider.set(covrsta.getRider());
				wsaaPlanSuffix.set(covrsta.getPlanSuffix());
				if (isEQ(wsaaHdrChg,"N")) {
					wsaaStcmthg.set(ZERO);
					wsaaStlmth.set(ZERO);
				}
				a600ReadRacdsta();
				updateRecord3100();
				wsaaGovtstat = "N";
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum.set(wsaaChdrAgntnum);
				wsaaTranno.set(covrsts.getTranno());
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaBonusInd.set(covrsts.getBonusInd());
				wsaaStcmth.set(ZERO);
				wsaaStvmth.set(1);
				processAgents3600();
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaAgntstat = "N";
			}
			goTo(GotoLabel.next3060);
		}
	}

protected void agent3055()
	{
		if (isNE(lifsttrrec.agntnum,SPACES)) {
			wsaaTransCode.set(lifsttrrec.batctrcde);
			wsaaStatcd.set(covrsts.getStatcode());
            if(t6628Map!=null&&t6628Map.containsKey(wsaaT6628Item.toString())){
                Itempf item = t6628Map.get(wsaaT6628Item.toString()).get(0);
                t6628rec.t6628Rec.set(StringUtil.rawToString(item.getGenarea()));
            }else{
                t6628rec.t6628Rec.set(SPACES);
            }
			if (isNE(t6628rec.statcatgs,SPACES)) {
				wsaaAgntstat = "Y";
				wsaaGovtstat = "N";
				wsaaAgntnum.set(lifsttrrec.agntnum);
				wsaaTranno.set(lifsttrrec.tranno);
				wsaaAnbAtCcd.set(covrsts.getAnbAtCcd());
				wsaaSumins.set(covrsts.getSumins());
				wsaaRiskCessTerm.set(covrsts.getRiskCessTerm());
				wsaaInstprem.set(covrsts.getInstprem());
				wsaaStbmthg.set(covrsts.getZbinstprem());
				wsaaStldmthg.set(covrsts.getZlinstprem());
				wsaaBonusInd.set(covrsts.getBonusInd());
				wsaaStvmth.set(1);
				wsaaStcmth.set(ZERO);
				wsaaStcmthg.set(ZERO);
				wsaaStlmth.set(ZERO);
				agentChange3900();
				wsaaAgntnum.set(SPACES);
				wsaaOvrdcat.set(SPACES);
				wsaaAgntstat = "N";
			}
		}
	}

protected void next3060()
	{
		goTo(GotoLabel.read3030);
	}

// done
protected void updateRecord3100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3110();
				}
				case loop3120: {
					loop3120();
					cont3125();
				}
				case next3130: {
					next3130();
				}
				case loop3140: {
					loop3140();
					next3150();
				}
				case next3160: {
					next3160();
				}
				case exit3190: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3110()
	{
		wsaaBandage.set(SPACES);
		wsaaBandsa.set(SPACES);
		wsaaBandtrm.set(SPACES);
		wsaaBandprm.set(SPACES);
		wsaaAgntage.set(SPACES);
		wsaaAgntsa.set(SPACES);
		wsaaAgnttrm.set(SPACES);
		wsaaAgntprm.set(SPACES);
		wsaaGovtage.set(SPACES);
		wsaaGovtsa.set(SPACES);
		wsaaGovttrm.set(SPACES);
		wsaaGovtprm.set(SPACES);
		if (isEQ(wsaaValidflag,"1")) {
			wsaaSub.set(22);
		}
		else {
			wsaaSub.set(ZERO);
		}
	}

protected void loop3120()
 {
        wsaaSub.add(1);
        if (isGT(wsaaSub, 44)) {
            goTo(GotoLabel.next3130);
        }
        wsaaTableSub.set(wsaaSub);
        if (isEQ(wsaaValidflag, "1")) {
            if (!subtractCurrent.isTrue() && !addCurrent.isTrue()) {
                goTo(GotoLabel.next3130);
            }
        }
        if (isEQ(wsaaValidflag, "2")) {
            if (!subtractPrevious.isTrue() && !addPrevious.isTrue()) {
                goTo(GotoLabel.next3130);
            }
        }
        if (isEQ(t6628rec.statcatg[wsaaSub.toInt()], SPACES)) {
            goTo(GotoLabel.loop3120);
        }
        String itemitem = t6628rec.statcatg[wsaaSub.toInt()].toString();
        if (t6629Map != null && t6629Map.containsKey(itemitem)) {
            Itempf item = t6629Map.get(itemitem).get(0);
            t6629rec.t6629Rec.set(StringUtil.rawToString(item.getGenarea()));
        } else {
            t6629rec.t6629Rec.set(SPACES);
            goTo(GotoLabel.loop3120);
        }
        if (isNE(t6629rec.agntstat, "Y") && isNE(t6629rec.govtstat, "Y")) {
            goTo(GotoLabel.loop3120);
        }
        if (isEQ(wsaaGovtstat, "Y") && isNE(t6629rec.govtstat, "Y")) {
            goTo(GotoLabel.loop3120);
        }
        if (isEQ(wsaaAgntstat, "Y") && isNE(t6629rec.agntstat, "Y")) {
            goTo(GotoLabel.loop3120);
        }
    }

protected void cont3125()
	{
		generateMovements3300();
		writeSttrRecord3800();
		goTo(GotoLabel.loop3120);
	}

protected void next3130()
	{
		wsaaBandage.set(SPACES);
		wsaaBandsa.set(SPACES);
		wsaaBandtrm.set(SPACES);
		wsaaBandprm.set(SPACES);
		wsaaAgntage.set(SPACES);
		wsaaAgntsa.set(SPACES);
		wsaaAgnttrm.set(SPACES);
		wsaaAgntprm.set(SPACES);
		wsaaGovtage.set(SPACES);
		wsaaGovtsa.set(SPACES);
		wsaaGovttrm.set(SPACES);
		wsaaGovtprm.set(SPACES);
		
		if (covrstaList == null || covrstaList.isEmpty()
		&& isEQ(wsaaHdrChg,"Y")
		&& isEQ(covrsts.getTranno(),lifsttrrec.tranno)) {
			goTo(GotoLabel.exit3190);
		}
		
		if (covrstaList == null || covrstaList.isEmpty()
		&& isEQ(wsaaHdrChg,"N")
		&& isEQ(covrsts.getTranno(),lifsttrrec.tranno)) {
			goTo(GotoLabel.exit3190);
		}
		if (isEQ(wsaaHdrChg,"Y")) {
			goTo(GotoLabel.exit3190);
		}
		if (isNE(lifsttrrec.agntnum,SPACES)) {
			goTo(GotoLabel.exit3190);
		}
		if (isGTE(wsaaCurrPrem,wsaaPrevPrem)) {
			wsaaSub.set(44);
		}
		else {
			wsaaSub.set(66);
		}
	}

protected void loop3140()
	{
		wsaaSub.add(1);
		if (isGT(wsaaSub,88)) {
			goTo(GotoLabel.next3160);
		}
		wsaaTableSub.set(wsaaSub);
		if (isLT(wsaaCurrPrem,wsaaPrevPrem)
		&& isLT(wsaaSub,66)) {
			goTo(GotoLabel.next3160);
		}
		if (isGTE(wsaaCurrPrem,wsaaPrevPrem)
		&& isGT(wsaaSub,66)) {
			goTo(GotoLabel.next3160);
		}
		if (isEQ(lifsttrrec.minorChg,"Y")
		&& isGT(wsaaSub,66)) {
			goTo(GotoLabel.next3160);
		}
		if (isEQ(t6628rec.statcatg[wsaaSub.toInt()],SPACES)) {
			goTo(GotoLabel.loop3140);
		}
		String itemitem = t6628rec.statcatg[wsaaSub.toInt()].toString();
		if(t6629Map !=null&&t6629Map.containsKey(itemitem)){
		    Itempf item = t6629Map.get(itemitem).get(0);
		    t6629rec.t6629Rec.set(StringUtil.rawToString(item.getGenarea()));
		}else{
	           t6629rec.t6629Rec.set(SPACES);
	     goTo(GotoLabel.loop3140); 
		}
		if (isNE(t6629rec.agntstat,"Y")
		&& isNE(t6629rec.govtstat,"Y")) {
			goTo(GotoLabel.loop3140);
		}
		if (isEQ(wsaaGovtstat,"Y")
		&& isNE(t6629rec.govtstat,"Y")) {
			goTo(GotoLabel.loop3140);
		}
		if (isEQ(wsaaAgntstat,"Y")
		&& isNE(t6629rec.agntstat,"Y")) {
			goTo(GotoLabel.loop3140);
		}
		generateMovements3300();
		writeSttrRecord3800();
	}

protected void next3150()
	{
		goTo(GotoLabel.loop3140);
	}

protected void next3160()
	{
		wsaaAnbAtCcd.set(ZERO);
		wsaaSumins.set(ZERO);
		wsaaRiskCessTerm.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaInstprem.set(ZERO);
	}

protected void readAglfRecord3200()
	{
		/*START*/
		aglfIO.setDataArea(SPACES);
		aglfIO.setAgntcoy(lifsttrrec.chdrcoy);
		aglfIO.setAgntnum(wsaaChdrAgntnum);
		aglfIO.setFormat(aglfrec);
		aglfIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, aglfIO);
		if (isNE(aglfIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(aglfIO.getParams());
			syserrrec.statuz.set(aglfIO.getStatuz());
			//dbError580(); ILIFE-5114
		}
		/*EXIT*/
	}

protected void generateMovements3300()
	{
        start3310();
        govt3350();
	}

protected void start3310()
 {
        wsaaBandage.set(SPACES);
        wsaaBandsa.set(SPACES);
        wsaaBandtrm.set(SPACES);
        wsaaBandprm.set(SPACES);
        wsaaAgntage.set(SPACES);
        wsaaAgntsa.set(SPACES);
        wsaaAgnttrm.set(SPACES);
        wsaaAgntprm.set(SPACES);
        if (isNE(wsaaAgntstat, "Y")) {
            wsaaStcmth.set(ZERO);
            return;
        }

        if (t6627Map != null && t6627Map.containsKey(t6629rec.ageband01.toString())) {
            Itempf item = t6627Map.get(t6629rec.ageband01.toString()).get(0);
            t6627rec.t6627Rec.set(StringUtil.rawToString(item.getGenarea()));
            accumulateAge4000();
        } else {
            t6627rec.t6627Rec.set(SPACES);
        }

        if (t6627Map != null && t6627Map.containsKey(t6629rec.sumband01.toString())) {
            Itempf item = t6627Map.get(t6629rec.sumband01.toString()).get(0);
            t6627rec.t6627Rec.set(StringUtil.rawToString(item.getGenarea()));
            accumulateSum4100();
        } else {
            t6627rec.t6627Rec.set(SPACES);
        }

        if (t6627Map != null && t6627Map.containsKey(t6629rec.rskband01.toString())) {
            Itempf item = t6627Map.get(t6629rec.rskband01.toString()).get(0);
            t6627rec.t6627Rec.set(StringUtil.rawToString(item.getGenarea()));
            accumulateRsk4200();
        } else {
            t6627rec.t6627Rec.set(SPACES);
        }

        if (t6627Map != null && t6627Map.containsKey(t6629rec.prmband01.toString())) {
            Itempf item = t6627Map.get(t6629rec.prmband01.toString()).get(0);
            t6627rec.t6627Rec.set(StringUtil.rawToString(item.getGenarea()));
            accumulatePrm4300();
        } else {
            t6627rec.t6627Rec.set(SPACES);
        }

        if (isEQ(lifsttrrec.minorChg, "Y")) {
            wsaaStsmth.set(wsaaAnnprem);
        } else {
            wsaaStpmth.set(wsaaAnnprem);
        }
        wsaaAgntage.set(wsaaBandage);
        wsaaAgntsa.set(wsaaBandsa);
        wsaaAgntprm.set(wsaaBandprm);
        wsaaAgnttrm.set(wsaaBandtrm);
    }

protected void govt3350()
 {
        wsaaBandage.set(SPACES);
        wsaaBandsa.set(SPACES);
        wsaaBandtrm.set(SPACES);
        wsaaBandprm.set(SPACES);
        wsaaGovtage.set(SPACES);
        wsaaGovtsa.set(SPACES);
        wsaaGovttrm.set(SPACES);
        wsaaGovtprm.set(SPACES);
        if (isNE(wsaaGovtstat, "Y")) {
            wsaaStcmthg.set(ZERO);
            wsaaStlmth.set(ZERO);
            return;
        }

        if (t6627Map != null && t6627Map.containsKey(t6629rec.ageband02.toString())) {
            Itempf item = t6627Map.get(t6629rec.ageband02.toString()).get(0);
            t6627rec.t6627Rec.set(StringUtil.rawToString(item.getGenarea()));
            accumulateAge4000();
        } else {
            t6627rec.t6627Rec.set(SPACES);
        }

        if (t6627Map != null && t6627Map.containsKey(t6629rec.sumband02.toString())) {
            Itempf item = t6627Map.get(t6629rec.sumband02.toString()).get(0);
            t6627rec.t6627Rec.set(StringUtil.rawToString(item.getGenarea()));
            accumulateSum4100();
        } else {
            t6627rec.t6627Rec.set(SPACES);
        }

        wsaaStimth.set(wsaaSumins);
        if (t6627Map != null && t6627Map.containsKey(t6629rec.rskband02.toString())) {
            Itempf item = t6627Map.get(t6629rec.rskband02.toString()).get(0);
            t6627rec.t6627Rec.set(StringUtil.rawToString(item.getGenarea()));
            accumulateRsk4200();
        } else {
            t6627rec.t6627Rec.set(SPACES);
        }

        if (t6627Map != null && t6627Map.containsKey(t6629rec.prmband02.toString())) {
            Itempf item = t6627Map.get(t6629rec.prmband02.toString()).get(0);
            t6627rec.t6627Rec.set(StringUtil.rawToString(item.getGenarea()));
            if (isEQ(wsaaValidflag, "2") && isEQ(wsaaHdrChg, "Y")) {
                compute(wsaaPremium, 2).set((mult(wsaaInstprem, wsaaBillfreqPrev)));
            } else {
                compute(wsaaPremium, 2).set((mult(wsaaInstprem, wsaaBillfreq)));
            }
            accumulatePrm4300();
        } else {
            t6627rec.t6627Rec.set(SPACES);
        }
        if (isEQ(wsaaValidflag, "2") && isEQ(wsaaHdrChg, "Y")) {
            compute(wsaaPremium, 2).set((mult(wsaaInstprem, wsaaBillfreqPrev)));
        } else {
            compute(wsaaPremium, 2).set((mult(wsaaInstprem, wsaaBillfreq)));
        }
        wsaaStsmthg.set(covrsts.getSingp());
        wsaaStpmthg.set(wsaaPremium);
        wsaaStpmthg.add(wsaaHdrPremium);
        wsaaGovtage.set(wsaaBandage);
        wsaaGovtsa.set(wsaaBandsa);
        wsaaGovtprm.set(wsaaBandprm);
        wsaaGovttrm.set(wsaaBandtrm);
    }

protected void headerPremium3400()
	{
		/*START*/
		compute(wsaaHdrPremium, 2).set((mult((add(add(wsaaSinstamt02,wsaaSinstamt03),wsaaSinstamt04)),wsaaBillfreq)));
		/*EXIT*/
	}
// done
protected void processAgents3600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3610();
					read3615();
				}
				case loop3620: {
					loop3620();
				}
				case next3650: {
					next3650();
				}
				case cont3660: {
					cont3660();
				}
				case next3670: {
					next3670();
				}
				case exit3695: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3610()
 {
        wsaaStcmthg.set(ZERO);
        wsaaStlmth.set(ZERO);
        wsaaStraamt = BigDecimal.ZERO;
        wsaaStbmthg.set(ZERO);
        wsaaStldmthg.set(ZERO);
        wsaaStpmthg.set(ZERO);
        wsaaStsmthg.set(ZERO);
        wsaaStimth.set(ZERO);
        wsaaStumth.set(ZERO);
        wsaaStnmth.set(ZERO);
        wsaaAnnprem.set(ZERO);
        wsaaAnnTotPrem = BigDecimal.ZERO;
        wsaaAnnpremCurr = BigDecimal.ZERO;
        wsaaAnnpremPrev = BigDecimal.ZERO;
        wsaaCommission.set(ZERO);
        wsaaAnnTotCurrent = BigDecimal.ZERO;
        wsaaAnnCurrent = BigDecimal.ZERO;
        wsaaAnnPrevious = BigDecimal.ZERO;

        List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlanSuffix(), "1");
        if (agcmstsList != null) {
            agcmstsIterator = agcmstsList.iterator();
        }
    }

protected void read3615()
	{
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont3660);
        }
        agcmsts = agcmstsIterator.next();
    	wsaaAgntnum.set(agcmsts.getAgntnum());
   }

protected void loop3620()
	{
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont3660);
        }
		if (isNE(agcmsts.getAgntnum(),wsaaAgntnum)) {
			goTo(GotoLabel.cont3660);
		}
		wsaaOvrdcat.set(agcmsts.getOvrdcat());
		if (isNE(agcmsts.getDormflag(),"Y")) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsts.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsts.getAnnprem());
		}
		if (isNE(agcmsts.getTranno(), covrsts.getTranno())) {
			goTo(GotoLabel.next3650);
		}
		if (isNE(agcmsts.getDormflag(),"Y")) {
			wsaaAnnpremCurr = wsaaAnnpremCurr.add(agcmsts.getAnnprem());
			wsaaAnnCurrent = wsaaAnnCurrent.add(agcmsts.getInitcom());
		}
		else {
			wsaaAnnpremPrev = wsaaAnnpremPrev.add(agcmsts.getAnnprem());
			wsaaAnnPrevious = wsaaAnnPrevious.add(agcmsts.getInitcom());
		}
	}

protected void next3650()
	{
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont3660);
        }
        agcmsts = agcmstsIterator.next();
        goTo(GotoLabel.loop3620);
	}

protected void cont3660()
	{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0) {
			goTo(GotoLabel.next3670);
		}
		wsaaCommission.set(wsaaAnnTotCurrent);
		wsaaAnnprem.set(wsaaAnnTotPrem);
		wsaaValidflag.set("1");
		updateRecord3100();
		
		if (covrstaList == null || covrstaList.isEmpty()){
			goTo(GotoLabel.next3670);
		}
		if ((BigDecimal.ZERO.compareTo(wsaaAnnpremPrev) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnPrevious) == 0)) {
			compute(wsaaAnnprem, 2).set((sub(wsaaAnnTotPrem,wsaaAnnpremCurr)));
			compute(wsaaCommission, 2).set((sub(wsaaAnnTotCurrent,wsaaAnnCurrent)));
		}
		else {
			compute(wsaaAnnprem, 2).set((add(wsaaAnnTotPrem,wsaaAnnpremPrev)));
			compute(wsaaCommission, 2).set((add(wsaaAnnTotCurrent,wsaaAnnPrevious)));
		}
		wsaaValidflag.set("2");
		updateRecord3100();
	}

protected void next3670()
	{
		wsaaAnnprem.set(ZERO);
		wsaaCommission.set(ZERO);
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		if(agcmsts != null){
		    wsaaOvrdcat.set(agcmsts.getOvrdcat());
		}
		if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
			wsaaAgntnum.set(ZERO);
			wsaaOvrdcat.set(SPACES);
			goTo(GotoLabel.exit3695);
		}
		wsaaAgntnum.set(agcmsts.getAgntnum());
		wsaaStcmthg.set(ZERO);
		wsaaStlmth.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg.set(ZERO);
		wsaaStsmthg.set(ZERO);
		wsaaStimth.set(ZERO);
		wsaaStumth.set(ZERO);
		wsaaStnmth.set(ZERO);
		goTo(GotoLabel.loop3620);
	}

protected void validateCovrStatus3700()
	{
		/*PARA*/
		if (isEQ(t5679rec.covRiskStat[wsaaIndex.toInt()],wsaaStatcode)) {
			for (wsaaIndex.set(1); !(isGT(wsaaIndex,12)
			|| isEQ(wsaaValidStatus,"Y")); wsaaIndex.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaIndex.toInt()],wsaaPstatcode)) {
					wsaaValidStatus.set("Y");
				}
			}
		}
		/*EXIT*/
	}

protected void writeSttrRecord3800()
	{
        start3810();
        write3850();
	}

protected void start3810()
 {
        sttrIO.setBandage(SPACES.toString());
        sttrIO.setBandageg(SPACES.toString());
        sttrIO.setBandsa(SPACES.toString());
        sttrIO.setBandsag(SPACES.toString());
        sttrIO.setBandprm(SPACES.toString());
        sttrIO.setBandprmg(SPACES.toString());
        sttrIO.setBandtrm(SPACES.toString());
        sttrIO.setCnPremStat(SPACES.toString());
        sttrIO.setRevtrcde(SPACES.toString());
        sttrIO.setBandtrmg(SPACES.toString());
        sttrIO.setCommyr(ZERO.intValue());
        sttrIO.setStmmth(BigDecimal.ZERO);
        sttrIO.setStumth(BigDecimal.ZERO);
        sttrIO.setStnmth(BigDecimal.ZERO);
        sttrIO.setStimth(BigDecimal.ZERO);
        sttrIO.setStvmth(BigDecimal.ZERO);
        sttrIO.setStsmth(BigDecimal.ZERO);
        sttrIO.setStsmthg(BigDecimal.ZERO);
        sttrIO.setStpmth(BigDecimal.ZERO);
        sttrIO.setStpmthg(BigDecimal.ZERO);
        sttrIO.setTranno(ZERO.intValue());
        sttrIO.setTrannor(ZERO.intValue());
        sttrIO.setBatcactyr(ZERO.intValue());
        sttrIO.setBatcactmn(ZERO.intValue());
        sttrIO.setPlanSuffix(ZERO.intValue());
        sttrIO.setStlmth(BigDecimal.ZERO);
        sttrIO.setStbmthg(BigDecimal.ZERO);
        sttrIO.setStldmthg(BigDecimal.ZERO);
        sttrIO.setStraamt(BigDecimal.ZERO);
        sttrIO.setTransactionDate(ZERO.intValue());
        sttrIO.setCnPremStat(wsaaChdrPstatcode.toString());
        sttrIO.setBatccoy(lifsttrrec.batccoy.toString());
        sttrIO.setBatcbrn(lifsttrrec.batcbrn.toString());
        sttrIO.setBatcactyr(lifsttrrec.batcactyr.toInt());
        sttrIO.setBatcactmn(lifsttrrec.batcactmn.toInt());
        sttrIO.setBatctrcde(lifsttrrec.batctrcde.toString());
        sttrIO.setBatcbatch(lifsttrrec.batcbatch.toString());
        sttrIO.setReptcd01(covrsts.getReptcd01());
        sttrIO.setReptcd02(covrsts.getReptcd02());
        sttrIO.setReptcd03(covrsts.getReptcd03());
        sttrIO.setReptcd04(covrsts.getReptcd04());
        sttrIO.setReptcd05(covrsts.getReptcd05());
        sttrIO.setReptcd06(covrsts.getReptcd06());
        sttrIO.setChdrpfx(wsaaChdrpfx.toString());
        sttrIO.setChdrcoy(covrsts.getChdrcoy());
        sttrIO.setChdrnum(covrsts.getChdrnum());
        sttrIO.setLife(covrsts.getLife());
        sttrIO.setCoverage(covrsts.getCoverage());
        sttrIO.setRider(covrsts.getRider());
        sttrIO.setPlanSuffix(covrsts.getPlanSuffix());
        sttrIO.setTranno(wsaaTranno.toInt());
        sttrIO.setAgntnum(wsaaAgntnum.toString());
        sttrIO.setOvrdcat(wsaaOvrdcat.toString());
        sttrIO.setAracde(aglfIO.getAracde().toString());
        sttrIO.setCntbranch(wsaaChdrCntbranch.toString());
        sttrIO.setCnttype(wsaaCnttype.toString());
        sttrIO.setCrtable(covrsts.getCrtable());
        wsaaCommYear.set(covrsts.getCrrcd());
        sttrIO.setCommyr(wsaaCommyr.toInt());
        sttrIO.setPstatcode(covrsts.getPstatcode());
        sttrIO.setCntcurr(covrsts.getPremCurrency());
        sttrIO.setRegister(wsaaChdrReg.toString());
        if (isNE(sttrIO.getCntcurr(), wsaaPrevCntcurr)) {
            a400ReadT3629(sttrIO.getCntcurr());
            wsaaPrevCntcurr.set(sttrIO.getCntcurr());
        }
        sttrIO.setAcctccy(t3629rec.ledgcurr.toString());
        sttrIO.setStatFund(covrsts.getStatFund());
        sttrIO.setStatSect(covrsts.getStatSect());
        sttrIO.setStatSubsect(covrsts.getStatSubsect());
        sttrIO.setSrcebus(wsaaSrcebus.toString());
        sttrIO.setBillfreq(wsaaCharBillfreq.toString());
        sttrIO.setSex(covrsts.getSex());
        sttrIO.setMortcls(covrsts.getMortcls());
        sttrIO.setChdrstcda(wsaaStatA.toString());
        sttrIO.setChdrstcdb(wsaaStatB.toString());
        sttrIO.setChdrstcdc(wsaaStatC.toString());
        sttrIO.setChdrstcdd(wsaaStatD.toString());
        sttrIO.setChdrstcde(wsaaStatE.toString());
        sttrIO.setStatcat(t6628rec.statcatg[wsaaSub.toInt()].toString());
        sttrIO.setTransactionDate(covrsts.getTransactionDate());
        sttrIO.setStatagt("N");
        sttrIO.setStatgov("N");
        if (isEQ(t6629rec.agntstat, "Y")) {
            if (isEQ(wsaaAgntstat, "Y")) {
                sttrIO.setStatagt("Y");
                wsaaStmmth.set(wsaaCommission);
                wsaaStimth.set(ZERO);
                sttrIO.setStimth(BigDecimal.ZERO);
                sttrIO.setBandage(wsaaAgntage.toString());
                sttrIO.setBandsa(wsaaAgntsa.toString());
                sttrIO.setBandtrm(wsaaAgnttrm.toString());
                sttrIO.setBandprm(wsaaAgntprm.toString());
            } else {
                sttrIO.setStatagt("N");
                wsaaStmmth.set(ZERO);
                sttrIO.setStimth(BigDecimal.ZERO);
                wsaaStsmth.set(ZERO);
                wsaaStpmth.set(ZERO);
                wsaaStvmth.set(ZERO);
                sttrIO.setBandage(SPACES.toString());
                sttrIO.setBandsa(SPACES.toString());
                sttrIO.setBandtrm(SPACES.toString());
                sttrIO.setBandprm(SPACES.toString());
            }
        }
        if (isEQ(t6629rec.govtstat, "Y")) {
            if (isEQ(wsaaGovtstat, "Y")) {
                sttrIO.setStatgov("Y");
                wsaaStimth.set(wsaaSumins);
                sttrIO.setBandageg(wsaaGovtage.toString());
                sttrIO.setBandsag(wsaaGovtsa.toString());
                sttrIO.setBandtrmg(wsaaGovttrm.toString());
                sttrIO.setBandprmg(wsaaGovtprm.toString());
            } else {
                sttrIO.setStatgov("N");
                wsaaStimth.set(ZERO);
                sttrIO.setStimth(BigDecimal.ZERO);
                wsaaStpmthg.set(ZERO);
                wsaaStsmthg.set(ZERO);
                wsaaStbmthg.set(ZERO);
                wsaaStldmthg.set(ZERO);
                wsaaStraamt = BigDecimal.ZERO;
                sttrIO.setBandageg(SPACES.toString());
                sttrIO.setBandsag(SPACES.toString());
                sttrIO.setBandtrmg(SPACES.toString());
                sttrIO.setBandprmg(SPACES.toString());
            }
        }
        if (isEQ(wsaaHdrAccum, "Y")) {
            wsaaStpmth.set(ZERO);
            wsaaStsmth.set(ZERO);
            wsaaStsmthg.set(ZERO);
            wsaaStimth.set(ZERO);
            wsaaStmmth.set(ZERO);
            wsaaStvmth.set(ZERO);
            wsaaStbmthg.set(ZERO);
            wsaaStldmthg.set(ZERO);
            wsaaStraamt = BigDecimal.ZERO;
            wsaaStpmthg.set(wsaaHdrPremium);
        }
        if (subtractPrevious.isTrue()) {

            sttrIO.setStcmth((mult(wsaaStcmth, -1)).getbigdata());
            sttrIO.setStvmth((mult(wsaaStvmth, -1)).getbigdata());
            sttrIO.setStpmth((mult(wsaaStpmth, -1)).getbigdata());
            sttrIO.setStsmth((mult(wsaaStsmth, -1)).getbigdata());
            sttrIO.setStmmth((mult(wsaaStmmth, -1)).getbigdata());
            sttrIO.setStimth((mult(wsaaStimth, -1)).getbigdata());
            sttrIO.setStcmthg((mult(wsaaStcmthg, -1)).getbigdata());
            sttrIO.setStpmthg((mult(wsaaStpmthg, -1)).getbigdata());
            sttrIO.setStlmth((mult(wsaaStlmth, -1)).getbigdata());
            sttrIO.setStbmthg((mult(wsaaStbmthg, -1)).getbigdata());
            sttrIO.setStldmthg((mult(wsaaStldmthg, -1)).getbigdata());
            sttrIO.setStraamt((mult(wsaaStraamt, -1)).getbigdata());
            sttrIO.setStsmthg((mult(wsaaStsmthg, -1)).getbigdata());
            sttrIO.setStumth((mult(wsaaStumth, -1)).getbigdata());
            sttrIO.setStnmth((mult(wsaaStnmth, -1)).getbigdata());
            return ;
        }
        if (subtractCurrent.isTrue()) {
            sttrIO.setStcmth((mult(wsaaStcmth, -1)).getbigdata());
            sttrIO.setStvmth((mult(wsaaStvmth, -1)).getbigdata());
            sttrIO.setStpmth((mult(wsaaStpmth, -1)).getbigdata());
            sttrIO.setStsmth((mult(wsaaStsmth, -1)).getbigdata());
            sttrIO.setStmmth((mult(wsaaStmmth, -1)).getbigdata());
            sttrIO.setStimth((mult(wsaaStimth, -1)).getbigdata());
            sttrIO.setStcmthg((mult(wsaaStcmthg, -1)).getbigdata());
            sttrIO.setStpmthg((mult(wsaaStpmthg, -1)).getbigdata());
            sttrIO.setStlmth((mult(wsaaStlmth, -1)).getbigdata());
            sttrIO.setStbmthg((mult(wsaaStbmthg, -1)).getbigdata());
            sttrIO.setStldmthg((mult(wsaaStldmthg, -1)).getbigdata());
            sttrIO.setStraamt((mult(wsaaStraamt, -1)).getbigdata());
            sttrIO.setStsmthg((mult(wsaaStsmthg, -1)).getbigdata());
            sttrIO.setStumth((mult(wsaaStumth, -1)).getbigdata());
            sttrIO.setStnmth((mult(wsaaStnmth, -1)).getbigdata());
            return ;
        }
        if (isEQ(wsaaValidflag, "1") && subtractIncrease.isTrue()) {

            sttrIO.setStcmth((mult(wsaaStcmth, -1)).getbigdata());
            sttrIO.setStvmth((mult(wsaaStvmth, -1)).getbigdata());
            sttrIO.setStpmth((mult(wsaaStpmth, -1)).getbigdata());
            sttrIO.setStsmth((mult(wsaaStsmth, -1)).getbigdata());
            sttrIO.setStmmth((mult(wsaaStmmth, -1)).getbigdata());
            sttrIO.setStimth((mult(wsaaStimth, -1)).getbigdata());
            sttrIO.setStcmthg((mult(wsaaStcmthg, -1)).getbigdata());
            sttrIO.setStpmthg((mult(wsaaStpmthg, -1)).getbigdata());
            sttrIO.setStlmth((mult(wsaaStlmth, -1)).getbigdata());
            sttrIO.setStbmthg((mult(wsaaStbmthg, -1)).getbigdata());
            sttrIO.setStldmthg((mult(wsaaStldmthg, -1)).getbigdata());
            sttrIO.setStraamt((mult(wsaaStraamt, -1)).getbigdata());
            sttrIO.setStsmthg((mult(wsaaStsmthg, -1)).getbigdata());
            sttrIO.setStumth((mult(wsaaStumth, -1)).getbigdata());
            sttrIO.setStnmth((mult(wsaaStnmth, -1)).getbigdata());
            return ;
        }
        if (isEQ(wsaaValidflag, "2") && addIncrease.isTrue()) {

            sttrIO.setStcmth((mult(wsaaStcmth, -1)).getbigdata());
            sttrIO.setStvmth((mult(wsaaStvmth, -1)).getbigdata());
            sttrIO.setStpmth((mult(wsaaStpmth, -1)).getbigdata());
            sttrIO.setStsmth((mult(wsaaStsmth, -1)).getbigdata());
            sttrIO.setStmmth((mult(wsaaStmmth, -1)).getbigdata());
            sttrIO.setStimth((mult(wsaaStimth, -1)).getbigdata());
            sttrIO.setStcmthg((mult(wsaaStcmthg, -1)).getbigdata());
            sttrIO.setStpmthg((mult(wsaaStpmthg, -1)).getbigdata());
            sttrIO.setStlmth((mult(wsaaStlmth, -1)).getbigdata());
            sttrIO.setStbmthg((mult(wsaaStbmthg, -1)).getbigdata());
            sttrIO.setStldmthg((mult(wsaaStldmthg, -1)).getbigdata());
            sttrIO.setStraamt((mult(wsaaStraamt, -1)).getbigdata());
            sttrIO.setStsmthg((mult(wsaaStsmthg, -1)).getbigdata());
            sttrIO.setStumth((mult(wsaaStumth, -1)).getbigdata());
            sttrIO.setStnmth((mult(wsaaStnmth, -1)).getbigdata());
            return ;
        }
        if (isEQ(wsaaValidflag, "1") && subtractDecrease.isTrue()) {

            sttrIO.setStcmth((mult(wsaaStcmth, -1)).getbigdata());
            sttrIO.setStvmth((mult(wsaaStvmth, -1)).getbigdata());
            sttrIO.setStpmth((mult(wsaaStpmth, -1)).getbigdata());
            sttrIO.setStsmth((mult(wsaaStsmth, -1)).getbigdata());
            sttrIO.setStmmth((mult(wsaaStmmth, -1)).getbigdata());
            sttrIO.setStimth((mult(wsaaStimth, -1)).getbigdata());
            sttrIO.setStcmthg((mult(wsaaStcmthg, -1)).getbigdata());
            sttrIO.setStpmthg((mult(wsaaStpmthg, -1)).getbigdata());
            sttrIO.setStlmth((mult(wsaaStlmth, -1)).getbigdata());
            sttrIO.setStbmthg((mult(wsaaStbmthg, -1)).getbigdata());
            sttrIO.setStldmthg((mult(wsaaStldmthg, -1)).getbigdata());
            sttrIO.setStraamt((mult(wsaaStraamt, -1)).getbigdata());
            sttrIO.setStsmthg((mult(wsaaStsmthg, -1)).getbigdata());
            sttrIO.setStumth((mult(wsaaStumth, -1)).getbigdata());
            sttrIO.setStnmth((mult(wsaaStnmth, -1)).getbigdata());
            return ;
        }
        if (isEQ(wsaaValidflag, "2") && addDecrease.isTrue()) {

            sttrIO.setStcmth((mult(wsaaStcmth, -1)).getbigdata());
            sttrIO.setStvmth((mult(wsaaStvmth, -1)).getbigdata());
            sttrIO.setStpmth((mult(wsaaStpmth, -1)).getbigdata());
            sttrIO.setStsmth((mult(wsaaStsmth, -1)).getbigdata());
            sttrIO.setStmmth((mult(wsaaStmmth, -1)).getbigdata());
            sttrIO.setStimth((mult(wsaaStimth, -1)).getbigdata());
            sttrIO.setStcmthg((mult(wsaaStcmthg, -1)).getbigdata());
            sttrIO.setStpmthg((mult(wsaaStpmthg, -1)).getbigdata());
            sttrIO.setStlmth((mult(wsaaStlmth, -1)).getbigdata());
            sttrIO.setStbmthg((mult(wsaaStbmthg, -1)).getbigdata());
            sttrIO.setStldmthg((mult(wsaaStldmthg, -1)).getbigdata());
            sttrIO.setStraamt((mult(wsaaStraamt, -1)).getbigdata());
            sttrIO.setStsmthg((mult(wsaaStsmthg, -1)).getbigdata());
            sttrIO.setStumth((mult(wsaaStumth, -1)).getbigdata());
            sttrIO.setStnmth((mult(wsaaStnmth, -1)).getbigdata());
            return ;
        }
        sttrIO.setStcmth(wsaaStcmth.getbigdata());
        sttrIO.setStvmth(wsaaStvmth.getbigdata());
        sttrIO.setStpmth(wsaaStpmth.getbigdata());
        sttrIO.setStsmth(wsaaStsmth.getbigdata());
        sttrIO.setStmmth(wsaaStmmth.getbigdata());
        sttrIO.setStimth(wsaaStimth.getbigdata());
        sttrIO.setStcmthg(wsaaStcmthg.getbigdata());
        sttrIO.setStpmthg(wsaaStpmthg.getbigdata());
        sttrIO.setStlmth(wsaaStlmth.getbigdata());
        sttrIO.setStbmthg(wsaaStbmthg.getbigdata());
        sttrIO.setStldmthg(wsaaStldmthg.getbigdata());
        sttrIO.setStraamt(wsaaStraamt);
        sttrIO.setStsmthg(wsaaStsmthg.getbigdata());
        sttrIO.setStumth(wsaaStumth.getbigdata());
        sttrIO.setStnmth(wsaaStnmth.getbigdata());
    }

protected void write3850()
	{
		if (isNE(wsaaBonusInd,SPACES)) {
			sttrIO.setParind("P");
		}
		else {
			if(t5540Map!=null&&t5540Map.containsKey(covrsts.getCrtable())){
			    sttrIO.setParind("P");
			}else{
			    sttrIO.setParind("N");
			}
		}
		wsaaSttrWritten.set("Y");
		if(insertSttrpfList == null){
		    insertSttrpfList = new ArrayList<Sttrpf>();
		}
		insertSttrpfList.add(new Sttrpf(sttrIO));
	}

// done
protected void agentChange3900()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start3910();
					read3915();
				}
				case loop3920: {
					loop3920();
				}
				case next3950: {
					next3950();
				}
				case cont3960: {
					cont3960();
				}
				case next3970: {
					next3970();
					call3975();
				}
				case loop3977: {
					loop3977();
				}
				case next3980: {
					next3980();
				}
				case write3985: {
					write3985();
				}
				case next3987: {
					next3987();
				}
				case exit3990: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3910()
 {
        wsaaStcmthg.set(ZERO);
        wsaaStlmth.set(ZERO);
        wsaaStbmthg.set(ZERO);
        wsaaStldmthg.set(ZERO);
        wsaaStraamt = BigDecimal.ZERO;
        wsaaStpmthg.set(ZERO);
        wsaaStsmthg.set(ZERO);
        wsaaStimth.set(ZERO);
        wsaaStumth.set(ZERO);
        wsaaStnmth.set(ZERO);
        wsaaCommission.set(ZERO);
        wsaaAnnprem.set(ZERO);
        wsaaAnnpremCurr = BigDecimal.ZERO;
        wsaaAnnpremPrev = BigDecimal.ZERO;
        wsaaAnnTotCurrent = BigDecimal.ZERO;
        wsaaAnnTotPrem = BigDecimal.ZERO;
        wsaaAnnCurrent = BigDecimal.ZERO;
        wsaaAnnPrevious = BigDecimal.ZERO;

        List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlanSuffix(), "1");

        if (agcmstsList != null) {
            agcmstsIterator = agcmstsList.iterator();
        }
    }

protected void read3915()
 {
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont3960);
        }
        agcmsts = agcmstsIterator.next();
        wsaaAgntnum.set(agcmsts.getAgntnum());
    }

protected void loop3920()
	{
		if (isNE(agcmsts.getTranno(),lifsttrrec.tranno)) {
			goTo(GotoLabel.next3950);
		}
        if (isNE(agcmsts.getAgntnum(),wsaaAgntnum)) {
            goTo(GotoLabel.cont3960);
        }
        wsaaOvrdcat.set(agcmsts.getOvrdcat());
        if (isNE(agcmsts.getDormflag(),"Y")) {
            wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsts.getInitcom());
            wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsts.getAnnprem());
        }
	}

protected void next3950()
	{
		if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
			goTo(GotoLabel.cont3960);
		}
		agcmsts = agcmstsIterator.next();
		goTo(GotoLabel.loop3920);
	}

protected void cont3960()
	{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
			goTo(GotoLabel.next3970);
		}
		wsaaCommission.set(wsaaAnnTotCurrent);
		wsaaAnnprem.set(wsaaAnnTotPrem);
		wsaaValidflag.set("1");
		updateRecord3100();
		wsaaStcmthg.set(ZERO);
		wsaaStlmth.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg.set(ZERO);
		wsaaStsmthg.set(ZERO);
		wsaaStimth.set(ZERO);
		wsaaStumth.set(ZERO);
		wsaaStnmth.set(ZERO);
		wsaaAnnprem.set(ZERO);
		wsaaCommission.set(ZERO);
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
			goTo(GotoLabel.next3970);
		}
		wsaaAgntnum.set(agcmsts.getAgntnum());
		wsaaOvrdcat.set(agcmsts.getOvrdcat());
		goTo(GotoLabel.loop3920);
	}

protected void next3970()
 {
        wsaaStcmthg.set(ZERO);
        wsaaStlmth.set(ZERO);
        wsaaStbmthg.set(ZERO);
        wsaaStldmthg.set(ZERO);
        wsaaStraamt = BigDecimal.ZERO;
        wsaaStpmthg.set(ZERO);
        wsaaStsmthg.set(ZERO);
        wsaaStimth.set(ZERO);
        wsaaStumth.set(ZERO);
        wsaaStnmth.set(ZERO);
        wsaaAnnprem.set(ZERO);
        wsaaAnnpremCurr = BigDecimal.ZERO;
        wsaaAnnpremPrev = BigDecimal.ZERO;
        wsaaAnnTotPrem = BigDecimal.ZERO;
        wsaaCommission.set(ZERO);
        wsaaAnnTotCurrent = BigDecimal.ZERO;
        wsaaAnnCurrent = BigDecimal.ZERO;
        wsaaAnnPrevious = BigDecimal.ZERO;
        wsaaAgntnum.set(ZERO);
        wsaaOvrdcat.set(SPACES);

        List<Agcmpf> agcmstaList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlanSuffix(), "2");

        if (agcmstaList != null) {
            agcmstaIterator = agcmstaList.iterator();
        }
    }

protected void call3975()
	{
		if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
			goTo(GotoLabel.write3985);
		}
		agcmsta = agcmstaIterator.next();
		wsaaAgntnum.set(agcmsta.getAgntnum());
		wsaaOvrdcat.set(agcmsta.getOvrdcat());
	}

protected void loop3977()
	{
		if ((agcmsta.getTrtm() != agcmsts.getTrtm())
		|| (agcmsta.getTrdt() != agcmsts.getTrdt())) {
			goTo(GotoLabel.next3980);
		}

		if (isNE(agcmsta.getAgntnum(),wsaaAgntnum)) {
			goTo(GotoLabel.write3985);
		}
		if (isNE(agcmsta.getDormflag(),"Y")) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsta.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsta.getAnnprem());
		}
	}

protected void next3980()
	{
        if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
            goTo(GotoLabel.write3985);
        }
        agcmsta = agcmstaIterator.next();
		goTo(GotoLabel.loop3977);
	}

protected void write3985()
	{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent)==0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
			goTo(GotoLabel.next3987);
		}
		wsaaAnnprem.set(wsaaAnnTotPrem);
		wsaaCommission.set(wsaaAnnTotCurrent);
		wsaaValidflag.set("2");
		updateRecord3100();
	}

protected void next3987()
	{
        if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
            goTo(GotoLabel.exit3990);
        }
		wsaaStcmthg.set(ZERO);
		wsaaStlmth.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg.set(ZERO);
		wsaaStsmthg.set(ZERO);
		wsaaStimth.set(ZERO);
		wsaaStumth.set(ZERO);
		wsaaStnmth.set(ZERO);
		wsaaAnnprem.set(ZERO);
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaCommission.set(ZERO);
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaOvrdcat.set(agcmsta.getOvrdcat());
		wsaaAgntnum.set(agcmsta.getAgntnum());
		goTo(GotoLabel.loop3977);
	}

protected void accumulateAge4000()
	{
		/*START*/
		wsaaBandage.set(SPACES);
		wsaaSub1.set(1);
		while ( !(isEQ(wsaaSub1,16))) {
			if ((isEQ(wsaaAnbAtCcd,t6627rec.statfrom[wsaaSub1.toInt()])
			|| isGT(wsaaAnbAtCcd,t6627rec.statfrom[wsaaSub1.toInt()]))
			&& (isEQ(wsaaAnbAtCcd,t6627rec.statto[wsaaSub1.toInt()])
			|| isLT(wsaaAnbAtCcd,t6627rec.statto[wsaaSub1.toInt()]))) {
				wsaaBandage.set(t6627rec.statband[wsaaSub1.toInt()]);
				wsaaSub1.set(16);
			}
			else {
				wsaaSub1.add(1);
				wsaaBandage.set(SPACES);
			}
		}
		
		/*EXIT*/
	}

protected void accumulateSum4100()
	{
		/*START*/
		wsaaBandsa.set(SPACES);
		wsaaSub1.set(1);
		while ( !(isEQ(wsaaSub1,16))) {
			if ((isEQ(wsaaSumins,t6627rec.statfrom[wsaaSub1.toInt()])
			|| isGT(wsaaSumins,t6627rec.statfrom[wsaaSub1.toInt()]))
			&& (isEQ(wsaaSumins,t6627rec.statto[wsaaSub1.toInt()])
			|| isLT(wsaaSumins,t6627rec.statto[wsaaSub1.toInt()]))) {
				wsaaBandsa.set(t6627rec.statband[wsaaSub1.toInt()]);
				wsaaSub1.set(16);
			}
			else {
				wsaaSub1.add(1);
				wsaaBandsa.set(SPACES);
			}
		}
		
		/*EXIT*/
	}

protected void accumulateRsk4200()
	{
		/*START*/
		wsaaBandtrm.set(SPACES);
		wsaaSub1.set(1);
		while ( !(isEQ(wsaaSub1,16))) {
			if ((isEQ(wsaaRiskCessTerm,t6627rec.statfrom[wsaaSub1.toInt()])
			|| isGT(wsaaRiskCessTerm,t6627rec.statfrom[wsaaSub1.toInt()]))
			&& (isEQ(wsaaRiskCessTerm,t6627rec.statto[wsaaSub1.toInt()])
			|| isLT(wsaaRiskCessTerm,t6627rec.statto[wsaaSub1.toInt()]))) {
				wsaaBandtrm.set(t6627rec.statband[wsaaSub1.toInt()]);
				wsaaSub1.set(16);
			}
			else {
				wsaaSub1.add(1);
				wsaaBandtrm.set(SPACES);
			}
		}
		
		/*EXIT*/
	}

protected void accumulatePrm4300()
	{
		/*START*/
		wsaaBandprm.set(SPACES);
		wsaaSub1.set(1);
		while ( !(isEQ(wsaaSub1,16))) {
			if ((isEQ(wsaaPremium,t6627rec.statfrom[wsaaSub1.toInt()])
			|| isGT(wsaaPremium,t6627rec.statfrom[wsaaSub1.toInt()]))
			&& (isEQ(wsaaPremium,t6627rec.statto[wsaaSub1.toInt()])
			|| isLT(wsaaPremium,t6627rec.statto[wsaaSub1.toInt()]))) {
				wsaaBandprm.set(t6627rec.statband[wsaaSub1.toInt()]);
				wsaaSub1.set(16);
			}
			else {
				wsaaSub1.add(1);
				wsaaBandprm.set(SPACES);
			}
		}
		
		/*EXIT*/
	}
// done
protected void agentCommChange4400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start4410();
					read4415();
				}
				case check4420: {
					check4420();
				}
				case next4450: {
					next4450();
				}
				case exit4490: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start4410()
	{
        List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlanSuffix(), "1");
        
        if(agcmstsList!=null){
            agcmstsIterator = agcmstsList.iterator();
        }
	}

protected void read4415()
	{
	    if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
	        goTo(GotoLabel.exit4490);
	    }
	    agcmsts = agcmstsIterator.next();
		wsaaAgntnum.set(agcmsts.getAgntnum());
	}

protected void check4420()
	{
		if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.exit4490);
        }
		if (isLTE(agcmsts.getTranno(),covrsts.getTranno())) {
			goTo(GotoLabel.next4450);
		}
		if (isNE(agcmsts.getTranno(),lifsttrrec.tranno)) {
			goTo(GotoLabel.next4450);
		}
		singlePremAgents5100();
	}

protected void next4450()
	{
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.exit4490);
        }
        agcmsts = agcmstsIterator.next();
		goTo(GotoLabel.check4420);
	}

protected void singlePremAgents5100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5110();
					read5115();
				}
				case loop5120: {
					loop5120();
				}
				case next5150: {
					next5150();
				}
				case cont5160: {
					cont5160();
				}
				case next5170: {
					next5170();
				}
				case exit5195: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5110()
	{
		wsaaStcmthg.set(ZERO);
		wsaaStlmth.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg.set(ZERO);
		wsaaStsmthg.set(ZERO);
		wsaaStimth.set(ZERO);
		wsaaStumth.set(ZERO);
		wsaaStnmth.set(ZERO);
		wsaaStcmth.set(ZERO);
		wsaaAnnprem.set(ZERO);
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaCommission.set(ZERO);
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		
        List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlanSuffix(), "1");
        
        if(agcmstsList!=null){
            agcmstsIterator = agcmstsList.iterator();
        }
	}

protected void read5115()
	{
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont5160);
        }
        agcmsts = agcmstsIterator.next();
		wsaaAgntnum.set(agcmsts.getAgntnum());
	}

protected void loop5120()
	{
		if (isNE(agcmsts.getTranno(),lifsttrrec.tranno)) {
			goTo(GotoLabel.next5150);
		}
		if (isNE(agcmsts.getAgntnum(),wsaaAgntnum)) {
			goTo(GotoLabel.cont5160);
		}
		wsaaOvrdcat.set(agcmsts.getOvrdcat());
		if (isNE(agcmsts.getDormflag(),"Y")) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsts.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsts.getAnnprem());
		}
		if (isNE(agcmsts.getDormflag(),"Y")) {
			wsaaAnnpremCurr = wsaaAnnpremCurr.add(agcmsts.getAnnprem());
			wsaaAnnCurrent = wsaaAnnCurrent.add(agcmsts.getInitcom());
		}
	}

protected void next5150()
	{
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont5160);
        }
        agcmsts = agcmstsIterator.next();
		goTo(GotoLabel.loop5120);
	}

protected void cont5160()
	{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
			goTo(GotoLabel.next5170);
		}
		wsaaCommission.set(wsaaAnnTotCurrent);
		wsaaAnnprem.set(wsaaAnnTotPrem);
		wsaaValidflag.set("1");
		wsaaAgntstat = "Y";
		wsaaGovtstat = "N";
		updateRecord3100();
		compute(wsaaAnnprem, 2).set((sub(wsaaAnnTotPrem,wsaaAnnpremCurr)));
		compute(wsaaCommission, 2).set((sub(wsaaAnnTotCurrent,wsaaAnnCurrent)));
		wsaaValidflag.set("2");
		wsaaAgntstat = "Y";
		wsaaGovtstat = "N";
		updateRecord3100();
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnprem.set(ZERO);
		wsaaCommission.set(ZERO);
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
	}

protected void next5170()
	{
		wsaaOvrdcat.set(agcmsts.getOvrdcat());
		if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
			wsaaCommission.set(ZERO);
			wsaaAnnTotPrem = BigDecimal.ZERO;
			wsaaAnnpremCurr = BigDecimal.ZERO;
			wsaaAnnpremPrev = BigDecimal.ZERO;
			wsaaAnnprem.set(ZERO);
			wsaaAnnTotCurrent = BigDecimal.ZERO;
			wsaaAnnCurrent = BigDecimal.ZERO;
			wsaaAnnPrevious = BigDecimal.ZERO;
			wsaaAgntnum.set(ZERO);
			wsaaOvrdcat.set(SPACES);
			goTo(GotoLabel.exit5195);
		}
		wsaaAgntnum.set(agcmsts.getAgntnum());
		wsaaStcmthg.set(ZERO);
		wsaaStlmth.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg.set(ZERO);
		wsaaStsmthg.set(ZERO);
		wsaaStimth.set(ZERO);
		wsaaStumth.set(ZERO);
		wsaaStnmth.set(ZERO);
		wsaaCommission.set(ZERO);
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnprem.set(ZERO);
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		goTo(GotoLabel.loop5120);
	}

protected void previousHeaderPremium5400()
	{
		/*START*/
		compute(wsaaHdrPremium, 2).set((mult((add(add(chdrIO.getSinstamt02(),chdrIO.getSinstamt03()),chdrIO.getSinstamt04())),wsaaBillfreqPrev)));
		/*EXIT*/
	}
// done
protected void processAgents5500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					start5510();
					read5515();
				}
				case loop5520: {
					loop5520();
				}
				case next5550: {
					next5550();
				}
				case cont5560: {
					cont5560();
				}
				case next5570: {
					next5570();
					call5575();
				}
				case loop5577: {
					loop5577();
				}
				case next5580: {
					next5580();
				}
				case write5585: {
					write5585();
				}
				case next5587: {
					next5587();
				}
				case exit5590: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start5510()
	{
		wsaaStcmthg.set(ZERO);
		wsaaStlmth.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg.set(ZERO);
		wsaaStsmthg.set(ZERO);
		wsaaStimth.set(ZERO);
		wsaaStumth.set(ZERO);
		wsaaStnmth.set(ZERO);
		wsaaCommission.set(ZERO);
		wsaaAnnprem.set(ZERO);
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		
        List<Agcmpf> agcmstsList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlanSuffix(), "1");
        
        if(agcmstsList!=null){
            agcmstsIterator = agcmstsList.iterator();
        }
	}

protected void read5515()
	{
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont5560);
        }
        agcmsts = agcmstsIterator.next();
		wsaaAgntnum.set(agcmsts.getAgntnum());
	}

protected void loop5520()
	{
		if (isNE(agcmsts.getTranno(),lifsttrrec.tranno)) {
			goTo(GotoLabel.next5550);
		}
		if (isNE(agcmsts.getAgntnum(),wsaaAgntnum)) {
			goTo(GotoLabel.cont5560);
		}
		wsaaOvrdcat.set(agcmsts.getOvrdcat());
		wsaaTransDate.set(agcmsts.getTrdt());
		wsaaTransTime.set(agcmsts.getTrtm());
		if (isNE(agcmsts.getDormflag(),"Y")) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsts.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsts.getAnnprem());
		}
	}

protected void next5550()
	{
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.cont5560);
        }
        agcmsts = agcmstsIterator.next();
		goTo(GotoLabel.loop5520);
	}

protected void cont5560()
	{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
			goTo(GotoLabel.next5570);
		}
		wsaaCommission.set(wsaaAnnTotCurrent);
		wsaaAnnprem.set(wsaaAnnTotPrem);
		wsaaValidflag.set("1");
		updateRecord3100();
		wsaaStcmthg.set(ZERO);
		wsaaStlmth.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg.set(ZERO);
		wsaaStsmthg.set(ZERO);
		wsaaStimth.set(ZERO);
		wsaaStumth.set(ZERO);
		wsaaStnmth.set(ZERO);
		wsaaAnnprem.set(ZERO);
		wsaaCommission.set(ZERO);
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnpremCurr = BigDecimal.ZERO;
        if (agcmstsIterator == null || !agcmstsIterator.hasNext()) {
            goTo(GotoLabel.next5570);
        }
		wsaaAgntnum.set(agcmsts.getAgntnum());
		wsaaOvrdcat.set(agcmsts.getOvrdcat());
		goTo(GotoLabel.loop5520);
	}

protected void next5570()
	{
		wsaaStcmthg.set(ZERO);
		wsaaStlmth.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg.set(ZERO);
		wsaaStsmthg.set(ZERO);
		wsaaStimth.set(ZERO);
		wsaaStumth.set(ZERO);
		wsaaStnmth.set(ZERO);
		wsaaAnnprem.set(ZERO);
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaCommission.set(ZERO);
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaAgntnum.set(ZERO);
		wsaaOvrdcat.set(SPACES);
		
        List<Agcmpf> agcmstaList = agcmpfDAO.searchAgcmstRecord(covrsts.getChdrcoy(), covrsts.getChdrnum(),
                covrsts.getLife(), covrsts.getCoverage(), covrsts.getRider(), covrsts.getPlanSuffix(), "2");
        
        if(agcmstaList!=null){
            agcmstaIterator = agcmstaList.iterator();
        }
	}

protected void call5575()
	{
        if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
            goTo(GotoLabel.write5585);
        }
        agcmsta = agcmstaIterator.next();
		wsaaAgntnum.set(agcmsta.getAgntnum());
		wsaaOvrdcat.set(agcmsta.getOvrdcat());
	}

protected void loop5577()
	{
		if (isNE(agcmsta.getTrtm(), wsaaTransTime)
		|| isNE(agcmsta.getTrdt(), wsaaTransDate)) {
			goTo(GotoLabel.next5580);
		}
		if (isNE(agcmsta.getAgntnum(),wsaaAgntnum)) {
			goTo(GotoLabel.write5585);
		}
		if (isNE(agcmsta.getDormflag(),"Y")) {
		    wsaaAnnTotCurrent = wsaaAnnTotCurrent.add(agcmsta.getInitcom());
			wsaaAnnTotPrem = wsaaAnnTotPrem.add(agcmsta.getAnnprem());
		}
	}

protected void next5580()
	{
        if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
            goTo(GotoLabel.write5585);
        }
        agcmsta = agcmstaIterator.next();
		goTo(GotoLabel.loop5577);
	}

protected void write5585()
	{
		if (BigDecimal.ZERO.compareTo(wsaaAnnTotCurrent) == 0
		&& BigDecimal.ZERO.compareTo(wsaaAnnTotPrem) == 0) {
			goTo(GotoLabel.next5587);
		}
		wsaaAnnprem.set(wsaaAnnTotPrem);
		wsaaCommission.set(wsaaAnnTotCurrent);
		wsaaValidflag.set("2");
		updateRecord3100();
	}

protected void next5587()
	{
		if (agcmstaIterator == null || !agcmstaIterator.hasNext()) {
			goTo(GotoLabel.exit5590);
		}
		wsaaStcmthg.set(ZERO);
		wsaaStlmth.set(ZERO);
		wsaaStbmthg.set(ZERO);
		wsaaStldmthg.set(ZERO);
		wsaaStraamt = BigDecimal.ZERO;
		wsaaStpmthg.set(ZERO);
		wsaaStsmthg.set(ZERO);
		wsaaStimth.set(ZERO);
		wsaaStumth.set(ZERO);
		wsaaStnmth.set(ZERO);
		wsaaAnnprem.set(ZERO);
		wsaaAnnpremCurr = BigDecimal.ZERO;
		wsaaAnnpremPrev = BigDecimal.ZERO;
		wsaaAnnTotPrem = BigDecimal.ZERO;
		wsaaCommission.set(ZERO);
		wsaaAnnTotCurrent = BigDecimal.ZERO;
		wsaaAnnCurrent = BigDecimal.ZERO;
		wsaaAnnPrevious = BigDecimal.ZERO;
		wsaaOvrdcat.set(agcmsta.getOvrdcat());
		wsaaAgntnum.set(agcmsta.getAgntnum());
		goTo(GotoLabel.loop5577);
	}

protected void checkTranscation6000()
	{      
        itemIO.setParams(SPACES);
        itemIO.setItemitem(lifsttrrec.batctrcde);
        readTableTj6887000();
        if (isNE(itemIO.getStatuz(),varcom.oK)) {
            return;
        }
        a200ReversalTrans();
	}

protected void readTableTj6887000()
	{
        itemIO.setItempfx("IT");
        itemIO.setItemcoy(lifsttrrec.chdrcoy);
        itemIO.setItemtabl(tj688);
        itemIO.setFormat(itemrec);
        itemIO.setFunction(varcom.readr);
        SmartFileCode.execute(appVars, itemIO);
        if (isNE(itemIO.getStatuz(),varcom.oK)
        && isNE(itemIO.getStatuz(),varcom.mrnf)) {
            syserrrec.params.set(itemIO.getParams());
            syserrrec.statuz.set(itemIO.getStatuz());
            dbError580();
        }
        if (isEQ(itemIO.getStatuz(),varcom.oK)) {
            tj688rec.tj688Rec.set(itemIO.getGenarea());
        }
        else {
            tj688rec.tj688Rec.set(SPACES);
        }

	}


protected void a100CheckNoflives()
 {
        wsaaLives.set(ZERO);
        List<Lifepf> lifeList = lifepfDAO.searchLifeRecordByChdrNum(lifsttrrec.chdrcoy.toString(),
                lifsttrrec.chdrnum.toString());
        if (lifeList != null && !lifeList.isEmpty()) {
            for (Lifepf lifeIO : lifeList) {
                if (isNE(lifeIO.getLife(), covrsts.getLife())) {
                    continue;
                }
                if (isEQ(lifeIO.getValidflag(), "2")) {
                    continue;
                }
                if (isEQ(lifeIO.getCurrfrom(), covrsts.getCrrcd()) || isNE(lifeIO.getStatcode(), "IF")) {
                    wsaaLives.add(1);
                }
            }
        }
    }


protected void a200ReversalTrans()
	{
        a200Para();
        a200Next();
	}

protected void a200Para()
	{
		wsaaLastTranno.set(sub(lifsttrrec.tranno,1));
		sttrrevList = sttrpfDAO.searchSttrpfRecord(lifsttrrec.chdrcoy.toString(), lifsttrrec.chdrnum.toString());
	}

protected void a200Next()
 {
        if (sttrrevList != null && !sttrrevList.isEmpty()) {
            for (Sttrpf sttrrevIO : sttrrevList) {
                if (isNE(sttrrevIO.getTranno(), wsaaLastTranno)) {
                    continue;
                }
                wsaaFound.set(SPACES);
                for (wsaaIdx.set(1); !(isGT(wsaaIdx, 15) || isEQ(wsaaFound, "Y")); wsaaIdx.add(1)) {
                    if (isEQ(sttrrevIO.getBatctrcde(), tj688rec.trcode[wsaaIdx.toInt()])) {
                        wsaaFound.set("Y");
                        a200CheckTrans(sttrrevIO);
                    }
                }
            }
        }
    }

protected void a200CheckTrans(Sttrpf sttrrevIO)
	{
		wsaaTransCode.set(sttrrevIO.getBatctrcde());
		wsaaStatcd.set("**");
        if (t6628Map != null && t6628Map.containsKey(wsaaT6628Item)) {
            t6628rec.t6628Rec.set(StringUtil.rawToString(t6628Map.get(wsaaT6628Item).get(0).getGenarea()));
        }else{
            t6628rec.t6628Rec.set(SPACES);
            return;
        }
		
		if (isEQ(t6628rec.statcatgs,SPACES)) {
			return;
		}
		wsaaFound.set(SPACES);
		for (wsaaIdx.set(1); !(isGT(wsaaIdx,88)
		|| isEQ(wsaaFound,"Y")); wsaaIdx.add(1)){
			if (isEQ(t6628rec.statcatg[wsaaIdx.toInt()],sttrrevIO.getStatcat())) {
				wsaaFound.set("Y");
			}
		}
		if (isNE(wsaaFound,"Y")) {
		    return;
		}
		sttrrevIO.setRevtrcde(sttrrevIO.getBatctrcde());
		sttrrevIO.setStcmth((mult(sttrrevIO.getStcmth(),-1)).getbigdata());
		sttrrevIO.setStvmth((mult(sttrrevIO.getStvmth(),-1)).getbigdata());
		sttrrevIO.setStpmth((mult(sttrrevIO.getStpmth(),-1)).getbigdata());
		sttrrevIO.setStsmth((mult(sttrrevIO.getStsmth(),-1)).getbigdata());
		sttrrevIO.setStmmth((mult(sttrrevIO.getStmmth(),-1)).getbigdata());
		sttrrevIO.setStimth((mult(sttrrevIO.getStimth(),-1)).getbigdata());
		sttrrevIO.setStcmthg((mult(sttrrevIO.getStcmthg(),-1)).getbigdata());
		sttrrevIO.setStpmthg((mult(sttrrevIO.getStpmthg(),-1)).getbigdata());
		sttrrevIO.setStsmthg((mult(sttrrevIO.getStsmthg(),-1)).getbigdata());
		sttrrevIO.setStumth((mult(sttrrevIO.getStumth(),-1)).getbigdata());
		sttrrevIO.setStnmth((mult(sttrrevIO.getStnmth(),-1)).getbigdata());
		sttrrevIO.setStlmth((mult(sttrrevIO.getStlmth(),-1)).getbigdata());
		sttrrevIO.setStbmthg((mult(sttrrevIO.getStbmthg(),-1)).getbigdata());
		sttrrevIO.setStldmthg((mult(sttrrevIO.getStldmthg(),-1)).getbigdata());
		sttrrevIO.setStraamt((mult(sttrrevIO.getStraamt(),-1)).getbigdata());
		sttrrevIO.setTranno(lifsttrrec.tranno.toInt());
		sttrrevIO.setBatccoy(lifsttrrec.batccoy.toString());
		sttrrevIO.setBatcbrn(lifsttrrec.batcbrn.toString());
		sttrrevIO.setBatcactyr(lifsttrrec.batcactyr.toInt());
		sttrrevIO.setBatcactmn(lifsttrrec.batcactmn.toInt());
		sttrrevIO.setBatcbatch(lifsttrrec.batcbatch.toString());
		sttrrevIO.setBatctrcde(lifsttrrec.batctrcde.toString());
		
		if(insertSttrrevList== null){
		    insertSttrrevList = new ArrayList<>();
		}
		insertSttrrevList.add(sttrrevIO);
		return;
	}

protected void a400ReadT3629(String cntcurr)
	{
        if(t3629Map != null && t3629Map.containsKey(cntcurr)){
            Itempf item = t3629Map.get(cntcurr).get(0);
            t3629rec.t3629Rec.set(StringUtil.rawToString(item.getGenarea()));
        }else{
            syserrrec.statuz.set("MRNF");
            dbError580();
        }
	}

// done
protected void a500ReadRacdsts()
 {
        List<Racdpf> racdstsList = racdpfDAO.searchRacdstRecord(lifsttrrec.batccoy.toString(),
                lifsttrrec.chdrnum.toString(), wsaaLife.toString(), wsaaCoverage.toString(), wsaaRider.toString(),
                wsaaPlanSuffix.toInt(), "1");
        if (racdstsList != null && !racdstsList.isEmpty()) {
            for (Racdpf r : racdstsList) {
                wsaaStraamt = wsaaStraamt.add(r.getRaAmount());
            }
        }
        if (BigDecimal.ZERO.compareTo(wsaaStraamt) == 0) {
            a800ReadRacdstb();
        }
    }

protected void a600ReadRacdsta()
 {
        wsaaRacdstaFirstTime = "Y";
        List<Racdpf> racdstaList = racdpfDAO.searchRacdstRecord(lifsttrrec.batccoy.toString(),
                lifsttrrec.chdrnum.toString(), wsaaLife.toString(), wsaaCoverage.toString(), wsaaRider.toString(),
                wsaaPlanSuffix.toInt(), "2");

        if (racdstaList != null && !racdstaList.isEmpty()) {
            for (Racdpf r : racdstaList) {
                if (isEQ(wsaaRacdstaFirstTime, "Y")) {
                    wsaaRacdstaLastTranno.set(r.getTranno());
                    wsaaRacdstaFirstTime = "N";
                } else {
                    if (isNE(r.getTranno(), wsaaRacdstaLastTranno)) {
                        break;
                    }
                }
                wsaaStraamt = wsaaStraamt.add(r.getRaAmount());
            }
        }
    }

protected void a800ReadRacdstb()
 {
        List<Racdpf> racdstbList = racdpfDAO.searchRacdstRecord(lifsttrrec.batccoy.toString(),
                lifsttrrec.chdrnum.toString(), wsaaLife.toString(), wsaaCoverage.toString(), wsaaRider.toString(),
                wsaaPlanSuffix.toInt(), "4");
        if (racdstbList != null && !racdstbList.isEmpty()) {
            for (Racdpf r : racdstbList) {
                if (isNE(r.getRecovamt(), ZERO)) {
                    wsaaStraamt = wsaaStraamt.add(r.getRecovamt());
                } else {
                    wsaaStraamt = wsaaStraamt.add(r.getRaAmount());
                }
            }
        }
    }
}
