package com.csc.life.statistics.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:46
 * Description:
 * Copybook name: TJ688REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tj688rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tj688Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData trcodes = new FixedLengthStringData(60).isAPartOf(tj688Rec, 0);
  	public FixedLengthStringData[] trcode = FLSArrayPartOfStructure(15, 4, trcodes, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(trcodes, 0, FILLER_REDEFINE);
  	public FixedLengthStringData trcode01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData trcode02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData trcode03 = new FixedLengthStringData(4).isAPartOf(filler, 8);
  	public FixedLengthStringData trcode04 = new FixedLengthStringData(4).isAPartOf(filler, 12);
  	public FixedLengthStringData trcode05 = new FixedLengthStringData(4).isAPartOf(filler, 16);
  	public FixedLengthStringData trcode06 = new FixedLengthStringData(4).isAPartOf(filler, 20);
  	public FixedLengthStringData trcode07 = new FixedLengthStringData(4).isAPartOf(filler, 24);
  	public FixedLengthStringData trcode08 = new FixedLengthStringData(4).isAPartOf(filler, 28);
  	public FixedLengthStringData trcode09 = new FixedLengthStringData(4).isAPartOf(filler, 32);
  	public FixedLengthStringData trcode10 = new FixedLengthStringData(4).isAPartOf(filler, 36);
  	public FixedLengthStringData trcode11 = new FixedLengthStringData(4).isAPartOf(filler, 40);
  	public FixedLengthStringData trcode12 = new FixedLengthStringData(4).isAPartOf(filler, 44);
  	public FixedLengthStringData trcode13 = new FixedLengthStringData(4).isAPartOf(filler, 48);
  	public FixedLengthStringData trcode14 = new FixedLengthStringData(4).isAPartOf(filler, 52);
  	public FixedLengthStringData trcode15 = new FixedLengthStringData(4).isAPartOf(filler, 56);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(440).isAPartOf(tj688Rec, 60, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tj688Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tj688Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}