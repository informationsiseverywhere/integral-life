package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:24
 * Description:
 * Copybook name: GVSTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Gvstkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData gvstFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData gvstKey = new FixedLengthStringData(64).isAPartOf(gvstFileKey, 0, REDEFINE);
  	public FixedLengthStringData gvstBatccoy = new FixedLengthStringData(1).isAPartOf(gvstKey, 0);
  	public FixedLengthStringData gvstBatcbrn = new FixedLengthStringData(2).isAPartOf(gvstKey, 1);
  	public PackedDecimalData gvstBatcactyr = new PackedDecimalData(4, 0).isAPartOf(gvstKey, 3);
  	public PackedDecimalData gvstBatcactmn = new PackedDecimalData(2, 0).isAPartOf(gvstKey, 6);
  	public FixedLengthStringData gvstBatctrcde = new FixedLengthStringData(4).isAPartOf(gvstKey, 8);
  	public FixedLengthStringData gvstBatcbatch = new FixedLengthStringData(5).isAPartOf(gvstKey, 12);
  	public FixedLengthStringData gvstAcctccy = new FixedLengthStringData(3).isAPartOf(gvstKey, 17);
  	public FixedLengthStringData gvstChdrnum = new FixedLengthStringData(8).isAPartOf(gvstKey, 20);
  	public FixedLengthStringData gvstLife = new FixedLengthStringData(2).isAPartOf(gvstKey, 28);
  	public FixedLengthStringData gvstCoverage = new FixedLengthStringData(2).isAPartOf(gvstKey, 30);
  	public FixedLengthStringData gvstRider = new FixedLengthStringData(2).isAPartOf(gvstKey, 32);
  	public PackedDecimalData gvstPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(gvstKey, 34);
  	public FixedLengthStringData filler = new FixedLengthStringData(27).isAPartOf(gvstKey, 37, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(gvstFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		gvstFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}