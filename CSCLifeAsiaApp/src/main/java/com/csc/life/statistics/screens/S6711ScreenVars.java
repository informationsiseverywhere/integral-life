package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6711
 * @version 1.0 generated on 30/08/09 06:59
 * @author Quipoz
 */
public class S6711ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(581);
	public FixedLengthStringData dataFields = new FixedLengthStringData(165).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctyr = DD.acctyr.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acmn = DD.acmn.copyToZonedDecimal().isAPartOf(dataFields,4);
	public ZonedDecimalData acyr = DD.acyr.copyToZonedDecimal().isAPartOf(dataFields,6);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(dataFields,12);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(dataFields,14);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,18);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,20);
	public ZonedDecimalData commyr = DD.commyr.copyToZonedDecimal().isAPartOf(dataFields,23);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData descrip = DD.descrip.copy().isAPartOf(dataFields,28);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,58);
	public FixedLengthStringData mthldesc = DD.mthldesc.copy().isAPartOf(dataFields,66);
	public FixedLengthStringData pstatcd = DD.pstatcd.copy().isAPartOf(dataFields,76);
	public FixedLengthStringData register = DD.register.copy().isAPartOf(dataFields,78);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(dataFields,81);
	public ZonedDecimalData stcmth = DD.stcmth.copyToZonedDecimal().isAPartOf(dataFields,83);
	public FixedLengthStringData statFund = DD.stfund.copy().isAPartOf(dataFields,92);
	public ZonedDecimalData stimth = DD.stimth.copyToZonedDecimal().isAPartOf(dataFields,93);
	public ZonedDecimalData stpmth = DD.stpmth.copyToZonedDecimal().isAPartOf(dataFields,111);
	public FixedLengthStringData statSect = DD.stsect.copy().isAPartOf(dataFields,129);
	public ZonedDecimalData stsmth = DD.stsmth.copyToZonedDecimal().isAPartOf(dataFields,131);
	public FixedLengthStringData stsubsect = DD.stsubsect.copy().isAPartOf(dataFields,149);
	public ZonedDecimalData transactionTime = DD.trtm.copyToZonedDecimal().isAPartOf(dataFields,153);
	public ZonedDecimalData user = DD.user.copyToZonedDecimal().isAPartOf(dataFields,159);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(104).isAPartOf(dataArea, 165);
	public FixedLengthStringData acctyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acmnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData acyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData bandageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bandprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bandsaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bandtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData commyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData descripErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData mthldescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData pstatcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData registerErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData statcatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	public FixedLengthStringData stcmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 68);
	public FixedLengthStringData stfundErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData stimthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 76);
	public FixedLengthStringData stpmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 80);
	public FixedLengthStringData stsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 84);
	public FixedLengthStringData stsmthErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 88);
	public FixedLengthStringData stsubsectErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 92);
	public FixedLengthStringData trtmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 96);
	public FixedLengthStringData userErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 100);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(312).isAPartOf(dataArea, 269);
	public FixedLengthStringData[] acctyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acmnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] acyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] bandageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bandprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bandsaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bandtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] commyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] descripOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] mthldescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] pstatcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] registerOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] statcatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public FixedLengthStringData[] stcmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 204);
	public FixedLengthStringData[] stfundOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 216);
	public FixedLengthStringData[] stimthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 228);
	public FixedLengthStringData[] stpmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 240);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 252);
	public FixedLengthStringData[] stsmthOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 264);
	public FixedLengthStringData[] stsubsectOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 276);
	public FixedLengthStringData[] trtmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 288);
	public FixedLengthStringData[] userOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 300);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);

	public LongData S6711screenWritten = new LongData(0);
	public LongData S6711protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6711ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(acctyrOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acmnOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(commyrOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, statcat, acctyr, statFund, statSect, stsubsect, register, cntbranch, bandage, bandsa, bandprm, bandtrm, pstatcd, cntcurr, acmn, acyr, descrip, effdate, transactionTime, user, mthldesc, commyr, stcmth, stpmth, stsmth, stimth};
		screenOutFields = new BaseData[][] {companyOut, statcatOut, acctyrOut, stfundOut, stsectOut, stsubsectOut, registerOut, cntbranchOut, bandageOut, bandsaOut, bandprmOut, bandtrmOut, pstatcdOut, cntcurrOut, acmnOut, acyrOut, descripOut, effdateOut, trtmOut, userOut, mthldescOut, commyrOut, stcmthOut, stpmthOut, stsmthOut, stimthOut};
		screenErrFields = new BaseData[] {companyErr, statcatErr, acctyrErr, stfundErr, stsectErr, stsubsectErr, registerErr, cntbranchErr, bandageErr, bandsaErr, bandprmErr, bandtrmErr, pstatcdErr, cntcurrErr, acmnErr, acyrErr, descripErr, effdateErr, trtmErr, userErr, mthldescErr, commyrErr, stcmthErr, stpmthErr, stsmthErr, stimthErr};
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6711screen.class;
		protectRecord = S6711protect.class;
	}

}
