package com.csc.life.statistics.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:45
 * @author Quipoz
 */
public class S6706screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 18, 5, 23, 15, 24, 16, 1, 2, 3, 12, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 22, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6706ScreenVars sv = (S6706ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6706screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6706ScreenVars screenVars = (S6706ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.agntsel.setClassString("");
		screenVars.statcat.setClassString("");
		screenVars.acctyr.setClassString("");
		screenVars.aracde.setClassString("");
		screenVars.cntbranch.setClassString("");
		screenVars.bandage.setClassString("");
		screenVars.bandsa.setClassString("");
		screenVars.bandprm.setClassString("");
		screenVars.bandtrm.setClassString("");
		screenVars.chdrtype.setClassString("");
		screenVars.crtable.setClassString("");
		screenVars.cntcurr.setClassString("");
		screenVars.stcmth.setClassString("");
		screenVars.stvmth.setClassString("");
		screenVars.stpmth.setClassString("");
		screenVars.stsmth.setClassString("");
		screenVars.stmmth.setClassString("");
		screenVars.descrip.setClassString("");
		screenVars.acyr.setClassString("");
		screenVars.acmn.setClassString("");
		screenVars.pstatcd.setClassString("");
		screenVars.stcmthAdj.setClassString("");
		screenVars.stvmthAdj.setClassString("");
		screenVars.stpmthAdj.setClassString("");
		screenVars.stsmthAdj.setClassString("");
		screenVars.stmmthAdj.setClassString("");
		screenVars.stcmtho.setClassString("");
		screenVars.stvmtho.setClassString("");
		screenVars.stpmtho.setClassString("");
		screenVars.stsmtho.setClassString("");
		screenVars.stmmtho.setClassString("");
		screenVars.mthldesc.setClassString("");
		screenVars.ovrdcat.setClassString("");
	}

/**
 * Clear all the variables in S6706screen
 */
	public static void clear(VarModel pv) {
		S6706ScreenVars screenVars = (S6706ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.agntsel.clear();
		screenVars.statcat.clear();
		screenVars.acctyr.clear();
		screenVars.aracde.clear();
		screenVars.cntbranch.clear();
		screenVars.bandage.clear();
		screenVars.bandsa.clear();
		screenVars.bandprm.clear();
		screenVars.bandtrm.clear();
		screenVars.chdrtype.clear();
		screenVars.crtable.clear();
		screenVars.cntcurr.clear();
		screenVars.stcmth.clear();
		screenVars.stvmth.clear();
		screenVars.stpmth.clear();
		screenVars.stsmth.clear();
		screenVars.stmmth.clear();
		screenVars.descrip.clear();
		screenVars.acyr.clear();
		screenVars.acmn.clear();
		screenVars.pstatcd.clear();
		screenVars.stcmthAdj.clear();
		screenVars.stvmthAdj.clear();
		screenVars.stpmthAdj.clear();
		screenVars.stsmthAdj.clear();
		screenVars.stmmthAdj.clear();
		screenVars.stcmtho.clear();
		screenVars.stvmtho.clear();
		screenVars.stpmtho.clear();
		screenVars.stsmtho.clear();
		screenVars.stmmtho.clear();
		screenVars.mthldesc.clear();
		screenVars.ovrdcat.clear();
	}
}
