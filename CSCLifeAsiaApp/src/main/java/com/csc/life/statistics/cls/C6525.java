/*
 * File: C6525.java
 * Date: 30 August 2009 2:58:59
 * Author: $Id$
 * 
 * Class transformed from C6525.CLP
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.cls;

import com.csc.life.statistics.batchprograms.B6525;
import com.quipoz.COBOLFramework.common.exception.ExtMsgException;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.util.QPUtilities;

public class C6525 extends COBOLConvCodeModel {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData params = new FixedLengthStringData(342);

	public C6525() {
		super();
	}


public void mainline(Object... parmArray)
		throws ExtMsgException
	{
		params = convertAndSetParam(params, parmArray, 0);
		final int QS_START = 0;
		final int QS_END = 99;
		int qState = 0;
		final int error = 1;
		final int returnVar = 2;
		while (qState != QS_END) {
			try {
				switch (qState) {
				case QS_START: {
					callProgram(B6525.class, new Object[] {params});
				}
				case returnVar: {
					return ;
				}
				case error: {
					appVars.sendMessageToQueue("Unexpected errors occurred", "*");
					params.setSub1String(1, 4, "ENDP");
					qState = returnVar;
					break;
				}
				default:{
					qState = QS_END;
				}
				}
			}
			catch (ExtMsgException ex){
				if (ex.messageMatches("CPF0000")
				|| ex.messageMatches("CBE0000")) {
					qState = error;
				}
				else {
					throw ex;
				}
			}
		}
		
	}
}
