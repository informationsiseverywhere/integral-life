package com.csc.life.statistics.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.RPGTimeData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from RJ532.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 30/08/09 07:51
 * @author Quipoz
 */
public class Rj532Report extends SMARTReportLayout { 

	private FixedLengthStringData acctccy = new FixedLengthStringData(3);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30);
	private ZonedDecimalData dancar01 = new ZonedDecimalData(13, 0);
	private ZonedDecimalData dancar02 = new ZonedDecimalData(13, 0);
	private ZonedDecimalData dancar03 = new ZonedDecimalData(13, 0);
	private ZonedDecimalData dancar04 = new ZonedDecimalData(13, 0);
	private FixedLengthStringData descrip = new FixedLengthStringData(30);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData register = new FixedLengthStringData(3);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private ZonedDecimalData stcmthg = new ZonedDecimalData(9, 0);
	private ZonedDecimalData stlmth = new ZonedDecimalData(9, 0);
	private FixedLengthStringData stsect = new FixedLengthStringData(2);
	private FixedLengthStringData stssect = new FixedLengthStringData(4);
	private RPGTimeData time = new RPGTimeData();

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public Rj532Report() {
		super();
	}


	/**
	 * Print the XML for Rj532d01
	 */
	public void printRj532d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(16).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		stcmthg.setFieldName("stcmthg");
		stcmthg.setInternal(subString(recordData, 1, 9));
		stlmth.setFieldName("stlmth");
		stlmth.setInternal(subString(recordData, 10, 9));
		dancar01.setFieldName("dancar01");
		dancar01.setInternal(subString(recordData, 19, 13));
		dancar02.setFieldName("dancar02");
		dancar02.setInternal(subString(recordData, 32, 13));
		dancar03.setFieldName("dancar03");
		dancar03.setInternal(subString(recordData, 45, 13));
		dancar04.setFieldName("dancar04");
		dancar04.setInternal(subString(recordData, 58, 13));
		printLayout("Rj532d01",			// Record name
			new BaseData[]{			// Fields:
				stcmthg,
				stlmth,
				dancar01,
				dancar02,
				dancar03,
				dancar04
			}
			, new Object[] {			// indicators
				new Object[]{"ind02", indicArea.charAt(2)},
				new Object[]{"ind03", indicArea.charAt(3)},
				new Object[]{"ind04", indicArea.charAt(4)},
				new Object[]{"ind05", indicArea.charAt(5)},
				new Object[]{"ind06", indicArea.charAt(6)},
				new Object[]{"ind07", indicArea.charAt(7)},
				new Object[]{"ind08", indicArea.charAt(8)},
				new Object[]{"ind15", indicArea.charAt(15)},
				new Object[]{"ind16", indicArea.charAt(16)}
			}
		);

	}

	/**
	 * Print the XML for Rj532h01
	 */
	public void printRj532h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		acctmonth.setFieldName("acctmonth");
		acctmonth.setInternal(subString(recordData, 1, 2));
		acctyear.setFieldName("acctyear");
		acctyear.setInternal(subString(recordData, 3, 4));
		company.setFieldName("company");
		company.setInternal(subString(recordData, 7, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 8, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 38, 10));
		acctccy.setFieldName("acctccy");
		acctccy.setInternal(subString(recordData, 48, 3));
		currdesc.setFieldName("currdesc");
		currdesc.setInternal(subString(recordData, 51, 30));
		time.setFieldName("time");
		time.set(getTime());
		register.setFieldName("register");
		register.setInternal(subString(recordData, 81, 3));
		descrip.setFieldName("descrip");
		descrip.setInternal(subString(recordData, 84, 30));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		stsect.setFieldName("stsect");
		stsect.setInternal(subString(recordData, 114, 2));
		itmdesc.setFieldName("itmdesc");
		itmdesc.setInternal(subString(recordData, 116, 30));
		stssect.setFieldName("stssect");
		stssect.setInternal(subString(recordData, 146, 4));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 150, 30));
		printLayout("Rj532h01",			// Record name
			new BaseData[]{			// Fields:
				acctmonth,
				acctyear,
				company,
				companynm,
				sdate,
				acctccy,
				currdesc,
				time,
				register,
				descrip,
				pagnbr,
				stsect,
				itmdesc,
				stssect,
				longdesc
			}
		);

		currentPrintLine.set(11);
	}

	/**
	 * Print the XML for Rj532h02
	 */
	public void printRj532h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(9).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);
		printLayout("Rj532h02",			// Record name
			new BaseData[]{			// Fields:
			}
			, new Object[] {			// indicators
				new Object[]{"ind01", indicArea.charAt(1)},
				new Object[]{"ind09", indicArea.charAt(9)}
			}
		);

	}

	/**
	 * Print the XML for Rj532h03
	 */
	public void printRj532h03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = new FixedLengthStringData(17).init(SPACES);//IJTI-320
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		cntdesc.setFieldName("cntdesc");
		cntdesc.setInternal(subString(recordData, 1, 30));
		printLayout("Rj532h03",			// Record name
			new BaseData[]{			// Fields:
				cntdesc
			}
			, new Object[] {			// indicators
				new Object[]{"ind17", indicArea.charAt(17)},
				new Object[]{"ind10", indicArea.charAt(10)},
				new Object[]{"ind11", indicArea.charAt(11)},
				new Object[]{"ind12", indicArea.charAt(12)},
				new Object[]{"ind13", indicArea.charAt(13)},
				new Object[]{"ind14", indicArea.charAt(14)}
			}
		);

	}


}
