/*
 * File: Bj524.java
 * Date: 29 August 2009 21:41:48
 * Author: Quipoz Limited
 *
 * Class transformed from BJ524.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj524Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*               Reassurance Details
*               ===================
*   This report will list the premiums on reassurance ceded and
*   commission paid on reassurance premium ceded for the specify
*   period.
*
*****************************************************************
* </pre>
*/
public class Bj524 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgvahpfrs = null;
	private java.sql.PreparedStatement sqlgvahpfps = null;
	private java.sql.Connection sqlgvahpfconn = null;
	private String sqlgvahpf = "";
	private Rj524Report printerFile = new Rj524Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ524");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaTamt01 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt02 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt03 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt04 = new PackedDecimalData(18, 2);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaRegister = new FixedLengthStringData(3);
	private String wsaaFrecord = "";
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-GVAHPF */
	private FixedLengthStringData sqlGvahrec = new FixedLengthStringData(255);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGvahrec, 0);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGvahrec, 1);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGvahrec, 4);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGvahrec, 7);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGvahrec, 9);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGvahrec, 12);
	private PackedDecimalData sqlStaccrip01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 15);
	private PackedDecimalData sqlStaccrip02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 25);
	private PackedDecimalData sqlStaccrip03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 35);
	private PackedDecimalData sqlStaccrip04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 45);
	private PackedDecimalData sqlStaccrip05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 55);
	private PackedDecimalData sqlStaccrip06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 65);
	private PackedDecimalData sqlStaccrip07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 75);
	private PackedDecimalData sqlStaccrip08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 85);
	private PackedDecimalData sqlStaccrip09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 95);
	private PackedDecimalData sqlStaccrip10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 105);
	private PackedDecimalData sqlStaccrip11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 115);
	private PackedDecimalData sqlStaccrip12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 125);
	private PackedDecimalData sqlStaccric01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 135);
	private PackedDecimalData sqlStaccric02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 145);
	private PackedDecimalData sqlStaccric03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 155);
	private PackedDecimalData sqlStaccric04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 165);
	private PackedDecimalData sqlStaccric05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 175);
	private PackedDecimalData sqlStaccric06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 185);
	private PackedDecimalData sqlStaccric07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 195);
	private PackedDecimalData sqlStaccric08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 205);
	private PackedDecimalData sqlStaccric09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 215);
	private PackedDecimalData sqlStaccric10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 225);
	private PackedDecimalData sqlStaccric11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 235);
	private PackedDecimalData sqlStaccric12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvahrec, 245);

	private FixedLengthStringData wsaaSqlKeep = new FixedLengthStringData(255);
	private FixedLengthStringData wsaaSChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 0);
	private PackedDecimalData wsaaSAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaSqlKeep, 1);
	private FixedLengthStringData wsaaSRegister = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 4);
	private FixedLengthStringData wsaaSCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 7);
	private FixedLengthStringData wsaaSCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 9);
	private FixedLengthStringData wsaaSAcctccy = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 12);
	private PackedDecimalData[] wsaaSStaccrip = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 15);
	private PackedDecimalData[] wsaaSStaccric = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 135);
	private String descrec = "DESCREC";
		/* ERRORS */
	private String esql = "ESQL";
		/* TABLES */
	private String t1693 = "T1693";
	private String t3629 = "T3629";
	private String t3589 = "T3589";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct04 = 4;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rj524H01 = new FixedLengthStringData(113);
	private FixedLengthStringData rj524h01O = new FixedLengthStringData(113).isAPartOf(rj524H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj524h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj524h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj524h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj524h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj524h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj524h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj524h01O, 50);
	private FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(rj524h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj524h01O, 83);

	private FixedLengthStringData rj524D01 = new FixedLengthStringData(18);
	private FixedLengthStringData rj524d01O = new FixedLengthStringData(18).isAPartOf(rj524D01, 0);
	private ZonedDecimalData tamt = new ZonedDecimalData(18, 2).isAPartOf(rj524d01O, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Pj517par pj517par = new Pj517par();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090
	}

	public Bj524() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaAcctyr.set(pj517par.acctyr);
		acctyear.set(pj517par.acctyr);
		acctmonth.set(pj517par.acctmnth);
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaTamt01.set(ZERO);
		wsaaTamt03.set(ZERO);
		wsaaRegister.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaFrecord = "Y";
		//ILIFE-1149
		//sqlgvahpf = " SELECT  CHDRCOY, ACCTYR, REGISTER, CNTBRANCH, CNTTYPE, ACCTCCY, STACCRIP01, STACCRIP02, STACCRIP03, STACCRIP04, STACCRIP05, STACCRIP06, STACCRIP07, STACCRIP08, STACCRIP09, STACCRIP10, STACCRIP11, STACCRIP12, STACCRIC01, STACCRIC02, STACCRIC03, STACCRIC04, STACCRIC05, STACCRIC06, STACCRIC07, STACCRIC08, STACCRIC09, STACCRIC10, STACCRIC11, STACCRIC12" +
		sqlgvahpf = " SELECT  CHDRCOY, ACCTYR, REG, CNTBRANCH, CNTTYPE, ACCTCCY, STACCRIP01, STACCRIP02, STACCRIP03, STACCRIP04, STACCRIP05, STACCRIP06, STACCRIP07, STACCRIP08, STACCRIP09, STACCRIP10, STACCRIP11, STACCRIP12, STACCRIC01, STACCRIC02, STACCRIC03, STACCRIC04, STACCRIC05, STACCRIC06, STACCRIC07, STACCRIC08, STACCRIC09, STACCRIC10, STACCRIC11, STACCRIC12" +
" FROM   " + appVars.getTableNameOverriden("GVAHPF") + " " +
" WHERE ACCTYR = ?" +
//ILIFE-1149
//" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REGISTER, CNTTYPE";
" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REG, CNTTYPE";
		sqlerrorflag = false;
		try {
			sqlgvahpfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvahpfTableDAM());
			sqlgvahpfps = appVars.prepareStatementEmbeded(sqlgvahpfconn, sqlgvahpf, "GVAHPF");
			appVars.setDBDouble(sqlgvahpfps, 1, wsaaAcctyr.toDouble());
			sqlgvahpfrs = appVars.executeQuery(sqlgvahpfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		/*EXIT*/
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlgvahpfrs.next()) {
				appVars.getDBObject(sqlgvahpfrs, 1, sqlChdrcoy);
				appVars.getDBObject(sqlgvahpfrs, 2, sqlAcctyr);
				appVars.getDBObject(sqlgvahpfrs, 3, sqlRegister);
				appVars.getDBObject(sqlgvahpfrs, 4, sqlCntbranch);
				appVars.getDBObject(sqlgvahpfrs, 5, sqlCnttype);
				appVars.getDBObject(sqlgvahpfrs, 6, sqlAcctccy);
				appVars.getDBObject(sqlgvahpfrs, 7, sqlStaccrip01);
				appVars.getDBObject(sqlgvahpfrs, 8, sqlStaccrip02);
				appVars.getDBObject(sqlgvahpfrs, 9, sqlStaccrip03);
				appVars.getDBObject(sqlgvahpfrs, 10, sqlStaccrip04);
				appVars.getDBObject(sqlgvahpfrs, 11, sqlStaccrip05);
				appVars.getDBObject(sqlgvahpfrs, 12, sqlStaccrip06);
				appVars.getDBObject(sqlgvahpfrs, 13, sqlStaccrip07);
				appVars.getDBObject(sqlgvahpfrs, 14, sqlStaccrip08);
				appVars.getDBObject(sqlgvahpfrs, 15, sqlStaccrip09);
				appVars.getDBObject(sqlgvahpfrs, 16, sqlStaccrip10);
				appVars.getDBObject(sqlgvahpfrs, 17, sqlStaccrip11);
				appVars.getDBObject(sqlgvahpfrs, 18, sqlStaccrip12);
				appVars.getDBObject(sqlgvahpfrs, 19, sqlStaccric01);
				appVars.getDBObject(sqlgvahpfrs, 20, sqlStaccric02);
				appVars.getDBObject(sqlgvahpfrs, 21, sqlStaccric03);
				appVars.getDBObject(sqlgvahpfrs, 22, sqlStaccric04);
				appVars.getDBObject(sqlgvahpfrs, 23, sqlStaccric05);
				appVars.getDBObject(sqlgvahpfrs, 24, sqlStaccric06);
				appVars.getDBObject(sqlgvahpfrs, 25, sqlStaccric07);
				appVars.getDBObject(sqlgvahpfrs, 26, sqlStaccric08);
				appVars.getDBObject(sqlgvahpfrs, 27, sqlStaccric09);
				appVars.getDBObject(sqlgvahpfrs, 28, sqlStaccric10);
				appVars.getDBObject(sqlgvahpfrs, 29, sqlStaccric11);
				appVars.getDBObject(sqlgvahpfrs, 30, sqlStaccric12);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		wsaaSqlKeep.set(sqlGvahrec);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		if (isNE(wsaaFrecord,"Y")) {
			h600SetHeading();
			h100NewPage();
			h200WriteDetail();
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		writeDetail3080();
	}

protected void writeDetail3080()
	{
		if (isEQ(wsaaFrecord,"Y")) {
			wsaaFrecord = "N";
		}
		else {
			if (isNE(wsaaRegister,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaCompany,sqlChdrcoy)
			|| isNE(wsaaBranch,sqlCntbranch)) {
				h600SetHeading();
				h100NewPage();
				h200WriteDetail();
			}
		}
		wsaaTamt01.add(wsaaSStaccrip[pj517par.acctmnth.toInt()]);
		wsaaTamt03.add(wsaaSStaccric[pj517par.acctmnth.toInt()]);
		wsaaRegister.set(sqlRegister);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaCompany.set(sqlChdrcoy);
		wsaaBranch.set(sqlCntbranch);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlgvahpfconn, sqlgvahpfps, sqlgvahpfrs);
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void h100NewPage()
	{
		/*H110-START*/
		printerFile.printRj524h01(rj524H01, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*H190-EXIT*/
	}

protected void h200WriteDetail()
	{
		h210Start();
	}

protected void h210Start()
	{
		indOn.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(13);
		indOff.setTrue(14);
		tamt.set(wsaaTamt01);
		contotrec.totno.set(ct02);
		contotrec.totval.set(wsaaTamt01);
		callContot001();
		printerFile.printRj524d01(rj524D01, indicArea);
		indOn.setTrue(12);
		indOff.setTrue(11);
		indOff.setTrue(13);
		indOff.setTrue(14);
		tamt.set(ZERO);
		printerFile.printRj524d01(rj524D01, indicArea);
		indOn.setTrue(13);
		indOff.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(14);
		tamt.set(wsaaTamt03);
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaTamt03);
		callContot001();
		printerFile.printRj524d01(rj524D01, indicArea);
		indOn.setTrue(14);
		indOff.setTrue(11);
		indOff.setTrue(12);
		indOff.setTrue(13);
		tamt.set(ZERO);
		printerFile.printRj524d01(rj524D01, indicArea);
		wsaaOverflow.set("Y");
		wsaaTamt01.set(ZERO);
		wsaaTamt03.set(ZERO);
	}

protected void h600SetHeading()
	{
		h610Start();
	}

protected void h610Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(wsaaCompany);
		rh01Companynm.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(wsaaAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		acctccy.set(wsaaAcctccy);
		currdesc.set(descIO.getLongdesc());
		//ILIFE-6360 starts
		if(isNE(wsaaRegister,SPACES)){
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3589);
		descIO.setDescitem(wsaaRegister);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		register.set(wsaaRegister);
		descrip.set(descIO.getLongdesc());
	}
		
		else{
			register.set(SPACES);
			descrip.set(SPACES);
			
		}//ILIFE-6360 ends
		
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
