package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6629
 * @version 1.0 generated on 30/08/09 06:54
 * @author Quipoz
 */
public class S6629ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(302);
	public FixedLengthStringData dataFields = new FixedLengthStringData(78).isAPartOf(dataArea, 0);
	public FixedLengthStringData agebands = new FixedLengthStringData(8).isAPartOf(dataFields, 0);
	public FixedLengthStringData[] ageband = FLSArrayPartOfStructure(2, 4, agebands, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(agebands, 0, FILLER_REDEFINE);
	public FixedLengthStringData ageband01 = DD.ageband.copy().isAPartOf(filler,0);
	public FixedLengthStringData ageband02 = DD.ageband.copy().isAPartOf(filler,4);
	public FixedLengthStringData agntstat = DD.agntstat.copy().isAPartOf(dataFields,8);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData govtstat = DD.govtstat.copy().isAPartOf(dataFields,10);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,11);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData prmbands = new FixedLengthStringData(8).isAPartOf(dataFields, 49);
	public FixedLengthStringData[] prmband = FLSArrayPartOfStructure(2, 4, prmbands, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(prmbands, 0, FILLER_REDEFINE);
	public FixedLengthStringData prmband01 = DD.prmband.copy().isAPartOf(filler1,0);
	public FixedLengthStringData prmband02 = DD.prmband.copy().isAPartOf(filler1,4);
	public FixedLengthStringData rskbands = new FixedLengthStringData(8).isAPartOf(dataFields, 57);
	public FixedLengthStringData[] rskband = FLSArrayPartOfStructure(2, 4, rskbands, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(rskbands, 0, FILLER_REDEFINE);
	public FixedLengthStringData rskband01 = DD.rskband.copy().isAPartOf(filler2,0);
	public FixedLengthStringData rskband02 = DD.rskband.copy().isAPartOf(filler2,4);
	public FixedLengthStringData sumbands = new FixedLengthStringData(8).isAPartOf(dataFields, 65);
	public FixedLengthStringData[] sumband = FLSArrayPartOfStructure(2, 4, sumbands, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(sumbands, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumband01 = DD.sumband.copy().isAPartOf(filler3,0);
	public FixedLengthStringData sumband02 = DD.sumband.copy().isAPartOf(filler3,4);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,73);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 78);
	public FixedLengthStringData agebandsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData[] agebandErr = FLSArrayPartOfStructure(2, 4, agebandsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(8).isAPartOf(agebandsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData ageband01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData ageband02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData agntstatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData govtstatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData prmbandsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData[] prmbandErr = FLSArrayPartOfStructure(2, 4, prmbandsErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(8).isAPartOf(prmbandsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData prmband01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData prmband02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData rskbandsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData[] rskbandErr = FLSArrayPartOfStructure(2, 4, rskbandsErr, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(8).isAPartOf(rskbandsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData rskband01Err = new FixedLengthStringData(4).isAPartOf(filler6, 0);
	public FixedLengthStringData rskband02Err = new FixedLengthStringData(4).isAPartOf(filler6, 4);
	public FixedLengthStringData sumbandsErr = new FixedLengthStringData(8).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData[] sumbandErr = FLSArrayPartOfStructure(2, 4, sumbandsErr, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(8).isAPartOf(sumbandsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData sumband01Err = new FixedLengthStringData(4).isAPartOf(filler7, 0);
	public FixedLengthStringData sumband02Err = new FixedLengthStringData(4).isAPartOf(filler7, 4);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 134);
	public FixedLengthStringData agebandsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 0);
	public FixedLengthStringData[] agebandOut = FLSArrayPartOfStructure(2, 12, agebandsOut, 0);
	public FixedLengthStringData[][] agebandO = FLSDArrayPartOfArrayStructure(12, 1, agebandOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(24).isAPartOf(agebandsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] ageband01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] ageband02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] agntstatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] govtstatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData prmbandsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 84);
	public FixedLengthStringData[] prmbandOut = FLSArrayPartOfStructure(2, 12, prmbandsOut, 0);
	public FixedLengthStringData[][] prmbandO = FLSDArrayPartOfArrayStructure(12, 1, prmbandOut, 0);
	public FixedLengthStringData filler9 = new FixedLengthStringData(24).isAPartOf(prmbandsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] prmband01Out = FLSArrayPartOfStructure(12, 1, filler9, 0);
	public FixedLengthStringData[] prmband02Out = FLSArrayPartOfStructure(12, 1, filler9, 12);
	public FixedLengthStringData rskbandsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 108);
	public FixedLengthStringData[] rskbandOut = FLSArrayPartOfStructure(2, 12, rskbandsOut, 0);
	public FixedLengthStringData[][] rskbandO = FLSDArrayPartOfArrayStructure(12, 1, rskbandOut, 0);
	public FixedLengthStringData filler10 = new FixedLengthStringData(24).isAPartOf(rskbandsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] rskband01Out = FLSArrayPartOfStructure(12, 1, filler10, 0);
	public FixedLengthStringData[] rskband02Out = FLSArrayPartOfStructure(12, 1, filler10, 12);
	public FixedLengthStringData sumbandsOut = new FixedLengthStringData(24).isAPartOf(outputIndicators, 132);
	public FixedLengthStringData[] sumbandOut = FLSArrayPartOfStructure(2, 12, sumbandsOut, 0);
	public FixedLengthStringData[][] sumbandO = FLSDArrayPartOfArrayStructure(12, 1, sumbandOut, 0);
	public FixedLengthStringData filler11 = new FixedLengthStringData(24).isAPartOf(sumbandsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] sumband01Out = FLSArrayPartOfStructure(12, 1, filler11, 0);
	public FixedLengthStringData[] sumband02Out = FLSArrayPartOfStructure(12, 1, filler11, 12);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6629screenWritten = new LongData(0);
	public LongData S6629protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6629ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, agntstat, ageband01, sumband01, rskband01, prmband01, govtstat, ageband02, sumband02, rskband02, prmband02, longdesc};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, agntstatOut, ageband01Out, sumband01Out, rskband01Out, prmband01Out, govtstatOut, ageband02Out, sumband02Out, rskband02Out, prmband02Out, longdescOut};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, agntstatErr, ageband01Err, sumband01Err, rskband01Err, prmband01Err, govtstatErr, ageband02Err, sumband02Err, rskband02Err, prmband02Err, longdescErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6629screen.class;
		protectRecord = S6629protect.class;
	}

}
