/*
 * File: Bj521.java
 * Date: 29 August 2009 21:40:35
 * Author: Quipoz Limited
 *
 * Class transformed from BJ521.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.delimitedExp;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.sql.SQLException;

import com.csc.life.statistics.dataaccess.GvacTableDAM;
import com.csc.life.statistics.dataaccess.GvahTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*           Government Statistics Posting from GVST
*           =======================================
*
*   This batch program is activated by L2ACSTAPST. It will read
*   BAKXPF and for each BAKX it will read all its GVST records.
*   GVST records are created by batch program BJ518. Thus, the
*   batch schedule L2NEWSTEXT must be run prior to L2ACSTAPST.
*
*   Two new accumulation files, GVAH and GVAC will be created.
*   GVAH is the accumulation file by contract type while GVAC
*   is by component type.
*
*   Initialise
*     Override BAKXPF and use SQL to read BAKX and GVST.
*     Select all records that falls within the accounting year
*     and accounting month specified.
*     The records are group by Contract company,
*                              Contract branch,
*                              Batch accounting year,
*                              Batch accounting month,
*                              Accounting currency,
*                              Source of business,
*                              Register,
*                              Statutory Fund,
*                              Statutory Section,
*                              Statutory Subsection,
*                              Contract Type,
*                              Coverage Type,
*                              Par/Nonpar indicator,
*                              Contract premium status,
*                              Coverage premium status
*
*    Perform    Until End of File
*      Edit
*       - Fetch the SQL record
*       - At end of file, update GVAH.
*
*      Update
*       - If there is any change in contract type, contract premium
*         status, contract company, register, contract branch, source
*         of business, accounting yr or accounting month, update GVAH.
*
*         - Read GVAH to check if record already exists.
*         - Create or update GVAH
*         - Update balance brought forward and carried forward amounts
*
*       - Update GVAC. Similarly, we need to check if GVAC already
*         exists. Create of update GVAC and its balance brought forward
*         and carried forward amounts.
*    End Perform
*
* Multi-Thread Batch Environment Notes:
*
*   1.  This program must follow B0321 not B0236.
*   2.  The BAKX file read has the name 'BAKX' + XX + 9999
*       where 'XX' is the first 2 chars of BSSC-SYSTEM-PARAM04
*       and 9999 is the last four digits of the schedule number.
*       This naming convention is used to allow this program
*       to be used in multiple processes across multiple
*       companies in a single schedule. Regular archiving of
*       these files will avoid conflict with the truncation
*       of the schedule number and unique use of system param
*       will avoid conflict across companies and/or processes.
*
*   Control totals:
*     01  -  No. of BAKX records read
*     02  -  No. of GVAH created
*     03  -  No. of GVAC created
*     04  -  Total of Rev Bonus
*     05  -  Total of Extra Bonus
*     06  -  Total of Terminal Bonus
*     07  -  Total of Interim Bonus
*     08  -  Total of Bonus Surrender
*     09  -  Total of Maturity Benfit
*     10  -  Total of Other Benefits
*     11  -  Total of SP Due
*     12  -  Total of FY Premium Due
*     13  -  Total of Rnl Premium Due
*     14  -  Total of FY Commission
*     15  -  Total of Rnl Commission
*     16  -  Total of SP Commission
*     17  -  Total of RI Prem Ceded
*     18  -  Total of RI Comm Ceded
*     19  -  Total of Claim Recovery
*     20  -  Total of Pending Death Benefit
*     21  -  Total of Advance Premium
*     22  -  Total of Dividend Allocated
*     23  -  Total of Dividend Interest
*     24  -  Total of Approved Death Benefit
*     25  -  Total of Death benefit adjustment
*     26  -  Total of Death benefit interest
*
*****************************************************************
* </pre>
*/
public class Bj521 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgvstpfrs = null;
	private java.sql.PreparedStatement sqlgvstpfps = null;
	private java.sql.Connection sqlgvstpfconn = null;
	private String sqlgvstpf = "";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ521");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaNewGvah = "";
	private String wsaaNewGvac = "";
	private String wsaaGetGvacBfFlag = "";
	private String wsaaGetGvahBfFlag = "";
	private PackedDecimalData wsaaAcctmnth = new PackedDecimalData(2, 0);
	private PackedDecimalData wsaaAcctyear = new PackedDecimalData(4, 0);

	private FixedLengthStringData wsaaGvahKey = new FixedLengthStringData(21);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaGvahKey, 0);
	private FixedLengthStringData wsaaRegister = new FixedLengthStringData(3).isAPartOf(wsaaGvahKey, 1);
	private FixedLengthStringData wsaaCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaGvahKey, 4);
	private FixedLengthStringData wsaaSrcebus = new FixedLengthStringData(2).isAPartOf(wsaaGvahKey, 6);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2).isAPartOf(wsaaGvahKey, 8);
	private FixedLengthStringData wsaaCnpstat = new FixedLengthStringData(2).isAPartOf(wsaaGvahKey, 10);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3).isAPartOf(wsaaGvahKey, 12);
	private PackedDecimalData wsaaAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaGvahKey, 15);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaGvahKey, 18);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");
	private PackedDecimalData wsaaGvahBfwdrb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdxb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdib = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwddd = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwddi = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdbs = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdmtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdob = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdspd = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdfyp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdrnp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdfyc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdrlc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdspc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdrip = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdric = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdclr = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdpdb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdadb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdadj = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdint = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvahBfwdadv = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdrb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdxb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdib = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdbs = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwddd = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwddi = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdmtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdob = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdspd = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdfyp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdrnp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdfyc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdrlc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdspc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdrip = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdric = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdclr = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdpdb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdadb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdadj = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdint = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaGvacBfwdadv = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccrb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccxb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStacctb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccib = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccdd = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccdi = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccbs = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccmtb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccob = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccspd = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccfyp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccrnp = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccfyc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccrlc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccspc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccrip = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccric = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccclr = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccpdb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccadb = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccadj = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccint = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaHdrStaccadv = new PackedDecimalData(18, 2);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);

	private FixedLengthStringData wsaaBakxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaBakxFn, 0, FILLER).init("BAKX");
	private FixedLengthStringData wsaaBakxRunid = new FixedLengthStringData(2).isAPartOf(wsaaBakxFn, 4);
	private ZonedDecimalData wsaaBakxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaBakxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);

		/* SQL-GVSTPF */
	private FixedLengthStringData sqlGvstrec = new FixedLengthStringData(267);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGvstrec, 0);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGvstrec, 1);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGvstrec, 4);
	private FixedLengthStringData sqlSrcebus = new FixedLengthStringData(2).isAPartOf(sqlGvstrec, 6);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGvstrec, 8);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGvstrec, 9);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGvstrec, 11);
	private FixedLengthStringData sqlStatcode = new FixedLengthStringData(2).isAPartOf(sqlGvstrec, 15);
	private FixedLengthStringData sqlCnpstat = new FixedLengthStringData(2).isAPartOf(sqlGvstrec, 17);
	private FixedLengthStringData sqlCrpstat = new FixedLengthStringData(2).isAPartOf(sqlGvstrec, 19);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGvstrec, 21);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGvstrec, 24);
	private PackedDecimalData sqlAcctmn = new PackedDecimalData(2, 0).isAPartOf(sqlGvstrec, 27);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGvstrec, 29);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGvstrec, 32);
	private FixedLengthStringData sqlParind = new FixedLengthStringData(1).isAPartOf(sqlGvstrec, 36);
	private PackedDecimalData sqlStaccrb = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 37);
	private PackedDecimalData sqlStaccxb = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 47);
	private PackedDecimalData sqlStacctb = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 57);
	private PackedDecimalData sqlStaccib = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 67);
	private PackedDecimalData sqlStaccbs = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 77);
	private PackedDecimalData sqlStaccmtb = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 87);
	private PackedDecimalData sqlStaccob = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 97);
	private PackedDecimalData sqlStaccspd = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 107);
	private PackedDecimalData sqlStaccfyp = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 117);
	private PackedDecimalData sqlStaccrnp = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 127);
	private PackedDecimalData sqlStaccfyc = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 137);
	private PackedDecimalData sqlStaccrlc = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 147);
	private PackedDecimalData sqlStaccspc = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 157);
	private PackedDecimalData sqlStaccrip = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 167);
	private PackedDecimalData sqlStaccric = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 177);
	private PackedDecimalData sqlStaccclr = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 187);
	private PackedDecimalData sqlStaccpdb = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 197);
	private PackedDecimalData sqlStaccadb = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 207);
	private PackedDecimalData sqlStaccadj = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 217);
	private PackedDecimalData sqlStaccint = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 227);
	private PackedDecimalData sqlStaccadv = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 237);
	private PackedDecimalData sqlStaccdd = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 247);
	private PackedDecimalData sqlStaccdi = new PackedDecimalData(18, 2).isAPartOf(sqlGvstrec, 257);
		/* ERRORS */
	private String esql = "ESQL";
	private String gvacrec = "GVACREC";
	private String gvahrec = "GVAHREC";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private int ct08 = 8;
	private int ct09 = 9;
	private int ct10 = 10;
	private int ct11 = 11;
	private int ct12 = 12;
	private int ct13 = 13;
	private int ct14 = 14;
	private int ct15 = 15;
	private int ct16 = 16;
	private int ct17 = 17;
	private int ct18 = 18;
	private int ct19 = 19;
	private int ct20 = 20;
	private int ct21 = 21;
	private int ct22 = 22;
	private int ct23 = 23;
	private int ct24 = 24;
	private int ct25 = 25;
	private int ct26 = 26;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
		/*Government Accumulation File by Coverage*/
	private GvacTableDAM gvacIO = new GvacTableDAM();
		/*Government Accumulation File by Contract*/
	private GvahTableDAM gvahIO = new GvahTableDAM();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		exit7190,
		exit10190
	}

	public Bj521() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		wsaaFirstTime.set("Y");
		initialize(wsaaGvahKey);
		wsaaBakxRunid.set(bprdIO.getSystemParam04());
		wsaaBakxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append("OVRDBF FILE(BAKXPF) TOFILE(");
		stringVariable1.append(delimitedExp(bprdIO.getRunLibrary(), SPACES));
		stringVariable1.append("/");
		stringVariable1.append(wsaaBakxFn.toString());
		stringVariable1.append(") ");
		stringVariable1.append("MBR(");
		stringVariable1.append(wsaaThreadMember.toString());
		stringVariable1.append(")");
		wsaaQcmdexc.setLeft(stringVariable1.toString());
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		sqlgvstpf = " SELECT  GV.CHDRCOY, GV.REG, GV.CNTBRANCH, GV.SRCEBUS, GV.STFUND, GV.STSECT, GV.STSSECT, GV.STATCODE, GV.CNPSTAT, GV.CRPSTAT, GV.ACCTCCY, GV.BATCACTYR, GV.BATCACTMN, GV.CNTTYPE, GV.CRTABLE, GV.PARIND, SUM(GV.STACCRB), SUM(GV.STACCXB), SUM(GV.STACCTB), SUM(GV.STACCIB), SUM(GV.STACCBS), SUM(GV.STACCMTB), SUM(GV.STACCOB), SUM(GV.STACCSPD), SUM(GV.STACCFYP), SUM(GV.STACCRNP), SUM(GV.STACCFYC), SUM(GV.STACCRLC), SUM(GV.STACCSPC), SUM(GV.STACCRIP), SUM(GV.STACCRIC), SUM(GV.STACCCLR), SUM(GV.STACCPDB), SUM(GV.STACCADB), SUM(GV.STACCADJ), SUM(GV.STACCINT), SUM(GV.STACCADV), SUM(GV.STACCDD), SUM(GV.STACCDI)" +
" FROM   " + appVars.getTableNameOverriden("BAKXPF") + "  BK,  " + appVars.getTableNameOverriden("GVSTPF") + "  GV" +
" WHERE BK.BATCCOY = GV.BATCCOY" +
" AND BK.BATCBRN = GV.BATCBRN" +
" AND BK.BATCACTYR = GV.BATCACTYR" +
" AND BK.BATCACTMN = GV.BATCACTMN" +
" AND BK.BATCTRCDE = GV.BATCTRCDE" +
" AND BK.BATCBATCH = GV.BATCBATCH" +
//" GROUP BY GV.CHDRCOY, GV.CNTBRANCH, GV.BATCACTYR, GV.BATCACTMN, GV.ACCTCCY, GV.SRCEBUS, GV.REG, GV.STFUND, GV.STSECT, GV.STSSECT, GV.CNTTYPE, GV.CRTABLE, GV.PARIND, GV.STATCODE, GV.CN_PREM_STAT, GV.COV_PREM_STAT" +
//" ORDER BY GV.CHDRCOY, GV.CNTBRANCH, GV.BATCACTYR, GV.BATCACTMN, GV.ACCTCCY, GV.SRCEBUS, GV.REG, GV.STFUND, GV.STSECT, GV.STSSECT, GV.CNTTYPE, GV.CRTABLE, GV.PARIND, GV.STATCODE, GV.CN_PREM_STAT, GV.COV_PREM_STAT";
//tom chi bug 979
" GROUP BY GV.CHDRCOY, GV.CNTBRANCH, GV.BATCACTYR, GV.BATCACTMN, GV.ACCTCCY, GV.SRCEBUS, GV.REG, GV.STFUND, GV.STSECT, GV.STSSECT, GV.CNTTYPE, GV.CRTABLE, GV.PARIND, GV.STATCODE, GV.CNPSTAT, GV.CRPSTAT" +
" ORDER BY GV.CHDRCOY, GV.CNTBRANCH, GV.BATCACTYR, GV.BATCACTMN, GV.ACCTCCY, GV.SRCEBUS, GV.REG, GV.STFUND, GV.STSECT, GV.STSSECT, GV.CNTTYPE, GV.CRTABLE, GV.PARIND, GV.STATCODE,GV.CNPSTAT, GV.CRPSTAT";
//end
		sqlerrorflag = false;
		try {
			//tom chi bug 979
			//sqlgvstpfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.smart400framework.dataaccess.SmartFileCode[] {new com.csc.smart.dataaccess.BakxpfTableDAM(), new com.csc.life.statistics.dataaccess.GvstpfTableDAM()});
			sqlgvstpfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvstpfTableDAM());
			//end
			sqlgvstpfps = appVars.prepareStatementEmbeded(sqlgvstpfconn, sqlgvstpf);
			sqlgvstpfrs = appVars.executeQuery(sqlgvstpfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		sqlerrorflag = false;
		try {
			if (sqlgvstpfrs.next()) {
				appVars.getDBObject(sqlgvstpfrs, 1, sqlChdrcoy);
				appVars.getDBObject(sqlgvstpfrs, 2, sqlRegister);
				appVars.getDBObject(sqlgvstpfrs, 3, sqlCntbranch);
				appVars.getDBObject(sqlgvstpfrs, 4, sqlSrcebus);
				appVars.getDBObject(sqlgvstpfrs, 5, sqlStfund);
				appVars.getDBObject(sqlgvstpfrs, 6, sqlStsect);
				appVars.getDBObject(sqlgvstpfrs, 7, sqlStssect);
				appVars.getDBObject(sqlgvstpfrs, 8, sqlStatcode);
				appVars.getDBObject(sqlgvstpfrs, 9, sqlCnpstat);
				appVars.getDBObject(sqlgvstpfrs, 10, sqlCrpstat);
				appVars.getDBObject(sqlgvstpfrs, 11, sqlAcctccy);
				appVars.getDBObject(sqlgvstpfrs, 12, sqlAcctyr);
				appVars.getDBObject(sqlgvstpfrs, 13, sqlAcctmn);
				appVars.getDBObject(sqlgvstpfrs, 14, sqlCnttype);
				appVars.getDBObject(sqlgvstpfrs, 15, sqlCrtable);
				appVars.getDBObject(sqlgvstpfrs, 16, sqlParind);
				appVars.getDBObject(sqlgvstpfrs, 17, sqlStaccrb);
				appVars.getDBObject(sqlgvstpfrs, 18, sqlStaccxb);
				appVars.getDBObject(sqlgvstpfrs, 19, sqlStacctb);
				appVars.getDBObject(sqlgvstpfrs, 20, sqlStaccib);
				appVars.getDBObject(sqlgvstpfrs, 21, sqlStaccbs);
				appVars.getDBObject(sqlgvstpfrs, 22, sqlStaccmtb);
				appVars.getDBObject(sqlgvstpfrs, 23, sqlStaccob);
				appVars.getDBObject(sqlgvstpfrs, 24, sqlStaccspd);
				appVars.getDBObject(sqlgvstpfrs, 25, sqlStaccfyp);
				appVars.getDBObject(sqlgvstpfrs, 26, sqlStaccrnp);
				appVars.getDBObject(sqlgvstpfrs, 27, sqlStaccfyc);
				appVars.getDBObject(sqlgvstpfrs, 28, sqlStaccrlc);
				appVars.getDBObject(sqlgvstpfrs, 29, sqlStaccspc);
				appVars.getDBObject(sqlgvstpfrs, 30, sqlStaccrip);
				appVars.getDBObject(sqlgvstpfrs, 31, sqlStaccric);
				appVars.getDBObject(sqlgvstpfrs, 32, sqlStaccclr);
				appVars.getDBObject(sqlgvstpfrs, 33, sqlStaccpdb);
				appVars.getDBObject(sqlgvstpfrs, 34, sqlStaccadb);
				appVars.getDBObject(sqlgvstpfrs, 35, sqlStaccadj);
				appVars.getDBObject(sqlgvstpfrs, 36, sqlStaccint);
				appVars.getDBObject(sqlgvstpfrs, 37, sqlStaccadv);
				appVars.getDBObject(sqlgvstpfrs, 38, sqlStaccdd);
				appVars.getDBObject(sqlgvstpfrs, 39, sqlStaccdi);
			}
			else {
				goTo(GotoLabel.eof2080);
			}
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		if (!firstTime.isTrue()) {
			readGvah8000();
			updatePostedRecord9000();
			updateGvah10000();
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (!firstTime.isTrue()) {
			if ((isNE(sqlCnttype,wsaaCnttype)
			|| isNE(sqlStatcode,wsaaStatcode)
			|| isNE(sqlCnpstat,wsaaCnpstat)
			|| isNE(sqlChdrcoy,wsaaChdrcoy)
			|| isNE(sqlRegister,wsaaRegister)
			|| isNE(sqlCntbranch,wsaaCntbranch)
			|| isNE(sqlSrcebus,wsaaSrcebus)
			|| isNE(sqlAcctccy,wsaaAcctccy)
			|| isNE(sqlAcctyr,wsaaAcctyr))) {
				readGvah8000();
				updatePostedRecord9000();
				updateGvah10000();
				wsaaHdrStaccrb.set(ZERO);
				wsaaHdrStaccxb.set(ZERO);
				wsaaHdrStacctb.set(ZERO);
				wsaaHdrStaccib.set(ZERO);
				wsaaHdrStaccdd.set(ZERO);
				wsaaHdrStaccdi.set(ZERO);
				wsaaHdrStaccbs.set(ZERO);
				wsaaHdrStaccmtb.set(ZERO);
				wsaaHdrStaccob.set(ZERO);
				wsaaHdrStaccspd.set(ZERO);
				wsaaHdrStaccfyp.set(ZERO);
				wsaaHdrStaccrnp.set(ZERO);
				wsaaHdrStaccfyc.set(ZERO);
				wsaaHdrStaccrlc.set(ZERO);
				wsaaHdrStaccspc.set(ZERO);
				wsaaHdrStaccrip.set(ZERO);
				wsaaHdrStaccric.set(ZERO);
				wsaaHdrStaccclr.set(ZERO);
				wsaaHdrStaccpdb.set(ZERO);
				wsaaHdrStaccadb.set(ZERO);
				wsaaHdrStaccadj.set(ZERO);
				wsaaHdrStaccint.set(ZERO);
				wsaaHdrStaccadv.set(ZERO);
			}
		}
		wsaaFirstTime.set("N");
		wsaaCnttype.set(sqlCnttype);
		wsaaStatcode.set(sqlStatcode);
		wsaaCnpstat.set(sqlCnpstat);
		wsaaChdrcoy.set(sqlChdrcoy);
		wsaaRegister.set(sqlRegister);
		wsaaCntbranch.set(sqlCntbranch);
		wsaaSrcebus.set(sqlSrcebus);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaAcctyr.set(sqlAcctyr);
		readGvac5000();
		updatePostedRecord6000();
		updateGvac7000();
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		appVars.freeDBConnectionIgnoreErr(sqlgvstpfconn, sqlgvstpfps, sqlgvstpfrs);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void readGvac5000()
	{
		para5010();
	}

protected void para5010()
	{
		gvacIO.setParams(SPACES);
		gvacIO.setChdrcoy(sqlChdrcoy);
		gvacIO.setCntbranch(sqlCntbranch);
		gvacIO.setAcctccy(sqlAcctccy);
		gvacIO.setAcctyr(sqlAcctyr);
		gvacIO.setSrcebus(sqlSrcebus);
		gvacIO.setRegister(sqlRegister);
		gvacIO.setStatFund(sqlStfund);
		gvacIO.setStatSect(sqlStsect);
		gvacIO.setStatSubsect(sqlStssect);
		gvacIO.setCnttype(sqlCnttype);
		gvacIO.setCrtable(sqlCrtable);
		gvacIO.setStatcode(sqlStatcode);
		gvacIO.setCnPremStat(sqlCnpstat);
		gvacIO.setCovPremStat(sqlCrpstat);
		gvacIO.setParind(sqlParind);
		gvacIO.setFormat(gvacrec);
		gvacIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, gvacIO);
		if (isEQ(gvacIO.getStatuz(),varcom.mrnf)) {
			wsaaNewGvac = "Y";
		}
		else {
			wsaaNewGvac = "N";
		}
	}

protected void updatePostedRecord6000()
	{
		para6010();
	}

protected void para6010()
	{
		if (isEQ(wsaaNewGvac,"Y")) {
			gvacIO.setFunction(varcom.writr);
			gvacIO.setBfwdrb(ZERO);
			gvacIO.setCfwdrb(ZERO);
			gvacIO.setBfwdxb(ZERO);
			gvacIO.setCfwdxb(ZERO);
			gvacIO.setBfwdtb(ZERO);
			gvacIO.setCfwdtb(ZERO);
			gvacIO.setBfwdib(ZERO);
			gvacIO.setCfwdib(ZERO);
			gvacIO.setBfwddd(ZERO);
			gvacIO.setCfwddd(ZERO);
			gvacIO.setBfwddi(ZERO);
			gvacIO.setCfwddi(ZERO);
			gvacIO.setBfwdbs(ZERO);
			gvacIO.setCfwdbs(ZERO);
			gvacIO.setBfwdmtb(ZERO);
			gvacIO.setCfwdmtb(ZERO);
			gvacIO.setBfwdob(ZERO);
			gvacIO.setCfwdob(ZERO);
			gvacIO.setBfwdspd(ZERO);
			gvacIO.setCfwdspd(ZERO);
			gvacIO.setBfwdfyp(ZERO);
			gvacIO.setCfwdfyp(ZERO);
			gvacIO.setBfwdrnp(ZERO);
			gvacIO.setCfwdrnp(ZERO);
			gvacIO.setBfwdfyc(ZERO);
			gvacIO.setCfwdfyc(ZERO);
			gvacIO.setBfwdrlc(ZERO);
			gvacIO.setCfwdrlc(ZERO);
			gvacIO.setBfwdspc(ZERO);
			gvacIO.setCfwdspc(ZERO);
			gvacIO.setBfwdrip(ZERO);
			gvacIO.setCfwdrip(ZERO);
			gvacIO.setBfwdric(ZERO);
			gvacIO.setCfwdric(ZERO);
			gvacIO.setBfwdclr(ZERO);
			gvacIO.setCfwdclr(ZERO);
			gvacIO.setBfwdpdb(ZERO);
			gvacIO.setCfwdpdb(ZERO);
			gvacIO.setBfwdadb(ZERO);
			gvacIO.setCfwdadb(ZERO);
			gvacIO.setBfwdadj(ZERO);
			gvacIO.setCfwdadj(ZERO);
			gvacIO.setBfwdint(ZERO);
			gvacIO.setCfwdint(ZERO);
			gvacIO.setBfwdadv(ZERO);
			gvacIO.setCfwdadv(ZERO);
			for (wsaaI.set(1); !(isGT(wsaaI,12)); wsaaI.add(1)){
				initializeGvacValues6100();
			}
		}
		else {
			gvacIO.setFunction(varcom.updat);
		}
		setPrecision(gvacIO.getStaccrb(sqlAcctmn), 2);
		gvacIO.setStaccrb(sqlAcctmn, add(gvacIO.getStaccrb(sqlAcctmn),sqlStaccrb));
		wsaaHdrStaccrb.add(sqlStaccrb);
		setPrecision(gvacIO.getCfwdrb(), 2);
		gvacIO.setCfwdrb(add(gvacIO.getCfwdrb(),sqlStaccrb));
		setPrecision(gvacIO.getStaccxb(sqlAcctmn), 2);
		gvacIO.setStaccxb(sqlAcctmn, add(gvacIO.getStaccxb(sqlAcctmn),sqlStaccxb));
		wsaaHdrStaccxb.add(sqlStaccxb);
		setPrecision(gvacIO.getCfwdxb(), 2);
		gvacIO.setCfwdxb(add(gvacIO.getCfwdxb(),sqlStaccxb));
		setPrecision(gvacIO.getStacctb(sqlAcctmn), 2);
		gvacIO.setStacctb(sqlAcctmn, add(gvacIO.getStacctb(sqlAcctmn),sqlStacctb));
		wsaaHdrStacctb.add(sqlStacctb);
		setPrecision(gvacIO.getCfwdtb(), 2);
		gvacIO.setCfwdtb(add(gvacIO.getCfwdtb(),sqlStacctb));
		setPrecision(gvacIO.getStaccib(sqlAcctmn), 2);
		gvacIO.setStaccib(sqlAcctmn, add(gvacIO.getStaccib(sqlAcctmn),sqlStaccib));
		wsaaHdrStaccib.add(sqlStaccib);
		setPrecision(gvacIO.getCfwdib(), 2);
		gvacIO.setCfwdib(add(gvacIO.getCfwdib(),sqlStaccib));
		setPrecision(gvacIO.getStaccdd(sqlAcctmn), 2);
		gvacIO.setStaccdd(sqlAcctmn, add(gvacIO.getStaccdd(sqlAcctmn),sqlStaccdd));
		wsaaHdrStaccdd.add(sqlStaccdd);
		setPrecision(gvacIO.getCfwddd(), 2);
		gvacIO.setCfwddd(add(gvacIO.getCfwddd(),sqlStaccdd));
		setPrecision(gvacIO.getStaccdi(sqlAcctmn), 2);
		gvacIO.setStaccdi(sqlAcctmn, add(gvacIO.getStaccdi(sqlAcctmn),sqlStaccdi));
		wsaaHdrStaccdi.add(sqlStaccdi);
		setPrecision(gvacIO.getCfwddi(), 2);
		gvacIO.setCfwddi(add(gvacIO.getCfwddi(),sqlStaccdi));
		setPrecision(gvacIO.getStaccbs(sqlAcctmn), 2);
		gvacIO.setStaccbs(sqlAcctmn, add(gvacIO.getStaccbs(sqlAcctmn),sqlStaccbs));
		wsaaHdrStaccbs.add(sqlStaccbs);
		setPrecision(gvacIO.getCfwdbs(), 2);
		gvacIO.setCfwdbs(add(gvacIO.getCfwdbs(),sqlStaccbs));
		setPrecision(gvacIO.getStaccmtb(sqlAcctmn), 2);
		gvacIO.setStaccmtb(sqlAcctmn, add(gvacIO.getStaccmtb(sqlAcctmn),sqlStaccmtb));
		wsaaHdrStaccmtb.add(sqlStaccmtb);
		setPrecision(gvacIO.getCfwdmtb(), 2);
		gvacIO.setCfwdmtb(add(gvacIO.getCfwdmtb(),sqlStaccmtb));
		setPrecision(gvacIO.getStaccob(sqlAcctmn), 2);
		gvacIO.setStaccob(sqlAcctmn, add(gvacIO.getStaccob(sqlAcctmn),sqlStaccob));
		wsaaHdrStaccob.add(sqlStaccob);
		setPrecision(gvacIO.getCfwdob(), 2);
		gvacIO.setCfwdob(add(gvacIO.getCfwdob(),sqlStaccob));
		setPrecision(gvacIO.getStaccspd(sqlAcctmn), 2);
		gvacIO.setStaccspd(sqlAcctmn, add(gvacIO.getStaccspd(sqlAcctmn),sqlStaccspd));
		wsaaHdrStaccspd.add(sqlStaccspd);
		setPrecision(gvacIO.getCfwdspd(), 2);
		gvacIO.setCfwdspd(add(gvacIO.getCfwdspd(),sqlStaccspd));
		setPrecision(gvacIO.getStaccfyp(sqlAcctmn), 2);
		gvacIO.setStaccfyp(sqlAcctmn, add(gvacIO.getStaccfyp(sqlAcctmn),sqlStaccfyp));
		wsaaHdrStaccfyp.add(sqlStaccfyp);
		setPrecision(gvacIO.getCfwdfyp(), 2);
		gvacIO.setCfwdfyp(add(gvacIO.getCfwdfyp(),sqlStaccfyp));
		setPrecision(gvacIO.getStaccrnp(sqlAcctmn), 2);
		gvacIO.setStaccrnp(sqlAcctmn, add(gvacIO.getStaccrnp(sqlAcctmn),sqlStaccrnp));
		wsaaHdrStaccrnp.add(sqlStaccrnp);
		setPrecision(gvacIO.getCfwdrnp(), 2);
		gvacIO.setCfwdrnp(add(gvacIO.getCfwdrnp(),sqlStaccrnp));
		setPrecision(gvacIO.getStaccfyc(sqlAcctmn), 2);
		gvacIO.setStaccfyc(sqlAcctmn, add(gvacIO.getStaccfyc(sqlAcctmn),sqlStaccfyc));
		wsaaHdrStaccfyc.add(sqlStaccfyc);
		setPrecision(gvacIO.getCfwdfyc(), 2);
		gvacIO.setCfwdfyc(add(gvacIO.getCfwdfyc(),sqlStaccfyc));
		setPrecision(gvacIO.getStaccrlc(sqlAcctmn), 2);
		gvacIO.setStaccrlc(sqlAcctmn, add(gvacIO.getStaccrlc(sqlAcctmn),sqlStaccrlc));
		wsaaHdrStaccrlc.add(sqlStaccrlc);
		setPrecision(gvacIO.getCfwdrlc(), 2);
		gvacIO.setCfwdrlc(add(gvacIO.getCfwdrlc(),sqlStaccrlc));
		setPrecision(gvacIO.getStaccspc(sqlAcctmn), 2);
		gvacIO.setStaccspc(sqlAcctmn, add(gvacIO.getStaccspc(sqlAcctmn),sqlStaccspc));
		wsaaHdrStaccspc.add(sqlStaccspc);
		setPrecision(gvacIO.getCfwdspc(), 2);
		gvacIO.setCfwdspc(add(gvacIO.getCfwdspc(),sqlStaccspc));
		setPrecision(gvacIO.getStaccrip(sqlAcctmn), 2);
		gvacIO.setStaccrip(sqlAcctmn, add(gvacIO.getStaccrip(sqlAcctmn),sqlStaccrip));
		wsaaHdrStaccrip.add(sqlStaccrip);
		setPrecision(gvacIO.getCfwdrip(), 2);
		gvacIO.setCfwdrip(add(gvacIO.getCfwdrip(),sqlStaccrip));
		setPrecision(gvacIO.getStaccric(sqlAcctmn), 2);
		gvacIO.setStaccric(sqlAcctmn, add(gvacIO.getStaccric(sqlAcctmn),sqlStaccric));
		wsaaHdrStaccric.add(sqlStaccric);
		setPrecision(gvacIO.getCfwdric(), 2);
		gvacIO.setCfwdric(add(gvacIO.getCfwdric(),sqlStaccric));
		setPrecision(gvacIO.getStaccclr(sqlAcctmn), 2);
		gvacIO.setStaccclr(sqlAcctmn, add(gvacIO.getStaccclr(sqlAcctmn),sqlStaccclr));
		wsaaHdrStaccclr.add(sqlStaccclr);
		setPrecision(gvacIO.getCfwdclr(), 2);
		gvacIO.setCfwdclr(add(gvacIO.getCfwdclr(),sqlStaccclr));
		setPrecision(gvacIO.getStaccpdb(sqlAcctmn), 2);
		gvacIO.setStaccpdb(sqlAcctmn, add(gvacIO.getStaccpdb(sqlAcctmn),sqlStaccpdb));
		wsaaHdrStaccpdb.add(sqlStaccpdb);
		setPrecision(gvacIO.getCfwdpdb(), 2);
		gvacIO.setCfwdpdb(add(gvacIO.getCfwdpdb(),sqlStaccpdb));
		setPrecision(gvacIO.getStaccadb(sqlAcctmn), 2);
		gvacIO.setStaccadb(sqlAcctmn, add(gvacIO.getStaccadb(sqlAcctmn),sqlStaccadb));
		wsaaHdrStaccadb.add(sqlStaccadb);
		setPrecision(gvacIO.getCfwdadb(), 2);
		gvacIO.setCfwdadb(add(gvacIO.getCfwdadb(),sqlStaccadb));
		setPrecision(gvacIO.getStaccadj(sqlAcctmn), 2);
		gvacIO.setStaccadj(sqlAcctmn, add(gvacIO.getStaccadj(sqlAcctmn),sqlStaccadj));
		wsaaHdrStaccadj.add(sqlStaccadj);
		setPrecision(gvacIO.getCfwdadj(), 2);
		gvacIO.setCfwdadj(add(gvacIO.getCfwdadj(),sqlStaccadj));
		setPrecision(gvacIO.getStaccint(sqlAcctmn), 2);
		gvacIO.setStaccint(sqlAcctmn, add(gvacIO.getStaccint(sqlAcctmn),sqlStaccint));
		wsaaHdrStaccint.add(sqlStaccint);
		setPrecision(gvacIO.getCfwdint(), 2);
		gvacIO.setCfwdint(add(gvacIO.getCfwdint(),sqlStaccint));
		setPrecision(gvacIO.getStaccadv(sqlAcctmn), 2);
		gvacIO.setStaccadv(sqlAcctmn, add(gvacIO.getStaccadv(sqlAcctmn),sqlStaccadv));
		wsaaHdrStaccadv.add(sqlStaccadv);
		setPrecision(gvacIO.getCfwdadv(), 2);
		gvacIO.setCfwdadv(add(gvacIO.getCfwdadv(),sqlStaccadv));
		gvacIO.setFormat(gvacrec);
		SmartFileCode.execute(appVars, gvacIO);
		if (isNE(gvacIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvacIO.getParams());
			fatalError600();
		}
		controlTotals6200();
	}

protected void initializeGvacValues6100()
	{
		/*PARA*/
		gvacIO.setStaccrb(wsaaI, ZERO);
		gvacIO.setStaccxb(wsaaI, ZERO);
		gvacIO.setStacctb(wsaaI, ZERO);
		gvacIO.setStaccib(wsaaI, ZERO);
		gvacIO.setStaccdd(wsaaI, ZERO);
		gvacIO.setStaccdi(wsaaI, ZERO);
		gvacIO.setStaccbs(wsaaI, ZERO);
		gvacIO.setStaccmtb(wsaaI, ZERO);
		gvacIO.setStaccob(wsaaI, ZERO);
		gvacIO.setStaccspd(wsaaI, ZERO);
		gvacIO.setStaccfyp(wsaaI, ZERO);
		gvacIO.setStaccrnp(wsaaI, ZERO);
		gvacIO.setStaccfyc(wsaaI, ZERO);
		gvacIO.setStaccrlc(wsaaI, ZERO);
		gvacIO.setStaccspc(wsaaI, ZERO);
		gvacIO.setStaccrip(wsaaI, ZERO);
		gvacIO.setStaccric(wsaaI, ZERO);
		gvacIO.setStaccclr(wsaaI, ZERO);
		gvacIO.setStaccpdb(wsaaI, ZERO);
		gvacIO.setStaccadb(wsaaI, ZERO);
		gvacIO.setStaccadj(wsaaI, ZERO);
		gvacIO.setStaccint(wsaaI, ZERO);
		gvacIO.setStaccadv(wsaaI, ZERO);
		/*EXIT*/
	}

protected void controlTotals6200()
	{
		para6210();
	}

protected void para6210()
	{
		contotrec.totno.set(ct03);
		contotrec.totval.set(1);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(sqlStaccrb);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(sqlStaccxb);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(sqlStacctb);
		callContot001();
		contotrec.totno.set(ct07);
		contotrec.totval.set(sqlStaccib);
		callContot001();
		contotrec.totno.set(ct08);
		contotrec.totval.set(sqlStaccbs);
		callContot001();
		contotrec.totno.set(ct09);
		contotrec.totval.set(sqlStaccmtb);
		callContot001();
		contotrec.totno.set(ct10);
		contotrec.totval.set(sqlStaccob);
		callContot001();
		contotrec.totno.set(ct11);
		contotrec.totval.set(sqlStaccspd);
		callContot001();
		contotrec.totno.set(ct12);
		contotrec.totval.set(sqlStaccfyp);
		callContot001();
		contotrec.totno.set(ct13);
		contotrec.totval.set(sqlStaccrnp);
		callContot001();
		contotrec.totno.set(ct14);
		contotrec.totval.set(sqlStaccfyc);
		callContot001();
		contotrec.totno.set(ct15);
		contotrec.totval.set(sqlStaccrlc);
		callContot001();
		contotrec.totno.set(ct16);
		contotrec.totval.set(sqlStaccspc);
		callContot001();
		contotrec.totno.set(ct17);
		contotrec.totval.set(sqlStaccrip);
		callContot001();
		contotrec.totno.set(ct18);
		contotrec.totval.set(sqlStaccric);
		callContot001();
		contotrec.totno.set(ct19);
		contotrec.totval.set(sqlStaccclr);
		callContot001();
		contotrec.totno.set(ct20);
		contotrec.totval.set(sqlStaccpdb);
		callContot001();
		contotrec.totno.set(ct21);
		contotrec.totval.set(sqlStaccadv);
		callContot001();
		contotrec.totno.set(ct22);
		contotrec.totval.set(sqlStaccdd);
		callContot001();
		contotrec.totno.set(ct23);
		contotrec.totval.set(sqlStaccdi);
		callContot001();
		contotrec.totno.set(ct24);
		contotrec.totval.set(sqlStaccadb);
		callContot001();
		contotrec.totno.set(ct25);
		contotrec.totval.set(sqlStaccadj);
		callContot001();
		contotrec.totno.set(ct26);
		contotrec.totval.set(sqlStaccint);
		callContot001();
	}

protected void updateGvac7000()
	{
		/*PARA*/
		wsaaGvacBfwdrb.set(ZERO);
		wsaaGvacBfwdxb.set(ZERO);
		wsaaGvacBfwdtb.set(ZERO);
		wsaaGvacBfwdib.set(ZERO);
		wsaaGvacBfwddd.set(ZERO);
		wsaaGvacBfwddi.set(ZERO);
		wsaaGvacBfwdbs.set(ZERO);
		wsaaGvacBfwdmtb.set(ZERO);
		wsaaGvacBfwdob.set(ZERO);
		wsaaGvacBfwdspd.set(ZERO);
		wsaaGvacBfwdfyp.set(ZERO);
		wsaaGvacBfwdrnp.set(ZERO);
		wsaaGvacBfwdfyc.set(ZERO);
		wsaaGvacBfwdrlc.set(ZERO);
		wsaaGvacBfwdspc.set(ZERO);
		wsaaGvacBfwdrip.set(ZERO);
		wsaaGvacBfwdric.set(ZERO);
		wsaaGvacBfwdclr.set(ZERO);
		wsaaGvacBfwdpdb.set(ZERO);
		wsaaGvacBfwdadb.set(ZERO);
		wsaaGvacBfwdadj.set(ZERO);
		wsaaGvacBfwdint.set(ZERO);
		wsaaGvacBfwdadv.set(ZERO);
		gvacIO.setAcctyr(ZERO);
		gvacIO.setFormat(gvacrec);
		gvacIO.setFunction(varcom.begn);
		wsaaGetGvacBfFlag = "N";
		while ( !(isEQ(wsaaGetGvacBfFlag,"Y"))) {


			//performance improvement --  atiwari23
			gvacIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			gvacIO.setFitKeysSearch("ACCTYR");


			checkBalForwardValue7100();
		}

		/*EXIT*/
	}

protected void checkBalForwardValue7100()
	{
		try {
			para7110();
			readNextRecord7120();
		}
		catch (GOTOException e){
		}
	}

protected void para7110()
	{


		SmartFileCode.execute(appVars, gvacIO);
		if (isNE(gvacIO.getStatuz(),varcom.endp)
		&& isNE(gvacIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvacIO.getParams());
			fatalError600();
		}
		if (isEQ(gvacIO.getStatuz(),varcom.endp)) {
			wsaaGetGvacBfFlag = "Y";
			goTo(GotoLabel.exit7190);
		}
		if (isEQ(sqlChdrcoy,gvacIO.getChdrcoy())
		&& isEQ(sqlStfund,gvacIO.getStatFund())
		&& isEQ(sqlStsect,gvacIO.getStatSect())
		&& isEQ(sqlStssect,gvacIO.getStatSubsect())
		&& isEQ(sqlRegister,gvacIO.getRegister())
		&& isEQ(sqlSrcebus,gvacIO.getSrcebus())
		&& isEQ(sqlCntbranch,gvacIO.getCntbranch())
		&& isEQ(sqlStatcode,gvacIO.getStatcode())
		&& isEQ(sqlCnpstat,gvacIO.getCnPremStat())
		&& isEQ(sqlCrpstat,gvacIO.getCovPremStat())
		&& isEQ(sqlCnttype,gvacIO.getCnttype())
		&& isEQ(sqlCrtable,gvacIO.getCrtable())
		&& isEQ(sqlAcctccy,gvacIO.getAcctccy())
		&& isEQ(sqlParind,gvacIO.getParind())) {
			if (isLT(sqlAcctyr,gvacIO.getAcctyr())) {
				rollForwardUpdate7200();
			}
			else {
				if (isEQ(sqlAcctyr,gvacIO.getAcctyr())) {
					updateBfAmount7300();
				}
				else {
					wsaaGvacBfwdrb.set(gvacIO.getCfwdrb());
					wsaaGvacBfwdxb.set(gvacIO.getCfwdxb());
					wsaaGvacBfwdtb.set(gvacIO.getCfwdtb());
					wsaaGvacBfwdib.set(gvacIO.getCfwdib());
					wsaaGvacBfwddd.set(gvacIO.getCfwddd());
					wsaaGvacBfwddi.set(gvacIO.getCfwddi());
					wsaaGvacBfwdbs.set(gvacIO.getCfwdbs());
					wsaaGvacBfwdmtb.set(gvacIO.getCfwdmtb());
					wsaaGvacBfwdob.set(gvacIO.getCfwdob());
					wsaaGvacBfwdspd.set(gvacIO.getCfwdspd());
					wsaaGvacBfwdfyp.set(gvacIO.getCfwdfyp());
					wsaaGvacBfwdrnp.set(gvacIO.getCfwdrnp());
					wsaaGvacBfwdfyc.set(gvacIO.getCfwdfyc());
					wsaaGvacBfwdrlc.set(gvacIO.getCfwdrlc());
					wsaaGvacBfwdspc.set(gvacIO.getCfwdspc());
					wsaaGvacBfwdrip.set(gvacIO.getCfwdrip());
					wsaaGvacBfwdric.set(gvacIO.getCfwdric());
					wsaaGvacBfwdclr.set(gvacIO.getCfwdclr());
					wsaaGvacBfwdpdb.set(gvacIO.getCfwdpdb());
					wsaaGvacBfwdadb.set(gvacIO.getCfwdadb());
					wsaaGvacBfwdadj.set(gvacIO.getCfwdadj());
					wsaaGvacBfwdint.set(gvacIO.getCfwdint());
					wsaaGvacBfwdadv.set(gvacIO.getCfwdadv());
				}
			}
		}
		else {
			wsaaGetGvacBfFlag = "Y";
			goTo(GotoLabel.exit7190);
		}
	}

protected void readNextRecord7120()
	{
		gvacIO.setFunction(varcom.nextr);
	}

protected void rollForwardUpdate7200()
	{
		para7210();
	}

protected void para7210()
	{
		setPrecision(gvacIO.getBfwdrb(), 2);
		gvacIO.setBfwdrb(add(gvacIO.getBfwdrb(),sqlStaccrb));
		setPrecision(gvacIO.getCfwdrb(), 2);
		gvacIO.setCfwdrb(add(gvacIO.getCfwdrb(),sqlStaccrb));
		setPrecision(gvacIO.getBfwdxb(), 2);
		gvacIO.setBfwdxb(add(gvacIO.getBfwdxb(),sqlStaccxb));
		setPrecision(gvacIO.getCfwdxb(), 2);
		gvacIO.setCfwdxb(add(gvacIO.getCfwdxb(),sqlStaccxb));
		setPrecision(gvacIO.getBfwdtb(), 2);
		gvacIO.setBfwdtb(add(gvacIO.getBfwdtb(),sqlStacctb));
		setPrecision(gvacIO.getCfwdtb(), 2);
		gvacIO.setCfwdtb(add(gvacIO.getCfwdtb(),sqlStacctb));
		setPrecision(gvacIO.getBfwdib(), 2);
		gvacIO.setBfwdib(add(gvacIO.getBfwdib(),sqlStaccib));
		setPrecision(gvacIO.getCfwdib(), 2);
		gvacIO.setCfwdib(add(gvacIO.getCfwdib(),sqlStaccib));
		setPrecision(gvacIO.getBfwddd(), 2);
		gvacIO.setBfwddd(add(gvacIO.getBfwddd(),sqlStaccdd));
		setPrecision(gvacIO.getCfwddd(), 2);
		gvacIO.setCfwddd(add(gvacIO.getCfwddd(),sqlStaccdd));
		setPrecision(gvacIO.getBfwddi(), 2);
		gvacIO.setBfwddi(add(gvacIO.getBfwddi(),sqlStaccdi));
		setPrecision(gvacIO.getCfwddi(), 2);
		gvacIO.setCfwddi(add(gvacIO.getCfwddi(),sqlStaccdi));
		setPrecision(gvacIO.getBfwdbs(), 2);
		gvacIO.setBfwdbs(add(gvacIO.getBfwdbs(),sqlStaccbs));
		setPrecision(gvacIO.getCfwdbs(), 2);
		gvacIO.setCfwdbs(add(gvacIO.getCfwdbs(),sqlStaccbs));
		setPrecision(gvacIO.getBfwdmtb(), 2);
		gvacIO.setBfwdmtb(add(gvacIO.getBfwdmtb(),sqlStaccmtb));
		setPrecision(gvacIO.getCfwdmtb(), 2);
		gvacIO.setCfwdmtb(add(gvacIO.getCfwdmtb(),sqlStaccmtb));
		setPrecision(gvacIO.getBfwdob(), 2);
		gvacIO.setBfwdob(add(gvacIO.getBfwdob(),sqlStaccob));
		setPrecision(gvacIO.getCfwdob(), 2);
		gvacIO.setCfwdob(add(gvacIO.getCfwdob(),sqlStaccob));
		setPrecision(gvacIO.getBfwdspd(), 2);
		gvacIO.setBfwdspd(add(gvacIO.getBfwdspd(),sqlStaccspd));
		setPrecision(gvacIO.getCfwdspd(), 2);
		gvacIO.setCfwdspd(add(gvacIO.getCfwdspd(),sqlStaccspd));
		setPrecision(gvacIO.getBfwdfyp(), 2);
		gvacIO.setBfwdfyp(add(gvacIO.getBfwdfyp(),sqlStaccfyp));
		setPrecision(gvacIO.getCfwdfyp(), 2);
		gvacIO.setCfwdfyp(add(gvacIO.getCfwdfyp(),sqlStaccfyp));
		setPrecision(gvacIO.getBfwdrnp(), 2);
		gvacIO.setBfwdrnp(add(gvacIO.getBfwdrnp(),sqlStaccrnp));
		setPrecision(gvacIO.getCfwdrnp(), 2);
		gvacIO.setCfwdrnp(add(gvacIO.getCfwdrnp(),sqlStaccrnp));
		setPrecision(gvacIO.getBfwdfyc(), 2);
		gvacIO.setBfwdfyc(add(gvacIO.getBfwdfyc(),sqlStaccfyc));
		setPrecision(gvacIO.getCfwdfyc(), 2);
		gvacIO.setCfwdfyc(add(gvacIO.getCfwdfyc(),sqlStaccfyc));
		setPrecision(gvacIO.getBfwdrlc(), 2);
		gvacIO.setBfwdrlc(add(gvacIO.getBfwdrlc(),sqlStaccrlc));
		setPrecision(gvacIO.getCfwdrlc(), 2);
		gvacIO.setCfwdrlc(add(gvacIO.getCfwdrlc(),sqlStaccrlc));
		setPrecision(gvacIO.getBfwdspc(), 2);
		gvacIO.setBfwdspc(add(gvacIO.getBfwdspc(),sqlStaccspc));
		setPrecision(gvacIO.getCfwdspc(), 2);
		gvacIO.setCfwdspc(add(gvacIO.getCfwdspc(),sqlStaccspc));
		setPrecision(gvacIO.getBfwdrip(), 2);
		gvacIO.setBfwdrip(add(gvacIO.getBfwdrip(),sqlStaccrip));
		setPrecision(gvacIO.getCfwdrip(), 2);
		gvacIO.setCfwdrip(add(gvacIO.getCfwdrip(),sqlStaccrip));
		setPrecision(gvacIO.getBfwdric(), 2);
		gvacIO.setBfwdric(add(gvacIO.getBfwdric(),sqlStaccric));
		setPrecision(gvacIO.getCfwdric(), 2);
		gvacIO.setCfwdric(add(gvacIO.getCfwdric(),sqlStaccric));
		setPrecision(gvacIO.getBfwdclr(), 2);
		gvacIO.setBfwdclr(add(gvacIO.getBfwdclr(),sqlStaccclr));
		setPrecision(gvacIO.getCfwdclr(), 2);
		gvacIO.setCfwdclr(add(gvacIO.getCfwdclr(),sqlStaccclr));
		setPrecision(gvacIO.getBfwdpdb(), 2);
		gvacIO.setBfwdpdb(add(gvacIO.getBfwdpdb(),sqlStaccpdb));
		setPrecision(gvacIO.getCfwdpdb(), 2);
		gvacIO.setCfwdpdb(add(gvacIO.getCfwdpdb(),sqlStaccpdb));
		setPrecision(gvacIO.getBfwdadb(), 2);
		gvacIO.setBfwdadb(add(gvacIO.getBfwdadb(),sqlStaccadb));
		setPrecision(gvacIO.getCfwdadb(), 2);
		gvacIO.setCfwdadb(add(gvacIO.getCfwdadb(),sqlStaccadb));
		setPrecision(gvacIO.getBfwdadj(), 2);
		gvacIO.setBfwdadj(add(gvacIO.getBfwdadj(),sqlStaccadj));
		setPrecision(gvacIO.getCfwdadj(), 2);
		gvacIO.setCfwdadj(add(gvacIO.getCfwdadj(),sqlStaccadj));
		setPrecision(gvacIO.getBfwdint(), 2);
		gvacIO.setBfwdint(add(gvacIO.getBfwdint(),sqlStaccint));
		setPrecision(gvacIO.getCfwdint(), 2);
		gvacIO.setCfwdint(add(gvacIO.getCfwdint(),sqlStaccint));
		setPrecision(gvacIO.getBfwdadv(), 2);
		gvacIO.setBfwdadv(add(gvacIO.getBfwdadv(),sqlStaccadv));
		setPrecision(gvacIO.getCfwdadv(), 2);
		gvacIO.setCfwdadv(add(gvacIO.getCfwdadv(),sqlStaccadv));
		gvacIO.setFormat(gvacrec);
		gvacIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, gvacIO);
		if (isNE(gvacIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvacIO.getParams());
			fatalError600();
		}
	}

protected void updateBfAmount7300()
	{
		para7310();
	}

protected void para7310()
	{
		gvacIO.setBfwdrb(wsaaGvacBfwdrb);
		gvacIO.setBfwdxb(wsaaGvacBfwdxb);
		gvacIO.setBfwdtb(wsaaGvacBfwdtb);
		gvacIO.setBfwdib(wsaaGvacBfwdib);
		gvacIO.setBfwddd(wsaaGvacBfwddd);
		gvacIO.setBfwddi(wsaaGvacBfwddi);
		gvacIO.setBfwdbs(wsaaGvacBfwdbs);
		gvacIO.setBfwdmtb(wsaaGvacBfwdmtb);
		gvacIO.setBfwdob(wsaaGvacBfwdob);
		gvacIO.setBfwdspd(wsaaGvacBfwdspd);
		gvacIO.setBfwdfyp(wsaaGvacBfwdfyp);
		gvacIO.setBfwdrnp(wsaaGvacBfwdrnp);
		gvacIO.setBfwdfyc(wsaaGvacBfwdfyc);
		gvacIO.setBfwdrlc(wsaaGvacBfwdrlc);
		gvacIO.setBfwdspc(wsaaGvacBfwdspc);
		gvacIO.setBfwdrip(wsaaGvacBfwdrip);
		gvacIO.setBfwdric(wsaaGvacBfwdric);
		gvacIO.setBfwdclr(wsaaGvacBfwdclr);
		gvacIO.setBfwdpdb(wsaaGvacBfwdpdb);
		gvacIO.setBfwdadb(wsaaGvacBfwdadb);
		gvacIO.setBfwdadj(wsaaGvacBfwdadj);
		gvacIO.setBfwdint(wsaaGvacBfwdint);
		gvacIO.setBfwdadv(wsaaGvacBfwdadv);
		if (isEQ(wsaaNewGvac,"Y")) {
			setPrecision(gvacIO.getCfwdrb(), 2);
			gvacIO.setCfwdrb(add(gvacIO.getCfwdrb(),wsaaGvacBfwdrb));
			setPrecision(gvacIO.getCfwdxb(), 2);
			gvacIO.setCfwdxb(add(gvacIO.getCfwdxb(),wsaaGvacBfwdxb));
			setPrecision(gvacIO.getCfwdtb(), 2);
			gvacIO.setCfwdtb(add(gvacIO.getCfwdtb(),wsaaGvacBfwdtb));
			setPrecision(gvacIO.getCfwdib(), 2);
			gvacIO.setCfwdib(add(gvacIO.getCfwdib(),wsaaGvacBfwdib));
			setPrecision(gvacIO.getCfwddd(), 2);
			gvacIO.setCfwddd(add(gvacIO.getCfwddd(),wsaaGvacBfwddd));
			setPrecision(gvacIO.getCfwddi(), 2);
			gvacIO.setCfwddi(add(gvacIO.getCfwddi(),wsaaGvacBfwddi));
			setPrecision(gvacIO.getCfwdbs(), 2);
			gvacIO.setCfwdbs(add(gvacIO.getCfwdbs(),wsaaGvacBfwdbs));
			setPrecision(gvacIO.getCfwdmtb(), 2);
			gvacIO.setCfwdmtb(add(gvacIO.getCfwdmtb(),wsaaGvacBfwdmtb));
			setPrecision(gvacIO.getCfwdob(), 2);
			gvacIO.setCfwdob(add(gvacIO.getCfwdob(),wsaaGvacBfwdob));
			setPrecision(gvacIO.getCfwdspd(), 2);
			gvacIO.setCfwdspd(add(gvacIO.getCfwdspd(),wsaaGvacBfwdspd));
			setPrecision(gvacIO.getCfwdfyp(), 2);
			gvacIO.setCfwdfyp(add(gvacIO.getCfwdfyp(),wsaaGvacBfwdfyp));
			setPrecision(gvacIO.getCfwdrnp(), 2);
			gvacIO.setCfwdrnp(add(gvacIO.getCfwdrnp(),wsaaGvacBfwdrnp));
			setPrecision(gvacIO.getCfwdfyc(), 2);
			gvacIO.setCfwdfyc(add(gvacIO.getCfwdfyc(),wsaaGvacBfwdfyc));
			setPrecision(gvacIO.getCfwdrlc(), 2);
			gvacIO.setCfwdrlc(add(gvacIO.getCfwdrlc(),wsaaGvacBfwdrlc));
			setPrecision(gvacIO.getCfwdspc(), 2);
			gvacIO.setCfwdspc(add(gvacIO.getCfwdspc(),wsaaGvacBfwdspc));
			setPrecision(gvacIO.getCfwdrip(), 2);
			gvacIO.setCfwdrip(add(gvacIO.getCfwdrip(),wsaaGvacBfwdrip));
			setPrecision(gvacIO.getCfwdric(), 2);
			gvacIO.setCfwdric(add(gvacIO.getCfwdric(),wsaaGvacBfwdric));
			setPrecision(gvacIO.getCfwdclr(), 2);
			gvacIO.setCfwdclr(add(gvacIO.getCfwdclr(),wsaaGvacBfwdclr));
			setPrecision(gvacIO.getCfwdpdb(), 2);
			gvacIO.setCfwdpdb(add(gvacIO.getCfwdpdb(),wsaaGvacBfwdpdb));
			setPrecision(gvacIO.getCfwdadb(), 2);
			gvacIO.setCfwdadb(add(gvacIO.getCfwdadb(),wsaaGvacBfwdadb));
			setPrecision(gvacIO.getCfwdadj(), 2);
			gvacIO.setCfwdadj(add(gvacIO.getCfwdadj(),wsaaGvacBfwdadj));
			setPrecision(gvacIO.getCfwdint(), 2);
			gvacIO.setCfwdint(add(gvacIO.getCfwdint(),wsaaGvacBfwdint));
			setPrecision(gvacIO.getCfwdadv(), 2);
			gvacIO.setCfwdadv(add(gvacIO.getCfwdadv(),wsaaGvacBfwdadv));
		}
		gvacIO.setFormat(gvacrec);
		gvacIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, gvacIO);
		if (isNE(gvacIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvacIO.getParams());
			fatalError600();
		}
	}

protected void readGvah8000()
	{
		para8010();
	}

protected void para8010()
	{
		gvahIO.setParams(SPACES);
		gvahIO.setChdrcoy(wsaaChdrcoy);
		gvahIO.setCntbranch(wsaaCntbranch);
		gvahIO.setAcctccy(wsaaAcctccy);
		gvahIO.setAcctyr(wsaaAcctyr);
		gvahIO.setRegister(wsaaRegister);
		gvahIO.setSrcebus(wsaaSrcebus);
		gvahIO.setCnttype(wsaaCnttype);
		gvahIO.setCnPremStat(wsaaCnpstat);
		gvahIO.setStatcode(wsaaStatcode);
		gvahIO.setFormat(gvahrec);
		gvahIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, gvahIO);
		if (isEQ(gvahIO.getStatuz(),varcom.mrnf)) {
			wsaaNewGvah = "Y";
		}
		else {
			wsaaNewGvah = "N";
		}
	}

protected void updatePostedRecord9000()
	{
		para9010();
	}

protected void para9010()
	{
		if (isEQ(wsaaNewGvah,"Y")) {
			gvahIO.setFunction(varcom.writr);
			gvahIO.setBfwdrb(ZERO);
			gvahIO.setCfwdrb(ZERO);
			gvahIO.setBfwdxb(ZERO);
			gvahIO.setCfwdxb(ZERO);
			gvahIO.setBfwdtb(ZERO);
			gvahIO.setCfwdtb(ZERO);
			gvahIO.setBfwdib(ZERO);
			gvahIO.setCfwdib(ZERO);
			gvahIO.setBfwddd(ZERO);
			gvahIO.setCfwddd(ZERO);
			gvahIO.setBfwddi(ZERO);
			gvahIO.setCfwddi(ZERO);
			gvahIO.setBfwdbs(ZERO);
			gvahIO.setCfwdbs(ZERO);
			gvahIO.setBfwdmtb(ZERO);
			gvahIO.setCfwdmtb(ZERO);
			gvahIO.setBfwdob(ZERO);
			gvahIO.setCfwdob(ZERO);
			gvahIO.setBfwdspd(ZERO);
			gvahIO.setCfwdspd(ZERO);
			gvahIO.setBfwdfyp(ZERO);
			gvahIO.setCfwdfyp(ZERO);
			gvahIO.setBfwdrnp(ZERO);
			gvahIO.setCfwdrnp(ZERO);
			gvahIO.setBfwdfyc(ZERO);
			gvahIO.setCfwdfyc(ZERO);
			gvahIO.setBfwdrlc(ZERO);
			gvahIO.setCfwdrlc(ZERO);
			gvahIO.setBfwdspc(ZERO);
			gvahIO.setCfwdspc(ZERO);
			gvahIO.setBfwdrip(ZERO);
			gvahIO.setCfwdrip(ZERO);
			gvahIO.setBfwdric(ZERO);
			gvahIO.setCfwdric(ZERO);
			gvahIO.setBfwdclr(ZERO);
			gvahIO.setCfwdclr(ZERO);
			gvahIO.setBfwdpdb(ZERO);
			gvahIO.setCfwdpdb(ZERO);
			gvahIO.setBfwdadb(ZERO);
			gvahIO.setCfwdadb(ZERO);
			gvahIO.setBfwdadj(ZERO);
			gvahIO.setCfwdadj(ZERO);
			gvahIO.setBfwdint(ZERO);
			gvahIO.setCfwdint(ZERO);
			gvahIO.setBfwdadv(ZERO);
			gvahIO.setCfwdadv(ZERO);
			for (wsaaI.set(1); !(isGT(wsaaI,12)); wsaaI.add(1)){
				initializeGvahValues9100();
			}
		}
		else {
			gvahIO.setFunction(varcom.updat);
		}
		setPrecision(gvahIO.getStaccrb(sqlAcctmn), 2);
		gvahIO.setStaccrb(sqlAcctmn, add(gvahIO.getStaccrb(sqlAcctmn),wsaaHdrStaccrb));
		setPrecision(gvahIO.getCfwdrb(), 2);
		gvahIO.setCfwdrb(add(gvahIO.getCfwdrb(),wsaaHdrStaccrb));
		setPrecision(gvahIO.getStaccxb(sqlAcctmn), 2);
		gvahIO.setStaccxb(sqlAcctmn, add(gvahIO.getStaccxb(sqlAcctmn),wsaaHdrStaccxb));
		setPrecision(gvahIO.getCfwdxb(), 2);
		gvahIO.setCfwdxb(add(gvahIO.getCfwdxb(),wsaaHdrStaccxb));
		setPrecision(gvahIO.getStacctb(sqlAcctmn), 2);
		gvahIO.setStacctb(sqlAcctmn, add(gvahIO.getStacctb(sqlAcctmn),wsaaHdrStacctb));
		setPrecision(gvahIO.getCfwdtb(), 2);
		gvahIO.setCfwdtb(add(gvahIO.getCfwdtb(),wsaaHdrStacctb));
		setPrecision(gvahIO.getStaccib(sqlAcctmn), 2);
		gvahIO.setStaccib(sqlAcctmn, add(gvahIO.getStaccib(sqlAcctmn),wsaaHdrStaccib));
		setPrecision(gvahIO.getCfwdib(), 2);
		gvahIO.setCfwdib(add(gvahIO.getCfwdib(),wsaaHdrStaccib));
		setPrecision(gvahIO.getStaccdd(sqlAcctmn), 2);
		gvahIO.setStaccdd(sqlAcctmn, add(gvahIO.getStaccdd(sqlAcctmn),wsaaHdrStaccdd));
		setPrecision(gvahIO.getCfwddd(), 2);
		gvahIO.setCfwddd(add(gvahIO.getCfwddd(),wsaaHdrStaccdd));
		setPrecision(gvahIO.getStaccdi(sqlAcctmn), 2);
		gvahIO.setStaccdi(sqlAcctmn, add(gvahIO.getStaccdi(sqlAcctmn),wsaaHdrStaccdi));
		setPrecision(gvahIO.getCfwddi(), 2);
		gvahIO.setCfwddi(add(gvahIO.getCfwddi(),wsaaHdrStaccdi));
		setPrecision(gvahIO.getStaccbs(sqlAcctmn), 2);
		gvahIO.setStaccbs(sqlAcctmn, add(gvahIO.getStaccbs(sqlAcctmn),wsaaHdrStaccbs));
		setPrecision(gvahIO.getCfwdbs(), 2);
		gvahIO.setCfwdbs(add(gvahIO.getCfwdbs(),wsaaHdrStaccbs));
		setPrecision(gvahIO.getStaccmtb(sqlAcctmn), 2);
		gvahIO.setStaccmtb(sqlAcctmn, add(gvahIO.getStaccmtb(sqlAcctmn),wsaaHdrStaccmtb));
		setPrecision(gvahIO.getCfwdmtb(), 2);
		gvahIO.setCfwdmtb(add(gvahIO.getCfwdmtb(),wsaaHdrStaccmtb));
		setPrecision(gvahIO.getStaccob(sqlAcctmn), 2);
		gvahIO.setStaccob(sqlAcctmn, add(gvahIO.getStaccob(sqlAcctmn),wsaaHdrStaccob));
		setPrecision(gvahIO.getCfwdob(), 2);
		gvahIO.setCfwdob(add(gvahIO.getCfwdob(),wsaaHdrStaccob));
		setPrecision(gvahIO.getStaccspd(sqlAcctmn), 2);
		gvahIO.setStaccspd(sqlAcctmn, add(gvahIO.getStaccspd(sqlAcctmn),wsaaHdrStaccspd));
		setPrecision(gvahIO.getCfwdspd(), 2);
		gvahIO.setCfwdspd(add(gvahIO.getCfwdspd(),wsaaHdrStaccspd));
		setPrecision(gvahIO.getStaccfyp(sqlAcctmn), 2);
		gvahIO.setStaccfyp(sqlAcctmn, add(gvahIO.getStaccfyp(sqlAcctmn),wsaaHdrStaccfyp));
		setPrecision(gvahIO.getCfwdfyp(), 2);
		gvahIO.setCfwdfyp(add(gvahIO.getCfwdfyp(),wsaaHdrStaccfyp));
		setPrecision(gvahIO.getStaccrnp(sqlAcctmn), 2);
		gvahIO.setStaccrnp(sqlAcctmn, add(gvahIO.getStaccrnp(sqlAcctmn),wsaaHdrStaccrnp));
		setPrecision(gvahIO.getCfwdrnp(), 2);
		gvahIO.setCfwdrnp(add(gvahIO.getCfwdrnp(),wsaaHdrStaccrnp));
		setPrecision(gvahIO.getStaccfyc(sqlAcctmn), 2);
		gvahIO.setStaccfyc(sqlAcctmn, add(gvahIO.getStaccfyc(sqlAcctmn),wsaaHdrStaccfyc));
		setPrecision(gvahIO.getCfwdfyc(), 2);
		gvahIO.setCfwdfyc(add(gvahIO.getCfwdfyc(),wsaaHdrStaccfyc));
		setPrecision(gvahIO.getStaccrlc(sqlAcctmn), 2);
		gvahIO.setStaccrlc(sqlAcctmn, add(gvahIO.getStaccrlc(sqlAcctmn),wsaaHdrStaccrlc));
		setPrecision(gvahIO.getCfwdrlc(), 2);
		gvahIO.setCfwdrlc(add(gvahIO.getCfwdrlc(),wsaaHdrStaccrlc));
		setPrecision(gvahIO.getStaccspc(sqlAcctmn), 2);
		gvahIO.setStaccspc(sqlAcctmn, add(gvahIO.getStaccspc(sqlAcctmn),wsaaHdrStaccspc));
		setPrecision(gvahIO.getCfwdspc(), 2);
		gvahIO.setCfwdspc(add(gvahIO.getCfwdspc(),wsaaHdrStaccspc));
		setPrecision(gvahIO.getStaccrip(sqlAcctmn), 2);
		gvahIO.setStaccrip(sqlAcctmn, add(gvahIO.getStaccrip(sqlAcctmn),wsaaHdrStaccrip));
		setPrecision(gvahIO.getCfwdrip(), 2);
		gvahIO.setCfwdrip(add(gvahIO.getCfwdrip(),wsaaHdrStaccrip));
		setPrecision(gvahIO.getStaccric(sqlAcctmn), 2);
		gvahIO.setStaccric(sqlAcctmn, add(gvahIO.getStaccric(sqlAcctmn),wsaaHdrStaccric));
		setPrecision(gvahIO.getCfwdric(), 2);
		gvahIO.setCfwdric(add(gvahIO.getCfwdric(),wsaaHdrStaccric));
		setPrecision(gvahIO.getStaccclr(sqlAcctmn), 2);
		gvahIO.setStaccclr(sqlAcctmn, add(gvahIO.getStaccclr(sqlAcctmn),wsaaHdrStaccclr));
		setPrecision(gvahIO.getCfwdclr(), 2);
		gvahIO.setCfwdclr(add(gvahIO.getCfwdclr(),wsaaHdrStaccclr));
		setPrecision(gvahIO.getStaccpdb(sqlAcctmn), 2);
		gvahIO.setStaccpdb(sqlAcctmn, add(gvahIO.getStaccpdb(sqlAcctmn),wsaaHdrStaccpdb));
		setPrecision(gvahIO.getCfwdpdb(), 2);
		gvahIO.setCfwdpdb(add(gvahIO.getCfwdpdb(),wsaaHdrStaccpdb));
		setPrecision(gvahIO.getStaccadb(sqlAcctmn), 2);
		gvahIO.setStaccadb(sqlAcctmn, add(gvahIO.getStaccadb(sqlAcctmn),wsaaHdrStaccadb));
		setPrecision(gvahIO.getCfwdadb(), 2);
		gvahIO.setCfwdadb(add(gvahIO.getCfwdadb(),wsaaHdrStaccadb));
		setPrecision(gvahIO.getStaccadj(sqlAcctmn), 2);
		gvahIO.setStaccadj(sqlAcctmn, add(gvahIO.getStaccadj(sqlAcctmn),wsaaHdrStaccadj));
		setPrecision(gvahIO.getCfwdadj(), 2);
		gvahIO.setCfwdadj(add(gvahIO.getCfwdadj(),wsaaHdrStaccadj));
		setPrecision(gvahIO.getStaccint(sqlAcctmn), 2);
		gvahIO.setStaccint(sqlAcctmn, add(gvahIO.getStaccint(sqlAcctmn),wsaaHdrStaccint));
		setPrecision(gvahIO.getCfwdint(), 2);
		gvahIO.setCfwdint(add(gvahIO.getCfwdint(),wsaaHdrStaccint));
		setPrecision(gvahIO.getStaccadv(sqlAcctmn), 2);
		gvahIO.setStaccadv(sqlAcctmn, add(gvahIO.getStaccadv(sqlAcctmn),wsaaHdrStaccadv));
		setPrecision(gvahIO.getCfwdadv(), 2);
		gvahIO.setCfwdadv(add(gvahIO.getCfwdadv(),wsaaHdrStaccadv));
		gvahIO.setFormat(gvahrec);
		SmartFileCode.execute(appVars, gvahIO);
		if (isNE(gvahIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvahIO.getParams());
			fatalError600();
		}
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
	}

protected void initializeGvahValues9100()
	{
		/*PARA*/
		gvahIO.setStaccrb(wsaaI, ZERO);
		gvahIO.setStaccxb(wsaaI, ZERO);
		gvahIO.setStacctb(wsaaI, ZERO);
		gvahIO.setStaccib(wsaaI, ZERO);
		gvahIO.setStaccdd(wsaaI, ZERO);
		gvahIO.setStaccdi(wsaaI, ZERO);
		gvahIO.setStaccbs(wsaaI, ZERO);
		gvahIO.setStaccmtb(wsaaI, ZERO);
		gvahIO.setStaccob(wsaaI, ZERO);
		gvahIO.setStaccspd(wsaaI, ZERO);
		gvahIO.setStaccfyp(wsaaI, ZERO);
		gvahIO.setStaccrnp(wsaaI, ZERO);
		gvahIO.setStaccfyc(wsaaI, ZERO);
		gvahIO.setStaccrlc(wsaaI, ZERO);
		gvahIO.setStaccspc(wsaaI, ZERO);
		gvahIO.setStaccrip(wsaaI, ZERO);
		gvahIO.setStaccric(wsaaI, ZERO);
		gvahIO.setStaccclr(wsaaI, ZERO);
		gvahIO.setStaccpdb(wsaaI, ZERO);
		gvahIO.setStaccadb(wsaaI, ZERO);
		gvahIO.setStaccadj(wsaaI, ZERO);
		gvahIO.setStaccint(wsaaI, ZERO);
		gvahIO.setStaccadv(wsaaI, ZERO);
		/*EXIT*/
	}

protected void updateGvah10000()
	{
		/*PARA*/
		wsaaGvahBfwdrb.set(ZERO);
		wsaaGvahBfwdxb.set(ZERO);
		wsaaGvahBfwdtb.set(ZERO);
		wsaaGvahBfwdib.set(ZERO);
		wsaaGvahBfwddd.set(ZERO);
		wsaaGvahBfwddi.set(ZERO);
		wsaaGvahBfwdbs.set(ZERO);
		wsaaGvahBfwdmtb.set(ZERO);
		wsaaGvahBfwdob.set(ZERO);
		wsaaGvahBfwdspd.set(ZERO);
		wsaaGvahBfwdfyp.set(ZERO);
		wsaaGvahBfwdrnp.set(ZERO);
		wsaaGvahBfwdfyc.set(ZERO);
		wsaaGvahBfwdrlc.set(ZERO);
		wsaaGvahBfwdspc.set(ZERO);
		wsaaGvahBfwdrip.set(ZERO);
		wsaaGvahBfwdric.set(ZERO);
		wsaaGvahBfwdclr.set(ZERO);
		wsaaGvahBfwdpdb.set(ZERO);
		wsaaGvahBfwdadb.set(ZERO);
		wsaaGvahBfwdadj.set(ZERO);
		wsaaGvahBfwdint.set(ZERO);
		wsaaGvahBfwdadv.set(ZERO);
		gvahIO.setAcctyr(ZERO);
		gvahIO.setFormat(gvahrec);
		gvahIO.setFunction(varcom.begn);
		wsaaGetGvahBfFlag = "N";
		while ( !(isEQ(wsaaGetGvahBfFlag,"Y"))) {
			//performance improvement --  atiwari23
			gvahIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
			gvahIO.setFitKeysSearch("ACCTYR");
			checkBalForwardValue10100();
		}

		/*EXIT*/
	}

protected void checkBalForwardValue10100()
	{
		try {
			para10110();
			readNextRecord10120();
		}
		catch (GOTOException e){
		}
	}

protected void para10110()
	{
		SmartFileCode.execute(appVars, gvahIO);
		if (isNE(gvahIO.getStatuz(),varcom.endp)
		&& isNE(gvahIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvahIO.getParams());
			fatalError600();
		}
		if (isEQ(gvahIO.getStatuz(),varcom.endp)) {
			wsaaGetGvahBfFlag = "Y";
			goTo(GotoLabel.exit10190);
		}
		if (isEQ(wsaaChdrcoy,gvahIO.getChdrcoy())
		&& isEQ(wsaaSrcebus,gvahIO.getSrcebus())
		&& isEQ(wsaaRegister,gvahIO.getRegister())
		&& isEQ(wsaaCntbranch,gvahIO.getCntbranch())
		&& isEQ(wsaaCnpstat,gvahIO.getCnPremStat())
		&& isEQ(wsaaStatcode,gvahIO.getStatcode())
		&& isEQ(wsaaCnttype,gvahIO.getCnttype())
		&& isEQ(wsaaAcctccy,gvahIO.getAcctccy())) {
			if (isLT(wsaaAcctyr,gvahIO.getAcctyr())) {
				rollForwardUpdate10200();
			}
			else {
				if (isEQ(wsaaAcctyr,gvahIO.getAcctyr())) {
					updateBfAmount10300();
				}
				else {
					wsaaGvahBfwdrb.set(gvahIO.getCfwdrb());
					wsaaGvahBfwdxb.set(gvahIO.getCfwdxb());
					wsaaGvahBfwdtb.set(gvahIO.getCfwdtb());
					wsaaGvahBfwdib.set(gvahIO.getCfwdib());
					wsaaGvahBfwddd.set(gvahIO.getCfwddd());
					wsaaGvahBfwddi.set(gvahIO.getCfwddi());
					wsaaGvahBfwdbs.set(gvahIO.getCfwdbs());
					wsaaGvahBfwdmtb.set(gvahIO.getCfwdmtb());
					wsaaGvahBfwdob.set(gvahIO.getCfwdob());
					wsaaGvahBfwdspd.set(gvahIO.getCfwdspd());
					wsaaGvahBfwdfyp.set(gvahIO.getCfwdfyp());
					wsaaGvahBfwdrnp.set(gvahIO.getCfwdrnp());
					wsaaGvahBfwdfyc.set(gvahIO.getCfwdfyc());
					wsaaGvahBfwdrlc.set(gvahIO.getCfwdrlc());
					wsaaGvahBfwdspc.set(gvahIO.getCfwdspc());
					wsaaGvahBfwdrip.set(gvahIO.getCfwdrip());
					wsaaGvahBfwdric.set(gvahIO.getCfwdric());
					wsaaGvahBfwdclr.set(gvahIO.getCfwdclr());
					wsaaGvahBfwdpdb.set(gvahIO.getCfwdpdb());
					wsaaGvahBfwdadb.set(gvahIO.getCfwdadb());
					wsaaGvahBfwdadj.set(gvahIO.getCfwdadj());
					wsaaGvahBfwdint.set(gvahIO.getCfwdint());
					wsaaGvahBfwdadv.set(gvahIO.getCfwdadv());
				}
			}
		}
		else {
			wsaaGetGvahBfFlag = "Y";
			goTo(GotoLabel.exit10190);
		}
	}

protected void readNextRecord10120()
	{
		gvahIO.setFunction(varcom.nextr);
	}

protected void rollForwardUpdate10200()
	{
		para10210();
	}

protected void para10210()
	{
		setPrecision(gvahIO.getBfwdrb(), 2);
		gvahIO.setBfwdrb(add(gvahIO.getBfwdrb(),wsaaHdrStaccrb));
		setPrecision(gvahIO.getCfwdrb(), 2);
		gvahIO.setCfwdrb(add(gvahIO.getCfwdrb(),wsaaHdrStaccrb));
		setPrecision(gvahIO.getBfwdxb(), 2);
		gvahIO.setBfwdxb(add(gvahIO.getBfwdxb(),wsaaHdrStaccxb));
		setPrecision(gvahIO.getCfwdxb(), 2);
		gvahIO.setCfwdxb(add(gvahIO.getCfwdxb(),wsaaHdrStaccxb));
		setPrecision(gvahIO.getBfwdtb(), 2);
		gvahIO.setBfwdtb(add(gvahIO.getBfwdtb(),wsaaHdrStacctb));
		setPrecision(gvahIO.getCfwdtb(), 2);
		gvahIO.setCfwdtb(add(gvahIO.getCfwdtb(),wsaaHdrStacctb));
		setPrecision(gvahIO.getBfwdib(), 2);
		gvahIO.setBfwdib(add(gvahIO.getBfwdib(),wsaaHdrStaccib));
		setPrecision(gvahIO.getCfwdib(), 2);
		gvahIO.setCfwdib(add(gvahIO.getCfwdib(),wsaaHdrStaccib));
		setPrecision(gvahIO.getBfwddd(), 2);
		gvahIO.setBfwddd(add(gvahIO.getBfwddd(),wsaaHdrStaccdd));
		setPrecision(gvahIO.getCfwddd(), 2);
		gvahIO.setCfwddd(add(gvahIO.getCfwddd(),wsaaHdrStaccdd));
		setPrecision(gvahIO.getBfwddi(), 2);
		gvahIO.setBfwddi(add(gvahIO.getBfwddi(),wsaaHdrStaccdi));
		setPrecision(gvahIO.getCfwddi(), 2);
		gvahIO.setCfwddi(add(gvahIO.getCfwddi(),wsaaHdrStaccdi));
		setPrecision(gvahIO.getBfwdbs(), 2);
		gvahIO.setBfwdbs(add(gvahIO.getBfwdbs(),wsaaHdrStaccbs));
		setPrecision(gvahIO.getCfwdbs(), 2);
		gvahIO.setCfwdbs(add(gvahIO.getCfwdbs(),wsaaHdrStaccbs));
		setPrecision(gvahIO.getBfwdmtb(), 2);
		gvahIO.setBfwdmtb(add(gvahIO.getBfwdmtb(),wsaaHdrStaccmtb));
		setPrecision(gvahIO.getCfwdmtb(), 2);
		gvahIO.setCfwdmtb(add(gvahIO.getCfwdmtb(),wsaaHdrStaccmtb));
		setPrecision(gvahIO.getBfwdob(), 2);
		gvahIO.setBfwdob(add(gvahIO.getBfwdob(),wsaaHdrStaccob));
		setPrecision(gvahIO.getCfwdob(), 2);
		gvahIO.setCfwdob(add(gvahIO.getCfwdob(),wsaaHdrStaccob));
		setPrecision(gvahIO.getBfwdspd(), 2);
		gvahIO.setBfwdspd(add(gvahIO.getBfwdspd(),wsaaHdrStaccspd));
		setPrecision(gvahIO.getCfwdspd(), 2);
		gvahIO.setCfwdspd(add(gvahIO.getCfwdspd(),wsaaHdrStaccspd));
		setPrecision(gvahIO.getBfwdfyp(), 2);
		gvahIO.setBfwdfyp(add(gvahIO.getBfwdfyp(),wsaaHdrStaccfyp));
		setPrecision(gvahIO.getCfwdfyp(), 2);
		gvahIO.setCfwdfyp(add(gvahIO.getCfwdfyp(),wsaaHdrStaccfyp));
		setPrecision(gvahIO.getBfwdrnp(), 2);
		gvahIO.setBfwdrnp(add(gvahIO.getBfwdrnp(),wsaaHdrStaccrnp));
		setPrecision(gvahIO.getCfwdrnp(), 2);
		gvahIO.setCfwdrnp(add(gvahIO.getCfwdrnp(),wsaaHdrStaccrnp));
		setPrecision(gvahIO.getBfwdfyc(), 2);
		gvahIO.setBfwdfyc(add(gvahIO.getBfwdfyc(),wsaaHdrStaccfyc));
		setPrecision(gvahIO.getCfwdfyc(), 2);
		gvahIO.setCfwdfyc(add(gvahIO.getCfwdfyc(),wsaaHdrStaccfyc));
		setPrecision(gvahIO.getBfwdrlc(), 2);
		gvahIO.setBfwdrlc(add(gvahIO.getBfwdrlc(),wsaaHdrStaccrlc));
		setPrecision(gvahIO.getCfwdrlc(), 2);
		gvahIO.setCfwdrlc(add(gvahIO.getCfwdrlc(),wsaaHdrStaccrlc));
		setPrecision(gvahIO.getBfwdspc(), 2);
		gvahIO.setBfwdspc(add(gvahIO.getBfwdspc(),wsaaHdrStaccspc));
		setPrecision(gvahIO.getCfwdspc(), 2);
		gvahIO.setCfwdspc(add(gvahIO.getCfwdspc(),wsaaHdrStaccspc));
		setPrecision(gvahIO.getBfwdrip(), 2);
		gvahIO.setBfwdrip(add(gvahIO.getBfwdrip(),wsaaHdrStaccrip));
		setPrecision(gvahIO.getCfwdrip(), 2);
		gvahIO.setCfwdrip(add(gvahIO.getCfwdrip(),wsaaHdrStaccrip));
		setPrecision(gvahIO.getBfwdric(), 2);
		gvahIO.setBfwdric(add(gvahIO.getBfwdric(),wsaaHdrStaccric));
		setPrecision(gvahIO.getCfwdric(), 2);
		gvahIO.setCfwdric(add(gvahIO.getCfwdric(),wsaaHdrStaccric));
		setPrecision(gvahIO.getBfwdclr(), 2);
		gvahIO.setBfwdclr(add(gvahIO.getBfwdclr(),wsaaHdrStaccclr));
		setPrecision(gvahIO.getCfwdclr(), 2);
		gvahIO.setCfwdclr(add(gvahIO.getCfwdclr(),wsaaHdrStaccclr));
		setPrecision(gvahIO.getBfwdpdb(), 2);
		gvahIO.setBfwdpdb(add(gvahIO.getBfwdpdb(),wsaaHdrStaccpdb));
		setPrecision(gvahIO.getCfwdpdb(), 2);
		gvahIO.setCfwdpdb(add(gvahIO.getCfwdpdb(),wsaaHdrStaccpdb));
		setPrecision(gvahIO.getBfwdadb(), 2);
		gvahIO.setBfwdadb(add(gvahIO.getBfwdadb(),wsaaHdrStaccadb));
		setPrecision(gvahIO.getCfwdadb(), 2);
		gvahIO.setCfwdadb(add(gvahIO.getCfwdadb(),wsaaHdrStaccadb));
		setPrecision(gvahIO.getBfwdadj(), 2);
		gvahIO.setBfwdadj(add(gvahIO.getBfwdadj(),wsaaHdrStaccadj));
		setPrecision(gvahIO.getCfwdadj(), 2);
		gvahIO.setCfwdadj(add(gvahIO.getCfwdadj(),wsaaHdrStaccadj));
		setPrecision(gvahIO.getBfwdint(), 2);
		gvahIO.setBfwdint(add(gvahIO.getBfwdint(),wsaaHdrStaccint));
		setPrecision(gvahIO.getCfwdint(), 2);
		gvahIO.setCfwdint(add(gvahIO.getCfwdint(),wsaaHdrStaccint));
		setPrecision(gvahIO.getBfwdadv(), 2);
		gvahIO.setBfwdadv(add(gvahIO.getBfwdadv(),wsaaHdrStaccadv));
		setPrecision(gvahIO.getCfwdadv(), 2);
		gvahIO.setCfwdadv(add(gvahIO.getCfwdadv(),wsaaHdrStaccadv));
		gvahIO.setFormat(gvahrec);
		gvahIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, gvahIO);
		if (isNE(gvahIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvahIO.getParams());
			fatalError600();
		}
	}

protected void updateBfAmount10300()
	{
		para10310();
	}

protected void para10310()
	{
		gvahIO.setBfwdrb(wsaaGvahBfwdrb);
		gvahIO.setBfwdxb(wsaaGvahBfwdxb);
		gvahIO.setBfwdtb(wsaaGvahBfwdtb);
		gvahIO.setBfwdib(wsaaGvahBfwdib);
		gvahIO.setBfwddd(wsaaGvahBfwddd);
		gvahIO.setBfwddi(wsaaGvahBfwddi);
		gvahIO.setBfwdbs(wsaaGvahBfwdbs);
		gvahIO.setBfwdmtb(wsaaGvahBfwdmtb);
		gvahIO.setBfwdob(wsaaGvahBfwdob);
		gvahIO.setBfwdspd(wsaaGvahBfwdspd);
		gvahIO.setBfwdfyp(wsaaGvahBfwdfyp);
		gvahIO.setBfwdrnp(wsaaGvahBfwdrnp);
		gvahIO.setBfwdfyc(wsaaGvahBfwdfyc);
		gvahIO.setBfwdrlc(wsaaGvahBfwdrlc);
		gvahIO.setBfwdspc(wsaaGvahBfwdspc);
		gvahIO.setBfwdrip(wsaaGvahBfwdrip);
		gvahIO.setBfwdric(wsaaGvahBfwdric);
		gvahIO.setBfwdclr(wsaaGvahBfwdclr);
		gvahIO.setBfwdpdb(wsaaGvahBfwdpdb);
		gvahIO.setBfwdadb(wsaaGvahBfwdadb);
		gvahIO.setBfwdadj(wsaaGvahBfwdadj);
		gvahIO.setBfwdint(wsaaGvahBfwdint);
		gvahIO.setBfwdadv(wsaaGvahBfwdadv);
		if (isEQ(wsaaNewGvac,"Y")) {
			setPrecision(gvahIO.getCfwdrb(), 2);
			gvahIO.setCfwdrb(add(gvahIO.getCfwdrb(),wsaaGvahBfwdrb));
			setPrecision(gvahIO.getCfwdxb(), 2);
			gvahIO.setCfwdxb(add(gvahIO.getCfwdxb(),wsaaGvahBfwdxb));
			setPrecision(gvahIO.getCfwdtb(), 2);
			gvahIO.setCfwdtb(add(gvahIO.getCfwdtb(),wsaaGvahBfwdtb));
			setPrecision(gvahIO.getCfwdib(), 2);
			gvahIO.setCfwdib(add(gvahIO.getCfwdib(),wsaaGvahBfwdib));
			setPrecision(gvahIO.getCfwddd(), 2);
			gvahIO.setCfwddd(add(gvahIO.getCfwddd(),wsaaGvahBfwddd));
			setPrecision(gvahIO.getCfwddi(), 2);
			gvahIO.setCfwddi(add(gvahIO.getCfwddi(),wsaaGvahBfwddi));
			setPrecision(gvahIO.getCfwdbs(), 2);
			gvahIO.setCfwdbs(add(gvahIO.getCfwdbs(),wsaaGvahBfwdbs));
			setPrecision(gvahIO.getCfwdmtb(), 2);
			gvahIO.setCfwdmtb(add(gvahIO.getCfwdmtb(),wsaaGvahBfwdmtb));
			setPrecision(gvahIO.getCfwdob(), 2);
			gvahIO.setCfwdob(add(gvahIO.getCfwdob(),wsaaGvahBfwdob));
			setPrecision(gvahIO.getCfwdspd(), 2);
			gvahIO.setCfwdspd(add(gvahIO.getCfwdspd(),wsaaGvahBfwdspd));
			setPrecision(gvahIO.getCfwdfyp(), 2);
			gvahIO.setCfwdfyp(add(gvahIO.getCfwdfyp(),wsaaGvahBfwdfyp));
			setPrecision(gvahIO.getCfwdrnp(), 2);
			gvahIO.setCfwdrnp(add(gvahIO.getCfwdrnp(),wsaaGvahBfwdrnp));
			setPrecision(gvahIO.getCfwdfyc(), 2);
			gvahIO.setCfwdfyc(add(gvahIO.getCfwdfyc(),wsaaGvahBfwdfyc));
			setPrecision(gvahIO.getCfwdrlc(), 2);
			gvahIO.setCfwdrlc(add(gvahIO.getCfwdrlc(),wsaaGvahBfwdrlc));
			setPrecision(gvahIO.getCfwdspc(), 2);
			gvahIO.setCfwdspc(add(gvahIO.getCfwdspc(),wsaaGvahBfwdspc));
			setPrecision(gvahIO.getCfwdrip(), 2);
			gvahIO.setCfwdrip(add(gvahIO.getCfwdrip(),wsaaGvahBfwdrip));
			setPrecision(gvahIO.getCfwdric(), 2);
			gvahIO.setCfwdric(add(gvahIO.getCfwdric(),wsaaGvahBfwdric));
			setPrecision(gvahIO.getCfwdclr(), 2);
			gvahIO.setCfwdclr(add(gvahIO.getCfwdclr(),wsaaGvahBfwdclr));
			setPrecision(gvahIO.getCfwdpdb(), 2);
			gvahIO.setCfwdpdb(add(gvahIO.getCfwdpdb(),wsaaGvahBfwdpdb));
			setPrecision(gvahIO.getCfwdadb(), 2);
			gvahIO.setCfwdadb(add(gvahIO.getCfwdadb(),wsaaGvahBfwdadb));
			setPrecision(gvahIO.getCfwdadj(), 2);
			gvahIO.setCfwdadj(add(gvahIO.getCfwdadj(),wsaaGvahBfwdadj));
			setPrecision(gvahIO.getCfwdint(), 2);
			gvahIO.setCfwdint(add(gvahIO.getCfwdint(),wsaaGvahBfwdint));
			setPrecision(gvahIO.getCfwdadv(), 2);
			gvahIO.setCfwdadv(add(gvahIO.getCfwdadv(),wsaaGvahBfwdadv));
		}
		gvahIO.setFormat(gvahrec);
		gvahIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, gvahIO);
		if (isNE(gvahIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvahIO.getParams());
			fatalError600();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
