package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SJ675
 * @version 1.0 generated on 30/08/09 07:06
 * @author Quipoz
 */
public class Sj675ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(288);
	public FixedLengthStringData dataFields = new FixedLengthStringData(64).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData statSects = new FixedLengthStringData(20).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] statSect = FLSArrayPartOfStructure(10, 2, statSects, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(statSects, 0, FILLER_REDEFINE);
	public FixedLengthStringData statSect01 = DD.stsect.copy().isAPartOf(filler,0);
	public FixedLengthStringData statSect02 = DD.stsect.copy().isAPartOf(filler,2);
	public FixedLengthStringData statSect03 = DD.stsect.copy().isAPartOf(filler,4);
	public FixedLengthStringData statSect04 = DD.stsect.copy().isAPartOf(filler,6);
	public FixedLengthStringData statSect05 = DD.stsect.copy().isAPartOf(filler,8);
	public FixedLengthStringData statSect06 = DD.stsect.copy().isAPartOf(filler,10);
	public FixedLengthStringData statSect07 = DD.stsect.copy().isAPartOf(filler,12);
	public FixedLengthStringData statSect08 = DD.stsect.copy().isAPartOf(filler,14);
	public FixedLengthStringData statSect09 = DD.stsect.copy().isAPartOf(filler,16);
	public FixedLengthStringData statSect10 = DD.stsect.copy().isAPartOf(filler,18);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,59);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(56).isAPartOf(dataArea, 64);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData stsectsErr = new FixedLengthStringData(40).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] stsectErr = FLSArrayPartOfStructure(10, 4, stsectsErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(40).isAPartOf(stsectsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData stsect01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData stsect02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData stsect03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData stsect04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData stsect05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData stsect06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData stsect07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData stsect08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData stsect09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData stsect10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(168).isAPartOf(dataArea, 120);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData stsectsOut = new FixedLengthStringData(120).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] stsectOut = FLSArrayPartOfStructure(10, 12, stsectsOut, 0);
	public FixedLengthStringData[][] stsectO = FLSDArrayPartOfArrayStructure(12, 1, stsectOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(120).isAPartOf(stsectsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] stsect01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] stsect02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] stsect03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] stsect04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] stsect05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] stsect06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] stsect07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] stsect08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] stsect09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] stsect10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sj675screenWritten = new LongData(0);
	public LongData Sj675protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sj675ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(stsect01Out,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsect02Out,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsect03Out,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsect04Out,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsect05Out,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsect06Out,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsect07Out,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsect08Out,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsect09Out,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(stsect10Out,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, tabl, item, longdesc, statSect01, statSect02, statSect03, statSect04, statSect05, statSect06, statSect07, statSect08, statSect09, statSect10};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, stsect01Out, stsect02Out, stsect03Out, stsect04Out, stsect05Out, stsect06Out, stsect07Out, stsect08Out, stsect09Out, stsect10Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, stsect01Err, stsect02Err, stsect03Err, stsect04Err, stsect05Err, stsect06Err, stsect07Err, stsect08Err, stsect09Err, stsect10Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sj675screen.class;
		protectRecord = Sj675protect.class;
	}

}
