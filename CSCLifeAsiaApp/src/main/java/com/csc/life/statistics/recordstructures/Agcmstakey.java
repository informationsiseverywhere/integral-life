package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:02
 * Description:
 * Copybook name: AGCMSTAKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmstakey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmstaFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmstaKey = new FixedLengthStringData(64).isAPartOf(agcmstaFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmstaChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmstaKey, 0);
  	public FixedLengthStringData agcmstaChdrnum = new FixedLengthStringData(8).isAPartOf(agcmstaKey, 1);
  	public FixedLengthStringData agcmstaLife = new FixedLengthStringData(2).isAPartOf(agcmstaKey, 9);
  	public FixedLengthStringData agcmstaCoverage = new FixedLengthStringData(2).isAPartOf(agcmstaKey, 11);
  	public FixedLengthStringData agcmstaRider = new FixedLengthStringData(2).isAPartOf(agcmstaKey, 13);
  	public PackedDecimalData agcmstaPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmstaKey, 15);
  	public FixedLengthStringData agcmstaAgntnum = new FixedLengthStringData(8).isAPartOf(agcmstaKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(agcmstaKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmstaFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmstaFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}