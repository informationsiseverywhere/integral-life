/*********************  */
/*Author  :Liwei						*/
/*Purpose :Instead of subroutine Rlagprd*/
/*Date    :2018.10.26					*/
package com.csc.life.statistics.procedures;

public interface RlagprdUtils {
	public void callRlagprd(RlagprdPojo rlagprdPojo);
}
