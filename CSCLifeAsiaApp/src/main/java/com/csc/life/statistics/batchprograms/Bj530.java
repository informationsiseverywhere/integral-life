/*
 * File: Bj530.java
 * Date: 29 August 2009 21:43:52
 * Author: Quipoz Limited
 *
 * Class transformed from BJ530.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.statistics.dataaccess.GvacstsTableDAM;
import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj530Report;
import com.csc.life.statistics.tablestructures.Tj675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.datatype.Dbcstrcpy2;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*              Overall Business Portfolio
*              ==========================
*
*   This report will list the Overall Business Portfolio for the
*     specified period.
*
*   There are 2 levels of reporting, product level and business
*     level, it is reporting for each subsection. It is recognized
*     by the 2nd and 3rd characters represent of the sub-section
*     (GVACSTS-STSSECT) i.e. Life Business, General Annuity,
*     Pension, Linked Business, Non-Linked Business.
*
*   The basic procedure division logic is for reading and
*     printing a simple input primary file. The overall structure
*     is as follows:
*
*   1000-INITIALISE section.
*   Read table TR386 using BSSC-LANGUAGE and WSAA-PROG.Store the
*     information in the working storage array. This table is an
*     extra data multiple screen.
*     Read GOVE using SQL.Selet records where ACCTYR=PJ517-ACCTYR
*       Order by company,accounting currenc,register,            y
*       source of business, statutory section, statutory subsection,
*       contract type and coverage type.
*
*
*   2000-READ-FILE section.
*     Fetch the SQL record. If end-of-file, set WSSP-EDTERROR to ENDP.
*
*   2500-EDIT section.
*   Set WSSP-EDITERROR to O-K.
*
*   3000-UPDATE section.
*   If there is a change in company, accounting currency or
*     register or Business Category
*     Print the Product level Totals
*       Print the Reassurance Ceded
*       Print the Totals after reassurance ceded
*     Print the Business Level
*       Print Before Reassurance header line
*       Print all the details from working storage table
*       Print Total before reassurance
*       Print After Reassurance header line
*    Skip to a new page and print report heading
*    End-if.
*
*    If there is a change in company, accounting currency or
*      register or Business Category or Product type
*      You will need to use the indicator to decide which heading
*        and amounts to print.
*
*   For each SQL record read, do the following:
*     Read GVACSTS using function READR to get the STACIB and    ount
*       STACCXB and STACCRB and STACCTB.
*
*     Read T3629 using SQL-CNTCURR to get the currency rate. Amounts
*       coming from GOVE are in premium currency.
*
*     If Accounting currency is not equal to Premium currency,
*        Convert the amounts to accounting currency amount by
*        multiplying with T3629-SCRATE.
*
*    To convert the Contract currency amounts to Accounting
*      currency amount before the caculation by multiplying
*      T3629-SCRATE for the following fields:
*
*      GOVE-STIMTH(PJ517-ACCTMNTH)
*      GOVE-STAMTH(PJ517-ACCTMNTH)
*      GOVE-STBMTH(PJ517-ACCTMNTH)
*
*    Accumulate the Overall Business Transacted details: for
*      Product level and the Business level as well.
*
*    Product Level: we need to differentiate between 'Fully Paid up
*      and 'Reduced Paid up' then Regular and Single premium for
*      each product for Non-Linked Business and Health Insurance
*      only.
*
*    Business Level: we need to differentiate between 'Life      up
*      Business', 'General annuity' and 'Pension'.
*
*   Control totals:  (Product Level exclude RI)
*     01  -  Totals of read records
*     02  -  Totals of pages printed
*     03  -  Totals of policy count
*     04  -  Totals of lives count
*     05  -  Totals of sum assured
*     06  -  Totals of Annuity PA
*     07  -  Totals of basic prem
*     08  -  Totals of vested bonus
*     09  -  Totals of terminal bonus
*
*   Error Processing:
*     If a system error move the error code into the SYSR-STATUZ
*     If a database error move the XXXX-PARAMS to SYSR-PARAMS.
*     Perform the 600-FATAL-ERROR section.
*
*   These remarks must be replaced by what the program actually
*     does.
*
*****************************************************************
* </pre>
*/
public class Bj530 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgovepfAllrs = null;
	private java.sql.PreparedStatement sqlgovepfAllps = null;
	private java.sql.Connection sqlgovepfAllconn = null;
	private String sqlgovepfAll = "";
	private java.sql.ResultSet sqlgovepfrs = null;
	private java.sql.PreparedStatement sqlgovepfps = null;
	private java.sql.Connection sqlgovepfconn = null;
	private String sqlgovepf = "";
	private Rj530Report rj530 = new Rj530Report();
	private FixedLengthStringData rj530Rec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ530");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private String wsaaFirstRecord = "";
	private String wsaaFound = "";
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);
	private ZonedDecimalData wsaaScrate = new ZonedDecimalData(12, 7);
	private ZonedDecimalData wsaaX = new ZonedDecimalData(2, 0).setUnsigned();
	private String wsaaFp = "FP";
	private String wsaaPu = "PU";
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaStcmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStlmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStimthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStamthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStbmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStsmthc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStraamtc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaRiStbamt = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaVestedBonus = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTerminalBonus = new PackedDecimalData(18, 2);
	private ZonedDecimalData wsaaRatio = new ZonedDecimalData(12, 9).setUnsigned();
	private ZonedDecimalData mth = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaSelectAll = new FixedLengthStringData(1);
	private Validator selectAll = new Validator(wsaaSelectAll, "Y");
	private FixedLengthStringData wsaaTj675Stsect01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect02 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect03 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect04 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect05 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect06 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect07 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect08 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect09 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect10 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaStssect = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaSrcebus = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaReg = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaProduct = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaBlProduct = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaTblCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaTblCnttype = new FixedLengthStringData(3);

		/* WSAA-AMOUNT-AREA */
	private FixedLengthStringData wsaaFullyPaidUp = new FixedLengthStringData(69);
	private PackedDecimalData wsaaFpuNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaFullyPaidUp, 0).init(0);
	private PackedDecimalData wsaaFpuNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaFullyPaidUp, 10).init(0);
	private PackedDecimalData wsaaFpuSum = new PackedDecimalData(18, 2).isAPartOf(wsaaFullyPaidUp, 20).init(0);
	private PackedDecimalData wsaaFpuAnnty = new PackedDecimalData(18, 2).isAPartOf(wsaaFullyPaidUp, 30).init(0);
	private PackedDecimalData wsaaFpuBprem = new PackedDecimalData(16, 0).isAPartOf(wsaaFullyPaidUp, 40).init(0);
	private PackedDecimalData wsaaFpuVbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaFullyPaidUp, 49).init(0);
	private PackedDecimalData wsaaFpuTbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaFullyPaidUp, 59).init(0);

	private FixedLengthStringData wsaaRegularPremium = new FixedLengthStringData(69);
	private PackedDecimalData wsaaRegNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 0).init(0);
	private PackedDecimalData wsaaRegNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 10).init(0);
	private PackedDecimalData wsaaRegSum = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 20).init(0);
	private PackedDecimalData wsaaRegAnnty = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 30).init(0);
	private PackedDecimalData wsaaRegBprem = new PackedDecimalData(16, 0).isAPartOf(wsaaRegularPremium, 40).init(0);
	private PackedDecimalData wsaaRegVbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 49).init(0);
	private PackedDecimalData wsaaRegTbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaRegularPremium, 59).init(0);

	private FixedLengthStringData wsaaSinglePremium = new FixedLengthStringData(69);
	private PackedDecimalData wsaaSglNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 0).init(0);
	private PackedDecimalData wsaaSglNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 10).init(0);
	private PackedDecimalData wsaaSglSum = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 20).init(0);
	private PackedDecimalData wsaaSglAnnty = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 30).init(0);
	private PackedDecimalData wsaaSglBprem = new PackedDecimalData(16, 0).isAPartOf(wsaaSinglePremium, 40).init(0);
	private PackedDecimalData wsaaSglVbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 49).init(0);
	private PackedDecimalData wsaaSglTbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaSinglePremium, 59).init(0);

	private FixedLengthStringData wsaaReducedPaidUp = new FixedLengthStringData(69);
	private PackedDecimalData wsaaRpuNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaReducedPaidUp, 0).init(0);
	private PackedDecimalData wsaaRpuNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaReducedPaidUp, 10).init(0);
	private PackedDecimalData wsaaRpuSum = new PackedDecimalData(18, 2).isAPartOf(wsaaReducedPaidUp, 20).init(0);
	private PackedDecimalData wsaaRpuAnnty = new PackedDecimalData(18, 2).isAPartOf(wsaaReducedPaidUp, 30).init(0);
	private PackedDecimalData wsaaRpuBprem = new PackedDecimalData(16, 0).isAPartOf(wsaaReducedPaidUp, 40).init(0);
	private PackedDecimalData wsaaRpuVbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaReducedPaidUp, 49).init(0);
	private PackedDecimalData wsaaRpuTbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaReducedPaidUp, 59).init(0);

	private FixedLengthStringData wsaaBusinessLevelArea = new FixedLengthStringData(420);
	private FixedLengthStringData[] wsaaBusinessLevel = FLSArrayPartOfStructure(3, 70, wsaaBusinessLevelArea, 0);
	private FixedLengthStringData[] wsaaBlCnttype = FLSDArrayPartOfArrayStructure(1, wsaaBusinessLevel, 0);
	private PackedDecimalData[] wsaaBlNofpol = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevel, 1);
	private PackedDecimalData[] wsaaBlNoflif = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevel, 11);
	private PackedDecimalData[] wsaaBlSum = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevel, 21);
	private PackedDecimalData[] wsaaBlAnnty = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevel, 31);
	private PackedDecimalData[] wsaaBlBprem = PDArrayPartOfArrayStructure(16, 0, wsaaBusinessLevel, 41);
	private PackedDecimalData[] wsaaBlVbonu = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevel, 50);
	private PackedDecimalData[] wsaaBlTbonu = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevel, 60);
	private FixedLengthStringData[] wsaaBusinessLevelRi = FLSArrayPartOfStructure(3, 70, wsaaBusinessLevelArea, 210);
	private FixedLengthStringData[] wsaaBlRiCnttype = FLSDArrayPartOfArrayStructure(1, wsaaBusinessLevelRi, 0);
	private PackedDecimalData[] wsaaBlRiNofpol = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevelRi, 1);
	private PackedDecimalData[] wsaaBlRiNoflif = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevelRi, 11);
	private PackedDecimalData[] wsaaBlRiSum = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevelRi, 21);
	private PackedDecimalData[] wsaaBlRiAnnty = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevelRi, 31);
	private PackedDecimalData[] wsaaBlRiBprem = PDArrayPartOfArrayStructure(16, 0, wsaaBusinessLevelRi, 41);
	private PackedDecimalData[] wsaaBlRiVbonu = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevelRi, 50);
	private PackedDecimalData[] wsaaBlRiTbonu = PDArrayPartOfArrayStructure(18, 2, wsaaBusinessLevelRi, 60);

		/* WSAA-TOTAL-AREAS */
	private FixedLengthStringData wsaaProductTotal = new FixedLengthStringData(69);
	private PackedDecimalData wsaaPrdTotNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaProductTotal, 0).init(0);
	private PackedDecimalData wsaaPrdTotNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaProductTotal, 10).init(0);
	private PackedDecimalData wsaaPrdTotSum = new PackedDecimalData(18, 2).isAPartOf(wsaaProductTotal, 20).init(0);
	private PackedDecimalData wsaaPrdTotAnnty = new PackedDecimalData(18, 2).isAPartOf(wsaaProductTotal, 30).init(0);
	private PackedDecimalData wsaaPrdTotBprem = new PackedDecimalData(16, 0).isAPartOf(wsaaProductTotal, 40).init(0);
	private PackedDecimalData wsaaPrdTotVbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaProductTotal, 49).init(0);
	private PackedDecimalData wsaaPrdTotTbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaProductTotal, 59).init(0);

	private FixedLengthStringData wsaaReassuranceTotal = new FixedLengthStringData(69);
	private PackedDecimalData wsaaRiTotNofpol = new PackedDecimalData(18, 2).isAPartOf(wsaaReassuranceTotal, 0).init(0);
	private PackedDecimalData wsaaRiTotNoflif = new PackedDecimalData(18, 2).isAPartOf(wsaaReassuranceTotal, 10).init(0);
	private PackedDecimalData wsaaRiTotSum = new PackedDecimalData(18, 2).isAPartOf(wsaaReassuranceTotal, 20).init(0);
	private PackedDecimalData wsaaRiTotAnnty = new PackedDecimalData(18, 2).isAPartOf(wsaaReassuranceTotal, 30).init(0);
	private PackedDecimalData wsaaRiTotBprem = new PackedDecimalData(16, 0).isAPartOf(wsaaReassuranceTotal, 40).init(0);
	private PackedDecimalData wsaaRiTotVbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaReassuranceTotal, 49).init(0);
	private PackedDecimalData wsaaRiTotTbonu = new PackedDecimalData(18, 2).isAPartOf(wsaaReassuranceTotal, 59).init(0);
		/* WSAA-BL-TOTAL-AREA */
	private PackedDecimalData wsaaBlNofpolT = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaBlNoflifT = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaBlSumT = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaBlAnntyT = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaBlBpremT = new PackedDecimalData(16, 0).init(0);
	private PackedDecimalData wsaaBlVbonuT = new PackedDecimalData(18, 2).init(0);
	private PackedDecimalData wsaaBlTbonuT = new PackedDecimalData(18, 2).init(0);

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaLanguage = new FixedLengthStringData(1).isAPartOf(wsaaItem, 0);
	private FixedLengthStringData wsaaProgram = new FixedLengthStringData(5).isAPartOf(wsaaItem, 1);

	private FixedLengthStringData wsaaSqlKeep = new FixedLengthStringData(1017);
	private FixedLengthStringData wsaaSqlChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 0);
	private FixedLengthStringData wsaaSqlStatcat = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 1);
	private PackedDecimalData wsaaSqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaSqlKeep, 3);
	private FixedLengthStringData wsaaSqlStfund = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 6);
	private FixedLengthStringData wsaaSqlStsect = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 7);
	private FixedLengthStringData wsaaSqlStssect = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 9);
	private FixedLengthStringData wsaaSqlRegister = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 13);
	private FixedLengthStringData wsaaSqlCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 16);
	private FixedLengthStringData wsaaSqlCrpstat = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 18);
	private FixedLengthStringData wsaaSqlCnpstat = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 20);
	private FixedLengthStringData wsaaSqlCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 22);
	private FixedLengthStringData wsaaSqlCrtable = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 25);
	private FixedLengthStringData wsaaSqlAcctccy = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 29);
	private FixedLengthStringData wsaaSqlCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 32);
	private FixedLengthStringData wsaaSqlBillfreq = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 35);
	private PackedDecimalData wsaaBfwdc = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 37);
	private FixedLengthStringData wsaaStcmths = new FixedLengthStringData(60).isAPartOf(wsaaSqlKeep, 47);
	private PackedDecimalData[] wsaaStcmth = PDArrayPartOfStructure(12, 9, 0, wsaaStcmths, 0);
	private PackedDecimalData wsaaBfwdl = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 107);
	private FixedLengthStringData wsaaStlmths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 117);
	private PackedDecimalData[] wsaaStlmth = PDArrayPartOfStructure(12, 18, 2, wsaaStlmths, 0);
	private PackedDecimalData wsaaBfwdi = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 237);
	private FixedLengthStringData wsaaStimths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 247);
	private PackedDecimalData[] wsaaStimth = PDArrayPartOfStructure(12, 18, 2, wsaaStimths, 0);
	private PackedDecimalData wsaaBfwda = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 367);
	private FixedLengthStringData wsaaStamths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 377);
	private PackedDecimalData[] wsaaStamth = PDArrayPartOfStructure(12, 18, 2, wsaaStamths, 0);
	private PackedDecimalData wsaaBfwdb = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 497);
	private FixedLengthStringData wsaaStbmthgs = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 507);
	private PackedDecimalData[] wsaaStbmthg = PDArrayPartOfStructure(12, 18, 2, wsaaStbmthgs, 0);
	private PackedDecimalData wsaaBfwds = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 627);
	private FixedLengthStringData wsaaStsmths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 637);
	private PackedDecimalData[] wsaaStsmth = PDArrayPartOfStructure(12, 18, 2, wsaaStsmths, 0);
	private PackedDecimalData wsaaBfwdp = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 757);
	private FixedLengthStringData wsaaStpmths = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 767);
	private PackedDecimalData[] wsaaStpmth = PDArrayPartOfStructure(12, 18, 2, wsaaStpmths, 0);
	private PackedDecimalData wsaaBfwdra = new PackedDecimalData(18, 2).isAPartOf(wsaaSqlKeep, 887);
	private FixedLengthStringData wsaaStraamts = new FixedLengthStringData(120).isAPartOf(wsaaSqlKeep, 897);
	private PackedDecimalData[] wsaaStraamt = PDArrayPartOfStructure(12, 18, 2, wsaaStraamts, 0);
		/* ERRORS */
	private String esql = "ESQL";
	private String g418 = "G418";

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-GOVEPF */
	private FixedLengthStringData sqlGoverec = new FixedLengthStringData(1017);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 0);
	private FixedLengthStringData sqlStatcat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 1);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGoverec, 3);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGoverec, 6);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 7);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 9);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 13);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 16);
	private FixedLengthStringData sqlCrpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 18);
	private FixedLengthStringData sqlCnpstat = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 20);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 22);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGoverec, 25);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 29);
	private FixedLengthStringData sqlCntcurr = new FixedLengthStringData(3).isAPartOf(sqlGoverec, 32);
	private FixedLengthStringData sqlBillfreq = new FixedLengthStringData(2).isAPartOf(sqlGoverec, 35);
	private PackedDecimalData sqlBfwdc = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 37);
	private PackedDecimalData sqlStcmth01 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 47);
	private PackedDecimalData sqlStcmth02 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 52);
	private PackedDecimalData sqlStcmth03 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 57);
	private PackedDecimalData sqlStcmth04 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 62);
	private PackedDecimalData sqlStcmth05 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 67);
	private PackedDecimalData sqlStcmth06 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 72);
	private PackedDecimalData sqlStcmth07 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 77);
	private PackedDecimalData sqlStcmth08 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 82);
	private PackedDecimalData sqlStcmth09 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 87);
	private PackedDecimalData sqlStcmth10 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 92);
	private PackedDecimalData sqlStcmth11 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 97);
	private PackedDecimalData sqlStcmth12 = new PackedDecimalData(9, 0).isAPartOf(sqlGoverec, 102);
	private PackedDecimalData sqlBfwdl = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 107);
	private PackedDecimalData sqlStlmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 117);
	private PackedDecimalData sqlStlmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 127);
	private PackedDecimalData sqlStlmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 137);
	private PackedDecimalData sqlStlmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 147);
	private PackedDecimalData sqlStlmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 157);
	private PackedDecimalData sqlStlmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 167);
	private PackedDecimalData sqlStlmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 177);
	private PackedDecimalData sqlStlmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 187);
	private PackedDecimalData sqlStlmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 197);
	private PackedDecimalData sqlStlmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 207);
	private PackedDecimalData sqlStlmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 217);
	private PackedDecimalData sqlStlmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 227);
	private PackedDecimalData sqlBfwdi = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 237);
	private PackedDecimalData sqlStimth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 247);
	private PackedDecimalData sqlStimth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 257);
	private PackedDecimalData sqlStimth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 267);
	private PackedDecimalData sqlStimth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 277);
	private PackedDecimalData sqlStimth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 287);
	private PackedDecimalData sqlStimth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 297);
	private PackedDecimalData sqlStimth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 307);
	private PackedDecimalData sqlStimth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 317);
	private PackedDecimalData sqlStimth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 327);
	private PackedDecimalData sqlStimth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 337);
	private PackedDecimalData sqlStimth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 347);
	private PackedDecimalData sqlStimth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 357);
	private PackedDecimalData sqlBfwda = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 367);
	private PackedDecimalData sqlStamth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 377);
	private PackedDecimalData sqlStamth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 387);
	private PackedDecimalData sqlStamth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 397);
	private PackedDecimalData sqlStamth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 407);
	private PackedDecimalData sqlStamth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 417);
	private PackedDecimalData sqlStamth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 427);
	private PackedDecimalData sqlStamth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 437);
	private PackedDecimalData sqlStamth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 447);
	private PackedDecimalData sqlStamth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 457);
	private PackedDecimalData sqlStamth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 467);
	private PackedDecimalData sqlStamth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 477);
	private PackedDecimalData sqlStamth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 487);
	private PackedDecimalData sqlBfwdb = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 497);
	private PackedDecimalData sqlStbmthg01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 507);
	private PackedDecimalData sqlStbmthg02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 517);
	private PackedDecimalData sqlStbmthg03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 527);
	private PackedDecimalData sqlStbmthg04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 537);
	private PackedDecimalData sqlStbmthg05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 547);
	private PackedDecimalData sqlStbmthg06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 557);
	private PackedDecimalData sqlStbmthg07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 567);
	private PackedDecimalData sqlStbmthg08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 577);
	private PackedDecimalData sqlStbmthg09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 587);
	private PackedDecimalData sqlStbmthg10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 597);
	private PackedDecimalData sqlStbmthg11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 607);
	private PackedDecimalData sqlStbmthg12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 617);
	private PackedDecimalData sqlBfwds = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 627);
	private PackedDecimalData sqlStsmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 637);
	private PackedDecimalData sqlStsmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 647);
	private PackedDecimalData sqlStsmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 657);
	private PackedDecimalData sqlStsmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 667);
	private PackedDecimalData sqlStsmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 677);
	private PackedDecimalData sqlStsmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 687);
	private PackedDecimalData sqlStsmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 697);
	private PackedDecimalData sqlStsmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 707);
	private PackedDecimalData sqlStsmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 717);
	private PackedDecimalData sqlStsmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 727);
	private PackedDecimalData sqlStsmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 737);
	private PackedDecimalData sqlStsmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 747);
	private PackedDecimalData sqlBfwdp = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 757);
	private PackedDecimalData sqlStpmth01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 767);
	private PackedDecimalData sqlStpmth02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 777);
	private PackedDecimalData sqlStpmth03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 787);
	private PackedDecimalData sqlStpmth04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 797);
	private PackedDecimalData sqlStpmth05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 807);
	private PackedDecimalData sqlStpmth06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 817);
	private PackedDecimalData sqlStpmth07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 827);
	private PackedDecimalData sqlStpmth08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 837);
	private PackedDecimalData sqlStpmth09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 847);
	private PackedDecimalData sqlStpmth10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 857);
	private PackedDecimalData sqlStpmth11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 867);
	private PackedDecimalData sqlStpmth12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 877);
	private PackedDecimalData sqlBfwdra = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 887);
	private PackedDecimalData sqlStraamt01 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 897);
	private PackedDecimalData sqlStraamt02 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 907);
	private PackedDecimalData sqlStraamt03 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 917);
	private PackedDecimalData sqlStraamt04 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 927);
	private PackedDecimalData sqlStraamt05 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 937);
	private PackedDecimalData sqlStraamt06 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 947);
	private PackedDecimalData sqlStraamt07 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 957);
	private PackedDecimalData sqlStraamt08 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 967);
	private PackedDecimalData sqlStraamt09 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 977);
	private PackedDecimalData sqlStraamt10 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 987);
	private PackedDecimalData sqlStraamt11 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 997);
	private PackedDecimalData sqlStraamt12 = new PackedDecimalData(18, 2).isAPartOf(sqlGoverec, 1007);
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
	private String gvacstsrec = "GVACSTSREC";
	private String t1693 = "T1693";
	private String t3589 = "T3589";
	private String t3629 = "T3629";
	private String t5684 = "T5684";
	private String t5685 = "T5685";
	private String t5688 = "T5688";
	private String t6697 = "T6697";
	private String t6625 = "T6625";
	private String tj675 = "TJ675";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;
	private int ct05 = 5;
	private int ct06 = 6;
	private int ct07 = 7;
	private int ct08 = 8;
	private int ct09 = 9;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rj530H01 = new FixedLengthStringData(179);
	private FixedLengthStringData rj530h01O = new FixedLengthStringData(179).isAPartOf(rj530H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj530h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj530h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj530h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj530h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj530h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj530h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj530h01O, 50);
	private FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(rj530h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj530h01O, 83);
	private FixedLengthStringData stsect = new FixedLengthStringData(2).isAPartOf(rj530h01O, 113);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30).isAPartOf(rj530h01O, 115);
	private FixedLengthStringData stssect = new FixedLengthStringData(4).isAPartOf(rj530h01O, 145);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rj530h01O, 149);

	private FixedLengthStringData rj530H02 = new FixedLengthStringData(2);

	private FixedLengthStringData rj530D01 = new FixedLengthStringData(30);
	private FixedLengthStringData rj530d01O = new FixedLengthStringData(30).isAPartOf(rj530D01, 0);
	private FixedLengthStringData cntdesc = new FixedLengthStringData(30).isAPartOf(rj530d01O, 0);

	private FixedLengthStringData rj530D02 = new FixedLengthStringData(78);
	private FixedLengthStringData rj530d02O = new FixedLengthStringData(78).isAPartOf(rj530D02, 0);
	private ZonedDecimalData stcmthg = new ZonedDecimalData(9, 0).isAPartOf(rj530d02O, 0);
	private ZonedDecimalData stlmth = new ZonedDecimalData(9, 0).isAPartOf(rj530d02O, 9);
	private ZonedDecimalData newtotsi01 = new ZonedDecimalData(12, 0).isAPartOf(rj530d02O, 18);
	private ZonedDecimalData newtotsi02 = new ZonedDecimalData(12, 0).isAPartOf(rj530d02O, 30);
	private ZonedDecimalData newtotsi03 = new ZonedDecimalData(12, 0).isAPartOf(rj530d02O, 42);
	private ZonedDecimalData newtotsi04 = new ZonedDecimalData(12, 0).isAPartOf(rj530d02O, 54);
	private ZonedDecimalData newtotsi05 = new ZonedDecimalData(12, 0).isAPartOf(rj530d02O, 66);

	private FixedLengthStringData rj530D03 = new FixedLengthStringData(2);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Dbcstrcpy2 dbcstrcpy2 = new Dbcstrcpy2();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Statistic Accumulation for policy admin*/
	private GvacstsTableDAM gvacstsIO = new GvacstsTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Pj517par pj517par = new Pj517par();
	private T3629rec t3629rec = new T3629rec();
	private Tj675rec tj675rec = new Tj675rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090,
		a220CallIo,
		a290Exit,
		a420CallItemio,
		h590Exit
	}

	public Bj530() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		rj530.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsaaAcctyr.set(pj517par.acctyr);
		wsspEdterror.set(varcom.oK);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaProgram.set(wsaaProg);
		wsaaLanguage.set(bsscIO.getLanguage());
		acctyear.set(pj517par.acctyr);
		acctmonth.set(pj517par.acctmnth);
		a100InitialAllWs();
		wsaaBlCnttype[1].set("1");
		wsaaBlRiCnttype[1].set("1");
		wsaaBlCnttype[2].set("2");
		wsaaBlRiCnttype[2].set("2");
		wsaaBlCnttype[3].set("3");
		wsaaBlRiCnttype[3].set("3");
		for (wsaaX.set(1); !(isGT(wsaaX,12)); wsaaX.add(1)){
			wsaaStcmth[wsaaX.toInt()].set(0);
			wsaaStlmth[wsaaX.toInt()].set(0);
			wsaaStimth[wsaaX.toInt()].set(0);
			wsaaStsmth[wsaaX.toInt()].set(0);
			wsaaStamth[wsaaX.toInt()].set(0);
			wsaaStbmthg[wsaaX.toInt()].set(0);
		}
		wsaaX.set(0);
		wsaaStimthc.set(0);
		wsaaStamthc.set(0);
		wsaaStsmthc.set(0);
		wsaaStbmthc.set(0);
		wsaaStsect.set(SPACES);
		wsaaStssect.set(SPACES);
		wsaaBlProduct.set(SPACES);
		wsaaProduct.set(SPACES);
		wsaaCrtable.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaReg.set(SPACES);
		wsaaFirstRecord = "Y";
		wsaaFound = "N";
		readTj6751200();
	}

protected void readTj6751200()
	{
		para1210();
	}

protected void para1210()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tj675);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		wsaaSelectAll.set("Y");
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			selectAll1300();
		}
		else {
			wsaaSelectAll.set("N");
			tj675rec.tj675Rec.set(itemIO.getGenarea());
			selectSpecified1400();
		}
	}

protected void selectAll1300()
	{
		/*PARA*/
		sqlgovepfAll = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, BILLFREQ, BFWDC, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, BFWDL, STLMTH01, STLMTH02, STLMTH03, STLMTH04, STLMTH05, STLMTH06, STLMTH07, STLMTH08, STLMTH09, STLMTH10, STLMTH11, STLMTH12, BFWDI, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, BFWDA, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12, BFWDB, STBMTHG01, STBMTHG02, STBMTHG03, STBMTHG04, STBMTHG05, STBMTHG06, STBMTHG07, STBMTHG08, STBMTHG09, STBMTHG10, STBMTHG11, STBMTHG12, BFWDS, STSMTH01, STSMTH02, STSMTH03, STSMTH04, STSMTH05, STSMTH06, STSMTH07, STSMTH08, STSMTH09, STSMTH10, STSMTH11, STSMTH12, BFWDP, STPMTH01, STPMTH02, STPMTH03, STPMTH04, STPMTH05, STPMTH06, STPMTH07, STPMTH08, STPMTH09, STPMTH10, STPMTH11, STPMTH12, BFWDRA, STRAAMT01, STRAAMT02, STRAAMT03, STRAAMT04, STRAAMT05, STRAAMT06, STRAAMT07, STRAAMT08, STRAAMT09, STRAAMT10, STRAAMT11, STRAAMT12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE ACCTYR = ?" +
" AND (STATCAT = 'IF'" +
" OR STATCAT = 'IN'" +
" OR STATCAT = 'DE')" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfAllconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfAllps = appVars.prepareStatementEmbeded(sqlgovepfAllconn, sqlgovepfAll, "GOVEPF");
			appVars.setDBDouble(sqlgovepfAllps, 1, wsaaAcctyr.toDouble());
			sqlgovepfAllrs = appVars.executeQuery(sqlgovepfAllps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*EXIT*/
	}

protected void selectSpecified1400()
	{
		para1410();
	}

protected void para1410()
	{
		wsaaTj675Stsect01.set(tj675rec.statSect[1]);
		wsaaTj675Stsect02.set(tj675rec.statSect[2]);
		wsaaTj675Stsect03.set(tj675rec.statSect[3]);
		wsaaTj675Stsect04.set(tj675rec.statSect[4]);
		wsaaTj675Stsect05.set(tj675rec.statSect[5]);
		wsaaTj675Stsect06.set(tj675rec.statSect[6]);
		wsaaTj675Stsect07.set(tj675rec.statSect[7]);
		wsaaTj675Stsect08.set(tj675rec.statSect[8]);
		wsaaTj675Stsect09.set(tj675rec.statSect[9]);
		wsaaTj675Stsect10.set(tj675rec.statSect[10]);
		sqlgovepf = " SELECT  CHDRCOY, STATCAT, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CRPSTAT, CNPSTAT, CNTTYPE, CRTABLE, ACCTCCY, CNTCURR, BILLFREQ, BFWDC, STCMTH01, STCMTH02, STCMTH03, STCMTH04, STCMTH05, STCMTH06, STCMTH07, STCMTH08, STCMTH09, STCMTH10, STCMTH11, STCMTH12, BFWDL, STLMTH01, STLMTH02, STLMTH03, STLMTH04, STLMTH05, STLMTH06, STLMTH07, STLMTH08, STLMTH09, STLMTH10, STLMTH11, STLMTH12, BFWDI, STIMTH01, STIMTH02, STIMTH03, STIMTH04, STIMTH05, STIMTH06, STIMTH07, STIMTH08, STIMTH09, STIMTH10, STIMTH11, STIMTH12, BFWDA, STAMTH01, STAMTH02, STAMTH03, STAMTH04, STAMTH05, STAMTH06, STAMTH07, STAMTH08, STAMTH09, STAMTH10, STAMTH11, STAMTH12, BFWDB, STBMTHG01, STBMTHG02, STBMTHG03, STBMTHG04, STBMTHG05, STBMTHG06, STBMTHG07, STBMTHG08, STBMTHG09, STBMTHG10, STBMTHG11, STBMTHG12, BFWDS, STSMTH01, STSMTH02, STSMTH03, STSMTH04, STSMTH05, STSMTH06, STSMTH07, STSMTH08, STSMTH09, STSMTH10, STSMTH11, STSMTH12, BFWDP, STPMTH01, STPMTH02, STPMTH03, STPMTH04, STPMTH05, STPMTH06, STPMTH07, STPMTH08, STPMTH09, STPMTH10, STPMTH11, STPMTH12, BFWDRA, STRAAMT01, STRAAMT02, STRAAMT03, STRAAMT04, STRAAMT05, STRAAMT06, STRAAMT07, STRAAMT08, STRAAMT09, STRAAMT10, STRAAMT11, STRAAMT12" + //ILIFE-3496
" FROM   " + appVars.getTableNameOverriden("GOVEPF") + " " +
" WHERE ACCTYR = ?" +
" AND (STATCAT = 'IF'" +
" OR STATCAT = 'IN'" +
" OR STATCAT = 'DE')" +
" AND (STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?)" +
" ORDER BY CHDRCOY, ACCTCCY, REG, STSECT, STSSECT, CNTTYPE, CRTABLE"; //ILIFE-3496
		sqlerrorflag = false;
		try {
			sqlgovepfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GovepfTableDAM());
			sqlgovepfps = appVars.prepareStatementEmbeded(sqlgovepfconn, sqlgovepf, "GOVEPF");
			appVars.setDBDouble(sqlgovepfps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgovepfps, 2, wsaaTj675Stsect01.toString());
			appVars.setDBString(sqlgovepfps, 3, wsaaTj675Stsect02.toString());
			appVars.setDBString(sqlgovepfps, 4, wsaaTj675Stsect03.toString());
			appVars.setDBString(sqlgovepfps, 5, wsaaTj675Stsect04.toString());
			appVars.setDBString(sqlgovepfps, 6, wsaaTj675Stsect05.toString());
			appVars.setDBString(sqlgovepfps, 7, wsaaTj675Stsect06.toString());
			appVars.setDBString(sqlgovepfps, 8, wsaaTj675Stsect07.toString());
			appVars.setDBString(sqlgovepfps, 9, wsaaTj675Stsect08.toString());
			appVars.setDBString(sqlgovepfps, 10, wsaaTj675Stsect09.toString());
			appVars.setDBString(sqlgovepfps, 11, wsaaTj675Stsect10.toString());
			sqlgovepfrs = appVars.executeQuery(sqlgovepfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		if (selectAll.isTrue()) {
			sqlerrorflag = false;
			try {
				if (sqlgovepfAllrs.next()) {
					appVars.getDBObject(sqlgovepfAllrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfAllrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfAllrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfAllrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfAllrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfAllrs, 6, sqlStssect);
					appVars.getDBObject(sqlgovepfAllrs, 7, sqlRegister);
					appVars.getDBObject(sqlgovepfAllrs, 8, sqlCntbranch);
					appVars.getDBObject(sqlgovepfAllrs, 9, sqlCrpstat);
					appVars.getDBObject(sqlgovepfAllrs, 10, sqlCnpstat);
					appVars.getDBObject(sqlgovepfAllrs, 11, sqlCnttype);
					appVars.getDBObject(sqlgovepfAllrs, 12, sqlCrtable);
					appVars.getDBObject(sqlgovepfAllrs, 13, sqlAcctccy);
					appVars.getDBObject(sqlgovepfAllrs, 14, sqlCntcurr);
					appVars.getDBObject(sqlgovepfAllrs, 15, sqlBillfreq);
					appVars.getDBObject(sqlgovepfAllrs, 16, sqlBfwdc);
					appVars.getDBObject(sqlgovepfAllrs, 17, sqlStcmth01);
					appVars.getDBObject(sqlgovepfAllrs, 18, sqlStcmth02);
					appVars.getDBObject(sqlgovepfAllrs, 19, sqlStcmth03);
					appVars.getDBObject(sqlgovepfAllrs, 20, sqlStcmth04);
					appVars.getDBObject(sqlgovepfAllrs, 21, sqlStcmth05);
					appVars.getDBObject(sqlgovepfAllrs, 22, sqlStcmth06);
					appVars.getDBObject(sqlgovepfAllrs, 23, sqlStcmth07);
					appVars.getDBObject(sqlgovepfAllrs, 24, sqlStcmth08);
					appVars.getDBObject(sqlgovepfAllrs, 25, sqlStcmth09);
					appVars.getDBObject(sqlgovepfAllrs, 26, sqlStcmth10);
					appVars.getDBObject(sqlgovepfAllrs, 27, sqlStcmth11);
					appVars.getDBObject(sqlgovepfAllrs, 28, sqlStcmth12);
					appVars.getDBObject(sqlgovepfAllrs, 29, sqlBfwdl);
					appVars.getDBObject(sqlgovepfAllrs, 30, sqlStlmth01);
					appVars.getDBObject(sqlgovepfAllrs, 31, sqlStlmth02);
					appVars.getDBObject(sqlgovepfAllrs, 32, sqlStlmth03);
					appVars.getDBObject(sqlgovepfAllrs, 33, sqlStlmth04);
					appVars.getDBObject(sqlgovepfAllrs, 34, sqlStlmth05);
					appVars.getDBObject(sqlgovepfAllrs, 35, sqlStlmth06);
					appVars.getDBObject(sqlgovepfAllrs, 36, sqlStlmth07);
					appVars.getDBObject(sqlgovepfAllrs, 37, sqlStlmth08);
					appVars.getDBObject(sqlgovepfAllrs, 38, sqlStlmth09);
					appVars.getDBObject(sqlgovepfAllrs, 39, sqlStlmth10);
					appVars.getDBObject(sqlgovepfAllrs, 40, sqlStlmth11);
					appVars.getDBObject(sqlgovepfAllrs, 41, sqlStlmth12);
					appVars.getDBObject(sqlgovepfAllrs, 42, sqlBfwdi);
					appVars.getDBObject(sqlgovepfAllrs, 43, sqlStimth01);
					appVars.getDBObject(sqlgovepfAllrs, 44, sqlStimth02);
					appVars.getDBObject(sqlgovepfAllrs, 45, sqlStimth03);
					appVars.getDBObject(sqlgovepfAllrs, 46, sqlStimth04);
					appVars.getDBObject(sqlgovepfAllrs, 47, sqlStimth05);
					appVars.getDBObject(sqlgovepfAllrs, 48, sqlStimth06);
					appVars.getDBObject(sqlgovepfAllrs, 49, sqlStimth07);
					appVars.getDBObject(sqlgovepfAllrs, 50, sqlStimth08);
					appVars.getDBObject(sqlgovepfAllrs, 51, sqlStimth09);
					appVars.getDBObject(sqlgovepfAllrs, 52, sqlStimth10);
					appVars.getDBObject(sqlgovepfAllrs, 53, sqlStimth11);
					appVars.getDBObject(sqlgovepfAllrs, 54, sqlStimth12);
					appVars.getDBObject(sqlgovepfAllrs, 55, sqlBfwda);
					appVars.getDBObject(sqlgovepfAllrs, 56, sqlStamth01);
					appVars.getDBObject(sqlgovepfAllrs, 57, sqlStamth02);
					appVars.getDBObject(sqlgovepfAllrs, 58, sqlStamth03);
					appVars.getDBObject(sqlgovepfAllrs, 59, sqlStamth04);
					appVars.getDBObject(sqlgovepfAllrs, 60, sqlStamth05);
					appVars.getDBObject(sqlgovepfAllrs, 61, sqlStamth06);
					appVars.getDBObject(sqlgovepfAllrs, 62, sqlStamth07);
					appVars.getDBObject(sqlgovepfAllrs, 63, sqlStamth08);
					appVars.getDBObject(sqlgovepfAllrs, 64, sqlStamth09);
					appVars.getDBObject(sqlgovepfAllrs, 65, sqlStamth10);
					appVars.getDBObject(sqlgovepfAllrs, 66, sqlStamth11);
					appVars.getDBObject(sqlgovepfAllrs, 67, sqlStamth12);
					appVars.getDBObject(sqlgovepfAllrs, 68, sqlBfwdb);
					appVars.getDBObject(sqlgovepfAllrs, 69, sqlStbmthg01);
					appVars.getDBObject(sqlgovepfAllrs, 70, sqlStbmthg02);
					appVars.getDBObject(sqlgovepfAllrs, 71, sqlStbmthg03);
					appVars.getDBObject(sqlgovepfAllrs, 72, sqlStbmthg04);
					appVars.getDBObject(sqlgovepfAllrs, 73, sqlStbmthg05);
					appVars.getDBObject(sqlgovepfAllrs, 74, sqlStbmthg06);
					appVars.getDBObject(sqlgovepfAllrs, 75, sqlStbmthg07);
					appVars.getDBObject(sqlgovepfAllrs, 76, sqlStbmthg08);
					appVars.getDBObject(sqlgovepfAllrs, 77, sqlStbmthg09);
					appVars.getDBObject(sqlgovepfAllrs, 78, sqlStbmthg10);
					appVars.getDBObject(sqlgovepfAllrs, 79, sqlStbmthg11);
					appVars.getDBObject(sqlgovepfAllrs, 80, sqlStbmthg12);
					appVars.getDBObject(sqlgovepfAllrs, 81, sqlBfwds);
					appVars.getDBObject(sqlgovepfAllrs, 82, sqlStsmth01);
					appVars.getDBObject(sqlgovepfAllrs, 83, sqlStsmth02);
					appVars.getDBObject(sqlgovepfAllrs, 84, sqlStsmth03);
					appVars.getDBObject(sqlgovepfAllrs, 85, sqlStsmth04);
					appVars.getDBObject(sqlgovepfAllrs, 86, sqlStsmth05);
					appVars.getDBObject(sqlgovepfAllrs, 87, sqlStsmth06);
					appVars.getDBObject(sqlgovepfAllrs, 88, sqlStsmth07);
					appVars.getDBObject(sqlgovepfAllrs, 89, sqlStsmth08);
					appVars.getDBObject(sqlgovepfAllrs, 90, sqlStsmth09);
					appVars.getDBObject(sqlgovepfAllrs, 91, sqlStsmth10);
					appVars.getDBObject(sqlgovepfAllrs, 92, sqlStsmth11);
					appVars.getDBObject(sqlgovepfAllrs, 93, sqlStsmth12);
					appVars.getDBObject(sqlgovepfAllrs, 94, sqlBfwdp);
					appVars.getDBObject(sqlgovepfAllrs, 95, sqlStpmth01);
					appVars.getDBObject(sqlgovepfAllrs, 96, sqlStpmth02);
					appVars.getDBObject(sqlgovepfAllrs, 97, sqlStpmth03);
					appVars.getDBObject(sqlgovepfAllrs, 98, sqlStpmth04);
					appVars.getDBObject(sqlgovepfAllrs, 99, sqlStpmth05);
					appVars.getDBObject(sqlgovepfAllrs, 100, sqlStpmth06);
					appVars.getDBObject(sqlgovepfAllrs, 101, sqlStpmth07);
					appVars.getDBObject(sqlgovepfAllrs, 102, sqlStpmth08);
					appVars.getDBObject(sqlgovepfAllrs, 103, sqlStpmth09);
					appVars.getDBObject(sqlgovepfAllrs, 104, sqlStpmth10);
					appVars.getDBObject(sqlgovepfAllrs, 105, sqlStpmth11);
					appVars.getDBObject(sqlgovepfAllrs, 106, sqlStpmth12);
					appVars.getDBObject(sqlgovepfAllrs, 107, sqlBfwdra);
					appVars.getDBObject(sqlgovepfAllrs, 108, sqlStraamt01);
					appVars.getDBObject(sqlgovepfAllrs, 109, sqlStraamt02);
					appVars.getDBObject(sqlgovepfAllrs, 110, sqlStraamt03);
					appVars.getDBObject(sqlgovepfAllrs, 111, sqlStraamt04);
					appVars.getDBObject(sqlgovepfAllrs, 112, sqlStraamt05);
					appVars.getDBObject(sqlgovepfAllrs, 113, sqlStraamt06);
					appVars.getDBObject(sqlgovepfAllrs, 114, sqlStraamt07);
					appVars.getDBObject(sqlgovepfAllrs, 115, sqlStraamt08);
					appVars.getDBObject(sqlgovepfAllrs, 116, sqlStraamt09);
					appVars.getDBObject(sqlgovepfAllrs, 117, sqlStraamt10);
					appVars.getDBObject(sqlgovepfAllrs, 118, sqlStraamt11);
					appVars.getDBObject(sqlgovepfAllrs, 119, sqlStraamt12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		else {
			sqlerrorflag = false;
			try {
				if (sqlgovepfrs.next()) {
					appVars.getDBObject(sqlgovepfrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgovepfrs, 2, sqlStatcat);
					appVars.getDBObject(sqlgovepfrs, 3, sqlAcctyr);
					appVars.getDBObject(sqlgovepfrs, 4, sqlStfund);
					appVars.getDBObject(sqlgovepfrs, 5, sqlStsect);
					appVars.getDBObject(sqlgovepfrs, 6, sqlStssect);
					appVars.getDBObject(sqlgovepfrs, 7, sqlRegister);
					appVars.getDBObject(sqlgovepfrs, 8, sqlCntbranch);
					appVars.getDBObject(sqlgovepfrs, 9, sqlCrpstat);
					appVars.getDBObject(sqlgovepfrs, 10, sqlCnpstat);
					appVars.getDBObject(sqlgovepfrs, 11, sqlCnttype);
					appVars.getDBObject(sqlgovepfrs, 12, sqlCrtable);
					appVars.getDBObject(sqlgovepfrs, 13, sqlAcctccy);
					appVars.getDBObject(sqlgovepfrs, 14, sqlCntcurr);
					appVars.getDBObject(sqlgovepfrs, 15, sqlBillfreq);
					appVars.getDBObject(sqlgovepfrs, 16, sqlBfwdc);
					appVars.getDBObject(sqlgovepfrs, 17, sqlStcmth01);
					appVars.getDBObject(sqlgovepfrs, 18, sqlStcmth02);
					appVars.getDBObject(sqlgovepfrs, 19, sqlStcmth03);
					appVars.getDBObject(sqlgovepfrs, 20, sqlStcmth04);
					appVars.getDBObject(sqlgovepfrs, 21, sqlStcmth05);
					appVars.getDBObject(sqlgovepfrs, 22, sqlStcmth06);
					appVars.getDBObject(sqlgovepfrs, 23, sqlStcmth07);
					appVars.getDBObject(sqlgovepfrs, 24, sqlStcmth08);
					appVars.getDBObject(sqlgovepfrs, 25, sqlStcmth09);
					appVars.getDBObject(sqlgovepfrs, 26, sqlStcmth10);
					appVars.getDBObject(sqlgovepfrs, 27, sqlStcmth11);
					appVars.getDBObject(sqlgovepfrs, 28, sqlStcmth12);
					appVars.getDBObject(sqlgovepfrs, 29, sqlBfwdl);
					appVars.getDBObject(sqlgovepfrs, 30, sqlStlmth01);
					appVars.getDBObject(sqlgovepfrs, 31, sqlStlmth02);
					appVars.getDBObject(sqlgovepfrs, 32, sqlStlmth03);
					appVars.getDBObject(sqlgovepfrs, 33, sqlStlmth04);
					appVars.getDBObject(sqlgovepfrs, 34, sqlStlmth05);
					appVars.getDBObject(sqlgovepfrs, 35, sqlStlmth06);
					appVars.getDBObject(sqlgovepfrs, 36, sqlStlmth07);
					appVars.getDBObject(sqlgovepfrs, 37, sqlStlmth08);
					appVars.getDBObject(sqlgovepfrs, 38, sqlStlmth09);
					appVars.getDBObject(sqlgovepfrs, 39, sqlStlmth10);
					appVars.getDBObject(sqlgovepfrs, 40, sqlStlmth11);
					appVars.getDBObject(sqlgovepfrs, 41, sqlStlmth12);
					appVars.getDBObject(sqlgovepfrs, 42, sqlBfwdi);
					appVars.getDBObject(sqlgovepfrs, 43, sqlStimth01);
					appVars.getDBObject(sqlgovepfrs, 44, sqlStimth02);
					appVars.getDBObject(sqlgovepfrs, 45, sqlStimth03);
					appVars.getDBObject(sqlgovepfrs, 46, sqlStimth04);
					appVars.getDBObject(sqlgovepfrs, 47, sqlStimth05);
					appVars.getDBObject(sqlgovepfrs, 48, sqlStimth06);
					appVars.getDBObject(sqlgovepfrs, 49, sqlStimth07);
					appVars.getDBObject(sqlgovepfrs, 50, sqlStimth08);
					appVars.getDBObject(sqlgovepfrs, 51, sqlStimth09);
					appVars.getDBObject(sqlgovepfrs, 52, sqlStimth10);
					appVars.getDBObject(sqlgovepfrs, 53, sqlStimth11);
					appVars.getDBObject(sqlgovepfrs, 54, sqlStimth12);
					appVars.getDBObject(sqlgovepfrs, 55, sqlBfwda);
					appVars.getDBObject(sqlgovepfrs, 56, sqlStamth01);
					appVars.getDBObject(sqlgovepfrs, 57, sqlStamth02);
					appVars.getDBObject(sqlgovepfrs, 58, sqlStamth03);
					appVars.getDBObject(sqlgovepfrs, 59, sqlStamth04);
					appVars.getDBObject(sqlgovepfrs, 60, sqlStamth05);
					appVars.getDBObject(sqlgovepfrs, 61, sqlStamth06);
					appVars.getDBObject(sqlgovepfrs, 62, sqlStamth07);
					appVars.getDBObject(sqlgovepfrs, 63, sqlStamth08);
					appVars.getDBObject(sqlgovepfrs, 64, sqlStamth09);
					appVars.getDBObject(sqlgovepfrs, 65, sqlStamth10);
					appVars.getDBObject(sqlgovepfrs, 66, sqlStamth11);
					appVars.getDBObject(sqlgovepfrs, 67, sqlStamth12);
					appVars.getDBObject(sqlgovepfrs, 68, sqlBfwdb);
					appVars.getDBObject(sqlgovepfrs, 69, sqlStbmthg01);
					appVars.getDBObject(sqlgovepfrs, 70, sqlStbmthg02);
					appVars.getDBObject(sqlgovepfrs, 71, sqlStbmthg03);
					appVars.getDBObject(sqlgovepfrs, 72, sqlStbmthg04);
					appVars.getDBObject(sqlgovepfrs, 73, sqlStbmthg05);
					appVars.getDBObject(sqlgovepfrs, 74, sqlStbmthg06);
					appVars.getDBObject(sqlgovepfrs, 75, sqlStbmthg07);
					appVars.getDBObject(sqlgovepfrs, 76, sqlStbmthg08);
					appVars.getDBObject(sqlgovepfrs, 77, sqlStbmthg09);
					appVars.getDBObject(sqlgovepfrs, 78, sqlStbmthg10);
					appVars.getDBObject(sqlgovepfrs, 79, sqlStbmthg11);
					appVars.getDBObject(sqlgovepfrs, 80, sqlStbmthg12);
					appVars.getDBObject(sqlgovepfrs, 81, sqlBfwds);
					appVars.getDBObject(sqlgovepfrs, 82, sqlStsmth01);
					appVars.getDBObject(sqlgovepfrs, 83, sqlStsmth02);
					appVars.getDBObject(sqlgovepfrs, 84, sqlStsmth03);
					appVars.getDBObject(sqlgovepfrs, 85, sqlStsmth04);
					appVars.getDBObject(sqlgovepfrs, 86, sqlStsmth05);
					appVars.getDBObject(sqlgovepfrs, 87, sqlStsmth06);
					appVars.getDBObject(sqlgovepfrs, 88, sqlStsmth07);
					appVars.getDBObject(sqlgovepfrs, 89, sqlStsmth08);
					appVars.getDBObject(sqlgovepfrs, 90, sqlStsmth09);
					appVars.getDBObject(sqlgovepfrs, 91, sqlStsmth10);
					appVars.getDBObject(sqlgovepfrs, 92, sqlStsmth11);
					appVars.getDBObject(sqlgovepfrs, 93, sqlStsmth12);
					appVars.getDBObject(sqlgovepfrs, 94, sqlBfwdp);
					appVars.getDBObject(sqlgovepfrs, 95, sqlStpmth01);
					appVars.getDBObject(sqlgovepfrs, 96, sqlStpmth02);
					appVars.getDBObject(sqlgovepfrs, 97, sqlStpmth03);
					appVars.getDBObject(sqlgovepfrs, 98, sqlStpmth04);
					appVars.getDBObject(sqlgovepfrs, 99, sqlStpmth05);
					appVars.getDBObject(sqlgovepfrs, 100, sqlStpmth06);
					appVars.getDBObject(sqlgovepfrs, 101, sqlStpmth07);
					appVars.getDBObject(sqlgovepfrs, 102, sqlStpmth08);
					appVars.getDBObject(sqlgovepfrs, 103, sqlStpmth09);
					appVars.getDBObject(sqlgovepfrs, 104, sqlStpmth10);
					appVars.getDBObject(sqlgovepfrs, 105, sqlStpmth11);
					appVars.getDBObject(sqlgovepfrs, 106, sqlStpmth12);
					appVars.getDBObject(sqlgovepfrs, 107, sqlBfwdra);
					appVars.getDBObject(sqlgovepfrs, 108, sqlStraamt01);
					appVars.getDBObject(sqlgovepfrs, 109, sqlStraamt02);
					appVars.getDBObject(sqlgovepfrs, 110, sqlStraamt03);
					appVars.getDBObject(sqlgovepfrs, 111, sqlStraamt04);
					appVars.getDBObject(sqlgovepfrs, 112, sqlStraamt05);
					appVars.getDBObject(sqlgovepfrs, 113, sqlStraamt06);
					appVars.getDBObject(sqlgovepfrs, 114, sqlStraamt07);
					appVars.getDBObject(sqlgovepfrs, 115, sqlStraamt08);
					appVars.getDBObject(sqlgovepfrs, 116, sqlStraamt09);
					appVars.getDBObject(sqlgovepfrs, 117, sqlStraamt10);
					appVars.getDBObject(sqlgovepfrs, 118, sqlStraamt11);
					appVars.getDBObject(sqlgovepfrs, 119, sqlStraamt12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
			if (sqlerrorflag) {
				sqlError500();
			}
		}
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		wsaaSqlKeep.set(sqlGoverec);
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		if (isNE(wsaaFirstRecord,"Y")) {
			h200PrintProductLevel();
			h300PrintTotal();
			h400PrintBusinessLevel();
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		if (isEQ(wsaaFirstRecord,"Y")) {
			wsaaFirstRecord = "N";
			wsaaOverflow.set("Y");
		}
		else {
			if (isNE(wsaaStsect,sqlStsect)
			|| isNE(wsaaStssect,sqlStssect)
			|| isNE(wsaaReg,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaCompany,sqlChdrcoy)
			|| isNE(wsaaBranch,sqlCntbranch)) {
				h000GetHeading();
				h200PrintProductLevel();
				h300PrintTotal();
				h400PrintBusinessLevel();
				a100InitialAllWs();
			}
			else {
				if (isNE(wsaaProduct,sqlCnttype)) {
					h200PrintProductLevel();
					a000InitialProductWs();
				}
			}
		}
		a200GetGvacsts();
		for (mth.set(1); !(isGT(mth,pj517par.acctmnth)); mth.add(1)){
			if (isEQ(wsaaSqlStatcat,"IF")) {
				wsaaStcmthc.add(wsaaStcmth[mth.toInt()]);
				wsaaStlmthc.add(wsaaStlmth[mth.toInt()]);
			}
			wsaaStimthc.add(wsaaStimth[mth.toInt()]);
			wsaaStamthc.add(wsaaStamth[mth.toInt()]);
			wsaaStbmthc.add(wsaaStbmthg[mth.toInt()]);
			wsaaStraamtc.add(wsaaStraamt[mth.toInt()]);
			wsaaStsmthc.add(wsaaStsmth[mth.toInt()]);
		}
		if (isEQ(wsaaSqlStatcat,"IF")) {
			wsaaStcmthc.add(wsaaBfwdc);
			wsaaStlmthc.add(wsaaBfwdl);
		}
		wsaaStimthc.add(wsaaBfwdi);
		wsaaStamthc.add(wsaaBfwda);
		wsaaStbmthc.add(wsaaBfwdb);
		wsaaStraamtc.add(wsaaBfwdra);
		wsaaStsmthc.add(wsaaBfwds);
		if (isNE(sqlCntcurr,sqlAcctccy)) {
			a400ReadT3629();
			compute(wsaaStimthc, 8).setRounded(mult(wsaaStimthc,wsaaScrate));
			compute(wsaaStamthc, 8).setRounded(mult(wsaaStamthc,wsaaScrate));
			compute(wsaaStbmthc, 8).setRounded(mult(wsaaStbmthc,wsaaScrate));
			compute(wsaaStsmthc, 8).setRounded(mult(wsaaStsmthc,wsaaScrate));
			compute(wsaaStraamtc, 8).setRounded(mult(wsaaStraamtc,wsaaScrate));
		}
		if (isEQ(sqlCnpstat,wsaaFp)) {
			wsaaFpuNofpol.add(wsaaStcmthc);
			wsaaFpuNoflif.add(wsaaStlmthc);
			wsaaFpuSum.add(wsaaStimthc);
			wsaaFpuAnnty.add(wsaaStamthc);
			wsaaFpuBprem.add(wsaaStbmthc);
			wsaaFpuBprem.add(wsaaStsmthc);
			wsaaFpuVbonu.add(wsaaVestedBonus);
			wsaaFpuTbonu.add(wsaaTerminalBonus);
		}
		else {
			if (isEQ(sqlCnpstat,wsaaPu)) {
				wsaaRpuNofpol.add(wsaaStcmthc);
				wsaaRpuNoflif.add(wsaaStlmthc);
				wsaaRpuSum.add(wsaaStimthc);
				wsaaRpuAnnty.add(wsaaStamthc);
				wsaaRpuBprem.add(wsaaStbmthc);
				wsaaRpuBprem.add(wsaaStsmthc);
				wsaaRpuVbonu.add(wsaaVestedBonus);
				wsaaRpuTbonu.add(wsaaTerminalBonus);
			}
			else {
				if (isNE(sqlBillfreq,"00")) {
					wsaaRegNofpol.add(wsaaStcmthc);
					wsaaRegNoflif.add(wsaaStlmthc);
					wsaaRegSum.add(wsaaStimthc);
					wsaaRegAnnty.add(wsaaStamthc);
					wsaaRegBprem.add(wsaaStbmthc);
					wsaaRegVbonu.add(wsaaVestedBonus);
					wsaaRegTbonu.add(wsaaTerminalBonus);
				}
				else {
					wsaaSglNofpol.add(wsaaStcmthc);
					wsaaSglNoflif.add(wsaaStlmthc);
					wsaaSglSum.add(wsaaStimthc);
					wsaaSglAnnty.add(wsaaStamthc);
					wsaaSglBprem.add(wsaaStsmthc);
					wsaaSglVbonu.add(wsaaVestedBonus);
					wsaaSglTbonu.add(wsaaTerminalBonus);
				}
			}
		}
		if (isNE(wsaaStraamtc,ZERO)) {
			wsaaRiTotNofpol.add(wsaaStcmthc);
			wsaaRiTotNoflif.add(wsaaStlmthc);
			if (isNE(wsaaStimthc,ZERO)) {
				wsaaRiTotSum.add(wsaaStraamtc);
			}
			else {
				wsaaRiTotAnnty.add(wsaaStraamtc);
			}
			compute(wsaaRatio, 9).set(div(wsaaStraamtc,wsaaStimthc));
			compute(wsaaRiStbamt, 10).setRounded(mult(wsaaRatio,wsaaStbmthc));
			wsaaRiTotBprem.add(wsaaRiStbamt);
		}
		wsaaBlProduct.set(SPACES);
		wsaaTblCrtable.set(sqlCrtable);
		wsaaTblCnttype.set(sqlCnttype);
		h500CheckProduct();
		wsaaFound = "N";
		if (isNE(wsaaStraamtc,ZERO)) {
			for (wsaaX.set(1); !(isGT(wsaaX,3)
			|| isEQ(wsaaFound,"Y")); wsaaX.add(1)){
				if (isEQ(wsaaBlRiCnttype[wsaaX.toInt()],wsaaBlProduct)) {
					wsaaFound = "Y";
					wsaaBlRiNofpol[wsaaX.toInt()].add(wsaaStcmthc);
					wsaaBlRiNoflif[wsaaX.toInt()].add(wsaaStlmthc);
					wsaaBlRiBprem[wsaaX.toInt()].add(wsaaRiStbamt);
					if (isNE(wsaaStimthc,ZERO)) {
						wsaaBlRiSum[wsaaX.toInt()].add(wsaaStraamtc);
					}
					else {
						wsaaBlRiAnnty[wsaaX.toInt()].add(wsaaStraamtc);
					}
				}
			}
		}
		wsaaX.set(0);
		wsaaStsect.set(sqlStsect);
		wsaaStssect.set(sqlStssect);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaCompany.set(sqlChdrcoy);
		wsaaBranch.set(sqlCntbranch);
		wsaaReg.set(sqlRegister);
		wsaaProduct.set(sqlCnttype);
		wsaaCrtable.set(sqlCrtable);
		wsaaStcmthc.set(ZERO);
		wsaaStlmthc.set(ZERO);
		wsaaStimthc.set(ZERO);
		wsaaStamthc.set(ZERO);
		wsaaStbmthc.set(ZERO);
		wsaaStsmthc.set(ZERO);
		wsaaStraamtc.set(ZERO);
		wsaaVestedBonus.set(ZERO);
		wsaaTerminalBonus.set(ZERO);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (selectAll.isTrue()) {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfAllconn, sqlgovepfAllps, sqlgovepfAllrs);
		}
		else {
			appVars.freeDBConnectionIgnoreErr(sqlgovepfconn, sqlgovepfps, sqlgovepfrs);
		}
		rj530.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}

protected void a000InitialProductWs()
	{
		/*A010-START*/
		wsaaRegNofpol.set(0);
		wsaaRegNoflif.set(0);
		wsaaRegSum.set(0);
		wsaaRegAnnty.set(0);
		wsaaRegBprem.set(0);
		wsaaRegVbonu.set(0);
		wsaaRegTbonu.set(0);
		wsaaSglNofpol.set(0);
		wsaaSglNoflif.set(0);
		wsaaSglSum.set(0);
		wsaaSglAnnty.set(0);
		wsaaSglBprem.set(0);
		wsaaSglVbonu.set(0);
		wsaaSglTbonu.set(0);
		wsaaFpuNofpol.set(0);
		wsaaFpuNoflif.set(0);
		wsaaFpuSum.set(0);
		wsaaFpuAnnty.set(0);
		wsaaFpuBprem.set(0);
		wsaaFpuVbonu.set(0);
		wsaaFpuTbonu.set(0);
		wsaaRpuNofpol.set(0);
		wsaaRpuNoflif.set(0);
		wsaaRpuSum.set(0);
		wsaaRpuAnnty.set(0);
		wsaaRpuBprem.set(0);
		wsaaRpuVbonu.set(0);
		wsaaRpuTbonu.set(0);
		wsaaTerminalBonus.set(0);
		wsaaVestedBonus.set(0);
		/*A090-EXIT*/
	}

protected void a100InitialAllWs()
	{
		/*A110-START*/
		wsaaRegNofpol.set(0);
		wsaaRegNoflif.set(0);
		wsaaRegSum.set(0);
		wsaaRegAnnty.set(0);
		wsaaRegBprem.set(0);
		wsaaRegVbonu.set(0);
		wsaaRegTbonu.set(0);
		wsaaRiTotNofpol.set(0);
		wsaaRiTotNoflif.set(0);
		wsaaRiTotSum.set(0);
		wsaaRiTotAnnty.set(0);
		wsaaRiTotBprem.set(0);
		wsaaRiTotVbonu.set(0);
		wsaaRiTotTbonu.set(0);
		wsaaPrdTotNofpol.set(0);
		wsaaPrdTotNoflif.set(0);
		wsaaPrdTotSum.set(0);
		wsaaPrdTotAnnty.set(0);
		wsaaPrdTotBprem.set(0);
		wsaaPrdTotVbonu.set(0);
		wsaaPrdTotTbonu.set(0);
		wsaaSglNofpol.set(0);
		wsaaSglNoflif.set(0);
		wsaaSglSum.set(0);
		wsaaSglAnnty.set(0);
		wsaaSglBprem.set(0);
		wsaaSglVbonu.set(0);
		wsaaSglTbonu.set(0);
		wsaaFpuNofpol.set(0);
		wsaaFpuNoflif.set(0);
		wsaaFpuSum.set(0);
		wsaaFpuAnnty.set(0);
		wsaaFpuBprem.set(0);
		wsaaFpuVbonu.set(0);
		wsaaFpuTbonu.set(0);
		wsaaRpuNofpol.set(0);
		wsaaRpuNoflif.set(0);
		wsaaRpuSum.set(0);
		wsaaRpuAnnty.set(0);
		wsaaRpuBprem.set(0);
		wsaaRpuVbonu.set(0);
		wsaaRpuTbonu.set(0);
		wsaaTerminalBonus.set(0);
		wsaaVestedBonus.set(0);
		for (wsaaX.set(1); !(isGT(wsaaX,3)); wsaaX.add(1)){
			wsaaBlNofpol[wsaaX.toInt()].set(0);
			wsaaBlNoflif[wsaaX.toInt()].set(0);
			wsaaBlSum[wsaaX.toInt()].set(0);
			wsaaBlAnnty[wsaaX.toInt()].set(0);
			wsaaBlBprem[wsaaX.toInt()].set(0);
			wsaaBlVbonu[wsaaX.toInt()].set(0);
			wsaaBlTbonu[wsaaX.toInt()].set(0);
			wsaaBlRiNofpol[wsaaX.toInt()].set(0);
			wsaaBlRiNoflif[wsaaX.toInt()].set(0);
			wsaaBlRiSum[wsaaX.toInt()].set(0);
			wsaaBlRiAnnty[wsaaX.toInt()].set(0);
			wsaaBlRiBprem[wsaaX.toInt()].set(0);
			wsaaBlRiVbonu[wsaaX.toInt()].set(0);
			wsaaBlRiTbonu[wsaaX.toInt()].set(0);
		}
		wsaaX.set(0);
		wsaaBlNofpolT.set(0);
		wsaaBlNoflifT.set(0);
		wsaaBlSumT.set(0);
		wsaaBlAnntyT.set(0);
		wsaaBlBpremT.set(0);
		wsaaBlVbonuT.set(0);
		wsaaBlTbonuT.set(0);
		wsaaStcmthc.set(ZERO);
		wsaaStlmthc.set(ZERO);
		wsaaStimthc.set(ZERO);
		wsaaStamthc.set(ZERO);
		wsaaStbmthc.set(ZERO);
		wsaaStsmthc.set(ZERO);
		wsaaStraamtc.set(ZERO);
		/*A190-EXIT*/
	}

protected void a200GetGvacsts()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a210Start();
				}
				case a220CallIo: {
					a220CallIo();
					a280Nextr();
				}
				case a290Exit: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a210Start()
	{
		gvacstsIO.setParams(SPACES);
		gvacstsIO.setChdrcoy(sqlChdrcoy);
		gvacstsIO.setCntbranch(sqlCntbranch);
		gvacstsIO.setAcctyr(pj517par.acctyr);
		gvacstsIO.setAcctccy(sqlAcctccy);
		gvacstsIO.setRegister(sqlRegister);
		gvacstsIO.setStatSect(sqlStsect);
		gvacstsIO.setStatSubsect(sqlStssect);
		gvacstsIO.setCnttype(sqlCnttype);
		gvacstsIO.setCnPremStat(sqlCnpstat);
		gvacstsIO.setCovPremStat(sqlCrpstat);
		gvacstsIO.setCrtable(sqlCrtable);
		gvacstsIO.setFunction(varcom.begn);
		gvacstsIO.setFormat(gvacstsrec);
	}


protected void a220CallIo()
	{
	//performance improvement --  atiwari23
    gvacstsIO.setSelectStatementType(SmartFileCode.SELECT_SINGLE);



		SmartFileCode.execute(appVars, gvacstsIO);
		if (isNE(gvacstsIO.getStatuz(),varcom.endp)
		&& isNE(gvacstsIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(gvacstsIO.getParams());
			fatalError600();
		}
		if (isEQ(gvacstsIO.getStatuz(),varcom.endp)
		|| isNE(sqlRegister,gvacstsIO.getRegister())
		|| isNE(sqlAcctccy,gvacstsIO.getAcctccy())
		|| isNE(sqlChdrcoy,gvacstsIO.getChdrcoy())
		|| isNE(sqlCntbranch,gvacstsIO.getCntbranch())
		|| isNE(sqlStsect,gvacstsIO.getStatSect())
		|| isNE(sqlStssect,gvacstsIO.getStatSubsect())
		|| isNE(sqlCnttype,gvacstsIO.getCnttype())
		|| isNE(sqlCrtable,gvacstsIO.getCrtable())
		|| isNE(sqlCnpstat,gvacstsIO.getCnPremStat())
		|| isNE(sqlCrpstat,gvacstsIO.getCovPremStat())
		|| isNE(pj517par.acctyr,gvacstsIO.getAcctyr())) {
			goTo(GotoLabel.a290Exit);
		}
		wsaaTerminalBonus.add(gvacstsIO.getBfwdtb());
		compute(wsaaVestedBonus, 2).add(add(gvacstsIO.getBfwdib(),add(gvacstsIO.getBfwdxb(),gvacstsIO.getBfwdrb())));
		for (mth.set(1); !(isGT(mth,pj517par.acctmnth)); mth.add(1)){
			wsaaTerminalBonus.add(gvacstsIO.getStacctb(mth));
			compute(wsaaVestedBonus, 2).add(add(gvacstsIO.getStaccib(mth),add(gvacstsIO.getStaccxb(mth),gvacstsIO.getStaccrb(mth))));
		}
	}

protected void a280Nextr()
	{
		gvacstsIO.setFunction(varcom.nextr);
		goTo(GotoLabel.a220CallIo);
	}

protected void a400ReadT3629()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					a410();
				}
				case a420CallItemio: {
					a420CallItemio();
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a410()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemitem(sqlCntcurr);
		itemIO.setItemtabl(t3629);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
	}

protected void a420CallItemio()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
		t3629rec.t3629Rec.set(itemIO.getGenarea());
		wsaaScrate.set(0);
		wsaaX.set(1);
		while ( !(isNE(wsaaScrate,ZERO)
		|| isGT(wsaaX,7))) {
			if (isGTE(datcon1rec.intDate,t3629rec.frmdate[wsaaX.toInt()])
			&& isLTE(datcon1rec.intDate,t3629rec.todate[wsaaX.toInt()])) {
				wsaaScrate.set(t3629rec.scrate[wsaaX.toInt()]);
			}
			else {
				wsaaX.add(1);
			}
		}

		if (isEQ(wsaaScrate,ZERO)) {
			if (isNE(t3629rec.contitem,SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				goTo(GotoLabel.a420CallItemio);
			}
			else {
				syserrrec.statuz.set(g418);
				fatalError600();
			}
		}
	}

protected void a500ReadT5688()
	{
		a510Start();
	}

protected void a510Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5688);
		descIO.setDescitem(wsaaProduct);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		cntdesc.set(descIO.getLongdesc());
	}

protected void h000GetHeading()
	{
		h020SetUpHeadingCompany();
		h050SetUpHeadingCurrency();
	}

protected void h020SetUpHeadingCompany()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(wsaaCompany);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(wsaaCompany);
		rh01Companynm.set(descIO.getLongdesc());
	}

protected void h050SetUpHeadingCurrency()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(wsaaAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		acctccy.set(wsaaAcctccy);
		currdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3589);
		descIO.setDescitem(wsaaReg);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		register.set(wsaaReg);
		descrip.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5685);
		descIO.setDescitem(wsaaStsect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stsect.set(wsaaStsect);
		itmdesc.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5684);
		descIO.setDescitem(wsaaStssect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stssect.set(wsaaStssect);
		longdesc.set(descIO.getLongdesc());
	}

protected void h100NewPage()
	{
		/*H100-START*/
		h000GetHeading();
		contotrec.totno.set(ct02);
		contotrec.totval.set(1);
		callContot001();
		rj530.printRj530h01(rj530H01, indicArea);
		wsaaOverflow.set("N");
		/*H190-EXIT*/
	}

protected void h100aNewPage()
	{
		/*H100A-START*/
		rj530.printRj530h02(rj530H02, indicArea);
		/*H190A-EXIT*/
	}

protected void h200PrintProductLevel()
	{
		h210Start();
	}

protected void h210Start()
	{
		if (newPageReq.isTrue()) {
			h100NewPage();
			indOn.setTrue(11);
			indOff.setTrue(12);
			h100aNewPage();
		}
		a500ReadT5688();
		rj530.printRj530d01(rj530D01, indicArea);
		wsaaBlProduct.set(SPACES);
		wsaaTblCnttype.set(wsaaProduct);
		wsaaTblCrtable.set(wsaaCrtable);
		h500CheckProduct();
		indOn.setTrue(21);
		stcmthg.set(wsaaRegNofpol);
		stlmth.set(wsaaRegNoflif);
		compute(newtotsi01, 3).setRounded(div(wsaaRegSum,1000));
		compute(newtotsi02, 3).setRounded(div(wsaaRegAnnty,1000));
		compute(newtotsi03, 1).setRounded(div(wsaaRegBprem,1000));
		compute(newtotsi04, 3).setRounded(div(wsaaRegVbonu,1000));
		compute(newtotsi05, 3).setRounded(div(wsaaRegTbonu,1000));
		wsaaRegSum.set(newtotsi01);
		wsaaRegAnnty.set(newtotsi02);
		wsaaRegBprem.set(newtotsi03);
		wsaaRegVbonu.set(newtotsi04);
		wsaaRegTbonu.set(newtotsi05);
		rj530.printRj530d02(rj530D02, indicArea);
		for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		wsaaFound = "N";
		for (wsaaX.set(1); !(isGT(wsaaX,3)
		|| isEQ(wsaaFound,"Y")); wsaaX.add(1)){
			if (isEQ(wsaaBlCnttype[wsaaX.toInt()],wsaaBlProduct)) {
				wsaaFound = "Y";
				wsaaBlNofpol[wsaaX.toInt()].add(wsaaRegNofpol);
				wsaaBlNoflif[wsaaX.toInt()].add(wsaaRegNoflif);
				wsaaBlSum[wsaaX.toInt()].add(wsaaRegSum);
				wsaaBlAnnty[wsaaX.toInt()].add(wsaaRegAnnty);
				wsaaBlBprem[wsaaX.toInt()].add(wsaaRegBprem);
				wsaaBlVbonu[wsaaX.toInt()].add(wsaaRegVbonu);
				wsaaBlTbonu[wsaaX.toInt()].add(wsaaRegTbonu);
			}
		}
		if (newPageReq.isTrue()) {
			h100NewPage();
			indOn.setTrue(11);
			indOff.setTrue(12);
			h100aNewPage();
			rj530.printRj530d03(rj530D03);
		}
		indOn.setTrue(22);
		stcmthg.set(wsaaSglNofpol);
		stlmth.set(wsaaSglNoflif);
		compute(newtotsi01, 3).setRounded(div(wsaaSglSum,1000));
		compute(newtotsi02, 3).setRounded(div(wsaaSglAnnty,1000));
		compute(newtotsi03, 1).setRounded(div(wsaaSglBprem,1000));
		compute(newtotsi04, 3).setRounded(div(wsaaSglVbonu,1000));
		compute(newtotsi05, 3).setRounded(div(wsaaSglTbonu,1000));
		wsaaSglSum.set(newtotsi01);
		wsaaSglAnnty.set(newtotsi02);
		wsaaSglBprem.set(newtotsi03);
		wsaaSglVbonu.set(newtotsi04);
		wsaaSglTbonu.set(newtotsi05);
		rj530.printRj530d02(rj530D02, indicArea);
		for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		wsaaFound = "N";
		for (wsaaX.set(1); !(isGT(wsaaX,3)
		|| isEQ(wsaaFound,"Y")); wsaaX.add(1)){
			if (isEQ(wsaaBlCnttype[wsaaX.toInt()],wsaaBlProduct)) {
				wsaaFound = "Y";
				wsaaBlNofpol[wsaaX.toInt()].add(wsaaSglNofpol);
				wsaaBlNoflif[wsaaX.toInt()].add(wsaaSglNoflif);
				wsaaBlSum[wsaaX.toInt()].add(wsaaSglSum);
				wsaaBlAnnty[wsaaX.toInt()].add(wsaaSglAnnty);
				wsaaBlBprem[wsaaX.toInt()].add(wsaaSglBprem);
				wsaaBlVbonu[wsaaX.toInt()].add(wsaaSglVbonu);
				wsaaBlTbonu[wsaaX.toInt()].add(wsaaSglTbonu);
			}
		}
		if (newPageReq.isTrue()) {
			h100NewPage();
			indOn.setTrue(11);
			indOff.setTrue(12);
			h100aNewPage();
			rj530.printRj530d03(rj530D03);
		}
		indOn.setTrue(23);
		stcmthg.set(wsaaFpuNofpol);
		stlmth.set(wsaaFpuNoflif);
		compute(newtotsi01, 3).setRounded(div(wsaaFpuSum,1000));
		compute(newtotsi02, 3).setRounded(div(wsaaFpuAnnty,1000));
		compute(newtotsi03, 1).setRounded(div(wsaaFpuBprem,1000));
		compute(newtotsi04, 3).setRounded(div(wsaaFpuVbonu,1000));
		compute(newtotsi05, 3).setRounded(div(wsaaFpuTbonu,1000));
		wsaaFpuSum.set(newtotsi01);
		wsaaFpuAnnty.set(newtotsi02);
		wsaaFpuBprem.set(newtotsi03);
		wsaaFpuVbonu.set(newtotsi04);
		wsaaFpuTbonu.set(newtotsi05);
		rj530.printRj530d02(rj530D02, indicArea);
		for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		wsaaFound = "N";
		for (wsaaX.set(1); !(isGT(wsaaX,3)
		|| isEQ(wsaaFound,"Y")); wsaaX.add(1)){
			if (isEQ(wsaaBlCnttype[wsaaX.toInt()],wsaaBlProduct)) {
				wsaaFound = "Y";
				wsaaBlNofpol[wsaaX.toInt()].add(wsaaFpuNofpol);
				wsaaBlNoflif[wsaaX.toInt()].add(wsaaFpuNoflif);
				wsaaBlSum[wsaaX.toInt()].add(wsaaFpuSum);
				wsaaBlAnnty[wsaaX.toInt()].add(wsaaFpuAnnty);
				wsaaBlBprem[wsaaX.toInt()].add(wsaaFpuBprem);
				wsaaBlVbonu[wsaaX.toInt()].add(wsaaFpuVbonu);
				wsaaBlTbonu[wsaaX.toInt()].add(wsaaFpuTbonu);
			}
		}
		if (newPageReq.isTrue()) {
			h100NewPage();
			indOn.setTrue(11);
			indOff.setTrue(12);
			h100aNewPage();
			rj530.printRj530d03(rj530D03);
		}
		indOn.setTrue(24);
		stcmthg.set(wsaaRpuNofpol);
		stlmth.set(wsaaRpuNoflif);
		compute(newtotsi01, 3).setRounded(div(wsaaRpuSum,1000));
		compute(newtotsi02, 3).setRounded(div(wsaaRpuAnnty,1000));
		compute(newtotsi03, 1).setRounded(div(wsaaRpuBprem,1000));
		compute(newtotsi04, 3).setRounded(div(wsaaRpuVbonu,1000));
		compute(newtotsi05, 3).setRounded(div(wsaaRpuTbonu,1000));
		wsaaRpuSum.set(newtotsi01);
		wsaaRpuAnnty.set(newtotsi02);
		wsaaRpuBprem.set(newtotsi03);
		wsaaRpuVbonu.set(newtotsi04);
		wsaaRpuTbonu.set(newtotsi05);
		rj530.printRj530d02(rj530D02, indicArea);
		for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		wsaaFound = "N";
		for (wsaaX.set(1); !(isGT(wsaaX,3)
		|| isEQ(wsaaFound,"Y")); wsaaX.add(1)){
			if (isEQ(wsaaBlCnttype[wsaaX.toInt()],wsaaBlProduct)) {
				wsaaFound = "Y";
				wsaaBlNofpol[wsaaX.toInt()].add(wsaaRpuNofpol);
				wsaaBlNoflif[wsaaX.toInt()].add(wsaaRpuNoflif);
				wsaaBlSum[wsaaX.toInt()].add(wsaaRpuSum);
				wsaaBlAnnty[wsaaX.toInt()].add(wsaaRpuAnnty);
				wsaaBlBprem[wsaaX.toInt()].add(wsaaRpuBprem);
				wsaaBlVbonu[wsaaX.toInt()].add(wsaaRpuVbonu);
				wsaaBlTbonu[wsaaX.toInt()].add(wsaaRpuTbonu);
			}
		}
		compute(wsaaPrdTotNofpol, 2).set(add(add(add(add(wsaaPrdTotNofpol,wsaaRegNofpol),wsaaSglNofpol),wsaaFpuNofpol),wsaaRpuNofpol));
		compute(wsaaPrdTotNoflif, 2).set(add(add(add(add(wsaaPrdTotNoflif,wsaaRegNoflif),wsaaSglNoflif),wsaaFpuNoflif),wsaaRpuNoflif));
		compute(wsaaPrdTotSum, 2).set(add(add(add(add(wsaaPrdTotSum,wsaaRegSum),wsaaSglSum),wsaaFpuSum),wsaaRpuSum));
		compute(wsaaPrdTotAnnty, 2).set(add(add(add(add(wsaaPrdTotAnnty,wsaaRegAnnty),wsaaSglAnnty),wsaaFpuAnnty),wsaaRpuAnnty));
		compute(wsaaPrdTotBprem, 0).set(add(add(add(add(wsaaPrdTotBprem,wsaaRegBprem),wsaaSglBprem),wsaaFpuBprem),wsaaRpuBprem));
		compute(wsaaPrdTotVbonu, 2).set(add(add(add(add(wsaaPrdTotVbonu,wsaaRegVbonu),wsaaSglVbonu),wsaaFpuVbonu),wsaaRpuVbonu));
		compute(wsaaPrdTotTbonu, 2).set(add(add(add(add(wsaaPrdTotTbonu,wsaaRegTbonu),wsaaSglTbonu),wsaaFpuTbonu),wsaaRpuTbonu));
	}

protected void h300PrintTotal()
	{
		h310Start();
	}

protected void h310Start()
	{
		if (newPageReq.isTrue()) {
			h100NewPage();
			indOn.setTrue(11);
			indOff.setTrue(12);
			h100aNewPage();
		}
		indOn.setTrue(13);
		stcmthg.set(wsaaPrdTotNofpol);
		stlmth.set(wsaaPrdTotNoflif);
		newtotsi01.set(wsaaPrdTotSum);
		newtotsi02.set(wsaaPrdTotAnnty);
		newtotsi03.set(wsaaPrdTotBprem);
		newtotsi04.set(wsaaPrdTotVbonu);
		newtotsi05.set(wsaaPrdTotTbonu);
		rj530.printRj530d02(rj530D02, indicArea);
		for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		if (newPageReq.isTrue()) {
			h100NewPage();
			indOn.setTrue(11);
			indOff.setTrue(12);
			h100aNewPage();
		}
		indOn.setTrue(14);
		stcmthg.set(wsaaRiTotNofpol);
		stlmth.set(wsaaRiTotNoflif);
		compute(newtotsi01, 3).setRounded(div(wsaaRiTotSum,1000));
		compute(newtotsi02, 3).setRounded(div(wsaaRiTotAnnty,1000));
		compute(newtotsi03, 1).setRounded(div(wsaaRiTotBprem,1000));
		compute(newtotsi04, 3).setRounded(div(wsaaRiTotVbonu,1000));
		compute(newtotsi05, 3).setRounded(div(wsaaRiTotTbonu,1000));
		wsaaRiTotSum.set(newtotsi01);
		wsaaRiTotAnnty.set(newtotsi02);
		wsaaRiTotBprem.set(newtotsi03);
		wsaaRiTotVbonu.set(newtotsi04);
		wsaaRiTotTbonu.set(newtotsi05);
		rj530.printRj530d02(rj530D02, indicArea);
		for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		if (newPageReq.isTrue()) {
			h100NewPage();
			indOn.setTrue(11);
			indOff.setTrue(12);
			h100aNewPage();
		}
		indOn.setTrue(15);
		compute(stcmthg, 2).set(sub(wsaaPrdTotNofpol,wsaaRiTotNofpol));
		compute(stlmth, 2).set(sub(wsaaPrdTotNoflif,wsaaRiTotNoflif));
		compute(newtotsi01, 3).setRounded(sub(wsaaPrdTotSum,wsaaRiTotSum));
		compute(newtotsi02, 3).setRounded(sub(wsaaPrdTotAnnty,wsaaRiTotAnnty));
		compute(newtotsi03, 1).setRounded(sub(wsaaPrdTotBprem,wsaaRiTotBprem));
		compute(newtotsi04, 3).setRounded(sub(wsaaPrdTotVbonu,wsaaRiTotVbonu));
		compute(newtotsi05, 3).setRounded(sub(wsaaPrdTotTbonu,wsaaRiTotTbonu));
		rj530.printRj530d02(rj530D02, indicArea);
		for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaPrdTotNofpol);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaPrdTotNoflif);
		callContot001();
		contotrec.totno.set(ct05);
		contotrec.totval.set(wsaaPrdTotSum);
		callContot001();
		contotrec.totno.set(ct06);
		contotrec.totval.set(wsaaPrdTotAnnty);
		callContot001();
		contotrec.totno.set(ct07);
		contotrec.totval.set(wsaaPrdTotBprem);
		callContot001();
		contotrec.totno.set(ct08);
		contotrec.totval.set(wsaaPrdTotVbonu);
		callContot001();
		contotrec.totno.set(ct09);
		contotrec.totval.set(wsaaPrdTotTbonu);
		callContot001();
	}

protected void h400PrintBusinessLevel()
	{
		h410Start();
	}

protected void h410Start()
	{
		h100NewPage();
		indOn.setTrue(12);
		indOff.setTrue(11);
		h100aNewPage();
		indOn.setTrue(16);
		indOff.setTrue(17);
		rj530.printRj530d01(rj530D01, indicArea);
		for (wsaaX.set(1); !(isGT(wsaaX,3)); wsaaX.add(1)){
			if (isEQ(wsaaBlCnttype[wsaaX.toInt()],"1")) {
				indOn.setTrue(18);
			}
			else {
				if (isEQ(wsaaBlCnttype[wsaaX.toInt()],"2")) {
					indOn.setTrue(19);
				}
				else {
					indOn.setTrue(20);
				}
			}
			stcmthg.set(wsaaBlNofpol[wsaaX.toInt()]);
			stlmth.set(wsaaBlNoflif[wsaaX.toInt()]);
			newtotsi01.set(wsaaBlSum[wsaaX.toInt()]);
			newtotsi02.set(wsaaBlAnnty[wsaaX.toInt()]);
			newtotsi03.set(wsaaBlBprem[wsaaX.toInt()]);
			newtotsi04.set(wsaaBlVbonu[wsaaX.toInt()]);
			newtotsi05.set(wsaaBlTbonu[wsaaX.toInt()]);
			rj530.printRj530d02(rj530D02, indicArea);
			for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
				indOff.setTrue(wsaaSub);
			}
			wsaaBlNofpolT.add(wsaaBlNofpol[wsaaX.toInt()]);
			wsaaBlNoflifT.add(wsaaBlNoflif[wsaaX.toInt()]);
			wsaaBlSumT.add(wsaaBlSum[wsaaX.toInt()]);
			wsaaBlAnntyT.add(wsaaBlAnnty[wsaaX.toInt()]);
			wsaaBlBpremT.add(wsaaBlBprem[wsaaX.toInt()]);
			wsaaBlVbonuT.add(wsaaBlVbonu[wsaaX.toInt()]);
			wsaaBlTbonuT.add(wsaaBlTbonu[wsaaX.toInt()]);
		}
		indOn.setTrue(13);
		stcmthg.set(wsaaBlNofpolT);
		stlmth.set(wsaaBlNoflifT);
		newtotsi01.set(wsaaBlSumT);
		newtotsi02.set(wsaaBlAnntyT);
		newtotsi03.set(wsaaBlBpremT);
		newtotsi04.set(wsaaBlVbonuT);
		newtotsi05.set(wsaaBlTbonuT);
		rj530.printRj530d02(rj530D02, indicArea);
		for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		wsaaBlNofpolT.set(0);
		wsaaBlNoflifT.set(0);
		wsaaBlSumT.set(0);
		wsaaBlAnntyT.set(0);
		wsaaBlBpremT.set(0);
		wsaaBlVbonuT.set(0);
		wsaaBlTbonuT.set(0);
		indOn.setTrue(17);
		indOff.setTrue(16);
		rj530.printRj530d01(rj530D01, indicArea);
		for (wsaaX.set(1); !(isGT(wsaaX,3)); wsaaX.add(1)){
			compute(wsaaBlRiSum[wsaaX.toInt()], 3).setRounded(div((wsaaBlRiSum[wsaaX.toInt()]),1000));
			compute(wsaaBlRiAnnty[wsaaX.toInt()], 3).setRounded(div((wsaaBlRiAnnty[wsaaX.toInt()]),1000));
			compute(wsaaBlRiBprem[wsaaX.toInt()], 1).setRounded(div((wsaaBlRiBprem[wsaaX.toInt()]),1000));
			compute(wsaaBlRiVbonu[wsaaX.toInt()], 3).setRounded(div((wsaaBlRiVbonu[wsaaX.toInt()]),1000));
			compute(wsaaBlRiTbonu[wsaaX.toInt()], 3).setRounded(div((wsaaBlRiTbonu[wsaaX.toInt()]),1000));
		}
		for (wsaaX.set(1); !(isGT(wsaaX,3)); wsaaX.add(1)){
			if (isEQ(wsaaBlCnttype[wsaaX.toInt()],"1")) {
				indOn.setTrue(18);
			}
			else {
				if (isEQ(wsaaBlCnttype[wsaaX.toInt()],"2")) {
					indOn.setTrue(19);
				}
				else {
					indOn.setTrue(20);
				}
			}
			compute(stcmthg, 2).set(sub(wsaaBlNofpol[wsaaX.toInt()],wsaaBlRiNofpol[wsaaX.toInt()]));
			compute(stlmth, 2).set(sub(wsaaBlNoflif[wsaaX.toInt()],wsaaBlRiNoflif[wsaaX.toInt()]));
			compute(newtotsi01, 3).setRounded(sub(wsaaBlSum[wsaaX.toInt()],wsaaBlRiSum[wsaaX.toInt()]));
			compute(newtotsi02, 3).setRounded(sub(wsaaBlAnnty[wsaaX.toInt()],wsaaBlRiAnnty[wsaaX.toInt()]));
			compute(newtotsi03, 1).setRounded(sub(wsaaBlBprem[wsaaX.toInt()],wsaaBlRiBprem[wsaaX.toInt()]));
			compute(newtotsi04, 2).set(sub(wsaaBlVbonu[wsaaX.toInt()],wsaaBlRiVbonu[wsaaX.toInt()]));
			compute(newtotsi05, 2).set(sub(wsaaBlTbonu[wsaaX.toInt()],wsaaBlRiTbonu[wsaaX.toInt()]));
			compute(wsaaBlNofpolT, 2).set(sub(add(wsaaBlNofpolT,wsaaBlNofpol[wsaaX.toInt()]),wsaaBlRiNofpol[wsaaX.toInt()]));
			compute(wsaaBlNoflifT, 2).set(sub(add(wsaaBlNoflifT,wsaaBlNoflif[wsaaX.toInt()]),wsaaBlRiNoflif[wsaaX.toInt()]));
			compute(wsaaBlSumT, 2).set(sub(add(wsaaBlSumT,wsaaBlSum[wsaaX.toInt()]),wsaaBlRiSum[wsaaX.toInt()]));
			compute(wsaaBlAnntyT, 2).set(sub(add(wsaaBlAnntyT,wsaaBlAnnty[wsaaX.toInt()]),wsaaBlRiAnnty[wsaaX.toInt()]));
			compute(wsaaBlBpremT, 0).set(sub(add(wsaaBlBpremT,wsaaBlBprem[wsaaX.toInt()]),wsaaBlRiBprem[wsaaX.toInt()]));
			compute(wsaaBlVbonuT, 2).set(sub(add(wsaaBlVbonuT,wsaaBlVbonu[wsaaX.toInt()]),wsaaBlRiVbonu[wsaaX.toInt()]));
			compute(wsaaBlTbonuT, 2).set(sub(add(wsaaBlTbonuT,wsaaBlTbonu[wsaaX.toInt()]),wsaaBlRiTbonu[wsaaX.toInt()]));
			rj530.printRj530d02(rj530D02, indicArea);
			for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
				indOff.setTrue(wsaaSub);
			}
		}
		indOn.setTrue(13);
		stcmthg.set(wsaaBlNofpolT);
		stlmth.set(wsaaBlNoflifT);
		newtotsi01.set(wsaaBlSumT);
		newtotsi02.set(wsaaBlAnntyT);
		newtotsi03.set(wsaaBlBpremT);
		newtotsi04.set(wsaaBlVbonuT);
		newtotsi05.set(wsaaBlTbonuT);
		rj530.printRj530d02(rj530D02, indicArea);
		for (wsaaSub.set(1); !(isGT(wsaaSub,24)); wsaaSub.add(1)){
			indOff.setTrue(wsaaSub);
		}
		wsaaOverflow.set("Y");
	}

protected void h500CheckProduct()
	{
		try {
			h510Start();
		}
		catch (GOTOException e){
		}
	}

protected void h510Start()
	{
		h600ReadT6697();
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			wsaaBlProduct.set("3");
			goTo(GotoLabel.h590Exit);
		}
		h700ReadT6625();
		if (isEQ(itemIO.getStatuz(),varcom.oK)) {
			wsaaBlProduct.set("2");
			goTo(GotoLabel.h590Exit);
		}
		wsaaBlProduct.set("1");
	}

protected void h600ReadT6697()
	{
		a610Start();
	}

protected void a610Start()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6697);
		itemIO.setItemseq(SPACES);
		itemIO.setItemitem(wsaaTblCnttype);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void h700ReadT6625()
	{
		h710Start();
	}

protected void h710Start()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t6625);
		itemIO.setItemseq(SPACES);
		itemIO.setItemitem(wsaaTblCrtable);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(),varcom.oK)
		&& isNE(itemIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(itemIO.getParams());
			fatalError600();
		}
	}

protected void callDbcs6000()
	{
		/*CALL-DBCS-SUB*/
		dbcstrcpy2.dbcsStatuz.set(SPACES);
		dbcsTrnc2(dbcstrcpy2.rec);
		if (isNE(dbcstrcpy2.dbcsStatuz,varcom.oK)) {
			syserrrec.statuz.set(dbcstrcpy2.dbcsStatuz);
			fatalError600();
		}
		/*EXIT*/
	}
}
