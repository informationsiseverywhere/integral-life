package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6703
 * @version 1.0 generated on 30/08/09 06:58
 * @author Quipoz
 */
public class S6703ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(301);
	public FixedLengthStringData dataFields = new FixedLengthStringData(45).isAPartOf(dataArea, 0);
	public ZonedDecimalData acctyr = DD.acctyr.copyToZonedDecimal().isAPartOf(dataFields,0);
	public ZonedDecimalData acmn = DD.acmn.copyToZonedDecimal().isAPartOf(dataFields,4);
	public FixedLengthStringData agntsel = DD.agntsel.copy().isAPartOf(dataFields,6);
	public FixedLengthStringData aracde = DD.aracde.copy().isAPartOf(dataFields,16);
	public FixedLengthStringData bandage = DD.bandage.copy().isAPartOf(dataFields,19);
	public FixedLengthStringData bandprm = DD.bandprm.copy().isAPartOf(dataFields,21);
	public FixedLengthStringData bandsa = DD.bandsa.copy().isAPartOf(dataFields,23);
	public FixedLengthStringData bandtrm = DD.bandtrm.copy().isAPartOf(dataFields,25);
	public FixedLengthStringData chdrtype = DD.chdrtype.copy().isAPartOf(dataFields,27);
	public FixedLengthStringData cntbranch = DD.cntbranch.copy().isAPartOf(dataFields,30);
	public FixedLengthStringData cntcurr = DD.cntcurr.copy().isAPartOf(dataFields,32);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,35);
	public FixedLengthStringData crtable = DD.crtable.copy().isAPartOf(dataFields,36);
	public FixedLengthStringData ovrdcat = DD.ovrdcat.copy().isAPartOf(dataFields,40);
	public FixedLengthStringData pstatcd = DD.pstatcd.copy().isAPartOf(dataFields,41);
	public FixedLengthStringData statcat = DD.statcat.copy().isAPartOf(dataFields,43);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(64).isAPartOf(dataArea, 45);
	public FixedLengthStringData acctyrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData acmnErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agntselErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData aracdeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData bandageErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData bandprmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData bandsaErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData bandtrmErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData chdrtypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData cntbranchErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData cntcurrErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData crtableErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData ovrdcatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData pstatcdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData statcatErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(192).isAPartOf(dataArea, 109);
	public FixedLengthStringData[] acctyrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] acmnOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agntselOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] aracdeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] bandageOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] bandprmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] bandsaOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] bandtrmOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] chdrtypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] cntbranchOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] cntcurrOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] crtableOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] ovrdcatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] pstatcdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] statcatOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6703screenWritten = new LongData(0);
	public LongData S6703protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6703ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		fieldIndMap.put(agntselOut,new String[] {"01",null, "-01",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(statcatOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acctyrOut,new String[] {"03",null, "-03",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(aracdeOut,new String[] {"04",null, "-04",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntbranchOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bandageOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bandsaOut,new String[] {"07",null, "-07",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bandprmOut,new String[] {"08",null, "-08",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(bandtrmOut,new String[] {"09",null, "-09",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(chdrtypeOut,new String[] {"10",null, "-10",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(crtableOut,new String[] {"11",null, "-11",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(cntcurrOut,new String[] {"13",null, "-13",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(acmnOut,new String[] {"16",null, "-16",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(pstatcdOut,new String[] {"15",null, "-15",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(ovrdcatOut,new String[] {"14",null, "-14",null, null, null, null, null, null, null, null, null});
		screenFields = new BaseData[] {company, agntsel, statcat, acctyr, aracde, cntbranch, bandage, bandsa, bandprm, bandtrm, chdrtype, crtable, cntcurr, acmn, pstatcd, ovrdcat};
		screenOutFields = new BaseData[][] {companyOut, agntselOut, statcatOut, acctyrOut, aracdeOut, cntbranchOut, bandageOut, bandsaOut, bandprmOut, bandtrmOut, chdrtypeOut, crtableOut, cntcurrOut, acmnOut, pstatcdOut, ovrdcatOut};
		screenErrFields = new BaseData[] {companyErr, agntselErr, statcatErr, acctyrErr, aracdeErr, cntbranchErr, bandageErr, bandsaErr, bandprmErr, bandtrmErr, chdrtypeErr, crtableErr, cntcurrErr, acmnErr, pstatcdErr, ovrdcatErr};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6703screen.class;
		protectRecord = S6703protect.class;
	}

}
