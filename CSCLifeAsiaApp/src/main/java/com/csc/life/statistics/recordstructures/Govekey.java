package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:14
 * Description:
 * Copybook name: GOVEKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Govekey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData goveFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData goveKey = new FixedLengthStringData(64).isAPartOf(goveFileKey, 0, REDEFINE);
  	public FixedLengthStringData goveChdrcoy = new FixedLengthStringData(1).isAPartOf(goveKey, 0);
  	public FixedLengthStringData goveCntbranch = new FixedLengthStringData(2).isAPartOf(goveKey, 1);
  	public FixedLengthStringData goveRegister = new FixedLengthStringData(3).isAPartOf(goveKey, 3);
  	public FixedLengthStringData goveSrcebus = new FixedLengthStringData(2).isAPartOf(goveKey, 6);
  	public FixedLengthStringData goveStatcat = new FixedLengthStringData(2).isAPartOf(goveKey, 8);
  	public FixedLengthStringData goveStatFund = new FixedLengthStringData(1).isAPartOf(goveKey, 10);
  	public FixedLengthStringData goveStatSect = new FixedLengthStringData(2).isAPartOf(goveKey, 11);
  	public FixedLengthStringData goveStatSubsect = new FixedLengthStringData(4).isAPartOf(goveKey, 13);
  	public FixedLengthStringData goveCnPremStat = new FixedLengthStringData(2).isAPartOf(goveKey, 17);
  	public FixedLengthStringData goveCovPremStat = new FixedLengthStringData(2).isAPartOf(goveKey, 19);
  	public FixedLengthStringData goveAcctccy = new FixedLengthStringData(3).isAPartOf(goveKey, 21);
  	public FixedLengthStringData goveCntcurr = new FixedLengthStringData(3).isAPartOf(goveKey, 24);
  	public FixedLengthStringData goveCnttype = new FixedLengthStringData(3).isAPartOf(goveKey, 27);
  	public FixedLengthStringData goveCrtable = new FixedLengthStringData(4).isAPartOf(goveKey, 30);
  	public FixedLengthStringData goveParind = new FixedLengthStringData(1).isAPartOf(goveKey, 34);
  	public FixedLengthStringData goveBillfreq = new FixedLengthStringData(2).isAPartOf(goveKey, 35);
  	public PackedDecimalData goveAcctyr = new PackedDecimalData(4, 0).isAPartOf(goveKey, 37);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(goveKey, 40, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(goveFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		goveFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}