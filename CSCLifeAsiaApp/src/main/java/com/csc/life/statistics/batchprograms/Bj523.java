/*
 * File: Bj523.java
 * Date: 29 August 2009 21:41:32
 * Author: Quipoz Limited
 *
 * Class transformed from BJ523.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.sql.SQLException;

import com.csc.life.statistics.recordstructures.Pj517par;
import com.csc.life.statistics.reports.Rj523Report;
import com.csc.life.statistics.tablestructures.Tj675rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                  Commission Details
*                  ==================
*
*   This report will list all commissions paid during the
*   specified accounting year and month, segregating them into
*   first year commission, renewal commission and single premium
*   commission. It will be reported at coverage level. An
*   accumulated figure will be printed for each source of business
*
*   Control totals:
*     01  -  Number of pages printed
*
*****************************************************************
* </pre>
*/
public class Bj523 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private int sqlcode = 0;
	private boolean sqlerrorflag;
	private SQLException sqlca = new SQLException();
	private java.sql.ResultSet sqlgvacpfAllrs = null;
	private java.sql.PreparedStatement sqlgvacpfAllps = null;
	private java.sql.Connection sqlgvacpfAllconn = null;
	private String sqlgvacpfAll = "";
	private java.sql.ResultSet sqlgvacpfrs = null;
	private java.sql.PreparedStatement sqlgvacpfps = null;
	private java.sql.Connection sqlgvacpfconn = null;
	private String sqlgvacpf = "";
	private Rj523Report printerFile = new Rj523Report();
	private FixedLengthStringData printerRec = new FixedLengthStringData(250);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BJ523");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaItem = new FixedLengthStringData(6);
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private PackedDecimalData wsaaTotal01 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTotal02 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTotal03 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt01 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt02 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaTamt03 = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaccfyc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaccrlc = new PackedDecimalData(18, 2);
	private PackedDecimalData wsaaStaccspc = new PackedDecimalData(18, 2);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaParind = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaBranch = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaAcctccy = new FixedLengthStringData(3);
	private FixedLengthStringData wsaaRegister = new FixedLengthStringData(3);
	private String wsaaFrecord = "";
	private String wsaaFlag = "";
	private ZonedDecimalData wsaaAcctyr = new ZonedDecimalData(4, 0);

	private FixedLengthStringData wsaaSelectAll = new FixedLengthStringData(1);
	private Validator selectAll = new Validator(wsaaSelectAll, "Y");
	private FixedLengthStringData wsaaTj675Stsect01 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect02 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect03 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect04 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect05 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect06 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect07 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect08 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect09 = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaTj675Stsect10 = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler2 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler2, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler2, 8);

		/* SQL-GVACPF */
	private FixedLengthStringData sqlGvacrec = new FixedLengthStringData(389);
	private FixedLengthStringData sqlChdrcoy = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 0);
	private PackedDecimalData sqlAcctyr = new PackedDecimalData(4, 0).isAPartOf(sqlGvacrec, 1);
	private FixedLengthStringData sqlStfund = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 4);
	private FixedLengthStringData sqlStsect = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 5);
	private FixedLengthStringData sqlStssect = new FixedLengthStringData(4).isAPartOf(sqlGvacrec, 7);
	private FixedLengthStringData sqlRegister = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 11);
	private FixedLengthStringData sqlCntbranch = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 14);
	private FixedLengthStringData sqlCnttype = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 16);
	private FixedLengthStringData sqlCrtable = new FixedLengthStringData(4).isAPartOf(sqlGvacrec, 19);
	private FixedLengthStringData sqlAcctccy = new FixedLengthStringData(3).isAPartOf(sqlGvacrec, 23);
	private FixedLengthStringData sqlParind = new FixedLengthStringData(1).isAPartOf(sqlGvacrec, 26);
	private FixedLengthStringData sqlSrcebus = new FixedLengthStringData(2).isAPartOf(sqlGvacrec, 27);
	private PackedDecimalData sqlStaccfyc01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 29);
	private PackedDecimalData sqlStaccfyc02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 39);
	private PackedDecimalData sqlStaccfyc03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 49);
	private PackedDecimalData sqlStaccfyc04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 59);
	private PackedDecimalData sqlStaccfyc05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 69);
	private PackedDecimalData sqlStaccfyc06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 79);
	private PackedDecimalData sqlStaccfyc07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 89);
	private PackedDecimalData sqlStaccfyc08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 99);
	private PackedDecimalData sqlStaccfyc09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 109);
	private PackedDecimalData sqlStaccfyc10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 119);
	private PackedDecimalData sqlStaccfyc11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 129);
	private PackedDecimalData sqlStaccfyc12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 139);
	private PackedDecimalData sqlStaccrlc01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 149);
	private PackedDecimalData sqlStaccrlc02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 159);
	private PackedDecimalData sqlStaccrlc03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 169);
	private PackedDecimalData sqlStaccrlc04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 179);
	private PackedDecimalData sqlStaccrlc05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 189);
	private PackedDecimalData sqlStaccrlc06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 199);
	private PackedDecimalData sqlStaccrlc07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 209);
	private PackedDecimalData sqlStaccrlc08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 219);
	private PackedDecimalData sqlStaccrlc09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 229);
	private PackedDecimalData sqlStaccrlc10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 239);
	private PackedDecimalData sqlStaccrlc11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 249);
	private PackedDecimalData sqlStaccrlc12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 259);
	private PackedDecimalData sqlStaccspc01 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 269);
	private PackedDecimalData sqlStaccspc02 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 279);
	private PackedDecimalData sqlStaccspc03 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 289);
	private PackedDecimalData sqlStaccspc04 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 299);
	private PackedDecimalData sqlStaccspc05 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 309);
	private PackedDecimalData sqlStaccspc06 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 319);
	private PackedDecimalData sqlStaccspc07 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 329);
	private PackedDecimalData sqlStaccspc08 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 339);
	private PackedDecimalData sqlStaccspc09 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 349);
	private PackedDecimalData sqlStaccspc10 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 359);
	private PackedDecimalData sqlStaccspc11 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 369);
	private PackedDecimalData sqlStaccspc12 = new PackedDecimalData(18, 2).isAPartOf(sqlGvacrec, 379);

	private FixedLengthStringData wsaaSqlKeep = new FixedLengthStringData(389);
	private FixedLengthStringData wsaaSChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 0);
	private PackedDecimalData wsaaSAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaSqlKeep, 1);
	private FixedLengthStringData wsaaSStfund = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 4);
	private FixedLengthStringData wsaaSStsect = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 5);
	private FixedLengthStringData wsaaSStssect = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 7);
	private FixedLengthStringData wsaaSRegister = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 11);
	private FixedLengthStringData wsaaSCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 14);
	private FixedLengthStringData wsaaSCnttype = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 16);
	private FixedLengthStringData wsaaSCrtable = new FixedLengthStringData(4).isAPartOf(wsaaSqlKeep, 19);
	private FixedLengthStringData wsaaSAcctccy = new FixedLengthStringData(3).isAPartOf(wsaaSqlKeep, 23);
	private FixedLengthStringData wsaaSParind = new FixedLengthStringData(1).isAPartOf(wsaaSqlKeep, 26);
	private FixedLengthStringData wsaaSSrcebus = new FixedLengthStringData(2).isAPartOf(wsaaSqlKeep, 27);
	private PackedDecimalData[] wsaaSStaccfyc = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 29);
	private PackedDecimalData[] wsaaSStaccrlc = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 149);
	private PackedDecimalData[] wsaaSStaccspc = PDArrayPartOfStructure(12, 18, 2, wsaaSqlKeep, 269);
	private String descrec = "DESCREC";
	private String itemrec = "ITEMREC";
		/* ERRORS */
	private String esql = "ESQL";
		/* TABLES */
	private String t1693 = "T1693";
	private String t3629 = "T3629";
	private String t3589 = "T3589";
	private String t5685 = "T5685";
	private String t5687 = "T5687";
	private String tj675 = "TJ675";
		/* CONTROL-TOTALS */
	private int ct01 = 1;
	private int ct02 = 2;
	private int ct03 = 3;
	private int ct04 = 4;

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1).init("Y");
	private Validator newPageReq = new Validator(wsaaOverflow, "Y");

	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator endOfFile = new Validator(wsaaEof, "Y");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rj523H01 = new FixedLengthStringData(145);
	private FixedLengthStringData rj523h01O = new FixedLengthStringData(145).isAPartOf(rj523H01, 0);
	private ZonedDecimalData acctmonth = new ZonedDecimalData(2, 0).isAPartOf(rj523h01O, 0);
	private ZonedDecimalData acctyear = new ZonedDecimalData(4, 0).isAPartOf(rj523h01O, 2);
	private FixedLengthStringData rh01Company = new FixedLengthStringData(1).isAPartOf(rj523h01O, 6);
	private FixedLengthStringData rh01Companynm = new FixedLengthStringData(30).isAPartOf(rj523h01O, 7);
	private FixedLengthStringData rh01Sdate = new FixedLengthStringData(10).isAPartOf(rj523h01O, 37);
	private FixedLengthStringData acctccy = new FixedLengthStringData(3).isAPartOf(rj523h01O, 47);
	private FixedLengthStringData currdesc = new FixedLengthStringData(30).isAPartOf(rj523h01O, 50);
	private FixedLengthStringData register = new FixedLengthStringData(3).isAPartOf(rj523h01O, 80);
	private FixedLengthStringData descrip = new FixedLengthStringData(30).isAPartOf(rj523h01O, 83);
	private FixedLengthStringData stsect = new FixedLengthStringData(2).isAPartOf(rj523h01O, 113);
	private FixedLengthStringData itmdesc = new FixedLengthStringData(30).isAPartOf(rj523h01O, 115);

	private FixedLengthStringData rj523H02 = new FixedLengthStringData(50);
	private FixedLengthStringData rj523h02O = new FixedLengthStringData(50).isAPartOf(rj523H02, 0);
	private FixedLengthStringData adsc = new FixedLengthStringData(50).isAPartOf(rj523h02O, 0);

	private FixedLengthStringData rj523H03 = new FixedLengthStringData(2);

	private FixedLengthStringData rj523D01 = new FixedLengthStringData(88);
	private FixedLengthStringData rj523d01O = new FixedLengthStringData(88).isAPartOf(rj523D01, 0);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(rj523d01O, 0);
	private FixedLengthStringData crtabled = new FixedLengthStringData(30).isAPartOf(rj523d01O, 4);
	private ZonedDecimalData staccfyc = new ZonedDecimalData(18, 2).isAPartOf(rj523d01O, 34);
	private ZonedDecimalData staccrlc = new ZonedDecimalData(18, 2).isAPartOf(rj523d01O, 52);
	private ZonedDecimalData staccspc = new ZonedDecimalData(18, 2).isAPartOf(rj523d01O, 70);

	private FixedLengthStringData rj523D02 = new FixedLengthStringData(2);

	private FixedLengthStringData rj523T01 = new FixedLengthStringData(54);
	private FixedLengthStringData rj523t01O = new FixedLengthStringData(54).isAPartOf(rj523T01, 0);
	private ZonedDecimalData total01 = new ZonedDecimalData(18, 2).isAPartOf(rj523t01O, 0);
	private ZonedDecimalData total02 = new ZonedDecimalData(18, 2).isAPartOf(rj523t01O, 18);
	private ZonedDecimalData total03 = new ZonedDecimalData(18, 2).isAPartOf(rj523t01O, 36);

	private FixedLengthStringData rj523T02 = new FixedLengthStringData(54);
	private FixedLengthStringData rj523t02O = new FixedLengthStringData(54).isAPartOf(rj523T02, 0);
	private ZonedDecimalData totalamt01 = new ZonedDecimalData(18, 2).isAPartOf(rj523t02O, 0);
	private ZonedDecimalData totalamt02 = new ZonedDecimalData(18, 2).isAPartOf(rj523t02O, 18);
	private ZonedDecimalData totalamt03 = new ZonedDecimalData(18, 2).isAPartOf(rj523t02O, 36);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private Datcon1rec datcon1rec = new Datcon1rec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Logical File: SMART table reference data*/
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Pj517par pj517par = new Pj517par();
	private Tj675rec tj675rec = new Tj675rec();

	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		eof2080,
		exit2090
	}

	public Bj523() {
		super();
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}

public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		setUpHeadingDates1040();
	}

protected void initialise1010()
	{
		printerFile.openOutput();
		pj517par.parmRecord.set(bupaIO.getParmarea());
		wsspEdterror.set(varcom.oK);
		wsaaAcctyr.set(pj517par.acctyr);
		acctyear.set(pj517par.acctyr);
		acctmonth.set(pj517par.acctmnth);
	}

protected void setUpHeadingDates1040()
	{
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		rh01Sdate.set(datcon1rec.extDate);
		wsaaStaccfyc.set(ZERO);
		wsaaStaccrlc.set(ZERO);
		wsaaStaccspc.set(ZERO);
		wsaaTotal01.set(ZERO);
		wsaaTotal02.set(ZERO);
		wsaaTotal03.set(ZERO);
		wsaaTamt01.set(ZERO);
		wsaaTamt02.set(ZERO);
		wsaaTamt03.set(ZERO);
		wsaaStsect.set(SPACES);
		wsaaParind.set(SPACES);
		wsaaCrtable.set(SPACES);
		wsaaCompany.set(SPACES);
		wsaaBranch.set(SPACES);
		wsaaAcctccy.set(SPACES);
		wsaaRegister.set(SPACES);
		wsaaFrecord = "Y";
		wsaaFlag = "N";
		readTj6751300();
		/*EXIT*/
	}

protected void readTj6751300()
	{
		para1310();
	}

protected void para1310()
	{
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(tj675);
		itemIO.setItemitem(wsaaItem);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		wsaaSelectAll.set("Y");
		if (isEQ(itemIO.getStatuz(),varcom.mrnf)) {
			selectAll1400();
		}
		else {
			wsaaSelectAll.set("N");
			tj675rec.tj675Rec.set(itemIO.getGenarea());
			selectSpecified1500();
		}
	}

protected void selectAll1400()
	{
		/*PARA*/
	//ILIFE-1149
		//sqlgvacpfAll = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REGISTER, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STACCFYC01, STACCFYC02, STACCFYC03, STACCFYC04, STACCFYC05, STACCFYC06, STACCFYC07, STACCFYC08, STACCFYC09, STACCFYC10, STACCFYC11, STACCFYC12, STACCRLC01, STACCRLC02, STACCRLC03, STACCRLC04, STACCRLC05, STACCRLC06, STACCRLC07, STACCRLC08, STACCRLC09, STACCRLC10, STACCRLC11, STACCRLC12, STACCSPC01, STACCSPC02, STACCSPC03, STACCSPC04, STACCSPC05, STACCSPC06, STACCSPC07, STACCSPC08, STACCSPC09, STACCSPC10, STACCSPC11, STACCSPC12" +
	sqlgvacpfAll = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STACCFYC01, STACCFYC02, STACCFYC03, STACCFYC04, STACCFYC05, STACCFYC06, STACCFYC07, STACCFYC08, STACCFYC09, STACCFYC10, STACCFYC11, STACCFYC12, STACCRLC01, STACCRLC02, STACCRLC03, STACCRLC04, STACCRLC05, STACCRLC06, STACCRLC07, STACCRLC08, STACCRLC09, STACCRLC10, STACCRLC11, STACCRLC12, STACCSPC01, STACCSPC02, STACCSPC03, STACCSPC04, STACCSPC05, STACCSPC06, STACCSPC07, STACCSPC08, STACCSPC09, STACCSPC10, STACCSPC11, STACCSPC12" +
" FROM   " + appVars.getTableNameOverriden("GVACPF") + " " +
" WHERE ACCTYR = ?" +
//ILIFE-1149
//" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REGISTER, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REG, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
		sqlerrorflag = false;
		try {
			sqlgvacpfAllconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvacpfTableDAM());
			sqlgvacpfAllps = appVars.prepareStatementEmbeded(sqlgvacpfAllconn, sqlgvacpfAll, "GVACPF");
			appVars.setDBDouble(sqlgvacpfAllps, 1, wsaaAcctyr.toDouble());
			sqlgvacpfAllrs = appVars.executeQuery(sqlgvacpfAllps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
		/*EXIT*/
	}

protected void selectSpecified1500()
	{
		para1510();
	}

protected void para1510()
	{
		wsaaTj675Stsect01.set(tj675rec.statSect[1]);
		wsaaTj675Stsect02.set(tj675rec.statSect[2]);
		wsaaTj675Stsect03.set(tj675rec.statSect[3]);
		wsaaTj675Stsect04.set(tj675rec.statSect[4]);
		wsaaTj675Stsect05.set(tj675rec.statSect[5]);
		wsaaTj675Stsect06.set(tj675rec.statSect[6]);
		wsaaTj675Stsect07.set(tj675rec.statSect[7]);
		wsaaTj675Stsect08.set(tj675rec.statSect[8]);
		wsaaTj675Stsect09.set(tj675rec.statSect[9]);
		wsaaTj675Stsect10.set(tj675rec.statSect[10]);
		//ILIFE-1149
		//sqlgvacpf = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REGISTER, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STACCFYC01, STACCFYC02, STACCFYC03, STACCFYC04, STACCFYC05, STACCFYC06, STACCFYC07, STACCFYC08, STACCFYC09, STACCFYC10, STACCFYC11, STACCFYC12, STACCRLC01, STACCRLC02, STACCRLC03, STACCRLC04, STACCRLC05, STACCRLC06, STACCRLC07, STACCRLC08, STACCRLC09, STACCRLC10, STACCRLC11, STACCRLC12, STACCSPC01, STACCSPC02, STACCSPC03, STACCSPC04, STACCSPC05, STACCSPC06, STACCSPC07, STACCSPC08, STACCSPC09, STACCSPC10, STACCSPC11, STACCSPC12" +
		sqlgvacpf = " SELECT  CHDRCOY, ACCTYR, STFUND, STSECT, STSSECT, REG, CNTBRANCH, CNTTYPE, CRTABLE, ACCTCCY, PARIND, SRCEBUS, STACCFYC01, STACCFYC02, STACCFYC03, STACCFYC04, STACCFYC05, STACCFYC06, STACCFYC07, STACCFYC08, STACCFYC09, STACCFYC10, STACCFYC11, STACCFYC12, STACCRLC01, STACCRLC02, STACCRLC03, STACCRLC04, STACCRLC05, STACCRLC06, STACCRLC07, STACCRLC08, STACCRLC09, STACCRLC10, STACCRLC11, STACCRLC12, STACCSPC01, STACCSPC02, STACCSPC03, STACCSPC04, STACCSPC05, STACCSPC06, STACCSPC07, STACCSPC08, STACCSPC09, STACCSPC10, STACCSPC11, STACCSPC12" +
" FROM   " + appVars.getTableNameOverriden("GVACPF") + " " +
" WHERE ACCTYR = ?" +
" AND (STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?" +
" OR STSECT = ?)" +
//ILIFE-1149
//" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REGISTER, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
" ORDER BY CHDRCOY, CNTBRANCH, ACCTCCY, REG, STSECT, PARIND DESC, CNTTYPE, CRTABLE";
		sqlerrorflag = false;
		try {
			sqlgvacpfconn = ((com.csc.smart400framework.SMARTAppVars)appVars).getDBConnectionForTable(new com.csc.life.statistics.dataaccess.GvacpfTableDAM());
			sqlgvacpfps = appVars.prepareStatementEmbeded(sqlgvacpfconn, sqlgvacpf, "GVACPF");
			appVars.setDBDouble(sqlgvacpfps, 1, wsaaAcctyr.toDouble());
			appVars.setDBString(sqlgvacpfps, 2, wsaaTj675Stsect01.toString());
			appVars.setDBString(sqlgvacpfps, 3, wsaaTj675Stsect02.toString());
			appVars.setDBString(sqlgvacpfps, 4, wsaaTj675Stsect03.toString());
			appVars.setDBString(sqlgvacpfps, 5, wsaaTj675Stsect04.toString());
			appVars.setDBString(sqlgvacpfps, 6, wsaaTj675Stsect05.toString());
			appVars.setDBString(sqlgvacpfps, 7, wsaaTj675Stsect06.toString());
			appVars.setDBString(sqlgvacpfps, 8, wsaaTj675Stsect07.toString());
			appVars.setDBString(sqlgvacpfps, 9, wsaaTj675Stsect08.toString());
			appVars.setDBString(sqlgvacpfps, 10, wsaaTj675Stsect09.toString());
			appVars.setDBString(sqlgvacpfps, 11, wsaaTj675Stsect10.toString());
			sqlgvacpfrs = appVars.executeQuery(sqlgvacpfps);
		}
		catch (SQLException ex){
			sqlca = ex;
			sqlerrorflag = true;
		}
	}

protected void readFile2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: {
					readFile2010();
				}
				case eof2080: {
					eof2080();
				}
				case exit2090: {
				}
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readFile2010()
	{
		if (selectAll.isTrue()) {
			sqlerrorflag = false;
			try {
				if (sqlgvacpfAllrs.next()) {
					appVars.getDBObject(sqlgvacpfAllrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgvacpfAllrs, 2, sqlAcctyr);
					appVars.getDBObject(sqlgvacpfAllrs, 3, sqlStfund);
					appVars.getDBObject(sqlgvacpfAllrs, 4, sqlStsect);
					appVars.getDBObject(sqlgvacpfAllrs, 5, sqlStssect);
					appVars.getDBObject(sqlgvacpfAllrs, 6, sqlRegister);
					appVars.getDBObject(sqlgvacpfAllrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgvacpfAllrs, 8, sqlCnttype);
					appVars.getDBObject(sqlgvacpfAllrs, 9, sqlCrtable);
					appVars.getDBObject(sqlgvacpfAllrs, 10, sqlAcctccy);
					appVars.getDBObject(sqlgvacpfAllrs, 11, sqlParind);
					appVars.getDBObject(sqlgvacpfAllrs, 12, sqlSrcebus);
					appVars.getDBObject(sqlgvacpfAllrs, 13, sqlStaccfyc01);
					appVars.getDBObject(sqlgvacpfAllrs, 14, sqlStaccfyc02);
					appVars.getDBObject(sqlgvacpfAllrs, 15, sqlStaccfyc03);
					appVars.getDBObject(sqlgvacpfAllrs, 16, sqlStaccfyc04);
					appVars.getDBObject(sqlgvacpfAllrs, 17, sqlStaccfyc05);
					appVars.getDBObject(sqlgvacpfAllrs, 18, sqlStaccfyc06);
					appVars.getDBObject(sqlgvacpfAllrs, 19, sqlStaccfyc07);
					appVars.getDBObject(sqlgvacpfAllrs, 20, sqlStaccfyc08);
					appVars.getDBObject(sqlgvacpfAllrs, 21, sqlStaccfyc09);
					appVars.getDBObject(sqlgvacpfAllrs, 22, sqlStaccfyc10);
					appVars.getDBObject(sqlgvacpfAllrs, 23, sqlStaccfyc11);
					appVars.getDBObject(sqlgvacpfAllrs, 24, sqlStaccfyc12);
					appVars.getDBObject(sqlgvacpfAllrs, 25, sqlStaccrlc01);
					appVars.getDBObject(sqlgvacpfAllrs, 26, sqlStaccrlc02);
					appVars.getDBObject(sqlgvacpfAllrs, 27, sqlStaccrlc03);
					appVars.getDBObject(sqlgvacpfAllrs, 28, sqlStaccrlc04);
					appVars.getDBObject(sqlgvacpfAllrs, 29, sqlStaccrlc05);
					appVars.getDBObject(sqlgvacpfAllrs, 30, sqlStaccrlc06);
					appVars.getDBObject(sqlgvacpfAllrs, 31, sqlStaccrlc07);
					appVars.getDBObject(sqlgvacpfAllrs, 32, sqlStaccrlc08);
					appVars.getDBObject(sqlgvacpfAllrs, 33, sqlStaccrlc09);
					appVars.getDBObject(sqlgvacpfAllrs, 34, sqlStaccrlc10);
					appVars.getDBObject(sqlgvacpfAllrs, 35, sqlStaccrlc11);
					appVars.getDBObject(sqlgvacpfAllrs, 36, sqlStaccrlc12);
					appVars.getDBObject(sqlgvacpfAllrs, 37, sqlStaccspc01);
					appVars.getDBObject(sqlgvacpfAllrs, 38, sqlStaccspc02);
					appVars.getDBObject(sqlgvacpfAllrs, 39, sqlStaccspc03);
					appVars.getDBObject(sqlgvacpfAllrs, 40, sqlStaccspc04);
					appVars.getDBObject(sqlgvacpfAllrs, 41, sqlStaccspc05);
					appVars.getDBObject(sqlgvacpfAllrs, 42, sqlStaccspc06);
					appVars.getDBObject(sqlgvacpfAllrs, 43, sqlStaccspc07);
					appVars.getDBObject(sqlgvacpfAllrs, 44, sqlStaccspc08);
					appVars.getDBObject(sqlgvacpfAllrs, 45, sqlStaccspc09);
					appVars.getDBObject(sqlgvacpfAllrs, 46, sqlStaccspc10);
					appVars.getDBObject(sqlgvacpfAllrs, 47, sqlStaccspc11);
					appVars.getDBObject(sqlgvacpfAllrs, 48, sqlStaccspc12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
		}
		else {
			sqlerrorflag = false;
			try {
				if (sqlgvacpfrs.next()) {
					appVars.getDBObject(sqlgvacpfrs, 1, sqlChdrcoy);
					appVars.getDBObject(sqlgvacpfrs, 2, sqlAcctyr);
					appVars.getDBObject(sqlgvacpfrs, 3, sqlStfund);
					appVars.getDBObject(sqlgvacpfrs, 4, sqlStsect);
					appVars.getDBObject(sqlgvacpfrs, 5, sqlStssect);
					appVars.getDBObject(sqlgvacpfrs, 6, sqlRegister);
					appVars.getDBObject(sqlgvacpfrs, 7, sqlCntbranch);
					appVars.getDBObject(sqlgvacpfrs, 8, sqlCnttype);
					appVars.getDBObject(sqlgvacpfrs, 9, sqlCrtable);
					appVars.getDBObject(sqlgvacpfrs, 10, sqlAcctccy);
					appVars.getDBObject(sqlgvacpfrs, 11, sqlParind);
					appVars.getDBObject(sqlgvacpfrs, 12, sqlSrcebus);
					appVars.getDBObject(sqlgvacpfrs, 13, sqlStaccfyc01);
					appVars.getDBObject(sqlgvacpfrs, 14, sqlStaccfyc02);
					appVars.getDBObject(sqlgvacpfrs, 15, sqlStaccfyc03);
					appVars.getDBObject(sqlgvacpfrs, 16, sqlStaccfyc04);
					appVars.getDBObject(sqlgvacpfrs, 17, sqlStaccfyc05);
					appVars.getDBObject(sqlgvacpfrs, 18, sqlStaccfyc06);
					appVars.getDBObject(sqlgvacpfrs, 19, sqlStaccfyc07);
					appVars.getDBObject(sqlgvacpfrs, 20, sqlStaccfyc08);
					appVars.getDBObject(sqlgvacpfrs, 21, sqlStaccfyc09);
					appVars.getDBObject(sqlgvacpfrs, 22, sqlStaccfyc10);
					appVars.getDBObject(sqlgvacpfrs, 23, sqlStaccfyc11);
					appVars.getDBObject(sqlgvacpfrs, 24, sqlStaccfyc12);
					appVars.getDBObject(sqlgvacpfrs, 25, sqlStaccrlc01);
					appVars.getDBObject(sqlgvacpfrs, 26, sqlStaccrlc02);
					appVars.getDBObject(sqlgvacpfrs, 27, sqlStaccrlc03);
					appVars.getDBObject(sqlgvacpfrs, 28, sqlStaccrlc04);
					appVars.getDBObject(sqlgvacpfrs, 29, sqlStaccrlc05);
					appVars.getDBObject(sqlgvacpfrs, 30, sqlStaccrlc06);
					appVars.getDBObject(sqlgvacpfrs, 31, sqlStaccrlc07);
					appVars.getDBObject(sqlgvacpfrs, 32, sqlStaccrlc08);
					appVars.getDBObject(sqlgvacpfrs, 33, sqlStaccrlc09);
					appVars.getDBObject(sqlgvacpfrs, 34, sqlStaccrlc10);
					appVars.getDBObject(sqlgvacpfrs, 35, sqlStaccrlc11);
					appVars.getDBObject(sqlgvacpfrs, 36, sqlStaccrlc12);
					appVars.getDBObject(sqlgvacpfrs, 37, sqlStaccspc01);
					appVars.getDBObject(sqlgvacpfrs, 38, sqlStaccspc02);
					appVars.getDBObject(sqlgvacpfrs, 39, sqlStaccspc03);
					appVars.getDBObject(sqlgvacpfrs, 40, sqlStaccspc04);
					appVars.getDBObject(sqlgvacpfrs, 41, sqlStaccspc05);
					appVars.getDBObject(sqlgvacpfrs, 42, sqlStaccspc06);
					appVars.getDBObject(sqlgvacpfrs, 43, sqlStaccspc07);
					appVars.getDBObject(sqlgvacpfrs, 44, sqlStaccspc08);
					appVars.getDBObject(sqlgvacpfrs, 45, sqlStaccspc09);
					appVars.getDBObject(sqlgvacpfrs, 46, sqlStaccspc10);
					appVars.getDBObject(sqlgvacpfrs, 47, sqlStaccspc11);
					appVars.getDBObject(sqlgvacpfrs, 48, sqlStaccspc12);
				}
				else {
					goTo(GotoLabel.eof2080);
				}
			}
			catch (SQLException ex){
				sqlca = ex;
				sqlerrorflag = true;
			}
		}
		wsaaSqlKeep.set(sqlGvacrec);
		h600SetHeading();
		if (newPageReq.isTrue()) {
			h100NewPage();
		}
		if (isNE(wsaaFrecord,"Y")) {
			if (isNE(wsaaStsect,sqlStsect)
			|| isNE(wsaaRegister,sqlRegister)
			|| isNE(wsaaAcctccy,sqlAcctccy)
			|| isNE(wsaaCompany,sqlChdrcoy)
			|| isNE(wsaaBranch,sqlCntbranch)) {
				wsaaOverflow.set("Y");
				wsaaFlag = "Y";
				h200NewHeading();
			}
			else {
				if (isNE(wsaaParind,sqlParind)) {
					h200NewHeading();
				}
			}
		}
		if (isEQ(wsaaFrecord,"Y")) {
			if (isEQ(sqlParind,"P")) {
				indOn.setTrue(11);
				indOff.setTrue(12);
			}
			else {
				indOn.setTrue(12);
				indOff.setTrue(11);
			}
			printerFile.printRj523h02(rj523H02, indicArea);
			printerFile.printRj523h03(rj523H03, indicArea);
		}
		goTo(GotoLabel.exit2090);
	}

protected void eof2080()
	{
		if (isNE(wsaaTamt01,ZERO)
		|| isNE(wsaaTamt02,ZERO)
		|| isNE(wsaaTamt03,ZERO)) {
			h500WriteEnd();
		}
		wsspEdterror.set(varcom.endp);
	}

protected void edit2500()
	{
		/*EDIT*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		writeDetail3080();
	}

protected void writeDetail3080()
	{
		if (isEQ(wsaaFrecord,"Y")) {
			wsaaFrecord = "N";
		}
		else {
			if (isNE(wsaaCrtable,sqlCrtable)) {
				staccfyc.set(wsaaStaccfyc);
				staccrlc.set(wsaaStaccrlc);
				staccspc.set(wsaaStaccspc);
				printerFile.printRj523d01(rj523D01, indicArea);
				wsaaStaccfyc.set(ZERO);
				wsaaStaccrlc.set(ZERO);
				wsaaStaccspc.set(ZERO);
			}
		}
		h400ReadT5687();
		crtabled.set(descIO.getLongdesc());
		crtable.set(sqlCrtable);
		wsaaStaccfyc.add(wsaaSStaccfyc[pj517par.acctmnth.toInt()]);
		wsaaStaccrlc.add(wsaaSStaccrlc[pj517par.acctmnth.toInt()]);
		wsaaStaccspc.add(wsaaSStaccspc[pj517par.acctmnth.toInt()]);
		wsaaTotal01.add(wsaaSStaccfyc[pj517par.acctmnth.toInt()]);
		wsaaTamt01.add(wsaaSStaccfyc[pj517par.acctmnth.toInt()]);
		wsaaTotal02.add(wsaaSStaccrlc[pj517par.acctmnth.toInt()]);
		wsaaTamt02.add(wsaaSStaccrlc[pj517par.acctmnth.toInt()]);
		wsaaTotal03.add(wsaaSStaccspc[pj517par.acctmnth.toInt()]);
		wsaaTamt03.add(wsaaSStaccspc[pj517par.acctmnth.toInt()]);
		wsaaStsect.set(sqlStsect);
		wsaaParind.set(sqlParind);
		wsaaAcctccy.set(sqlAcctccy);
		wsaaCompany.set(sqlChdrcoy);
		wsaaBranch.set(sqlCntbranch);
		wsaaRegister.set(sqlRegister);
		wsaaCrtable.set(sqlCrtable);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		if (selectAll.isTrue()) {
			appVars.freeDBConnectionIgnoreErr(sqlgvacpfAllconn, sqlgvacpfAllps, sqlgvacpfAllrs);
		}
		else {
			appVars.freeDBConnectionIgnoreErr(sqlgvacpfconn, sqlgvacpfps, sqlgvacpfrs);
		}
		printerFile.close();
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void h100NewPage()
	{
		h110Start();
	}

protected void h110Start()
	{
		printerFile.printRj523h01(rj523H01, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		if (isEQ(wsaaFrecord,"N")) {
			if (isEQ(sqlParind,"P")) {
				indOn.setTrue(11);
				indOff.setTrue(12);
			}
			else {
				indOn.setTrue(12);
				indOff.setTrue(11);
			}
			printerFile.printRj523h02(rj523H02, indicArea);
			printerFile.printRj523h03(rj523H03, indicArea);
		}
	}

protected void h200NewHeading()
	{
		h210Start();
	}

protected void h210Start()
	{
		staccfyc.set(wsaaStaccfyc);
		staccrlc.set(wsaaStaccrlc);
		staccspc.set(wsaaStaccspc);
		printerFile.printRj523d01(rj523D01, indicArea);
		wsaaStaccfyc.set(ZERO);
		wsaaStaccrlc.set(ZERO);
		wsaaStaccspc.set(ZERO);
		wsaaCrtable.set(sqlCrtable);
		total01.set(wsaaTotal01);
		total02.set(wsaaTotal02);
		total03.set(wsaaTotal03);
		printerFile.printRj523t01(rj523T01, indicArea);
		if (isEQ(wsaaFlag,"Y")) {
			wsaaFlag = "N";
			totalamt01.set(wsaaTamt01);
			totalamt02.set(wsaaTamt02);
			totalamt03.set(wsaaTamt03);
			printerFile.printRj523t02(rj523T02, indicArea);
			contotrec.totno.set(ct02);
			contotrec.totval.set(wsaaTamt01);
			callContot001();
			contotrec.totno.set(ct03);
			contotrec.totval.set(wsaaTamt02);
			callContot001();
			contotrec.totno.set(ct04);
			contotrec.totval.set(wsaaTamt03);
			callContot001();
			wsaaTamt01.set(ZERO);
			wsaaTamt02.set(ZERO);
			wsaaTamt03.set(ZERO);
		}
		if (newPageReq.isTrue()) {
			h100NewPage();
		}
		else {
			if (isEQ(sqlParind,"P")) {
				indOn.setTrue(11);
				indOff.setTrue(12);
			}
			else {
				indOn.setTrue(12);
				indOff.setTrue(11);
			}
			printerFile.printRj523h02(rj523H02, indicArea);
			printerFile.printRj523d02(rj523D02, indicArea);
		}
		wsaaTotal01.set(ZERO);
		wsaaTotal02.set(ZERO);
		wsaaTotal03.set(ZERO);
	}

protected void h400ReadT5687()
	{
		h410Start();
	}

protected void h410Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5687);
		descIO.setDescitem(sqlCrtable);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("?????");
		}
	}

protected void h500WriteEnd()
	{
		h510Start();
	}

protected void h510Start()
	{
		staccfyc.set(wsaaStaccfyc);
		staccrlc.set(wsaaStaccrlc);
		staccspc.set(wsaaStaccspc);
		printerFile.printRj523d01(rj523D01, indicArea);
		total01.set(wsaaTotal01);
		total02.set(wsaaTotal02);
		total03.set(wsaaTotal03);
		printerFile.printRj523t01(rj523T01, indicArea);
		totalamt01.set(wsaaTamt01);
		totalamt02.set(wsaaTamt02);
		totalamt03.set(wsaaTamt03);
		printerFile.printRj523t02(rj523T02, indicArea);
		wsaaOverflow.set("N");
		contotrec.totno.set(ct02);
		contotrec.totval.set(wsaaTamt01);
		callContot001();
		contotrec.totno.set(ct03);
		contotrec.totval.set(wsaaTamt02);
		callContot001();
		contotrec.totno.set(ct04);
		contotrec.totval.set(wsaaTamt03);
		callContot001();
	}

protected void h600SetHeading()
	{
		h610Start();
	}

protected void h610Start()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(sqlChdrcoy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		rh01Company.set(sqlChdrcoy);
		rh01Companynm.set(descIO.getLongdesc());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(sqlAcctccy);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		acctccy.set(sqlAcctccy);
		currdesc.set(descIO.getLongdesc());
//ILIFE-6360 starts		
		if(isNE(sqlRegister,SPACES)){
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t3589);
		descIO.setDescitem(sqlRegister);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		register.set(sqlRegister);
		descrip.set(descIO.getLongdesc());
		}
		
		else{
			
			register.set(SPACES);
			descrip.set(SPACES);
		}//ILIFE-6360 ends
		
		descIO.setDescpfx("IT");
		descIO.setDesccoy(bsprIO.getCompany());
		descIO.setDesctabl(t5685);
		descIO.setDescitem(sqlStsect);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(bsscIO.getLanguage());
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)
		&& isNE(descIO.getStatuz(),varcom.mrnf)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		if (isEQ(descIO.getStatuz(),varcom.mrnf)) {
			descIO.setLongdesc("????");
		}
		stsect.set(sqlStsect);
		itmdesc.set(descIO.getLongdesc());
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(sqlca.getErrorCode());
		StringBuilder stringVariable1 = new StringBuilder();
		stringVariable1.append(sqlSign.toString());
		stringVariable1.append(sqlStatuz.toString());
		wsaaSqlcode.setLeft(stringVariable1.toString());
		wsaaSqlmessage.set(sqlca.getMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
