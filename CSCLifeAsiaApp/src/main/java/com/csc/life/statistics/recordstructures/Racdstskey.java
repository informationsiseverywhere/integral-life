package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:09:23
 * Description:
 * Copybook name: RACDSTSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Racdstskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData racdstsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData racdstsKey = new FixedLengthStringData(64).isAPartOf(racdstsFileKey, 0, REDEFINE);
  	public FixedLengthStringData racdstsChdrcoy = new FixedLengthStringData(1).isAPartOf(racdstsKey, 0);
  	public FixedLengthStringData racdstsChdrnum = new FixedLengthStringData(8).isAPartOf(racdstsKey, 1);
  	public FixedLengthStringData racdstsLife = new FixedLengthStringData(2).isAPartOf(racdstsKey, 9);
  	public FixedLengthStringData racdstsCoverage = new FixedLengthStringData(2).isAPartOf(racdstsKey, 11);
  	public FixedLengthStringData racdstsRider = new FixedLengthStringData(2).isAPartOf(racdstsKey, 13);
  	public PackedDecimalData racdstsPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(racdstsKey, 15);
  	public FixedLengthStringData racdstsRasnum = new FixedLengthStringData(8).isAPartOf(racdstsKey, 18);
  	public PackedDecimalData racdstsTranno = new PackedDecimalData(5, 0).isAPartOf(racdstsKey, 26);
  	public FixedLengthStringData filler = new FixedLengthStringData(35).isAPartOf(racdstsKey, 29, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(racdstsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		racdstsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}