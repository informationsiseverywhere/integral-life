package com.csc.life.statistics.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GvacTableDAM.java
 * Date: Sun, 30 Aug 2009 03:39:08
 * Class transformed from GVAC.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GvacTableDAM extends GvacpfTableDAM {

	public GvacTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("GVAC");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CNTBRANCH"
		             + ", REG"
		             + ", SRCEBUS"
		             + ", STFUND"
		             + ", STSECT"
		             + ", STSSECT"
		             + ", STATCODE"
		             + ", CNPSTAT"
		             + ", CRPSTAT"
		             + ", ACCTCCY"
		             + ", CNTTYPE"
		             + ", CRTABLE"
		             + ", PARIND"
		             + ", ACCTYR";
		
		QUALIFIEDCOLUMNS = 
		            "REG, " +
		            "CHDRCOY, " +
		            "CNTBRANCH, " +
		            "SRCEBUS, " +
		            "STFUND, " +
		            "STSECT, " +
		            "STSSECT, " +
		            "ACCTCCY, " +
		            "ACCTYR, " +
		            "CNTTYPE, " +
		            "CRTABLE, " +
		            "STATCODE, " +
		            "CNPSTAT, " +
		            "CRPSTAT, " +
		            "PARIND, " +
		            "BFWDFYP, " +
		            "STACCFYP01, " +
		            "STACCFYP02, " +
		            "STACCFYP03, " +
		            "STACCFYP04, " +
		            "STACCFYP05, " +
		            "STACCFYP06, " +
		            "STACCFYP07, " +
		            "STACCFYP08, " +
		            "STACCFYP09, " +
		            "STACCFYP10, " +
		            "STACCFYP11, " +
		            "STACCFYP12, " +
		            "CFWDFYP, " +
		            "BFWDRNP, " +
		            "STACCRNP01, " +
		            "STACCRNP02, " +
		            "STACCRNP03, " +
		            "STACCRNP04, " +
		            "STACCRNP05, " +
		            "STACCRNP06, " +
		            "STACCRNP07, " +
		            "STACCRNP08, " +
		            "STACCRNP09, " +
		            "STACCRNP10, " +
		            "STACCRNP11, " +
		            "STACCRNP12, " +
		            "CFWDRNP, " +
		            "BFWDSPD, " +
		            "STACCSPD01, " +
		            "STACCSPD02, " +
		            "STACCSPD03, " +
		            "STACCSPD04, " +
		            "STACCSPD05, " +
		            "STACCSPD06, " +
		            "STACCSPD07, " +
		            "STACCSPD08, " +
		            "STACCSPD09, " +
		            "STACCSPD10, " +
		            "STACCSPD11, " +
		            "STACCSPD12, " +
		            "CFWDSPD, " +
		            "BFWDFYC, " +
		            "STACCFYC01, " +
		            "STACCFYC02, " +
		            "STACCFYC03, " +
		            "STACCFYC04, " +
		            "STACCFYC05, " +
		            "STACCFYC06, " +
		            "STACCFYC07, " +
		            "STACCFYC08, " +
		            "STACCFYC09, " +
		            "STACCFYC10, " +
		            "STACCFYC11, " +
		            "STACCFYC12, " +
		            "CFWDFYC, " +
		            "BFWDRLC, " +
		            "STACCRLC01, " +
		            "STACCRLC02, " +
		            "STACCRLC03, " +
		            "STACCRLC04, " +
		            "STACCRLC05, " +
		            "STACCRLC06, " +
		            "STACCRLC07, " +
		            "STACCRLC08, " +
		            "STACCRLC09, " +
		            "STACCRLC10, " +
		            "STACCRLC11, " +
		            "STACCRLC12, " +
		            "CFWDRLC, " +
		            "BFWDSPC, " +
		            "STACCSPC01, " +
		            "STACCSPC02, " +
		            "STACCSPC03, " +
		            "STACCSPC04, " +
		            "STACCSPC05, " +
		            "STACCSPC06, " +
		            "STACCSPC07, " +
		            "STACCSPC08, " +
		            "STACCSPC09, " +
		            "STACCSPC10, " +
		            "STACCSPC11, " +
		            "STACCSPC12, " +
		            "CFWDSPC, " +
		            "BFWDRB, " +
		            "STACCRB01, " +
		            "STACCRB02, " +
		            "STACCRB03, " +
		            "STACCRB04, " +
		            "STACCRB05, " +
		            "STACCRB06, " +
		            "STACCRB07, " +
		            "STACCRB08, " +
		            "STACCRB09, " +
		            "STACCRB10, " +
		            "STACCRB11, " +
		            "STACCRB12, " +
		            "CFWDRB, " +
		            "BFWDXB, " +
		            "STACCXB01, " +
		            "STACCXB02, " +
		            "STACCXB03, " +
		            "STACCXB04, " +
		            "STACCXB05, " +
		            "STACCXB06, " +
		            "STACCXB07, " +
		            "STACCXB08, " +
		            "STACCXB09, " +
		            "STACCXB10, " +
		            "STACCXB11, " +
		            "STACCXB12, " +
		            "CFWDXB, " +
		            "BFWDTB, " +
		            "STACCTB01, " +
		            "STACCTB02, " +
		            "STACCTB03, " +
		            "STACCTB04, " +
		            "STACCTB05, " +
		            "STACCTB06, " +
		            "STACCTB07, " +
		            "STACCTB08, " +
		            "STACCTB09, " +
		            "STACCTB10, " +
		            "STACCTB11, " +
		            "STACCTB12, " +
		            "CFWDTB, " +
		            "BFWDIB, " +
		            "STACCIB01, " +
		            "STACCIB02, " +
		            "STACCIB03, " +
		            "STACCIB04, " +
		            "STACCIB05, " +
		            "STACCIB06, " +
		            "STACCIB07, " +
		            "STACCIB08, " +
		            "STACCIB09, " +
		            "STACCIB10, " +
		            "STACCIB11, " +
		            "STACCIB12, " +
		            "CFWDIB, " +
		            "BFWDDD, " +
		            "STACCDD01, " +
		            "STACCDD02, " +
		            "STACCDD03, " +
		            "STACCDD04, " +
		            "STACCDD05, " +
		            "STACCDD06, " +
		            "STACCDD07, " +
		            "STACCDD08, " +
		            "STACCDD09, " +
		            "STACCDD10, " +
		            "STACCDD11, " +
		            "STACCDD12, " +
		            "CFWDDD, " +
		            "BFWDDI, " +
		            "STACCDI01, " +
		            "STACCDI02, " +
		            "STACCDI03, " +
		            "STACCDI04, " +
		            "STACCDI05, " +
		            "STACCDI06, " +
		            "STACCDI07, " +
		            "STACCDI08, " +
		            "STACCDI09, " +
		            "STACCDI10, " +
		            "STACCDI11, " +
		            "STACCDI12, " +
		            "CFWDDI, " +
		            "BFWDBS, " +
		            "STACCBS01, " +
		            "STACCBS02, " +
		            "STACCBS03, " +
		            "STACCBS04, " +
		            "STACCBS05, " +
		            "STACCBS06, " +
		            "STACCBS07, " +
		            "STACCBS08, " +
		            "STACCBS09, " +
		            "STACCBS10, " +
		            "STACCBS11, " +
		            "STACCBS12, " +
		            "CFWDBS, " +
		            "BFWDOB, " +
		            "STACCOB01, " +
		            "STACCOB02, " +
		            "STACCOB03, " +
		            "STACCOB04, " +
		            "STACCOB05, " +
		            "STACCOB06, " +
		            "STACCOB07, " +
		            "STACCOB08, " +
		            "STACCOB09, " +
		            "STACCOB10, " +
		            "STACCOB11, " +
		            "STACCOB12, " +
		            "CFWDOB, " +
		            "BFWDMTB, " +
		            "STACCMTB01, " +
		            "STACCMTB02, " +
		            "STACCMTB03, " +
		            "STACCMTB04, " +
		            "STACCMTB05, " +
		            "STACCMTB06, " +
		            "STACCMTB07, " +
		            "STACCMTB08, " +
		            "STACCMTB09, " +
		            "STACCMTB10, " +
		            "STACCMTB11, " +
		            "STACCMTB12, " +
		            "CFWDMTB, " +
		            "BFWDCLR, " +
		            "STACCCLR01, " +
		            "STACCCLR02, " +
		            "STACCCLR03, " +
		            "STACCCLR04, " +
		            "STACCCLR05, " +
		            "STACCCLR06, " +
		            "STACCCLR07, " +
		            "STACCCLR08, " +
		            "STACCCLR09, " +
		            "STACCCLR10, " +
		            "STACCCLR11, " +
		            "STACCCLR12, " +
		            "CFWDCLR, " +
		            "BFWDRIP, " +
		            "STACCRIP01, " +
		            "STACCRIP02, " +
		            "STACCRIP03, " +
		            "STACCRIP04, " +
		            "STACCRIP05, " +
		            "STACCRIP06, " +
		            "STACCRIP07, " +
		            "STACCRIP08, " +
		            "STACCRIP09, " +
		            "STACCRIP10, " +
		            "STACCRIP11, " +
		            "STACCRIP12, " +
		            "CFWDRIP, " +
		            "BFWDRIC, " +
		            "STACCRIC01, " +
		            "STACCRIC02, " +
		            "STACCRIC03, " +
		            "STACCRIC04, " +
		            "STACCRIC05, " +
		            "STACCRIC06, " +
		            "STACCRIC07, " +
		            "STACCRIC08, " +
		            "STACCRIC09, " +
		            "STACCRIC10, " +
		            "STACCRIC11, " +
		            "STACCRIC12, " +
		            "CFWDRIC, " +
		            "BFWDADV, " +
		            "STACCADV01, " +
		            "STACCADV02, " +
		            "STACCADV03, " +
		            "STACCADV04, " +
		            "STACCADV05, " +
		            "STACCADV06, " +
		            "STACCADV07, " +
		            "STACCADV08, " +
		            "STACCADV09, " +
		            "STACCADV10, " +
		            "STACCADV11, " +
		            "STACCADV12, " +
		            "CFWDADV, " +
		            "BFWDPDB, " +
		            "STACCPDB01, " +
		            "STACCPDB02, " +
		            "STACCPDB03, " +
		            "STACCPDB04, " +
		            "STACCPDB05, " +
		            "STACCPDB06, " +
		            "STACCPDB07, " +
		            "STACCPDB08, " +
		            "STACCPDB09, " +
		            "STACCPDB10, " +
		            "STACCPDB11, " +
		            "STACCPDB12, " +
		            "CFWDPDB, " +
		            "BFWDADB, " +
		            "STACCADB01, " +
		            "STACCADB02, " +
		            "STACCADB03, " +
		            "STACCADB04, " +
		            "STACCADB05, " +
		            "STACCADB06, " +
		            "STACCADB07, " +
		            "STACCADB08, " +
		            "STACCADB09, " +
		            "STACCADB10, " +
		            "STACCADB11, " +
		            "STACCADB12, " +
		            "CFWDADB, " +
		            "BFWDADJ, " +
		            "STACCADJ01, " +
		            "STACCADJ02, " +
		            "STACCADJ03, " +
		            "STACCADJ04, " +
		            "STACCADJ05, " +
		            "STACCADJ06, " +
		            "STACCADJ07, " +
		            "STACCADJ08, " +
		            "STACCADJ09, " +
		            "STACCADJ10, " +
		            "STACCADJ11, " +
		            "STACCADJ12, " +
		            "CFWDADJ, " +
		            "BFWDINT, " +
		            "STACCINT01, " +
		            "STACCINT02, " +
		            "STACCINT03, " +
		            "STACCINT04, " +
		            "STACCINT05, " +
		            "STACCINT06, " +
		            "STACCINT07, " +
		            "STACCINT08, " +
		            "STACCINT09, " +
		            "STACCINT10, " +
		            "STACCINT11, " +
		            "STACCINT12, " +
		            "CFWDINT, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CNTBRANCH ASC, " +
		            "REG ASC, " +
		            "SRCEBUS ASC, " +
		            "STFUND ASC, " +
		            "STSECT ASC, " +
		            "STSSECT ASC, " +
		            "STATCODE ASC, " +
		            "CNPSTAT ASC, " +
		            "CRPSTAT ASC, " +
		            "ACCTCCY ASC, " +
		            "CNTTYPE ASC, " +
		            "CRTABLE ASC, " +
		            "PARIND ASC, " +
		            "ACCTYR ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CNTBRANCH DESC, " +
		            "REG DESC, " +
		            "SRCEBUS DESC, " +
		            "STFUND DESC, " +
		            "STSECT DESC, " +
		            "STSSECT DESC, " +
		            "STATCODE DESC, " +
		            "CNPSTAT DESC, " +
		            "CRPSTAT DESC, " +
		            "ACCTCCY DESC, " +
		            "CNTTYPE DESC, " +
		            "CRTABLE DESC, " +
		            "PARIND DESC, " +
		            "ACCTYR DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               register,
                               chdrcoy,
                               cntbranch,
                               srcebus,
                               statFund,
                               statSect,
                               statSubsect,
                               acctccy,
                               acctyr,
                               cnttype,
                               crtable,
                               statcode,
                               cnPremStat,
                               covPremStat,
                               parind,
                               bfwdfyp,
                               staccfyp01,
                               staccfyp02,
                               staccfyp03,
                               staccfyp04,
                               staccfyp05,
                               staccfyp06,
                               staccfyp07,
                               staccfyp08,
                               staccfyp09,
                               staccfyp10,
                               staccfyp11,
                               staccfyp12,
                               cfwdfyp,
                               bfwdrnp,
                               staccrnp01,
                               staccrnp02,
                               staccrnp03,
                               staccrnp04,
                               staccrnp05,
                               staccrnp06,
                               staccrnp07,
                               staccrnp08,
                               staccrnp09,
                               staccrnp10,
                               staccrnp11,
                               staccrnp12,
                               cfwdrnp,
                               bfwdspd,
                               staccspd01,
                               staccspd02,
                               staccspd03,
                               staccspd04,
                               staccspd05,
                               staccspd06,
                               staccspd07,
                               staccspd08,
                               staccspd09,
                               staccspd10,
                               staccspd11,
                               staccspd12,
                               cfwdspd,
                               bfwdfyc,
                               staccfyc01,
                               staccfyc02,
                               staccfyc03,
                               staccfyc04,
                               staccfyc05,
                               staccfyc06,
                               staccfyc07,
                               staccfyc08,
                               staccfyc09,
                               staccfyc10,
                               staccfyc11,
                               staccfyc12,
                               cfwdfyc,
                               bfwdrlc,
                               staccrlc01,
                               staccrlc02,
                               staccrlc03,
                               staccrlc04,
                               staccrlc05,
                               staccrlc06,
                               staccrlc07,
                               staccrlc08,
                               staccrlc09,
                               staccrlc10,
                               staccrlc11,
                               staccrlc12,
                               cfwdrlc,
                               bfwdspc,
                               staccspc01,
                               staccspc02,
                               staccspc03,
                               staccspc04,
                               staccspc05,
                               staccspc06,
                               staccspc07,
                               staccspc08,
                               staccspc09,
                               staccspc10,
                               staccspc11,
                               staccspc12,
                               cfwdspc,
                               bfwdrb,
                               staccrb01,
                               staccrb02,
                               staccrb03,
                               staccrb04,
                               staccrb05,
                               staccrb06,
                               staccrb07,
                               staccrb08,
                               staccrb09,
                               staccrb10,
                               staccrb11,
                               staccrb12,
                               cfwdrb,
                               bfwdxb,
                               staccxb01,
                               staccxb02,
                               staccxb03,
                               staccxb04,
                               staccxb05,
                               staccxb06,
                               staccxb07,
                               staccxb08,
                               staccxb09,
                               staccxb10,
                               staccxb11,
                               staccxb12,
                               cfwdxb,
                               bfwdtb,
                               stacctb01,
                               stacctb02,
                               stacctb03,
                               stacctb04,
                               stacctb05,
                               stacctb06,
                               stacctb07,
                               stacctb08,
                               stacctb09,
                               stacctb10,
                               stacctb11,
                               stacctb12,
                               cfwdtb,
                               bfwdib,
                               staccib01,
                               staccib02,
                               staccib03,
                               staccib04,
                               staccib05,
                               staccib06,
                               staccib07,
                               staccib08,
                               staccib09,
                               staccib10,
                               staccib11,
                               staccib12,
                               cfwdib,
                               bfwddd,
                               staccdd01,
                               staccdd02,
                               staccdd03,
                               staccdd04,
                               staccdd05,
                               staccdd06,
                               staccdd07,
                               staccdd08,
                               staccdd09,
                               staccdd10,
                               staccdd11,
                               staccdd12,
                               cfwddd,
                               bfwddi,
                               staccdi01,
                               staccdi02,
                               staccdi03,
                               staccdi04,
                               staccdi05,
                               staccdi06,
                               staccdi07,
                               staccdi08,
                               staccdi09,
                               staccdi10,
                               staccdi11,
                               staccdi12,
                               cfwddi,
                               bfwdbs,
                               staccbs01,
                               staccbs02,
                               staccbs03,
                               staccbs04,
                               staccbs05,
                               staccbs06,
                               staccbs07,
                               staccbs08,
                               staccbs09,
                               staccbs10,
                               staccbs11,
                               staccbs12,
                               cfwdbs,
                               bfwdob,
                               staccob01,
                               staccob02,
                               staccob03,
                               staccob04,
                               staccob05,
                               staccob06,
                               staccob07,
                               staccob08,
                               staccob09,
                               staccob10,
                               staccob11,
                               staccob12,
                               cfwdob,
                               bfwdmtb,
                               staccmtb01,
                               staccmtb02,
                               staccmtb03,
                               staccmtb04,
                               staccmtb05,
                               staccmtb06,
                               staccmtb07,
                               staccmtb08,
                               staccmtb09,
                               staccmtb10,
                               staccmtb11,
                               staccmtb12,
                               cfwdmtb,
                               bfwdclr,
                               staccclr01,
                               staccclr02,
                               staccclr03,
                               staccclr04,
                               staccclr05,
                               staccclr06,
                               staccclr07,
                               staccclr08,
                               staccclr09,
                               staccclr10,
                               staccclr11,
                               staccclr12,
                               cfwdclr,
                               bfwdrip,
                               staccrip01,
                               staccrip02,
                               staccrip03,
                               staccrip04,
                               staccrip05,
                               staccrip06,
                               staccrip07,
                               staccrip08,
                               staccrip09,
                               staccrip10,
                               staccrip11,
                               staccrip12,
                               cfwdrip,
                               bfwdric,
                               staccric01,
                               staccric02,
                               staccric03,
                               staccric04,
                               staccric05,
                               staccric06,
                               staccric07,
                               staccric08,
                               staccric09,
                               staccric10,
                               staccric11,
                               staccric12,
                               cfwdric,
                               bfwdadv,
                               staccadv01,
                               staccadv02,
                               staccadv03,
                               staccadv04,
                               staccadv05,
                               staccadv06,
                               staccadv07,
                               staccadv08,
                               staccadv09,
                               staccadv10,
                               staccadv11,
                               staccadv12,
                               cfwdadv,
                               bfwdpdb,
                               staccpdb01,
                               staccpdb02,
                               staccpdb03,
                               staccpdb04,
                               staccpdb05,
                               staccpdb06,
                               staccpdb07,
                               staccpdb08,
                               staccpdb09,
                               staccpdb10,
                               staccpdb11,
                               staccpdb12,
                               cfwdpdb,
                               bfwdadb,
                               staccadb01,
                               staccadb02,
                               staccadb03,
                               staccadb04,
                               staccadb05,
                               staccadb06,
                               staccadb07,
                               staccadb08,
                               staccadb09,
                               staccadb10,
                               staccadb11,
                               staccadb12,
                               cfwdadb,
                               bfwdadj,
                               staccadj01,
                               staccadj02,
                               staccadj03,
                               staccadj04,
                               staccadj05,
                               staccadj06,
                               staccadj07,
                               staccadj08,
                               staccadj09,
                               staccadj10,
                               staccadj11,
                               staccadj12,
                               cfwdadj,
                               bfwdint,
                               staccint01,
                               staccint02,
                               staccint03,
                               staccint04,
                               staccint05,
                               staccint06,
                               staccint07,
                               staccint08,
                               staccint09,
                               staccint10,
                               staccint11,
                               staccint12,
                               cfwdint,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(29);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getChdrcoy().toInternal()
					+ getCntbranch().toInternal()
					+ getRegister().toInternal()
					+ getSrcebus().toInternal()
					+ getStatFund().toInternal()
					+ getStatSect().toInternal()
					+ getStatSubsect().toInternal()
					+ getStatcode().toInternal()
					+ getCnPremStat().toInternal()
					+ getCovPremStat().toInternal()
					+ getAcctccy().toInternal()
					+ getCnttype().toInternal()
					+ getCrtable().toInternal()
					+ getParind().toInternal()
					+ getAcctyr().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, statFund);
			what = ExternalData.chop(what, statSect);
			what = ExternalData.chop(what, statSubsect);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, cnPremStat);
			what = ExternalData.chop(what, covPremStat);
			what = ExternalData.chop(what, acctccy);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, parind);
			what = ExternalData.chop(what, acctyr);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller130 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller140 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller150 = new FixedLengthStringData(1);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(cntbranch.toInternal());
	nonKeyFiller30.setInternal(register.toInternal());
	nonKeyFiller40.setInternal(srcebus.toInternal());
	nonKeyFiller50.setInternal(statFund.toInternal());
	nonKeyFiller60.setInternal(statSect.toInternal());
	nonKeyFiller70.setInternal(statSubsect.toInternal());
	nonKeyFiller80.setInternal(acctccy.toInternal());
	nonKeyFiller90.setInternal(acctyr.toInternal());
	nonKeyFiller100.setInternal(cnttype.toInternal());
	nonKeyFiller110.setInternal(crtable.toInternal());
	nonKeyFiller120.setInternal(statcode.toInternal());
	nonKeyFiller130.setInternal(cnPremStat.toInternal());
	nonKeyFiller140.setInternal(covPremStat.toInternal());
	nonKeyFiller150.setInternal(parind.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(3301);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ nonKeyFiller130.toInternal()
					+ nonKeyFiller140.toInternal()
					+ nonKeyFiller150.toInternal()
					+ getBfwdfyp().toInternal()
					+ getStaccfyp01().toInternal()
					+ getStaccfyp02().toInternal()
					+ getStaccfyp03().toInternal()
					+ getStaccfyp04().toInternal()
					+ getStaccfyp05().toInternal()
					+ getStaccfyp06().toInternal()
					+ getStaccfyp07().toInternal()
					+ getStaccfyp08().toInternal()
					+ getStaccfyp09().toInternal()
					+ getStaccfyp10().toInternal()
					+ getStaccfyp11().toInternal()
					+ getStaccfyp12().toInternal()
					+ getCfwdfyp().toInternal()
					+ getBfwdrnp().toInternal()
					+ getStaccrnp01().toInternal()
					+ getStaccrnp02().toInternal()
					+ getStaccrnp03().toInternal()
					+ getStaccrnp04().toInternal()
					+ getStaccrnp05().toInternal()
					+ getStaccrnp06().toInternal()
					+ getStaccrnp07().toInternal()
					+ getStaccrnp08().toInternal()
					+ getStaccrnp09().toInternal()
					+ getStaccrnp10().toInternal()
					+ getStaccrnp11().toInternal()
					+ getStaccrnp12().toInternal()
					+ getCfwdrnp().toInternal()
					+ getBfwdspd().toInternal()
					+ getStaccspd01().toInternal()
					+ getStaccspd02().toInternal()
					+ getStaccspd03().toInternal()
					+ getStaccspd04().toInternal()
					+ getStaccspd05().toInternal()
					+ getStaccspd06().toInternal()
					+ getStaccspd07().toInternal()
					+ getStaccspd08().toInternal()
					+ getStaccspd09().toInternal()
					+ getStaccspd10().toInternal()
					+ getStaccspd11().toInternal()
					+ getStaccspd12().toInternal()
					+ getCfwdspd().toInternal()
					+ getBfwdfyc().toInternal()
					+ getStaccfyc01().toInternal()
					+ getStaccfyc02().toInternal()
					+ getStaccfyc03().toInternal()
					+ getStaccfyc04().toInternal()
					+ getStaccfyc05().toInternal()
					+ getStaccfyc06().toInternal()
					+ getStaccfyc07().toInternal()
					+ getStaccfyc08().toInternal()
					+ getStaccfyc09().toInternal()
					+ getStaccfyc10().toInternal()
					+ getStaccfyc11().toInternal()
					+ getStaccfyc12().toInternal()
					+ getCfwdfyc().toInternal()
					+ getBfwdrlc().toInternal()
					+ getStaccrlc01().toInternal()
					+ getStaccrlc02().toInternal()
					+ getStaccrlc03().toInternal()
					+ getStaccrlc04().toInternal()
					+ getStaccrlc05().toInternal()
					+ getStaccrlc06().toInternal()
					+ getStaccrlc07().toInternal()
					+ getStaccrlc08().toInternal()
					+ getStaccrlc09().toInternal()
					+ getStaccrlc10().toInternal()
					+ getStaccrlc11().toInternal()
					+ getStaccrlc12().toInternal()
					+ getCfwdrlc().toInternal()
					+ getBfwdspc().toInternal()
					+ getStaccspc01().toInternal()
					+ getStaccspc02().toInternal()
					+ getStaccspc03().toInternal()
					+ getStaccspc04().toInternal()
					+ getStaccspc05().toInternal()
					+ getStaccspc06().toInternal()
					+ getStaccspc07().toInternal()
					+ getStaccspc08().toInternal()
					+ getStaccspc09().toInternal()
					+ getStaccspc10().toInternal()
					+ getStaccspc11().toInternal()
					+ getStaccspc12().toInternal()
					+ getCfwdspc().toInternal()
					+ getBfwdrb().toInternal()
					+ getStaccrb01().toInternal()
					+ getStaccrb02().toInternal()
					+ getStaccrb03().toInternal()
					+ getStaccrb04().toInternal()
					+ getStaccrb05().toInternal()
					+ getStaccrb06().toInternal()
					+ getStaccrb07().toInternal()
					+ getStaccrb08().toInternal()
					+ getStaccrb09().toInternal()
					+ getStaccrb10().toInternal()
					+ getStaccrb11().toInternal()
					+ getStaccrb12().toInternal()
					+ getCfwdrb().toInternal()
					+ getBfwdxb().toInternal()
					+ getStaccxb01().toInternal()
					+ getStaccxb02().toInternal()
					+ getStaccxb03().toInternal()
					+ getStaccxb04().toInternal()
					+ getStaccxb05().toInternal()
					+ getStaccxb06().toInternal()
					+ getStaccxb07().toInternal()
					+ getStaccxb08().toInternal()
					+ getStaccxb09().toInternal()
					+ getStaccxb10().toInternal()
					+ getStaccxb11().toInternal()
					+ getStaccxb12().toInternal()
					+ getCfwdxb().toInternal()
					+ getBfwdtb().toInternal()
					+ getStacctb01().toInternal()
					+ getStacctb02().toInternal()
					+ getStacctb03().toInternal()
					+ getStacctb04().toInternal()
					+ getStacctb05().toInternal()
					+ getStacctb06().toInternal()
					+ getStacctb07().toInternal()
					+ getStacctb08().toInternal()
					+ getStacctb09().toInternal()
					+ getStacctb10().toInternal()
					+ getStacctb11().toInternal()
					+ getStacctb12().toInternal()
					+ getCfwdtb().toInternal()
					+ getBfwdib().toInternal()
					+ getStaccib01().toInternal()
					+ getStaccib02().toInternal()
					+ getStaccib03().toInternal()
					+ getStaccib04().toInternal()
					+ getStaccib05().toInternal()
					+ getStaccib06().toInternal()
					+ getStaccib07().toInternal()
					+ getStaccib08().toInternal()
					+ getStaccib09().toInternal()
					+ getStaccib10().toInternal()
					+ getStaccib11().toInternal()
					+ getStaccib12().toInternal()
					+ getCfwdib().toInternal()
					+ getBfwddd().toInternal()
					+ getStaccdd01().toInternal()
					+ getStaccdd02().toInternal()
					+ getStaccdd03().toInternal()
					+ getStaccdd04().toInternal()
					+ getStaccdd05().toInternal()
					+ getStaccdd06().toInternal()
					+ getStaccdd07().toInternal()
					+ getStaccdd08().toInternal()
					+ getStaccdd09().toInternal()
					+ getStaccdd10().toInternal()
					+ getStaccdd11().toInternal()
					+ getStaccdd12().toInternal()
					+ getCfwddd().toInternal()
					+ getBfwddi().toInternal()
					+ getStaccdi01().toInternal()
					+ getStaccdi02().toInternal()
					+ getStaccdi03().toInternal()
					+ getStaccdi04().toInternal()
					+ getStaccdi05().toInternal()
					+ getStaccdi06().toInternal()
					+ getStaccdi07().toInternal()
					+ getStaccdi08().toInternal()
					+ getStaccdi09().toInternal()
					+ getStaccdi10().toInternal()
					+ getStaccdi11().toInternal()
					+ getStaccdi12().toInternal()
					+ getCfwddi().toInternal()
					+ getBfwdbs().toInternal()
					+ getStaccbs01().toInternal()
					+ getStaccbs02().toInternal()
					+ getStaccbs03().toInternal()
					+ getStaccbs04().toInternal()
					+ getStaccbs05().toInternal()
					+ getStaccbs06().toInternal()
					+ getStaccbs07().toInternal()
					+ getStaccbs08().toInternal()
					+ getStaccbs09().toInternal()
					+ getStaccbs10().toInternal()
					+ getStaccbs11().toInternal()
					+ getStaccbs12().toInternal()
					+ getCfwdbs().toInternal()
					+ getBfwdob().toInternal()
					+ getStaccob01().toInternal()
					+ getStaccob02().toInternal()
					+ getStaccob03().toInternal()
					+ getStaccob04().toInternal()
					+ getStaccob05().toInternal()
					+ getStaccob06().toInternal()
					+ getStaccob07().toInternal()
					+ getStaccob08().toInternal()
					+ getStaccob09().toInternal()
					+ getStaccob10().toInternal()
					+ getStaccob11().toInternal()
					+ getStaccob12().toInternal()
					+ getCfwdob().toInternal()
					+ getBfwdmtb().toInternal()
					+ getStaccmtb01().toInternal()
					+ getStaccmtb02().toInternal()
					+ getStaccmtb03().toInternal()
					+ getStaccmtb04().toInternal()
					+ getStaccmtb05().toInternal()
					+ getStaccmtb06().toInternal()
					+ getStaccmtb07().toInternal()
					+ getStaccmtb08().toInternal()
					+ getStaccmtb09().toInternal()
					+ getStaccmtb10().toInternal()
					+ getStaccmtb11().toInternal()
					+ getStaccmtb12().toInternal()
					+ getCfwdmtb().toInternal()
					+ getBfwdclr().toInternal()
					+ getStaccclr01().toInternal()
					+ getStaccclr02().toInternal()
					+ getStaccclr03().toInternal()
					+ getStaccclr04().toInternal()
					+ getStaccclr05().toInternal()
					+ getStaccclr06().toInternal()
					+ getStaccclr07().toInternal()
					+ getStaccclr08().toInternal()
					+ getStaccclr09().toInternal()
					+ getStaccclr10().toInternal()
					+ getStaccclr11().toInternal()
					+ getStaccclr12().toInternal()
					+ getCfwdclr().toInternal()
					+ getBfwdrip().toInternal()
					+ getStaccrip01().toInternal()
					+ getStaccrip02().toInternal()
					+ getStaccrip03().toInternal()
					+ getStaccrip04().toInternal()
					+ getStaccrip05().toInternal()
					+ getStaccrip06().toInternal()
					+ getStaccrip07().toInternal()
					+ getStaccrip08().toInternal()
					+ getStaccrip09().toInternal()
					+ getStaccrip10().toInternal()
					+ getStaccrip11().toInternal()
					+ getStaccrip12().toInternal()
					+ getCfwdrip().toInternal()
					+ getBfwdric().toInternal()
					+ getStaccric01().toInternal()
					+ getStaccric02().toInternal()
					+ getStaccric03().toInternal()
					+ getStaccric04().toInternal()
					+ getStaccric05().toInternal()
					+ getStaccric06().toInternal()
					+ getStaccric07().toInternal()
					+ getStaccric08().toInternal()
					+ getStaccric09().toInternal()
					+ getStaccric10().toInternal()
					+ getStaccric11().toInternal()
					+ getStaccric12().toInternal()
					+ getCfwdric().toInternal()
					+ getBfwdadv().toInternal()
					+ getStaccadv01().toInternal()
					+ getStaccadv02().toInternal()
					+ getStaccadv03().toInternal()
					+ getStaccadv04().toInternal()
					+ getStaccadv05().toInternal()
					+ getStaccadv06().toInternal()
					+ getStaccadv07().toInternal()
					+ getStaccadv08().toInternal()
					+ getStaccadv09().toInternal()
					+ getStaccadv10().toInternal()
					+ getStaccadv11().toInternal()
					+ getStaccadv12().toInternal()
					+ getCfwdadv().toInternal()
					+ getBfwdpdb().toInternal()
					+ getStaccpdb01().toInternal()
					+ getStaccpdb02().toInternal()
					+ getStaccpdb03().toInternal()
					+ getStaccpdb04().toInternal()
					+ getStaccpdb05().toInternal()
					+ getStaccpdb06().toInternal()
					+ getStaccpdb07().toInternal()
					+ getStaccpdb08().toInternal()
					+ getStaccpdb09().toInternal()
					+ getStaccpdb10().toInternal()
					+ getStaccpdb11().toInternal()
					+ getStaccpdb12().toInternal()
					+ getCfwdpdb().toInternal()
					+ getBfwdadb().toInternal()
					+ getStaccadb01().toInternal()
					+ getStaccadb02().toInternal()
					+ getStaccadb03().toInternal()
					+ getStaccadb04().toInternal()
					+ getStaccadb05().toInternal()
					+ getStaccadb06().toInternal()
					+ getStaccadb07().toInternal()
					+ getStaccadb08().toInternal()
					+ getStaccadb09().toInternal()
					+ getStaccadb10().toInternal()
					+ getStaccadb11().toInternal()
					+ getStaccadb12().toInternal()
					+ getCfwdadb().toInternal()
					+ getBfwdadj().toInternal()
					+ getStaccadj01().toInternal()
					+ getStaccadj02().toInternal()
					+ getStaccadj03().toInternal()
					+ getStaccadj04().toInternal()
					+ getStaccadj05().toInternal()
					+ getStaccadj06().toInternal()
					+ getStaccadj07().toInternal()
					+ getStaccadj08().toInternal()
					+ getStaccadj09().toInternal()
					+ getStaccadj10().toInternal()
					+ getStaccadj11().toInternal()
					+ getStaccadj12().toInternal()
					+ getCfwdadj().toInternal()
					+ getBfwdint().toInternal()
					+ getStaccint01().toInternal()
					+ getStaccint02().toInternal()
					+ getStaccint03().toInternal()
					+ getStaccint04().toInternal()
					+ getStaccint05().toInternal()
					+ getStaccint06().toInternal()
					+ getStaccint07().toInternal()
					+ getStaccint08().toInternal()
					+ getStaccint09().toInternal()
					+ getStaccint10().toInternal()
					+ getStaccint11().toInternal()
					+ getStaccint12().toInternal()
					+ getCfwdint().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, nonKeyFiller130);
			what = ExternalData.chop(what, nonKeyFiller140);
			what = ExternalData.chop(what, nonKeyFiller150);
			what = ExternalData.chop(what, bfwdfyp);
			what = ExternalData.chop(what, staccfyp01);
			what = ExternalData.chop(what, staccfyp02);
			what = ExternalData.chop(what, staccfyp03);
			what = ExternalData.chop(what, staccfyp04);
			what = ExternalData.chop(what, staccfyp05);
			what = ExternalData.chop(what, staccfyp06);
			what = ExternalData.chop(what, staccfyp07);
			what = ExternalData.chop(what, staccfyp08);
			what = ExternalData.chop(what, staccfyp09);
			what = ExternalData.chop(what, staccfyp10);
			what = ExternalData.chop(what, staccfyp11);
			what = ExternalData.chop(what, staccfyp12);
			what = ExternalData.chop(what, cfwdfyp);
			what = ExternalData.chop(what, bfwdrnp);
			what = ExternalData.chop(what, staccrnp01);
			what = ExternalData.chop(what, staccrnp02);
			what = ExternalData.chop(what, staccrnp03);
			what = ExternalData.chop(what, staccrnp04);
			what = ExternalData.chop(what, staccrnp05);
			what = ExternalData.chop(what, staccrnp06);
			what = ExternalData.chop(what, staccrnp07);
			what = ExternalData.chop(what, staccrnp08);
			what = ExternalData.chop(what, staccrnp09);
			what = ExternalData.chop(what, staccrnp10);
			what = ExternalData.chop(what, staccrnp11);
			what = ExternalData.chop(what, staccrnp12);
			what = ExternalData.chop(what, cfwdrnp);
			what = ExternalData.chop(what, bfwdspd);
			what = ExternalData.chop(what, staccspd01);
			what = ExternalData.chop(what, staccspd02);
			what = ExternalData.chop(what, staccspd03);
			what = ExternalData.chop(what, staccspd04);
			what = ExternalData.chop(what, staccspd05);
			what = ExternalData.chop(what, staccspd06);
			what = ExternalData.chop(what, staccspd07);
			what = ExternalData.chop(what, staccspd08);
			what = ExternalData.chop(what, staccspd09);
			what = ExternalData.chop(what, staccspd10);
			what = ExternalData.chop(what, staccspd11);
			what = ExternalData.chop(what, staccspd12);
			what = ExternalData.chop(what, cfwdspd);
			what = ExternalData.chop(what, bfwdfyc);
			what = ExternalData.chop(what, staccfyc01);
			what = ExternalData.chop(what, staccfyc02);
			what = ExternalData.chop(what, staccfyc03);
			what = ExternalData.chop(what, staccfyc04);
			what = ExternalData.chop(what, staccfyc05);
			what = ExternalData.chop(what, staccfyc06);
			what = ExternalData.chop(what, staccfyc07);
			what = ExternalData.chop(what, staccfyc08);
			what = ExternalData.chop(what, staccfyc09);
			what = ExternalData.chop(what, staccfyc10);
			what = ExternalData.chop(what, staccfyc11);
			what = ExternalData.chop(what, staccfyc12);
			what = ExternalData.chop(what, cfwdfyc);
			what = ExternalData.chop(what, bfwdrlc);
			what = ExternalData.chop(what, staccrlc01);
			what = ExternalData.chop(what, staccrlc02);
			what = ExternalData.chop(what, staccrlc03);
			what = ExternalData.chop(what, staccrlc04);
			what = ExternalData.chop(what, staccrlc05);
			what = ExternalData.chop(what, staccrlc06);
			what = ExternalData.chop(what, staccrlc07);
			what = ExternalData.chop(what, staccrlc08);
			what = ExternalData.chop(what, staccrlc09);
			what = ExternalData.chop(what, staccrlc10);
			what = ExternalData.chop(what, staccrlc11);
			what = ExternalData.chop(what, staccrlc12);
			what = ExternalData.chop(what, cfwdrlc);
			what = ExternalData.chop(what, bfwdspc);
			what = ExternalData.chop(what, staccspc01);
			what = ExternalData.chop(what, staccspc02);
			what = ExternalData.chop(what, staccspc03);
			what = ExternalData.chop(what, staccspc04);
			what = ExternalData.chop(what, staccspc05);
			what = ExternalData.chop(what, staccspc06);
			what = ExternalData.chop(what, staccspc07);
			what = ExternalData.chop(what, staccspc08);
			what = ExternalData.chop(what, staccspc09);
			what = ExternalData.chop(what, staccspc10);
			what = ExternalData.chop(what, staccspc11);
			what = ExternalData.chop(what, staccspc12);
			what = ExternalData.chop(what, cfwdspc);
			what = ExternalData.chop(what, bfwdrb);
			what = ExternalData.chop(what, staccrb01);
			what = ExternalData.chop(what, staccrb02);
			what = ExternalData.chop(what, staccrb03);
			what = ExternalData.chop(what, staccrb04);
			what = ExternalData.chop(what, staccrb05);
			what = ExternalData.chop(what, staccrb06);
			what = ExternalData.chop(what, staccrb07);
			what = ExternalData.chop(what, staccrb08);
			what = ExternalData.chop(what, staccrb09);
			what = ExternalData.chop(what, staccrb10);
			what = ExternalData.chop(what, staccrb11);
			what = ExternalData.chop(what, staccrb12);
			what = ExternalData.chop(what, cfwdrb);
			what = ExternalData.chop(what, bfwdxb);
			what = ExternalData.chop(what, staccxb01);
			what = ExternalData.chop(what, staccxb02);
			what = ExternalData.chop(what, staccxb03);
			what = ExternalData.chop(what, staccxb04);
			what = ExternalData.chop(what, staccxb05);
			what = ExternalData.chop(what, staccxb06);
			what = ExternalData.chop(what, staccxb07);
			what = ExternalData.chop(what, staccxb08);
			what = ExternalData.chop(what, staccxb09);
			what = ExternalData.chop(what, staccxb10);
			what = ExternalData.chop(what, staccxb11);
			what = ExternalData.chop(what, staccxb12);
			what = ExternalData.chop(what, cfwdxb);
			what = ExternalData.chop(what, bfwdtb);
			what = ExternalData.chop(what, stacctb01);
			what = ExternalData.chop(what, stacctb02);
			what = ExternalData.chop(what, stacctb03);
			what = ExternalData.chop(what, stacctb04);
			what = ExternalData.chop(what, stacctb05);
			what = ExternalData.chop(what, stacctb06);
			what = ExternalData.chop(what, stacctb07);
			what = ExternalData.chop(what, stacctb08);
			what = ExternalData.chop(what, stacctb09);
			what = ExternalData.chop(what, stacctb10);
			what = ExternalData.chop(what, stacctb11);
			what = ExternalData.chop(what, stacctb12);
			what = ExternalData.chop(what, cfwdtb);
			what = ExternalData.chop(what, bfwdib);
			what = ExternalData.chop(what, staccib01);
			what = ExternalData.chop(what, staccib02);
			what = ExternalData.chop(what, staccib03);
			what = ExternalData.chop(what, staccib04);
			what = ExternalData.chop(what, staccib05);
			what = ExternalData.chop(what, staccib06);
			what = ExternalData.chop(what, staccib07);
			what = ExternalData.chop(what, staccib08);
			what = ExternalData.chop(what, staccib09);
			what = ExternalData.chop(what, staccib10);
			what = ExternalData.chop(what, staccib11);
			what = ExternalData.chop(what, staccib12);
			what = ExternalData.chop(what, cfwdib);
			what = ExternalData.chop(what, bfwddd);
			what = ExternalData.chop(what, staccdd01);
			what = ExternalData.chop(what, staccdd02);
			what = ExternalData.chop(what, staccdd03);
			what = ExternalData.chop(what, staccdd04);
			what = ExternalData.chop(what, staccdd05);
			what = ExternalData.chop(what, staccdd06);
			what = ExternalData.chop(what, staccdd07);
			what = ExternalData.chop(what, staccdd08);
			what = ExternalData.chop(what, staccdd09);
			what = ExternalData.chop(what, staccdd10);
			what = ExternalData.chop(what, staccdd11);
			what = ExternalData.chop(what, staccdd12);
			what = ExternalData.chop(what, cfwddd);
			what = ExternalData.chop(what, bfwddi);
			what = ExternalData.chop(what, staccdi01);
			what = ExternalData.chop(what, staccdi02);
			what = ExternalData.chop(what, staccdi03);
			what = ExternalData.chop(what, staccdi04);
			what = ExternalData.chop(what, staccdi05);
			what = ExternalData.chop(what, staccdi06);
			what = ExternalData.chop(what, staccdi07);
			what = ExternalData.chop(what, staccdi08);
			what = ExternalData.chop(what, staccdi09);
			what = ExternalData.chop(what, staccdi10);
			what = ExternalData.chop(what, staccdi11);
			what = ExternalData.chop(what, staccdi12);
			what = ExternalData.chop(what, cfwddi);
			what = ExternalData.chop(what, bfwdbs);
			what = ExternalData.chop(what, staccbs01);
			what = ExternalData.chop(what, staccbs02);
			what = ExternalData.chop(what, staccbs03);
			what = ExternalData.chop(what, staccbs04);
			what = ExternalData.chop(what, staccbs05);
			what = ExternalData.chop(what, staccbs06);
			what = ExternalData.chop(what, staccbs07);
			what = ExternalData.chop(what, staccbs08);
			what = ExternalData.chop(what, staccbs09);
			what = ExternalData.chop(what, staccbs10);
			what = ExternalData.chop(what, staccbs11);
			what = ExternalData.chop(what, staccbs12);
			what = ExternalData.chop(what, cfwdbs);
			what = ExternalData.chop(what, bfwdob);
			what = ExternalData.chop(what, staccob01);
			what = ExternalData.chop(what, staccob02);
			what = ExternalData.chop(what, staccob03);
			what = ExternalData.chop(what, staccob04);
			what = ExternalData.chop(what, staccob05);
			what = ExternalData.chop(what, staccob06);
			what = ExternalData.chop(what, staccob07);
			what = ExternalData.chop(what, staccob08);
			what = ExternalData.chop(what, staccob09);
			what = ExternalData.chop(what, staccob10);
			what = ExternalData.chop(what, staccob11);
			what = ExternalData.chop(what, staccob12);
			what = ExternalData.chop(what, cfwdob);
			what = ExternalData.chop(what, bfwdmtb);
			what = ExternalData.chop(what, staccmtb01);
			what = ExternalData.chop(what, staccmtb02);
			what = ExternalData.chop(what, staccmtb03);
			what = ExternalData.chop(what, staccmtb04);
			what = ExternalData.chop(what, staccmtb05);
			what = ExternalData.chop(what, staccmtb06);
			what = ExternalData.chop(what, staccmtb07);
			what = ExternalData.chop(what, staccmtb08);
			what = ExternalData.chop(what, staccmtb09);
			what = ExternalData.chop(what, staccmtb10);
			what = ExternalData.chop(what, staccmtb11);
			what = ExternalData.chop(what, staccmtb12);
			what = ExternalData.chop(what, cfwdmtb);
			what = ExternalData.chop(what, bfwdclr);
			what = ExternalData.chop(what, staccclr01);
			what = ExternalData.chop(what, staccclr02);
			what = ExternalData.chop(what, staccclr03);
			what = ExternalData.chop(what, staccclr04);
			what = ExternalData.chop(what, staccclr05);
			what = ExternalData.chop(what, staccclr06);
			what = ExternalData.chop(what, staccclr07);
			what = ExternalData.chop(what, staccclr08);
			what = ExternalData.chop(what, staccclr09);
			what = ExternalData.chop(what, staccclr10);
			what = ExternalData.chop(what, staccclr11);
			what = ExternalData.chop(what, staccclr12);
			what = ExternalData.chop(what, cfwdclr);
			what = ExternalData.chop(what, bfwdrip);
			what = ExternalData.chop(what, staccrip01);
			what = ExternalData.chop(what, staccrip02);
			what = ExternalData.chop(what, staccrip03);
			what = ExternalData.chop(what, staccrip04);
			what = ExternalData.chop(what, staccrip05);
			what = ExternalData.chop(what, staccrip06);
			what = ExternalData.chop(what, staccrip07);
			what = ExternalData.chop(what, staccrip08);
			what = ExternalData.chop(what, staccrip09);
			what = ExternalData.chop(what, staccrip10);
			what = ExternalData.chop(what, staccrip11);
			what = ExternalData.chop(what, staccrip12);
			what = ExternalData.chop(what, cfwdrip);
			what = ExternalData.chop(what, bfwdric);
			what = ExternalData.chop(what, staccric01);
			what = ExternalData.chop(what, staccric02);
			what = ExternalData.chop(what, staccric03);
			what = ExternalData.chop(what, staccric04);
			what = ExternalData.chop(what, staccric05);
			what = ExternalData.chop(what, staccric06);
			what = ExternalData.chop(what, staccric07);
			what = ExternalData.chop(what, staccric08);
			what = ExternalData.chop(what, staccric09);
			what = ExternalData.chop(what, staccric10);
			what = ExternalData.chop(what, staccric11);
			what = ExternalData.chop(what, staccric12);
			what = ExternalData.chop(what, cfwdric);
			what = ExternalData.chop(what, bfwdadv);
			what = ExternalData.chop(what, staccadv01);
			what = ExternalData.chop(what, staccadv02);
			what = ExternalData.chop(what, staccadv03);
			what = ExternalData.chop(what, staccadv04);
			what = ExternalData.chop(what, staccadv05);
			what = ExternalData.chop(what, staccadv06);
			what = ExternalData.chop(what, staccadv07);
			what = ExternalData.chop(what, staccadv08);
			what = ExternalData.chop(what, staccadv09);
			what = ExternalData.chop(what, staccadv10);
			what = ExternalData.chop(what, staccadv11);
			what = ExternalData.chop(what, staccadv12);
			what = ExternalData.chop(what, cfwdadv);
			what = ExternalData.chop(what, bfwdpdb);
			what = ExternalData.chop(what, staccpdb01);
			what = ExternalData.chop(what, staccpdb02);
			what = ExternalData.chop(what, staccpdb03);
			what = ExternalData.chop(what, staccpdb04);
			what = ExternalData.chop(what, staccpdb05);
			what = ExternalData.chop(what, staccpdb06);
			what = ExternalData.chop(what, staccpdb07);
			what = ExternalData.chop(what, staccpdb08);
			what = ExternalData.chop(what, staccpdb09);
			what = ExternalData.chop(what, staccpdb10);
			what = ExternalData.chop(what, staccpdb11);
			what = ExternalData.chop(what, staccpdb12);
			what = ExternalData.chop(what, cfwdpdb);
			what = ExternalData.chop(what, bfwdadb);
			what = ExternalData.chop(what, staccadb01);
			what = ExternalData.chop(what, staccadb02);
			what = ExternalData.chop(what, staccadb03);
			what = ExternalData.chop(what, staccadb04);
			what = ExternalData.chop(what, staccadb05);
			what = ExternalData.chop(what, staccadb06);
			what = ExternalData.chop(what, staccadb07);
			what = ExternalData.chop(what, staccadb08);
			what = ExternalData.chop(what, staccadb09);
			what = ExternalData.chop(what, staccadb10);
			what = ExternalData.chop(what, staccadb11);
			what = ExternalData.chop(what, staccadb12);
			what = ExternalData.chop(what, cfwdadb);
			what = ExternalData.chop(what, bfwdadj);
			what = ExternalData.chop(what, staccadj01);
			what = ExternalData.chop(what, staccadj02);
			what = ExternalData.chop(what, staccadj03);
			what = ExternalData.chop(what, staccadj04);
			what = ExternalData.chop(what, staccadj05);
			what = ExternalData.chop(what, staccadj06);
			what = ExternalData.chop(what, staccadj07);
			what = ExternalData.chop(what, staccadj08);
			what = ExternalData.chop(what, staccadj09);
			what = ExternalData.chop(what, staccadj10);
			what = ExternalData.chop(what, staccadj11);
			what = ExternalData.chop(what, staccadj12);
			what = ExternalData.chop(what, cfwdadj);
			what = ExternalData.chop(what, bfwdint);
			what = ExternalData.chop(what, staccint01);
			what = ExternalData.chop(what, staccint02);
			what = ExternalData.chop(what, staccint03);
			what = ExternalData.chop(what, staccint04);
			what = ExternalData.chop(what, staccint05);
			what = ExternalData.chop(what, staccint06);
			what = ExternalData.chop(what, staccint07);
			what = ExternalData.chop(what, staccint08);
			what = ExternalData.chop(what, staccint09);
			what = ExternalData.chop(what, staccint10);
			what = ExternalData.chop(what, staccint11);
			what = ExternalData.chop(what, staccint12);
			what = ExternalData.chop(what, cfwdint);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}
	public FixedLengthStringData getStatFund() {
		return statFund;
	}
	public void setStatFund(Object what) {
		statFund.set(what);
	}
	public FixedLengthStringData getStatSect() {
		return statSect;
	}
	public void setStatSect(Object what) {
		statSect.set(what);
	}
	public FixedLengthStringData getStatSubsect() {
		return statSubsect;
	}
	public void setStatSubsect(Object what) {
		statSubsect.set(what);
	}
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}
	public FixedLengthStringData getCnPremStat() {
		return cnPremStat;
	}
	public void setCnPremStat(Object what) {
		cnPremStat.set(what);
	}
	public FixedLengthStringData getCovPremStat() {
		return covPremStat;
	}
	public void setCovPremStat(Object what) {
		covPremStat.set(what);
	}
	public FixedLengthStringData getAcctccy() {
		return acctccy;
	}
	public void setAcctccy(Object what) {
		acctccy.set(what);
	}
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}
	public FixedLengthStringData getParind() {
		return parind;
	}
	public void setParind(Object what) {
		parind.set(what);
	}
	public PackedDecimalData getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(Object what) {
		setAcctyr(what, false);
	}
	public void setAcctyr(Object what, boolean rounded) {
		if (rounded)
			acctyr.setRounded(what);
		else
			acctyr.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public PackedDecimalData getBfwdfyp() {
		return bfwdfyp;
	}
	public void setBfwdfyp(Object what) {
		setBfwdfyp(what, false);
	}
	public void setBfwdfyp(Object what, boolean rounded) {
		if (rounded)
			bfwdfyp.setRounded(what);
		else
			bfwdfyp.set(what);
	}	
	public PackedDecimalData getStaccfyp01() {
		return staccfyp01;
	}
	public void setStaccfyp01(Object what) {
		setStaccfyp01(what, false);
	}
	public void setStaccfyp01(Object what, boolean rounded) {
		if (rounded)
			staccfyp01.setRounded(what);
		else
			staccfyp01.set(what);
	}	
	public PackedDecimalData getStaccfyp02() {
		return staccfyp02;
	}
	public void setStaccfyp02(Object what) {
		setStaccfyp02(what, false);
	}
	public void setStaccfyp02(Object what, boolean rounded) {
		if (rounded)
			staccfyp02.setRounded(what);
		else
			staccfyp02.set(what);
	}	
	public PackedDecimalData getStaccfyp03() {
		return staccfyp03;
	}
	public void setStaccfyp03(Object what) {
		setStaccfyp03(what, false);
	}
	public void setStaccfyp03(Object what, boolean rounded) {
		if (rounded)
			staccfyp03.setRounded(what);
		else
			staccfyp03.set(what);
	}	
	public PackedDecimalData getStaccfyp04() {
		return staccfyp04;
	}
	public void setStaccfyp04(Object what) {
		setStaccfyp04(what, false);
	}
	public void setStaccfyp04(Object what, boolean rounded) {
		if (rounded)
			staccfyp04.setRounded(what);
		else
			staccfyp04.set(what);
	}	
	public PackedDecimalData getStaccfyp05() {
		return staccfyp05;
	}
	public void setStaccfyp05(Object what) {
		setStaccfyp05(what, false);
	}
	public void setStaccfyp05(Object what, boolean rounded) {
		if (rounded)
			staccfyp05.setRounded(what);
		else
			staccfyp05.set(what);
	}	
	public PackedDecimalData getStaccfyp06() {
		return staccfyp06;
	}
	public void setStaccfyp06(Object what) {
		setStaccfyp06(what, false);
	}
	public void setStaccfyp06(Object what, boolean rounded) {
		if (rounded)
			staccfyp06.setRounded(what);
		else
			staccfyp06.set(what);
	}	
	public PackedDecimalData getStaccfyp07() {
		return staccfyp07;
	}
	public void setStaccfyp07(Object what) {
		setStaccfyp07(what, false);
	}
	public void setStaccfyp07(Object what, boolean rounded) {
		if (rounded)
			staccfyp07.setRounded(what);
		else
			staccfyp07.set(what);
	}	
	public PackedDecimalData getStaccfyp08() {
		return staccfyp08;
	}
	public void setStaccfyp08(Object what) {
		setStaccfyp08(what, false);
	}
	public void setStaccfyp08(Object what, boolean rounded) {
		if (rounded)
			staccfyp08.setRounded(what);
		else
			staccfyp08.set(what);
	}	
	public PackedDecimalData getStaccfyp09() {
		return staccfyp09;
	}
	public void setStaccfyp09(Object what) {
		setStaccfyp09(what, false);
	}
	public void setStaccfyp09(Object what, boolean rounded) {
		if (rounded)
			staccfyp09.setRounded(what);
		else
			staccfyp09.set(what);
	}	
	public PackedDecimalData getStaccfyp10() {
		return staccfyp10;
	}
	public void setStaccfyp10(Object what) {
		setStaccfyp10(what, false);
	}
	public void setStaccfyp10(Object what, boolean rounded) {
		if (rounded)
			staccfyp10.setRounded(what);
		else
			staccfyp10.set(what);
	}	
	public PackedDecimalData getStaccfyp11() {
		return staccfyp11;
	}
	public void setStaccfyp11(Object what) {
		setStaccfyp11(what, false);
	}
	public void setStaccfyp11(Object what, boolean rounded) {
		if (rounded)
			staccfyp11.setRounded(what);
		else
			staccfyp11.set(what);
	}	
	public PackedDecimalData getStaccfyp12() {
		return staccfyp12;
	}
	public void setStaccfyp12(Object what) {
		setStaccfyp12(what, false);
	}
	public void setStaccfyp12(Object what, boolean rounded) {
		if (rounded)
			staccfyp12.setRounded(what);
		else
			staccfyp12.set(what);
	}	
	public PackedDecimalData getCfwdfyp() {
		return cfwdfyp;
	}
	public void setCfwdfyp(Object what) {
		setCfwdfyp(what, false);
	}
	public void setCfwdfyp(Object what, boolean rounded) {
		if (rounded)
			cfwdfyp.setRounded(what);
		else
			cfwdfyp.set(what);
	}	
	public PackedDecimalData getBfwdrnp() {
		return bfwdrnp;
	}
	public void setBfwdrnp(Object what) {
		setBfwdrnp(what, false);
	}
	public void setBfwdrnp(Object what, boolean rounded) {
		if (rounded)
			bfwdrnp.setRounded(what);
		else
			bfwdrnp.set(what);
	}	
	public PackedDecimalData getStaccrnp01() {
		return staccrnp01;
	}
	public void setStaccrnp01(Object what) {
		setStaccrnp01(what, false);
	}
	public void setStaccrnp01(Object what, boolean rounded) {
		if (rounded)
			staccrnp01.setRounded(what);
		else
			staccrnp01.set(what);
	}	
	public PackedDecimalData getStaccrnp02() {
		return staccrnp02;
	}
	public void setStaccrnp02(Object what) {
		setStaccrnp02(what, false);
	}
	public void setStaccrnp02(Object what, boolean rounded) {
		if (rounded)
			staccrnp02.setRounded(what);
		else
			staccrnp02.set(what);
	}	
	public PackedDecimalData getStaccrnp03() {
		return staccrnp03;
	}
	public void setStaccrnp03(Object what) {
		setStaccrnp03(what, false);
	}
	public void setStaccrnp03(Object what, boolean rounded) {
		if (rounded)
			staccrnp03.setRounded(what);
		else
			staccrnp03.set(what);
	}	
	public PackedDecimalData getStaccrnp04() {
		return staccrnp04;
	}
	public void setStaccrnp04(Object what) {
		setStaccrnp04(what, false);
	}
	public void setStaccrnp04(Object what, boolean rounded) {
		if (rounded)
			staccrnp04.setRounded(what);
		else
			staccrnp04.set(what);
	}	
	public PackedDecimalData getStaccrnp05() {
		return staccrnp05;
	}
	public void setStaccrnp05(Object what) {
		setStaccrnp05(what, false);
	}
	public void setStaccrnp05(Object what, boolean rounded) {
		if (rounded)
			staccrnp05.setRounded(what);
		else
			staccrnp05.set(what);
	}	
	public PackedDecimalData getStaccrnp06() {
		return staccrnp06;
	}
	public void setStaccrnp06(Object what) {
		setStaccrnp06(what, false);
	}
	public void setStaccrnp06(Object what, boolean rounded) {
		if (rounded)
			staccrnp06.setRounded(what);
		else
			staccrnp06.set(what);
	}	
	public PackedDecimalData getStaccrnp07() {
		return staccrnp07;
	}
	public void setStaccrnp07(Object what) {
		setStaccrnp07(what, false);
	}
	public void setStaccrnp07(Object what, boolean rounded) {
		if (rounded)
			staccrnp07.setRounded(what);
		else
			staccrnp07.set(what);
	}	
	public PackedDecimalData getStaccrnp08() {
		return staccrnp08;
	}
	public void setStaccrnp08(Object what) {
		setStaccrnp08(what, false);
	}
	public void setStaccrnp08(Object what, boolean rounded) {
		if (rounded)
			staccrnp08.setRounded(what);
		else
			staccrnp08.set(what);
	}	
	public PackedDecimalData getStaccrnp09() {
		return staccrnp09;
	}
	public void setStaccrnp09(Object what) {
		setStaccrnp09(what, false);
	}
	public void setStaccrnp09(Object what, boolean rounded) {
		if (rounded)
			staccrnp09.setRounded(what);
		else
			staccrnp09.set(what);
	}	
	public PackedDecimalData getStaccrnp10() {
		return staccrnp10;
	}
	public void setStaccrnp10(Object what) {
		setStaccrnp10(what, false);
	}
	public void setStaccrnp10(Object what, boolean rounded) {
		if (rounded)
			staccrnp10.setRounded(what);
		else
			staccrnp10.set(what);
	}	
	public PackedDecimalData getStaccrnp11() {
		return staccrnp11;
	}
	public void setStaccrnp11(Object what) {
		setStaccrnp11(what, false);
	}
	public void setStaccrnp11(Object what, boolean rounded) {
		if (rounded)
			staccrnp11.setRounded(what);
		else
			staccrnp11.set(what);
	}	
	public PackedDecimalData getStaccrnp12() {
		return staccrnp12;
	}
	public void setStaccrnp12(Object what) {
		setStaccrnp12(what, false);
	}
	public void setStaccrnp12(Object what, boolean rounded) {
		if (rounded)
			staccrnp12.setRounded(what);
		else
			staccrnp12.set(what);
	}	
	public PackedDecimalData getCfwdrnp() {
		return cfwdrnp;
	}
	public void setCfwdrnp(Object what) {
		setCfwdrnp(what, false);
	}
	public void setCfwdrnp(Object what, boolean rounded) {
		if (rounded)
			cfwdrnp.setRounded(what);
		else
			cfwdrnp.set(what);
	}	
	public PackedDecimalData getBfwdspd() {
		return bfwdspd;
	}
	public void setBfwdspd(Object what) {
		setBfwdspd(what, false);
	}
	public void setBfwdspd(Object what, boolean rounded) {
		if (rounded)
			bfwdspd.setRounded(what);
		else
			bfwdspd.set(what);
	}	
	public PackedDecimalData getStaccspd01() {
		return staccspd01;
	}
	public void setStaccspd01(Object what) {
		setStaccspd01(what, false);
	}
	public void setStaccspd01(Object what, boolean rounded) {
		if (rounded)
			staccspd01.setRounded(what);
		else
			staccspd01.set(what);
	}	
	public PackedDecimalData getStaccspd02() {
		return staccspd02;
	}
	public void setStaccspd02(Object what) {
		setStaccspd02(what, false);
	}
	public void setStaccspd02(Object what, boolean rounded) {
		if (rounded)
			staccspd02.setRounded(what);
		else
			staccspd02.set(what);
	}	
	public PackedDecimalData getStaccspd03() {
		return staccspd03;
	}
	public void setStaccspd03(Object what) {
		setStaccspd03(what, false);
	}
	public void setStaccspd03(Object what, boolean rounded) {
		if (rounded)
			staccspd03.setRounded(what);
		else
			staccspd03.set(what);
	}	
	public PackedDecimalData getStaccspd04() {
		return staccspd04;
	}
	public void setStaccspd04(Object what) {
		setStaccspd04(what, false);
	}
	public void setStaccspd04(Object what, boolean rounded) {
		if (rounded)
			staccspd04.setRounded(what);
		else
			staccspd04.set(what);
	}	
	public PackedDecimalData getStaccspd05() {
		return staccspd05;
	}
	public void setStaccspd05(Object what) {
		setStaccspd05(what, false);
	}
	public void setStaccspd05(Object what, boolean rounded) {
		if (rounded)
			staccspd05.setRounded(what);
		else
			staccspd05.set(what);
	}	
	public PackedDecimalData getStaccspd06() {
		return staccspd06;
	}
	public void setStaccspd06(Object what) {
		setStaccspd06(what, false);
	}
	public void setStaccspd06(Object what, boolean rounded) {
		if (rounded)
			staccspd06.setRounded(what);
		else
			staccspd06.set(what);
	}	
	public PackedDecimalData getStaccspd07() {
		return staccspd07;
	}
	public void setStaccspd07(Object what) {
		setStaccspd07(what, false);
	}
	public void setStaccspd07(Object what, boolean rounded) {
		if (rounded)
			staccspd07.setRounded(what);
		else
			staccspd07.set(what);
	}	
	public PackedDecimalData getStaccspd08() {
		return staccspd08;
	}
	public void setStaccspd08(Object what) {
		setStaccspd08(what, false);
	}
	public void setStaccspd08(Object what, boolean rounded) {
		if (rounded)
			staccspd08.setRounded(what);
		else
			staccspd08.set(what);
	}	
	public PackedDecimalData getStaccspd09() {
		return staccspd09;
	}
	public void setStaccspd09(Object what) {
		setStaccspd09(what, false);
	}
	public void setStaccspd09(Object what, boolean rounded) {
		if (rounded)
			staccspd09.setRounded(what);
		else
			staccspd09.set(what);
	}	
	public PackedDecimalData getStaccspd10() {
		return staccspd10;
	}
	public void setStaccspd10(Object what) {
		setStaccspd10(what, false);
	}
	public void setStaccspd10(Object what, boolean rounded) {
		if (rounded)
			staccspd10.setRounded(what);
		else
			staccspd10.set(what);
	}	
	public PackedDecimalData getStaccspd11() {
		return staccspd11;
	}
	public void setStaccspd11(Object what) {
		setStaccspd11(what, false);
	}
	public void setStaccspd11(Object what, boolean rounded) {
		if (rounded)
			staccspd11.setRounded(what);
		else
			staccspd11.set(what);
	}	
	public PackedDecimalData getStaccspd12() {
		return staccspd12;
	}
	public void setStaccspd12(Object what) {
		setStaccspd12(what, false);
	}
	public void setStaccspd12(Object what, boolean rounded) {
		if (rounded)
			staccspd12.setRounded(what);
		else
			staccspd12.set(what);
	}	
	public PackedDecimalData getCfwdspd() {
		return cfwdspd;
	}
	public void setCfwdspd(Object what) {
		setCfwdspd(what, false);
	}
	public void setCfwdspd(Object what, boolean rounded) {
		if (rounded)
			cfwdspd.setRounded(what);
		else
			cfwdspd.set(what);
	}	
	public PackedDecimalData getBfwdfyc() {
		return bfwdfyc;
	}
	public void setBfwdfyc(Object what) {
		setBfwdfyc(what, false);
	}
	public void setBfwdfyc(Object what, boolean rounded) {
		if (rounded)
			bfwdfyc.setRounded(what);
		else
			bfwdfyc.set(what);
	}	
	public PackedDecimalData getStaccfyc01() {
		return staccfyc01;
	}
	public void setStaccfyc01(Object what) {
		setStaccfyc01(what, false);
	}
	public void setStaccfyc01(Object what, boolean rounded) {
		if (rounded)
			staccfyc01.setRounded(what);
		else
			staccfyc01.set(what);
	}	
	public PackedDecimalData getStaccfyc02() {
		return staccfyc02;
	}
	public void setStaccfyc02(Object what) {
		setStaccfyc02(what, false);
	}
	public void setStaccfyc02(Object what, boolean rounded) {
		if (rounded)
			staccfyc02.setRounded(what);
		else
			staccfyc02.set(what);
	}	
	public PackedDecimalData getStaccfyc03() {
		return staccfyc03;
	}
	public void setStaccfyc03(Object what) {
		setStaccfyc03(what, false);
	}
	public void setStaccfyc03(Object what, boolean rounded) {
		if (rounded)
			staccfyc03.setRounded(what);
		else
			staccfyc03.set(what);
	}	
	public PackedDecimalData getStaccfyc04() {
		return staccfyc04;
	}
	public void setStaccfyc04(Object what) {
		setStaccfyc04(what, false);
	}
	public void setStaccfyc04(Object what, boolean rounded) {
		if (rounded)
			staccfyc04.setRounded(what);
		else
			staccfyc04.set(what);
	}	
	public PackedDecimalData getStaccfyc05() {
		return staccfyc05;
	}
	public void setStaccfyc05(Object what) {
		setStaccfyc05(what, false);
	}
	public void setStaccfyc05(Object what, boolean rounded) {
		if (rounded)
			staccfyc05.setRounded(what);
		else
			staccfyc05.set(what);
	}	
	public PackedDecimalData getStaccfyc06() {
		return staccfyc06;
	}
	public void setStaccfyc06(Object what) {
		setStaccfyc06(what, false);
	}
	public void setStaccfyc06(Object what, boolean rounded) {
		if (rounded)
			staccfyc06.setRounded(what);
		else
			staccfyc06.set(what);
	}	
	public PackedDecimalData getStaccfyc07() {
		return staccfyc07;
	}
	public void setStaccfyc07(Object what) {
		setStaccfyc07(what, false);
	}
	public void setStaccfyc07(Object what, boolean rounded) {
		if (rounded)
			staccfyc07.setRounded(what);
		else
			staccfyc07.set(what);
	}	
	public PackedDecimalData getStaccfyc08() {
		return staccfyc08;
	}
	public void setStaccfyc08(Object what) {
		setStaccfyc08(what, false);
	}
	public void setStaccfyc08(Object what, boolean rounded) {
		if (rounded)
			staccfyc08.setRounded(what);
		else
			staccfyc08.set(what);
	}	
	public PackedDecimalData getStaccfyc09() {
		return staccfyc09;
	}
	public void setStaccfyc09(Object what) {
		setStaccfyc09(what, false);
	}
	public void setStaccfyc09(Object what, boolean rounded) {
		if (rounded)
			staccfyc09.setRounded(what);
		else
			staccfyc09.set(what);
	}	
	public PackedDecimalData getStaccfyc10() {
		return staccfyc10;
	}
	public void setStaccfyc10(Object what) {
		setStaccfyc10(what, false);
	}
	public void setStaccfyc10(Object what, boolean rounded) {
		if (rounded)
			staccfyc10.setRounded(what);
		else
			staccfyc10.set(what);
	}	
	public PackedDecimalData getStaccfyc11() {
		return staccfyc11;
	}
	public void setStaccfyc11(Object what) {
		setStaccfyc11(what, false);
	}
	public void setStaccfyc11(Object what, boolean rounded) {
		if (rounded)
			staccfyc11.setRounded(what);
		else
			staccfyc11.set(what);
	}	
	public PackedDecimalData getStaccfyc12() {
		return staccfyc12;
	}
	public void setStaccfyc12(Object what) {
		setStaccfyc12(what, false);
	}
	public void setStaccfyc12(Object what, boolean rounded) {
		if (rounded)
			staccfyc12.setRounded(what);
		else
			staccfyc12.set(what);
	}	
	public PackedDecimalData getCfwdfyc() {
		return cfwdfyc;
	}
	public void setCfwdfyc(Object what) {
		setCfwdfyc(what, false);
	}
	public void setCfwdfyc(Object what, boolean rounded) {
		if (rounded)
			cfwdfyc.setRounded(what);
		else
			cfwdfyc.set(what);
	}	
	public PackedDecimalData getBfwdrlc() {
		return bfwdrlc;
	}
	public void setBfwdrlc(Object what) {
		setBfwdrlc(what, false);
	}
	public void setBfwdrlc(Object what, boolean rounded) {
		if (rounded)
			bfwdrlc.setRounded(what);
		else
			bfwdrlc.set(what);
	}	
	public PackedDecimalData getStaccrlc01() {
		return staccrlc01;
	}
	public void setStaccrlc01(Object what) {
		setStaccrlc01(what, false);
	}
	public void setStaccrlc01(Object what, boolean rounded) {
		if (rounded)
			staccrlc01.setRounded(what);
		else
			staccrlc01.set(what);
	}	
	public PackedDecimalData getStaccrlc02() {
		return staccrlc02;
	}
	public void setStaccrlc02(Object what) {
		setStaccrlc02(what, false);
	}
	public void setStaccrlc02(Object what, boolean rounded) {
		if (rounded)
			staccrlc02.setRounded(what);
		else
			staccrlc02.set(what);
	}	
	public PackedDecimalData getStaccrlc03() {
		return staccrlc03;
	}
	public void setStaccrlc03(Object what) {
		setStaccrlc03(what, false);
	}
	public void setStaccrlc03(Object what, boolean rounded) {
		if (rounded)
			staccrlc03.setRounded(what);
		else
			staccrlc03.set(what);
	}	
	public PackedDecimalData getStaccrlc04() {
		return staccrlc04;
	}
	public void setStaccrlc04(Object what) {
		setStaccrlc04(what, false);
	}
	public void setStaccrlc04(Object what, boolean rounded) {
		if (rounded)
			staccrlc04.setRounded(what);
		else
			staccrlc04.set(what);
	}	
	public PackedDecimalData getStaccrlc05() {
		return staccrlc05;
	}
	public void setStaccrlc05(Object what) {
		setStaccrlc05(what, false);
	}
	public void setStaccrlc05(Object what, boolean rounded) {
		if (rounded)
			staccrlc05.setRounded(what);
		else
			staccrlc05.set(what);
	}	
	public PackedDecimalData getStaccrlc06() {
		return staccrlc06;
	}
	public void setStaccrlc06(Object what) {
		setStaccrlc06(what, false);
	}
	public void setStaccrlc06(Object what, boolean rounded) {
		if (rounded)
			staccrlc06.setRounded(what);
		else
			staccrlc06.set(what);
	}	
	public PackedDecimalData getStaccrlc07() {
		return staccrlc07;
	}
	public void setStaccrlc07(Object what) {
		setStaccrlc07(what, false);
	}
	public void setStaccrlc07(Object what, boolean rounded) {
		if (rounded)
			staccrlc07.setRounded(what);
		else
			staccrlc07.set(what);
	}	
	public PackedDecimalData getStaccrlc08() {
		return staccrlc08;
	}
	public void setStaccrlc08(Object what) {
		setStaccrlc08(what, false);
	}
	public void setStaccrlc08(Object what, boolean rounded) {
		if (rounded)
			staccrlc08.setRounded(what);
		else
			staccrlc08.set(what);
	}	
	public PackedDecimalData getStaccrlc09() {
		return staccrlc09;
	}
	public void setStaccrlc09(Object what) {
		setStaccrlc09(what, false);
	}
	public void setStaccrlc09(Object what, boolean rounded) {
		if (rounded)
			staccrlc09.setRounded(what);
		else
			staccrlc09.set(what);
	}	
	public PackedDecimalData getStaccrlc10() {
		return staccrlc10;
	}
	public void setStaccrlc10(Object what) {
		setStaccrlc10(what, false);
	}
	public void setStaccrlc10(Object what, boolean rounded) {
		if (rounded)
			staccrlc10.setRounded(what);
		else
			staccrlc10.set(what);
	}	
	public PackedDecimalData getStaccrlc11() {
		return staccrlc11;
	}
	public void setStaccrlc11(Object what) {
		setStaccrlc11(what, false);
	}
	public void setStaccrlc11(Object what, boolean rounded) {
		if (rounded)
			staccrlc11.setRounded(what);
		else
			staccrlc11.set(what);
	}	
	public PackedDecimalData getStaccrlc12() {
		return staccrlc12;
	}
	public void setStaccrlc12(Object what) {
		setStaccrlc12(what, false);
	}
	public void setStaccrlc12(Object what, boolean rounded) {
		if (rounded)
			staccrlc12.setRounded(what);
		else
			staccrlc12.set(what);
	}	
	public PackedDecimalData getCfwdrlc() {
		return cfwdrlc;
	}
	public void setCfwdrlc(Object what) {
		setCfwdrlc(what, false);
	}
	public void setCfwdrlc(Object what, boolean rounded) {
		if (rounded)
			cfwdrlc.setRounded(what);
		else
			cfwdrlc.set(what);
	}	
	public PackedDecimalData getBfwdspc() {
		return bfwdspc;
	}
	public void setBfwdspc(Object what) {
		setBfwdspc(what, false);
	}
	public void setBfwdspc(Object what, boolean rounded) {
		if (rounded)
			bfwdspc.setRounded(what);
		else
			bfwdspc.set(what);
	}	
	public PackedDecimalData getStaccspc01() {
		return staccspc01;
	}
	public void setStaccspc01(Object what) {
		setStaccspc01(what, false);
	}
	public void setStaccspc01(Object what, boolean rounded) {
		if (rounded)
			staccspc01.setRounded(what);
		else
			staccspc01.set(what);
	}	
	public PackedDecimalData getStaccspc02() {
		return staccspc02;
	}
	public void setStaccspc02(Object what) {
		setStaccspc02(what, false);
	}
	public void setStaccspc02(Object what, boolean rounded) {
		if (rounded)
			staccspc02.setRounded(what);
		else
			staccspc02.set(what);
	}	
	public PackedDecimalData getStaccspc03() {
		return staccspc03;
	}
	public void setStaccspc03(Object what) {
		setStaccspc03(what, false);
	}
	public void setStaccspc03(Object what, boolean rounded) {
		if (rounded)
			staccspc03.setRounded(what);
		else
			staccspc03.set(what);
	}	
	public PackedDecimalData getStaccspc04() {
		return staccspc04;
	}
	public void setStaccspc04(Object what) {
		setStaccspc04(what, false);
	}
	public void setStaccspc04(Object what, boolean rounded) {
		if (rounded)
			staccspc04.setRounded(what);
		else
			staccspc04.set(what);
	}	
	public PackedDecimalData getStaccspc05() {
		return staccspc05;
	}
	public void setStaccspc05(Object what) {
		setStaccspc05(what, false);
	}
	public void setStaccspc05(Object what, boolean rounded) {
		if (rounded)
			staccspc05.setRounded(what);
		else
			staccspc05.set(what);
	}	
	public PackedDecimalData getStaccspc06() {
		return staccspc06;
	}
	public void setStaccspc06(Object what) {
		setStaccspc06(what, false);
	}
	public void setStaccspc06(Object what, boolean rounded) {
		if (rounded)
			staccspc06.setRounded(what);
		else
			staccspc06.set(what);
	}	
	public PackedDecimalData getStaccspc07() {
		return staccspc07;
	}
	public void setStaccspc07(Object what) {
		setStaccspc07(what, false);
	}
	public void setStaccspc07(Object what, boolean rounded) {
		if (rounded)
			staccspc07.setRounded(what);
		else
			staccspc07.set(what);
	}	
	public PackedDecimalData getStaccspc08() {
		return staccspc08;
	}
	public void setStaccspc08(Object what) {
		setStaccspc08(what, false);
	}
	public void setStaccspc08(Object what, boolean rounded) {
		if (rounded)
			staccspc08.setRounded(what);
		else
			staccspc08.set(what);
	}	
	public PackedDecimalData getStaccspc09() {
		return staccspc09;
	}
	public void setStaccspc09(Object what) {
		setStaccspc09(what, false);
	}
	public void setStaccspc09(Object what, boolean rounded) {
		if (rounded)
			staccspc09.setRounded(what);
		else
			staccspc09.set(what);
	}	
	public PackedDecimalData getStaccspc10() {
		return staccspc10;
	}
	public void setStaccspc10(Object what) {
		setStaccspc10(what, false);
	}
	public void setStaccspc10(Object what, boolean rounded) {
		if (rounded)
			staccspc10.setRounded(what);
		else
			staccspc10.set(what);
	}	
	public PackedDecimalData getStaccspc11() {
		return staccspc11;
	}
	public void setStaccspc11(Object what) {
		setStaccspc11(what, false);
	}
	public void setStaccspc11(Object what, boolean rounded) {
		if (rounded)
			staccspc11.setRounded(what);
		else
			staccspc11.set(what);
	}	
	public PackedDecimalData getStaccspc12() {
		return staccspc12;
	}
	public void setStaccspc12(Object what) {
		setStaccspc12(what, false);
	}
	public void setStaccspc12(Object what, boolean rounded) {
		if (rounded)
			staccspc12.setRounded(what);
		else
			staccspc12.set(what);
	}	
	public PackedDecimalData getCfwdspc() {
		return cfwdspc;
	}
	public void setCfwdspc(Object what) {
		setCfwdspc(what, false);
	}
	public void setCfwdspc(Object what, boolean rounded) {
		if (rounded)
			cfwdspc.setRounded(what);
		else
			cfwdspc.set(what);
	}	
	public PackedDecimalData getBfwdrb() {
		return bfwdrb;
	}
	public void setBfwdrb(Object what) {
		setBfwdrb(what, false);
	}
	public void setBfwdrb(Object what, boolean rounded) {
		if (rounded)
			bfwdrb.setRounded(what);
		else
			bfwdrb.set(what);
	}	
	public PackedDecimalData getStaccrb01() {
		return staccrb01;
	}
	public void setStaccrb01(Object what) {
		setStaccrb01(what, false);
	}
	public void setStaccrb01(Object what, boolean rounded) {
		if (rounded)
			staccrb01.setRounded(what);
		else
			staccrb01.set(what);
	}	
	public PackedDecimalData getStaccrb02() {
		return staccrb02;
	}
	public void setStaccrb02(Object what) {
		setStaccrb02(what, false);
	}
	public void setStaccrb02(Object what, boolean rounded) {
		if (rounded)
			staccrb02.setRounded(what);
		else
			staccrb02.set(what);
	}	
	public PackedDecimalData getStaccrb03() {
		return staccrb03;
	}
	public void setStaccrb03(Object what) {
		setStaccrb03(what, false);
	}
	public void setStaccrb03(Object what, boolean rounded) {
		if (rounded)
			staccrb03.setRounded(what);
		else
			staccrb03.set(what);
	}	
	public PackedDecimalData getStaccrb04() {
		return staccrb04;
	}
	public void setStaccrb04(Object what) {
		setStaccrb04(what, false);
	}
	public void setStaccrb04(Object what, boolean rounded) {
		if (rounded)
			staccrb04.setRounded(what);
		else
			staccrb04.set(what);
	}	
	public PackedDecimalData getStaccrb05() {
		return staccrb05;
	}
	public void setStaccrb05(Object what) {
		setStaccrb05(what, false);
	}
	public void setStaccrb05(Object what, boolean rounded) {
		if (rounded)
			staccrb05.setRounded(what);
		else
			staccrb05.set(what);
	}	
	public PackedDecimalData getStaccrb06() {
		return staccrb06;
	}
	public void setStaccrb06(Object what) {
		setStaccrb06(what, false);
	}
	public void setStaccrb06(Object what, boolean rounded) {
		if (rounded)
			staccrb06.setRounded(what);
		else
			staccrb06.set(what);
	}	
	public PackedDecimalData getStaccrb07() {
		return staccrb07;
	}
	public void setStaccrb07(Object what) {
		setStaccrb07(what, false);
	}
	public void setStaccrb07(Object what, boolean rounded) {
		if (rounded)
			staccrb07.setRounded(what);
		else
			staccrb07.set(what);
	}	
	public PackedDecimalData getStaccrb08() {
		return staccrb08;
	}
	public void setStaccrb08(Object what) {
		setStaccrb08(what, false);
	}
	public void setStaccrb08(Object what, boolean rounded) {
		if (rounded)
			staccrb08.setRounded(what);
		else
			staccrb08.set(what);
	}	
	public PackedDecimalData getStaccrb09() {
		return staccrb09;
	}
	public void setStaccrb09(Object what) {
		setStaccrb09(what, false);
	}
	public void setStaccrb09(Object what, boolean rounded) {
		if (rounded)
			staccrb09.setRounded(what);
		else
			staccrb09.set(what);
	}	
	public PackedDecimalData getStaccrb10() {
		return staccrb10;
	}
	public void setStaccrb10(Object what) {
		setStaccrb10(what, false);
	}
	public void setStaccrb10(Object what, boolean rounded) {
		if (rounded)
			staccrb10.setRounded(what);
		else
			staccrb10.set(what);
	}	
	public PackedDecimalData getStaccrb11() {
		return staccrb11;
	}
	public void setStaccrb11(Object what) {
		setStaccrb11(what, false);
	}
	public void setStaccrb11(Object what, boolean rounded) {
		if (rounded)
			staccrb11.setRounded(what);
		else
			staccrb11.set(what);
	}	
	public PackedDecimalData getStaccrb12() {
		return staccrb12;
	}
	public void setStaccrb12(Object what) {
		setStaccrb12(what, false);
	}
	public void setStaccrb12(Object what, boolean rounded) {
		if (rounded)
			staccrb12.setRounded(what);
		else
			staccrb12.set(what);
	}	
	public PackedDecimalData getCfwdrb() {
		return cfwdrb;
	}
	public void setCfwdrb(Object what) {
		setCfwdrb(what, false);
	}
	public void setCfwdrb(Object what, boolean rounded) {
		if (rounded)
			cfwdrb.setRounded(what);
		else
			cfwdrb.set(what);
	}	
	public PackedDecimalData getBfwdxb() {
		return bfwdxb;
	}
	public void setBfwdxb(Object what) {
		setBfwdxb(what, false);
	}
	public void setBfwdxb(Object what, boolean rounded) {
		if (rounded)
			bfwdxb.setRounded(what);
		else
			bfwdxb.set(what);
	}	
	public PackedDecimalData getStaccxb01() {
		return staccxb01;
	}
	public void setStaccxb01(Object what) {
		setStaccxb01(what, false);
	}
	public void setStaccxb01(Object what, boolean rounded) {
		if (rounded)
			staccxb01.setRounded(what);
		else
			staccxb01.set(what);
	}	
	public PackedDecimalData getStaccxb02() {
		return staccxb02;
	}
	public void setStaccxb02(Object what) {
		setStaccxb02(what, false);
	}
	public void setStaccxb02(Object what, boolean rounded) {
		if (rounded)
			staccxb02.setRounded(what);
		else
			staccxb02.set(what);
	}	
	public PackedDecimalData getStaccxb03() {
		return staccxb03;
	}
	public void setStaccxb03(Object what) {
		setStaccxb03(what, false);
	}
	public void setStaccxb03(Object what, boolean rounded) {
		if (rounded)
			staccxb03.setRounded(what);
		else
			staccxb03.set(what);
	}	
	public PackedDecimalData getStaccxb04() {
		return staccxb04;
	}
	public void setStaccxb04(Object what) {
		setStaccxb04(what, false);
	}
	public void setStaccxb04(Object what, boolean rounded) {
		if (rounded)
			staccxb04.setRounded(what);
		else
			staccxb04.set(what);
	}	
	public PackedDecimalData getStaccxb05() {
		return staccxb05;
	}
	public void setStaccxb05(Object what) {
		setStaccxb05(what, false);
	}
	public void setStaccxb05(Object what, boolean rounded) {
		if (rounded)
			staccxb05.setRounded(what);
		else
			staccxb05.set(what);
	}	
	public PackedDecimalData getStaccxb06() {
		return staccxb06;
	}
	public void setStaccxb06(Object what) {
		setStaccxb06(what, false);
	}
	public void setStaccxb06(Object what, boolean rounded) {
		if (rounded)
			staccxb06.setRounded(what);
		else
			staccxb06.set(what);
	}	
	public PackedDecimalData getStaccxb07() {
		return staccxb07;
	}
	public void setStaccxb07(Object what) {
		setStaccxb07(what, false);
	}
	public void setStaccxb07(Object what, boolean rounded) {
		if (rounded)
			staccxb07.setRounded(what);
		else
			staccxb07.set(what);
	}	
	public PackedDecimalData getStaccxb08() {
		return staccxb08;
	}
	public void setStaccxb08(Object what) {
		setStaccxb08(what, false);
	}
	public void setStaccxb08(Object what, boolean rounded) {
		if (rounded)
			staccxb08.setRounded(what);
		else
			staccxb08.set(what);
	}	
	public PackedDecimalData getStaccxb09() {
		return staccxb09;
	}
	public void setStaccxb09(Object what) {
		setStaccxb09(what, false);
	}
	public void setStaccxb09(Object what, boolean rounded) {
		if (rounded)
			staccxb09.setRounded(what);
		else
			staccxb09.set(what);
	}	
	public PackedDecimalData getStaccxb10() {
		return staccxb10;
	}
	public void setStaccxb10(Object what) {
		setStaccxb10(what, false);
	}
	public void setStaccxb10(Object what, boolean rounded) {
		if (rounded)
			staccxb10.setRounded(what);
		else
			staccxb10.set(what);
	}	
	public PackedDecimalData getStaccxb11() {
		return staccxb11;
	}
	public void setStaccxb11(Object what) {
		setStaccxb11(what, false);
	}
	public void setStaccxb11(Object what, boolean rounded) {
		if (rounded)
			staccxb11.setRounded(what);
		else
			staccxb11.set(what);
	}	
	public PackedDecimalData getStaccxb12() {
		return staccxb12;
	}
	public void setStaccxb12(Object what) {
		setStaccxb12(what, false);
	}
	public void setStaccxb12(Object what, boolean rounded) {
		if (rounded)
			staccxb12.setRounded(what);
		else
			staccxb12.set(what);
	}	
	public PackedDecimalData getCfwdxb() {
		return cfwdxb;
	}
	public void setCfwdxb(Object what) {
		setCfwdxb(what, false);
	}
	public void setCfwdxb(Object what, boolean rounded) {
		if (rounded)
			cfwdxb.setRounded(what);
		else
			cfwdxb.set(what);
	}	
	public PackedDecimalData getBfwdtb() {
		return bfwdtb;
	}
	public void setBfwdtb(Object what) {
		setBfwdtb(what, false);
	}
	public void setBfwdtb(Object what, boolean rounded) {
		if (rounded)
			bfwdtb.setRounded(what);
		else
			bfwdtb.set(what);
	}	
	public PackedDecimalData getStacctb01() {
		return stacctb01;
	}
	public void setStacctb01(Object what) {
		setStacctb01(what, false);
	}
	public void setStacctb01(Object what, boolean rounded) {
		if (rounded)
			stacctb01.setRounded(what);
		else
			stacctb01.set(what);
	}	
	public PackedDecimalData getStacctb02() {
		return stacctb02;
	}
	public void setStacctb02(Object what) {
		setStacctb02(what, false);
	}
	public void setStacctb02(Object what, boolean rounded) {
		if (rounded)
			stacctb02.setRounded(what);
		else
			stacctb02.set(what);
	}	
	public PackedDecimalData getStacctb03() {
		return stacctb03;
	}
	public void setStacctb03(Object what) {
		setStacctb03(what, false);
	}
	public void setStacctb03(Object what, boolean rounded) {
		if (rounded)
			stacctb03.setRounded(what);
		else
			stacctb03.set(what);
	}	
	public PackedDecimalData getStacctb04() {
		return stacctb04;
	}
	public void setStacctb04(Object what) {
		setStacctb04(what, false);
	}
	public void setStacctb04(Object what, boolean rounded) {
		if (rounded)
			stacctb04.setRounded(what);
		else
			stacctb04.set(what);
	}	
	public PackedDecimalData getStacctb05() {
		return stacctb05;
	}
	public void setStacctb05(Object what) {
		setStacctb05(what, false);
	}
	public void setStacctb05(Object what, boolean rounded) {
		if (rounded)
			stacctb05.setRounded(what);
		else
			stacctb05.set(what);
	}	
	public PackedDecimalData getStacctb06() {
		return stacctb06;
	}
	public void setStacctb06(Object what) {
		setStacctb06(what, false);
	}
	public void setStacctb06(Object what, boolean rounded) {
		if (rounded)
			stacctb06.setRounded(what);
		else
			stacctb06.set(what);
	}	
	public PackedDecimalData getStacctb07() {
		return stacctb07;
	}
	public void setStacctb07(Object what) {
		setStacctb07(what, false);
	}
	public void setStacctb07(Object what, boolean rounded) {
		if (rounded)
			stacctb07.setRounded(what);
		else
			stacctb07.set(what);
	}	
	public PackedDecimalData getStacctb08() {
		return stacctb08;
	}
	public void setStacctb08(Object what) {
		setStacctb08(what, false);
	}
	public void setStacctb08(Object what, boolean rounded) {
		if (rounded)
			stacctb08.setRounded(what);
		else
			stacctb08.set(what);
	}	
	public PackedDecimalData getStacctb09() {
		return stacctb09;
	}
	public void setStacctb09(Object what) {
		setStacctb09(what, false);
	}
	public void setStacctb09(Object what, boolean rounded) {
		if (rounded)
			stacctb09.setRounded(what);
		else
			stacctb09.set(what);
	}	
	public PackedDecimalData getStacctb10() {
		return stacctb10;
	}
	public void setStacctb10(Object what) {
		setStacctb10(what, false);
	}
	public void setStacctb10(Object what, boolean rounded) {
		if (rounded)
			stacctb10.setRounded(what);
		else
			stacctb10.set(what);
	}	
	public PackedDecimalData getStacctb11() {
		return stacctb11;
	}
	public void setStacctb11(Object what) {
		setStacctb11(what, false);
	}
	public void setStacctb11(Object what, boolean rounded) {
		if (rounded)
			stacctb11.setRounded(what);
		else
			stacctb11.set(what);
	}	
	public PackedDecimalData getStacctb12() {
		return stacctb12;
	}
	public void setStacctb12(Object what) {
		setStacctb12(what, false);
	}
	public void setStacctb12(Object what, boolean rounded) {
		if (rounded)
			stacctb12.setRounded(what);
		else
			stacctb12.set(what);
	}	
	public PackedDecimalData getCfwdtb() {
		return cfwdtb;
	}
	public void setCfwdtb(Object what) {
		setCfwdtb(what, false);
	}
	public void setCfwdtb(Object what, boolean rounded) {
		if (rounded)
			cfwdtb.setRounded(what);
		else
			cfwdtb.set(what);
	}	
	public PackedDecimalData getBfwdib() {
		return bfwdib;
	}
	public void setBfwdib(Object what) {
		setBfwdib(what, false);
	}
	public void setBfwdib(Object what, boolean rounded) {
		if (rounded)
			bfwdib.setRounded(what);
		else
			bfwdib.set(what);
	}	
	public PackedDecimalData getStaccib01() {
		return staccib01;
	}
	public void setStaccib01(Object what) {
		setStaccib01(what, false);
	}
	public void setStaccib01(Object what, boolean rounded) {
		if (rounded)
			staccib01.setRounded(what);
		else
			staccib01.set(what);
	}	
	public PackedDecimalData getStaccib02() {
		return staccib02;
	}
	public void setStaccib02(Object what) {
		setStaccib02(what, false);
	}
	public void setStaccib02(Object what, boolean rounded) {
		if (rounded)
			staccib02.setRounded(what);
		else
			staccib02.set(what);
	}	
	public PackedDecimalData getStaccib03() {
		return staccib03;
	}
	public void setStaccib03(Object what) {
		setStaccib03(what, false);
	}
	public void setStaccib03(Object what, boolean rounded) {
		if (rounded)
			staccib03.setRounded(what);
		else
			staccib03.set(what);
	}	
	public PackedDecimalData getStaccib04() {
		return staccib04;
	}
	public void setStaccib04(Object what) {
		setStaccib04(what, false);
	}
	public void setStaccib04(Object what, boolean rounded) {
		if (rounded)
			staccib04.setRounded(what);
		else
			staccib04.set(what);
	}	
	public PackedDecimalData getStaccib05() {
		return staccib05;
	}
	public void setStaccib05(Object what) {
		setStaccib05(what, false);
	}
	public void setStaccib05(Object what, boolean rounded) {
		if (rounded)
			staccib05.setRounded(what);
		else
			staccib05.set(what);
	}	
	public PackedDecimalData getStaccib06() {
		return staccib06;
	}
	public void setStaccib06(Object what) {
		setStaccib06(what, false);
	}
	public void setStaccib06(Object what, boolean rounded) {
		if (rounded)
			staccib06.setRounded(what);
		else
			staccib06.set(what);
	}	
	public PackedDecimalData getStaccib07() {
		return staccib07;
	}
	public void setStaccib07(Object what) {
		setStaccib07(what, false);
	}
	public void setStaccib07(Object what, boolean rounded) {
		if (rounded)
			staccib07.setRounded(what);
		else
			staccib07.set(what);
	}	
	public PackedDecimalData getStaccib08() {
		return staccib08;
	}
	public void setStaccib08(Object what) {
		setStaccib08(what, false);
	}
	public void setStaccib08(Object what, boolean rounded) {
		if (rounded)
			staccib08.setRounded(what);
		else
			staccib08.set(what);
	}	
	public PackedDecimalData getStaccib09() {
		return staccib09;
	}
	public void setStaccib09(Object what) {
		setStaccib09(what, false);
	}
	public void setStaccib09(Object what, boolean rounded) {
		if (rounded)
			staccib09.setRounded(what);
		else
			staccib09.set(what);
	}	
	public PackedDecimalData getStaccib10() {
		return staccib10;
	}
	public void setStaccib10(Object what) {
		setStaccib10(what, false);
	}
	public void setStaccib10(Object what, boolean rounded) {
		if (rounded)
			staccib10.setRounded(what);
		else
			staccib10.set(what);
	}	
	public PackedDecimalData getStaccib11() {
		return staccib11;
	}
	public void setStaccib11(Object what) {
		setStaccib11(what, false);
	}
	public void setStaccib11(Object what, boolean rounded) {
		if (rounded)
			staccib11.setRounded(what);
		else
			staccib11.set(what);
	}	
	public PackedDecimalData getStaccib12() {
		return staccib12;
	}
	public void setStaccib12(Object what) {
		setStaccib12(what, false);
	}
	public void setStaccib12(Object what, boolean rounded) {
		if (rounded)
			staccib12.setRounded(what);
		else
			staccib12.set(what);
	}	
	public PackedDecimalData getCfwdib() {
		return cfwdib;
	}
	public void setCfwdib(Object what) {
		setCfwdib(what, false);
	}
	public void setCfwdib(Object what, boolean rounded) {
		if (rounded)
			cfwdib.setRounded(what);
		else
			cfwdib.set(what);
	}	
	public PackedDecimalData getBfwddd() {
		return bfwddd;
	}
	public void setBfwddd(Object what) {
		setBfwddd(what, false);
	}
	public void setBfwddd(Object what, boolean rounded) {
		if (rounded)
			bfwddd.setRounded(what);
		else
			bfwddd.set(what);
	}	
	public PackedDecimalData getStaccdd01() {
		return staccdd01;
	}
	public void setStaccdd01(Object what) {
		setStaccdd01(what, false);
	}
	public void setStaccdd01(Object what, boolean rounded) {
		if (rounded)
			staccdd01.setRounded(what);
		else
			staccdd01.set(what);
	}	
	public PackedDecimalData getStaccdd02() {
		return staccdd02;
	}
	public void setStaccdd02(Object what) {
		setStaccdd02(what, false);
	}
	public void setStaccdd02(Object what, boolean rounded) {
		if (rounded)
			staccdd02.setRounded(what);
		else
			staccdd02.set(what);
	}	
	public PackedDecimalData getStaccdd03() {
		return staccdd03;
	}
	public void setStaccdd03(Object what) {
		setStaccdd03(what, false);
	}
	public void setStaccdd03(Object what, boolean rounded) {
		if (rounded)
			staccdd03.setRounded(what);
		else
			staccdd03.set(what);
	}	
	public PackedDecimalData getStaccdd04() {
		return staccdd04;
	}
	public void setStaccdd04(Object what) {
		setStaccdd04(what, false);
	}
	public void setStaccdd04(Object what, boolean rounded) {
		if (rounded)
			staccdd04.setRounded(what);
		else
			staccdd04.set(what);
	}	
	public PackedDecimalData getStaccdd05() {
		return staccdd05;
	}
	public void setStaccdd05(Object what) {
		setStaccdd05(what, false);
	}
	public void setStaccdd05(Object what, boolean rounded) {
		if (rounded)
			staccdd05.setRounded(what);
		else
			staccdd05.set(what);
	}	
	public PackedDecimalData getStaccdd06() {
		return staccdd06;
	}
	public void setStaccdd06(Object what) {
		setStaccdd06(what, false);
	}
	public void setStaccdd06(Object what, boolean rounded) {
		if (rounded)
			staccdd06.setRounded(what);
		else
			staccdd06.set(what);
	}	
	public PackedDecimalData getStaccdd07() {
		return staccdd07;
	}
	public void setStaccdd07(Object what) {
		setStaccdd07(what, false);
	}
	public void setStaccdd07(Object what, boolean rounded) {
		if (rounded)
			staccdd07.setRounded(what);
		else
			staccdd07.set(what);
	}	
	public PackedDecimalData getStaccdd08() {
		return staccdd08;
	}
	public void setStaccdd08(Object what) {
		setStaccdd08(what, false);
	}
	public void setStaccdd08(Object what, boolean rounded) {
		if (rounded)
			staccdd08.setRounded(what);
		else
			staccdd08.set(what);
	}	
	public PackedDecimalData getStaccdd09() {
		return staccdd09;
	}
	public void setStaccdd09(Object what) {
		setStaccdd09(what, false);
	}
	public void setStaccdd09(Object what, boolean rounded) {
		if (rounded)
			staccdd09.setRounded(what);
		else
			staccdd09.set(what);
	}	
	public PackedDecimalData getStaccdd10() {
		return staccdd10;
	}
	public void setStaccdd10(Object what) {
		setStaccdd10(what, false);
	}
	public void setStaccdd10(Object what, boolean rounded) {
		if (rounded)
			staccdd10.setRounded(what);
		else
			staccdd10.set(what);
	}	
	public PackedDecimalData getStaccdd11() {
		return staccdd11;
	}
	public void setStaccdd11(Object what) {
		setStaccdd11(what, false);
	}
	public void setStaccdd11(Object what, boolean rounded) {
		if (rounded)
			staccdd11.setRounded(what);
		else
			staccdd11.set(what);
	}	
	public PackedDecimalData getStaccdd12() {
		return staccdd12;
	}
	public void setStaccdd12(Object what) {
		setStaccdd12(what, false);
	}
	public void setStaccdd12(Object what, boolean rounded) {
		if (rounded)
			staccdd12.setRounded(what);
		else
			staccdd12.set(what);
	}	
	public PackedDecimalData getCfwddd() {
		return cfwddd;
	}
	public void setCfwddd(Object what) {
		setCfwddd(what, false);
	}
	public void setCfwddd(Object what, boolean rounded) {
		if (rounded)
			cfwddd.setRounded(what);
		else
			cfwddd.set(what);
	}	
	public PackedDecimalData getBfwddi() {
		return bfwddi;
	}
	public void setBfwddi(Object what) {
		setBfwddi(what, false);
	}
	public void setBfwddi(Object what, boolean rounded) {
		if (rounded)
			bfwddi.setRounded(what);
		else
			bfwddi.set(what);
	}	
	public PackedDecimalData getStaccdi01() {
		return staccdi01;
	}
	public void setStaccdi01(Object what) {
		setStaccdi01(what, false);
	}
	public void setStaccdi01(Object what, boolean rounded) {
		if (rounded)
			staccdi01.setRounded(what);
		else
			staccdi01.set(what);
	}	
	public PackedDecimalData getStaccdi02() {
		return staccdi02;
	}
	public void setStaccdi02(Object what) {
		setStaccdi02(what, false);
	}
	public void setStaccdi02(Object what, boolean rounded) {
		if (rounded)
			staccdi02.setRounded(what);
		else
			staccdi02.set(what);
	}	
	public PackedDecimalData getStaccdi03() {
		return staccdi03;
	}
	public void setStaccdi03(Object what) {
		setStaccdi03(what, false);
	}
	public void setStaccdi03(Object what, boolean rounded) {
		if (rounded)
			staccdi03.setRounded(what);
		else
			staccdi03.set(what);
	}	
	public PackedDecimalData getStaccdi04() {
		return staccdi04;
	}
	public void setStaccdi04(Object what) {
		setStaccdi04(what, false);
	}
	public void setStaccdi04(Object what, boolean rounded) {
		if (rounded)
			staccdi04.setRounded(what);
		else
			staccdi04.set(what);
	}	
	public PackedDecimalData getStaccdi05() {
		return staccdi05;
	}
	public void setStaccdi05(Object what) {
		setStaccdi05(what, false);
	}
	public void setStaccdi05(Object what, boolean rounded) {
		if (rounded)
			staccdi05.setRounded(what);
		else
			staccdi05.set(what);
	}	
	public PackedDecimalData getStaccdi06() {
		return staccdi06;
	}
	public void setStaccdi06(Object what) {
		setStaccdi06(what, false);
	}
	public void setStaccdi06(Object what, boolean rounded) {
		if (rounded)
			staccdi06.setRounded(what);
		else
			staccdi06.set(what);
	}	
	public PackedDecimalData getStaccdi07() {
		return staccdi07;
	}
	public void setStaccdi07(Object what) {
		setStaccdi07(what, false);
	}
	public void setStaccdi07(Object what, boolean rounded) {
		if (rounded)
			staccdi07.setRounded(what);
		else
			staccdi07.set(what);
	}	
	public PackedDecimalData getStaccdi08() {
		return staccdi08;
	}
	public void setStaccdi08(Object what) {
		setStaccdi08(what, false);
	}
	public void setStaccdi08(Object what, boolean rounded) {
		if (rounded)
			staccdi08.setRounded(what);
		else
			staccdi08.set(what);
	}	
	public PackedDecimalData getStaccdi09() {
		return staccdi09;
	}
	public void setStaccdi09(Object what) {
		setStaccdi09(what, false);
	}
	public void setStaccdi09(Object what, boolean rounded) {
		if (rounded)
			staccdi09.setRounded(what);
		else
			staccdi09.set(what);
	}	
	public PackedDecimalData getStaccdi10() {
		return staccdi10;
	}
	public void setStaccdi10(Object what) {
		setStaccdi10(what, false);
	}
	public void setStaccdi10(Object what, boolean rounded) {
		if (rounded)
			staccdi10.setRounded(what);
		else
			staccdi10.set(what);
	}	
	public PackedDecimalData getStaccdi11() {
		return staccdi11;
	}
	public void setStaccdi11(Object what) {
		setStaccdi11(what, false);
	}
	public void setStaccdi11(Object what, boolean rounded) {
		if (rounded)
			staccdi11.setRounded(what);
		else
			staccdi11.set(what);
	}	
	public PackedDecimalData getStaccdi12() {
		return staccdi12;
	}
	public void setStaccdi12(Object what) {
		setStaccdi12(what, false);
	}
	public void setStaccdi12(Object what, boolean rounded) {
		if (rounded)
			staccdi12.setRounded(what);
		else
			staccdi12.set(what);
	}	
	public PackedDecimalData getCfwddi() {
		return cfwddi;
	}
	public void setCfwddi(Object what) {
		setCfwddi(what, false);
	}
	public void setCfwddi(Object what, boolean rounded) {
		if (rounded)
			cfwddi.setRounded(what);
		else
			cfwddi.set(what);
	}	
	public PackedDecimalData getBfwdbs() {
		return bfwdbs;
	}
	public void setBfwdbs(Object what) {
		setBfwdbs(what, false);
	}
	public void setBfwdbs(Object what, boolean rounded) {
		if (rounded)
			bfwdbs.setRounded(what);
		else
			bfwdbs.set(what);
	}	
	public PackedDecimalData getStaccbs01() {
		return staccbs01;
	}
	public void setStaccbs01(Object what) {
		setStaccbs01(what, false);
	}
	public void setStaccbs01(Object what, boolean rounded) {
		if (rounded)
			staccbs01.setRounded(what);
		else
			staccbs01.set(what);
	}	
	public PackedDecimalData getStaccbs02() {
		return staccbs02;
	}
	public void setStaccbs02(Object what) {
		setStaccbs02(what, false);
	}
	public void setStaccbs02(Object what, boolean rounded) {
		if (rounded)
			staccbs02.setRounded(what);
		else
			staccbs02.set(what);
	}	
	public PackedDecimalData getStaccbs03() {
		return staccbs03;
	}
	public void setStaccbs03(Object what) {
		setStaccbs03(what, false);
	}
	public void setStaccbs03(Object what, boolean rounded) {
		if (rounded)
			staccbs03.setRounded(what);
		else
			staccbs03.set(what);
	}	
	public PackedDecimalData getStaccbs04() {
		return staccbs04;
	}
	public void setStaccbs04(Object what) {
		setStaccbs04(what, false);
	}
	public void setStaccbs04(Object what, boolean rounded) {
		if (rounded)
			staccbs04.setRounded(what);
		else
			staccbs04.set(what);
	}	
	public PackedDecimalData getStaccbs05() {
		return staccbs05;
	}
	public void setStaccbs05(Object what) {
		setStaccbs05(what, false);
	}
	public void setStaccbs05(Object what, boolean rounded) {
		if (rounded)
			staccbs05.setRounded(what);
		else
			staccbs05.set(what);
	}	
	public PackedDecimalData getStaccbs06() {
		return staccbs06;
	}
	public void setStaccbs06(Object what) {
		setStaccbs06(what, false);
	}
	public void setStaccbs06(Object what, boolean rounded) {
		if (rounded)
			staccbs06.setRounded(what);
		else
			staccbs06.set(what);
	}	
	public PackedDecimalData getStaccbs07() {
		return staccbs07;
	}
	public void setStaccbs07(Object what) {
		setStaccbs07(what, false);
	}
	public void setStaccbs07(Object what, boolean rounded) {
		if (rounded)
			staccbs07.setRounded(what);
		else
			staccbs07.set(what);
	}	
	public PackedDecimalData getStaccbs08() {
		return staccbs08;
	}
	public void setStaccbs08(Object what) {
		setStaccbs08(what, false);
	}
	public void setStaccbs08(Object what, boolean rounded) {
		if (rounded)
			staccbs08.setRounded(what);
		else
			staccbs08.set(what);
	}	
	public PackedDecimalData getStaccbs09() {
		return staccbs09;
	}
	public void setStaccbs09(Object what) {
		setStaccbs09(what, false);
	}
	public void setStaccbs09(Object what, boolean rounded) {
		if (rounded)
			staccbs09.setRounded(what);
		else
			staccbs09.set(what);
	}	
	public PackedDecimalData getStaccbs10() {
		return staccbs10;
	}
	public void setStaccbs10(Object what) {
		setStaccbs10(what, false);
	}
	public void setStaccbs10(Object what, boolean rounded) {
		if (rounded)
			staccbs10.setRounded(what);
		else
			staccbs10.set(what);
	}	
	public PackedDecimalData getStaccbs11() {
		return staccbs11;
	}
	public void setStaccbs11(Object what) {
		setStaccbs11(what, false);
	}
	public void setStaccbs11(Object what, boolean rounded) {
		if (rounded)
			staccbs11.setRounded(what);
		else
			staccbs11.set(what);
	}	
	public PackedDecimalData getStaccbs12() {
		return staccbs12;
	}
	public void setStaccbs12(Object what) {
		setStaccbs12(what, false);
	}
	public void setStaccbs12(Object what, boolean rounded) {
		if (rounded)
			staccbs12.setRounded(what);
		else
			staccbs12.set(what);
	}	
	public PackedDecimalData getCfwdbs() {
		return cfwdbs;
	}
	public void setCfwdbs(Object what) {
		setCfwdbs(what, false);
	}
	public void setCfwdbs(Object what, boolean rounded) {
		if (rounded)
			cfwdbs.setRounded(what);
		else
			cfwdbs.set(what);
	}	
	public PackedDecimalData getBfwdob() {
		return bfwdob;
	}
	public void setBfwdob(Object what) {
		setBfwdob(what, false);
	}
	public void setBfwdob(Object what, boolean rounded) {
		if (rounded)
			bfwdob.setRounded(what);
		else
			bfwdob.set(what);
	}	
	public PackedDecimalData getStaccob01() {
		return staccob01;
	}
	public void setStaccob01(Object what) {
		setStaccob01(what, false);
	}
	public void setStaccob01(Object what, boolean rounded) {
		if (rounded)
			staccob01.setRounded(what);
		else
			staccob01.set(what);
	}	
	public PackedDecimalData getStaccob02() {
		return staccob02;
	}
	public void setStaccob02(Object what) {
		setStaccob02(what, false);
	}
	public void setStaccob02(Object what, boolean rounded) {
		if (rounded)
			staccob02.setRounded(what);
		else
			staccob02.set(what);
	}	
	public PackedDecimalData getStaccob03() {
		return staccob03;
	}
	public void setStaccob03(Object what) {
		setStaccob03(what, false);
	}
	public void setStaccob03(Object what, boolean rounded) {
		if (rounded)
			staccob03.setRounded(what);
		else
			staccob03.set(what);
	}	
	public PackedDecimalData getStaccob04() {
		return staccob04;
	}
	public void setStaccob04(Object what) {
		setStaccob04(what, false);
	}
	public void setStaccob04(Object what, boolean rounded) {
		if (rounded)
			staccob04.setRounded(what);
		else
			staccob04.set(what);
	}	
	public PackedDecimalData getStaccob05() {
		return staccob05;
	}
	public void setStaccob05(Object what) {
		setStaccob05(what, false);
	}
	public void setStaccob05(Object what, boolean rounded) {
		if (rounded)
			staccob05.setRounded(what);
		else
			staccob05.set(what);
	}	
	public PackedDecimalData getStaccob06() {
		return staccob06;
	}
	public void setStaccob06(Object what) {
		setStaccob06(what, false);
	}
	public void setStaccob06(Object what, boolean rounded) {
		if (rounded)
			staccob06.setRounded(what);
		else
			staccob06.set(what);
	}	
	public PackedDecimalData getStaccob07() {
		return staccob07;
	}
	public void setStaccob07(Object what) {
		setStaccob07(what, false);
	}
	public void setStaccob07(Object what, boolean rounded) {
		if (rounded)
			staccob07.setRounded(what);
		else
			staccob07.set(what);
	}	
	public PackedDecimalData getStaccob08() {
		return staccob08;
	}
	public void setStaccob08(Object what) {
		setStaccob08(what, false);
	}
	public void setStaccob08(Object what, boolean rounded) {
		if (rounded)
			staccob08.setRounded(what);
		else
			staccob08.set(what);
	}	
	public PackedDecimalData getStaccob09() {
		return staccob09;
	}
	public void setStaccob09(Object what) {
		setStaccob09(what, false);
	}
	public void setStaccob09(Object what, boolean rounded) {
		if (rounded)
			staccob09.setRounded(what);
		else
			staccob09.set(what);
	}	
	public PackedDecimalData getStaccob10() {
		return staccob10;
	}
	public void setStaccob10(Object what) {
		setStaccob10(what, false);
	}
	public void setStaccob10(Object what, boolean rounded) {
		if (rounded)
			staccob10.setRounded(what);
		else
			staccob10.set(what);
	}	
	public PackedDecimalData getStaccob11() {
		return staccob11;
	}
	public void setStaccob11(Object what) {
		setStaccob11(what, false);
	}
	public void setStaccob11(Object what, boolean rounded) {
		if (rounded)
			staccob11.setRounded(what);
		else
			staccob11.set(what);
	}	
	public PackedDecimalData getStaccob12() {
		return staccob12;
	}
	public void setStaccob12(Object what) {
		setStaccob12(what, false);
	}
	public void setStaccob12(Object what, boolean rounded) {
		if (rounded)
			staccob12.setRounded(what);
		else
			staccob12.set(what);
	}	
	public PackedDecimalData getCfwdob() {
		return cfwdob;
	}
	public void setCfwdob(Object what) {
		setCfwdob(what, false);
	}
	public void setCfwdob(Object what, boolean rounded) {
		if (rounded)
			cfwdob.setRounded(what);
		else
			cfwdob.set(what);
	}	
	public PackedDecimalData getBfwdmtb() {
		return bfwdmtb;
	}
	public void setBfwdmtb(Object what) {
		setBfwdmtb(what, false);
	}
	public void setBfwdmtb(Object what, boolean rounded) {
		if (rounded)
			bfwdmtb.setRounded(what);
		else
			bfwdmtb.set(what);
	}	
	public PackedDecimalData getStaccmtb01() {
		return staccmtb01;
	}
	public void setStaccmtb01(Object what) {
		setStaccmtb01(what, false);
	}
	public void setStaccmtb01(Object what, boolean rounded) {
		if (rounded)
			staccmtb01.setRounded(what);
		else
			staccmtb01.set(what);
	}	
	public PackedDecimalData getStaccmtb02() {
		return staccmtb02;
	}
	public void setStaccmtb02(Object what) {
		setStaccmtb02(what, false);
	}
	public void setStaccmtb02(Object what, boolean rounded) {
		if (rounded)
			staccmtb02.setRounded(what);
		else
			staccmtb02.set(what);
	}	
	public PackedDecimalData getStaccmtb03() {
		return staccmtb03;
	}
	public void setStaccmtb03(Object what) {
		setStaccmtb03(what, false);
	}
	public void setStaccmtb03(Object what, boolean rounded) {
		if (rounded)
			staccmtb03.setRounded(what);
		else
			staccmtb03.set(what);
	}	
	public PackedDecimalData getStaccmtb04() {
		return staccmtb04;
	}
	public void setStaccmtb04(Object what) {
		setStaccmtb04(what, false);
	}
	public void setStaccmtb04(Object what, boolean rounded) {
		if (rounded)
			staccmtb04.setRounded(what);
		else
			staccmtb04.set(what);
	}	
	public PackedDecimalData getStaccmtb05() {
		return staccmtb05;
	}
	public void setStaccmtb05(Object what) {
		setStaccmtb05(what, false);
	}
	public void setStaccmtb05(Object what, boolean rounded) {
		if (rounded)
			staccmtb05.setRounded(what);
		else
			staccmtb05.set(what);
	}	
	public PackedDecimalData getStaccmtb06() {
		return staccmtb06;
	}
	public void setStaccmtb06(Object what) {
		setStaccmtb06(what, false);
	}
	public void setStaccmtb06(Object what, boolean rounded) {
		if (rounded)
			staccmtb06.setRounded(what);
		else
			staccmtb06.set(what);
	}	
	public PackedDecimalData getStaccmtb07() {
		return staccmtb07;
	}
	public void setStaccmtb07(Object what) {
		setStaccmtb07(what, false);
	}
	public void setStaccmtb07(Object what, boolean rounded) {
		if (rounded)
			staccmtb07.setRounded(what);
		else
			staccmtb07.set(what);
	}	
	public PackedDecimalData getStaccmtb08() {
		return staccmtb08;
	}
	public void setStaccmtb08(Object what) {
		setStaccmtb08(what, false);
	}
	public void setStaccmtb08(Object what, boolean rounded) {
		if (rounded)
			staccmtb08.setRounded(what);
		else
			staccmtb08.set(what);
	}	
	public PackedDecimalData getStaccmtb09() {
		return staccmtb09;
	}
	public void setStaccmtb09(Object what) {
		setStaccmtb09(what, false);
	}
	public void setStaccmtb09(Object what, boolean rounded) {
		if (rounded)
			staccmtb09.setRounded(what);
		else
			staccmtb09.set(what);
	}	
	public PackedDecimalData getStaccmtb10() {
		return staccmtb10;
	}
	public void setStaccmtb10(Object what) {
		setStaccmtb10(what, false);
	}
	public void setStaccmtb10(Object what, boolean rounded) {
		if (rounded)
			staccmtb10.setRounded(what);
		else
			staccmtb10.set(what);
	}	
	public PackedDecimalData getStaccmtb11() {
		return staccmtb11;
	}
	public void setStaccmtb11(Object what) {
		setStaccmtb11(what, false);
	}
	public void setStaccmtb11(Object what, boolean rounded) {
		if (rounded)
			staccmtb11.setRounded(what);
		else
			staccmtb11.set(what);
	}	
	public PackedDecimalData getStaccmtb12() {
		return staccmtb12;
	}
	public void setStaccmtb12(Object what) {
		setStaccmtb12(what, false);
	}
	public void setStaccmtb12(Object what, boolean rounded) {
		if (rounded)
			staccmtb12.setRounded(what);
		else
			staccmtb12.set(what);
	}	
	public PackedDecimalData getCfwdmtb() {
		return cfwdmtb;
	}
	public void setCfwdmtb(Object what) {
		setCfwdmtb(what, false);
	}
	public void setCfwdmtb(Object what, boolean rounded) {
		if (rounded)
			cfwdmtb.setRounded(what);
		else
			cfwdmtb.set(what);
	}	
	public PackedDecimalData getBfwdclr() {
		return bfwdclr;
	}
	public void setBfwdclr(Object what) {
		setBfwdclr(what, false);
	}
	public void setBfwdclr(Object what, boolean rounded) {
		if (rounded)
			bfwdclr.setRounded(what);
		else
			bfwdclr.set(what);
	}	
	public PackedDecimalData getStaccclr01() {
		return staccclr01;
	}
	public void setStaccclr01(Object what) {
		setStaccclr01(what, false);
	}
	public void setStaccclr01(Object what, boolean rounded) {
		if (rounded)
			staccclr01.setRounded(what);
		else
			staccclr01.set(what);
	}	
	public PackedDecimalData getStaccclr02() {
		return staccclr02;
	}
	public void setStaccclr02(Object what) {
		setStaccclr02(what, false);
	}
	public void setStaccclr02(Object what, boolean rounded) {
		if (rounded)
			staccclr02.setRounded(what);
		else
			staccclr02.set(what);
	}	
	public PackedDecimalData getStaccclr03() {
		return staccclr03;
	}
	public void setStaccclr03(Object what) {
		setStaccclr03(what, false);
	}
	public void setStaccclr03(Object what, boolean rounded) {
		if (rounded)
			staccclr03.setRounded(what);
		else
			staccclr03.set(what);
	}	
	public PackedDecimalData getStaccclr04() {
		return staccclr04;
	}
	public void setStaccclr04(Object what) {
		setStaccclr04(what, false);
	}
	public void setStaccclr04(Object what, boolean rounded) {
		if (rounded)
			staccclr04.setRounded(what);
		else
			staccclr04.set(what);
	}	
	public PackedDecimalData getStaccclr05() {
		return staccclr05;
	}
	public void setStaccclr05(Object what) {
		setStaccclr05(what, false);
	}
	public void setStaccclr05(Object what, boolean rounded) {
		if (rounded)
			staccclr05.setRounded(what);
		else
			staccclr05.set(what);
	}	
	public PackedDecimalData getStaccclr06() {
		return staccclr06;
	}
	public void setStaccclr06(Object what) {
		setStaccclr06(what, false);
	}
	public void setStaccclr06(Object what, boolean rounded) {
		if (rounded)
			staccclr06.setRounded(what);
		else
			staccclr06.set(what);
	}	
	public PackedDecimalData getStaccclr07() {
		return staccclr07;
	}
	public void setStaccclr07(Object what) {
		setStaccclr07(what, false);
	}
	public void setStaccclr07(Object what, boolean rounded) {
		if (rounded)
			staccclr07.setRounded(what);
		else
			staccclr07.set(what);
	}	
	public PackedDecimalData getStaccclr08() {
		return staccclr08;
	}
	public void setStaccclr08(Object what) {
		setStaccclr08(what, false);
	}
	public void setStaccclr08(Object what, boolean rounded) {
		if (rounded)
			staccclr08.setRounded(what);
		else
			staccclr08.set(what);
	}	
	public PackedDecimalData getStaccclr09() {
		return staccclr09;
	}
	public void setStaccclr09(Object what) {
		setStaccclr09(what, false);
	}
	public void setStaccclr09(Object what, boolean rounded) {
		if (rounded)
			staccclr09.setRounded(what);
		else
			staccclr09.set(what);
	}	
	public PackedDecimalData getStaccclr10() {
		return staccclr10;
	}
	public void setStaccclr10(Object what) {
		setStaccclr10(what, false);
	}
	public void setStaccclr10(Object what, boolean rounded) {
		if (rounded)
			staccclr10.setRounded(what);
		else
			staccclr10.set(what);
	}	
	public PackedDecimalData getStaccclr11() {
		return staccclr11;
	}
	public void setStaccclr11(Object what) {
		setStaccclr11(what, false);
	}
	public void setStaccclr11(Object what, boolean rounded) {
		if (rounded)
			staccclr11.setRounded(what);
		else
			staccclr11.set(what);
	}	
	public PackedDecimalData getStaccclr12() {
		return staccclr12;
	}
	public void setStaccclr12(Object what) {
		setStaccclr12(what, false);
	}
	public void setStaccclr12(Object what, boolean rounded) {
		if (rounded)
			staccclr12.setRounded(what);
		else
			staccclr12.set(what);
	}	
	public PackedDecimalData getCfwdclr() {
		return cfwdclr;
	}
	public void setCfwdclr(Object what) {
		setCfwdclr(what, false);
	}
	public void setCfwdclr(Object what, boolean rounded) {
		if (rounded)
			cfwdclr.setRounded(what);
		else
			cfwdclr.set(what);
	}	
	public PackedDecimalData getBfwdrip() {
		return bfwdrip;
	}
	public void setBfwdrip(Object what) {
		setBfwdrip(what, false);
	}
	public void setBfwdrip(Object what, boolean rounded) {
		if (rounded)
			bfwdrip.setRounded(what);
		else
			bfwdrip.set(what);
	}	
	public PackedDecimalData getStaccrip01() {
		return staccrip01;
	}
	public void setStaccrip01(Object what) {
		setStaccrip01(what, false);
	}
	public void setStaccrip01(Object what, boolean rounded) {
		if (rounded)
			staccrip01.setRounded(what);
		else
			staccrip01.set(what);
	}	
	public PackedDecimalData getStaccrip02() {
		return staccrip02;
	}
	public void setStaccrip02(Object what) {
		setStaccrip02(what, false);
	}
	public void setStaccrip02(Object what, boolean rounded) {
		if (rounded)
			staccrip02.setRounded(what);
		else
			staccrip02.set(what);
	}	
	public PackedDecimalData getStaccrip03() {
		return staccrip03;
	}
	public void setStaccrip03(Object what) {
		setStaccrip03(what, false);
	}
	public void setStaccrip03(Object what, boolean rounded) {
		if (rounded)
			staccrip03.setRounded(what);
		else
			staccrip03.set(what);
	}	
	public PackedDecimalData getStaccrip04() {
		return staccrip04;
	}
	public void setStaccrip04(Object what) {
		setStaccrip04(what, false);
	}
	public void setStaccrip04(Object what, boolean rounded) {
		if (rounded)
			staccrip04.setRounded(what);
		else
			staccrip04.set(what);
	}	
	public PackedDecimalData getStaccrip05() {
		return staccrip05;
	}
	public void setStaccrip05(Object what) {
		setStaccrip05(what, false);
	}
	public void setStaccrip05(Object what, boolean rounded) {
		if (rounded)
			staccrip05.setRounded(what);
		else
			staccrip05.set(what);
	}	
	public PackedDecimalData getStaccrip06() {
		return staccrip06;
	}
	public void setStaccrip06(Object what) {
		setStaccrip06(what, false);
	}
	public void setStaccrip06(Object what, boolean rounded) {
		if (rounded)
			staccrip06.setRounded(what);
		else
			staccrip06.set(what);
	}	
	public PackedDecimalData getStaccrip07() {
		return staccrip07;
	}
	public void setStaccrip07(Object what) {
		setStaccrip07(what, false);
	}
	public void setStaccrip07(Object what, boolean rounded) {
		if (rounded)
			staccrip07.setRounded(what);
		else
			staccrip07.set(what);
	}	
	public PackedDecimalData getStaccrip08() {
		return staccrip08;
	}
	public void setStaccrip08(Object what) {
		setStaccrip08(what, false);
	}
	public void setStaccrip08(Object what, boolean rounded) {
		if (rounded)
			staccrip08.setRounded(what);
		else
			staccrip08.set(what);
	}	
	public PackedDecimalData getStaccrip09() {
		return staccrip09;
	}
	public void setStaccrip09(Object what) {
		setStaccrip09(what, false);
	}
	public void setStaccrip09(Object what, boolean rounded) {
		if (rounded)
			staccrip09.setRounded(what);
		else
			staccrip09.set(what);
	}	
	public PackedDecimalData getStaccrip10() {
		return staccrip10;
	}
	public void setStaccrip10(Object what) {
		setStaccrip10(what, false);
	}
	public void setStaccrip10(Object what, boolean rounded) {
		if (rounded)
			staccrip10.setRounded(what);
		else
			staccrip10.set(what);
	}	
	public PackedDecimalData getStaccrip11() {
		return staccrip11;
	}
	public void setStaccrip11(Object what) {
		setStaccrip11(what, false);
	}
	public void setStaccrip11(Object what, boolean rounded) {
		if (rounded)
			staccrip11.setRounded(what);
		else
			staccrip11.set(what);
	}	
	public PackedDecimalData getStaccrip12() {
		return staccrip12;
	}
	public void setStaccrip12(Object what) {
		setStaccrip12(what, false);
	}
	public void setStaccrip12(Object what, boolean rounded) {
		if (rounded)
			staccrip12.setRounded(what);
		else
			staccrip12.set(what);
	}	
	public PackedDecimalData getCfwdrip() {
		return cfwdrip;
	}
	public void setCfwdrip(Object what) {
		setCfwdrip(what, false);
	}
	public void setCfwdrip(Object what, boolean rounded) {
		if (rounded)
			cfwdrip.setRounded(what);
		else
			cfwdrip.set(what);
	}	
	public PackedDecimalData getBfwdric() {
		return bfwdric;
	}
	public void setBfwdric(Object what) {
		setBfwdric(what, false);
	}
	public void setBfwdric(Object what, boolean rounded) {
		if (rounded)
			bfwdric.setRounded(what);
		else
			bfwdric.set(what);
	}	
	public PackedDecimalData getStaccric01() {
		return staccric01;
	}
	public void setStaccric01(Object what) {
		setStaccric01(what, false);
	}
	public void setStaccric01(Object what, boolean rounded) {
		if (rounded)
			staccric01.setRounded(what);
		else
			staccric01.set(what);
	}	
	public PackedDecimalData getStaccric02() {
		return staccric02;
	}
	public void setStaccric02(Object what) {
		setStaccric02(what, false);
	}
	public void setStaccric02(Object what, boolean rounded) {
		if (rounded)
			staccric02.setRounded(what);
		else
			staccric02.set(what);
	}	
	public PackedDecimalData getStaccric03() {
		return staccric03;
	}
	public void setStaccric03(Object what) {
		setStaccric03(what, false);
	}
	public void setStaccric03(Object what, boolean rounded) {
		if (rounded)
			staccric03.setRounded(what);
		else
			staccric03.set(what);
	}	
	public PackedDecimalData getStaccric04() {
		return staccric04;
	}
	public void setStaccric04(Object what) {
		setStaccric04(what, false);
	}
	public void setStaccric04(Object what, boolean rounded) {
		if (rounded)
			staccric04.setRounded(what);
		else
			staccric04.set(what);
	}	
	public PackedDecimalData getStaccric05() {
		return staccric05;
	}
	public void setStaccric05(Object what) {
		setStaccric05(what, false);
	}
	public void setStaccric05(Object what, boolean rounded) {
		if (rounded)
			staccric05.setRounded(what);
		else
			staccric05.set(what);
	}	
	public PackedDecimalData getStaccric06() {
		return staccric06;
	}
	public void setStaccric06(Object what) {
		setStaccric06(what, false);
	}
	public void setStaccric06(Object what, boolean rounded) {
		if (rounded)
			staccric06.setRounded(what);
		else
			staccric06.set(what);
	}	
	public PackedDecimalData getStaccric07() {
		return staccric07;
	}
	public void setStaccric07(Object what) {
		setStaccric07(what, false);
	}
	public void setStaccric07(Object what, boolean rounded) {
		if (rounded)
			staccric07.setRounded(what);
		else
			staccric07.set(what);
	}	
	public PackedDecimalData getStaccric08() {
		return staccric08;
	}
	public void setStaccric08(Object what) {
		setStaccric08(what, false);
	}
	public void setStaccric08(Object what, boolean rounded) {
		if (rounded)
			staccric08.setRounded(what);
		else
			staccric08.set(what);
	}	
	public PackedDecimalData getStaccric09() {
		return staccric09;
	}
	public void setStaccric09(Object what) {
		setStaccric09(what, false);
	}
	public void setStaccric09(Object what, boolean rounded) {
		if (rounded)
			staccric09.setRounded(what);
		else
			staccric09.set(what);
	}	
	public PackedDecimalData getStaccric10() {
		return staccric10;
	}
	public void setStaccric10(Object what) {
		setStaccric10(what, false);
	}
	public void setStaccric10(Object what, boolean rounded) {
		if (rounded)
			staccric10.setRounded(what);
		else
			staccric10.set(what);
	}	
	public PackedDecimalData getStaccric11() {
		return staccric11;
	}
	public void setStaccric11(Object what) {
		setStaccric11(what, false);
	}
	public void setStaccric11(Object what, boolean rounded) {
		if (rounded)
			staccric11.setRounded(what);
		else
			staccric11.set(what);
	}	
	public PackedDecimalData getStaccric12() {
		return staccric12;
	}
	public void setStaccric12(Object what) {
		setStaccric12(what, false);
	}
	public void setStaccric12(Object what, boolean rounded) {
		if (rounded)
			staccric12.setRounded(what);
		else
			staccric12.set(what);
	}	
	public PackedDecimalData getCfwdric() {
		return cfwdric;
	}
	public void setCfwdric(Object what) {
		setCfwdric(what, false);
	}
	public void setCfwdric(Object what, boolean rounded) {
		if (rounded)
			cfwdric.setRounded(what);
		else
			cfwdric.set(what);
	}	
	public PackedDecimalData getBfwdadv() {
		return bfwdadv;
	}
	public void setBfwdadv(Object what) {
		setBfwdadv(what, false);
	}
	public void setBfwdadv(Object what, boolean rounded) {
		if (rounded)
			bfwdadv.setRounded(what);
		else
			bfwdadv.set(what);
	}	
	public PackedDecimalData getStaccadv01() {
		return staccadv01;
	}
	public void setStaccadv01(Object what) {
		setStaccadv01(what, false);
	}
	public void setStaccadv01(Object what, boolean rounded) {
		if (rounded)
			staccadv01.setRounded(what);
		else
			staccadv01.set(what);
	}	
	public PackedDecimalData getStaccadv02() {
		return staccadv02;
	}
	public void setStaccadv02(Object what) {
		setStaccadv02(what, false);
	}
	public void setStaccadv02(Object what, boolean rounded) {
		if (rounded)
			staccadv02.setRounded(what);
		else
			staccadv02.set(what);
	}	
	public PackedDecimalData getStaccadv03() {
		return staccadv03;
	}
	public void setStaccadv03(Object what) {
		setStaccadv03(what, false);
	}
	public void setStaccadv03(Object what, boolean rounded) {
		if (rounded)
			staccadv03.setRounded(what);
		else
			staccadv03.set(what);
	}	
	public PackedDecimalData getStaccadv04() {
		return staccadv04;
	}
	public void setStaccadv04(Object what) {
		setStaccadv04(what, false);
	}
	public void setStaccadv04(Object what, boolean rounded) {
		if (rounded)
			staccadv04.setRounded(what);
		else
			staccadv04.set(what);
	}	
	public PackedDecimalData getStaccadv05() {
		return staccadv05;
	}
	public void setStaccadv05(Object what) {
		setStaccadv05(what, false);
	}
	public void setStaccadv05(Object what, boolean rounded) {
		if (rounded)
			staccadv05.setRounded(what);
		else
			staccadv05.set(what);
	}	
	public PackedDecimalData getStaccadv06() {
		return staccadv06;
	}
	public void setStaccadv06(Object what) {
		setStaccadv06(what, false);
	}
	public void setStaccadv06(Object what, boolean rounded) {
		if (rounded)
			staccadv06.setRounded(what);
		else
			staccadv06.set(what);
	}	
	public PackedDecimalData getStaccadv07() {
		return staccadv07;
	}
	public void setStaccadv07(Object what) {
		setStaccadv07(what, false);
	}
	public void setStaccadv07(Object what, boolean rounded) {
		if (rounded)
			staccadv07.setRounded(what);
		else
			staccadv07.set(what);
	}	
	public PackedDecimalData getStaccadv08() {
		return staccadv08;
	}
	public void setStaccadv08(Object what) {
		setStaccadv08(what, false);
	}
	public void setStaccadv08(Object what, boolean rounded) {
		if (rounded)
			staccadv08.setRounded(what);
		else
			staccadv08.set(what);
	}	
	public PackedDecimalData getStaccadv09() {
		return staccadv09;
	}
	public void setStaccadv09(Object what) {
		setStaccadv09(what, false);
	}
	public void setStaccadv09(Object what, boolean rounded) {
		if (rounded)
			staccadv09.setRounded(what);
		else
			staccadv09.set(what);
	}	
	public PackedDecimalData getStaccadv10() {
		return staccadv10;
	}
	public void setStaccadv10(Object what) {
		setStaccadv10(what, false);
	}
	public void setStaccadv10(Object what, boolean rounded) {
		if (rounded)
			staccadv10.setRounded(what);
		else
			staccadv10.set(what);
	}	
	public PackedDecimalData getStaccadv11() {
		return staccadv11;
	}
	public void setStaccadv11(Object what) {
		setStaccadv11(what, false);
	}
	public void setStaccadv11(Object what, boolean rounded) {
		if (rounded)
			staccadv11.setRounded(what);
		else
			staccadv11.set(what);
	}	
	public PackedDecimalData getStaccadv12() {
		return staccadv12;
	}
	public void setStaccadv12(Object what) {
		setStaccadv12(what, false);
	}
	public void setStaccadv12(Object what, boolean rounded) {
		if (rounded)
			staccadv12.setRounded(what);
		else
			staccadv12.set(what);
	}	
	public PackedDecimalData getCfwdadv() {
		return cfwdadv;
	}
	public void setCfwdadv(Object what) {
		setCfwdadv(what, false);
	}
	public void setCfwdadv(Object what, boolean rounded) {
		if (rounded)
			cfwdadv.setRounded(what);
		else
			cfwdadv.set(what);
	}	
	public PackedDecimalData getBfwdpdb() {
		return bfwdpdb;
	}
	public void setBfwdpdb(Object what) {
		setBfwdpdb(what, false);
	}
	public void setBfwdpdb(Object what, boolean rounded) {
		if (rounded)
			bfwdpdb.setRounded(what);
		else
			bfwdpdb.set(what);
	}	
	public PackedDecimalData getStaccpdb01() {
		return staccpdb01;
	}
	public void setStaccpdb01(Object what) {
		setStaccpdb01(what, false);
	}
	public void setStaccpdb01(Object what, boolean rounded) {
		if (rounded)
			staccpdb01.setRounded(what);
		else
			staccpdb01.set(what);
	}	
	public PackedDecimalData getStaccpdb02() {
		return staccpdb02;
	}
	public void setStaccpdb02(Object what) {
		setStaccpdb02(what, false);
	}
	public void setStaccpdb02(Object what, boolean rounded) {
		if (rounded)
			staccpdb02.setRounded(what);
		else
			staccpdb02.set(what);
	}	
	public PackedDecimalData getStaccpdb03() {
		return staccpdb03;
	}
	public void setStaccpdb03(Object what) {
		setStaccpdb03(what, false);
	}
	public void setStaccpdb03(Object what, boolean rounded) {
		if (rounded)
			staccpdb03.setRounded(what);
		else
			staccpdb03.set(what);
	}	
	public PackedDecimalData getStaccpdb04() {
		return staccpdb04;
	}
	public void setStaccpdb04(Object what) {
		setStaccpdb04(what, false);
	}
	public void setStaccpdb04(Object what, boolean rounded) {
		if (rounded)
			staccpdb04.setRounded(what);
		else
			staccpdb04.set(what);
	}	
	public PackedDecimalData getStaccpdb05() {
		return staccpdb05;
	}
	public void setStaccpdb05(Object what) {
		setStaccpdb05(what, false);
	}
	public void setStaccpdb05(Object what, boolean rounded) {
		if (rounded)
			staccpdb05.setRounded(what);
		else
			staccpdb05.set(what);
	}	
	public PackedDecimalData getStaccpdb06() {
		return staccpdb06;
	}
	public void setStaccpdb06(Object what) {
		setStaccpdb06(what, false);
	}
	public void setStaccpdb06(Object what, boolean rounded) {
		if (rounded)
			staccpdb06.setRounded(what);
		else
			staccpdb06.set(what);
	}	
	public PackedDecimalData getStaccpdb07() {
		return staccpdb07;
	}
	public void setStaccpdb07(Object what) {
		setStaccpdb07(what, false);
	}
	public void setStaccpdb07(Object what, boolean rounded) {
		if (rounded)
			staccpdb07.setRounded(what);
		else
			staccpdb07.set(what);
	}	
	public PackedDecimalData getStaccpdb08() {
		return staccpdb08;
	}
	public void setStaccpdb08(Object what) {
		setStaccpdb08(what, false);
	}
	public void setStaccpdb08(Object what, boolean rounded) {
		if (rounded)
			staccpdb08.setRounded(what);
		else
			staccpdb08.set(what);
	}	
	public PackedDecimalData getStaccpdb09() {
		return staccpdb09;
	}
	public void setStaccpdb09(Object what) {
		setStaccpdb09(what, false);
	}
	public void setStaccpdb09(Object what, boolean rounded) {
		if (rounded)
			staccpdb09.setRounded(what);
		else
			staccpdb09.set(what);
	}	
	public PackedDecimalData getStaccpdb10() {
		return staccpdb10;
	}
	public void setStaccpdb10(Object what) {
		setStaccpdb10(what, false);
	}
	public void setStaccpdb10(Object what, boolean rounded) {
		if (rounded)
			staccpdb10.setRounded(what);
		else
			staccpdb10.set(what);
	}	
	public PackedDecimalData getStaccpdb11() {
		return staccpdb11;
	}
	public void setStaccpdb11(Object what) {
		setStaccpdb11(what, false);
	}
	public void setStaccpdb11(Object what, boolean rounded) {
		if (rounded)
			staccpdb11.setRounded(what);
		else
			staccpdb11.set(what);
	}	
	public PackedDecimalData getStaccpdb12() {
		return staccpdb12;
	}
	public void setStaccpdb12(Object what) {
		setStaccpdb12(what, false);
	}
	public void setStaccpdb12(Object what, boolean rounded) {
		if (rounded)
			staccpdb12.setRounded(what);
		else
			staccpdb12.set(what);
	}	
	public PackedDecimalData getCfwdpdb() {
		return cfwdpdb;
	}
	public void setCfwdpdb(Object what) {
		setCfwdpdb(what, false);
	}
	public void setCfwdpdb(Object what, boolean rounded) {
		if (rounded)
			cfwdpdb.setRounded(what);
		else
			cfwdpdb.set(what);
	}	
	public PackedDecimalData getBfwdadb() {
		return bfwdadb;
	}
	public void setBfwdadb(Object what) {
		setBfwdadb(what, false);
	}
	public void setBfwdadb(Object what, boolean rounded) {
		if (rounded)
			bfwdadb.setRounded(what);
		else
			bfwdadb.set(what);
	}	
	public PackedDecimalData getStaccadb01() {
		return staccadb01;
	}
	public void setStaccadb01(Object what) {
		setStaccadb01(what, false);
	}
	public void setStaccadb01(Object what, boolean rounded) {
		if (rounded)
			staccadb01.setRounded(what);
		else
			staccadb01.set(what);
	}	
	public PackedDecimalData getStaccadb02() {
		return staccadb02;
	}
	public void setStaccadb02(Object what) {
		setStaccadb02(what, false);
	}
	public void setStaccadb02(Object what, boolean rounded) {
		if (rounded)
			staccadb02.setRounded(what);
		else
			staccadb02.set(what);
	}	
	public PackedDecimalData getStaccadb03() {
		return staccadb03;
	}
	public void setStaccadb03(Object what) {
		setStaccadb03(what, false);
	}
	public void setStaccadb03(Object what, boolean rounded) {
		if (rounded)
			staccadb03.setRounded(what);
		else
			staccadb03.set(what);
	}	
	public PackedDecimalData getStaccadb04() {
		return staccadb04;
	}
	public void setStaccadb04(Object what) {
		setStaccadb04(what, false);
	}
	public void setStaccadb04(Object what, boolean rounded) {
		if (rounded)
			staccadb04.setRounded(what);
		else
			staccadb04.set(what);
	}	
	public PackedDecimalData getStaccadb05() {
		return staccadb05;
	}
	public void setStaccadb05(Object what) {
		setStaccadb05(what, false);
	}
	public void setStaccadb05(Object what, boolean rounded) {
		if (rounded)
			staccadb05.setRounded(what);
		else
			staccadb05.set(what);
	}	
	public PackedDecimalData getStaccadb06() {
		return staccadb06;
	}
	public void setStaccadb06(Object what) {
		setStaccadb06(what, false);
	}
	public void setStaccadb06(Object what, boolean rounded) {
		if (rounded)
			staccadb06.setRounded(what);
		else
			staccadb06.set(what);
	}	
	public PackedDecimalData getStaccadb07() {
		return staccadb07;
	}
	public void setStaccadb07(Object what) {
		setStaccadb07(what, false);
	}
	public void setStaccadb07(Object what, boolean rounded) {
		if (rounded)
			staccadb07.setRounded(what);
		else
			staccadb07.set(what);
	}	
	public PackedDecimalData getStaccadb08() {
		return staccadb08;
	}
	public void setStaccadb08(Object what) {
		setStaccadb08(what, false);
	}
	public void setStaccadb08(Object what, boolean rounded) {
		if (rounded)
			staccadb08.setRounded(what);
		else
			staccadb08.set(what);
	}	
	public PackedDecimalData getStaccadb09() {
		return staccadb09;
	}
	public void setStaccadb09(Object what) {
		setStaccadb09(what, false);
	}
	public void setStaccadb09(Object what, boolean rounded) {
		if (rounded)
			staccadb09.setRounded(what);
		else
			staccadb09.set(what);
	}	
	public PackedDecimalData getStaccadb10() {
		return staccadb10;
	}
	public void setStaccadb10(Object what) {
		setStaccadb10(what, false);
	}
	public void setStaccadb10(Object what, boolean rounded) {
		if (rounded)
			staccadb10.setRounded(what);
		else
			staccadb10.set(what);
	}	
	public PackedDecimalData getStaccadb11() {
		return staccadb11;
	}
	public void setStaccadb11(Object what) {
		setStaccadb11(what, false);
	}
	public void setStaccadb11(Object what, boolean rounded) {
		if (rounded)
			staccadb11.setRounded(what);
		else
			staccadb11.set(what);
	}	
	public PackedDecimalData getStaccadb12() {
		return staccadb12;
	}
	public void setStaccadb12(Object what) {
		setStaccadb12(what, false);
	}
	public void setStaccadb12(Object what, boolean rounded) {
		if (rounded)
			staccadb12.setRounded(what);
		else
			staccadb12.set(what);
	}	
	public PackedDecimalData getCfwdadb() {
		return cfwdadb;
	}
	public void setCfwdadb(Object what) {
		setCfwdadb(what, false);
	}
	public void setCfwdadb(Object what, boolean rounded) {
		if (rounded)
			cfwdadb.setRounded(what);
		else
			cfwdadb.set(what);
	}	
	public PackedDecimalData getBfwdadj() {
		return bfwdadj;
	}
	public void setBfwdadj(Object what) {
		setBfwdadj(what, false);
	}
	public void setBfwdadj(Object what, boolean rounded) {
		if (rounded)
			bfwdadj.setRounded(what);
		else
			bfwdadj.set(what);
	}	
	public PackedDecimalData getStaccadj01() {
		return staccadj01;
	}
	public void setStaccadj01(Object what) {
		setStaccadj01(what, false);
	}
	public void setStaccadj01(Object what, boolean rounded) {
		if (rounded)
			staccadj01.setRounded(what);
		else
			staccadj01.set(what);
	}	
	public PackedDecimalData getStaccadj02() {
		return staccadj02;
	}
	public void setStaccadj02(Object what) {
		setStaccadj02(what, false);
	}
	public void setStaccadj02(Object what, boolean rounded) {
		if (rounded)
			staccadj02.setRounded(what);
		else
			staccadj02.set(what);
	}	
	public PackedDecimalData getStaccadj03() {
		return staccadj03;
	}
	public void setStaccadj03(Object what) {
		setStaccadj03(what, false);
	}
	public void setStaccadj03(Object what, boolean rounded) {
		if (rounded)
			staccadj03.setRounded(what);
		else
			staccadj03.set(what);
	}	
	public PackedDecimalData getStaccadj04() {
		return staccadj04;
	}
	public void setStaccadj04(Object what) {
		setStaccadj04(what, false);
	}
	public void setStaccadj04(Object what, boolean rounded) {
		if (rounded)
			staccadj04.setRounded(what);
		else
			staccadj04.set(what);
	}	
	public PackedDecimalData getStaccadj05() {
		return staccadj05;
	}
	public void setStaccadj05(Object what) {
		setStaccadj05(what, false);
	}
	public void setStaccadj05(Object what, boolean rounded) {
		if (rounded)
			staccadj05.setRounded(what);
		else
			staccadj05.set(what);
	}	
	public PackedDecimalData getStaccadj06() {
		return staccadj06;
	}
	public void setStaccadj06(Object what) {
		setStaccadj06(what, false);
	}
	public void setStaccadj06(Object what, boolean rounded) {
		if (rounded)
			staccadj06.setRounded(what);
		else
			staccadj06.set(what);
	}	
	public PackedDecimalData getStaccadj07() {
		return staccadj07;
	}
	public void setStaccadj07(Object what) {
		setStaccadj07(what, false);
	}
	public void setStaccadj07(Object what, boolean rounded) {
		if (rounded)
			staccadj07.setRounded(what);
		else
			staccadj07.set(what);
	}	
	public PackedDecimalData getStaccadj08() {
		return staccadj08;
	}
	public void setStaccadj08(Object what) {
		setStaccadj08(what, false);
	}
	public void setStaccadj08(Object what, boolean rounded) {
		if (rounded)
			staccadj08.setRounded(what);
		else
			staccadj08.set(what);
	}	
	public PackedDecimalData getStaccadj09() {
		return staccadj09;
	}
	public void setStaccadj09(Object what) {
		setStaccadj09(what, false);
	}
	public void setStaccadj09(Object what, boolean rounded) {
		if (rounded)
			staccadj09.setRounded(what);
		else
			staccadj09.set(what);
	}	
	public PackedDecimalData getStaccadj10() {
		return staccadj10;
	}
	public void setStaccadj10(Object what) {
		setStaccadj10(what, false);
	}
	public void setStaccadj10(Object what, boolean rounded) {
		if (rounded)
			staccadj10.setRounded(what);
		else
			staccadj10.set(what);
	}	
	public PackedDecimalData getStaccadj11() {
		return staccadj11;
	}
	public void setStaccadj11(Object what) {
		setStaccadj11(what, false);
	}
	public void setStaccadj11(Object what, boolean rounded) {
		if (rounded)
			staccadj11.setRounded(what);
		else
			staccadj11.set(what);
	}	
	public PackedDecimalData getStaccadj12() {
		return staccadj12;
	}
	public void setStaccadj12(Object what) {
		setStaccadj12(what, false);
	}
	public void setStaccadj12(Object what, boolean rounded) {
		if (rounded)
			staccadj12.setRounded(what);
		else
			staccadj12.set(what);
	}	
	public PackedDecimalData getCfwdadj() {
		return cfwdadj;
	}
	public void setCfwdadj(Object what) {
		setCfwdadj(what, false);
	}
	public void setCfwdadj(Object what, boolean rounded) {
		if (rounded)
			cfwdadj.setRounded(what);
		else
			cfwdadj.set(what);
	}	
	public PackedDecimalData getBfwdint() {
		return bfwdint;
	}
	public void setBfwdint(Object what) {
		setBfwdint(what, false);
	}
	public void setBfwdint(Object what, boolean rounded) {
		if (rounded)
			bfwdint.setRounded(what);
		else
			bfwdint.set(what);
	}	
	public PackedDecimalData getStaccint01() {
		return staccint01;
	}
	public void setStaccint01(Object what) {
		setStaccint01(what, false);
	}
	public void setStaccint01(Object what, boolean rounded) {
		if (rounded)
			staccint01.setRounded(what);
		else
			staccint01.set(what);
	}	
	public PackedDecimalData getStaccint02() {
		return staccint02;
	}
	public void setStaccint02(Object what) {
		setStaccint02(what, false);
	}
	public void setStaccint02(Object what, boolean rounded) {
		if (rounded)
			staccint02.setRounded(what);
		else
			staccint02.set(what);
	}	
	public PackedDecimalData getStaccint03() {
		return staccint03;
	}
	public void setStaccint03(Object what) {
		setStaccint03(what, false);
	}
	public void setStaccint03(Object what, boolean rounded) {
		if (rounded)
			staccint03.setRounded(what);
		else
			staccint03.set(what);
	}	
	public PackedDecimalData getStaccint04() {
		return staccint04;
	}
	public void setStaccint04(Object what) {
		setStaccint04(what, false);
	}
	public void setStaccint04(Object what, boolean rounded) {
		if (rounded)
			staccint04.setRounded(what);
		else
			staccint04.set(what);
	}	
	public PackedDecimalData getStaccint05() {
		return staccint05;
	}
	public void setStaccint05(Object what) {
		setStaccint05(what, false);
	}
	public void setStaccint05(Object what, boolean rounded) {
		if (rounded)
			staccint05.setRounded(what);
		else
			staccint05.set(what);
	}	
	public PackedDecimalData getStaccint06() {
		return staccint06;
	}
	public void setStaccint06(Object what) {
		setStaccint06(what, false);
	}
	public void setStaccint06(Object what, boolean rounded) {
		if (rounded)
			staccint06.setRounded(what);
		else
			staccint06.set(what);
	}	
	public PackedDecimalData getStaccint07() {
		return staccint07;
	}
	public void setStaccint07(Object what) {
		setStaccint07(what, false);
	}
	public void setStaccint07(Object what, boolean rounded) {
		if (rounded)
			staccint07.setRounded(what);
		else
			staccint07.set(what);
	}	
	public PackedDecimalData getStaccint08() {
		return staccint08;
	}
	public void setStaccint08(Object what) {
		setStaccint08(what, false);
	}
	public void setStaccint08(Object what, boolean rounded) {
		if (rounded)
			staccint08.setRounded(what);
		else
			staccint08.set(what);
	}	
	public PackedDecimalData getStaccint09() {
		return staccint09;
	}
	public void setStaccint09(Object what) {
		setStaccint09(what, false);
	}
	public void setStaccint09(Object what, boolean rounded) {
		if (rounded)
			staccint09.setRounded(what);
		else
			staccint09.set(what);
	}	
	public PackedDecimalData getStaccint10() {
		return staccint10;
	}
	public void setStaccint10(Object what) {
		setStaccint10(what, false);
	}
	public void setStaccint10(Object what, boolean rounded) {
		if (rounded)
			staccint10.setRounded(what);
		else
			staccint10.set(what);
	}	
	public PackedDecimalData getStaccint11() {
		return staccint11;
	}
	public void setStaccint11(Object what) {
		setStaccint11(what, false);
	}
	public void setStaccint11(Object what, boolean rounded) {
		if (rounded)
			staccint11.setRounded(what);
		else
			staccint11.set(what);
	}	
	public PackedDecimalData getStaccint12() {
		return staccint12;
	}
	public void setStaccint12(Object what) {
		setStaccint12(what, false);
	}
	public void setStaccint12(Object what, boolean rounded) {
		if (rounded)
			staccint12.setRounded(what);
		else
			staccint12.set(what);
	}	
	public PackedDecimalData getCfwdint() {
		return cfwdint;
	}
	public void setCfwdint(Object what) {
		setCfwdint(what, false);
	}
	public void setCfwdint(Object what, boolean rounded) {
		if (rounded)
			cfwdint.setRounded(what);
		else
			cfwdint.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	/****************************************************************/
	/* Getters and setters for array SKM fields                     */
	/****************************************************************/

	public FixedLengthStringData getStaccxbs() {
		return new FixedLengthStringData(staccxb01.toInternal()
										+ staccxb02.toInternal()
										+ staccxb03.toInternal()
										+ staccxb04.toInternal()
										+ staccxb05.toInternal()
										+ staccxb06.toInternal()
										+ staccxb07.toInternal()
										+ staccxb08.toInternal()
										+ staccxb09.toInternal()
										+ staccxb10.toInternal()
										+ staccxb11.toInternal()
										+ staccxb12.toInternal());
	}
	public void setStaccxbs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccxbs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccxb01);
		what = ExternalData.chop(what, staccxb02);
		what = ExternalData.chop(what, staccxb03);
		what = ExternalData.chop(what, staccxb04);
		what = ExternalData.chop(what, staccxb05);
		what = ExternalData.chop(what, staccxb06);
		what = ExternalData.chop(what, staccxb07);
		what = ExternalData.chop(what, staccxb08);
		what = ExternalData.chop(what, staccxb09);
		what = ExternalData.chop(what, staccxb10);
		what = ExternalData.chop(what, staccxb11);
		what = ExternalData.chop(what, staccxb12);
	}
	public PackedDecimalData getStaccxb(BaseData indx) {
		return getStaccxb(indx.toInt());
	}
	public PackedDecimalData getStaccxb(int indx) {

		switch (indx) {
			case 1 : return staccxb01;
			case 2 : return staccxb02;
			case 3 : return staccxb03;
			case 4 : return staccxb04;
			case 5 : return staccxb05;
			case 6 : return staccxb06;
			case 7 : return staccxb07;
			case 8 : return staccxb08;
			case 9 : return staccxb09;
			case 10 : return staccxb10;
			case 11 : return staccxb11;
			case 12 : return staccxb12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccxb(BaseData indx, Object what) {
		setStaccxb(indx, what, false);
	}
	public void setStaccxb(BaseData indx, Object what, boolean rounded) {
		setStaccxb(indx.toInt(), what, rounded);
	}
	public void setStaccxb(int indx, Object what) {
		setStaccxb(indx, what, false);
	}
	public void setStaccxb(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccxb01(what, rounded);
					 break;
			case 2 : setStaccxb02(what, rounded);
					 break;
			case 3 : setStaccxb03(what, rounded);
					 break;
			case 4 : setStaccxb04(what, rounded);
					 break;
			case 5 : setStaccxb05(what, rounded);
					 break;
			case 6 : setStaccxb06(what, rounded);
					 break;
			case 7 : setStaccxb07(what, rounded);
					 break;
			case 8 : setStaccxb08(what, rounded);
					 break;
			case 9 : setStaccxb09(what, rounded);
					 break;
			case 10 : setStaccxb10(what, rounded);
					 break;
			case 11 : setStaccxb11(what, rounded);
					 break;
			case 12 : setStaccxb12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStacctbs() {
		return new FixedLengthStringData(stacctb01.toInternal()
										+ stacctb02.toInternal()
										+ stacctb03.toInternal()
										+ stacctb04.toInternal()
										+ stacctb05.toInternal()
										+ stacctb06.toInternal()
										+ stacctb07.toInternal()
										+ stacctb08.toInternal()
										+ stacctb09.toInternal()
										+ stacctb10.toInternal()
										+ stacctb11.toInternal()
										+ stacctb12.toInternal());
	}
	public void setStacctbs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStacctbs().getLength()).init(obj);
	
		what = ExternalData.chop(what, stacctb01);
		what = ExternalData.chop(what, stacctb02);
		what = ExternalData.chop(what, stacctb03);
		what = ExternalData.chop(what, stacctb04);
		what = ExternalData.chop(what, stacctb05);
		what = ExternalData.chop(what, stacctb06);
		what = ExternalData.chop(what, stacctb07);
		what = ExternalData.chop(what, stacctb08);
		what = ExternalData.chop(what, stacctb09);
		what = ExternalData.chop(what, stacctb10);
		what = ExternalData.chop(what, stacctb11);
		what = ExternalData.chop(what, stacctb12);
	}
	public PackedDecimalData getStacctb(BaseData indx) {
		return getStacctb(indx.toInt());
	}
	public PackedDecimalData getStacctb(int indx) {

		switch (indx) {
			case 1 : return stacctb01;
			case 2 : return stacctb02;
			case 3 : return stacctb03;
			case 4 : return stacctb04;
			case 5 : return stacctb05;
			case 6 : return stacctb06;
			case 7 : return stacctb07;
			case 8 : return stacctb08;
			case 9 : return stacctb09;
			case 10 : return stacctb10;
			case 11 : return stacctb11;
			case 12 : return stacctb12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStacctb(BaseData indx, Object what) {
		setStacctb(indx, what, false);
	}
	public void setStacctb(BaseData indx, Object what, boolean rounded) {
		setStacctb(indx.toInt(), what, rounded);
	}
	public void setStacctb(int indx, Object what) {
		setStacctb(indx, what, false);
	}
	public void setStacctb(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStacctb01(what, rounded);
					 break;
			case 2 : setStacctb02(what, rounded);
					 break;
			case 3 : setStacctb03(what, rounded);
					 break;
			case 4 : setStacctb04(what, rounded);
					 break;
			case 5 : setStacctb05(what, rounded);
					 break;
			case 6 : setStacctb06(what, rounded);
					 break;
			case 7 : setStacctb07(what, rounded);
					 break;
			case 8 : setStacctb08(what, rounded);
					 break;
			case 9 : setStacctb09(what, rounded);
					 break;
			case 10 : setStacctb10(what, rounded);
					 break;
			case 11 : setStacctb11(what, rounded);
					 break;
			case 12 : setStacctb12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccspds() {
		return new FixedLengthStringData(staccspd01.toInternal()
										+ staccspd02.toInternal()
										+ staccspd03.toInternal()
										+ staccspd04.toInternal()
										+ staccspd05.toInternal()
										+ staccspd06.toInternal()
										+ staccspd07.toInternal()
										+ staccspd08.toInternal()
										+ staccspd09.toInternal()
										+ staccspd10.toInternal()
										+ staccspd11.toInternal()
										+ staccspd12.toInternal());
	}
	public void setStaccspds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccspds().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccspd01);
		what = ExternalData.chop(what, staccspd02);
		what = ExternalData.chop(what, staccspd03);
		what = ExternalData.chop(what, staccspd04);
		what = ExternalData.chop(what, staccspd05);
		what = ExternalData.chop(what, staccspd06);
		what = ExternalData.chop(what, staccspd07);
		what = ExternalData.chop(what, staccspd08);
		what = ExternalData.chop(what, staccspd09);
		what = ExternalData.chop(what, staccspd10);
		what = ExternalData.chop(what, staccspd11);
		what = ExternalData.chop(what, staccspd12);
	}
	public PackedDecimalData getStaccspd(BaseData indx) {
		return getStaccspd(indx.toInt());
	}
	public PackedDecimalData getStaccspd(int indx) {

		switch (indx) {
			case 1 : return staccspd01;
			case 2 : return staccspd02;
			case 3 : return staccspd03;
			case 4 : return staccspd04;
			case 5 : return staccspd05;
			case 6 : return staccspd06;
			case 7 : return staccspd07;
			case 8 : return staccspd08;
			case 9 : return staccspd09;
			case 10 : return staccspd10;
			case 11 : return staccspd11;
			case 12 : return staccspd12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccspd(BaseData indx, Object what) {
		setStaccspd(indx, what, false);
	}
	public void setStaccspd(BaseData indx, Object what, boolean rounded) {
		setStaccspd(indx.toInt(), what, rounded);
	}
	public void setStaccspd(int indx, Object what) {
		setStaccspd(indx, what, false);
	}
	public void setStaccspd(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccspd01(what, rounded);
					 break;
			case 2 : setStaccspd02(what, rounded);
					 break;
			case 3 : setStaccspd03(what, rounded);
					 break;
			case 4 : setStaccspd04(what, rounded);
					 break;
			case 5 : setStaccspd05(what, rounded);
					 break;
			case 6 : setStaccspd06(what, rounded);
					 break;
			case 7 : setStaccspd07(what, rounded);
					 break;
			case 8 : setStaccspd08(what, rounded);
					 break;
			case 9 : setStaccspd09(what, rounded);
					 break;
			case 10 : setStaccspd10(what, rounded);
					 break;
			case 11 : setStaccspd11(what, rounded);
					 break;
			case 12 : setStaccspd12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccspcs() {
		return new FixedLengthStringData(staccspc01.toInternal()
										+ staccspc02.toInternal()
										+ staccspc03.toInternal()
										+ staccspc04.toInternal()
										+ staccspc05.toInternal()
										+ staccspc06.toInternal()
										+ staccspc07.toInternal()
										+ staccspc08.toInternal()
										+ staccspc09.toInternal()
										+ staccspc10.toInternal()
										+ staccspc11.toInternal()
										+ staccspc12.toInternal());
	}
	public void setStaccspcs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccspcs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccspc01);
		what = ExternalData.chop(what, staccspc02);
		what = ExternalData.chop(what, staccspc03);
		what = ExternalData.chop(what, staccspc04);
		what = ExternalData.chop(what, staccspc05);
		what = ExternalData.chop(what, staccspc06);
		what = ExternalData.chop(what, staccspc07);
		what = ExternalData.chop(what, staccspc08);
		what = ExternalData.chop(what, staccspc09);
		what = ExternalData.chop(what, staccspc10);
		what = ExternalData.chop(what, staccspc11);
		what = ExternalData.chop(what, staccspc12);
	}
	public PackedDecimalData getStaccspc(BaseData indx) {
		return getStaccspc(indx.toInt());
	}
	public PackedDecimalData getStaccspc(int indx) {

		switch (indx) {
			case 1 : return staccspc01;
			case 2 : return staccspc02;
			case 3 : return staccspc03;
			case 4 : return staccspc04;
			case 5 : return staccspc05;
			case 6 : return staccspc06;
			case 7 : return staccspc07;
			case 8 : return staccspc08;
			case 9 : return staccspc09;
			case 10 : return staccspc10;
			case 11 : return staccspc11;
			case 12 : return staccspc12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccspc(BaseData indx, Object what) {
		setStaccspc(indx, what, false);
	}
	public void setStaccspc(BaseData indx, Object what, boolean rounded) {
		setStaccspc(indx.toInt(), what, rounded);
	}
	public void setStaccspc(int indx, Object what) {
		setStaccspc(indx, what, false);
	}
	public void setStaccspc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccspc01(what, rounded);
					 break;
			case 2 : setStaccspc02(what, rounded);
					 break;
			case 3 : setStaccspc03(what, rounded);
					 break;
			case 4 : setStaccspc04(what, rounded);
					 break;
			case 5 : setStaccspc05(what, rounded);
					 break;
			case 6 : setStaccspc06(what, rounded);
					 break;
			case 7 : setStaccspc07(what, rounded);
					 break;
			case 8 : setStaccspc08(what, rounded);
					 break;
			case 9 : setStaccspc09(what, rounded);
					 break;
			case 10 : setStaccspc10(what, rounded);
					 break;
			case 11 : setStaccspc11(what, rounded);
					 break;
			case 12 : setStaccspc12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccrnps() {
		return new FixedLengthStringData(staccrnp01.toInternal()
										+ staccrnp02.toInternal()
										+ staccrnp03.toInternal()
										+ staccrnp04.toInternal()
										+ staccrnp05.toInternal()
										+ staccrnp06.toInternal()
										+ staccrnp07.toInternal()
										+ staccrnp08.toInternal()
										+ staccrnp09.toInternal()
										+ staccrnp10.toInternal()
										+ staccrnp11.toInternal()
										+ staccrnp12.toInternal());
	}
	public void setStaccrnps(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccrnps().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccrnp01);
		what = ExternalData.chop(what, staccrnp02);
		what = ExternalData.chop(what, staccrnp03);
		what = ExternalData.chop(what, staccrnp04);
		what = ExternalData.chop(what, staccrnp05);
		what = ExternalData.chop(what, staccrnp06);
		what = ExternalData.chop(what, staccrnp07);
		what = ExternalData.chop(what, staccrnp08);
		what = ExternalData.chop(what, staccrnp09);
		what = ExternalData.chop(what, staccrnp10);
		what = ExternalData.chop(what, staccrnp11);
		what = ExternalData.chop(what, staccrnp12);
	}
	public PackedDecimalData getStaccrnp(BaseData indx) {
		return getStaccrnp(indx.toInt());
	}
	public PackedDecimalData getStaccrnp(int indx) {

		switch (indx) {
			case 1 : return staccrnp01;
			case 2 : return staccrnp02;
			case 3 : return staccrnp03;
			case 4 : return staccrnp04;
			case 5 : return staccrnp05;
			case 6 : return staccrnp06;
			case 7 : return staccrnp07;
			case 8 : return staccrnp08;
			case 9 : return staccrnp09;
			case 10 : return staccrnp10;
			case 11 : return staccrnp11;
			case 12 : return staccrnp12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccrnp(BaseData indx, Object what) {
		setStaccrnp(indx, what, false);
	}
	public void setStaccrnp(BaseData indx, Object what, boolean rounded) {
		setStaccrnp(indx.toInt(), what, rounded);
	}
	public void setStaccrnp(int indx, Object what) {
		setStaccrnp(indx, what, false);
	}
	public void setStaccrnp(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccrnp01(what, rounded);
					 break;
			case 2 : setStaccrnp02(what, rounded);
					 break;
			case 3 : setStaccrnp03(what, rounded);
					 break;
			case 4 : setStaccrnp04(what, rounded);
					 break;
			case 5 : setStaccrnp05(what, rounded);
					 break;
			case 6 : setStaccrnp06(what, rounded);
					 break;
			case 7 : setStaccrnp07(what, rounded);
					 break;
			case 8 : setStaccrnp08(what, rounded);
					 break;
			case 9 : setStaccrnp09(what, rounded);
					 break;
			case 10 : setStaccrnp10(what, rounded);
					 break;
			case 11 : setStaccrnp11(what, rounded);
					 break;
			case 12 : setStaccrnp12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccrlcs() {
		return new FixedLengthStringData(staccrlc01.toInternal()
										+ staccrlc02.toInternal()
										+ staccrlc03.toInternal()
										+ staccrlc04.toInternal()
										+ staccrlc05.toInternal()
										+ staccrlc06.toInternal()
										+ staccrlc07.toInternal()
										+ staccrlc08.toInternal()
										+ staccrlc09.toInternal()
										+ staccrlc10.toInternal()
										+ staccrlc11.toInternal()
										+ staccrlc12.toInternal());
	}
	public void setStaccrlcs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccrlcs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccrlc01);
		what = ExternalData.chop(what, staccrlc02);
		what = ExternalData.chop(what, staccrlc03);
		what = ExternalData.chop(what, staccrlc04);
		what = ExternalData.chop(what, staccrlc05);
		what = ExternalData.chop(what, staccrlc06);
		what = ExternalData.chop(what, staccrlc07);
		what = ExternalData.chop(what, staccrlc08);
		what = ExternalData.chop(what, staccrlc09);
		what = ExternalData.chop(what, staccrlc10);
		what = ExternalData.chop(what, staccrlc11);
		what = ExternalData.chop(what, staccrlc12);
	}
	public PackedDecimalData getStaccrlc(BaseData indx) {
		return getStaccrlc(indx.toInt());
	}
	public PackedDecimalData getStaccrlc(int indx) {

		switch (indx) {
			case 1 : return staccrlc01;
			case 2 : return staccrlc02;
			case 3 : return staccrlc03;
			case 4 : return staccrlc04;
			case 5 : return staccrlc05;
			case 6 : return staccrlc06;
			case 7 : return staccrlc07;
			case 8 : return staccrlc08;
			case 9 : return staccrlc09;
			case 10 : return staccrlc10;
			case 11 : return staccrlc11;
			case 12 : return staccrlc12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccrlc(BaseData indx, Object what) {
		setStaccrlc(indx, what, false);
	}
	public void setStaccrlc(BaseData indx, Object what, boolean rounded) {
		setStaccrlc(indx.toInt(), what, rounded);
	}
	public void setStaccrlc(int indx, Object what) {
		setStaccrlc(indx, what, false);
	}
	public void setStaccrlc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccrlc01(what, rounded);
					 break;
			case 2 : setStaccrlc02(what, rounded);
					 break;
			case 3 : setStaccrlc03(what, rounded);
					 break;
			case 4 : setStaccrlc04(what, rounded);
					 break;
			case 5 : setStaccrlc05(what, rounded);
					 break;
			case 6 : setStaccrlc06(what, rounded);
					 break;
			case 7 : setStaccrlc07(what, rounded);
					 break;
			case 8 : setStaccrlc08(what, rounded);
					 break;
			case 9 : setStaccrlc09(what, rounded);
					 break;
			case 10 : setStaccrlc10(what, rounded);
					 break;
			case 11 : setStaccrlc11(what, rounded);
					 break;
			case 12 : setStaccrlc12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccrips() {
		return new FixedLengthStringData(staccrip01.toInternal()
										+ staccrip02.toInternal()
										+ staccrip03.toInternal()
										+ staccrip04.toInternal()
										+ staccrip05.toInternal()
										+ staccrip06.toInternal()
										+ staccrip07.toInternal()
										+ staccrip08.toInternal()
										+ staccrip09.toInternal()
										+ staccrip10.toInternal()
										+ staccrip11.toInternal()
										+ staccrip12.toInternal());
	}
	public void setStaccrips(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccrips().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccrip01);
		what = ExternalData.chop(what, staccrip02);
		what = ExternalData.chop(what, staccrip03);
		what = ExternalData.chop(what, staccrip04);
		what = ExternalData.chop(what, staccrip05);
		what = ExternalData.chop(what, staccrip06);
		what = ExternalData.chop(what, staccrip07);
		what = ExternalData.chop(what, staccrip08);
		what = ExternalData.chop(what, staccrip09);
		what = ExternalData.chop(what, staccrip10);
		what = ExternalData.chop(what, staccrip11);
		what = ExternalData.chop(what, staccrip12);
	}
	public PackedDecimalData getStaccrip(BaseData indx) {
		return getStaccrip(indx.toInt());
	}
	public PackedDecimalData getStaccrip(int indx) {

		switch (indx) {
			case 1 : return staccrip01;
			case 2 : return staccrip02;
			case 3 : return staccrip03;
			case 4 : return staccrip04;
			case 5 : return staccrip05;
			case 6 : return staccrip06;
			case 7 : return staccrip07;
			case 8 : return staccrip08;
			case 9 : return staccrip09;
			case 10 : return staccrip10;
			case 11 : return staccrip11;
			case 12 : return staccrip12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccrip(BaseData indx, Object what) {
		setStaccrip(indx, what, false);
	}
	public void setStaccrip(BaseData indx, Object what, boolean rounded) {
		setStaccrip(indx.toInt(), what, rounded);
	}
	public void setStaccrip(int indx, Object what) {
		setStaccrip(indx, what, false);
	}
	public void setStaccrip(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccrip01(what, rounded);
					 break;
			case 2 : setStaccrip02(what, rounded);
					 break;
			case 3 : setStaccrip03(what, rounded);
					 break;
			case 4 : setStaccrip04(what, rounded);
					 break;
			case 5 : setStaccrip05(what, rounded);
					 break;
			case 6 : setStaccrip06(what, rounded);
					 break;
			case 7 : setStaccrip07(what, rounded);
					 break;
			case 8 : setStaccrip08(what, rounded);
					 break;
			case 9 : setStaccrip09(what, rounded);
					 break;
			case 10 : setStaccrip10(what, rounded);
					 break;
			case 11 : setStaccrip11(what, rounded);
					 break;
			case 12 : setStaccrip12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccrics() {
		return new FixedLengthStringData(staccric01.toInternal()
										+ staccric02.toInternal()
										+ staccric03.toInternal()
										+ staccric04.toInternal()
										+ staccric05.toInternal()
										+ staccric06.toInternal()
										+ staccric07.toInternal()
										+ staccric08.toInternal()
										+ staccric09.toInternal()
										+ staccric10.toInternal()
										+ staccric11.toInternal()
										+ staccric12.toInternal());
	}
	public void setStaccrics(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccrics().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccric01);
		what = ExternalData.chop(what, staccric02);
		what = ExternalData.chop(what, staccric03);
		what = ExternalData.chop(what, staccric04);
		what = ExternalData.chop(what, staccric05);
		what = ExternalData.chop(what, staccric06);
		what = ExternalData.chop(what, staccric07);
		what = ExternalData.chop(what, staccric08);
		what = ExternalData.chop(what, staccric09);
		what = ExternalData.chop(what, staccric10);
		what = ExternalData.chop(what, staccric11);
		what = ExternalData.chop(what, staccric12);
	}
	public PackedDecimalData getStaccric(BaseData indx) {
		return getStaccric(indx.toInt());
	}
	public PackedDecimalData getStaccric(int indx) {

		switch (indx) {
			case 1 : return staccric01;
			case 2 : return staccric02;
			case 3 : return staccric03;
			case 4 : return staccric04;
			case 5 : return staccric05;
			case 6 : return staccric06;
			case 7 : return staccric07;
			case 8 : return staccric08;
			case 9 : return staccric09;
			case 10 : return staccric10;
			case 11 : return staccric11;
			case 12 : return staccric12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccric(BaseData indx, Object what) {
		setStaccric(indx, what, false);
	}
	public void setStaccric(BaseData indx, Object what, boolean rounded) {
		setStaccric(indx.toInt(), what, rounded);
	}
	public void setStaccric(int indx, Object what) {
		setStaccric(indx, what, false);
	}
	public void setStaccric(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccric01(what, rounded);
					 break;
			case 2 : setStaccric02(what, rounded);
					 break;
			case 3 : setStaccric03(what, rounded);
					 break;
			case 4 : setStaccric04(what, rounded);
					 break;
			case 5 : setStaccric05(what, rounded);
					 break;
			case 6 : setStaccric06(what, rounded);
					 break;
			case 7 : setStaccric07(what, rounded);
					 break;
			case 8 : setStaccric08(what, rounded);
					 break;
			case 9 : setStaccric09(what, rounded);
					 break;
			case 10 : setStaccric10(what, rounded);
					 break;
			case 11 : setStaccric11(what, rounded);
					 break;
			case 12 : setStaccric12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccrbs() {
		return new FixedLengthStringData(staccrb01.toInternal()
										+ staccrb02.toInternal()
										+ staccrb03.toInternal()
										+ staccrb04.toInternal()
										+ staccrb05.toInternal()
										+ staccrb06.toInternal()
										+ staccrb07.toInternal()
										+ staccrb08.toInternal()
										+ staccrb09.toInternal()
										+ staccrb10.toInternal()
										+ staccrb11.toInternal()
										+ staccrb12.toInternal());
	}
	public void setStaccrbs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccrbs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccrb01);
		what = ExternalData.chop(what, staccrb02);
		what = ExternalData.chop(what, staccrb03);
		what = ExternalData.chop(what, staccrb04);
		what = ExternalData.chop(what, staccrb05);
		what = ExternalData.chop(what, staccrb06);
		what = ExternalData.chop(what, staccrb07);
		what = ExternalData.chop(what, staccrb08);
		what = ExternalData.chop(what, staccrb09);
		what = ExternalData.chop(what, staccrb10);
		what = ExternalData.chop(what, staccrb11);
		what = ExternalData.chop(what, staccrb12);
	}
	public PackedDecimalData getStaccrb(BaseData indx) {
		return getStaccrb(indx.toInt());
	}
	public PackedDecimalData getStaccrb(int indx) {

		switch (indx) {
			case 1 : return staccrb01;
			case 2 : return staccrb02;
			case 3 : return staccrb03;
			case 4 : return staccrb04;
			case 5 : return staccrb05;
			case 6 : return staccrb06;
			case 7 : return staccrb07;
			case 8 : return staccrb08;
			case 9 : return staccrb09;
			case 10 : return staccrb10;
			case 11 : return staccrb11;
			case 12 : return staccrb12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccrb(BaseData indx, Object what) {
		setStaccrb(indx, what, false);
	}
	public void setStaccrb(BaseData indx, Object what, boolean rounded) {
		setStaccrb(indx.toInt(), what, rounded);
	}
	public void setStaccrb(int indx, Object what) {
		setStaccrb(indx, what, false);
	}
	public void setStaccrb(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccrb01(what, rounded);
					 break;
			case 2 : setStaccrb02(what, rounded);
					 break;
			case 3 : setStaccrb03(what, rounded);
					 break;
			case 4 : setStaccrb04(what, rounded);
					 break;
			case 5 : setStaccrb05(what, rounded);
					 break;
			case 6 : setStaccrb06(what, rounded);
					 break;
			case 7 : setStaccrb07(what, rounded);
					 break;
			case 8 : setStaccrb08(what, rounded);
					 break;
			case 9 : setStaccrb09(what, rounded);
					 break;
			case 10 : setStaccrb10(what, rounded);
					 break;
			case 11 : setStaccrb11(what, rounded);
					 break;
			case 12 : setStaccrb12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccpdbs() {
		return new FixedLengthStringData(staccpdb01.toInternal()
										+ staccpdb02.toInternal()
										+ staccpdb03.toInternal()
										+ staccpdb04.toInternal()
										+ staccpdb05.toInternal()
										+ staccpdb06.toInternal()
										+ staccpdb07.toInternal()
										+ staccpdb08.toInternal()
										+ staccpdb09.toInternal()
										+ staccpdb10.toInternal()
										+ staccpdb11.toInternal()
										+ staccpdb12.toInternal());
	}
	public void setStaccpdbs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccpdbs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccpdb01);
		what = ExternalData.chop(what, staccpdb02);
		what = ExternalData.chop(what, staccpdb03);
		what = ExternalData.chop(what, staccpdb04);
		what = ExternalData.chop(what, staccpdb05);
		what = ExternalData.chop(what, staccpdb06);
		what = ExternalData.chop(what, staccpdb07);
		what = ExternalData.chop(what, staccpdb08);
		what = ExternalData.chop(what, staccpdb09);
		what = ExternalData.chop(what, staccpdb10);
		what = ExternalData.chop(what, staccpdb11);
		what = ExternalData.chop(what, staccpdb12);
	}
	public PackedDecimalData getStaccpdb(BaseData indx) {
		return getStaccpdb(indx.toInt());
	}
	public PackedDecimalData getStaccpdb(int indx) {

		switch (indx) {
			case 1 : return staccpdb01;
			case 2 : return staccpdb02;
			case 3 : return staccpdb03;
			case 4 : return staccpdb04;
			case 5 : return staccpdb05;
			case 6 : return staccpdb06;
			case 7 : return staccpdb07;
			case 8 : return staccpdb08;
			case 9 : return staccpdb09;
			case 10 : return staccpdb10;
			case 11 : return staccpdb11;
			case 12 : return staccpdb12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccpdb(BaseData indx, Object what) {
		setStaccpdb(indx, what, false);
	}
	public void setStaccpdb(BaseData indx, Object what, boolean rounded) {
		setStaccpdb(indx.toInt(), what, rounded);
	}
	public void setStaccpdb(int indx, Object what) {
		setStaccpdb(indx, what, false);
	}
	public void setStaccpdb(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccpdb01(what, rounded);
					 break;
			case 2 : setStaccpdb02(what, rounded);
					 break;
			case 3 : setStaccpdb03(what, rounded);
					 break;
			case 4 : setStaccpdb04(what, rounded);
					 break;
			case 5 : setStaccpdb05(what, rounded);
					 break;
			case 6 : setStaccpdb06(what, rounded);
					 break;
			case 7 : setStaccpdb07(what, rounded);
					 break;
			case 8 : setStaccpdb08(what, rounded);
					 break;
			case 9 : setStaccpdb09(what, rounded);
					 break;
			case 10 : setStaccpdb10(what, rounded);
					 break;
			case 11 : setStaccpdb11(what, rounded);
					 break;
			case 12 : setStaccpdb12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccobs() {
		return new FixedLengthStringData(staccob01.toInternal()
										+ staccob02.toInternal()
										+ staccob03.toInternal()
										+ staccob04.toInternal()
										+ staccob05.toInternal()
										+ staccob06.toInternal()
										+ staccob07.toInternal()
										+ staccob08.toInternal()
										+ staccob09.toInternal()
										+ staccob10.toInternal()
										+ staccob11.toInternal()
										+ staccob12.toInternal());
	}
	public void setStaccobs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccobs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccob01);
		what = ExternalData.chop(what, staccob02);
		what = ExternalData.chop(what, staccob03);
		what = ExternalData.chop(what, staccob04);
		what = ExternalData.chop(what, staccob05);
		what = ExternalData.chop(what, staccob06);
		what = ExternalData.chop(what, staccob07);
		what = ExternalData.chop(what, staccob08);
		what = ExternalData.chop(what, staccob09);
		what = ExternalData.chop(what, staccob10);
		what = ExternalData.chop(what, staccob11);
		what = ExternalData.chop(what, staccob12);
	}
	public PackedDecimalData getStaccob(BaseData indx) {
		return getStaccob(indx.toInt());
	}
	public PackedDecimalData getStaccob(int indx) {

		switch (indx) {
			case 1 : return staccob01;
			case 2 : return staccob02;
			case 3 : return staccob03;
			case 4 : return staccob04;
			case 5 : return staccob05;
			case 6 : return staccob06;
			case 7 : return staccob07;
			case 8 : return staccob08;
			case 9 : return staccob09;
			case 10 : return staccob10;
			case 11 : return staccob11;
			case 12 : return staccob12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccob(BaseData indx, Object what) {
		setStaccob(indx, what, false);
	}
	public void setStaccob(BaseData indx, Object what, boolean rounded) {
		setStaccob(indx.toInt(), what, rounded);
	}
	public void setStaccob(int indx, Object what) {
		setStaccob(indx, what, false);
	}
	public void setStaccob(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccob01(what, rounded);
					 break;
			case 2 : setStaccob02(what, rounded);
					 break;
			case 3 : setStaccob03(what, rounded);
					 break;
			case 4 : setStaccob04(what, rounded);
					 break;
			case 5 : setStaccob05(what, rounded);
					 break;
			case 6 : setStaccob06(what, rounded);
					 break;
			case 7 : setStaccob07(what, rounded);
					 break;
			case 8 : setStaccob08(what, rounded);
					 break;
			case 9 : setStaccob09(what, rounded);
					 break;
			case 10 : setStaccob10(what, rounded);
					 break;
			case 11 : setStaccob11(what, rounded);
					 break;
			case 12 : setStaccob12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccmtbs() {
		return new FixedLengthStringData(staccmtb01.toInternal()
										+ staccmtb02.toInternal()
										+ staccmtb03.toInternal()
										+ staccmtb04.toInternal()
										+ staccmtb05.toInternal()
										+ staccmtb06.toInternal()
										+ staccmtb07.toInternal()
										+ staccmtb08.toInternal()
										+ staccmtb09.toInternal()
										+ staccmtb10.toInternal()
										+ staccmtb11.toInternal()
										+ staccmtb12.toInternal());
	}
	public void setStaccmtbs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccmtbs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccmtb01);
		what = ExternalData.chop(what, staccmtb02);
		what = ExternalData.chop(what, staccmtb03);
		what = ExternalData.chop(what, staccmtb04);
		what = ExternalData.chop(what, staccmtb05);
		what = ExternalData.chop(what, staccmtb06);
		what = ExternalData.chop(what, staccmtb07);
		what = ExternalData.chop(what, staccmtb08);
		what = ExternalData.chop(what, staccmtb09);
		what = ExternalData.chop(what, staccmtb10);
		what = ExternalData.chop(what, staccmtb11);
		what = ExternalData.chop(what, staccmtb12);
	}
	public PackedDecimalData getStaccmtb(BaseData indx) {
		return getStaccmtb(indx.toInt());
	}
	public PackedDecimalData getStaccmtb(int indx) {

		switch (indx) {
			case 1 : return staccmtb01;
			case 2 : return staccmtb02;
			case 3 : return staccmtb03;
			case 4 : return staccmtb04;
			case 5 : return staccmtb05;
			case 6 : return staccmtb06;
			case 7 : return staccmtb07;
			case 8 : return staccmtb08;
			case 9 : return staccmtb09;
			case 10 : return staccmtb10;
			case 11 : return staccmtb11;
			case 12 : return staccmtb12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccmtb(BaseData indx, Object what) {
		setStaccmtb(indx, what, false);
	}
	public void setStaccmtb(BaseData indx, Object what, boolean rounded) {
		setStaccmtb(indx.toInt(), what, rounded);
	}
	public void setStaccmtb(int indx, Object what) {
		setStaccmtb(indx, what, false);
	}
	public void setStaccmtb(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccmtb01(what, rounded);
					 break;
			case 2 : setStaccmtb02(what, rounded);
					 break;
			case 3 : setStaccmtb03(what, rounded);
					 break;
			case 4 : setStaccmtb04(what, rounded);
					 break;
			case 5 : setStaccmtb05(what, rounded);
					 break;
			case 6 : setStaccmtb06(what, rounded);
					 break;
			case 7 : setStaccmtb07(what, rounded);
					 break;
			case 8 : setStaccmtb08(what, rounded);
					 break;
			case 9 : setStaccmtb09(what, rounded);
					 break;
			case 10 : setStaccmtb10(what, rounded);
					 break;
			case 11 : setStaccmtb11(what, rounded);
					 break;
			case 12 : setStaccmtb12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccints() {
		return new FixedLengthStringData(staccint01.toInternal()
										+ staccint02.toInternal()
										+ staccint03.toInternal()
										+ staccint04.toInternal()
										+ staccint05.toInternal()
										+ staccint06.toInternal()
										+ staccint07.toInternal()
										+ staccint08.toInternal()
										+ staccint09.toInternal()
										+ staccint10.toInternal()
										+ staccint11.toInternal()
										+ staccint12.toInternal());
	}
	public void setStaccints(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccints().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccint01);
		what = ExternalData.chop(what, staccint02);
		what = ExternalData.chop(what, staccint03);
		what = ExternalData.chop(what, staccint04);
		what = ExternalData.chop(what, staccint05);
		what = ExternalData.chop(what, staccint06);
		what = ExternalData.chop(what, staccint07);
		what = ExternalData.chop(what, staccint08);
		what = ExternalData.chop(what, staccint09);
		what = ExternalData.chop(what, staccint10);
		what = ExternalData.chop(what, staccint11);
		what = ExternalData.chop(what, staccint12);
	}
	public PackedDecimalData getStaccint(BaseData indx) {
		return getStaccint(indx.toInt());
	}
	public PackedDecimalData getStaccint(int indx) {

		switch (indx) {
			case 1 : return staccint01;
			case 2 : return staccint02;
			case 3 : return staccint03;
			case 4 : return staccint04;
			case 5 : return staccint05;
			case 6 : return staccint06;
			case 7 : return staccint07;
			case 8 : return staccint08;
			case 9 : return staccint09;
			case 10 : return staccint10;
			case 11 : return staccint11;
			case 12 : return staccint12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccint(BaseData indx, Object what) {
		setStaccint(indx, what, false);
	}
	public void setStaccint(BaseData indx, Object what, boolean rounded) {
		setStaccint(indx.toInt(), what, rounded);
	}
	public void setStaccint(int indx, Object what) {
		setStaccint(indx, what, false);
	}
	public void setStaccint(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccint01(what, rounded);
					 break;
			case 2 : setStaccint02(what, rounded);
					 break;
			case 3 : setStaccint03(what, rounded);
					 break;
			case 4 : setStaccint04(what, rounded);
					 break;
			case 5 : setStaccint05(what, rounded);
					 break;
			case 6 : setStaccint06(what, rounded);
					 break;
			case 7 : setStaccint07(what, rounded);
					 break;
			case 8 : setStaccint08(what, rounded);
					 break;
			case 9 : setStaccint09(what, rounded);
					 break;
			case 10 : setStaccint10(what, rounded);
					 break;
			case 11 : setStaccint11(what, rounded);
					 break;
			case 12 : setStaccint12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccibs() {
		return new FixedLengthStringData(staccib01.toInternal()
										+ staccib02.toInternal()
										+ staccib03.toInternal()
										+ staccib04.toInternal()
										+ staccib05.toInternal()
										+ staccib06.toInternal()
										+ staccib07.toInternal()
										+ staccib08.toInternal()
										+ staccib09.toInternal()
										+ staccib10.toInternal()
										+ staccib11.toInternal()
										+ staccib12.toInternal());
	}
	public void setStaccibs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccibs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccib01);
		what = ExternalData.chop(what, staccib02);
		what = ExternalData.chop(what, staccib03);
		what = ExternalData.chop(what, staccib04);
		what = ExternalData.chop(what, staccib05);
		what = ExternalData.chop(what, staccib06);
		what = ExternalData.chop(what, staccib07);
		what = ExternalData.chop(what, staccib08);
		what = ExternalData.chop(what, staccib09);
		what = ExternalData.chop(what, staccib10);
		what = ExternalData.chop(what, staccib11);
		what = ExternalData.chop(what, staccib12);
	}
	public PackedDecimalData getStaccib(BaseData indx) {
		return getStaccib(indx.toInt());
	}
	public PackedDecimalData getStaccib(int indx) {

		switch (indx) {
			case 1 : return staccib01;
			case 2 : return staccib02;
			case 3 : return staccib03;
			case 4 : return staccib04;
			case 5 : return staccib05;
			case 6 : return staccib06;
			case 7 : return staccib07;
			case 8 : return staccib08;
			case 9 : return staccib09;
			case 10 : return staccib10;
			case 11 : return staccib11;
			case 12 : return staccib12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccib(BaseData indx, Object what) {
		setStaccib(indx, what, false);
	}
	public void setStaccib(BaseData indx, Object what, boolean rounded) {
		setStaccib(indx.toInt(), what, rounded);
	}
	public void setStaccib(int indx, Object what) {
		setStaccib(indx, what, false);
	}
	public void setStaccib(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccib01(what, rounded);
					 break;
			case 2 : setStaccib02(what, rounded);
					 break;
			case 3 : setStaccib03(what, rounded);
					 break;
			case 4 : setStaccib04(what, rounded);
					 break;
			case 5 : setStaccib05(what, rounded);
					 break;
			case 6 : setStaccib06(what, rounded);
					 break;
			case 7 : setStaccib07(what, rounded);
					 break;
			case 8 : setStaccib08(what, rounded);
					 break;
			case 9 : setStaccib09(what, rounded);
					 break;
			case 10 : setStaccib10(what, rounded);
					 break;
			case 11 : setStaccib11(what, rounded);
					 break;
			case 12 : setStaccib12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccfyps() {
		return new FixedLengthStringData(staccfyp01.toInternal()
										+ staccfyp02.toInternal()
										+ staccfyp03.toInternal()
										+ staccfyp04.toInternal()
										+ staccfyp05.toInternal()
										+ staccfyp06.toInternal()
										+ staccfyp07.toInternal()
										+ staccfyp08.toInternal()
										+ staccfyp09.toInternal()
										+ staccfyp10.toInternal()
										+ staccfyp11.toInternal()
										+ staccfyp12.toInternal());
	}
	public void setStaccfyps(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccfyps().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccfyp01);
		what = ExternalData.chop(what, staccfyp02);
		what = ExternalData.chop(what, staccfyp03);
		what = ExternalData.chop(what, staccfyp04);
		what = ExternalData.chop(what, staccfyp05);
		what = ExternalData.chop(what, staccfyp06);
		what = ExternalData.chop(what, staccfyp07);
		what = ExternalData.chop(what, staccfyp08);
		what = ExternalData.chop(what, staccfyp09);
		what = ExternalData.chop(what, staccfyp10);
		what = ExternalData.chop(what, staccfyp11);
		what = ExternalData.chop(what, staccfyp12);
	}
	public PackedDecimalData getStaccfyp(BaseData indx) {
		return getStaccfyp(indx.toInt());
	}
	public PackedDecimalData getStaccfyp(int indx) {

		switch (indx) {
			case 1 : return staccfyp01;
			case 2 : return staccfyp02;
			case 3 : return staccfyp03;
			case 4 : return staccfyp04;
			case 5 : return staccfyp05;
			case 6 : return staccfyp06;
			case 7 : return staccfyp07;
			case 8 : return staccfyp08;
			case 9 : return staccfyp09;
			case 10 : return staccfyp10;
			case 11 : return staccfyp11;
			case 12 : return staccfyp12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccfyp(BaseData indx, Object what) {
		setStaccfyp(indx, what, false);
	}
	public void setStaccfyp(BaseData indx, Object what, boolean rounded) {
		setStaccfyp(indx.toInt(), what, rounded);
	}
	public void setStaccfyp(int indx, Object what) {
		setStaccfyp(indx, what, false);
	}
	public void setStaccfyp(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccfyp01(what, rounded);
					 break;
			case 2 : setStaccfyp02(what, rounded);
					 break;
			case 3 : setStaccfyp03(what, rounded);
					 break;
			case 4 : setStaccfyp04(what, rounded);
					 break;
			case 5 : setStaccfyp05(what, rounded);
					 break;
			case 6 : setStaccfyp06(what, rounded);
					 break;
			case 7 : setStaccfyp07(what, rounded);
					 break;
			case 8 : setStaccfyp08(what, rounded);
					 break;
			case 9 : setStaccfyp09(what, rounded);
					 break;
			case 10 : setStaccfyp10(what, rounded);
					 break;
			case 11 : setStaccfyp11(what, rounded);
					 break;
			case 12 : setStaccfyp12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccfycs() {
		return new FixedLengthStringData(staccfyc01.toInternal()
										+ staccfyc02.toInternal()
										+ staccfyc03.toInternal()
										+ staccfyc04.toInternal()
										+ staccfyc05.toInternal()
										+ staccfyc06.toInternal()
										+ staccfyc07.toInternal()
										+ staccfyc08.toInternal()
										+ staccfyc09.toInternal()
										+ staccfyc10.toInternal()
										+ staccfyc11.toInternal()
										+ staccfyc12.toInternal());
	}
	public void setStaccfycs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccfycs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccfyc01);
		what = ExternalData.chop(what, staccfyc02);
		what = ExternalData.chop(what, staccfyc03);
		what = ExternalData.chop(what, staccfyc04);
		what = ExternalData.chop(what, staccfyc05);
		what = ExternalData.chop(what, staccfyc06);
		what = ExternalData.chop(what, staccfyc07);
		what = ExternalData.chop(what, staccfyc08);
		what = ExternalData.chop(what, staccfyc09);
		what = ExternalData.chop(what, staccfyc10);
		what = ExternalData.chop(what, staccfyc11);
		what = ExternalData.chop(what, staccfyc12);
	}
	public PackedDecimalData getStaccfyc(BaseData indx) {
		return getStaccfyc(indx.toInt());
	}
	public PackedDecimalData getStaccfyc(int indx) {

		switch (indx) {
			case 1 : return staccfyc01;
			case 2 : return staccfyc02;
			case 3 : return staccfyc03;
			case 4 : return staccfyc04;
			case 5 : return staccfyc05;
			case 6 : return staccfyc06;
			case 7 : return staccfyc07;
			case 8 : return staccfyc08;
			case 9 : return staccfyc09;
			case 10 : return staccfyc10;
			case 11 : return staccfyc11;
			case 12 : return staccfyc12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccfyc(BaseData indx, Object what) {
		setStaccfyc(indx, what, false);
	}
	public void setStaccfyc(BaseData indx, Object what, boolean rounded) {
		setStaccfyc(indx.toInt(), what, rounded);
	}
	public void setStaccfyc(int indx, Object what) {
		setStaccfyc(indx, what, false);
	}
	public void setStaccfyc(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccfyc01(what, rounded);
					 break;
			case 2 : setStaccfyc02(what, rounded);
					 break;
			case 3 : setStaccfyc03(what, rounded);
					 break;
			case 4 : setStaccfyc04(what, rounded);
					 break;
			case 5 : setStaccfyc05(what, rounded);
					 break;
			case 6 : setStaccfyc06(what, rounded);
					 break;
			case 7 : setStaccfyc07(what, rounded);
					 break;
			case 8 : setStaccfyc08(what, rounded);
					 break;
			case 9 : setStaccfyc09(what, rounded);
					 break;
			case 10 : setStaccfyc10(what, rounded);
					 break;
			case 11 : setStaccfyc11(what, rounded);
					 break;
			case 12 : setStaccfyc12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccdis() {
		return new FixedLengthStringData(staccdi01.toInternal()
										+ staccdi02.toInternal()
										+ staccdi03.toInternal()
										+ staccdi04.toInternal()
										+ staccdi05.toInternal()
										+ staccdi06.toInternal()
										+ staccdi07.toInternal()
										+ staccdi08.toInternal()
										+ staccdi09.toInternal()
										+ staccdi10.toInternal()
										+ staccdi11.toInternal()
										+ staccdi12.toInternal());
	}
	public void setStaccdis(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccdis().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccdi01);
		what = ExternalData.chop(what, staccdi02);
		what = ExternalData.chop(what, staccdi03);
		what = ExternalData.chop(what, staccdi04);
		what = ExternalData.chop(what, staccdi05);
		what = ExternalData.chop(what, staccdi06);
		what = ExternalData.chop(what, staccdi07);
		what = ExternalData.chop(what, staccdi08);
		what = ExternalData.chop(what, staccdi09);
		what = ExternalData.chop(what, staccdi10);
		what = ExternalData.chop(what, staccdi11);
		what = ExternalData.chop(what, staccdi12);
	}
	public PackedDecimalData getStaccdi(BaseData indx) {
		return getStaccdi(indx.toInt());
	}
	public PackedDecimalData getStaccdi(int indx) {

		switch (indx) {
			case 1 : return staccdi01;
			case 2 : return staccdi02;
			case 3 : return staccdi03;
			case 4 : return staccdi04;
			case 5 : return staccdi05;
			case 6 : return staccdi06;
			case 7 : return staccdi07;
			case 8 : return staccdi08;
			case 9 : return staccdi09;
			case 10 : return staccdi10;
			case 11 : return staccdi11;
			case 12 : return staccdi12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccdi(BaseData indx, Object what) {
		setStaccdi(indx, what, false);
	}
	public void setStaccdi(BaseData indx, Object what, boolean rounded) {
		setStaccdi(indx.toInt(), what, rounded);
	}
	public void setStaccdi(int indx, Object what) {
		setStaccdi(indx, what, false);
	}
	public void setStaccdi(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccdi01(what, rounded);
					 break;
			case 2 : setStaccdi02(what, rounded);
					 break;
			case 3 : setStaccdi03(what, rounded);
					 break;
			case 4 : setStaccdi04(what, rounded);
					 break;
			case 5 : setStaccdi05(what, rounded);
					 break;
			case 6 : setStaccdi06(what, rounded);
					 break;
			case 7 : setStaccdi07(what, rounded);
					 break;
			case 8 : setStaccdi08(what, rounded);
					 break;
			case 9 : setStaccdi09(what, rounded);
					 break;
			case 10 : setStaccdi10(what, rounded);
					 break;
			case 11 : setStaccdi11(what, rounded);
					 break;
			case 12 : setStaccdi12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccdds() {
		return new FixedLengthStringData(staccdd01.toInternal()
										+ staccdd02.toInternal()
										+ staccdd03.toInternal()
										+ staccdd04.toInternal()
										+ staccdd05.toInternal()
										+ staccdd06.toInternal()
										+ staccdd07.toInternal()
										+ staccdd08.toInternal()
										+ staccdd09.toInternal()
										+ staccdd10.toInternal()
										+ staccdd11.toInternal()
										+ staccdd12.toInternal());
	}
	public void setStaccdds(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccdds().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccdd01);
		what = ExternalData.chop(what, staccdd02);
		what = ExternalData.chop(what, staccdd03);
		what = ExternalData.chop(what, staccdd04);
		what = ExternalData.chop(what, staccdd05);
		what = ExternalData.chop(what, staccdd06);
		what = ExternalData.chop(what, staccdd07);
		what = ExternalData.chop(what, staccdd08);
		what = ExternalData.chop(what, staccdd09);
		what = ExternalData.chop(what, staccdd10);
		what = ExternalData.chop(what, staccdd11);
		what = ExternalData.chop(what, staccdd12);
	}
	public PackedDecimalData getStaccdd(BaseData indx) {
		return getStaccdd(indx.toInt());
	}
	public PackedDecimalData getStaccdd(int indx) {

		switch (indx) {
			case 1 : return staccdd01;
			case 2 : return staccdd02;
			case 3 : return staccdd03;
			case 4 : return staccdd04;
			case 5 : return staccdd05;
			case 6 : return staccdd06;
			case 7 : return staccdd07;
			case 8 : return staccdd08;
			case 9 : return staccdd09;
			case 10 : return staccdd10;
			case 11 : return staccdd11;
			case 12 : return staccdd12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccdd(BaseData indx, Object what) {
		setStaccdd(indx, what, false);
	}
	public void setStaccdd(BaseData indx, Object what, boolean rounded) {
		setStaccdd(indx.toInt(), what, rounded);
	}
	public void setStaccdd(int indx, Object what) {
		setStaccdd(indx, what, false);
	}
	public void setStaccdd(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccdd01(what, rounded);
					 break;
			case 2 : setStaccdd02(what, rounded);
					 break;
			case 3 : setStaccdd03(what, rounded);
					 break;
			case 4 : setStaccdd04(what, rounded);
					 break;
			case 5 : setStaccdd05(what, rounded);
					 break;
			case 6 : setStaccdd06(what, rounded);
					 break;
			case 7 : setStaccdd07(what, rounded);
					 break;
			case 8 : setStaccdd08(what, rounded);
					 break;
			case 9 : setStaccdd09(what, rounded);
					 break;
			case 10 : setStaccdd10(what, rounded);
					 break;
			case 11 : setStaccdd11(what, rounded);
					 break;
			case 12 : setStaccdd12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccclrs() {
		return new FixedLengthStringData(staccclr01.toInternal()
										+ staccclr02.toInternal()
										+ staccclr03.toInternal()
										+ staccclr04.toInternal()
										+ staccclr05.toInternal()
										+ staccclr06.toInternal()
										+ staccclr07.toInternal()
										+ staccclr08.toInternal()
										+ staccclr09.toInternal()
										+ staccclr10.toInternal()
										+ staccclr11.toInternal()
										+ staccclr12.toInternal());
	}
	public void setStaccclrs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccclrs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccclr01);
		what = ExternalData.chop(what, staccclr02);
		what = ExternalData.chop(what, staccclr03);
		what = ExternalData.chop(what, staccclr04);
		what = ExternalData.chop(what, staccclr05);
		what = ExternalData.chop(what, staccclr06);
		what = ExternalData.chop(what, staccclr07);
		what = ExternalData.chop(what, staccclr08);
		what = ExternalData.chop(what, staccclr09);
		what = ExternalData.chop(what, staccclr10);
		what = ExternalData.chop(what, staccclr11);
		what = ExternalData.chop(what, staccclr12);
	}
	public PackedDecimalData getStaccclr(BaseData indx) {
		return getStaccclr(indx.toInt());
	}
	public PackedDecimalData getStaccclr(int indx) {

		switch (indx) {
			case 1 : return staccclr01;
			case 2 : return staccclr02;
			case 3 : return staccclr03;
			case 4 : return staccclr04;
			case 5 : return staccclr05;
			case 6 : return staccclr06;
			case 7 : return staccclr07;
			case 8 : return staccclr08;
			case 9 : return staccclr09;
			case 10 : return staccclr10;
			case 11 : return staccclr11;
			case 12 : return staccclr12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccclr(BaseData indx, Object what) {
		setStaccclr(indx, what, false);
	}
	public void setStaccclr(BaseData indx, Object what, boolean rounded) {
		setStaccclr(indx.toInt(), what, rounded);
	}
	public void setStaccclr(int indx, Object what) {
		setStaccclr(indx, what, false);
	}
	public void setStaccclr(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccclr01(what, rounded);
					 break;
			case 2 : setStaccclr02(what, rounded);
					 break;
			case 3 : setStaccclr03(what, rounded);
					 break;
			case 4 : setStaccclr04(what, rounded);
					 break;
			case 5 : setStaccclr05(what, rounded);
					 break;
			case 6 : setStaccclr06(what, rounded);
					 break;
			case 7 : setStaccclr07(what, rounded);
					 break;
			case 8 : setStaccclr08(what, rounded);
					 break;
			case 9 : setStaccclr09(what, rounded);
					 break;
			case 10 : setStaccclr10(what, rounded);
					 break;
			case 11 : setStaccclr11(what, rounded);
					 break;
			case 12 : setStaccclr12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccbss() {
		return new FixedLengthStringData(staccbs01.toInternal()
										+ staccbs02.toInternal()
										+ staccbs03.toInternal()
										+ staccbs04.toInternal()
										+ staccbs05.toInternal()
										+ staccbs06.toInternal()
										+ staccbs07.toInternal()
										+ staccbs08.toInternal()
										+ staccbs09.toInternal()
										+ staccbs10.toInternal()
										+ staccbs11.toInternal()
										+ staccbs12.toInternal());
	}
	public void setStaccbss(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccbss().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccbs01);
		what = ExternalData.chop(what, staccbs02);
		what = ExternalData.chop(what, staccbs03);
		what = ExternalData.chop(what, staccbs04);
		what = ExternalData.chop(what, staccbs05);
		what = ExternalData.chop(what, staccbs06);
		what = ExternalData.chop(what, staccbs07);
		what = ExternalData.chop(what, staccbs08);
		what = ExternalData.chop(what, staccbs09);
		what = ExternalData.chop(what, staccbs10);
		what = ExternalData.chop(what, staccbs11);
		what = ExternalData.chop(what, staccbs12);
	}
	public PackedDecimalData getStaccbs(BaseData indx) {
		return getStaccbs(indx.toInt());
	}
	public PackedDecimalData getStaccbs(int indx) {

		switch (indx) {
			case 1 : return staccbs01;
			case 2 : return staccbs02;
			case 3 : return staccbs03;
			case 4 : return staccbs04;
			case 5 : return staccbs05;
			case 6 : return staccbs06;
			case 7 : return staccbs07;
			case 8 : return staccbs08;
			case 9 : return staccbs09;
			case 10 : return staccbs10;
			case 11 : return staccbs11;
			case 12 : return staccbs12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccbs(BaseData indx, Object what) {
		setStaccbs(indx, what, false);
	}
	public void setStaccbs(BaseData indx, Object what, boolean rounded) {
		setStaccbs(indx.toInt(), what, rounded);
	}
	public void setStaccbs(int indx, Object what) {
		setStaccbs(indx, what, false);
	}
	public void setStaccbs(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccbs01(what, rounded);
					 break;
			case 2 : setStaccbs02(what, rounded);
					 break;
			case 3 : setStaccbs03(what, rounded);
					 break;
			case 4 : setStaccbs04(what, rounded);
					 break;
			case 5 : setStaccbs05(what, rounded);
					 break;
			case 6 : setStaccbs06(what, rounded);
					 break;
			case 7 : setStaccbs07(what, rounded);
					 break;
			case 8 : setStaccbs08(what, rounded);
					 break;
			case 9 : setStaccbs09(what, rounded);
					 break;
			case 10 : setStaccbs10(what, rounded);
					 break;
			case 11 : setStaccbs11(what, rounded);
					 break;
			case 12 : setStaccbs12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccadvs() {
		return new FixedLengthStringData(staccadv01.toInternal()
										+ staccadv02.toInternal()
										+ staccadv03.toInternal()
										+ staccadv04.toInternal()
										+ staccadv05.toInternal()
										+ staccadv06.toInternal()
										+ staccadv07.toInternal()
										+ staccadv08.toInternal()
										+ staccadv09.toInternal()
										+ staccadv10.toInternal()
										+ staccadv11.toInternal()
										+ staccadv12.toInternal());
	}
	public void setStaccadvs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccadvs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccadv01);
		what = ExternalData.chop(what, staccadv02);
		what = ExternalData.chop(what, staccadv03);
		what = ExternalData.chop(what, staccadv04);
		what = ExternalData.chop(what, staccadv05);
		what = ExternalData.chop(what, staccadv06);
		what = ExternalData.chop(what, staccadv07);
		what = ExternalData.chop(what, staccadv08);
		what = ExternalData.chop(what, staccadv09);
		what = ExternalData.chop(what, staccadv10);
		what = ExternalData.chop(what, staccadv11);
		what = ExternalData.chop(what, staccadv12);
	}
	public PackedDecimalData getStaccadv(BaseData indx) {
		return getStaccadv(indx.toInt());
	}
	public PackedDecimalData getStaccadv(int indx) {

		switch (indx) {
			case 1 : return staccadv01;
			case 2 : return staccadv02;
			case 3 : return staccadv03;
			case 4 : return staccadv04;
			case 5 : return staccadv05;
			case 6 : return staccadv06;
			case 7 : return staccadv07;
			case 8 : return staccadv08;
			case 9 : return staccadv09;
			case 10 : return staccadv10;
			case 11 : return staccadv11;
			case 12 : return staccadv12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccadv(BaseData indx, Object what) {
		setStaccadv(indx, what, false);
	}
	public void setStaccadv(BaseData indx, Object what, boolean rounded) {
		setStaccadv(indx.toInt(), what, rounded);
	}
	public void setStaccadv(int indx, Object what) {
		setStaccadv(indx, what, false);
	}
	public void setStaccadv(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccadv01(what, rounded);
					 break;
			case 2 : setStaccadv02(what, rounded);
					 break;
			case 3 : setStaccadv03(what, rounded);
					 break;
			case 4 : setStaccadv04(what, rounded);
					 break;
			case 5 : setStaccadv05(what, rounded);
					 break;
			case 6 : setStaccadv06(what, rounded);
					 break;
			case 7 : setStaccadv07(what, rounded);
					 break;
			case 8 : setStaccadv08(what, rounded);
					 break;
			case 9 : setStaccadv09(what, rounded);
					 break;
			case 10 : setStaccadv10(what, rounded);
					 break;
			case 11 : setStaccadv11(what, rounded);
					 break;
			case 12 : setStaccadv12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccadjs() {
		return new FixedLengthStringData(staccadj01.toInternal()
										+ staccadj02.toInternal()
										+ staccadj03.toInternal()
										+ staccadj04.toInternal()
										+ staccadj05.toInternal()
										+ staccadj06.toInternal()
										+ staccadj07.toInternal()
										+ staccadj08.toInternal()
										+ staccadj09.toInternal()
										+ staccadj10.toInternal()
										+ staccadj11.toInternal()
										+ staccadj12.toInternal());
	}
	public void setStaccadjs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccadjs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccadj01);
		what = ExternalData.chop(what, staccadj02);
		what = ExternalData.chop(what, staccadj03);
		what = ExternalData.chop(what, staccadj04);
		what = ExternalData.chop(what, staccadj05);
		what = ExternalData.chop(what, staccadj06);
		what = ExternalData.chop(what, staccadj07);
		what = ExternalData.chop(what, staccadj08);
		what = ExternalData.chop(what, staccadj09);
		what = ExternalData.chop(what, staccadj10);
		what = ExternalData.chop(what, staccadj11);
		what = ExternalData.chop(what, staccadj12);
	}
	public PackedDecimalData getStaccadj(BaseData indx) {
		return getStaccadj(indx.toInt());
	}
	public PackedDecimalData getStaccadj(int indx) {

		switch (indx) {
			case 1 : return staccadj01;
			case 2 : return staccadj02;
			case 3 : return staccadj03;
			case 4 : return staccadj04;
			case 5 : return staccadj05;
			case 6 : return staccadj06;
			case 7 : return staccadj07;
			case 8 : return staccadj08;
			case 9 : return staccadj09;
			case 10 : return staccadj10;
			case 11 : return staccadj11;
			case 12 : return staccadj12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccadj(BaseData indx, Object what) {
		setStaccadj(indx, what, false);
	}
	public void setStaccadj(BaseData indx, Object what, boolean rounded) {
		setStaccadj(indx.toInt(), what, rounded);
	}
	public void setStaccadj(int indx, Object what) {
		setStaccadj(indx, what, false);
	}
	public void setStaccadj(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccadj01(what, rounded);
					 break;
			case 2 : setStaccadj02(what, rounded);
					 break;
			case 3 : setStaccadj03(what, rounded);
					 break;
			case 4 : setStaccadj04(what, rounded);
					 break;
			case 5 : setStaccadj05(what, rounded);
					 break;
			case 6 : setStaccadj06(what, rounded);
					 break;
			case 7 : setStaccadj07(what, rounded);
					 break;
			case 8 : setStaccadj08(what, rounded);
					 break;
			case 9 : setStaccadj09(what, rounded);
					 break;
			case 10 : setStaccadj10(what, rounded);
					 break;
			case 11 : setStaccadj11(what, rounded);
					 break;
			case 12 : setStaccadj12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}


	public FixedLengthStringData getStaccadbs() {
		return new FixedLengthStringData(staccadb01.toInternal()
										+ staccadb02.toInternal()
										+ staccadb03.toInternal()
										+ staccadb04.toInternal()
										+ staccadb05.toInternal()
										+ staccadb06.toInternal()
										+ staccadb07.toInternal()
										+ staccadb08.toInternal()
										+ staccadb09.toInternal()
										+ staccadb10.toInternal()
										+ staccadb11.toInternal()
										+ staccadb12.toInternal());
	}
	public void setStaccadbs(Object obj) {
		FixedLengthStringData what = new FixedLengthStringData(getStaccadbs().getLength()).init(obj);
	
		what = ExternalData.chop(what, staccadb01);
		what = ExternalData.chop(what, staccadb02);
		what = ExternalData.chop(what, staccadb03);
		what = ExternalData.chop(what, staccadb04);
		what = ExternalData.chop(what, staccadb05);
		what = ExternalData.chop(what, staccadb06);
		what = ExternalData.chop(what, staccadb07);
		what = ExternalData.chop(what, staccadb08);
		what = ExternalData.chop(what, staccadb09);
		what = ExternalData.chop(what, staccadb10);
		what = ExternalData.chop(what, staccadb11);
		what = ExternalData.chop(what, staccadb12);
	}
	public PackedDecimalData getStaccadb(BaseData indx) {
		return getStaccadb(indx.toInt());
	}
	public PackedDecimalData getStaccadb(int indx) {

		switch (indx) {
			case 1 : return staccadb01;
			case 2 : return staccadb02;
			case 3 : return staccadb03;
			case 4 : return staccadb04;
			case 5 : return staccadb05;
			case 6 : return staccadb06;
			case 7 : return staccadb07;
			case 8 : return staccadb08;
			case 9 : return staccadb09;
			case 10 : return staccadb10;
			case 11 : return staccadb11;
			case 12 : return staccadb12;
			default: return null; // Throw error instead?
		}
	
	}
	public void setStaccadb(BaseData indx, Object what) {
		setStaccadb(indx, what, false);
	}
	public void setStaccadb(BaseData indx, Object what, boolean rounded) {
		setStaccadb(indx.toInt(), what, rounded);
	}
	public void setStaccadb(int indx, Object what) {
		setStaccadb(indx, what, false);
	}
	public void setStaccadb(int indx, Object what, boolean rounded) {

		switch (indx) {
			case 1 : setStaccadb01(what, rounded);
					 break;
			case 2 : setStaccadb02(what, rounded);
					 break;
			case 3 : setStaccadb03(what, rounded);
					 break;
			case 4 : setStaccadb04(what, rounded);
					 break;
			case 5 : setStaccadb05(what, rounded);
					 break;
			case 6 : setStaccadb06(what, rounded);
					 break;
			case 7 : setStaccadb07(what, rounded);
					 break;
			case 8 : setStaccadb08(what, rounded);
					 break;
			case 9 : setStaccadb09(what, rounded);
					 break;
			case 10 : setStaccadb10(what, rounded);
					 break;
			case 11 : setStaccadb11(what, rounded);
					 break;
			case 12 : setStaccadb12(what, rounded);
					 break;
			default: return; // Throw error instead?
		}
	
	}

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		cntbranch.clear();
		register.clear();
		srcebus.clear();
		statFund.clear();
		statSect.clear();
		statSubsect.clear();
		statcode.clear();
		cnPremStat.clear();
		covPremStat.clear();
		acctccy.clear();
		cnttype.clear();
		crtable.clear();
		parind.clear();
		acctyr.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		nonKeyFiller130.clear();
		nonKeyFiller140.clear();
		nonKeyFiller150.clear();
		bfwdfyp.clear();
		staccfyp01.clear();
		staccfyp02.clear();
		staccfyp03.clear();
		staccfyp04.clear();
		staccfyp05.clear();
		staccfyp06.clear();
		staccfyp07.clear();
		staccfyp08.clear();
		staccfyp09.clear();
		staccfyp10.clear();
		staccfyp11.clear();
		staccfyp12.clear();
		cfwdfyp.clear();
		bfwdrnp.clear();
		staccrnp01.clear();
		staccrnp02.clear();
		staccrnp03.clear();
		staccrnp04.clear();
		staccrnp05.clear();
		staccrnp06.clear();
		staccrnp07.clear();
		staccrnp08.clear();
		staccrnp09.clear();
		staccrnp10.clear();
		staccrnp11.clear();
		staccrnp12.clear();
		cfwdrnp.clear();
		bfwdspd.clear();
		staccspd01.clear();
		staccspd02.clear();
		staccspd03.clear();
		staccspd04.clear();
		staccspd05.clear();
		staccspd06.clear();
		staccspd07.clear();
		staccspd08.clear();
		staccspd09.clear();
		staccspd10.clear();
		staccspd11.clear();
		staccspd12.clear();
		cfwdspd.clear();
		bfwdfyc.clear();
		staccfyc01.clear();
		staccfyc02.clear();
		staccfyc03.clear();
		staccfyc04.clear();
		staccfyc05.clear();
		staccfyc06.clear();
		staccfyc07.clear();
		staccfyc08.clear();
		staccfyc09.clear();
		staccfyc10.clear();
		staccfyc11.clear();
		staccfyc12.clear();
		cfwdfyc.clear();
		bfwdrlc.clear();
		staccrlc01.clear();
		staccrlc02.clear();
		staccrlc03.clear();
		staccrlc04.clear();
		staccrlc05.clear();
		staccrlc06.clear();
		staccrlc07.clear();
		staccrlc08.clear();
		staccrlc09.clear();
		staccrlc10.clear();
		staccrlc11.clear();
		staccrlc12.clear();
		cfwdrlc.clear();
		bfwdspc.clear();
		staccspc01.clear();
		staccspc02.clear();
		staccspc03.clear();
		staccspc04.clear();
		staccspc05.clear();
		staccspc06.clear();
		staccspc07.clear();
		staccspc08.clear();
		staccspc09.clear();
		staccspc10.clear();
		staccspc11.clear();
		staccspc12.clear();
		cfwdspc.clear();
		bfwdrb.clear();
		staccrb01.clear();
		staccrb02.clear();
		staccrb03.clear();
		staccrb04.clear();
		staccrb05.clear();
		staccrb06.clear();
		staccrb07.clear();
		staccrb08.clear();
		staccrb09.clear();
		staccrb10.clear();
		staccrb11.clear();
		staccrb12.clear();
		cfwdrb.clear();
		bfwdxb.clear();
		staccxb01.clear();
		staccxb02.clear();
		staccxb03.clear();
		staccxb04.clear();
		staccxb05.clear();
		staccxb06.clear();
		staccxb07.clear();
		staccxb08.clear();
		staccxb09.clear();
		staccxb10.clear();
		staccxb11.clear();
		staccxb12.clear();
		cfwdxb.clear();
		bfwdtb.clear();
		stacctb01.clear();
		stacctb02.clear();
		stacctb03.clear();
		stacctb04.clear();
		stacctb05.clear();
		stacctb06.clear();
		stacctb07.clear();
		stacctb08.clear();
		stacctb09.clear();
		stacctb10.clear();
		stacctb11.clear();
		stacctb12.clear();
		cfwdtb.clear();
		bfwdib.clear();
		staccib01.clear();
		staccib02.clear();
		staccib03.clear();
		staccib04.clear();
		staccib05.clear();
		staccib06.clear();
		staccib07.clear();
		staccib08.clear();
		staccib09.clear();
		staccib10.clear();
		staccib11.clear();
		staccib12.clear();
		cfwdib.clear();
		bfwddd.clear();
		staccdd01.clear();
		staccdd02.clear();
		staccdd03.clear();
		staccdd04.clear();
		staccdd05.clear();
		staccdd06.clear();
		staccdd07.clear();
		staccdd08.clear();
		staccdd09.clear();
		staccdd10.clear();
		staccdd11.clear();
		staccdd12.clear();
		cfwddd.clear();
		bfwddi.clear();
		staccdi01.clear();
		staccdi02.clear();
		staccdi03.clear();
		staccdi04.clear();
		staccdi05.clear();
		staccdi06.clear();
		staccdi07.clear();
		staccdi08.clear();
		staccdi09.clear();
		staccdi10.clear();
		staccdi11.clear();
		staccdi12.clear();
		cfwddi.clear();
		bfwdbs.clear();
		staccbs01.clear();
		staccbs02.clear();
		staccbs03.clear();
		staccbs04.clear();
		staccbs05.clear();
		staccbs06.clear();
		staccbs07.clear();
		staccbs08.clear();
		staccbs09.clear();
		staccbs10.clear();
		staccbs11.clear();
		staccbs12.clear();
		cfwdbs.clear();
		bfwdob.clear();
		staccob01.clear();
		staccob02.clear();
		staccob03.clear();
		staccob04.clear();
		staccob05.clear();
		staccob06.clear();
		staccob07.clear();
		staccob08.clear();
		staccob09.clear();
		staccob10.clear();
		staccob11.clear();
		staccob12.clear();
		cfwdob.clear();
		bfwdmtb.clear();
		staccmtb01.clear();
		staccmtb02.clear();
		staccmtb03.clear();
		staccmtb04.clear();
		staccmtb05.clear();
		staccmtb06.clear();
		staccmtb07.clear();
		staccmtb08.clear();
		staccmtb09.clear();
		staccmtb10.clear();
		staccmtb11.clear();
		staccmtb12.clear();
		cfwdmtb.clear();
		bfwdclr.clear();
		staccclr01.clear();
		staccclr02.clear();
		staccclr03.clear();
		staccclr04.clear();
		staccclr05.clear();
		staccclr06.clear();
		staccclr07.clear();
		staccclr08.clear();
		staccclr09.clear();
		staccclr10.clear();
		staccclr11.clear();
		staccclr12.clear();
		cfwdclr.clear();
		bfwdrip.clear();
		staccrip01.clear();
		staccrip02.clear();
		staccrip03.clear();
		staccrip04.clear();
		staccrip05.clear();
		staccrip06.clear();
		staccrip07.clear();
		staccrip08.clear();
		staccrip09.clear();
		staccrip10.clear();
		staccrip11.clear();
		staccrip12.clear();
		cfwdrip.clear();
		bfwdric.clear();
		staccric01.clear();
		staccric02.clear();
		staccric03.clear();
		staccric04.clear();
		staccric05.clear();
		staccric06.clear();
		staccric07.clear();
		staccric08.clear();
		staccric09.clear();
		staccric10.clear();
		staccric11.clear();
		staccric12.clear();
		cfwdric.clear();
		bfwdadv.clear();
		staccadv01.clear();
		staccadv02.clear();
		staccadv03.clear();
		staccadv04.clear();
		staccadv05.clear();
		staccadv06.clear();
		staccadv07.clear();
		staccadv08.clear();
		staccadv09.clear();
		staccadv10.clear();
		staccadv11.clear();
		staccadv12.clear();
		cfwdadv.clear();
		bfwdpdb.clear();
		staccpdb01.clear();
		staccpdb02.clear();
		staccpdb03.clear();
		staccpdb04.clear();
		staccpdb05.clear();
		staccpdb06.clear();
		staccpdb07.clear();
		staccpdb08.clear();
		staccpdb09.clear();
		staccpdb10.clear();
		staccpdb11.clear();
		staccpdb12.clear();
		cfwdpdb.clear();
		bfwdadb.clear();
		staccadb01.clear();
		staccadb02.clear();
		staccadb03.clear();
		staccadb04.clear();
		staccadb05.clear();
		staccadb06.clear();
		staccadb07.clear();
		staccadb08.clear();
		staccadb09.clear();
		staccadb10.clear();
		staccadb11.clear();
		staccadb12.clear();
		cfwdadb.clear();
		bfwdadj.clear();
		staccadj01.clear();
		staccadj02.clear();
		staccadj03.clear();
		staccadj04.clear();
		staccadj05.clear();
		staccadj06.clear();
		staccadj07.clear();
		staccadj08.clear();
		staccadj09.clear();
		staccadj10.clear();
		staccadj11.clear();
		staccadj12.clear();
		cfwdadj.clear();
		bfwdint.clear();
		staccint01.clear();
		staccint02.clear();
		staccint03.clear();
		staccint04.clear();
		staccint05.clear();
		staccint06.clear();
		staccint07.clear();
		staccint08.clear();
		staccint09.clear();
		staccint10.clear();
		staccint11.clear();
		staccint12.clear();
		cfwdint.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}