package com.csc.life.statistics.dataaccess;

import com.csc.common.DD;
import com.csc.smart400framework.dataaccess.PFAdapterDAM;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: AgprpfTableDAM.java
 * Date: Sun, 30 Aug 2009 03:23:47
 * Class transformed from AGPRPF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class AgprpfTableDAM extends PFAdapterDAM {

	public int pfRecLen = 629;
	public FixedLengthStringData agprrec = new FixedLengthStringData(pfRecLen);
	public FixedLengthStringData agprpfRecord = agprrec;
	
	public FixedLengthStringData agntcoy = DD.agntcoy.copy().isAPartOf(agprrec);
	public FixedLengthStringData agntnum = DD.agntnum.copy().isAPartOf(agprrec);
	public PackedDecimalData acctyr = DD.acctyr.copy().isAPartOf(agprrec);
	public PackedDecimalData mnth = DD.mnth.copy().isAPartOf(agprrec);
	public FixedLengthStringData chdrnum = DD.chdrnum.copy().isAPartOf(agprrec);
	public FixedLengthStringData cnttype = DD.cnttype.copy().isAPartOf(agprrec);
	public PackedDecimalData effdate = DD.effdate.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp01 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp02 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp03 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp04 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp05 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp06 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp07 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp08 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp09 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpp10 = DD.mlperpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc01 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc02 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc03 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc04 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc05 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc06 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc07 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc08 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc09 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlperpc10 = DD.mlperpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp01 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp02 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp03 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp04 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp05 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp06 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp07 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp08 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp09 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpp10 = DD.mldirpp.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc01 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc02 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc03 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc04 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc05 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc06 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc07 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc08 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc09 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mldirpc10 = DD.mldirpc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp01 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp02 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp03 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp04 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp05 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp06 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp07 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp08 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp09 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppp10 = DD.mlgrppp.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc01 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc02 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc03 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc04 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc05 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc06 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc07 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc08 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc09 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData mlgrppc10 = DD.mlgrppc.copy().isAPartOf(agprrec);
	public PackedDecimalData cntcount = DD.cntcount.copy().isAPartOf(agprrec);
	public PackedDecimalData sumins = DD.sumins.copy().isAPartOf(agprrec);
	public FixedLengthStringData userProfile = DD.usrprf.copy().isAPartOf(agprrec);
	public FixedLengthStringData jobName = DD.jobnm.copy().isAPartOf(agprrec);
	public FixedLengthStringData datime = DD.datime.copy().isAPartOf(agprrec);

	/**
	* Default Constructor for TableDAMs. Locks are not taken on data.
	*/	
	public AgprpfTableDAM() {
  		super();
  		setColumns();
  		journalled = true;
	}

	/**
	* Constructor for AgprpfTableDAM that accepts boolean update flag indicating
	* whether or not to lock the table.
	*/	
	public AgprpfTableDAM(boolean updateMode) {
  		super(updateMode);
  		setColumns();
	}

	/**
	* Constructor for AgprpfTableDAM that accepts boolean update indicator,
	* and a data structure for file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgprpfTableDAM(boolean updateMode, FixedLengthStringData feedbackArea) {
  		super(updateMode, feedbackArea);
  		setColumns();
	}

	/**
	* Constructor for AgprpfTableDAM that accepts a data structure for
	* file feedback information (equivalent of INFDS on RPG)
	*/	
	public AgprpfTableDAM(FixedLengthStringData feedbackArea) {
  		super(feedbackArea);
  		setColumns();
	}

	public void setTable() {
		TABLEPF = getTableName("AGPRPF");
	}

	public String getPFTable() {
		return TABLEPF;
	}

	public void setColumnConstants() {
	
		QUALIFIEDCOLUMNS = 
							"AGNTCOY, " +
							"AGNTNUM, " +
							"ACCTYR, " +
							"MNTH, " +
							"CHDRNUM, " +
							"CNTTYPE, " +
							"EFFDATE, " +
							"MLPERPP01, " +
							"MLPERPP02, " +
							"MLPERPP03, " +
							"MLPERPP04, " +
							"MLPERPP05, " +
							"MLPERPP06, " +
							"MLPERPP07, " +
							"MLPERPP08, " +
							"MLPERPP09, " +
							"MLPERPP10, " +
							"MLPERPC01, " +
							"MLPERPC02, " +
							"MLPERPC03, " +
							"MLPERPC04, " +
							"MLPERPC05, " +
							"MLPERPC06, " +
							"MLPERPC07, " +
							"MLPERPC08, " +
							"MLPERPC09, " +
							"MLPERPC10, " +
							"MLDIRPP01, " +
							"MLDIRPP02, " +
							"MLDIRPP03, " +
							"MLDIRPP04, " +
							"MLDIRPP05, " +
							"MLDIRPP06, " +
							"MLDIRPP07, " +
							"MLDIRPP08, " +
							"MLDIRPP09, " +
							"MLDIRPP10, " +
							"MLDIRPC01, " +
							"MLDIRPC02, " +
							"MLDIRPC03, " +
							"MLDIRPC04, " +
							"MLDIRPC05, " +
							"MLDIRPC06, " +
							"MLDIRPC07, " +
							"MLDIRPC08, " +
							"MLDIRPC09, " +
							"MLDIRPC10, " +
							"MLGRPPP01, " +
							"MLGRPPP02, " +
							"MLGRPPP03, " +
							"MLGRPPP04, " +
							"MLGRPPP05, " +
							"MLGRPPP06, " +
							"MLGRPPP07, " +
							"MLGRPPP08, " +
							"MLGRPPP09, " +
							"MLGRPPP10, " +
							"MLGRPPC01, " +
							"MLGRPPC02, " +
							"MLGRPPC03, " +
							"MLGRPPC04, " +
							"MLGRPPC05, " +
							"MLGRPPC06, " +
							"MLGRPPC07, " +
							"MLGRPPC08, " +
							"MLGRPPC09, " +
							"MLGRPPC10, " +
							"CNTCOUNT, " +
							"SUMINS, " +
							"USRPRF, " +
							"JOBNM, " +
							"DATIME, " +
							"UNIQUE_NUMBER";
	}

	public void setColumns() {
	
		qualifiedColumns = new BaseData[] {
                                     agntcoy,
                                     agntnum,
                                     acctyr,
                                     mnth,
                                     chdrnum,
                                     cnttype,
                                     effdate,
                                     mlperpp01,
                                     mlperpp02,
                                     mlperpp03,
                                     mlperpp04,
                                     mlperpp05,
                                     mlperpp06,
                                     mlperpp07,
                                     mlperpp08,
                                     mlperpp09,
                                     mlperpp10,
                                     mlperpc01,
                                     mlperpc02,
                                     mlperpc03,
                                     mlperpc04,
                                     mlperpc05,
                                     mlperpc06,
                                     mlperpc07,
                                     mlperpc08,
                                     mlperpc09,
                                     mlperpc10,
                                     mldirpp01,
                                     mldirpp02,
                                     mldirpp03,
                                     mldirpp04,
                                     mldirpp05,
                                     mldirpp06,
                                     mldirpp07,
                                     mldirpp08,
                                     mldirpp09,
                                     mldirpp10,
                                     mldirpc01,
                                     mldirpc02,
                                     mldirpc03,
                                     mldirpc04,
                                     mldirpc05,
                                     mldirpc06,
                                     mldirpc07,
                                     mldirpc08,
                                     mldirpc09,
                                     mldirpc10,
                                     mlgrppp01,
                                     mlgrppp02,
                                     mlgrppp03,
                                     mlgrppp04,
                                     mlgrppp05,
                                     mlgrppp06,
                                     mlgrppp07,
                                     mlgrppp08,
                                     mlgrppp09,
                                     mlgrppp10,
                                     mlgrppc01,
                                     mlgrppc02,
                                     mlgrppc03,
                                     mlgrppc04,
                                     mlgrppc05,
                                     mlgrppc06,
                                     mlgrppc07,
                                     mlgrppc08,
                                     mlgrppc09,
                                     mlgrppc10,
                                     cntcount,
                                     sumins,
                                     userProfile,
                                     jobName,
                                     datime,
                                     unique_number
		                 };
	}

	/**
	* Method to clear all fields in the Physical File record
	* @return
	*/	
	public void initialize() {
  		agntcoy.clear();
  		agntnum.clear();
  		acctyr.clear();
  		mnth.clear();
  		chdrnum.clear();
  		cnttype.clear();
  		effdate.clear();
  		mlperpp01.clear();
  		mlperpp02.clear();
  		mlperpp03.clear();
  		mlperpp04.clear();
  		mlperpp05.clear();
  		mlperpp06.clear();
  		mlperpp07.clear();
  		mlperpp08.clear();
  		mlperpp09.clear();
  		mlperpp10.clear();
  		mlperpc01.clear();
  		mlperpc02.clear();
  		mlperpc03.clear();
  		mlperpc04.clear();
  		mlperpc05.clear();
  		mlperpc06.clear();
  		mlperpc07.clear();
  		mlperpc08.clear();
  		mlperpc09.clear();
  		mlperpc10.clear();
  		mldirpp01.clear();
  		mldirpp02.clear();
  		mldirpp03.clear();
  		mldirpp04.clear();
  		mldirpp05.clear();
  		mldirpp06.clear();
  		mldirpp07.clear();
  		mldirpp08.clear();
  		mldirpp09.clear();
  		mldirpp10.clear();
  		mldirpc01.clear();
  		mldirpc02.clear();
  		mldirpc03.clear();
  		mldirpc04.clear();
  		mldirpc05.clear();
  		mldirpc06.clear();
  		mldirpc07.clear();
  		mldirpc08.clear();
  		mldirpc09.clear();
  		mldirpc10.clear();
  		mlgrppp01.clear();
  		mlgrppp02.clear();
  		mlgrppp03.clear();
  		mlgrppp04.clear();
  		mlgrppp05.clear();
  		mlgrppp06.clear();
  		mlgrppp07.clear();
  		mlgrppp08.clear();
  		mlgrppp09.clear();
  		mlgrppp10.clear();
  		mlgrppc01.clear();
  		mlgrppc02.clear();
  		mlgrppc03.clear();
  		mlgrppc04.clear();
  		mlgrppc05.clear();
  		mlgrppc06.clear();
  		mlgrppc07.clear();
  		mlgrppc08.clear();
  		mlgrppc09.clear();
  		mlgrppc10.clear();
  		cntcount.clear();
  		sumins.clear();
  		userProfile.clear();
  		jobName.clear();
  		datime.clear();
	}

	/**
	* Getter corresponding to record's DD-ALL-FORMATS record
	* @return
	*/	
	public FixedLengthStringData getAgprrec() {
  		return agprrec;
	}

	public FixedLengthStringData getAgprpfRecord() {
  		return agprpfRecord;
	}

	/**
	* Setter corresponding to record's DD-ALL-FORMATS record
	*/	
	public void set(Object what) {
  		setAgprrec(what);
	}

	public void setAgprrec(Object what) {
  		this.agprrec.set(what);
	}

	public void setAgprpfRecord(Object what) {
  		this.agprpfRecord.set(what);
	}

	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(agprrec.getLength());
		result.set(agprrec);
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}
	
	// The following methods are not required by the PF tableDAM, but are fully implemented 
	// in the LF tableDAM
	@Override
	public FixedLengthStringData setHeader(Object what) {
		return null;
	}

	@Override
	public FixedLengthStringData getHeader() {
		return null;
	}

	@Override
	protected void clearRecKeyData() {
	}

	@Override
	protected void clearRecNonKeyData() {
	}
	

}