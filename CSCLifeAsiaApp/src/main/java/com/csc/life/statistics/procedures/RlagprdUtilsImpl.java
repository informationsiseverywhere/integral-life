/*********************  */
/*Author  :Liwei						*/
/*Purpose :Instead of subroutine Rlagprd*/
/*Date    :2018.10.26					*/
package com.csc.life.statistics.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.math.BigDecimal;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.csc.fsu.general.procedures.Datcon3Pojo;
import com.csc.fsu.general.procedures.Datcon3Utils;
import com.csc.integral.context.IntegralApplicationContext;
import com.csc.life.agents.dataaccess.dao.MacfpfDAO;
import com.csc.life.agents.dataaccess.model.Macfpf;
import com.csc.life.agents.tablestructures.Tm603rec;
import com.csc.life.newbusiness.dataaccess.dao.HpadpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Hpadpf;
import com.csc.life.regularprocessing.dataaccess.dao.AglfpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Aglfpf;
import com.csc.life.statistics.dataaccess.dao.AgprpfDAO;
import com.csc.life.statistics.dataaccess.model.Agprpf;
import com.csc.smart.recordstructures.Syserrrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.procedures.Syserr;
import com.quipoz.COBOLFramework.util.COBOLConvCodeModel.COBOLExitProgramException;
import com.quipoz.COBOLFramework.util.StringUtil;

public class RlagprdUtilsImpl implements RlagprdUtils {
	private static final Logger LOGGER = LoggerFactory.getLogger(RlagprdUtilsImpl.class);
	private Syserrrec syserrrec = new Syserrrec();
	private int wsaaInteger = 0;
	private int wsaaEffdate = 0;
	private int wsaaCurrfrom = 0;
	private BigDecimal wsaaMlperpc = BigDecimal.ZERO;
	private BigDecimal wsaaMlperpp = BigDecimal.ZERO;
	private BigDecimal wsaaMlgrppc = BigDecimal.ZERO;
	private BigDecimal wsaaMlgrppp = BigDecimal.ZERO;
	private BigDecimal wsaaMldirpc = BigDecimal.ZERO;
	private BigDecimal wsaaMldirpp = BigDecimal.ZERO;
	private BigDecimal wsaaTotSumins = BigDecimal.ZERO;
	
	private Varcom varcom = new Varcom();
	private String wsaaTm603Agtype = "";
	private String wsaaTm603Cnttype = "";
	private String wsaaAgntnum = "";
	private String wsaaTeamFlg = "";
	private String wsaaOvrFlg = "";
	private int wsaaPolicyCount = 0;
	private String wsaaExist = "";
	private String[] wsaaReportags = new String[5];
	private int wsaaIndex = 0;
	private Tm603rec tm603rec = new Tm603rec();
	private Aglfpf aglflnb = null;
	private List<Itempf> tm603List = null;
	private Datcon3Utils datcon3Utils = getApplicationContext().getBean("datcon3Utils", Datcon3Utils.class);
	private HpadpfDAO hpadpfDAO = getApplicationContext().getBean("hpadpfDAO", HpadpfDAO.class); 
	private AglfpfDAO aglfpfDAO = getApplicationContext().getBean("aglfpfDAO", AglfpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private MacfpfDAO macfpfDAO = getApplicationContext().getBean("macfpfDAO",MacfpfDAO.class);
	private AgprpfDAO agprpfDAO = getApplicationContext().getBean("agprpfDAO",AgprpfDAO.class);
	private List<Macfpf> macfpfList = null;
	private Macfpf macfprd = null;
	private Macfpf macfenq = null;
	@Override
	public void callRlagprd(RlagprdPojo rlagprdPojo) {
		syserrrec.subrname.set("RLAGPRD");
		Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
		datcon3Pojo.setIntDate1(String.valueOf(rlagprdPojo.getOccdate()));
		datcon3Pojo.setIntDate2(String.valueOf(rlagprdPojo.getEffdate()));
		datcon3Pojo.setFrequency("01");
		datcon3Utils.calDatcon3(datcon3Pojo);
		if (isNE(datcon3Pojo.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(datcon3Pojo.getStatuz());
			fatalError600(rlagprdPojo);
		}
		wsaaInteger = datcon3Pojo.getFreqFactor().intValue() + 1;
		Hpadpf hpadpf = this.hpadpfDAO.getHpadData(rlagprdPojo.getChdrcoy(), rlagprdPojo.getChdrnum());
		if(hpadpf == null){
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600(rlagprdPojo);
		}
		wsaaCurrfrom = (hpadpf.getHissdte());
		aglflnb = aglfpfDAO.searchAglflnb(rlagprdPojo.getAgntcoy(), rlagprdPojo.getAgntnum());
		if(aglflnb == null){
			syserrrec.statuz.set(varcom.mrnf);
			fatalError600(rlagprdPojo);
		}
		macfpfList = macfpfDAO.searchMacfpfRecord(rlagprdPojo.getAgntcoy(), rlagprdPojo.getAgntnum(), wsaaCurrfrom);
		if(macfpfList == null || macfpfList.size() == 0){
			macfpfList = macfpfDAO.searchMacfpfRecord(rlagprdPojo.getAgntcoy(), rlagprdPojo.getAgntnum(), -1);
			if(macfpfList == null || macfpfList.size() == 0){
				syserrrec.statuz.set(varcom.endp);
				fatalError600(rlagprdPojo);
			}
		}
		macfprd = macfpfList.get(0);
		
		readTm603100(rlagprdPojo);
		if (tm603List == null) {
			rlagprdPojo.setStatuz("ENDP");
			return ;
		}
		if (isNE(tm603rec.active01, "Y") && isNE(tm603rec.active02, "Y") && isNE(tm603rec.active03, "Y")) {
			return ;
		}
		personalProduction400(rlagprdPojo);
		wsaaExist = "N";
		
		wsaaReportags[1] = macfprd.getZrptga().trim();/* IJTI-1523 */
		wsaaReportags[2] = macfprd.getZrptgb().trim();/* IJTI-1523 */
		wsaaReportags[3] = macfprd.getZrptgc().trim();/* IJTI-1523 */
		wsaaReportags[4] = macfprd.getZrptgd().trim();/* IJTI-1523 */
		for(int wsaaIndex1 = 1 ; !(wsaaIndex1 > 4 || "".equals(wsaaReportags[wsaaIndex1])); wsaaIndex1++){
			updateProduction300(rlagprdPojo,wsaaIndex1);
		}
		
	}
	
	protected void updateProduction300(RlagprdPojo rlagprdPojo,int wsaaIndex1){
		macfpfList = macfpfDAO.searchMacfpfRecord(rlagprdPojo.getAgntcoy(), wsaaReportags[wsaaIndex1], wsaaCurrfrom);
		if(macfpfList == null || macfpfList.size() == 0){
			macfpfList = macfpfDAO.searchMacfpfRecord(rlagprdPojo.getAgntcoy(), wsaaReportags[wsaaIndex1], -1);
			if(macfpfList == null || macfpfList.size() == 0){
				syserrrec.statuz.set("EDNP");
				fatalError600(rlagprdPojo);
			}
		}
		macfenq = macfpfList.get(0);
		
		wsaaTeamFlg = "N";
		wsaaOvrFlg = "N";
		wsaaMlgrppc = BigDecimal.ZERO;
		wsaaMlgrppp = BigDecimal.ZERO;
		wsaaMldirpc = BigDecimal.ZERO;
		wsaaMldirpp = BigDecimal.ZERO;
		wsaaMlperpc = BigDecimal.ZERO;
		wsaaMlperpp = BigDecimal.ZERO;
		for (wsaaIndex1= 1; !(isGT(wsaaIndex, 10)|| isEQ(wsaaTeamFlg, "Y")); wsaaIndex++){
			if (isEQ(macfenq.getMlagttyp(), tm603rec.mlagttyp[wsaaIndex])) {
				wsaaTeamFlg = "Y";
			}
		}
		if (isEQ(wsaaTeamFlg, "Y")) {
			if (isEQ(rlagprdPojo.getPrdflg(), "C")) {
				wsaaMlgrppc = rlagprdPojo.getOrigamt();
			}
			else {
				wsaaMlgrppp = rlagprdPojo.getOrigamt();
			}
		}
		for (wsaaIndex = 1; !(isGT(wsaaIndex, 10)|| isEQ(wsaaOvrFlg, "Y")|| isEQ(wsaaExist, "Y")); wsaaIndex++){
			if (isEQ(macfenq.getMlagttyp(), tm603rec.agtype[wsaaIndex])) {
				wsaaOvrFlg = "Y";
				wsaaExist = "Y";
			}
		}
		if (isEQ(wsaaOvrFlg, "Y")) {
			if (isEQ(rlagprdPojo.getPrdflg(), "C")) {
				wsaaMldirpc = rlagprdPojo.getOrigamt();
			}
			else {
				wsaaMldirpp = rlagprdPojo.getOrigamt();
			}
		}
		/*    MOVE MACFPRD-REPORTAG(WSAA-INDEX1)  TO WSAA-AGNTNUM.         */
		wsaaAgntnum = wsaaReportags[wsaaIndex1];
		wsaaPolicyCount = 0;
		wsaaTotSumins = BigDecimal.ZERO;
		wsaaEffdate  = macfenq.getEffdate();
		if (isEQ(subString(rlagprdPojo.getBatctrcde(), 1, 1), "B")) {
			deferredMaprUpdate900(rlagprdPojo);
		}
	}

	protected void personalProduction400(RlagprdPojo rlagprdPojo){
		if (isEQ(tm603rec.active03, "Y")) {
			if (isEQ(rlagprdPojo.getPrdflg(), "C")) {
				wsaaMlperpc = wsaaMlperpc.add(rlagprdPojo.getOrigamt());
			}
			else {
				wsaaMlperpp = wsaaMlperpp.add(rlagprdPojo.getOrigamt());
			}
		}
		if (isEQ(tm603rec.active01, "Y")) {
			if (isEQ(rlagprdPojo.getPrdflg(), "C")) {
				wsaaMldirpc.add(rlagprdPojo.getOrigamt());
			}
			else {
				wsaaMldirpp.add(rlagprdPojo.getOrigamt());
			}
		}
		if (isEQ(tm603rec.active02, "Y")) {
			if (isEQ(rlagprdPojo.getPrdflg(), "C")) {
				wsaaMlgrppc.add(rlagprdPojo.getOrigamt());
			}
			else {
				wsaaMlgrppp.add(rlagprdPojo.getOrigamt());
			}
		}
		wsaaAgntnum = rlagprdPojo.getAgntnum();
		wsaaEffdate = macfprd.getEffdate();

		if (isEQ(subString(rlagprdPojo.getBatctrcde(), 1, 1), "B")) {
			deferredMaprUpdate900(rlagprdPojo);
		}
	}
	
	protected void readTm603100(RlagprdPojo rlagprdPojo){
		tm603rec.tm603Rec.set(SPACES);
		if (isEQ(macfprd.getMlagttyp(), SPACES)) {
			wsaaTm603Agtype = rlagprdPojo.getAgtype();
		}
		else {
			wsaaTm603Agtype = macfprd.getMlagttyp().trim();
		}
		wsaaTm603Cnttype = rlagprdPojo.getCnttype();
		tm603List = itemDAO.getItdmByFrmdate(rlagprdPojo.getAgntcoy(), "TM603", wsaaTm603Agtype + wsaaTm603Cnttype, rlagprdPojo.getOccdate());
		if(tm603List == null || tm603List.size() == 0){
			wsaaTm603Cnttype = "***";
			tm603List = itemDAO.getItdmByFrmdate(rlagprdPojo.getAgntcoy(), "TM603", wsaaTm603Agtype + wsaaTm603Cnttype, rlagprdPojo.getOccdate());
			if(tm603List == null || tm603List.size() == 0){
				syserrrec.params.set(wsaaTm603Agtype + wsaaTm603Cnttype);
				syserrrec.statuz.set("E031");
				fatalError600(rlagprdPojo);
			}else{
				tm603rec.tm603Rec.set(StringUtil.rawToString(tm603List.get(0).getGenarea()));
			}
	    }else{
	    	tm603rec.tm603Rec.set(StringUtil.rawToString(tm603List.get(0).getGenarea()));
	    }
		
	}

	protected void deferredMaprUpdate900(RlagprdPojo rlagprdPojo){
		Agprpf agprpf = new Agprpf();
		for (wsaaIndex = 1; !(isGT(wsaaIndex, 10)); wsaaIndex++){
			agprpf.setMlperpp(wsaaIndex, 0);
			agprpf.setMlperpc(wsaaIndex, 0);
			agprpf.setMlgrppp(wsaaIndex, 0);
			agprpf.setMlgrppc(wsaaIndex, 0);
			agprpf.setMldirpp(wsaaIndex, 0);
			agprpf.setMldirpc(wsaaIndex, 0);
		}
		agprpf.setCntcount(0);
		agprpf.setSumins(0);
		agprpf.setAgntcoy(rlagprdPojo.getAgntcoy());
		agprpf.setAgntnum(wsaaAgntnum);
		agprpf.setAcctyr(rlagprdPojo.getActyear());
		agprpf.setMnth(rlagprdPojo.getActmnth());
		agprpf.setCnttype(rlagprdPojo.getCnttype());
		agprpf.setChdrnum(rlagprdPojo.getChdrnum());
		agprpf.setEffdate(wsaaEffdate);
		if (isGT(wsaaInteger, 10)) {
			setPrecision(agprpf.getMlperpc(10), 2);
			agprpf.setMlperpc(10, add(agprpf.getMlperpc(10), wsaaMlperpc).toFloat());
			setPrecision(agprpf.getMlperpp(10), 2);
			agprpf.setMlperpp(10, add(agprpf.getMlperpp(10), wsaaMlperpp).toFloat());
			setPrecision(agprpf.getMlgrppc(10), 2);
			agprpf.setMlgrppc(10, add(agprpf.getMlgrppc(10), wsaaMlgrppc).toFloat());
			setPrecision(agprpf.getMlgrppp(10), 2);
			agprpf.setMlgrppp(10, add(agprpf.getMlgrppp(10), wsaaMlgrppp).toFloat());
			setPrecision(agprpf.getMldirpc(10), 2);
			agprpf.setMldirpc(10, add(agprpf.getMldirpc(10), wsaaMldirpc).toFloat());
			setPrecision(agprpf.getMldirpp(10), 2);
			agprpf.setMldirpp(10, add(agprpf.getMldirpp(10), wsaaMldirpp).toFloat());
		}
		else {
			setPrecision(agprpf.getMlperpc(wsaaInteger), 2);
			agprpf.setMlperpc(wsaaInteger, add(agprpf.getMlperpc(wsaaInteger), wsaaMlperpc).toFloat());
			setPrecision(agprpf.getMlperpp(wsaaInteger), 2);
			agprpf.setMlperpp(wsaaInteger, add(agprpf.getMlperpp(wsaaInteger), wsaaMlperpp).toFloat());
			setPrecision(agprpf.getMlgrppc(wsaaInteger), 2);
			agprpf.setMlgrppc(wsaaInteger, add(agprpf.getMlgrppc(wsaaInteger), wsaaMlgrppc).toFloat());
			setPrecision(agprpf.getMlgrppp(wsaaInteger), 2);
			agprpf.setMlgrppp(wsaaInteger, add(agprpf.getMlgrppp(wsaaInteger), wsaaMlgrppp).toFloat());
			setPrecision(agprpf.getMldirpc(wsaaInteger), 2);
			agprpf.setMldirpc(wsaaInteger, add(agprpf.getMldirpc(wsaaInteger), wsaaMldirpc).toFloat());
			setPrecision(agprpf.getMldirpp(wsaaInteger), 2);
			agprpf.setMldirpp(wsaaInteger, add(agprpf.getMldirpp(wsaaInteger), wsaaMldirpp).toFloat());
		}
		agprpf.setCntcount(0);
		agprpf.setSumins(wsaaTotSumins.floatValue());
		agprpfDAO.insertAgprpfRecord(agprpf);
	}
	protected void fatalError600(RlagprdPojo rlagprdPojo){
		syserrrec.syserrStatuz.set(syserrrec.statuz);
		syserrrec.syserrType.set("2");
		Syserr syserr = new Syserr();
		syserr.mainline(new Object[] { syserrrec.syserrRec });;
		rlagprdPojo.setStatuz("BOMB");
		LOGGER.error("COBOLConvCodeModel.callProgram exception", new COBOLExitProgramException());
		throw new RuntimeException(new COBOLExitProgramException());
	}
	private ApplicationContext getApplicationContext() {
		return IntegralApplicationContext.getApplicationContext();
	}
}
