package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:04:15
 * Description:
 * Copybook name: GOVRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Govrkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData govrFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData govrKey = new FixedLengthStringData(256).isAPartOf(govrFileKey, 0, REDEFINE);
  	public FixedLengthStringData govrChdrcoy = new FixedLengthStringData(1).isAPartOf(govrKey, 0);
  	public FixedLengthStringData govrStatcat = new FixedLengthStringData(2).isAPartOf(govrKey, 1);
  	public PackedDecimalData govrAcctyr = new PackedDecimalData(4, 0).isAPartOf(govrKey, 3);
  	public FixedLengthStringData govrStatFund = new FixedLengthStringData(1).isAPartOf(govrKey, 6);
  	public FixedLengthStringData govrStatSect = new FixedLengthStringData(2).isAPartOf(govrKey, 7);
  	public FixedLengthStringData govrStatSubsect = new FixedLengthStringData(4).isAPartOf(govrKey, 9);
  	public FixedLengthStringData govrRegister = new FixedLengthStringData(3).isAPartOf(govrKey, 13);
  	public FixedLengthStringData govrCntbranch = new FixedLengthStringData(2).isAPartOf(govrKey, 16);
  	public FixedLengthStringData govrBandage = new FixedLengthStringData(2).isAPartOf(govrKey, 18);
  	public FixedLengthStringData govrBandsa = new FixedLengthStringData(2).isAPartOf(govrKey, 20);
  	public FixedLengthStringData govrBandprm = new FixedLengthStringData(2).isAPartOf(govrKey, 22);
  	public FixedLengthStringData govrBandtrm = new FixedLengthStringData(2).isAPartOf(govrKey, 24);
  	public PackedDecimalData govrCommyr = new PackedDecimalData(4, 0).isAPartOf(govrKey, 26);
  	public FixedLengthStringData govrPstatcode = new FixedLengthStringData(2).isAPartOf(govrKey, 29);
  	public FixedLengthStringData govrCntcurr = new FixedLengthStringData(3).isAPartOf(govrKey, 31);
  	public FixedLengthStringData filler = new FixedLengthStringData(222).isAPartOf(govrKey, 34, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(govrFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		govrFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}