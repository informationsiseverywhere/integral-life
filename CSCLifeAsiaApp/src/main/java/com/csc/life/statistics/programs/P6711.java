/*
 * File: P6711.java
 * Date: 30 August 2009 0:54:40
 * Author: Quipoz Limited
 * 
 * Class transformed from P6711.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Acmdesc;
import com.csc.fsu.general.recordstructures.Acmdescrec;
import com.csc.life.statistics.dataaccess.GojnTableDAM;
import com.csc.life.statistics.screens.S6711ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*           ENQUIRE ON GOVERNMENT STATISTICAL JOURNAL
*
* This program accepts the enquiry key and displays the corresponding
* record from GOJN file. If the required record is not found, the next
* record retrieved when the file is read will be displayed.
*
* The GOJN records will be arranged in the following ascending
* sequence:
*
*          Category
*          Statutory fund
*          Section
*          Sub-section
*          Register
*          Servicing branch
*          Age band
*          Sum insured band
*          Premium band
*          Term band
*          Commencement year
*          Premium statistical code
*          Currency
*          Account year
*
* The statistical accumulation key will be used to search the GOJN
* data. The first record found with a key equal to or greater than
* that input will be displayed.
*
* FUNCTION KEYS:
*               <EXIT>  Return to sub-menu.
*
*               <KILL>  Allow user to select another accumulation
*                       enquiry key. It clears the screen S6711
*                       and prompts user to enter a new enquiry key.
*
*               <ENTER> Display the accounts for the accumulation key
*                       if it has been changed, else display the
*                       accumulation account for the next month.
*
*                       If no enquiry key is entered, the first record
*                       in the file is displayed.
*
* FILES USED:
*               GOJNSKM
*               DESCSKM
*
*
*****************************************************************
* </pre>
*/
public class P6711 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6711");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaScreenKey = new FixedLengthStringData(35);
	private FixedLengthStringData wsaaStatcat = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 0);
	private PackedDecimalData wsaaAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaScreenKey, 2);
	private FixedLengthStringData wsaaStfund = new FixedLengthStringData(1).isAPartOf(wsaaScreenKey, 5);
	private FixedLengthStringData wsaaStsect = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 6);
	private FixedLengthStringData wsaaStsubsect = new FixedLengthStringData(4).isAPartOf(wsaaScreenKey, 8);
	private FixedLengthStringData wsaaRegister = new FixedLengthStringData(3).isAPartOf(wsaaScreenKey, 12);
	private FixedLengthStringData wsaaCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 15);
	private FixedLengthStringData wsaaBandage = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 17);
	private FixedLengthStringData wsaaBandsa = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 19);
	private FixedLengthStringData wsaaBandprm = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 21);
	private FixedLengthStringData wsaaBandtrm = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 23);
	private PackedDecimalData wsaaCommyr = new PackedDecimalData(4, 0).isAPartOf(wsaaScreenKey, 25);
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 28);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaScreenKey, 30);
	private ZonedDecimalData wsaaAcctmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaScreenKey, 33).setUnsigned();
	private String e382 = "E382";
	private String t3629 = "T3629";
		/* FORMATS */
	private String gojnrec = "GOJNREC";
	private String descrec = "DESCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Acmdescrec acmdescrec = new Acmdescrec();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
		/*Government Statistical Journal*/
	private GojnTableDAM gojnIO = new GojnTableDAM();
	private S6711ScreenVars sv = ScreenProgram.getScreenVars( S6711ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2090, 
		exit2190, 
		exit2290
	}

	public P6711() {
		super();
		screenVars = sv;
		new ScreenModel("S6711", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsaaScreenKey.set(SPACES);
		wsaaAcctmn.set(ZERO);
		wsaaCommyr.set(ZERO);
		wsaaAcctyr.set(9999);
		initialiseScreen1100();
		/*EXIT*/
	}

protected void initialiseScreen1100()
	{
		para1100();
	}

protected void para1100()
	{
		sv.dataArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		sv.stcmthOut[varcom.nd.toInt()].set("Y");
		sv.acctyr.set(ZERO);
		sv.acmn.set(ZERO);
		sv.acyr.set(ZERO);
		sv.commyr.set(ZERO);
		sv.stcmth.set(ZERO);
		sv.stimth.set(ZERO);
		sv.stpmth.set(ZERO);
		sv.stsmth.set(ZERO);
		sv.effdate.set(ZERO);
		sv.transactionTime.set(ZERO);
		sv.user.set(ZERO);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			validate2020();
			displayGojnRecord2040();
		}
		catch (GOTOException e){
		}
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			initialiseScreen1100();
			wsspcomn.edterror.set("Y");
			sv.stcmthOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(sv.statcat,wsaaStatcat)
		&& isEQ(sv.acctyr,wsaaAcctyr)
		&& isEQ(sv.acmn,wsaaAcctmn)
		&& isEQ(sv.statFund,wsaaStfund)
		&& isEQ(sv.statSect,wsaaStsect)
		&& isEQ(sv.stsubsect,wsaaStsubsect)
		&& isEQ(sv.register,wsaaRegister)
		&& isEQ(sv.cntbranch,wsaaCntbranch)
		&& isEQ(sv.bandage,wsaaBandage)
		&& isEQ(sv.bandsa,wsaaBandsa)
		&& isEQ(sv.bandprm,wsaaBandprm)
		&& isEQ(sv.bandtrm,wsaaBandtrm)
		&& isEQ(sv.commyr,wsaaCommyr)
		&& isEQ(sv.pstatcd,wsaaPstatcode)
		&& isEQ(sv.cntcurr,wsaaCntcurr)) {
			gojnIO.setFunction(varcom.nextr);
		}
		else {
			gojnIO.setFunction(varcom.begn);
		}
		readGojnRecord2100();
		moveScreenToWsaa2200();
		if (isEQ(gojnIO.getStatuz(),varcom.endp)) {
			initialiseScreen1100();
			scrnparams.errorCode.set(e382);
			wsspcomn.edterror.set("Y");
			sv.stcmthOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void displayGojnRecord2040()
	{
		sv.stcmthOut[varcom.nd.toInt()].set("N");
		sv.acmn.set(gojnIO.getAcctmonth());
		sv.acyr.set(gojnIO.getAcctyr());
		sv.acctyr.set(gojnIO.getAcctyr());
		sv.statcat.set(gojnIO.getStatcat());
		sv.statFund.set(gojnIO.getStatFund());
		sv.statSect.set(gojnIO.getStatSect());
		sv.stsubsect.set(gojnIO.getStatSubsect());
		sv.register.set(gojnIO.getRegister());
		sv.cntbranch.set(gojnIO.getCntbranch());
		sv.bandage.set(gojnIO.getBandage());
		sv.bandprm.set(gojnIO.getBandprm());
		sv.bandsa.set(gojnIO.getBandsa());
		sv.bandtrm.set(gojnIO.getBandtrm());
		sv.commyr.set(gojnIO.getCommyr());
		sv.pstatcd.set(gojnIO.getPstatcode());
		sv.cntcurr.set(gojnIO.getCntcurr());
		sv.stcmth.set(gojnIO.getStcmth());
		sv.stpmth.set(gojnIO.getStpmth());
		sv.stsmth.set(gojnIO.getStsmth());
		sv.stimth.set(gojnIO.getStimth());
		sv.effdate.set(gojnIO.getEffdate());
		sv.transactionTime.set(gojnIO.getTransactionTime());
		sv.user.set(gojnIO.getUser());
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readGojnRecord2100()
	{
		try {
			para2110();
			setUpCurrencyDesc2120();
			setUpMonthDesc2130();
		}
		catch (GOTOException e){
		}
	}

protected void para2110()
	{
		gojnIO.setChdrcoy(wsspcomn.company);
		gojnIO.setStatcat(sv.statcat);
		gojnIO.setAcctyr(sv.acctyr);
		gojnIO.setAcctmonth(sv.acmn);
		gojnIO.setStatFund(sv.statFund);
		gojnIO.setStatSect(sv.statSect);
		gojnIO.setStatSubsect(sv.stsubsect);
		gojnIO.setRegister(sv.register);
		gojnIO.setCntbranch(sv.cntbranch);
		gojnIO.setBandage(sv.bandage);
		gojnIO.setBandsa(sv.bandsa);
		gojnIO.setBandprm(sv.bandprm);
		gojnIO.setBandtrm(sv.bandtrm);
		gojnIO.setCommyr(sv.commyr);
		gojnIO.setPstatcode(sv.pstatcd);
		gojnIO.setCntcurr(sv.cntcurr);
		gojnIO.setEffdate(sv.effdate);
		gojnIO.setTransactionTime(sv.transactionTime);
		gojnIO.setFormat(gojnrec);
		SmartFileCode.execute(appVars, gojnIO);
		if (isNE(gojnIO.getStatuz(),varcom.oK)
		&& isNE(gojnIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(gojnIO.getParams());
			syserrrec.statuz.set(gojnIO.getStatuz());
			fatalError600();
		}
		if (isEQ(gojnIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2190);
		}
	}

protected void setUpCurrencyDesc2120()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(gojnIO.getChdrcoy());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(gojnIO.getCntcurr());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.descrip.set(descIO.getLongdesc());
	}

protected void setUpMonthDesc2130()
	{
		acmdescrec.company.set(gojnIO.getChdrcoy());
		acmdescrec.language.set(wsspcomn.language);
		acmdescrec.branch.set(gojnIO.getCntbranch());
		acmdescrec.function.set("GETD");
		callProgram(Acmdesc.class, acmdescrec.acmdescRec);
		if (isNE(acmdescrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(acmdescrec.statuz);
			fatalError600();
		}
		sv.mthldesc.set(acmdescrec.lngdesc[gojnIO.getAcctmonth().toInt()]);
	}

protected void moveScreenToWsaa2200()
	{
		try {
			para2210();
		}
		catch (GOTOException e){
		}
	}

protected void para2210()
	{
		if (isEQ(gojnIO.getStatuz(),varcom.endp)) {
			goTo(GotoLabel.exit2290);
		}
		wsaaStatcat.set(gojnIO.getStatcat());
		wsaaAcctyr.set(gojnIO.getAcctyr());
		wsaaAcctmn.set(gojnIO.getAcctmonth());
		wsaaStfund.set(gojnIO.getStatFund());
		wsaaStsect.set(gojnIO.getStatSect());
		wsaaStsubsect.set(gojnIO.getStatSubsect());
		wsaaRegister.set(gojnIO.getRegister());
		wsaaCntbranch.set(gojnIO.getCntbranch());
		wsaaBandage.set(gojnIO.getBandage());
		wsaaBandsa.set(gojnIO.getBandsa());
		wsaaBandprm.set(gojnIO.getBandprm());
		wsaaBandtrm.set(gojnIO.getBandtrm());
		wsaaCommyr.set(gojnIO.getCommyr());
		wsaaPstatcode.set(gojnIO.getPstatcode());
		wsaaCntcurr.set(gojnIO.getCntcurr());
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
