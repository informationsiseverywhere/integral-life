package com.csc.life.statistics.dataaccess.dao;

import java.util.List;

import com.csc.life.statistics.dataaccess.model.Sttrpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface SttrpfDAO extends BaseDAO<Sttrpf>{
	public List<Sttrpf> searchSttrpfRecord(String chdrcoy,String chdrnum);
	public void insertSttrpfRecord(List<Sttrpf> insertList);
}
