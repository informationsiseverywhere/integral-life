package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:02
 * Description:
 * Copybook name: AGCMSTSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agcmstskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agcmstsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agcmstsKey = new FixedLengthStringData(64).isAPartOf(agcmstsFileKey, 0, REDEFINE);
  	public FixedLengthStringData agcmstsChdrcoy = new FixedLengthStringData(1).isAPartOf(agcmstsKey, 0);
  	public FixedLengthStringData agcmstsChdrnum = new FixedLengthStringData(8).isAPartOf(agcmstsKey, 1);
  	public FixedLengthStringData agcmstsLife = new FixedLengthStringData(2).isAPartOf(agcmstsKey, 9);
  	public FixedLengthStringData agcmstsCoverage = new FixedLengthStringData(2).isAPartOf(agcmstsKey, 11);
  	public FixedLengthStringData agcmstsRider = new FixedLengthStringData(2).isAPartOf(agcmstsKey, 13);
  	public PackedDecimalData agcmstsPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(agcmstsKey, 15);
  	public FixedLengthStringData agcmstsAgntnum = new FixedLengthStringData(8).isAPartOf(agcmstsKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(38).isAPartOf(agcmstsKey, 26, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agcmstsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agcmstsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}