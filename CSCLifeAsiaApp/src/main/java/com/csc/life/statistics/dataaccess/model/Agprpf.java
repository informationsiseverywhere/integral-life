/*********************  */
/*Author  :Liwei		   */
/*Purpose :Model foe Agprpf*/
/*Date    :2018.11.24	   */
package com.csc.life.statistics.dataaccess.model;

public class Agprpf {
	private long unique_number;
	private String agntcoy;
	private String agntnum;
	private int acctyr;
	private int mnth;
	private String chdrnum;
	private String cnttype;
	private int effdate;
	private float mlperpp01;
	private float mlperpp02;
	private float mlperpp03;
	private float mlperpp04;
	private float mlperpp05;
	private float mlperpp06;
	private float mlperpp07;
	private float mlperpp08;
	private float mlperpp09;
	private float mlperpp10;
	private float mlperpc01;
	private float mlperpc02;
	private float mlperpc03;
	private float mlperpc04;
	private float mlperpc05;
	private float mlperpc06;
	private float mlperpc07;
	private float mlperpc08;
	private float mlperpc09;
	private float mlperpc10;
	private float mldirpp01;
	private float mldirpp02;
	private float mldirpp03;
	private float mldirpp04;
	private float mldirpp05;
	private float mldirpp06;
	private float mldirpp07;
	private float mldirpp08;
	private float mldirpp09;
	private float mldirpp10;
	private float mldirpc01;
	private float mldirpc02;
	private float mldirpc03;
	private float mldirpc04;
	private float mldirpc05;
	private float mldirpc06;
	private float mldirpc07;
	private float mldirpc08;
	private float mldirpc09;
	private float mldirpc10;
	private float mlgrppp01;
	private float mlgrppp02;
	private float mlgrppp03;
	private float mlgrppp04;
	private float mlgrppp05;
	private float mlgrppp06;
	private float mlgrppp07;
	private float mlgrppp08;
	private float mlgrppp09;
	private float mlgrppp10;
	private float mlgrppc01;
	private float mlgrppc02;
	private float mlgrppc03;
	private float mlgrppc04;
	private float mlgrppc05;
	private float mlgrppc06;
	private float mlgrppc07;
	private float mlgrppc08;
	private float mlgrppc09;
	private float mlgrppc10;
	private int cntcount;
	private float sumins;
	private String userProfile;
	private String jobName;
	private String datime;
	
	public Agprpf() {
		agntcoy = "";
		agntnum = "";
		acctyr = 0;
		mnth = 0;
		chdrnum = "";
		cnttype = "";
		effdate = 0;
		mlperpp01 = 0;
		mlperpp02 = 0;
		mlperpp03 = 0;
		mlperpp04 = 0;
		mlperpp05 = 0;
		mlperpp06 = 0;
		mlperpp07 = 0;
		mlperpp08 = 0;
		mlperpp09 = 0;
		mlperpp10 = 0;
		mlperpc01 = 0;
		mlperpc02 = 0;
		mlperpc03 = 0;
		mlperpc04 = 0;
		mlperpc05 = 0;
		mlperpc06 = 0;
		mlperpc07 = 0;
		mlperpc08 = 0;
		mlperpc09 = 0;
		mlperpc10 = 0;
		mldirpp01 = 0;
		mldirpp02 = 0;
		mldirpp03 = 0;
		mldirpp04 = 0;
		mldirpp05 = 0;
		mldirpp06 = 0;
		mldirpp07 = 0;
		mldirpp08 = 0;
		mldirpp09 = 0;
		mldirpp10 = 0;
		mldirpc01 = 0;
		mldirpc02 = 0;
		mldirpc03 = 0;
		mldirpc04 = 0;
		mldirpc05 = 0;
		mldirpc06 = 0;
		mldirpc07 = 0;
		mldirpc08 = 0;
		mldirpc09 = 0;
		mldirpc10 = 0;
		mlgrppp01 = 0;
		mlgrppp02 = 0;
		mlgrppp03 = 0;
		mlgrppp04 = 0;
		mlgrppp05 = 0;
		mlgrppp06 = 0;
		mlgrppp07 = 0;
		mlgrppp08 = 0;
		mlgrppp09 = 0;
		mlgrppp10 = 0;
		mlgrppc01 = 0;
		mlgrppc02 = 0;
		mlgrppc03 = 0;
		mlgrppc04 = 0;
		mlgrppc05 = 0;
		mlgrppc06 = 0;
		mlgrppc07 = 0;
		mlgrppc08 = 0;
		mlgrppc09 = 0;
		mlgrppc10 = 0;
		cntcount = 0;
		sumins = 0;
		userProfile = "";
		jobName = "";
		datime = "";
	}
	public long getUnique_number() {
		return unique_number;
	}
	public void setUnique_number(long unique_number) {
		this.unique_number = unique_number;
	}
	public String getAgntcoy() {
		return agntcoy;
	}
	public void setAgntcoy(String agntcoy) {
		this.agntcoy = agntcoy;
	}
	public String getAgntnum() {
		return agntnum;
	}
	public void setAgntnum(String agntnum) {
		this.agntnum = agntnum;
	}
	public int getAcctyr() {
		return acctyr;
	}
	public void setAcctyr(int acctyr) {
		this.acctyr = acctyr;
	}
	public int getMnth() {
		return mnth;
	}
	public void setMnth(int mnth) {
		this.mnth = mnth;
	}
	public String getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	public String getCnttype() {
		return cnttype;
	}
	public void setCnttype(String cnttype) {
		this.cnttype = cnttype;
	}
	public int getEffdate() {
		return effdate;
	}
	public void setEffdate(int effdate) {
		this.effdate = effdate;
	}
	public float getMlperpp01() {
		return mlperpp01;
	}
	public void setMlperpp01(float mlperpp01) {
		this.mlperpp01 = mlperpp01;
	}
	public float getMlperpp02() {
		return mlperpp02;
	}
	public void setMlperpp02(float mlperpp02) {
		this.mlperpp02 = mlperpp02;
	}
	public float getMlperpp03() {
		return mlperpp03;
	}
	public void setMlperpp03(float mlperpp03) {
		this.mlperpp03 = mlperpp03;
	}
	public float getMlperpp04() {
		return mlperpp04;
	}
	public void setMlperpp04(float mlperpp04) {
		this.mlperpp04 = mlperpp04;
	}
	public float getMlperpp05() {
		return mlperpp05;
	}
	public void setMlperpp05(float mlperpp05) {
		this.mlperpp05 = mlperpp05;
	}
	public float getMlperpp06() {
		return mlperpp06;
	}
	public void setMlperpp06(float mlperpp06) {
		this.mlperpp06 = mlperpp06;
	}
	public float getMlperpp07() {
		return mlperpp07;
	}
	public void setMlperpp07(float mlperpp07) {
		this.mlperpp07 = mlperpp07;
	}
	public float getMlperpp08() {
		return mlperpp08;
	}
	public void setMlperpp08(float mlperpp08) {
		this.mlperpp08 = mlperpp08;
	}
	public float getMlperpp09() {
		return mlperpp09;
	}
	public void setMlperpp09(float mlperpp09) {
		this.mlperpp09 = mlperpp09;
	}
	public float getMlperpp10() {
		return mlperpp10;
	}
	public void setMlperpp10(float mlperpp10) {
		this.mlperpp10 = mlperpp10;
	}
	public float getMlperpc01() {
		return mlperpc01;
	}
	public void setMlperpc01(float mlperpc01) {
		this.mlperpc01 = mlperpc01;
	}
	public float getMlperpc02() {
		return mlperpc02;
	}
	public void setMlperpc02(float mlperpc02) {
		this.mlperpc02 = mlperpc02;
	}
	public float getMlperpc03() {
		return mlperpc03;
	}
	public void setMlperpc03(float mlperpc03) {
		this.mlperpc03 = mlperpc03;
	}
	public float getMlperpc04() {
		return mlperpc04;
	}
	public void setMlperpc04(float mlperpc04) {
		this.mlperpc04 = mlperpc04;
	}
	public float getMlperpc05() {
		return mlperpc05;
	}
	public void setMlperpc05(float mlperpc05) {
		this.mlperpc05 = mlperpc05;
	}
	public float getMlperpc06() {
		return mlperpc06;
	}
	public void setMlperpc06(float mlperpc06) {
		this.mlperpc06 = mlperpc06;
	}
	public float getMlperpc07() {
		return mlperpc07;
	}
	public void setMlperpc07(float mlperpc07) {
		this.mlperpc07 = mlperpc07;
	}
	public float getMlperpc08() {
		return mlperpc08;
	}
	public void setMlperpc08(float mlperpc08) {
		this.mlperpc08 = mlperpc08;
	}
	public float getMlperpc09() {
		return mlperpc09;
	}
	public void setMlperpc09(float mlperpc09) {
		this.mlperpc09 = mlperpc09;
	}
	public float getMlperpc10() {
		return mlperpc10;
	}
	public void setMlperpc10(float mlperpc10) {
		this.mlperpc10 = mlperpc10;
	}
	public float getMldirpp01() {
		return mldirpp01;
	}
	public void setMldirpp01(float mldirpp01) {
		this.mldirpp01 = mldirpp01;
	}
	public float getMldirpp02() {
		return mldirpp02;
	}
	public void setMldirpp02(float mldirpp02) {
		this.mldirpp02 = mldirpp02;
	}
	public float getMldirpp03() {
		return mldirpp03;
	}
	public void setMldirpp03(float mldirpp03) {
		this.mldirpp03 = mldirpp03;
	}
	public float getMldirpp04() {
		return mldirpp04;
	}
	public void setMldirpp04(float mldirpp04) {
		this.mldirpp04 = mldirpp04;
	}
	public float getMldirpp05() {
		return mldirpp05;
	}
	public void setMldirpp05(float mldirpp05) {
		this.mldirpp05 = mldirpp05;
	}
	public float getMldirpp06() {
		return mldirpp06;
	}
	public void setMldirpp06(float mldirpp06) {
		this.mldirpp06 = mldirpp06;
	}
	public float getMldirpp07() {
		return mldirpp07;
	}
	public void setMldirpp07(float mldirpp07) {
		this.mldirpp07 = mldirpp07;
	}
	public float getMldirpp08() {
		return mldirpp08;
	}
	public void setMldirpp08(float mldirpp08) {
		this.mldirpp08 = mldirpp08;
	}
	public float getMldirpp09() {
		return mldirpp09;
	}
	public void setMldirpp09(float mldirpp09) {
		this.mldirpp09 = mldirpp09;
	}
	public float getMldirpp10() {
		return mldirpp10;
	}
	public void setMldirpp10(float mldirpp10) {
		this.mldirpp10 = mldirpp10;
	}
	public float getMldirpc01() {
		return mldirpc01;
	}
	public void setMldirpc01(float mldirpc01) {
		this.mldirpc01 = mldirpc01;
	}
	public float getMldirpc02() {
		return mldirpc02;
	}
	public void setMldirpc02(float mldirpc02) {
		this.mldirpc02 = mldirpc02;
	}
	public float getMldirpc03() {
		return mldirpc03;
	}
	public void setMldirpc03(float mldirpc03) {
		this.mldirpc03 = mldirpc03;
	}
	public float getMldirpc04() {
		return mldirpc04;
	}
	public void setMldirpc04(float mldirpc04) {
		this.mldirpc04 = mldirpc04;
	}
	public float getMldirpc05() {
		return mldirpc05;
	}
	public void setMldirpc05(float mldirpc05) {
		this.mldirpc05 = mldirpc05;
	}
	public float getMldirpc06() {
		return mldirpc06;
	}
	public void setMldirpc06(float mldirpc06) {
		this.mldirpc06 = mldirpc06;
	}
	public float getMldirpc07() {
		return mldirpc07;
	}
	public void setMldirpc07(float mldirpc07) {
		this.mldirpc07 = mldirpc07;
	}
	public float getMldirpc08() {
		return mldirpc08;
	}
	public void setMldirpc08(float mldirpc08) {
		this.mldirpc08 = mldirpc08;
	}
	public float getMldirpc09() {
		return mldirpc09;
	}
	public void setMldirpc09(float mldirpc09) {
		this.mldirpc09 = mldirpc09;
	}
	public float getMldirpc10() {
		return mldirpc10;
	}
	public void setMldirpc10(float mldirpc10) {
		this.mldirpc10 = mldirpc10;
	}
	public float getMlgrppp01() {
		return mlgrppp01;
	}
	public void setMlgrppp01(float mlgrppp01) {
		this.mlgrppp01 = mlgrppp01;
	}
	public float getMlgrppp02() {
		return mlgrppp02;
	}
	public void setMlgrppp02(float mlgrppp02) {
		this.mlgrppp02 = mlgrppp02;
	}
	public float getMlgrppp03() {
		return mlgrppp03;
	}
	public void setMlgrppp03(float mlgrppp03) {
		this.mlgrppp03 = mlgrppp03;
	}
	public float getMlgrppp04() {
		return mlgrppp04;
	}
	public void setMlgrppp04(float mlgrppp04) {
		this.mlgrppp04 = mlgrppp04;
	}
	public float getMlgrppp05() {
		return mlgrppp05;
	}
	public void setMlgrppp05(float mlgrppp05) {
		this.mlgrppp05 = mlgrppp05;
	}
	public float getMlgrppp06() {
		return mlgrppp06;
	}
	public void setMlgrppp06(float mlgrppp06) {
		this.mlgrppp06 = mlgrppp06;
	}
	public float getMlgrppp07() {
		return mlgrppp07;
	}
	public void setMlgrppp07(float mlgrppp07) {
		this.mlgrppp07 = mlgrppp07;
	}
	public float getMlgrppp08() {
		return mlgrppp08;
	}
	public void setMlgrppp08(float mlgrppp08) {
		this.mlgrppp08 = mlgrppp08;
	}
	public float getMlgrppp09() {
		return mlgrppp09;
	}
	public void setMlgrppp09(float mlgrppp09) {
		this.mlgrppp09 = mlgrppp09;
	}
	public float getMlgrppp10() {
		return mlgrppp10;
	}
	public void setMlgrppp10(float mlgrppp10) {
		this.mlgrppp10 = mlgrppp10;
	}
	public float getMlgrppc01() {
		return mlgrppc01;
	}
	public void setMlgrppc01(float mlgrppc01) {
		this.mlgrppc01 = mlgrppc01;
	}
	public float getMlgrppc02() {
		return mlgrppc02;
	}
	public void setMlgrppc02(float mlgrppc02) {
		this.mlgrppc02 = mlgrppc02;
	}
	public float getMlgrppc03() {
		return mlgrppc03;
	}
	public void setMlgrppc03(float mlgrppc03) {
		this.mlgrppc03 = mlgrppc03;
	}
	public float getMlgrppc04() {
		return mlgrppc04;
	}
	public void setMlgrppc04(float mlgrppc04) {
		this.mlgrppc04 = mlgrppc04;
	}
	public float getMlgrppc05() {
		return mlgrppc05;
	}
	public void setMlgrppc05(float mlgrppc05) {
		this.mlgrppc05 = mlgrppc05;
	}
	public float getMlgrppc06() {
		return mlgrppc06;
	}
	public void setMlgrppc06(float mlgrppc06) {
		this.mlgrppc06 = mlgrppc06;
	}
	public float getMlgrppc07() {
		return mlgrppc07;
	}
	public void setMlgrppc07(float mlgrppc07) {
		this.mlgrppc07 = mlgrppc07;
	}
	public float getMlgrppc08() {
		return mlgrppc08;
	}
	public void setMlgrppc08(float mlgrppc08) {
		this.mlgrppc08 = mlgrppc08;
	}
	public float getMlgrppc09() {
		return mlgrppc09;
	}
	public void setMlgrppc09(float mlgrppc09) {
		this.mlgrppc09 = mlgrppc09;
	}
	public float getMlgrppc10() {
		return mlgrppc10;
	}
	public void setMlgrppc10(float mlgrppc10) {
		this.mlgrppc10 = mlgrppc10;
	}
	public float getMlperpp(int index) {
		switch (index) {
			case 1 : return mlperpp01;
			case 2 : return mlperpp02;
			case 3 : return mlperpp03;
			case 4 : return mlperpp04;
			case 5 : return mlperpp05;
			case 6 : return mlperpp06;
			case 7 : return mlperpp07;
			case 8 : return mlperpp08;
			case 9 : return mlperpp09;
			case 10 : return mlperpp10;
			default: return 0; 
		}
	}
	public void setMlperpp(int index,float mlperpp) {
		switch (index) {
			case 1 : setMlperpp01(mlperpp);
					 break;
			case 2 : setMlperpp02(mlperpp);
					 break;
			case 3 : setMlperpp03(mlperpp);
					 break;
			case 4 : setMlperpp04(mlperpp);
					 break;
			case 5 : setMlperpp05(mlperpp);
					 break;
			case 6 : setMlperpp06(mlperpp);
					 break;
			case 7 : setMlperpp07(mlperpp);
					 break;
			case 8 : setMlperpp08(mlperpp);
					 break;
			case 9 : setMlperpp09(mlperpp);
					 break;
			case 10 : setMlperpp10(mlperpp);
					 break;
			default: return;
		}
	
	}
	public float getMlgrppc(int index) {
		switch (index) {
			case 1 : return mlgrppc01;
			case 2 : return mlgrppc02;
			case 3 : return mlgrppc03;
			case 4 : return mlgrppc04;
			case 5 : return mlgrppc05;
			case 6 : return mlgrppc06;
			case 7 : return mlgrppc07;
			case 8 : return mlgrppc08;
			case 9 : return mlgrppc09;
			case 10 : return mlgrppc10;
			default: return 0; 
		}
	}
	public void setMlgrppc(int index,float mlgrppc) {
		switch (index) {
			case 1 : setMlgrppc01(mlgrppc);
					 break;
			case 2 : setMlgrppc02(mlgrppc);
					 break;
			case 3 : setMlgrppc03(mlgrppc);
					 break;
			case 4 : setMlgrppc04(mlgrppc);
					 break;
			case 5 : setMlgrppc05(mlgrppc);
					 break;
			case 6 : setMlgrppc06(mlgrppc);
					 break;
			case 7 : setMlgrppc07(mlgrppc);
					 break;
			case 8 : setMlgrppc08(mlgrppc);
					 break;
			case 9 : setMlgrppc09(mlgrppc);
					 break;
			case 10 : setMlgrppc10(mlgrppc);
					 break;
			default: return;
		}
	}
	public float getMlperpc(int index) {
		switch (index) {
			case 1 : return mlperpc01;
			case 2 : return mlperpc02;
			case 3 : return mlperpc03;
			case 4 : return mlperpc04;
			case 5 : return mlperpc05;
			case 6 : return mlperpc06;
			case 7 : return mlperpc07;
			case 8 : return mlperpc08;
			case 9 : return mlperpc09;
			case 10 : return mlperpc10;
			default: return 0; 
		}
	}
	public void setMlperpc(int index,float mlperpc) {
		switch (index) {
			case 1 : setMlperpc01(mlperpc);
					 break;
			case 2 : setMlperpc02(mlperpc);
					 break;
			case 3 : setMlperpc03(mlperpc);
					 break;
			case 4 : setMlperpc04(mlperpc);
					 break;
			case 5 : setMlperpc05(mlperpc);
					 break;
			case 6 : setMlperpc06(mlperpc);
					 break;
			case 7 : setMlperpc07(mlperpc);
					 break;
			case 8 : setMlperpc08(mlperpc);
					 break;
			case 9 : setMlperpc09(mlperpc);
					 break;
			case 10 : setMlperpc10(mlperpc);
					 break;
			default: return;
		}
	}
	public float getMldirpp(int index) {
		switch (index) {
			case 1 : return mldirpp01;
			case 2 : return mldirpp02;
			case 3 : return mldirpp03;
			case 4 : return mldirpp04;
			case 5 : return mldirpp05;
			case 6 : return mldirpp06;
			case 7 : return mldirpp07;
			case 8 : return mldirpp08;
			case 9 : return mldirpp09;
			case 10 : return mldirpp10;
			default: return 0; 
		}
	}
	public void setMldirpp(int index,float mldirpp) {
		switch (index) {
			case 1 : setMldirpp01(mldirpp);
					 break;
			case 2 : setMldirpp02(mldirpp);
					 break;
			case 3 : setMldirpp03(mldirpp);
					 break;
			case 4 : setMldirpp04(mldirpp);
					 break;
			case 5 : setMldirpp05(mldirpp);
					 break;
			case 6 : setMldirpp06(mldirpp);
					 break;
			case 7 : setMldirpp07(mldirpp);
					 break;
			case 8 : setMldirpp08(mldirpp);
					 break;
			case 9 : setMldirpp09(mldirpp);
					 break;
			case 10 : setMldirpp10(mldirpp);
					 break;
			default: return;
		}
	}
	public float getMldirpc(int index) {
		switch (index) {
			case 1 : return mldirpc01;
			case 2 : return mldirpc02;
			case 3 : return mldirpc03;
			case 4 : return mldirpc04;
			case 5 : return mldirpc05;
			case 6 : return mldirpc06;
			case 7 : return mldirpc07;
			case 8 : return mldirpc08;
			case 9 : return mldirpc09;
			case 10 : return mldirpc10;
			default: return 0; 
		}
	}
	public void setMldirpc(int index,float mldirpc) {
		switch (index) {
			case 1 : setMldirpc01(mldirpc);
					 break;
			case 2 : setMldirpc02(mldirpc);
					 break;
			case 3 : setMldirpc03(mldirpc);
					 break;
			case 4 : setMldirpc04(mldirpc);
					 break;
			case 5 : setMldirpc05(mldirpc);
					 break;
			case 6 : setMldirpc06(mldirpc);
					 break;
			case 7 : setMldirpc07(mldirpc);
					 break;
			case 8 : setMldirpc08(mldirpc);
					 break;
			case 9 : setMldirpc09(mldirpc);
					 break;
			case 10 : setMldirpc10(mldirpc);
					 break;
			default: return;
		}
	}
	public float getMlgrppp(int index) {
		switch (index) {
			case 1 : return mlgrppp01;
			case 2 : return mlgrppp02;
			case 3 : return mlgrppp03;
			case 4 : return mlgrppp04;
			case 5 : return mlgrppp05;
			case 6 : return mlgrppp06;
			case 7 : return mlgrppp07;
			case 8 : return mlgrppp08;
			case 9 : return mlgrppp09;
			case 10 : return mlgrppp10;
			default: return 0; 
		}
	}
	public void setMlgrppp(int index,float mlgrppp) {
		switch (index) {
			case 1 : setMlgrppp01(mlgrppp);
					 break;
			case 2 : setMlgrppp02(mlgrppp);
					 break;
			case 3 : setMlgrppp03(mlgrppp);
					 break;
			case 4 : setMlgrppp04(mlgrppp);
					 break;
			case 5 : setMlgrppp05(mlgrppp);
					 break;
			case 6 : setMlgrppp06(mlgrppp);
					 break;
			case 7 : setMlgrppp07(mlgrppp);
					 break;
			case 8 : setMlgrppp08(mlgrppp);
					 break;
			case 9 : setMlgrppp09(mlgrppp);
					 break;
			case 10 : setMlgrppp10(mlgrppp);
					 break;
			default: return;
		}
	}
	public int getCntcount() {
		return cntcount;
	}
	public void setCntcount(int cntcount) {
		this.cntcount = cntcount;
	}
	public float getSumins() {
		return sumins;
	}
	public void setSumins(float sumins) {
		this.sumins = sumins;
	}
	public String getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(String userProfile) {
		this.userProfile = userProfile;
	}
	public String getJobName() {
		return jobName;
	}
	public void setJobName(String jobName) {
		this.jobName = jobName;
	}
	public String getDatime() {
		return datime;
	}
	public void setDatime(String datime) {
		this.datime = datime;
	}
	
	
	
}
