package com.csc.life.statistics.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:18:46
 * Description:
 * Copybook name: TJ675REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Tj675rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData tj675Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData statSects = new FixedLengthStringData(20).isAPartOf(tj675Rec, 0);
  	public FixedLengthStringData[] statSect = FLSArrayPartOfStructure(10, 2, statSects, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(20).isAPartOf(statSects, 0, FILLER_REDEFINE);
  	public FixedLengthStringData statSect01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData statSect02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData statSect03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData statSect04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData statSect05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData statSect06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData statSect07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData statSect08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData statSect09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData statSect10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(480).isAPartOf(tj675Rec, 20, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(tj675Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		tj675Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}