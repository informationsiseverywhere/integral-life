package com.csc.life.statistics.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:44
 * @author Quipoz
 */
public class S6628screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 5, 23, 18, 15, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 18, 3, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		S6628ScreenVars sv = (S6628ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.S6628screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		S6628ScreenVars screenVars = (S6628ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.statcatg01.setClassString("");
		screenVars.statcatg02.setClassString("");
		screenVars.statcatg03.setClassString("");
		screenVars.statcatg04.setClassString("");
		screenVars.statcatg05.setClassString("");
		screenVars.statcatg06.setClassString("");
		screenVars.statcatg07.setClassString("");
		screenVars.statcatg08.setClassString("");
		screenVars.statcatg09.setClassString("");
		screenVars.statcatg10.setClassString("");
		screenVars.statcatg11.setClassString("");
		screenVars.statcatg12.setClassString("");
		screenVars.statcatg13.setClassString("");
		screenVars.statcatg14.setClassString("");
		screenVars.statcatg15.setClassString("");
		screenVars.statcatg16.setClassString("");
		screenVars.statcatg17.setClassString("");
		screenVars.statcatg18.setClassString("");
		screenVars.statcatg19.setClassString("");
		screenVars.statcatg20.setClassString("");
		screenVars.statcatg21.setClassString("");
		screenVars.statcatg22.setClassString("");
		screenVars.statcatg23.setClassString("");
		screenVars.statcatg24.setClassString("");
		screenVars.statcatg25.setClassString("");
		screenVars.statcatg26.setClassString("");
		screenVars.statcatg27.setClassString("");
		screenVars.statcatg28.setClassString("");
		screenVars.statcatg29.setClassString("");
		screenVars.statcatg30.setClassString("");
		screenVars.statcatg31.setClassString("");
		screenVars.statcatg32.setClassString("");
		screenVars.statcatg33.setClassString("");
		screenVars.statcatg34.setClassString("");
		screenVars.statcatg35.setClassString("");
		screenVars.statcatg36.setClassString("");
		screenVars.statcatg37.setClassString("");
		screenVars.statcatg38.setClassString("");
		screenVars.statcatg39.setClassString("");
		screenVars.statcatg40.setClassString("");
		screenVars.statcatg41.setClassString("");
		screenVars.statcatg42.setClassString("");
		screenVars.statcatg43.setClassString("");
		screenVars.statcatg44.setClassString("");
		screenVars.statcatg45.setClassString("");
		screenVars.statcatg46.setClassString("");
		screenVars.statcatg47.setClassString("");
		screenVars.statcatg48.setClassString("");
		screenVars.statcatg49.setClassString("");
		screenVars.statcatg50.setClassString("");
		screenVars.statcatg51.setClassString("");
		screenVars.statcatg52.setClassString("");
		screenVars.statcatg53.setClassString("");
		screenVars.statcatg54.setClassString("");
		screenVars.statcatg55.setClassString("");
		screenVars.statcatg56.setClassString("");
		screenVars.statcatg57.setClassString("");
		screenVars.statcatg58.setClassString("");
		screenVars.statcatg59.setClassString("");
		screenVars.statcatg60.setClassString("");
		screenVars.statcatg61.setClassString("");
		screenVars.statcatg62.setClassString("");
		screenVars.statcatg63.setClassString("");
		screenVars.statcatg64.setClassString("");
		screenVars.statcatg65.setClassString("");
		screenVars.statcatg66.setClassString("");
		screenVars.statcatg67.setClassString("");
		screenVars.statcatg68.setClassString("");
		screenVars.statcatg69.setClassString("");
		screenVars.statcatg70.setClassString("");
		screenVars.statcatg71.setClassString("");
		screenVars.statcatg72.setClassString("");
		screenVars.statcatg73.setClassString("");
		screenVars.statcatg74.setClassString("");
		screenVars.statcatg75.setClassString("");
		screenVars.statcatg76.setClassString("");
		screenVars.statcatg77.setClassString("");
		screenVars.statcatg78.setClassString("");
		screenVars.statcatg79.setClassString("");
		screenVars.statcatg80.setClassString("");
		screenVars.statcatg81.setClassString("");
		screenVars.statcatg82.setClassString("");
		screenVars.statcatg83.setClassString("");
		screenVars.statcatg84.setClassString("");
		screenVars.statcatg85.setClassString("");
		screenVars.statcatg86.setClassString("");
		screenVars.statcatg87.setClassString("");
		screenVars.statcatg88.setClassString("");
		screenVars.longdesc.setClassString("");
	}

/**
 * Clear all the variables in S6628screen
 */
	public static void clear(VarModel pv) {
		S6628ScreenVars screenVars = (S6628ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.statcatg01.clear();
		screenVars.statcatg02.clear();
		screenVars.statcatg03.clear();
		screenVars.statcatg04.clear();
		screenVars.statcatg05.clear();
		screenVars.statcatg06.clear();
		screenVars.statcatg07.clear();
		screenVars.statcatg08.clear();
		screenVars.statcatg09.clear();
		screenVars.statcatg10.clear();
		screenVars.statcatg11.clear();
		screenVars.statcatg12.clear();
		screenVars.statcatg13.clear();
		screenVars.statcatg14.clear();
		screenVars.statcatg15.clear();
		screenVars.statcatg16.clear();
		screenVars.statcatg17.clear();
		screenVars.statcatg18.clear();
		screenVars.statcatg19.clear();
		screenVars.statcatg20.clear();
		screenVars.statcatg21.clear();
		screenVars.statcatg22.clear();
		screenVars.statcatg23.clear();
		screenVars.statcatg24.clear();
		screenVars.statcatg25.clear();
		screenVars.statcatg26.clear();
		screenVars.statcatg27.clear();
		screenVars.statcatg28.clear();
		screenVars.statcatg29.clear();
		screenVars.statcatg30.clear();
		screenVars.statcatg31.clear();
		screenVars.statcatg32.clear();
		screenVars.statcatg33.clear();
		screenVars.statcatg34.clear();
		screenVars.statcatg35.clear();
		screenVars.statcatg36.clear();
		screenVars.statcatg37.clear();
		screenVars.statcatg38.clear();
		screenVars.statcatg39.clear();
		screenVars.statcatg40.clear();
		screenVars.statcatg41.clear();
		screenVars.statcatg42.clear();
		screenVars.statcatg43.clear();
		screenVars.statcatg44.clear();
		screenVars.statcatg45.clear();
		screenVars.statcatg46.clear();
		screenVars.statcatg47.clear();
		screenVars.statcatg48.clear();
		screenVars.statcatg49.clear();
		screenVars.statcatg50.clear();
		screenVars.statcatg51.clear();
		screenVars.statcatg52.clear();
		screenVars.statcatg53.clear();
		screenVars.statcatg54.clear();
		screenVars.statcatg55.clear();
		screenVars.statcatg56.clear();
		screenVars.statcatg57.clear();
		screenVars.statcatg58.clear();
		screenVars.statcatg59.clear();
		screenVars.statcatg60.clear();
		screenVars.statcatg61.clear();
		screenVars.statcatg62.clear();
		screenVars.statcatg63.clear();
		screenVars.statcatg64.clear();
		screenVars.statcatg65.clear();
		screenVars.statcatg66.clear();
		screenVars.statcatg67.clear();
		screenVars.statcatg68.clear();
		screenVars.statcatg69.clear();
		screenVars.statcatg70.clear();
		screenVars.statcatg71.clear();
		screenVars.statcatg72.clear();
		screenVars.statcatg73.clear();
		screenVars.statcatg74.clear();
		screenVars.statcatg75.clear();
		screenVars.statcatg76.clear();
		screenVars.statcatg77.clear();
		screenVars.statcatg78.clear();
		screenVars.statcatg79.clear();
		screenVars.statcatg80.clear();
		screenVars.statcatg81.clear();
		screenVars.statcatg82.clear();
		screenVars.statcatg83.clear();
		screenVars.statcatg84.clear();
		screenVars.statcatg85.clear();
		screenVars.statcatg86.clear();
		screenVars.statcatg87.clear();
		screenVars.statcatg88.clear();
		screenVars.longdesc.clear();
	}
}
