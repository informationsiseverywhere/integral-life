package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:10
 * Description:
 * Copybook name: AGPRKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agprkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agprFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData agprKey = new FixedLengthStringData(256).isAPartOf(agprFileKey, 0, REDEFINE);
  	public FixedLengthStringData filler = new FixedLengthStringData(256).isAPartOf(agprKey, 0, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agprFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agprFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}