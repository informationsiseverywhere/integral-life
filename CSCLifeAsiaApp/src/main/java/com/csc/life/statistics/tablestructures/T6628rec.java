package com.csc.life.statistics.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:30
 * Description:
 * Copybook name: T6628REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6628rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6628Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData statcatgs = new FixedLengthStringData(176).isAPartOf(t6628Rec, 0);
  	public FixedLengthStringData[] statcatg = FLSArrayPartOfStructure(88, 2, statcatgs, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(176).isAPartOf(statcatgs, 0, FILLER_REDEFINE);
  	public FixedLengthStringData statcatg01 = new FixedLengthStringData(2).isAPartOf(filler, 0);
  	public FixedLengthStringData statcatg02 = new FixedLengthStringData(2).isAPartOf(filler, 2);
  	public FixedLengthStringData statcatg03 = new FixedLengthStringData(2).isAPartOf(filler, 4);
  	public FixedLengthStringData statcatg04 = new FixedLengthStringData(2).isAPartOf(filler, 6);
  	public FixedLengthStringData statcatg05 = new FixedLengthStringData(2).isAPartOf(filler, 8);
  	public FixedLengthStringData statcatg06 = new FixedLengthStringData(2).isAPartOf(filler, 10);
  	public FixedLengthStringData statcatg07 = new FixedLengthStringData(2).isAPartOf(filler, 12);
  	public FixedLengthStringData statcatg08 = new FixedLengthStringData(2).isAPartOf(filler, 14);
  	public FixedLengthStringData statcatg09 = new FixedLengthStringData(2).isAPartOf(filler, 16);
  	public FixedLengthStringData statcatg10 = new FixedLengthStringData(2).isAPartOf(filler, 18);
  	public FixedLengthStringData statcatg11 = new FixedLengthStringData(2).isAPartOf(filler, 20);
  	public FixedLengthStringData statcatg12 = new FixedLengthStringData(2).isAPartOf(filler, 22);
  	public FixedLengthStringData statcatg13 = new FixedLengthStringData(2).isAPartOf(filler, 24);
  	public FixedLengthStringData statcatg14 = new FixedLengthStringData(2).isAPartOf(filler, 26);
  	public FixedLengthStringData statcatg15 = new FixedLengthStringData(2).isAPartOf(filler, 28);
  	public FixedLengthStringData statcatg16 = new FixedLengthStringData(2).isAPartOf(filler, 30);
  	public FixedLengthStringData statcatg17 = new FixedLengthStringData(2).isAPartOf(filler, 32);
  	public FixedLengthStringData statcatg18 = new FixedLengthStringData(2).isAPartOf(filler, 34);
  	public FixedLengthStringData statcatg19 = new FixedLengthStringData(2).isAPartOf(filler, 36);
  	public FixedLengthStringData statcatg20 = new FixedLengthStringData(2).isAPartOf(filler, 38);
  	public FixedLengthStringData statcatg21 = new FixedLengthStringData(2).isAPartOf(filler, 40);
  	public FixedLengthStringData statcatg22 = new FixedLengthStringData(2).isAPartOf(filler, 42);
  	public FixedLengthStringData statcatg23 = new FixedLengthStringData(2).isAPartOf(filler, 44);
  	public FixedLengthStringData statcatg24 = new FixedLengthStringData(2).isAPartOf(filler, 46);
  	public FixedLengthStringData statcatg25 = new FixedLengthStringData(2).isAPartOf(filler, 48);
  	public FixedLengthStringData statcatg26 = new FixedLengthStringData(2).isAPartOf(filler, 50);
  	public FixedLengthStringData statcatg27 = new FixedLengthStringData(2).isAPartOf(filler, 52);
  	public FixedLengthStringData statcatg28 = new FixedLengthStringData(2).isAPartOf(filler, 54);
  	public FixedLengthStringData statcatg29 = new FixedLengthStringData(2).isAPartOf(filler, 56);
  	public FixedLengthStringData statcatg30 = new FixedLengthStringData(2).isAPartOf(filler, 58);
  	public FixedLengthStringData statcatg31 = new FixedLengthStringData(2).isAPartOf(filler, 60);
  	public FixedLengthStringData statcatg32 = new FixedLengthStringData(2).isAPartOf(filler, 62);
  	public FixedLengthStringData statcatg33 = new FixedLengthStringData(2).isAPartOf(filler, 64);
  	public FixedLengthStringData statcatg34 = new FixedLengthStringData(2).isAPartOf(filler, 66);
  	public FixedLengthStringData statcatg35 = new FixedLengthStringData(2).isAPartOf(filler, 68);
  	public FixedLengthStringData statcatg36 = new FixedLengthStringData(2).isAPartOf(filler, 70);
  	public FixedLengthStringData statcatg37 = new FixedLengthStringData(2).isAPartOf(filler, 72);
  	public FixedLengthStringData statcatg38 = new FixedLengthStringData(2).isAPartOf(filler, 74);
  	public FixedLengthStringData statcatg39 = new FixedLengthStringData(2).isAPartOf(filler, 76);
  	public FixedLengthStringData statcatg40 = new FixedLengthStringData(2).isAPartOf(filler, 78);
  	public FixedLengthStringData statcatg41 = new FixedLengthStringData(2).isAPartOf(filler, 80);
  	public FixedLengthStringData statcatg42 = new FixedLengthStringData(2).isAPartOf(filler, 82);
  	public FixedLengthStringData statcatg43 = new FixedLengthStringData(2).isAPartOf(filler, 84);
  	public FixedLengthStringData statcatg44 = new FixedLengthStringData(2).isAPartOf(filler, 86);
  	public FixedLengthStringData statcatg45 = new FixedLengthStringData(2).isAPartOf(filler, 88);
  	public FixedLengthStringData statcatg46 = new FixedLengthStringData(2).isAPartOf(filler, 90);
  	public FixedLengthStringData statcatg47 = new FixedLengthStringData(2).isAPartOf(filler, 92);
  	public FixedLengthStringData statcatg48 = new FixedLengthStringData(2).isAPartOf(filler, 94);
  	public FixedLengthStringData statcatg49 = new FixedLengthStringData(2).isAPartOf(filler, 96);
  	public FixedLengthStringData statcatg50 = new FixedLengthStringData(2).isAPartOf(filler, 98);
  	public FixedLengthStringData statcatg51 = new FixedLengthStringData(2).isAPartOf(filler, 100);
  	public FixedLengthStringData statcatg52 = new FixedLengthStringData(2).isAPartOf(filler, 102);
  	public FixedLengthStringData statcatg53 = new FixedLengthStringData(2).isAPartOf(filler, 104);
  	public FixedLengthStringData statcatg54 = new FixedLengthStringData(2).isAPartOf(filler, 106);
  	public FixedLengthStringData statcatg55 = new FixedLengthStringData(2).isAPartOf(filler, 108);
  	public FixedLengthStringData statcatg56 = new FixedLengthStringData(2).isAPartOf(filler, 110);
  	public FixedLengthStringData statcatg57 = new FixedLengthStringData(2).isAPartOf(filler, 112);
  	public FixedLengthStringData statcatg58 = new FixedLengthStringData(2).isAPartOf(filler, 114);
  	public FixedLengthStringData statcatg59 = new FixedLengthStringData(2).isAPartOf(filler, 116);
  	public FixedLengthStringData statcatg60 = new FixedLengthStringData(2).isAPartOf(filler, 118);
  	public FixedLengthStringData statcatg61 = new FixedLengthStringData(2).isAPartOf(filler, 120);
  	public FixedLengthStringData statcatg62 = new FixedLengthStringData(2).isAPartOf(filler, 122);
  	public FixedLengthStringData statcatg63 = new FixedLengthStringData(2).isAPartOf(filler, 124);
  	public FixedLengthStringData statcatg64 = new FixedLengthStringData(2).isAPartOf(filler, 126);
  	public FixedLengthStringData statcatg65 = new FixedLengthStringData(2).isAPartOf(filler, 128);
  	public FixedLengthStringData statcatg66 = new FixedLengthStringData(2).isAPartOf(filler, 130);
  	public FixedLengthStringData statcatg67 = new FixedLengthStringData(2).isAPartOf(filler, 132);
  	public FixedLengthStringData statcatg68 = new FixedLengthStringData(2).isAPartOf(filler, 134);
  	public FixedLengthStringData statcatg69 = new FixedLengthStringData(2).isAPartOf(filler, 136);
  	public FixedLengthStringData statcatg70 = new FixedLengthStringData(2).isAPartOf(filler, 138);
  	public FixedLengthStringData statcatg71 = new FixedLengthStringData(2).isAPartOf(filler, 140);
  	public FixedLengthStringData statcatg72 = new FixedLengthStringData(2).isAPartOf(filler, 142);
  	public FixedLengthStringData statcatg73 = new FixedLengthStringData(2).isAPartOf(filler, 144);
  	public FixedLengthStringData statcatg74 = new FixedLengthStringData(2).isAPartOf(filler, 146);
  	public FixedLengthStringData statcatg75 = new FixedLengthStringData(2).isAPartOf(filler, 148);
  	public FixedLengthStringData statcatg76 = new FixedLengthStringData(2).isAPartOf(filler, 150);
  	public FixedLengthStringData statcatg77 = new FixedLengthStringData(2).isAPartOf(filler, 152);
  	public FixedLengthStringData statcatg78 = new FixedLengthStringData(2).isAPartOf(filler, 154);
  	public FixedLengthStringData statcatg79 = new FixedLengthStringData(2).isAPartOf(filler, 156);
  	public FixedLengthStringData statcatg80 = new FixedLengthStringData(2).isAPartOf(filler, 158);
  	public FixedLengthStringData statcatg81 = new FixedLengthStringData(2).isAPartOf(filler, 160);
  	public FixedLengthStringData statcatg82 = new FixedLengthStringData(2).isAPartOf(filler, 162);
  	public FixedLengthStringData statcatg83 = new FixedLengthStringData(2).isAPartOf(filler, 164);
  	public FixedLengthStringData statcatg84 = new FixedLengthStringData(2).isAPartOf(filler, 166);
  	public FixedLengthStringData statcatg85 = new FixedLengthStringData(2).isAPartOf(filler, 168);
  	public FixedLengthStringData statcatg86 = new FixedLengthStringData(2).isAPartOf(filler, 170);
  	public FixedLengthStringData statcatg87 = new FixedLengthStringData(2).isAPartOf(filler, 172);
  	public FixedLengthStringData statcatg88 = new FixedLengthStringData(2).isAPartOf(filler, 174);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(324).isAPartOf(t6628Rec, 176, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6628Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6628Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}