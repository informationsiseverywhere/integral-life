/*
 * File: P6704.java
 * Date: 30 August 2009 0:54:09
 * Author: Quipoz Limited
 * 
 * Class transformed from P6704.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.statistics.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsu.general.procedures.Acmdesc;
import com.csc.fsu.general.recordstructures.Acmdescrec;
import com.csc.life.statistics.dataaccess.AgjnTableDAM;
import com.csc.life.statistics.screens.S6704ScreenVars;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* Generation Parameters SCRVER(02)               Do Not Delete!   <S9503>
*REMARKS.
*
*
*           INQUIRE ON AGENT STATISTICAL JOURNAL
*
* This program accepts the enquiry key and displays the corresponding
* record from AGJN file. If the required record is not found, the next
* record retrieved when the file is read will be displayed.
*
* The AGJN records will be arranged in the following ascending
* sequence:
*
*          Agency
*          Category
*          Area code
*          Servicing branch
*          Age band
*          Sum insured band
*          Premium band
*          Term band
*          Contract type
*          Coverage code
*          Overriding commission category
*          Premium statistical code
*          Currency
*          Account year
*
* The statistical accumulation key will be used to search the AGJN
* data. The first record found with a key equal to or greater than
* that input will be displayed.
*
*
*
* FUNCTION KEYS:
*               <EXIT>  Return to sub-menu.
*
*               <KILL>  Allow user to select another accumulation
*                       enquiry key. It clears the screen S6702
*                       and prompts user to enter a new enquiry key.
*
*               <ENTER> Display the accounts for the accumulation key
*                       if it has been changed, else display the
*                       accumulation account for the next month.
*                       Where enquiry months are exhausted
*                       (i.e. S6704-ACMN = 12), the first account
*                       month of the next AGJN record will be
*                       displayed.
*                       If no enquiry key is entered, the first record
*                       in the file is displayed.
*
*
* FILES USED:
*               AGJNSKM
*               DESCSKM
*
*
*****************************************************************
* </pre>
*/
public class P6704 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("P6704");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaAgntselPrev = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaScrAgntsel = new FixedLengthStringData(10);
	private FixedLengthStringData wsaaAgntsel = new FixedLengthStringData(8).isAPartOf(wsaaScrAgntsel, 0);
	private FixedLengthStringData wsaaFiller = new FixedLengthStringData(2).isAPartOf(wsaaScrAgntsel, 8);

	private FixedLengthStringData wsaaScreenKey = new FixedLengthStringData(41);
	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8).isAPartOf(wsaaScreenKey, 0);
	private FixedLengthStringData wsaaStatcat = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 8);
	private PackedDecimalData wsaaAcctyr = new PackedDecimalData(4, 0).isAPartOf(wsaaScreenKey, 10);
	private FixedLengthStringData wsaaAracde = new FixedLengthStringData(3).isAPartOf(wsaaScreenKey, 13);
	private FixedLengthStringData wsaaCntbranch = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 16);
	private FixedLengthStringData wsaaBandage = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 18);
	private FixedLengthStringData wsaaBandsa = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 20);
	private FixedLengthStringData wsaaBandprm = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 22);
	private FixedLengthStringData wsaaBandtrm = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 24);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaScreenKey, 26);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4).isAPartOf(wsaaScreenKey, 29);
	private FixedLengthStringData wsaaOvrdcat = new FixedLengthStringData(1).isAPartOf(wsaaScreenKey, 33);
	private FixedLengthStringData wsaaPstatcode = new FixedLengthStringData(2).isAPartOf(wsaaScreenKey, 34);
	private FixedLengthStringData wsaaCntcurr = new FixedLengthStringData(3).isAPartOf(wsaaScreenKey, 36);
	private ZonedDecimalData wsaaAcctmn = new ZonedDecimalData(2, 0).isAPartOf(wsaaScreenKey, 39).setUnsigned();
	private String e382 = "E382";
	private String t3629 = "T3629";
		/* FORMATS */
	private String agjnrec = "AGJNREC";
	private String descrec = "DESCREC";
	private FixedLengthStringData wsspUserArea = new FixedLengthStringData(768);
	private Acmdescrec acmdescrec = new Acmdescrec();
		/*Agent Statistical Accum. Journal*/
	private AgjnTableDAM agjnIO = new AgjnTableDAM();
		/*Logical File: Extra data screen*/
	private DescTableDAM descIO = new DescTableDAM();
	private S6704ScreenVars sv = ScreenProgram.getScreenVars( S6704ScreenVars.class);

	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		preExit, 
		exit2090, 
		exit2190, 
		exit2290
	}

	public P6704() {
		super();
		screenVars = sv;
		new ScreenModel("S6704", AppVars.getInstance(), sv);
	}

protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
	}

public void mainline(Object... parmArray)
	{
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspUserArea = convertAndSetParam(wsspUserArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		}
	}

protected void initialise1000()
	{
		/*INITIALISE*/
		wsaaScreenKey.set(SPACES);
		wsaaAcctyr.set(9999);
		wsaaAcctmn.set(ZERO);
		initialiseScreen1100();
		/*EXIT*/
	}

protected void initialiseScreen1100()
	{
		para1100();
	}

protected void para1100()
	{
		sv.dataArea.set(SPACES);
		sv.company.set(wsspcomn.company);
		sv.stcmthOut[varcom.nd.toInt()].set("Y");
		sv.acctyr.set(ZERO);
		sv.acmn.set(ZERO);
		sv.acyr.set(ZERO);
		sv.stcmth.set(ZERO);
		sv.stmmth.set(ZERO);
		sv.stpmth.set(ZERO);
		sv.stsmth.set(ZERO);
		sv.stvmth.set(ZERO);
		sv.effdate.set(ZERO);
		sv.transactionTime.set(ZERO);
		sv.user.set(ZERO);
	}

protected void preScreenEdit()
	{
		try {
			preStart();
		}
		catch (GOTOException e){
		}
	}

protected void preStart()
	{
		goTo(GotoLabel.preExit);
	}

protected void screenEdit2000()
	{
		try {
			validate2020();
			displayAgjnRecord2040();
		}
		catch (GOTOException e){
		}
	}

protected void validate2020()
	{
		if (isEQ(scrnparams.statuz,varcom.kill)) {
			initialiseScreen1100();
			wsspcomn.edterror.set("Y");
			sv.stcmthOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit2090);
		}
		wsaaScrAgntsel.set(sv.agntsel);
		if (isEQ(wsaaAgntsel,wsaaAgntselPrev)
		&& isEQ(sv.statcat,wsaaStatcat)
		&& isEQ(sv.acctyr,wsaaAcctyr)
		&& isEQ(sv.acmn,wsaaAcctmn)
		&& isEQ(sv.aracde,wsaaAracde)
		&& isEQ(sv.cntbranch,wsaaCntbranch)
		&& isEQ(sv.bandage,wsaaBandage)
		&& isEQ(sv.bandsa,wsaaBandsa)
		&& isEQ(sv.bandprm,wsaaBandprm)
		&& isEQ(sv.bandtrm,wsaaBandtrm)
		&& isEQ(sv.chdrtype,wsaaCnttype)
		&& isEQ(sv.crtable,wsaaCrtable)
		&& isEQ(sv.ovrdcat,wsaaOvrdcat)
		&& isEQ(sv.pstatcd,wsaaPstatcode)
		&& isEQ(sv.cntcurr,wsaaCntcurr)) {
			agjnIO.setFunction(varcom.nextr);
		}
		else {
			agjnIO.setFunction(varcom.begn);
		}
		readAgjnRecord2100();
		moveCurrentToWsaa2200();
		if (isEQ(agjnIO.getStatuz(),varcom.endp)
		|| isNE(agjnIO.getChdrcoy(),wsspcomn.company)) {
			initialiseScreen1100();
			sv.ovrdcatErr.set(e382);
			wsspcomn.edterror.set("Y");
			sv.stcmthOut[varcom.nd.toInt()].set("Y");
			goTo(GotoLabel.exit2090);
		}
	}

protected void displayAgjnRecord2040()
	{
		sv.stcmthOut[varcom.nd.toInt()].set("N");
		sv.acmn.set(agjnIO.getAcctmonth());
		sv.acyr.set(agjnIO.getAcctyr());
		sv.acctyr.set(agjnIO.getAcctyr());
		wsaaAgntsel.set(agjnIO.getAgntnum());
		wsaaFiller.set(SPACES);
		sv.agntsel.set(wsaaScrAgntsel);
		sv.statcat.set(agjnIO.getStatcat());
		sv.aracde.set(agjnIO.getAracde());
		sv.cntbranch.set(agjnIO.getCntbranch());
		sv.bandage.set(agjnIO.getBandage());
		sv.bandprm.set(agjnIO.getBandprm());
		sv.bandsa.set(agjnIO.getBandsa());
		sv.bandtrm.set(agjnIO.getBandtrm());
		sv.chdrtype.set(agjnIO.getCnttype());
		sv.crtable.set(agjnIO.getCrtable());
		sv.ovrdcat.set(agjnIO.getOvrdcat());
		sv.pstatcd.set(agjnIO.getPstatcode());
		sv.cntcurr.set(agjnIO.getCntcurr());
		sv.stcmth.set(agjnIO.getStcmth());
		sv.stvmth.set(agjnIO.getStvmth());
		sv.stpmth.set(agjnIO.getStpmth());
		sv.stsmth.set(agjnIO.getStsmth());
		sv.stmmth.set(agjnIO.getStmmth());
		sv.effdate.set(agjnIO.getEffdate());
		sv.transactionTime.set(agjnIO.getTransactionTime());
		sv.user.set(agjnIO.getUser());
		wsspcomn.edterror.set("Y");
		goTo(GotoLabel.exit2090);
	}

protected void checkForErrors2080()
	{
		if (isNE(sv.errorIndicators,SPACES)) {
			wsspcomn.edterror.set("Y");
		}
	}

protected void readAgjnRecord2100()
	{
		try {
			para2110();
			setUpCurrencyDesc2120();
			setUpMonthDesc2130();
		}
		catch (GOTOException e){
		}
	}

protected void para2110()
	{
		agjnIO.setChdrcoy(wsspcomn.company);
		wsaaScrAgntsel.set(sv.agntsel);
		agjnIO.setAgntnum(wsaaAgntsel);
		agjnIO.setStatcat(sv.statcat);
		agjnIO.setAcctyr(sv.acctyr);
		agjnIO.setAcctmonth(sv.acmn);
		agjnIO.setAracde(sv.aracde);
		agjnIO.setCntbranch(sv.cntbranch);
		agjnIO.setBandage(sv.bandage);
		agjnIO.setBandsa(sv.bandsa);
		agjnIO.setBandprm(sv.bandprm);
		agjnIO.setBandtrm(sv.bandtrm);
		agjnIO.setCnttype(sv.chdrtype);
		agjnIO.setCrtable(sv.crtable);
		agjnIO.setOvrdcat(sv.ovrdcat);
		agjnIO.setPstatcode(sv.pstatcd);
		agjnIO.setCntcurr(sv.cntcurr);
		agjnIO.setEffdate(sv.effdate);
		agjnIO.setTransactionTime(sv.transactionTime);
		agjnIO.setFormat(agjnrec);
		SmartFileCode.execute(appVars, agjnIO);
		if (isNE(agjnIO.getStatuz(),varcom.oK)
		&& isNE(agjnIO.getStatuz(),varcom.endp)) {
			syserrrec.params.set(agjnIO.getParams());
			syserrrec.statuz.set(agjnIO.getStatuz());
			fatalError600();
		}
		if (isEQ(agjnIO.getStatuz(),varcom.endp)
		|| isNE(agjnIO.getChdrcoy(),wsspcomn.company)) {
			goTo(GotoLabel.exit2190);
		}
	}

protected void setUpCurrencyDesc2120()
	{
		descIO.setDescpfx("IT");
		descIO.setDesccoy(agjnIO.getChdrcoy());
		descIO.setDesctabl(t3629);
		descIO.setDescitem(agjnIO.getCntcurr());
		descIO.setItemseq(SPACES);
		descIO.setLanguage(wsspcomn.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(),varcom.oK)) {
			syserrrec.params.set(descIO.getParams());
			fatalError600();
		}
		sv.descrip.set(descIO.getLongdesc());
	}

protected void setUpMonthDesc2130()
	{
		acmdescrec.company.set(agjnIO.getChdrcoy());
		acmdescrec.language.set(wsspcomn.language);
		acmdescrec.branch.set(agjnIO.getCntbranch());
		acmdescrec.function.set("GETD");
		callProgram(Acmdesc.class, acmdescrec.acmdescRec);
		if (isNE(acmdescrec.statuz,varcom.oK)) {
			syserrrec.statuz.set(acmdescrec.statuz);
			fatalError600();
		}
		sv.mthldesc.set(acmdescrec.lngdesc[agjnIO.getAcctmonth().toInt()]);
	}

protected void moveCurrentToWsaa2200()
	{
		try {
			para2210();
		}
		catch (GOTOException e){
		}
	}

protected void para2210()
	{
		if (isEQ(agjnIO.getStatuz(),varcom.endp)
		|| isNE(agjnIO.getChdrcoy(),wsspcomn.company)) {
			goTo(GotoLabel.exit2290);
		}
		wsaaAgntselPrev.set(agjnIO.getAgntnum());
		wsaaStatcat.set(agjnIO.getStatcat());
		wsaaAcctyr.set(agjnIO.getAcctyr());
		wsaaAcctmn.set(agjnIO.getAcctmonth());
		wsaaAracde.set(agjnIO.getAracde());
		wsaaCntbranch.set(agjnIO.getCntbranch());
		wsaaBandage.set(agjnIO.getBandage());
		wsaaBandsa.set(agjnIO.getBandsa());
		wsaaBandprm.set(agjnIO.getBandprm());
		wsaaBandtrm.set(agjnIO.getBandtrm());
		wsaaCnttype.set(agjnIO.getCnttype());
		wsaaCrtable.set(agjnIO.getCrtable());
		wsaaOvrdcat.set(agjnIO.getOvrdcat());
		wsaaPstatcode.set(agjnIO.getPstatcode());
		wsaaCntcurr.set(agjnIO.getCntcurr());
	}

protected void update3000()
	{
		/*UPDATE-DATABASE*/
		/*EXIT*/
	}

protected void whereNext4000()
	{
		/*NEXT-PROGRAM*/
		wsspcomn.programPtr.add(1);
		/*EXIT*/
	}
}
