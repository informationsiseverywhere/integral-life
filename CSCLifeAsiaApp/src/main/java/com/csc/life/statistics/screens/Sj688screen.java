package com.csc.life.statistics.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:46
 * @author Quipoz
 */
public class Sj688screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] {21, 4, 17, 22, 18, 23, 15, 24, 16, 1, 2, 3}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sj688ScreenVars sv = (Sj688ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sj688screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {}

	public static void clearClassString(VarModel pv) {
		Sj688ScreenVars screenVars = (Sj688ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.company.setClassString("");
		screenVars.tabl.setClassString("");
		screenVars.item.setClassString("");
		screenVars.longdesc.setClassString("");
		screenVars.trcode03.setClassString("");
		screenVars.trcode04.setClassString("");
		screenVars.trcode01.setClassString("");
		screenVars.trcode02.setClassString("");
		screenVars.trcode05.setClassString("");
		screenVars.trcode06.setClassString("");
		screenVars.trcode07.setClassString("");
		screenVars.trcode08.setClassString("");
		screenVars.trcode09.setClassString("");
		screenVars.trcode10.setClassString("");
		screenVars.trcode11.setClassString("");
		screenVars.trcode12.setClassString("");
		screenVars.trcode13.setClassString("");
		screenVars.trcode14.setClassString("");
		screenVars.trcode15.setClassString("");
	}

/**
 * Clear all the variables in Sj688screen
 */
	public static void clear(VarModel pv) {
		Sj688ScreenVars screenVars = (Sj688ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.company.clear();
		screenVars.tabl.clear();
		screenVars.item.clear();
		screenVars.longdesc.clear();
		screenVars.trcode03.clear();
		screenVars.trcode04.clear();
		screenVars.trcode01.clear();
		screenVars.trcode02.clear();
		screenVars.trcode05.clear();
		screenVars.trcode06.clear();
		screenVars.trcode07.clear();
		screenVars.trcode08.clear();
		screenVars.trcode09.clear();
		screenVars.trcode10.clear();
		screenVars.trcode11.clear();
		screenVars.trcode12.clear();
		screenVars.trcode13.clear();
		screenVars.trcode14.clear();
		screenVars.trcode15.clear();
	}
}
