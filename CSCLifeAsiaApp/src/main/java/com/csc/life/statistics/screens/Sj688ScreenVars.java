package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for SJ688
 * @version 1.0 generated on 30/08/09 07:06
 * @author Quipoz
 */
public class Sj688ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(408);
	public FixedLengthStringData dataFields = new FixedLengthStringData(104).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,39);
	public FixedLengthStringData trcodes = new FixedLengthStringData(60).isAPartOf(dataFields, 44);
	public FixedLengthStringData[] trcode = FLSArrayPartOfStructure(15, 4, trcodes, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(60).isAPartOf(trcodes, 0, FILLER_REDEFINE);
	public FixedLengthStringData trcode01 = DD.trcode.copy().isAPartOf(filler,0);
	public FixedLengthStringData trcode02 = DD.trcode.copy().isAPartOf(filler,4);
	public FixedLengthStringData trcode03 = DD.trcode.copy().isAPartOf(filler,8);
	public FixedLengthStringData trcode04 = DD.trcode.copy().isAPartOf(filler,12);
	public FixedLengthStringData trcode05 = DD.trcode.copy().isAPartOf(filler,16);
	public FixedLengthStringData trcode06 = DD.trcode.copy().isAPartOf(filler,20);
	public FixedLengthStringData trcode07 = DD.trcode.copy().isAPartOf(filler,24);
	public FixedLengthStringData trcode08 = DD.trcode.copy().isAPartOf(filler,28);
	public FixedLengthStringData trcode09 = DD.trcode.copy().isAPartOf(filler,32);
	public FixedLengthStringData trcode10 = DD.trcode.copy().isAPartOf(filler,36);
	public FixedLengthStringData trcode11 = DD.trcode.copy().isAPartOf(filler,40);
	public FixedLengthStringData trcode12 = DD.trcode.copy().isAPartOf(filler,44);
	public FixedLengthStringData trcode13 = DD.trcode.copy().isAPartOf(filler,48);
	public FixedLengthStringData trcode14 = DD.trcode.copy().isAPartOf(filler,52);
	public FixedLengthStringData trcode15 = DD.trcode.copy().isAPartOf(filler,56);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(76).isAPartOf(dataArea, 104);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData trcodesErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData[] trcodeErr = FLSArrayPartOfStructure(15, 4, trcodesErr, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(60).isAPartOf(trcodesErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData trcode01Err = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	public FixedLengthStringData trcode02Err = new FixedLengthStringData(4).isAPartOf(filler1, 4);
	public FixedLengthStringData trcode03Err = new FixedLengthStringData(4).isAPartOf(filler1, 8);
	public FixedLengthStringData trcode04Err = new FixedLengthStringData(4).isAPartOf(filler1, 12);
	public FixedLengthStringData trcode05Err = new FixedLengthStringData(4).isAPartOf(filler1, 16);
	public FixedLengthStringData trcode06Err = new FixedLengthStringData(4).isAPartOf(filler1, 20);
	public FixedLengthStringData trcode07Err = new FixedLengthStringData(4).isAPartOf(filler1, 24);
	public FixedLengthStringData trcode08Err = new FixedLengthStringData(4).isAPartOf(filler1, 28);
	public FixedLengthStringData trcode09Err = new FixedLengthStringData(4).isAPartOf(filler1, 32);
	public FixedLengthStringData trcode10Err = new FixedLengthStringData(4).isAPartOf(filler1, 36);
	public FixedLengthStringData trcode11Err = new FixedLengthStringData(4).isAPartOf(filler1, 40);
	public FixedLengthStringData trcode12Err = new FixedLengthStringData(4).isAPartOf(filler1, 44);
	public FixedLengthStringData trcode13Err = new FixedLengthStringData(4).isAPartOf(filler1, 48);
	public FixedLengthStringData trcode14Err = new FixedLengthStringData(4).isAPartOf(filler1, 52);
	public FixedLengthStringData trcode15Err = new FixedLengthStringData(4).isAPartOf(filler1, 56);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(228).isAPartOf(dataArea, 180);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData trcodesOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 48);
	public FixedLengthStringData[] trcodeOut = FLSArrayPartOfStructure(15, 12, trcodesOut, 0);
	public FixedLengthStringData[][] trcodeO = FLSDArrayPartOfArrayStructure(12, 1, trcodeOut, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(180).isAPartOf(trcodesOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] trcode01Out = FLSArrayPartOfStructure(12, 1, filler2, 0);
	public FixedLengthStringData[] trcode02Out = FLSArrayPartOfStructure(12, 1, filler2, 12);
	public FixedLengthStringData[] trcode03Out = FLSArrayPartOfStructure(12, 1, filler2, 24);
	public FixedLengthStringData[] trcode04Out = FLSArrayPartOfStructure(12, 1, filler2, 36);
	public FixedLengthStringData[] trcode05Out = FLSArrayPartOfStructure(12, 1, filler2, 48);
	public FixedLengthStringData[] trcode06Out = FLSArrayPartOfStructure(12, 1, filler2, 60);
	public FixedLengthStringData[] trcode07Out = FLSArrayPartOfStructure(12, 1, filler2, 72);
	public FixedLengthStringData[] trcode08Out = FLSArrayPartOfStructure(12, 1, filler2, 84);
	public FixedLengthStringData[] trcode09Out = FLSArrayPartOfStructure(12, 1, filler2, 96);
	public FixedLengthStringData[] trcode10Out = FLSArrayPartOfStructure(12, 1, filler2, 108);
	public FixedLengthStringData[] trcode11Out = FLSArrayPartOfStructure(12, 1, filler2, 120);
	public FixedLengthStringData[] trcode12Out = FLSArrayPartOfStructure(12, 1, filler2, 132);
	public FixedLengthStringData[] trcode13Out = FLSArrayPartOfStructure(12, 1, filler2, 144);
	public FixedLengthStringData[] trcode14Out = FLSArrayPartOfStructure(12, 1, filler2, 156);
	public FixedLengthStringData[] trcode15Out = FLSArrayPartOfStructure(12, 1, filler2, 168);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData Sj688screenWritten = new LongData(0);
	public LongData Sj688protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public Sj688ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, longdesc, trcode03, trcode04, trcode01, trcode02, trcode05, trcode06, trcode07, trcode08, trcode09, trcode10, trcode11, trcode12, trcode13, trcode14, trcode15};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, longdescOut, trcode03Out, trcode04Out, trcode01Out, trcode02Out, trcode05Out, trcode06Out, trcode07Out, trcode08Out, trcode09Out, trcode10Out, trcode11Out, trcode12Out, trcode13Out, trcode14Out, trcode15Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, longdescErr, trcode03Err, trcode04Err, trcode01Err, trcode02Err, trcode05Err, trcode06Err, trcode07Err, trcode08Err, trcode09Err, trcode10Err, trcode11Err, trcode12Err, trcode13Err, trcode14Err, trcode15Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sj688screen.class;
		protectRecord = Sj688protect.class;
	}

}
