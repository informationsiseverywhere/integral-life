package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:02:26
 * Description:
 * Copybook name: COVRSTSKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrstskey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrstsFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData covrstsKey = new FixedLengthStringData(64).isAPartOf(covrstsFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrstsChdrcoy = new FixedLengthStringData(1).isAPartOf(covrstsKey, 0);
  	public FixedLengthStringData covrstsChdrnum = new FixedLengthStringData(8).isAPartOf(covrstsKey, 1);
  	public FixedLengthStringData covrstsLife = new FixedLengthStringData(2).isAPartOf(covrstsKey, 9);
  	public FixedLengthStringData covrstsCoverage = new FixedLengthStringData(2).isAPartOf(covrstsKey, 11);
  	public FixedLengthStringData covrstsRider = new FixedLengthStringData(2).isAPartOf(covrstsKey, 13);
  	public PackedDecimalData covrstsPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrstsKey, 15);
  	public PackedDecimalData covrstsTranno = new PackedDecimalData(5, 0).isAPartOf(covrstsKey, 18);
  	public FixedLengthStringData filler = new FixedLengthStringData(43).isAPartOf(covrstsKey, 21, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrstsFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrstsFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}