package com.csc.life.statistics.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for S6627
 * @version 1.0 generated on 30/08/09 06:54
 * @author Quipoz
 */
public class S6627ScreenVars extends SmartVarModel { 


	public FixedLengthStringData dataArea = new FixedLengthStringData(1308);
	public FixedLengthStringData dataFields = new FixedLengthStringData(524).isAPartOf(dataArea, 0);
	public FixedLengthStringData company = DD.company.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData item = DD.item.copy().isAPartOf(dataFields,1);
	public FixedLengthStringData longdesc = DD.longdesc.copy().isAPartOf(dataFields,9);
	public FixedLengthStringData statbands = new FixedLengthStringData(30).isAPartOf(dataFields, 39);
	public FixedLengthStringData[] statband = FLSArrayPartOfStructure(15, 2, statbands, 0);
	public FixedLengthStringData filler = new FixedLengthStringData(30).isAPartOf(statbands, 0, FILLER_REDEFINE);
	public FixedLengthStringData statband01 = DD.statband.copy().isAPartOf(filler,0);
	public FixedLengthStringData statband02 = DD.statband.copy().isAPartOf(filler,2);
	public FixedLengthStringData statband03 = DD.statband.copy().isAPartOf(filler,4);
	public FixedLengthStringData statband04 = DD.statband.copy().isAPartOf(filler,6);
	public FixedLengthStringData statband05 = DD.statband.copy().isAPartOf(filler,8);
	public FixedLengthStringData statband06 = DD.statband.copy().isAPartOf(filler,10);
	public FixedLengthStringData statband07 = DD.statband.copy().isAPartOf(filler,12);
	public FixedLengthStringData statband08 = DD.statband.copy().isAPartOf(filler,14);
	public FixedLengthStringData statband09 = DD.statband.copy().isAPartOf(filler,16);
	public FixedLengthStringData statband10 = DD.statband.copy().isAPartOf(filler,18);
	public FixedLengthStringData statband11 = DD.statband.copy().isAPartOf(filler,20);
	public FixedLengthStringData statband12 = DD.statband.copy().isAPartOf(filler,22);
	public FixedLengthStringData statband13 = DD.statband.copy().isAPartOf(filler,24);
	public FixedLengthStringData statband14 = DD.statband.copy().isAPartOf(filler,26);
	public FixedLengthStringData statband15 = DD.statband.copy().isAPartOf(filler,28);
	public FixedLengthStringData statfroms = new FixedLengthStringData(225).isAPartOf(dataFields, 69);
	public ZonedDecimalData[] statfrom = ZDArrayPartOfStructure(15, 15, 0, statfroms, 0);
	public FixedLengthStringData filler1 = new FixedLengthStringData(225).isAPartOf(statfroms, 0, FILLER_REDEFINE);
	public ZonedDecimalData statfrom01 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,0);
	public ZonedDecimalData statfrom02 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,15);
	public ZonedDecimalData statfrom03 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,30);
	public ZonedDecimalData statfrom04 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,45);
	public ZonedDecimalData statfrom05 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,60);
	public ZonedDecimalData statfrom06 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,75);
	public ZonedDecimalData statfrom07 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,90);
	public ZonedDecimalData statfrom08 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,105);
	public ZonedDecimalData statfrom09 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,120);
	public ZonedDecimalData statfrom10 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,135);
	public ZonedDecimalData statfrom11 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,150);
	public ZonedDecimalData statfrom12 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,165);
	public ZonedDecimalData statfrom13 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,180);
	public ZonedDecimalData statfrom14 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,195);
	public ZonedDecimalData statfrom15 = DD.statfrom.copyToZonedDecimal().isAPartOf(filler1,210);
	public FixedLengthStringData stattos = new FixedLengthStringData(225).isAPartOf(dataFields, 294);
	public ZonedDecimalData[] statto = ZDArrayPartOfStructure(15, 15, 0, stattos, 0);
	public FixedLengthStringData filler2 = new FixedLengthStringData(225).isAPartOf(stattos, 0, FILLER_REDEFINE);
	public ZonedDecimalData statto01 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,0);
	public ZonedDecimalData statto02 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,15);
	public ZonedDecimalData statto03 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,30);
	public ZonedDecimalData statto04 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,45);
	public ZonedDecimalData statto05 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,60);
	public ZonedDecimalData statto06 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,75);
	public ZonedDecimalData statto07 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,90);
	public ZonedDecimalData statto08 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,105);
	public ZonedDecimalData statto09 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,120);
	public ZonedDecimalData statto10 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,135);
	public ZonedDecimalData statto11 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,150);
	public ZonedDecimalData statto12 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,165);
	public ZonedDecimalData statto13 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,180);
	public ZonedDecimalData statto14 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,195);
	public ZonedDecimalData statto15 = DD.statto.copyToZonedDecimal().isAPartOf(filler2,210);
	public FixedLengthStringData tabl = DD.tabl.copy().isAPartOf(dataFields,519);
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(196).isAPartOf(dataArea, 524);
	public FixedLengthStringData companyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData itemErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData longdescErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData statbandsErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData[] statbandErr = FLSArrayPartOfStructure(15, 4, statbandsErr, 0);
	public FixedLengthStringData filler3 = new FixedLengthStringData(60).isAPartOf(statbandsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData statband01Err = new FixedLengthStringData(4).isAPartOf(filler3, 0);
	public FixedLengthStringData statband02Err = new FixedLengthStringData(4).isAPartOf(filler3, 4);
	public FixedLengthStringData statband03Err = new FixedLengthStringData(4).isAPartOf(filler3, 8);
	public FixedLengthStringData statband04Err = new FixedLengthStringData(4).isAPartOf(filler3, 12);
	public FixedLengthStringData statband05Err = new FixedLengthStringData(4).isAPartOf(filler3, 16);
	public FixedLengthStringData statband06Err = new FixedLengthStringData(4).isAPartOf(filler3, 20);
	public FixedLengthStringData statband07Err = new FixedLengthStringData(4).isAPartOf(filler3, 24);
	public FixedLengthStringData statband08Err = new FixedLengthStringData(4).isAPartOf(filler3, 28);
	public FixedLengthStringData statband09Err = new FixedLengthStringData(4).isAPartOf(filler3, 32);
	public FixedLengthStringData statband10Err = new FixedLengthStringData(4).isAPartOf(filler3, 36);
	public FixedLengthStringData statband11Err = new FixedLengthStringData(4).isAPartOf(filler3, 40);
	public FixedLengthStringData statband12Err = new FixedLengthStringData(4).isAPartOf(filler3, 44);
	public FixedLengthStringData statband13Err = new FixedLengthStringData(4).isAPartOf(filler3, 48);
	public FixedLengthStringData statband14Err = new FixedLengthStringData(4).isAPartOf(filler3, 52);
	public FixedLengthStringData statband15Err = new FixedLengthStringData(4).isAPartOf(filler3, 56);
	public FixedLengthStringData statfromsErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 72);
	public FixedLengthStringData[] statfromErr = FLSArrayPartOfStructure(15, 4, statfromsErr, 0);
	public FixedLengthStringData filler4 = new FixedLengthStringData(60).isAPartOf(statfromsErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData statfrom01Err = new FixedLengthStringData(4).isAPartOf(filler4, 0);
	public FixedLengthStringData statfrom02Err = new FixedLengthStringData(4).isAPartOf(filler4, 4);
	public FixedLengthStringData statfrom03Err = new FixedLengthStringData(4).isAPartOf(filler4, 8);
	public FixedLengthStringData statfrom04Err = new FixedLengthStringData(4).isAPartOf(filler4, 12);
	public FixedLengthStringData statfrom05Err = new FixedLengthStringData(4).isAPartOf(filler4, 16);
	public FixedLengthStringData statfrom06Err = new FixedLengthStringData(4).isAPartOf(filler4, 20);
	public FixedLengthStringData statfrom07Err = new FixedLengthStringData(4).isAPartOf(filler4, 24);
	public FixedLengthStringData statfrom08Err = new FixedLengthStringData(4).isAPartOf(filler4, 28);
	public FixedLengthStringData statfrom09Err = new FixedLengthStringData(4).isAPartOf(filler4, 32);
	public FixedLengthStringData statfrom10Err = new FixedLengthStringData(4).isAPartOf(filler4, 36);
	public FixedLengthStringData statfrom11Err = new FixedLengthStringData(4).isAPartOf(filler4, 40);
	public FixedLengthStringData statfrom12Err = new FixedLengthStringData(4).isAPartOf(filler4, 44);
	public FixedLengthStringData statfrom13Err = new FixedLengthStringData(4).isAPartOf(filler4, 48);
	public FixedLengthStringData statfrom14Err = new FixedLengthStringData(4).isAPartOf(filler4, 52);
	public FixedLengthStringData statfrom15Err = new FixedLengthStringData(4).isAPartOf(filler4, 56);
	public FixedLengthStringData stattosErr = new FixedLengthStringData(60).isAPartOf(errorIndicators, 132);
	public FixedLengthStringData[] stattoErr = FLSArrayPartOfStructure(15, 4, stattosErr, 0);
	public FixedLengthStringData filler5 = new FixedLengthStringData(60).isAPartOf(stattosErr, 0, FILLER_REDEFINE);
	public FixedLengthStringData statto01Err = new FixedLengthStringData(4).isAPartOf(filler5, 0);
	public FixedLengthStringData statto02Err = new FixedLengthStringData(4).isAPartOf(filler5, 4);
	public FixedLengthStringData statto03Err = new FixedLengthStringData(4).isAPartOf(filler5, 8);
	public FixedLengthStringData statto04Err = new FixedLengthStringData(4).isAPartOf(filler5, 12);
	public FixedLengthStringData statto05Err = new FixedLengthStringData(4).isAPartOf(filler5, 16);
	public FixedLengthStringData statto06Err = new FixedLengthStringData(4).isAPartOf(filler5, 20);
	public FixedLengthStringData statto07Err = new FixedLengthStringData(4).isAPartOf(filler5, 24);
	public FixedLengthStringData statto08Err = new FixedLengthStringData(4).isAPartOf(filler5, 28);
	public FixedLengthStringData statto09Err = new FixedLengthStringData(4).isAPartOf(filler5, 32);
	public FixedLengthStringData statto10Err = new FixedLengthStringData(4).isAPartOf(filler5, 36);
	public FixedLengthStringData statto11Err = new FixedLengthStringData(4).isAPartOf(filler5, 40);
	public FixedLengthStringData statto12Err = new FixedLengthStringData(4).isAPartOf(filler5, 44);
	public FixedLengthStringData statto13Err = new FixedLengthStringData(4).isAPartOf(filler5, 48);
	public FixedLengthStringData statto14Err = new FixedLengthStringData(4).isAPartOf(filler5, 52);
	public FixedLengthStringData statto15Err = new FixedLengthStringData(4).isAPartOf(filler5, 56);
	public FixedLengthStringData tablErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 192);
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(588).isAPartOf(dataArea, 720);
	public FixedLengthStringData[] companyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] itemOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] longdescOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData statbandsOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 36);
	public FixedLengthStringData[] statbandOut = FLSArrayPartOfStructure(15, 12, statbandsOut, 0);
	public FixedLengthStringData[][] statbandO = FLSDArrayPartOfArrayStructure(12, 1, statbandOut, 0);
	public FixedLengthStringData filler6 = new FixedLengthStringData(180).isAPartOf(statbandsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] statband01Out = FLSArrayPartOfStructure(12, 1, filler6, 0);
	public FixedLengthStringData[] statband02Out = FLSArrayPartOfStructure(12, 1, filler6, 12);
	public FixedLengthStringData[] statband03Out = FLSArrayPartOfStructure(12, 1, filler6, 24);
	public FixedLengthStringData[] statband04Out = FLSArrayPartOfStructure(12, 1, filler6, 36);
	public FixedLengthStringData[] statband05Out = FLSArrayPartOfStructure(12, 1, filler6, 48);
	public FixedLengthStringData[] statband06Out = FLSArrayPartOfStructure(12, 1, filler6, 60);
	public FixedLengthStringData[] statband07Out = FLSArrayPartOfStructure(12, 1, filler6, 72);
	public FixedLengthStringData[] statband08Out = FLSArrayPartOfStructure(12, 1, filler6, 84);
	public FixedLengthStringData[] statband09Out = FLSArrayPartOfStructure(12, 1, filler6, 96);
	public FixedLengthStringData[] statband10Out = FLSArrayPartOfStructure(12, 1, filler6, 108);
	public FixedLengthStringData[] statband11Out = FLSArrayPartOfStructure(12, 1, filler6, 120);
	public FixedLengthStringData[] statband12Out = FLSArrayPartOfStructure(12, 1, filler6, 132);
	public FixedLengthStringData[] statband13Out = FLSArrayPartOfStructure(12, 1, filler6, 144);
	public FixedLengthStringData[] statband14Out = FLSArrayPartOfStructure(12, 1, filler6, 156);
	public FixedLengthStringData[] statband15Out = FLSArrayPartOfStructure(12, 1, filler6, 168);
	public FixedLengthStringData statfromsOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 216);
	public FixedLengthStringData[] statfromOut = FLSArrayPartOfStructure(15, 12, statfromsOut, 0);
	public FixedLengthStringData[][] statfromO = FLSDArrayPartOfArrayStructure(12, 1, statfromOut, 0);
	public FixedLengthStringData filler7 = new FixedLengthStringData(180).isAPartOf(statfromsOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] statfrom01Out = FLSArrayPartOfStructure(12, 1, filler7, 0);
	public FixedLengthStringData[] statfrom02Out = FLSArrayPartOfStructure(12, 1, filler7, 12);
	public FixedLengthStringData[] statfrom03Out = FLSArrayPartOfStructure(12, 1, filler7, 24);
	public FixedLengthStringData[] statfrom04Out = FLSArrayPartOfStructure(12, 1, filler7, 36);
	public FixedLengthStringData[] statfrom05Out = FLSArrayPartOfStructure(12, 1, filler7, 48);
	public FixedLengthStringData[] statfrom06Out = FLSArrayPartOfStructure(12, 1, filler7, 60);
	public FixedLengthStringData[] statfrom07Out = FLSArrayPartOfStructure(12, 1, filler7, 72);
	public FixedLengthStringData[] statfrom08Out = FLSArrayPartOfStructure(12, 1, filler7, 84);
	public FixedLengthStringData[] statfrom09Out = FLSArrayPartOfStructure(12, 1, filler7, 96);
	public FixedLengthStringData[] statfrom10Out = FLSArrayPartOfStructure(12, 1, filler7, 108);
	public FixedLengthStringData[] statfrom11Out = FLSArrayPartOfStructure(12, 1, filler7, 120);
	public FixedLengthStringData[] statfrom12Out = FLSArrayPartOfStructure(12, 1, filler7, 132);
	public FixedLengthStringData[] statfrom13Out = FLSArrayPartOfStructure(12, 1, filler7, 144);
	public FixedLengthStringData[] statfrom14Out = FLSArrayPartOfStructure(12, 1, filler7, 156);
	public FixedLengthStringData[] statfrom15Out = FLSArrayPartOfStructure(12, 1, filler7, 168);
	public FixedLengthStringData stattosOut = new FixedLengthStringData(180).isAPartOf(outputIndicators, 396);
	public FixedLengthStringData[] stattoOut = FLSArrayPartOfStructure(15, 12, stattosOut, 0);
	public FixedLengthStringData[][] stattoO = FLSDArrayPartOfArrayStructure(12, 1, stattoOut, 0);
	public FixedLengthStringData filler8 = new FixedLengthStringData(180).isAPartOf(stattosOut, 0, FILLER_REDEFINE);
	public FixedLengthStringData[] statto01Out = FLSArrayPartOfStructure(12, 1, filler8, 0);
	public FixedLengthStringData[] statto02Out = FLSArrayPartOfStructure(12, 1, filler8, 12);
	public FixedLengthStringData[] statto03Out = FLSArrayPartOfStructure(12, 1, filler8, 24);
	public FixedLengthStringData[] statto04Out = FLSArrayPartOfStructure(12, 1, filler8, 36);
	public FixedLengthStringData[] statto05Out = FLSArrayPartOfStructure(12, 1, filler8, 48);
	public FixedLengthStringData[] statto06Out = FLSArrayPartOfStructure(12, 1, filler8, 60);
	public FixedLengthStringData[] statto07Out = FLSArrayPartOfStructure(12, 1, filler8, 72);
	public FixedLengthStringData[] statto08Out = FLSArrayPartOfStructure(12, 1, filler8, 84);
	public FixedLengthStringData[] statto09Out = FLSArrayPartOfStructure(12, 1, filler8, 96);
	public FixedLengthStringData[] statto10Out = FLSArrayPartOfStructure(12, 1, filler8, 108);
	public FixedLengthStringData[] statto11Out = FLSArrayPartOfStructure(12, 1, filler8, 120);
	public FixedLengthStringData[] statto12Out = FLSArrayPartOfStructure(12, 1, filler8, 132);
	public FixedLengthStringData[] statto13Out = FLSArrayPartOfStructure(12, 1, filler8, 144);
	public FixedLengthStringData[] statto14Out = FLSArrayPartOfStructure(12, 1, filler8, 156);
	public FixedLengthStringData[] statto15Out = FLSArrayPartOfStructure(12, 1, filler8, 168);
	public FixedLengthStringData[] tablOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 576);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();


	public LongData S6627screenWritten = new LongData(0);
	public LongData S6627protectWritten = new LongData(0);

	public boolean hasSubfile() {
		return false;
	}


	public S6627ScreenVars() {
		super();
		initialiseScreenVars();
	}

	protected void initialiseScreenVars() {
		screenFields = new BaseData[] {company, tabl, item, statband01, statband02, statband03, statband04, statband05, statband06, statband07, statband08, statband09, statband10, statband11, statband12, statband13, statband14, statband15, longdesc, statfrom01, statfrom02, statfrom03, statfrom04, statfrom05, statfrom06, statfrom07, statfrom08, statfrom09, statfrom10, statfrom11, statfrom12, statfrom13, statfrom14, statfrom15, statto01, statto02, statto03, statto04, statto05, statto06, statto07, statto08, statto09, statto10, statto11, statto12, statto13, statto14, statto15};
		screenOutFields = new BaseData[][] {companyOut, tablOut, itemOut, statband01Out, statband02Out, statband03Out, statband04Out, statband05Out, statband06Out, statband07Out, statband08Out, statband09Out, statband10Out, statband11Out, statband12Out, statband13Out, statband14Out, statband15Out, longdescOut, statfrom01Out, statfrom02Out, statfrom03Out, statfrom04Out, statfrom05Out, statfrom06Out, statfrom07Out, statfrom08Out, statfrom09Out, statfrom10Out, statfrom11Out, statfrom12Out, statfrom13Out, statfrom14Out, statfrom15Out, statto01Out, statto02Out, statto03Out, statto04Out, statto05Out, statto06Out, statto07Out, statto08Out, statto09Out, statto10Out, statto11Out, statto12Out, statto13Out, statto14Out, statto15Out};
		screenErrFields = new BaseData[] {companyErr, tablErr, itemErr, statband01Err, statband02Err, statband03Err, statband04Err, statband05Err, statband06Err, statband07Err, statband08Err, statband09Err, statband10Err, statband11Err, statband12Err, statband13Err, statband14Err, statband15Err, longdescErr, statfrom01Err, statfrom02Err, statfrom03Err, statfrom04Err, statfrom05Err, statfrom06Err, statfrom07Err, statfrom08Err, statfrom09Err, statfrom10Err, statfrom11Err, statfrom12Err, statfrom13Err, statfrom14Err, statfrom15Err, statto01Err, statto02Err, statto03Err, statto04Err, statto05Err, statto06Err, statto07Err, statto08Err, statto09Err, statto10Err, statto11Err, statto12Err, statto13Err, statto14Err, statto15Err};
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};

		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = S6627screen.class;
		protectRecord = S6627protect.class;
	}

}
