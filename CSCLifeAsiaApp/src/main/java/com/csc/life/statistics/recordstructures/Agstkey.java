package com.csc.life.statistics.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:00:11
 * Description:
 * Copybook name: AGSTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Agstkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData agstFileKey = new FixedLengthStringData(64);
  
  	public FixedLengthStringData agstKey = new FixedLengthStringData(64).isAPartOf(agstFileKey, 0, REDEFINE);
  	public FixedLengthStringData agstChdrcoy = new FixedLengthStringData(1).isAPartOf(agstKey, 0);
  	public FixedLengthStringData agstAgntnum = new FixedLengthStringData(8).isAPartOf(agstKey, 1);
  	public FixedLengthStringData agstStatcat = new FixedLengthStringData(2).isAPartOf(agstKey, 9);
  	public PackedDecimalData agstAcctyr = new PackedDecimalData(4, 0).isAPartOf(agstKey, 11);
  	public FixedLengthStringData agstAracde = new FixedLengthStringData(3).isAPartOf(agstKey, 14);
  	public FixedLengthStringData agstCntbranch = new FixedLengthStringData(2).isAPartOf(agstKey, 17);
  	public FixedLengthStringData agstBandage = new FixedLengthStringData(2).isAPartOf(agstKey, 19);
  	public FixedLengthStringData agstBandsa = new FixedLengthStringData(2).isAPartOf(agstKey, 21);
  	public FixedLengthStringData agstBandprm = new FixedLengthStringData(2).isAPartOf(agstKey, 23);
  	public FixedLengthStringData agstBandtrm = new FixedLengthStringData(2).isAPartOf(agstKey, 25);
  	public FixedLengthStringData agstCnttype = new FixedLengthStringData(3).isAPartOf(agstKey, 27);
  	public FixedLengthStringData agstCrtable = new FixedLengthStringData(4).isAPartOf(agstKey, 30);
  	public FixedLengthStringData agstOvrdcat = new FixedLengthStringData(1).isAPartOf(agstKey, 34);
  	public FixedLengthStringData agstPstatcode = new FixedLengthStringData(2).isAPartOf(agstKey, 35);
  	public FixedLengthStringData agstCntcurr = new FixedLengthStringData(3).isAPartOf(agstKey, 37);
  	public FixedLengthStringData filler = new FixedLengthStringData(24).isAPartOf(agstKey, 40, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(agstFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		agstFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}