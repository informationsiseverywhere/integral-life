package com.csc.life.statistics.tablestructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;

/**
 * 	
 * @author: Quipoz Limited
 * @version
 * Creation Date: Sun, 30 Aug 2009 03:16:33
 * Description:
 * Copybook name: T6629REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class T6629rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData t6629Rec = new FixedLengthStringData(500);
  	public FixedLengthStringData agebands = new FixedLengthStringData(8).isAPartOf(t6629Rec, 0);
  	public FixedLengthStringData[] ageband = FLSArrayPartOfStructure(2, 4, agebands, 0);
  	public FixedLengthStringData filler = new FixedLengthStringData(8).isAPartOf(agebands, 0, FILLER_REDEFINE);
  	public FixedLengthStringData ageband01 = new FixedLengthStringData(4).isAPartOf(filler, 0);
  	public FixedLengthStringData ageband02 = new FixedLengthStringData(4).isAPartOf(filler, 4);
  	public FixedLengthStringData agntstat = new FixedLengthStringData(1).isAPartOf(t6629Rec, 8);
  	public FixedLengthStringData govtstat = new FixedLengthStringData(1).isAPartOf(t6629Rec, 9);
  	public FixedLengthStringData prmbands = new FixedLengthStringData(8).isAPartOf(t6629Rec, 10);
  	public FixedLengthStringData[] prmband = FLSArrayPartOfStructure(2, 4, prmbands, 0);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(prmbands, 0, FILLER_REDEFINE);
  	public FixedLengthStringData prmband01 = new FixedLengthStringData(4).isAPartOf(filler1, 0);
  	public FixedLengthStringData prmband02 = new FixedLengthStringData(4).isAPartOf(filler1, 4);
  	public FixedLengthStringData rskbands = new FixedLengthStringData(8).isAPartOf(t6629Rec, 18);
  	public FixedLengthStringData[] rskband = FLSArrayPartOfStructure(2, 4, rskbands, 0);
  	public FixedLengthStringData filler2 = new FixedLengthStringData(8).isAPartOf(rskbands, 0, FILLER_REDEFINE);
  	public FixedLengthStringData rskband01 = new FixedLengthStringData(4).isAPartOf(filler2, 0);
  	public FixedLengthStringData rskband02 = new FixedLengthStringData(4).isAPartOf(filler2, 4);
  	public FixedLengthStringData sumbands = new FixedLengthStringData(8).isAPartOf(t6629Rec, 26);
  	public FixedLengthStringData[] sumband = FLSArrayPartOfStructure(2, 4, sumbands, 0);
  	public FixedLengthStringData filler3 = new FixedLengthStringData(8).isAPartOf(sumbands, 0, FILLER_REDEFINE);
  	public FixedLengthStringData sumband01 = new FixedLengthStringData(4).isAPartOf(filler3, 0);
  	public FixedLengthStringData sumband02 = new FixedLengthStringData(4).isAPartOf(filler3, 4);
  	public FixedLengthStringData filler4 = new FixedLengthStringData(466).isAPartOf(t6629Rec, 34, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(t6629Rec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		t6629Rec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}