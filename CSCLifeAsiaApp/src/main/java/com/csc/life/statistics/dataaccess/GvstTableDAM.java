package com.csc.life.statistics.dataaccess;

import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: GvstTableDAM.java
 * Date: Sun, 30 Aug 2009 03:39:57
 * Class transformed from GVST.LF
 * Author: Quipoz Limited
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class GvstTableDAM extends GvstpfTableDAM {

	public GvstTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("GVST");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "BATCCOY"
		             + ", BATCBRN"
		             + ", BATCACTYR"
		             + ", BATCACTMN"
		             + ", BATCTRCDE"
		             + ", BATCBATCH"
		             + ", ACCTCCY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "REG, " +
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "BATCCOY, " +
		            "BATCBRN, " +
		            "BATCACTYR, " +
		            "BATCACTMN, " +
		            "BATCTRCDE, " +
		            "BATCBATCH, " +
		            "CNTBRANCH, " +
		            "BILLFREQ, " +
		            "ACCTCCY, " +
		            "COMMYR, " +
		            "SRCEBUS, " +
		            "STFUND, " +
		            "STSECT, " +
		            "STSSECT, " +
		            "CNTTYPE, " +
		            "CRTABLE, " +
		            "PARIND, " +
		            "STATCODE, " +
		            "CNPSTAT, " +
		            "CRPSTAT, " +
		            "STACCRB, " +
		            "STACCXB, " +
		            "STACCTB, " +
		            "STACCIB, " +
		            "STACCDD, " +
		            "STACCDI, " +
		            "STACCBS, " +
		            "STACCMTB, " +
		            "STACCOB, " +
		            "STACCSPD, " +
		            "STACCFYP, " +
		            "STACCRNP, " +
		            "STACCFYC, " +
		            "STACCRLC, " +
		            "STACCSPC, " +
		            "STACCRIP, " +
		            "STACCRIC, " +
		            "STACCCLR, " +
		            "STACCADV, " +
		            "STACCPDB, " +
		            "STACCADB, " +
		            "STACCADJ, " +
		            "STACCINT, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "BATCCOY ASC, " +
		            "BATCBRN ASC, " +
		            "BATCACTYR ASC, " +
		            "BATCACTMN ASC, " +
		            "BATCTRCDE ASC, " +
		            "BATCBATCH ASC, " +
		            "ACCTCCY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "BATCCOY DESC, " +
		            "BATCBRN DESC, " +
		            "BATCACTYR DESC, " +
		            "BATCACTMN DESC, " +
		            "BATCTRCDE DESC, " +
		            "BATCBATCH DESC, " +
		            "ACCTCCY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               register,
                               chdrcoy,
                               chdrnum,
                               life,
                               coverage,
                               rider,
                               planSuffix,
                               batccoy,
                               batcbrn,
                               batcactyr,
                               batcactmn,
                               batctrcde,
                               batcbatch,
                               cntbranch,
                               billfreq,
                               acctccy,
                               commyr,
                               srcebus,
                               statFund,
                               statSect,
                               statSubsect,
                               cnttype,
                               crtable,
                               parind,
                               statcode,
                               cnPremStat,
                               covPremStat,
                               staccrb,
                               staccxb,
                               stacctb,
                               staccib,
                               staccdd,
                               staccdi,
                               staccbs,
                               staccmtb,
                               staccob,
                               staccspd,
                               staccfyp,
                               staccrnp,
                               staccfyc,
                               staccrlc,
                               staccspc,
                               staccrip,
                               staccric,
                               staccclr,
                               staccadv,
                               staccpdb,
                               staccadb,
                               staccadj,
                               staccint,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(27);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(64);
		
		keyData.set(getBatccoy().toInternal()
					+ getBatcbrn().toInternal()
					+ getBatcactyr().toInternal()
					+ getBatcactmn().toInternal()
					+ getBatctrcde().toInternal()
					+ getBatcbatch().toInternal()
					+ getAcctccy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, batccoy);
			what = ExternalData.chop(what, batcbrn);
			what = ExternalData.chop(what, batcactyr);
			what = ExternalData.chop(what, batcactmn);
			what = ExternalData.chop(what, batctrcde);
			what = ExternalData.chop(what, batcbatch);
			what = ExternalData.chop(what, acctccy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller40 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller80 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller90 = new FixedLengthStringData(3);
	private FixedLengthStringData nonKeyFiller100 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller110 = new FixedLengthStringData(4);
	private FixedLengthStringData nonKeyFiller120 = new FixedLengthStringData(5);
	private FixedLengthStringData nonKeyFiller150 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller40.setInternal(coverage.toInternal());
	nonKeyFiller50.setInternal(rider.toInternal());
	nonKeyFiller60.setInternal(planSuffix.toInternal());
	nonKeyFiller70.setInternal(batccoy.toInternal());
	nonKeyFiller80.setInternal(batcbrn.toInternal());
	nonKeyFiller90.setInternal(batcactyr.toInternal());
	nonKeyFiller100.setInternal(batcactmn.toInternal());
	nonKeyFiller110.setInternal(batctrcde.toInternal());
	nonKeyFiller120.setInternal(batcbatch.toInternal());
	nonKeyFiller150.setInternal(acctccy.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(347);
		
		nonKeyData.set(
					getChdrcoy().toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ nonKeyFiller40.toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ nonKeyFiller80.toInternal()
					+ nonKeyFiller90.toInternal()
					+ nonKeyFiller100.toInternal()
					+ nonKeyFiller110.toInternal()
					+ nonKeyFiller120.toInternal()
					+ getCntbranch().toInternal()
					+ getBillfreq().toInternal()
					+ nonKeyFiller150.toInternal()
					+ getCommyr().toInternal()
					+ getRegister().toInternal()
					+ getSrcebus().toInternal()
					+ getStatFund().toInternal()
					+ getStatSect().toInternal()
					+ getStatSubsect().toInternal()
					+ getCnttype().toInternal()
					+ getCrtable().toInternal()
					+ getParind().toInternal()
					+ getStatcode().toInternal()
					+ getCnPremStat().toInternal()
					+ getCovPremStat().toInternal()
					+ getStaccrb().toInternal()
					+ getStaccxb().toInternal()
					+ getStacctb().toInternal()
					+ getStaccib().toInternal()
					+ getStaccdd().toInternal()
					+ getStaccdi().toInternal()
					+ getStaccbs().toInternal()
					+ getStaccmtb().toInternal()
					+ getStaccob().toInternal()
					+ getStaccspd().toInternal()
					+ getStaccfyp().toInternal()
					+ getStaccrnp().toInternal()
					+ getStaccfyc().toInternal()
					+ getStaccrlc().toInternal()
					+ getStaccspc().toInternal()
					+ getStaccrip().toInternal()
					+ getStaccric().toInternal()
					+ getStaccclr().toInternal()
					+ getStaccadv().toInternal()
					+ getStaccpdb().toInternal()
					+ getStaccadb().toInternal()
					+ getStaccadj().toInternal()
					+ getStaccint().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, nonKeyFiller40);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, nonKeyFiller80);
			what = ExternalData.chop(what, nonKeyFiller90);
			what = ExternalData.chop(what, nonKeyFiller100);
			what = ExternalData.chop(what, nonKeyFiller110);
			what = ExternalData.chop(what, nonKeyFiller120);
			what = ExternalData.chop(what, cntbranch);
			what = ExternalData.chop(what, billfreq);
			what = ExternalData.chop(what, nonKeyFiller150);
			what = ExternalData.chop(what, commyr);
			what = ExternalData.chop(what, register);
			what = ExternalData.chop(what, srcebus);
			what = ExternalData.chop(what, statFund);
			what = ExternalData.chop(what, statSect);
			what = ExternalData.chop(what, statSubsect);
			what = ExternalData.chop(what, cnttype);
			what = ExternalData.chop(what, crtable);
			what = ExternalData.chop(what, parind);
			what = ExternalData.chop(what, statcode);
			what = ExternalData.chop(what, cnPremStat);
			what = ExternalData.chop(what, covPremStat);
			what = ExternalData.chop(what, staccrb);
			what = ExternalData.chop(what, staccxb);
			what = ExternalData.chop(what, stacctb);
			what = ExternalData.chop(what, staccib);
			what = ExternalData.chop(what, staccdd);
			what = ExternalData.chop(what, staccdi);
			what = ExternalData.chop(what, staccbs);
			what = ExternalData.chop(what, staccmtb);
			what = ExternalData.chop(what, staccob);
			what = ExternalData.chop(what, staccspd);
			what = ExternalData.chop(what, staccfyp);
			what = ExternalData.chop(what, staccrnp);
			what = ExternalData.chop(what, staccfyc);
			what = ExternalData.chop(what, staccrlc);
			what = ExternalData.chop(what, staccspc);
			what = ExternalData.chop(what, staccrip);
			what = ExternalData.chop(what, staccric);
			what = ExternalData.chop(what, staccclr);
			what = ExternalData.chop(what, staccadv);
			what = ExternalData.chop(what, staccpdb);
			what = ExternalData.chop(what, staccadb);
			what = ExternalData.chop(what, staccadj);
			what = ExternalData.chop(what, staccint);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getBatccoy() {
		return batccoy;
	}
	public void setBatccoy(Object what) {
		batccoy.set(what);
	}
	public FixedLengthStringData getBatcbrn() {
		return batcbrn;
	}
	public void setBatcbrn(Object what) {
		batcbrn.set(what);
	}
	public PackedDecimalData getBatcactyr() {
		return batcactyr;
	}
	public void setBatcactyr(Object what) {
		setBatcactyr(what, false);
	}
	public void setBatcactyr(Object what, boolean rounded) {
		if (rounded)
			batcactyr.setRounded(what);
		else
			batcactyr.set(what);
	}
	public PackedDecimalData getBatcactmn() {
		return batcactmn;
	}
	public void setBatcactmn(Object what) {
		setBatcactmn(what, false);
	}
	public void setBatcactmn(Object what, boolean rounded) {
		if (rounded)
			batcactmn.setRounded(what);
		else
			batcactmn.set(what);
	}
	public FixedLengthStringData getBatctrcde() {
		return batctrcde;
	}
	public void setBatctrcde(Object what) {
		batctrcde.set(what);
	}
	public FixedLengthStringData getBatcbatch() {
		return batcbatch;
	}
	public void setBatcbatch(Object what) {
		batcbatch.set(what);
	}
	public FixedLengthStringData getAcctccy() {
		return acctccy;
	}
	public void setAcctccy(Object what) {
		acctccy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}	
	public FixedLengthStringData getCntbranch() {
		return cntbranch;
	}
	public void setCntbranch(Object what) {
		cntbranch.set(what);
	}	
	public FixedLengthStringData getBillfreq() {
		return billfreq;
	}
	public void setBillfreq(Object what) {
		billfreq.set(what);
	}	
	public PackedDecimalData getCommyr() {
		return commyr;
	}
	public void setCommyr(Object what) {
		setCommyr(what, false);
	}
	public void setCommyr(Object what, boolean rounded) {
		if (rounded)
			commyr.setRounded(what);
		else
			commyr.set(what);
	}	
	public FixedLengthStringData getRegister() {
		return register;
	}
	public void setRegister(Object what) {
		register.set(what);
	}	
	public FixedLengthStringData getSrcebus() {
		return srcebus;
	}
	public void setSrcebus(Object what) {
		srcebus.set(what);
	}	
	public FixedLengthStringData getStatFund() {
		return statFund;
	}
	public void setStatFund(Object what) {
		statFund.set(what);
	}	
	public FixedLengthStringData getStatSect() {
		return statSect;
	}
	public void setStatSect(Object what) {
		statSect.set(what);
	}	
	public FixedLengthStringData getStatSubsect() {
		return statSubsect;
	}
	public void setStatSubsect(Object what) {
		statSubsect.set(what);
	}	
	public FixedLengthStringData getCnttype() {
		return cnttype;
	}
	public void setCnttype(Object what) {
		cnttype.set(what);
	}	
	public FixedLengthStringData getCrtable() {
		return crtable;
	}
	public void setCrtable(Object what) {
		crtable.set(what);
	}	
	public FixedLengthStringData getParind() {
		return parind;
	}
	public void setParind(Object what) {
		parind.set(what);
	}	
	public FixedLengthStringData getStatcode() {
		return statcode;
	}
	public void setStatcode(Object what) {
		statcode.set(what);
	}	
	public FixedLengthStringData getCnPremStat() {
		return cnPremStat;
	}
	public void setCnPremStat(Object what) {
		cnPremStat.set(what);
	}	
	public FixedLengthStringData getCovPremStat() {
		return covPremStat;
	}
	public void setCovPremStat(Object what) {
		covPremStat.set(what);
	}	
	public PackedDecimalData getStaccrb() {
		return staccrb;
	}
	public void setStaccrb(Object what) {
		setStaccrb(what, false);
	}
	public void setStaccrb(Object what, boolean rounded) {
		if (rounded)
			staccrb.setRounded(what);
		else
			staccrb.set(what);
	}	
	public PackedDecimalData getStaccxb() {
		return staccxb;
	}
	public void setStaccxb(Object what) {
		setStaccxb(what, false);
	}
	public void setStaccxb(Object what, boolean rounded) {
		if (rounded)
			staccxb.setRounded(what);
		else
			staccxb.set(what);
	}	
	public PackedDecimalData getStacctb() {
		return stacctb;
	}
	public void setStacctb(Object what) {
		setStacctb(what, false);
	}
	public void setStacctb(Object what, boolean rounded) {
		if (rounded)
			stacctb.setRounded(what);
		else
			stacctb.set(what);
	}	
	public PackedDecimalData getStaccib() {
		return staccib;
	}
	public void setStaccib(Object what) {
		setStaccib(what, false);
	}
	public void setStaccib(Object what, boolean rounded) {
		if (rounded)
			staccib.setRounded(what);
		else
			staccib.set(what);
	}	
	public PackedDecimalData getStaccdd() {
		return staccdd;
	}
	public void setStaccdd(Object what) {
		setStaccdd(what, false);
	}
	public void setStaccdd(Object what, boolean rounded) {
		if (rounded)
			staccdd.setRounded(what);
		else
			staccdd.set(what);
	}	
	public PackedDecimalData getStaccdi() {
		return staccdi;
	}
	public void setStaccdi(Object what) {
		setStaccdi(what, false);
	}
	public void setStaccdi(Object what, boolean rounded) {
		if (rounded)
			staccdi.setRounded(what);
		else
			staccdi.set(what);
	}	
	public PackedDecimalData getStaccbs() {
		return staccbs;
	}
	public void setStaccbs(Object what) {
		setStaccbs(what, false);
	}
	public void setStaccbs(Object what, boolean rounded) {
		if (rounded)
			staccbs.setRounded(what);
		else
			staccbs.set(what);
	}	
	public PackedDecimalData getStaccmtb() {
		return staccmtb;
	}
	public void setStaccmtb(Object what) {
		setStaccmtb(what, false);
	}
	public void setStaccmtb(Object what, boolean rounded) {
		if (rounded)
			staccmtb.setRounded(what);
		else
			staccmtb.set(what);
	}	
	public PackedDecimalData getStaccob() {
		return staccob;
	}
	public void setStaccob(Object what) {
		setStaccob(what, false);
	}
	public void setStaccob(Object what, boolean rounded) {
		if (rounded)
			staccob.setRounded(what);
		else
			staccob.set(what);
	}	
	public PackedDecimalData getStaccspd() {
		return staccspd;
	}
	public void setStaccspd(Object what) {
		setStaccspd(what, false);
	}
	public void setStaccspd(Object what, boolean rounded) {
		if (rounded)
			staccspd.setRounded(what);
		else
			staccspd.set(what);
	}	
	public PackedDecimalData getStaccfyp() {
		return staccfyp;
	}
	public void setStaccfyp(Object what) {
		setStaccfyp(what, false);
	}
	public void setStaccfyp(Object what, boolean rounded) {
		if (rounded)
			staccfyp.setRounded(what);
		else
			staccfyp.set(what);
	}	
	public PackedDecimalData getStaccrnp() {
		return staccrnp;
	}
	public void setStaccrnp(Object what) {
		setStaccrnp(what, false);
	}
	public void setStaccrnp(Object what, boolean rounded) {
		if (rounded)
			staccrnp.setRounded(what);
		else
			staccrnp.set(what);
	}	
	public PackedDecimalData getStaccfyc() {
		return staccfyc;
	}
	public void setStaccfyc(Object what) {
		setStaccfyc(what, false);
	}
	public void setStaccfyc(Object what, boolean rounded) {
		if (rounded)
			staccfyc.setRounded(what);
		else
			staccfyc.set(what);
	}	
	public PackedDecimalData getStaccrlc() {
		return staccrlc;
	}
	public void setStaccrlc(Object what) {
		setStaccrlc(what, false);
	}
	public void setStaccrlc(Object what, boolean rounded) {
		if (rounded)
			staccrlc.setRounded(what);
		else
			staccrlc.set(what);
	}	
	public PackedDecimalData getStaccspc() {
		return staccspc;
	}
	public void setStaccspc(Object what) {
		setStaccspc(what, false);
	}
	public void setStaccspc(Object what, boolean rounded) {
		if (rounded)
			staccspc.setRounded(what);
		else
			staccspc.set(what);
	}	
	public PackedDecimalData getStaccrip() {
		return staccrip;
	}
	public void setStaccrip(Object what) {
		setStaccrip(what, false);
	}
	public void setStaccrip(Object what, boolean rounded) {
		if (rounded)
			staccrip.setRounded(what);
		else
			staccrip.set(what);
	}	
	public PackedDecimalData getStaccric() {
		return staccric;
	}
	public void setStaccric(Object what) {
		setStaccric(what, false);
	}
	public void setStaccric(Object what, boolean rounded) {
		if (rounded)
			staccric.setRounded(what);
		else
			staccric.set(what);
	}	
	public PackedDecimalData getStaccclr() {
		return staccclr;
	}
	public void setStaccclr(Object what) {
		setStaccclr(what, false);
	}
	public void setStaccclr(Object what, boolean rounded) {
		if (rounded)
			staccclr.setRounded(what);
		else
			staccclr.set(what);
	}	
	public PackedDecimalData getStaccadv() {
		return staccadv;
	}
	public void setStaccadv(Object what) {
		setStaccadv(what, false);
	}
	public void setStaccadv(Object what, boolean rounded) {
		if (rounded)
			staccadv.setRounded(what);
		else
			staccadv.set(what);
	}	
	public PackedDecimalData getStaccpdb() {
		return staccpdb;
	}
	public void setStaccpdb(Object what) {
		setStaccpdb(what, false);
	}
	public void setStaccpdb(Object what, boolean rounded) {
		if (rounded)
			staccpdb.setRounded(what);
		else
			staccpdb.set(what);
	}	
	public PackedDecimalData getStaccadb() {
		return staccadb;
	}
	public void setStaccadb(Object what) {
		setStaccadb(what, false);
	}
	public void setStaccadb(Object what, boolean rounded) {
		if (rounded)
			staccadb.setRounded(what);
		else
			staccadb.set(what);
	}	
	public PackedDecimalData getStaccadj() {
		return staccadj;
	}
	public void setStaccadj(Object what) {
		setStaccadj(what, false);
	}
	public void setStaccadj(Object what, boolean rounded) {
		if (rounded)
			staccadj.setRounded(what);
		else
			staccadj.set(what);
	}	
	public PackedDecimalData getStaccint() {
		return staccint;
	}
	public void setStaccint(Object what) {
		setStaccint(what, false);
	}
	public void setStaccint(Object what, boolean rounded) {
		if (rounded)
			staccint.setRounded(what);
		else
			staccint.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		batccoy.clear();
		batcbrn.clear();
		batcactyr.clear();
		batcactmn.clear();
		batctrcde.clear();
		batcbatch.clear();
		acctccy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		chdrcoy.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		nonKeyFiller40.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		nonKeyFiller80.clear();
		nonKeyFiller90.clear();
		nonKeyFiller100.clear();
		nonKeyFiller110.clear();
		nonKeyFiller120.clear();
		cntbranch.clear();
		billfreq.clear();
		nonKeyFiller150.clear();
		commyr.clear();
		register.clear();
		srcebus.clear();
		statFund.clear();
		statSect.clear();
		statSubsect.clear();
		cnttype.clear();
		crtable.clear();
		parind.clear();
		statcode.clear();
		cnPremStat.clear();
		covPremStat.clear();
		staccrb.clear();
		staccxb.clear();
		stacctb.clear();
		staccib.clear();
		staccdd.clear();
		staccdi.clear();
		staccbs.clear();
		staccmtb.clear();
		staccob.clear();
		staccspd.clear();
		staccfyp.clear();
		staccrnp.clear();
		staccfyc.clear();
		staccrlc.clear();
		staccspc.clear();
		staccrip.clear();
		staccric.clear();
		staccclr.clear();
		staccadv.clear();
		staccpdb.clear();
		staccadb.clear();
		staccadj.clear();
		staccint.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}