package com.csc.life.workflow.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.fsu.workflow.dataaccess.dao.UserrolesDAO;
import com.csc.fsu.workflow.utility.IntegralFsuWorkflowConfig;
import com.csc.ldap.LdapDao;
import com.csc.life.workflow.screens.Sa657ScreenVars;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.utils.UserMapingUtils;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Errmesg;
import com.csc.workflow.WorkflowClient;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * The Pa657 for Life New Business Retail Policy.
 * 
 * @author arichharia2
 *
 */
public class Pa657 extends ScreenProgCS {
	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final Logger LOGGER = LoggerFactory.getLogger(Pa657.class);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pa657");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Sa657ScreenVars sv = ScreenProgram.getScreenVars(Sa657ScreenVars.class);;
	private FixedLengthStringData wsaaPassword = new FixedLengthStringData(6);
	private Wsspsmart wsspsmart = new Wsspsmart();

	private FixedLengthStringData filler = new FixedLengthStringData(767).isAPartOf(wsspsmart.userArea, 0,
			FILLER_REDEFINE);
	
	private FixedLengthStringData wsspTaskid = new FixedLengthStringData(200).isAPartOf(filler, 0);
	private FixedLengthStringData wsspcontainerid = new FixedLengthStringData(200).isAPartOf(filler, 200);
	private FixedLengthStringData processandtaskidentifier = new FixedLengthStringData(150).isAPartOf(filler, 400);
	private FixedLengthStringData wsspProcessInstanceid = new FixedLengthStringData(10).isAPartOf(filler, 550);
	private FixedLengthStringData wsspTaskName = new FixedLengthStringData(100).isAPartOf(filler, 560);
	
	private WorkflowClient workflowClient = getApplicationContext().getBean("workflowClient", WorkflowClient.class);
	protected UserrolesDAO userrolesDAO = getApplicationContext().getBean("userrolesDAO", UserrolesDAO.class);
	protected String authString;
	private LdapDao ldap = new LdapDao(); 
	private String userId;
	private String pass="";
	private Errmesgrec errmesgrec = new Errmesgrec();

	public Pa657() {
		super();
		screenVars = sv;
		new ScreenModel("Sa657", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		super.mainline();

	}

	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		processBoMainline(sv, sv.dataArea, parmArray);

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.smart400framework.parent.ScreenProgram#initialise1000() Method
	 * call during screen is load. Its responsible to initialise the screen
	 * variables.
	 * 
	 */
	@Override
	protected void initialise1000() {
		/* INITIALISE */

		sv.dataArea.set(SPACES);
		String userId = UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString()).toLowerCase().trim();

		String pass = ldap.getLdapUserPwd(userId, appVars);

		authString = userId + ":" + pass;

		String authStringEnc = "\"Basic "
				+ new String(java.util.Base64.getEncoder().encode(authString.getBytes(StandardCharsets.UTF_8)),
						StandardCharsets.UTF_8)
				+ "\"";

		sv.authStringForJBPM.set(authStringEnc);
		sv.effdateDisp.set(SPACES);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

		sanctnrec.function.set("USRNO");
		sanctnrec.userid.set(wsspcomn.userid);
		sv.jbpmContainerId.set(wsspcontainerid);
		sv.jbpmprocessinstanceID.set(wsspProcessInstanceid);

		wsaaPassword.set(sanctnrec.password.getData());
		sv.screenLabel.set(wsspTaskName.toString().trim());

	}

	@Override
	protected void preScreenEdit() {
		// empty method required for framework

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.smart400framework.parent.ScreenProgram#screenEdit2000()
	 * 
	 * This method is used for checking validation.
	 */
	@Override
	protected void screenEdit2000() {
		//IBPTE-1239 starts
		if(isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
			return;
		}
		//IBPTE-1239 ends
		sanctnrec.userid.set(wsspcomn.userid);
		sv.jbpmUWdecision.getFormData();
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isNE(sanctnrec.statuz, Varcom.oK)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.subrname.set("SANCTN");
			syserrrec.statuz.set(Varcom.bomb);
			fatalError600();
		}
		wsaaPassword.set(sanctnrec.password.getData());
		/* If F9 pressed then set a srceen error so the screen */
		/* is re-displayed. */
		if (isEQ(scrnparams.statuz, Varcom.calc)) {
			wsspcomn.edterror.set("Y");
		}
	}
	
	protected void getUserPass() {
		if (IntegralFsuWorkflowConfig.isDBWFUserIntegrationEnabled()) {
			if (!pass.equalsIgnoreCase(userId)) {
				pass = userrolesDAO.checkCreateUserId(userId, userId, wsspcomn.company.toString());
			}
		} else {
			pass = ldap.getLdapUserPwd(userId, appVars);

		}

		authString = userId + ":" + pass;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.smart400framework.parent.ScreenProgram#update3000() Update
	 * section of Program.
	 */
	@Override
	protected void update3000() {
		try {
			wsaaPassword.set(sanctnrec.password.getData());
			HashMap<String, String> inputMap = new HashMap<>();
			if (wsspTaskName.toString().trim().contains("Check discrepancy")) {
				inputMap.put("discrepancyStatus", sv.jbpmUWdecision.getFormData().trim());
			
				workflowClient.completeHumanTask(
						UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString()).toLowerCase().trim(),
						wsspTaskid.toString().trim(), inputMap, wsspcontainerid.trim(),authString);
			} else if (wsspTaskName.toString().trim().contains("Payment dishonour")) {
				inputMap.put("paymentDishonour", sv.jbpmUWdecision.getFormData().trim());

				workflowClient.completeHumanTask(
						UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString()).toLowerCase().trim(),
						wsspTaskid.toString().trim(), inputMap, wsspcontainerid.trim(),authString);
			}

		} catch (Exception e) {
			/* IBPTE-1239 */
			LOGGER.error(getMessage("RUQ8", "Exception Occurred in completeHumanTask "), e);
			scrnparams.errorCode.set("RUQ8");
			wsspcomn.edterror.set("Y");
			/* IBPTE-1239 */
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.smart400framework.parent.ScreenProgram#whereNext4000() Method
	 * set next Program.
	 */
	@Override
	protected void whereNext4000() {
		/* NEXT-PROGRAM */
		wsspcomn.nextprog.set("Pr2fw");
		/* EXIT */
	}
	/* IBPTE-1239 START */
	private String getMessage(String errorCode, String errorMessage) {
		errmesgrec.eror.set(errorCode);
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaScreen);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.clear();
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		if (isEQ(errmesgrec.statuz, "BOMB")) {
			scrnparams.statuz.set(errmesgrec.statuz);
			screenErrors200();
		}
		return errorMessage + ". " + errmesgrec.errorline.trim();
	}
	/* IBPTE-1239 END */
}
