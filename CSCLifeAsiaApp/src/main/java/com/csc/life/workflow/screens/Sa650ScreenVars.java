package com.csc.life.workflow.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datadictionarydatatype.FLSDDObj;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sa650
 * 
 * @version 1.0 generated on 30/08/09 06:52
 * @author igarg2
 */
public class Sa650ScreenVars extends SmartVarModel {

	private static final long serialVersionUID = 1;
	public FixedLengthStringData dataArea = new FixedLengthStringData(1000);
	public FixedLengthStringData dataFields = new FixedLengthStringData(728).isAPartOf(dataArea, 0);

	public FixedLengthStringData contractNum = DD.chdrnum.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData contractType = new FLSDDObj(3).copy().isAPartOf(dataFields, 8);
	public FixedLengthStringData agentNum = DD.agentno.copy().isAPartOf(dataFields, 11);
	public FixedLengthStringData agentName = DD.agentname.copy().isAPartOf(dataFields, 19);
	public FixedLengthStringData contractOwnerNum = DD.cownnum.copy().isAPartOf(dataFields, 66);
	public FixedLengthStringData contractOwnerName = DD.ownername.copy().isAPartOf(dataFields, 74);
	public FixedLengthStringData lifeAssuredNum = DD.lifenum.copy().isAPartOf(dataFields, 121);
	public FixedLengthStringData lifeAssuredName = DD.lifename.copy().isAPartOf(dataFields, 129);
	public FixedLengthStringData followUpsInPolicy = DD.followupsInPolicy.copy().isAPartOf(dataFields, 176);
	public FixedLengthStringData jbpmtaskId = DD.jbpmtaskId.copy().isAPartOf(dataFields, 276);
	public FixedLengthStringData jbpmContainerId = DD.jbpmContainerId.copy().isAPartOf(dataFields, 286);
	public FixedLengthStringData jbpmprocessinstanceID = DD.jbpmprocessinstanceID.copy().isAPartOf(dataFields, 486);
	public FixedLengthStringData authStringForJBPM = DD.authStringForJBPM.copy().isAPartOf(dataFields, 496);
	public ZonedDecimalData lifeAssuredAge = DD.anbage.copyToZonedDecimal().isAPartOf(dataFields, 696);
	public ZonedDecimalData lifeAssuredBMI = DD.bmi.copyToZonedDecimal().isAPartOf(dataFields, 699);
	public ZonedDecimalData tsar = DD.tsar.copyToZonedDecimal().isAPartOf(dataFields, 703);
	public ZonedDecimalData cmncDate = DD.cmncDate.copyToZonedDecimal().isAPartOf(dataFields,720);
	
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 728);

	public FixedLengthStringData contractNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData contractTypeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData agentNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData agentNameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData contractOwnerNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData contractOwnerNameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData lifeAssuredNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifeAssuredNameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData followUpsInPolicyErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData jbpmtaskIdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData jbpmContainerIdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData jbpmprocessinstanceIDErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44);
	public FixedLengthStringData authStringForJBPMErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData lifeAssuredAgeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData lifeAssuredBMIErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData tsarErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData cmncDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 796);

	public FixedLengthStringData[] contractNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] contractTypeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] agentNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] agentNameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] contractOwnerNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] contractOwnerNameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] lifeAssuredNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifeAssuredNameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] followUpsInPolicyOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] jbpmtaskIdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] jbpmContainerIdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] jbpmprocessinstanceIDOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] authStringForJBPMOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] lifeAssuredAgeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] lifeAssuredBMIOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] tsarOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] cmncDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public LongData Sa650screenWritten = new LongData(0);
	public LongData Sa650protectWritten = new LongData(0);
	public FixedLengthStringData cmncDateDisp = new FixedLengthStringData(10);

	
	public Sa650ScreenVars() {
		super();
		init();
		
	}
	
	/**
	 * This method calls initialiseScreenVars.
	 */
	private void init(){
		initialiseScreenVars();
	}

	@Override
	protected void initialiseScreenVars() {

		screenFields = new BaseData[] { contractNum, contractType, agentNum, agentName, contractOwnerNum,
				contractOwnerName, lifeAssuredNum, lifeAssuredName, followUpsInPolicy, jbpmtaskId, jbpmContainerId,
				jbpmprocessinstanceID, authStringForJBPM,cmncDate };
		screenOutFields = new BaseData[][] { contractNumOut, contractTypeOut, agentNumOut, agentNameOut,
				contractOwnerNumOut, contractOwnerNameOut, lifeAssuredNumOut, lifeAssuredNameOut, followUpsInPolicyOut,
				jbpmtaskIdOut, jbpmContainerIdOut, jbpmprocessinstanceIDOut, authStringForJBPMOut, cmncDateOut };
		screenErrFields = new BaseData[] { contractNumErr, contractTypeErr, agentNumErr, agentNameErr,
				contractOwnerNumErr, contractOwnerNameErr, lifeAssuredNumErr, lifeAssuredNameErr, followUpsInPolicyErr,
				jbpmtaskIdErr, jbpmContainerIdErr, jbpmprocessinstanceIDErr, authStringForJBPMErr,cmncDateErr };

		fieldIndMap.put(contractNumOut,
				new String[] { "01", null, "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(contractTypeOut,
				new String[] { "02", null, "-02", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(agentNumOut,
				new String[] { "03", null, "-03", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(agentNameOut,
				new String[] { "04", null, "-04", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(contractOwnerNumOut,
				new String[] { "05", null, "-05", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(contractOwnerNameOut,
				new String[] { "06", null, "-06", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(lifeAssuredNumOut,
				new String[] { "07", null, "-07", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(lifeAssuredNameOut,
				new String[] { "08", null, "-08", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(followUpsInPolicyOut,
				new String[] { "09", null, "-09", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(jbpmtaskIdOut,
				new String[] { "10", null, "-10", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(jbpmContainerIdOut,
				new String[] { "11", null, "-11", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(jbpmprocessinstanceIDOut,
				new String[] { "12", null, "-12", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(authStringForJBPMOut,
				new String[] { "13", null, "-13", null, null, null, null, null, null, null, null, null });
		screenDateFields = new BaseData[] {cmncDate};
		screenDateErrFields = new BaseData[] {cmncDateErr};
		screenDateDispFields = new BaseData[] {cmncDateDisp};
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa650screen.class;
		protectRecord = Sa650protect.class;
	}

}
