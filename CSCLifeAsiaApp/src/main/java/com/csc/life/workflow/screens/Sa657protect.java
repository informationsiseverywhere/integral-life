package com.csc.life.workflow.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * The Sa657protect.
 * 
 * @version 1.0 generated on 18/09/19 06:52
 * @author arichharia2
 */
public class Sa657protect extends ScreenRecord {
	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] { 21, 4, 17, 22, 18, 23, 24, 15, 16, 1, 2, 3 };
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] { -1, -1, -1, -1 });
	}

	/**
	 * Writes a record to the screen.
	 * 
	 * @param errorInd
	 *            - will be set on if an error occurs
	 * @param noRecordFoundInd
	 *            - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3) {
		Sa657ScreenVars sv = (Sa657ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa657protectWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
	}

	public static void clearClassString(VarModel pv) {
		Sa657ScreenVars screenVars = (Sa657ScreenVars) pv;
	}

	/**
	 * Clear all the variables in S6633protect
	 */
	public static void clear(VarModel pv) {
		Sa657ScreenVars screenVars = (Sa657ScreenVars) pv;
	}

}
