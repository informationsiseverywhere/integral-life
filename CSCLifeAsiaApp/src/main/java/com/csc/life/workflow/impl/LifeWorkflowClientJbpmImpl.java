package com.csc.life.workflow.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.exceptions.WorkflowServiceException;
import com.csc.life.workflow.LifeWorkflowClient;
import com.csc.life.workflow.config.IntegralLifeWorkflowProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quipoz.framework.util.IntegralWorkflowProperties;

/**
 * Client to invoke Workflow Rest Service.
 * 
 * @author vhukumagrawa
 *
 */
public class LifeWorkflowClientJbpmImpl implements LifeWorkflowClient {

	/** The LOGGER for JBPM workflow client. */
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeWorkflowClientJbpmImpl.class);
	private static final String  CONTENTTYPE = "application/json";
	private static final String COLON_SEPARATOR = "\":\"";
	private static final String HTTP_FAILED = "Failed : HTTP error code : ";
	private static final String ACCEPTED_PROPERTY = "Accept";

	// IJTI-1196 starts

	public static final String POLICY_NUMBER = "policyNumber";
	public static final String LASSRDNME = "lifeAssuredName";
	public static final String CONTRACT_TYPE = "contractType";
	public static final String FOLLOW_UPS = "totalFollowUps_UW";
	public static final String AGENT_NAME = "agentName";
	public static final String AGENT_NUMBER = "agentNumber";
	public static final String CONTRACT_OWNER_NUM = "contractOwnerNumber";
	public static final String CONTRACT_OWNER_NAME = "contractOwnerName";
	public static final String LASSRDNUM = "lifeAssuredNumber";
	private static final String FATCA = "fatcaDetails_UW";
	private static final String FATCAJSON = "com.dxc.integral.workflow.beans.GetClientFATCADetailOutput";
	private static final String AML = "amlDetails_UW";
	private static final String AMLCHECK = "com.dxc.integral.workflow.beans.ClientAMLCheck";
	public static final String LASSRDAGE = "age";
	public static final String LASSRDBMI = "bmi";
	private static final String CEDET = "com.dxc.integral.workflow.beans.ContractExtraDetails";
	public static final String LASSRDINC = "lifeAssuredIncome_UW";
	public static final String TSAR = "tsar_UW";
	public static final String ISUSTAXPAYOR = "usTaxPayer";
	public static final String OCCPCLASS = "occupationClass";
	public static final String UWDECISIONDATE = "uwDecisionDate";
	public static final String UWDECISION = "uwDecision";
	public static final String ADDITIONALFOLLOWUPSCODE = "additionalFollowups";

	public static final String RISKINDICATOR = "riskIndicator";
	private static final String CONTAINER_NAME = "containers/";
	private static final String OCCDET = "occupationDetails_UW";
	private static final String AUTHORIZATION_PROPERTY = "Authorization";
	public static final String POLICYCMDATE = "policyCommDate";

	// IJTI-1196 ends

	private static final String BASIC_PROPERTY = "Basic ";

	private static final String CONTENT_PROPERTY = "Content-Type";

	private static final String TASK_NAME = "/tasks/";

	private static final String TASKS_COMPLETION_QUERY = "/states/completed?auto-progress=true&user=";

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.csc.life.workflow.LifeWorkflowClient#startNewBusinessWorkflow(java.
	 * lang.String)
	 */
	@Override
	public void startNewBusinessWorkflow(String policyNumber, String userName, String password)
			throws WorkflowServiceException {

		try {
			String url = IntegralLifeWorkflowProperties.getWorkflowServiceRuntimeURL()
					+ IntegralLifeWorkflowProperties.getNBWorkflowDeploymentId() + "/processes/"
					+ IntegralLifeWorkflowProperties.getNBProcessDefinition() + "/instances";
			URL startWorkflowService = new URL(url);

			HttpURLConnection httpConnection = (HttpURLConnection) startWorkflowService.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			String authString = IntegralLifeWorkflowProperties.getWorkflowUser() + ":"     
					+ IntegralLifeWorkflowProperties.getWorkflowPassword();
			String authStringEnc = new String(Base64.getEncoder().encode(authString.getBytes()));
			httpConnection.setRequestProperty(AUTHORIZATION_PROPERTY, BASIC_PROPERTY + authStringEnc);
			httpConnection.setRequestProperty(CONTENT_PROPERTY, CONTENTTYPE);
			httpConnection.setRequestProperty(ACCEPTED_PROPERTY, CONTENTTYPE);
			LOGGER.info("Started New Business workflow for policy number: {}", policyNumber);
			OutputStreamWriter dos = new OutputStreamWriter(httpConnection.getOutputStream());
			dos.write("{\"" + IntegralLifeWorkflowProperties.getNBProcessFormProperty() + COLON_SEPARATOR + policyNumber
					+ "\",");
			dos.write("\"" + IntegralLifeWorkflowProperties.getNBuserName() + COLON_SEPARATOR + userName + "\",");
			dos.write("\"" + IntegralLifeWorkflowProperties.getNBpassword() + COLON_SEPARATOR + password + "\" }");

			dos.flush();
		
			dos.close();
			if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK
					&& httpConnection.getResponseCode() != 201) {
				throw new WorkflowServiceException(HTTP_FAILED + httpConnection.getResponseCode());
			}
			try (BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));) {

				String output;
				LOGGER.info("Reading Web Service Response.");
				while ((output = br.readLine()) != null) {
					LOGGER.info(output);
				}

				httpConnection.disconnect();

				LOGGER.info("New Business workflow started successfully for policy number: {}", policyNumber);
			}
		} catch (IOException ex) {
			LOGGER.error("Exception Occurred during New Business Workflow launch" , ex);
			throw new WorkflowServiceException("Exception Occurred during New Business Workflow launch");
		}
	}
	/*
	 * (non-Javadoc)
	 * @see com.csc.life.workflow.LifeWorkflowClient#startUWWorkflow(java.lang.String, java.lang.String)
	 */
	@Override
	public void startUWWorkflow(String policyNumber, String userName, String password) throws WorkflowServiceException {
		try {
			String url = IntegralLifeWorkflowProperties.getWorkflowServiceRuntimeURL()
					+ IntegralLifeWorkflowProperties.getUWWorkflowDeploymentId() + "/processes/"
					+ IntegralLifeWorkflowProperties.getUWProcessDefinition() + "/instances";


			URL startWorkflowService = new URL(url);

			HttpURLConnection httpConnection = (HttpURLConnection) startWorkflowService.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			String authString = IntegralLifeWorkflowProperties.getWorkflowUser() + ":"
					+ IntegralLifeWorkflowProperties.getWorkflowPassword();
			String authStringEnc = new String(Base64.getEncoder().encode(authString.getBytes()));
			httpConnection.setRequestProperty(AUTHORIZATION_PROPERTY, BASIC_PROPERTY + authStringEnc);
			httpConnection.setRequestProperty(CONTENT_PROPERTY, CONTENTTYPE);
			httpConnection.setRequestProperty(ACCEPTED_PROPERTY, CONTENTTYPE);
			LOGGER.info("Started LifeCILUWProcess for policy number:  {}", policyNumber);
			OutputStreamWriter dos = new OutputStreamWriter(httpConnection.getOutputStream());

			/* IJTI-1196 starts */

			dos.write("{\"" + IntegralLifeWorkflowProperties.getNBProcessFormProperty() + COLON_SEPARATOR + policyNumber
					+ "\",");
			dos.write("\"" + IntegralLifeWorkflowProperties.getUWuserName() + COLON_SEPARATOR + userName + "\",");
			dos.write("\"" + IntegralLifeWorkflowProperties.getUWpassword() + COLON_SEPARATOR + password + "\" }");
			/* IJTI-1196 Ends */
			dos.flush();
			dos.close();
			if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK
					&& httpConnection.getResponseCode() != 201) {
				throw new WorkflowServiceException(HTTP_FAILED + httpConnection.getResponseCode());
			}
			try (BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));) {

				String output;
				while ((output = br.readLine()) != null) {
					LOGGER.info(output);
				}
			httpConnection.disconnect();
			LOGGER.info("Life CIL UWProcess started successfully for policy number: {}" , policyNumber);
			}
		} catch (IOException ex) {
			LOGGER.error("Exception Occurred during Life CILUW Process Workflow launch", ex);
			throw new WorkflowServiceException("Exception Occurred during Life CILUW Process Workflow launch");
		}

	}
	/* IJTI-1196 starts */

	/*
	 * com.csc.workflow.WorkflowClient#getTaskFormValues(java.lang.String,
	 * java.lang.String, java.lang.String) method for getting values from form
	 */
	public Map<String, String> getTaskFormValues(String userId, String processid, String containerid, String screenName,
			String authString) throws WorkflowServiceException {     // IJTI-1531
		Map<String, String> formoutput = new HashMap<>();
		try {
			String url = IntegralWorkflowProperties.getWorkflowServerURL() + CONTAINER_NAME + containerid
					+ "/processes/instances/" + processid + "/variables";

			URL startWorkflowService = new URL(url);
			StringBuilder output = new StringBuilder();

			HttpURLConnection httpConnection = (HttpURLConnection) startWorkflowService.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("GET");
			String authStringEnc = new String(Base64.getEncoder().encode(authString.getBytes()));
			httpConnection.setRequestProperty(AUTHORIZATION_PROPERTY, BASIC_PROPERTY + authStringEnc);
			httpConnection.setRequestProperty(CONTENT_PROPERTY, CONTENTTYPE);
			httpConnection.setRequestProperty(ACCEPTED_PROPERTY, CONTENTTYPE);
			httpConnection.connect();
			LOGGER.info("Started getTaskFormValues For User:  {}", processid);

			if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK
					&& httpConnection.getResponseCode() != 201) {
				throw new WorkflowServiceException(HTTP_FAILED + httpConnection.getResponseCode());
			}
			try (BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));) {
				String outputresult = "";
				while ((outputresult = br.readLine()) != null) {
					output.append(outputresult);
				}
				formoutput = parseFormOutput(output, screenName);

				httpConnection.disconnect();
				LOGGER.info("getTaskFormValues ended for : {}", processid);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception Occurred getTaskFormValues", ex);
			throw new WorkflowServiceException("Exception Occurred during getTaskFormValues ");
		}

		return formoutput;
	}

	/**
	 * @param output
	 * @return
	 * 
	 */

	private Map<String, String> parseFormOutput(StringBuilder output, String screenName) {
		JSONObject responserequest = new JSONObject(output.toString());
		Map<String, String> outputDataMap = new HashMap<>();
		if (screenName.equalsIgnoreCase("Sa650"))

		{
			outputDataMap = processSa650Screen(responserequest);

		}
		if ("Sa651".equalsIgnoreCase(screenName)) {
			outputDataMap.put(POLICY_NUMBER, responserequest.getString(POLICY_NUMBER));
			String lAssuredName = "";
			if (responserequest.has(LASSRDNME)) {
				lAssuredName = responserequest.getString(LASSRDNME);
			}

			outputDataMap.put(LASSRDNME, lAssuredName);
			String lAssuredNum = "";
			if (responserequest.has(LASSRDNUM)) {
				lAssuredNum = responserequest.getString(LASSRDNUM);
			}

			String lifeIncome = "";

			if (responserequest.has(LASSRDINC)) {
				lifeIncome = String.valueOf(responserequest.get(LASSRDINC));

			}

			outputDataMap.put(LASSRDNUM, lAssuredNum);
			outputDataMap.put(TSAR, String.valueOf(responserequest.get(TSAR)));
			String isUSTaxPayor_ = responserequest.getJSONObject(FATCA).getJSONObject(FATCAJSON)
					.getString(ISUSTAXPAYOR);
			outputDataMap.put(ISUSTAXPAYOR, isUSTaxPayor_);
			outputDataMap.put(LASSRDINC, lifeIncome);
			outputDataMap.put(OCCPCLASS,
					responserequest.getJSONObject(OCCDET).getJSONObject(CEDET).getString(OCCPCLASS));
			String age = responserequest.getJSONObject(OCCDET).getJSONObject(CEDET).getString(LASSRDAGE);
			outputDataMap.put(LASSRDAGE, age);
			String riskIndc = responserequest.getJSONObject(AML).getJSONObject(AMLCHECK).getString(RISKINDICATOR);
			outputDataMap.put(RISKINDICATOR, riskIndc);
			String bmi = responserequest.getJSONObject(OCCDET).getJSONObject(CEDET).getString(LASSRDBMI);
			outputDataMap.put(LASSRDBMI, bmi);
			String uwDecision = "";
			if (responserequest.has(UWDECISION)) {
				uwDecision = responserequest.getString(UWDECISION);
			}

			outputDataMap.put(UWDECISION, uwDecision);
			String uwDecisionDate = "";
			if (responserequest.has(UWDECISIONDATE)) {
				uwDecisionDate = responserequest.getString(UWDECISIONDATE);
			}

			outputDataMap.put(UWDECISIONDATE, uwDecisionDate);
			String policyCmncdate = "";
			if (responserequest.has(POLICYCMDATE)) {
				policyCmncdate = responserequest.getString(POLICYCMDATE);
			}

			outputDataMap.put(POLICYCMDATE, policyCmncdate);
			outputDataMap.put(LASSRDINC, lifeIncome);
		}

		return outputDataMap;
	}

	private Map<String, String> processSa650Screen(JSONObject responserequest) {
		Map<String, String> outputDataMap = new HashMap<>();
		outputDataMap.put(POLICY_NUMBER, responserequest.getString(POLICY_NUMBER));
		outputDataMap.put(LASSRDNME, responserequest.getString(LASSRDNME));
		outputDataMap.put(CONTRACT_TYPE, responserequest.getString(CONTRACT_TYPE));
		outputDataMap.put(FOLLOW_UPS, responserequest.getString(FOLLOW_UPS));
		outputDataMap.put(AGENT_NAME, responserequest.getString(AGENT_NAME));
		outputDataMap.put(AGENT_NUMBER, responserequest.getString(AGENT_NUMBER));
		outputDataMap.put(CONTRACT_OWNER_NUM, responserequest.getString(CONTRACT_OWNER_NUM));
		outputDataMap.put(LASSRDNUM, responserequest.getString(LASSRDNUM));
		outputDataMap.put(TSAR, String.valueOf(responserequest.get(TSAR)));
		outputDataMap.put(CONTRACT_OWNER_NAME, responserequest.getString(CONTRACT_OWNER_NAME));
		String age = responserequest.getJSONObject(OCCDET).getJSONObject(CEDET).getString(LASSRDAGE);
		outputDataMap.put(LASSRDAGE, age);
		String bmi = responserequest.getJSONObject(OCCDET).getJSONObject(CEDET).getString(LASSRDBMI);
		outputDataMap.put(LASSRDBMI, bmi);
		String policyCmncdate = "";
		if (responserequest.has(POLICYCMDATE)) {
			policyCmncdate = responserequest.getString(POLICYCMDATE);
		}

		outputDataMap.put(POLICYCMDATE, policyCmncdate);
		return outputDataMap;
	}

	@Override
	public void completeHumanTask(String userId, String taskid, HashMap<String, Object> valueMap, String containerid,
			String authString) throws WorkflowServiceException {      // IJTI-1531
		try {
			String url = IntegralWorkflowProperties.getWorkflowServerURL() + CONTAINER_NAME + containerid + TASK_NAME
					+ taskid + TASKS_COMPLETION_QUERY + userId;

			URL startWorkflowService = new URL(url);

			HttpURLConnection httpConnection = (HttpURLConnection) startWorkflowService.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("PUT");
			String authStringEnc = new String(Base64.getEncoder().encode(authString.getBytes()));
			httpConnection.setRequestProperty(AUTHORIZATION_PROPERTY, BASIC_PROPERTY + authStringEnc);
			httpConnection.setRequestProperty(CONTENT_PROPERTY, CONTENTTYPE);
			httpConnection.setRequestProperty(ACCEPTED_PROPERTY, CONTENTTYPE);
			LOGGER.info("Started completeHumanTask for taskid:  {}", taskid);
			OutputStreamWriter dos = new OutputStreamWriter(httpConnection.getOutputStream());
			ObjectMapper mapperObj = new ObjectMapper();
			String jsonResp = mapperObj.writeValueAsString(valueMap);
			dos.write(jsonResp);
			dos.flush();
			dos.close();
			if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK
					&& httpConnection.getResponseCode() != 201) {
				throw new WorkflowServiceException(HTTP_FAILED + httpConnection.getResponseCode());
			}
			LOGGER.info("completeHumanTask started successfully for taskid: {}", taskid);
			try (BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));) {

				String output;
				while ((output = br.readLine()) != null) {
					LOGGER.info(output);
				}
				httpConnection.disconnect();
				LOGGER.info("completeHumanTask ended successfully for taskid: {}", taskid);
			}
		} catch (IOException ex) {
			LOGGER.error("Exception Occurred completeHumanTask", ex);
			throw new WorkflowServiceException("Exception completeHumanTask");
		}

	}
	/* IJTI-1196 ends */
	/*
	 * This method is used to initiate MetLife Japan workflow
	 * (non-Javadoc)
	 * 
	 * @see
	 * 
	 * lang.String)
	 */
	@Override
	public void startNBWorkflow(String referenceNumber) throws WorkflowServiceException {

		try {
			String url = IntegralLifeWorkflowProperties.getWorkflowServiceRuntimeURL()
					+ IntegralLifeWorkflowProperties.getMetLifeNbWorkflowDeploymentId() + "/processes/"
					+ IntegralLifeWorkflowProperties.getNBWorkflowProcessDefinition() + "/instances";
			URL startWorkflowService = new URL(url);
             LOGGER.info("startWorkflowService"+url);
			HttpURLConnection httpConnection = (HttpURLConnection) startWorkflowService.openConnection();
			httpConnection.setDoOutput(true);
			httpConnection.setRequestMethod("POST");
			String authString = IntegralLifeWorkflowProperties.getWorkflowUser() + ":"
					+ IntegralLifeWorkflowProperties.getWorkflowPassword();
			 LOGGER.info("started Met Life "+authString);
			String authStringEnc = new String(Base64.getEncoder().encode(authString.getBytes()));
			httpConnection.setRequestProperty("Authorization", "Basic " + authStringEnc);
			httpConnection.setRequestProperty("Content-Type", CONTENTTYPE);
			httpConnection.setRequestProperty("Accept", CONTENTTYPE);
			LOGGER.info("Started Metlife NB workflow for  referenceNumber: {}",referenceNumber);
			OutputStreamWriter  dos=new OutputStreamWriter (httpConnection.getOutputStream());
			dos.write("{ 	\""+IntegralLifeWorkflowProperties.getReferenceNumber()+"\":\""+referenceNumber+ "\" }");
			dos.flush();
			LOGGER.info(authString);
			dos.close();
			if (httpConnection.getResponseCode() != HttpURLConnection.HTTP_OK && httpConnection.getResponseCode()!=201 ) {
				throw new WorkflowServiceException("Failed : HTTP error code : " + httpConnection.getResponseCode());
			}
			try (BufferedReader br = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));) {

				String output;
				LOGGER.info("Reading Web Service Response.");
				while ((output = br.readLine()) != null) {
					LOGGER.info(output);
				}

				httpConnection.disconnect();

				LOGGER.info(" Metlife NB workflow started successfully for policy number: {}",referenceNumber);
			}
		} catch (IOException ex) {
			LOGGER.error("Exception Occurred during  Metlife NB workflow launch" , ex);
			throw new WorkflowServiceException("Exception Occurred during Metlife NB workflow launch");
		}
	}

}
