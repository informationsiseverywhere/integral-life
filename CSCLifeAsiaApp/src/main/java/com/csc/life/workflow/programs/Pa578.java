/*
 * Copyright (2019) DXC Technology, all rights reserved.
 */
package com.csc.life.workflow.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.ldap.LdapDao;
import com.csc.life.workflow.screens.Sa578ScreenVars;
import com.csc.smart.procedures.Sanctn;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.utils.UserMapingUtils;
import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Errmesg;
import com.csc.workflow.WorkflowClient;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.screenmodel.ScreenModel;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * This program represents Update Follow-ups Human Task.
 *  
 * @author vparmar21
 *
 */
public class Pa578 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pa578");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Sanctnrec sanctnrec = new Sanctnrec();

	private Sa578ScreenVars sv = ScreenProgram.getScreenVars(Sa578ScreenVars.class);

	private FixedLengthStringData wsaaPassword = new FixedLengthStringData(6);
	private Wsspsmart wsspsmart = new Wsspsmart();
	private FixedLengthStringData filler = new FixedLengthStringData(767).isAPartOf(wsspsmart.userArea, 0,
			FILLER_REDEFINE);
	
	// IJTI-1090 starts
	private FixedLengthStringData wsspTaskid = new FixedLengthStringData(200).isAPartOf(filler, 0);
	private FixedLengthStringData wsspcontainerid = new FixedLengthStringData(200).isAPartOf(filler, 200);
	private FixedLengthStringData wsspProcessInstanceid = new FixedLengthStringData(10).isAPartOf(filler, 550);
	// IJTI-1090 ends
	
	private WorkflowClient workflowClient = getApplicationContext().getBean("workflowClient", WorkflowClient.class);
	private static final Logger LOGGER = LoggerFactory.getLogger(Pa578.class);
	private String authString="";     //IJTI-1531
	private LdapDao ldap = new LdapDao(); //IJTI-1531
	private Errmesgrec errmesgrec = new Errmesgrec();

	public Pa578() {
		super();
		screenVars = sv;
		new ScreenModel("Sa578", AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		super.mainline();
	}

	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		processBoMainline(sv, sv.dataArea, parmArray);
	}

	/**
	 * Initializes the screen.
	 * 
	 */
	@Override
	protected void initialise1000() {
		// IJTI-1131 starts
		sv.dataArea.set(SPACES);
		String userId=UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString()).toLowerCase().trim(); // IJTI-1131
		
		// IJTI-1531 Start
		String pass = ldap.getLdapUserPwd(userId, appVars);

		authString = userId + ":" + pass;

		String authStringEnc = "\"Basic "
				+ new String(java.util.Base64.getEncoder().encode(authString.getBytes(StandardCharsets.UTF_8)),
						StandardCharsets.UTF_8)
				+ "\"";
		// IJTI-1531 End
		sv.authStringForJBPM.set(authStringEnc);
		//IJTI-1131 ends
		sv.effdateDisp.set(SPACES);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

		sanctnrec.function.set("USRNO");
		sanctnrec.userid.set(wsspcomn.userid);
		// IJTI-1090 starts
		sv.jbpmContainerId.set(wsspcontainerid);
		sv.jbpmprocessinstanceID.set(wsspProcessInstanceid);
		// IJTI-1090 ends
		wsaaPassword.set(sanctnrec.password.getData());

	}

	@Override
	protected void preScreenEdit() {
		//empty method required for framework
	}
	/**
	 * Validates and displays the screen.
	 */
	@Override
	protected void screenEdit2000() {
		//IBPTE-1239 starts
		if(isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
			return;
		}
		//IBPTE-1239 ends
		wsspcomn.edterror.set(Varcom.oK);
		if (isEQ(scrnparams.statuz, Varcom.calc)) {
			wsspcomn.edterror.set("Y"); 
 
		} 
		sanctnrec.userid.set(wsspcomn.userid);
		
		callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
		if (isNE(sanctnrec.statuz, Varcom.oK)) {
			syserrrec.params.set(sanctnrec.sanctnRec);
			syserrrec.subrname.set("SANCTN");
			syserrrec.statuz.set(Varcom.bomb);
			fatalError600();
		}
		
		if(isEQ(sv.effdate, SPACES) || isEQ(sv.effdate, "99999999")) {
			sv.effdateErr.set("E186");	
			wsspcomn.edterror.set("E186");
		}
		
		if(isEQ(sv.zdoctor, SPACES)) {
			sv.zdoctorErr.set("E186");	
			wsspcomn.edterror.set("E186");
		}
		
	}
	
	@Override
	protected void update3000(){
		wsaaPassword.set(sanctnrec.password.getData());
		try {
			HashMap<String, String> inputMap = new HashMap<>();
			inputMap.put("doctor", sv.zdoctor.getFormData().trim());
			inputMap.put("receivedDate", sv.effdateDisp.toString().trim());

			workflowClient.completeHumanTask(
					UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString()).toLowerCase().trim(),
					wsspTaskid.toString().trim(), inputMap, wsspcontainerid.trim(),authString);		//IJTI-1531
		} catch (Exception e) {
			//IBPTE-1239
			LOGGER.error(getMessage("RUQ8", "Exception Occurred in completeHumanTask "), e);
			scrnparams.errorCode.set("RUQ8");
			wsspcomn.edterror.set("Y");
		}
	}
	
	/**
	 * Where to go next.
	 */
	@Override
	protected void whereNext4000() {
		wsspcomn.nextprog.set("Pr2fw");
	}
	
	/* IBPTE-1239 START */
	private String getMessage(String errorCode, String errorMessage) {
		errmesgrec.eror.set(errorCode);
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaScreen);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.clear();
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		if (isEQ(errmesgrec.statuz, "BOMB")) {
			scrnparams.statuz.set(errmesgrec.statuz);
			screenErrors200();
		}
		return errorMessage + ". " + errmesgrec.errorline.trim();
	}

	/* IBPTE-1239 END */
}
