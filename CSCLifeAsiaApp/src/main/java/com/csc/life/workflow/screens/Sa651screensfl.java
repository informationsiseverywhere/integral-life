package com.csc.life.workflow.screens;

import com.quipoz.COBOLFramework.TableModel.Subfile;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.DecimalData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.tablemodel.TableModel;
import com.quipoz.framework.util.DataModel;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for subfile SCREENSFL
 * 
 * @version 1.0 generated on 30/08/09 05:40
 * @author igarg2
 */
public class Sa651screensfl extends Subfile {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = false;
	public static final int[] pfInds = new int[] { 1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 20, 21, 22, 23, 24 };
	public static int maxRecords = 12;
	public static int nextChangeIndicator = 94;
	public static int[] affectedInds = new int[] { 1, 30, 31, 32, 33, 34, 35, 36, 38, 38, 39, 40, 41, 42, 43 };

	private static final String FUPCDES = "fupcdes";
	private static final String FUPNO = "fupno";
	private static final String LANGUAGE = "language";
	private static final String IND = "ind";

	private static final String ZITEM = "zitem";
	private static final String INDIC = "indic";
	
	private static final String SCREENINDICAREA = "screenIndicArea";
	private static final String FUPREMK= "fupremk";
	

	public static RecInfo lrec = new RecInfo();
	static {
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] { 11, 15, 2, 61 });
	}

	/**
	 * Writes a record to the screen.
	 * 
	 * @param errorInd
	 *            - will be set on if an error occurs
	 * @param noRecordFoundInd
	 *            - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3) {
		Sa651ScreenVars sv = (Sa651ScreenVars) pv;
		if (GeneralTable.isFull(maxRecords, sv.getSubfilePageSize(), sv.sa651screensfl.getRowCount())) {
			ind3.setOn();
			return;
		}
		TableModel tm = Subfile.write(ROUTINE, av, pv, sv.sa651screensfl, sv.Sa651screensflWritten, ind2, ind3,
				maxRecords);
		if (ind2.isOn() || ind3.isOn()) {
			return;
		}
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		clearInds(av, pfInds);
		tm.write();
	}

	public static void update(COBOLAppVars av, VarModel pv, Indicator ind2) {
		Sa651ScreenVars sv = (Sa651ScreenVars) pv;
		TableModel tm = Subfile.update(ROUTINE, av, pv, sv.sa651screensfl, ind2);
		setSubfileData(tm.bufferedRow, av, pv);
		if (av.getInd(nextChangeIndicator)) {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_MODIFIED);
		} else {
			tm.bufferedRow.getField(TableModel.ROW_MODIFIED_STATE).set(TableModel.ROW_UNMODIFIED);
		}
		tm.update();
	}

	public static void readNextChangedRecord(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3,
			DecimalData sflIndex) {
		Sa651ScreenVars sv = (Sa651ScreenVars) pv;
		DataModel dm = Subfile.readc(ROUTINE, av, pv, sv.sa651screensfl, ind2, ind3, sflIndex);
		getSubfileData(dm, av, pv);
		// if there are no more changed records, but the subfileModified flag indicates
		// that rows have been changed
		// we return to the start of the subfile for subsequent calls
		if (ind3.isOn() && sv.Sa651screensflWritten.gt(0)) {
			sv.sa651screensfl.setCurrentIndex(0);
			sv.Sa651screensflWritten.set(0);
		}
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv, int record, Indicator ind2, Indicator ind3) {
		Sa651ScreenVars sv = (Sa651ScreenVars) pv;
		DataModel dm = Subfile.chain(ROUTINE, av, pv, sv.sa651screensfl, record, ind2, ind3);
		getSubfileData(dm, av, pv);
		restoreInds(dm, av, affectedInds);
	}

	public static void chain(COBOLAppVars av, VarModel pv, BaseData record, Indicator ind2, Indicator ind3) {
		chain(av, pv, record.toInt(), ind2, ind3);
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv, int record, Indicator ind2, Indicator ind3) {
		av.COBOLFileError = false;
		chain(av, pv, record, ind2, ind3);
		if (ind3.isOn())
			av.COBOLFileError = true;
	}

	public static void chainErrorStatus(COBOLAppVars av, VarModel pv, BaseData record, Indicator ind2, Indicator ind3) {
		chainErrorStatus(av, pv, record.toInt(), ind2, ind3);
	}

	public static void getSubfileData(DataModel dm, COBOLAppVars av, VarModel pv) {
		if (dm != null) {
			Sa651ScreenVars screenVars = (Sa651ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName(SCREENINDICAREA);
				screenVars.fupno.setFieldName(FUPNO);
				screenVars.fupcdes.setFieldName(FUPCDES);
				screenVars.fupremk.setFieldName(FUPREMK);

			}
			screenVars.screenIndicArea.set(dm.getField(SCREENINDICAREA));
			screenVars.fupno.set(dm.getField(FUPNO));
			screenVars.fupcdes.set(dm.getField(FUPCDES));
			screenVars.fupremk.set(dm.getField(FUPREMK));

		}
	}

	public static void setSubfileData(DataModel dm, COBOLAppVars av, VarModel pv) {
		if (dm != null) {
			Sa651ScreenVars screenVars = (Sa651ScreenVars) pv;
			if (screenVars.screenIndicArea.getFieldName() == null) {
				screenVars.screenIndicArea.setFieldName(SCREENINDICAREA);
				screenVars.fupno.setFieldName(FUPNO);
				screenVars.fupcdes.setFieldName(FUPCDES);
				screenVars.language.setFieldName(LANGUAGE);
				screenVars.ind.setFieldName(IND);
				screenVars.zitem.setFieldName(ZITEM);
				screenVars.indic.setFieldName(INDIC);
				screenVars.fupremk.setFieldName(FUPREMK);

			}
			dm.getField(SCREENINDICAREA).set(screenVars.screenIndicArea);
			dm.getField(FUPNO).set(screenVars.fupno);
			dm.getField(FUPCDES).set(screenVars.fupcdes);
			dm.getField(LANGUAGE).set(screenVars.language);
			dm.getField(IND).set(screenVars.ind);
			dm.getField(ZITEM).set(screenVars.zitem);
			dm.getField(INDIC).set(screenVars.indic);
			dm.getField(FUPREMK).set(screenVars.fupremk);

			
		}
	}

	public static String getRecName() {
		return ROUTINE;
	}

	public static int getMaxRecords() {
		return maxRecords;
	}

	public static void getMaxRecords(int maxRecords) {
		Sa651screensfl.maxRecords = maxRecords;
	}

	public static void set1stScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.set1stScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void setNextScreenRow(GeneralTable gt, COBOLAppVars appVars, VarModel pv) {
		gt.setNextScreenRow();
		getSubfileData(gt.getCurrentScreenRow(), appVars, pv);
		restoreInds(gt.getCurrentScreenRow(), appVars, affectedInds);
		clearFormatting(pv);
	}

	public static void clearFormatting(VarModel pv) {
		Sa651ScreenVars screenVars = (Sa651ScreenVars) pv;
		screenVars.screenIndicArea.clearFormatting();
		screenVars.fupno.clearFormatting();
		screenVars.select.clearFormatting();
		screenVars.fupcdes.clearFormatting();
		screenVars.ind.clearFormatting();
		screenVars.indic.clearFormatting();
		screenVars.zitem.clearFormatting();
		screenVars.language.clearFormatting();
		screenVars.fupremk.clearFormatting();
		
		clearClassString(pv);
	}

	public static void clearClassString(VarModel pv) {
		Sa651ScreenVars screenVars = (Sa651ScreenVars) pv;
		screenVars.screenIndicArea.setClassString("");
		screenVars.fupno.setClassString("");
		screenVars.select.setClassString("");
		screenVars.fupcdes.setClassString("");
		screenVars.ind.setClassString("");
		screenVars.indic.setClassString("");
		screenVars.zitem.setClassString("");
		screenVars.language.setClassString("");
		screenVars.fupremk.setClassString("");
	}

	/**
	 * Clear all the variables in Sa651screensfl
	 */
	public static void clear(VarModel pv) {
		Sa651ScreenVars screenVars = (Sa651ScreenVars) pv;
		screenVars.screenIndicArea.clear();
		screenVars.fupno.clear();
		screenVars.select.clear();
		screenVars.fupcdes.clear();
		screenVars.ind.clear();
		screenVars.indic.clear();
		screenVars.zitem.clear();
		screenVars.language.clear();
		screenVars.fupremk.clear();
	}
}
