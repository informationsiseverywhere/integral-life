package com.csc.life.workflow.screens;


import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREENCTL
 * This is the subfile Control record for SCREENSFL
 * @version 1.0 generated on 30/08/09 05:40
 * @author Quipoz
 */
public class Sa651screenctl extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean overlay = true;
	public static final int[] pfInds = new int[] {1, 2, 3, 4, 5, 12, 15, 16, 17, 18, 20, 21, 22, 23, 24}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		lrec.relatedSubfile = "Sa651screensfl";
		lrec.subfileClass = Sa651screensfl.class;
		lrec.relatedSubfileRecordName = lrec.relatedSubfile + "Written";
		lrec.displaySubfileIndicator = QPUtilities.packByteIntoInt(90, lrec.displaySubfileIndicator );
		lrec.controlSubfileIndicator = new int[] {-91,-92};
		lrec.initializeSubfileIndicator = 91;
		lrec.clearSubfileIndicator = 92;
		lrec.endSubfileIndicator = -93;
		lrec.sizeSubfile = 20;
		lrec.pageSubfile = 20;
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 10, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa651ScreenVars sv = (Sa651ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa651screenctlWritten, sv.Sa651screensflWritten, av, sv.sa651screensfl, ind2, ind3, pv);
	}

	public static void read(Indicator ind2, Indicator ind3) { // 
		}
	

	public static void clearClassString(VarModel pv) {
		Sa651ScreenVars screenVars = (Sa651ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.subfilePosition.setClassString("");
		screenVars.jbpmtaskId.setClassString("");
		screenVars.jbpmContainerId.setClassString("");
		screenVars.jbpmprocessinstanceID.setClassString("");
		screenVars.authStringForJBPM.setClassString("");
		screenVars.huwdcdte.setClassString("");
		screenVars.isUSTaxPayor.setClassString("");
		screenVars.lifeAssuredAge.setClassString("");
		screenVars.lifeAssuredBMI.setClassString("");
		screenVars.lifeAssuredIncome.setClassString("");
		screenVars.lifeAssuredName.setClassString("");
		screenVars.lifeAssuredNum.setClassString("");
		screenVars.occupClass.setClassString("");
		screenVars.policyNum.setClassString("");
		screenVars.riskIndicator.setClassString("");
		screenVars.cmncDate.setClassString("");
	}

/**
 * Clear all the variables in Sa651screenctl
 */
	public static void clear(VarModel pv) {
		Sa651ScreenVars screenVars = (Sa651ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.subfilePosition.clear();
		screenVars.jbpmtaskId.clear();
		screenVars.jbpmContainerId.clear();
		screenVars.jbpmprocessinstanceID.clear();
		screenVars.huwdcdte.clear();
		screenVars.isUSTaxPayor.clear();
		screenVars.lifeAssuredAge.clear();
		screenVars.lifeAssuredBMI.clear();
		screenVars.lifeAssuredIncome.clear();
		screenVars.lifeAssuredName.clear();
		screenVars.lifeAssuredNum.clear();
		screenVars.occupClass.clear();
		screenVars.policyNum.clear();
		screenVars.riskIndicator.clear();
		screenVars.authStringForJBPM.clear();
		screenVars.cmncDate.clear();
	}
}
