package com.csc.life.workflow.config;

import com.csc.smart400framework.SMARTAppVars;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.IntegralWorkflowProperties;

/**
 * IntegralLifeWorkflowProperties use for Life specific workflow properties.
 * 
 * @author ajamodkar
 *
 */
public class IntegralLifeWorkflowProperties extends IntegralWorkflowProperties {

	private IntegralLifeWorkflowProperties() {
		super();
	}
	
	
	/**
	 * Getter for nbProcessDefinition
	 * 
	 * @return the nbProcessDefinition
	 */
	public static String getNBProcessDefinition() {
		String processDefinition = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.nbProcessDefinition"); // IJTI-1629
		if (processDefinition != null) {
			processDefinition = processDefinition.trim();
		}
		return processDefinition;
	}

	/**
	 * Getter for nbProcessFormProperty
	 * 
	 * @return the nbProcessFormProperty
	 */
	public static String getNBProcessFormProperty() {
		String processFormProperty = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.nbProcessFormProperty"); // IJTI-1629
		if (processFormProperty != null) {
			processFormProperty = processFormProperty.trim();
		}
		return processFormProperty;
	}
	
	/**
	 * Getter for UWProcessDefinition
	 * 
	 * @return the UWProcessDefinition
	 */
	public static String getUWProcessDefinition() {
		String processDefinition = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.uwProcessDefinition"); // IJTI-1629
		if (processDefinition != null) {
			processDefinition = processDefinition.trim();
		}
		return processDefinition;
	}
	
	/**
	 * Getter for PolicyNumber
	 * 
	 * @return the PolicyNumber
	 */
	public static String getPolicyNumber() {
		String polisyNumber = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.uw.policyNumber"); // IJTI-1629
		if (polisyNumber != null) {
			polisyNumber = polisyNumber.trim();
		}
		return polisyNumber;
	}
	
	/**
	 * Getter for UserProperty
	 * 
	 * @return the UserProperty
	 */
	public static String getUserProperty() {
		String userProperty = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.uw.userProperty"); // IJTI-1629
		if (userProperty != null) {
			userProperty = userProperty.trim();
		}
		return userProperty;
	}
	
	/**
	 * Getter for UW deploymentId
	 * 
	 * @return the deploymentId
	 */
	public static String getUWWorkflowDeploymentId() {
		String deploymentId = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.uw.deploymentId"); // IJTI-1629
		if (deploymentId != null) {
			deploymentId = deploymentId.trim();
		}
		return deploymentId;
	}
	
	/**
	 * Getter for NB user name
	 * 
	 * @return username
	 */
	public static String getNBuserName() {
		String userName = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.nbuserName"); // IJTI-1629
		if (userName != null) {
			userName = userName.trim();
		}
		return userName;
	}
	
	/**
	 * Getter for NB password
	 * 
	 * @return password
	 */
	public static String getNBpassword() {
		String password = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.nbpassword"); // IJTI-1629
		if (password != null) {
			password = password.trim();
		}
		return password;
	}
	//IJTI-1196 starts
	
	/**
	 * Getter for UW user name
	 * 
	 * @return username
	 */
	public static String getUWuserName() {
		String userName = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.uwuserName"); // IJTI-1629
		if (userName != null) {
			userName = userName.trim();
		}
		return userName;
	}
	
	/**
	 * Getter for UW password
	 * 
	 * @return password
	 */
	public static String getUWpassword() {
		String password = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.uwpassword"); // IJTI-1629
		if (password != null) {
			password = password.trim();
		}
		return password;
	}
	
	//IJTI-1196 ends
	

	/**
	 * Getter for deploymentId
	 * 
	 * @return the deploymentId
	 */
	public static String getNBWorkflowDeploymentId() {
		String deploymentId = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.nb.deploymentId"); // IJTI-1629
		if (deploymentId != null) {
			deploymentId = deploymentId.trim();
		}
		return deploymentId;
	}
	// IJTI-1531 End
	
	public static String getMetLifeNbWorkflowDeploymentId() {
		String deploymentId = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.nb.policy.deploymentId"); // IJTI-1629
		if (deploymentId != null) {
			deploymentId = deploymentId.trim();
		}
		return deploymentId;
	}
	
	/**
	 * Getter For NB Retail Workflow Reference Number
	 * @return Reference Number
	 */
	 	public static String getReferenceNumber() {
		String referenceNumber = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.nb.policy.referenceNumber"); // IJTI-1241
		if (referenceNumber != null) {
			referenceNumber = referenceNumber.trim();
		}
		return referenceNumber;
	}
	
	/**
	 * Getter For NB Retail Workflow Process Definition
	 * @return Process Definition
	 */
	 
	 public static String getNBWorkflowProcessDefinition() {
		String processDefinition = getIWPProperties(((SMARTAppVars) AppVars.getInstance()).getTenantid())
				.getProperty("workflow.life.nb.policy.processDefinition"); // IJTI-1241
		if (processDefinition != null) {
			processDefinition = processDefinition.trim();
		}
		return processDefinition;
	}
	
}
