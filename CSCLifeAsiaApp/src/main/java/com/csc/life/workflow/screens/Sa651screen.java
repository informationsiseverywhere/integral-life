package com.csc.life.workflow.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sa651screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean OVERLAY = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 23, 18, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa651ScreenVars sv = (Sa651ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa651screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
		//
	}

	public static void clearClassString(VarModel pv) {
		Sa651ScreenVars screenVars = (Sa651ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.jbpmtaskId.setClassString("");
		screenVars.jbpmContainerId.setClassString("");
		screenVars.jbpmprocessinstanceID.setClassString("");
		screenVars.authStringForJBPM.setClassString("");
		screenVars.huwdcdte.setClassString("");
		screenVars.isUSTaxPayor.setClassString("");
		screenVars.lifeAssuredAge.setClassString("");
		screenVars.lifeAssuredBMI.setClassString("");
		screenVars.lifeAssuredIncome.setClassString("");
		screenVars.lifeAssuredName.setClassString("");
		screenVars.lifeAssuredNum.setClassString("");
		screenVars.occupClass.setClassString("");
		screenVars.policyNum.setClassString("");
		screenVars.riskIndicator.setClassString("");
		screenVars.language.setClassString("");
		screenVars.zitem.setClassString("");
		screenVars.ind.setClassString("");
		screenVars.indic.setClassString("");
		screenVars.cmncDate.setClassString("");
	}

/**
 * Clear all the variables in Sa651screen
 */
	public static void clear(VarModel pv) {
		Sa651ScreenVars screenVars = (Sa651ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.jbpmtaskId.clear();
		screenVars.jbpmContainerId.clear();
		screenVars.jbpmprocessinstanceID.clear();
		screenVars.huwdcdte.clear();
		screenVars.isUSTaxPayor.clear();
		screenVars.lifeAssuredAge.clear();
		screenVars.lifeAssuredBMI.clear();
		screenVars.lifeAssuredIncome.clear();
		screenVars.lifeAssuredName.clear();
		screenVars.lifeAssuredNum.clear();
		screenVars.occupClass.clear();
		screenVars.policyNum.clear();
		screenVars.riskIndicator.clear();
		screenVars.authStringForJBPM.clear();
		screenVars.language.clear();
		screenVars.zitem.clear();
		screenVars.ind.clear();
		screenVars.indic.clear();
		screenVars.cmncDate.clear();
	}
}
