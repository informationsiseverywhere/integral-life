package com.csc.life.workflow.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * 
 * @version 1.0 generated on 30/08/09 05:43
 * @author igarg2
 */
public class Sa650screen extends ScreenRecord {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean OVERLAY = false;
	public static final int[] pfInds = new int[] { 4, 22, 17, 23, 18, 15, 6, 24, 16, 1, 2, 3, 21 };
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] { 1, 17, 2, 79 });
	}

	/**
	 * Writes a record to the screen.
	 * 
	 * @param errorInd
	 *            - will be set on if an error occurs
	 * @param noRecordFoundInd
	 *            - will be set on if no changed record is found
	 */
	public static void write(COBOLAppVars av, VarModel pv, Indicator ind2, Indicator ind3) {
		Sa650ScreenVars sv = (Sa650ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa650screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
		//
	}

	public static void clearClassString(VarModel pv) {
		Sa650ScreenVars screenVars = (Sa650ScreenVars) pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.agentName.setClassString("");
		screenVars.agentNum.setClassString("");
		screenVars.authStringForJBPM.setClassString("");
		screenVars.contractNum.setClassString("");
		screenVars.contractOwnerName.setClassString("");
		screenVars.contractOwnerNum.setClassString("");
		screenVars.contractType.setClassString("");
		screenVars.followUpsInPolicy.setClassString("");
		screenVars.lifeAssuredName.setClassString("");
		screenVars.lifeAssuredNum.setClassString("");
		screenVars.jbpmtaskId.setClassString("");
		screenVars.jbpmContainerId.setClassString("");
		screenVars.jbpmprocessinstanceID.setClassString("");
		screenVars.lifeAssuredAge.setClassString("");
		screenVars.lifeAssuredBMI.setClassString("");
		screenVars.tsar.setClassString("");
		screenVars.cmncDate.setClassString("");

	}

	/**
	 * Clear all the variables in Sa650screen
	 */
	public static void clear(VarModel pv) {
		Sa650ScreenVars screenVars = (Sa650ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.agentName.clear();
		screenVars.agentNum.clear();
		screenVars.authStringForJBPM.clear();
		screenVars.contractNum.clear();
		screenVars.contractOwnerName.clear();
		screenVars.contractOwnerNum.clear();
		screenVars.contractType.clear();
		screenVars.followUpsInPolicy.clear();
		screenVars.lifeAssuredName.clear();
		screenVars.lifeAssuredNum.clear();
		screenVars.jbpmtaskId.clear();
		screenVars.jbpmContainerId.clear();
		screenVars.jbpmprocessinstanceID.clear();
		screenVars.lifeAssuredAge.clear();
		screenVars.lifeAssuredBMI.clear();
		screenVars.tsar.clear();
		screenVars.cmncDate.clear();
	}
}
