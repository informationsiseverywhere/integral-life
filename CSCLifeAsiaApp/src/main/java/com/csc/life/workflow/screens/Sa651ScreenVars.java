package com.csc.life.workflow.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;

import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datadictionarydatatype.FLSDDObj;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.tablemodel.GeneralTable;
import com.quipoz.framework.util.AppVars;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
/**
 * Screen variables for Sa651
 * 
 * @version 1.0 generated on 30/08/09 06:52
 * @author igarg2
 */
public class Sa651ScreenVars extends SmartVarModel {

	private static final long serialVersionUID = 1;
	public FixedLengthStringData dataArea = new FixedLengthStringData(838);

	
	public FixedLengthStringData dataFields = new FixedLengthStringData(566).isAPartOf(dataArea, 0);
	public FixedLengthStringData policyNum = DD.chdrnum.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData lifeAssuredNum = DD.lifenum.copy().isAPartOf(dataFields, 8);
	public FixedLengthStringData lifeAssuredName = DD.lifename.copy().isAPartOf(dataFields, 16);
	public ZonedDecimalData lifeAssuredIncome = DD.lifeIncome.copyToZonedDecimal().isAPartOf(dataFields, 63);  
	public FixedLengthStringData riskIndicator = DD.clrskind.copy().isAPartOf(dataFields, 75);
	public FixedLengthStringData isUSTaxPayor = DD.payrnum.copy().isAPartOf(dataFields, 76); 
	public FixedLengthStringData occupClass = DD.occclass.copy().isAPartOf(dataFields, 84);
	public ZonedDecimalData lifeAssuredAge = DD.anbage.copyToZonedDecimal().isAPartOf(dataFields, 86);
	public ZonedDecimalData lifeAssuredBMI = DD.bmi.copyToZonedDecimal().isAPartOf(dataFields, 89);
	public ZonedDecimalData tsar = DD.tsar.copyToZonedDecimal().isAPartOf(dataFields, 93);
	public FixedLengthStringData uwDecision = new FLSDDObj(20).copy().isAPartOf(dataFields, 110);
	public ZonedDecimalData huwdcdte = DD.huwdcdte.copyToZonedDecimal().isAPartOf(dataFields, 130); 
	public FixedLengthStringData jbpmtaskId = DD.jbpmtaskId.copy().isAPartOf(dataFields, 138);
	public FixedLengthStringData jbpmContainerId = DD.jbpmContainerId.copy().isAPartOf(dataFields, 148);
	public FixedLengthStringData jbpmprocessinstanceID = DD.jbpmprocessinstanceID.copy().isAPartOf(dataFields, 348);
	public FixedLengthStringData authStringForJBPM = DD.authStringForJBPM.copy().isAPartOf(dataFields, 358); 
	public ZonedDecimalData cmncDate = DD.cmncDate.copyToZonedDecimal().isAPartOf(dataFields,558); 
	
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(68).isAPartOf(dataArea, 566);
	public FixedLengthStringData policyNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData lifeAssuredNumErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData lifeAssuredNameErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData lifeAssuredIncomeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData riskIndicatorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData isUSTaxPayorErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);
	public FixedLengthStringData occupClassErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 24);
	public FixedLengthStringData lifeAssuredAgeErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 28);
	public FixedLengthStringData lifeAssuredBMIErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 32);
	public FixedLengthStringData tsarErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 36);
	public FixedLengthStringData uwDecisionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 40);
	public FixedLengthStringData huwdcdteErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 44); 
	public FixedLengthStringData jbpmContainerIdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 48);
	public FixedLengthStringData jbpmprocessinstanceIDErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 52);
	public FixedLengthStringData jbpmtaskIdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 56);
	public FixedLengthStringData authStringForJBPMErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 60);
	public FixedLengthStringData cmncDateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 64);
	
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(204).isAPartOf(dataArea, 634);
	public FixedLengthStringData[] policyNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] lifeAssuredNumOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] lifeAssuredNameOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] lifeAssuredIncomeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] riskIndicatorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] isUSTaxPayorOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	public FixedLengthStringData[] occupClassOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 72);
	public FixedLengthStringData[] lifeAssuredAgeOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 84);
	public FixedLengthStringData[] lifeAssuredBMIOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 96);
	public FixedLengthStringData[] tsarOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 108);
	public FixedLengthStringData[] uwDecisionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 120);
	public FixedLengthStringData[] huwdcdteOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 132);
	public FixedLengthStringData[] jbpmContainerIdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 144);
	public FixedLengthStringData[] jbpmprocessinstanceIDOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 156);
	public FixedLengthStringData[] jbpmtaskIdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 168);
	public FixedLengthStringData[] authStringForJBPMOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 180);
	public FixedLengthStringData[] cmncDateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 192);
	
	
	
	
	
	public FixedLengthStringData subfileArea = new FixedLengthStringData(190);
	public FixedLengthStringData subfileFields = new FixedLengthStringData(58).isAPartOf(subfileArea, 0);
	public FixedLengthStringData fupcdes = DD.fupcdes.copy().isAPartOf(subfileFields,0);
	public ZonedDecimalData fupno = DD.fupno.copyToZonedDecimal().isAPartOf(subfileFields,4);
	public FixedLengthStringData select = DD.select.copy().isAPartOf(subfileFields,6);
	public FixedLengthStringData ind = DD.ind.copy().isAPartOf(subfileFields,7);
	public FixedLengthStringData indic = DD.indic.copy().isAPartOf(subfileFields,8);
	public FixedLengthStringData language = DD.language.copy().isAPartOf(subfileFields,9);
	public FixedLengthStringData zitem = DD.zitem.copy().isAPartOf(subfileFields,10);
	public FixedLengthStringData fupremk = DD.fuprmk.copy().isAPartOf(subfileFields,18);
	
	public FixedLengthStringData errorSubfile = new FixedLengthStringData(32).isAPartOf(subfileArea, 58);
	public FixedLengthStringData fupcdesErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 0);
	public FixedLengthStringData fupnoErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 4);
	public FixedLengthStringData selectErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 8);
	public FixedLengthStringData indErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 12);
	public FixedLengthStringData indicErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 16);
	public FixedLengthStringData languageErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 20);
	public FixedLengthStringData zitemErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 24);
	public FixedLengthStringData fupremkErr = new FixedLengthStringData(4).isAPartOf(errorSubfile, 28);
	
	public FixedLengthStringData outputSubfile = new FixedLengthStringData(96).isAPartOf(subfileArea, 90);
	public FixedLengthStringData[] fupcdesOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 0);
	public FixedLengthStringData[] fupnoOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 12);
	public FixedLengthStringData[] selectOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 24);
	public FixedLengthStringData[] indOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 36);
	public FixedLengthStringData[] indicOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 48);
	public FixedLengthStringData[] languageOut = FLSArrayPartOfStructure(12, 1, outputSubfile, 60);
	public FixedLengthStringData[] zitemOut = FLSArrayPartOfStructure(12, 1, outputSubfile,72);
	public FixedLengthStringData[] fupremkOut = FLSArrayPartOfStructure(12, 1, outputSubfile,84);
	public PackedDecimalData subfilePage = new PackedDecimalData(3, 0).isAPartOf(subfileArea, 186);
		/*Indicator Area*/
	public FixedLengthStringData screenIndicArea = DD.indicarea.copy();
		/*Subfile record no*/
	public ZonedDecimalData subfilePosition = DD.srno.copyToZonedDecimal();

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();
	public FixedLengthStringData huwdcdteDisp = new FixedLengthStringData(10);  
	public FixedLengthStringData cmncDateDisp = new FixedLengthStringData(10);
	public LongData Sa651screenWritten = new LongData(0);
	public LongData Sa651protectWritten = new LongData(0);
	public LongData Sa651screensflWritten = new LongData(0);
	public LongData Sa651screenctlWritten = new LongData(0);
	public GeneralTable sa651screensfl = new GeneralTable(AppVars.getInstance());


	@Override
	public boolean hasSubfile() {
		return true;
	}
	@Override
	public GeneralTable getScreenSubfileTable() {
		return sa651screensfl;
		
	}

	public Sa651ScreenVars() {
		super();
		init();
	}
	
	private void init(){
		initialiseScreenVars();
	}

	@Override
	protected void initialiseScreenVars() {

		screenFields = new BaseData[] { jbpmtaskId, jbpmContainerId, jbpmprocessinstanceID, authStringForJBPM,
				policyNum, isUSTaxPayor, riskIndicator, lifeAssuredAge, lifeAssuredBMI, lifeAssuredIncome,
				lifeAssuredName, lifeAssuredNum, uwDecision, huwdcdte, occupClass, tsar,cmncDate };
		screenOutFields = new BaseData[][] { jbpmtaskIdOut, jbpmContainerIdOut, jbpmprocessinstanceIDOut,
				authStringForJBPMOut, policyNumOut, isUSTaxPayorOut, riskIndicatorOut, lifeAssuredAgeOut,
				lifeAssuredBMIOut, lifeAssuredIncomeOut, lifeAssuredNameOut, lifeAssuredNumOut, uwDecisionOut,
				huwdcdteOut, occupClassOut, tsarOut,cmncDateOut};
		screenErrFields = new BaseData[] { jbpmtaskIdErr, jbpmContainerIdErr, jbpmprocessinstanceIDErr,
				authStringForJBPMErr, policyNumErr, isUSTaxPayorErr, riskIndicatorErr, lifeAssuredAgeErr,
				lifeAssuredBMIErr, lifeAssuredIncomeErr, lifeAssuredNameErr, lifeAssuredNumErr, uwDecisionErr,
				huwdcdteErr, occupClassErr, tsarErr,cmncDateErr };
		fieldIndMap.put(uwDecisionOut,
				new String[] { "13", null, "-13", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(huwdcdteOut,
				new String[] { "14", null, "-14", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(selectOut,
				new String[] { "15", null, "-15", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fupcdesOut,
				new String[] { "16", null, "-16", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(fupremkOut,
				new String[] { "17", null, "-17", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(cmncDateOut,
				new String[] { "18", null, "-18", null, null, null, null, null, null, null, null, null });
		screenDateFields = new BaseData[] { huwdcdte ,cmncDate};
		screenDateErrFields = new BaseData[] { huwdcdteErr ,cmncDateErr};
		screenDateDispFields = new BaseData[] { huwdcdteDisp ,cmncDateDisp};
		
		screenSflFields = new BaseData[] {fupno,select, fupcdes,language,zitem,ind,indic,fupremk};
		screenSflOutFields = new BaseData[][] {fupnoOut,selectOut,fupcdesOut,languageOut,zitemOut,indOut,indicOut,fupremkOut};
		screenSflErrFields = new BaseData[] {fupnoErr, selectErr, fupcdesErr,languageErr,zitemErr,indErr,indicErr,fupremkErr};
		screenSflDateFields = new BaseData[] {};
		screenSflDateErrFields = new BaseData[] {};
		screenSflDateDispFields = new BaseData[] {};
		
		screenDataArea = dataArea;
		screenSubfileArea = subfileArea;
		screenSflIndicators = screenIndicArea;
		errorInds = errorIndicators;
		errorSflInds = errorSubfile;
		
		screenRecord = Sa651screen.class;
		screenSflRecord = Sa651screensfl.class;
		screenCtlRecord = Sa651screenctl.class;
		initialiseSubfileArea();
		protectRecord = Sa651protect.class;
		
	}
	@Override
	public void initialiseSubfileArea() {
		initialize(screenSubfileArea);
		subfilePage.set(Sa651screenctl.lrec.pageSubfile);
	}
}
