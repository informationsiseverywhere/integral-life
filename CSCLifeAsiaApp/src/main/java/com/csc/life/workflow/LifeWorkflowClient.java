package com.csc.life.workflow;

import java.util.HashMap;
import java.util.Map;

import com.csc.exceptions.WorkflowServiceException;

/**
 * Interface for workflow client. Based on type of workflow system, we want to
 * integrate with Integral; we will have different implementations At a time
 * Integral will be integrated with only one workflow system. We will define the
 * specific implementation in IAFContext spring configuration file.
 * 
 * @author vhukumagrawa
 *
 */

public interface LifeWorkflowClient {

	/**
	 * Starts a process in workflow system. This can be used to launch workflow from
	 * Integral system.
	 * 
	 * @param policyNumber
	 *            - Policy Number
	 * 
	 * @param usreName
	 *            - User Name
	 * @param password
	 *            - Password
	 * 
	 * @throws WorkflowServiceException
	 *             - The WorkflowServiceException
	 */
	void startNewBusinessWorkflow(String policyNumber,String userName , String password) throws WorkflowServiceException;
	
	/**
	 * Starts UW process in workflow system. This can be used to launch UW
	 * process from Integral system.
	 * 
	 * @param policyNumber
	 *            - Policy Number
	 * @param userID
	 *            - User ID
	 * @throws WorkflowServiceException
	 *             - The WorkflowServiceException
	 */
	void startUWWorkflow(String policyNumber, String userName, String password) throws WorkflowServiceException;
	

	/**
	 * Get the Form Values
	 * from human task
	 *  
	 * @param userId
	 * @param processid
	 * @param containerid
	 * @param screenName
	 * @return
	 * @throws WorkflowServiceException
	 */
	
	public Map<String, String> getTaskFormValues(String userId, String processid, String containerid, String screenName,
			String authString) throws WorkflowServiceException; // IJTI-1531
	
	
	/**
	 * Complete Human Task
	 * 
	 * @param userId
	 * @param taskid
	 * @param valueMap
	 * @param containerid
	 * @throws WorkflowServiceException
	 */
	public void completeHumanTask(String userId, String taskid, HashMap<String, Object> valueMap, String containerid,
			String authString) throws WorkflowServiceException; // IJTI-1531
	
	/**
	 * @param referenceNumber
	 * 						-referenceNumber
	 * @throws WorkflowServiceException
	 * 						- The WorkflowServiceException
	 */
	public void startNBWorkflow(String referenceNumber) throws WorkflowServiceException;
	
}
