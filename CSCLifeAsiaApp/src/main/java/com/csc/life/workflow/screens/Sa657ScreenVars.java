package com.csc.life.workflow.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sa657ScreenVars.
 * 
 * @version 1.0 generated on 18/09/19 06:52
 * @author arichharia2
 */
public class Sa657ScreenVars extends SmartVarModel {

	private static final long serialVersionUID = 1L;
	public FixedLengthStringData dataArea = new FixedLengthStringData(726);
	public FixedLengthStringData dataFields = new FixedLengthStringData(630).isAPartOf(dataArea, 0);

	public FixedLengthStringData jbpmUWdecision = DD.jbpmUWdecision.copy().isAPartOf(dataFields, 0);
	public FixedLengthStringData jbpmtaskId = DD.jbpmtaskId.copy().isAPartOf(dataFields, 10);
	public FixedLengthStringData jbpminstanceurl = DD.jbpminstanceurl.copy().isAPartOf(dataFields, 20);
	public FixedLengthStringData jbpmContainerId = DD.jbpmContainerId.copy().isAPartOf(dataFields, 220);
	public FixedLengthStringData jbpmprocessinstanceID = DD.jbpmprocessinstanceID.copy().isAPartOf(dataFields, 420);
	public FixedLengthStringData authStringForJBPM = DD.authStringForJBPM.copy().isAPartOf(dataFields, 430);

	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 630);

	public FixedLengthStringData jbpmUWdecisionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData jbpminstanceurlErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	public FixedLengthStringData jbpmtaskIdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData jbpmContainerIdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	public FixedLengthStringData jbpmprocessinstanceIDErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData authStringForJBPMErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 20);

	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 654);

	public FixedLengthStringData[] jbpmUWdecisionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	public FixedLengthStringData[] jbpminstanceurlOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] jbpmtaskIdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	public FixedLengthStringData[] jbpmContainerIdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] jbpmprocessinstanceIDOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] authStringForJBPMOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);

	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	public FixedLengthStringData screenLabel = new FixedLengthStringData(100);

	public LongData Sa657screenWritten = new LongData(0);
	public LongData Sa657protectWritten = new LongData(0);

	/**
	 * Constructor of Sa657ScreenVars.
	 */
	public Sa657ScreenVars() {
		super();
		init();
	}

	/**
	 * Initialize Screen Variables.
	 */
	private void init() {
		initialiseScreenVars();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.smart400framework.SmartVarModel#initialiseScreenVars()
	 * method to initialise ScreenVars variables.
	 */
	@Override
	protected void initialiseScreenVars() {
		screenFields = new BaseData[] { jbpmtaskId, jbpmUWdecision, jbpminstanceurl, jbpmContainerId,
				jbpmprocessinstanceID, authStringForJBPM };
		screenOutFields = new BaseData[][] { jbpmtaskIdOut, jbpmUWdecisionOut, jbpminstanceurlOut, jbpmContainerIdOut,
				jbpmprocessinstanceIDOut, authStringForJBPMOut };
		screenErrFields = new BaseData[] { jbpmtaskIdErr, jbpmUWdecisionErr, jbpminstanceurlErr, jbpmContainerIdErr,
				jbpmprocessinstanceIDErr, authStringForJBPMErr };
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		fieldIndMap.put(jbpmUWdecisionOut,
				new String[] { "01", null, "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(jbpminstanceurlOut,
				new String[] { "03", null, "-03", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(jbpmtaskIdOut,
				new String[] { "05", null, "-05", null, null, null, null, null, null, null, null, null });

		screenDateDispFields = new BaseData[] { effdateDisp };
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa657screen.class;
		errorInds = errorIndicators;
		protectRecord = Sa657protect.class;
	}
}
