/*
 * Copyright (2019) DXC Technology, all rights reserved.
 */
package com.csc.life.workflow.programs;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.csc.exceptions.WorkflowServiceException;
import com.csc.ldap.LdapDao;
import com.csc.life.workflow.LifeWorkflowClient;
import com.csc.life.workflow.config.IntegralLifeWorkflowProperties;
import com.csc.life.workflow.impl.LifeWorkflowClientJbpmImpl;
import com.csc.life.workflow.screens.Sa651ScreenVars;
import com.csc.life.workflow.screens.Sa651screensfl;

import com.csc.smart.procedures.Sanctn;
import com.csc.smart.recordstructures.Errmesgrec;
import com.csc.smart.recordstructures.Sanctnrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.recordstructures.Wsspsmart;
import com.csc.smart.utils.UserMapingUtils;

import com.csc.smart400framework.parent.ScreenProgCS;
import com.csc.smart400framework.parent.ScreenProgram;
import com.csc.smart400framework.procedures.Errmesg;
import com.csc.smart400framework.utility.Datcon1;


import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.screenmodel.ScreenModel;

import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.dao.DescDAO;

import com.csc.smart400framework.dataaccess.model.Descpf;

/**
 * This program represents Underwriting Approval Human Task.
 * 
 * @author igarg2
 *
 */
public class Pa651 extends ScreenProgCS {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final Logger LOGGER = LoggerFactory.getLogger(Pa651.class);
	private static final String SCREEN = "Sa651";
	
	private DescDAO descDAO = getApplicationContext().getBean("descDAO", DescDAO.class);

	private Map<String, Descpf> fupMap = null;
	private WsaaUserFieldsInner wsaaUserFieldsInner = new WsaaUserFieldsInner();
	private FixedLengthStringData wsaaOptionCode = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaFoundSelection = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("Pa651");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	
	private FixedLengthStringData wsspFiller = new FixedLengthStringData(768);
	private Sanctnrec sanctnrec = new Sanctnrec();
	private Sa651ScreenVars sv = ScreenProgram.getScreenVars(Sa651ScreenVars.class);

	private FixedLengthStringData wsaaPassword = new FixedLengthStringData(6);
	private Wsspsmart wsspsmart = new Wsspsmart();
	private FixedLengthStringData filler = new FixedLengthStringData(767).isAPartOf(wsspsmart.userArea, 0,
			FILLER_REDEFINE);
	private FixedLengthStringData wsspTaskid = new FixedLengthStringData(200).isAPartOf(filler, 0);
	private FixedLengthStringData wsspcontainerid = new FixedLengthStringData(200).isAPartOf(filler, 200);
	private FixedLengthStringData wsspProcessInstanceid = new FixedLengthStringData(10).isAPartOf(filler, 550);
	private LifeWorkflowClient lcil = getApplicationContext().getBean("lifefWorkflowClient", LifeWorkflowClient.class);
	
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private String remarks = "";
	private SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");
	private String authString="";     //IJTI-1531
	private LdapDao ldap = new LdapDao(); //IJTI-1531
	private Errmesgrec errmesgrec = new Errmesgrec();

	public Pa651() {
		super();
		screenVars = sv;
		new ScreenModel(SCREEN, AppVars.getInstance(), sv);
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	@Override
	public void mainline(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspsmart.userArea = convertAndSetParam(wsspsmart.userArea, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		super.mainline();
	}

	@Override
	public void processBo(Object... parmArray) {
		sv.dataArea = convertAndSetParam(sv.dataArea, parmArray, 3);
		scrnparams.screenParams = convertAndSetParam(scrnparams.screenParams, parmArray, 2);
		wsspFiller = convertAndSetParam(wsspFiller, parmArray, 1);
		wsspcomn.commonArea = convertAndSetParam(wsspcomn.commonArea, parmArray, 0);
		processBoMainline(sv, sv.dataArea, parmArray);

	}

	/**
	 * Initializes the screen.
	 * 
	 */
	@Override
	protected void initialise1000() {
		Map<String, String> formData = null;
		sv.language.set(wsspcomn.language);
		sv.dataArea.set(SPACES);
		sv.subfileArea.set(SPACES);
		wsaaOptionCode.set(SPACES);
		wsaaFoundSelection.set("N");
		scrnparams.function.set(Varcom.sclr);

		fupMap = descDAO.getItems("IT", wsspcomn.company.toString(), "T5661", wsspcomn.language.toString());

		processScreen(SCREEN, sv);
		if (!scrnparams.statuz.equals(Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		scrnparams.subfileRrn.set(1);
		wsaaUserFieldsInner.wsaaFirstDisplay.set("Y");
		wsaaUserFieldsInner.wsaaCount.set(0);
		while (wsaaUserFieldsInner.wsaaCount.toInt() != sv.subfilePage.toInt()) {
			loadSubfile1300();

		}

		String processid = wsspProcessInstanceid.getFormData().trim();
		String containerId = IntegralLifeWorkflowProperties.getUWWorkflowDeploymentId();
		String userId = UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString()).toLowerCase(appVars.getLocale())
				.trim();
		
		// IJTI-1531 Start
		String pass = ldap.getLdapUserPwd(userId, appVars);

		authString = userId + ":" + pass;

		// IJTI-1531 End

		String authStringEnc = "\"Basic " + new String(
				Base64.getEncoder().encode(authString.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8) + "\"";
		try {
			formData = lcil.getTaskFormValues(userId, processid, containerId, SCREEN, authString); //IJTI-1531
		} catch (WorkflowServiceException e) {
			/*IBPTE-1239*/
			LOGGER.error(getMessage("RUQ6", "Exception while fetching data for CIL workflow "), e);
			scrnparams.errorCode.set("RUQ6");
			return;
			/* IBPTE-1239 */
		}

		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}

		sanctnrec.function.set("USRNO");
		sanctnrec.userid.set(wsspcomn.userid);
		if (formData != null) {
			Double tsar = 0.0;
			Double lifeIncome=0.0;
			if (formData.get(LifeWorkflowClientJbpmImpl.TSAR) != null
					&& !"".equalsIgnoreCase(formData.get(LifeWorkflowClientJbpmImpl.TSAR))) {
				tsar = Double.valueOf(formData.get(LifeWorkflowClientJbpmImpl.TSAR));
			}
			if (formData.get(LifeWorkflowClientJbpmImpl.LASSRDINC) != null
					&& !"".equalsIgnoreCase(formData.get(LifeWorkflowClientJbpmImpl.LASSRDINC))) {
				lifeIncome = Double.valueOf(formData.get(LifeWorkflowClientJbpmImpl.LASSRDINC));
			}

			sv.huwdcdte.set(formData.get(LifeWorkflowClientJbpmImpl.UWDECISIONDATE));
			sv.isUSTaxPayor.set(formData.get(LifeWorkflowClientJbpmImpl.ISUSTAXPAYOR));
			sv.lifeAssuredIncome.set(lifeIncome);
			sv.occupClass.set(formData.get(LifeWorkflowClientJbpmImpl.OCCPCLASS));
			sv.riskIndicator.set(formData.get(LifeWorkflowClientJbpmImpl.RISKINDICATOR));
			sv.uwDecision.set(formData.get(LifeWorkflowClientJbpmImpl.UWDECISION));
			sv.policyNum.set(formData.get(LifeWorkflowClientJbpmImpl.POLICY_NUMBER));
			sv.lifeAssuredNum.set(formData.get(LifeWorkflowClientJbpmImpl.LASSRDNUM));
			sv.lifeAssuredName.set(formData.get(LifeWorkflowClientJbpmImpl.LASSRDNME));
			sv.lifeAssuredBMI.set(formData.get(LifeWorkflowClientJbpmImpl.LASSRDBMI));
			sv.lifeAssuredAge.set(formData.get(LifeWorkflowClientJbpmImpl.LASSRDAGE));
			sv.cmncDate.set(formData.get(LifeWorkflowClientJbpmImpl.POLICYCMDATE));
			sv.tsar.set(tsar);
			

		}

		sv.authStringForJBPM.set(authStringEnc);
		sv.jbpmContainerId.set(wsspcontainerid);
		sv.jbpmprocessinstanceID.set(wsspProcessInstanceid);
		wsaaPassword.set(sanctnrec.password.getData());

	}

	@Override
	protected void preScreenEdit() {
		// empty method required for framework
	}

	/**
	 * Validates and displays the screen.
	 */
	@Override
	protected void screenEdit2000() {
		// IBPTE-1239 starts
		if (isNE(scrnparams.errorCode, SPACES)) {
			wsspcomn.edterror.set("Y");
		}
		// IBPTE-1239 ends
		else {
			wsspcomn.edterror.set(Varcom.oK);
			if (isEQ(scrnparams.statuz, Varcom.calc)) {
				wsspcomn.edterror.set("Y");

			}

			scrnparams.subfileRrn.set(1);

			Sa651screensfl.set1stScreenRow(sv.sa651screensfl, appVars, sv);

			showSflScreen();

			// validation to check contract commencement date with business date.

			if (isEQ(wsaaToday, 0)) {
				datcon1rec.datcon1Rec.set(SPACES);
				datcon1rec.function.set(Varcom.tday);
				Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
				wsaaToday.set(datcon1rec.intDate);

			}

			String cmncDate = sv.cmncDate.getFormData().trim();

			if (!compareDates(wsaaToday.getFormData().trim(), cmncDate)) {
				scrnparams.statuz.set("H359");
				wsspcomn.edterror.set("Y");
				sv.cmncDateErr.set("H359");

				return;

			}

			sanctnrec.userid.set(wsspcomn.userid);
			callProgram(Sanctn.class, wsspcomn.commonArea, sanctnrec.sanctnRec);
			if (isNE(sanctnrec.statuz, Varcom.oK)) {
				syserrrec.params.set(sanctnrec.sanctnRec);
				syserrrec.subrname.set("SANCTN");
				syserrrec.statuz.set(Varcom.bomb);
				fatalError600();
			}

			if (isEQ(sv.huwdcdte, SPACES) || isEQ(sv.huwdcdte, "99999999")) {
				sv.huwdcdteErr.set("E186");
				wsspcomn.edterror.set("E186");
			}

		}

	}
	private void showSflScreen()
	{
	
		while (Sa651screensfl.hasMoreScreenRows(sv.sa651screensfl)) {
			String data = "";
			String fupcdes = sv.fupcdes.getFormData().trim();
			if (!"".equalsIgnoreCase(fupcdes) && fupMap.get(wsspcomn.language + fupcdes) != null) {

				data = fupMap.get(wsspcomn.language + sv.fupcdes.getFormData().trim()).getLongdesc();
			}

			sv.fupremk.set(data);
			updatesubfile();
			Sa651screensfl.setNextScreenRow(sv.sa651screensfl, appVars, sv);

		}
	}

	@Override
	protected void update3000() {
		wsaaPassword.set(sanctnrec.password.getData());
		String addInfoData = "";
		List<String> addInfoList = new ArrayList<>();

		try {

			Sa651screensfl.set1stScreenRow(sv.sa651screensfl, appVars, sv);
			int count = 1;

			while (Sa651screensfl.hasMoreScreenRows(sv.sa651screensfl)) {
				addInfoData = sv.fupcdes.getFormData().trim();
				count = count + 1;
				if (!"".equalsIgnoreCase(addInfoData)) {
					addInfoList.add(addInfoData + "-" + fupMap.get(wsspcomn.language + addInfoData).getLongdesc());
				}
				Sa651screensfl.setNextScreenRow(sv.sa651screensfl, appVars, sv);
			}

			String userid = UserMapingUtils.getLongUserIdValue(wsspcomn.userid.toString())
					.toLowerCase(appVars.getLocale()).trim();

			HashMap<String, Object> inputMap = new HashMap<>();
			inputMap.put("uwDecision", sv.uwDecision.getFormData().trim());
			inputMap.put("uwDecisionDate", sv.huwdcdteDisp.toString().trim());
			inputMap.put("additionalFollowups", addInfoList);
			inputMap.put("userName", userid);
			inputMap.put("userPassword", userid);
			lcil.completeHumanTask(userid, wsspTaskid.toString().trim(), inputMap, wsspcontainerid.trim(), authString); // IJTI-1531

		} catch (Exception e) {
			/* IBPTE-1239 */
			LOGGER.error(getMessage("RUQ8", "Exception Occurred in completeHumanTask "), e);
			scrnparams.errorCode.set("RUQ8");
			wsspcomn.edterror.set("Y");
			/* IBPTE-1239 */
		}
	}

	private void updatesubfile() {
		scrnparams.function.set(Varcom.supd);
		processScreen(SCREEN, sv);
		if (isNE(scrnparams.statuz, Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
	}

	/**
	 * Where to go next.
	 */
	@Override
	protected void whereNext4000() {
		wsspcomn.nextprog.set("Pr2fw");
	}

	protected void loadSubfile1300() {
		load1310();
		addSubfileRecord1320();
	}

	protected void loadSubfileOnScreenEdit() {
		loadOnScreenEdit();
		updatesubfile();
	}

	protected void load1310() {
		wsaaUserFieldsInner.wsaaCount.add(1);
		/* Record belong to this contract or ENDP ? If not, empty line */
		/* on in the subfile page. */
		wsaaUserFieldsInner.wsaaFupno.add(1);
		sv.fupno.set(wsaaUserFieldsInner.wsaaFupno);
		sv.zitem.set(" ");
		sv.indic.set(" ");

		sv.language.set(wsspcomn.language);

	}

	protected void loadOnScreenEdit() {
		wsaaUserFieldsInner.wsaaCount.add(1);
		/* Record belong to this contract or ENDP ? If not, empty line */
		/* on in the subfile page. */
		wsaaUserFieldsInner.wsaaFupno.add(1);
		sv.fupno.set(wsaaUserFieldsInner.wsaaFupno);
		sv.zitem.set(" ");
		sv.indic.set(" ");
		sv.language.set(wsspcomn.language);
		Descpf fupValue = fupMap.get(sv.fupcdes.getFormData().trim());
		
		if (fupValue != null) {
			remarks = fupMap.get(sv.fupcdes.getFormData().trim()).getLongdesc();
			sv.fupremk.set(remarks);

		}
	}

	protected void addSubfileRecord1320() {

		scrnparams.function.set(Varcom.sadd);
		processScreen(SCREEN, sv);
		if (!scrnparams.statuz.equals(Varcom.oK)) {
			syserrrec.statuz.set(scrnparams.statuz);
			fatalError600();
		}
		/* EXIT */
	}

	private static final class WsaaUserFieldsInner {
		/* WSAA-USER-FIELDS */

		private FixedLengthStringData wsaaFirstDisplay = new FixedLengthStringData(1);
		private ZonedDecimalData wsaaCount = new ZonedDecimalData(5, 0).setUnsigned();

		private PackedDecimalData wsaaFupno = new PackedDecimalData(2, 0).init(0);

	}

	private boolean compareDates(String date1, String date2) {

		boolean flag = true;
		if (!"".equalsIgnoreCase(date1) && !"".equalsIgnoreCase(date2)) {
			Date d1 = null;
			try {
				d1 = sdfo.parse(date1.substring(0, 4) + "-" + date1.substring(4, 6) + "-" + date1.subSequence(6, 8));
			} catch (ParseException e) {
				LOGGER.error("Exception Occurred in compareDates ", e);
			}
			Date d2 = null;
			try {
				d2 = sdfo.parse(date2.substring(0, 4) + "-" + date2.substring(4, 6) + "-" + date2.subSequence(6, 8));
			} catch (ParseException e) {
				LOGGER.error("Exception Occurred in compareDates ", e);
			}
			if (d1 != null && d2 != null && d1.before(d2)) {
				flag = false;
			}

		}

		return flag;
	}
	/* IBPTE-1239 START */
	private String getMessage(String errorCode, String errorMessage) {
		errmesgrec.eror.set(errorCode);
		errmesgrec.language.set(scrnparams.language);
		errmesgrec.erorProg.set(wsaaScreen);
		errmesgrec.company.set(scrnparams.company);
		errmesgrec.function.clear();
		callProgram(Errmesg.class, errmesgrec.errmesgRec);
		if (isEQ(errmesgrec.statuz, "BOMB")) {
			scrnparams.statuz.set(errmesgrec.statuz);
			screenErrors200();
		}
		return errorMessage + ". " + errmesgrec.errorline.trim();
	}
	/* IBPTE-1239 END */
}
