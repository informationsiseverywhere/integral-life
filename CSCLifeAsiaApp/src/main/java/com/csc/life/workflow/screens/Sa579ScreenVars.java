package com.csc.life.workflow.screens;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;

import com.csc.common.DD;
import com.csc.smart400framework.SmartVarModel;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.LongData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Screen variables for Sa579
 * @version 1.0 generated on 30/08/09 06:52
 * @author Quipoz
 */
public class Sa579ScreenVars extends SmartVarModel { 

	private static final long serialVersionUID = 1;
	public FixedLengthStringData dataArea = new FixedLengthStringData(549);
	public FixedLengthStringData dataFields = new FixedLengthStringData(438).isAPartOf(dataArea, 0);

	public FixedLengthStringData jbpmUWdecision = DD.jbpmUWdecision.copy().isAPartOf(dataFields,0);
	public FixedLengthStringData jbpmtaskId = DD.jbpmtaskId.copy().isAPartOf(dataFields,10);
	public ZonedDecimalData effdate = DD.effdate.copyToZonedDecimal().isAPartOf(dataFields,20);
	// IJTI-1090 starts
	public FixedLengthStringData jbpmContainerId = DD.jbpmContainerId.copy().isAPartOf(dataFields,28);
	public FixedLengthStringData jbpmprocessinstanceID = DD.jbpmprocessinstanceID.copy().isAPartOf(dataFields,228);   
	// IJTI-1090 ends
	public FixedLengthStringData authStringForJBPM	= DD.authStringForJBPM.copy().isAPartOf(dataFields,238); //IJTI-1131
	
	public FixedLengthStringData errorIndicators = new FixedLengthStringData(24).isAPartOf(dataArea, 438);
	public FixedLengthStringData effdateErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 0);
	public FixedLengthStringData jbpmUWdecisionErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 4);
	// IJTI-1090 starts
	public FixedLengthStringData jbpmContainerIdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 8);
	public FixedLengthStringData jbpmprocessinstanceIDErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 12);
	// IJTI-1090 ends
	public FixedLengthStringData jbpmtaskIdErr = new FixedLengthStringData(4).isAPartOf(errorIndicators, 16);
	public FixedLengthStringData authStringForJBPMErr	= new FixedLengthStringData(4).isAPartOf(errorIndicators, 20); //IJTI-1131
	public FixedLengthStringData outputIndicators = new FixedLengthStringData(72).isAPartOf(dataArea, 462);
	
	public FixedLengthStringData[] jbpmUWdecisionOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 0);
	// IJTI-1090 starts
	public FixedLengthStringData[] jbpmContainerIdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 12);
	public FixedLengthStringData[] jbpmprocessinstanceIDOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 24);
	// IJTI-1090 ends
	public FixedLengthStringData[] jbpmtaskIdOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 36);
	public FixedLengthStringData[] effdateOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 48);
	public FixedLengthStringData[] authStringForJBPMOut = FLSArrayPartOfStructure(12, 1, outputIndicators, 60);
	
	
	public ZonedDecimalData screenRow = DD.row.copyToZonedDecimal();
	public ZonedDecimalData screenColumn = DD.column.copyToZonedDecimal();

	public FixedLengthStringData effdateDisp = new FixedLengthStringData(10);
	
	public LongData Sa579screenWritten = new LongData(0);
	public LongData Sa579protectWritten = new LongData(0);


	@Override
	public boolean hasSubfile() {
		return false;
	}


	public Sa579ScreenVars() {
		super();
		initialiseScreenVars();
	}


	@Override
	protected void initialiseScreenVars() {
		// IJTI-1090 starts
		screenFields = new BaseData[] { jbpmtaskId, jbpmUWdecision, jbpmContainerId, jbpmprocessinstanceID, effdate,authStringForJBPM };
		screenOutFields = new BaseData[][] { jbpmtaskIdOut, jbpmUWdecisionOut, jbpmContainerIdOut,
				jbpmprocessinstanceIDOut, effdateOut,authStringForJBPMOut };
		screenErrFields = new BaseData[] { jbpmtaskIdErr, jbpmUWdecisionErr, jbpmContainerIdErr,
				jbpmprocessinstanceIDErr, effdateErr,authStringForJBPMErr };
		// IJTI-1090 ends
		screenDateFields = new BaseData[] {};
		screenDateErrFields = new BaseData[] {};
		screenDateDispFields = new BaseData[] {};
		fieldIndMap.put(jbpmUWdecisionOut,
				new String[] { "01", null, "-01", null, null, null, null, null, null, null, null, null });
		fieldIndMap.put(jbpmContainerIdOut,
				new String[] { "03", null, "-03", null, null, null, null, null, null, null, null, null }); // IJTI-1090
		fieldIndMap.put(jbpmtaskIdOut,new String[] {"05",null, "-05",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(effdateOut,new String[] {"02",null, "-02",null, null, null, null, null, null, null, null, null});
		fieldIndMap.put(authStringForJBPMOut,new String[] {"06",null, "-06",null, null, null, null, null, null, null, null, null}); //IJTI-1131
		
		screenDateFields = new BaseData[] {effdate};
		screenDateErrFields = new BaseData[] {effdateErr};
		screenDateDispFields = new BaseData[] {effdateDisp};
		screenDataArea = dataArea;
		errorInds = errorIndicators;
		screenRecord = Sa579screen.class;
		errorInds = errorIndicators;	
		protectRecord = Sa579protect.class;
	}

}
