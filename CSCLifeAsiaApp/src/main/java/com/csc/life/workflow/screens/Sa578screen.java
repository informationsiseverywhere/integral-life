package com.csc.life.workflow.screens;

import com.quipoz.COBOLFramework.TableModel.ScreenRecord;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.util.QPUtilities;
import com.quipoz.framework.util.VarModel;

/**
 * Screen record for SCREEN
 * @version 1.0 generated on 30/08/09 05:43
 * @author Quipoz
 */
public class Sa578screen extends ScreenRecord { 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public static final boolean OVERLAY = false;
	public static final int[] pfInds = new int[] {4, 22, 17, 23, 18, 15, 6, 24, 16, 1, 2, 3, 21}; 
	public static RecInfo lrec = new RecInfo();
	static {
		lrec.recordName = ROUTINE + "Written";
		COBOLAppVars.recordSizes.put(ROUTINE, new int[] {1, 17, 2, 79}); 
	}

/**
 * Writes a record to the screen.
 * @param errorInd - will be set on if an error occurs
 * @param noRecordFoundInd - will be set on if no changed record is found
 */
	public static void write(COBOLAppVars av, VarModel pv,
		Indicator ind2, Indicator ind3) {
		Sa578ScreenVars sv = (Sa578ScreenVars) pv;
		clearInds(av, pfInds);
		write(lrec, sv.Sa578screenWritten, null, av, null, ind2, ind3);
	}

	public static void read(Indicator ind2, Indicator ind3) {
		//
	}

	public static void clearClassString(VarModel pv) {
		Sa578ScreenVars screenVars = (Sa578ScreenVars)pv;
		screenVars.screenRow.setClassString("");
		screenVars.screenColumn.setClassString("");
		screenVars.jbpmtaskId.setClassString("");
		// IJTI-1090 starts
		screenVars.jbpmContainerId.setClassString("");
		screenVars.jbpmprocessinstanceID.setClassString("");
		// IJTI-1090 ends
		screenVars.effdateDisp.setClassString("");
		screenVars.zdoctor.setClassString("");
		screenVars.authStringForJBPM.setClassString("");//IJTI-1131
		
	}

/**
 * Clear all the variables in Sa578screen
 */
	public static void clear(VarModel pv) {
		Sa578ScreenVars screenVars = (Sa578ScreenVars) pv;
		screenVars.screenRow.clear();
		screenVars.screenColumn.clear();
		screenVars.jbpmtaskId.clear();
		// IJTI-1090 starts
		screenVars.jbpmContainerId.clear();
		screenVars.jbpmprocessinstanceID.clear();
		// IJTI-1090 ends
		screenVars.effdateDisp.clear();
		screenVars.effdateDisp.clear();
		screenVars.effdate.clear();
		screenVars.zdoctor.clear();
		screenVars.authStringForJBPM.clear();//IJTI-1131
	}
}
