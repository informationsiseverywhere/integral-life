package com.csc.life.diary.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.diary.dataaccess.dao.Dry5134DAO;
import com.csc.life.diary.dataaccess.model.Dry5134Dto;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * DAO implementation to fetch records for pending auto increase.
 * 
 * @author hmahajan6
 *
 */
public class Dry5134DAOImpl extends BaseDAOImpl<Dry5134Dto> implements Dry5134DAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Dry5134DAOImpl.class);

	public Dry5134DAOImpl() {
		// constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.csc.diary.dataaccess.dao.Dry5134pfDAO#getPendingAutoIncDetails(java.lang.
	 * String, java.lang.String, java.lang.String)
	 */
	public List<Dry5134Dto> getPendingAutoIncDetails(String company, String effDate, String chdrNum) {
		List<Dry5134Dto> dry5134dtoList = new ArrayList<>();
		StringBuilder sql = new StringBuilder(
				"SELECT  T02.COWNNUM, T01.CHDRCOY, T01.CHDRNUM, T01.LIFE, T01.JLIFE, T01.COVERAGE,");
		sql.append(" T01.RIDER, T01.PLNSFX, T01.STATCODE, T01.PSTATCODE, T01.CRRCD, T01.PRMCUR, ");
		sql.append(" T01.CRTABLE, T01.RCESDTE, T01.SUMINS, T01.INSTPREM, T01.ZBINSTPREM, T01.ZLINSTPREM, ");
		sql.append(" T01.SINGP, T01.CPIDTE, T01.INDXIN, T01.MORTCLS, T01.USER_T, T01.VALIDFLAG FROM  COVRPF ");
		sql.append(" T01 INNER JOIN CHDRPF T02 ON T01.CHDRNUM = T02.CHDRNUM  AND T01.CHDRCOY = ? ");
		sql.append(" AND T01.CPIDTE <> ? AND T01.CPIDTE <= ? AND T01.INDXIN <> ? AND T01.CHDRNUM = ? ");
		sql.append(" AND T01.VALIDFLAG = ?  AND T02.VALIDFLAG = ? ORDER BY T02.COWNNUM, ");
		sql.append(" T01.CHDRCOY, T01.CHDRNUM, T01.LIFE, T01.COVERAGE, T01.RIDER, T01.PLNSFX");

		try (PreparedStatement ps = getPrepareStatement(sql.toString())) {

			ps.setString(1, company);
			ps.setString(2, "0");
			ps.setString(3, effDate);
			ps.setString(4, "P");
			ps.setString(5, chdrNum);
			ps.setString(6, "1");
			ps.setString(7, "1");
			try (ResultSet rs = ps.executeQuery()) {
				while (rs.next()) {
					Dry5134Dto dry5134dto = new Dry5134Dto();
					dry5134dto.setCownnum(rs.getString("COWNNUM"));
					dry5134dto.setChdrcoy(rs.getString("CHDRCOY"));
					dry5134dto.setChdrnum(rs.getString("CHDRNUM"));
					dry5134dto.setLife(rs.getString("LIFE"));
					dry5134dto.setJlife(rs.getString("JLIFE"));
					dry5134dto.setCoverage(rs.getString("COVERAGE"));
					dry5134dto.setRider(rs.getString("RIDER"));
					dry5134dto.setPlanSuffix(rs.getInt("PLNSFX"));
					dry5134dto.setStatcode(rs.getString("STATCODE"));
					dry5134dto.setPstatcode(rs.getString("PSTATCODE"));
					dry5134dto.setCrrcd(rs.getInt("CRRCD"));
					dry5134dto.setPremCurrency(rs.getString("PRMCUR"));
					dry5134dto.setCrtable(rs.getString("CRTABLE"));
					dry5134dto.setRiskCessDate(rs.getInt("RCESDTE"));
					dry5134dto.setSumins(rs.getBigDecimal("SUMINS"));
					dry5134dto.setInstprem(rs.getBigDecimal("INSTPREM"));
					dry5134dto.setZbinstprem(rs.getBigDecimal("ZBINSTPREM"));
					dry5134dto.setZlinstprem(rs.getBigDecimal("ZLINSTPREM"));
					dry5134dto.setSingp(rs.getBigDecimal("SINGP"));
					dry5134dto.setCpiDate(rs.getInt("CPIDTE"));
					dry5134dto.setIndexationInd(rs.getString("INDXIN"));
					dry5134dto.setMortcls(rs.getString("MORTCLS"));
					dry5134dto.setValidflag(rs.getString("VALIDFLAG"));

					dry5134dtoList.add(dry5134dto);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("Error occured while reading data for pending auto increase ", e);
			throw new SQLRuntimeException(e);
		}
		return dry5134dtoList;

	}

}
