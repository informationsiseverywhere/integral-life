/*
 * File: Dryovrdue.java
 * Date: December 3, 2013 2:25:28 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYOVRDUE.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.life.contractservicing.dataaccess.BfrqTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.terminationclaims.dataaccess.CnfsTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the transaction detail record change subroutine for
* overdue processing.
*
* The processing first checks to see if the contract has a
* billing channel that does not allow overdue (i.e. when
* billing channel is 'N' - no billing).  If this is the case then
* the DTRD-NO flag is set which will then remove the OVERDUE
* event for this contract.
*
* Next the processing reads table T5679 to determine if the
* contract still has a status that allows Overdue processing. If
* it does not meet this test then again the DTRD-NO flag is set
* and future events are cancelled.
*
* The next check is to ensure that an item exists on T6654.  If
* an item does not exist then this is a fatal error and all
* processing should cease with the contract remaining locked.
* Note the difference between a DATABASE-ERROR and SYSTEM-ERROR.
* A DATABASE-ERROR is an error that can has not been generated
* by a problem in the LIFE/400 application.  A SYSTEM-ERROR is
* an error generated because of an application problem, e.g. a
* table item is missing.
*
* The final check is to determine if overdue processing has been
* defined for this billing channel and contract type. This check
* is on the lag time in days field (three of them).  If all three
* are empty and the arrears lag time is empty then set the
* DTRD-NO flag to true as no OVERDUE processing should occur.
*
* If all the checks are passed then the overdue processing date
* needs to be found. The difference between the Paid To Date of
* the contract and the EFFECTIVE DATE of the event (not the Diary  DRYAP2
* submission date held in RUN-DATE) should yield the number of     DRYAP2
* the contract and the event's RUN-DATE should yield the no. of    DRYAP2
* days that the contract is overdue.  Using this number of days
* a check is made on the lag times from T6654 to determine which
* 'slot' we should use.
*
* If a next processing date has been calculated (using the lag
* times), a check is then made on the arrears processing supress
* date field.  If this date is greater than or equal to the date
* calculated then the next event should be scheduled for the day
* after the suppression date, otherwise simply the calculated
* date.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryovrdue extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYOVRDUE");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(2, 0).setUnsigned();
	private final int wsaaT5679Max = 12;

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");

	private FixedLengthStringData wsaaT6654Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key, 1);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaT6654Key, 4, FILLER).init(SPACES);
	private ZonedDecimalData wsaaT6654Sub = new ZonedDecimalData(2, 0).setUnsigned();
	private static final int wsaaT6654Max = 3;
	private String wsaaDaysFound = "";
	private ZonedDecimalData wsaaDaysOdue = new ZonedDecimalData(3, 0).setUnsigned();
	private PackedDecimalData wsaaAplsuptoPlus1 = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexPrem = new Validator(wsaaFlexPrem, "Y");
	private Validator nonFlexPrem = new Validator(wsaaFlexPrem, "N");
		/* ERRORS */
	private static final String f321 = "F321";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String cnfsrec = "CNFSREC";
	private static final String bfrqrec = "BFRQREC";
	private static final String chdrlifrec = "CHDRLIFREC";
		/* TABLES */
	private static final String t6654 = "T6654";
	private static final String t5679 = "T5679";
	private static final String t5729 = "T5729";
	private BfrqTableDAM bfrqIO = new BfrqTableDAM();
	private CnfsTableDAM cnfsIO = new CnfsTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private T6654rec t6654rec = new T6654rec();
	private T5679rec t5679rec = new T5679rec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Freqcpy freqcpy = new Freqcpy();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
	
	public Dryovrdue() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		if (isGTE(drypDryprcRecInner.drypPtdate, drypDryprcRecInner.drypRunDate)
		|| isEQ(drypDryprcRecInner.drypPtdate, drypDryprcRecInner.drypBtdate)) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		if (isEQ(drypDryprcRecInner.drypBillchnl, SPACES)
		|| isEQ(drypDryprcRecInner.drypBillchnl, "N")) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		/* Check the risk and premium status against T5679.*/
		validate100();
		if (invalidStatus.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		/* Check whether contract is Flexible Premium                      */
		nonFlexPrem.setTrue();
		checkT5729800();
		/*if (flexPrem.isTrue()) {
			invalidStatus.setTrue();
		}*/
		/* If Overdue Processing is suppressed, determine the date on      */
		/* which overdue procressing should resume (i.e. Suppression To    */
		/* Date Plus One Day).                                             */
		if (isNE(drypDryprcRecInner.drypAplsupto, ZERO)
		&& isNE(drypDryprcRecInner.drypAplsupto, varcom.vrcmMaxDate)) {
			datcon2rec.intDate1.set(drypDryprcRecInner.drypAplsupto);
			datcon2rec.frequency.set(freqcpy.daily);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				drylogrec.params.set(datcon2rec.datcon2Rec);
				drylogrec.statuz.set(datcon2rec.statuz);
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61 
			}
			wsaaAplsuptoPlus1.set(datcon2rec.intDate2);
		}
		else {
			wsaaAplsuptoPlus1.set(0);
		}
		/* If Overdue Processing is suppressed, set the Next Diary         */
		/* Processing Date to the date on which Overdue Processing should  */
		/* resume (i.e. Supression To Date Plus One Day).                  */
		if (isGTE(wsaaAplsuptoPlus1, drypDryprcRecInner.drypEffectiveDate)) {
			drypDryprcRecInner.drypNxtprcdate.set(wsaaAplsuptoPlus1);
			return ;
		}
		readT6654400();
		if (invalidStatus.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		calcOdueDays500();
		getNextDate600();
	}

protected void exit090()
	{
		exitProgram();
	}

protected void validate100()
	{
		vald110();
	}

protected void vald110()
	{
		/* Validate the Contract Risk and Premium status against T5679.*/
		readT5679200();
		validateStatus300();
		if (invalidStatus.isTrue()) {
			return ;
		}
		/* Check on the CNFS file to see whether policy has gone through   */
		/* the NFO Surrender process.                                      */
		cnfsIO.setParams(SPACES);
		cnfsIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		cnfsIO.setChdrnum(drypDryprcRecInner.drypEntity);
		cnfsIO.setFormat(cnfsrec);
		cnfsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, cnfsIO);
		/* Abort if a database error has been found                        */
		if (isNE(cnfsIO.getStatuz(), varcom.oK)
		&& isNE(cnfsIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(cnfsIO.getChdrnum());
			drylogrec.statuz.set(cnfsIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(cnfsIO.getStatuz(), varcom.mrnf)) {
			/*NEXT_SENTENCE*/
		}
		else {
			invalidStatus.setTrue();
			return ;
		}
		/* Check on the BFRQ file to see whether policy has gone through   */
		/* the NFO APL Billing Freq change.                                */
		bfrqIO.setParams(SPACES);
		bfrqIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		bfrqIO.setChdrnum(drypDryprcRecInner.drypEntity);
		bfrqIO.setEffdate(drypDryprcRecInner.drypRunDate);
		bfrqIO.setFormat(bfrqrec);
		bfrqIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, bfrqIO);
		if (isNE(bfrqIO.getStatuz(), varcom.oK)
		&& isNE(bfrqIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(bfrqIO.getChdrnum());
			drylogrec.statuz.set(bfrqIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(bfrqIO.getStatuz(), varcom.endp)
		|| isNE(bfrqIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(bfrqIO.getChdrnum(), drypDryprcRecInner.drypEntity)) {
			return ;
		}
		else {
			invalidStatus.setTrue();
		}
	}

protected void readT5679200()
	{
		t5679210();
	}

protected void t5679210()
	{
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void validateStatus300()
	{
		/*VALIDATE*/
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(drypDryprcRecInner.drypStatcode, t5679rec.cnRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(drypDryprcRecInner.drypPstatcode, t5679rec.cnPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}

protected void readT6654400()
	{
		read410();
	}

protected void read410()
	{
		/* Read T6654 to see if overdue processing should be*/
		/* triggered.*/
	    readChdr();
		String key;
		if(BTPRO028Permission) {
			key = drypDryprcRecInner.drypBillchnl.toString().trim() + drypDryprcRecInner.drypCnttype.toString().trim() + chdrlifIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = drypDryprcRecInner.drypBillchnl.toString().trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = drypDryprcRecInner.drypBillchnl.toString().trim().concat("*****");
					if(!readT6654(key)) {
						drylogrec.statuz.set(itemIO.getStatuz());
						drylogrec.params.set(itemIO.getParams());
						drylogrec.dryDatabaseError.setTrue();
						a000FatalError();
					}
				}
			}
		}
		wsaaT6654Cnttype.set(drypDryprcRecInner.drypCnttype);
		wsaaT6654Billchnl.set(drypDryprcRecInner.drypBillchnl);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t6654);
		itemIO.setItemitem(wsaaT6654Key);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaT6654Cnttype.set("***");
			itemIO.setItemitem(wsaaT6654Key);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)
			&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
				drylogrec.params.set(itemIO.getParams());
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61 
			}
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			invalidStatus.setTrue();
		}
		else {
			t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
	itempf.setItemtabl("t6654");
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void readChdr() {
	chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
	chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
	chdrlifIO.setFunction(varcom.readr);
	chdrlifIO.setFormat(chdrlifrec);
	SmartFileCode.execute(appVars, chdrlifIO);
	if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
		drylogrec.statuz.set(chdrlifIO.getStatuz());
		drylogrec.params.set(chdrlifIO.getParams());
		drylogrec.dryDatabaseError.setTrue();
		a000FatalError();
	}
}

protected void calcOdueDays500()
	{
		calc510();
	}

protected void calc510()
	{
		/* Calculate the number of days overdue..*/
		wsaaDaysOdue.set(ZERO);
		datcon3rec.intDate1.set(drypDryprcRecInner.drypPtdate);
		datcon3rec.intDate2.set(drypDryprcRecInner.drypEffectiveDate);
		/*    MOVE DRYP-EFFECTIVE-DATE    TO DTC3-INT-DATE-2.              */
		/* IF DRYP-APLSUPTO           >= DRYP-RUN-DATE          <LA5080>*/
		/*    MOVE DRYP-RUN-DATE       TO DTC2-INT-DATE-1       <LA5080>*/
		/*    MOVE FREQ-DAILY          TO DTC2-FREQUENCY        <LA5080>*/
		/*    MOVE 1                   TO DTC2-FREQ-FACTOR      <LA5080>*/
		/*                                                      <LA5080>*/
		/*    CALL 'DATCON2'        USING DTC2-DATCON2-REC      <LA5080>*/
		/*                                                      <LA5080>*/
		/*    IF DTC2-STATUZ       NOT = O-K                    <LA5080>*/
		/*       MOVE DTC2-DATCON2-REC TO DRYL-PARAMS           <LA5080>*/
		/*       MOVE DTC2-STATUZ      TO DRYL-STATUZ           <LA5080>*/
		/*       SET DRY-DATABASE-ERROR TO TRUE                 <LA5080>*/
		/*       PERFORM A000-FATAL-ERROR                       <LA5080>*/
		/*    END-IF                                            <LA5080>*/
		/*    MOVE DTC2-INT-DATE-2     TO DTC3-INT-DATE-2       <LA5080>*/
		/* ELSE                                                 <LA5080>*/
		/*    MOVE DRYP-RUN-DATE       TO DTC3-INT-DATE-2       <LA5080>*/
		/* END-IF.                                              <LA5080>*/
		datcon3rec.frequency.set(freqcpy.daily);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		wsaaDaysOdue.set(datcon3rec.freqFactor);
	}

protected void getNextDate600()
	{
		get610();
	}

protected void get610()
	{
		wsaaDaysFound = "N";
		/* Check the days set on the T6654 item.*/
		for (wsaaT6654Sub.set(1); !(isGT(wsaaT6654Sub, wsaaT6654Max)
		|| isEQ(wsaaDaysFound, "Y")); wsaaT6654Sub.add(1)){
			if (isNE(t6654rec.daexpy[wsaaT6654Sub.toInt()], 0)
			&& isNE(t6654rec.daexpy[wsaaT6654Sub.toInt()], 999)) {
				datcon2rec.freqFactor.set(t6654rec.daexpy[wsaaT6654Sub.toInt()]);
				wsaaDaysFound = "Y";
			}
		}
		if (isGT(wsaaT6654Sub, wsaaT6654Max)
		&& isEQ(wsaaDaysFound, SPACES)
		&& isEQ(t6654rec.nonForfietureDays, ZERO)) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		/* No. of overdue days found on T6654.will determine which*/
		/* line to use.*/
		/*    IF  WSAA-DAYS-ODUE            < T6654-DAEXPY-01              */
		if (isLTE(wsaaDaysOdue, t6654rec.daexpy01)
		&& isGT(t6654rec.daexpy01, 0)
		&& isNE(t6654rec.doctid01, SPACES)) {
			datcon2rec.freqFactor.set(t6654rec.daexpy01);
			setDate700();
			return ;
		}
		/*    IF WSAA-DAYS-ODUE           >= T6654-DAEXPY-01               */
		/*    AND WSAA-DAYS-ODUE           < T6654-DAEXPY-02               */
		if (isGT(wsaaDaysOdue, t6654rec.daexpy01)
		&& isLTE(wsaaDaysOdue, t6654rec.daexpy02)) {
			if (isGT(t6654rec.daexpy02, 0)
			&& isNE(t6654rec.doctid02, SPACES)) {
				datcon2rec.freqFactor.set(t6654rec.daexpy02);
				setDate700();
				return ;
			}
		}
		/*    IF WSAA-DAYS-ODUE           >= T6654-DAEXPY-02               */
		/*    AND WSAA-DAYS-ODUE           < T6654-DAEXPY-03               */
		if (isGT(wsaaDaysOdue, t6654rec.daexpy02)
		&& isLTE(wsaaDaysOdue, t6654rec.daexpy03)) {
			if (isGT(t6654rec.daexpy03, 0)
			&& isNE(t6654rec.doctid03, SPACES)) {
				datcon2rec.freqFactor.set(t6654rec.daexpy03);
				setDate700();
				return ;
			}
		}
		/*    IF  WSAA-DAYS-ODUE           >= T6654-DAEXPY-03              */
		/*    AND WSAA-DAYS-ODUE           <  T6654-NON-FORFIETURE-DAYS    */
		if (isGT(wsaaDaysOdue, t6654rec.daexpy03)
		&& isLTE(wsaaDaysOdue, t6654rec.nonForfietureDays)) {
			if (isGT(t6654rec.nonForfietureDays, 0)) {
				datcon2rec.freqFactor.set(t6654rec.nonForfietureDays);
				setDate700();
				return ;
			}
		}
		if (isGT(t6654rec.nonForfietureDays, 0)) {
			datcon2rec.freqFactor.set(t6654rec.nonForfietureDays);
			setDate700();
		}
	}

protected void setDate700()
	{
		/*DATE*/
		/* Set the processing date by incrementing PAID TO DATE*/
		/* by the number of days on the T6654.*/
		datcon2rec.intDate1.set(drypDryprcRecInner.drypPtdate);
		datcon2rec.intDate2.set(0);
		datcon2rec.frequency.set(freqcpy.daily);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		drypDryprcRecInner.drypNxtprcdate.set(datcon2rec.intDate2);
		/*EXIT*/
	}

protected void checkT5729800()
	{
		check810();
	}

protected void check810()
	{
		wsaaFlexPrem.set("N");
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(drypDryprcRecInner.drypCnttype);
		itdmIO.setItmfrm(drypDryprcRecInner.drypOccdate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5729)
		|| isNE(itdmIO.getItemitem(), drypDryprcRecInner.drypCnttype)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isLT(itdmIO.getItmto(), drypDryprcRecInner.drypOccdate)) {
			return ;
		}
		flexPrem.setTrue();
	}
//Modify for ILPI-61
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
