/*
 * File: Drydivalc.java
 * Date: December 3, 2013 2:24:28 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYDIVALC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* This is the transaction detail record update subroutine for
* Cash Dividend Allocation process.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Drydivalc extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYDIVALC");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("02");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	private FixedLengthStringData wsaaEof = new FixedLengthStringData(1).init("N");
	private Validator eof = new Validator(wsaaEof, "Y");
	private PackedDecimalData wsaaT5679Ix = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private PackedDecimalData wsaaNewBonusDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaAllocDate = new PackedDecimalData(8, 0);
		/* FORMATS */
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String itemrec = "ITEMREC   ";
		/* TABLES */
	private static final String t5679 = "T5679";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private Datcon2rec datcon2rec = new Datcon2rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private T5679rec t5679rec = new T5679rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private List<Covrpf> covrpfList;
	private Covrpf covrpfIO;
	private int covrpfCount = 0;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr280, 
		exit290
	}

	public Drydivalc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdNo.setTrue();
		wsaaAllocDate.set(varcom.vrcmMaxDate);
		/*  Validate contract status.*/
		validateContract100();
		if (!validContract.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		/*  Read the COVRBON logical to get the earliest next capitalisatio*/
		/*  date.*/
	//covrbonIO.setParams(SPACES);
	//	covrbonIO.setChdrcoy(drypDryprcRecInner.drypCompany);
	//	covrbonIO.setChdrnum(drypDryprcRecInner.drypEntity);
	//	covrbonIO.setPlanSuffix(ZERO);
	//	covrbonIO.setTranno(ZERO);
	//	covrbonIO.setFormat(covrbonrec);
	//	covrbonIO.setFunction(varcom.begn);
	//	covrbonIO.setStatuz(varcom.oK);
		Covrpf covrData = new Covrpf();		
		CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO",CovrpfDAO.class);		
		covrData.setChdrcoy(drypDryprcRecInner.drypCompany.toString());
		covrData.setChdrnum(drypDryprcRecInner.drypEntity.toString());
		covrpfList = covrpfDAO.getCovrbonRecord(covrData);
		if(isEQ(covrpfList.size(),ZERO))
		{
			wsaaEof.set("Y");
		}
		while ( !(isEQ(wsaaEof, "Y"))) {	
			   if(covrpfList.size()>covrpfCount)
			{
				covrpfIO=covrpfList.get(covrpfCount);
			validateCoverage400();
		}
		   }
		
		if (!validCoverage.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		if (isGT(wsaaAllocDate, ZERO)
		&& isLT(wsaaAllocDate, varcom.vrcmMaxDate)) {
			drypDryprcRecInner.dtrdYes.setTrue();
			drypDryprcRecInner.drypNxtprcdate.set(wsaaAllocDate);
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		}
		else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void validateContract100()
	{
		start110();
	}

protected void start110()
	{
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		/*  Read T5679 for valid status requirements for transactions.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypSystParm[1]);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/*  Validate the contract status against T5679.*/
		wsaaValidContract.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
		|| validContract.isTrue()); wsaaT5679Ix.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
				|| validContract.isTrue()); wsaaT5679Ix.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Ix.toInt()], chdrlifIO.getPstatcode())) {
						wsaaValidContract.set("Y");
					}
				}
			}
		}
	}

protected void validateCoverage400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read210();
				case nextr280: 
					nextr280();
				case exit290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read210()
	{
		//SmartFileCode.execute(appVars, covrbonIO);
		//if (isNE(covrbonIO.getStatuz(), varcom.oK)
		//&& isNE(covrbonIO.getStatuz(), varcom.endp)) {
		//	drylogrec.params.set(covrbonIO.getParams());
		//	drylogrec.statuz.set(covrbonIO.getStatuz());
		//	drylogrec.dryDatabaseError.setTrue();
		//	a000FatalError();//ILPI-61 
		//}
		if (isNE(covrpfIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(covrpfIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| (covrpfList.size() <= covrpfCount))
		//|| isEQ(covrbonIO.getStatuz(), varcom.endp)
			//covrbonIO.setStatuz(varcom.endp);
		    {
			wsaaEof.set("Y");
			goTo(GotoLabel.exit290);
		}
		/*  Validate the coverage status against T5679.*/
		wsaaValidCoverage.set("N");
		wsaaStatcode.set(covrpfIO.getStatcode());
		wsaaPstcde.set(covrpfIO.getPstatcode());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
			validateCovrStatus500();
		}
		if (!validCoverage.isTrue()) {
			goTo(GotoLabel.nextr280);
		}
		if (isNE(covrpfIO.getBonusInd(), "D")) {
			goTo(GotoLabel.nextr280);
		}
		datcon2rec.intDate1.set(covrpfIO.getUnitStatementDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaNewBonusDate.set(datcon2rec.intDate2);
		if (isLT(wsaaNewBonusDate, wsaaAllocDate)) {
			wsaaAllocDate.set(wsaaNewBonusDate);
		}
	}

protected void nextr280()
	{
		//covrbonIO.setFunction(varcom.nextr);
	     covrpfCount++;
	     if(covrpfList.size() <= covrpfCount)
			    {
				wsaaEof.set("Y");
	}
	}

protected void validateCovrStatus500()
	{
		/*START*/
		/*  Validate coverage risk status*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				validateCovrPremStatus600();
			}
		}
		/*EXIT*/
	}

protected void validateCovrPremStatus600()
	{
		/*START*/
		/*  Validate coverage premium status*/
		if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], wsaaPstcde)) {
			wsaaSub.set(13);
			validCoverage.setTrue();
		}
		/*EXIT*/
	}
//Modify for ILPI-61
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	//cluster support by vhukumagrawa
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	//ILPI-97 ends
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
