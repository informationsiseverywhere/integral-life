package com.csc.life.diary.dataaccess.model;

import com.csc.life.productdefinition.dataaccess.model.Covrpf;

/**
 * Dryr619Dto - POJO For Anniversary Renewals Processing
 * 
 * @author ptrivedi8
 *
 */
public class Dryr619Dto {

	private int bcesDte;
	private int cbanpr;
	private String chdrCoy;
	private String chdrNum;
	private String coverage;
	private Covrpf covrpf;
	private int crrcd;
	private String crTable;
	private int currFrom;
	private int currTo;
	private String life;
	private int planSuffix;
	private String rider;
	private String statCode;
	private String validFlag;

	/**
	 * Constructor
	 */
	public Dryr619Dto() {
		// Calling default constructor
	}

	/**
	 * @return the bcesDte
	 */
	public int getBcesDte() {
		return bcesDte;
	}

	/**
	 * @param bcesDte
	 *            the bcesDte to set
	 */
	public void setBcesDte(int bcesDte) {
		this.bcesDte = bcesDte;
	}

	/**
	 * @return the cbanpr
	 */
	public int getCbanpr() {
		return cbanpr;
	}

	/**
	 * @param cbanpr
	 *            the cbanpr to set
	 */
	public void setCbanpr(int cbanpr) {
		this.cbanpr = cbanpr;
	}

	/**
	 * @return the chdrCoy
	 */
	public String getChdrCoy() {
		return chdrCoy;
	}

	/**
	 * @param chdrCoy
	 *            the chdrCoy to set
	 */
	public void setChdrCoy(String chdrCoy) {
		this.chdrCoy = chdrCoy;
	}

	/**
	 * @return the chdrNum
	 */
	public String getChdrNum() {
		return chdrNum;
	}

	/**
	 * @param chdrNum
	 *            the chdrNum to set
	 */
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}

	/**
	 * @return the coverage
	 */
	public String getCoverage() {
		return coverage;
	}

	/**
	 * @param coverage
	 *            the coverage to set
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	/**
	 * @return the covrpf
	 */
	public Covrpf getCovrpf() {
		return covrpf;
	}

	/**
	 * @param covrpf
	 *            the covrpf to set
	 */
	public void setCovrpf(Covrpf covrpf) {
		this.covrpf = covrpf;
	}

	/**
	 * @return the crrcd
	 */
	public int getCrrcd() {
		return crrcd;
	}

	/**
	 * @param crrcd
	 *            the crrcd to set
	 */
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}

	/**
	 * @return the crTable
	 */
	public String getCrTable() {
		return crTable;
	}

	/**
	 * @param crTable
	 *            the crTable to set
	 */
	public void setCrTable(String crTable) {
		this.crTable = crTable;
	}

	/**
	 * @return the currFrom
	 */
	public int getCurrFrom() {
		return currFrom;
	}

	/**
	 * @param currFrom
	 *            the currFrom to set
	 */
	public void setCurrFrom(int currFrom) {
		this.currFrom = currFrom;
	}

	/**
	 * @return the currTo
	 */
	public int getCurrTo() {
		return currTo;
	}

	/**
	 * @param currTo
	 *            the currTo to set
	 */
	public void setCurrTo(int currTo) {
		this.currTo = currTo;
	}

	/**
	 * @return the life
	 */
	public String getLife() {
		return life;
	}

	/**
	 * @param life
	 *            the life to set
	 */
	public void setLife(String life) {
		this.life = life;
	}

	/**
	 * @return the planSuffix
	 */
	public int getPlanSuffix() {
		return planSuffix;
	}

	/**
	 * @param planSuffix
	 *            the planSuffix to set
	 */
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	/**
	 * @return the rider
	 */
	public String getRider() {
		return rider;
	}

	/**
	 * @param rider
	 *            the rider to set
	 */
	public void setRider(String rider) {
		this.rider = rider;
	}

	/**
	 * @return the statCode
	 */
	public String getStatCode() {
		return statCode;
	}

	/**
	 * @param statCode
	 *            the statCode to set
	 */
	public void setStatCode(String statCode) {
		this.statCode = statCode;
	}

	/**
	 * @return the validFlag
	 */
	public String getValidFlag() {
		return validFlag;
	}

	/**
	 * @param validFlag
	 *            the validFlag to set
	 */
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Dryr619Dto [bcesDte=" + bcesDte + ", cbanpr=" + cbanpr + ", chdrCoy=" + chdrCoy + ", chdrNum=" + chdrNum
				+ ", coverage=" + coverage + ", covrpf=" + covrpf + ", crrcd=" + crrcd + ", crTable=" + crTable
				+ ", currFrom=" + currFrom + ", currTo=" + currTo + ", life=" + life + ", planSuffix=" + planSuffix
				+ ", rider=" + rider + ", statCode=" + statCode + ", validFlag=" + validFlag + "]";
	}

}
