/*
 * File: Dryh522.java
 * Date: January 15, 2015 4:14:57 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYH522.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import java.util.Iterator;
import java.util.List;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.cashdividends.dataaccess.HcsdTableDAM;
import com.csc.life.cashdividends.dataaccess.HdisTableDAM;
import com.csc.life.cashdividends.dataaccess.HdivintTableDAM;
import com.csc.life.cashdividends.dataaccess.dao.HdispfDAO;
import com.csc.life.cashdividends.dataaccess.dao.HdivpfDAO;
import com.csc.life.cashdividends.dataaccess.model.Hdispf;
import com.csc.life.cashdividends.dataaccess.model.Hdivpf;
import com.csc.life.cashdividends.recordstructures.Hdvdintrec;
import com.csc.life.cashdividends.tablestructures.Th501rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*REMARKS
*
*   This diary program process the HDIS records based on the
*   Next Interest Date(HINTNDT).
*   Dividend Interest is calculated and allocated to respective
*   HDIS while the subroutine defined is in TH501.  Then these
*   dividend files will be affected,
*        HDIS - dividend transaction summary
*        HDIV - dividend transaction details
*
*
*   PROCESSING
*   ==========
*
*   Initialise
*
*  - Pre-load constantly referenced Tables T5645. Fetch CHDR
*    record, read T5688, T1688.
*  - Check if contract valid.
*  - Set up the general fields in LIFACMV.
*
*  Read.
*
*  - Read HDISDRY.
*  - Valid converage status.
*
*  Update.
*
*  - Read HCSD(Cash dividend details)
*  - Read TH501 with cash dividend method in HCSD and effective
*    date = Next Interest Date
*  - Read and hold the CHDRLIF(Contract header)
*  - set NEW-TRANNO to CHDRLIF-TRANNO + 1
*  - Read HDIS(Dividend & Interest Summary)
*  - If interest allocation routine in TH501 is not blank,
*    set up the linkage for the new copy book HDVDINTREC to
*    call the dividend interest calc subr.
*  - Accumulate HDIA-INT-AMOUNT into a working field
*  - Calculate interest levied on the withdrawn dividend,
*    by looping through each HDIVCSH from next interest date     .
*    of HDISDRY
*         set up linkage in HDVDINTREC with data in HDIVCSH
*         call the same dividend interest calc subr
*         add HDIA-INT-AMOUNT returned to same working field,
*         note these HDIA-INT-AMOUNT values are expected -ve
*            also apply the fix DD & MM in TH501
*            ensure the DD for the new month is valid
*  - For non zero interest amount accumulated, prepare to write
*    a HDIV record and update the HDIS of it by invalid the
*    existing one and write a new one.
*  - if dividend amount is not zero, write accounting entries,
*         look for the posting level in T5688 for CNTTYPE
*         set up fields in LIFACMV linkage
*         map for the relevant GL details in T5645
*         call LIFACMV twice to post the dividend expense and
*         its opposite entry for payable.
* </pre>
*/
public class Dryh522 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYH522");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaSystemDate = new FixedLengthStringData(8);
	private ZonedDecimalData wsaaSysDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaSystemDate, 0).setUnsigned();
	private PackedDecimalData ix = new PackedDecimalData(5, 0);
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);
	private PackedDecimalData wsaaNewTranno = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(4, 0).init(ZERO);
	private PackedDecimalData wsaaIntAmount = new PackedDecimalData(17, 2).init(0);

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaLastTime = new FixedLengthStringData(1);
	private Validator loopLastTime = new Validator(wsaaLastTime, "Y");

		/* WSAA-T5645-ARRAY */
	//ILIFE-2628 fixed--Array size increased
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray (1000, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);
	//private static final int wsaaT5645Size = 45;
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT5645Size = 1000;
	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaLastCapDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaNextIntDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaHdisDate = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaHdisDd = new FixedLengthStringData(2).isAPartOf(wsaaHdisDate, 6);

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
		/* TABLES */
	private static final String th501 = "TH501";
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	private static final String t5679 = "T5679";
		/* ERRORS */
	private static final String h791 = "H791";
	private static final String hl16 = "HL16";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private HcsdTableDAM hcsdIO = new HcsdTableDAM();
	private HdisTableDAM hdisIO = new HdisTableDAM();
//	private HdisdryTableDAM hdisdryIO = new HdisdryTableDAM();
//	private HdivTableDAM hdivIO = new HdivTableDAM();
	//private HdivcshTableDAM hdivcshIO = new HdivcshTableDAM();
	private HdivintTableDAM hdivintIO = new HdivintTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	//private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Varcom varcom = new Varcom();
	private Th501rec th501rec = new Th501rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private T5679rec t5679rec = new T5679rec();
	private Hdvdintrec hdvdintrec = new Hdvdintrec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Getdescrec getdescrec = new Getdescrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();
	/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction starts*/
	private Itempf itempf = new Itempf();
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList = null;
	private Iterator<Itempf> itemIterator = null;
	private Hdispf hdispf= new Hdispf();
	private HdispfDAO hdispfDAO = getApplicationContext().getBean("hdispfDAO", HdispfDAO.class);
	private List<Hdispf> hdispfList = null;
	private Iterator<Hdispf> hdispfIterotor = null;
	private Hdivpf hdivpf = new Hdivpf();
	private Hdivpf hdivcsh = new Hdivpf();
	private HdivpfDAO hdivpfDAO = getApplicationContext().getBean("hdivpfDAO", HdivpfDAO.class);
	private List<Hdivpf> hdivpfList=null;
	private Iterator<Hdivpf> hdivIterator = null;
	/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction ends*/

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		next2080, 
		exit2090
	}

	public Dryh522() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void startProcessing100()
	{
		start110();
	}

protected void start110()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		wsaaSystemDate.set(getCobolDate());
		initialise1000();
		/*  Check if contract valid.*/
		validateContract1500();
		if (!validContract.isTrue()) {
			return ;
		}
		/*  Set up the initial key for looping through the HDISDRY*/
		/*  records for the contract.*/
		/*hdisdryIO.setDataKey(SPACES);
		hdisdryIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		hdisdryIO.setChdrnum(drypDryprcRecInner.drypEntity);
		hdisdryIO.setPlanSuffix(ZERO);
		hdisdryIO.setFormat(formatsInner.hdisdryrec);
		hdisdryIO.setFunction(varcom.begn);
		while ( !(isEQ(hdisdryIO.getStatuz(), varcom.endp))) {
			loopHdis2000();
		}*/
		/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction*/
		hdispf.setChdrcoy(drypDryprcRecInner.drypCompany.toString());
		hdispf.setChdrnum(drypDryprcRecInner.drypEntity.toString());
		hdispf.setPlnsfx(0);
		hdispfList = hdispfDAO.readHdispfData(hdispf);
		if(hdispfList==null ||(hdispfList !=null && hdispfList.size()==0)){
			return;
		}
		hdispfIterotor = hdispfList.iterator();
		while(hdispfIterotor.hasNext()){
			loopHdis2000();
		}
		/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction ends*/
		
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*  Read T5645, load all account rules.*/
		/*itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.begn);
		iy.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT5645300();
		}*/
	/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction*/
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaProg.toString());
		itempf.setItemseq(" ");
		iy.set(1);
		itempfList = itempfDAO.readItempfData(itempf);
		if(itempfList==null ||(itempfList!=null && itempfList.size()==0)){
			a000FatalError();
		}
		itemIterator = itempfList.iterator();
		while(itemIterator.hasNext()){
			loadT5645300();
		}
		/*ILIFE-3791 Improve the performance of Dividend interest allocation transaction ends*/
		/*  Fetch the CHDR record for the entity parameters passed.*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*  Read T5688, load contract processing rules.*/
		itdmIO.setParams(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isLT(itdmIO.getItmto(), chdrlifIO.getOccdate())) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		/*    Fields which require initialisation only once.*/
		lifacmvrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.batcbrn.set(drypDryprcRecInner.drypBranch);
		lifacmvrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifacmvrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifacmvrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifacmvrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(drypDryprcRecInner.drypRunDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);
		lifacmvrec.threadNumber.set(drypDryprcRecInner.drypThreadNumber);
		/*itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);*/
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
		itempf.setItemtabl(t1688);
		itempf.setItemitem(drypDryprcRecInner.drypBatctrcde.toString());
	/*	getdescrec.itemkey.set(itemIO.getDataKey());*/
		getdescrec.itemkey.set(itempf.getRecKeyData());
		getdescrec.language.set(drypDryprcRecInner.drypLanguage);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);
		if (isNE(getdescrec.statuz, varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		}
		else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}
	}

protected void loadT5645300()
	{
		/*start310();
	}

protected void start310()
	{*/
		/*SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itemIO.getItemtabl(), t5645)
		|| isNE(itemIO.getItemitem(), wsaaProg)
		|| isNE(itemIO.getStatuz(), varcom.oK)) {
			if (isEQ(itemIO.getFunction(), varcom.begn)) {
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			else {
				itemIO.setStatuz(varcom.endp);
				return ;
			}
		}*/
		itempf = itemIterator.next();
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		if (isGT(iy, wsaaT5645Size)) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(t5645);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		ix.set(1);
		while ( !(isGT(ix, 15))) {
			wsaaT5645Cnttot[iy.toInt()].set(t5645rec.cnttot[ix.toInt()]);
			wsaaT5645Glmap[iy.toInt()].set(t5645rec.glmap[ix.toInt()]);
			wsaaT5645Sacscode[iy.toInt()].set(t5645rec.sacscode[ix.toInt()]);
			wsaaT5645Sacstype[iy.toInt()].set(t5645rec.sacstype[ix.toInt()]);
			wsaaT5645Sign[iy.toInt()].set(t5645rec.sign[ix.toInt()]);
			ix.add(1);
			iy.add(1);
		}
		/*
		itemIO.setFunction(varcom.nextr);*/
	}

protected void validateContract1500()
	{
		start1510();
	}

protected void start1510()
	{
		/*itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypSystParm[1]);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}*/
		itempf.setItempfx(smtpfxcpy.item.toString());
		itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
		itempf.setItemtabl(t5679);
		itempf.setItemitem(drypDryprcRecInner.drypSystParm[1].toString());
		itempf = itempfDAO.getItempfRecord(itempf);
		if(itempf==null)
		{
			return;
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		wsaaValidContract.set("N");
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaIx.toInt()], chdrlifIO.getStatcode())) {
				wsaaIx.set(13);
				wsaaValidContract.set("Y");
			}
		}
		if (!validContract.isTrue()) {
			return ;
		}
		for (wsaaIx.set(1); !(isGT(wsaaIx, 12)); wsaaIx.add(1)){
			if (isEQ(t5679rec.cnPremStat[wsaaIx.toInt()], chdrlifIO.getPstatcode())) {
				wsaaIx.set(13);
				wsaaValidContract.set("Y");
			}
		}
	}

protected void loopHdis2000()
	{
		/*GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					begin2010();
				case next2080: 
					next2080();
				case exit2090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void begin2010()
	{*/
		/*SmartFileCode.execute(appVars, hdisdryIO);
		if (isNE(hdisdryIO.getStatuz(), varcom.oK)
		&& isNE(hdisdryIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(hdisdryIO.getStatuz());
			drylogrec.params.set(hdisdryIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(hdisdryIO.getStatuz(), varcom.endp)
		|| isNE(hdisdryIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(hdisdryIO.getChdrnum(), drypDryprcRecInner.drypEntity)) {
			hdisdryIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isGT(hdisdryIO.getNextIntDate(), drypDryprcRecInner.drypRunDate)
		|| isEQ(hdisdryIO.getTranno(), wsaaNewTranno)) {
			goTo(GotoLabel.next2080);
		}*/
		hdispf =hdispfIterotor.next();
		if (isGT(hdispf.getHintldt(), drypDryprcRecInner.drypRunDate)
			|| isEQ(hdispf.getTranno(), wsaaNewTranno)) {
				return;
		}
		/*  Valid coverage status.*/
		validateCoverage2200();
		if (!validCoverage.isTrue()) {
			return;
		}
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		/*    Skip processing this HDIS if its dividend balance is zero*/
		if (isEQ(hdispf.getHdvbalc(), 0)) {
			drycntrec.contotNumber.set(ct02);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			return;
		}
		update3000();
	}



protected void validateCoverage2200()
	{
		begin2210();
	}

protected void begin2210()
	{
		readCovr2300();
		/* Validate the coverage risk strtus against T5679.*/
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrlnbIO.getStatcode())) {
				wsaaT5679Sub.set(13);
				wsaaValidCoverage.set("Y");
			}
		}
		if (!validCoverage.isTrue()) {
			return ;
		}
		/* Validate the prem status against T5679.*/
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrlnbIO.getPstatcode())) {
				wsaaT5679Sub.set(13);
				wsaaValidCoverage.set("Y");
			}
		}
	}

protected void readCovr2300()
	{
		begin2310();
	}

protected void begin2310()
	{
		covrlnbIO.setParams(SPACES);
		covrlnbIO.setChdrcoy(hdispf.getChdrcoy());
		covrlnbIO.setChdrnum(hdispf.getChdrnum());
		covrlnbIO.setLife(hdispf.getLife());
		covrlnbIO.setCoverage(hdispf.getCoverage());
		covrlnbIO.setRider(hdispf.getRider());
		covrlnbIO.setPlanSuffix(hdispf.getPlnsfx());
		covrlnbIO.setFunction(varcom.readr);
		covrlnbIO.setFormat(formatsInner.covrlnbrec);
		SmartFileCode.execute(appVars, covrlnbIO);
		if (isNE(covrlnbIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrlnbIO.getStatuz());
			drylogrec.params.set(covrlnbIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void update3000()
	{
		update3010();
		loop3020();
	}

protected void update3010()
	{
		hcsdIO.setChdrcoy(hdispf.getChdrcoy());
		hcsdIO.setChdrnum(hdispf.getChdrnum());
		hcsdIO.setLife(hdispf.getLife());
		hcsdIO.setCoverage(hdispf.getCoverage());
		hcsdIO.setRider(hdispf.getRider());
		hcsdIO.setPlanSuffix(hdispf.getPlnsfx());
		hcsdIO.setFunction(varcom.readr);
		readHcsd5120();
		/*  Set last time loop flag to blank.*/
		wsaaLastTime.set(SPACES);
	}

protected void loop3020()
	{
		hdisIO.setChdrcoy(hdispf.getChdrcoy());
		hdisIO.setChdrnum(hdispf.getChdrnum());
		hdisIO.setLife(hdispf.getLife());
		hdisIO.setCoverage(hdispf.getCoverage());
		hdisIO.setRider(hdispf.getRider());
		hdisIO.setPlanSuffix(hdispf.getPlnsfx());
		hdisIO.setFunction(varcom.readr);
		procHdis5130();
		/*    Do not allocate interest if next interest date > next*/
		/*    capitalisation date.*/
		if (isGT(hdispf.getHintndt(), hdisIO.getNextCapDate())
		&& isEQ(hdisIO.getLastIntDate(), hdisIO.getNextCapDate())) {
			return ;
		}
		/*    Allocate interest for partial month leading to*/
		/*    the next capitalisation date.*/
		wsaaNextIntDate.set(varcom.vrcmMaxDate);
		if (isGT(hdispf.getHintndt(), hdisIO.getNextCapDate())) {
			wsaaNextIntDate.set(hdispf.getHintndt());
			hdispf.setHintndt(hdisIO.getNextCapDate().toInt());
		}
		/*    Match for the TH501 item with the Cash Dividend method held*/
		/*    on HCSD and Next Interest Date from HDIS.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItmfrm(hdispf.getHintndt());
		itdmIO.setItemtabl(th501);
		itdmIO.setItemitem(hcsdIO.getZcshdivmth());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), th501)
		|| isNE(itdmIO.getItemitem(), hcsdIO.getZcshdivmth())) {
			itdmIO.setStatuz(varcom.endp);
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		th501rec.th501Rec.set(itdmIO.getGenarea());
		if (isEQ(th501rec.intCalcSbr, SPACES)) {
			drylogrec.statuz.set(hl16);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		chdrlifIO.setChdrcoy(hdispf.getChdrcoy());
		chdrlifIO.setChdrnum(hdispf.getChdrnum());
		chdrlifIO.setFunction(varcom.readh);
		procChdrlif5100();
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(), 1));
		hdisIO.setChdrcoy(hdispf.getChdrcoy());
		hdisIO.setChdrnum(hdispf.getChdrnum());
		hdisIO.setLife(hdispf.getLife());
		hdisIO.setCoverage(hdispf.getCoverage());
		hdisIO.setRider(hdispf.getRider());
		hdisIO.setPlanSuffix(hdispf.getPlnsfx());
		hdisIO.setFunction(varcom.readh);
		procHdis5130();
		/*    Initialise the accumulate field for interest*/
		wsaaIntAmount.set(0);
		/*    Prepare to call interest allocation subroutine*/
		initialize(hdvdintrec.divdIntRec);
		hdvdintrec.chdrChdrcoy.set(hdispf.getChdrcoy());
		hdvdintrec.chdrChdrnum.set(hdispf.getChdrnum());
		hdvdintrec.lifeLife.set(hdispf.getLife());
		hdvdintrec.covrCoverage.set(hdispf.getCoverage());
		hdvdintrec.covrRider.set(hdispf.getRider());
		hdvdintrec.plnsfx.set(hdispf.getPlnsfx());
		hdvdintrec.cntcurr.set(chdrlifIO.getCntcurr());
		hdvdintrec.transcd.set(drypDryprcRecInner.drypBatctrcde);
		hdvdintrec.crtable.set(covrlnbIO.getCrtable());
		hdvdintrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
		hdvdintrec.tranno.set(wsaaNewTranno);
		hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
		hdvdintrec.intTo.set(hdispf.getHintndt());
		hdvdintrec.capAmount.set(hdispf.getHdvbalc());
		hdvdintrec.intDuration.set(ZERO);
		hdvdintrec.intAmount.set(ZERO);
		hdvdintrec.intRate.set(ZERO);
		hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
		/*    Call Interest Allocation Subroutine*/
		callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
		if (isNE(hdvdintrec.statuz, varcom.oK)) {
			drylogrec.params.set(hdvdintrec.divdIntRec);
			drylogrec.statuz.set(hdvdintrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*    Accumulate interest amount in a working field*/
		wsaaIntAmount.add(hdvdintrec.intAmount);
		/*    Read through HDIVCSH from next interest date of HDSX,*/
		/*    calculate interest on the withdrawn dividend, accumulate*/
		/*    to the total interest allocated in this run*/
		/*hdivcshIO.setChdrcoy(hdispf.getChdrcoy());
		hdivcshIO.setChdrnum(hdispf.getChdrnum());
		hdivcshIO.setLife(hdispf.getLife());
		hdivcshIO.setCoverage(hdispf.getCoverage());
		hdivcshIO.setRider(hdispf.getRider());
		hdivcshIO.setPlanSuffix(hdispf.getPlnsfx());
		compute(wsaaLastCapDate, 0).set(add(1, hdisIO.getLastCapDate()));
		hdivcshIO.setDivdIntCapDate(wsaaLastCapDate);
		hdivcshIO.setFunction(varcom.begn);
		readHdivcsh5110();
		if (isNE(hdivcshIO.getChdrcoy(), hdispf.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(), hdispf.getChdrnum())
		|| isNE(hdivcshIO.getLife(), hdispf.getLife())
		|| isNE(hdivcshIO.getCoverage(), hdispf.getCoverage())
		|| isNE(hdivcshIO.getRider(), hdispf.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(), hdispf.getPlnsfx())) {
			hdivcshIO.setStatuz(varcom.endp);
		}*/
		hdivcsh.setChdrcoy(hdispf.getChdrcoy());
		hdivcsh.setChdrnum(hdispf.getChdrnum());
		hdivcsh.setLife(hdispf.getLife());
		hdivcsh.setCoverage(hdispf.getCoverage());
		hdivcsh.setRider(hdispf.getRider());
		hdivcsh.setPlnsfx(hdispf.getPlnsfx());
		compute(wsaaLastCapDate, 0).set(add(1, hdispf.getHcapldt()));
		hdivcsh.setHincapdt(wsaaLastCapDate.toInt());
		if(hdivpfList!=null) {
			hdivpfList.clear();
		}
		hdivpfList = hdivpfDAO.searchHdivpfRecord(hdivcsh);
		if(hdivpfList!=null && hdivpfList.size()>0) {
			hdivIterator = hdivpfList.iterator();
			if(hdivIterator.hasNext()){
				readHdivcsh5110();
			}
			while (hdivIterator.hasNext()) { 
			initialize(hdvdintrec.divdIntRec);
				hdvdintrec.chdrChdrcoy.set(hdispf.getChdrcoy());
				hdvdintrec.chdrChdrnum.set(hdispf.getChdrnum());
				hdvdintrec.lifeLife.set(hdispf.getLife());
				hdvdintrec.covrCoverage.set(hdispf.getCoverage());
				hdvdintrec.covrRider.set(hdispf.getRider());
				hdvdintrec.plnsfx.set(hdispf.getPlnsfx());
			hdvdintrec.cntcurr.set(chdrlifIO.getCntcurr());
			hdvdintrec.transcd.set(drypDryprcRecInner.drypBatctrcde);
			hdvdintrec.crtable.set(covrlnbIO.getCrtable());
			hdvdintrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
			hdvdintrec.tranno.set(wsaaNewTranno);
				if (isGT(hdisIO.getLastIntDate(), hdivcsh.getEffdate())) {
				hdvdintrec.intFrom.set(hdisIO.getLastIntDate());
			}
			else {
					hdvdintrec.intFrom.set(hdivcsh.getEffdate());
			}
				hdvdintrec.intTo.set(hdispf.getHintndt());
				hdvdintrec.capAmount.set(hdivcsh.getHdvamt());
			hdvdintrec.intDuration.set(ZERO);
			hdvdintrec.intAmount.set(ZERO);
			hdvdintrec.intRate.set(ZERO);
			hdvdintrec.rateDate.set(varcom.vrcmMaxDate);
			/*    Call Interest Allocation Subroutine*/
			callProgram(th501rec.intCalcSbr, hdvdintrec.divdIntRec);
			if (isNE(hdvdintrec.statuz, varcom.oK)) {
				drylogrec.params.set(hdvdintrec.divdIntRec);
				drylogrec.statuz.set(hdvdintrec.statuz);
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			wsaaIntAmount.add(hdvdintrec.intAmount);
				//hdivcshIO.setFunction(varcom.nextr);
			readHdivcsh5110();
				/*if (isNE(hdivcshIO.getChdrcoy(), hdispf.getChdrcoy())
				|| isNE(hdivcshIO.getChdrnum(), hdispf.getChdrnum())
				|| isNE(hdivcshIO.getLife(), hdispf.getLife())
				|| isNE(hdivcshIO.getCoverage(), hdispf.getCoverage())
				|| isNE(hdivcshIO.getRider(), hdispf.getRider())
				|| isNE(hdivcshIO.getPlanSuffix(), hdispf.getPlnsfx())) {
				hdivcshIO.setStatuz(varcom.endp);
				}*/
			}
		}
		/*if (isNE(hdivcshIO.getChdrcoy(), hdispf.getChdrcoy())
		|| isNE(hdivcshIO.getChdrnum(), hdispf.getChdrnum())
		|| isNE(hdivcshIO.getLife(), hdispf.getLife())
		|| isNE(hdivcshIO.getCoverage(), hdispf.getCoverage())
		|| isNE(hdivcshIO.getRider(), hdispf.getRider())
		|| isNE(hdivcshIO.getPlanSuffix(), hdispf.getPlnsfx())) {
			hdivcshIO.setStatuz(varcom.endp);
		}*/
		
		/*    When the accumulated interest amount is not zeroes,*/
		/*    write a HDIV (dividend detail) and update HDIS (dividend*/
		/*    history).*/
		if (isNE(wsaaIntAmount, 0)) {
			/*hdivIO.setChdrcoy(hdispf.getChdrcoy());
			hdivIO.setChdrnum(hdispf.getChdrnum());
			hdivIO.setLife(hdispf.getLife());
			hdivIO.setJlife(hdispf.getJlife());
			hdivIO.setCoverage(hdispf.getCoverage());
			hdivIO.setRider(hdispf.getRider());
			hdivIO.setPlanSuffix(hdispf.getPlnsfx());
			hdivIO.setTranno(wsaaNewTranno);
			hdivIO.setEffdate(hdispf.getHintndt());
			hdivIO.setDivdAllocDate(drypDryprcRecInner.drypRunDate);
			hdivIO.setDivdIntCapDate(hdisIO.getNextCapDate());
			hdivIO.setCntcurr(chdrlifIO.getCntcurr());
			hdivIO.setDivdAmount(wsaaIntAmount);
			hdivIO.setDivdRate(hdvdintrec.intRate);
			hdivIO.setDivdRtEffdt(hdvdintrec.rateDate);
			hdivIO.setBatccoy(drypDryprcRecInner.drypCompany);
			hdivIO.setBatcbrn(drypDryprcRecInner.drypBranch);
			hdivIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
			hdivIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
			hdivIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
			hdivIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
			hdivIO.setDivdType("I");
			hdivIO.setZdivopt(hcsdIO.getZdivopt());
			hdivIO.setZcshdivmth(hcsdIO.getZcshdivmth());
			hdivIO.setDivdOptprocTranno(ZERO);
			hdivIO.setDivdCapTranno(ZERO);
			hdivIO.setDivdStmtNo(ZERO);
			hdivIO.setPuAddNbr(ZERO);
			hdivIO.setFunction(varcom.writr);
			writeHdiv5140();*/
			//IJTI-1485 Start
			hdivpf.setChdrcoy(hdispf.getChdrcoy());
			hdivpf.setChdrnum(hdispf.getChdrnum());
			hdivpf.setLife(hdispf.getLife());
			hdivpf.setJlife(hdispf.getJlife());
			hdivpf.setCoverage(hdispf.getCoverage());
			hdivpf.setRider(hdispf.getRider());
			//IJTI-1485 End
			hdivpf.setPlnsfx(hdispf.getPlnsfx());
			hdivpf.setTranno(wsaaNewTranno.toInt());
			hdivpf.setEffdate(hdispf.getHintndt());
			hdivpf.setHdvaldt(drypDryprcRecInner.drypRunDate.toInt());
			hdivpf.setHincapdt(hdisIO.getNextCapDate().toInt());
			hdivpf.setCntcurr(chdrlifIO.getCntcurr().toString());
			hdivpf.setHdvamt(wsaaIntAmount.getbigdata());
			hdivpf.setHdvrate(hdvdintrec.intRate.getbigdata());
			hdivpf.setHdveffdt(hdvdintrec.rateDate.toInt());
			hdivpf.setBatccoy(drypDryprcRecInner.drypCompany.toString());
			hdivpf.setBatcbrn(drypDryprcRecInner.drypBranch.toString());
			hdivpf.setBatcactyr(drypDryprcRecInner.drypBatcactyr.toInt());
			hdivpf.setBatcactmn(drypDryprcRecInner.drypBatcactmn.toInt());
			hdivpf.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
			hdivpf.setBatcbatch(drypDryprcRecInner.drypBatcbatch.toString());
			hdivpf.setHdvtyp("I");
			hdivpf.setZdivopt(hcsdIO.getZdivopt().toString());
			hdivpf.setZcshdivmth(hcsdIO.getZcshdivmth().toString());
			hdivpf.setHdvopttx(0);
			hdivpf.setHdvcaptx(0);
			hdivpf.setHdvsmtno(0);
			hdivpf.setHpuanbr(0);
			writeHdiv5140();
		}
		/* Always re-calculate next interest date.*/
		hdisIO.setValidflag("2");
		hdisIO.setFunction(varcom.rewrt);
		procHdis5130();
		hdisIO.setValidflag("1");
		hdisIO.setTranno(wsaaNewTranno);
		hdisIO.setLastIntDate(hdispf.getHintndt());
		/*  If risk cessation date already hit and interest allocation*/
		/*  catered for the days elapsed between the last interest date*/
		/*  and the risk cessation date, no need to find the next interest*/
		/*  date anymore.*/
		if (!loopLastTime.isTrue()) {
			if (isNE(wsaaNextIntDate, varcom.vrcmMaxDate)) {
				hdisIO.setNextIntDate(wsaaNextIntDate);
			}
			else {
				datcon4rec.intDate1.set(hdisIO.getNextIntDate());
				wsaaHdisDate.set(hdisIO.getNextIntDate());
				datcon4rec.frequency.set(th501rec.freqcy02);
				datcon4rec.freqFactor.set(1);
				datcon4rec.billmonth.set(hdisIO.getIntDueMm());
				datcon4rec.billday.set(wsaaHdisDd);
				callProgram(Datcon4.class, datcon4rec.datcon4Rec);
				/*  If next interest date passes the risk cessation date, set*/
				/*  it to risk cessation date, this is for maturity handling*/
				if (isNE(datcon4rec.statuz, varcom.oK)) {
					drylogrec.params.set(datcon4rec.datcon4Rec);
					drylogrec.dryDatabaseError.setTrue();
					a000FatalError();
				}
				if (isGT(datcon4rec.intDate2, covrlnbIO.getRiskCessDate())) {
					hdisIO.setNextIntDate(covrlnbIO.getRiskCessDate());
				}
				else {
					hdisIO.setNextIntDate(datcon4rec.intDate2);
				}
			}
		}
		setPrecision(hdisIO.getOsInterest(), 2);
		hdisIO.setOsInterest(add(hdisIO.getOsInterest(), wsaaIntAmount));
		hdisIO.setFunction(varcom.writr);
		procHdis5130();
		/*    Now write accounting records for the calculated interest if*/
		/*    it is not zero.*/
		if (isNE(wsaaIntAmount, 0)) {
			writeAccounting3200();
		}
		/*    Update existing CHDR as history and write a new one.*/
		chdrlifIO.setValidflag("2");
		chdrlifIO.setCurrto(drypDryprcRecInner.drypRunDate);
		chdrlifIO.setFunction(varcom.rewrt);
		procChdrlif5100();
		chdrlifIO.setValidflag("1");
		chdrlifIO.setCurrfrom(drypDryprcRecInner.drypRunDate);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		chdrlifIO.setTranno(wsaaNewTranno);
		chdrlifIO.setFunction(varcom.writr);
		procChdrlif5100();
		/*    Write a new PTRN.*/
		ptrnIO.setDataArea(SPACES);
		ptrnIO.setBatcpfx("BA");
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBranch);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setChdrcoy(hdispf.getChdrcoy());
		ptrnIO.setChdrnum(hdispf.getChdrnum());
		ptrnIO.setTranno(wsaaNewTranno);
		ptrnIO.setValidflag("1");
		ptrnIO.setPtrneff(hdispf.getHintldt());
		ptrnIO.setTermid(SPACES);
		ptrnIO.setTransactionDate(wsaaSysDate);
		ptrnIO.setTransactionTime(varcom.vrcmTimen);
		ptrnIO.setUser(999999);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFunction(varcom.writr);
		ptrnIO.setFormat(formatsInner.ptrnrec);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*    Repeat 3000 section until HDIS-NEXT-INT-DATE has gone*/
		/*    pass DRYP-RUN-DATE, that means, all interests to be*/
		/*    allocated in the interval from last interest date to run*/
		/*    date has been performed.*/
		if (loopLastTime.isTrue()) {
			return ;
		}
		else {
			if (isEQ(hdisIO.getNextIntDate(), covrlnbIO.getRiskCessDate())) {
				wsaaLastTime.set("Y");
			}
		}
		if (isLTE(hdisIO.getNextIntDate(), drypDryprcRecInner.drypRunDate)) {
			hdispf.setHintldt(hdisIO.getNextIntDate().toInt());
			loop3020();
			return ;
		}
	}

protected void writeAccounting3200()
	{
		accounting3210();
	}

protected void accounting3210()
	{
		/*    Determine contract or component level accounting in T5688*/
		wsaaComponLevelAccounted.set(t5688rec.comlvlacc);
		lifacmvrec.origcurr.set(chdrlifIO.getCntcurr());
		lifacmvrec.origamt.set(wsaaIntAmount);
		/*    Post against expenses.*/
		wsaaJrnseq.add(1);
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(3);
			lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			x1000CallLifacmv();
		}
		else {
			t5645Ix.set(1);
			lifacmvrec.substituteCode[6].set(SPACES);
			x1000CallLifacmv();
		}
		/*    Post opposite entry in payable.*/
		wsaaJrnseq.add(1);
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(4);
			lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			x1000CallLifacmv();
		}
		else {
			t5645Ix.set(2);
			lifacmvrec.substituteCode[6].set(SPACES);
			x1000CallLifacmv();
		}
		/*    Count no.of HDIS records processed in CT03 and total*/
		/*    dividend amount in CT04*/
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		drycntrec.contotNumber.set(ct04);
		drycntrec.contotValue.set(wsaaIntAmount);
		d000ControlTotals();
	}

protected void procChdrlif5100()
	{
		/*CHDR*/
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getParams());
			drylogrec.params.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void readHdivcsh5110()
	{
		/*HDIVCSH*/
		/*hdivcshIO.setFormat(formatsInner.hdivcshrec);
		SmartFileCode.execute(appVars, hdivcshIO);
		if (isNE(hdivcshIO.getStatuz(), varcom.oK)
		&& isNE(hdivcshIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(hdivcshIO.getParams());
			drylogrec.statuz.set(hdivcshIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}*/
	hdivcsh = hdivIterator.next();
		/*EXIT*/
	}

protected void readHcsd5120()
	{
		/*HCSD*/
		hcsdIO.setFormat(formatsInner.hcsdrec);
		SmartFileCode.execute(appVars, hcsdIO);
		if (isNE(hcsdIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(hcsdIO.getParams());
			drylogrec.statuz.set(hcsdIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void procHdis5130()
	{
		/*HDIS*/
		hdisIO.setFormat(formatsInner.hdisrec);
		SmartFileCode.execute(appVars, hdisIO);
		if (isNE(hdisIO.getStatuz(), varcom.oK)
		&& isNE(hdisIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(hdisIO.getParams());
			drylogrec.statuz.set(hdisIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*EXIT*/
	}

protected void writeHdiv5140()
	{
		hdivpfDAO.insertHdivData(hdivpf);
	}

protected void x1000CallLifacmv()
	{
		x1010Call();
	}

protected void x1010Call()
	{
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return ;
		}
		lifacmvrec.function.set("PSTD");
		lifacmvrec.jrnseq.set(wsaaJrnseq);
		if (componLevelAccounted.isTrue()) {
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(hdispf.getPlnsfx());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(hdispf.getChdrnum());
			stringVariable1.addExpression(hdispf.getLife());
			stringVariable1.addExpression(hdispf.getCoverage());
			stringVariable1.addExpression(hdispf.getRider());
			stringVariable1.addExpression(wsaaPlan);
			stringVariable1.setStringInto(lifacmvrec.rldgacct);
		}
		else {
			lifacmvrec.rldgacct.set(hdispf.getChdrnum());
		}
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[t5645Ix.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[t5645Ix.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[t5645Ix.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[t5645Ix.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[t5645Ix.toInt()]);
		lifacmvrec.rdocnum.set(hdispf.getChdrnum());
		lifacmvrec.tranref.set(hdispf.getChdrnum());
		lifacmvrec.tranno.set(wsaaNewTranno);
		lifacmvrec.effdate.set(hdispf.getHintldt());
		lifacmvrec.substituteCode[1].set(chdrlifIO.getCnttype());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			drylogrec.params.set(lifacmvrec.lifacmvRec);
			drylogrec.statuz.set(lifacmvrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
		/* FORMATS */
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData hcsdrec = new FixedLengthStringData(10).init("HCSDREC");
	private FixedLengthStringData hdivrec = new FixedLengthStringData(10).init("HDIVREC");
	private FixedLengthStringData hdisrec = new FixedLengthStringData(10).init("HDISREC");
	private FixedLengthStringData hdisdryrec = new FixedLengthStringData(10).init("HDISDRYREC");
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
	private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData hdivcshrec = new FixedLengthStringData(10).init("HDIVCSHREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
}
}
