/*
 * File: Drybilreq.java
 * Date: December 3, 2013 2:24:08 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYBILREQ.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.diary.procedures.Drylog;
import com.csc.diary.recordstructures.Drylogrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.clients.dataaccess.ClntTableDAM;
import com.csc.fsu.financials.tablestructures.T3616rec;
import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.dataaccess.BextTableDAM;
import com.csc.fsu.general.dataaccess.GrpsTableDAM;
import com.csc.fsu.general.procedures.Addacmv;
import com.csc.fsu.general.recordstructures.Addacmvrec;
import com.csc.fsu.general.recordstructures.Billreqrec;
import com.csc.fsu.general.recordstructures.Ztrnamtcpy;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.general.tablestructures.T3684rec;
import com.csc.fsu.general.tablestructures.T3698rec;
import com.csc.fsu.general.tablestructures.T3700rec;
import com.csc.fsu.general.tablestructures.T3701rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Dryprcrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* Overview.
* =========
*
* This FSU subroutine is called to create billing data which will  e
* available for any type of FSU processing. (E.g Direct Debits)
*
* It passes two control totals back to DRY5349 :
*
* 1. BEXT records created
* 2. Total amount on BEXT records.
*
* Linkage Area.
* =============
* FUNCTION        X(05)         Calling Function
* STATUZ          X(04)         Returning Status
* COMPANY         X(01)         Batch Company
* BRANCH          X(02)         Batch Branch
* EFFDATE        S9(08) COMP-3  Effective Date
* ACCTYEAR       S9(02) COMP-3  Batch Accounting Year
* ACCTMONTH      S9(02) COMP-3  Batch Accounting Month
* TRANCODE        X(04)         Batch Transcode
* BATCH           X(05)         Batch Number
* TRANNO          9(05)         Transaction Number
* TERMID          X(04)         Terminal ID
* USER            9(06)         User Number
* CHDRPFX         X(02)         Contract Header Prefix
* CHDRCOY         X(01)         Contract Header Company
* CHDRNUM         X(08)         Contract Header Number
* SERVUNIT        X(02)         Service Unit
* CNTTYPE         X(03)         Contract Type
* CNTCURR         X(03)         Contract Currency
* OCCDATE        S9(08) COMP-3  Original Commencement Date
* CCDATE         S9(08) COMP-3  Contract Commencement Date
* PTDATE         S9(08) COMP-3  Paid To date
* BTDATE         S9(08) COMP-3  Billed To Date
* BILLDATE       S9(08) COMP-3  Billing Date
* BILLCHNL        X(02)         Billing Channel
* BANKCODE        X(02)         Bank Code
* INSTFROM       S9(08) COMP-3  Installment From Date
* INSTTO         S9(08) COMP-3  Installment To Date
* INSTBCHNL       X(02)         Installment Billing Channel
* INSTCCHNL       X(02)         Installment Collection Channel
* INSTFREQ        X(02)         Installment Frequency
* INSTJCTL        X(24)         Installment Run ID
* GRPSCOY         X(01)         Group Company
* GRPSNUM         X(08)         Group Number
* MEMBSEL         X(10)         Group Member Number
* FACTHOUS        X(02)         Factoring House
* BANKKEY         X(10)         Bank Key
* BANKACCKEY      X(10)         Bank Account Key
* COWNPFX         X(02)         Contract Owner Prefix
* COWNCOY         X(01)         Contract Owner Company
* COWNNUM         X(08)         Contract Owner Number
* PAYRPFX         X(02)         Payer Prefix
* PAYRCOY         X(01)         Payer Company
* PAYRNUM         X(08)         Payer Number
* CNTBRANCH       X(02)         Servicing Branch
* AGNTPFX         X(02)         Agent Prefix
* AGNTCOY         X(01)         Agent Company
* AGNTNUM         X(05)         Agent Number
* PAYFLAG         X(01)         Installment Paid Flag
* BILFLAG         X(01)         Installment Billed Flag
* OUTFLAG         X(01)         Installment Outstanding Flag
* SUPFLAG         X(01)         Billing Suppress Flag
* BILLCD         S9(08) COMP-3  Billing Renewal Date
* MANDREF         X(05)         Mandate Reference Number
* BILLCURR        X(03)         Billing Currency
* SACSCODE-01     X(02)         Sub-account Code
* SACSTYPE-01     X(02)         Sub-account Type
* GLMAP-01        X(14)         General Ledger Key Map
* GLSIGN-01       X(01)         General Ledger Sign(Debit +ve/Cred t -ve)
* SACSCODE-02     X(02)         Sub-account Code
* SACSTYPE-02     X(02)         Sub-account Type
* GLMAP-02        X(14)         General Ledger Key Map
* GLSIGN-02       X(01)         General Ledger Sign(Debit +ve/Cred t -ve)
* MANDSTAT        X(02)         Mandate Status Code
* CONTOT-01      S9(15)V9(02)   Control Total
* CONTOT-02      S9(15)V9(02)   Control Total
* INSTAMT01 S9(15)V9(02) COMP-3 Installment Amount
* INSTAMT02 S9(15)V9(02) COMP-3 Installment Amount
* INSTAMT03 S9(15)V9(02) COMP-3 Installment Amount
* INSTAMT04 S9(15)V9(02) COMP-3 Installment Amount
* INSTAMT05 S9(15)V9(02) COMP-3 Installment Amount
* INSTAMT06 S9(15)V9(02) COMP-3 Installment Amount
*
* These fields are all contained within BILREQREC.
*
* Processing.
* ===========
*
* If it is the first time in the subroutine and the mode
* indicator (BLRQ-MODE-IND) is set to 'BATCH', load the
* tables required for binary searching (T3616, T3620,
* T3700 and T3701).
*
* Read table T1688 to obtain the long description for
* the TRANCODE. Read table T3620 with BILLCHNL as the key.
* Since only one entry is used throughout, these tables
* do not need to be stored in an array and can be read
* once for both batch and on-line or AT.
*
* If processing is on account i.e. T3620-ACC-IND not equal
* to spaces, do the following:
*
* 1. Set up ACMV fields common to both debit and credit records
*    -Set up common ADDACMVREC fields.
*    -If there is a group number i.e. BLRQ-GRPSNUM not equal to
*     spaces, construct the statement sort key.
*    -Call 'GRPSIO' to obtain the group billing code and read
*     T3700.
*    -Perform the sort and string the BLRQ fields into
*     ADDA-SMTSORT.
*
* 2. Write an ACMV credit entry record
*    -Read T3616 with SACSCODE as the key.
*    -Write an ACMV credit entry record.
*
* 3. Write an ACMV debit entry record
*    -Read T3616 and T3698 with SACSCODE as the key.
*    -If trigger module is required i.e. T3620-TRIGGER-REQUIRED
*     not equal to spaces, read T3701.
*    -Write an ACMV debit entry record.
*
* If media is required  i.e. T3620-MEDIA-REQUIRED not equal to
* spaces, produce BEXT records. All the required BEXT fields are
* in linkage.
*
* Move 1 to CONTOT-01.
* Move total amount to CONTOT-02.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Drybilreq extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYBILREQ1");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private static final String bextrec = "BEXTREC";
	private static final String clntrec = "CLNTREC";
	private static final String dacmrec = "DACMREC";
	private static final String acagrnlrec = "ACAGRNLREC";
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t3616 = "T3616";
	private static final String t3620 = "T3620";
	private static final String t3698 = "T3698";
	private static final String t3700 = "T3700";
	private static final String t3701 = "T3701";
	private static final String t3684 = "T3684";
		/* ERRORS */
	private static final String h791 = "H791";
	private static final String e049 = "E049";
	private static final String stnf = "STNF";

	private FixedLengthStringData wsaaT3698Itemkey = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT3698Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT3698Itemkey, 0);
	private FixedLengthStringData wsaaT3698Sacscode = new FixedLengthStringData(2).isAPartOf(wsaaT3698Itemkey, 4);
	private PackedDecimalData wsaaT3698Sub = new PackedDecimalData(3, 0).init(1);

		/* WSAA-SF */
	private FixedLengthStringData[] wsaaSortFields = FLSInittedArray (6, 41);
	private ZonedDecimalData[] wsaaT3700Sort = ZDArrayPartOfArrayStructure(1, 0, wsaaSortFields, 0);
	private FixedLengthStringData[] wsaaField = FLSDArrayPartOfArrayStructure(20, wsaaSortFields, 1);
	private FixedLengthStringData[] wsaaBlrqField = FLSDArrayPartOfArrayStructure(20, wsaaSortFields, 21);
	private PackedDecimalData wsaaTop = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaSortIx = new PackedDecimalData(4, 0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaSacscode = new FixedLengthStringData(2);
	private String wsaaMapFound = "";

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaGrupkey = new FixedLengthStringData(12);
	private FixedLengthStringData wsaaGrpscoy = new FixedLengthStringData(1).isAPartOf(wsaaGrupkey, 0);
	private FixedLengthStringData wsaaGrpsnum = new FixedLengthStringData(8).isAPartOf(wsaaGrupkey, 1);
	private FixedLengthStringData wsaaLastTrancode = new FixedLengthStringData(4);
/*	private static final int wsaaT3616Size = 50;
	private static final int wsaaT3620Size = 30;
	private static final int wsaaT3700Size = 30;
	private static final int wsaaT3701Size = 30;
	private static final int wsaaT3684Size = 30; */
	//ILIFE-2628 fixed--Array size increased
	private static final int wsaaT3616Size = 1000;
	private static final int wsaaT3620Size = 1000;
	private static final int wsaaT3700Size = 1000;
	private static final int wsaaT3701Size = 1000;
	private static final int wsaaT3684Size = 1000;
	

		/* WSAA-T3616-ARRAY */
	private FixedLengthStringData[] wsaaT3616Rec = FLSInittedArray (1000, 4);
	private FixedLengthStringData[] wsaaT3616Key = FLSDArrayPartOfArrayStructure(2, wsaaT3616Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3616Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT3616Key, 0);
	private FixedLengthStringData[] wsaaT3616Data = FLSDArrayPartOfArrayStructure(2, wsaaT3616Rec, 2);
	private FixedLengthStringData[] wsaaT3616SubjPref = FLSDArrayPartOfArrayStructure(2, wsaaT3616Data, 0);

		/* WSAA-T3684-ARRAY */
	private FixedLengthStringData[] wsaaT3684Rec = FLSInittedArray (1000, 2);
	private FixedLengthStringData[] wsaaT3684Key = FLSDArrayPartOfArrayStructure(1, wsaaT3684Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3684Facthous = FLSDArrayPartOfArrayStructure(1, wsaaT3684Key, 0);
	private FixedLengthStringData[] wsaaT3684Data = FLSDArrayPartOfArrayStructure(1, wsaaT3684Rec, 1);
	private FixedLengthStringData[] wsaaT3684AssumePaid = FLSDArrayPartOfArrayStructure(1, wsaaT3684Data, 0);

		/* WSAA-T3701-ARRAY */
	private FixedLengthStringData[] wsaaT3701Rec = FLSInittedArray (1000, 17);
	private FixedLengthStringData[] wsaaT3701Key = FLSDArrayPartOfArrayStructure(7, wsaaT3701Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3701Trancode = FLSDArrayPartOfArrayStructure(4, wsaaT3701Key, 0);
	private FixedLengthStringData[] wsaaT3701Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT3701Key, 4);
	private FixedLengthStringData[] wsaaT3701Data = FLSDArrayPartOfArrayStructure(10, wsaaT3701Rec, 7);
	private FixedLengthStringData[] wsaaT3701Reconsbr = FLSDArrayPartOfArrayStructure(10, wsaaT3701Data, 0);

	private FixedLengthStringData wsaaT3701Key2 = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT3701Trancode2 = new FixedLengthStringData(4).isAPartOf(wsaaT3701Key2, 0);
	private FixedLengthStringData wsaaT3701Cnttype2 = new FixedLengthStringData(3).isAPartOf(wsaaT3701Key2, 4);
	private IntegerData wsaaT3616Ix = new IntegerData();
	private IntegerData wsaaT3620Ix = new IntegerData();
	private IntegerData wsaaT3684Ix = new IntegerData();
	private IntegerData wsaaT3700Ix = new IntegerData();
	private IntegerData wsaaT3701Ix = new IntegerData();
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private BextTableDAM bextIO = new BextTableDAM();
	private ClntTableDAM clntIO = new ClntTableDAM();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private GrpsTableDAM grpsIO = new GrpsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T3616rec t3616rec = new T3616rec();
	private T3620rec t3620rec = new T3620rec();
	private T3698rec t3698rec = new T3698rec();
	private T3700rec t3700rec = new T3700rec();
	private T3701rec t3701rec = new T3701rec();
	private T3684rec t3684rec = new T3684rec();
	private Addacmvrec addacmvrec = new Addacmvrec();
	private Varcom varcom = new Varcom();
	private Ztrnamtcpy ztrnamtcpy = new Ztrnamtcpy();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Dryprcrec dryprcrec = new Dryprcrec();
	private Billreqrec billreqrec = new Billreqrec();
	private WsaaT3620ArrayInner wsaaT3620ArrayInner = new WsaaT3620ArrayInner();
	private WsaaT3700ArrayInner wsaaT3700ArrayInner = new WsaaT3700ArrayInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		searchT36842520, 
		error2525, 
		exit2590, 
		searchT36203020, 
		error3025, 
		readT36203030, 
		exit3090, 
		searchT37004172, 
		error4173, 
		readT37004174, 
		exit4179, 
		exit4209, 
		searchT36164252, 
		error4253, 
		readT36164254, 
		exit4259, 
		exit4309, 
		readSkipped4355, 
		searchT37014372, 
		searchAgain4373, 
		error4374, 
		readT37014375, 
		exit4379
	}

	public Drybilreq() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryprcrec.dryprcRec = convertAndSetParam(dryprcrec.dryprcRec, parmArray, 1);
		billreqrec.billreqRec = convertAndSetParam(billreqrec.billreqRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(dryprcrec.dryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		mainlineExit010();
	}

protected void main010()
	{
		dryprcrec.statuz.set(varcom.oK);
		billreqrec.statuz.set(varcom.oK);
		if (firstTime.isTrue()) {
			if (isNE(billreqrec.modeInd, "BATCH")
			&& isNE(billreqrec.modeInd, "ONLIN")
			&& isNE(billreqrec.modeInd, "AT")) {
				drylogrec.statuz.set(e049);
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61 
			}
			if (isEQ(billreqrec.modeInd, "BATCH")) {
				wsaaFirstTime.set("N");
				setupT36161100();
				setupT36201200();
				setupT37001300();
				setupT37011400();
				setupT36841500();
			}
		}
		getT16882000();
		getT36842500();
		getT36203000();
		if (isNE(t3620rec.accInd, SPACES)) {
			setupCommonAcmvFields4100();
			writeCreditAcmv4200();
			writeDebitAcmv4300();
		}
		if (isNE(t3620rec.mediaRequired, SPACES)) {
			produceBextRecords5000();
			billreqrec.contot01.set(1);
			billreqrec.contot02.set(bextIO.getInstamt06());
		}
	}

protected void mainlineExit010()
	{
		exitProgram();
	}

protected void setupT36161100()
	{
		/*T3616*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3616);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		wsaaT3616Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT36161150();
		}
		
		/*EXIT*/
	}

protected void loadT36161150()
	{
		t36161151();
	}

protected void t36161151()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItemcoy(), billreqrec.company)
		|| isNE(itemIO.getItemtabl(), t3616)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		t3616rec.t3616Rec.set(itemIO.getGenarea());
		if (isGT(wsaaT3616Ix, wsaaT3616Size)) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(t3616);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		wsaaT3616Sacscode[wsaaT3616Ix.toInt()].set(itemIO.getItemitem());
		wsaaT3616SubjPref[wsaaT3616Ix.toInt()].set(t3616rec.subjPref);
		itemIO.setFunction(varcom.nextr);
		wsaaT3616Ix.add(1);
	}

protected void setupT36201200()
	{
		/*T3620*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3620);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		wsaaT3620Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT36201250();
		}
		
		/*EXIT*/
	}

protected void loadT36201250()
	{
		t36201251();
	}

protected void t36201251()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItemcoy(), billreqrec.company)
		|| isNE(itemIO.getItemtabl(), t3620)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
		if (isGT(wsaaT3620Ix, wsaaT3620Size)) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(t3620);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		wsaaT3620ArrayInner.wsaaT3620Billchnl[wsaaT3620Ix.toInt()].set(itemIO.getItemitem());
		wsaaT3620ArrayInner.wsaaT3620Agntsdets[wsaaT3620Ix.toInt()].set(t3620rec.agntsdets);
		wsaaT3620ArrayInner.wsaaT3620Ddind[wsaaT3620Ix.toInt()].set(t3620rec.ddind);
		wsaaT3620ArrayInner.wsaaT3620Crcind[wsaaT3620Ix.toInt()].set(t3620rec.crcind);
		wsaaT3620ArrayInner.wsaaT3620Grpind[wsaaT3620Ix.toInt()].set(t3620rec.grpind);
		wsaaT3620ArrayInner.wsaaT3620MediaRequired[wsaaT3620Ix.toInt()].set(t3620rec.mediaRequired);
		wsaaT3620ArrayInner.wsaaT3620Payind[wsaaT3620Ix.toInt()].set(t3620rec.payind);
		wsaaT3620ArrayInner.wsaaT3620Sacscode[wsaaT3620Ix.toInt()].set(t3620rec.sacscode);
		wsaaT3620ArrayInner.wsaaT3620Sacstyp[wsaaT3620Ix.toInt()].set(t3620rec.sacstyp);
		wsaaT3620ArrayInner.wsaaT3620TriggerRequired[wsaaT3620Ix.toInt()].set(t3620rec.triggerRequired);
		wsaaT3620ArrayInner.wsaaT3620AccInd[wsaaT3620Ix.toInt()].set(t3620rec.accInd);
		itemIO.setFunction(varcom.nextr);
		wsaaT3620Ix.add(1);
	}

protected void setupT37001300()
	{
		/*T3700*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3700);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(varcom.begn);
		wsaaT3700Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT37001350();
		}
		
		/*EXIT*/
	}

protected void loadT37001350()
	{
		t37001351();
	}

protected void t37001351()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItemcoy(), billreqrec.company)
		|| isNE(itemIO.getItemtabl(), t3700)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		t3700rec.t3700Rec.set(itemIO.getGenarea());
		wsaaT3700ArrayInner.wsaaT3700Billseq[wsaaT3700Ix.toInt()].set(itemIO.getItemitem());
		if (isGT(wsaaT3700Ix, wsaaT3700Size)) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(t3700);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		wsaaT3700ArrayInner.wsaaT3700Agntsort[wsaaT3700Ix.toInt()].set(t3700rec.agntsort);
		wsaaT3700ArrayInner.wsaaT3700Chdrsort[wsaaT3700Ix.toInt()].set(t3700rec.chdrsort);
		wsaaT3700ArrayInner.wsaaT3700Duedsort[wsaaT3700Ix.toInt()].set(t3700rec.duedsort);
		wsaaT3700ArrayInner.wsaaT3700Membsort[wsaaT3700Ix.toInt()].set(t3700rec.membsort);
		wsaaT3700ArrayInner.wsaaT3700Pnamsort[wsaaT3700Ix.toInt()].set(t3700rec.pnamsort);
		wsaaT3700ArrayInner.wsaaT3700Pnumsort[wsaaT3700Ix.toInt()].set(t3700rec.pnumsort);
		itemIO.setFunction(varcom.nextr);
		wsaaT3700Ix.add(1);
	}

protected void setupT37011400()
	{
		/*T3701*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3701);
		itemIO.setItemitem(billreqrec.trancode);
		itemIO.setFunction(varcom.begn);
		wsaaT3701Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT37011450();
		}
		
		/*EXIT*/
	}

protected void loadT37011450()
	{
		t37011451();
	}

protected void t37011451()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		wsaaT3701Key[wsaaT3701Ix.toInt()].set(itemIO.getItemitem());
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItemcoy(), billreqrec.company)
		|| isNE(itemIO.getItemtabl(), t3701)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		t3701rec.t3701Rec.set(itemIO.getGenarea());
		if (isGT(wsaaT3701Ix, wsaaT3701Size)) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(t3701);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		wsaaT3701Reconsbr[wsaaT3701Ix.toInt()].set(t3701rec.reconsbr);
		itemIO.setFunction(varcom.nextr);
		wsaaT3701Ix.add(1);
	}

protected void setupT36841500()
	{
		/*T3684*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.fsuco);
		itemIO.setItemtabl(t3684);
		itemIO.setItemitem(billreqrec.facthous);
		itemIO.setFunction(varcom.begn);
		wsaaT3684Ix.set(1);
		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
			loadT36841550();
		}
		
		/*EXIT*/
	}

protected void loadT36841550()
	{
		t36841551();
	}

protected void t36841551()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		wsaaT3684Key[wsaaT3684Ix.toInt()].set(itemIO.getItemitem());
		if (isEQ(itemIO.getStatuz(), varcom.endp)
		|| isNE(itemIO.getItemcoy(), billreqrec.fsuco)
		|| isNE(itemIO.getItemtabl(), t3684)) {
			itemIO.setStatuz(varcom.endp);
			return ;
		}
		t3684rec.t3684Rec.set(itemIO.getGenarea());
		if (isGT(wsaaT3684Ix, wsaaT3684Size)) {
			drylogrec.statuz.set(h791);
			drylogrec.params.set(t3684);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		wsaaT3684AssumePaid[wsaaT3684Ix.toInt()].set(t3684rec.assumePaid);
		itemIO.setFunction(varcom.nextr);
		wsaaT3684Ix.add(1);
	}

protected void getT16882000()
	{
		t16882010();
	}

protected void t16882010()
	{
		/*    Obtain long description for the transcode.*/
		if (isEQ(billreqrec.trancode, wsaaLastTrancode)) {
			return ;
		}
		wsaaLastTrancode.set(billreqrec.trancode);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(billreqrec.company);
		descIO.setDesctabl(t1688);
		descIO.setDescitem(billreqrec.trancode);
		descIO.setLanguage(billreqrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
	}

protected void getT36842500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t36842510();
				case searchT36842520: 
					searchT36842520();
				case error2525: 
					error2525();
					readT36842530();
				case exit2590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t36842510()
	{
		/*    Read Table T3684 with FATCHOUS as the key.*/
		if (isEQ(billreqrec.modeInd, "BATCH")) {
			goTo(GotoLabel.searchT36842520);
		}
		else {
			goTo(GotoLabel.error2525);
		}
	}

protected void searchT36842520()
	{
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3684Rec);
		as1.setIndices(wsaaT3684Ix);
		as1.addSearchKey(wsaaT3684Key, billreqrec.facthous, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			goTo(GotoLabel.error2525);
		}
		t3684rec.assumePaid.set(wsaaT3684AssumePaid[wsaaT3684Ix.toInt()]);
		goTo(GotoLabel.exit2590);
	}

protected void error2525()
	{
		if (isEQ(billreqrec.facthous, SPACES)) {
			t3684rec.assumePaid.set(SPACES);
			goTo(GotoLabel.exit2590);
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.fsuco);
		itemIO.setItemtabl(t3684);
		itemIO.setItemitem(billreqrec.facthous);
		itemIO.setFunction(varcom.readr);
	}

protected void readT36842530()
	{
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t3684rec.t3684Rec.set(itemIO.getGenarea());
	}

protected void getT36203000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t36203010();
				case searchT36203020: 
					searchT36203020();
				case error3025: 
					error3025();
				case readT36203030: 
					readT36203030();
				case exit3090: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t36203010()
	{
		/*    Read Table T3620 with BILLCHNL as the key.*/
		if (isEQ(billreqrec.modeInd, "BATCH")) {
			goTo(GotoLabel.searchT36203020);
		}
		else {
			goTo(GotoLabel.readT36203030);
		}
	}

protected void searchT36203020()
	{
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3620ArrayInner.wsaaT3620Rec);
		as1.setIndices(wsaaT3620Ix);
		as1.addSearchKey(wsaaT3620ArrayInner.wsaaT3620Key, billreqrec.billchnl, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			goTo(GotoLabel.error3025);
		}
		t3620rec.agntsdets.set(wsaaT3620ArrayInner.wsaaT3620Agntsdets[wsaaT3620Ix.toInt()]);
		t3620rec.ddind.set(wsaaT3620ArrayInner.wsaaT3620Ddind[wsaaT3620Ix.toInt()]);
		t3620rec.crcind.set(wsaaT3620ArrayInner.wsaaT3620Crcind[wsaaT3620Ix.toInt()]);
		t3620rec.grpind.set(wsaaT3620ArrayInner.wsaaT3620Grpind[wsaaT3620Ix.toInt()]);
		t3620rec.mediaRequired.set(wsaaT3620ArrayInner.wsaaT3620MediaRequired[wsaaT3620Ix.toInt()]);
		t3620rec.payind.set(wsaaT3620ArrayInner.wsaaT3620Payind[wsaaT3620Ix.toInt()]);
		t3620rec.sacscode.set(wsaaT3620ArrayInner.wsaaT3620Sacscode[wsaaT3620Ix.toInt()]);
		t3620rec.sacstyp.set(wsaaT3620ArrayInner.wsaaT3620Sacstyp[wsaaT3620Ix.toInt()]);
		t3620rec.triggerRequired.set(wsaaT3620ArrayInner.wsaaT3620TriggerRequired[wsaaT3620Ix.toInt()]);
		t3620rec.accInd.set(wsaaT3620ArrayInner.wsaaT3620AccInd[wsaaT3620Ix.toInt()]);
		goTo(GotoLabel.exit3090);
	}

protected void error3025()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3620);
		itemIO.setItemitem(billreqrec.billchnl);
		itemIO.setFunction(varcom.readr);
		itemIO.setStatuz(stnf);
		drylogrec.params.set(itemIO.getParams());
		drylogrec.dryDatabaseError.setTrue();
		a000FatalError();//ILPI-61 
	}

protected void readT36203030()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3620);
		itemIO.setItemitem(billreqrec.billchnl);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
	}

protected void setupCommonAcmvFields4100()
	{
		acmv4101();
	}

protected void acmv4101()
	{
		addacmvrec.stmtsort.set(LOVALUE);
		addacmvrec.transactionDate.set(ZERO);
		addacmvrec.transactionTime.set(ZERO);
		/*    Since both a debit and a credit ACMV record are required,*/
		/*    set up the fields that are common to the two.*/
		addacmvrec.batcpfx.set("BA");
		addacmvrec.genlcoy.set(billreqrec.company);
		addacmvrec.batccoy.set(billreqrec.company);
		addacmvrec.batcbrn.set(billreqrec.branch);
		addacmvrec.batcactyr.set(billreqrec.acctyear);
		addacmvrec.batcactmn.set(billreqrec.acctmonth);
		addacmvrec.batctrcde.set(billreqrec.trancode);
		addacmvrec.batcbatch.set(billreqrec.batch);
		addacmvrec.rdoccoy.set(billreqrec.chdrcoy);
		addacmvrec.rdocpfx.set(billreqrec.chdrpfx);
		addacmvrec.rdocnum.set(billreqrec.chdrnum);
		addacmvrec.tranref.set(billreqrec.chdrnum);
		addacmvrec.trandesc.set(descIO.getLongdesc());
		addacmvrec.origccy.set(billreqrec.billcurr);
		addacmvrec.genlcur.set(billreqrec.billcurr);
		addacmvrec.acctccy.set(billreqrec.billcurr);
		addacmvrec.crate.set(1);
		addacmvrec.origamt.set(billreqrec.instamt06);
		addacmvrec.acctamt.set(billreqrec.instamt06);
		addacmvrec.effdate.set(billreqrec.instfrom);
		addacmvrec.tranno.set(billreqrec.tranno);
		addacmvrec.termid.set(billreqrec.termid);
		addacmvrec.user.set(billreqrec.user);
		addacmvrec.creddte.set(varcom.maxdate);
		addacmvrec.function.set(SPACES);
		if (isNE(billreqrec.grpsnum, SPACES)) {
			statementSort4150();
		}
	}

protected void statementSort4150()
	{
		sort4151();
	}

protected void sort4151()
	{
		/*    Read GRPS to obtain group billing code.*/
		grpsIO.setGrupcoy(billreqrec.grpscoy);
		grpsIO.setGrupnum(billreqrec.grpsnum);
		grpsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, grpsIO);
		if (isNE(grpsIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(grpsIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		getT37004170();
		/*    The following sort works only if T3700 is set up such*/
		/*    that each field which is required in the sort is in*/
		/*    sequential order, i.e. 1,2,3. If the order is 1,3,4,*/
		/*    the sort may not work correctly. Ensure that T3700 does*/
		/*    not have missing numerical entries in the sort.*/
		wsaaT3700Sort[1].set(t3700rec.agntsort);
		wsaaT3700Sort[2].set(t3700rec.chdrsort);
		wsaaT3700Sort[3].set(t3700rec.duedsort);
		wsaaT3700Sort[4].set(t3700rec.membsort);
		wsaaT3700Sort[5].set(t3700rec.pnamsort);
		wsaaT3700Sort[6].set(t3700rec.pnumsort);
		/*    If any of the T3700 values are not numeric (i.e. spaces)*/
		/*    we must set them to zero in order to perform the sort.*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
			if (isNE(wsaaT3700Sort[wsaaSub.toInt()], NUMERIC)) {
				wsaaT3700Sort[wsaaSub.toInt()].set(ZERO);
			}
		}
		/*    If T3700-PNAMSORT is not equal to zero, we must read CLNT*/
		/*    to get the payer name.*/
		if (isNE(wsaaT3700Sort[5], 0)) {
			clntIO.setClntcoy(billreqrec.payrcoy);
			clntIO.setClntpfx(billreqrec.payrpfx);
			clntIO.setClntnum(billreqrec.payrnum);
			clntIO.setFormat(clntrec);
			clntIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, clntIO);
			if (isNE(clntIO.getStatuz(), varcom.oK)) {
				drylogrec.params.set(clntIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61 
			}
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(clntIO.getSurname(), SPACES);
			stringVariable1.addExpression(clntIO.getInitials(), SPACES);
			stringVariable1.setStringInto(billreqrec.payername);
		}
		wsaaBlrqField[1].set(billreqrec.agntnum);
		wsaaBlrqField[2].set(billreqrec.chdrnum);
		wsaaBlrqField[3].set(billreqrec.duedate);
		wsaaBlrqField[4].set(billreqrec.membsel);
		wsaaBlrqField[5].set(billreqrec.payername);
		wsaaBlrqField[6].set(billreqrec.payrnum);
		/*    Set the sort fields into the required T3700 sequence.*/
		wsaaTop.set(6);
		for (wsaaSub.set(1); !(isGT(wsaaSub, 6)); wsaaSub.add(1)){
			wsaaSortIx.set(wsaaT3700Sort[wsaaSub.toInt()]);
			if (isEQ(wsaaSortIx, 0)) {
				wsaaField[wsaaTop.toInt()].set(LOVALUE);
				wsaaTop.subtract(1);
			}
			else {
				wsaaField[wsaaSortIx.toInt()].set(wsaaBlrqField[wsaaSub.toInt()]);
			}
		}
		addacmvrec.stmtsort.set(LOVALUE);
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression(wsaaField[1], SPACES);
		stringVariable2.addExpression(wsaaField[2], SPACES);
		stringVariable2.addExpression(wsaaField[3], SPACES);
		stringVariable2.addExpression(wsaaField[4], SPACES);
		stringVariable2.addExpression(wsaaField[5], SPACES);
		stringVariable2.addExpression(wsaaField[6], SPACES);
		stringVariable2.setStringInto(addacmvrec.stmtsort);
	}

protected void getT37004170()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t37004171();
				case searchT37004172: 
					searchT37004172();
				case error4173: 
					error4173();
				case readT37004174: 
					readT37004174();
				case exit4179: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t37004171()
	{
		/*    Obtain Group Billing Sort Order from table T3700,*/
		/*    key is group billing sequence code*/
		if (isEQ(billreqrec.modeInd, "BATCH")) {
			goTo(GotoLabel.searchT37004172);
		}
		else {
			goTo(GotoLabel.readT37004174);
		}
	}

protected void searchT37004172()
	{
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3700ArrayInner.wsaaT3700Rec);
		as1.setIndices(wsaaT3700Ix);
		as1.addSearchKey(wsaaT3700ArrayInner.wsaaT3700Key, grpsIO.getBillseq(), true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			goTo(GotoLabel.error4173);
		}
		t3700rec.agntsort.set(wsaaT3700ArrayInner.wsaaT3700Agntsort[wsaaT3700Ix.toInt()]);
		t3700rec.chdrsort.set(wsaaT3700ArrayInner.wsaaT3700Chdrsort[wsaaT3700Ix.toInt()]);
		t3700rec.duedsort.set(wsaaT3700ArrayInner.wsaaT3700Duedsort[wsaaT3700Ix.toInt()]);
		t3700rec.membsort.set(wsaaT3700ArrayInner.wsaaT3700Membsort[wsaaT3700Ix.toInt()]);
		t3700rec.pnamsort.set(wsaaT3700ArrayInner.wsaaT3700Pnamsort[wsaaT3700Ix.toInt()]);
		t3700rec.pnumsort.set(wsaaT3700ArrayInner.wsaaT3700Pnumsort[wsaaT3700Ix.toInt()]);
		goTo(GotoLabel.exit4179);
	}

protected void error4173()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3700);
		itemIO.setItemitem(grpsIO.getBillseq());
		itemIO.setFunction(varcom.readr);
		itemIO.setStatuz(stnf);
		drylogrec.params.set(itemIO.getParams());
		drylogrec.dryDatabaseError.setTrue();
		a000FatalError();//ILPI-61 
	}

protected void readT37004174()
	{
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3700);
		itemIO.setItemitem(grpsIO.getBillseq());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t3700rec.t3700Rec.set(itemIO.getGenarea());
	}

protected void writeCreditAcmv4200()
	{
		try {
			credit4201();
			deferredUpdate4205();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void credit4201()
	{
		addacmvrec.transeq.set("0001");
		wsaaSacscode.set(billreqrec.sacscode01);
		getT36164250();
		addacmvrec.rldgpfx.set(t3616rec.subjPref);
		addacmvrec.rldgcoy.set(billreqrec.chdrcoy);
		addacmvrec.rldgacct.set(billreqrec.chdrnum);
		if (isNE(t3684rec.assumePaid, SPACES)) {
			addacmvrec.function.set(SPACES);
			addacmvrec.contot.set(billreqrec.contot01);
			addacmvrec.glcode.set(billreqrec.glmap01);
			addacmvrec.glsign.set(billreqrec.glsign01);
			addacmvrec.sacscode.set(billreqrec.sacscode01);
			addacmvrec.sacstyp.set(billreqrec.sacstype01);
		}
		else {
			addacmvrec.function.set("GLEDG");
			addacmvrec.contot.set(billreqrec.contot02);
			addacmvrec.glcode.set(billreqrec.glmap02);
			addacmvrec.glsign.set(billreqrec.glsign02);
			addacmvrec.sacscode.set(billreqrec.sacscode02);
			addacmvrec.sacstyp.set(billreqrec.sacstype02);
		}
		callProgram(Addacmv.class, addacmvrec.addacmvRec);
		if (isNE(addacmvrec.statuz, varcom.oK)) {
			drylogrec.params.set(addacmvrec.addacmvRec);
			drylogrec.statuz.set(addacmvrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isNE(t3684rec.assumePaid, SPACES)) {
			goTo(GotoLabel.exit4209);
		}
	}

protected void deferredUpdate4205()
	{
		/* A deferred ACBL update has been found, so set up the fields*/
		/* the ACAGRNL data area.*/
		/* Once this is complete, write a new DACM record using the*/
		/* ACAGRNL data area.*/
		acagrnlIO.setParams(SPACES);
		acagrnlIO.setRecKeyData(SPACES);
		acagrnlIO.setRecNonKeyData(SPACES);
		addacmvrec.rldgacct.set(billreqrec.grpsnum);
		addacmvrec.rldgcoy.set(billreqrec.grpscoy);
		acagrnlIO.setRldgcoy(billreqrec.chdrcoy);
		acagrnlIO.setSacscode(billreqrec.sacscode02);
		acagrnlIO.setRldgacct(billreqrec.chdrnum);
		acagrnlIO.setOrigcurr(billreqrec.billcurr);
		acagrnlIO.setSacstyp(billreqrec.sacstype02);
		acagrnlIO.setSacscurbal(billreqrec.instamt06);
		acagrnlIO.setRdocnum(billreqrec.chdrnum);
		acagrnlIO.setGlsign(billreqrec.glsign02);
		/* Set up the DACM record, used for deferred processing*/
		/* within the batch diary system.*/
		dacmIO.setParams(SPACES);
		dacmIO.setRecKeyData(SPACES);
		dacmIO.setRecNonKeyData(SPACES);
		dacmIO.setScheduleThreadNo(dryprcrec.threadNumber);
		dacmIO.setCompany(dryprcrec.company);
		dacmIO.setRecformat(acagrnlrec);
		dacmIO.setDataarea(acagrnlIO.getDataArea());
		dacmIO.setFormat(dacmrec);
		dacmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, dacmIO);
		if (isNE(dacmIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(dacmIO.getParams());
			drylogrec.statuz.set(dacmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
	}

protected void getT36164250()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t36164251();
				case searchT36164252: 
					searchT36164252();
				case error4253: 
					error4253();
				case readT36164254: 
					readT36164254();
				case exit4259: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t36164251()
	{
		/*    Read Table T3616 with SACSCODE as the key.*/
		if (isEQ(billreqrec.modeInd, "BATCH")) {
			goTo(GotoLabel.searchT36164252);
		}
		else {
			goTo(GotoLabel.readT36164254);
		}
	}

protected void searchT36164252()
	{
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3616Rec);
		as1.setIndices(wsaaT3616Ix);
		as1.addSearchKey(wsaaT3616Key, wsaaSacscode, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			goTo(GotoLabel.error4253);
		}
		t3616rec.subjPref.set(wsaaT3616SubjPref[wsaaT3616Ix.toInt()]);
		goTo(GotoLabel.exit4259);
	}

protected void error4253()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3616);
		itemIO.setItemitem(wsaaSacscode);
		itemIO.setFunction(varcom.readr);
		drylogrec.statuz.set(stnf);
		drylogrec.params.set(itemIO.getParams());
		drylogrec.dryDatabaseError.setTrue();
		a000FatalError();//ILPI-61 
	}

protected void readT36164254()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3616);
		itemIO.setItemitem(wsaaSacscode);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t3616rec.t3616Rec.set(itemIO.getGenarea());
	}

protected void writeDebitAcmv4300()
	{
		try {
			debit4301();
			deferredUpdate4305();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void debit4301()
	{
		addacmvrec.transeq.set("0002");
		wsaaSacscode.set(t3620rec.sacscode);
		getT36164250();
		addacmvrec.rldgpfx.set(t3616rec.subjPref);
		readT36984350();
		if (isNE(t3620rec.triggerRequired, SPACES)) {
			getT37014370();
		}
		/*    The Entity depends upon the T3620 table entry.*/
		addacmvrec.rldgacct.set(SPACES);
		addacmvrec.rldgcoy.set(SPACES);
		if (isNE(t3620rec.agntsdets, SPACES)) {
			addacmvrec.function.set(SPACES);
			addacmvrec.rldgacct.set(billreqrec.agntnum);
			addacmvrec.rldgcoy.set(billreqrec.agntcoy);
		}
		if (isNE(t3620rec.ddind, SPACES)
		|| isNE(t3620rec.payind, SPACES)
		|| isNE(t3620rec.crcind, SPACES)) {
			addacmvrec.function.set(SPACES);
			addacmvrec.rldgacct.set(billreqrec.payrnum);
			addacmvrec.rldgcoy.set(billreqrec.payrcoy);
		}
		if (isNE(t3620rec.grpind, SPACES)) {
			addacmvrec.function.set("GLEDG");
			addacmvrec.rldgacct.set(billreqrec.grpsnum);
			addacmvrec.rldgcoy.set(billreqrec.grpscoy);
		}
		addacmvrec.contot.set(t3698rec.cnttot[wsaaT3698Sub.toInt()]);
		addacmvrec.glcode.set(t3698rec.glmap[wsaaT3698Sub.toInt()]);
		addacmvrec.glsign.set(t3698rec.sign[wsaaT3698Sub.toInt()]);
		addacmvrec.sacscode.set(t3620rec.sacscode);
		addacmvrec.sacstyp.set(t3620rec.sacstyp);
		addacmvrec.reconsbr.set(t3701rec.reconsbr);
		callProgram(Addacmv.class, addacmvrec.addacmvRec);
		if (isNE(addacmvrec.statuz, varcom.oK)) {
			drylogrec.params.set(addacmvrec.addacmvRec);
			drylogrec.statuz.set(addacmvrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(t3620rec.grpind, SPACES)) {
			goTo(GotoLabel.exit4309);
		}
	}

protected void deferredUpdate4305()
	{
		/* A deferred ACBL update has been found, so set up the fields*/
		/* the ACAGRNL data area.*/
		/* Once this is complete, write a new DACM record using the*/
		/* ACAGRNL data area.*/
		acagrnlIO.setParams(SPACES);
		acagrnlIO.setRecKeyData(SPACES);
		acagrnlIO.setRecNonKeyData(SPACES);
		addacmvrec.rldgacct.set(billreqrec.grpsnum);
		addacmvrec.rldgcoy.set(billreqrec.grpscoy);
		acagrnlIO.setRldgcoy(billreqrec.chdrcoy);
		acagrnlIO.setSacscode(billreqrec.sacscode02);
		acagrnlIO.setRldgacct(billreqrec.chdrnum);
		acagrnlIO.setOrigcurr(billreqrec.billcurr);
		acagrnlIO.setSacstyp(billreqrec.sacstype02);
		acagrnlIO.setSacscurbal(billreqrec.instamt06);
		acagrnlIO.setRdocnum(billreqrec.chdrnum);
		acagrnlIO.setGlsign(billreqrec.glsign02);
		/* Set up the DACM record, used for deferred processing*/
		/* within the batch diary system.*/
		dacmIO.setParams(SPACES);
		dacmIO.setRecKeyData(SPACES);
		dacmIO.setRecNonKeyData(SPACES);
		dacmIO.setScheduleThreadNo(dryprcrec.threadNumber);
		dacmIO.setCompany(dryprcrec.company);
		dacmIO.setRecformat(acagrnlrec);
		dacmIO.setDataarea(acagrnlIO.getDataArea());
		dacmIO.setFormat(dacmrec);
		dacmIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, dacmIO);
		if (isNE(dacmIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(dacmIO.getParams());
			drylogrec.statuz.set(dacmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
	}

protected void readT36984350()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t36984351();
				case readSkipped4355: 
					readSkipped4355();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t36984351()
	{
		/*    If the trancode and sacscode have not changed, do not*/
		/*    re-read the table.*/
		if (isEQ(wsaaT3698Trancode, billreqrec.trancode)
		&& isEQ(wsaaT3698Sacscode, wsaaSacscode)) {
			goTo(GotoLabel.readSkipped4355);
		}
		/*    Read Table T3698 with SACSCODE as the key.*/
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3698);
		wsaaT3698Trancode.set(billreqrec.trancode);
		wsaaT3698Sacscode.set(wsaaSacscode);
		itemIO.setItemitem(wsaaT3698Itemkey);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t3698rec.t3698Rec.set(itemIO.getGenarea());
	}

protected void readSkipped4355()
	{
		if (isEQ(t3698rec.sacstyp[wsaaT3698Sub.toInt()], t3620rec.sacstyp)) {
			return ;
		}
		wsaaMapFound = "N";
		for (wsaaT3698Sub.set(1); !(isGT(wsaaT3698Sub, 20)
		|| isEQ(wsaaMapFound, "Y")); wsaaT3698Sub.add(1)){
			if (isEQ(t3698rec.sacstyp[wsaaT3698Sub.toInt()], t3620rec.sacstyp)) {
				wsaaMapFound = "Y";
			}
		}
		/*    If the sacstype is not set up on the table produce error*/
		if (isNE(wsaaMapFound, "Y")) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set("NFND");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
	}

protected void getT37014370()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					t37014371();
				case searchT37014372: 
					searchT37014372();
				case searchAgain4373: 
					searchAgain4373();
				case error4374: 
					error4374();
				case readT37014375: 
					readT37014375();
				case exit4379: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void t37014371()
	{
		/*    Read Table T3701. Use transcode joined with contract*/
		/*    type as the key. If not found, read again with contract*/
		/*    type wildcard.*/
		if (isEQ(billreqrec.modeInd, "BATCH")) {
			goTo(GotoLabel.searchT37014372);
		}
		else {
			goTo(GotoLabel.readT37014375);
		}
	}

protected void searchT37014372()
	{
		wsaaT3701Trancode2.set(billreqrec.trancode);
		wsaaT3701Cnttype2.set(billreqrec.cnttype);
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3701Rec);
		as1.setIndices(wsaaT3701Ix);
		as1.addSearchKey(wsaaT3701Key, wsaaT3701Key2, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			goTo(GotoLabel.searchAgain4373);
		}
		t3701rec.reconsbr.set(wsaaT3701Reconsbr[wsaaT3701Ix.toInt()]);
		goTo(GotoLabel.exit4379);
	}

protected void searchAgain4373()
	{
		wsaaT3701Cnttype2.set("***");
		ArraySearch as1 = ArraySearch.getInstance(wsaaT3701Rec);
		as1.setIndices(wsaaT3701Ix);
		as1.addSearchKey(wsaaT3701Key, wsaaT3701Key2, true);
		if (as1.binarySearch()) {
			/*CONTINUE_STMT*/
		}
		else {
			goTo(GotoLabel.error4374);
		}
		goTo(GotoLabel.exit4379);
	}

protected void error4374()
	{
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3701);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(billreqrec.trancode);
		stringVariable1.addExpression(billreqrec.cnttype);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setFunction(varcom.readr);
		itemIO.setStatuz(stnf);
		drylogrec.params.set(itemIO.getParams());
		drylogrec.dryDatabaseError.setTrue();
		a000FatalError();//ILPI-61 
	}

protected void readT37014375()
	{
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(billreqrec.trancode);
		stringVariable1.addExpression(billreqrec.cnttype);
		stringVariable1.setStringInto(itemIO.getItemitem());
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(billreqrec.company);
		itemIO.setItemtabl(t3701);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(billreqrec.trancode);
			stringVariable2.addExpression("***");
			stringVariable2.setStringInto(itemIO.getItemitem());
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
		}
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t3701rec.t3701Rec.set(itemIO.getGenarea());
	}

protected void produceBextRecords5000()
	{
		bext5010();
	}

protected void bext5010()
	{
		/*    Write BEXT record.*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, ztrnamtcpy.nofAmts)); wsaaSub.add(1)){
			bextIO.setInstamt(wsaaSub, billreqrec.instamt[wsaaSub.toInt()]);
		}
		wsaaGrpscoy.set(billreqrec.grpscoy);
		wsaaGrpsnum.set(billreqrec.grpsnum);
		bextIO.setChdrpfx(billreqrec.chdrpfx);
		bextIO.setChdrcoy(billreqrec.chdrcoy);
		bextIO.setChdrnum(billreqrec.chdrnum);
		bextIO.setServunit(billreqrec.servunit);
		bextIO.setCnttype(billreqrec.cnttype);
		bextIO.setCntcurr(billreqrec.billcurr);
		bextIO.setOccdate(billreqrec.occdate);
		bextIO.setCcdate(billreqrec.ccdate);
		bextIO.setPtdate(billreqrec.ptdate);
		bextIO.setBtdate(billreqrec.btdate);
		bextIO.setBilldate(billreqrec.billdate);
		bextIO.setBillchnl(billreqrec.billchnl);
		bextIO.setBankcode(billreqrec.bankcode);
		bextIO.setInstfrom(billreqrec.instfrom);
		bextIO.setInstto(billreqrec.instto);
		bextIO.setInstbchnl(billreqrec.instbchnl);
		bextIO.setInstcchnl(billreqrec.instcchnl);
		bextIO.setInstfreq(billreqrec.instfreq);
		bextIO.setInstjctl(billreqrec.instjctl);
		bextIO.setGrupkey(wsaaGrupkey);
		bextIO.setMembsel(billreqrec.membsel);
		bextIO.setFacthous(billreqrec.facthous);
		bextIO.setBankkey(billreqrec.bankkey);
		bextIO.setBankacckey(billreqrec.bankacckey);
		bextIO.setCownpfx(billreqrec.cownpfx);
		bextIO.setCowncoy(billreqrec.cowncoy);
		bextIO.setCownnum(billreqrec.cownnum);
		bextIO.setPayrpfx(billreqrec.payrpfx);
		bextIO.setPayrcoy(billreqrec.payrcoy);
		bextIO.setPayrnum(billreqrec.payrnum);
		bextIO.setCntbranch(billreqrec.cntbranch);
		bextIO.setAgntpfx(billreqrec.agntpfx);
		bextIO.setAgntcoy(billreqrec.agntcoy);
		bextIO.setAgntnum(billreqrec.agntnum);
		bextIO.setPayflag(billreqrec.payflag);
		bextIO.setBilflag(billreqrec.bilflag);
		bextIO.setOutflag(billreqrec.outflag);
		bextIO.setSupflag(billreqrec.supflag);
		bextIO.setBillcd(billreqrec.billcd);
		bextIO.setMandref(billreqrec.mandref);
		bextIO.setSacscode(billreqrec.sacscode01);
		bextIO.setSacstyp(billreqrec.sacstype01);
		bextIO.setGlmap(billreqrec.glmap01);
		bextIO.setMandstat(billreqrec.mandstat);
		bextIO.setDdderef(ZERO);
		bextIO.setEffdatex(ZERO);
		bextIO.setJobno(ZERO);
		bextIO.setNextdate(billreqrec.nextdate);
		bextIO.setFunction(varcom.writr);
		bextIO.setFormat(bextrec);
		SmartFileCode.execute(appVars, bextIO);
		if (isNE(bextIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(bextIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
	}
//Modify for ILPI-61
//protected void fatalError()
//	{
//		a010Fatal();
//		//a090Exit();//Modify for ILPI-65 
//	}
//
//protected void a010Fatal()
//	{
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(dryprcrec.runDate);
//		drylogrec.entityKey.set(dryprcrec.entityKey);
//		if (dryprcrec.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(dryprcrec.runNumber);
//		}
//		drylogrec.language.set(dryprcrec.language);
//		dryprcrec.statuz.set(drylogrec.statuz);
//		dryprcrec.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		billreqrec.statuz.set(varcom.bomb);
//		a000FatalError();
//	} 

//protected void a090Exit()
//	{
//		exitProgram();
//	}
/*
 * Class transformed  from Data Structure WSAA-T3620-ARRAY--INNER
 */
private static final class WsaaT3620ArrayInner { 

		/* WSAA-T3620-ARRAY */
	private FixedLengthStringData[] wsaaT3620Rec = FLSInittedArray (30, 13);
	private FixedLengthStringData[] wsaaT3620Key = FLSDArrayPartOfArrayStructure(1, wsaaT3620Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3620Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT3620Key, 0);
	private FixedLengthStringData[] wsaaT3620Data = FLSDArrayPartOfArrayStructure(12, wsaaT3620Rec, 1);
	private FixedLengthStringData[] wsaaT3620Agntsdets = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 0);
	private FixedLengthStringData[] wsaaT3620Ddind = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 1);
	private FixedLengthStringData[] wsaaT3620Crcind = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 2);
	private FixedLengthStringData[] wsaaT3620Grpind = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 3);
	private FixedLengthStringData[] wsaaT3620MediaRequired = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 4);
	private FixedLengthStringData[] wsaaT3620Payind = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 5);
	private FixedLengthStringData[] wsaaT3620Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT3620Data, 6);
	private FixedLengthStringData[] wsaaT3620Sacstyp = FLSDArrayPartOfArrayStructure(2, wsaaT3620Data, 8);
	private FixedLengthStringData[] wsaaT3620TriggerRequired = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 10);
	private FixedLengthStringData[] wsaaT3620AccInd = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 11);
}
/*
 * Class transformed  from Data Structure WSAA-T3700-ARRAY--INNER
 */
private static final class WsaaT3700ArrayInner { 

		/* WSAA-T3700-ARRAY */
	private FixedLengthStringData[] wsaaT3700Rec = FLSInittedArray (30, 501);
	private FixedLengthStringData[] wsaaT3700Key = FLSDArrayPartOfArrayStructure(1, wsaaT3700Rec, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT3700Billseq = FLSDArrayPartOfArrayStructure(1, wsaaT3700Key, 0);
	private FixedLengthStringData[] wsaaT3700Data = FLSDArrayPartOfArrayStructure(500, wsaaT3700Rec, 1);
	private ZonedDecimalData[] wsaaT3700Agntsort = ZDArrayPartOfArrayStructure(1, 0, wsaaT3700Data, 0);
	private ZonedDecimalData[] wsaaT3700Chdrsort = ZDArrayPartOfArrayStructure(1, 0, wsaaT3700Data, 1);
	private ZonedDecimalData[] wsaaT3700Duedsort = ZDArrayPartOfArrayStructure(1, 0, wsaaT3700Data, 2);
	private ZonedDecimalData[] wsaaT3700Membsort = ZDArrayPartOfArrayStructure(1, 0, wsaaT3700Data, 3);
	private ZonedDecimalData[] wsaaT3700Pnamsort = ZDArrayPartOfArrayStructure(1, 0, wsaaT3700Data, 4);
	private ZonedDecimalData[] wsaaT3700Pnumsort = ZDArrayPartOfArrayStructure(1, 0, wsaaT3700Data, 5);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
