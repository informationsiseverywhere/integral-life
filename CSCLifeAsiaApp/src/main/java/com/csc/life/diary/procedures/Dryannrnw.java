/*
 * File: Dryannrnw.java
 * Date: December 3, 2013 2:23:45 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYANNRNW.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diaryframework.parent.Maind;
import com.csc.life.diary.dataaccess.CovrrnwTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the transaction detail record change subroutine for
* Coverage Anniversary Renewal Processing.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryannrnw extends Maind {//Modify for IPLI-65

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYANNRNW");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();
	private static final int wsaaT5679Max = 12;
	private PackedDecimalData wsaaNxtprcdate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaCovrStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaCovrStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaCovrStatus, "N");
		/* FORMATS */
	private static final String covrrnwrec = "COVRRNWREC";
	private static final String f321 = "F321";
		/* TABLES */
	private static final String t5679 = "T5679";
	private CovrrnwTableDAM covrrnwIO = new CovrrnwTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for IPLI-65
	private T5679rec t5679rec = new T5679rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr180, 
		exit190
	}

	public Dryannrnw() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		wsaaNxtprcdate.set(varcom.vrcmMaxDate);
		/* Check whether it is Single Premium policy.                      */
		if (isEQ(drypDryprcRecInner.drypBillchnl, drypDryprcRecInner.drypSystParm[2])) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		/* Read table T5679 to determine if coverages have a valid status.*/
		readT5679200();
		/* Read through the coverage records for the contract using the*/
		/* COVRRNW logical to get the earliest valid Anniversary Process*/
		/* Date for the Diary Transaction Record.*/
		covrrnwIO.setParams(SPACES);
		covrrnwIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covrrnwIO.setChdrnum(drypDryprcRecInner.drypEntity);
		covrrnwIO.setAnnivProcDate(ZERO);
		covrrnwIO.setFormat(covrrnwrec);
		covrrnwIO.setFunction(varcom.begn);
		while ( !(isEQ(covrrnwIO.getStatuz(), varcom.endp))) {
			readCoverages100();
		}
		
		drypDryprcRecInner.drypNxtprcdate.set(wsaaNxtprcdate);
		drypDryprcRecInner.drypNxtprctime.set(ZERO);
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readCoverages100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					call110();
				case nextr180: 
					nextr180();
				case exit190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void call110()
	{
		SmartFileCode.execute(appVars, covrrnwIO);
		if (isNE(covrrnwIO.getStatuz(), varcom.oK)
		&& isNE(covrrnwIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covrrnwIO.getParams());
			drylogrec.statuz.set(covrrnwIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//Modify for IPLI-61
		}
		if (isNE(covrrnwIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(covrrnwIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isEQ(covrrnwIO.getStatuz(), varcom.endp)) {
			if (isEQ(covrrnwIO.getFunction(), varcom.begn)) {
				drypDryprcRecInner.dtrdNo.setTrue();
			}
			covrrnwIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit190);
		}
		checkCovrStatus300();
		if (invalidStatus.isTrue()) {
			goTo(GotoLabel.nextr180);
		}
		if (isEQ(covrrnwIO.getAnnivProcDate(), covrrnwIO.getRiskCessDate())) {
			goTo(GotoLabel.nextr180);
		}
		if (isLT(covrrnwIO.getAnnivProcDate(), wsaaNxtprcdate)) {
			wsaaNxtprcdate.set(covrrnwIO.getAnnivProcDate());
		}
	}

protected void nextr180()
	{
		covrrnwIO.setFunction(varcom.nextr);
	}

protected void readT5679200()
	{
		
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		Itempf itemIO = new Itempf();
		itemIO.setItempfx(smtpfxcpy.item.toString());
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany.toString());
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key.toString());
		itemIO = itemDAO.getItempfRecord(itemIO);
		
		if (itemIO == null) {
			drylogrec.params.set(drypDryprcRecInner.drypCompany.toString().concat(t5679).concat(wsaaT5679Key.toString()));
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itemIO.getGenarea()));
	}

protected void checkCovrStatus300()
	{
		/*CHECK*/
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(covrrnwIO.getStatcode(), t5679rec.covRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		/*EXIT*/
	}

//ILPI-61
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
}
//IPLI-65
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
