package com.csc.life.diary.reports;

import static com.quipoz.COBOLFramework.COBOLFunctions.getTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import com.csc.smart400framework.printing.SMARTReportLayout;
import com.quipoz.COBOLFramework.util.COBOLAppVars;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * Report converted from R537003.prtf
 * This class will generate the XML file for the report.
 * @version 1.0 generated on 12/3/13 4:20 AM
 * @author CSC
 */
public class R537003Report extends SMARTReportLayout { 

	private FixedLengthStringData branch = new FixedLengthStringData(2);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData company = new FixedLengthStringData(1);
	private FixedLengthStringData companynm = new FixedLengthStringData(30);
	private FixedLengthStringData coverage = new FixedLengthStringData(2);
	private FixedLengthStringData currcd = new FixedLengthStringData(3);
	private FixedLengthStringData dateReportVariable = new FixedLengthStringData(10);
	private FixedLengthStringData jnumb = new FixedLengthStringData(8);
	private FixedLengthStringData life = new FixedLengthStringData(2);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30);
	private FixedLengthStringData longstr = new FixedLengthStringData(30);
	private ZonedDecimalData pagnbr = new ZonedDecimalData(6, 0);
	private FixedLengthStringData payreason = new FixedLengthStringData(2);
	private ZonedDecimalData prcnt = new ZonedDecimalData(5, 2);
	private ZonedDecimalData pymt = new ZonedDecimalData(17, 2);
	private FixedLengthStringData repdate = new FixedLengthStringData(10);
	private ZonedDecimalData rgpynum = new ZonedDecimalData(5, 0);
	private FixedLengthStringData rgpystat = new FixedLengthStringData(2);
	private FixedLengthStringData rgpytype = new FixedLengthStringData(2);
	private FixedLengthStringData rider = new FixedLengthStringData(2);
	private FixedLengthStringData rstate = new FixedLengthStringData(10);
	private FixedLengthStringData sdate = new FixedLengthStringData(10);
	private FixedLengthStringData time = new FixedLengthStringData(8);

	private COBOLAppVars appVars = (COBOLAppVars)COBOLAppVars.getInstance();

	/**
	 * Constructors
	 */

	public R537003Report() {
		super();
	}


	/**
	 * Print the XML for R537003d01
	 */
	public void printR537003d01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 13, 2));
		rgpynum.setFieldName("rgpynum");
		rgpynum.setInternal(subString(recordData, 15, 5));
		pymt.setFieldName("pymt");
		pymt.setInternal(subString(recordData, 20, 17));
		currcd.setFieldName("currcd");
		currcd.setInternal(subString(recordData, 37, 3));
		prcnt.setFieldName("prcnt");
		prcnt.setInternal(subString(recordData, 40, 5));
		rgpytype.setFieldName("rgpytype");
		rgpytype.setInternal(subString(recordData, 45, 2));
		payreason.setFieldName("payreason");
		payreason.setInternal(subString(recordData, 47, 2));
		rgpystat.setFieldName("rgpystat");
		rgpystat.setInternal(subString(recordData, 49, 2));
		dateReportVariable.setFieldName("dateReportVariable");
		dateReportVariable.setInternal(subString(recordData, 51, 10));
		printLayout("R537003d01",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				rider,
				rgpynum,
				pymt,
				currcd,
				prcnt,
				rgpytype,
				payreason,
				rgpystat,
				dateReportVariable
			}
		);

	}

	/**
	 * Print the XML for R537003d02
	 */
	public void printR537003d02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(1);

		chdrnum.setFieldName("chdrnum");
		chdrnum.setInternal(subString(recordData, 1, 8));
		life.setFieldName("life");
		life.setInternal(subString(recordData, 9, 2));
		coverage.setFieldName("coverage");
		coverage.setInternal(subString(recordData, 11, 2));
		rider.setFieldName("rider");
		rider.setInternal(subString(recordData, 13, 2));
		rgpynum.setFieldName("rgpynum");
		rgpynum.setInternal(subString(recordData, 15, 5));
		pymt.setFieldName("pymt");
		pymt.setInternal(subString(recordData, 20, 17));
		currcd.setFieldName("currcd");
		currcd.setInternal(subString(recordData, 37, 3));
		prcnt.setFieldName("prcnt");
		prcnt.setInternal(subString(recordData, 40, 5));
		rgpytype.setFieldName("rgpytype");
		rgpytype.setInternal(subString(recordData, 45, 2));
		payreason.setFieldName("payreason");
		payreason.setInternal(subString(recordData, 47, 2));
		rgpystat.setFieldName("rgpystat");
		rgpystat.setInternal(subString(recordData, 49, 2));
		longdesc.setFieldName("longdesc");
		longdesc.setInternal(subString(recordData, 51, 30));
		printLayout("R537003d02",			// Record name
			new BaseData[]{			// Fields:
				chdrnum,
				life,
				coverage,
				rider,
				rgpynum,
				pymt,
				currcd,
				prcnt,
				rgpytype,
				payreason,
				rgpystat,
				longdesc
			}
		);

	}

	/**
	 * Print the XML for R537003d03
	 */
	public void printR537003d03(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.add(4);

		longstr.setFieldName("longstr");
		longstr.setInternal(subString(recordData, 1, 30));
		printLayout("R537003d03",			// Record name
			new BaseData[]{			// Fields:
				longstr
			}
		);

	}

	/**
	 * Print the XML for R537003h01
	 */
	public void printR537003h01(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.set(3);

		rstate.setFieldName("rstate");
		rstate.setInternal(subString(recordData, 1, 10));
		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 11, 10));
		jnumb.setFieldName("jnumb");
		jnumb.setInternal(subString(recordData, 21, 8));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		company.setFieldName("company");
		company.setInternal(subString(recordData, 29, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 30, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 60, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 70, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 72, 30));
		time.setFieldName("time");
		time.set(getTime());
		printLayout("R537003h01",			// Record name
			new BaseData[]{			// Fields:
				rstate,
				repdate,
				jnumb,
				pagnbr,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time
			}
		);

	}

	/**
	 * Print the XML for R537003h02
	 */
	public void printR537003h02(FixedLengthStringData... printData) {

		//extract parameters from input variable parameter
		FixedLengthStringData recordData = null;
		FixedLengthStringData indicArea = null;
		if (printData.length == 1) {
			recordData = printData[0];
		} else if (printData.length == 2) {
			recordData = printData[0];
			indicArea = printData[1];
		} else {
			throw new RuntimeException("Parameter length is not correct");
		}

		currentPrintLine.set(3);

		rstate.setFieldName("rstate");
		rstate.setInternal(subString(recordData, 1, 10));
		repdate.setFieldName("repdate");
		repdate.setInternal(subString(recordData, 11, 10));
		jnumb.setFieldName("jnumb");
		jnumb.setInternal(subString(recordData, 21, 8));
		pagnbr.setFieldName("pagnbr");
		pagnbr.set(pageNumber);
		company.setFieldName("company");
		company.setInternal(subString(recordData, 29, 1));
		companynm.setFieldName("companynm");
		companynm.setInternal(subString(recordData, 30, 30));
		sdate.setFieldName("sdate");
		sdate.setInternal(subString(recordData, 60, 10));
		branch.setFieldName("branch");
		branch.setInternal(subString(recordData, 70, 2));
		branchnm.setFieldName("branchnm");
		branchnm.setInternal(subString(recordData, 72, 30));
		time.setFieldName("time");
		time.set(getTime());
		printLayout("R537003h02",			// Record name
			new BaseData[]{			// Fields:
				rstate,
				repdate,
				jnumb,
				pagnbr,
				company,
				companynm,
				sdate,
				branch,
				branchnm,
				time
			}
		);

	}


}
