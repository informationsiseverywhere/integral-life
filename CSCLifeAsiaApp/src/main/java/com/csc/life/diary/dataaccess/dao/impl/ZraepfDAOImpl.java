package com.csc.life.diary.dataaccess.dao.impl;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import com.csc.life.diary.dataaccess.dao.ZraepfDAO;
import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

public class ZraepfDAOImpl extends BaseDAOImpl<Zraepf> implements ZraepfDAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(ZraepfDAOImpl.class);
	
	public List<Zraepf> getZraeData(Zraepf zraepfModel){
		StringBuilder sql = new StringBuilder(
				"SELECT CHDRCOY, CHDRNUM, NPAYDATE, LIFE, COVERAGE, RIDER, PLNSFX from ZRAEDTE  WHERE CHDRCOY=? and CHDRNUM=?");
		ResultSet rs = null;
		List<Zraepf> list = new ArrayList<Zraepf>();
		Zraepf zraepf;
		
		try {
			PreparedStatement stmt = getPrepareStatement(sql.toString());
			stmt.setString(1, zraepfModel.getChdrcoy());
			stmt.setString(2, zraepfModel.getChdrnum());
			rs = stmt.executeQuery();

			while (rs.next()) {
				zraepf = new Zraepf();
				zraepf.setChdrcoy(rs.getString(1));
				zraepf.setChdrnum(rs.getString(2));
				zraepf.setNpaydate(rs.getInt(3));
				zraepf.setLife(rs.getString(4));
				zraepf.setCoverage(rs.getString(5));
				zraepf.setRider(rs.getString(6));
				zraepf.setPlnsfx(rs.getInt(7));
				list.add(zraepf);
			}

		} 
		catch (SQLException e) {
		LOGGER.error("error has occured in getZraeData", e);
		}
		return list;
	}
	
	public boolean updateZrae(List<Zraepf> zraeBulkOpList) {
		if (zraeBulkOpList != null && !zraeBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE ZRAEPF SET APNXTINTBDTE=?,APNXTCAPDATE=?,APCAPLAMT=?,JOBNM=?,USRPRF=?,DATIME=?,APINTAMT=?,APLSTINTBDTE=?,APLSTCAPDATE=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psZraeUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Zraepf r : zraeBulkOpList) {
					psZraeUpdate.setInt(1, r.getAplstcapdate());
					psZraeUpdate.setInt(2, r.getApnxtcapdate());
					psZraeUpdate.setBigDecimal(3, r.getApcaplamt());
					psZraeUpdate.setString(4, getJobnm());
					psZraeUpdate.setString(5, getUsrprf());
					psZraeUpdate.setTimestamp(6, getDatime());
					psZraeUpdate.setBigDecimal(7, r.getApintamt());
					psZraeUpdate.setInt(8, r.getAplstintbdte());
					psZraeUpdate.setInt(9, r.getAplstcapdate());
					psZraeUpdate.setLong(10, r.getUnique_number());
					psZraeUpdate.addBatch();
				}
				psZraeUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateZrae()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psZraeUpdate, null);
			}
		}
		return true;
	}
	
	public boolean updateZraeRecIntDate(List<Zraepf> zraeBulkOpList) {
		if (zraeBulkOpList != null && !zraeBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE ZRAEPF SET APNXTINTBDTE=?,APLSTINTBDTE=?,JOBNM=?,USRPRF=?,DATIME=?,APINTAMT=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psZraeUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Zraepf r : zraeBulkOpList) {
					psZraeUpdate.setInt(1, r.getApnxtintbdte());
					psZraeUpdate.setInt(2, r.getAplstintbdte());
					psZraeUpdate.setString(3, getJobnm());
					psZraeUpdate.setString(4, getUsrprf());
					psZraeUpdate.setTimestamp(5, getDatime());
					psZraeUpdate.setBigDecimal(6, r.getApintamt());
					psZraeUpdate.setLong(7, r.getUnique_number());
					psZraeUpdate.addBatch();
				}
				psZraeUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateRegpRecIntDate()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psZraeUpdate, null);
			}
		}
		return true;
	}
	
	
	public List<Zraepf> searchZraepfRecord(List<String> chdrnumList, String coy) {	

		StringBuilder sqlSelect = new StringBuilder();

		sqlSelect.append("SELECT UNIQUE_NUMBER, ");
		sqlSelect.append("CHDRCOY, CHDRNUM, PLNSFX, LIFE, COVERAGE, ");
		sqlSelect.append("RIDER, APCAPLAMT, APLSTCAPDATE, APNXTCAPDATE, APLSTINTBDTE, ");
		sqlSelect.append("APNXTINTBDTE, VALIDFLAG,FLAG, TRANNO, USRPRF, JOBNM,TOTAMNT, ");
		sqlSelect.append("WDRGPYMOP,WDBANKKEY,WDBANKACCKEY,PAYCURR,");
		sqlSelect.append("DATIME, BANKKEY, BANKACCKEY, PAYCLT ");
		sqlSelect.append("FROM ZRAEPF WHERE  FLAG = '1' AND VALIDFLAG='1' AND CHDRCOY = ? AND ");
		sqlSelect.append(getSqlInStr(" CHDRNUM", chdrnumList));
		sqlSelect.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,UNIQUE_NUMBER DESC ");


		PreparedStatement psRpdetpfSelect = getPrepareStatement(sqlSelect.toString());
		ResultSet sqlRpdetpfRs = null;
		List<Zraepf> outputList = new ArrayList<Zraepf>();

		try {

			psRpdetpfSelect.setString(1, coy);
			sqlRpdetpfRs = executeQuery(psRpdetpfSelect);

			while (sqlRpdetpfRs.next()) {

				Zraepf zraepf = new Zraepf();

				zraepf.setUnique_number(sqlRpdetpfRs.getInt("UNIQUE_NUMBER"));
				zraepf.setChdrcoy(sqlRpdetpfRs.getString("CHDRCOY"));
				zraepf.setChdrnum(sqlRpdetpfRs.getString("CHDRNUM"));
				zraepf.setPlnsfx(sqlRpdetpfRs.getInt("PLNSFX"));
				zraepf.setLife(sqlRpdetpfRs.getString("LIFE"));
				zraepf.setCoverage(sqlRpdetpfRs.getString("COVERAGE"));
				zraepf.setRider(sqlRpdetpfRs.getString("RIDER"));
				zraepf.setApcaplamt(sqlRpdetpfRs.getBigDecimal("APCAPLAMT"));
				zraepf.setAplstcapdate(sqlRpdetpfRs.getInt("APLSTCAPDATE"));
				zraepf.setApnxtcapdate(sqlRpdetpfRs.getInt("APNXTCAPDATE"));
				zraepf.setAplstintbdte(sqlRpdetpfRs.getInt("APLSTINTBDTE"));
				zraepf.setApnxtintbdte(sqlRpdetpfRs.getInt("APNXTINTBDTE"));
				zraepf.setValidflag(sqlRpdetpfRs.getString("VALIDFLAG"));
				zraepf.setFlag(sqlRpdetpfRs.getString("FLAG"));
				zraepf.setTranno(sqlRpdetpfRs.getInt("TRANNO"));
				zraepf.setUsrprf(sqlRpdetpfRs.getString("USRPRF"));
				zraepf.setJobnm(sqlRpdetpfRs.getString("JOBNM"));
				zraepf.setDatime(sqlRpdetpfRs.getString("DATIME"));
				zraepf.setTotamnt(sqlRpdetpfRs.getInt("TOTAMNT"));
				zraepf.setWdrgpymop(sqlRpdetpfRs.getString("WDRGPYMOP")==null?" ":sqlRpdetpfRs.getString("WDRGPYMOP"));
				zraepf.setWdbankkey(sqlRpdetpfRs.getString("WDBANKKEY")==null?" ":sqlRpdetpfRs.getString("WDBANKKEY"));
				zraepf.setWdbankacckey(sqlRpdetpfRs.getString("WDBANKACCKEY")==null?" ":sqlRpdetpfRs.getString("WDBANKACCKEY"));
				zraepf.setPaycurr(sqlRpdetpfRs.getString("PAYCURR"));
				zraepf.setBankkey(sqlRpdetpfRs.getString("BANKKEY"));
				zraepf.setBankacckey(sqlRpdetpfRs.getString("BANKACCKEY"));
				zraepf.setPayclt(sqlRpdetpfRs.getString("PAYCLT"));
				
				outputList.add(zraepf);
			}

		} catch (SQLException e) {
			LOGGER.error("searchZraepfRecord()", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(psRpdetpfSelect, sqlRpdetpfRs);
		}

		return outputList;
	}
	
	
	public boolean updateZraeRecCapDate(List<Zraepf> zraeBulkOpList) {
		if (zraeBulkOpList != null && !zraeBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE ZRAEPF SET APNXTINTBDTE=?,APNXTCAPDATE=?,APCAPLAMT=?,JOBNM=?,USRPRF=?,DATIME=?,APINTAMT=?,FLAG='2',TOTAMNT=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Zraepf r : zraeBulkOpList) {
					psRegpUpdate.setInt(1, r.getApnxtintbdte());
					psRegpUpdate.setInt(2, r.getApnxtcapdate());
					psRegpUpdate.setBigDecimal(3, r.getApcaplamt());
					psRegpUpdate.setString(4, getJobnm());
					psRegpUpdate.setString(5, getUsrprf());
					psRegpUpdate.setTimestamp(6, getDatime());
					psRegpUpdate.setBigDecimal(7, r.getApintamt());
					psRegpUpdate.setInt(8, r.getTotamnt());
					psRegpUpdate.setLong(9, r.getUnique_number());
					psRegpUpdate.addBatch();
				}
				psRegpUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateZraeRecCapDate()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpUpdate, null);
			}
		}
		return true;
	}
	
	public boolean updateValidflag(List<Zraepf> zraeBulkOpList) {
		if (zraeBulkOpList != null && !zraeBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE ZRAEPF SET VALIDFLAG='2' WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Zraepf r : zraeBulkOpList) {
					
					psRegpUpdate.setLong(1, r.getUnique_number());
					psRegpUpdate.addBatch();
				}
				psRegpUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateZraeRecCapDate()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpUpdate, null);
			}
		}
		return true;
	}

	
	public void insertZraeRecord(Zraepf zraepf) {
                  
		String stmt  = "INSERT INTO ZRAEPF(CHDRCOY,CHDRNUM,LIFE,COVERAGE,RIDER,PLNSFX,VALIDFLAG,TRANNO,CURRFROM,CURRTO,ZRDUEDTE01,ZRDUEDTE02,"
				+ "ZRDUEDTE03,ZRDUEDTE04,ZRDUEDTE05,ZRDUEDTE06,ZRDUEDTE07,ZRDUEDTE08,PRCNT01,PRCNT02,PRCNT03,PRCNT04,PRCNT05,PRCNT06,PRCNT07,"
				+ "PRCNT08,PAYDTE01,PAYDTE02,PAYDTE03,PAYDTE04,PAYDTE05,PAYDTE06,PAYDTE07,PAYDTE08,PAID01,PAID02,PAID03,PAID04,PAID05,PAID06,"
				+ "PAID07,PAID08,PAYMMETH01,PAYMMETH02,PAYMMETH03,PAYMMETH04,PAYMMETH05,PAYMMETH06,PAYMMETH07,PAYMMETH08,ZRPAYOPT01,"
				+ "ZRPAYOPT02,ZRPAYOPT03,ZRPAYOPT04,ZRPAYOPT05,ZRPAYOPT06,ZRPAYOPT07,ZRPAYOPT08,TOTAMNT,NPAYDATE,PAYCLT,BANKKEY,"
				+ "BANKACCKEY,PAYCURR,USRPRF,JOBNM,DATIME,FLAG,APCAPLAMT,APINTAMT,APLSTCAPDATE,APNXTCAPDATE,APLSTINTBDTE,APNXTINTBDTE,"
				+ "WDRGPYMOP,WDBANKKEY,WDBANKACCKEY) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
       
            PreparedStatement psZraeInsert = getPrepareStatement(stmt);
            int count;
            try {        	    
            	    psZraeInsert.setString(1,zraepf.getChdrcoy());	
            	    psZraeInsert.setString(2,zraepf.getChdrnum());	
            	    psZraeInsert.setString(3,zraepf.getLife());	
            	    psZraeInsert.setString(4,zraepf.getCoverage());	
            	    psZraeInsert.setString(5,zraepf.getRider());	
            	    psZraeInsert.setInt(6,zraepf.getPlnsfx());	
            	    psZraeInsert.setString(7,zraepf.getValidflag());	
            	    psZraeInsert.setInt(8,zraepf.getTranno());	
            	    psZraeInsert.setInt(9,zraepf.getCurrfrom());	
            	    psZraeInsert.setInt(10,zraepf.getCurrto());	
            	    psZraeInsert.setInt(11,zraepf.getZrduedte01());	
            	    psZraeInsert.setInt(12,zraepf.getZrduedte02());	
            	    psZraeInsert.setInt(13,zraepf.getZrduedte03());	
            	    psZraeInsert.setInt(14,zraepf.getZrduedte04());	
            	    psZraeInsert.setInt(15,zraepf.getZrduedte05());	
            	    psZraeInsert.setInt(16,zraepf.getZrduedte06());	
            	    psZraeInsert.setInt(17,zraepf.getZrduedte07());	
            	    psZraeInsert.setInt(18,zraepf.getZrduedte08());	
            	    psZraeInsert.setInt(19,zraepf.getPrcnt01());	
            	    psZraeInsert.setInt(20,zraepf.getPrcnt02());	
            	    psZraeInsert.setInt(21,zraepf.getPrcnt03());	
            	    psZraeInsert.setInt(22,zraepf.getPrcnt04());	
            	    psZraeInsert.setInt(23,zraepf.getPrcnt05());	
            	    psZraeInsert.setInt(24,zraepf.getPrcnt06());	
            	    psZraeInsert.setInt(25,zraepf.getPrcnt07());	
            	    psZraeInsert.setInt(26,zraepf.getPrcnt08());	
            	    psZraeInsert.setInt(27,zraepf.getPaydte01());	
            	    psZraeInsert.setInt(28,zraepf.getPaydte02());	
            	    psZraeInsert.setInt(29,zraepf.getPaydte03());	
            	    psZraeInsert.setInt(30,zraepf.getPaydte04());	
            	    psZraeInsert.setInt(31,zraepf.getPaydte05());	
            	    psZraeInsert.setInt(32,zraepf.getPaydte06());	
            	    psZraeInsert.setInt(33,zraepf.getPaydte07());	
            	    psZraeInsert.setInt(34,zraepf.getPaydte08());	
            	    psZraeInsert.setInt(35,zraepf.getPaid01());	
            	    psZraeInsert.setInt(36,zraepf.getPaid02());	
            	    psZraeInsert.setInt(37,zraepf.getPaid03());	
            	    psZraeInsert.setInt(38,zraepf.getPaid04());	
            	    psZraeInsert.setInt(39,zraepf.getPaid05());	
            	    psZraeInsert.setInt(40,zraepf.getPaid06());	
            	    psZraeInsert.setInt(41,zraepf.getPaid07());	
            	    psZraeInsert.setInt(42,zraepf.getPaid08());	
            	    psZraeInsert.setString(43,zraepf.getPaymmeth01());	
            	    psZraeInsert.setString(44,zraepf.getPaymmeth02());	
            	    psZraeInsert.setString(45,zraepf.getPaymmeth03());	
            	    psZraeInsert.setString(46,zraepf.getPaymmeth04());	
            	    psZraeInsert.setString(47,zraepf.getPaymmeth05());	
            	    psZraeInsert.setString(48,zraepf.getPaymmeth06());	
            	    psZraeInsert.setString(49,zraepf.getPaymmeth07());	
            	    psZraeInsert.setString(50,zraepf.getPaymmeth08());	
            	    psZraeInsert.setString(51,zraepf.getZrpayopt01());	
            	    psZraeInsert.setString(52,zraepf.getZrpayopt02());	
            	    psZraeInsert.setString(53,zraepf.getZrpayopt03());	
            	    psZraeInsert.setString(54,zraepf.getZrpayopt04());	
            	    psZraeInsert.setString(55,zraepf.getZrpayopt05());	
            	    psZraeInsert.setString(56,zraepf.getZrpayopt06());	
            	    psZraeInsert.setString(57,zraepf.getZrpayopt07());	
            	    psZraeInsert.setString(58,zraepf.getZrpayopt08());
            	    psZraeInsert.setInt(59,zraepf.getTotamnt());
            	    psZraeInsert.setInt(60,zraepf.getNpaydate());
            	    psZraeInsert.setString(61,zraepf.getPayclt());
            	    psZraeInsert.setString(62,zraepf.getBankkey());
            	    psZraeInsert.setString(63,zraepf.getBankacckey());
            	    psZraeInsert.setString(64,zraepf.getPaycurr());
            	    psZraeInsert.setString(65,zraepf.getUsrprf());
            	    psZraeInsert.setString(66,zraepf.getJobnm());
            	    psZraeInsert.setString(67,zraepf.getDatime());
            	    psZraeInsert.setString(68,zraepf.getFlag());
            	    psZraeInsert.setBigDecimal(69,zraepf.getApcaplamt());
            	    psZraeInsert.setBigDecimal(70,zraepf.getApintamt());
            	    psZraeInsert.setInt(71,(zraepf.getAplstcapdate()== null? 0 : zraepf.getAplstcapdate()));
            	    psZraeInsert.setInt(72,(zraepf.getApnxtcapdate()== null? 0 : zraepf.getApnxtcapdate()));
            	    psZraeInsert.setInt(73,(zraepf.getAplstintbdte()== null? 0 : zraepf.getAplstintbdte()));
            	    psZraeInsert.setInt(74,(zraepf.getApnxtintbdte()== null? 0 : zraepf.getApnxtintbdte())); 
            	    psZraeInsert.setString(75,zraepf.getWdrgpymop());
            	    psZraeInsert.setString(76,zraepf.getWdbankkey());
            	    psZraeInsert.setString(77,zraepf.getWdbankacckey());
            	    
                    count=psZraeInsert.executeUpdate();
                          
            } catch (SQLException e) {
                LOGGER.error("insertZraeRcd()", e); /* IJTI-1479 */
                throw new SQLRuntimeException(e);
            } finally {
                close(psZraeInsert, null);
            }
    }
	public Zraepf getAllItem(String chdrnum) {
		Zraepf zraepf = new Zraepf();
      	
      	StringBuilder sql = new StringBuilder("SELECT * FROM ZRAEPF");
      	sql.append(" WHERE CHDRNUM=? AND VALIDFLAG='1' AND FLAG='1' ");
      	PreparedStatement ps=null;
      	ResultSet rs=null;
      	
      	try {
      		ps=getPrepareStatement(sql.toString());
      		ps.setString(1, chdrnum);
      		rs=ps.executeQuery();
        while (rs.next()) {
        	zraepf.setChdrcoy(rs.getString("CHDRCOY"));
        	zraepf.setChdrnum(rs.getString("CHDRNUM"));
        	zraepf.setAplstintbdte(rs.getInt("APLSTINTBDTE"));
        	zraepf.setApnxtintbdte(rs.getInt("APNXTINTBDTE"));
        	zraepf.setApnxtcapdate(rs.getInt("APNXTCAPDATE"));
        	zraepf.setAplstcapdate(rs.getInt("APLSTCAPDATE"));
        	zraepf.setApintamt(rs.getBigDecimal("APINTAMT"));
        	zraepf.setApcaplamt(rs.getBigDecimal("APCAPLAMT"));
        	zraepf.setPaycurr(rs.getString("PAYCURR"));
            }
        } catch (SQLException e) {
			LOGGER.error("getAllItem()", e); /* IJTI-1479 */
        throw new SQLRuntimeException(e);
    } finally {
        close(ps, rs);
    }
    return zraepf;
}
	
	public List<Zraepf> getAllData(String chdrnum) {
		StringBuilder sb = new StringBuilder("");
		sb.append("SELECT * FROM ZRAEPF ");
		sb.append("WHERE CHDRNUM=? AND VALIDFLAG='1' AND FLAG='1' ");
		LOGGER.info(sb.toString());		
		PreparedStatement ps = null;
		ResultSet rs = null;		
		List<Zraepf> zraeList = new ArrayList<Zraepf>();
		try{
			ps = getPrepareStatement(sb.toString());
			ps.setString(1, chdrnum);
			
			rs = ps.executeQuery();
			while(rs.next()){
				Zraepf zraepf = new Zraepf();
				
				zraepf.setChdrcoy(rs.getString("CHDRCOY"));
	        	zraepf.setChdrnum(rs.getString("CHDRNUM"));
                zraepf.setValidflag(rs.getString("Validflag"));
                zraepf.setFlag(rs.getString("Flag"));
                zraepf.setApcaplamt(rs.getBigDecimal("APCAPLAMT"));
                zraepf.setApintamt(rs.getBigDecimal("APINTAMT") == null?BigDecimal.ZERO:rs.getBigDecimal("APINTAMT"));	
                zraepf.setAplstcapdate(rs.getInt("APLSTCAPDATE"));
                zraepf.setApnxtcapdate(rs.getInt("APNXTCAPDATE"));
                zraepf.setAplstintbdte(rs.getInt("APLSTINTBDTE"));
                zraepf.setApnxtintbdte(rs.getInt("APNXTINTBDTE"));                
                zraepf.setPaycurr(rs.getString("PAYCURR"));
				zraeList.add(zraepf);	
			}
		}catch (SQLException e) {
			LOGGER.error("getAllData()", e); /* IJTI-1479 */	
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);			
		}
		return zraeList;
	}	
	
	
	public Zraepf getItemByContractNum(String chdrnum) {
		Zraepf zraepf = new Zraepf();
      	//IBPLIFE-14041 start
      	StringBuilder sql = new StringBuilder("SELECT CHDRCOY,CHDRNUM,Validflag,Flag,APCAPLAMT,APINTAMT,APLSTCAPDATE,APNXTCAPDATE,"
      			+ "APLSTINTBDTE,APNXTINTBDTE,PAYCURR  FROM ZRAEPF");
	//IBPLIFE-14041 end
      	sql.append(" WHERE CHDRNUM=? AND VALIDFLAG='1' AND FLAG='1' ");
      	PreparedStatement ps=null;
      	ResultSet rs=null;
      	
      	try {
      		ps=getPrepareStatement(sql.toString());
      		ps.setString(1, chdrnum);
      		rs=ps.executeQuery();
        while (rs.next()) {
        	zraepf.setChdrcoy(rs.getString("CHDRCOY"));
        	zraepf.setChdrnum(rs.getString("CHDRNUM"));
            zraepf.setValidflag(rs.getString("Validflag"));
            zraepf.setFlag(rs.getString("Flag"));
            zraepf.setApcaplamt(rs.getBigDecimal("APCAPLAMT"));
            zraepf.setApintamt(rs.getBigDecimal("APINTAMT") == null?BigDecimal.ZERO:rs.getBigDecimal("APINTAMT"));	
            zraepf.setAplstcapdate(rs.getInt("APLSTCAPDATE"));
            zraepf.setApnxtcapdate(rs.getInt("APNXTCAPDATE"));
            zraepf.setAplstintbdte(rs.getInt("APLSTINTBDTE"));
            zraepf.setApnxtintbdte(rs.getInt("APNXTINTBDTE"));                
            zraepf.setPaycurr(rs.getString("PAYCURR"));
            }
        } catch (SQLException e) {
			LOGGER.error("getItemByContractNum()", e); /* IJTI-1479 */
        throw new SQLRuntimeException(e);
    } finally {
        close(ps, rs);
    }
    return zraepf;
}
	
	public boolean updateZraeWithBankDetails(List<Zraepf> zraeBulkOpList) {
		if (zraeBulkOpList != null && !zraeBulkOpList.isEmpty()) {
			String SQL_UPDATE = "UPDATE ZRAEPF SET APNXTINTBDTE=?,APNXTCAPDATE=?,APCAPLAMT=?,JOBNM=?,USRPRF=?,DATIME=?,APINTAMT=?,FLAG=?,TOTAMNT=?,WDRGPYMOP=?,WDBANKKEY=?,WDBANKACCKEY=?,TRANNO=? WHERE UNIQUE_NUMBER=? ";

			PreparedStatement psRegpUpdate = getPrepareStatement(SQL_UPDATE);
			try {
				for (Zraepf r : zraeBulkOpList) {
					psRegpUpdate.setInt(1, r.getApnxtintbdte());
					psRegpUpdate.setInt(2, r.getApnxtcapdate());
					psRegpUpdate.setBigDecimal(3, r.getApcaplamt());
					psRegpUpdate.setString(4, getJobnm());
					psRegpUpdate.setString(5, getUsrprf());
					psRegpUpdate.setTimestamp(6, getDatime());
					psRegpUpdate.setBigDecimal(7, r.getApintamt());
					psRegpUpdate.setString(8, r.getFlag());
					psRegpUpdate.setInt(9, r.getTotamnt());
					psRegpUpdate.setString(10, r.getWdrgpymop());
					psRegpUpdate.setString(11, r.getWdbankkey());
					psRegpUpdate.setString(12, r.getWdbankacckey());
					psRegpUpdate.setInt(13, r.getTranno());
					psRegpUpdate.setLong(14, r.getUnique_number());
					psRegpUpdate.addBatch();
				}
				psRegpUpdate.executeBatch();
			} catch (SQLException e) {
				LOGGER.error("updateZraeRecCapDate()", e); /* IJTI-1479 */
				throw new SQLRuntimeException(e);
			} finally {
				close(psRegpUpdate, null);
			}
		}
		return true;
	}
	
}
