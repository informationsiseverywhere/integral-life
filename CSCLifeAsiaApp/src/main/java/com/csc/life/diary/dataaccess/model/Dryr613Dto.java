package com.csc.life.diary.dataaccess.model;

/**
 * Bean for Dryr613
 * 
 * @author akash
 *
 */
public class Dryr613Dto {

	public Dryr613Dto() {
		// Default constructor
	}

	private String chdrcoy;
	private String chdrNum;
	private String life;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private String validFlag;

	/**
	 * Getter for company
	 * 
	 * @return The chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * Setter for company
	 * 
	 * @param chdrcoy - The chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * Getter for chdrnum
	 * 
	 * @return The Contract Number
	 */
	public String getChdrNum() {
		return chdrNum;
	}

	/**
	 * Setter for chdrnum
	 * 
	 * @param chdrnum - The Contract Number to set
	 */
	public void setChdrNum(String chdrNum) {
		this.chdrNum = chdrNum;
	}

	/**
	 * Getter for life
	 * 
	 * @return - The life
	 */
	public String getLife() {
		return life;
	}

	/**
	 * Setter for life
	 * 
	 * @param life - The life to set
	 */
	public void setLife(String life) {
		this.life = life;
	}

	/**
	 * Getter for coverage
	 * 
	 * @return - The coverage
	 */
	public String getCoverage() {
		return coverage;
	}

	/**
	 * Setter for coverage
	 * 
	 * @param coverage - The coverage to set
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	/**
	 * Getter for rider
	 * 
	 * @return - The rider
	 */
	public String getRider() {
		return rider;
	}

	/**
	 * Setter for rider
	 * 
	 * @param rider - The rider to set
	 */
	public void setRider(String rider) {
		this.rider = rider;
	}

	/**
	 * Getter for plnsfx
	 * 
	 * @return -The plnsfx
	 */
	public Integer getPlnsfx() {
		return plnsfx;
	}

	/**
	 * Setter for plnsfx
	 * 
	 * @param plnsfx -The plnsfx to set
	 */
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}

	/**
	 * Getter for validflag
	 * 
	 * @return - The validflag
	 */
	public String getValidFlag() {
		return validFlag;
	}

	/**
	 * Setter for validflag
	 * 
	 * @param validflag - The validflag to set
	 */
	public void setValidFlag(String validFlag) {
		this.validFlag = validFlag;
	}

}
