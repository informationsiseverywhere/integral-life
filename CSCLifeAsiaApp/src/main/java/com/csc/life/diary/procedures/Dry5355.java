/*
 * File: Dry5355.java
 * Date: March 26, 2014 3:01:30 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY5355.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Vflagcpy;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.cashdividends.dataaccess.HpuaTableDAM;
import com.csc.life.contractservicing.recordstructures.Nfloanrec;
import com.csc.life.diary.dataaccess.dao.Dry5355DAO;
import com.csc.life.diary.dataaccess.model.Dry5355Dto;
import com.csc.life.diary.recordstructures.Ovbldryrec;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.newbusiness.dataaccess.HltxTableDAM;
import com.csc.life.newbusiness.dataaccess.LifeTableDAM;
import com.csc.life.newbusiness.dataaccess.dao.NlgtpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Nlgtpf;
import com.csc.life.newbusiness.procedures.Nlgcalc;
import com.csc.life.newbusiness.recordstructures.Nlgcalcrec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.life.regularprocessing.procedures.Crtloan;
import com.csc.life.regularprocessing.recordstructures.Crtloanrec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.regularprocessing.tablestructures.T5399rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.terminationclaims.procedures.Calcfee;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.terminationclaims.tablestructures.Th584rec;
import com.csc.life.terminationclaims.tablestructures.Tr5b2rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*      *
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* REGULAR PROCESSING - OVERDUE.
*==============================
*
* It performs all the processing of overdue contracts that is
* applicable to LIFE/400. A new subroutine, CRTLOAN, will be
* called from within the subroutine to perform any processing
* related to the creation of APLs.
*
* Control Totals.
* ===============
* 01 : Number of PAYR records read
* 02 : Number of overdue type 1 records processed
* 03 : Total value of overdue type 1 records
* 04 : Number of overdue type 2 records processed
* 05 : Total value of overdue type 2 records
* 06 : Number of overdue type 3 records processed
* 07 : Total value of overdue type 3 records
* 08 : Number of records in arrears
* 09 : Total value of arrears records
* 10 : Number of APLs granted
* 11 : Total value of APLs
* 12 : Number of records not processed
*
* NOTE: This program includes the splitter selection logic
* from the existing splitter program (B5354).
*
****************************************************************** ****
 * </pre>
 */
public class Dry5355 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final String F781 = "F781";
	private static final String F791 = "H791";
	private static final String STNF = "STNF";
	/* TABLES */
	private static final String T5687 = "T5687";
	private static final String T6654 = "T6654";
	private static final String T6597 = "T6597";
	private static final String T6647 = "T6647";
	private static final String T5645 = "T5645";
	private static final String T5399 = "T5399";
	private static final String TH584 = "TH584";
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5355");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaEndOfCovr = new FixedLengthStringData(1).init("N");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaSkipContract = new FixedLengthStringData(1).init("N");
	private Validator skipContract = new Validator(wsaaSkipContract, "Y");
	private PackedDecimalData wsaaSub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaIdx = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaType = new FixedLengthStringData(1).init(SPACES);
	private ZonedDecimalData wsaaType9 = new ZonedDecimalData(1, 0).init(0);
	private String wsaaAplFlag = "N";
	/* WSAA-TRANID */
	private FixedLengthStringData wsaaTermid = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaTransactionDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaTransactionTime = new ZonedDecimalData(6, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaCovrRskMatch = new FixedLengthStringData(1).init("N");
	private Validator covrRskMatch = new Validator(wsaaCovrRskMatch, "Y");

	private FixedLengthStringData wsaaCovrPrmMatch = new FixedLengthStringData(1).init("N");
	private Validator covrPrmMatch = new Validator(wsaaCovrPrmMatch, "Y");
	private ZonedDecimalData wsaaOverdueDays = new ZonedDecimalData(4, 0).init(0);

	private ZonedDecimalData wsaaDurationInForce = new ZonedDecimalData(3, 0).init(ZERO);

	private FixedLengthStringData wsaaLastPtrn = new FixedLengthStringData(8).init(SPACES);
	private String wsaaChdrOk = "";
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPayrAplsupr = new FixedLengthStringData(1).init(SPACES);
	private PackedDecimalData wsaaPayrAplspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrAplspto = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaCovrrnlStatiiSet = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaCovrrnlPstat = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaCovrrnlStat = new FixedLengthStringData(2);

	private ZonedDecimalData wsaaOverdueDate = new ZonedDecimalData(8, 0).init(0);

	private FixedLengthStringData wsaaT5399Key2 = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaAuthCode = new FixedLengthStringData(4).isAPartOf(wsaaT5399Key2, 0);
	private FixedLengthStringData wsaaCnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5399Key2, 4);
	private PackedDecimalData wsaaT5679Ix = new PackedDecimalData(3, 0).init(0);

	private FixedLengthStringData wsaaTr384Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr384Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr384Key, 0);
	private FixedLengthStringData wsaaTr384Trcde = new FixedLengthStringData(4).isAPartOf(wsaaTr384Key, 3);

	private FixedLengthStringData wsaaCovrStatusArray = new FixedLengthStringData(192);
	private FixedLengthStringData[] wsaaCovrRec = FLSArrayPartOfStructure(48, 4, wsaaCovrStatusArray, 0);
	private FixedLengthStringData[] wsaaCovrRist = FLSDArrayPartOfArrayStructure(2, wsaaCovrRec, 0);
	private FixedLengthStringData[] wsaaCovrPrst = FLSDArrayPartOfArrayStructure(2, wsaaCovrRec, 2);

	private IntegerData wsaaT5687Ix = new IntegerData();
	private IntegerData wsaaT6654Ix = new IntegerData();
	private IntegerData wsaaT6654SubrIx = new IntegerData();
	private IntegerData wsaaT6654ExpyIx = new IntegerData();
	private IntegerData wsaaT5399Ix = new IntegerData();
	private IntegerData wsaaT5399CrskIx = new IntegerData();
	private IntegerData wsaaT5399CprmIx = new IntegerData();
	private IntegerData wsaaT5399SrskIx = new IntegerData();
	private IntegerData wsaaT5399SprmIx = new IntegerData();
	private IntegerData wsaaCovrIx = new IntegerData();
	private DescTableDAM descIO = new DescTableDAM();
	private HltxTableDAM hltxIO = new HltxTableDAM();
	private HpuaTableDAM hpuaIO = new HpuaTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LifeTableDAM lifeIO = new LifeTableDAM();
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Vflagcpy vflagcpy = new Vflagcpy();
	private T5399rec t5399rec = new T5399rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6597rec t6597rec = new T6597rec();
	private T6647rec t6647rec = new T6647rec();
	private T6654rec t6654rec = getT6654rec();
	private Th584rec th584rec = new Th584rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private Nfloanrec nfloanrec = new Nfloanrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Ovbldryrec ovbldryrec = new Ovbldryrec();
	private Crtloanrec crtloanrec = new Crtloanrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private WsaaT5399ArrayInner wsaaT5399ArrayInner = new WsaaT5399ArrayInner();
	private Nlgcalcrec nlgcalcrec = new Nlgcalcrec();
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private NlgtpfDAO nlgtpfDAO = getApplicationContext().getBean("nlgtpfDAO", NlgtpfDAO.class);
	private List<Nlgtpf> nlgtpfList = new ArrayList<Nlgtpf>();
	private Chdrpf chdrpf;
	private PackedDecimalData wsaaMaxAge = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaMaxYear = new PackedDecimalData(2, 0);
	private FixedLengthStringData wsaaNlgflag = new FixedLengthStringData(1);

	private PackedDecimalData wsaaYear = new PackedDecimalData(2, 0);
	private boolean updateRequired;

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Tr5b2rec tr5b2rec = new Tr5b2rec();

	private Chdrpf chdrlifIO = new Chdrpf();
	private Covrpf covrrnlIO = new Covrpf();
	private Payrpf payrIO = new Payrpf();
	private Ptrnpf ptrnIO = new Ptrnpf();

	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private List<Ptrnpf> ptrnpfList = new ArrayList<Ptrnpf>();
	private List<Itempf> itempfList = new ArrayList<Itempf>();

	private Map<String, List<Chdrpf>> chdrpfMap;
	private Map<String, List<Payrpf>> payrpfMap;
	private Map<String, List<Covrpf>> covrpfMap;
	private List<Chdrpf> chdrUpdateList = new ArrayList<Chdrpf>();
	private List<Chdrpf> chdrUpdateInsertedList = new ArrayList<Chdrpf>();
	private List<Payrpf> payrUpdatePstcdeList = new ArrayList<Payrpf>();
	private List<Chdrpf> chdrInsertList = new ArrayList<Chdrpf>();
	private List<Payrpf> payrpfUpdateList = new ArrayList<Payrpf>();
	private List<Payrpf> payrpfInsertList = new ArrayList<Payrpf>();
	private List<Covrpf> covrpfInsertList = new ArrayList<Covrpf>();
	private List<Covrpf> covrpfInsertList2 = new ArrayList<Covrpf>();
	private int ct01Val;
	private int ct02Val;
	private BigDecimal ct03Val = BigDecimal.ZERO;
	private int ct04Val;
	private BigDecimal ct05Val = BigDecimal.ZERO;
	private int ct06Val;
	private BigDecimal ct07Val = BigDecimal.ZERO;
	private int ct08Val;
	private BigDecimal ct09Val = BigDecimal.ZERO;
	private int ct10Val;
	private BigDecimal ct11Val = BigDecimal.ZERO;
	private int ct12Val;
	private boolean goNext;
	private Map<String, List<Itempf>> t5399Map;
	private Map<String, List<Itempf>> t6654Map; // ILIFE-4323 by dpuhawan
	private ArrayList<String> riskStats = new ArrayList<String>();
	private ArrayList<String> premStats = new ArrayList<String>();
	private ArrayList<String> cnRiskStats = new ArrayList<String>();
	private ArrayList<String> cnPremStats = new ArrayList<String>();
	private ArrayList<String> covrPremStats = new ArrayList<String>();
	private ArrayList<String> covrRiskStats = new ArrayList<String>();
	private boolean prmhldtrad;
	private Map<String, Itempf> ta524Map;
	private Dry5355DAO dry5355DAO = getApplicationContext().getBean("dry5355DAO", Dry5355DAO.class);
	private static final String T5399_SEARCH_PATTERN = "(?<=\\G.{2})";
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	
	Map<String, List<Itempf>> t6597Map;
	Map<String, List<Itempf>> t5687Map;
	
	public Dry5355() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}


	public T6654rec getT6654rec() {
		return new T6654rec();
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void startProcessing() {
		initialise1000();
		List<Dry5355Dto> dry5355dtoList = readChunkRecord();
		//IJTI-1758 starts
		if(dry5355dtoList.isEmpty()) {
			return;
		}//IJTI-1758 ends
		readChdr5000(drypDryprcRecInner.drypCompany.toString(), drypDryprcRecInner.drypEntity.toString());
		loadTables1050();
		for (Dry5355Dto dto : dry5355dtoList) {
			readFile2000(dto);
			edit2500(dto);
			if (isEQ(wsspEdterror, Varcom.oK)) {
				update3000(dto);
			}
		}
		updateDiaryRecord();
		commit3500();
	}

	protected void initialise1000() {
		t5399Map = new HashMap<String, List<Itempf>>();
		t6654Map = new HashMap<String, List<Itempf>>();
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

		wsaaToday.set(datcon1rec.intDate);
		wsaaTime.set(getCobolTime());
		wsaaTermid.set(varcom.vrcmTermid);
		wsaaTransactionDate.set(wsaaToday);
		wsaaTransactionTime.set(varcom.vrcmTime);
		wsaaUser.set(ZERO);
		/* Initialise and set up all necessary information. */
		ptrnIO.setUserT(0);
		lifacmvrec.lifacmvRec.set(SPACES);
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate.toInt());
		ptrnIO.setTrdt(drypDryprcRecInner.drypRunDate.toInt());
		lifacmvrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.drypCompany);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany.toString());
		lifacmvrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr.toInt());
		lifacmvrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn.toInt());
		lifacmvrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
		lifacmvrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch.toString());
		lifacmvrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn.toString());
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.user.set(0);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		ptrnIO.setTrtm(wsaaTime.toInt());
		String prmhldtradItem = "CSOTH010";
		prmhldtrad = FeaConfg.isFeatureExist(drypDryprcRecInner.drypCompany.toString(), prmhldtradItem, appVars, "IT");
	}

	private List<Dry5355Dto> readChunkRecord() {
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon2rec.statuz);
			a000FatalError();
		}
		wsaaOverdueDate.set(datcon2rec.intDate2);

		List<Dry5355Dto> recordList = dry5355DAO.getOverdue(drypDryprcRecInner.drypCompany.toString(),
				drypDryprcRecInner.drypRunDate.toString(), drypDryprcRecInner.drypEntity.toString(),
				wsaaOverdueDate.toInt());

		if (!recordList.isEmpty()) {
			List<String> chdrnumList = Arrays.asList(drypDryprcRecInner.drypEntity.toString());
			chdrpfMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrnumList);
			payrpfMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(drypDryprcRecInner.drypCompany.trim(), chdrnumList);
			covrpfMap = covrpfDAO.searchCovrrnlByChdrnum(chdrnumList);
		}
		return recordList;
	}

	protected void loadTables1050() {

		String itemcoy = drypDryprcRecInner.drypCompany.toString();

		List<Itempf> items = itemDAO.getAllItemitem("IT", itemcoy, "T5679", drypDryprcRecInner.drypBatctrcde.toString());
		if (null == items || items.isEmpty()) {
			drylogrec.params.set("T5679");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		} else {
			t5679rec.t5679Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		}

		Map<String, List<Itempf>> itemMap = itemDAO.loadSmartTable("IT", itemcoy, T6647);
		List<Itempf> t6647List = itemMap.get(drypDryprcRecInner.drypBatctrcde.trim() + chdrlifIO.getCnttype().trim());
		
		t6647rec.t6647Rec.set(SPACES);
		t6647rec.procSeqNo.set(ZERO);
		if(t6647List != null) {
			for(Itempf itempf : t6647List) {
				if(isGTE(drypDryprcRecInner.drypRunDate, itempf.getItmfrm())
					&& isLT(drypDryprcRecInner.drypRunDate, itempf.getItmto())) {
					t6647rec.t6647Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				}
			}
		}
		
		t5687Map = itemDAO.loadSmartTable("IT", itemcoy, T5687);
		t6597Map = itemDAO.loadSmartTable("IT", itemcoy, T6597);

		wsaaT6654Ix.set(1);
		t6654Map = itemDAO.loadSmartTable("IT", itemcoy, T6654);
		if (null == t6654Map || t6654Map.isEmpty()) {
			drylogrec.params.set(T6654);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

		wsaaT5399Ix.set(1);
		t5399Map = itemDAO.loadSmartTable("IT", itemcoy, T5399);

		wsaaPrevChdrnum.set(SPACES);
		if (prmhldtrad) {
			ta524Map = itemDAO.getItemMap("IT", itemcoy, "TA524");
		}
	}

	protected void readT6654() {
		String key = payrIO.getBillchnl().trim().concat(chdrlifIO.getCnttype().trim());
		List<Itempf> t6654List;
		if (BTPRO028Permission) {
			t6654List = t6654Map.get(key.concat(payrIO.getBillfreq()));
			if(t6654List == null || t6654List.isEmpty()) {
				t6654List = t6654Map.get(key.concat("**"));
				if(t6654List == null || t6654List.isEmpty()) {
					t6654List = t6654Map.get(payrIO.getBillchnl().trim().concat("*****"));
					if(t6654List == null || t6654List.isEmpty()) {
						drylogrec.params.set("Item not found in T6654");
						drylogrec.statuz.set("MRNF");
						drylogrec.dryDatabaseError.setTrue();
						a000FatalError();
					}
				}
			}
			if(t6654List != null && !t6654List.isEmpty()) {
				for(Itempf t6654Item : t6654List) {
					if(isGTE(drypDryprcRecInner.drypRunDate, t6654Item.getItmfrm())) {
						t6654rec.t6654Rec.set(t6654Item.getGenareaString());
						break;
					}
				}
			}
		}
		else {
			t6654List = t6654Map.get(key);
			boolean itemFound = false;
			if(t6654List == null || t6654List.isEmpty()) {
				t6654List = t6654Map.get(payrIO.getBillchnl().trim().concat("***"));
			}
			
			if(t6654List != null && !t6654List.isEmpty()) {
				for(Itempf t6654Item : t6654List) {
					if(isGTE(drypDryprcRecInner.drypRunDate, t6654Item.getItmfrm())) {
						t6654rec.t6654Rec.set(t6654Item.getGenareaString());
						itemFound = true;
						break;
					}
				}
			}
			
			if(!itemFound) {
				drylogrec.params.set("Item not found in T6654");
				drylogrec.statuz.set("MRNF");
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
				
	}

	protected boolean readT5399(String searchItem) {
		String strEffDate = drypDryprcRecInner.drypRunDate.toString();
		String keyItemitem = searchItem.trim();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		if (t5399Map.containsKey(keyItemitem)) {
			itempfList = t5399Map.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
					if ((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()))
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())) {
						t5399rec.t5399Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						riskStats = new ArrayList<>(
								Arrays.asList(t5399rec.covRiskStats.toString().trim().split(T5399_SEARCH_PATTERN)));
						premStats = new ArrayList<>(
								Arrays.asList(t5399rec.covPremStats.toString().trim().split(T5399_SEARCH_PATTERN)));
						cnRiskStats = new ArrayList<>(
								Arrays.asList(t5399rec.setCnRiskStats.toString().trim().split(T5399_SEARCH_PATTERN)));
						cnPremStats = new ArrayList<>(
								Arrays.asList(t5399rec.setCnPremStats.toString().trim().split(T5399_SEARCH_PATTERN)));
						itemFound = true;
					}
				} else {
					t5399rec.t5399Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					riskStats = new ArrayList<>(
							Arrays.asList(t5399rec.covRiskStats.toString().trim().split(T5399_SEARCH_PATTERN)));
					premStats = new ArrayList<>(
							Arrays.asList(t5399rec.covPremStats.toString().trim().split(T5399_SEARCH_PATTERN)));
					cnRiskStats = new ArrayList<>(
							Arrays.asList(t5399rec.setCnRiskStats.toString().trim().split(T5399_SEARCH_PATTERN)));
					cnPremStats = new ArrayList<>(
							Arrays.asList(t5399rec.setCnPremStats.toString().trim().split(T5399_SEARCH_PATTERN)));
					itemFound = true;
				}
			}
		}
		return itemFound;
	}

	protected void readFile2000(Dry5355Dto dry5355Dto) {

		chdrpf = chdrpfDAO.getchdrRecord(dry5355Dto.getChdrcoy(), dry5355Dto.getChdrnum());
	}

	protected void edit2500(Dry5355Dto dry5355Dto) {
		boolean isIgnoreContract = edit2510(dry5355Dto);
		if(isIgnoreContract) {
			ignoreContract2580();
		}
	}

	protected boolean edit2510(Dry5355Dto dry5355Dto) {
		boolean isIgnoreContract = false;
		ct01Val++;
		wsspEdterror.set(Varcom.oK);
		readChdr5000(dry5355Dto.getChdrcoy(), dry5355Dto.getChdrnum());
		readPayr5500(dry5355Dto);
		/* If the contract number has changed, validate the contract. */
		/* If the contract status is not valid, read the next record. */
		if (isNE(dry5355Dto.getChdrnum(), wsaaPrevChdrnum)) {
			validateChdr5100(dry5355Dto);
			if (!validContract.isTrue()) {
				isIgnoreContract = true;
				return isIgnoreContract;
			}
		}
		readTr5b2();
		readT6654();

		calcOverdueDays2800(dry5355Dto);
		/* Once this stage is reached, the contract has gone into */
		/* non-forfeiture and therefore T6654 should contain at */
		/* least one non-forfeiture entry. If there is no such */
		/* entry, report this, ignore the contract and continue */
		/* processing the next. */
		for (wsaaSub
				.set(1); !(isGT(wsaaSub, 3) || isNE(t6654rec.nonForfietureDays, 0)
						|| isNE(t6654rec.daexpy[wsaaSub.toInt()], 0)); wsaaSub
								.add(1)) {
			/* CONTINUE_STMT */
		}
		if (isGT(wsaaSub, 3)) {
			isIgnoreContract = true;
			return isIgnoreContract;
		}
		/* Determine which type of overdue processing by comparing */
		/* the number of overdue days with the entries in T6654. For */
		/* arrears, move 'A' to WSAA-TYPE and '4' to the subroutine */
		/* index. For types 1, 2 and 3 processing, move 1, 2 and 3 */
		/* respectively to both WSAA-TYPE and the subroutine index. */
		/* If the number of overdue days does not correspond to any */
		/* of the entries in T6654, ignore this contract and proceed */
		/* to the next. */
		if (isLTE(t6654rec.nonForfietureDays, wsaaOverdueDays)
				&& isNE(t6654rec.nonForfietureDays, 0)) {
			wsaaT6654SubrIx.set(4);
			wsaaType.set("A");
			return isIgnoreContract;
		}
		for (wsaaT6654ExpyIx.set(3); !(isEQ(wsaaT6654ExpyIx, 0)
				|| (isLTE(t6654rec.daexpy[wsaaT6654ExpyIx.toInt()],
						wsaaOverdueDays)
						&& isNE(t6654rec.daexpy[wsaaT6654ExpyIx.toInt()],
								0))); wsaaT6654ExpyIx.add(-1)) {
			/* CONTINUE_STMT */
		}
		if (isEQ(wsaaT6654ExpyIx, 0)) {
			isIgnoreContract = true;
			return isIgnoreContract;
		} else {
			wsaaT6654SubrIx.set(wsaaT6654ExpyIx);
			wsaaType9.set(wsaaT6654ExpyIx);
			wsaaType.set(wsaaType9);
		}
		return isIgnoreContract;
	}

	private void readTr5b2() {
		itempfList = itemDAO.getAllItemitem("IT", drypDryprcRecInner.drypCompany.toString(), "TR5B2",
				chdrlifIO.getCnttype());/* IJTI-1523 */
		if (itempfList != null && !itempfList.isEmpty()) {
			for (Itempf item : itempfList) {
				tr5b2rec.tr5b2Rec.set(StringUtil.rawToString(item.getGenarea()));
			}
		}
		wsaaNlgflag.set(tr5b2rec.nlg);
		wsaaMaxAge.set(tr5b2rec.nlgczag);
		wsaaMaxYear.set(tr5b2rec.maxnlgyrs);

	}

	protected void ignoreContract2580() {
		/* Get here, therefore no overdue processing is required and */
		/* we can ignore this contract. */
		wsspEdterror.set(SPACES);
		ct12Val++;
	}

	protected void calcOverdueDays2800(Dry5355Dto dry5355Dto) {
		days2810(dry5355Dto);
	}

	protected void days2810(Dry5355Dto dry5355Dto) {
		/* Call DATCON3 to calculate the no. of overdue days. */
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(dry5355Dto.getPtdate());
		datcon3rec.intDate2.set(drypDryprcRecInner.drypRunDate);
		datcon3rec.frequency.set("DY");
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaOverdueDays.set(datcon3rec.freqFactor);
	}

	protected void update3000(Dry5355Dto dry5355Dto) {
		/* UPDATE */
		if (isEQ(wsaaType, "1")) {
			callT6654Subr3100(dry5355Dto);
		}
		if (isEQ(wsaaType, "2")) {
			callT6654Subr3100(dry5355Dto);
		}
		if (isEQ(wsaaType, "3")) {
			callT6654Subr3100(dry5355Dto);
		}
		if (isEQ(wsaaType, "A")) {
			callT6654Subr3100(dry5355Dto);
		}
		a000Statistics();
		/* EXIT */
	}

	protected void callT6654Subr3100(Dry5355Dto dry5355Dto) {
		/* If there is no letter subroutine, for arrears contracts we */
		/* still need to go into PROCESS-ARREARS because there may */
		/* nevertheless be non-forfeiture processing. For all other types */
		/* nothing is done to the contract - A PTRN is NOT written either. */
		if (isEQ(t6654rec.doctid[wsaaT6654SubrIx.toInt()], SPACES)) {
			if (isEQ(wsaaType, "A")) {
				skipLetter3125(dry5355Dto);
				return;
			} else {
				controlTotal3350();
				return;
			}
		}

		if (isNE(wsaaType, "A")) {
			writeChdrValidflag25400(dry5355Dto);
		}

		chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
		setupOverdueRec3400(dry5355Dto);
		/* Call the appropriate letter subroutine. */
		callProgram(t6654rec.doctid[wsaaT6654SubrIx.toInt()], ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, Varcom.oK)) {
			drylogrec.subrname.set(t6654rec.doctid[wsaaT6654SubrIx.toInt()]);
			drylogrec.statuz.set(ovrduerec.statuz);
			drylogrec.params.set(ovrduerec.ovrdueRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		skipLetter3125(dry5355Dto);
	}

	protected void skipLetter3125(Dry5355Dto dry5355Dto) {
		if (isEQ(wsaaType, "A")) {
			processArrears3200(dry5355Dto);
		}
		/* If type 1, 2 or 3 processing has been performed, the PAYR */
		/* suppression details will have changed and the following */
		/* updating will be required: */
		/* - rewrite the PAYR record with validflag 2 */
		/* - create a new PAYR record with validflag 1. */
		if (isEQ(wsaaType, "1") || isEQ(wsaaType, "2") || isEQ(wsaaType, "3")) {
			processOverdue3300(dry5355Dto);
			rewritePayrValidflag25700();
			createPayrValidflag15600();
		}
	}

	protected void processArrears3200(Dry5355Dto dry5355Dto) {
		/* Read HPAD to determine if client has defined a non-forfeiture */
		/* option. If so, read TH584 with this option and the contract */
		/* type to obtain the non-forfeiture method, then use this */
		/* client defined method for arrear processing than T6654's. */
		if (isNE(dry5355Dto.getZnfopt(), SPACES)) {
			setItemIO(dry5355Dto);

			checkItemIOStatus();

		}
		wsaaSkipContract.set("N");
		wsaaChdrOk = "N";
		/* Calculate the number of months the contract has been */
		/* in force. */
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.function.set("CMDF");
		datcon3rec.frequency.set("12");
		datcon3rec.intDate1.set(chdrlifIO.getOccdate());
		datcon3rec.intDate2.set(dry5355Dto.getPtdate());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaDurationInForce.set(datcon3rec.freqFactor);

		/* Decide whether Contract is going to go to APL (automatic */
		/* policy loan ) and if so, process accordingly, otherwise */
		/* carry on as normal. */
		if (isEQ(wsaaType, "A") && isNE(t6654rec.arrearsMethod, SPACES)) {
			/* PERFORM 5400-WRITE-CHDR-VALIDFLAG2 */
			/* PERFORM 5300-WRITE-CHDR-VALIDFLAG1 */
			contractLevelNf7000(dry5355Dto);
			if (isEQ(wsaaAplFlag, "N")) {
				lapsePolicy3220(dry5355Dto);
				return;
			}
			/* If the policy goes into APL - Advance with change of Premium */
			/* (NONF-STATUZ = 'APLA'), we need to do Reverse Contract Billing */
			/* to make PTDATE = BTDATE and without changing policy status. */
			if (isEQ(nfloanrec.statuz, "APLA")) {
				callOverbill9000(dry5355Dto);
				Chdrpf chdrpfLocal = chdrpfDAO.getchdrRecord(dry5355Dto.getChdrcoy(), dry5355Dto.getChdrnum());
				chdrlifIO = chdrpfLocal;
				chdrlifIO.setTranno(chdrlifIO.getTranno() - 1);
				Integer wsaaTranno = chdrlifIO.getTranno();
				writeChdrValidflag25400(dry5355Dto);
				setPrecision(chdrlifIO.getTranno(), 0);
				chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
				writeChdrValidflag15300(dry5355Dto);
				if (isEQ(wsaaType, "A")) {
					Payrpf payrpf = payrpfDAO.getpayrRecord(dry5355Dto.getChdrcoy(), dry5355Dto.getChdrnum());
					payrIO = payrpf;
					setPrecision(payrIO.getTranno(), 0);
					payrIO.setTranno(wsaaTranno);
				}
				updatePayr5900(dry5355Dto);
				writePtrn5800(dry5355Dto);
				if (chdrpfLocal != null && chdrpfLocal.getNlgflg() != null && chdrpfLocal.getNlgflg() == 'Y') {
					writeNLGRec();
				}
			} else {
				updatePayr5900(dry5355Dto);
			}
			controlTotals3280();
		}
	}

	private void setItemIO(Dry5355Dto dry5355Dto) {
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(TH584);
		itemIO.getItemitem().setSub1String(1, 3, dry5355Dto.getZnfopt());
		itemIO.getItemitem().setSub1String(4, 3, chdrlifIO.getCnttype());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);

	}

	private void checkItemIOStatus() {
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		} else {
			if (isEQ(itemIO.getStatuz(), Varcom.oK)) {
				th584rec.th584Rec.set(itemIO.getGenarea());
				if (isEQ(th584rec.nonForfeitMethod, SPACES)) {
					/* MOVE IVRM TO SYSR-SYSERR-STATUZ <Q83> */
					/* PERFORM 600-FATAL-ERROR <Q83> */
					t6654rec.arrearsMethod.set(SPACES);
				} else {
					t6654rec.arrearsMethod.set(th584rec.nonForfeitMethod);
				}
			}
		}

	}

	private void readChdrpf() {

		if (chdrpf.getNlgflg() == 'Y' && isNE(tr5b2rec.nlg, "N")) {
			ovrduerec.statcode.set(covrrnlIO.getStatcode());
			updateRequired = true;

		}

	}

	protected void checkYear2880() {

		wsaaYear.set(ZERO);
		datcon3rec.intDate1.set(chdrlifIO.getOccdate());
		datcon3rec.intDate2.set(drypDryprcRecInner.drypRunDate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaYear.set(datcon3rec.freqFactor);
	}

	protected void lapsePolicy3220(Dry5355Dto dry5355Dto) {
		/* Because the Billing process writes a PRTN, and since this */
		/* contract is about to go into Non-forfeiture and the BTDATE */
		/* and PTDATE do not match, use the overdue processing reversal */
		/* subroutine OVERBILL to attempt to bring the two dates back */
		/* into line. */
		/* A report will be produced by OVERBILL detailing what */
		/* transactions have been reversed for the contract being */
		/* processed. */
		/* Check the BTDATE and PTDATE again as these get reset in the */
		/* in the 9000 section. If they still do not match, ignore */
		/* this contract. */

		callOverbill9000(dry5355Dto);
		Chdrpf chdrpfLocal = chdrpfDAO.getchdrRecord(dry5355Dto.getChdrcoy(), dry5355Dto.getChdrnum());
		chdrlifIO = chdrpfLocal;
		chdrlifIO.setTranno(chdrlifIO.getTranno() - 1);
		Integer wsaaTranno = chdrlifIO.getTranno();
		writeChdrValidflag25400(dry5355Dto);
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
		writeChdrValidflag15300(dry5355Dto);
		if (isEQ(wsaaType, "A")) {
			Payrpf payrpf = payrpfDAO.getpayrRecord(dry5355Dto.getChdrcoy(), dry5355Dto.getChdrnum());
			payrIO = payrpf;
			setPrecision(payrIO.getTranno(), 0);
			payrIO.setTranno(wsaaTranno);
		}
		updatePayr5900(dry5355Dto);
		if (isLT(dry5355Dto.getPtdate(), dry5355Dto.getBtdate())) {
			writePtrn5800(dry5355Dto);
			if (chdrpfLocal != null && chdrpfLocal.getNlgflg() != null && chdrpfLocal.getNlgflg() == 'Y')
				writeNLGRec();
			controlTotals3280();
			return;
		}
		/* Read COVRRNL to get all valid records relevant to . */
		/* this contract. */
		wsaaEndOfCovr.set(SPACES);

		covrrnlIO = null;
		initialize(wsaaCovrStatusArray);
		wsaaCovrIx.set(1);
		goNext = false;
		if (covrpfMap != null && covrpfMap.containsKey(dry5355Dto.getChdrnum())) {
			for (Covrpf c : covrpfMap.get(dry5355Dto.getChdrnum())) {
				if (c.getChdrcoy().equals(dry5355Dto.getChdrcoy()) && 0 == c.getPlanSuffix()) {
					covrrnlIO = c;
					callCovrrnlio6000(dry5355Dto);
					if (goNext)
						continue;
				}
			}
		}

		if (covrrnlIO == null) {
			drylogrec.params.set(dry5355Dto.getChdrnum());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

		/* Skip Contract and write control total */
		if (skipContract.isTrue()) {
			writePtrn5800(dry5355Dto);
			if (chdrpfLocal != null && chdrpfLocal.getNlgflg() != null && chdrpfLocal.getNlgflg() == 'Y')
				writeNLGRec();
			drycntrec.contotNumber.set(controlTotalsInner.ct12);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			ct12Val++;
			return;
		}
		endOfCovrProcess6800(dry5355Dto);
		if (isNE(chdrlifIO.getChdrnum(), wsaaLastPtrn)) {
			wsaaLastPtrn.set(chdrlifIO.getChdrnum());
			writePtrn5800(dry5355Dto);
			if (chdrpfLocal != null && chdrpfLocal.getNlgflg() != null && chdrpfLocal.getNlgflg() == 'Y')
				writeNLGRec();
		}
		/* IF WSAA-LEAVE-IN-FORCE = 'Y' */
		/* PERFORM 6800-END-OF-COVR-PROCESS */
		/* END-IF. */
		a200DelUnderwritting();
	}

	protected void controlTotals3280() {
		ct08Val++;
		ct09Val = ct09Val.add(payrIO.getOutstamt());
	}

	protected void processOverdue3300(Dry5355Dto dry5355Dto) {
		para3310(dry5355Dto);
		controlTotals3350();
	}

	protected void para3310(Dry5355Dto dry5355Dto) {
		chdrlifIO.setAplsupr('Y');
		wsaaPayrAplsupr.set("Y");
		chdrlifIO.setAplspfrom(drypDryprcRecInner.drypRunDate.toInt());
		wsaaPayrAplspfrom.set(drypDryprcRecInner.drypRunDate);
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(dry5355Dto.getPtdate());
		datcon2rec.intDate2.set(0);
		datcon2rec.frequency.set("DY");
		/* Once we have processed an overdue record, we want to */
		/* indicate this on the contract and payer files so that */
		/* the record is not picked up again until the next overdue */
		/* action date. This date is calculated by adding the next */
		/* non-blank lag time to the paid-to-date. If all the */
		/* subsequent lag time fields are blank, record this and */
		/* use a default value of 10 days. This will ensure that */
		/* T6654 is set up correctly and that a record only gets */
		/* processed once for each type of overdue. */
		for (wsaaSub.set(wsaaType9); !(isGT(wsaaSub, 2)
				|| (setPrecision(t6654rec.daexpy[add(wsaaSub, 1).toInt()], 0)
						&& isNE(t6654rec.daexpy[add(wsaaSub, 1).toInt()], 0))); wsaaSub.add(1)) {
			/* CONTINUE_STMT */
		}
		if (isGT(wsaaSub, 2)) {
			if (isEQ(t6654rec.nonForfietureDays, SPACES)) {
				datcon2rec.freqFactor.set(10);
			} else {
				datcon2rec.freqFactor.set(t6654rec.nonForfietureDays);
			}
		} else {
			compute(datcon2rec.freqFactor, 0)
					.set(t6654rec.daexpy[add(wsaaSub, 1).toInt()]);
		}
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		chdrlifIO.setAplspto(datcon2rec.intDate2.toInt());
		wsaaPayrAplspto.set(datcon2rec.intDate2);
		writeChdrValidflag15300(dry5355Dto);
		writePtrn5800(dry5355Dto);
		if (chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')
			writeNLGRec();
	}

	protected void controlTotals3350() {
		controlTotal3350();
		/* EXIT */
	}

	protected void controlTotal3350() {
		start3351();
	}

	protected void start3351() {
		if (isEQ(wsaaT6654SubrIx, 1)) {
			ct02Val++;
			ct03Val = ct03Val.add(payrIO.getOutstamt());
		} else if (isEQ(wsaaT6654SubrIx, 2)) {
			ct04Val++;
			ct05Val = ct05Val.add(payrIO.getOutstamt());

		} else if (isEQ(wsaaT6654SubrIx, 3)) {
			ct06Val++;
			ct07Val = ct07Val.add(payrIO.getOutstamt());
		}
	}

	protected void setupOverdueRec3400(Dry5355Dto dry5355Dto) {
		para3401(dry5355Dto);
	}

	protected void para3401(Dry5355Dto dry5355Dto) {
		ovrduerec.ovrdueRec.set(SPACES);
		searchT66473450();
		setOvrduerecVariable(dry5355Dto);
		ovrduerec.outstamt.set(payrIO.getOutstamt());
		ovrduerec.ptdate.set(dry5355Dto.getPtdate());
		ovrduerec.btdate.set(dry5355Dto.getBtdate());
		ovrduerec.ovrdueDays.set(wsaaOverdueDays);
		ovrduerec.agntnum.set(chdrlifIO.getAgntnum());
		ovrduerec.cownnum.set(chdrlifIO.getCownnum());
		ovrduerec.trancode.set(drypDryprcRecInner.drypBatctrcde);
		ovrduerec.acctyear.set(drypDryprcRecInner.drypBatcactyr);
		ovrduerec.acctmonth.set(drypDryprcRecInner.drypBatcactmn);
		ovrduerec.batcbrn.set(drypDryprcRecInner.drypBranch);
		ovrduerec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		ovrduerec.user.set(payrIO.getUser());
		/* MOVE BSPR-COMPANY TO OVRD-COMPANY. */
		ovrduerec.company.set(chdrlifIO.getCowncoy());
		ovrduerec.tranDate.set(wsaaToday);
		ovrduerec.tranTime.set(wsaaTime);
		ovrduerec.termid.set(varcom.vrcmTermid);
		ovrduerec.crtable.set(covrrnlIO.getCrtable());
		ovrduerec.pumeth.set(t5687rec.pumeth);
		ovrduerec.billfreq.set(payrIO.getBillfreq());
		ovrduerec.instprem.set(ZERO);
		ovrduerec.sumins.set(ZERO);
		ovrduerec.crrcd.set(ZERO);
		ovrduerec.newCrrcd.set(ZERO);
		ovrduerec.newSingp.set(ZERO);
		ovrduerec.newAnb.set(ZERO);
		ovrduerec.newPua.set(ZERO);
		ovrduerec.newInstprem.set(ZERO);
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.premCessDate.set(ZERO);
		ovrduerec.cnttype.set(chdrlifIO.getCnttype());
		ovrduerec.occdate.set(chdrlifIO.getOccdate());
		ovrduerec.polsum.set(chdrlifIO.getPolsum());
		/* Call the CALCFEE subroutine to calculate OVRD-PUPFEE. */
		callProgram(Calcfee.class, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(ovrduerec.statuz);
			drylogrec.params.set(ovrduerec.ovrdueRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		ovrduerec.newRiskCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newPremCessDate.set(varcom.vrcmMaxDate);
		ovrduerec.newBonusDate.set(varcom.vrcmMaxDate);
		ovrduerec.newRerateDate.set(varcom.vrcmMaxDate);
		ovrduerec.cvRefund.set(ZERO);
		ovrduerec.surrenderValue.set(ZERO);
		ovrduerec.etiYears.set(ZERO);
		ovrduerec.etiDays.set(ZERO);
	}

	private void setOvrduerecVariable(Dry5355Dto dry5355Dto) {
		ovrduerec.language.set(drypDryprcRecInner.drypLanguage);
		ovrduerec.chdrcoy.set(dry5355Dto.getChdrcoy());
		ovrduerec.chdrnum.set(dry5355Dto.getChdrnum());
		ovrduerec.life.set(covrrnlIO.getLife());
		ovrduerec.coverage.set(covrrnlIO.getCoverage());
		ovrduerec.rider.set(covrrnlIO.getRider());
		ovrduerec.planSuffix.set(ZERO);
		ovrduerec.tranno.set(chdrlifIO.getTranno());
		ovrduerec.cntcurr.set(payrIO.getCntcurr());
		/* MOVE CHDRLIF-CCDATE TO OVRD-EFFDATE. */
		ovrduerec.effdate.set(wsaaToday);

	}

	protected void searchT66473450() {
		ovrduerec.aloind.set(t6647rec.aloind);
		ovrduerec.efdcode.set(t6647rec.efdcode);
		ovrduerec.procSeqNo.set(t6647rec.procSeqNo);
	}

	protected void commit3500() {
		commitControlTotals();
		chdrpfDAO.updateInvalidChdrRecord(this.chdrUpdateList);
		chdrUpdateList.clear();
		chdrpfDAO.insertChdrValidRecord(this.chdrInsertList);
		chdrInsertList.clear();
		chdrpfDAO.updateChdrLastInserted(chdrUpdateInsertedList);
		chdrUpdateInsertedList.clear();
		payrpfDAO.updatePayrRecord(payrpfUpdateList, drypDryprcRecInner.drypRunDate.toInt());
		payrpfUpdateList.clear();
		payrpfDAO.insertPayrpfList(payrpfInsertList);
		payrpfInsertList.clear();
		payrpfDAO.updatePayrPstCde(payrUpdatePstcdeList);
		payrUpdatePstcdeList.clear();
		covrpfDAO.updateCovrValidFlag(covrpfInsertList);
		covrpfInsertList.clear();
		covrpfDAO.insertCovrpfList(covrpfInsertList2);
		covrpfInsertList2.clear();
		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		ptrnpfList.clear();
	}

	private void commitControlTotals() {

		drycntrec.contotNumber.set(controlTotalsInner.ct01);
		drycntrec.contotValue.set(ct01Val);
		d000ControlTotals();
		ct01Val = 0;

		drycntrec.contotNumber.set(controlTotalsInner.ct02);
		drycntrec.contotValue.set(ct02Val);
		d000ControlTotals();
		ct02Val = 0;

		drycntrec.contotNumber.set(controlTotalsInner.ct03);
		drycntrec.contotValue.set(ct03Val);
		d000ControlTotals();
		ct03Val = BigDecimal.ZERO;

		drycntrec.contotNumber.set(controlTotalsInner.ct04);
		drycntrec.contotValue.set(ct04Val);
		d000ControlTotals();
		ct04Val = 0;

		drycntrec.contotNumber.set(controlTotalsInner.ct05);
		drycntrec.contotValue.set(ct05Val);
		d000ControlTotals();
		ct05Val = BigDecimal.ZERO;

		drycntrec.contotNumber.set(controlTotalsInner.ct06);
		drycntrec.contotValue.set(ct06Val);
		d000ControlTotals();
		ct06Val = 0;

		drycntrec.contotNumber.set(controlTotalsInner.ct07);
		drycntrec.contotValue.set(ct07Val);
		d000ControlTotals();
		ct07Val = BigDecimal.ZERO;

		drycntrec.contotNumber.set(controlTotalsInner.ct08);
		drycntrec.contotValue.set(ct08Val);
		d000ControlTotals();
		ct08Val = 0;

		drycntrec.contotNumber.set(controlTotalsInner.ct09);
		drycntrec.contotValue.set(ct09Val);
		d000ControlTotals();
		ct09Val = BigDecimal.ZERO;

		drycntrec.contotNumber.set(controlTotalsInner.ct10);
		drycntrec.contotValue.set(ct10Val);
		d000ControlTotals();
		ct10Val = 0;

		drycntrec.contotNumber.set(controlTotalsInner.ct11);
		drycntrec.contotValue.set(ct11Val);
		d000ControlTotals();
		ct11Val = BigDecimal.ZERO;

		drycntrec.contotNumber.set(controlTotalsInner.ct12);
		drycntrec.contotValue.set(ct12Val);
		d000ControlTotals();
		ct12Val = 0;

	}

	protected void readChdr5000(String company, String contractNo) {
		chdrlifIO = null;
		if (chdrpfMap != null && chdrpfMap.containsKey(contractNo)) {
			for (Chdrpf c : chdrpfMap.get(contractNo)) {
				if (c.getChdrcoy().toString().equals(company) && c.getValidflag() == '1') {
					chdrlifIO = c;
				}
			}
		}

		if (chdrlifIO == null) {
			drylogrec.params.set(contractNo);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

	}

	protected void validateChdr5100(Dry5355Dto dry5355Dto) {
		/* PARA */
		/* Validate the new contract status against T5679. */
		wsaaValidContract.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12) || validContract.isTrue()); wsaaT5679Ix.add(1)) {
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12) || validContract.isTrue()); wsaaT5679Ix.add(1)) {
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Ix.toInt()], chdrlifIO.getPstcde())) {
						wsaaValidContract.set("Y");
					}
				}
			}
		}
		wsaaPrevChdrnum.set(dry5355Dto.getChdrnum());
		/* EXIT */
	}

	protected void rewriteChdr5200(Dry5355Dto dry5355Dto) {
		Chdrpf chdr = new Chdrpf(chdrlifIO);
		chdr.setCurrto(dry5355Dto.getPtdate());
		chdrUpdateList.add(chdr);
	}

	protected void writeChdrValidflag15300(Dry5355Dto dry5355Dto) {
		chdrlifIO.setValidflag('1');
		chdrlifIO.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrlifIO.setCurrfrom(dry5355Dto.getPtdate());
		chdrInsertList.add(chdrlifIO);
	}

	protected void writeChdrValidflag25400(Dry5355Dto dry5355Dto) {
		Chdrpf chdrpfInvalid = new Chdrpf(chdrlifIO);
		chdrpfInvalid.setValidflag('2');
		chdrpfInvalid.setCurrto(dry5355Dto.getPtdate());
		chdrpfInvalid.setUniqueNumber(chdrlifIO.getUniqueNumber());
		chdrUpdateList.add(chdrpfInvalid);
	}

	protected void readPayr5500(Dry5355Dto dry5355Dto) {
		payrIO = null;
		if (payrpfMap != null && payrpfMap.containsKey(dry5355Dto.getChdrnum())) {
			for (Payrpf c : payrpfMap.get(dry5355Dto.getChdrnum())) {
				if (dry5355Dto.getChdrcoy().equals(c.getChdrcoy()) && c.getPayrseqno() == dry5355Dto.getPayrseqno()
						&& "1".equals(c.getValidflag())) {
					payrIO = c;
				}
			}
		}
	}

	protected void createPayrValidflag15600() {
		Payrpf payrInsert = new Payrpf(payrIO);
		payrInsert.setValidflag("1");
		payrInsert.setTranno(chdrlifIO.getTranno());
		payrInsert.setAplsupr(wsaaPayrAplsupr.toString());
		payrInsert.setAplspfrom(wsaaPayrAplspfrom.toInt());
		payrInsert.setAplspto(wsaaPayrAplspto.toInt());
		this.payrpfInsertList.add(payrInsert);
	}

	protected void rewritePayrValidflag25700() {
		/* PARA */
		/* Rewrite the PAYR with validflag = 2 */
		payrIO.setValidflag("2");
		this.payrpfUpdateList.add(payrIO);
	}

	protected void writePtrn5800(Dry5355Dto dry5355Dto) {
		ptrnIO = new Ptrnpf();
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx.toString());
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany.toString());
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBranch.toString());
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr.toInt());
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn.toInt());
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch.toString());
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setValidflag("1");
		ptrnIO.setTrdt(wsaaToday.toInt());
		ptrnIO.setTrtm(wsaaTime.toInt());
		ptrnIO.setPtrneff(dry5355Dto.getPtdate());
		ptrnIO.setTermid(varcom.vrcmTermid.toString());
		ptrnIO.setUserT(payrIO.getUser());
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate.toInt());

		ptrnpfList.add(ptrnIO);
	}

	protected void writeNLGRec() {
		nlgtpfList = nlgtpfDAO.readNlgtpf(ptrnIO.getChdrcoy(), ptrnIO.getChdrnum());
		for (Nlgtpf nlgtpf : nlgtpfList) {
			if (("B521".equals(nlgtpf.getBatctrcde()) && isEQ(nlgtpf.getEffdate(), ptrnIO.getPtrneff())
					&& nlgtpf.getAmnt04().compareTo(BigDecimal.ZERO) != 0)) {
				return;
			}
		}
		nlgcalcrec.chdrcoy.set(ptrnIO.getChdrcoy());
		nlgcalcrec.chdrnum.set(ptrnIO.getChdrnum());
		nlgcalcrec.effdate.set(ptrnIO.getPtrneff());
		nlgcalcrec.batcactyr.set(ptrnIO.getBatcactyr());
		nlgcalcrec.batcactmn.set(ptrnIO.getBatcactmn());
		nlgcalcrec.batctrcde.set(ptrnIO.getBatctrcde());
		nlgcalcrec.billfreq.set(chdrlifIO.getBillfreq());
		nlgcalcrec.cnttype.set(chdrlifIO.getCnttype());
		nlgcalcrec.language.set(drypDryprcRecInner.drypLanguage);
		nlgcalcrec.frmdate.set(chdrlifIO.getOccdate());
		nlgcalcrec.todate.set(varcom.vrcmMaxDate);
		nlgcalcrec.occdate.set(chdrlifIO.getOccdate());
		nlgcalcrec.ptdate.set(chdrlifIO.getPtdate());
		nlgcalcrec.billfreq.set(chdrlifIO.getBillfreq());
		nlgcalcrec.inputAmt.set(payrIO.getSinstamt01());
		compute(nlgcalcrec.inputAmt, 0).set(mult(-1, nlgcalcrec.inputAmt));
		nlgcalcrec.ovduePrem.set(payrIO.getSinstamt01());
		compute(nlgcalcrec.ovduePrem, 0).set(mult(-1, nlgcalcrec.ovduePrem));
		nlgcalcrec.function.set("OVDUE");
		callProgram(Nlgcalc.class, nlgcalcrec.nlgcalcRec);
		if (isNE(nlgcalcrec.status, Varcom.oK)) {
			drylogrec.statuz.set(drypDryprcRecInner.drypStatuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void updatePayr5900(Dry5355Dto dry5355Dto) {
		Payrpf payrUpdate = new Payrpf(payrIO);
		payrUpdate.setValidflag("2");
		payrUpdate.setUniqueNumber(payrIO.getUniqueNumber());
		this.payrpfUpdateList.add(payrUpdate);
		Payrpf payrIOInsert = new Payrpf(payrIO);
		payrIOInsert.setValidflag("1");
		payrIOInsert.setTranno(chdrlifIO.getTranno());
		payrIOInsert.setEffdate(dry5355Dto.getPtdate());
		payrpfInsertList.add(payrIOInsert);

	}

	protected void callCovrrnlio6000(Dry5355Dto dry5355Dto) {
		if (isNE(covrrnlIO.getValidflag(), vflagcpy.inForceVal)) {
			/* PERFORM 6000A-CHECK-FOR-LOCK <V65L19> */
			goNext = true;
			return;
		}
		validateCoverageStatus6050();
		if (!validCoverage.isTrue()) {
			loadCovrStatuses();
			goNext = true;
			return;
		}
		
		readT5687(covrrnlIO.getCrtable());
		searchT6597NonForfeit6100(dry5355Dto);
		if (isEQ(t5687rec.nonForfeitMethod, SPACES)) {
			goNext = true;
			return;
		}
		wsaaCovrrnlStatiiSet.set(SPACES);
		wsaaCovrrnlPstat.set(SPACES);
		wsaaCovrrnlStat.set(SPACES);

		for (wsaaSub.set(1); !(isGT(wsaaSub, 3)
				|| (isLTE(wsaaDurationInForce, t6597rec.durmnth[wsaaSub.toInt()])
				&& isNE(t6597rec.premsubr[wsaaSub.toInt()], SPACES))); wsaaSub.add(1))
		{
					/*CONTINUE_STMT*/
		}

		if (isGT(wsaaSub, 3)) {
			loadCovrStatuses();
			/* PERFORM 6000A-CHECK-FOR-LOCK <V65L19> */
			goNext = true;
			return;
			/***** NEXT SENTENCE */
		} else {
			checkT65976200(dry5355Dto);
		}
		if (skipContract.isTrue()) {
			wsaaEndOfCovr.set("Y");
			/* PERFORM 6000A-CHECK-FOR-LOCK <V65L19> */
			goNext = true;
			return;
		}
		if (isEQ(ovrduerec.statuz, "OMIT")) {
			goNext = true;
			return;
		}
		writeCovrHistory6300(dry5355Dto);
		if (isEQ(wsaaCovrrnlStatiiSet, "Y")) {
			covrrnlIO.setPstatcode(wsaaCovrrnlPstat.toString());
			covrrnlIO.setStatcode(wsaaCovrrnlStat.toString());
		}
		loadCovrStatuses();
		writeCovrValidflag16400();
	}
	
	private void readT5687(String crtable) {
		List<Itempf> t5687List = t5687Map.get(crtable);
		
		boolean isFound = false;
		if(t5687List != null && !t5687List.isEmpty()) {
			for(Itempf t5687Item : t5687List) {
				if(isGTE(drypDryprcRecInner.drypRunDate, t5687Item.getItmfrm())) {
					t5687rec.t5687Rec.set(t5687Item.getGenareaString());
					isFound = true;
					break;
				}
			}
		} 
		
		if(!isFound) {
			drylogrec.params.set("Coverage/Rider not found in T5687");
			drylogrec.statuz.set("BOMB");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void checkForLock6000a() {
		/* A-START */
		/** If key break is detected, program is about to exit the loop. */
		/** However, the previous record read is still being locked. To */
		/** avoid locking problem, rewrite the record to release the lock */
		/** before exiting the current loop. */
		/***** IF COVRRNL-CHDRNUM NOT = CHDRNUM <V65L19> */
		/***** OR COVRRNL-CHDRCOY NOT = CHDRCOY <V65L19> */
		/***** MOVE REWRT TO COVRRNL-FUNCTION <V65L19> */
		/***** CALL 'COVRRNLIO' USING COVRRNL-PARAMS <V65L19> */
		/***** <V65L19> */
		/***** IF COVRRNL-STATUZ NOT = O-K <V65L19> */
		/***** MOVE COVRRNL-PARAMS TO SYSR-PARAMS <V65L19> */
		/***** PERFORM 600-FATAL-ERROR <V65L19> */
		/***** END-IF <V65L19> */
		/***** END-IF. <V65L19> */
		/***** <V65L19> */
		/** 6000A-EXIT. <V65L19> */
		/***** EXIT. <V65L19> */
	}

	protected void loadCovrStatuses6020() {
		/* START */
		/* If this COVR's statuses are to be loaded, ensure we have not */
		/* exceeded the number of occurences in WSAA-COVR-REC. */
		if (isGT(wsaaCovrIx, 48)) {
			drylogrec.statuz.set(F791);
			drylogrec.params.set(covrrnlIO.getChdrnum());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* If the OVRD-STATUZ is 'OMIT', skip the loading of COVR status, */
		/* this is to avoid incorrect status been set up on the Contract. */
		if (isEQ(ovrduerec.statuz, "OMIT")) {
			return;
		}
		wsaaCovrPrst[wsaaCovrIx.toInt()].set(covrrnlIO.getPstatcode());
		wsaaCovrRist[wsaaCovrIx.toInt()].set(covrrnlIO.getStatcode());
		wsaaCovrIx.add(1);
		/* EXIT */
	}

	protected void loadCovrStatuses() {
		/* START */
		/* If this COVR's statuses are to be loaded, ensure we have not */
		/* exceeded the number of occurences in WSAA-COVR-REC. */
		if (isGT(wsaaCovrIx, 48)) {
			drylogrec.statuz.set(F791);
			drylogrec.params.set(covrrnlIO.getChdrnum());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* If the OVRD-STATUZ is 'OMIT', skip the loading of COVR status, */
		/* this is to avoid incorrect status been set up on the Contract. */
		if (isEQ(ovrduerec.statuz, "OMIT")) {
			return;
		}
		covrPremStats.add(covrrnlIO.getPstatcode());
		covrRiskStats.add(covrrnlIO.getStatcode());
		wsaaCovrIx.add(1);
	}

	protected void validateCoverageStatus6050() {
		wsaaValidCoverage.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12) || validCoverage.isTrue()); wsaaT5679Ix.add(1)) {
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Ix.toInt()], covrrnlIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12) || validCoverage.isTrue()); wsaaT5679Ix.add(1)) {
					if (isEQ(t5679rec.covPremStat[wsaaT5679Ix.toInt()], covrrnlIO.getPstatcode())) {
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
	}

	protected void searchT6597NonForfeit6100(Dry5355Dto dry5355Dto) {
		/* If HPAD contains a non-forfeiture option, read TH584 with */
		/* this option and the component code, then use this client */
		/* defined method for non-forfeiture processing than T5687's. */
		if (isNE(dry5355Dto.getZnfopt(), SPACES)) {
			itemIO.setDataKey(SPACES);
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itemIO.setItemtabl(TH584);
			itemIO.getItemitem().setSub1String(1, 3, dry5355Dto.getZnfopt());
			itemIO.getItemitem().setSub1String(4, 4, covrrnlIO.getCrtable());
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.mrnf)) {
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			} else {
				if (isEQ(itemIO.getStatuz(), Varcom.oK)) {
					th584rec.th584Rec.set(itemIO.getGenarea());
					if (!isEQ(th584rec.nonForfeitMethod, SPACES)) {
						wsaaT5687Ix.set(1);
						t5687rec.nonForfeitMethod.set(th584rec.nonForfeitMethod);
						searchT65976120();
						return;
					}
				}
			}
		}
		
		/*  Search T6597 using T5687-NON-FORFEIT-METHOD as the key.*/
		if (isEQ(t5687rec.nonForfeitMethod, SPACES)) {
			t6597rec.t6597Rec.set(SPACES);
			return;
		}
		searchT65976120();
	}

	protected void searchT65976120() {
		List<Itempf> t6597List = t6597Map.get(t5687rec.nonForfeitMethod.trim());
		boolean isFound = false;
		if(t6597List != null && !t6597List.isEmpty()) {
			for(Itempf t6597Item : t6597List) {
				if(isGTE(drypDryprcRecInner.drypRunDate, t6597Item.getItmfrm())) {
					t6597rec.t6597Rec.set(t6597Item.getGenareaString());
					isFound = true;
					break;
				}
			}
		} 
		
		if(!isFound) {
			drylogrec.params.set("Record Not found in T6597");
			drylogrec.statuz.set("ENDP");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void checkT65976200(Dry5355Dto dry5355Dto) {
		t65976210(dry5355Dto);
	}

	protected void t65976210(Dry5355Dto dry5355Dto) {

		if (chdrpf != null && chdrpf.getNlgflg() != null && chdrpf.getNlgflg() == 'Y')
			writeNLGRec();
		setupOverdueRec3400(dry5355Dto);

		readChdrpf();

		ovrduerec.planSuffix.set(covrrnlIO.getPlanSuffix());
		ovrduerec.instprem.set(covrrnlIO.getInstprem());
		ovrduerec.sumins.set(covrrnlIO.getSumins());
		ovrduerec.crrcd.set(covrrnlIO.getCrrcd());
		ovrduerec.premCessDate.set(covrrnlIO.getPremCessDate());
		ovrduerec.riskCessDate.set(covrrnlIO.getRiskCessDate());
		ovrduerec.newInstprem.set(covrrnlIO.getInstprem());

		checkPrmhldtrad();

		if (isEQ(ovrduerec.statuz, Varcom.oK)) {
			if ((!updateRequired && isLTE(nlgcalcrec.nlgBalance, 0))
					&& isNE(t6597rec.cpstat[wsaaSub.toInt()], SPACES)
					&& isNE(t6597rec.crstat[wsaaSub.toInt()], SPACES)) {
				wsaaCovrrnlStatiiSet.set("Y");
				ovrduerec.pstatcode.set(t6597rec.cpstat[wsaaSub.toInt()]);
				wsaaCovrrnlPstat.set(t6597rec.cpstat[wsaaSub.toInt()]);
				ovrduerec.statcode.set(t6597rec.crstat[wsaaSub.toInt()]);
				wsaaCovrrnlStat.set(t6597rec.crstat[wsaaSub.toInt()]);
			}
		} else {
			if (isEQ(ovrduerec.statuz, "LAPS")) {
				wsaaSub.set(4);
				callProgram(t6597rec.premsubr[wsaaSub.toInt()], ovrduerec.ovrdueRec, ovrduerec.ovrdueRec);

				wsaaCovrrnlStatiiSet.set("Y");
				ovrduerec.pstatcode.set(t6597rec.cpstat[wsaaSub.toInt()]);
				wsaaCovrrnlPstat.set(t6597rec.cpstat[wsaaSub.toInt()]);
				ovrduerec.statcode.set(t6597rec.crstat[wsaaSub.toInt()]);
				wsaaCovrrnlStat.set(t6597rec.crstat[wsaaSub.toInt()]);

			} else {
				if (isEQ(ovrduerec.statuz, "SKIP")) {
					wsaaSkipContract.set("Y");
				} else {
					if (isNE(ovrduerec.statuz, "OMIT")) {
						drylogrec.subrname.set(t6597rec.premsubr[wsaaSub.toInt()]);
						drylogrec.statuz.set(ovrduerec.statuz);
						drylogrec.params.set(ovrduerec.ovrdueRec);
						drylogrec.drySystemError.setTrue();
						a000FatalError();
						/** ELSE <LA3910> */
						/** MOVE 'Y' TO WSAA-SKIP-CONTRACT <LA3910> */
					}
				}
			}
		}
		readChdr5000(dry5355Dto.getChdrcoy(), dry5355Dto.getChdrnum());
		wsaaChdrOk = "Y";
		if (isNE(chdrlifIO.getChdrnum(), wsaaLastPtrn)) {
			wsaaLastPtrn.set(chdrlifIO.getChdrnum());
			/* PERFORM 5800-WRITE-PTRN <C16N> */
			if (!skipContract.isTrue()) {
				chdrlifIO.setTranno(chdrlifIO.getTranno() + 1); // ILIFE-4323 by dpuhawan
				writePtrn5800(dry5355Dto);
			}
		}
	}

	private void checkPrmhldtrad() {
		if (prmhldtrad) { // ILIFE-8179
			Itempf itempf;
			itempf = ta524Map.get(chdrlifIO.getCnttype());
			if (itempf != null && itempf.getItmfrm().compareTo(drypDryprcRecInner.drypRunDate.getbigdata()) <= 0
					&& itempf.getItmto().compareTo(drypDryprcRecInner.drypRunDate.getbigdata()) >= 0)
				ovrduerec.statuz.set("LAPS");
			else
				callProgram(t6597rec.premsubr[wsaaSub.toInt()],
						ovrduerec.ovrdueRec);
		} else {
			callProgram(t6597rec.premsubr[wsaaSub.toInt()],
					ovrduerec.ovrdueRec);
		}

	}

	protected void writeCovrHistory6300(Dry5355Dto dry5355Dto) {
		/* HIST */
		/* Rewrite the COVR at this point with a valid flag '2', */
		/* for historical purposes. */
		/* MOVE BSSC-EFFECTIVE-DATE TO COVRRNL-CURRTO. */
		Covrpf covrrnlUpdate = new Covrpf(covrrnlIO);
		covrrnlUpdate.setCurrto(dry5355Dto.getPtdate());
		covrrnlUpdate.setUniqueNumber(covrrnlIO.getUniqueNumber());
		covrpfInsertList.add(covrrnlUpdate);
	}

	protected void writeCovrValidflag16400() {
		/* <C16N> */
		/* <C16N> */
		/* Write a valid flag '1' record for COVR */
		setcovrrnlIO();

		/* MOVE COVRRNL-SUMINS TO ZRDP-AMOUNT-IN. */
		/* MOVE PAYR-CNTCURR TO ZRDP-CURRENCY. */
		/* PERFORM B000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO COVRRNL-SUMINS. */
		if (isNE(covrrnlIO.getSumins(), 0)) {
			zrdecplrec.amountIn.set(covrrnlIO.getSumins());
			zrdecplrec.currency.set(payrIO.getCntcurr());
			b000CallRounding();
			covrrnlIO.setSumins(zrdecplrec.amountOut.getbigdata());
		}
		if (isNE(ovrduerec.newCrrcd, varcom.vrcmMaxDate) && isNE(ovrduerec.newCrrcd, ZERO)) {
			covrrnlIO.setCrrcd(ovrduerec.newCrrcd.toInt());
		}
		if (isNE(ovrduerec.newSingp, ZERO)) {
			covrrnlIO.setSingp(ovrduerec.newSingp.getbigdata());
		}
		if (isNE(ovrduerec.newAnb, ZERO)) {
			covrrnlIO.setAnbAtCcd(ovrduerec.newAnb.toInt());
		}
		covrrnlIO.setInstprem(ovrduerec.newInstprem.getbigdata());
		/* MOVE COVRRNL-INSTPREM TO ZRDP-AMOUNT-IN. */
		/* MOVE PAYR-CNTCURR TO ZRDP-CURRENCY. */
		/* PERFORM B000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO COVRRNL-INSTPREM. */
		if (isNE(covrrnlIO.getInstprem(), 0)) {
			zrdecplrec.amountIn.set(covrrnlIO.getInstprem());
			zrdecplrec.currency.set(payrIO.getCntcurr());
			b000CallRounding();
			covrrnlIO.setInstprem(zrdecplrec.amountOut.getbigdata());
		}
		if (isNE(ovrduerec.newPua, ZERO)) {
			setPrecision(covrrnlIO.getVarSumInsured(), 2);
			covrrnlIO.setVarSumInsured(add(covrrnlIO.getVarSumInsured(), ovrduerec.newPua).getbigdata());
		}
		/* MOVE COVRRNL-VAR-SUM-INSURED TO ZRDP-AMOUNT-IN. */
		/* MOVE PAYR-CNTCURR TO ZRDP-CURRENCY. */
		/* PERFORM B000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO COVRRNL-VAR-SUM-INSURED. */
		if (isNE(covrrnlIO.getVarSumInsured(), 0)) {
			zrdecplrec.amountIn.set(covrrnlIO.getVarSumInsured());
			zrdecplrec.currency.set(payrIO.getCntcurr());
			b000CallRounding();
			covrrnlIO.setVarSumInsured(zrdecplrec.amountOut.getbigdata());
		}
		if (isNE(ovrduerec.newBonusDate, varcom.vrcmMaxDate) && isNE(ovrduerec.newBonusDate, ZERO)) {
			covrrnlIO.setUnitStatementDate(ovrduerec.newBonusDate.toInt());
		}
		covrrnlIO.setValidflag("1");
		/* MOVE MAXDATE TO COVRRNL-CURRTO. */
		covrrnlIO.setCurrto(varcom.vrcmMaxDate.toInt());
		/* MOVE BSSC-EFFECTIVE-DATE TO COVRRNL-CURRFROM. */
		covrrnlIO.setCurrfrom(chdrlifIO.getPtdate());
		covrrnlIO.setTranno(chdrlifIO.getTranno());
		covrrnlIO.setTermid(wsaaTermid.toString());
		covrrnlIO.setTransactionDate(wsaaTransactionDate.toInt());
		covrrnlIO.setTransactionTime(wsaaTransactionTime.toInt());
		covrrnlIO.setUser(wsaaUser.toInt());
		/* Lapse all the HPUA records and update COVRRNL-VARSI before */
		/* writing the new COVRRNL. */
		lapsePua6600();

		covrpfInsertList2.add(covrrnlIO);
	}

	private void setcovrrnlIO() {
		if (isNE(ovrduerec.newRiskCessDate, varcom.vrcmMaxDate) && isNE(ovrduerec.newRiskCessDate, ZERO)) {
			covrrnlIO.setRiskCessDate(ovrduerec.newRiskCessDate.toInt());
		}
		if (isNE(ovrduerec.newPremCessDate, varcom.vrcmMaxDate) && isNE(ovrduerec.newPremCessDate, ZERO)) {
			covrrnlIO.setPremCessDate(ovrduerec.newPremCessDate.toInt());
		}
		if (isNE(ovrduerec.newRerateDate, varcom.vrcmMaxDate) && isNE(ovrduerec.newRerateDate, ZERO)) {
			covrrnlIO.setRerateDate(ovrduerec.newRerateDate.toInt());
		}
		if (isNE(ovrduerec.newSumins, ZERO)) {
			covrrnlIO.setSumins(ovrduerec.newSumins.getbigdata());
			covrrnlIO.setVarSumInsured(BigDecimal.ZERO);
		} else if (isGT(covrrnlIO.getVarSumInsured(), ZERO)) {
			setPrecision(covrrnlIO.getVarSumInsured(), 2);
			covrrnlIO.setVarSumInsured(sub(covrrnlIO.getVarSumInsured(), covrrnlIO.getSumins()).getbigdata());
			/** MOVE ZERO TO COVRRNL-SUMINS <C16N> */
		}

	}

	protected void lapsePua6600() {
		hpuaIO.setParams(SPACES);
		hpuaIO.setChdrcoy(covrrnlIO.getChdrcoy());
		hpuaIO.setChdrnum(covrrnlIO.getChdrnum());
		hpuaIO.setLife(covrrnlIO.getLife());
		hpuaIO.setCoverage(covrrnlIO.getCoverage());
		hpuaIO.setRider(covrrnlIO.getRider());
		hpuaIO.setPlanSuffix(covrrnlIO.getPlanSuffix());
		hpuaIO.setPuAddNbr(ZERO);
		hpuaIO.setFormat(formatsInner.hpuarec);
		hpuaIO.setFunction(Varcom.begn);
		while (!(isEQ(hpuaIO.getStatuz(), Varcom.endp))) {
			updateHpua6650();
		}

	}

	protected void updateHpua6650() {
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), Varcom.oK) && isNE(hpuaIO.getStatuz(), Varcom.endp)) {
			drylogrec.params.set(hpuaIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Key break */
		if (isNE(covrrnlIO.getChdrcoy(), hpuaIO.getChdrcoy()) || isNE(covrrnlIO.getChdrnum(), hpuaIO.getChdrnum())
				|| isNE(covrrnlIO.getLife(), hpuaIO.getLife()) || isNE(covrrnlIO.getCoverage(), hpuaIO.getCoverage())
				|| isNE(covrrnlIO.getRider(), hpuaIO.getRider())
				|| isNE(covrrnlIO.getPlanSuffix(), hpuaIO.getPlanSuffix())) {
			hpuaIO.setStatuz(Varcom.endp);
		}
		if (isEQ(hpuaIO.getStatuz(), Varcom.oK)) {
			if (isEQ(hpuaIO.getTranno(), ovrduerec.tranno)) {
				/* ADD 1 TO HPUA-PU-ADD-NBR <R67> */
				/* MOVE BEGN TO HPUA-FUNCTION <R67> */
				hpuaIO.setFunction(Varcom.nextr);
				return;
			}
			/* Rewrite the old HPUA */
			reWriteHPUA();

			/* Update COVRRNL-VARSI */
			if (isGT(covrrnlIO.getVarSumInsured(), ZERO)) {
				setPrecision(covrrnlIO.getVarSumInsured(), 2);
				covrrnlIO.setVarSumInsured(sub(covrrnlIO.getVarSumInsured(), hpuaIO.getSumin()).getbigdata());
			}
			/* <R67> */
			/* Write a new HPUA <R67> */
			/* <R67> */
			hpuaIO.setValidflag("1");
			hpuaIO.setTranno(covrrnlIO.getTranno());
			hpuaIO.setPstatcode(t6597rec.cpstat[4]);
			hpuaIO.setRstatcode(t6597rec.crstat[4]);
			hpuaIO.setRiskCessDate(covrrnlIO.getRiskCessDate());
			hpuaIO.setFunction(Varcom.writr);
			hpuaio9100();
			/* Next HPUA record */
			/* ADD 1 TO HPUA-PU-ADD-NBR <R67> */
			/* MOVE BEGN TO HPUA-FUNCTION <R67> */
			hpuaIO.setFunction(Varcom.nextr);
		}
	}

	private void reWriteHPUA() {
		hpuaIO.setValidflag("2");
		hpuaIO.setFunction(Varcom.keeps);
		hpuaio9100();
		hpuaIO.setFunction(Varcom.writs);
		hpuaio9100();

	}

	protected void endOfCovrProcess6800(Dry5355Dto dry5355Dto) {
		endCovr6810(dry5355Dto);
	}

	protected void endCovr6810(Dry5355Dto dry5355Dto) {
		/* Update statii fields in CHDRLIF. */
		/* IF WSAA-STATCODE = SPACE */
		/* MOVE T5679-SET-CN-PREM-STAT TO CHDRLIF-PSTATCODE */
		/* MOVE T5679-SET-CN-RISK-STAT TO CHDRLIF-STATCODE */
		/* ELSE */
		/* MOVE WSAA-PSTATCODE TO CHDRLIF-PSTATCODE */
		/* MOVE WSAA-STATCODE TO CHDRLIF-STATCODE */
		/* END-IF. */
		wsaaCovrRskMatch.set("N");
		wsaaCovrPrmMatch.set("N");

		String keyItem = drypDryprcRecInner.drypBatctrcde.toString().trim()
				.concat(chdrlifIO.getCnttype().trim());/* IJTI-1523 */
		if (!readT5399(keyItem)) {
			wsaaAuthCode.set(drypDryprcRecInner.drypBatctrcde);
			wsaaCnttype.set(chdrlifIO.getCnttype());
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itemIO.setItemtabl(T5399);
			itemIO.setItemitem(wsaaT5399Key2);
			itemIO.setFunction(Varcom.readr);
			itemIO.setStatuz(STNF);
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		readRist();

		if ((!updateRequired && isNE(tr5b2rec.nlg, "Y") && !covrRskMatch.isTrue() && isNE(tr5b2rec.nlg, ""))) {
			// ILIFE-5297 END
			chdrlifIO.setStatcode(t5679rec.setCnRiskStat.toString());
		}

		readPrst();
		if (!updateRequired && !covrPrmMatch.isTrue() && isLTE(nlgcalcrec.nlgBalance, 0)) {
			chdrlifIO.setPstcde(t5679rec.setCnPremStat.toString());
			payrIO.setPstatcode(t5679rec.setCnPremStat.toString());
		}
		if (isEQ(wsaaChdrOk, "N")) {
			readChdr5000(dry5355Dto.getChdrcoy(), dry5355Dto.getChdrnum());
		}
		rewriteInsertedChdr();
		rewritePayrPstcde(dry5355Dto);
	}

	protected void rewriteInsertedChdr() {
		this.chdrUpdateInsertedList.add(chdrlifIO);
	}

	protected void rewritePayrPstcde(Dry5355Dto dry5355Dto) {
		Payrpf payr = new Payrpf(payrIO);
		payr.setBtdate(dry5355Dto.getBtdate());
		payr.setBillcd(dry5355Dto.getBtdate());
		payr.setPstatcode(payrIO.getPstatcode()); // ILIFE-4323 by dpuhawan
		this.payrUpdatePstcdeList.add(payr);
	}

	protected void readRist() {
		if (riskStats.isEmpty())
			return;
		for (String covrRisk : covrRiskStats) {
			int riskIndex = riskStats.indexOf(covrRisk);
			if (riskIndex >= 0) { // ILIFE-5297 start by dpuhawan
				chdrlifIO.setStatcode(cnRiskStats.get(riskStats.indexOf(covrRisk)));
				wsaaCovrRskMatch.set("Y");
				break;
			}
		}
	}

	protected void readRist6820() {
		start6821();
	}

	protected void start6821() {
		if (isEQ(wsaaT5399ArrayInner.wsaaT5399RiskStat[wsaaT5399Ix.toInt()][wsaaT5399CrskIx.toInt()], "  ")) {
			return;
		}
		wsaaCovrIx.set(1);
		searchlabel1: {
			for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)) {
				if (isEQ(wsaaT5399ArrayInner.wsaaT5399RiskStat[wsaaT5399Ix.toInt()][wsaaT5399CrskIx.toInt()],
						wsaaCovrRist[wsaaCovrIx.toInt()])) {
					wsaaT5399SrskIx.set(wsaaT5399CrskIx);
					chdrlifIO.setStatcode(
							wsaaT5399ArrayInner.wsaaT5399SetRiskStat[wsaaT5399Ix.toInt()][wsaaT5399SrskIx.toInt()]
									.toString());
					wsaaCovrRskMatch.set("Y");
					break searchlabel1;
				}
			}
			/* CONTINUE_STMT */
		}
	}

	protected void readPrst() {
		if (premStats.isEmpty())
			return;
		for (String covrPrem : covrPremStats) {
			int premIndex = premStats.indexOf(covrPrem);
			if (premIndex >= 0 && !updateRequired && isLTE(nlgcalcrec.nlgBalance, 0)) { // ILIFE-5297 start by dpuhawan
				chdrlifIO.setPstcde(cnPremStats.get(premStats.indexOf(covrPrem)));
				payrIO.setPstatcode(cnPremStats.get(premStats.indexOf(covrPrem)));
				wsaaCovrPrmMatch.set("Y");
				break;
			}
		}
	}

	protected void readPrst6830() {
		start6831();
	}

	protected void start6831() {
		if (isEQ(wsaaT5399ArrayInner.wsaaT5399PremStat[wsaaT5399Ix.toInt()][wsaaT5399CprmIx.toInt()], "  ")) {
			return;
		}
		wsaaCovrIx.set(1);
		searchlabel1: {
			for (; isLT(wsaaCovrIx, wsaaCovrRec.length); wsaaCovrIx.add(1)) {
				if (!updateRequired && isEQ(wsaaT5399ArrayInner.wsaaT5399PremStat[wsaaT5399Ix.toInt()][wsaaT5399CprmIx.toInt()],
						wsaaCovrPrst[wsaaCovrIx.toInt()]) && isLT(nlgcalcrec.nlgBalance, 0)) {
					wsaaT5399SprmIx.set(wsaaT5399CprmIx);
					chdrlifIO.setPstcde(
							(wsaaT5399ArrayInner.wsaaT5399SetPremStat[wsaaT5399Ix.toInt()][wsaaT5399SprmIx.toInt()]
									.toString()));
					payrIO.setPstatcode(
							wsaaT5399ArrayInner.wsaaT5399SetPremStat[wsaaT5399Ix.toInt()][wsaaT5399SprmIx.toInt()]
									.toString());
					wsaaCovrPrmMatch.set("Y");
					break searchlabel1;
				}
			}
			/* CONTINUE_STMT */
		}
	}

	protected void contractLevelNf7000(Dry5355Dto dry5355Dto) {
		/* NF */
		wsaaAplFlag = "Y";
		searchT6597Arrears7100();
		/* Process the T6597 record we have just read */
		for (wsaaSub.set(1); !(isGT(wsaaSub, 3) || (isLTE(wsaaDurationInForce, t6597rec.durmnth[wsaaSub.toInt()])
				&& isNE(t6597rec.premsubr[wsaaSub.toInt()], SPACES))); wsaaSub.add(1)) {
			/* CONTINUE_STMT */
		}
		if (isGT(wsaaSub, 3)) {
			/* NEXT_SENTENCE */
		} else {
			/* PERFORM 7200-PROCESS-T6597 */
			if (isEQ(t6597rec.cpstat[wsaaSub.toInt()], SPACES)
					&& isEQ(t6597rec.crstat[wsaaSub.toInt()], SPACES)) {
				processT65977200(dry5355Dto);
			} else {
				wsaaAplFlag = "N";
			}
		}
		/* EXIT */
	}

	protected void searchT6597Arrears7100() {
		t65977110();
	}

	protected void t65977110() {
		
		List<Itempf> t6597List = t6597Map.get(t6654rec.arrearsMethod.trim());
		
		boolean isFound = false;
		if(t6597List != null) {
			for(Itempf t6597Item : t6597List) {
				if(isGTE(drypDryprcRecInner.drypRunDate, t6597Item.getItmfrm())) {
					t6597rec.t6597Rec.set(t6597Item.getGenareaString());
					isFound = true;
				}
			}
		}
		
		if(!isFound) {
			drylogrec.params.set("Record not found in T6597");
			drylogrec.statuz.set(F781);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		
	}

	protected void processT65977200(Dry5355Dto dry5355Dto) {
		t65977210(dry5355Dto);
	}

	protected void t65977210(Dry5355Dto dry5355Dto) {
		/* Get overdue instalement amount from LINSRNL */
		linsrnlIO.setChdrcoy(dry5355Dto.getChdrcoy());
		linsrnlIO.setChdrnum(dry5355Dto.getChdrnum());
		linsrnlIO.setInstfrom(dry5355Dto.getPtdate());
		linsrnlIO.setFormat(formatsInner.linsrnlrec);
		linsrnlIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, linsrnlIO);
		if (isNE(linsrnlIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(linsrnlIO.getStatuz());
			drylogrec.params.set(linsrnlIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		nfloanrec.nfloanRec.set(SPACES);
		nfloanrec.language.set(drypDryprcRecInner.drypLanguage);
		nfloanrec.chdrcoy.set(dry5355Dto.getChdrcoy());
		nfloanrec.chdrnum.set(dry5355Dto.getChdrnum());
		nfloanrec.cntcurr.set(payrIO.getCntcurr());
		nfloanrec.effdate.set(drypDryprcRecInner.drypRunDate);
		nfloanrec.ptdate.set(dry5355Dto.getPtdate());
		nfloanrec.batcbrn.set(drypDryprcRecInner.drypBranch);
		nfloanrec.company.set(drypDryprcRecInner.drypCompany);
		nfloanrec.billfreq.set(payrIO.getBillfreq());
		nfloanrec.cnttype.set(chdrlifIO.getCnttype());
		nfloanrec.polsum.set(chdrlifIO.getPolsum());
		nfloanrec.polinc.set(chdrlifIO.getPolinc());
		nfloanrec.batcBatctrcde.set(drypDryprcRecInner.drypBatctrcde);
		/* MOVE PAYR-OUTSTAMT TO NONF-OUTSTAMT. */
		nfloanrec.outstamt.set(linsrnlIO.getInstamt06());
		callProgram(t6597rec.premsubr[wsaaSub.toInt()], nfloanrec.nfloanRec);
		if (isNE(nfloanrec.statuz, Varcom.oK) && isNE(nfloanrec.statuz, "NAPL") && isNE(nfloanrec.statuz, "APLA")) {
			drylogrec.subrname.set(t6597rec.premsubr[wsaaSub.toInt()]);
			drylogrec.statuz.set(nfloanrec.statuz);
			drylogrec.params.set(nfloanrec.nfloanRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* IF NONF-STATUZ = 'NAPL' */
		if (isEQ(nfloanrec.statuz, "NAPL") || isEQ(nfloanrec.statuz, "APLA")) {
			return;
		}
		/* Get here so contract is OK to have an APL attached to it. */
		/* PERFORM 5000-READ-CHDR. */
		/* PERFORM 5200-REWRITE-CHDR. */
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
		writeChdrValidflag25400(dry5355Dto);
		writeChdrValidflag15300(dry5355Dto);
		callCrtloan8000(dry5355Dto);
		writeLetc8500();
		wsaaChdrOk = "Y";
		wsaaLastPtrn.set(chdrlifIO.getChdrnum());

		writePtrn5800(dry5355Dto);
		ct10Val++;
		ct11Val = ct11Val.add(payrIO.getOutstamt());
	}

	protected void callCrtloan8000(Dry5355Dto dry5355Dto) {
		start8010(dry5355Dto);
	}

	protected void start8010(Dry5355Dto dry5355Dto) {
		/* Read T5645 to get the Account postings for APLs */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dry5355Dto.getChdrcoy());
		itemIO.setItemtabl(T5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Get item description from T5645. */
		descIO.setParams(SPACES);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(nfloanrec.chdrcoy);
		descIO.setDesctabl(T5645);
		descIO.setDescitem(wsaaProg);
		descIO.setLanguage(drypDryprcRecInner.drypLanguage);
		descIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), Varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/* Set up linkage to CRTLOAN, which will write LOAN */
		/* and APL ACMV records. */
		setCrtloan();
		/* MOVE PAYR-OUTSTAMT TO CRTL-OUTSTAMT. */
		crtloanrec.outstamt.set(linsrnlIO.getInstamt06());
		crtloanrec.batchkey.set(drypDryprcRecInner.drypBatchKey);
		crtloanrec.sacscode01.set(t5645rec.sacscode01);
		crtloanrec.sacscode02.set(t5645rec.sacscode02);
		crtloanrec.sacscode03.set(t5645rec.sacscode03);
		crtloanrec.sacscode04.set(t5645rec.sacscode04);
		crtloanrec.sacstyp01.set(t5645rec.sacstype01);
		crtloanrec.sacstyp02.set(t5645rec.sacstype02);
		crtloanrec.sacstyp03.set(t5645rec.sacstype03);
		crtloanrec.sacstyp04.set(t5645rec.sacstype04);
		crtloanrec.glcode01.set(t5645rec.glmap01);
		crtloanrec.glcode02.set(t5645rec.glmap02);
		crtloanrec.glcode03.set(t5645rec.glmap03);
		crtloanrec.glcode04.set(t5645rec.glmap04);
		crtloanrec.glsign01.set(t5645rec.sign01);
		crtloanrec.glsign02.set(t5645rec.sign02);
		crtloanrec.glsign03.set(t5645rec.sign03);
		crtloanrec.glsign04.set(t5645rec.sign04);
		callProgram(Crtloan.class, crtloanrec.crtloanRec);
		if (isNE(crtloanrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(crtloanrec.statuz);
			drylogrec.params.set(crtloanrec.crtloanRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	private void setCrtloan() {
		crtloanrec.longdesc.set(descIO.getLongdesc());
		crtloanrec.chdrcoy.set(nfloanrec.chdrcoy);
		crtloanrec.chdrnum.set(nfloanrec.chdrnum);
		crtloanrec.cnttype.set(nfloanrec.cnttype);
		crtloanrec.loantype.set("A");
		crtloanrec.cntcurr.set(nfloanrec.cntcurr);
		crtloanrec.billcurr.set(payrIO.getBillcurr());
		crtloanrec.tranno.set(chdrlifIO.getTranno());
		crtloanrec.occdate.set(chdrlifIO.getOccdate());
		crtloanrec.effdate.set(drypDryprcRecInner.drypRunDate);
		crtloanrec.authCode.set(drypDryprcRecInner.drypBatctrcde);
		crtloanrec.language.set(drypDryprcRecInner.drypLanguage);

	}

	protected void writeLetc8500() {
		start8510();
	}

	protected void start8510() {
		/* Read T6634 to obtain the Letter Type */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		/* MOVE T6634 TO ITEM-ITEMTABL. <PCPPRT> */
		/* MOVE CHDRLIF-CNTTYPE TO WSAA-T6634-CNTTYPE. <PCPPRT> */
		/* MOVE BPRD-SYSTEM-PARAM01 TO WSAA-T6634-TRCDE. <PCPPRT> */
		/* MOVE WSAA-T6634-KEY TO ITEM-ITEMITEM. <PCPPRT> */
		itemIO.setItemtabl("TR384");
		wsaaTr384Cnttype.set(chdrlifIO.getCnttype());
		//IBPTE-73 corrected logic to fetch transaction code
		wsaaTr384Trcde.set(drypDryprcRecInner.drypSystParams);
		itemIO.setItemitem(wsaaTr384Key);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* MOVE ITEM-GENAREA TO T6634-T6634-REC. <PCPPRT> */
		tr384rec.tr384Rec.set(itemIO.getGenarea());
		/* IF T6634-LETTER-TYPE = SPACES <PCPPRT> */
		if (isEQ(tr384rec.letterType, SPACES)) {
			return;
		}
		/* Write HLTX to store information for printing <LA2101> */
		hltxIO.setParams(SPACES);
		for (wsaaIdx.set(1); !(isGT(wsaaIdx, 8)); wsaaIdx.add(1)) {
			hltxIO.setHnumfld(wsaaIdx, ZERO);
		}
		setHltxIO();

		letrqstrec.requestCompany.set(drypDryprcRecInner.drypCompany);
		/* MOVE T6634-LETTER-TYPE TO LETRQST-LETTER-TYPE. <PCPPRT> */
		letrqstrec.letterType.set(tr384rec.letterType);
		letrqstrec.letterRequestDate.set(wsaaToday);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.otherKeys.set(drypDryprcRecInner.drypLanguage);
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.function.set("ADD");
		/* CALL 'LETRQST' USING LETRQST-PARAMS. <P002> */
		/* CALL 'HLETRQS' USING LETRQST-PARAMS. <PCPPRT> */
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(letrqstrec.statuz);
			drylogrec.params.set(letrqstrec.params);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	private void setHltxIO() {
		hltxIO.setRequestCompany(drypDryprcRecInner.drypCompany);
		/* MOVE T6634-LETTER-TYPE TO HLTX-LETTER-TYPE. <PCPPRT> */
		hltxIO.setLetterType(tr384rec.letterType);
		hltxIO.setClntcoy(chdrlifIO.getCowncoy());
		hltxIO.setClntnum(chdrlifIO.getCownnum());
		hltxIO.setLetterSeqno(chdrlifIO.getTranno());
		hltxIO.setChdrcoy(chdrlifIO.getChdrcoy());
		hltxIO.setChdrnum(chdrlifIO.getChdrnum());
		/* Modal Premium */
		hltxIO.setHnumfld01(payrIO.getSinstamt06());
		/* Overdue Premium */
		/* MOVE PAYR-OUTSTAMT TO HLTX-HNUMFLD02. <LA2101> */
		hltxIO.setHnumfld02(payrIO.getSinstamt06());
		/* Premium Default Date - PTD before APL */
		datcon1rec.intDate.set(payrIO.getPtdate());
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		hltxIO.setHshtchar01(datcon1rec.extDate);
		/* New Paid To Date - PTD after APL */
		/* MOVE PAYR-BTDATE TO DTC1-INT-DATE. <LA4352> */
		/* Only 1 overdue premium for APL */
		datcon2rec.intDate1.set(payrIO.getPtdate());
		datcon2rec.intDate2.set(0);
		datcon2rec.frequency.set(payrIO.getBillfreq());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		datcon1rec.intDate.set(datcon2rec.intDate2);
		datcon1rec.function.set("CONV");
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		hltxIO.setHshtchar02(datcon1rec.extDate);
		hltxIO.setHshtchar03(payrIO.getCntcurr());
		hltxIO.setFormat(formatsInner.hltxrec);
		hltxIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, hltxIO);
		if (isNE(hltxIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(hltxIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

	}

	protected void callOverbill9000(Dry5355Dto dry5355Dto) {
		ovrbll9010(dry5355Dto);
	}

	protected void ovrbll9010(Dry5355Dto dry5355Dto) {
		ovbldryrec.overbillRec.set(SPACES);
		compute(ovbldryrec.newTranno, 0).set(add(1, chdrlifIO.getTranno()));
		ovbldryrec.btdate.set(ZERO);
		ovbldryrec.ptdate.set(ZERO);
		ovbldryrec.tranDate.set(wsaaTransactionDate);
		ovbldryrec.tranTime.set(wsaaTransactionTime);
		ovbldryrec.user.set(wsaaUser);
		ovbldryrec.termid.set(wsaaTermid);
		callProgram(Overbildry.class, drypDryprcRecInner.drypDryprcRec, ovbldryrec.overbillRec);
		if (isNE(ovbldryrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(ovbldryrec.statuz);
			drylogrec.params.set(ovbldryrec.overbillRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		dry5355Dto.setBtdate(ovbldryrec.btdate.toInt());
		dry5355Dto.setPtdate(ovbldryrec.ptdate.toInt());
	}

	protected void a000Statistics() {
		a010Start();
	}

	protected void a010Start() {
		lifsttrrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifsttrrec.batcbrn.set(drypDryprcRecInner.drypBranch);
		lifsttrrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifsttrrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifsttrrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifsttrrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(chdrlifIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(lifsttrrec.statuz);
			drylogrec.params.set(lifsttrrec.lifsttrRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void hpuaio9100() {
		/* START */
		SmartFileCode.execute(appVars, hpuaIO);
		if (isNE(hpuaIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(hpuaIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* EXIT */
	}

	protected void a200DelUnderwritting() {
		if (isEQ(chdrlifIO.getStatcode(), "IF")) {
			return;
		}
		a300BegnLifeio();
		a200Delet();
	}

	protected void a200Delet() {
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifeIO.getLifcnum());
		crtundwrec.coy.set(chdrlifIO.getChdrcoy());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.chdrnum.set(chdrlifIO.getChdrnum());
		crtundwrec.crtable.fill("*");
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, Varcom.oK)) {
			drylogrec.statuz.set(crtundwrec.status);
			drylogrec.params.set(crtundwrec.parmRec);
			drylogrec.iomod.set("CRTUNDWRT");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		
		lifeIO.setFunction(Varcom.nextr);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (isNE(lifeIO.getStatuz(), Varcom.oK)) {
			return;
		}
		if (isEQ(chdrlifIO.getChdrcoy(), lifeIO.getChdrcoy()) && isEQ(chdrlifIO.getChdrnum(), lifeIO.getChdrnum())) {
			a200Delet();
		}
	}

	protected void a300BegnLifeio() {
		a300Ctrl();
	}

	protected void a300Ctrl() {
		lifeIO.setRecKeyData(SPACES);
		lifeIO.setChdrcoy(chdrlifIO.getChdrcoy());
		lifeIO.setChdrnum(chdrlifIO.getChdrnum());
		lifeIO.setFunction(Varcom.begn);
		lifeIO.setFormat(formatsInner.liferec);
		SmartFileCode.execute(appVars, lifeIO);
		if (!(isEQ(lifeIO.getStatuz(), Varcom.oK) && isEQ(chdrlifIO.getChdrcoy(), lifeIO.getChdrcoy())
				&& isEQ(chdrlifIO.getChdrnum(), lifeIO.getChdrnum()))) {
			lifeIO.setStatuz(Varcom.endp);
			drylogrec.statuz.set(lifeIO.getStatuz());
			drylogrec.params.set(lifeIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void b000CallRounding() {
		/* B100-CALL */
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(drypDryprcRecInner.drypCompany);
		zrdecplrec.statuz.set(Varcom.oK);
		zrdecplrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(zrdecplrec.statuz);
			drylogrec.params.set(zrdecplrec.zrdecplRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* B900-EXIT */
	}

	/*
	 * Class transformed from Data Structure WSAA-T5399-ARRAY--INNER
	 */
	private static final class WsaaT5399ArrayInner {

		public WsaaT5399ArrayInner() {
			// Default Constructor
		}

		/* WSAA-T5399-ARRAY */
		// ILIFE-3472 Code Promotion for VPMS externalization of LIFE TRX product
		// calculations to LIFE Target2 Environment
		private FixedLengthStringData[] wsaaT5399Rec = FLSInittedArray(200, 103);
		private FixedLengthStringData[] wsaaT5399Key = FLSDArrayPartOfArrayStructure(7, wsaaT5399Rec, 0, HIVALUES);
		private FixedLengthStringData[] wsaaT5399AuthCode = FLSDArrayPartOfArrayStructure(4, wsaaT5399Key, 0);
		private FixedLengthStringData[] wsaaT5399Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5399Key, 4);
		private FixedLengthStringData[] wsaaT5399Data = FLSDArrayPartOfArrayStructure(96, wsaaT5399Rec, 7);
		private FixedLengthStringData[] wsaaT5399RiskStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 0);
		private FixedLengthStringData[][] wsaaT5399RiskStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399RiskStats,
				0);
		private FixedLengthStringData[] wsaaT5399PremStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 24);
		private FixedLengthStringData[][] wsaaT5399PremStat = FLSDArrayPartOfArrayStructure(12, 2, wsaaT5399PremStats,
				0);
		private FixedLengthStringData[] wsaaT5399SetRiskStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 48);
		private FixedLengthStringData[][] wsaaT5399SetRiskStat = FLSDArrayPartOfArrayStructure(12, 2,
				wsaaT5399SetRiskStats, 0);
		private FixedLengthStringData[] wsaaT5399SetPremStats = FLSDArrayPartOfArrayStructure(24, wsaaT5399Data, 72);
		private FixedLengthStringData[][] wsaaT5399SetPremStat = FLSDArrayPartOfArrayStructure(12, 2,
				wsaaT5399SetPremStats, 0);
	}

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		public FormatsInner() {
			// Default Constructor
		}

		private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
		private FixedLengthStringData liferec = new FixedLengthStringData(10).init("LIFEREC");
		private FixedLengthStringData hpuarec = new FixedLengthStringData(10).init("HPUAREC");
		private FixedLengthStringData hltxrec = new FixedLengthStringData(10).init("HLTXREC");
		private FixedLengthStringData linsrnlrec = new FixedLengthStringData(10).init("LINSRNLREC");
	}

	/*
	 * Class transformed from Data Structure CONTROL-TOTALS--INNER
	 */
	private static final class ControlTotalsInner {
		public ControlTotalsInner() {
			// Default Constructor
		}

		/* CONTROL-TOTALS */
		private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
		private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
		private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
		private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
		private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
		private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
		private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
		private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
		private ZonedDecimalData ct09 = new ZonedDecimalData(2, 0).init(9).setUnsigned();
		private ZonedDecimalData ct10 = new ZonedDecimalData(2, 0).init(10).setUnsigned();
		private ZonedDecimalData ct11 = new ZonedDecimalData(2, 0).init(11).setUnsigned();
		private ZonedDecimalData ct12 = new ZonedDecimalData(2, 0).init(12).setUnsigned();
	}
	
	private void updateDiaryRecord() {
		drypDryprcRecInner.drypBtdate.set(payrIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(payrIO.getPtdate());
		drypDryprcRecInner.drypBillchnl.set(payrIO.getBillchnl());
		drypDryprcRecInner.drypAplsupto.set(wsaaPayrAplspto);
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstcde());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStmdte());
	}

	/*
	 * Class transformed from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {
		public DrypDryprcRecInner() {
			// Default Constructor
		}

		// ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		// cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		// ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0,
				REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	}

	protected void setAutoLapsePeriodCustomerSpecific() {
		//default blank injection point for customers
	}
}
