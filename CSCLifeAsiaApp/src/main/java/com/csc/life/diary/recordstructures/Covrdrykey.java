package com.csc.life.diary.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:08:44
 * Description:
 * Copybook name: COVRDRYKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrdrykey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrdryFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData covrdryKey = new FixedLengthStringData(256).isAPartOf(covrdryFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrdryChdrcoy = new FixedLengthStringData(1).isAPartOf(covrdryKey, 0);
  	public FixedLengthStringData covrdryChdrnum = new FixedLengthStringData(8).isAPartOf(covrdryKey, 1);
  	public FixedLengthStringData covrdryLife = new FixedLengthStringData(2).isAPartOf(covrdryKey, 9);
  	public FixedLengthStringData covrdryCoverage = new FixedLengthStringData(2).isAPartOf(covrdryKey, 11);
  	public FixedLengthStringData covrdryRider = new FixedLengthStringData(2).isAPartOf(covrdryKey, 13);
  	public PackedDecimalData covrdryPlanSuffix = new PackedDecimalData(4, 0).isAPartOf(covrdryKey, 15);
  	public FixedLengthStringData filler = new FixedLengthStringData(238).isAPartOf(covrdryKey, 18, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrdryFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrdryFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}