/**
 * 
 */
package com.csc.life.diary.dataaccess.model;

/**
 * Bean for Dry5355
 * 
 * @author aashish4
 *
 */
public class Dry5355Dto {
	
	public Dry5355Dto()
	{
		//Default Constructor
	}

	private String chdrcoy;
	private String chdrnum;
	private int payrseqno;
	private int btdate;
	private int ptdate;
	private String billchnl;
	private String znfopt;
	private String validflag;
	private String aplsupr;
	private int aplspfrom;
	private int aplspto;
	private String notssupr;
	private int notsspfrom;
	private int notsspto;

	/**
	 * Getter for company
	 * 
	 * @return The chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * Setter for company
	 * 
	 * @param chdrcoy - The chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * Getter for Contract Number
	 * 
	 * @return the chdrnum
	 */
	public String getChdrnum() {
		return chdrnum;
	}

	/**
	 * Setter for Contract Number
	 * 
	 * @param chdrnum - The chdrnum to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	/**
	 * Getter for payrseqno
	 * 
	 * @return the payrseqno
	 */
	public int getPayrseqno() {
		return payrseqno;
	}

	/**
	 * Setter for payrseqno
	 * 
	 * @param payrseqno - The payrseqno to set
	 */
	public void setPayrseqno(int payrseqno) {
		this.payrseqno = payrseqno;
	}

	/**
	 * Getter for btdate
	 * 
	 * @return the btdate
	 */
	public int getBtdate() {
		return btdate;
	}

	/**
	 * Setter for btdate
	 * 
	 * @param btdate - The btdate to set
	 */
	public void setBtdate(int btdate) {
		this.btdate = btdate;
	}

	/**
	 * Getter for ptdate
	 * 
	 * @return the ptdate
	 */
	public int getPtdate() {
		return ptdate;
	}

	/**
	 * Setter for ptdate
	 * 
	 * @param ptdate - The ptdate to set
	 */
	public void setPtdate(int ptdate) {
		this.ptdate = ptdate;
	}

	/**
	 * Getter for billchnl
	 * 
	 * @return the billchnl
	 */
	public String getBillchnl() {
		return billchnl;
	}

	/**
	 * Setter for billchnl
	 * 
	 * @param billchnl - The billchnl to set
	 */
	public void setBillchnl(String billchnl) {
		this.billchnl = billchnl;
	}

	/**
	 * Getter for znfopt
	 * 
	 * @return the znfopt
	 */
	public String getZnfopt() {
		return znfopt;
	}

	/**
	 * Setter for znfopt
	 * 
	 * @param znfopt - The znfopt to set
	 */
	public void setZnfopt(String znfopt) {
		this.znfopt = znfopt;
	}

	/**
	 * Getter for validflag
	 * 
	 * @return the validflag
	 */
	public String getValidflag() {
		return validflag;
	}

	/**
	 * Setter for validflag
	 * 
	 * @param validflag - The validflag to set
	 */
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}

	/**
	 * Getter for aplsupr
	 * 
	 * @return the aplsupr
	 */
	public String getAplsupr() {
		return aplsupr;
	}

	/**
	 * Setter for aplsupr
	 * 
	 * @param aplsupr - The aplsupr to set
	 */
	public void setAplsupr(String aplsupr) {
		this.aplsupr = aplsupr;
	}

	/**
	 * Getter for aplspfrom
	 * 
	 * @return the aplspfrom
	 */
	public int getAplspfrom() {
		return aplspfrom;
	}

	/**
	 * Setter for aplspfrom
	 * 
	 * @param aplspfrom - The aplspfrom to set
	 */
	public void setAplspfrom(int aplspfrom) {
		this.aplspfrom = aplspfrom;
	}

	/**
	 * Getter for aplspto
	 * 
	 * @return the aplspto
	 */
	public int getAplspto() {
		return aplspto;
	}

	/**
	 * Setter for aplspto
	 * 
	 * @param aplspto - The aplspto to set
	 */
	public void setAplspto(int aplspto) {
		this.aplspto = aplspto;
	}

	/**
	 * Getter for notssupr
	 * 
	 * @return the notssupr
	 */
	public String getNotssupr() {
		return notssupr;
	}

	/**
	 * Setter for notssupr
	 * 
	 * @param notssupr - The notssupr to set
	 */
	public void setNotssupr(String notssupr) {
		this.notssupr = notssupr;
	}

	/**
	 * Getter for notsspfrom
	 * 
	 * @return the notsspfrom
	 */
	public int getNotsspfrom() {
		return notsspfrom;
	}

	/**
	 * Setter for notsspfrom
	 * 
	 * @param notsspfrom - The notsspfrom to set
	 */
	public void setNotsspfrom(int notsspfrom) {
		this.notsspfrom = notsspfrom;
	}

	/**
	 * Getter for notsspto
	 * 
	 * @return the notsspto
	 */
	public int getNotsspto() {
		return notsspto;
	}

	/**
	 * Setter for notsspto
	 * 
	 * @param notsspto - The notsspto to set
	 */
	public void setNotsspto(int notsspto) {
		this.notsspto = notsspto;
	}

}
