/*
 * File: Br598.java
 * Date: December 3, 2013 2:12:00 AM ICT
 * Author: CSC
 * 
 * Class transformed from BR598.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.LOVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.sql.SQLException;

import com.csc.fsu.general.dataaccess.ChdypfTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart400framework.batch.cls.Clrtmpf;
import com.csc.smart400framework.dataaccess.DiskFileDAM;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
* COMPILE-OPTIONS-SQL   CSRSQLCSR(*ENDJOB) COMMIT(*NONE) <Do Not D lete>
*      *
*
*(C) Copyright CSC Corporation Limited 1986 - 2000.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
* This program is the Splitter program for Portfolio Conversion
* for Diary Batch Processing to run in mulit-thread environment.
*
* The CHDRPF will be read via SQL with the following criteria:-
* i   chdrnum    between  <p6671-chdrnum>
*                    and  <p6671-chdrnum-1>
*
* Control totals used in this program:
*
*    01  -  No. of thread members
*    02  -  No. of CHDR extracted records
*
* Splitter programs should be simple. They should only deal with
* a single file. If there is insufficient data on the primary file
* to completely identify a transaction then the further editing
* should be done in the subsequent process. The objective of a
* Splitter is to rapidly isolate potential transactions, not
* to process the transaction.   The primary concern for Splitter
* program design is to minimise elapsed time and this is achieved
* by minimising disk IO.
*
* Before the Splitter process is invoked, the program CRTTMPF
* should be run.  CRTTMPF (Create Temporary File) will create
* a duplicate of a physical file (created under Smart and defined
* as a Query file) which will have as many members as are defined
* in that process's 'subsequent threads' field. The temporary file
* will have the name XXXXDD9999 where XXXX is the first four
* characters of the file which was duplicated, DD is the first two
* characters of the 4th system parameter and 9999 is the schedule
* run number. Each member added to the temporary file will be
* called THREAD999 where 999 is the number of the thread.
*
* The CRTTMPF and Splitter processes must be run single thread.
*
* The Splitter will simply read records from a primary file
* and, for each record selected,  it will write a record to a
* temporary file member. Exactly which temporary file member
* is written to is decided by a working storage count. The
* objective of the count is to spread the transaction records
* evenly amongst each thread member.
*
* The Splitter program should always have a restart method of
* '1' indicating a re-run. In the event of a restart, the Splitter
* program will always start with empty file members. Apart from
* writing records to the temporary file members, Splitter programs
* should not be doing any further updates.
*
*
* Notes for programs which use Splitter Program Temporary Files.
* --------------------------------------------------------------
*
* The Multi Thread Batch Environment will be able to submit multip e
* versions of the program which use the file members created from
* this program. It is important that the process which runs the
* splitter program has the same 'number of subsequent threads' as
* the following process has defined in 'number of threads this
* process'.
*
* The subsequent program need only point itself to one member
* based around the field BSPR-SCHEDULE-THREAD-NO which indicates
* in which thread the copy of the subsequent program is running.
*
****************************************************************** ****
* </pre>
*/
public class Br598 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private boolean sqlerrorflag;
	private java.sql.ResultSet sqlchdrCursorrs = null;
	private java.sql.PreparedStatement sqlchdrCursorps = null;
	private java.sql.Connection sqlchdrCursorconn = null;
	private String sqlchdrCursor = "";
	private int chdrCursorLoopIndex = 0;
	private ChdypfTableDAM chdypf = new ChdypfTableDAM();
	private DiskFileDAM chdy01 = new DiskFileDAM("CHDY01");
	private DiskFileDAM chdy02 = new DiskFileDAM("CHDY02");
	private DiskFileDAM chdy03 = new DiskFileDAM("CHDY03");
	private DiskFileDAM chdy04 = new DiskFileDAM("CHDY04");
	private DiskFileDAM chdy05 = new DiskFileDAM("CHDY05");
	private DiskFileDAM chdy06 = new DiskFileDAM("CHDY06");
	private DiskFileDAM chdy07 = new DiskFileDAM("CHDY07");
	private DiskFileDAM chdy08 = new DiskFileDAM("CHDY08");
	private DiskFileDAM chdy09 = new DiskFileDAM("CHDY09");
	private DiskFileDAM chdy10 = new DiskFileDAM("CHDY10");
	private DiskFileDAM chdy11 = new DiskFileDAM("CHDY11");
	private DiskFileDAM chdy12 = new DiskFileDAM("CHDY12");
	private DiskFileDAM chdy13 = new DiskFileDAM("CHDY13");
	private DiskFileDAM chdy14 = new DiskFileDAM("CHDY14");
	private DiskFileDAM chdy15 = new DiskFileDAM("CHDY15");
	private DiskFileDAM chdy16 = new DiskFileDAM("CHDY16");
	private DiskFileDAM chdy17 = new DiskFileDAM("CHDY17");
	private DiskFileDAM chdy18 = new DiskFileDAM("CHDY18");
	private DiskFileDAM chdy19 = new DiskFileDAM("CHDY19");
	private DiskFileDAM chdy20 = new DiskFileDAM("CHDY20");
	private ChdypfTableDAM chdypfData = new ChdypfTableDAM();
	private FixedLengthStringData chdy01Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy02Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy03Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy04Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy05Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy06Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy07Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy08Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy09Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy10Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy11Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy12Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy13Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy14Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy15Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy16Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy17Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy18Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy19Rec = new FixedLengthStringData(14);
	private FixedLengthStringData chdy20Rec = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR598");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0).setUnsigned();
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private FixedLengthStringData wsaa1 = new FixedLengthStringData(1).init("1");

		/*  CHDY member parametres.*/
	private FixedLengthStringData wsaaChdyFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaChdyFn, 0, FILLER).init("CHDY");
	private FixedLengthStringData wsaaChdyRunid = new FixedLengthStringData(2).isAPartOf(wsaaChdyFn, 4);
	private ZonedDecimalData wsaaChdyJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaChdyFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaChdrnumFrom = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaChdrnumTo = new FixedLengthStringData(8).init(SPACES);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1).init("Y");
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaEofInBlock = new FixedLengthStringData(1).init("N");
	private Validator eofInBlock = new Validator(wsaaEofInBlock, "Y");
	private FixedLengthStringData wsaaClrtmpfError = new FixedLengthStringData(97);
		/*  Pointers to point to the member to read (IY) and write (IZ).*/
	private PackedDecimalData iy = new PackedDecimalData(5, 0);
	private ZonedDecimalData iz = new ZonedDecimalData(2, 0).setUnsigned();

		/*  SQL error message formatting.*/
	private FixedLengthStringData wsaaSqlError = new FixedLengthStringData(97);
	private FixedLengthStringData filler3 = new FixedLengthStringData(16).isAPartOf(wsaaSqlError, 0, FILLER).init(SPACES);
	private FixedLengthStringData wsaaSqlcode = new FixedLengthStringData(5).isAPartOf(wsaaSqlError, 16);
	private FixedLengthStringData wsaaSqlmessage = new FixedLengthStringData(76).isAPartOf(wsaaSqlError, 21);
	private ZonedDecimalData sqlErrorCode = new ZonedDecimalData(10, 0).setPattern("-Z999999999");

	private FixedLengthStringData filler5 = new FixedLengthStringData(11).isAPartOf(sqlErrorCode, 0, FILLER_REDEFINE);
	private FixedLengthStringData sqlSign = new FixedLengthStringData(1).isAPartOf(filler5, 0);
	private FixedLengthStringData sqlStatuz = new FixedLengthStringData(3).isAPartOf(filler5, 8);
		/*  Define the number of rows in the block to be fetched.*/
	private ZonedDecimalData wsaaRowsInBlock = new ZonedDecimalData(8, 0).init(1000);

		/* WSAA-FETCH-ARRAY */
	private FixedLengthStringData[] wsaaChdrData = FLSInittedArray (1000, 14);
	private FixedLengthStringData[] wsaaChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaChdrData, 0);
	private FixedLengthStringData[] wsaaChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaChdrData, 1);
	private PackedDecimalData[] wsaaOccdate = PDArrayPartOfArrayStructure(8, 0, wsaaChdrData, 9);

		/* WSAA-IND-ARRAY */
	private FixedLengthStringData[] wsaaChdrInd = FLSInittedArray (1000, 12);
	private BinaryData[][] wsaaNullInd = BDArrayPartOfArrayStructure(6, 4, 0, wsaaChdrInd, 0);
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String esql = "ESQL";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private IntegerData wsaaInd = new IntegerData();
	private P6671par p6671par = new P6671par();

	public Br598() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** This section is invoked in restart mode. Note section 1100 must*/
		/** clear each temporary file member as the members are not under*/
		/** commitment control. Note also that Splitter programs must have*/
		/** a Restart Method of 1 (re-run). Thus no updating should occur*/
		/** which would result in different records being returned from the*/
		/** primary file in a restart!*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		/*    Check the restart method is compatible with the program.*/
		/*    This program is restartable but will always re-run from*/
		/*    the beginning. This is because the temporary file members*/
		/*    are not under commitment control.*/
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*    Do a ovrdbf for each temporary file member. (Note we have*/
		/*    allowed for a maximum of 20.)*/
		if (isGT(bprdIO.getThreadsSubsqntProc(), 20)) {
			bprdIO.setThreadsSubsqntProc(20);
		}
		wsaaChdyRunid.set(bprdIO.getSystemParam04());
		wsaaCompany.set(bprdIO.getCompany());
		wsaaChdyJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		/*    Since this program runs from a parameter screen move the*/
		/*    internal parameter area to the original parameter copybook*/
		/*    to retrieve the contract range*/
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		if (isEQ(p6671par.chdrnum, SPACES)
		&& isEQ(p6671par.chdrnum1, SPACES)) {
			wsaaChdrnumFrom.set(LOVALUE);
			wsaaChdrnumTo.set(HIVALUE);
		}
		else {
			wsaaChdrnumFrom.set(p6671par.chdrnum);
			wsaaChdrnumTo.set(p6671par.chdrnum1);
		}
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			openThreadMember1100();
		}
		/*    Log the number of threads being used*/
		contotrec.totval.set(bprdIO.getThreadsSubsqntProc());
		contotrec.totno.set(ct01);
		callContot001();
		iy.set(1);
		sqlchdrCursor = " SELECT  CHDRCOY, CHDRNUM, OCCDATE" +
" FROM   " + getAppVars().getTableNameOverriden("CHDRPF") + " " +
" WHERE CHDRNUM >= ?" +
" AND CHDRNUM <= ?" +
" AND CHDRCOY = ?" +
" AND VALIDFLAG = ?";
		/*    Initialise the array which will hold all the records*/
		/*    returned from each fetch. (INITIALIZE will not work as the*/
		/*    the array is indexed.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		sqlerrorflag = false;
		try {
			sqlchdrCursorconn = getAppVars().getDBConnectionForTable(new com.csc.fsu.general.dataaccess.ChdrpfTableDAM());
			sqlchdrCursorps = getAppVars().prepareStatementEmbeded(sqlchdrCursorconn, sqlchdrCursor, "CHDRPF");
			getAppVars().setDBString(sqlchdrCursorps, 1, wsaaChdrnumFrom);
			getAppVars().setDBString(sqlchdrCursorps, 2, wsaaChdrnumTo);
			getAppVars().setDBString(sqlchdrCursorps, 3, wsaaCompany);
			getAppVars().setDBString(sqlchdrCursorps, 4, wsaa1);
			sqlchdrCursorrs = getAppVars().executeQuery(sqlchdrCursorps);
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
	}

protected void openThreadMember1100()
	{
		openThreadMember1110();
	}

protected void openThreadMember1110()
	{
		/*    The next thing we must do is construct the name of the*/
		/*    temporary file which we will be working with.*/
		compute(wsaaThreadNumber, 0).set(add(sub(bsprIO.getStartMember(), 1), iz));
		callProgram(Clrtmpf.class, bprdIO.getRunLibrary(), wsaaChdyFn, wsaaThreadMember, wsaaStatuz);
		if (isNE(wsaaStatuz, varcom.oK)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression("CLRTMPF", SPACES);
			stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaChdyFn);
			stringVariable1.addExpression(SPACES);
			stringVariable1.addExpression(wsaaThreadMember);
			stringVariable1.setStringInto(wsaaClrtmpfError);
			syserrrec.statuz.set(wsaaStatuz);
			syserrrec.params.set(wsaaClrtmpfError);
			fatalError600();
		}
		/*    Do the override.*/
		StringUtil stringVariable2 = new StringUtil();
		stringVariable2.addExpression("OVRDBF FILE(CHDY");
		stringVariable2.addExpression(iz);
		stringVariable2.addExpression(") TOFILE(");
		stringVariable2.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable2.addExpression("/");
		stringVariable2.addExpression(wsaaChdyFn);
		stringVariable2.addExpression(") MBR(");
		stringVariable2.addExpression(wsaaThreadMember);
		stringVariable2.addExpression(")");
		stringVariable2.addExpression(" SEQONLY(*YES 1000)");
		stringVariable2.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		/*    Open the file.*/
		if (isEQ(iz, 1)) {
			chdy01.openOutput();
		}
		if (isEQ(iz, 2)) {
			chdy02.openOutput();
		}
		if (isEQ(iz, 3)) {
			chdy03.openOutput();
		}
		if (isEQ(iz, 4)) {
			chdy04.openOutput();
		}
		if (isEQ(iz, 5)) {
			chdy05.openOutput();
		}
		if (isEQ(iz, 6)) {
			chdy06.openOutput();
		}
		if (isEQ(iz, 7)) {
			chdy07.openOutput();
		}
		if (isEQ(iz, 8)) {
			chdy08.openOutput();
		}
		if (isEQ(iz, 9)) {
			chdy09.openOutput();
		}
		if (isEQ(iz, 10)) {
			chdy10.openOutput();
		}
		if (isEQ(iz, 11)) {
			chdy11.openOutput();
		}
		if (isEQ(iz, 12)) {
			chdy12.openOutput();
		}
		if (isEQ(iz, 13)) {
			chdy13.openOutput();
		}
		if (isEQ(iz, 14)) {
			chdy14.openOutput();
		}
		if (isEQ(iz, 15)) {
			chdy15.openOutput();
		}
		if (isEQ(iz, 16)) {
			chdy16.openOutput();
		}
		if (isEQ(iz, 17)) {
			chdy17.openOutput();
		}
		if (isEQ(iz, 18)) {
			chdy18.openOutput();
		}
		if (isEQ(iz, 19)) {
			chdy19.openOutput();
		}
		if (isEQ(iz, 20)) {
			chdy20.openOutput();
		}
	}

protected void initialiseArray1500()
	{
		/*START*/
		wsaaChdrcoy[wsaaInd.toInt()].set(SPACES);
		wsaaChdrnum[wsaaInd.toInt()].set(SPACES);
		wsaaOccdate[wsaaInd.toInt()].set(ZERO);
		/*EXIT*/
	}

protected void readFile2000()
	{
		readFiles2010();
	}

protected void readFiles2010()
	{
		/*  We only need to FETCH records when a full block has been*/
		/*  processed or if the end of file was retrieved in the last*/
		/*  block.*/
		if (eofInBlock.isTrue()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		if (isNE(wsaaInd, 1)) {
			return ;
		}
		sqlerrorflag = false;
		try {
			for (chdrCursorLoopIndex = 1; isLT(chdrCursorLoopIndex, wsaaRowsInBlock.toInt())
			&& getAppVars().fetchNext(sqlchdrCursorrs); chdrCursorLoopIndex++ ){
				getAppVars().getDBObject(sqlchdrCursorrs, 1, wsaaChdrcoy[chdrCursorLoopIndex], wsaaNullInd[chdrCursorLoopIndex][1]);
				getAppVars().getDBObject(sqlchdrCursorrs, 2, wsaaChdrnum[chdrCursorLoopIndex], wsaaNullInd[chdrCursorLoopIndex][2]);
				getAppVars().getDBObject(sqlchdrCursorrs, 3, wsaaOccdate[chdrCursorLoopIndex], wsaaNullInd[chdrCursorLoopIndex][3]);
			}
		}
		catch (SQLException ex){
			sqlerrorflag = true;
			getAppVars().setSqlErrorCode(ex);
		}
		if (sqlerrorflag) {
			sqlError500();
		}
		/*  An SQLCODE = +100 is returned when :-*/
		/*      a) no rows are returned on the first fetch*/
		/*      b) the last row of the cursor is either in the block or is*/
		/*         the last row of the block.*/
		/*  We must continue processing to the 3000- section for case b)*/
		if (isEQ(getAppVars().getSqlErrorCode(), 100)
		&& isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)) {
			wsspEdterror.set(varcom.endp);
		}
		else {
			if (firstTime.isTrue()) {
				wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
				wsaaFirstTime.set("N");
			}
		}
	}

protected void edit2500()
	{
		/*READ*/
		wsspEdterror.set(varcom.oK);
		/*EXIT*/
	}

protected void update3000()
	{
		/*UPDATE*/
		wsaaInd.set(1);
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| eofInBlock.isTrue())) {
			loadThreads3100();
		}
		
		/*  Re-initialise the block for next fetch and point to first row.*/
		for (wsaaInd.set(1); !(isGT(wsaaInd, wsaaRowsInBlock)); wsaaInd.add(1)){
			initialiseArray1500();
		}
		wsaaInd.set(1);
		/*EXIT*/
	}

protected void loadThreads3100()
	{
		/*START*/
		/*  If the the CHDRNUM being processed is not equal to the*/
		/*  to the previous we should move to the next output file member.*/
		/*  The condition is here to allow for checking the last chdrnum*/
		/*  of the old block with the first of the new and to move to the*/
		/*  next file member to write to if they have changed.*/
		if (isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)) {
			wsaaPrevChdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
			iy.add(1);
		}
		/*  Load from storage all CHDY data for the same contract until*/
		/*  the CHDRNUM on CHDY has changed,*/
		/*    OR*/
		/*  The end of an incomplete block is reached.*/
		while ( !(isGT(wsaaInd, wsaaRowsInBlock)
		|| isNE(wsaaChdrnum[wsaaInd.toInt()], wsaaPrevChdrnum)
		|| eofInBlock.isTrue())) {
			loadSameContracts3200();
		}
		
		/*EXIT*/
	}

protected void loadSameContracts3200()
	{
		start3200();
	}

protected void start3200()
	{
		chdypfData.chdrcoy.set(wsaaChdrcoy[wsaaInd.toInt()]);
		chdypfData.chdrnum.set(wsaaChdrnum[wsaaInd.toInt()]);
		chdypfData.occdate.set(wsaaOccdate[wsaaInd.toInt()]);
		if (isGT(iy, bprdIO.getThreadsSubsqntProc())) {
			iy.set(1);
		}
		if (isEQ(iy, 1)) {
			chdy01.write(chdypfData);
		}
		if (isEQ(iy, 2)) {
			chdy02.write(chdypfData);
		}
		if (isEQ(iy, 3)) {
			chdy03.write(chdypfData);
		}
		if (isEQ(iy, 4)) {
			chdy04.write(chdypfData);
		}
		if (isEQ(iy, 5)) {
			chdy05.write(chdypfData);
		}
		if (isEQ(iy, 6)) {
			chdy06.write(chdypfData);
		}
		if (isEQ(iy, 7)) {
			chdy07.write(chdypfData);
		}
		if (isEQ(iy, 8)) {
			chdy08.write(chdypfData);
		}
		if (isEQ(iy, 9)) {
			chdy09.write(chdypfData);
		}
		if (isEQ(iy, 10)) {
			chdy10.write(chdypfData);
		}
		if (isEQ(iy, 11)) {
			chdy11.write(chdypfData);
		}
		if (isEQ(iy, 12)) {
			chdy12.write(chdypfData);
		}
		if (isEQ(iy, 13)) {
			chdy13.write(chdypfData);
		}
		if (isEQ(iy, 14)) {
			chdy14.write(chdypfData);
		}
		if (isEQ(iy, 15)) {
			chdy15.write(chdypfData);
		}
		if (isEQ(iy, 16)) {
			chdy16.write(chdypfData);
		}
		if (isEQ(iy, 17)) {
			chdy17.write(chdypfData);
		}
		if (isEQ(iy, 18)) {
			chdy18.write(chdypfData);
		}
		if (isEQ(iy, 19)) {
			chdy19.write(chdypfData);
		}
		if (isEQ(iy, 20)) {
			chdy20.write(chdypfData);
		}
		/*    Log the number of extracted records.*/
		contotrec.totval.set(1);
		contotrec.totno.set(ct02);
		callContot001();
		/*  Set up the array for the next block of records.*/
		/*  Otherwise process the next record in the array.*/
		wsaaInd.add(1);
		/*  Check for an incomplete block retrieved.*/
		if (isEQ(wsaaChdrnum[wsaaInd.toInt()], SPACES)
		&& isLTE(wsaaInd, wsaaRowsInBlock)) {
			wsaaEofInBlock.set("Y");
		}
	}

protected void commit3500()
	{
		/*COMMIT*/
		/**    There is no pre-commit processing to be done*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/**    There is no pre-rollback processing to be done*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		/*  Close the open files and remove the overrides.*/
		getAppVars().freeDBConnectionIgnoreErr(sqlchdrCursorconn, sqlchdrCursorps, sqlchdrCursorrs);
		for (iz.set(1); !(isGT(iz, bprdIO.getThreadsSubsqntProc())); iz.add(1)){
			close4100();
		}
		wsaaQcmdexc.set("DLTOVR FILE(*ALL)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void close4100()
	{
		close4110();
	}

protected void close4110()
	{
		if (isEQ(iz, 1)) {
			chdy01.close();
		}
		if (isEQ(iz, 2)) {
			chdy02.close();
		}
		if (isEQ(iz, 3)) {
			chdy03.close();
		}
		if (isEQ(iz, 4)) {
			chdy04.close();
		}
		if (isEQ(iz, 5)) {
			chdy05.close();
		}
		if (isEQ(iz, 6)) {
			chdy06.close();
		}
		if (isEQ(iz, 7)) {
			chdy07.close();
		}
		if (isEQ(iz, 8)) {
			chdy08.close();
		}
		if (isEQ(iz, 9)) {
			chdy09.close();
		}
		if (isEQ(iz, 10)) {
			chdy10.close();
		}
		if (isEQ(iz, 11)) {
			chdy11.close();
		}
		if (isEQ(iz, 12)) {
			chdy12.close();
		}
		if (isEQ(iz, 13)) {
			chdy13.close();
		}
		if (isEQ(iz, 14)) {
			chdy14.close();
		}
		if (isEQ(iz, 15)) {
			chdy15.close();
		}
		if (isEQ(iz, 16)) {
			chdy16.close();
		}
		if (isEQ(iz, 17)) {
			chdy17.close();
		}
		if (isEQ(iz, 18)) {
			chdy18.close();
		}
		if (isEQ(iz, 19)) {
			chdy19.close();
		}
		if (isEQ(iz, 20)) {
			chdy20.close();
		}
	}

protected void sqlError500()
	{
		/*CALL-SYSTEM-ERROR*/
		sqlErrorCode.set(getAppVars().getSqlErrorCode());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(sqlSign);
		stringVariable1.addExpression(sqlStatuz);
		stringVariable1.setStringInto(wsaaSqlcode);
		wsaaSqlmessage.set(getAppVars().getSqlMessage());
		syserrrec.statuz.set(esql);
		syserrrec.params.set(wsaaSqlError);
		fatalError600();
	}
}
