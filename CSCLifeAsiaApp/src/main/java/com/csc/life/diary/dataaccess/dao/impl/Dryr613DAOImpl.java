package com.csc.life.diary.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.diary.dataaccess.dao.Dryr613DAO;
import com.csc.life.diary.dataaccess.model.Dryr613Dto;
import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * DAO Implementation for Dryr613
 * 
 * @author akash
 *
 */
public class Dryr613DAOImpl extends BaseDAOImpl<Dryr613Dto> implements Dryr613DAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(Dryr613DAOImpl.class);
	
	
	public Dryr613DAOImpl()
	{
		//Default Constructor
	}
	
	
	private static final String CHDRCOY  = "CHDRCOY";
	private static final String CHDRNUM  = "CHDRNUM";
	private static final String COVERAGE = "COVERAGE";
	private static final String CURRFROM = "CURRFROM";
	private static final String CURRTO   = "CURRTO";
	private static final String PLNSFX   = "PLNSFX";
	private static final String RIDER    = "RIDER";
	private static final String TERMID   = "TERMID";
	private static final String TRANNO   = "TRANNO";
	private static final String UNIQUE_NUMBER = "UNIQUE_NUMBER";
	private static final String USER_T = "USER_T";
	private static final String VALIDFLAG = "VALIDFLAG";
	
	
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.life.diary.dataaccess.dao.Dryr613DAO#getReRate(java.lang.String,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public List<Dryr613Dto> getReRate(String company, String billDate, String chdrNum) {

		StringBuilder query = new StringBuilder();

		query.append("SELECT CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX");
		query.append(" FROM COVRPF WHERE CHDRCOY = ? AND CHDRNUM = ?");
		query.append(" AND VALIDFLAG = ? AND NOT RRTDAT = ? AND RRTDAT <= ?");
		query.append(" ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX");

		List<Dryr613Dto> dryr613list = new ArrayList<>();
		try (PreparedStatement preparedStatement = getPrepareStatement(query.toString())) {

			preparedStatement.setString(1, company);
			preparedStatement.setString(2, chdrNum);
			preparedStatement.setString(3, "1");
			preparedStatement.setString(4, "0");
			preparedStatement.setString(5, billDate);

			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Dryr613Dto dryr613dto = new Dryr613Dto();
					dryr613dto.setChdrcoy(resultSet.getString(CHDRCOY));
					dryr613dto.setChdrNum(resultSet.getString(CHDRNUM));
					dryr613dto.setLife(resultSet.getString("LIFE"));
					dryr613dto.setCoverage(resultSet.getString(COVERAGE));
					dryr613dto.setRider(resultSet.getString(RIDER));
					dryr613dto.setPlnsfx(resultSet.getInt(PLNSFX));
					dryr613dto.setValidFlag("1");
					dryr613list.add(dryr613dto);
				}
			}
			return dryr613list;
		} catch (SQLException e) {
			LOGGER.error("Error occurred when reading COVRPF data", e);
			throw new SQLRuntimeException(e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.life.diary.dataaccess.dao.Dryr613DAO#searchCovrRecord(java.lang.
	 * String, java.util.List)
	 */
	@Override
	public Map<String, List<Covrpf>> searchCovrRecord(String coy, List<String> chdrnumList) {
		StringBuilder sqlCovrSelect1 = new StringBuilder();
		sqlCovrSelect1.append("SELECT CHDRCOY,CHDRNUM,CBCVIN,COVERAGE,CRRCD,CRTABLE,CURRFROM,INSTPREM,");
		sqlCovrSelect1.append("JLIFE,LIFE,PLNSFX,PCESTRM,PRMCUR,PSTATCODE,RRTDAT,RIDER,RCESTRM,SINGP,");
		sqlCovrSelect1.append("STATCODE,SUMINS,UNIQUE_NUMBER,VALIDFLAG,ZLINSTPREM, TRANNO, CURRTO, STFUND, ");
		sqlCovrSelect1.append("STSECT, STSSECT, RCESDTE, PCESDTE, BCESDTE, NXTDTE, RCESAGE, PCESAGE, BCESAGE, ");
		sqlCovrSelect1.append("BCESTRM, SICURR, VARSI, MORTCLS, REPTCD01, REPTCD02, REPTCD03, REPTCD04, REPTCD05, ");
		sqlCovrSelect1.append("REPTCD06, CRINST01, CRINST02, CRINST03, CRINST04, CRINST05, TERMID, TRDT, TRTM, USER_T, ");
		sqlCovrSelect1.append("LIENCD, RATCLS, INDXIN, BNUSIN, DPCD, DPAMT, DPIND, TMBEN, EMV01, EMV02, EMVDTE01, ");
		sqlCovrSelect1.append("EMVDTE02, EMVINT01, EMVINT02, CAMPAIGN, STSMIN, RTRNYRS, RSUNIN, RUNDTE, CHGOPT, ");
		sqlCovrSelect1.append("FUNDSP, STATREASN, ANBCCD,  SEX, PCAMTH, PCADAY, PCTMTH, PCTDAY, RCAMTH, RCADAY, ");
		sqlCovrSelect1.append("RCTMTH, RCTDAY, JLLSID,  RRTFRM, BBLDAT, CBANPR,  CBRVPR, CBUNST, CPIDTE, ");
		sqlCovrSelect1.append("ICANDT, EXALDT, IINCDT, CRDEBT, PAYRSEQNO, BAPPMETH, ZBINSTPREM, LOADPER, ");
		sqlCovrSelect1.append("RATEADJ, FLTMORT, PREMADJ, AGEADJ, ZCLSTATE, LNKGNO, LNKGSUBREFNO, REINSTATED, ");
		sqlCovrSelect1.append("ZSTPDUTY01, TPDTYPE, PRORATEPREM  FROM COVRPF WHERE CHDRCOY=? AND ");
		sqlCovrSelect1.append("VALIDFLAG='1' AND ");
		sqlCovrSelect1.append(getSqlInStr(CHDRNUM, chdrnumList));
		sqlCovrSelect1.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC, COVERAGE ASC, ");
		sqlCovrSelect1.append("RIDER ASC, PLNSFX ASC, UNIQUE_NUMBER DESC");
		PreparedStatement psCovrSelect = getPrepareStatement(sqlCovrSelect1.toString());
		ResultSet sqlcovrpf1rs = null;
		Map<String, List<Covrpf>> covrMap = new HashMap<String, List<Covrpf>>();
		List<Covrpf> covrpfSearchResult = new LinkedList<Covrpf>();
		try {
			psCovrSelect.setInt(1, Integer.parseInt(coy));
			sqlcovrpf1rs = psCovrSelect.executeQuery();

			while (sqlcovrpf1rs.next()) {
				Covrpf covrpf = createCovrpfFromResultSet1(sqlcovrpf1rs);
				String chdrnum = covrpf.getChdrnum();
				if (covrMap.containsKey(chdrnum)) {
					covrMap.get(chdrnum).add(covrpf);
				} else {
					covrpfSearchResult = new LinkedList<Covrpf>();
					covrpfSearchResult.add(covrpf);
					covrMap.put(chdrnum, covrpfSearchResult);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchCovrRecord()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(psCovrSelect, sqlcovrpf1rs);
		}
		return covrMap;
	}
	
	private Covrpf createCovrpfFromResultSet1(ResultSet sqlcovrpf1rs) throws SQLException {
		Covrpf covrpf = new Covrpf();
		covrpf.setChdrcoy(sqlcovrpf1rs.getString(CHDRCOY));
		covrpf.setChdrnum(sqlcovrpf1rs.getString(CHDRNUM));
		covrpf.setConvertInitialUnits(sqlcovrpf1rs.getInt("CBCVIN"));
		covrpf.setCoverage(sqlcovrpf1rs.getString(COVERAGE));
		covrpf.setCrrcd(sqlcovrpf1rs.getInt("CRRCD"));
		covrpf.setCrtable(sqlcovrpf1rs.getString("CRTABLE"));
		covrpf.setCurrfrom(sqlcovrpf1rs.getInt(CURRFROM));
		covrpf.setInstprem(sqlcovrpf1rs.getBigDecimal("INSTPREM"));
		covrpf.setJlife(sqlcovrpf1rs.getString("JLIFE"));
		covrpf.setLife(sqlcovrpf1rs.getString("LIFE"));
		covrpf.setPlanSuffix(sqlcovrpf1rs.getInt(PLNSFX));
		covrpf.setPremCessTerm(sqlcovrpf1rs.getInt("PCESTRM"));
		covrpf.setPremCurrency(sqlcovrpf1rs.getString("PRMCUR"));
		covrpf.setPstatcode(sqlcovrpf1rs.getString("PSTATCODE"));
		covrpf.setRerateDate(sqlcovrpf1rs.getInt("RRTDAT"));
		covrpf.setRider(sqlcovrpf1rs.getString(RIDER));
		covrpf.setRiskCessTerm(sqlcovrpf1rs.getInt("RCESTRM"));
		covrpf.setSingp(sqlcovrpf1rs.getBigDecimal("SINGP"));
		covrpf.setStatcode(sqlcovrpf1rs.getString("STATCODE"));
		covrpf.setSumins(sqlcovrpf1rs.getBigDecimal("SUMINS"));
		covrpf.setUniqueNumber(sqlcovrpf1rs.getLong(UNIQUE_NUMBER));
		covrpf.setValidflag(sqlcovrpf1rs.getString(VALIDFLAG));
		covrpf.setZlinstprem(sqlcovrpf1rs.getBigDecimal("ZLINSTPREM"));
		covrpf.setTranno(sqlcovrpf1rs.getInt(TRANNO));
		covrpf.setCurrto(sqlcovrpf1rs.getInt(CURRTO));
		covrpf.setStatFund(sqlcovrpf1rs.getString("STFUND"));
		covrpf.setStatSect(sqlcovrpf1rs.getString("STSECT"));
		covrpf.setStatSubsect(sqlcovrpf1rs.getString("STSSECT"));
		covrpf.setRiskCessDate(sqlcovrpf1rs.getInt("RCESDTE"));
		covrpf.setPremCessDate(sqlcovrpf1rs.getInt("PCESDTE"));
		covrpf.setBenCessDate(sqlcovrpf1rs.getInt("BCESDTE"));
		covrpf.setNextActDate(sqlcovrpf1rs.getInt("NXTDTE"));
		covrpf.setRiskCessAge(sqlcovrpf1rs.getInt("RCESAGE"));
		covrpf.setPremCessAge(sqlcovrpf1rs.getInt("PCESAGE"));
		covrpf.setBenCessAge(sqlcovrpf1rs.getInt("BCESAGE"));
		covrpf.setBenCessTerm(sqlcovrpf1rs.getInt("BCESTRM"));
		covrpf.setSicurr(sqlcovrpf1rs.getString("SICURR"));
		covrpf.setVarSumInsured(sqlcovrpf1rs.getBigDecimal("VARSI"));
		covrpf.setMortcls(sqlcovrpf1rs.getString("MORTCLS"));
		covrpf.setReptcd01(sqlcovrpf1rs.getString("REPTCD01"));
		covrpf.setReptcd02(sqlcovrpf1rs.getString("REPTCD02"));
		covrpf.setReptcd03(sqlcovrpf1rs.getString("REPTCD03"));
		covrpf.setReptcd04(sqlcovrpf1rs.getString("REPTCD04"));
		covrpf.setReptcd05(sqlcovrpf1rs.getString("REPTCD05"));
		covrpf.setReptcd06(sqlcovrpf1rs.getString("REPTCD06"));
		covrpf.setCrInstamt01(sqlcovrpf1rs.getBigDecimal("CRINST01"));
		covrpf.setCrInstamt02(sqlcovrpf1rs.getBigDecimal("CRINST02"));
		covrpf.setCrInstamt03(sqlcovrpf1rs.getBigDecimal("CRINST03"));
		covrpf.setCrInstamt04(sqlcovrpf1rs.getBigDecimal("CRINST04"));
		covrpf.setCrInstamt05(sqlcovrpf1rs.getBigDecimal("CRINST05"));
		covrpf.setTermid(sqlcovrpf1rs.getString(TERMID));
		covrpf.setTransactionDate(sqlcovrpf1rs.getInt("TRDT"));
		covrpf.setTransactionTime(sqlcovrpf1rs.getInt("TRTM"));
		covrpf.setUser(sqlcovrpf1rs.getInt(USER_T));
		covrpf.setLiencd(sqlcovrpf1rs.getString("LIENCD"));
		covrpf.setRatingClass(sqlcovrpf1rs.getString("RATCLS"));
		covrpf.setIndexationInd(sqlcovrpf1rs.getString("INDXIN"));
		covrpf.setBonusInd(sqlcovrpf1rs.getString("BNUSIN"));
		covrpf.setDeferPerdCode(sqlcovrpf1rs.getString("DPCD"));
		covrpf.setDeferPerdAmt(sqlcovrpf1rs.getBigDecimal("DPAMT"));
		covrpf.setDeferPerdInd(sqlcovrpf1rs.getString("DPIND"));
		covrpf.setTotMthlyBenefit(sqlcovrpf1rs.getBigDecimal("TMBEN"));
		createCovrpfFromResultSet2(covrpf, sqlcovrpf1rs);
		return covrpf;
	}
	
	private void createCovrpfFromResultSet2(Covrpf covrpf, ResultSet sqlcovrpf1rs) throws SQLException {
		covrpf.setEstMatValue01(sqlcovrpf1rs.getBigDecimal("EMV01"));
		covrpf.setEstMatValue02(sqlcovrpf1rs.getBigDecimal("EMV02"));
		covrpf.setEstMatDate01(sqlcovrpf1rs.getInt("EMVDTE01"));
		covrpf.setEstMatDate02(sqlcovrpf1rs.getInt("EMVDTE02"));
		covrpf.setEstMatInt01(sqlcovrpf1rs.getBigDecimal("EMVINT01"));
		covrpf.setEstMatInt02(sqlcovrpf1rs.getBigDecimal("EMVINT02"));
		covrpf.setCampaign(sqlcovrpf1rs.getString("CAMPAIGN"));
		covrpf.setStatSumins(sqlcovrpf1rs.getBigDecimal("STSMIN"));
		covrpf.setRtrnyrs(sqlcovrpf1rs.getInt("RTRNYRS"));
		covrpf.setReserveUnitsInd(sqlcovrpf1rs.getString("RSUNIN"));
		covrpf.setReserveUnitsDate(sqlcovrpf1rs.getInt("RUNDTE"));
		covrpf.setChargeOptionsInd(sqlcovrpf1rs.getString("CHGOPT"));
		covrpf.setFundSplitPlan(sqlcovrpf1rs.getString("FUNDSP"));
		covrpf.setStatreasn(sqlcovrpf1rs.getString("STATREASN"));
		covrpf.setAnbAtCcd(sqlcovrpf1rs.getInt("ANBCCD"));
		covrpf.setSex(sqlcovrpf1rs.getString("SEX"));
		covrpf.setPremCessAgeMth(sqlcovrpf1rs.getInt("PCAMTH"));
		covrpf.setPremCessAgeDay(sqlcovrpf1rs.getInt("PCADAY"));
		covrpf.setPremCessTermMth(sqlcovrpf1rs.getInt("PCTMTH"));
		covrpf.setPremCessTermDay(sqlcovrpf1rs.getInt("PCTDAY"));
		covrpf.setRiskCessAgeMth(sqlcovrpf1rs.getInt("RCAMTH"));
		covrpf.setRiskCessAgeDay(sqlcovrpf1rs.getInt("RCADAY"));
		covrpf.setRiskCessTermMth(sqlcovrpf1rs.getInt("RCTMTH"));
		covrpf.setRiskCessTermDay(sqlcovrpf1rs.getInt("RCTDAY"));
		covrpf.setJlLsInd(sqlcovrpf1rs.getString("JLLSID"));
		covrpf.setRerateFromDate(sqlcovrpf1rs.getInt("RRTFRM"));
		covrpf.setBenBillDate(sqlcovrpf1rs.getInt("BBLDAT"));
		covrpf.setAnnivProcDate(sqlcovrpf1rs.getInt("CBANPR"));
		covrpf.setReviewProcessing(sqlcovrpf1rs.getInt("CBRVPR"));
		covrpf.setUnitStatementDate(sqlcovrpf1rs.getInt("CBUNST"));
		covrpf.setCpiDate(sqlcovrpf1rs.getInt("CPIDTE"));
		covrpf.setInitUnitCancDate(sqlcovrpf1rs.getInt("ICANDT"));
		covrpf.setExtraAllocDate(sqlcovrpf1rs.getInt("EXALDT"));
		covrpf.setInitUnitIncrsDate(sqlcovrpf1rs.getInt("IINCDT"));
		covrpf.setCoverageDebt(sqlcovrpf1rs.getBigDecimal("CRDEBT"));
		covrpf.setPayrseqno(sqlcovrpf1rs.getInt("PAYRSEQNO"));
		covrpf.setBappmeth(sqlcovrpf1rs.getString("BAPPMETH"));
		covrpf.setZbinstprem(sqlcovrpf1rs.getBigDecimal("ZBINSTPREM"));
		covrpf.setLoadper(sqlcovrpf1rs.getBigDecimal("LOADPER"));
		covrpf.setRateadj(sqlcovrpf1rs.getBigDecimal("RATEADJ"));
		covrpf.setFltmort(sqlcovrpf1rs.getBigDecimal("FLTMORT"));
		covrpf.setPremadj(sqlcovrpf1rs.getBigDecimal("PREMADJ"));
		covrpf.setAgeadj(sqlcovrpf1rs.getBigDecimal("AGEADJ"));
		covrpf.setZclstate(sqlcovrpf1rs.getString("ZCLSTATE"));
		covrpf.setLnkgno(sqlcovrpf1rs.getString("LNKGNO"));// ILIFE-8248
		covrpf.setLnkgsubrefno(sqlcovrpf1rs.getString("LNKGSUBREFNO"));// ILIFE-8248
		covrpf.setReinstated(sqlcovrpf1rs.getString("REINSTATED"));// ILIFE-8509
		covrpf.setZstpduty01(sqlcovrpf1rs.getBigDecimal("ZSTPDUTY01"));
		covrpf.setTpdtype(sqlcovrpf1rs.getString("TPDTYPE"));
		covrpf.setProrateprem(sqlcovrpf1rs.getBigDecimal("PRORATEPREM"));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.csc.life.diary.dataaccess.dao.Dryr613DAO#getAgcmRecords(java.lang.String,
	 * java.util.List)
	 */
	@Override
	public Map<String, List<Agcmpf>> getAgcmRecords(String coy, List<String> chdrnumList) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,AGNTNUM,LIFE,JLIFE,COVERAGE,RIDER,PLNSFX,");
		sql.append("TRANNO,EFDATE,ANNPREM,BASCMETH,INITCOM,BASCPY,COMPAY,COMERN,SRVCPY,SCMDUE,SCMEARN,");
		sql.append("RNWCPY,RNLCDUE,RNLCEARN,AGCLS,TERMID,TRDT,TRTM,USER_T,CRTABLE,CURRFROM,CURRTO,");
		sql.append("VALIDFLAG,SEQNO,PTDATE,CEDAGENT,OVRDCAT,DORMFLAG FROM AGCMPF WHERE CHDRCOY=? AND ");
		sql.append(getSqlInStr(CHDRNUM, chdrnumList));
		sql.append(" AND (VALIDFLAG = '1' OR VALIDFLAG = ' ') ");
		sql.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC, LIFE ASC,COVERAGE ASC, RIDER ASC,PLNSFX ASC,AGNTNUM ASC ");
		PreparedStatement ps = getPrepareStatement(sql.toString());
		ResultSet rs = null;
		Map<String, List<Agcmpf>> agcmpfMap = new HashMap<String, List<Agcmpf>>();
		try {
			ps.setInt(1, Integer.parseInt(coy));
			rs = executeQuery(ps);

			while (rs.next()) {
				Agcmpf agcmpf = new Agcmpf();
				agcmpf.setUniqueNumber(rs.getLong(UNIQUE_NUMBER));
				agcmpf.setChdrcoy(rs.getString(CHDRCOY));
				agcmpf.setChdrnum(rs.getString(CHDRNUM));
				agcmpf.setAgntnum(rs.getString("AGNTNUM"));
				agcmpf.setLife(rs.getString("LIFE"));
				agcmpf.setJlife(rs.getString("JLIFE"));
				agcmpf.setCoverage(rs.getString(COVERAGE));
				agcmpf.setRider(rs.getString(RIDER));
				agcmpf.setPlanSuffix(rs.getInt(PLNSFX));
				agcmpf.setTranno(rs.getInt(TRANNO));
				agcmpf.setEfdate(rs.getInt("EFDATE"));
				agcmpf.setAnnprem(rs.getBigDecimal("ANNPREM"));
				agcmpf.setBasicCommMeth(rs.getString("BASCMETH"));
				agcmpf.setInitcom(rs.getBigDecimal("INITCOM"));
				agcmpf.setBascpy(rs.getString("BASCPY"));
				agcmpf.setCompay(rs.getBigDecimal("COMPAY"));
				agcmpf.setComern(rs.getBigDecimal("COMERN"));
				agcmpf.setSrvcpy(rs.getString("SRVCPY"));
				agcmpf.setScmdue(rs.getBigDecimal("SCMDUE"));
				agcmpf.setScmearn(rs.getBigDecimal("SCMEARN"));
				agcmpf.setRnwcpy(rs.getString("RNWCPY"));
				agcmpf.setRnlcdue(rs.getBigDecimal("RNLCDUE"));
				agcmpf.setRnlcearn(rs.getBigDecimal("RNLCEARN"));
				agcmpf.setAgentClass(rs.getString("AGCLS"));
				agcmpf.setTermid(rs.getString(TERMID));
				agcmpf.setTransactionDate(rs.getInt("TRDT"));
				agcmpf.setTransactionTime(rs.getInt("TRTM"));
				agcmpf.setUser(rs.getInt(USER_T));
				agcmpf.setCrtable(rs.getString("CRTABLE"));
				agcmpf.setCurrfrom(rs.getInt(CURRFROM));
				agcmpf.setCurrto(rs.getInt(CURRTO));
				agcmpf.setValidflag(rs.getString(VALIDFLAG));
				agcmpf.setSeqno(rs.getInt("SEQNO"));
				agcmpf.setPtdate(rs.getInt("PTDATE"));
				agcmpf.setCedagent(rs.getString("CEDAGENT"));
				agcmpf.setOvrdcat(rs.getString("OVRDCAT"));
				agcmpf.setDormantFlag(rs.getString("DORMFLAG"));

				String chdrnum = agcmpf.getChdrnum();
				if (agcmpfMap.containsKey(chdrnum)) {
					agcmpfMap.get(chdrnum).add(agcmpf);
				} else {
					List<Agcmpf> agcmpfSearchResult = new LinkedList<Agcmpf>();
					agcmpfSearchResult.add(agcmpf);
					agcmpfMap.put(chdrnum, agcmpfSearchResult);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchAgcmpf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return agcmpfMap;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.life.diary.dataaccess.dao.Dryr613DAO#getPcddpfRecords(java.lang.
	 * String, java.util.List)
	 */
	@Override
	public Map<String, List<Pcddpf>> getPcddpfRecords(String coy, List<String> chdrnumList) {
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT UNIQUE_NUMBER,CHDRCOY,CHDRNUM,AGNTNUM,SPLITC,SPLITB,VALIDFLAG,TRANNO,CURRFROM,CURRTO,");
		sb.append("USER_T,TERMID,TRTM,TRDT FROM PCDDPF WHERE CHDRCOY=? AND ");
		sb.append(getSqlInStr(CHDRNUM, chdrnumList));
		sb.append(" AND VALIDFLAG = '1' ");
		sb.append(" ORDER BY CHDRCOY ASC, CHDRNUM ASC,AGNTNUM ASC,UNIQUE_NUMBER DESC ");
		PreparedStatement ps = getPrepareStatement(sb.toString());
		ResultSet rs = null;
		Pcddpf pcddpf = null;
		Map<String, List<Pcddpf>> pcddpfMap = new HashMap<String, List<Pcddpf>>();
		try {
			ps.setInt(1, Integer.parseInt(coy));
			rs = executeQuery(ps);

			while (rs.next()) {
				pcddpf = new Pcddpf();
				pcddpf.setUniqueNumber(rs.getLong(UNIQUE_NUMBER));
				pcddpf.setChdrcoy(rs.getString(CHDRCOY).charAt(0));
				pcddpf.setChdrnum(rs.getString(CHDRNUM));
				pcddpf.setAgntNum(rs.getString("AGNTNUM"));
				pcddpf.setSplitC(rs.getBigDecimal("SPLITC"));
				pcddpf.setSplitB(rs.getBigDecimal("SPLITB"));
				pcddpf.setValidFlag(rs.getString(VALIDFLAG).charAt(0));
				pcddpf.setTranNo(rs.getInt(TRANNO));
				pcddpf.setCurrFrom(rs.getInt(CURRFROM));
				pcddpf.setCurrTo(rs.getInt(CURRTO));
				pcddpf.setUserT(rs.getInt(USER_T));
				pcddpf.setTermId(rs.getString(TERMID));
				pcddpf.setTrtM(rs.getInt("TRTM"));
				pcddpf.setTrtD(rs.getInt("TRDT"));

				String chdrnum = pcddpf.getChdrnum();
				if (pcddpfMap.containsKey(chdrnum)) {
					pcddpfMap.get(chdrnum).add(pcddpf);
				} else {
					List<Pcddpf> pcddpfSearchResult = new LinkedList<Pcddpf>();
					pcddpfSearchResult.add(pcddpf);
					pcddpfMap.put(chdrnum, pcddpfSearchResult);
				}
			}

		} catch (SQLException e) {
			LOGGER.error("searchPcddpf()", e); /* IJTI-1479 */
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return pcddpfMap;
	}
}
