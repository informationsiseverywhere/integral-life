/*
 * File: Dryautinc.java
 * Date: December 3, 2013 2:23:54 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYAUTINC.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import java.util.Iterator;
import java.util.List;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the transaction detail record change subroutine for
* auto increase transactions.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryautinc extends Maind {//Modify for ILPI-65

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYAUTINC");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaLeadDays = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaAdjustedDate = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaCpiDate = new PackedDecimalData(8, 0).init(0);

	private FixedLengthStringData wsaaIndexationReq = new FixedLengthStringData(1).init("N");
	private Validator indexationRequired = new Validator(wsaaIndexationReq, "Y");
	private Validator indexationNotReq = new Validator(wsaaIndexationReq, "N");

	private FixedLengthStringData wsaaT5655Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5655Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 0);
	private FixedLengthStringData wsaaT5655Suffix = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 4).init("****");

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();
	private static final int wsaaT5679Max = 12;

	private FixedLengthStringData wsaaCovrStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaCovrStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaCovrStatus, "N");
		/* ERRORS */
	private static final String h038 = "H038";
	private static final String f321 = "F321";
		/* FORMATS */
		/* TABLES */
	private static final String t5655 = "T5655";
	private static final String t5679 = "T5679";
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Freqcpy freqcpy = new Freqcpy();
	private T5655rec t5655rec = new T5655rec();
	private T5679rec t5679rec = new T5679rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
    //ILIFE-4884
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private Itempf itempf = null;
	private Itempf itdmpf = null;
	private Covrpf covrpf = null;
	private Chdrpf chdrpf = null;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr380, 
		exit390
	}

	public Dryautinc() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		/*MAIN*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		/* Read table T5679 to determine if coverages have a valid status.*/
		readT5679100();
		/* Read through the Coverages to determine if and when an Increase*/
		/* is due for this entity.*/
		determineIncrease200();
		if (indexationRequired.isTrue()){
			getAdjustedDate500();
			drypDryprcRecInner.drypNxtprcdate.set(wsaaAdjustedDate);
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		}
		else{
			drypDryprcRecInner.dtrdNo.setTrue();
		}
		/*EXIT*/
		exitProgram();
	}

protected void readT5679100()
	{

		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
        itempf = new Itempf();
        itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
        itempf.setItempfx(smtpfxcpy.item.toString());
        itempf.setItemtabl(t5679);
        itempf.setItemitem(wsaaT5679Key.toString());
	    itempf.setItemseq("  ");
        itempf = itemDAO.getItempfRecordBySeq(itempf);
		
		if (itempf == null) {
			drylogrec.params.set(t5679.concat(wsaaT5679Key.toString()));
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	}

protected void determineIncrease200()
	{
		
		/* Loop through all of the valid flag '1' coverages for this*/
		/* entity and determine the earliest date that an increase is*/
		/* required for.  If the INDEXATION-IND has been set to 'P' then*/
		/* that date is pending the actual transaction and should not be*/
		/* scheduled again.*/
		indexationNotReq.setTrue();
		wsaaCpiDate.set(varcom.vrcmMaxDate);
		covrpf = new Covrpf();

        List<Covrpf> list = covrpfDAO.getCovrincrRecord(drypDryprcRecInner.drypCompany.toString(),drypDryprcRecInner.drypEntity.toString(),"00");
        if(list == null){
        	goTo(GotoLabel.exit390);
        } else {//IJTI-320 START
        	Iterator<Covrpf> covrpfIter = list.iterator();
            while(covrpfIter.hasNext()){
            	covrpf = covrpfIter.next();
            	checkCovrStatus400();
            	 if (invalidStatus.isTrue()) {
         			continue;
         		}
            	 if (isEQ(covrpf.getCpiDate(), ZERO)||isEQ(covrpf.getCpiDate(), wsaaCpiDate)|| isGT(covrpf.getCpiDate(), wsaaCpiDate)) {
            		 continue;
            	}
         		wsaaCpiDate.set(covrpf.getCpiDate());
         		indexationRequired.setTrue();
         	
            }
        }
        //IJTI-320 END
       
	}


protected void checkCovrStatus400()
	{
		/*CHECK*/
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(covrpf.getStatcode(), t5679rec.covRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(covrpf.getPstatcode(), t5679rec.covPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}

protected void getAdjustedDate500()
	{
		/*GET*/
		wsaaLeadDays.set(0);
		wsaaAdjustedDate.set(0);
		readChdrlif600();
		readT5655700();
		adjustDate800();
		/*EXIT*/
	}

protected void readChdrlif600()
	{
		chdrpf = new Chdrpf();
		chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(drypDryprcRecInner.drypCompany.toString(), drypDryprcRecInner.drypEntity.toString());
		
		if (chdrpf == null) {
			drylogrec.params.set(drypDryprcRecInner.drypEntity);
			drylogrec.statuz.set("MRNF");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
	}

protected void readT5655700()
	{
		wsaaT5655Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		wsaaT5655Suffix.set("****");

		List<Itempf> templist = itemDAO.getItdmByFrmdate(drypDryprcRecInner.drypCompany.toString(), t5655, wsaaT5655Key.toString(), chdrpf.getBillcd());
		
		if (templist == null || templist.size() == 0) {
			drylogrec.params.set(t5655.concat(wsaaT5655Key.toString()).concat(chdrpf.getBillcd().toString()));
			drylogrec.statuz.set(h038);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		} else {//IJTI-320 START
			itdmpf = templist.get(0);
			
			t5655rec.t5655Rec.set(StringUtil.rawToString(itdmpf.getGenarea()));	
		}
		//IJTI-320 END
	}

protected void adjustDate800()
	{
		compute(wsaaLeadDays, 0).set(mult(t5655rec.leadDays, -1));
		datcon2rec.freqFactor.set(wsaaLeadDays);
		datcon2rec.frequency.set(freqcpy.daily);
		datcon2rec.intDate1.set(wsaaCpiDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61
		}
		wsaaAdjustedDate.set(datcon2rec.intDate2);
	}
//Modify for ILPI-61
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);

	//ILPI-97 ends
}
//Modify for ILPI-65
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
