/*
 * File: Dry5359.java
 * Date: March 26, 2014 3:02:13 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY5359.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.clients.dataaccess.ClrfTableDAM;
import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;
import com.csc.fsu.general.dataaccess.Dddelf2TableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3620rec;
import com.csc.fsu.general.tablestructures.T3695rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.agents.dataaccess.ZctnTableDAM;
import com.csc.life.agents.dataaccess.ZptnTableDAM;
import com.csc.life.agents.recordstructures.Comlinkrec;
import com.csc.life.agents.tablestructures.T5644rec;
import com.csc.life.agents.tablestructures.Zorlnkrec;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.TaxdTableDAM;
import com.csc.life.contractservicing.procedures.Zorcompy;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.diary.dataaccess.dao.Dry5359DAO;
import com.csc.life.diary.dataaccess.model.Dry5359Dto;
import com.csc.life.flexiblepremium.dataaccess.AgcmseqTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FpcoTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.flexiblepremium.dataaccess.FrepTableDAM;
import com.csc.life.flexiblepremium.procedures.Overaloc;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.CovrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.procedures.Lifrtrn;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.recordstructures.Lifrtrnrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.productdefinition.tablestructures.Th605rec;
import com.csc.life.regularprocessing.dataaccess.LinsrnlTableDAM;
import com.csc.life.regularprocessing.recordstructures.Rnlallrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.fsuframework.core.FeaConfg;

/**
 * <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This program processes premium received for Flexible Premium
* Contracts. All premium in suspense will be allocated to the
* contract unless there are billed direct debit premiums where
* due date has not been reached or the amount exceeds the maximum
* allowed or is below the minimum.
*
* Control totals used in this program:
*
*    01  -  No. of FPRM with no remaining LP S
*    02  -  Tot. amount of LP S allocated
*    03  -  No. of outstanding DDDE records
*    04  -  Tot. amount outstanding on DDDE records
*    05  -  No. of records under minimum LP S
*    06  -  No. of LINS records created
*    07  -  No. of PTRN records created
*    08  -  No. of DRPT Varoiance records created
*
*
****************************************************************** ****
 * </pre>
 */
public class Dry5359 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5359");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	/* ERRORS */
	private static final String H791 = "H791";
	private static final String H979 = "H979";
	private static final String E308 = "E308";
	private static final String I086 = "I086";
	private static final String E103 = "E103";
	
	private static final String ITEMCOY = "ITEMCOY";  
	private static final String ITEMTABL = "ITEMTABL";
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	/* Calculated amounts */
	private ZonedDecimalData wsaaSuspAvail = new ZonedDecimalData(18, 2);
	private FixedLengthStringData wsaaSuspRed = new FixedLengthStringData(18).isAPartOf(wsaaSuspAvail, 0, REDEFINE);
	private ZonedDecimalData wsaaBascpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBascpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBascpyPay = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSrvcpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaSrvcpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRnwcpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaRnwcpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyDue = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyErn = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaOvrdBascpyPay = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaT5645Offset = new ZonedDecimalData(2, 0).init(1).setUnsigned();
	private PackedDecimalData wsaaDifference = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaMaxAllowed = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaMinAllowed = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaTotAllocated = new ZonedDecimalData(18, 2).init(0);
	private FixedLengthStringData wsaaTotRed = new FixedLengthStringData(18).isAPartOf(wsaaTotAllocated, 0, REDEFINE);
	private PackedDecimalData wsaaPolamnt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTotamt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaTgtDiff = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaUpToTgt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOvrTgt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaOvrTarget = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaPolAmt = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaBilled = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaAfterPremPaid = new PackedDecimalData(17, 2).init(0);
	private ZonedDecimalData wsaaCommType = new ZonedDecimalData(1, 0).setUnsigned();
	private PackedDecimalData wsaaProportion = new PackedDecimalData(10, 7);
	/* WSAA-SAVED-AMOUNTS */
	private PackedDecimalData[] wsaaPayamnt = PDInittedArray(3, 13, 2);
	private PackedDecimalData[] wsaaErndamt = PDInittedArray(3, 13, 2);
	private PackedDecimalData wsaaGlSub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5645Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaT5729Sub = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaTest = new PackedDecimalData(5, 0);
	private ZonedDecimalData wsaaNumPeriod = new ZonedDecimalData(11, 5).init(0);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	/* WSAA-DATES */
	private ZonedDecimalData wsaaTerm = new ZonedDecimalData(11, 5);

	private FixedLengthStringData filler3 = new FixedLengthStringData(11).isAPartOf(wsaaTerm, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaTermLeftInteger = new ZonedDecimalData(6, 0).isAPartOf(filler3, 0).setUnsigned();
	private ZonedDecimalData wsaaTermLeftRemain = new ZonedDecimalData(5, 0).isAPartOf(filler3, 6).setUnsigned();
	private FixedLengthStringData wsaaPayrnum = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaForenum = new FixedLengthStringData(30);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaForenum, 0);
	private FixedLengthStringData wsaaPayrseqno = new FixedLengthStringData(1).isAPartOf(wsaaForenum, 8);
	/* WSAA-BILLFREQ-VAR */
	private FixedLengthStringData wsaaBillfreq = new FixedLengthStringData(2);
	/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	private FixedLengthStringData wsaaRldgagnt = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaAgntChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgagnt, 0);
	private FixedLengthStringData wsaaAgntLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 8);
	private FixedLengthStringData wsaaAgntCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 10);
	private FixedLengthStringData wsaaAgntRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 12);
	private FixedLengthStringData wsaaAgntPlanSuffix = new FixedLengthStringData(2).isAPartOf(wsaaRldgagnt, 14);

	/* Status indicators */
	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaValidCovr = new FixedLengthStringData(1).init("N");
	private Validator validCovr = new Validator(wsaaValidCovr, "Y");

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");
	private Validator dateNotFound = new Validator(wsaaDateFound, "N");

	private FixedLengthStringData wsaaFreqFound = new FixedLengthStringData(1).init("N");
	private Validator freqFound = new Validator(wsaaFreqFound, "Y");

	private FixedLengthStringData wsaaDurationFound = new FixedLengthStringData(1).init("N");
	private Validator durationFound = new Validator(wsaaDurationFound, "Y");
	private Validator durationNotFound = new Validator(wsaaDurationFound, "N");

	private FixedLengthStringData wsaaMaxFail = new FixedLengthStringData(1).init("N");
	private Validator failMaximumTest = new Validator(wsaaMaxFail, "Y");

	private FixedLengthStringData wsaaMinFail = new FixedLengthStringData(1).init("N");
	private Validator failMinimumTest = new Validator(wsaaMinFail, "Y");

	private FixedLengthStringData wsaaOutstandingDdde = new FixedLengthStringData(1).init("N");
	private Validator outstandingDdde = new Validator(wsaaOutstandingDdde, "Y");
	private Validator noOutstandingDdde = new Validator(wsaaOutstandingDdde, "N");
	// ILIFE-2628 fixed--Array size increased
	private static final int WSAA_T5644_SIZE = 1000;

	/*
	 * WSAA-T5644-ARRAY 03 WSAA-T5644-REC OCCURS 30
	 */
	private FixedLengthStringData[] wsaaT5644Rec = FLSInittedArray(1000, 11);
	private FixedLengthStringData[] wsaaT5644Key = FLSDArrayPartOfArrayStructure(4, wsaaT5644Rec, 0);
	private FixedLengthStringData[] wsaaT5644Commth = FLSDArrayPartOfArrayStructure(4, wsaaT5644Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5644Data = FLSDArrayPartOfArrayStructure(7, wsaaT5644Rec, 4);
	private FixedLengthStringData[] wsaaT5644Comsub = FLSDArrayPartOfArrayStructure(7, wsaaT5644Data, 0);
	private static final int wsaaT5645Size = 1000;

	/* WSAA-T5645-ARRAY */
	private FixedLengthStringData[] wsaaT5645Data = FLSInittedArray(1000, 21);// ILIFE-1985
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Data, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Data, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Data, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Data, 20);

	private FixedLengthStringData wsaaDurations = new FixedLengthStringData(16);
	private ZonedDecimalData[] wsaaDuration = ZDArrayPartOfStructure(4, 4, 0, wsaaDurations, 0);

	private FixedLengthStringData wsaaTargetMaxs = new FixedLengthStringData(20);
	private ZonedDecimalData[] wsaaTargetMax = ZDArrayPartOfStructure(4, 5, 0, wsaaTargetMaxs, 0);

	private FixedLengthStringData wsaaTargetMins = new FixedLengthStringData(12);
	private ZonedDecimalData[] wsaaTargetMin = ZDArrayPartOfStructure(4, 3, 0, wsaaTargetMins, 0);
	
	private static final int WSAA_T5729_SIZE = 1000;
	// ILIFE-2628 fixed--Array size increased
	private static final int WSAA_T3620_SIZE = 1000;

	/* WSAA-T3620-ARRAY */
	private FixedLengthStringData[] wsaaT3620Rec = FLSInittedArray(1000, 2);// ILIFE-1985
	private FixedLengthStringData[] wsaaT3620Key = FLSDArrayPartOfArrayStructure(1, wsaaT3620Rec, 0);
	private FixedLengthStringData[] wsaaT3620Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT3620Key, 0);
	private FixedLengthStringData[] wsaaT3620Data = FLSDArrayPartOfArrayStructure(1, wsaaT3620Rec, 1);
	private FixedLengthStringData[] wsaaT3620Ddind = FLSDArrayPartOfArrayStructure(1, wsaaT3620Data, 0);
	/* Storage for T5671 table items. */
	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(4);
	private static final int WSAA_T561_SIZE = 3000;// ILIFE-1985

	private FixedLengthStringData[] wsaaT5671Rec = FLSInittedArray(3000, 48);// ILIFE-1985
	private FixedLengthStringData[] wsaaT5671Key = FLSDArrayPartOfArrayStructure(8, wsaaT5671Rec, 0);
	private FixedLengthStringData[] wsaaT5671Trcde = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5671Key, 4, HIVALUES);
	private FixedLengthStringData[] wsaaT5671Data = FLSDArrayPartOfArrayStructure(40, wsaaT5671Rec, 8);
	private FixedLengthStringData[] wsaaT5671Subprogs = FLSDArrayPartOfArrayStructure(40, wsaaT5671Data, 0);
	private FixedLengthStringData[][] wsaaT5671Subprog = FLSDArrayPartOfArrayStructure(4, 10, wsaaT5671Subprogs, 0);

	private FixedLengthStringData wsaaTrcdeCrtable = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaAuthCode = new FixedLengthStringData(4).isAPartOf(wsaaTrcdeCrtable, 0);
	private FixedLengthStringData wsaaCovrlnbCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTrcdeCrtable, 4);
	/* Storage for T5688 table items. */
	// ILIFE-2628 fixed--Array size increased
	private static final int WSAA_T5688_SIZE = 1000;

	/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = FLSInittedArray(1000, 10);
	private FixedLengthStringData[] wsaaT5688Key = FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5688Data = FLSDArrayPartOfArrayStructure(7, wsaaT5688Rec, 3);
	private PackedDecimalData[] wsaaT5688Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5688Data, 0);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 5);
	private FixedLengthStringData[] wsaaT5688Revacc = FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 6);
	/* Storage for T6654 table items. */
	// ILIFE-2628 fixed--Array size increased
	private static final int wsaaT6654Size = 1000;

	/*
	 * WSAA-T6654-ARRAY 03 WSAA-T6654-REC OCCURS 80
	 */
	private FixedLengthStringData[] wsaaT6654Rec = FLSInittedArray(1000, 12);
	private FixedLengthStringData[] wsaaT6654Key = FLSDArrayPartOfArrayStructure(4, wsaaT6654Rec, 0);
	private FixedLengthStringData[] wsaaT6654Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT6654Key, 0, HIVALUES);
	private FixedLengthStringData[] wsaaT6654Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6654Key, 1, HIVALUES);
	private FixedLengthStringData[] wsaaT6654Data = FLSDArrayPartOfArrayStructure(8, wsaaT6654Rec, 4);
	private FixedLengthStringData[] wsaaT6654Collsub = FLSDArrayPartOfArrayStructure(8, wsaaT6654Data, 0);

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(97);
	private FixedLengthStringData wsysFprmkey = new FixedLengthStringData(11).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(2).isAPartOf(wsysFprmkey, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysFprmkey, 2);
	private FixedLengthStringData wsysPayrseqno = new FixedLengthStringData(1).isAPartOf(wsysFprmkey, 10);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(86).isAPartOf(wsysSystemErrorParams, 11);
	private FixedLengthStringData wsaaBillfq = new FixedLengthStringData(2);
	private static final int wsaaAgcmIxSize = 100;
	private ZonedDecimalData wsaaAgcmIx = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIa = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private static final int wsaaAgcmIySize = 500;
	private ZonedDecimalData wsaaAgcmIy = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaAgcmIb = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaAgcmPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgcmAlcprem = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaAgcmPremLeft = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaReceivedPremium = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOutstandingPremium = new PackedDecimalData(17, 2);

	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private FixedLengthStringData wsaaAgcmFound = new FixedLengthStringData(1);
	private Validator agcmFound = new Validator(wsaaAgcmFound, "Y");

	private FixedLengthStringData wsaaPremType = new FixedLengthStringData(1);
	private Validator firstPrem = new Validator(wsaaPremType, "I");
	private Validator renPrem = new Validator(wsaaPremType, "R");

	private FixedLengthStringData wsaaTr52eKey = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr52eTxcode = new FixedLengthStringData(1).isAPartOf(wsaaTr52eKey, 0);
	private FixedLengthStringData wsaaTr52eCnttype = new FixedLengthStringData(3).isAPartOf(wsaaTr52eKey, 1);
	private FixedLengthStringData wsaaTr52eCrtable = new FixedLengthStringData(4).isAPartOf(wsaaTr52eKey, 4);

	private FixedLengthStringData wsaaRateItem = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCntCurr = new FixedLengthStringData(3).isAPartOf(wsaaRateItem, 0);
	private FixedLengthStringData wsaaTxitem = new FixedLengthStringData(4).isAPartOf(wsaaRateItem, 3);

	private FixedLengthStringData wsaaEndPost = new FixedLengthStringData(1).init("N");
	private Validator endPost = new Validator(wsaaEndPost, "Y");
	private static final String ZPTNREC = "ZPTNREC";
	private static final String ZCTNREC = "ZCTNREC";
	private static final String WSAA_SUBROUTINE_A = "LBONCALA";
	private static final String WSAA_SUBROUTINE_B = "LBONCALB";
	private IntegerData wsaaT5644Ix = new IntegerData();
	private IntegerData wsaaT5729Ix = new IntegerData();
	private IntegerData wsaaT3620Ix = new IntegerData();
	private IntegerData wsaaT5671Ix = new IntegerData();
	private IntegerData wsaaSubprogIx = new IntegerData();
	private IntegerData wsaaT5688Ix = new IntegerData();
	private IntegerData wsaaT6654Ix = new IntegerData();
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private AcblTableDAM acblIO = new AcblTableDAM();
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private AgcmseqTableDAM agcmseqIO = new AgcmseqTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ClrfTableDAM clrfIO = new ClrfTableDAM();
	private CovrlnbTableDAM covrlnbIO = new CovrlnbTableDAM();
	private Dddelf2TableDAM dddelf2IO = new Dddelf2TableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private FpcoTableDAM fpcoIO = new FpcoTableDAM();
	private FprmTableDAM fprmIO = new FprmTableDAM();
	private FrepTableDAM frepIO = new FrepTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LinsrnlTableDAM linsrnlIO = new LinsrnlTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private TaxdTableDAM taxdIO = new TaxdTableDAM();
	private ZctnTableDAM zctnIO = new ZctnTableDAM();
	private ZptnTableDAM zptnIO = new ZptnTableDAM();
	private Comlinkrec comlinkrec = new Comlinkrec();
	private Rnlallrec rnlallrec = new Rnlallrec();
	private Lifrtrnrec lifrtrnrec = new Lifrtrnrec();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Zorlnkrec zorlnkrec = new Zorlnkrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private T3695rec t3695rec = new T3695rec();
	private T5644rec t5644rec = new T5644rec();
	private T5645rec t5645rec = new T5645rec();
	private T5671rec t5671rec = new T5671rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6654rec t6654rec = new T6654rec();
	private T5729rec t5729rec = new T5729rec();
	private T3620rec t3620rec = new T3620rec();
	private Th605rec th605rec = new Th605rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private ControlTotalsInner controlTotalsInner = new ControlTotalsInner();
	private FormatsInner formatsInner = new FormatsInner();
	private TablesInner tablesInner = new TablesInner();
	private WsaaAgcmSummaryInner wsaaAgcmSummaryInner = new WsaaAgcmSummaryInner();
	private WsaaT5729ArrayInner wsaaT5729ArrayInner = new WsaaT5729ArrayInner();
	private WsaaVariablesInner wsaaVariablesInner = new WsaaVariablesInner();

	private FixedLengthStringData wsaaFrepCreated = new FixedLengthStringData(1).init("N");
	private Validator frepCreated = new Validator(wsaaFrepCreated, "Y");
	private Datcon1rec datcon1rec = new Datcon1rec();
	private static final String T1693 = "T1693";
	private FixedLengthStringData wsaaFirst = new FixedLengthStringData(1);
	private T5540rec t5540rec = new T5540rec();
	private FixedLengthStringData wsaaLoyaltyApp = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaOverdueDays = new ZonedDecimalData(4, 0).init(0);
	private FixedLengthStringData wsaaCrtable = new FixedLengthStringData(4);
	private WsaaT6654ArrayInner wsaaT6654ArrayInner = new WsaaT6654ArrayInner();
	private Map<String, List<Itempf>> t6654ListMap;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private IntegerData wsaaT6654SubrIx = new IntegerData();
	private static final String T6654 = "T6654";
	private IntegerData wsaaT6654ExpyIx = new IntegerData();
	// Ticket#1296 ends by vhukumagrawa

	/* Calculated amounts */
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private Dry5359DAO dry5359DAO = getApplicationContext().getBean("dry5359DAO", Dry5359DAO.class);
	
	private ZonedDecimalData wsaaIx = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTomorrow = new PackedDecimalData(8, 0);
	private Datcon2rec datcon2rec = new Datcon2rec();
	private boolean isCollectionHappened = false;
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, POST3632, EXIT3639, B220CALL, B270NEXT, B290EXIT, B320LOCATE, B330LOOP, B390EXIT
	}

	public Dry5359() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			// ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void startProcessing() {
		initialise1000();
		List<Dry5359Dto> dry5359List = readChunkRecords(); 
		Iterator<Dry5359Dto> iterator = dry5359List.iterator();
		while (iterator.hasNext()) {
			Dry5359Dto dto = iterator.next();
			readFile2000(dto);
			edit2500(dto);

			if (isEQ(wsspEdterror, Varcom.oK)) {
				update3000(dto);
			}
		}
		updateDiaryData();
		commit3500();
	}

	protected void initialise1000() {
		initialise1010();
	}

	protected void initialise1010() {
		/* Move the parameter screen data from P6671 into its original */
		/* field map. */
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		t6654ListMap = new HashMap<String, List<Itempf>>();
		wsaaLoyaltyApp.set("N");
		wsaaFirst.set("Y");
		wsspEdterror.set(Varcom.oK);
		/* Get transaction description. */
		descIO.setDescpfx("IT");
		descIO.setDesccoy(drypDryprcRecInner.drypCompany);
		descIO.setDesctabl(tablesInner.t1688);
		descIO.setDescitem(drypDryprcRecInner.drypBatctrcde);
		descIO.setLanguage(drypDryprcRecInner.drypLanguage);
		descIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), Varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
		/* Initialise common LIFA, LIFR, RNLA and PTRN fields */
		ptrnIO.setParams(SPACES);
		ptrnIO.setUser(0);
		lifrtrnrec.lifrtrnRec.set(SPACES);
		lifacmvrec.lifacmvRec.set(SPACES);
		rnlallrec.rnlallRec.set(SPACES);
		rnlallrec.effdate.set(drypDryprcRecInner.drypRunDate);
		rnlallrec.moniesDate.set(drypDryprcRecInner.drypRunDate);
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate);
		ptrnIO.setTransactionDate(drypDryprcRecInner.drypRunDate);
		lifrtrnrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		lifrtrnrec.rldgcoy.set(drypDryprcRecInner.drypBatccoy);
		lifrtrnrec.genlcoy.set(drypDryprcRecInner.drypBatccoy);
		lifacmvrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.drypBatccoy);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.drypBatccoy);
		rnlallrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		rnlallrec.language.set(drypDryprcRecInner.drypLanguage);
		lifrtrnrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifacmvrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		rnlallrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		lifrtrnrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifacmvrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		rnlallrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		lifrtrnrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifacmvrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		rnlallrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		lifrtrnrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifacmvrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		rnlallrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		lifrtrnrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		lifacmvrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		rnlallrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		lifrtrnrec.rcamt.set(0);
		lifacmvrec.rcamt.set(0);
		lifrtrnrec.crate.set(0);
		lifacmvrec.crate.set(0);
		lifrtrnrec.acctamt.set(0);
		lifacmvrec.acctamt.set(0);
		lifrtrnrec.user.set(0);
		lifacmvrec.user.set(0);
		rnlallrec.user.set(0);
		lifrtrnrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifrtrnrec.transactionTime.set(varcom.vrcmTime);
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		lifrtrnrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifrtrnrec.transactionDate.set(drypDryprcRecInner.drypRunDate);
		wsaaVariablesInner.wsaaTotTaxCharged.set(ZERO);
		wsaaVariablesInner.wsaaPremNet.set(ZERO);
		wsaaVariablesInner.wsaaOrigSusp.set(ZERO);
		wsaaVariablesInner.wsaaTaxPercent.set(ZERO);
		wsaaVariablesInner.wsaaDiff.set(ZERO);
		wsaaVariablesInner.wsaaTaxProp.set(ZERO);
		/* Initialise tax array */
		for (wsaaVariablesInner.wsaaIx.set(1); !(isGT(wsaaVariablesInner.wsaaIx, 98)); wsaaVariablesInner.wsaaIx
				.add(1)) {
			int one = 1;
			int two = 2;
			wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][one].set(SPACES);
			wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][two].set(SPACES);
			wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][one].set(SPACES);
			wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][two].set(SPACES);
			wsaaVariablesInner.wsaaTaxRule[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaTaxItem[wsaaVariablesInner.wsaaIx.toInt()].set(SPACES);
			wsaaVariablesInner.wsaaPlanSuffix[wsaaVariablesInner.wsaaIx.toInt()].set(ZERO);
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one].set(ZERO);
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two].set(ZERO);
		}
		/* Load account codes */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFunction(Varcom.begn);
		wsaaT5645Sub.set(1);
		while (!(isEQ(itemIO.getStatuz(), Varcom.endp))) {
			loadT56451100();
		}

		/* Retrieve the suspense account sign */
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t3695);
		int one = 1;
		itemIO.setItemitem(wsaaT5645Sacstype[one]);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t3695rec.t3695Rec.set(itemIO.getGenarea());
		/* Load contract type details. */
		itdmIO.setStatuz(Varcom.oK);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(tablesInner.t5688);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmfrm(0);
		itdmIO.setFunction(Varcom.begn);
		// performance improvement -- Anjali
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch(ITEMTABL);
		wsaaT5688Ix.set(1);
		while (!(isEQ(itdmIO.getStatuz(), Varcom.endp))) {
			loadT56881400();
		}

		/* Load Contract statii. */
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Load the commission methods. */
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemitem(SPACES);
		itemIO.setItemtabl(tablesInner.t5644);
		itemIO.setFunction(Varcom.begn);
		// performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch(ITEMCOY,ITEMTABL);
		wsaaT5644Ix.set(1);
		while (!(isEQ(itemIO.getStatuz(), Varcom.endp))) {
			loadT56441600();
		}

		/* Load the Collection routines */
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.t6654);
		itemIO.setItemitem(SPACES);
		itemIO.setFunction(Varcom.begn);
		// performance improvement -- Anjali
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch(ITEMTABL);
		wsaaT6654Ix.set(1);
		while (!(isEQ(itemIO.getStatuz(), Varcom.endp))) {
			loadT66541700();
		}

		/* Load the coverage switching details. */
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setItemtabl(tablesInner.t5671);
		itemIO.setFunction(Varcom.begn);
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch(ITEMCOY, ITEMTABL);
		itemIO.setStatuz(Varcom.oK);
		wsaaT5671Ix.set(1);
		while (!(isEQ(itemIO.getStatuz(), Varcom.endp))) {
			loadT56711900();
		}

		/* Load the premium variances. */
		itdmIO.setStatuz(Varcom.oK);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(tablesInner.t5729);
		itdmIO.setItemitem(SPACES);
		itdmIO.setItmto(0);
		itdmIO.setItmfrm(0);
		itdmIO.setFunction(Varcom.begn);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch(ITEMTABL);
		wsaaT5729Ix.set(1);
		wsaaT5729Sub.set(1);
		while (!(isEQ(itdmIO.getStatuz(), Varcom.endp))) {
			loadT57291950();
		}

		/* Load the Direct Debit Indicators */
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemitem(SPACES);
		itemIO.setItemtabl(tablesInner.t3620);
		itemIO.setFunction(Varcom.begn);
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch(ITEMCOY, ITEMTABL);
		itemIO.setStatuz(Varcom.oK);
		wsaaT3620Ix.set(1);
		while (!(isEQ(itemIO.getStatuz(), Varcom.endp))) {
			loadT36201800();
		}

		/* Load Company Defaults */
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(tablesInner.th605);
		itemIO.setItemitem(drypDryprcRecInner.drypCompany);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		th605rec.th605Rec.set(itemIO.getGenarea());
	}

	protected void loadT56451100() {
		start1110();
	}

	protected void start1110() {
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(drypDryprcRecInner.drypCompany, itemIO.getItemcoy()) || isNE(itemIO.getItemtabl(), tablesInner.t5645)
				|| isNE(itemIO.getItemitem(), wsaaProg) || isNE(itemIO.getStatuz(), Varcom.oK)) {
			if (isEQ(itemIO.getFunction(), Varcom.begn)) {
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			} else {
				itemIO.setStatuz(Varcom.endp);
				return;
			}
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		wsaaT5645Sub.set(1);
		if (isGT(wsaaT5645Sub, wsaaT5645Size)) {
			drylogrec.statuz.set(H791);
			drylogrec.params.set(tablesInner.t5645);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		while (!(isGT(wsaaT5645Sub, 15))) {
			wsaaT5645Cnttot[wsaaT5645Offset.toInt()].set(t5645rec.cnttot[wsaaT5645Sub.toInt()]);
			wsaaT5645Glmap[wsaaT5645Offset.toInt()].set(t5645rec.glmap[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacscode[wsaaT5645Offset.toInt()].set(t5645rec.sacscode[wsaaT5645Sub.toInt()]);
			wsaaT5645Sacstype[wsaaT5645Offset.toInt()].set(t5645rec.sacstype[wsaaT5645Sub.toInt()]);
			wsaaT5645Sign[wsaaT5645Offset.toInt()].set(t5645rec.sign[wsaaT5645Sub.toInt()]);
			wsaaT5645Sub.add(1);
			wsaaT5645Offset.add(1);
		}

		itemIO.setFunction(Varcom.nextr);
	}

	protected void loadT56881400() {
		start1410();
	}

	protected void start1410() {
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), Varcom.oK) && isNE(itdmIO.getStatuz(), Varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), Varcom.endp) || isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(itdmIO.getItemtabl(), tablesInner.t5688)) {
			if (isEQ(itdmIO.getFunction(), Varcom.begn)) {
				drylogrec.params.set(itdmIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			} else {
				itdmIO.setStatuz(Varcom.endp);
				return;
			}
		}
		if (isGT(wsaaT5688Ix, WSAA_T5688_SIZE)) {
			drylogrec.statuz.set(H791);
			drylogrec.params.set(tablesInner.t5688);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaT5688Itmfrm[wsaaT5688Ix.toInt()].set(itdmIO.getItmfrm());
		wsaaT5688Cnttype[wsaaT5688Ix.toInt()].set(itdmIO.getItemitem());
		wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()].set(t5688rec.comlvlacc);
		wsaaT5688Revacc[wsaaT5688Ix.toInt()].set(t5688rec.revacc);
		itdmIO.setFunction(Varcom.nextr);
		wsaaT5688Ix.add(1);
	}

	protected void loadT56441600() {
		start1610();
	}

	protected void start1610() {
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getItemcoy(), drypDryprcRecInner.drypCompany) || isNE(itemIO.getItemtabl(), tablesInner.t5644)
				|| isNE(itemIO.getStatuz(), Varcom.oK)) {
			itemIO.setStatuz(Varcom.endp);
			return;
		}
		if (isGT(wsaaT5644Ix, WSAA_T5644_SIZE)) {
			drylogrec.statuz.set(H791);
			drylogrec.params.set(tablesInner.t5644);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5644rec.t5644Rec.set(itemIO.getGenarea());
		wsaaT5644Commth[wsaaT5644Ix.toInt()].set(itemIO.getItemitem());
		wsaaT5644Comsub[wsaaT5644Ix.toInt()].set(t5644rec.compysubr);
		itemIO.setFunction(Varcom.nextr);
		wsaaT5644Ix.add(1);
	}

	protected void loadT66541700() {
		start1710();
	}

	protected void start1710() {
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.endp)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.endp) || isNE(itemIO.getItemcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(itemIO.getItemtabl(), tablesInner.t6654)) {
			itemIO.setStatuz(Varcom.endp);
			return;
		}
		if (isGT(wsaaT6654Ix, wsaaT6654Size)) {
			drylogrec.statuz.set(H791);
			drylogrec.params.set(tablesInner.t6654);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		itemIO.setFunction(Varcom.nextr);
		wsaaT6654Ix.add(1);
	}

	protected void loadT36201800() {
		start1810();
	}

	protected void start1810() {
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.endp)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itemIO.getItemcoy(), drypDryprcRecInner.drypCompany) || isNE(itemIO.getItemtabl(), tablesInner.t3620)
				|| isEQ(itemIO.getStatuz(), Varcom.endp)) {
			itemIO.setStatuz(Varcom.endp);
			return;
		}
		t3620rec.t3620Rec.set(itemIO.getGenarea());
		if (isGT(wsaaT3620Ix, WSAA_T3620_SIZE)) {
			drylogrec.statuz.set(H791);
			drylogrec.params.set(tablesInner.t3620);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaT3620Key[wsaaT3620Ix.toInt()].set(itemIO.getItemitem());
		wsaaT3620Ddind[wsaaT3620Ix.toInt()].set(t3620rec.ddind);
		wsaaT3620Ix.add(1);
		itemIO.setFunction(Varcom.nextr);
	}

	protected void loadT56711900() {
		start1910();
	}

	protected void start1910() {
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isGT(wsaaT5671Ix, WSAA_T561_SIZE)) {
			drylogrec.statuz.set(H791);
			drylogrec.params.set(tablesInner.t5671);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaT5671Item.set(itemIO.getItemitem());
		if (isEQ(itemIO.getStatuz(), Varcom.endp) || isNE(itemIO.getItemcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(itemIO.getItemtabl(), tablesInner.t5671)
				|| isNE(wsaaT5671Item, drypDryprcRecInner.drypBatctrcde)) {
			itemIO.setStatuz(Varcom.endp);
			return;
		}
		t5671rec.t5671Rec.set(itemIO.getGenarea());
		wsaaT5671Key[wsaaT5671Ix.toInt()].set(itemIO.getItemitem());
		wsaaT5671Subprogs[wsaaT5671Ix.toInt()].set(t5671rec.subprogs);
		itemIO.setFunction(Varcom.nextr);
		wsaaT5671Ix.add(1);
	}

	protected void loadT57291950() {
		start1951();
	}

	protected void start1951() {
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), Varcom.oK) && isNE(itdmIO.getStatuz(), Varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany) || isNE(itdmIO.getItemtabl(), tablesInner.t5729)
				|| isEQ(itdmIO.getStatuz(), Varcom.endp)) {
			itdmIO.setStatuz(Varcom.endp);
			return;
		}
		t5729rec.t5729Rec.set(itdmIO.getGenarea());
		if (isGT(wsaaT5729Ix, WSAA_T5729_SIZE)) {
			drylogrec.statuz.set(H791);
			drylogrec.params.set(tablesInner.t5729);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaT5729ArrayInner.wsaaT5729Key[wsaaT5729Ix.toInt()].set(itdmIO.getItemitem());
		wsaaT5729ArrayInner.wsaaT5729Itmfrm[wsaaT5729Ix.toInt()].set(itdmIO.getItmfrm());
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub, 6)); wsaaT5729Sub.add(1)) {
			wsaaT5729ArrayInner.wsaaT5729Frqcys[wsaaT5729Ix.toInt()].set(t5729rec.frqcys);
		}
		wsaaT5729Sub.set(1);
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub, 4)); wsaaT5729Sub.add(1)) {
			wsaaT5729ArrayInner.wsaaT5729Durationas[wsaaT5729Ix.toInt()].set(t5729rec.durationas);
			wsaaT5729ArrayInner.wsaaT5729Durationbs[wsaaT5729Ix.toInt()].set(t5729rec.durationbs);
			wsaaT5729ArrayInner.wsaaT5729Durationcs[wsaaT5729Ix.toInt()].set(t5729rec.durationcs);
			wsaaT5729ArrayInner.wsaaT5729Durationds[wsaaT5729Ix.toInt()].set(t5729rec.durationds);
			wsaaT5729ArrayInner.wsaaT5729Durationes[wsaaT5729Ix.toInt()].set(t5729rec.durationes);
			wsaaT5729ArrayInner.wsaaT5729Durationfs[wsaaT5729Ix.toInt()].set(t5729rec.durationfs);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinas[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinas);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinbs[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinbs);
			wsaaT5729ArrayInner.wsaaT5729OverdueMincs[wsaaT5729Ix.toInt()].set(t5729rec.overdueMincs);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinds[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinds);
			wsaaT5729ArrayInner.wsaaT5729OverdueMines[wsaaT5729Ix.toInt()].set(t5729rec.overdueMines);
			wsaaT5729ArrayInner.wsaaT5729OverdueMinfs[wsaaT5729Ix.toInt()].set(t5729rec.overdueMinfs);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxas[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxas);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxbs[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxbs);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxcs[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxcs);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxds[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxds);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxes[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxes);
			wsaaT5729ArrayInner.wsaaT5729TargetMaxfs[wsaaT5729Ix.toInt()].set(t5729rec.targetMaxfs);
			wsaaT5729ArrayInner.wsaaT5729TargetMinas[wsaaT5729Ix.toInt()].set(t5729rec.targetMinas);
			wsaaT5729ArrayInner.wsaaT5729TargetMinbs[wsaaT5729Ix.toInt()].set(t5729rec.targetMinbs);
			wsaaT5729ArrayInner.wsaaT5729TargetMincs[wsaaT5729Ix.toInt()].set(t5729rec.targetMincs);
			wsaaT5729ArrayInner.wsaaT5729TargetMinds[wsaaT5729Ix.toInt()].set(t5729rec.targetMinds);
			wsaaT5729ArrayInner.wsaaT5729TargetMines[wsaaT5729Ix.toInt()].set(t5729rec.targetMines);
			wsaaT5729ArrayInner.wsaaT5729TargetMinfs[wsaaT5729Ix.toInt()].set(t5729rec.targetMinfs);
		}
		wsaaT5729Ix.add(1);
		itdmIO.setFunction(Varcom.nextr);
	}

	protected List<Dry5359Dto> readChunkRecords() {
		List<Dry5359Dto> recordList = dry5359DAO.getFprmDetails("1", drypDryprcRecInner.drypCompany.toString(),
				drypDryprcRecInner.drypRunDate.toInt(), drypDryprcRecInner.drypRunDate.toInt(),
				drypDryprcRecInner.drypEntity.toString());
		return recordList;

	}

	protected void readFile2000(Dry5359Dto dto) {
		/* READ-FILE */
		/* Set up the key for the SYSR- copybook should a system error */
		/* for this instalment occur. */
		wsysChdrcoy.set(dto.getChdrCoy());
		wsysChdrnum.set(dto.getChdrNum());
		wsysPayrseqno.set(dto.getPayrseqno());
		wsaaFrepCreated.set("N");
		drycntrec.contotNumber.set(controlTotalsInner.ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	protected void edit2500(Dry5359Dto dto) {
		edit2510(dto);
	}

	protected void edit2510(Dry5359Dto dto) {
		wsspEdterror.set(Varcom.oK);
		/* Read PAYR record */
		payrIO.setDataKey(SPACES);
		payrIO.setChdrcoy(dto.getChdrCoy());
		payrIO.setChdrnum(dto.getChdrNum());
		payrIO.setPayrseqno(dto.getPayrseqno());
		payrIO.setValidflag("1");
		payrIO.setFunction(Varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Now we should read the contract header record */
		readChdr2100(dto);
		/* Here we must validate the CHDR details. */
		validateContract2200();
		if (!validContract.isTrue()) {
			wsspEdterror.set(SPACES);
			return;
		}
		/* Read table TR52D. <V74L01> */
		b800ReadTr52d();
		int one =1;
		/* Read ACBL record */
		acblIO.setRldgacct(dto.getChdrNum());
		acblIO.setRldgcoy(dto.getChdrCoy());
		acblIO.setOrigcurr(payrIO.getBillcurr());
		acblIO.setSacscode(wsaaT5645Sacscode[one]);
		acblIO.setSacstyp(wsaaT5645Sacstype[one]);
		acblIO.setFunction(Varcom.readr);
		acblIO.setFormat(formatsInner.acblrec);
		SmartFileCode.execute(appVars, acblIO);
		if (isNE(acblIO.getStatuz(), Varcom.oK) && isNE(acblIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.params.set(acblIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Move LP S balance to working storage as a positive balance */
		if (isEQ(acblIO.getStatuz(), Varcom.mrnf)) {
			wsspEdterror.set(SPACES);
			return;
		} else {
			if (isEQ(t3695rec.sign, "-")) {
				compute(wsaaSuspAvail, 2).set(sub(0, acblIO.getSacscurbal()));
			} else {
				wsaaSuspAvail.set(acblIO.getSacscurbal());
			}
		}
		if (isGTE(acblIO.getSacscurbal(), 0)) {
			drycntrec.contotNumber.set(controlTotalsInner.ct02);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			wsspEdterror.set(SPACES);
			return;
		}
		drycntrec.contotNumber.set(controlTotalsInner.ct03);
		drycntrec.contotValue.set(wsaaSuspAvail);
		d000ControlTotals();
		readClrf2400(dto);
		wsaaBilled.set(dto.getTotalBilled());
		/* Search table T3620 for the billing channel indicator for the */
		/* contract to establish whether this is a Direct Debit case or no */
		wsaaT3620Ix.set(1);
		searchlabel1: {
			for (; isLT(wsaaT3620Ix, wsaaT3620Rec.length); wsaaT3620Ix.add(1)) {
				if (isEQ(wsaaT3620Key[wsaaT3620Ix.toInt()], payrIO.getBillchnl())) {
					break searchlabel1;
				}
			}
			drylogrec.statuz.set(I086);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(tablesInner.t3620);
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(drylogrec.params);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Check to see if the DDIND on table T3620 is not spaces */
		/* for the billing channel on the PAYR record */
		wsaaOutstandingDdde.set("N");
		/* If the DDIND is not spaces the DDDE record should be read */
		if (isNE(wsaaT3620Data[wsaaT3620Ix.toInt()], SPACES)) {
			readDdde2450();
		}
		if (isLTE(wsaaSuspAvail, 0)) {
			drycntrec.contotNumber.set(controlTotalsInner.ct05);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			wsspEdterror.set(SPACES);
			return;
		}
		/* Check the premium variances on table T5729 */
		/* We must search table T5729 to find the record which matches */
		/* the product type of the contract. */
		wsaaT5729Ix.set(1);
		searchlabel2: {
			for (; isLT(wsaaT5729Ix, wsaaT5729ArrayInner.wsaaT5729Rec.length); wsaaT5729Ix.add(1)) {
				if (isEQ(wsaaT5729ArrayInner.wsaaT5729Key[wsaaT5729Ix.toInt()], chdrlifIO.getCnttype())) {
					break searchlabel2;
				}
			}
			drylogrec.statuz.set(I086);
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(tablesInner.t5729);
			stringVariable2.addExpression(wsysSystemErrorParams);
			stringVariable2.setStringInto(drylogrec.params);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* We must find the effective T5729 entry for the contract */
		/* (T5729 entries will have been loaded in descending sequence */
		/* into the array). */
		wsaaDateFound.set("N");
		while (!(isNE(chdrlifIO.getCnttype(), wsaaT5729ArrayInner.wsaaT5729Key[wsaaT5729Ix.toInt()])
				|| isGT(wsaaT5729Ix, WSAA_T5729_SIZE) || dateFound.isTrue())) {
			if (isGTE(chdrlifIO.getOccdate(), wsaaT5729ArrayInner.wsaaT5729Itmfrm[wsaaT5729Ix.toInt()])) {
				wsaaDateFound.set("Y");
			} else {
				wsaaT5729Ix.add(1);
			}
		}

		if (dateNotFound.isTrue()) {
			drylogrec.statuz.set(I086);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Load the values from table T5729 which match the frequency */
		/* on the PAYR record */
		wsaaFreqFound.set("N");
		wsaaT5729Sub.set(1);
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub, 6) || freqFound.isTrue()); wsaaT5729Sub.add(1)) {
			if (isEQ(wsaaT5729ArrayInner.wsaaT5729Frqcy[wsaaT5729Ix.toInt()][wsaaT5729Sub.toInt()],
					payrIO.getBillfreq())) {
				wsaaTest.set(wsaaT5729Sub);
				wsaaFreqFound.set("Y");
			}
		}
		if (isGT(wsaaT5729Sub, 6)) {
			drylogrec.statuz.set(I086);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Frequency found *************/
		if (isEQ(wsaaTest, 1)) {
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationas[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxas[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMinas[wsaaT5729Ix.toInt()]);
		} else if (isEQ(wsaaTest, 2)) {
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationbs[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxbs[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMinbs[wsaaT5729Ix.toInt()]);
		} else if (isEQ(wsaaTest, 3)) {
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationcs[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxcs[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMincs[wsaaT5729Ix.toInt()]);
		} else if (isEQ(wsaaTest, 4)) {
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationds[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxds[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMinds[wsaaT5729Ix.toInt()]);
		} else if (isEQ(wsaaTest, 5)) {
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationes[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxes[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMines[wsaaT5729Ix.toInt()]);
		} else if (isEQ(wsaaTest, 6)) {
			wsaaDurations.set(wsaaT5729ArrayInner.wsaaT5729Durationfs[wsaaT5729Ix.toInt()]);
			wsaaTargetMaxs.set(wsaaT5729ArrayInner.wsaaT5729TargetMaxfs[wsaaT5729Ix.toInt()]);
			wsaaTargetMins.set(wsaaT5729ArrayInner.wsaaT5729TargetMinfs[wsaaT5729Ix.toInt()]);
		}
		/* Determine the current variances from T5729 based on the */
		/* difference between the Occdate and the effective date of this */
		/* job. */
		/* Use the DATCON3 subroutine to calculate the freq factor */
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(chdrlifIO.getOccdate());
		datcon3rec.intDate2.set(drypDryprcRecInner.drypRunDate);
		datcon3rec.frequency.set(payrIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaNumPeriod.set(datcon3rec.freqFactor);
		wsaaSub2.set(0);
		durationNotFound.setTrue();
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4) || isEQ(wsaaDuration[wsaaSub.toInt()], SPACES)
				|| durationFound.isTrue()); wsaaSub.add(1)) {
			if (isLT(wsaaNumPeriod, wsaaDuration[wsaaSub.toInt()])) {
				wsaaSub2.set(wsaaSub);
				durationFound.setTrue();
			}
		}
		if (durationNotFound.isTrue()) {
			drylogrec.statuz.set(I086);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Calculate tax */
		wsaaVariablesInner.wsaaTotTaxCharged.set(ZERO);
		wsaaVariablesInner.wsaaPremNet.set(ZERO);
		wsaaVariablesInner.wsaaOrigSusp.set(ZERO);
		wsaaVariablesInner.wsaaTaxPercent.set(ZERO);
		wsaaVariablesInner.wsaaDiff.set(ZERO);
		wsaaVariablesInner.wsaaTaxProp.set(ZERO);
		calcTax2800(dto);
		wsaaVariablesInner.wsaaOrigSusp.set(wsaaSuspAvail);
		wsaaVariablesInner.wsaaPremNet.set(wsaaSuspAvail);
		if (isGT(wsaaVariablesInner.wsaaTotTaxCharged, 0)) {
			compute(wsaaVariablesInner.wsaaTaxPercent, 3)
					.setRounded(add((div(wsaaVariablesInner.wsaaTotTaxCharged, payrIO.getSinstamt01())), 1));
			compute(wsaaVariablesInner.wsaaPremNet, 3)
					.setRounded(div(wsaaSuspAvail, wsaaVariablesInner.wsaaTaxPercent));
			compute(wsaaVariablesInner.wsaaTaxProp, 3).setRounded(sub(wsaaSuspAvail, wsaaVariablesInner.wsaaPremNet));
			compute(wsaaSuspAvail, 3).setRounded(sub(wsaaSuspAvail, wsaaVariablesInner.wsaaTaxProp));
		}
		/* If the total amount billed is greater than the total amount */
		/* received the suspense balance should be reduced by the */
		/* difference between the total billed and the total received */
		/* to allow for payment of outstanding premiums before the */
		/* maximum variances are checked */
		if (isGT(wsaaBilled, dto.getTotalRecd())) {
			compute(wsaaDifference, 2).set(sub(wsaaBilled, dto.getTotalRecd()));
		} else {
			wsaaDifference.set(0);
		}
		wsaaMaxFail.set("N");
		if (isGT(wsaaSuspAvail, wsaaDifference)) {
			compute(wsaaAfterPremPaid, 2).set(sub(wsaaSuspAvail, wsaaDifference));
			maximumTest2600(dto);
		}
		// ILIFE-1296 STARTS
		if (failMaximumTest.isTrue()) {
			printMaxVariance3020();
		}
		// ILIFE-1296 ENDS
		if (isLTE(wsaaSuspAvail, 0)) {
			wsspEdterror.set(SPACES);
			return;
		}
		wsaaMaxFail.set("N");
		minimumTest2700(dto);
		// ILIFE-1296 STARTS
		if (failMinimumTest.isTrue()) {
			printMinVariance3010();
		}
		// ILIFE-1296 ENDS
		if (failMinimumTest.isTrue()) {
			wsspEdterror.set(SPACES); // error here
			return;
		}
	}

	protected void readChdr2100(Dry5359Dto dto) {
		/* START */
		chdrlifIO.setChdrcoy(dto.getChdrCoy());
		chdrlifIO.setChdrnum(dto.getChdrNum());
		chdrlifIO.setFunction(Varcom.readh);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* EXIT */
	}

	protected void validateContract2200() {
		start2210();
	}

	protected void start2210() {
		/* For each contract retrieved we must look up the contract type */
		/* details on T5688 */
		wsaaT5688Ix.set(1);
		searchlabel1: {
			for (; isLT(wsaaT5688Ix, wsaaT5688Rec.length); wsaaT5688Ix.add(1)) {
				if (isEQ(wsaaT5688Key[wsaaT5688Ix.toInt()], chdrlifIO.getCnttype())) {
					break searchlabel1;
				}
			}
			drylogrec.statuz.set(E308);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(tablesInner.t5688);
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(drylogrec.params);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* We must find the effective T5688 entry for the contract */
		/* (T5688 entries will have been loaded in descending sequence */
		/* into the array). */
		while (!(isNE(chdrlifIO.getCnttype(), wsaaT5688Cnttype[wsaaT5688Ix.toInt()]) || isGT(wsaaT5688Ix, WSAA_T5688_SIZE)
				|| dateFound.isTrue())) {
			if (isGTE(chdrlifIO.getOccdate(), wsaaT5688Itmfrm[wsaaT5688Ix.toInt()])) {
				wsaaDateFound.set("Y");
			} else {
				wsaaT5688Ix.add(1);
			}
		}

		if (!dateFound.isTrue()) {
			drylogrec.statuz.set(H979);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Validate the statii of the contract */
		wsaaValidContract.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()], chdrlifIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidContract.set("Y");
					}
				}
			}
		}
	}


	protected void readClrf2400(Dry5359Dto dto) {
		start2410(dto);
	}

	protected void start2410(Dry5359Dto dto) {
		clrfIO.setForepfx(chdrlifIO.getChdrpfx());
		clrfIO.setForecoy(dto.getChdrCoy());
		wsaaChdrnum.set(dto.getChdrNum());
		wsaaPayrseqno.set(dto.getPayrseqno());
		clrfIO.setForenum(wsaaForenum);
		clrfIO.setClrrrole("PY");
		clrfIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, clrfIO);
		if (isNE(clrfIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(clrfIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaPayrnum.set(clrfIO.getClntnum());
	}

	protected void readDdde2450() {
		start2451();
	}

	protected void start2451() {
		/* Set up key to read any DDDE records for the contract where */
		/* the billing date is greater than the effective date */
		dddelf2IO.setChdrcoy(chdrlifIO.getChdrcoy());
		dddelf2IO.setChdrnum(chdrlifIO.getChdrnum());
		dddelf2IO.setPayrnum(wsaaPayrnum);
		dddelf2IO.setBilldate(drypDryprcRecInner.drypRunDate);
		/* Start reading DDDELF2 by using BEGN */
		dddelf2IO.setFunction(Varcom.begn);
		dddelf2IO.setFormat(formatsInner.dddelf2rec);
		SmartFileCode.execute(appVars, dddelf2IO);
		if ((isNE(dddelf2IO.getStatuz(), Varcom.oK)) && (isNE(dddelf2IO.getStatuz(), Varcom.endp))) {
			drylogrec.params.set(dddelf2IO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* If the status is endp or the key has changed we should go */
		/* to exit. If the record matches the key we should compare */
		/* the amount remaining in suspense with the instalment amount. */
		/* If the amount in suspense is greater than the instalment amount */
		/* we subtract the outstanding instalment from suspense and from */
		/* the total billed. */
		while (!(isEQ(dddelf2IO.getStatuz(), Varcom.endp))) {
			if (isEQ(dddelf2IO.getStatuz(), Varcom.endp) || (isNE(dddelf2IO.getChdrcoy(), chdrlifIO.getChdrcoy()))
					|| (isNE(dddelf2IO.getChdrnum(), chdrlifIO.getChdrnum()))
					|| (isNE(dddelf2IO.getPayrnum(), wsaaPayrnum))) {
				dddelf2IO.setStatuz(Varcom.endp);
				return;
			}
			if (isGT(dddelf2IO.getBilldate(), drypDryprcRecInner.drypRunDate)) {
				if (isGTE(wsaaSuspAvail, dddelf2IO.getInstamt06())) {
					compute(wsaaSuspAvail, 2).set(sub(wsaaSuspAvail, dddelf2IO.getInstamt06()));
					compute(wsaaBilled, 2).set(sub(wsaaBilled, dddelf2IO.getInstamt01()));
					outstandingDdde.setTrue();
					drycntrec.contotNumber.set(controlTotalsInner.ct07);
					drycntrec.contotValue.set(1);
					d000ControlTotals();
					drycntrec.contotNumber.set(controlTotalsInner.ct08);
					drycntrec.contotValue.set(dddelf2IO.getInstamt06());
					d000ControlTotals();
				}
			}
			dddelf2IO.setFunction(Varcom.nextr);
			SmartFileCode.execute(appVars, dddelf2IO);
		}

	}

	protected void maximumTest2600(Dry5359Dto dto) {
		start2610(dto);
	}

	protected void start2610(Dry5359Dto dto) {
		/* Check premium to be allocated is not greater than the maximum */
		/* allowed by table T5729 */
		wsaaMaxFail.set("N");
		compute(wsaaMaxAllowed, 3)
				.setRounded(mult(payrIO.getSinstamt06(), (div(wsaaTargetMax[wsaaSub2.toInt()], 100))));
		/* MOVE WSAA-MAX-ALLOWED TO ZRDP-AMOUNT-IN. */
		/* PERFORM A000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO WSAA-MAX-ALLOWED. */
		if (isNE(wsaaMaxAllowed, 0)) {
			zrdecplrec.amountIn.set(wsaaMaxAllowed);
			zrdecplrec.currency.set(payrIO.getCntcurr());
			a000CallRounding();
			wsaaMaxAllowed.set(zrdecplrec.amountOut);
		}
		if (isGT(wsaaAfterPremPaid, wsaaMaxAllowed)) {
			failMaximumTest.setTrue();
			compute(wsaaSuspAvail, 2).set(sub(wsaaSuspAvail, wsaaAfterPremPaid));
			/* PERFORM 11000-WRITE-VARIANCE-REPORT */
			writeFrepRecord12000(dto);
		}
	}

	protected void minimumTest2700(Dry5359Dto dto) {
		/* START */
		wsaaMinFail.set("N");
		compute(wsaaMinAllowed, 3)
				.setRounded(mult(payrIO.getSinstamt06(), (div(wsaaTargetMin[wsaaSub2.toInt()], 100))));
		/* MOVE WSAA-MIN-ALLOWED TO ZRDP-AMOUNT-IN. */
		/* PERFORM A000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO WSAA-MIN-ALLOWED. */
		if (isNE(wsaaMinAllowed, 0)) {
			zrdecplrec.amountIn.set(wsaaMinAllowed);
			zrdecplrec.currency.set(payrIO.getCntcurr());
			a000CallRounding();
			wsaaMinAllowed.set(zrdecplrec.amountOut);
		}
		if (isLT(wsaaSuspAvail.getbigdata(), wsaaMinAllowed.getbigdata())) {
			failMinimumTest.setTrue();
			/* PERFORM 11000-WRITE-VARIANCE-REPORT */
			writeFrepRecord12000(dto);
		}
		/* EXIT */
	}

	protected void calcTax2800(Dry5359Dto dto) {
		start2810(dto);
	}

	protected void start2810(Dry5359Dto dto) {
		wsaaVariablesInner.wsaaIx.set(ZERO);
		if (isEQ(tr52drec.txcode, SPACES)) {
			return;
		}
		/* Read COVRLNB */
		covrlnbIO.setStatuz(Varcom.oK);
		covrlnbIO.setDataKey(SPACES);
		covrlnbIO.setChdrcoy(dto.getChdrCoy());
		covrlnbIO.setChdrnum(dto.getChdrNum());
		covrlnbIO.setPlanSuffix(ZERO);
		covrlnbIO.setFormat(formatsInner.covrlnbrec);
		covrlnbIO.setFunction(Varcom.begn);
		while (!(isEQ(covrlnbIO.getStatuz(), Varcom.endp))) {
			validateCovr2900(dto);
		}

	}

	protected void validateCovr2900(Dry5359Dto dto) {
		start2910(dto);
	}

	protected void start2910(Dry5359Dto dto) {
		wsaaValidCovr.set("N");
		SmartFileCode.execute(appVars, covrlnbIO);
		if ((isNE(covrlnbIO.getStatuz(), Varcom.oK)) && (isNE(covrlnbIO.getStatuz(), Varcom.endp))) {
			drylogrec.statuz.set(covrlnbIO.getStatuz());
			drylogrec.params.set(covrlnbIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if ((isNE(covrlnbIO.getChdrcoy(), dto.getChdrCoy())) || (isNE(covrlnbIO.getChdrnum(), dto.getChdrNum()))
				|| isEQ(covrlnbIO.getStatuz(), Varcom.endp)) {
			covrlnbIO.setStatuz(Varcom.endp);
			return;
		}
		/* Validate conditions for COVR */
		/* CURRTO > BSSC-EFFECTIVE-DATE */
		if (isLTE(covrlnbIO.getCurrto(), drypDryprcRecInner.drypRunDate)) {
			covrlnbIO.setFunction(Varcom.nextr);
			return;
		}
		/* Validflag should be '1' */
		if (isNE(covrlnbIO.getValidflag(), "1")) {
			covrlnbIO.setFunction(Varcom.nextr);
			return;
		}
		/* Statcode and Pstatcode exists in T5679 item */
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrlnbIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrlnbIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidCovr.set("Y");
					}
				}
			}
		}
		if (!validCovr.isTrue()) {
			covrlnbIO.setFunction(Varcom.nextr);
			return;
		}
		/* Installment premium not = 0 */
		if (isEQ(covrlnbIO.getInstprem(), 0)) {
			covrlnbIO.setFunction(Varcom.nextr);
			return;
		}
		/* Read table TR52E. */
		wsaaTr52eKey.set(SPACES);
		wsaaTr52eTxcode.set(tr52drec.txcode);
		wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
		wsaaTr52eCrtable.set(covrlnbIO.getCrtable());
		b900ReadTr52e(dto);
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set(chdrlifIO.getCnttype());
			wsaaTr52eCrtable.set("****");
			b900ReadTr52e(dto);
		}
		if (isEQ(tr52erec.tr52eRec, SPACES)) {
			wsaaTr52eKey.set(SPACES);
			wsaaTr52eTxcode.set(tr52drec.txcode);
			wsaaTr52eCnttype.set("***");
			wsaaTr52eCrtable.set("****");
			b900ReadTr52e(dto);
		}
		if (isNE(tr52erec.taxind01, "Y")) {
			covrlnbIO.setFunction(Varcom.nextr);
			return;
		}
		initialize(txcalcrec.linkRec);
		txcalcrec.function.set("CALC");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.transType.set("PREM");
		txcalcrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		txcalcrec.chdrnum.set(chdrlifIO.getChdrnum());
		txcalcrec.life.set(covrlnbIO.getLife());
		txcalcrec.coverage.set(covrlnbIO.getCoverage());
		txcalcrec.rider.set(covrlnbIO.getRider());
		txcalcrec.planSuffix.set(covrlnbIO.getPlanSuffix());
		txcalcrec.crtable.set(covrlnbIO.getCrtable());
		txcalcrec.taxrule.set(wsaaTr52eKey);
		wsaaRateItem.set(SPACES);
		wsaaCntCurr.set(chdrlifIO.getCntcurr());
		wsaaTxitem.set(tr52erec.txitem);
		txcalcrec.rateItem.set(wsaaRateItem);
		int one = 1;
		int two = 2;
		txcalcrec.taxType[one].set(SPACES);
		txcalcrec.taxType[two].set(SPACES);
		txcalcrec.taxAmt[one].set(ZERO);
		txcalcrec.taxAmt[two].set(ZERO);
		txcalcrec.taxAbsorb[one].set(SPACES);
		txcalcrec.taxAbsorb[two].set(SPACES);
		txcalcrec.effdate.set(drypDryprcRecInner.drypRunDate);
		if (isEQ(tr52erec.zbastyp, "Y")) {
			txcalcrec.amountIn.set(covrlnbIO.getZbinstprem());
		} else {
			txcalcrec.amountIn.set(covrlnbIO.getInstprem());
		}
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			drylogrec.params.set(txcalcrec.linkRec);
			drylogrec.statuz.set(txcalcrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Move variables to working storage array. */
		wsaaVariablesInner.wsaaIx.add(1);
		wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.life);
		wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.coverage);
		wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.rider);
		wsaaVariablesInner.wsaaPlanSuffix[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.planSuffix);
		wsaaVariablesInner.wsaaTaxRule[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.taxrule);
		wsaaVariablesInner.wsaaTaxItem[wsaaVariablesInner.wsaaIx.toInt()].set(txcalcrec.rateItem);
		wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][one].set(txcalcrec.taxType[one]);
		wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][two].set(txcalcrec.taxType[two]);
		wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][one].set(txcalcrec.taxAbsorb[one]);
		wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][two].set(txcalcrec.taxAbsorb[two]);
		if (isNE(txcalcrec.taxAbsorb[one], "Y")) {
			wsaaVariablesInner.wsaaTotTaxCharged.add(txcalcrec.taxAmt[one]);
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one].set(txcalcrec.taxAmt[one]);
		} else {
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one].set(txcalcrec.taxAmt[one]);
		}
		if (isNE(txcalcrec.taxAbsorb[two], "Y")) {
			wsaaVariablesInner.wsaaTotTaxCharged.add(txcalcrec.taxAmt[two]);
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two].set(txcalcrec.taxAmt[two]);
		} else {
			wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two].set(txcalcrec.taxAmt[two]);
		}
		/* Read next component */
		covrlnbIO.setFunction(Varcom.nextr);
	}

	protected void update3000(Dry5359Dto dto) {
		update3010(dto);
	}

	protected void update3010(Dry5359Dto dto) {
		/* Read and hold the FPRM record for updating later */
		fprmIO.setChdrcoy(dto.getChdrCoy());
		fprmIO.setChdrnum(dto.getChdrNum());
		fprmIO.setPayrseqno(dto.getPayrseqno());
		fprmIO.setFormat(formatsInner.fprmrec);
		fprmIO.setFunction(Varcom.readh);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(fprmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		checkOverdueprocess();
		writeLins3100(dto);
		updateChdrFprmPayr3200();
		updateSuspense3300(dto);
		individualLedger3400(dto);
		covrsAcmvsPtrn3500(dto);
		writePtrn3800(dto);
		isCollectionHappened = true;
	}

	// ILIFE-1296 STARTS
	protected void printMinVariance3010() {
		datcon1rec.intDate.set(payrIO.getBtdate());
		datcon1rec.function.set(Varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		dryrDryrptRecInner.r5358Btdate.set(datcon1rec.extDate);
		dryrDryrptRecInner.r5358Linstamt.set(payrIO.getSinstamt06());
		dryrDryrptRecInner.r5358Minprem.set(wsaaMinAllowed);
		dryrDryrptRecInner.r5358Susamt.set(wsaaSuspAvail);
		dryrDryrptRecInner.r5358Chdrnum.set(fprmIO.getChdrnum());
		dryrDryrptRecInner.r5358Payrseqno.set(fprmIO.getPayrseqno());
		e000ReportRecords();
	}

	protected void getCompanyName3110() {
		descIO.setDescpfx("IT");
		descIO.setDesccoy("0");
		descIO.setDesctabl(T1693);
		descIO.setDescitem(drypDryprcRecInner.drypCompany);
		descIO.setLanguage(drypDryprcRecInner.drypLanguage);
		descIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), Varcom.oK)) {
			descIO.setLongdesc(SPACES);
		}
	}

	protected void printMaxVariance3020() {
		datcon1rec.intDate.set(payrIO.getBtdate());
		datcon1rec.function.set(Varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		dryrDryrptRecInner.r5359Btdate.set(datcon1rec.extDate);
		dryrDryrptRecInner.r5359Linstamt.set(payrIO.getSinstamt06());
		dryrDryrptRecInner.r5359Maxprem.set(wsaaMaxAllowed);
		dryrDryrptRecInner.r5359Susamt.set(wsaaAfterPremPaid);
		dryrDryrptRecInner.r5359Chdrnum.set(fprmIO.getChdrnum());
		dryrDryrptRecInner.r5359Payrseqno.set(fprmIO.getPayrseqno());
		e000ReportRecords();
	}

	protected void writeLins3100(Dry5359Dto dto) {
		start3110(dto);
	}

	protected void start3110(Dry5359Dto dto) {
		/* A LINS record is created for the assigned amount with a status */
		/* of paid to complete the audit trail and ensure reversals can */
		/* function correctly */
		linsrnlIO.setInstjctl(SPACES);
		linsrnlIO.setDueflg(SPACES);
		linsrnlIO.setInstamt02(ZERO);
		linsrnlIO.setInstamt03(ZERO);
		linsrnlIO.setInstamt04(ZERO);
		linsrnlIO.setInstamt05(ZERO);
		linsrnlIO.setInstamt06(wsaaSuspAvail);
		linsrnlIO.setInstamt01(wsaaSuspAvail);
		linsrnlIO.setCbillamt(wsaaSuspAvail);
		setPrecision(linsrnlIO.getInstamt06(), 2);
		linsrnlIO.setInstamt06(add(linsrnlIO.getInstamt06(), wsaaVariablesInner.wsaaTaxProp));
		setPrecision(linsrnlIO.getCbillamt(), 2);
		linsrnlIO.setCbillamt(add(linsrnlIO.getCbillamt(), wsaaVariablesInner.wsaaTaxProp));
		linsrnlIO.setChdrnum(dto.getChdrNum());
		linsrnlIO.setChdrcoy(dto.getChdrCoy());
		linsrnlIO.setBillchnl(payrIO.getBillchnl());
		linsrnlIO.setPayrseqno(dto.getPayrseqno());
		linsrnlIO.setBranch(drypDryprcRecInner.drypBatcbrn);
		linsrnlIO.setTranscode(drypDryprcRecInner.drypBatctrcde);
		linsrnlIO.setInstfreq(payrIO.getBillfreq());
		linsrnlIO.setCntcurr(payrIO.getCntcurr());
		linsrnlIO.setBillcurr(payrIO.getBillcurr());
		linsrnlIO.setInstto(payrIO.getBtdate());
		linsrnlIO.setMandref(payrIO.getMandref());
		linsrnlIO.setInstfrom(payrIO.getPtdate());
		linsrnlIO.setBillcd(drypDryprcRecInner.drypRunDate);
		linsrnlIO.setAcctmeth(chdrlifIO.getAcctmeth());
		linsrnlIO.setPayflag("P");
		linsrnlIO.setValidflag("1");
		linsrnlIO.setFunction(Varcom.writr);
		linsrnlIO.setFormat(formatsInner.linsrnlrec);
		SmartFileCode.execute(appVars, linsrnlIO);
		if (isNE(linsrnlIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(linsrnlIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Log number of LINS written */
		drycntrec.contotNumber.set(controlTotalsInner.ct13);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	protected void updateChdrFprmPayr3200() {
		start3210();
	}

	protected void start3210() {
		chdrlifIO.setInstjctl(SPACES);
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		if (noOutstandingDdde.isTrue()) {
			chdrlifIO.setPtdate(chdrlifIO.getBtdate());
		}
		chdrlifIO.setFunction(Varcom.rewrt);
		chdrlifIO.setFormat(formatsInner.chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		setPrecision(fprmIO.getTotalRecd(), 2);
		fprmIO.setTotalRecd(add(fprmIO.getTotalRecd(), wsaaSuspAvail));
		fprmIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(fprmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		payrIO.setTranno(chdrlifIO.getTranno());
		if (noOutstandingDdde.isTrue()) {
			payrIO.setPtdate(payrIO.getBtdate());
		}
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void updateSuspense3300(Dry5359Dto dto) {
		start3310(dto);
	}

	protected void start3310(Dry5359Dto dto) {
		lifrtrnrec.origamt.set(wsaaSuspAvail);
		lifrtrnrec.origamt.add(wsaaVariablesInner.wsaaTaxProp);
		lifrtrnrec.rdocnum.set(dto.getChdrNum());
		lifrtrnrec.tranref.set(dto.getChdrNum());
		lifrtrnrec.jrnseq.set(0);
		int one = 1;
		lifrtrnrec.contot.set(wsaaT5645Cnttot[one]);
		lifrtrnrec.sacscode.set(wsaaT5645Sacscode[one]);
		lifrtrnrec.sacstyp.set(wsaaT5645Sacstype[one]);
		lifrtrnrec.glsign.set(wsaaT5645Sign[one]);
		lifrtrnrec.glcode.set(wsaaT5645Glmap[one]);
		lifrtrnrec.tranno.set(chdrlifIO.getTranno());
		lifrtrnrec.origcurr.set(payrIO.getBillcurr());
		lifrtrnrec.effdate.set(drypDryprcRecInner.drypRunDate);
		lifrtrnrec.substituteCode[one].set(chdrlifIO.getCnttype());
		lifrtrnrec.rldgacct.set(dto.getChdrNum());
		lifrtrnrec.function.set("PSTW");
		callProgram(Lifrtrn.class, lifrtrnrec.lifrtrnRec);
		if (isNE(lifrtrnrec.statuz, Varcom.oK)) {
			wsysSysparams.set(lifrtrnrec.lifrtrnRec);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.statuz.set(lifrtrnrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isEQ(chdrlifIO.getCntcurr(), chdrlifIO.getBillcurr())) {
			return;
		}
	}

	protected void individualLedger3400(Dry5359Dto dto) {
		/* START */
		lifacmvrec.effdate.set(drypDryprcRecInner.drypRunDate);
		lifacmvrec.tranref.set(dto.getChdrNum());
		lifacmvrec.rldgacct.set(dto.getChdrNum());
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.origcurr.set(payrIO.getCntcurr());
		/* Post the Premium-Income. */
		if (isGT(wsaaSuspAvail, 0) && (isNE(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")
				&& isNE(wsaaT5688Revacc[wsaaT5688Ix.toInt()], "Y"))) {
			lifacmvrec.origamt.set(wsaaSuspAvail);
			wsaaGlSub.set(3);
			callLifacmv6000(dto);
		}
		/* EXIT */
	}

	protected void covrsAcmvsPtrn3500(Dry5359Dto dto) {
		start3501();
		next3515(dto);
	}

	protected void start3501() {
		initialize(wsaaTotAllocated);
		initialize(wsaaTotamt);
		initialize(wsaaPolamnt);
		if (isNE(th605rec.bonusInd, "Y")) {
			return;
		}
		/* Initialize the AGCM arrays */
		b100InitializeArrays();
		wsaaBillfq.set(payrIO.getBillfreq());
		wsaaAgcmIx.set(0);
	}

	protected void next3515(Dry5359Dto dto) {
		/* Set up key to read COVR records for contract. */
		covrlnbIO.setStatuz(Varcom.oK);
		covrlnbIO.setDataKey(SPACES);
		covrlnbIO.setPlanSuffix(0);
		covrlnbIO.setChdrnum(dto.getChdrNum());
		covrlnbIO.setChdrcoy(dto.getChdrCoy());
		covrlnbIO.setFormat(formatsInner.covrlnbrec);
		covrlnbIO.setFunction(Varcom.begn);
		// performance improvement -- Anjali
		covrlnbIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		covrlnbIO.setFitKeysSearch("CHDRCOY", "CHDRNUM");
		SmartFileCode.execute(appVars, covrlnbIO);
		if ((isNE(covrlnbIO.getStatuz(), Varcom.oK)) && (isNE(covrlnbIO.getStatuz(), Varcom.endp))) {
			drylogrec.params.set(covrlnbIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if ((isNE(covrlnbIO.getChdrcoy(), dto.getChdrCoy())) || (isNE(covrlnbIO.getChdrnum(), dto.getChdrNum()))
				|| isEQ(covrlnbIO.getStatuz(), Varcom.endp)) {
			drylogrec.params.set(covrlnbIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		// ILIFE-1055 - adding contract fees to amount
		wsaaTotamt.add(payrIO.getSinstamt02());
		while (!(isEQ(covrlnbIO.getStatuz(), Varcom.endp))) {
			/* Validate suspense available against total allocated */
			processCovrs3510(dto);
		}

		if (isNE(wsaaSuspAvail, wsaaTotAllocated)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(wsaaSuspRed);
			stringVariable1.addExpression(" ");
			stringVariable1.addExpression(wsaaTotRed);
			stringVariable1.setStringInto(wsysSystemErrorParams);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(controlTotalsInner.ct06);
		drycntrec.contotValue.set(wsaaTotAllocated);
		d000ControlTotals();
	}

	protected void processCovrs3510(Dry5359Dto dto) {
		boolean isSkipBonusWorkBench = start3511();
		if(isSkipBonusWorkBench) {
			skipBonusWorkbench3515(dto);
		}
		readNextCovr3517(dto);
	}

	protected boolean start3511() {
		/* Check to see if coverage is of a valid status */
		boolean isSkipBonusWorkBench = false;
		wsaaValidCoverage.set("N");
		if (isGT(drypDryprcRecInner.drypRunDate, covrlnbIO.getCurrto()) || isNE(covrlnbIO.getValidflag(), "1")) {
			isSkipBonusWorkBench = false;
			return isSkipBonusWorkBench;
		}
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrlnbIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrlnbIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
		/* If the coverage is not of a valid status read the next covr */
		/* for the contract */
		if (!validCoverage.isTrue() || isEQ(covrlnbIO.getInstprem(), 0)) {
			isSkipBonusWorkBench = false;
			return isSkipBonusWorkBench;
		}
		wsaaTotamt.add(covrlnbIO.getInstprem());
		if (isLT(wsaaTotamt, payrIO.getSinstamt06())) {
			compute(wsaaProportion, 7).set(div(covrlnbIO.getInstprem(), payrIO.getSinstamt06()));
			wsaaProportion.set(1);
			compute(wsaaPolamnt, 8).setRounded(mult(wsaaProportion, wsaaSuspAvail));
			if (isNE(wsaaPolamnt, 0)) {
				zrdecplrec.amountIn.set(wsaaPolamnt);
				zrdecplrec.currency.set(acblIO.getOrigcurr());
				a000CallRounding();
				wsaaPolamnt.set(zrdecplrec.amountOut);
			}
			wsaaTotAllocated.add(wsaaPolamnt);
		} else {
			compute(wsaaPolamnt, 2).set(sub(wsaaSuspAvail, wsaaTotAllocated));
			if (isNE(wsaaPolamnt, 0)) {
				zrdecplrec.amountIn.set(wsaaPolamnt);
				zrdecplrec.currency.set(acblIO.getOrigcurr());
				a000CallRounding();
				wsaaPolamnt.set(zrdecplrec.amountOut);
			}
			wsaaTotAllocated.add(wsaaPolamnt);
		}
		if (isNE(th605rec.bonusInd, "Y")) {
			isSkipBonusWorkBench = true;
			return isSkipBonusWorkBench;
		}
		/* Bonus Workbench * */
		wsaaAgcmIx.add(1);
		wsaaAgcmSummaryInner.wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(covrlnbIO.getChdrcoy());
		wsaaAgcmSummaryInner.wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(covrlnbIO.getChdrnum());
		wsaaAgcmSummaryInner.wsaaAgcmLife[wsaaAgcmIx.toInt()].set(covrlnbIO.getLife());
		wsaaAgcmSummaryInner.wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(covrlnbIO.getCoverage());
		wsaaAgcmSummaryInner.wsaaAgcmRider[wsaaAgcmIx.toInt()].set(covrlnbIO.getRider());
		b200PremiumHistory();
		isSkipBonusWorkBench = true;
		return isSkipBonusWorkBench;
	}

	protected void skipBonusWorkbench3515(Dry5359Dto dto) {
		wsaaVariablesInner.wsaaTotTaxCharged.set(0);
		if (isEQ(tr52drec.txcode, SPACES)) {
			/* NEXT_SENTENCE */
		} else {
			wsaaEndPost.set("N");
			for (wsaaVariablesInner.wsaaIx.set(
					1); !(isGT(wsaaVariablesInner.wsaaIx, 98) || endPost.isTrue()); wsaaVariablesInner.wsaaIx.add(1)) {
				b700PropTax();
			}
			wsaaEndPost.set("N");
			for (wsaaVariablesInner.wsaaIx.set(
					1); !(isGT(wsaaVariablesInner.wsaaIx, 98) || endPost.isTrue()); wsaaVariablesInner.wsaaIx.add(1)) {
				b750PostTax();
			}
		}
		componentAcc3530(dto);
		readFpco3550(dto);
	}

	protected void readNextCovr3517(Dry5359Dto dto) {
		covrlnbIO.setFunction(Varcom.nextr);
		SmartFileCode.execute(appVars, covrlnbIO);
		if ((isNE(covrlnbIO.getStatuz(), Varcom.oK)) && (isNE(covrlnbIO.getStatuz(), Varcom.endp))) {
			drylogrec.params.set(covrlnbIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if ((isNE(covrlnbIO.getChdrcoy(), dto.getChdrCoy())) || (isNE(covrlnbIO.getChdrnum(), dto.getChdrNum()))
				|| isEQ(covrlnbIO.getStatuz(), Varcom.endp)) {
			covrlnbIO.setStatuz(Varcom.endp);
			return;
		}
		/* EXIT */
	}

	protected void componentAcc3530(Dry5359Dto dto) {
		start3531(dto);
	}

	protected void start3531(Dry5359Dto dto) {
		/* Do component level accounting for coverage */
		if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y") && isNE(wsaaT5688Revacc[wsaaT5688Ix.toInt()], "Y")) {
			/* AND COVRLNB-RIDER NOT = ZEROES OR SPACES */
			wsaaGlSub.set(22);
			wsaaRldgChdrnum.set(covrlnbIO.getChdrnum());
			wsaaRldgLife.set(covrlnbIO.getLife());
			wsaaRldgCoverage.set(covrlnbIO.getCoverage());
			wsaaRldgRider.set(covrlnbIO.getRider());
			wsaaPlan.set(covrlnbIO.getPlanSuffix());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			lifacmvrec.origamt.set(wsaaPolamnt);
			// lifacmvrec.origamt.set(covrlnbIO.getInstprem());//ILIFE-4941
			lifacmvrec.jrnseq.set(0);
			lifacmvrec.origcurr.set(covrlnbIO.getPremCurrency());
			lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			lifacmvrec.effdate.set(drypDryprcRecInner.drypRunDate);
			callLifacmv6000(dto);
		}
	}

	protected void readFpco3550(Dry5359Dto dto) {
		start3551(dto);
	}

	protected void start3551(Dry5359Dto dto) {
		/* Read the first FPCO record for the coverage */
		wsaaPolAmt.set(wsaaPolamnt);
		fpcoIO.setChdrcoy(covrlnbIO.getChdrcoy());
		fpcoIO.setChdrnum(covrlnbIO.getChdrnum());
		fpcoIO.setLife(covrlnbIO.getLife());
		fpcoIO.setCoverage(covrlnbIO.getCoverage());
		fpcoIO.setRider(covrlnbIO.getRider());
		fpcoIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		fpcoIO.setTargfrom(0);
		fpcoIO.setFunction(Varcom.begnh);
		fpcoIO.setFormat(formatsInner.fpcorec);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(fpcoIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(fpcoIO.getChdrcoy(), covrlnbIO.getChdrcoy()) || isNE(fpcoIO.getChdrnum(), covrlnbIO.getChdrnum())
				|| isNE(fpcoIO.getLife(), covrlnbIO.getLife()) || isNE(fpcoIO.getCoverage(), covrlnbIO.getCoverage())
				|| isNE(fpcoIO.getRider(), covrlnbIO.getRider())
				|| isNE(fpcoIO.getPlanSuffix(), covrlnbIO.getPlanSuffix())) {
			drylogrec.params.set(covrlnbIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		while (!(isEQ(fpcoIO.getStatuz(), Varcom.endp))) {
			processFpco3570(dto);
		}

	}

	protected void processFpco3570(Dry5359Dto dto) {
		boolean isSkipBonusWorkbench = start3571();
		if(isSkipBonusWorkbench) {
			skipBonusWorkbench3572(dto);
		}
		readNextFpco3575();
	}

	protected boolean start3571() {
		/* Work out the difference between the target premium and the */
		/* premium received in the period so far */
		/* If the current to date on the FPCO record is less than the */
		/* billed to date on the PAYR record, we are looking at the */
		/* record for the previous period. */
		/* If the pro-rata amount for the policy is less than the amount */
		/* left to pay in the period we should add the amount to the prem */
		/* received field on the FPCO and add this amount to the working */
		/* storage total for up to target premium */
		/* If the pro-rata amount is greater or equal to the amount left */
		/* to pay in the period we should 'pay up' the premium received */
		/* field on the FPCO for this period, set the active indicator to */
		/* 'N' if all the target has been billed for and add this <D9604> */
		/* to the up to target working storage <D9604> */
		/* If we are looking at the current FPCO record we should pay */
		/* the whole amount on to the current record */
		/* If the pro-rata amount is less than or equal to the amount */
		/* left to pay, the whole amount should be added into up to */
		/* target working storage */
		/* If the pro-rata amount is greater than the amount left to pay */
		/* we add the amount left to pay into the up to target working */
		/* storage and the remainder into over target working storage */
		wsaaUpToTgt.set(0);
		wsaaOvrTgt.set(0);
		boolean isSkipBonusWorkbench = false;
		if (isLTE(fpcoIO.getTargetPremium(), fpcoIO.getPremRecPer())) {
			wsaaTgtDiff.set(0);
		} else {
			compute(wsaaTgtDiff, 2).set(sub(fpcoIO.getTargetPremium(), fpcoIO.getPremRecPer()));
		}
		rnlallrec.totrecd.set(fpcoIO.getPremRecPer());
		if (isEQ(th605rec.bonusInd, "Y")) {
			wsaaReceivedPremium.set(fpcoIO.getPremRecPer());
		}
		if (isLT(fpcoIO.getTargto(), payrIO.getBtdate())) {
			if (isLT(wsaaPolAmt, wsaaTgtDiff)) {
				setPrecision(fpcoIO.getPremRecPer(), 2);
				fpcoIO.setPremRecPer(add(fpcoIO.getPremRecPer(), wsaaPolAmt));
				wsaaUpToTgt.add(wsaaPolAmt);
			} else {
				setPrecision(fpcoIO.getPremRecPer(), 2);
				fpcoIO.setPremRecPer(add(fpcoIO.getPremRecPer(), wsaaTgtDiff));
				wsaaUpToTgt.add(wsaaTgtDiff);
				if (isGTE(fpcoIO.getBilledInPeriod(), fpcoIO.getTargetPremium())) {
					fpcoIO.setActiveInd("N");
				}
			}
			compute(wsaaPolAmt, 2).set(sub(wsaaPolAmt, wsaaUpToTgt));
		} else {
			setPrecision(fpcoIO.getPremRecPer(), 2);
			fpcoIO.setPremRecPer(add(fpcoIO.getPremRecPer(), wsaaPolAmt));
			if (isLTE(wsaaPolAmt, wsaaTgtDiff)) {
				wsaaUpToTgt.add(wsaaPolAmt);
			} else {
				wsaaUpToTgt.add(wsaaTgtDiff);
				compute(wsaaOvrTgt, 2).set(sub(wsaaPolAmt, wsaaUpToTgt));
			}
			wsaaPolAmt.set(ZERO);
		}
		if (isNE(th605rec.bonusInd, "Y")) {
			isSkipBonusWorkbench = true;
			return isSkipBonusWorkbench;
		}
		/* Bonus Workbench * */
		if (isNE(wsaaUpToTgt, ZERO)) {
			b300WriteArrays();
		}
		/* Write the Over Target Premium */
		if (isNE(wsaaOvrTgt, ZERO)) {
			b500WriteOverTarget();
		}
		isSkipBonusWorkbench = true;
		return isSkipBonusWorkbench;
	}

	protected void skipBonusWorkbench3572(Dry5359Dto dto) {
		/* We need to do the REWRT of the FPCO here for the calculation */
		/* of initial commission */
		fpcoIO.setFormat(formatsInner.fpcorec);
		fpcoIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, fpcoIO);
		if (isNE(fpcoIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(fpcoIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		processCommission3590(dto);
		genericProcessing5000(dto);
	}

	protected void readNextFpco3575() {
		if (isEQ(wsaaPolAmt, 0)) {
			fpcoIO.setStatuz(Varcom.endp);
			return;
		}
		fpcoIO.setFunction(Varcom.nextr);
		SmartFileCode.execute(appVars, fpcoIO);
		if ((isNE(fpcoIO.getStatuz(), Varcom.oK)) && (isNE(fpcoIO.getStatuz(), Varcom.endp))) {
			drylogrec.params.set(fpcoIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(fpcoIO.getChdrcoy(), covrlnbIO.getChdrcoy()) || isNE(fpcoIO.getChdrnum(), covrlnbIO.getChdrnum())
				|| isNE(fpcoIO.getLife(), covrlnbIO.getLife()) || isNE(fpcoIO.getCoverage(), covrlnbIO.getCoverage())
				|| isNE(fpcoIO.getRider(), covrlnbIO.getRider())
				|| isNE(fpcoIO.getPlanSuffix(), covrlnbIO.getPlanSuffix())) {
			fpcoIO.setStatuz(Varcom.endp);
			return;
		}
	}

	protected void processCommission3590(Dry5359Dto dto) {
		start3591(dto);
	}

	protected void start3591(Dry5359Dto dto) {
		agcmbchIO.setChdrcoy(fpcoIO.getChdrcoy());
		agcmbchIO.setChdrnum(fpcoIO.getChdrnum());
		agcmbchIO.setLife(fpcoIO.getLife());
		agcmbchIO.setCoverage(fpcoIO.getCoverage());
		agcmbchIO.setRider(fpcoIO.getRider());
		agcmbchIO.setPlanSuffix(fpcoIO.getPlanSuffix());
		/* MOVE BEGNH TO AGCMBCH-FUNCTION. */
		agcmbchIO.setFunction(Varcom.begn);
		agcmbchIO.setAgntnum(SPACES);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		agcmbchIO.setStatuz(Varcom.oK);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), Varcom.oK) && isNE(agcmbchIO.getStatuz(), Varcom.endp)) {
			drylogrec.params.set(agcmbchIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(agcmbchIO.getChdrcoy(), fpcoIO.getChdrcoy()) || isNE(agcmbchIO.getChdrnum(), fpcoIO.getChdrnum())
				|| isNE(agcmbchIO.getLife(), fpcoIO.getLife()) || isNE(agcmbchIO.getCoverage(), fpcoIO.getCoverage())
				|| isNE(agcmbchIO.getRider(), fpcoIO.getRider())
				|| isNE(agcmbchIO.getPlanSuffix(), fpcoIO.getPlanSuffix())) {
			/* MOVE REWRT TO AGCMBCH-FUNCTION */
			/* CALL 'AGCMBCHIO' USING AGCMBCH-PARAMS */
			/* IF AGCMBCH-STATUZ NOT = O-K */
			/* MOVE AGCMBCH-PARAMS TO SYSR-PARAMS */
			/* PERFORM 600-FATAL-ERROR */
			/* END-IF */
			agcmbchIO.setStatuz(Varcom.endp);
			return;
		}
		while (!(isEQ(agcmbchIO.getStatuz(), Varcom.endp))) {
			processAgcm3610(dto);
		}

	}

	protected void processAgcm3610(Dry5359Dto dto) {
		start3611(dto);
		readNextAgcm3615();
	}

	protected void start3611(Dry5359Dto dto) {
		/* <D9604> */
		/* * <D9604> */
		/* Dont process either dormant or single premium AGCM's * <D9604> */
		/* Also, add over target commission to first SEQNO * <D9604> */
		/* only. * <D9604> */
		/* * <D9604> */
		/* <D9604> */
		if (isEQ(agcmbchIO.getDormantFlag(), "Y") || isEQ(agcmbchIO.getPtdate(), 0)) {
			/* MOVE REWRT TO AGCMBCH-FUNCTION */
			/* MOVE AGCMBCHREC TO AGCMBCH-FORMAT */
			/* CALL 'AGCMBCHIO' USING AGCMBCH-PARAMS */
			/* IF AGCMBCH-STATUZ NOT = O-K */
			/* MOVE AGCMBCH-STATUZ TO SYSR-STATUZ */
			/* MOVE AGCMBCH-PARAMS TO SYSR-PARAMS */
			/* PERFORM 600-FATAL-ERROR */
			/* END-IF */
			return;
		}
		if (isEQ(wsaaUpToTgt, 0) && isGT(agcmbchIO.getSeqno(), 1)) {
			/* MOVE REWRT TO AGCMBCH-FUNCTION <A06966> */
			/* MOVE AGCMBCHREC TO AGCMBCH-FORMAT <A06966> */
			/* CALL 'AGCMBCHIO' USING AGCMBCH-PARAMS <A06966> */
			/* IF AGCMBCH-STATUZ NOT = O-K <A06966> */
			/* MOVE AGCMBCH-STATUZ TO SYSR-STATUZ <A06966> */
			/* MOVE AGCMBCH-PARAMS TO SYSR-PARAMS <A06966> */
			/* PERFORM 600-FATAL-ERROR <A06966> */
			/* END-IF <A06966> */
			return;
		}
		comlinkrec.agent.set(agcmbchIO.getAgntnum());
		comlinkrec.chdrcoy.set(dto.getChdrCoy());
		comlinkrec.chdrnum.set(dto.getChdrNum());
		comlinkrec.language.set(drypDryprcRecInner.drypLanguage);
		comlinkrec.life.set(agcmbchIO.getLife());
		comlinkrec.coverage.set(agcmbchIO.getCoverage());
		comlinkrec.rider.set(agcmbchIO.getRider());
		comlinkrec.planSuffix.set(agcmbchIO.getPlanSuffix());
		comlinkrec.crtable.set(covrlnbIO.getCrtable());
		comlinkrec.jlife.set(covrlnbIO.getJlife());
		comlinkrec.agentClass.set(agcmbchIO.getAgentClass());
		comlinkrec.effdate.set(agcmbchIO.getEfdate());
		comlinkrec.billfreq.set(payrIO.getBillfreq());
		wsaaBillfreq.set(payrIO.getBillfreq());
		comlinkrec.annprem.set(agcmbchIO.getAnnprem());
		comlinkrec.targetPrem.set(fpcoIO.getTargetPremium());
		comlinkrec.currto.set(fpcoIO.getTargto());
		comlinkrec.seqno.set(agcmbchIO.getSeqno());
		/* MOVE 0 TO CLNK-PTDATE. <V65L18> */
		comlinkrec.ptdate.set(chdrlifIO.getPtdate());
		for (wsaaCommType.set(1); !(isEQ(wsaaCommType, 4)); wsaaCommType.add(1)) {
			callSubroutine3630();
		}
		postCommission3650(dto);
		rewriteAgcm3670();
	}

	protected void readNextAgcm3615() {
		agcmbchIO.setFunction(Varcom.nextr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), Varcom.oK) && isNE(agcmbchIO.getStatuz(), Varcom.endp)) {
			drylogrec.params.set(agcmbchIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(agcmbchIO.getChdrcoy(), fpcoIO.getChdrcoy()) || isNE(agcmbchIO.getChdrnum(), fpcoIO.getChdrnum())
				|| isNE(agcmbchIO.getLife(), fpcoIO.getLife()) || isNE(agcmbchIO.getCoverage(), fpcoIO.getCoverage())
				|| isNE(agcmbchIO.getRider(), fpcoIO.getRider())
				|| isNE(agcmbchIO.getPlanSuffix(), fpcoIO.getPlanSuffix())) {
			/* MOVE REWRT TO AGCMBCH-FUNCTION */
			/* CALL 'AGCMBCHIO' USING AGCMBCH-PARAMS */
			/* IF AGCMBCH-STATUZ NOT = O-K */
			/* MOVE AGCMBCH-PARAMS TO SYSR-PARAMS */
			/* PERFORM 600-FATAL-ERROR */
			/* END-IF */
			agcmbchIO.setStatuz(Varcom.endp);
		}
		/* EXIT */
	}

	protected void callSubroutine3630() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start3631();
				case POST3632:
					post3632();
				case EXIT3639:
				default:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start3631() {
		wsaaPayamnt[wsaaCommType.toInt()].set(ZERO);
		wsaaErndamt[wsaaCommType.toInt()].set(ZERO);
		/* Set up linkage for commission subroutine */
		comlinkrec.function.set(SPACES);
		comlinkrec.statuz.set(SPACES);
		comlinkrec.icommtot.set(ZERO);
		comlinkrec.icommpd.set(ZERO);
		comlinkrec.icommernd.set(ZERO);
		comlinkrec.payamnt.set(ZERO);
		comlinkrec.erndamt.set(ZERO);
		comlinkrec.instprem.set(ZERO);
		if (isEQ(wsaaCommType, 1) && isEQ(wsaaUpToTgt, 0)) {
			goTo(GotoLabel.EXIT3639);
		}
		if (isEQ(wsaaCommType, 1)) {
			if (isEQ(agcmbchIO.getBascpy(), SPACES) || (isEQ(agcmbchIO.getCompay(), agcmbchIO.getInitcom())
					&& isEQ(agcmbchIO.getComern(), agcmbchIO.getInitcom()))) {
				goTo(GotoLabel.EXIT3639);
			} else {
				itemIO.setItemitem(agcmbchIO.getBascpy());
			}
		}
		if (isEQ(wsaaCommType, 2)) {
			if (isEQ(agcmbchIO.getRnwcpy(), SPACES) || isEQ(wsaaUpToTgt, ZERO)) {
				goTo(GotoLabel.EXIT3639);
			} else {
				itemIO.setItemitem(agcmbchIO.getRnwcpy());
			}
		}
		if (isEQ(wsaaCommType, 3)) {
			if (isEQ(agcmbchIO.getSrvcpy(), SPACES) || isEQ(wsaaOvrTgt, ZERO) || isGT(agcmbchIO.getSeqno(), 1)) {
				goTo(GotoLabel.EXIT3639);
			} else {
				itemIO.setItemitem(agcmbchIO.getSrvcpy());
			}
		}
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5644Rec);
		as1.setIndices(wsaaT5644Ix);
		as1.addSearchKey(wsaaT5644Key, itemIO.getItemitem(), true);
		if (as1.binarySearch()) {
			/* CONTINUE_STMT */
		} else {
			goTo(GotoLabel.EXIT3639);
		}
		comlinkrec.method.set(itemIO.getItemitem());
		if (isEQ(wsaaCommType, 1)) {
			comlinkrec.icommtot.set(agcmbchIO.getInitcom());
			comlinkrec.icommpd.set(agcmbchIO.getCompay());
			comlinkrec.icommernd.set(agcmbchIO.getComern());
		} else {
			if (isEQ(wsaaCommType, 2)) {
				comlinkrec.instprem.set(wsaaUpToTgt);
			} else {
				comlinkrec.instprem.set(wsaaOvrTgt);
			}
		}
		callProgram(wsaaT5644Comsub[wsaaT5644Ix.toInt()], comlinkrec.clnkallRec);
		if (isNE(comlinkrec.statuz, Varcom.oK)) {
			drylogrec.params.set(comlinkrec.clnkallRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* MOVE CLNK-PAYAMNT TO ZRDP-AMOUNT-IN. */
		/* PERFORM A000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO CLNK-PAYAMNT. */
		if (isNE(comlinkrec.payamnt, 0)) {
			zrdecplrec.amountIn.set(comlinkrec.payamnt);
			zrdecplrec.currency.set(chdrlifIO.getCntcurr());
			a000CallRounding();
			comlinkrec.payamnt.set(zrdecplrec.amountOut);
		}
		/* MOVE CLNK-ERNDAMT TO ZRDP-AMOUNT-IN. */
		/* PERFORM A000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO CLNK-ERNDAMT. */
		if (isNE(comlinkrec.erndamt, 0)) {
			zrdecplrec.amountIn.set(comlinkrec.erndamt);
			zrdecplrec.currency.set(chdrlifIO.getCntcurr());
			a000CallRounding();
			comlinkrec.erndamt.set(zrdecplrec.amountOut);
		}
		wsaaPayamnt[wsaaCommType.toInt()].set(comlinkrec.payamnt);
		wsaaErndamt[wsaaCommType.toInt()].set(comlinkrec.erndamt);
		if (isNE(th605rec.bonusInd, "Y")) {
			goTo(GotoLabel.POST3632);
		}
		/* Bonus Workbench * */
		if (isEQ(wsaaCommType, "1")) {
			firstPrem.setTrue();
		} else if (isEQ(wsaaCommType, "2")) {
			renPrem.setTrue();
		} else {
			goTo(GotoLabel.POST3632);
		}
		/* Skip overriding commission */
		if (isEQ(agcmbchIO.getOvrdcat(), "B") && isNE(comlinkrec.payamnt, ZERO)) {
			zctnIO.setParams(SPACES);
			zctnIO.setAgntcoy(agcmbchIO.getChdrcoy());
			zctnIO.setAgntnum(agcmbchIO.getAgntnum());
			zctnIO.setCommAmt(comlinkrec.payamnt);
			b400LocatePremium();
			zctnIO.setPremium(wsaaAgcmAlcprem);
			setPrecision(zctnIO.getSplitBcomm(), 3);
			zctnIO.setSplitBcomm(div(mult(agcmbchIO.getAnnprem(), 100), wsaaAgcmPremium), true);
			zctnIO.setZprflg(wsaaPremType);
			zctnIO.setChdrcoy(comlinkrec.chdrcoy);
			zctnIO.setChdrnum(comlinkrec.chdrnum);
			zctnIO.setLife(comlinkrec.life);
			zctnIO.setCoverage(comlinkrec.coverage);
			zctnIO.setRider(comlinkrec.rider);
			zctnIO.setTranno(chdrlifIO.getTranno());
			zctnIO.setTransCode(drypDryprcRecInner.drypBatctrcde);
			zctnIO.setEffdate(linsrnlIO.getInstfrom());
			zctnIO.setTrandate(drypDryprcRecInner.drypRunDate);
			zctnIO.setFormat(ZCTNREC);
			zctnIO.setFunction(Varcom.writr);
			SmartFileCode.execute(appVars, zctnIO);
			if (isNE(zctnIO.getStatuz(), Varcom.oK)) {
				drylogrec.statuz.set(zctnIO.getStatuz());
				drylogrec.params.set(zctnIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
	}

	protected void post3632() {
		setUpPostingValues3690();
	}

	protected void postCommission3650(Dry5359Dto dto) {
		start3651(dto);
	}

	protected void start3651(Dry5359Dto dto) {
		int one = 1;
		/* Initialise the ACMV area and move the contract type to its */
		/* relevant field (same for all calls to LIFACMV). */
		lifacmvrec.rldgacct.set(SPACES);
		lifacmvrec.substituteCode[one].set(chdrlifIO.getCnttype());
		wsaaAgntChdrnum.set(covrlnbIO.getChdrnum());
		wsaaAgntLife.set(covrlnbIO.getLife());
		wsaaAgntCoverage.set(covrlnbIO.getCoverage());
		wsaaAgntRider.set(covrlnbIO.getRider());
		wsaaPlan.set(covrlnbIO.getPlanSuffix());
		wsaaAgntPlanSuffix.set(wsaaPlansuff);
		postInitialCommission7000(dto);
		postServiceCommission8000(dto);
		postRenewalCommission9000(dto);
		postOverridCommission10000(dto);
		wsaaBascpyDue.set(0);
		wsaaBascpyErn.set(0);
		wsaaBascpyPay.set(0);
		wsaaSrvcpyDue.set(0);
		wsaaSrvcpyErn.set(0);
		wsaaRnwcpyDue.set(0);
		wsaaRnwcpyErn.set(0);
		wsaaOvrdBascpyPay.set(0);
		wsaaOvrdBascpyDue.set(0);
		wsaaOvrdBascpyErn.set(0);
	}

	protected void rewriteAgcm3670() {
		start3671();
	}

	protected void start3671() {
		agcmbchIO.setFunction(Varcom.readh);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(agcmbchIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(agcmbchIO.getBascpy(), SPACES)) {
			int one = 1;
			setPrecision(agcmbchIO.getCompay(), 2);
			agcmbchIO.setCompay(add(agcmbchIO.getCompay(), wsaaPayamnt[one]));
			setPrecision(agcmbchIO.getComern(), 2);
			agcmbchIO.setComern(add(agcmbchIO.getComern(), wsaaErndamt[one]));
		}
		if (isNE(agcmbchIO.getRnwcpy(), SPACES)) {
			int two = 2;
			setPrecision(agcmbchIO.getRnlcdue(), 2);
			agcmbchIO.setRnlcdue(add(agcmbchIO.getRnlcdue(), wsaaPayamnt[two]));
			setPrecision(agcmbchIO.getRnlcearn(), 2);
			agcmbchIO.setRnlcearn(add(agcmbchIO.getRnlcearn(), wsaaErndamt[two]));
		}
		if (isNE(agcmbchIO.getSrvcpy(), SPACES)) {
			int three = 3;
			setPrecision(agcmbchIO.getScmdue(), 2);
			agcmbchIO.setScmdue(add(agcmbchIO.getScmdue(), wsaaPayamnt[three]));
			setPrecision(agcmbchIO.getScmearn(), 2);
			agcmbchIO.setScmearn(add(agcmbchIO.getScmearn(), wsaaErndamt[three]));
		}
		agcmbchIO.setPtdate(payrIO.getPtdate());
		agcmbchIO.setFunction(Varcom.rewrt);
		agcmbchIO.setFormat(formatsInner.agcmbchrec);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(agcmbchIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void setUpPostingValues3690() {
		start3691();
	}

	protected void start3691() {
		/* Basic Commission */
		if (isEQ(wsaaCommType, 1)) {
			if (isEQ(agcmbchIO.getOvrdcat(), "O")) {
				wsaaOvrdBascpyDue.set(comlinkrec.payamnt);
				wsaaOvrdBascpyErn.set(comlinkrec.erndamt);
				compute(wsaaOvrdBascpyPay, 2)
						.set(add(wsaaOvrdBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
			} else {
				wsaaBascpyDue.set(comlinkrec.payamnt);
				wsaaBascpyErn.set(comlinkrec.erndamt);
				compute(wsaaBascpyPay, 2).set(add(wsaaBascpyPay, (sub(comlinkrec.payamnt, comlinkrec.erndamt))));
			}
		}
		/* Service Commission */
		if (isEQ(wsaaCommType, 3)) {
			wsaaSrvcpyDue.set(comlinkrec.payamnt);
			wsaaSrvcpyErn.set(comlinkrec.erndamt);
		}
		/* Renewal Commission */
		if (isEQ(wsaaCommType, 2)) {
			wsaaRnwcpyDue.set(comlinkrec.payamnt);
			wsaaRnwcpyErn.set(comlinkrec.erndamt);
		}
	}

	protected void writePtrn3800(Dry5359Dto dto) {
		start3810(dto);
	}

	protected void start3810(Dry5359Dto dto) {
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setChdrcoy(dto.getChdrCoy());
		ptrnIO.setChdrnum(dto.getChdrNum());
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx);
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(controlTotalsInner.ct14);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}


	protected void genericProcessing5000(Dry5359Dto dto) {
		start5010(dto);
	}

	protected void start5010(Dry5359Dto dto) {
		wsaaAuthCode.set(drypDryprcRecInner.drypBatctrcde);
		wsaaCovrlnbCrtable.set(covrlnbIO.getCrtable());
		wsaaOvrTarget.set(wsaaOvrTgt);
		ArraySearch as1 = ArraySearch.getInstance(wsaaT5671Rec);
		as1.setIndices(wsaaT5671Ix);
		as1.addSearchKey(wsaaT5671Key, wsaaTrcdeCrtable, true);
		if (as1.binarySearch()) {
			/* CONTINUE_STMT */
		} else {
			return;
		}
		for (wsaaSubprogIx.set(1); !(isGT(wsaaSubprogIx, 4)); wsaaSubprogIx.add(1)) {
			if (isNE(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], SPACES)) {
				callGeneric5100(dto);
			}
		}
	}

	protected void callGeneric5100(Dry5359Dto dto) {
		start5110(dto);
	}

	protected void start5110(Dry5359Dto dto) {
		rnlallrec.statuz.set(Varcom.oK);
		rnlallrec.company.set(dto.getChdrCoy());
		rnlallrec.chdrnum.set(dto.getChdrNum());
		rnlallrec.life.set(covrlnbIO.getLife());
		rnlallrec.coverage.set(covrlnbIO.getCoverage());
		rnlallrec.rider.set(covrlnbIO.getRider());
		rnlallrec.planSuffix.set(covrlnbIO.getPlanSuffix());
		rnlallrec.crdate.set(covrlnbIO.getCrrcd());
		rnlallrec.crtable.set(covrlnbIO.getCrtable());
		rnlallrec.billcd.set(drypDryprcRecInner.drypRunDate);
		rnlallrec.tranno.set(chdrlifIO.getTranno());
		if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[30]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[30]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[30]);
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[31]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[31]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[31]);
		} else {
			rnlallrec.sacscode.set(wsaaT5645Sacscode[20]);
			rnlallrec.sacstyp.set(wsaaT5645Sacstype[20]);
			rnlallrec.genlcde.set(wsaaT5645Glmap[20]);
			rnlallrec.sacscode02.set(wsaaT5645Sacscode[21]);
			rnlallrec.sacstyp02.set(wsaaT5645Sacstype[21]);
			rnlallrec.genlcde02.set(wsaaT5645Glmap[21]);
		}
		rnlallrec.cntcurr.set(covrlnbIO.getPremCurrency());
		rnlallrec.cnttype.set(chdrlifIO.getCnttype());
		rnlallrec.billfreq.set(payrIO.getBillfreq());
		rnlallrec.duedate.set(drypDryprcRecInner.drypRunDate);
		rnlallrec.anbAtCcd.set(covrlnbIO.getAnbAtCcd());
		/* Calculate the term left to run. It is needed in the generic */
		/* processing routine to work out the initial unit discount factor */
		datcon3rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		datcon3rec.intDate2.set(covrlnbIO.getPremCessDate());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			datcon3rec.freqFactorx.set(ZERO);
		}
		wsaaTerm.set(datcon3rec.freqFactor);
		if (isNE(wsaaTermLeftRemain, 0)) {
			wsaaTerm.add(1);
		}
		rnlallrec.termLeftToRun.set(wsaaTermLeftInteger);
		/* Get the total premium from the temporary table for the coverage */
		if (isGT(wsaaUpToTgt, 0)) {
			rnlallrec.covrInstprem.set(wsaaUpToTgt);
			if ((isEQ(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], WSAA_SUBROUTINE_A)
					|| isEQ(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], WSAA_SUBROUTINE_B))) {
				if (isEQ(wsaaFirst, "Y") && isEQ(wsaaLoyaltyApp, "Y")) {
					callProgram(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], rnlallrec.rnlallRec);
					wsaaFirst.set("N");
					wsaaLoyaltyApp.set("N");
				}
			} else {
				callProgram(wsaaT5671Subprog[wsaaT5671Ix.toInt()][wsaaSubprogIx.toInt()], rnlallrec.rnlallRec);

			}
			if (isNE(rnlallrec.statuz, Varcom.oK)) {
				wsysSysparams.set(rnlallrec.rnlallRec);
				drylogrec.params.set(wsysSystemErrorParams);
				drylogrec.statuz.set(rnlallrec.statuz);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
		}
		start5110Continue();
		
	}
	
	private void start5110Continue(){
		/* IF WSAA-OVR-TGT > 0 <LA1647> */
		if (isGT(wsaaOvrTarget, 0)) {
			rnlallrec.covrInstprem.set(wsaaOvrTgt);
			callProgram(Overaloc.class, rnlallrec.rnlallRec);
			if (isNE(rnlallrec.statuz, Varcom.oK)) {
				wsysSysparams.set(rnlallrec.rnlallRec);
				drylogrec.params.set(wsysSystemErrorParams);
				drylogrec.statuz.set(rnlallrec.statuz);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
			wsaaOvrTarget.set(0);
		}
	}

	protected void callLifacmv6000(Dry5359Dto dto) {
		start6010(dto);
	}

	protected void start6010(Dry5359Dto dto) {
		/* This is the only place LIFACMV parameters are referenced. LIFA */
		/* fields that are changeable are set outside this section within */
		/* a working storage variable. */
		lifacmvrec.rdocnum.set(dto.getChdrNum());
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		/* If the ACMV is being created for the COVR-INSTPREM, the */
		/* seventh table entries will be used. Move the appropriate */
		/* value for the COVR posting. */
		lifacmvrec.effdate.set(drypDryprcRecInner.drypRunDate);
		lifacmvrec.termid.set(varcom.vrcmTermid);
		lifacmvrec.user.set(0);
		int one = 1;
		lifacmvrec.substituteCode[one].set(chdrlifIO.getCnttype());
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[wsaaGlSub.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[wsaaGlSub.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[wsaaGlSub.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[wsaaGlSub.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[wsaaGlSub.toInt()]);
		/* Where the system parameter 3 on the process definition */
		/* is not spaces, LIFACMV will be called with the function */
		/* NPSTW instead of PSTW, so that the ACBL file will not */
		/* be updated by this program. */
		/* MOVE 'PSTW' TO LIFA-FUNCTION. */
		lifacmvrec.function.set("PSTD");
		/* To avoid locking when updating the ACBLs of agent commission */
		/* accounts, use the deferred method of updating the ACBLs. */
		deferCheck6100();
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			/* MOVE LIFR-LIFRTRN-REC TO WSYS-SYSPARAMS */
			wsysSysparams.set(lifacmvrec.lifacmvRec);
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.statuz.set(lifacmvrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaRldgacct.set(SPACES);
	}

	protected void deferCheck6100() {
		check6110();
	}

	protected void check6110() {
		/* Check through all the parameters passed via DRYPRCREC to */
		/* determine if the current LIFA-SACSCODE is one where the */
		/* ACBL account should be deferred ( i.e. updated outside of */
		/* this program to avoid locking problems). */
		for (wsaaIx.set(1); !(isGT(wsaaIx, 25)); wsaaIx.add(1)) {
			if (isEQ(drypDryprcRecInner.drypSystParm[wsaaIx.toInt()], lifacmvrec.sacscode)) {
				/* MOVE 'NPSTW' TO LIFA-FUNCTION */
				lifacmvrec.function.set("NPSTD");
				wsaaIx.set(26);
			}
		}
		/* No ACBL updates are to be deferred */
		/* IF LIFA-FUNCTION = 'PSTW' */
		if (isEQ(lifacmvrec.function, "PSTD")) {
			return;
		}
		/* A deferred ACBL update has been found, so its time for action. */
		/* Start by completeing the fields on the ACAGRNL data area. */
		/* Once this is complete, write a new DACM record using the */
		/* ACAGRNL data area. */
		acagrnlIO.setParams(SPACES);
		acagrnlIO.setRecKeyData(SPACES);
		acagrnlIO.setRecNonKeyData(SPACES);
		acagrnlIO.setRldgcoy(lifacmvrec.rldgcoy);
		acagrnlIO.setSacscode(lifacmvrec.sacscode);
		acagrnlIO.setRldgacct(lifacmvrec.rldgacct);
		acagrnlIO.setOrigcurr(lifacmvrec.origcurr);
		acagrnlIO.setSacstyp(lifacmvrec.sacstyp);
		acagrnlIO.setSacscurbal(lifacmvrec.origamt);
		acagrnlIO.setRdocnum(lifacmvrec.rdocnum);
		acagrnlIO.setGlsign(lifacmvrec.glsign);
		/* OK, set up the DACM record, used for deferred processing */
		/* within the batch diary system. */
		dacmIO.setParams(SPACES);
		dacmIO.setRecKeyData(SPACES);
		dacmIO.setRecNonKeyData(SPACES);
		dacmIO.setScheduleThreadNo(drypDryprcRecInner.drypThreadNumber);
		dacmIO.setCompany(drypDryprcRecInner.drypCompany);
		dacmIO.setRecformat(formatsInner.acagrnlrec);
		dacmIO.setDataarea(acagrnlIO.getDataArea());
		dacmIO.setFormat(formatsInner.dacmrec);
		dacmIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, dacmIO);
		if (isNE(dacmIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(dacmIO.getParams());
			drylogrec.statuz.set(dacmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void deferredPostings6100() {
		writeRecord6100();
	}

	protected void writeRecord6100() {
		acagrnlIO.setRldgcoy(lifacmvrec.rldgcoy);
		acagrnlIO.setSacscode(lifacmvrec.sacscode);
		acagrnlIO.setRldgacct(lifacmvrec.rldgacct);
		acagrnlIO.setOrigcurr(lifacmvrec.origcurr);
		acagrnlIO.setSacstyp(lifacmvrec.sacstyp);
		acagrnlIO.setSacscurbal(lifacmvrec.origamt);
		acagrnlIO.setRdocnum(lifacmvrec.rdocnum);
		acagrnlIO.setGlsign(lifacmvrec.glsign);
		acagrnlIO.setFunction(Varcom.writr);
		acagrnlIO.setFormat(formatsInner.acagrnlrec);
		SmartFileCode.execute(appVars, acagrnlIO);
		if (isNE(acagrnlIO.getStatuz(), Varcom.oK) && isNE(acagrnlIO.getStatuz(), Varcom.dupr)) {
			drylogrec.statuz.set(acagrnlIO.getStatuz());
			drylogrec.params.set(acagrnlIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void postInitialCommission7000(Dry5359Dto dto) {
		start7010(dto);
	}

	protected void start7010(Dry5359Dto dto) {
		/* Initial commission due */
		if (isNE(wsaaBascpyDue, 0)) {
			lifacmvrec.jrnseq.add(1);
			wsaaGlSub.set(8);
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.origamt.set(wsaaBascpyDue);
			lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
			callLifacmv6000(dto);
			/* ILIFE-4941-- Post the Fees. */
			if (isGT(payrIO.getSinstamt02(), 0) && isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")
					&& isNE(wsaaT5688Revacc[wsaaT5688Ix.toInt()], "Y")) {
				lifacmvrec.origamt.set(payrIO.getSinstamt02());
				wsaaGlSub.set(32);
				callLifacmv6000(dto);
			}

			/* ILIFE-4941 ends */
			/* Override Commission */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
				b600CallZorcompy();
			}
		}
		/* Initial commission earned */
		if (isNE(wsaaBascpyErn, 0)) {
			lifacmvrec.jrnseq.add(1);
			lifacmvrec.origamt.set(wsaaBascpyErn);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(23);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			} else {
				wsaaGlSub.set(9);
				lifacmvrec.rldgacct.set(dto.getChdrNum());
			}
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			callLifacmv6000(dto);
		}
		/* Initial commission paid */
		if (isNE(wsaaBascpyPay, 0)) {
			lifacmvrec.origamt.set(wsaaBascpyPay);
			lifacmvrec.jrnseq.add(1);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(24);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			} else {
				wsaaGlSub.set(10);
				lifacmvrec.rldgacct.set(dto.getChdrNum());
			}
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			callLifacmv6000(dto);
		}
	}

	protected void postServiceCommission8000(Dry5359Dto dto) {
		start8010(dto);
	}

	protected void start8010(Dry5359Dto dto) {
		/* Service commission due */
		if (isNE(wsaaSrvcpyDue, 0)) {
			lifacmvrec.jrnseq.add(1);
			lifacmvrec.origamt.set(wsaaSrvcpyDue);
			/* MOVE 9 TO WSAA-GL-SUB */
			wsaaGlSub.set(11);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
			lifacmvrec.tranref.set(dto.getChdrNum());
			callLifacmv6000(dto);
			/* Override Commission */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
				b600CallZorcompy();
			}
		}
		/* Service commission earned */
		if (isNE(wsaaSrvcpyErn, 0)) {
			lifacmvrec.jrnseq.add(1);
			lifacmvrec.origamt.set(wsaaSrvcpyErn);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(25);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			} else {
				wsaaGlSub.set(12);
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(dto.getChdrNum());
			}
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			callLifacmv6000(dto);
		}
	}

	protected void postRenewalCommission9000(Dry5359Dto dto) {
		start9010(dto);
	}

	protected void start9010(Dry5359Dto dto) {
		/* Renewal commission due */
		if (isNE(wsaaRnwcpyDue, 0)) {
			lifacmvrec.jrnseq.add(1);
			wsaaGlSub.set(13);
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.origamt.set(wsaaRnwcpyDue);
			lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
			/* MOVE CHDRNUM TO LIFA-TRANREF */
			wsaaRldgChdrnum.set(agcmbchIO.getChdrnum());
			wsaaRldgLife.set(agcmbchIO.getLife());
			wsaaRldgCoverage.set(agcmbchIO.getCoverage());
			wsaaRldgRider.set(agcmbchIO.getRider());
			wsaaPlan.set(agcmbchIO.getPlanSuffix());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.tranref.set(wsaaRldgacct);
			callLifacmv6000(dto);
			/* Override Commission */
			if (isEQ(th605rec.indic, "Y")) {
				zorlnkrec.annprem.set(agcmbchIO.getAnnprem());
				zorlnkrec.effdate.set(agcmbchIO.getEfdate());
				b600CallZorcompy();
			}
		}
		/* Renewal commission earned */
		if (isNE(wsaaRnwcpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaRnwcpyErn);
			lifacmvrec.jrnseq.add(1);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(26);
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			} else {
				wsaaGlSub.set(14);
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(dto.getChdrNum());
			}
			lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
			callLifacmv6000(dto);
		}
	}

	protected void postOverridCommission10000(Dry5359Dto dto) {
		start10100(dto);
	}

	protected void start10100(Dry5359Dto dto) {
		/* Initial over-riding commission due */
		if (isNE(wsaaOvrdBascpyDue, 0)) {
			lifacmvrec.jrnseq.add(1);
			wsaaGlSub.set(16);
			lifacmvrec.origamt.set(wsaaOvrdBascpyDue);
			lifacmvrec.rldgacct.set(agcmbchIO.getAgntnum());
			lifacmvrec.substituteCode[6].set(SPACES);
			lifacmvrec.tranref.set(agcmbchIO.getCedagent());
			callLifacmv6000(dto);
		}
		/* Initial over-riding commission earned */
		if (isNE(wsaaOvrdBascpyErn, 0)) {
			lifacmvrec.origamt.set(wsaaOvrdBascpyErn);
			lifacmvrec.jrnseq.add(1);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(27);
				lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			} else {
				wsaaGlSub.set(17);
				lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
				lifacmvrec.rldgacct.set(dto.getChdrNum());
				lifacmvrec.substituteCode[6].set(SPACES);
			}
			callLifacmv6000(dto);
		}
		/* Initial over-riding commission paid */
		if (isNE(wsaaOvrdBascpyPay, 0)) {
			lifacmvrec.jrnseq.add(1);
			lifacmvrec.origamt.set(wsaaOvrdBascpyPay);
			if (isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix.toInt()], "Y")) {
				wsaaGlSub.set(28);
				lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
				lifacmvrec.rldgacct.set(wsaaRldgagnt);
				lifacmvrec.substituteCode[6].set(covrlnbIO.getCrtable());
			} else {
				wsaaGlSub.set(18);
				lifacmvrec.tranref.set(agcmbchIO.getAgntnum());
				lifacmvrec.substituteCode[6].set(SPACES);
				lifacmvrec.rldgacct.set(chdrlifIO.getChdrnum());
			}
			callLifacmv6000(dto);
		}
	}

	protected void writeFrepRecord12000(Dry5359Dto dto) {
		para12000(dto);
	}

	protected void para12000(Dry5359Dto dto) {
		frepIO.setParams(SPACES);
		frepIO.setPlanSuffix(ZERO);
		frepIO.setTargetPremium(ZERO);
		frepIO.setPremRecPer(ZERO);
		frepIO.setBilledInPeriod(ZERO);
		frepIO.setOverdueMin(ZERO);
		frepIO.setPayrseqno(ZERO);
		frepIO.setMinprem(ZERO);
		frepIO.setMaxprem(ZERO);
		frepIO.setMinOverduePer(ZERO);
		frepIO.setTargfrom(varcom.maxdate);
		frepIO.setAnnivProcDate(varcom.maxdate);
		frepIO.setTargto(varcom.maxdate);
		frepIO.setCompany(drypDryprcRecInner.drypCompany);
		frepIO.setChdrnum(dto.getChdrNum());
		frepIO.setPayrseqno(dto.getPayrseqno());
		frepIO.setProcflag("C");
		frepIO.setBtdate(payrIO.getBtdate());
		frepIO.setLinstamt(payrIO.getSinstamt06());
		if (failMaximumTest.isTrue()) {
			frepIO.setMaxprem(wsaaMaxAllowed);
			frepIO.setSusamt(wsaaAfterPremPaid);
		} else {
			frepIO.setSusamt(wsaaSuspAvail);
			frepIO.setMinprem(wsaaMinAllowed);
		}
		frepIO.setFormat(formatsInner.freprec);
		frepIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, frepIO);
		if (isNE(frepIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(frepIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaFrepCreated.set("Y");
		drycntrec.contotNumber.set(controlTotalsInner.ct15);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	protected void commit3500() {
		/* COMMIT */
		/** Place any additional commitment processing in here. */
		/* EXIT */
	}

	protected void a000CallRounding() {
		/* A100-CALL */
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(drypDryprcRecInner.drypCompany);
		zrdecplrec.statuz.set(Varcom.oK);
		/* MOVE PAYR-BILLCURR TO ZRDP-CURRENCY. */
		zrdecplrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(zrdecplrec.statuz);
			drylogrec.params.set(zrdecplrec.zrdecplRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* A900-EXIT */
	}

	protected void b100InitializeArrays() {
		/* B110-INIT */
		for (wsaaAgcmIx.set(1); !(isGT(wsaaAgcmIx, wsaaAgcmIxSize)); wsaaAgcmIx.add(1)) {
			wsaaAgcmSummaryInner.wsaaAgcmChdrcoy[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmSummaryInner.wsaaAgcmChdrnum[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmSummaryInner.wsaaAgcmLife[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmSummaryInner.wsaaAgcmCoverage[wsaaAgcmIx.toInt()].set(SPACES);
			wsaaAgcmSummaryInner.wsaaAgcmRider[wsaaAgcmIx.toInt()].set(SPACES);
			for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy, wsaaAgcmIySize)); wsaaAgcmIy.add(1)) {
				wsaaAgcmSummaryInner.wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
				wsaaAgcmSummaryInner.wsaaAgcmRecprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(ZERO);
			}
		}
		/* B190-EXIT */
	}

	protected void b200PremiumHistory() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b210Init();
				case B220CALL:
					b220Call();
					b230Summary();
				case B270NEXT:
					b270Next();
				case B290EXIT:
				default:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void b210Init() {
		/* Read the AGCM for differentiating the First & Renewal Year */
		/* Premium */
		agcmseqIO.setDataArea(SPACES);
		agcmseqIO.setChdrcoy(covrlnbIO.getChdrcoy());
		agcmseqIO.setChdrnum(covrlnbIO.getChdrnum());
		agcmseqIO.setLife(covrlnbIO.getLife());
		agcmseqIO.setCoverage(covrlnbIO.getCoverage());
		agcmseqIO.setRider(covrlnbIO.getRider());
		agcmseqIO.setPlanSuffix(covrlnbIO.getPlanSuffix());
		agcmseqIO.setSeqno(ZERO);
		agcmseqIO.setAgntnum(SPACES);
		agcmseqIO.setFormat("AGCMSEQREC");
		agcmseqIO.setFunction(Varcom.begn);
	}

	protected void b220Call() {
		SmartFileCode.execute(appVars, agcmseqIO);
		if (isNE(agcmseqIO.getStatuz(), Varcom.oK) && isNE(agcmseqIO.getStatuz(), Varcom.endp)) {
			drylogrec.statuz.set(agcmseqIO.getStatuz());
			drylogrec.params.set(agcmseqIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(agcmseqIO.getStatuz(), Varcom.endp) || isNE(agcmseqIO.getChdrcoy(), covrlnbIO.getChdrcoy())
				|| isNE(agcmseqIO.getChdrnum(), covrlnbIO.getChdrnum())
				|| isNE(agcmseqIO.getLife(), covrlnbIO.getLife())
				|| isNE(agcmseqIO.getCoverage(), covrlnbIO.getCoverage())
				|| isNE(agcmseqIO.getRider(), covrlnbIO.getRider())) {
			agcmbchIO.setStatuz(Varcom.endp);
			goTo(GotoLabel.B290EXIT);
		}
		/* Skip those irrelevant records */
		if (isNE(agcmseqIO.getOvrdcat(), "B") || isEQ(agcmseqIO.getPtdate(), ZERO) || isEQ(agcmseqIO.getEfdate(), ZERO)
				|| isEQ(agcmseqIO.getEfdate(), varcom.vrcmMaxDate)) {
			goTo(GotoLabel.B270NEXT);
		}
	}

	protected void b230Summary() {
		wsaaAgcmFound.set("N");
		for (wsaaAgcmIy.set(1); !(isGT(wsaaAgcmIy, wsaaAgcmIySize)
				|| isEQ(wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO)
				|| agcmFound.isTrue()); wsaaAgcmIy.add(1)) {
			if (isEQ(agcmseqIO.getSeqno(),
					wsaaAgcmSummaryInner.wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()])) {
				wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]
						.add(agcmseqIO.getAnnprem());
				agcmFound.setTrue();
			}
		}
		if (!agcmFound.isTrue()) {
			if (isGT(wsaaAgcmIy, wsaaAgcmIySize)) {
				drylogrec.statuz.set(E103);
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			wsaaAgcmSummaryInner.wsaaAgcmSeqno[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getSeqno());
			wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getEfdate());
			wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(agcmseqIO.getAnnprem());
		}
	}

	protected void b270Next() {
		agcmseqIO.setFunction(Varcom.nextr);
		goTo(GotoLabel.B220CALL);
	}

	protected void b300WriteArrays() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					b310Init();
				case B320LOCATE:
					b320Locate();
				case B330LOOP:
					b330Loop();
					b340Writ();
					b380Next();
				case B390EXIT:
				default:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void b310Init() {
		wsaaAgcmIy.set(1);
		wsaaAgcmPremLeft.set(wsaaUpToTgt);
		wsaaOutstandingPremium.set(ZERO);
		firstTime.setTrue();
	}

	protected void b320Locate() {
		if (isEQ(wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], ZERO)) {
			goTo(GotoLabel.B390EXIT);
		}
		if (isGTE(wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], fpcoIO.getTargto())) {
			wsaaAgcmIy.add(1);
			goTo(GotoLabel.B320LOCATE);
		}
		/* Locate the current AGCM that premium is allocated */
		if (isLTE(wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], wsaaReceivedPremium)) {
			compute(wsaaReceivedPremium, 2).set(sub(wsaaReceivedPremium,
					wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]));
			wsaaAgcmIy.add(1);
			goTo(GotoLabel.B320LOCATE);
		}
	}

	protected void b330Loop() {
		if (firstTime.isTrue()) {
			compute(wsaaOutstandingPremium, 2).set(sub(
					wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()], wsaaReceivedPremium));
			wsaaFirstTime.set("N");
		} else {
			wsaaOutstandingPremium.set(wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]);
		}
		if (isEQ(wsaaAgcmPremLeft, ZERO)) {
			goTo(GotoLabel.B390EXIT);
		}
	}

	protected void b340Writ() {
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(wsaaAgcmSummaryInner.wsaaAgcmEfdate[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()]);
		datcon3rec.intDate2.set(fpcoIO.getTargfrom());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isGTE(datcon3rec.freqFactor, 1)) {
			renPrem.setTrue();
		} else {
			firstPrem.setTrue();
		}
		if (isNE(wsaaAgcmPremLeft, ZERO)) {
			zptnIO.setParams(SPACES);
			zptnIO.setChdrcoy(covrlnbIO.getChdrcoy());
			zptnIO.setChdrnum(covrlnbIO.getChdrnum());
			zptnIO.setLife(covrlnbIO.getLife());
			zptnIO.setCoverage(covrlnbIO.getCoverage());
			zptnIO.setRider(covrlnbIO.getRider());
			zptnIO.setTranno(chdrlifIO.getTranno());
			if (isGTE(wsaaOutstandingPremium, wsaaAgcmPremLeft)) {
				zptnIO.setOrigamt(wsaaAgcmPremLeft);
				wsaaAgcmPremLeft.set(ZERO);
			} else {
				zptnIO.setOrigamt(wsaaOutstandingPremium);
				compute(wsaaAgcmPremLeft, 2).set(sub(wsaaAgcmPremLeft, wsaaOutstandingPremium));
			}
			wsaaAgcmSummaryInner.wsaaAgcmRecprem[wsaaAgcmIx.toInt()][wsaaAgcmIy.toInt()].set(zptnIO.getOrigamt());
			zptnIO.setTransCode(drypDryprcRecInner.drypBatctrcde);
			if (isGTE(linsrnlIO.getInstfrom(), fpcoIO.getTargto())) {
				zptnIO.setEffdate(fpcoIO.getTargto());
				zptnIO.setInstfrom(fpcoIO.getTargto());
			} else {
				zptnIO.setEffdate(linsrnlIO.getInstfrom());
				zptnIO.setInstfrom(linsrnlIO.getInstfrom());
			}
			zptnIO.setBillcd(linsrnlIO.getBillcd());
			if (isGTE(linsrnlIO.getInstto(), fpcoIO.getTargto())) {
				zptnIO.setInstto(fpcoIO.getTargto());
			} else {
				zptnIO.setInstto(linsrnlIO.getInstto());
			}
			writeZptn(zptnIO);
		}
	}
	
	private void writeZptn(ZptnTableDAM zptnIO){
		zptnIO.setTrandate(drypDryprcRecInner.drypRunDate);
		zptnIO.setZprflg(wsaaPremType);
		zptnIO.setFormat(ZPTNREC);
		zptnIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(zptnIO.getStatuz());
			drylogrec.params.set(zptnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void b380Next() {
		wsaaAgcmIy.add(1);
		goTo(GotoLabel.B330LOOP);
	}

	protected void b400LocatePremium() {
		/* B410-INIT */
		wsaaAgcmFound.set("N");
		wsaaAgcmPremium.set(ZERO);
		wsaaAgcmAlcprem.set(ZERO);
		for (wsaaAgcmIa.set(1); !(isGT(wsaaAgcmIa, wsaaAgcmIxSize) || agcmFound.isTrue()); wsaaAgcmIa.add(1)) {
			if (isEQ(agcmbchIO.getChdrcoy(), wsaaAgcmSummaryInner.wsaaAgcmChdrcoy[wsaaAgcmIa.toInt()])
					&& isEQ(agcmbchIO.getChdrnum(), wsaaAgcmSummaryInner.wsaaAgcmChdrnum[wsaaAgcmIa.toInt()])
					&& isEQ(agcmbchIO.getLife(), wsaaAgcmSummaryInner.wsaaAgcmLife[wsaaAgcmIa.toInt()])
					&& isEQ(agcmbchIO.getCoverage(), wsaaAgcmSummaryInner.wsaaAgcmCoverage[wsaaAgcmIa.toInt()])
					&& isEQ(agcmbchIO.getRider(), wsaaAgcmSummaryInner.wsaaAgcmRider[wsaaAgcmIa.toInt()])) {
				for (wsaaAgcmIb.set(1); !(isGT(wsaaAgcmIb, wsaaAgcmIySize) || agcmFound.isTrue()); wsaaAgcmIb.add(1)) {
					if (isEQ(agcmbchIO.getSeqno(),
							wsaaAgcmSummaryInner.wsaaAgcmSeqno[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()])) {
						agcmFound.setTrue();
						wsaaAgcmPremium
								.set(wsaaAgcmSummaryInner.wsaaAgcmAnnprem[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()]);
						wsaaAgcmAlcprem
								.set(wsaaAgcmSummaryInner.wsaaAgcmRecprem[wsaaAgcmIa.toInt()][wsaaAgcmIb.toInt()]);
					}
				}
			}
		}
		/* B490-EXIT */
	}

	protected void b500WriteOverTarget() {
		b510Writ();
	}

	protected void b510Writ() {
		initialize(datcon3rec.datcon3Rec);
		datcon3rec.intDate1.set(covrlnbIO.getCrrcd());
		datcon3rec.intDate2.set(fpcoIO.getTargfrom());
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isGTE(datcon3rec.freqFactor, 1)) {
			renPrem.setTrue();
		} else {
			firstPrem.setTrue();
		}
		zptnIO.setParams(SPACES);
		zptnIO.setChdrcoy(covrlnbIO.getChdrcoy());
		zptnIO.setChdrnum(covrlnbIO.getChdrnum());
		zptnIO.setLife(covrlnbIO.getLife());
		zptnIO.setCoverage(covrlnbIO.getCoverage());
		zptnIO.setRider(covrlnbIO.getRider());
		zptnIO.setTranno(chdrlifIO.getTranno());
		zptnIO.setOrigamt(wsaaOvrTgt);
		zptnIO.setTransCode(drypDryprcRecInner.drypBatctrcde);
		if (isGTE(linsrnlIO.getInstfrom(), fpcoIO.getTargto())) {
			zptnIO.setEffdate(fpcoIO.getTargto());
			zptnIO.setInstfrom(fpcoIO.getTargto());
		} else {
			zptnIO.setEffdate(linsrnlIO.getInstfrom());
			zptnIO.setInstfrom(linsrnlIO.getInstfrom());
		}
		zptnIO.setBillcd(linsrnlIO.getBillcd());
		if (isGTE(linsrnlIO.getInstto(), fpcoIO.getTargto())) {
			zptnIO.setInstto(fpcoIO.getTargto());
		} else {
			zptnIO.setInstto(linsrnlIO.getInstto());
		}
		zptnIO.setTrandate(drypDryprcRecInner.drypRunDate);
		zptnIO.setZprflg(wsaaPremType);
		zptnIO.setFormat(ZPTNREC);
		zptnIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, zptnIO);
		if (isNE(zptnIO.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(zptnIO.getStatuz());
			drylogrec.params.set(zptnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void b600CallZorcompy() {
		b610Start();
	}

	protected void b610Start() {
		zorlnkrec.function.set(SPACES);
		zorlnkrec.clawback.set(SPACES);
		zorlnkrec.agent.set(lifacmvrec.rldgacct);
		zorlnkrec.chdrcoy.set(lifacmvrec.rldgcoy);
		zorlnkrec.chdrnum.set(lifacmvrec.rdocnum);
		zorlnkrec.crtable.set(lifacmvrec.substituteCode[6]);
		zorlnkrec.ptdate.set(chdrlifIO.getPtdate());
		zorlnkrec.origcurr.set(lifacmvrec.origcurr);
		zorlnkrec.crate.set(lifacmvrec.crate);
		zorlnkrec.origamt.set(lifacmvrec.origamt);
		zorlnkrec.tranno.set(chdrlifIO.getTranno());
		zorlnkrec.trandesc.set(lifacmvrec.trandesc);
		zorlnkrec.tranref.set(lifacmvrec.tranref);
		zorlnkrec.genlcur.set(lifacmvrec.genlcur);
		zorlnkrec.sacstyp.set(lifacmvrec.sacstyp);
		zorlnkrec.termid.set(lifacmvrec.termid);
		int one = 1;
		zorlnkrec.cnttype.set(lifacmvrec.substituteCode[one]);
		zorlnkrec.batchKey.set(lifacmvrec.batckey);
		zorlnkrec.clawback.set(SPACES);
		callProgram(Zorcompy.class, zorlnkrec.zorlnkRec);
		if (isNE(zorlnkrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(zorlnkrec.statuz);
			drylogrec.params.set(zorlnkrec.zorlnkRec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void b700PropTax() {
		b700Start();
	}

	/**
	 * <pre>
	* this section will prorate the tax in proportion to the
	* net premium
	 * </pre>
	 */
	protected void b700Start() {
		if (isEQ(wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()], SPACES)
				&& isEQ(wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()], SPACES)
				&& isEQ(wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()], SPACES)) {
			wsaaEndPost.set("Y");
			return;
		}
		if (isEQ(covrlnbIO.getLife(), wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()])
				&& isEQ(covrlnbIO.getCoverage(), wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()])
				&& isEQ(covrlnbIO.getRider(), wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()])
				&& isEQ(covrlnbIO.getPlanSuffix(),
						wsaaVariablesInner.wsaaPlanSuffix[wsaaVariablesInner.wsaaIx.toInt()])) {
			int one = 1;
			int two = 2;
			if (isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one], 0)) {
				compute(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one], 2)
						.set(mult(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one],
								(div(wsaaVariablesInner.wsaaPremNet, payrIO.getSinstamt01()))));
				if (isNE(wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][one], "Y")) {
					wsaaVariablesInner.wsaaTotTaxCharged
							.add(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one]);
				}
			}
			if (isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two], 0)) {
				compute(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two], 2)
						.set(mult(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two],
								(div(wsaaVariablesInner.wsaaPremNet, payrIO.getSinstamt01()))));
				if (isNE(wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][two], "Y")) {
					wsaaVariablesInner.wsaaTotTaxCharged
							.add(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two]);
				}
			}
			/* ensure that the premium amount + tax is equal */
			/* to the amount in suspense */
			if ((setPrecision(wsaaVariablesInner.wsaaOrigSusp, 2) && isNE(
					(add(wsaaPolamnt, wsaaVariablesInner.wsaaTotTaxCharged)), wsaaVariablesInner.wsaaOrigSusp))) {
				compute(wsaaVariablesInner.wsaaDiff, 2).set(
						sub(sub(wsaaVariablesInner.wsaaOrigSusp, wsaaPolamnt), wsaaVariablesInner.wsaaTotTaxCharged));
				/* add/subtract the rounding differences to the tax amount */
				if (isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one], 0)) {
					wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one].add(wsaaVariablesInner.wsaaDiff);
				} else {
					wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two].add(wsaaVariablesInner.wsaaDiff);
				}
			}
		}
	}

	protected void b750PostTax() {
		b750Start();
	}

	protected void b750Start() {
		int one = 1;
		int two = 2;
		if (isEQ(wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()], SPACES)
				&& isEQ(wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()], SPACES)
				&& isEQ(wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()], SPACES)) {
			wsaaEndPost.set("Y");
			return;
		}
		if (isEQ(covrlnbIO.getLife(), wsaaVariablesInner.wsaaLife[wsaaVariablesInner.wsaaIx.toInt()])
				&& isEQ(covrlnbIO.getCoverage(), wsaaVariablesInner.wsaaCoverage[wsaaVariablesInner.wsaaIx.toInt()])
				&& isEQ(covrlnbIO.getRider(), wsaaVariablesInner.wsaaRider[wsaaVariablesInner.wsaaIx.toInt()])
				&& isEQ(covrlnbIO.getPlanSuffix(), wsaaVariablesInner.wsaaPlanSuffix[wsaaVariablesInner.wsaaIx.toInt()])
				&& (isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one], 0)
						|| isNE(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two], 0))) {
			initialize(txcalcrec.linkRec);
			txcalcrec.function.set("POST");
			txcalcrec.statuz.set(Varcom.oK);
			txcalcrec.transType.set("PREM");
			txcalcrec.chdrcoy.set(chdrlifIO.getChdrcoy());
			txcalcrec.chdrnum.set(chdrlifIO.getChdrnum());
			txcalcrec.life.set(covrlnbIO.getLife());
			txcalcrec.coverage.set(covrlnbIO.getCoverage());
			txcalcrec.rider.set(covrlnbIO.getRider());
			txcalcrec.planSuffix.set(covrlnbIO.getPlanSuffix());
			txcalcrec.crtable.set(covrlnbIO.getCrtable());
			txcalcrec.cnttype.set(chdrlifIO.getCnttype());
			txcalcrec.register.set(chdrlifIO.getCnttype());
			txcalcrec.taxrule.set(wsaaVariablesInner.wsaaTaxRule[wsaaVariablesInner.wsaaIx.toInt()]);
			txcalcrec.rateItem.set(wsaaVariablesInner.wsaaTaxItem[wsaaVariablesInner.wsaaIx.toInt()]);
			txcalcrec.amountIn.set(wsaaPolamnt);
			txcalcrec.effdate.set(drypDryprcRecInner.drypRunDate);
			txcalcrec.taxType[one].set(wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][one]);
			txcalcrec.taxType[two].set(wsaaVariablesInner.wsaaTaxType[wsaaVariablesInner.wsaaIx.toInt()][two]);
			txcalcrec.taxAbsorb[one].set(wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][one]);
			txcalcrec.taxAbsorb[two].set(wsaaVariablesInner.wsaaTaxAbs[wsaaVariablesInner.wsaaIx.toInt()][two]);
			txcalcrec.taxAmt[one].set(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][one]);
			txcalcrec.taxAmt[two].set(wsaaVariablesInner.wsaaTax[wsaaVariablesInner.wsaaIx.toInt()][two]);
			txcalcrec.jrnseq.set(lifacmvrec.jrnseq);
			txcalcrec.jrnseq.add(1);
			txcalcrec.batckey.set(drypDryprcRecInner.drypBatchKey);
			txcalcrec.ccy.set(chdrlifIO.getCntcurr());
			txcalcrec.tranno.set(chdrlifIO.getTranno());
			txcalcrec.language.set(drypDryprcRecInner.drypLanguage);
			callProgram(tr52drec.txsubr, txcalcrec.linkRec);
			if (isNE(txcalcrec.statuz, Varcom.oK)) {
				drylogrec.params.set(txcalcrec.linkRec);
				drylogrec.statuz.set(txcalcrec.statuz);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
			b90aCreateTaxd();
		}
	}

	protected void b800ReadTr52d() {
		b810Start();
	}

	protected void b810Start() {
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(tablesInner.tr52d);
		itemIO.setItemitem(chdrlifIO.getRegister());
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			itemIO.setItempfx("IT");
			itemIO.setItemcoy(chdrlifIO.getChdrcoy());
			itemIO.setItemtabl(tablesInner.tr52d);
			itemIO.setItemitem("***");
			itemIO.setFormat(formatsInner.itemrec);
			itemIO.setFunction(Varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), Varcom.oK)) {
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.params.set(itemIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
		tr52drec.tr52dRec.set(itemIO.getGenarea());
	}

	protected void b900ReadTr52e(Dry5359Dto dto) {
		b910Start(dto);
	}

	protected void b910Start(Dry5359Dto dto) {
		itdmIO.setDataArea(SPACES);
		tr52erec.tr52eRec.set(SPACES);
		itdmIO.setItemcoy(dto.getChdrCoy());
		itdmIO.setItemtabl(tablesInner.tr52e);
		itdmIO.setItemitem(wsaaTr52eKey);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(Varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), Varcom.oK)) && (isNE(itdmIO.getStatuz(), Varcom.endp))) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (((isNE(itdmIO.getItemcoy(), dto.getChdrCoy())) || (isNE(itdmIO.getItemtabl(), tablesInner.tr52e))
				|| (isNE(itdmIO.getItemitem(), wsaaTr52eKey)) || (isEQ(itdmIO.getStatuz(), Varcom.endp)))
				&& (isEQ(subString(wsaaTr52eKey, 2, 7), "*******"))) {
			drylogrec.params.set(wsaaTr52eKey);
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (((isEQ(itdmIO.getItemcoy(), dto.getChdrCoy())) && (isEQ(itdmIO.getItemtabl(), tablesInner.tr52e))
				&& (isEQ(itdmIO.getItemitem(), wsaaTr52eKey)) && (isNE(itdmIO.getStatuz(), Varcom.endp)))) {
			tr52erec.tr52eRec.set(itdmIO.getGenarea());
		}
	}

	protected void b90aCreateTaxd() {
		b90aStart();
	}

	protected void b90aStart() {
		taxdIO.setDataArea(SPACES);
		taxdIO.setChdrcoy(chdrlifIO.getChdrcoy());
		taxdIO.setChdrnum(chdrlifIO.getChdrnum());
		taxdIO.setTrantype("PREM");
		taxdIO.setLife(covrlnbIO.getLife());
		taxdIO.setCoverage(covrlnbIO.getCoverage());
		taxdIO.setRider(covrlnbIO.getRider());
		taxdIO.setPlansfx(covrlnbIO.getPlanSuffix());
		taxdIO.setEffdate(txcalcrec.effdate);
		taxdIO.setBillcd(txcalcrec.effdate);
		wsaaVariablesInner.wsaaTranref.set(SPACES);
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression(txcalcrec.taxrule);
		stringVariable1.addExpression(txcalcrec.rateItem);
		stringVariable1.setStringInto(wsaaVariablesInner.wsaaTranref);
		taxdIO.setTranref(wsaaVariablesInner.wsaaTranref);
		taxdIO.setInstfrom(payrIO.getPtdate());
		taxdIO.setInstto(payrIO.getBtdate());
		taxdIO.setTranno(chdrlifIO.getTranno());
		taxdIO.setBaseamt(txcalcrec.amountIn);
		int one = 1;
		int two = 2;
		taxdIO.setTaxamt01(txcalcrec.taxAmt[one]);
		taxdIO.setTaxamt02(txcalcrec.taxAmt[two]);
		taxdIO.setTaxamt03(ZERO);
		taxdIO.setTxabsind01(txcalcrec.taxAbsorb[one]);
		taxdIO.setTxabsind02(txcalcrec.taxAbsorb[two]);
		taxdIO.setTxabsind03(SPACES);
		taxdIO.setTxtype01(txcalcrec.taxType[one]);
		taxdIO.setTxtype02(txcalcrec.taxType[two]);
		taxdIO.setTxtype03(SPACES);
		taxdIO.setPostflg("P");
		taxdIO.setFormat(formatsInner.taxdrec);
		taxdIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, taxdIO);
		if (isNE(taxdIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(taxdIO.getParams());
			drylogrec.statuz.set(taxdIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void checkOverdueprocess() {
		getT554010300();
		getT553511100();

	}

	protected void getT554010300() {
		para10310();
	}

	protected void para10310() {
		wsaaCrtable.set(covrlnbIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrlifIO.getChdrcoy());
		itdmIO.setItemtabl(tablesInner.t5540);
		itdmIO.setItemitem(wsaaCrtable);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(Varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if ((isNE(itdmIO.getStatuz(), Varcom.oK)) && (isNE(itdmIO.getStatuz(), Varcom.mrnf))) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		} else {
			t5540rec.t5540Rec.set(itdmIO.getGenarea());
		}
	}

	protected void getT553511100() {
		para11110();
	}

	protected void para11110() {

		if (isEQ(t5540rec.ltypst, SPACES)) {
			wsaaLoyaltyApp.set("N");
		} else {
			calcLeadDays();
		}

	}

	protected void calcLeadDays() {
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(payrIO.getPtdate());
		datcon3rec.intDate2.set(drypDryprcRecInner.drypRunDate);
		datcon3rec.frequency.set("DY");
		datcon3rec.freqFactor.set(0);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaOverdueDays.set(datcon3rec.freqFactor);

		String key = payrIO.getBillchnl().toString().trim().concat(chdrlifIO.getCnttype().toString().trim());
		if(BTPRO028Permission) {
			key = key.concat(payrIO.getBillfreq().toString().trim());
		}
		wsaaT6654Ix.set(1);
		if (readT6654(key)) {
			if (isLT(t6654rec.leadDays, wsaaOverdueDays) && isNE(t6654rec.leadDays, 0)) {

				wsaaLoyaltyApp.set("N");
			} else {
				wsaaLoyaltyApp.set("Y");
			}
		}
	}

	protected boolean readT6654(String searchItem) {
		String strEffDate = drypDryprcRecInner.drypRunDate.toString();
		String keyItemitem = searchItem.trim();
		List<Itempf> itempfList = new ArrayList<Itempf>();
		boolean itemFound = false;
		String itemcoy = drypDryprcRecInner.drypCompany.toString();

		t6654ListMap = itemDAO.loadSmartTable("IT", itemcoy, T6654);
		if (null == t6654ListMap || t6654ListMap.isEmpty()) {
			drylogrec.params.set("t6654");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if (t6654ListMap.containsKey(keyItemitem)) {
			itempfList = t6654ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = new Itempf();
				itempf = iterator.next();
				if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
					if ((Integer.parseInt(strEffDate) >= Integer.parseInt(itempf.getItmfrm().toString()))
							&& Integer.parseInt(strEffDate) <= Integer.parseInt(itempf.getItmto().toString())) {
						t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));

						wsaaT6654ArrayInner.wsaaT6654Key[wsaaT6654Ix.toInt()].set(itempf.getItemitem());
						wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()].set(t6654rec.nonForfietureDays);
						wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()].set(t6654rec.arrearsMethod);
						for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)) {
							wsaaT6654SubrIx.set(wsaaSub);
							wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()]
									.set(t6654rec.doctid[wsaaSub.toInt()]);
						}
						for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)) {
							wsaaT6654ExpyIx.set(wsaaSub);
							wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaT6654ExpyIx.toInt()]
									.set(t6654rec.daexpy[wsaaSub.toInt()]);
						}
						wsaaT6654Ix.add(1);
						itemFound = true;
					}
				} else {
					t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					wsaaT6654ArrayInner.wsaaT6654Key[wsaaT6654Ix.toInt()].set(itempf.getItemitem());
					wsaaT6654ArrayInner.wsaaT6654NonForfDays[wsaaT6654Ix.toInt()].set(t6654rec.nonForfietureDays);
					wsaaT6654ArrayInner.wsaaT6654ArrearsMethod[wsaaT6654Ix.toInt()].set(t6654rec.arrearsMethod);
					for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)) {
						wsaaT6654SubrIx.set(wsaaSub);
						wsaaT6654ArrayInner.wsaaT6654Doctid[wsaaT6654Ix.toInt()][wsaaT6654SubrIx.toInt()]
								.set(t6654rec.doctid[wsaaSub.toInt()]);
					}
					for (wsaaSub.set(1); !(isGT(wsaaSub, 3)); wsaaSub.add(1)) {
						wsaaT6654ExpyIx.set(wsaaSub);
						wsaaT6654ArrayInner.wsaaT6654Daexpy[wsaaT6654Ix.toInt()][wsaaT6654ExpyIx.toInt()]
								.set(t6654rec.daexpy[wsaaSub.toInt()]);
					}
					wsaaT6654Ix.add(1);
					itemFound = true;
				}
			}
		}
		return itemFound;

	}
	
	private void updateDiaryData() {
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		wsaaTomorrow.set(datcon2rec.intDate2);
		/* Update the DRYP parameters.*/
		if (isCollectionHappened) {
			drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		}
		else {
			if (isGTE(drypDryprcRecInner.drypRunDate, wsaaToday)) {
				drypDryprcRecInner.drypBillcd.set(wsaaTomorrow);
			}
			else {
				drypDryprcRecInner.drypBillcd.set(wsaaToday);
			}
		}
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		//IJTI-2136 Added code to pass BTDATE and PTDATE to diary subroutine
		// without it overdue trigger was not coming properly
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());//IJTI-2136
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());//IJTI-2136
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
	}

	/*
	 * Class transformed from Data Structure WSAA-T5729-ARRAY--INNER
	 */
	private static final class WsaaT5729ArrayInner {

		/* WSAA-T5729-ARRAY */
		private FixedLengthStringData[] wsaaT5729Rec = FLSInittedArray(60, 380);
		private FixedLengthStringData[] wsaaT5729Key = FLSDArrayPartOfArrayStructure(3, wsaaT5729Rec, 0);
		private FixedLengthStringData[] wsaaT5729Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT5729Key, 0, SPACES);
		private FixedLengthStringData[] wsaaT5729Data = FLSDArrayPartOfArrayStructure(377, wsaaT5729Rec, 3);
		private PackedDecimalData[] wsaaT5729Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5729Data, 0);
		private FixedLengthStringData[] wsaaT5729Frqcys = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 5);
		private FixedLengthStringData[][] wsaaT5729Frqcy = FLSDArrayPartOfArrayStructure(6, 2, wsaaT5729Frqcys, 0);
		private FixedLengthStringData[] wsaaT5729Durationas = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 17);
		private ZonedDecimalData[][] wsaaT5729Durationa = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationas, 0);
		private FixedLengthStringData[] wsaaT5729Durationbs = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 33);
		private ZonedDecimalData[][] wsaaT5729Durationb = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationbs, 0);
		private FixedLengthStringData[] wsaaT5729Durationcs = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 49);
		private ZonedDecimalData[][] wsaaT5729Durationc = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationcs, 0);
		private FixedLengthStringData[] wsaaT5729Durationds = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 65);
		private ZonedDecimalData[][] wsaaT5729Durationd = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationds, 0);
		private FixedLengthStringData[] wsaaT5729Durationes = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 81);
		private ZonedDecimalData[][] wsaaT5729Duratione = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationes, 0);
		private FixedLengthStringData[] wsaaT5729Durationfs = FLSDArrayPartOfArrayStructure(16, wsaaT5729Data, 97);
		private ZonedDecimalData[][] wsaaT5729Durationf = ZDArrayPartOfArrayStructure(4, 4, 0, wsaaT5729Durationfs, 0);
		private FixedLengthStringData[] wsaaT5729TargetMinas = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 113);
		private ZonedDecimalData[][] wsaaT5729TargetMina = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinas,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMinbs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 125);
		private ZonedDecimalData[][] wsaaT5729TargetMinb = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinbs,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMincs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 137);
		private ZonedDecimalData[][] wsaaT5729TargetMinc = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMincs,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMinds = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 149);
		private ZonedDecimalData[][] wsaaT5729TargetMind = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinds,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMines = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 161);
		private ZonedDecimalData[][] wsaaT5729TargetMine = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMines,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMinfs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 173);
		private ZonedDecimalData[][] wsaaT5729TargetMinf = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729TargetMinfs,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMaxas = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 185);
		private ZonedDecimalData[][] wsaaT5729TargetMaxa = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxas,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMaxbs = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 205);
		private ZonedDecimalData[][] wsaaT5729TargetMaxb = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxbs,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMaxcs = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 225);
		private ZonedDecimalData[][] wsaaT5729TargetMaxc = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxcs,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMaxds = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 245);
		private ZonedDecimalData[][] wsaaT5729TargetMaxd = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxds,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMaxes = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 265);
		private ZonedDecimalData[][] wsaaT5729TargetMaxe = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxes,
				0);
		private FixedLengthStringData[] wsaaT5729TargetMaxfs = FLSDArrayPartOfArrayStructure(20, wsaaT5729Data, 285);
		private ZonedDecimalData[][] wsaaT5729TargetMaxf = ZDArrayPartOfArrayStructure(4, 5, 0, wsaaT5729TargetMaxfs,
				0);
		private FixedLengthStringData[] wsaaT5729OverdueMinas = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 305);
		private ZonedDecimalData[][] wsaaT5729OverdueMina = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinas,
				0);
		private FixedLengthStringData[] wsaaT5729OverdueMinbs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 317);
		private ZonedDecimalData[][] wsaaT5729OverdueMinb = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinbs,
				0);
		private FixedLengthStringData[] wsaaT5729OverdueMincs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 329);
		private ZonedDecimalData[][] wsaaT5729OverdueMinc = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMincs,
				0);
		private FixedLengthStringData[] wsaaT5729OverdueMinds = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 341);
		private ZonedDecimalData[][] wsaaT5729OverdueMind = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinds,
				0);
		private FixedLengthStringData[] wsaaT5729OverdueMines = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 353);
		private ZonedDecimalData[][] wsaaT5729OverdueMine = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMines,
				0);
		private FixedLengthStringData[] wsaaT5729OverdueMinfs = FLSDArrayPartOfArrayStructure(12, wsaaT5729Data, 365);
		private ZonedDecimalData[][] wsaaT5729OverdueMinf = ZDArrayPartOfArrayStructure(4, 3, 0, wsaaT5729OverdueMinfs,
				0);
	}

	/*
	 * Class transformed from Data Structure WSAA-AGCM-SUMMARY--INNER
	 */
	private static final class WsaaAgcmSummaryInner {

		/* WSAA-AGCM-SUMMARY */
		private FixedLengthStringData[] wsaaAgcmRec = FLSInittedArray(100, 13015);
		private FixedLengthStringData[] wsaaAgcmChdrcoy = FLSDArrayPartOfArrayStructure(1, wsaaAgcmRec, 0);
		private FixedLengthStringData[] wsaaAgcmChdrnum = FLSDArrayPartOfArrayStructure(8, wsaaAgcmRec, 1);
		private FixedLengthStringData[] wsaaAgcmLife = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 9);
		private FixedLengthStringData[] wsaaAgcmCoverage = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 11);
		private FixedLengthStringData[] wsaaAgcmRider = FLSDArrayPartOfArrayStructure(2, wsaaAgcmRec, 13);
		private FixedLengthStringData[][] wsaaAgcmPremRec = FLSDArrayPartOfArrayStructure(500, 26, wsaaAgcmRec, 15);
		private ZonedDecimalData[][] wsaaAgcmSeqno = ZDArrayPartOfArrayStructure(3, 0, wsaaAgcmPremRec, 0,
				UNSIGNED_TRUE);
		private PackedDecimalData[][] wsaaAgcmEfdate = PDArrayPartOfArrayStructure(8, 0, wsaaAgcmPremRec, 3,
				UNSIGNED_TRUE);
		private PackedDecimalData[][] wsaaAgcmAnnprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 8);
		private PackedDecimalData[][] wsaaAgcmRecprem = PDArrayPartOfArrayStructure(17, 2, wsaaAgcmPremRec, 17);
	}

	/*
	 * Class transformed from Data Structure WSAA-VARIABLES--INNER
	 */
	private static final class WsaaVariablesInner {
		/* WSAA-VARIABLES */
		private ZonedDecimalData wsaaTotTaxCharged = new ZonedDecimalData(11, 2);
		private ZonedDecimalData wsaaIx = new ZonedDecimalData(2, 0).setUnsigned();

		private FixedLengthStringData[] wsaaComponents = FLSInittedArray(99, 62);
		private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaComponents, 0);
		private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaComponents, 2);
		private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaComponents, 4);
		private ZonedDecimalData[] wsaaPlanSuffix = ZDArrayPartOfArrayStructure(2, 0, wsaaComponents, 6, UNSIGNED_TRUE);
		private ZonedDecimalData[][] wsaaTax = ZDArrayPartOfArrayStructure(2, 16, 5, wsaaComponents, 8);
		private FixedLengthStringData[][] wsaaTaxAbs = FLSDArrayPartOfArrayStructure(2, 1, wsaaComponents, 40);
		private FixedLengthStringData[][] wsaaTaxType = FLSDArrayPartOfArrayStructure(2, 2, wsaaComponents, 42);
		private FixedLengthStringData[] wsaaTaxRule = FLSDArrayPartOfArrayStructure(8, wsaaComponents, 46);
		private FixedLengthStringData[] wsaaTaxItem = FLSDArrayPartOfArrayStructure(8, wsaaComponents, 54);
		private FixedLengthStringData wsaaTranref = new FixedLengthStringData(30);
		private ZonedDecimalData wsaaTaxProp = new ZonedDecimalData(17, 2);
		private ZonedDecimalData wsaaPremNet = new ZonedDecimalData(17, 2);
		private ZonedDecimalData wsaaOrigSusp = new ZonedDecimalData(17, 2);
		private ZonedDecimalData wsaaTaxPercent = new ZonedDecimalData(8, 5);
		private ZonedDecimalData wsaaDiff = new ZonedDecimalData(7, 2);
		private FixedLengthStringData wsaaTimex = new FixedLengthStringData(8);
		private ZonedDecimalData wsaaTime = new ZonedDecimalData(6, 0).isAPartOf(wsaaTimex, 0).setUnsigned()
				.setPattern("99/99/99");

		private FixedLengthStringData wsaaDatex = new FixedLengthStringData(10);
		private ZonedDecimalData wsaaDate = new ZonedDecimalData(8, 0).isAPartOf(wsaaDatex, 0).setUnsigned()
				.setPattern("99/99/9999");
	}

	/*
	 * Class transformed from Data Structure TABLES--INNER
	 */
	private static final class TablesInner {
		/* TABLES */
		private FixedLengthStringData t1688 = new FixedLengthStringData(6).init("T1688");
		private FixedLengthStringData t3695 = new FixedLengthStringData(6).init("T3695");
		private FixedLengthStringData t5644 = new FixedLengthStringData(6).init("T5644");
		private FixedLengthStringData t5645 = new FixedLengthStringData(6).init("T5645");
		private FixedLengthStringData t5671 = new FixedLengthStringData(6).init("T5671");
		private FixedLengthStringData t5679 = new FixedLengthStringData(6).init("T5679");
		private FixedLengthStringData t5688 = new FixedLengthStringData(6).init("T5688");
		private FixedLengthStringData t6654 = new FixedLengthStringData(6).init("T6654");
		private FixedLengthStringData t5729 = new FixedLengthStringData(6).init("T5729");
		private FixedLengthStringData t3620 = new FixedLengthStringData(6).init("T3620");
		private FixedLengthStringData th605 = new FixedLengthStringData(6).init("TH605");
		private FixedLengthStringData tr52d = new FixedLengthStringData(6).init("TR52D");
		private FixedLengthStringData tr52e = new FixedLengthStringData(6).init("TR52E");
		private FixedLengthStringData t5540 = new FixedLengthStringData(6).init("T5540");
	}

	/*
	 * Class transformed from Data Structure CONTROL-TOTALS--INNER
	 */
	private static final class ControlTotalsInner {
		/* CONTROL-TOTALS */
		private ZonedDecimalData ct01 = new ZonedDecimalData(2, 0).init(1).setUnsigned();
		private ZonedDecimalData ct02 = new ZonedDecimalData(2, 0).init(2).setUnsigned();
		private ZonedDecimalData ct03 = new ZonedDecimalData(2, 0).init(3).setUnsigned();
		private ZonedDecimalData ct04 = new ZonedDecimalData(2, 0).init(4).setUnsigned();
		private ZonedDecimalData ct05 = new ZonedDecimalData(2, 0).init(5).setUnsigned();
		private ZonedDecimalData ct06 = new ZonedDecimalData(2, 0).init(6).setUnsigned();
		private ZonedDecimalData ct07 = new ZonedDecimalData(2, 0).init(7).setUnsigned();
		private ZonedDecimalData ct08 = new ZonedDecimalData(2, 0).init(8).setUnsigned();
		private ZonedDecimalData ct13 = new ZonedDecimalData(2, 0).init(13).setUnsigned();
		private ZonedDecimalData ct14 = new ZonedDecimalData(2, 0).init(14).setUnsigned();
		private ZonedDecimalData ct15 = new ZonedDecimalData(2, 0).init(15).setUnsigned();
	}

	/*
	 * Class transformed from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner {
		/* FORMATS */
		private FixedLengthStringData itemrec = new FixedLengthStringData(7).init("ITEMREC");
		private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
		private FixedLengthStringData linsrnlrec = new FixedLengthStringData(10).init("LINSRNLREC");
		private FixedLengthStringData acblrec = new FixedLengthStringData(10).init("ACBLREC");
		private FixedLengthStringData agcmbchrec = new FixedLengthStringData(10).init("AGCMBCHREC");
		private FixedLengthStringData covrlnbrec = new FixedLengthStringData(10).init("COVRLNBREC");
		private FixedLengthStringData ptrnrec = new FixedLengthStringData(7).init("PTRNREC");
		private FixedLengthStringData payrrec = new FixedLengthStringData(7).init("PAYRREC");
		private FixedLengthStringData fprmrec = new FixedLengthStringData(7).init("FPRMREC");
		private FixedLengthStringData fpcorec = new FixedLengthStringData(7).init("FPCOREC");
		private FixedLengthStringData dddelf2rec = new FixedLengthStringData(10).init("DDDELF2REC");
		private FixedLengthStringData freprec = new FixedLengthStringData(10).init("FREPREC");
		private FixedLengthStringData acagrnlrec = new FixedLengthStringData(10).init("ACAGRNLREC");
		private FixedLengthStringData taxdrec = new FixedLengthStringData(10).init("TAXDREC");
		private FixedLengthStringData dacmrec = new FixedLengthStringData(10).init("DACMREC");
	}

	private static final class WsaaT6654ArrayInner {

		/*
		 * WSAA-T6654-ARRAY 03 WSAA-T6654-REC OCCURS 100
		 */
		private FixedLengthStringData[] wsaaT6654Rec = FLSInittedArray(500, 52);
		private FixedLengthStringData[] wsaaT6654Key = FLSDArrayPartOfArrayStructure(4, wsaaT6654Rec, 0, HIVALUES);
		private FixedLengthStringData[] wsaaT6654Billchnl = FLSDArrayPartOfArrayStructure(1, wsaaT6654Key, 0);
		private FixedLengthStringData[] wsaaT6654Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaT6654Key, 1);
		private FixedLengthStringData[] wsaaT6654Data = FLSDArrayPartOfArrayStructure(48, wsaaT6654Rec, 4);
		private ZonedDecimalData[] wsaaT6654NonForfDays = ZDArrayPartOfArrayStructure(3, 0, wsaaT6654Data, 0);
		private FixedLengthStringData[] wsaaT6654ArrearsMethod = FLSDArrayPartOfArrayStructure(4, wsaaT6654Data, 3);
		private FixedLengthStringData[] wsaaT6654Subroutine = FLSDArrayPartOfArrayStructure(32, wsaaT6654Data, 7);
		private FixedLengthStringData[][] wsaaT6654Doctid = FLSDArrayPartOfArrayStructure(4, 8, wsaaT6654Subroutine, 0);
		private FixedLengthStringData[] wsaaT6654Expiry = FLSDArrayPartOfArrayStructure(9, wsaaT6654Data, 39);
		private ZonedDecimalData[][] wsaaT6654Daexpy = ZDArrayPartOfArrayStructure(3, 3, 0, wsaaT6654Expiry, 0);
	}

	/*
	 * Class transformed from Data Structure DRYR-DRYRPT-REC--INNER
	 */
	private static final class DryrDryrptRecInner {
		private FixedLengthStringData dryrReportName = new FixedLengthStringData(10);
		private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);
		private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

		private FixedLengthStringData r5358DataArea = new FixedLengthStringData(497).isAPartOf(dryrGenarea, 0,
				REDEFINE);
		private FixedLengthStringData r5358Btdate = new FixedLengthStringData(10).isAPartOf(r5358DataArea, 0);
		private ZonedDecimalData r5358Linstamt = new ZonedDecimalData(11, 2).isAPartOf(r5358DataArea, 10);
		private ZonedDecimalData r5358Minprem = new ZonedDecimalData(11, 2).isAPartOf(r5358DataArea, 21);
		private ZonedDecimalData r5358Susamt = new ZonedDecimalData(11, 2).isAPartOf(r5358DataArea, 32);

		private FixedLengthStringData r5358SortKey = new FixedLengthStringData(38);
		private FixedLengthStringData r5358Chdrnum = new FixedLengthStringData(8).isAPartOf(r5358SortKey, 0);
		private ZonedDecimalData r5358Payrseqno = new ZonedDecimalData(1, 0).isAPartOf(r5358SortKey, 8).setUnsigned();
		private FixedLengthStringData r5358ReportName = new FixedLengthStringData(10).init("R5358     ");

		private FixedLengthStringData r5359DataArea = new FixedLengthStringData(497).isAPartOf(dryrGenarea, 0,
				REDEFINE);
		private FixedLengthStringData r5359Btdate = new FixedLengthStringData(10).isAPartOf(r5359DataArea, 0);
		private ZonedDecimalData r5359Linstamt = new ZonedDecimalData(11, 2).isAPartOf(r5359DataArea, 10);
		private ZonedDecimalData r5359Maxprem = new ZonedDecimalData(11, 2).isAPartOf(r5359DataArea, 21);
		private ZonedDecimalData r5359Susamt = new ZonedDecimalData(11, 2).isAPartOf(r5359DataArea, 32);

		private FixedLengthStringData r5359SortKey = new FixedLengthStringData(38);
		private FixedLengthStringData r5359Chdrnum = new FixedLengthStringData(8).isAPartOf(r5359SortKey, 0);
		private ZonedDecimalData r5359Payrseqno = new ZonedDecimalData(1, 0).isAPartOf(r5359SortKey, 8).setUnsigned();
		private FixedLengthStringData r5359ReportName = new FixedLengthStringData(10).init("R5359     ");
	}

	/*
	 * Class transformed from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {

		// ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		// cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		// ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0,
				REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);//IJTI-2136
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);//IJTI-2136
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	}
}
