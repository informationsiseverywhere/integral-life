package com.csc.life.diary.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:08:46
 * Description:
 * Copybook name: COVRRRTKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrrrtkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrrrtFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData covrrrtKey = new FixedLengthStringData(256).isAPartOf(covrrrtFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrrrtChdrcoy = new FixedLengthStringData(1).isAPartOf(covrrrtKey, 0);
  	public FixedLengthStringData covrrrtChdrnum = new FixedLengthStringData(8).isAPartOf(covrrrtKey, 1);
  	public PackedDecimalData covrrrtRerateDate = new PackedDecimalData(8, 0).isAPartOf(covrrrtKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(242).isAPartOf(covrrrtKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrrrtFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrrrtFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}