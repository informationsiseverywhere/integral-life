/*
 * File: Br622.java
 * Date: 29 August 2009 22:28:30
 * Author: Quipoz Limited
 *
 * Class transformed from BR622.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.inspectReplaceFirst;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Subcoderec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.anticipatedendowment.dataaccess.dao.ZraepfDAO;
import com.csc.life.anticipatedendowment.dataaccess.model.Zraepf;
import com.csc.life.anticipatedendowment.procedures.Zrgetusr;
import com.csc.life.anticipatedendowment.recordstructures.Zrcshoprec;
import com.csc.life.anticipatedendowment.recordstructures.Zrgtusrrec;
import com.csc.life.diary.dataaccess.dao.Br622TempDAO;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.Td5h7rec;
import com.csc.life.regularprocessing.dataaccess.model.Arlxpf;
import com.csc.life.regularprocessing.procedures.Crtloan;
import com.csc.life.regularprocessing.recordstructures.Crtloanrec;
import com.csc.smart.dataaccess.dao.DescpfDAO;
import com.csc.smart.dataaccess.model.Descpf;
import com.csc.smart.procedures.Batcup;
import com.csc.smart.procedures.Contot;
import com.csc.smart.procedures.Sftlock;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 2005.
*    All rights reserved. CSC Confidential.
*
*REMARKS.
*
*                   Anticipated Endowment Release
*                   -----------------------------
* Overview
* ________
*
* This program is part of the new 'Multi-Threading Batch
* Performance' suite. It runs directly after BR621, which 'splits'
* the ZRAEPF according to the number of Ant Endowment Rel programs to run
* All references to the ZRAE are via ARLXPF - a temporary file
* holding all the ZRAE records for this program to process.
*
* BR621 will perform all the processing for the selected contracts
* which are due for ant endow pay processing. All contracts due
* for such processing are then processed to have the coupon
* released. If the payment option is not empty, the payment
* option will also be exercised.
*
*    Control totals maintained by this program are:
*
*      1. No of ARLX rec read
*      2. No of COVR invalid status
*      3. No of CHDR locked up
*      4. No of ARLX processed
*      5. No of ARLX blank payment option
*      6. No of ARLX payment made
*
* 1000-INITIALISE SECTION
* _______________________
*
*  -  Issue an override to read the correct ARLXPF for this run.
*     The CRTTMPF process has already created a file specific to
*     the run using the  ARLXPF fields and is identified  by
*     concatenating the following:-
*
*     'ARLX'
*      BPRD-SYSTEM-PARAM04
*      BSSC-SCHEDULE-NUMBER
*
*      eg ARLX2B0001,  for the first run
*         ARLX2B0002,  for the second etc.
*
*     The number of threads would have been created by the
*     CRTTMPF process given the parameters of the process
*     definition of the CRTTMPF.
*     To get the correct member of the above physical for BR621
*     to read from, concatenate :-
*
*     'THREAD'
*     BSPR-PROCESS-OCC-NUM
*
* 2000-READ SECTION
* _________________
*
* -  Read the ARLX records sequentially incrementing the control
*    total.
*
* -  If end of file move ENDP to WSSP-EDTERROR.
*
* 2500-EDIT SECTION
* _________________
*
* -  Move OK to WSSP-EDTERROR.
*
* -  Read and validate the COVR risk status and premium status
*    against those obtained from T5679. If the status is invalid
*    add 1 to control total 2 and move SPACES to WSSP-EDTERROR.
*
* - 'Soft lock' the contract, if it is to be processed.
*    If the contract is already 'locked' increment control
*    total number 3 and  move SPACES to WSSP-EDTERROR.
*
*  3000-UPDATE SECTION
*  ___________________
*
*  - Read & hold the CHDR record using logical file CHDRLIF.
*
*  - Setup the parameters to call the T6659 Subroutine and
*    update the Unit Statement Date in CHDRLIF.
*
*  - Rewrite CHDRLIF
*
*  - Unlock the contract.
*
* 4000-CLOSE SECTION
* __________________
*
*  - Close Files.
*
*  - Delete the Override function for the ARLXPF file
*
*   Error Processing:
*
*     Perform the 600-FATAL-ERROR section. The
*     SYSR-SYSERR-TYPE flag does not need to be set in this
*     program, because MAINB takes care of a system errors.
*
*****************************************************************
*
* </pre>
*/
public class Br622 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
//	private ArlxpfTableDAM arlxpf = new ArlxpfTableDAM();
//	private ArlxpfTableDAM arlxpfRec = new ArlxpfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(5).init("BR622");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/*  These fields are required by MAINB processing and should not
		  be deleted.*/
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaArlxFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaArlxFn, 0, FILLER).init("ARLX");
	private FixedLengthStringData wsaaArlxRunid = new FixedLengthStringData(2).isAPartOf(wsaaArlxFn, 4);
	private ZonedDecimalData wsaaArlxJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaArlxFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
		/* FORMATS */
	private static final String chdrlifrec = "CHDRLIFREC";
		/* ERRORS */
	private static final String ivrm = "IVRM";
	private static final String g450 = "G450";
	private static final String rrhf = "RRHF";
		/* TABLES */
	private static final String t3629 = "T3629";
	private static final String t5679 = "T5679";
	private static final String t3695 = "T3695";
	private static final String t5645 = "T5645";
	private static final String td5h7 = "TD5H7";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private PackedDecimalData wsaaPayIdx = new PackedDecimalData(5, 0);
	private PackedDecimalData wsaaCurrAmount = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaNextPaydate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaIdx = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaPayopt = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPaymmeth = new FixedLengthStringData(1);
	private static final int wsaaPayMax = 8;
	private PackedDecimalData wsaaPercent = new PackedDecimalData(5, 2);
	private ZonedDecimalData wsaaTransDate = new ZonedDecimalData(6, 0).init(0).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(14);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private ZonedDecimalData wsaaRldgLoanno = new ZonedDecimalData(2, 0).isAPartOf(wsaaRldgacct, 8).setUnsigned();

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(100);
	private FixedLengthStringData wsysSysparams = new FixedLengthStringData(100).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);//ILIFE-3955
	private Itempf itempf = null;
	private T3629rec t3629rec = new T3629rec();
	private T5679rec t5679rec = new T5679rec();
	private T5645rec t5645rec = new T5645rec();
	private Td5h7rec td5h7rec = new Td5h7rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Subcoderec subcoderec = new Subcoderec();
	private Zrcshoprec zrcshoprec = new Zrcshoprec();
	private Zrgtusrrec zrgtusrrec = new Zrgtusrrec();
	private Crtloanrec crtloanrec = new Crtloanrec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private boolean endowFlag = false;
	private static final String ACCUINST = "ACCUINST";
	private static final String BNKDCRDT = "BNKDCRDT";
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private PtrnpfDAO ptrnpfDAO =  getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ZraepfDAO zraepfDAO =  getApplicationContext().getBean("zraeDAO", ZraepfDAO.class);
	private CovrpfDAO covrpfDAO =  getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private DescpfDAO descpfDAO =  getApplicationContext().getBean("iafDescDAO", DescpfDAO.class);
	private ChdrpfDAO chdrpfDAO =  getApplicationContext().getBean("chdrDAO", ChdrpfDAO.class);
	private Br622TempDAO br622TempDAO = getApplicationContext().getBean("br622TempDAO", Br622TempDAO.class);
	private List<Itempf> t5679List = null;
	private List<Itempf> td5h7List = null;
	private List<Itempf> t3629List = null;
	private Ptrnpf ptrnpf;
	private Zraepf zraepf;
	private Covrpf covrpf;
	private Descpf descpf;
	private Chdrpf chdrpf;
	private List<Ptrnpf> ptrnBulkInsList = new ArrayList<Ptrnpf>();
	private List<Chdrpf> updateList;
	
	private int ctrCT01; 
	private int ctrCT02;
	private int ctrCT03; 
	private int ctrCT04;
	private int ctrCT05; 
	private int ctrCT06;
	private String coy;
	
	
	private Map<String, List<Itempf>> t3629ListMap = new HashMap<String, List<Itempf>>();
	private Map<String, List<Itempf>> td5h7ListMap = new HashMap<String, List<Itempf>>();
	private List<Arlxpf> pfList = new ArrayList<Arlxpf>();
	
	private int batchExtractSize;
	private int batchID;
	private Iterator<Arlxpf> iter;
	private Arlxpf arlxpfRec = new Arlxpf();
	private Boolean isBatchSkipFeatureEnabled;//IBPTE-74
	private Zraepf zraepfInsert = null;

	public Br622() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/** Restarting of this program is handled by MAINB,*/
		/** using a restart method of '3'.*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
		//IBPTE-74 starts
		isBatchSkipFeatureEnabled = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "BTPRO025", appVars,
				"IT");
		if(isBatchSkipFeatureEnabled) {
			setSkippedEntities(getAlreadySkippedEntities("CH"));
		}
		//IBPTE-74 ends
	}

protected void initialise1010()
	{
		if (isNE(bprdIO.getRestartMethod(), "3")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*  Point to correct member of ARLXPF.*/
		wsaaTransDate.set(getCobolDate());
		varcom.vrcmTranid.set(batcdorrec.tranid);
		wsaaArlxRunid.set(bprdIO.getSystemParam04());
		wsaaArlxJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		
		if (bprdIO.systemParam01.isNumeric()) {
			if (bprdIO.systemParam01.toInt() > 0) {
				batchExtractSize = bprdIO.systemParam01.toInt();
			} else {
				batchExtractSize = bprdIO.cyclesPerCommit.toInt();
			}
		} else {
			batchExtractSize = bprdIO.cyclesPerCommit.toInt();
		}
		
//		StringUtil stringVariable1 = new StringUtil();
		
		readChunkRecord();
//		stringVariable1.addExpression("OVRDBF FILE(ARLXPF) TOFILE(");
//		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
//		stringVariable1.addExpression("/");
//		stringVariable1.addExpression(wsaaArlxFn);
//		stringVariable1.addExpression(") ");
//		stringVariable1.addExpression("MBR(");
//		stringVariable1.addExpression(wsaaThreadMember);
//		stringVariable1.addExpression(")");
//		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
//		stringVariable1.setStringInto(wsaaQcmdexc);
//		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
//		arlxpf.openInput();
		/* Read T5679 for valid statii.*/
		coy = bsprIO.getCompany().toString();
		
		td5h7ListMap = itemDAO.loadSmartTable("IT", coy, td5h7);
		t3629ListMap = itemDAO.loadSmartTable("IT", coy, t3629);
	
		readT5679();
		
		/* Read T5679 for valid statii.*/
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemcoy(bsprIO.getCompany().toString());
		itempf.setItemtabl(t5645);
		itempf.setItemitem(wsaaProg.toString());
		
		itempf = itemDAO.getItemRecordByItemkey(itempf);
		if (itempf==null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(t5645.concat(wsaaProg.toString()));
			fatalError600();
		}
		
		t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		endowFlag = FeaConfg.isFeatureExist(bsprIO.getCompany().toString(), "PRPRO001", appVars, "IT");
		
		ctrCT01 = 0;
		ctrCT02 = 0;
		ctrCT03 = 0;
		ctrCT04 = 0;
		ctrCT05 = 0;
		ctrCT06 = 0;
	}

private void readChunkRecord() {
	 pfList = this.br622TempDAO.findResults(wsaaArlxFn.toString(), wsaaThreadMember.toString(),
			batchExtractSize, batchID);
	if (null == pfList || pfList.isEmpty()) {
		wsspEdterror.set(Varcom.endp);
		return;
	}
	this.iter = pfList.iterator();




}

protected void readT5679() {

	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(bsprIO.getCompany().toString());
	itempf.setItemtabl(t5679);
	itempf.setItemitem(bprdIO.getAuthCode().toString().trim());
	itempf = itemDAO.getItemRecordByItemkey(itempf);
	if (itempf==null) {
		syserrrec.statuz.set("MRNF");
		syserrrec.params.set(t5679.concat(bprdIO.getAuthCode().toString()));
		fatalError600();
	}
	t5679rec.t5679Rec.set(StringUtil.rawToString(itempf.getGenarea()));
	
}

protected void readFile2000()
	{
		/*READ-FILE*/
		if (!iter.hasNext()) {
			batchID++;
			readChunkRecord();
			if (wsspEdterror.equals(Varcom.endp)) {
				return;
			}
		}
		this.arlxpfRec = iter.next();
		/* No of ARLX records read*/
		ctrCT01++;
		/*EXIT*/
	}

protected void edit2500()
	{
		read2510();
	}

protected void read2510()
	{
		wsspEdterror.set(varcom.oK);
		validateCovr2550();
		/*  No of COVRs with invalid statii*/
		if (!validCoverage.isTrue()) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
			wsspEdterror.set(SPACES);
			return ;
		}
		
		zraepf = zraepfDAO.getZraepf(arlxpfRec.getChdrcoy(), arlxpfRec.getChdrnum(), arlxpfRec.getLife(), arlxpfRec.getCoverage(), arlxpfRec.getRider(),
				arlxpfRec.getPlanSuffix());
		
		if (zraepf==null) {
			syserrrec.params.set(arlxpfRec.getChdrcoy().concat(arlxpfRec.getChdrnum()));
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
		softlock2580();
		boolean isAlreadyAddedSkippedEntity = false;//IBPTE-74
		if (isEQ(sftlockrec.statuz, "LOCK")) {
			/*  No of CHDRs locked*/
			ctrCT03++;
			wsspEdterror.set(SPACES);
			// IBPTE-74 starts
			// store skipped entity
			if (isBatchSkipFeatureEnabled) {
				addSkippedEntity("CH", arlxpfRec.getChdrnum());
				isAlreadyAddedSkippedEntity = true;
			}
			// IBPTE-74 ends
		}
		
		// IBPTE-74 starts
		// If entity was skipped by previous batch due to soft lock
     	// and by the time this batch is run and entity was released from soft lock, 
     	// this batch should also skip this.
		if (isBatchSkipFeatureEnabled && isEntitySkippedBefore(arlxpfRec.getChdrnum())) {
			wsspEdterror.set(SPACES);
			if (!isAlreadyAddedSkippedEntity) {
				addSkippedEntity("CH", arlxpfRec.getChdrnum());
			}
		}
		// IBPTE-74 ends
	}



protected void validateCovr2550()
	{
		start2550();
	}

protected void start2550()
	{
	    covrpf = new Covrpf();
		covrpf.setChdrcoy(arlxpfRec.getChdrcoy());
		covrpf.setChdrnum(arlxpfRec.getChdrnum());
		covrpf.setLife(arlxpfRec.getLife());
		covrpf.setCoverage(arlxpfRec.getCoverage());
		covrpf.setRider(arlxpfRec.getRider());
		covrpf.setPlanSuffix(0);
		covrpf.setValidflag("1");
		List<Covrpf> list = covrpfDAO.selectCovrRecord(covrpf);
		if (list==null|| list.size()<=0) {
			syserrrec.params.set(arlxpfRec.getChdrnum().concat(arlxpfRec.getLife()).concat(arlxpfRec.getCoverage()).concat(arlxpfRec.getRider()));
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
		covrpf = list.get(0);
		/* Check to see if coverage is of a valid status*/
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrpf.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrpf.getPstatcode())) {
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
	}

protected void softlock2580()
	{
		start2580();
	}

protected void start2580()
	{
		/* Soft lock the contract, if it is to be processed.*/
		sftlockrec.function.set("LOCK");
		sftlockrec.company.set(arlxpfRec.getChdrcoy());
		sftlockrec.enttyp.set("CH");
		sftlockrec.entity.set(arlxpfRec.getChdrnum());
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.user.set(999999);
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)
		&& isNE(sftlockrec.statuz, "LOCK")) {
			wsysSysparams.set(sftlockrec.sftlockRec);
			syserrrec.params.set(wsysSystemErrorParams);
			syserrrec.statuz.set(sftlockrec.statuz);
			fatalError600();
		}
	}

protected void update3000()
	{
		update3010();
		unlock3080();
	}

protected void update3010()
	{
		readhChdrlif3100();
		calculatePaymentDue3200();
		updateContractHeader3300();
		writePtrnBatcup3400();
		if(!endowFlag && isNE(zraepf.getZrpayopt(wsaaIdx.toInt()), ACCUINST)  && isNE(zraepf.getZrpayopt(wsaaIdx.toInt()), BNKDCRDT)){
		createLoanRec3500();
		}
		updateZraeRec3600();
		/* No of Payment Release Record processed*/
		ctrCT04++;
		/* Check the Cash Option attached to this Payments Due.*/
		/* If there is one then call that Cash Option sub-routine,*/
		/* otherwise skip this section.*/
		if (isEQ(wsaaPayopt, SPACES)) {
			/* No of Release Payment rec with empty option*/
			contotrec.totno.set(ct05);
			contotrec.totval.set(1);
			callContot001();
			return ;
		}
		getBankcodeDetails3700();
		getContractDetails3710();
		getUserNumber3720();
		/* Setup the linkage variables before calling the Cash Option*/
		/* sub-routine.*/
		zrcshoprec.rec.set(SPACES);
		zrcshoprec.bankcode.set(t3629rec.bankcode);
		zrcshoprec.bankkey.set(zraepfInsert.getBankkey());
		zrcshoprec.bankacckey.set(zraepfInsert.getBankacckey());
		zrcshoprec.clntcoy.set(bsprIO.getFsuco());
		zrcshoprec.clntnum.set(zraepfInsert.getPayclt());
		zrcshoprec.paycurr.set(chdrpf.getCntcurr());
		zrcshoprec.cntcurr.set(chdrpf.getCntcurr());
		zrcshoprec.reqntype.set(wsaaPaymmeth);
		zrcshoprec.pymt.set(wsaaCurrAmount);
		zrcshoprec.tranref.set(zraepfInsert.getChdrnum());
		zrcshoprec.chdrnum.set(zraepfInsert.getChdrnum());
		zrcshoprec.chdrcoy.set(zraepfInsert.getChdrcoy());
		zrcshoprec.tranno.set(zraepfInsert.getTranno());
		zrcshoprec.effdate.set(bsscIO.getEffectiveDate());
		zrcshoprec.language.set(bsscIO.getLanguage());
		zrcshoprec.trandesc.set(descpf.getLongdesc());
		zrcshoprec.user.set(zrgtusrrec.usernum);
		wsaaRldgacct.set(SPACES);
		wsaaRldgChdrnum.set(zraepfInsert.getChdrnum());
		wsaaRldgLoanno.set(crtloanrec.loanno);
		zrcshoprec.frmRldgacct.set(wsaaRldgacct);
		/* Use new procedure division copybook SUBCODE to perform the*/
		/* substitution in the GLCODE field.*/
		subcoderec.codeRec.set(SPACES);
		subcoderec.code[1].set(chdrpf.getCnttype());
		/*subcoderec.glcode.set(t5645rec.glmap03);
		subcode();
		zrcshoprec.glcode.set(subcoderec.glcode);*/
		
		zrcshoprec.batcpfx.set(batcdorrec.prefix);
		zrcshoprec.batccoy.set(batcdorrec.company);
		zrcshoprec.batcbrn.set(batcdorrec.branch);
		zrcshoprec.batcactyr.set(batcdorrec.actyear);
		zrcshoprec.batcactmn.set(batcdorrec.actmonth);
		zrcshoprec.batctrcde.set(batcdorrec.trcde);	
		zrcshoprec.batcbatch.set(batcdorrec.batch);
		
		if(endowFlag){
			zrcshoprec.sacscode.set(t5645rec.sacscode01);
			zrcshoprec.sacstyp.set(t5645rec.sacstype01);
			zrcshoprec.sign.set(t5645rec.sign01);
			zrcshoprec.cnttot.set(t5645rec.cnttot01);
			subcoderec.glcode.set(t5645rec.glmap01);
			subcode();
			zrcshoprec.glcode.set(subcoderec.glcode);
			updateAcmvRec();//ICIL-749
		}
		if(endowFlag && isEQ(wsaaPayopt,ACCUINST)){
			zrcshoprec.sacscode.set(t5645rec.sacscode04);
			zrcshoprec.sacstyp.set(t5645rec.sacstype04);
			zrcshoprec.sign.set(t5645rec.sign04);
			zrcshoprec.cnttot.set(t5645rec.cnttot04);
			subcoderec.glcode.set(t5645rec.glmap04);
			subcode();
			zrcshoprec.glcode.set(subcoderec.glcode);
		} else if(endowFlag && isEQ(wsaaPayopt,BNKDCRDT)){
			zrcshoprec.sacscode.set(t5645rec.sacscode06);
			zrcshoprec.sacstyp.set(t5645rec.sacstype06);
			zrcshoprec.sign.set(t5645rec.sign06);
			zrcshoprec.cnttot.set(t5645rec.cnttot06);
			subcoderec.glcode.set(t5645rec.glmap06);
			subcode();
			zrcshoprec.glcode.set(subcoderec.glcode);
		} else{
			zrcshoprec.sacscode.set(t5645rec.sacscode03);
			zrcshoprec.sacstyp.set(t5645rec.sacstype03);
			zrcshoprec.sign.set(t5645rec.sign03);
			zrcshoprec.cnttot.set(t5645rec.cnttot03); 
			subcoderec.glcode.set(t5645rec.glmap03);
			subcode();
			zrcshoprec.glcode.set(subcoderec.glcode);
		}
		
		callProgram(wsaaPayopt, zrcshoprec.rec);
		if (isNE(zrcshoprec.statuz, varcom.oK)) {
			syserrrec.params.set(zrcshoprec.rec);
			syserrrec.statuz.set(zrcshoprec.statuz);
			fatalError600();
		}
		/* No of Release Payment rec with empty option*/
		ctrCT06++;
	}

protected void unlock3080()
	{
		unlock3800();
		/*EXIT*/
	}

protected void readhChdrlif3100()
	{
		/*START*/
		/* Read and hold the CHDRLIF record.*/
	    chdrpf = chdrpfDAO.getChdrpfByConAndNumAndServunit(arlxpfRec.getChdrcoy(), arlxpfRec.getChdrnum());
	
		if (chdrpf == null) {
			syserrrec.statuz.set("MRNF");
			syserrrec.params.set(arlxpfRec.getChdrcoy().concat(arlxpfRec.getChdrnum()));
			fatalError600();
		}
		/*EXIT*/
	}

protected void calculatePaymentDue3200()
	{
		start3210();
	}

protected void start3210()
	{
		wsaaPayIdx.set(1);
		while ( !(isEQ(zraepf.getZrduedte(wsaaPayIdx.toInt()), zraepf.getNpaydate()))) {
			wsaaPayIdx.add(1);
		}

		if (isEQ(wsaaPayIdx, wsaaPayMax)) {
			wsaaNextPaydate.set(varcom.vrcmMaxDate);
			if(endowFlag){
				compute(wsaaIdx, 0).set(wsaaPayIdx);
			}
		}
		else {
			compute(wsaaIdx, 0).set(add(wsaaPayIdx, 1));
			wsaaNextPaydate.set(zraepf.getZrduedte(wsaaIdx.toInt()));
		}
		wsaaPayopt.set(zraepf.getZrpayopt(wsaaPayIdx.toInt()));
		wsaaPaymmeth.set(zraepf.getPaymmeth(wsaaPayIdx.toInt()));
		wsaaPercent.set(zraepf.getPrcnt(wsaaPayIdx.toInt()));
		compute(wsaaCurrAmount, 2).set(div(mult(wsaaPercent, covrpf.getSumins()), 100));
		/* MOVE WSAA-CURR-AMOUNT        TO ZRDP-AMOUNT-IN.              */
		/* PERFORM 5000-CALL-ROUNDING.                                  */
		/* MOVE ZRDP-AMOUNT-OUT         TO WSAA-CURR-AMOUNT.            */
		if (isNE(wsaaCurrAmount, 0)) {
			zrdecplrec.amountIn.set(wsaaCurrAmount);
			callRounding5000();
			wsaaCurrAmount.set(zrdecplrec.amountOut);
		}
	}

protected void updateContractHeader3300()
	{
		/*READ-CHDR*/
		/* Rewriting the current contract header record a new*/
		/* Transaction Number.*/
		
		chdrpf.setTranno(chdrpf.getTranno()+1);
		updateList = new ArrayList<Chdrpf>();
		updateList.add(chdrpf);
		chdrpfDAO.updateChdrTrannoByUniqueNo(updateList);
		/*EXIT*/
	}

protected void writePtrnBatcup3400()
	{
		writePtrn3400();
		updateBatch3450();
	}

protected void writePtrn3400()
	{
	ptrnpf = new Ptrnpf();
	ptrnpf.setChdrcoy(chdrpf.getChdrcoy().toString());
	ptrnpf.setChdrpfx(chdrpf.getChdrpfx());
	ptrnpf.setChdrnum(chdrpf.getChdrnum());
	ptrnpf.setTranno(chdrpf.getTranno());
	ptrnpf.setTrdt(wsaaTransDate.toInt());
	ptrnpf.setTrtm(varcom.vrcmTime.toInt());
	ptrnpf.setPtrneff(bsscIO.getEffectiveDate().toInt());
	ptrnpf.setUserT(0);
	ptrnpf.setBatccoy(batcdorrec.company.toString());
	ptrnpf.setBatcpfx(batcdorrec.prefix.toString());
	ptrnpf.setBatcbrn(batcdorrec.branch.toString());
	ptrnpf.setBatcactyr(batcdorrec.actyear.toInt());
	ptrnpf.setBatctrcde(batcdorrec.trcde.toString());
	ptrnpf.setBatcactmn(batcdorrec.actmonth.toInt());
	ptrnpf.setBatcbatch(batcdorrec.batch.toString());
	ptrnpf.setDatesub(bsscIO.getEffectiveDate().toInt());
	ptrnpf.setCrtuser(bsscIO.getUserName().toString());   //IJS-523
	ptrnBulkInsList.add(ptrnpf);
}

protected void commitPtrnBulkInsert(){	 
	 boolean isInsertPtrnPF = ptrnpfDAO.insertPtrnPF(ptrnBulkInsList);	
	if (!isInsertPtrnPF) {
		fatalError600();
	}else ptrnBulkInsList.clear();
}

protected void updateBatch3450()
	{
		batcuprec.batcupRec.set(SPACES);
		batcuprec.trancnt.set(1);
		batcuprec.etreqcnt.set(0);
		batcuprec.sub.set(0);
		batcuprec.bcnt.set(0);
		batcuprec.bval.set(0);
		batcuprec.ascnt.set(0);
		batcuprec.batcpfx.set(batcdorrec.prefix);
		batcuprec.batccoy.set(batcdorrec.company);
		batcuprec.batcbrn.set(batcdorrec.branch);
		batcuprec.batcactmn.set(batcdorrec.actmonth);
		batcuprec.batcactyr.set(batcdorrec.actyear);
		batcuprec.batctrcde.set(batcdorrec.trcde);
		batcuprec.batcbatch.set(batcdorrec.batch);
		batcuprec.function.set("WRITS");
		callProgram(Batcup.class, batcuprec.batcupRec);
		if (isNE(batcuprec.statuz, varcom.oK)) {
			syserrrec.params.set(batcuprec.batcupRec);
			syserrrec.statuz.set(batcuprec.statuz);
			fatalError600();
		}
	}

protected void createLoanRec3500()
	{
		start3510();
	}

protected void start3510()
	{
		/* Get item description from T5645.*/
	    descpf = descpfDAO.getTableDescByItem("IT", zraepf.getChdrcoy(), t5645, bsscIO.getLanguage().toString(), wsaaProg.toString());
		if (descpf==null||descpf.getLongdesc()==null) {
			descpf.setLongdesc("");
		}
		/* Set up linkage to CRTLOAN, which will write LOAN*/
		/* and ACMV records.*/
		crtloanrec.longdesc.set(descpf.getLongdesc());
		crtloanrec.chdrcoy.set(zraepf.getChdrcoy());
		crtloanrec.chdrnum.set(zraepf.getChdrnum());
		crtloanrec.cnttype.set(chdrpf.getCnttype());
		crtloanrec.loantype.set("E");
		crtloanrec.cntcurr.set(chdrpf.getCntcurr());
		crtloanrec.billcurr.set(chdrpf.getBillcurr());
		crtloanrec.tranno.set(chdrpf.getTranno());
		crtloanrec.occdate.set(chdrpf.getOccdate());
		crtloanrec.effdate.set(bsscIO.getEffectiveDate());
		crtloanrec.authCode.set(bprdIO.getAuthCode());
		crtloanrec.language.set(bsscIO.getLanguage());
		crtloanrec.outstamt.set(wsaaCurrAmount);
		crtloanrec.cbillamt.set(wsaaCurrAmount);
		crtloanrec.batchkey.set(batcdorrec.batchkey);
		crtloanrec.sacscode01.set(t5645rec.sacscode01);
		crtloanrec.sacscode02.set(t5645rec.sacscode02);
		crtloanrec.sacstyp01.set(t5645rec.sacstype01);
		crtloanrec.sacstyp02.set(t5645rec.sacstype02);
		crtloanrec.glcode01.set(t5645rec.glmap01);
		crtloanrec.glcode02.set(t5645rec.glmap02);
		crtloanrec.glsign01.set(t5645rec.sign01);
		crtloanrec.glsign02.set(t5645rec.sign02);
		callProgram(Crtloan.class, crtloanrec.crtloanRec);
		if (isNE(crtloanrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(crtloanrec.statuz);
			syserrrec.params.set(crtloanrec.crtloanRec);
			fatalError600();
		}
	}

protected void updateZraeRec3600()
	{
		rewriteOldZrae3610();
		writeNewZrae3625();
	}

protected void rewriteOldZrae3610()
	{
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(bsscIO.getEffectiveDate());
		datcon2rec.frequency.set("DY");
		datcon2rec.freqFactor.set(-1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			syserrrec.params.set(datcon2rec.datcon2Rec);
			syserrrec.statuz.set(datcon2rec.statuz);
			fatalError600();
		}

		int result = zraepfDAO.updateZraepf(datcon2rec.intDate2.toInt(), "2", zraepf.getUnique_number());
		if (result<=0) {
			syserrrec.params.set(zraepf.getChdrcoy().concat(zraepf.getChdrnum()));
			syserrrec.statuz.set("MRNF");
			fatalError600();
		}
	}

protected void writeNewZrae3625()
	{
		zraepfInsert = new Zraepf(zraepf);
		zraepfInsert.setNpaydate(wsaaNextPaydate.toInt());
		zraepfInsert.setPaydte(wsaaPayIdx.toInt(), bsscIO.getEffectiveDate().toInt());
		zraepfInsert.setPaid(wsaaPayIdx.toInt(), new BigDecimal(wsaaCurrAmount.toDouble()));
		zraepfInsert.setTotamnt(zraepfInsert.getTotamnt().add(new BigDecimal(wsaaCurrAmount.toDouble())));
		zraepfInsert.setTranno(chdrpf.getTranno());
		zraepfInsert.setCurrfrom(bsscIO.getEffectiveDate().toInt());
		zraepfInsert.setCurrto(99999999);
		zraepfInsert.setValidflag("1");
		if(endowFlag){
		  updateZrae();
		 }
		zraepfDAO.insertZraeRecord(zraepfInsert);
		
	}

public  Integer getCurrentBusinessDate() {
	final Datcon1rec datcon1rec = new Datcon1rec();
	datcon1rec.function.set(varcom.tday);
	Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	return datcon1rec.intDate.toInt();
   }

protected void readTd5h7()
{
    boolean flag = false;
	td5h7List = td5h7ListMap.get(chdrpf.getCnttype().trim());
	if (td5h7List != null && td5h7List.size() > 0) {
	 for(Itempf itempf : td5h7List) {
		if(itempf.getItmfrm()==null||itempf.getItmto()==null) {
			continue;
		}
		if(itempf.getItmfrm().compareTo(new BigDecimal(getCurrentBusinessDate()))<=0 
				&& itempf.getItmto().compareTo(new BigDecimal(getCurrentBusinessDate()))>=0) {
			td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			flag =true;
		}		
		
	 }
	}
	
	
	if(!flag) {
		td5h7List = td5h7ListMap.get("***");
		if (td5h7List == null || td5h7List.size() == 0) {
			syserrrec.params.set("TD5H7" + bprdIO.getSystemParam01());
			fatalError600();
		}
	    for(Itempf itempf : td5h7List) {
			if(itempf.getItmfrm()==null||itempf.getItmto()==null) {
			  syserrrec.params.set("TD5H7" + bprdIO.getSystemParam01());
			  fatalError600();
			 }
			if(itempf.getItmfrm().compareTo(new BigDecimal(getCurrentBusinessDate()))<=0 
						&& itempf.getItmto().compareTo(new BigDecimal(getCurrentBusinessDate()))>=0) {
				td5h7rec.td5h7Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				}		
				
			 }
	}
}

public void updateZrae(){
	
		if( isEQ(zraepfInsert.getZrpayopt(wsaaPayIdx.toInt()), "ACCUINST")  && isNE(zraepfInsert.getFlag(),"2")){
			zraepfInsert.setFlag("1");
			zraepfInsert.setApcaplamt(zraepfInsert.getTotamnt());
			zraepfInsert.setApintamt(BigDecimal.ZERO);
			readTd5h7();
			zraepfInsert.setAplstcapdate(getCurrentBusinessDate());
			zraepfInsert.setAplstintbdte(getCurrentBusinessDate());
			if(isEQ(zraepfInsert.getAplstcapdate().toString(),"       0")){
				datcon2rec.intDate1.set(getCurrentBusinessDate());	
			}
			else
			{
				datcon2rec.intDate1.set(zraepfInsert.getAplstcapdate());
			}
			if (isNE(td5h7rec.intcapfreq,SPACES)) {
				datcon2rec.frequency.set(td5h7rec.intcapfreq);
				datcon2rec.freqFactor.set("1");
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					fatalError600();
				}
				zraepfInsert.setApnxtcapdate(datcon2rec.intDate2.toInt());
				
			}
			else {
				zraepfInsert.setApnxtcapdate(varcom.vrcmMaxDate.toInt());
			}
			datcon2rec.intDate1.set(zraepfInsert.getAplstintbdte());
			if (isNE(td5h7rec.intcalfreq,SPACES)) {
				datcon2rec.frequency.set(td5h7rec.intcalfreq);
				datcon2rec.freqFactor.set("1");
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				if (isNE(datcon2rec.statuz,varcom.oK)) {
					syserrrec.params.set(datcon2rec.datcon2Rec);
					syserrrec.statuz.set(datcon2rec.statuz);
					fatalError600();
				}
				zraepfInsert.setApnxtintbdte(datcon2rec.intDate2.toInt());
			}
			else {
				zraepfInsert.setApnxtintbdte(varcom.vrcmMaxDate.toInt());
			 }
		}
	}

protected void getBankcodeDetails3700()
	{
		start3701();
	}

protected void start3701()
	{
		/* Read table T3629 in order to obtain the bankcode*/
		t3629List = t3629ListMap.get(zraepf.getPaycurr().trim());
		if (t3629List == null || t3629List.size() == 0) {
			syserrrec.params.set("t3629" + zraepf.getPaycurr());
			fatalError600();
		}

		t3629rec.t3629Rec.set(StringUtil.rawToString(t3629List.get(0).getGenarea()));
	}

protected void getContractDetails3710()
	{
		start3711();
	}

protected void start3711()
	{
		/* Ensure that the sacscode,type, GL account, sign and control*/
		/* all exist on T5645.*/
		if (isEQ(t5645rec.sacscode[1], SPACES)
		|| isEQ(t5645rec.sacstype[1], SPACES)
		|| isEQ(t5645rec.glmap[1], SPACES)
		|| isEQ(t5645rec.sign[1], SPACES)
		|| isEQ(t5645rec.cnttot[1], ZERO)) {
			itempf = new Itempf();
			itempf.setItempfx("IT");
			itempf.setItemcoy(bsprIO.getCompany().toString());
			itempf.setItemtabl(t5645);
			itempf.setItemitem(wsaaProg.toString());
			syserrrec.params.set(t5645.concat(wsaaProg.toString()));
			syserrrec.statuz.set(g450);
			fatalError600();
		}
		/* Read T3695 to obtain the 'From' transaction description.*/

		  descpf = descpfDAO.getTableDescByItem("IT", zraepf.getChdrcoy(), t3695, bsscIO.getLanguage().toString(), t5645rec.sacstype01.toString());
		  if (descpf==null||descpf.getLongdesc()==null) {			
			syserrrec.params.set(t3695.concat(t5645rec.sacstype01.toString()));
			fatalError600();
		}
	}

protected void getUserNumber3720()
	{
		/*START*/
		/* Using ZRGETUSR sub-routine, get the User Number which is*/
		/* not available from the new batch programs.*/
		zrgtusrrec.rec.set(SPACES);
		zrgtusrrec.userid.set(bsscIO.getUserName());
		zrgtusrrec.function.set("USID");
		zrgtusrrec.usernum.set(0);
		callProgram(Zrgetusr.class, zrgtusrrec.rec);
		if (isNE(zrgtusrrec.statuz, varcom.oK)) {
			syserrrec.params.set(zrgtusrrec.rec);
			syserrrec.statuz.set(zrgtusrrec.statuz);
			fatalError600();
		}
		/*EXIT*/
	}

protected void unlock3800()
	{
		para3810();
	}

protected void para3810()
	{
		/* Undone soft lock.*/
		sftlockrec.sftlockRec.set(SPACES);
		sftlockrec.company.set(arlxpfRec.getChdrcoy());
		sftlockrec.entity.set(arlxpfRec.getChdrnum());
		sftlockrec.enttyp.set("CH");
		sftlockrec.user.set(varcom.vrcmUser);
		sftlockrec.transaction.set(bprdIO.getAuthCode());
		sftlockrec.statuz.set(SPACES);
		sftlockrec.function.set("UNLK");
		callProgram(Sftlock.class, sftlockrec.sftlockRec);
		if (isNE(sftlockrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(sftlockrec.statuz);
			syserrrec.params.set(sftlockrec.sftlockRec);
			fatalError600();
		}
	}

protected void commit3500()
	{
	 commitPtrnBulkInsert();
	 commitControlTotals();
	 
	}

protected void commitControlTotals(){
	//ct01
	contotrec.totno.set(ct01);
	contotrec.totval.set(ctrCT01);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT01 = 0;

	//ct02
	contotrec.totno.set(ct02);
	contotrec.totval.set(ctrCT02);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT02 = 0;
	
	//ct03
	contotrec.totno.set(ct03);
	contotrec.totval.set(ctrCT03);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT03 = 0;

	//ct04
	contotrec.totno.set(ct04);
	contotrec.totval.set(ctrCT04);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT04 = 0;
	
	//ct05
	contotrec.totno.set(ct05);
	contotrec.totval.set(ctrCT05);
	callProgram(Contot.class, contotrec.contotRec);		
	ctrCT05 = 0;

	//ct06
	contotrec.totno.set(ct06);
	contotrec.totval.set(ctrCT06);
	callProgram(Contot.class, contotrec.contotRec);	
	ctrCT06 = 0;
		
}

protected void rollback3600()
	{
		/*ROLLBACK*/
		/** Place any additional rollback processing in here.*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}

protected void callRounding5000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.company.set(bsprIO.getCompany());
		zrdecplrec.currency.set(chdrpf.getCntcurr());
		zrdecplrec.batctrcde.set(batcdorrec.trcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			syserrrec.statuz.set(zrdecplrec.statuz);
			syserrrec.params.set(zrdecplrec.zrdecplRec);
			fatalError600();
		}
		/*EXIT*/
	}

protected void subcode()
	{
		sbcd100Start();
	}

protected void sbcd100Start()
	{
		if (isNE(subcoderec.code[1], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[1]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "@", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[2], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[2]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "*", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[3], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[3]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "%", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[4], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[4]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "!", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[5], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[5]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "+", subcoderec.subscode04));
		}
		if (isNE(subcoderec.code[6], SPACES)) {
			subcoderec.substituteCode.set(subcoderec.code[6]);
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode01));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode02));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode03));
			subcoderec.glcode.set(inspectReplaceFirst(subcoderec.glcode, "=", subcoderec.subscode04));
		}
	}
protected void updateAcmvRec(){
	zrcshoprec.statuz.set(varcom.oK);
	lifacmvrec.lifacmvRec.set(SPACES);
	lifacmvrec.batccoy.set(zrcshoprec.batccoy);
	lifacmvrec.batcbrn.set(zrcshoprec.batcbrn);
	lifacmvrec.batcactyr.set(zrcshoprec.batcactyr);
	lifacmvrec.batcactmn.set(zrcshoprec.batcactmn);
	lifacmvrec.batctrcde.set(zrcshoprec.batctrcde);
	lifacmvrec.batcbatch.set(zrcshoprec.batcbatch);
	lifacmvrec.rldgcoy.set(zrcshoprec.chdrcoy);
	lifacmvrec.genlcoy.set(zrcshoprec.chdrcoy);
	lifacmvrec.rdocnum.set(zrcshoprec.chdrnum);
	lifacmvrec.genlcur.set(SPACES);
	lifacmvrec.origcurr.set(zrcshoprec.cntcurr);
	lifacmvrec.origamt.set(zrcshoprec.pymt);
	lifacmvrec.jrnseq.set(0);
	lifacmvrec.rcamt.set(0);
	lifacmvrec.crate.set(0);
	lifacmvrec.acctamt.set(0);
	lifacmvrec.contot.set(0);
	lifacmvrec.tranno.set(zrcshoprec.tranno);
	lifacmvrec.frcdate.set(99999999);
	lifacmvrec.effdate.set(zrcshoprec.effdate);
	lifacmvrec.tranref.set(zrcshoprec.chdrnum);
	lifacmvrec.trandesc.set(zrcshoprec.trandesc);
	lifacmvrec.rldgacct.set(zrcshoprec.chdrnum);
	lifacmvrec.termid.set(zrcshoprec.termid);
	lifacmvrec.transactionDate.set(wsaaTransDate);
	lifacmvrec.transactionTime.set(varcom.vrcmTime);
	lifacmvrec.user.set(zrcshoprec.user);
	lifacmvrec.sacscode.set(zrcshoprec.sacscode);
	lifacmvrec.sacstyp.set(zrcshoprec.sacstyp);
	lifacmvrec.glcode.set(zrcshoprec.glcode);
	lifacmvrec.glsign.set(zrcshoprec.sign);
	lifacmvrec.function.set("PSTW");
	callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
	if (isNE(lifacmvrec.statuz, varcom.oK)) {
		zrcshoprec.errorFormat.set(lifacmvrec.lifacmvRec);
		zrcshoprec.statuz.set(lifacmvrec.statuz);
		fatalError600();
	}

}
}
