/*
 * File: Dryagpr.java
 * Date: May 11, 2015 12:40:15 PM IST
 * Author: CSC
 * 
 * Class transformed from DRYAGPR.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import com.quipoz.framework.datatype.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import com.csc.life.agents.dataaccess.MapreffTableDAM;
import com.csc.life.statistics.dataaccess.AgprTableDAM;

import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the AGPR Accumulation subroutine.
*
* It will update all MAPREFF records.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryagpr extends SMARTCodeModel {

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(6).init("DRYAGPR");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
		/* FORMATS */
	private String agprrec = "AGPRREC";
	private String mapreffrec = "MAPREFFREC";

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaDataArea = new FixedLengthStringData(1000);
	
	private AgprTableDAM agprIO = new AgprTableDAM();
	private MapreffTableDAM mapreffIO = new MapreffTableDAM();
	private Varcom varcom = new Varcom();

	public Dryagpr() {
		super();
	}

	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
public void mainline(Object... parmArray)
	{
		lsaaDataArea = convertAndSetParam(lsaaDataArea, parmArray, 2);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			mainLine100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine100()
	{
		main110();
		exit190();
	}

protected void main110()
	{
		lsaaStatuz.set(varcom.oK);
		agprIO.setDataArea(lsaaDataArea);
		
		mapreffIO.setParams(SPACES);
		mapreffIO.setAgntcoy(agprIO.getAgntcoy());
		mapreffIO.setAgntnum(agprIO.getAgntnum());
		mapreffIO.setEffdate(agprIO.getEffdate());
		mapreffIO.setAcctyr(agprIO.getAcctyr());
		mapreffIO.setMnth(agprIO.getMnth());
		mapreffIO.setCnttype(agprIO.getCnttype());
		
		mapreffIO.setFormat(mapreffrec);
		mapreffIO.setFunction(varcom.readh);
		
		SmartFileCode.execute(appVars, mapreffIO);

		if (isNE(mapreffIO.getStatuz(), varcom.oK)
		&& isNE(mapreffIO.getStatuz(), varcom.mrnf)) {
			lsaaStatuz.set(mapreffIO.getStatuz());
			exit190();
		}
		
		if (isEQ(mapreffIO.getStatuz(), varcom.mrnf)){
			for (wsaaI.set(1); !(isGT(wsaaI,10)); wsaaI.add(1)){
				mapreffIO.setMlperpc(wsaaI, ZERO);
				mapreffIO.setMlperpp(wsaaI, ZERO);
				mapreffIO.setMlgrppc(wsaaI, ZERO);
				mapreffIO.setMlgrppp(wsaaI, ZERO);
				mapreffIO.setMldirpc(wsaaI, ZERO);
				mapreffIO.setMldirpp(wsaaI, ZERO);
			}
			mapreffIO.setCntcount(ZERO);
			mapreffIO.setSumins(ZERO);
		}
		
		for (wsaaI.set(1); !(isGT(wsaaI,10)); wsaaI.add(1)){
			mapreffIO.setMlperpc(wsaaI, agprIO.getMlperpc(wsaaI));
			mapreffIO.setMlperpp(wsaaI, agprIO.getMlperpp(wsaaI));
			mapreffIO.setMlgrppc(wsaaI, agprIO.getMlgrppc(wsaaI));
			mapreffIO.setMlgrppp(wsaaI, agprIO.getMlgrppp(wsaaI));
			mapreffIO.setMldirpc(wsaaI, agprIO.getMldirpc(wsaaI));
			mapreffIO.setMldirpp(wsaaI, agprIO.getMldirpp(wsaaI));

		}
		
		mapreffIO.setCntcount(agprIO.getCntcount());
		mapreffIO.setSumins(agprIO.getSumins());
		
		if (isEQ(mapreffIO.getStatuz(), varcom.oK)) {
			mapreffIO.setFunction(varcom.rewrt);
		}else {
			mapreffIO.setFunction(varcom.writr);
		}
		
		SmartFileCode.execute(appVars, mapreffIO);

		if (isNE(mapreffIO.getStatuz(), varcom.oK)) {
			lsaaStatuz.set(mapreffIO.getStatuz());
			exit190();
		}
		
	}

protected void exit190()
	{
		exitProgram();
	}
}