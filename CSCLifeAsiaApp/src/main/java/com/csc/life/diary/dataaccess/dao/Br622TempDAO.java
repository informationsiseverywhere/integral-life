package com.csc.life.diary.dataaccess.dao;


import java.util.List;

import com.csc.life.regularprocessing.dataaccess.model.Arlxpf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface Br622TempDAO extends BaseDAO<Object>{
    public List<Arlxpf> findResults(String tableId, String memName, int batchExtractSize, int batchID);
}
