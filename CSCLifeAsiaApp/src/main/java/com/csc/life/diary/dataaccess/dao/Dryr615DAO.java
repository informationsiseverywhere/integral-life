package com.csc.life.diary.dataaccess.dao;

import java.util.List;
import com.csc.life.diary.dataaccess.model.Dryr615Dto;

/**
 * DAO to fetch records for Re-rate for Wavier of Premium.
 * 
 * @author hmahajan6
 *
 */
public interface Dryr615DAO {
	/**
	 * Gets records for Re-rate for Wavier of Premium.
	 * 
	 * @param company
	 *            - Company number
	 * @param chdrNum
	 *            - Contract number
	 * @param sqlDate
	 *            - Effective date
	 * @return List of Dryr615Dto
	 */
	public List<Dryr615Dto> getRerateWavierPremiumDetails(String company, String chdrNum, String sqlDate);
}
