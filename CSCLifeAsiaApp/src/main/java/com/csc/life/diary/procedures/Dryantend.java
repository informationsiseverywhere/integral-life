/*
 * File: Dryantend.java
 * Date: December 3, 2013 2:23:48 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYANTEND.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import java.util.List;

import com.csc.diaryframework.parent.Maind;
import com.csc.life.diary.dataaccess.ZraedteTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
* This is the transaction detail record change subroutine for
* Regular Payment Review.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryantend extends Maind {//Modify for ILPI-65

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYANTEND");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaZraeStatuz = new FixedLengthStringData(1).init("N");
	private Validator validZrae = new Validator(wsaaZraeStatuz, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();
		/* FORMATS */
	private static final String covrlnbrec = "COVRLNBREC";
	private static final String zraedterec = "ZRAEDTEREC";
		/* TABLES */
	private static final String t5679 = "T5679";
	private ZraedteTableDAM zraedteIO = new ZraedteTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65
	private T5679rec t5679rec = new T5679rec();
	
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr180, 
		exit190
	}

	public Dryantend() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdNo.setTrue();
		wsaaZraeStatuz.set("N");
		/* Read the ZRAEDTE logical to get the earliest Next Payment*/
		/* Date for the contract details passed in linkage.*/
		zraedteIO.setParams(SPACES);
		zraedteIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		zraedteIO.setChdrnum(drypDryprcRecInner.drypEntity);
		zraedteIO.setNextPaydate(ZERO);
		zraedteIO.setFormat(zraedterec);
		zraedteIO.setFunction(varcom.begn);
		zraedteIO.setStatuz(varcom.oK);
		while ( !(isEQ(zraedteIO.getStatuz(), varcom.endp)
		|| validZrae.isTrue())) {
			readZrae100();
		}
		
		if (validZrae.isTrue()) {
			drypDryprcRecInner.dtrdYes.setTrue();
			drypDryprcRecInner.drypNxtprcdate.set(zraedteIO.getNextPaydate());
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		}
		else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readZrae100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read110();
				case nextr180: 
					nextr180();
				case exit190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read110()
	{
		SmartFileCode.execute(appVars, zraedteIO);
		if (isNE(zraedteIO.getStatuz(), varcom.oK)
		&& isNE(zraedteIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(zraedteIO.getParams());
			drylogrec.statuz.set(zraedteIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isNE(zraedteIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(zraedteIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isEQ(zraedteIO.getStatuz(), varcom.endp)) {
			zraedteIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit190);
		}
		if (isEQ(zraedteIO.getNextPaydate(), varcom.vrcmMaxDate)
		|| isEQ(zraedteIO.getNextPaydate(), ZERO)) {
			goTo(GotoLabel.nextr180);
		}
		validateCoverage400();
		if (!validCoverage.isTrue()) {
			goTo(GotoLabel.nextr180);
		}
		validZrae.setTrue();
	}

protected void nextr180()
	{
		zraedteIO.setFunction(varcom.nextr);
	}

protected void validateCoverage400()
	{
		start410();
	}

protected void start410()
	{
		/* Read the coverage record and validate the status against*/
		/* those on T5679.*/
		Itempf itemIO = new Itempf();
	    itemIO.setItempfx(smtpfxcpy.item.toString());
	    itemIO.setItemcoy(drypDryprcRecInner.drypCompany.toString());
    	itemIO.setItemtabl(t5679);
	    itemIO.setItemitem(drypDryprcRecInner.drypSystParm[1].toString());
	    itemIO = itemDAO.getItempfRecord(itemIO);
	    if(itemIO==null) {
	    	drylogrec.statuz.set("MRNF");
			drylogrec.params.set(drypDryprcRecInner.drypCompany.toString().concat(t5679).concat(drypDryprcRecInner.drypSystParm[1].toString()));
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
	    }
	    
		t5679rec.t5679Rec.set(StringUtil.rawToString(itemIO.getGenarea()));
		/* Read in the relevant COVR record.*/
		Covrpf covrlnbIO = new Covrpf();
		covrlnbIO.setChdrcoy(zraedteIO.getChdrcoy().toString());
		covrlnbIO.setChdrnum(zraedteIO.getChdrnum().toString());
		covrlnbIO.setLife(zraedteIO.getLife().toString());
		covrlnbIO.setCoverage(zraedteIO.getCoverage().toString());
		covrlnbIO.setRider(zraedteIO.getRider().toString());
		covrlnbIO.setPlanSuffix(0);
		List<Covrpf> list = covrpfDAO.selectCovrRecordBegin(covrlnbIO);
		if (list == null || list.isEmpty()) {
			drylogrec.params.set(zraedteIO.getChdrcoy().concat(zraedteIO.getChdrnum()));
			drylogrec.statuz.set("MRNF");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		covrlnbIO = list.get(0);
		/* Check to see if coverage is of a valid status*/
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrlnbIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrlnbIO.getPstatcode())) {
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
	}
//Modify for ILPI-61
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
}
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
