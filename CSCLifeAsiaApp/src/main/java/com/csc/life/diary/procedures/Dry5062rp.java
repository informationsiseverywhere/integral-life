/*
 * File: Dry5062rp.java
 * Date: March 26, 2014 2:59:25 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5062RP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DrptsrtTableDAM;
import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.regularprocessing.reports.R5062Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*  This is the report printing program for R5062.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5062rp extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5062Report printFile = new R5062Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5062RP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8);
		/* TABLES */
	private static final String t1693 = "T1693";
		/* FORMATS */
	private static final String drptsrtrec = "DRPTSRTREC";
	private static final String descrec = "DESCREC";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5062H01Record = new FixedLengthStringData(51);
	private FixedLengthStringData r5062h01O = new FixedLengthStringData(51).isAPartOf(r5062H01Record, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10).isAPartOf(r5062h01O, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5062h01O, 10);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5062h01O, 11);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(r5062h01O, 41);

	private FixedLengthStringData r5062D01Record = new FixedLengthStringData(57);
	private FixedLengthStringData r5062d01O = new FixedLengthStringData(57).isAPartOf(r5062D01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5062d01O, 0);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(r5062d01O, 8);
	private FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(r5062d01O, 12);
	private ZonedDecimalData tranno = new ZonedDecimalData(5, 0).isAPartOf(r5062d01O, 42);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(r5062d01O, 47);

	private FixedLengthStringData r5062D02Record = new FixedLengthStringData(2);
	private DescTableDAM descIO = new DescTableDAM();
	private DrptsrtTableDAM drptsrtIO = new DrptsrtTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Dryoutrec dryoutrec = new Dryoutrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();

	public Dry5062rp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryoutrec.dryoutRec = convertAndSetParam(dryoutrec.dryoutRec, parmArray, 0);
		try {
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
		printFile.openOutput();
		wsaaOverflow.set("Y");
		wsaaPrevChdrnum.set(SPACES);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		dryoutrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		drptsrtIO.setRecKeyData(SPACES);
		drptsrtIO.setStatuz(varcom.oK);
		drptsrtIO.setEffdate(dryoutrec.effectiveDate);
		drptsrtIO.setDiaryRunNumber(dryoutrec.runNumber);
		drptsrtIO.setDiaryEntityCompany(dryoutrec.company);
		drptsrtIO.setDiaryEntityBranch(dryoutrec.branch);
		drptsrtIO.setDiaryReportName(dryoutrec.reportName);
		drptsrtIO.setSortkey(SPACES);
		drptsrtIO.setFormat(drptsrtrec);
		drptsrtIO.setFunction(varcom.begn);
		while ( !(isEQ(drptsrtIO.getStatuz(), varcom.endp))) {
			callDrptsrt200();
		}
		
		endReport500();
		printFile.close();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void callDrptsrt200()
	{
		call210();
	}

protected void call210()
	{
		SmartFileCode.execute(appVars, drptsrtIO);
		if (isNE(drptsrtIO.getStatuz(), varcom.oK)
		&& isNE(drptsrtIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(drptsrtIO.getParams());
			drylogrec.statuz.set(drptsrtIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		if (isEQ(drptsrtIO.getStatuz(), varcom.endp)
		|| isNE(drptsrtIO.getDiaryEntityCompany(), dryoutrec.company)
		|| isNE(drptsrtIO.getDiaryEntityBranch(), dryoutrec.branch)
		|| isNE(drptsrtIO.getDiaryReportName(), dryoutrec.reportName)
		|| isNE(drptsrtIO.getEffdate(), dryoutrec.effectiveDate)) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(dryoutrec.runNumber, ZERO)
		&& isNE(dryoutrec.runNumber, drptsrtIO.getDiaryRunNumber())) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(drptsrtIO.getDiaryEntity(), wsaaPrevChdrnum)) {
			pageOverflow.setTrue();
			wsaaPrevChdrnum.set(drptsrtIO.getDiaryEntity());
		}
		writeLine300();
		drptsrtIO.setFunction(varcom.nextr);
	}

protected void writeLine300()
	{
		write310();
	}

protected void write310()
	{
		/* Write New Page if required.*/
		if (pageOverflow.isTrue()) {
			wsaaOverflow.set("N");
			newPage400();
		}
		/* Fill detail record.*/
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		dryrDryrptRecInner.r5062SortKey.set(drptsrtIO.getSortkey());
		if (isEQ(dryrDryrptRecInner.r5062ErrorLine, "Y")) {
			printRecord.set(SPACES);
			printFile.printR5062d02(r5062D02Record, indicArea);
		}
		/* Skip the detail line printting if this is error line.           */
		if (isEQ(dryrDryrptRecInner.r5062ErrorLine, "Y")) {
			return ;
		}
		chdrnum.set(dryrDryrptRecInner.r5062Chdrnum);
		batctrcde.set(dryrDryrptRecInner.r5062Batctrcde);
		trandesc.set(dryrDryrptRecInner.r5062Trandesc);
		tranno.set(dryrDryrptRecInner.r5062Tranno);
		effdate.set(dryrDryrptRecInner.r5062Effdate);
		printRecord.set(SPACES);
		printFile.printR5062d01(r5062D01Record, indicArea);
	}

protected void newPage400()
	{
		new400();
	}

protected void new400()
	{
		/* Fill header record.*/
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(wsaaToday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sdate.set(datcon1rec.extDate);
		company.set(dryoutrec.company);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(dryoutrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		companynm.set(descIO.getLongdesc());
		printRecord.set(SPACES);
		printFile.printR5062h01(r5062H01Record, indicArea);
	}

protected void endReport500()
	{
		/*NEW*/
		/*EXIT*/
	}
//Modify for ILPI-65 
protected void fatalError()
	{
		/*A010-FATAL*/
		drylogrec.subrname.set(wsaaProg);
		drylogrec.entityKey.set(SPACES);
		drylogrec.runNumber.set(ZERO);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		dryoutrec.statuz.set(drylogrec.statuz);
		//callProgram(Drylog.class, drylogrec.drylogRec);
		//exitProgram();
		a000FatalError();
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r5062DataArea = new FixedLengthStringData(500).isAPartOf(dryrGenarea, 0, REDEFINE);
	private FixedLengthStringData r5062Batctrcde = new FixedLengthStringData(4).isAPartOf(r5062DataArea, 0);
	private FixedLengthStringData r5062Trandesc = new FixedLengthStringData(30).isAPartOf(r5062DataArea, 4);
	private ZonedDecimalData r5062Tranno = new ZonedDecimalData(5, 0).isAPartOf(r5062DataArea, 34).setUnsigned();
	private FixedLengthStringData r5062Effdate = new FixedLengthStringData(10).isAPartOf(r5062DataArea, 39);
	private FixedLengthStringData r5062ErrorLine = new FixedLengthStringData(1).isAPartOf(r5062DataArea, 49);

	private FixedLengthStringData r5062SortKey = new FixedLengthStringData(38);
	private FixedLengthStringData r5062Chdrnum = new FixedLengthStringData(8).isAPartOf(r5062SortKey, 0);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
