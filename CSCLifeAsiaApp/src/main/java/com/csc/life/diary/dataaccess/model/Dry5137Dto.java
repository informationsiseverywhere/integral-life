/**
 * 
 */
package com.csc.life.diary.dataaccess.model;

import java.math.BigDecimal;

/**
 * Bean for Dry5137
 * 
 * @author aashish4
 *
 */

public class Dry5137Dto {

	public Dry5137Dto() {
		// Default constructor
	}

	private String lifcnum;
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String jlife;
	private String coverage;
	private String rider;
	private Integer plnsfx;
	private int crrcd;
	private String crtable;
	private String validflag;
	private String statcode;
	private String pstatcode;
	private String annvry;
	private String bascmeth;
	private String bascpy;
	private String rnwcpy;
	private String srvcpy;
	private BigDecimal newinst;
	private String basicCommMeth;
	private BigDecimal lastInst;
	private BigDecimal newsum;
	private BigDecimal lastSum;
	private BigDecimal zllastinst;
	private BigDecimal zbnewinst;
	private BigDecimal zblastinst;
	private BigDecimal zlnewinst;
	private BigDecimal zstpduty01;
	/**
	 * Getter for lifcnum
	 * 
	 * @return The lifcnum
	 */
	public String getLifcnum() {
		return lifcnum;
	}
	/**
	 * Setter for lifcnum
	 * 
	 * @param lifcnum - The lifcnum to set
	 */
	public void setLifcnum(String lifcnum) {
		this.lifcnum = lifcnum;
	}
	/**
	 * Getter for company
	 * 
	 * @return The chdrcoy
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}
	/**
	 * Setter for company
	 * 
	 * @param chdrcoy - The chdrcoy to set
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}
	/**
	 * Getter for chdrnum
	 * 
	 * @return The Contract Number
	 */
	public String getChdrnum() {
		return chdrnum;
	}
	/**
	 * Setter for chdrnum
	 * 
	 * @param chdrnum - The Contract Number to set
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}
	/**
	 * Getter for life
	 * 
	 * @return - The life
	 */
	public String getLife() {
		return life;
	}
	/**
	 * Setter for life
	 * 
	 * @param life - The life to set
	 */
	public void setLife(String life) {
		this.life = life;
	}
	/**
	 * Getter for jlife
	 * 
	 * @return - The jlife
	 */
	public String getJlife() {
		return jlife;
	}
	/**
	 * Setter for jlife
	 * 
	 * @param jlife - The jlife to set
	 */
	public void setJlife(String jlife) {
		this.jlife = jlife;
	}
	/**
	 * Getter for coverage
	 * 
	 * @return - The coverage
	 */
	public String getCoverage() {
		return coverage;
	}
	/**
	 * Setter for coverage
	 * 
	 * @param coverage - The coverage to set
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}
	/**
	 * Getter for rider
	 * 
	 * @return - The rider
	 */
	public String getRider() {
		return rider;
	}
	/**
	 * Setter for rider
	 * 
	 * @param rider - The rider to set
	 */
	public void setRider(String rider) {
		this.rider = rider;
	}
	/**
	 * Getter for plnsfx
	 * 
	 * @return -The plnsfx
	 */
	public Integer getPlnsfx() {
		return plnsfx;
	}
	/**
	 * Setter for plnsfx
	 * 
	 * @param plnsfx -The plnsfx to set
	 */
	public void setPlnsfx(Integer plnsfx) {
		this.plnsfx = plnsfx;
	}
	/**
	 * Getter for crrcd
	 * 
	 * @return - The crrcd
	 */
	public int getCrrcd() {
		return crrcd;
	}
	/**
	 * Setter for crrcd
	 * 
	 * @param crrcd - The crrcd to set
	 */
	public void setCrrcd(int crrcd) {
		this.crrcd = crrcd;
	}
	/**
	 * Getter for crtable
	 * 
	 * @return - The crtable
	 */
	public String getCrtable() {
		return crtable;
	}
	/**
	 * Setter for crtable
	 * 
	 * @param crtable - The crtable to set
	 */
	public void setCrtable(String crtable) {
		this.crtable = crtable;
	}
	/**
	 * Getter for validflag
	 * 
	 * @return - The validflag
	 */
	public String getValidflag() {
		return validflag;
	}
	/**
	 * Setter for validflag
	 * 
	 * @param validflag - The validflag to set
	 */
	public void setValidflag(String validflag) {
		this.validflag = validflag;
	}
	/**
	 * Getter for statcode
	 * 
	 * @return - The statcode
	 */
	public String getStatcode() {
		return statcode;
	}
	/**
	 * Setter for statcode
	 * 
	 * @param statcode - The statcode to set
	 */
	public void setStatcode(String statcode) {
		this.statcode = statcode;
	}
	/**
	 * Getter for pstatcode
	 * 
	 * @return - The pstatcode
	 */
	public String getPstatcode() {
		return pstatcode;
	}
	/**
	 * Setter for pstatcode
	 * 
	 * @param pstatcode - The pstatcode to set
	 */
	public void setPstatcode(String pstatcode) {
		this.pstatcode = pstatcode;
	}
	/**
	 * Getter for annvry
	 * 
	 * @return - The annvry
	 */
	public String getAnnvry() {
		return annvry;
	}
	/**
	 * Setter for annvry
	 * 
	 * @param annvry - The annvry to set
	 */
	public void setAnnvry(String annvry) {
		this.annvry = annvry;
	}
	/**
	 * Getter for bascmeth
	 * 
	 * @return - The bascmeth
	 */
	public String getBascmeth() {
		return bascmeth;
	}
	/**
	 * Setter for bascmeth
	 * 
	 * @param bascmeth - The bascmeth to set
	 */
	public void setBascmeth(String bascmeth) {
		this.bascmeth = bascmeth;
	}
	/**
	 * Getter for bascpy
	 * 
	 * @return - The bascpy
	 */
	public String getBascpy() {
		return bascpy;
	}
	/**
	 * Setter for bascpy
	 * 
	 * @param bascpy - The bascpy to set
	 */
	public void setBascpy(String bascpy) {
		this.bascpy = bascpy;
	}
	/**
	 * Getter for rnwcpy
	 * 
	 * @return - The rnwcpy
	 */
	public String getRnwcpy() {
		return rnwcpy;
	}
	/**
	 * Setter for rnwcpy
	 * 
	 * @param rnwcpy - The rnwcpy to set
	 */
	public void setRnwcpy(String rnwcpy) {
		this.rnwcpy = rnwcpy;
	}
	/**
	 * Getter for srvcpy
	 * 
	 * @return - The srvcpy
	 */
	public String getSrvcpy() {
		return srvcpy;
	}
	/**
	 * Setter for srvcpy
	 * 
	 * @param srvcpy - The srvcpy to set
	 */
	public void setSrvcpy(String srvcpy) {
		this.srvcpy = srvcpy;
	}
	/**
	 * Getter for newinst
	 * 
	 * @return - The newinst
	 */
	public BigDecimal getNewinst() {
		return newinst;
	}
	/**
	 * Setter for newinst
	 * 
	 * @param newinst - The newinst to set
	 */
	public void setNewinst(BigDecimal newinst) {
		this.newinst = newinst;
	}
	/**
	 * Getter for basicCommMeth
	 * 
	 * @return - The basicCommMeth
	 */
	public String getBasicCommMeth() {
		return basicCommMeth;
	}
	/**
	 * Setter for basicCommMeth
	 * 
	 * @param basicCommMeth - The basicCommMeth to set
	 */
	public void setBasicCommMeth(String basicCommMeth) {
		this.basicCommMeth = basicCommMeth;
	}
	/**
	 * Getter for lastInst
	 * 
	 * @return - The lastInst
	 */
	public BigDecimal getLastInst() {
		return lastInst;
	}
	/**
	 * Setter for lastInst
	 * 
	 * @param lastInst - The lastInst to set
	 */
	public void setLastInst(BigDecimal lastInst) {
		this.lastInst = lastInst;
	}
	/**
	 * Getter for newsum
	 * 
	 * @return - The newsum
	 */
	public BigDecimal getNewsum() {
		return newsum;
	}
	/**
	 * Setter for newsum
	 * 
	 * @param newsum - The newsum to set
	 */
	public void setNewsum(BigDecimal newsum) {
		this.newsum = newsum;
	}
	/**
	 * Getter for lastSum
	 * 
	 * @return - The lastSum
	 */
	public BigDecimal getLastSum() {
		return lastSum;
	}
	/**
	 * Setter for lifcnum
	 * 
	 * @param lastSum - The lastSum to set
	 */
	public void setLastSum(BigDecimal lastSum) {
		this.lastSum = lastSum;
	}
	/**
	 * Getter for zllastinst
	 * 
	 * @return -The zllastinst
	 */
	public BigDecimal getZllastinst() {
		return zllastinst;
	}
	/**
	 * Setter for zllastinst
	 * 
	 * @param zllastinst - The zllastinst to set
	 */
	public void setZllastinst(BigDecimal zllastinst) {
		this.zllastinst = zllastinst;
	}
	/**
	 * Getter for zbnewinst
	 * 
	 * @return - The zbnewinst
	 */
	public BigDecimal getZbnewinst() {
		return zbnewinst;
	}
	/**
	 * Setter for zbnewinst
	 * 
	 * @param zbnewinst - The zbnewinst to set
	 */
	public void setZbnewinst(BigDecimal zbnewinst) {
		this.zbnewinst = zbnewinst;
	}
	/**
	 * Getter for zblastinst
	 * 
	 * @return - The zblastinst
	 */
	public BigDecimal getZblastinst() {
		return zblastinst;
	}
	/**
	 * Setter for zblastinst
	 * 
	 * @param zblastinst - The zblastinst to set
	 */
	public void setZblastinst(BigDecimal zblastinst) {
		this.zblastinst = zblastinst;
	}
	/**
	 * Getter for zlnewinst
	 * 
	 * @return - The zlnewinst
	 */
	public BigDecimal getZlnewinst() {
		return zlnewinst;
	}
	/**
	 * Setter for zlnewinst
	 * 
	 * @param zlnewinst - The zlnewinst to set
	 */
	public void setZlnewinst(BigDecimal zlnewinst) {
		this.zlnewinst = zlnewinst;
	}
	/**
	 * Getter for zstpduty01
	 * 
	 * @return - The zstpduty01
	 */
	public BigDecimal getZstpduty01() {
		return zstpduty01;
	}
	/**
	 * Setter for zstpduty01
	 * 
	 * @param zstpduty01 - The zstpduty01 to set
	 */
	public void setZstpduty01(BigDecimal zstpduty01) {
		this.zstpduty01 = zstpduty01;
	}

	
	
}
