/*
 * File: Dry5361.java
 * Date: March 26, 2014 3:03:18 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY5361.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.fsuframework.core.FeaConfg;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.diary.dataaccess.FpcodryTableDAM;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.quipoz.COBOLFramework.util.StringUtil;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* Flexible Premium Anniversary Processing
*
* The subroutine will use the FPCODRY logical which selects
* FPCO records which have a validflag of '1' and the ANPROCIND
* and the ANPROCIND is not set to 'Y'.
*
* 100- section is where the main processing occurs.
*
* 200- section is the initialiase section and contains
* the contract level processing. This contract level
* processing will read T5679, T6654 and T5729. It will
* also validate the contract risk and premium statii,
* if these are not valid then no further processing will
* occur.
*
* 300- section is where the FPCODRY records are read
* it is performed until a change of key is reached or the
* end of file is encountered.
* This section contains the FPCO/Component level validation.
* It is where the corresponding coverage for the FPCODRY
* record is validated and new FPCODRY records are written.
*
* 400- section  validates the components.
* 500- section  updates the existing FPCODRY.
* 600- section  writes a new FPCODRY.
* 700- section  find the minimum %s for the new FPCO record
* 800- section  writes the DRPT record for any FPCO which
*               has not reached it's target premium
* 900- section  updates the CHDR and writes a PTRN
* 1000- section passes the new target to date through the
*               DRYPRCLNK linkage.
*
* Control totals used in this subrouinte
*    01  -  No. of FPCO records read
*    02  -  No. of FPCO records written
*    03  -  No. of PTRN records created
*    04  -  No. of FPCO with invalid statii
*    05  -  No. Ann date not reached
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5361 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5361");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");
	private Validator invalidContract = new Validator(wsaaValidContract, "N");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	private Validator invalidCoverage = new Validator(wsaaValidCoverage, "N");

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");
	private Validator dateNotFound = new Validator(wsaaDateFound, "N");

	private FixedLengthStringData wsaaFreqFound = new FixedLengthStringData(1).init("N");
	private Validator freqFound = new Validator(wsaaFreqFound, "Y");
	private Validator freqNotFound = new Validator(wsaaFreqFound, "N");

	private FixedLengthStringData wsaaDurationFound = new FixedLengthStringData(1).init("N");
	private Validator durationFound = new Validator(wsaaDurationFound, "Y");
	private Validator durationNotFound = new Validator(wsaaDurationFound, "N");
	private PackedDecimalData wsaaEffdatePlusCntlead = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(3, 0).init(0);
	private ZonedDecimalData wsaaT5729Sub = new ZonedDecimalData(3, 0);
	private PackedDecimalData wsaaOldCurrto = new PackedDecimalData(8, 0).init(0);
	private PackedDecimalData wsaaTest = new PackedDecimalData(5, 0).init(0);
	private ZonedDecimalData wsaaNumPeriod = new ZonedDecimalData(11, 5).init(0);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaFpcoUpdated = new ZonedDecimalData(2, 0).init(0).setUnsigned();
	private PackedDecimalData wsaaTargto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTargtoEarliest = new PackedDecimalData(8, 0);

		/*  Storage for T6654 items*/
	private FixedLengthStringData wsaaT6654Key2 = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBillchnl2 = new FixedLengthStringData(1).isAPartOf(wsaaT6654Key2, 0);
	private FixedLengthStringData wsaaCnttype2 = new FixedLengthStringData(3).isAPartOf(wsaaT6654Key2, 1);

		/*  Storage for T5729 table items.*/
	private FixedLengthStringData wsaaDurations = new FixedLengthStringData(16);
	private ZonedDecimalData[] wsaaDuration = ZDArrayPartOfStructure(4, 4, 0, wsaaDurations, 0);

	private FixedLengthStringData wsaaOverdueMins = new FixedLengthStringData(12);
	private ZonedDecimalData[] wsaaOverdueMin = ZDArrayPartOfStructure(4, 3, 0, wsaaOverdueMins, 0);
	private PackedDecimalData wsaaOldAnnivProcDate = new PackedDecimalData(8, 0);
		/* FORMATS */
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String fpcodryrec = "FPCODRYREC";
	private static final String itemrec = "ITEMREC";
	private static final String covrrec = "COVRREC";
	private static final String ptrnrec = "PTRNREC";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t6654 = "T6654";
	private static final String t5729 = "T5729";
		/* ERRORS */
	private static final String i086 = "I086";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private FpcodryTableDAM fpcodryIO = new FpcodryTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private T5679rec t5679rec = new T5679rec();
	private T5729rec t5729rec = new T5729rec();
	private T6654rec t6654rec = new T6654rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nearExit380,
		exit390
	}

	public Dry5361() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}



protected void startProcessing100()
	{
		start110();
	}

protected void start110()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		initialise200();
		if (invalidContract.isTrue()) {
			return ;
		}
		/* Set up the initial key for loopoing through the FPCO*/
		/* records for the contract.*/
		fpcodryIO.setParams(SPACES);
		fpcodryIO.setRecKeyData(SPACES);
		fpcodryIO.setRecNonKeyData(SPACES);
		fpcodryIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		fpcodryIO.setChdrnum(drypDryprcRecInner.drypEntity);
		fpcodryIO.setFunction(varcom.begn);
		fpcodryIO.setFormat(fpcodryrec);
		compute(wsaaTranno, 0).set(add(chdrlifIO.getTranno(), 1));
		/* Read through all the records for the contract*/
		while ( !(isEQ(fpcodryIO.getStatuz(), varcom.endp))) {
			fpcoLoop300();
		}

		/* Only update the contract and write a PTRN if FPCO's have*/
		/* been processed.*/
		if (isGT(wsaaFpcoUpdated, 0)) {
			writeChdrPtrn900();
			close1000();
		}
		else {
			wsaaTargto.set(wsaaTargtoEarliest);
			close1000();
		}
	}

protected void initialise200()
	{
		init210();
	}

protected void init210()
	{
		wsaaTargto.set(varcom.vrcmMaxDate);
		wsaaTargtoEarliest.set(varcom.vrcmMaxDate);
		BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		/*  Load Contract statii.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Read the contract details for the entity number passed*/
		/* through linkage..*/
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Validate the contract statii, if not valid, then*/
		/* report this on the error log and do not process this*/
		/* contract*/
		invalidContract.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()], chdrlifIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						validContract.setTrue();
					}
				}
			}
		}
		/* Report on any contract with an invalid status..*/
		/* Don't do any further processing on this contract*/
		if (invalidContract.isTrue()) {
			return ;
		}
		/* The contract is valid, so find the number of*/
		/* lead days to be used.*/
		String key;
		if(BTPRO028Permission) {
			key = chdrlifIO.getBillchnl().toString().trim() + chdrlifIO.getCnttype().toString().trim() + chdrlifIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = chdrlifIO.getBillchnl().toString().trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = chdrlifIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						drylogrec.statuz.set(itemIO.getStatuz());
						drylogrec.params.set(itemIO.getParams());
						drylogrec.dryDatabaseError.setTrue();
						a000FatalError();
					}
				}
			}
		}
		else {
		wsaaBillchnl2.set(chdrlifIO.getBillchnl());
		wsaaCnttype2.set(chdrlifIO.getCnttype());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t6654);
		itemIO.setItemitem(wsaaT6654Key2);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* If the item is not found for the contract type*/
		/* try to find the generic item for the billing channel*/
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			wsaaBillchnl2.set(chdrlifIO.getBillchnl());
			wsaaCnttype2.set("***");
			itemIO.setStatuz(varcom.oK);
			itemIO.setItempfx(smtpfxcpy.item);
			itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itemIO.setItemtabl(t6654);
			itemIO.setItemitem(wsaaT6654Key2);
			itemIO.setFunction(varcom.readr);
			SmartFileCode.execute(appVars, itemIO);
			if (isNE(itemIO.getStatuz(), varcom.oK)) {
				drylogrec.params.set(itemIO.getParams());
				drylogrec.statuz.set(itemIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
		}
		/* An item has been found, either a generic one, or one*/
		/* that matches the contract type.*/
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		/* Call 'DATCON2' to increment the effective date by the T6654*/
		/* LEAD DAYS.*/
		datcon2rec.freqFactor.set(t6654rec.leadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaEffdatePlusCntlead.set(datcon2rec.intDate2);
		/*  We must find the effective T5729 entry for the contract*/
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5729);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setItmto(0);
		itdmIO.setFunction(varcom.begn);
		dateNotFound.setTrue();
		while ( !(isNE(itdmIO.getStatuz(), varcom.oK)
		|| dateFound.isTrue())) {
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)) {
				drylogrec.statuz.set(i086);
				drylogrec.params.set(t5729);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
			if (isNE(drypDryprcRecInner.drypCompany, itdmIO.getItemcoy())
			|| isNE(t5729, itdmIO.getItemtabl())
			|| isNE(chdrlifIO.getCnttype(), itdmIO.getItemitem())) {
				drylogrec.statuz.set(i086);
				drylogrec.params.set(t5729);
				drylogrec.drySystemError.setTrue();
				a000FatalError();
			}
			itdmIO.setFunction(varcom.nextr);
			t5729rec.t5729Rec.set(itdmIO.getGenarea());
			if (isGTE(chdrlifIO.getOccdate(), itdmIO.getItmfrm())) {
				dateFound.setTrue();
			}
		}

		/*  Load the values from table T5729 which match the frequency*/
		/*  on the CHDR record*/
		freqNotFound.setTrue();
		for (wsaaT5729Sub.set(1); !(isGT(wsaaT5729Sub, 6)
		|| freqFound.isTrue()); wsaaT5729Sub.add(1)){
			if (isEQ(t5729rec.frqcy[wsaaT5729Sub.toInt()], chdrlifIO.getBillfreq())) {
				wsaaTest.set(wsaaT5729Sub);
				freqFound.setTrue();
			}
		}
		if (isGT(wsaaT5729Sub, 6)) {
			drylogrec.statuz.set(i086);
			drylogrec.params.set(t5729);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/*  Determine the current minimum % from T5729 based on the*/
		/*  difference between the Occdate and the effective date of this*/
		/*  job. These durations will be used in the 700- section when*/
		/*  updating valid FPCODRY records.*/
		if (isEQ(wsaaTest, 1)){
			wsaaDurations.set(t5729rec.durationas);
			wsaaOverdueMins.set(t5729rec.overdueMinas);
		}
		else if (isEQ(wsaaTest, 2)){
			wsaaDurations.set(t5729rec.durationbs);
			wsaaOverdueMins.set(t5729rec.overdueMinbs);
		}
		else if (isEQ(wsaaTest, 3)){
			wsaaDurations.set(t5729rec.durationcs);
			wsaaOverdueMins.set(t5729rec.overdueMincs);
		}
		else if (isEQ(wsaaTest, 4)){
			wsaaDurations.set(t5729rec.durationds);
			wsaaOverdueMins.set(t5729rec.overdueMinds);
		}
		else if (isEQ(wsaaTest, 5)){
			wsaaDurations.set(t5729rec.durationes);
			wsaaOverdueMins.set(t5729rec.overdueMines);
		}
		else if (isEQ(wsaaTest, 6)){
			wsaaDurations.set(t5729rec.durationfs);
			wsaaOverdueMins.set(t5729rec.overdueMinfs);
		}
		else{
			drylogrec.statuz.set(i086);
			drylogrec.params.set(t5729);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaFpcoUpdated.set(0);
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	itempf.setItempfx("IT");
	itempf.setItemcoy(drypDryprcRecInner.drypCompany.toString());
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void fpcoLoop300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					loop310();
				case nearExit380:
					nearExit380();
				case exit390:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void loop310()
	{
		SmartFileCode.execute(appVars, fpcodryIO);
		if (isNE(fpcodryIO.getStatuz(), varcom.oK)
		&& isNE(fpcodryIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(fpcodryIO.getStatuz());
			drylogrec.params.set(fpcodryIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* If the key has changed, or the end of file is reached*/
		/* exit this section.*/
		if (isEQ(fpcodryIO.getStatuz(), varcom.endp)
		|| isNE(fpcodryIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(fpcodryIO.getChdrnum(), drypDryprcRecInner.drypEntity)) {
			fpcodryIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit390);
		}
		/* Increment the totals for the number of FPCOs read.*/
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		/* Calculate the number of lead days from the run date*/
		/* Also increment the totals for the number of FPCOs*/
		/* which haven't reached their anniversary yet.*/
		if (isGT(fpcodryIO.getAnnivProcDate(), wsaaEffdatePlusCntlead)) {
			drycntrec.contotNumber.set(ct05);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			if (isLT(fpcodryIO.getTargto(), wsaaTargtoEarliest)) {
				wsaaTargtoEarliest.set(fpcodryIO.getTargto());
			}
			goTo(GotoLabel.nearExit380);
		}
		/* Validate the coverage statii..*/
		validateCoverage400();
		/* Increment the totals for FPCOs with invalid statii.*/
		if (invalidCoverage.isTrue()) {
			drycntrec.contotNumber.set(ct04);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			goTo(GotoLabel.exit390);
		}
		/* We've got a valid FPCO record, time to do some updates..*/
		update500();
	}

protected void nearExit380()
	{
		fpcodryIO.setFunction(varcom.nextr);
	}

protected void validateCoverage400()
	{
		validate410();
	}

protected void validate410()
	{
		/* First read the corresponding COVR record*/
		covrIO.setParams(SPACES);
		covrIO.setRecKeyData(SPACES);
		covrIO.setRecNonKeyData(SPACES);
		covrIO.setChdrcoy(fpcodryIO.getChdrcoy());
		covrIO.setChdrnum(fpcodryIO.getChdrnum());
		covrIO.setLife(fpcodryIO.getLife());
		covrIO.setCoverage(fpcodryIO.getCoverage());
		covrIO.setRider(fpcodryIO.getRider());
		covrIO.setPlanSuffix(fpcodryIO.getPlanSuffix());
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Validate the component statii against T5679..*/
		invalidCoverage.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)){
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrIO.getPstatcode())) {
						wsaaT5679Sub.set(13);
						validCoverage.setTrue();
					}
				}
			}
		}
	}

protected void update500()
	{
		update510();
	}

protected void update510()
	{
		/* Update the FPCO details*/
		if (isLT(fpcodryIO.getPremRecPer(), fpcodryIO.getTargetPremium())) {
			writeRpt800();
		}
		else {
			if (isGTE(fpcodryIO.getBilledInPeriod(), fpcodryIO.getTargetPremium())) {
				fpcodryIO.setActiveInd("N");
			}
		}
		/* The FPCO record has been read but not held.*/
		/* UPDAT rereads the record and holds it and then*/
		/* rewrites it. It does NOT overwrite the data area*/
		/* when it rereads and hold the record so the Anniversay*/
		/* Processing Indicator below is set correctly.*/
		fpcodryIO.setAnnProcessInd("Y");
		fpcodryIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, fpcodryIO);
		if (isNE(fpcodryIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(fpcodryIO.getStatuz());
			drylogrec.params.set(fpcodryIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		writeNewFpco600();
		wsaaFpcoUpdated.add(1);
	}

protected void writeNewFpco600()
	{
		start610();
	}

protected void start610()
	{
		wsaaOldCurrto.set(fpcodryIO.getTargto());
		wsaaOldAnnivProcDate.set(fpcodryIO.getAnnivProcDate());
		/* Calculate TARGTO as old TARGTO + 1 Year*/
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(fpcodryIO.getTargto());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		fpcodryIO.setTargto(datcon2rec.intDate2);
		wsaaTargto.set(datcon2rec.intDate2);
		/* Calculate CBANPR as old CBANPR + 1 Year*/
		datcon2rec.frequency.set("01");
		datcon2rec.intDate1.set(fpcodryIO.getAnnivProcDate());
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Set up the new FPCODRY details..*/
		fpcodryIO.setAnnivProcDate(datcon2rec.intDate2);
		fpcodryIO.setTranno(wsaaTranno);
		fpcodryIO.setTargfrom(wsaaOldCurrto);
		fpcodryIO.setCurrfrom(drypDryprcRecInner.drypRunDate);
		fpcodryIO.setEffdate(drypDryprcRecInner.drypRunDate);
		fpcodryIO.setEffdate(wsaaOldAnnivProcDate);
		fpcodryIO.setCurrto(varcom.vrcmMaxDate);
		fpcodryIO.setAnnProcessInd(SPACES);
		fpcodryIO.setActiveInd("Y");
		fpcodryIO.setPremRecPer(ZERO);
		fpcodryIO.setOverdueMin(ZERO);
		fpcodryIO.setBilledInPeriod(ZERO);
		findMinPercentage700();
		fpcodryIO.setFormat(fpcodryrec);
		fpcodryIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, fpcodryIO);
		if (isNE(fpcodryIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(fpcodryIO.getStatuz());
			drylogrec.params.set(fpcodryIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Increment the totals for the FPCOs written.*/
		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void findMinPercentage700()
	{
		start710();
	}

protected void start710()
	{
		/* Use the DATCON3 subroutine to calculate the freq factor*/
		/* The durations were loaded into 200- section.*/
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(covrIO.getCrrcd());
		datcon3rec.intDate2.set(fpcodryIO.getTargto());
		datcon3rec.frequency.set(chdrlifIO.getBillfreq());
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		//smalchi2 for ILPI-8
		wsaaNumPeriod.set(datcon3rec.freqFactor);
		wsaaSub2.set(0);
		durationNotFound.setTrue();
		/* Find the corresponding duration*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)
		|| isEQ(wsaaDuration[wsaaSub.toInt()], SPACES)
		|| durationFound.isTrue()); wsaaSub.add(1)){
			if (isLTE(wsaaNumPeriod, wsaaDuration[wsaaSub.toInt()])) {
				wsaaSub2.set(wsaaSub);
				durationFound.setTrue();
			}
		}
		if (durationNotFound.isTrue()) {
			drylogrec.statuz.set(i086);
			drylogrec.params.set(t5729);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		fpcodryIO.setMinOverduePer(wsaaOverdueMin[wsaaSub2.toInt()]);
	}

protected void writeRpt800()
	{
		write810();
	}

protected void write810()
	{
		/* Set up the sort key*/
		dryrDryrptRecInner.r5361Chdrnum.set(fpcodryIO.getChdrnum());
		dryrDryrptRecInner.r5361Coverage.set(fpcodryIO.getCoverage());
		/* Set up the report detail record level fields.*/
		dryrDryrptRecInner.r5361Life.set(fpcodryIO.getLife());
		dryrDryrptRecInner.r5361PlanSuffix.set(fpcodryIO.getPlanSuffix());
		dryrDryrptRecInner.r5361Targfrom.set(fpcodryIO.getTargfrom());
		dryrDryrptRecInner.r5361Targto.set(fpcodryIO.getTargto());
		dryrDryrptRecInner.r5361AnnivProcDate.set(fpcodryIO.getAnnivProcDate());
		dryrDryrptRecInner.r5361TargetPremium.set(fpcodryIO.getTargetPremium());
		dryrDryrptRecInner.r5361PremRecPer.set(fpcodryIO.getPremRecPer());
		dryrDryrptRecInner.r5361BilledInPeriod.set(fpcodryIO.getBilledInPeriod());
		dryrDryrptRecInner.r5361OverdueMin.set(fpcodryIO.getOverdueMin());
		/* Write the DRPT record.*/
		e000ReportRecords();
	}

protected void writeChdrPtrn900()
	{
		write910();
	}

protected void write910()
	{
		/* Hold the Contract Header before updating it.*/
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Update the CHDR details.*/
		chdrlifIO.setTranno(wsaaTranno);
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Write the PTRN..*/
		/* INITIALIZE                     PTRNREC-KEY-DATA              */
		/*                                PTRNREC-NON-KEY-DATA.         */
		ptrnIO.setParams(SPACES);
		ptrnIO.setUser(0);
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setTransactionDate(drypDryprcRecInner.drypRunDate);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setTransactionTime(ZERO);
		ptrnIO.setTranno(wsaaTranno);
		/*    MOVE FPCODRY-CHDRCOY        TO PTRN-CHDRCOY.                 */
		/*    MOVE FPCODRY-CHDRNUM        TO PTRN-CHDRNUM.                 */
		ptrnIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setChdrnum(drypDryprcRecInner.drypEntity);
		ptrnIO.setBatcpfx(smtpfxcpy.batc);
		/* MOVE FPCODRY-ANNIV-PROC-DATE                                 */
		/*                             TO PTRN-PTRNEFF.                 */
		ptrnIO.setPtrneff(wsaaOldAnnivProcDate);
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		/* MOVE DRYP-EFFECTIVE-DATE    TO PTRN-DATESUB.         <LA5184>*/
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Increment the totals for the number of PTRNs written.*/
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void close1000()
	{
		/*CLOSE*/
		drypDryprcRecInner.drypCpiDate.set(wsaaTargto);
		drypDryprcRecInner.drypTargto.set(wsaaTargto);
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner {
	private FixedLengthStringData dryrReportName = new FixedLengthStringData(10);
	private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r5361DataArea = new FixedLengthStringData(440).isAPartOf(dryrGenarea, 0, REDEFINE);
	private FixedLengthStringData r5361Life = new FixedLengthStringData(2).isAPartOf(r5361DataArea, 0);
	private ZonedDecimalData r5361PlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(r5361DataArea, 2);
	private ZonedDecimalData r5361Targfrom = new ZonedDecimalData(8, 0).isAPartOf(r5361DataArea, 6);
	private ZonedDecimalData r5361Targto = new ZonedDecimalData(8, 0).isAPartOf(r5361DataArea, 14);
	private ZonedDecimalData r5361TargetPremium = new ZonedDecimalData(17, 2).isAPartOf(r5361DataArea, 22);
	private ZonedDecimalData r5361PremRecPer = new ZonedDecimalData(17, 2).isAPartOf(r5361DataArea, 39);
	private ZonedDecimalData r5361BilledInPeriod = new ZonedDecimalData(17, 2).isAPartOf(r5361DataArea, 56);
	private ZonedDecimalData r5361OverdueMin = new ZonedDecimalData(17, 2).isAPartOf(r5361DataArea, 73);
	private ZonedDecimalData r5361AnnivProcDate = new ZonedDecimalData(8, 0).isAPartOf(r5361DataArea, 90);

	private FixedLengthStringData r5361SortKey = new FixedLengthStringData(38);
	private FixedLengthStringData r5361Chdrnum = new FixedLengthStringData(8).isAPartOf(r5361SortKey, 0);
	private FixedLengthStringData r5361Coverage = new FixedLengthStringData(2).isAPartOf(r5361SortKey, 8);
	private FixedLengthStringData r5361ReportName = new FixedLengthStringData(10).init("R5361     ");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
}
}
