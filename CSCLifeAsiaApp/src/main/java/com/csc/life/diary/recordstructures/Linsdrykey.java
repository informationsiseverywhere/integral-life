package com.csc.life.diary.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:08:59
 * Description:
 * Copybook name: LINSDRYKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Linsdrykey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData linsdryFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData linsdryKey = new FixedLengthStringData(256).isAPartOf(linsdryFileKey, 0, REDEFINE);
  	public FixedLengthStringData linsdryChdrcoy = new FixedLengthStringData(1).isAPartOf(linsdryKey, 0);
  	public FixedLengthStringData linsdryChdrnum = new FixedLengthStringData(8).isAPartOf(linsdryKey, 1);
  	public PackedDecimalData linsdryInstfrom = new PackedDecimalData(8, 0).isAPartOf(linsdryKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(242).isAPartOf(linsdryKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(linsdryFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		linsdryFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}