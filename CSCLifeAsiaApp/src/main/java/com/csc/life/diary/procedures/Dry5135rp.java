/*
 * File: Dry5135rp.java
 * Date: March 26, 2014 2:59:44 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5135RP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DrptTableDAM;
import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.regularprocessing.reports.R5135Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5135rp extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5135Report printFile = new R5135Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5135RP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
		/* FORMATS */
	private static final String drptrec = "DRPTREC";
	private static final String descrec = "DESCREC";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5135H01Record = new FixedLengthStringData(73);
	private FixedLengthStringData r5135h01O = new FixedLengthStringData(73).isAPartOf(r5135H01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5135h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5135h01O, 1);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(r5135h01O, 31);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(r5135h01O, 41);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(r5135h01O, 43);
	private DescTableDAM descIO = new DescTableDAM();
	private DrptTableDAM drptIO = new DrptTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Dryoutrec dryoutrec = new Dryoutrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private R5135D01RecordInner r5135D01RecordInner = new R5135D01RecordInner();

	public Dry5135rp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryoutrec.dryoutRec = convertAndSetParam(dryoutrec.dryoutRec, parmArray, 0);
		try {
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
		printFile.openOutput();
		wsaaOverflow.set("Y");
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaToday.set(datcon1rec.intDate);
		dryoutrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		drptIO.setRecKeyData(SPACES);
		drptIO.setStatuz(varcom.oK);
		drptIO.setEffdate(dryoutrec.effectiveDate);
		drptIO.setDiaryEntityCompany(dryoutrec.company);
		drptIO.setDiaryEntityBranch(dryoutrec.branch);
		drptIO.setDiaryReportName(dryoutrec.reportName);
		drptIO.setDiaryRunNumber(dryoutrec.runNumber);
		drptIO.setFormat(drptrec);
		drptIO.setFunction(varcom.begn);
		while ( !(isEQ(drptIO.getStatuz(), varcom.endp))) {
			callDrpt200();
		}
		
		endReport500();
		printFile.close();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void callDrpt200()
	{
		call210();
	}

protected void call210()
	{
		SmartFileCode.execute(appVars, drptIO);
		if (isNE(drptIO.getStatuz(), varcom.oK)
		&& isNE(drptIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(drptIO.getParams());
			drylogrec.statuz.set(drptIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		if (isEQ(drptIO.getStatuz(), varcom.endp)
		|| isNE(drptIO.getDiaryEntityCompany(), dryoutrec.company)
		|| isNE(drptIO.getDiaryEntityBranch(), dryoutrec.branch)
		|| isNE(drptIO.getDiaryReportName(), dryoutrec.reportName)) {
			drptIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(dryoutrec.runNumber, ZERO)
		&& isNE(dryoutrec.runNumber, drptIO.getDiaryRunNumber())) {
			drptIO.setStatuz(varcom.endp);
			return ;
		}
		writeLine300();
		drptIO.setFunction(varcom.nextr);
	}

protected void writeLine300()
	{
		write310();
	}

protected void write310()
	{
		/* Write New Page if required.*/
		if (pageOverflow.isTrue()) {
			wsaaOverflow.set("N");
			newPage400();
		}
		/* Fill detail record.*/
		dryrDryrptRecInner.dryrGenarea.set(drptIO.getGenarea());
		r5135D01RecordInner.chdrnum.set(dryrDryrptRecInner.r5135Chdrnum);
		r5135D01RecordInner.life.set(dryrDryrptRecInner.r5135Life);
		r5135D01RecordInner.coverage.set(dryrDryrptRecInner.r5135Coverage);
		r5135D01RecordInner.rider.set(dryrDryrptRecInner.r5135Rider);
		r5135D01RecordInner.plnsfx.set(dryrDryrptRecInner.r5135PlanSuffix);
		r5135D01RecordInner.crtable.set(dryrDryrptRecInner.r5135Crtable);
		r5135D01RecordInner.oldsum.set(dryrDryrptRecInner.r5135Oldsum);
		r5135D01RecordInner.newsumi.set(dryrDryrptRecInner.r5135Newsumi);
		r5135D01RecordInner.oldinst.set(dryrDryrptRecInner.r5135Oldinst);
		r5135D01RecordInner.newinst.set(dryrDryrptRecInner.r5135Newinst);
		r5135D01RecordInner.rcesdte.set(dryrDryrptRecInner.r5135Rcesdte);
		r5135D01RecordInner.currency.set(dryrDryrptRecInner.r5135Currency);
		printRecord.set(SPACES);
		printFile.printR5135d01(r5135D01RecordInner.r5135D01Record, indicArea);
	}

protected void newPage400()
	{
		new400();
	}

protected void new400()
	{
		/* Fill header record.*/
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(wsaaToday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sdate.set(datcon1rec.extDate);
		company.set(dryoutrec.company);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(dryoutrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		companynm.set(descIO.getLongdesc());
		branch.set(dryoutrec.branch);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(dryoutrec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(dryoutrec.branch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		branchnm.set(descIO.getLongdesc());
		printRecord.set(SPACES);
		printFile.printR5135h01(r5135H01Record, indicArea);
	}

protected void endReport500()
	{
		/*NEW*/
		/*EXIT*/
	}
//Modify for ILPI-65 
protected void fatalError()
	{
		/*A010-FATAL*/
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		dryoutrec.statuz.set(drylogrec.statuz);
		drylogrec.runNumber.set(dryoutrec.runNumber);
		drylogrec.entityKey.set(SPACES);
		//callProgram(Drylog.class, drylogrec.drylogRec);
		//exitProgram();
		//ILPI-61 
		a001FatalError();
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r5135DataArea = new FixedLengthStringData(500).isAPartOf(dryrGenarea, 0, REDEFINE);
	private FixedLengthStringData r5135Chdrnum = new FixedLengthStringData(8).isAPartOf(r5135DataArea, 0);
	private FixedLengthStringData r5135Life = new FixedLengthStringData(2).isAPartOf(r5135DataArea, 8);
	private FixedLengthStringData r5135Coverage = new FixedLengthStringData(2).isAPartOf(r5135DataArea, 10);
	private FixedLengthStringData r5135Rider = new FixedLengthStringData(2).isAPartOf(r5135DataArea, 12);
	private FixedLengthStringData r5135Crtable = new FixedLengthStringData(4).isAPartOf(r5135DataArea, 14);
	private ZonedDecimalData r5135PlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(r5135DataArea, 18);
	private ZonedDecimalData r5135Oldsum = new ZonedDecimalData(13, 2).isAPartOf(r5135DataArea, 22);
	private ZonedDecimalData r5135Newsumi = new ZonedDecimalData(13, 2).isAPartOf(r5135DataArea, 35);
	private ZonedDecimalData r5135Oldinst = new ZonedDecimalData(13, 2).isAPartOf(r5135DataArea, 48);
	private ZonedDecimalData r5135Newinst = new ZonedDecimalData(13, 2).isAPartOf(r5135DataArea, 61);
	private FixedLengthStringData r5135Rcesdte = new FixedLengthStringData(10).isAPartOf(r5135DataArea, 74);
	private FixedLengthStringData r5135Currency = new FixedLengthStringData(3).isAPartOf(r5135DataArea, 84);
}
/*
 * Class transformed  from Data Structure R5135-D01-RECORD--INNER
 */
private static final class R5135D01RecordInner { 

	private FixedLengthStringData r5135D01Record = new FixedLengthStringData(99);
	private FixedLengthStringData r5135d01O = new FixedLengthStringData(99).isAPartOf(r5135D01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5135d01O, 0);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5135d01O, 8);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5135d01O, 10);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r5135d01O, 12);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5135d01O, 14);
	private FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(r5135d01O, 18);
	private ZonedDecimalData oldsum = new ZonedDecimalData(15, 0).isAPartOf(r5135d01O, 22);
	private ZonedDecimalData newsumi = new ZonedDecimalData(15, 0).isAPartOf(r5135d01O, 37);
	private ZonedDecimalData oldinst = new ZonedDecimalData(17, 2).isAPartOf(r5135d01O, 52);
	private ZonedDecimalData newinst = new ZonedDecimalData(17, 2).isAPartOf(r5135d01O, 69);
	private FixedLengthStringData rcesdte = new FixedLengthStringData(10).isAPartOf(r5135d01O, 86);
	private FixedLengthStringData currency = new FixedLengthStringData(3).isAPartOf(r5135d01O, 96);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
