package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import com.csc.life.interestbearing.dataaccess.HifdTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.SMARTCodeModel;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

public class Dryhifd extends SMARTCodeModel {

   private FixedLengthStringData wsaaProg = new FixedLengthStringData(6).init("DRYACAGRNL");
   private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
   
   private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
   private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
   private FixedLengthStringData lsaaDataArea = new FixedLengthStringData(1000);
   
   private PackedDecimalData wsaaFundValue = new PackedDecimalData(17, 2);
   
   private HifdTableDAM hifdIO = new HifdTableDAM();
   private String hifdrec = "HIFDREC";
	   
	public void mainline(Object... parmArray)
	{
	   lsaaDataArea = convertAndSetParam(lsaaDataArea, parmArray, 2);
	   lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 1);
	   lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
	   try {
	       mainLine100();
	   }
	   catch (COBOLExitProgramException e) {
	   // Expected exception for control flow purposes
	   }
    }
	
	protected void mainLine100()
    {
     main110();
     exit190();
    }
	
	protected void main110()
    {
		 lsaaStatuz.set(Varcom.oK);
		 hifdIO.setDataArea(lsaaDataArea);
		 wsaaFundValue.set(hifdIO.getZtotfndval());
		 hifdIO.setFormat(hifdrec);
		 hifdIO.setFunction(Varcom.readh);
		 SmartFileCode.execute(appVars, hifdIO);
		 if (isNE(hifdIO.getStatuz(), Varcom.oK)
	       && isNE(hifdIO.getStatuz(), Varcom.mrnf)) {
	           lsaaStatuz.set(hifdIO.getStatuz());
	           exit190();
	     }
		 
		 if(isEQ(hifdIO.getStatuz(), Varcom.oK)){
			 hifdIO.ztotfndval.add(wsaaFundValue);
			 hifdIO.setFunction(Varcom.rewrt);
		 } else{
			 hifdIO.setZtotfndval(wsaaFundValue);
			 hifdIO.setFunction(Varcom.writr);
		 }
		 
		 SmartFileCode.execute(appVars, hifdIO);
		 if (isNE(hifdIO.getStatuz(), Varcom.oK)) {
			 lsaaStatuz.set(hifdIO.getStatuz());
			 exit190();
	     }
		
    }
	
	protected void exit190()
    {
     exitProgram();
    }
}
