/*
 * File: Dry5367.java
 * Date: January 15, 2015 4:14:35 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5367.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.diary.dataaccess.RegrdteTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrpayTableDAM;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.dataaccess.RegprgpTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.life.terminationclaims.tablestructures.T6762rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*                   REGULAR PAYMENTS IN REVIEW
*                   --------------------------
* Overview
* ________
*
* Regular payments processes only those REGP records which have
* the REVDTE <= effective date
*
* The following control totals are maintained within the program:
*
*       CT01 : Number of payments extracted
*       CT02 : Total processed
*       CT03 : Total rejected
*       CT04 : Payments with ineligible status
*       CT05 : Contract with ineligible status
*       CT06 : Components with ineligible status
*
* 1000-INITIALISE SECTION
* _______________________
*
*  -  Tables used are :
*
*  . T5679 - Transaction Codes For Status
*            Key: Transaction Code.
*
*  . T6693 - Allowable Actions for Status
*            Key: Payment Status || CRTABLE
*            CRTABLE may be set to '****'.
*
*  . T6762 - Regular payments Exception codes
*            Key: Program name
*
* 2000-READ SECTION
* _________________
*
* - Read the REGRDTE records for the contract selected
*   sequentially incrementing the control total.
*
* 2500-EDIT SECTION
* _________________
*
*  Check that Regular Payment is in a fit state to be processed
*  by reading T6693 and matching  the current transaction code,
*  against those on  the  extra  data screen.
*  If the transaction  code cannot be found then the Regular
*  Payment may not be processed so go on to the next record.
*
*  If  it  may  be  processed read the associated CHDR and COVR
*  records. Read CHDR with a READR. For the read  of  COVR  use
*  the  Plan  Suffix  from REGP. If this is zero then perform a
*  BEGN on COVR otherwise use READR for a direct read.
*
*  Check that the component is able to be processed by checking
*  its Premium  Status and Risk status code against those on
*  T5679 for the transaction.
*  If the status is invalid add 1 to control total 6.
*
*  3000-UPDATE SECTION
*  ___________________
*
*  Read CHDR, PAYR and REGP with READH. Set the Validflag on CHDR
*  to '2' and perform a REWRT. Increment the TRANNO by 1, set the
*  Validflag back to '1' and write the new CHDR record.
*  Set the Validflag on PAYR to '2' and perform a REWRT.
*  Set the TRANNO the CHDR-TRANNO from above, set the validflag
*  back to '1' and WRITR the new PAYR.
*
*  Write a PTRN record with the new incremented TRANNO.
*
*  Set the Validflag on REGP to '2' and perform a REWRT.
*
*  Set  the  Validflag  back to '1' and place the new TRANNO on
*  REGP. Replace the Regular Payment Status Code on  REGP  with
*  the corresponding Status Code from T6693.
*
*  Perform a WRITR on REGP.
*
*  Read the next REGP record.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5367 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5367");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaSub2 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData index1 = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5679Ix = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaFoundItem = new FixedLengthStringData(1).init("N");
	private Validator foundItem = new Validator(wsaaFoundItem, "Y");

	private FixedLengthStringData wsaaValidTrcode = new FixedLengthStringData(1).init("N");
	private Validator validTrcode = new Validator(wsaaValidTrcode, "Y");

	private FixedLengthStringData wsaaNotProcessPayment = new FixedLengthStringData(1).init("N");
	private Validator notProcessPayment = new Validator(wsaaNotProcessPayment, "Y");

	private FixedLengthStringData wsaaT6693Key1 = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT6693Paystat1 = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 0);
	private FixedLengthStringData wsaaT6693Crtable1 = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key1, 2);
	private static final String h946 = "H946";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t6693 = "T6693";
	private static final String t6762 = "T6762";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;

	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
		/* WSAA-MISCELLANEOUS */
	private String wsaaValidStatus = "";
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaNextPaystat = new FixedLengthStringData(2);
	private ChdrpayTableDAM chdrpayIO = new ChdrpayTableDAM();
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private RegprgpTableDAM regprgpIO = new RegprgpTableDAM();
	private RegrdteTableDAM regrdteIO = new RegrdteTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private T5679rec t5679rec = new T5679rec();
	private T6693rec t6693rec = new T6693rec();
	private T6762rec t6762rec = new T6762rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		notFound2650
	}

	public Dry5367() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		/*START*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		initialise200();
		validateContract300();
		readRegp400();
		finish4000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		wsaaNotProcessPayment.set("N");
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrrgpIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrrgpIO.setFunction(varcom.readr);
		chdrrgpIO.setFormat(formatsInner.chdrrgprec);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrrgpIO.getStatuz());
			drylogrec.params.set(chdrrgpIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void validateContract300()
	{
		validate310();
	}

protected void validate310()
	{
		/* Read T5679 for valid status requirements for transactions.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Validate the new contract status against T5679.*/
		wsaaValidChdr.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
		|| validContract.isTrue()); wsaaT5679Ix.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()], chdrrgpIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
				|| validContract.isTrue()); wsaaT5679Ix.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Ix.toInt()], chdrrgpIO.getPstatcode())) {
						wsaaValidChdr.set("Y");
					}
				}
			}
		}
	}

protected void readRegp400()
	{
		/*READ*/
		regrdteIO.setParams(SPACES);
		regrdteIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		regrdteIO.setChdrnum(drypDryprcRecInner.drypEntity);
		regrdteIO.setRevdte(ZERO);
		regrdteIO.setFormat(formatsInner.regrdterec);
		regrdteIO.setFunction(varcom.begn);
		while ( !(isEQ(regrdteIO.getStatuz(), varcom.endp))) {
			processRegp500();
		}
		
		/*EXIT*/
	}

protected void processRegp500()
	{
		process510();
	}

protected void process510()
	{
		SmartFileCode.execute(appVars, regrdteIO);
		if (isNE(regrdteIO.getStatuz(), varcom.oK)
		&& isNE(regrdteIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(regrdteIO.getStatuz());
			drylogrec.params.set(regrdteIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(drypDryprcRecInner.drypCompany, regrdteIO.getChdrcoy())
		|| isNE(drypDryprcRecInner.drypEntity, regrdteIO.getChdrnum())
		|| isEQ(regrdteIO.getStatuz(), varcom.endp)
		|| isGT(regrdteIO.getRevdte(), drypDryprcRecInner.drypRunDate)) {
			regrdteIO.setStatuz(varcom.endp);
			return ;
		}
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		readTables1000();
		if (!validContract.isTrue()) {
			drycntrec.contotNumber.set(ct05);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			drycntrec.contotNumber.set(ct03);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			drypDryprcRecInner.processUnsuccesful.setTrue();
			dryrDryrptRecInner.r537002Excode.set(t6762rec.regpayExcpIchs);
			dryrDryrptRecInner.r537002Exreport.set("Y");
			reportProcess2900();
			drylogrec.statuz.set(h946);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		processRegp2000();
		if (!notProcessPayment.isTrue()) {
			updateRegp3000();
		}
		regrdteIO.setFunction(varcom.nextr);
	}

protected void readTables1000()
	{
		start1010();
	}

protected void start1010()
	{
		/* Call routine to get processing date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Read T6762 to get valid exception codes.*/
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t6762);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(formatsInner.itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t6762rec.t6762Rec.set(itemIO.getGenarea());
	}

protected void processRegp2000()
	{
		/*START*/
		readT66932600();
		if (notProcessPayment.isTrue()) {
			return ;
		}
		else {
			readChdrCovr2700();
		}
		/*EXIT*/
	}

protected void readT66932600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start2610();
				case notFound2650: 
					notFound2650();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start2610()
	{
		/* Check through T6693 to find allowable actions from current*/
		/* status.*/
		/* If actions from current status found read through array of*/
		/* allowable transactions to determine if current transaction*/
		/* valid.*/
		wsaaFoundItem.set("N");
		wsaaValidTrcode.set("N");
		wsaaT6693Paystat1.set(regrdteIO.getRgpystat());
		wsaaT6693Crtable1.set(regrdteIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6693);
		itdmIO.setItempfx("IT");
		itdmIO.setItemitem(wsaaT6693Key1);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFormat(formatsInner.itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(drypDryprcRecInner.drypCompany, itdmIO.getItemcoy())
		|| isNE(t6693, itdmIO.getItemtabl())
		|| isNE(wsaaT6693Key1, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaT6693Crtable1.set("****");
			itdmIO.setItemitem(wsaaT6693Key1);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				drylogrec.params.set(itdmIO.getParams());
				drylogrec.statuz.set(itdmIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			if (isNE(drypDryprcRecInner.drypCompany, itdmIO.getItemcoy())
			|| isNE(t6693, itdmIO.getItemtabl())
			|| isNE(wsaaT6693Key1, itdmIO.getItemitem())
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				itdmIO.setStatuz(varcom.endp);
				itdmIO.setGenarea(SPACES);
				goTo(GotoLabel.notFound2650);
			}
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		for (index1.set(1); !(isGT(index1, 12)); index1.add(1)){
			if (isEQ(t6693rec.trcode[index1.toInt()], drypDryprcRecInner.drypBatctrcde)) {
				validTrcode.setTrue();
				wsaaNextPaystat.set(t6693rec.rgpystat[index1.toInt()]);
				index1.set(15);
			}
		}
	}

protected void notFound2650()
	{
		if (isEQ(wsaaValidTrcode, "N")) {
			notProcessPayment.setTrue();
			drycntrec.contotNumber.set(ct04);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			drycntrec.contotNumber.set(ct03);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			drypDryprcRecInner.processUnsuccesful.setTrue();
			dryrDryrptRecInner.r537002Excode.set(t6762rec.regpayExcpIpst);
			dryrDryrptRecInner.r537002Exreport.set("Y");
			reportProcess2900();
		}
	}

protected void readChdrCovr2700()
	{
		read2710();
	}

protected void read2710()
	{
		/* Read the coverage record and validate the status against*/
		/* those on T5679.*/
		/* Read in the relevant COVR record.*/
		covrIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covrIO.setChdrnum(regrdteIO.getChdrnum());
		covrIO.setLife(regrdteIO.getLife());
		covrIO.setCoverage(regrdteIO.getCoverage());
		covrIO.setRider(regrdteIO.getRider());
		covrIO.setPlanSuffix(regrdteIO.getPlanSuffix());
		covrIO.setFunction(varcom.readr);
		covrIO.setFormat(formatsInner.covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrIO.getParams());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Validate the coverage statii against T5679.*/
		wsaaValidStatus = "N";
		wsaaStatcode.set(covrIO.getStatcode());
		wsaaPstcde.set(covrIO.getPstatcode());
		for (wsaaSub1.set(1); !(isGT(wsaaSub1, 12)); wsaaSub1.add(1)){
			validateCovrStatus2860();
		}
		if (isEQ(wsaaValidStatus, "N")) {
			drycntrec.contotNumber.set(ct06);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			drycntrec.contotNumber.set(ct03);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			drypDryprcRecInner.processUnsuccesful.setTrue();
			dryrDryrptRecInner.r537002Excode.set(t6762rec.regpayExcpIcos);
			dryrDryrptRecInner.r537002Exreport.set("Y");
			reportProcess2900();
			notProcessPayment.setTrue();
		}
	}

protected void validateCovrStatus2860()
	{
		/*PARA*/
		/* Validate coverage risk status*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub1.toInt()], wsaaStatcode)) {
			for (wsaaSub2.set(1); !(isGT(wsaaSub2, 12)); wsaaSub2.add(1)){
				if (isEQ(t5679rec.covPremStat[wsaaSub2.toInt()], wsaaPstcde)) {
					wsaaSub2.set(13);
					wsaaValidStatus = "Y";
				}
			}
		}
		/*EXIT*/
	}

protected void reportProcess2900()
	{
		start2910();
	}

protected void start2910()
	{
		/* Write detail record to regular payment copybook*/
		/* This will be printed out in the Regular Payments*/
		/* processing program DRY537002. Note that the records*/
		/* may be exception or non-exception records.*/
		dryrDryrptRecInner.r537002Chdrnum.set(regrdteIO.getChdrnum());
		dryrDryrptRecInner.r537002Life.set(regrdteIO.getLife());
		dryrDryrptRecInner.r537002Coverage.set(regrdteIO.getCoverage());
		dryrDryrptRecInner.r537002Rider.set(regrdteIO.getRider());
		dryrDryrptRecInner.r537002Rgpynum.set(regrdteIO.getRgpynum());
		dryrDryrptRecInner.r537002Rgpytype.set(regrdteIO.getRgpytype());
		dryrDryrptRecInner.r537002Rgpystat.set(regrdteIO.getRgpystat());
		dryrDryrptRecInner.r537002Crtable.set(regrdteIO.getCrtable());
		dryrDryrptRecInner.r537002Tranno.set(regrdteIO.getTranno());
		dryrDryrptRecInner.r537002Pymt.set(regrdteIO.getPymt());
		dryrDryrptRecInner.r537002Currcd.set(regrdteIO.getCurrcd());
		dryrDryrptRecInner.r537002Prcnt.set(regrdteIO.getPrcnt());
		dryrDryrptRecInner.r537002Payreason.set(regrdteIO.getPayreason());
		dryrDryrptRecInner.r537002Revdte.set(regrdteIO.getRevdte());
		dryrDryrptRecInner.r537002FirstPaydate.set(regrdteIO.getFirstPaydate());
		dryrDryrptRecInner.r537002LastPaydate.set(regrdteIO.getLastPaydate());
		/* Set up the report name.*/
		/* Move up the sort key.*/
		/* Write the DRPT record.*/
		e000ReportRecords();
	}

protected void updateRegp3000()
	{
		/*UPDATE*/
		/* Perform update processing.*/
		rewriteChdr3200();
		rewritePayr3300();
		writePtrn3400();
		rewriteRegp3500();
		/*EXIT*/
	}

protected void rewriteChdr3200()
	{
		start3210();
	}

protected void start3210()
	{
		/* Obtain the CHDR record using CHDRPAY logical.*/
		chdrpayIO.setParams(SPACES);
		chdrpayIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		chdrpayIO.setChdrnum(chdrrgpIO.getChdrnum());
		chdrpayIO.setFunction(varcom.readh);
		chdrpayIO.setFormat(formatsInner.chdrpayrec);
		SmartFileCode.execute(appVars, chdrpayIO);
		if (isNE(chdrpayIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrpayIO.getParams());
			drylogrec.statuz.set(chdrpayIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Write validflag '2' Contract Header (CHDR) record*/
		chdrpayIO.setValidflag("2");
		chdrpayIO.setCurrto(drypDryprcRecInner.drypRunDate);
		chdrpayIO.setFunction(varcom.rewrt);
		chdrpayIO.setFormat(formatsInner.chdrpayrec);
		SmartFileCode.execute(appVars, chdrpayIO);
		if (isNE(chdrpayIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrpayIO.getParams());
			drylogrec.statuz.set(chdrpayIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Write new contract record with incremented TRANNO*/
		/* and updated SINSTAMT's if required*/
		setPrecision(chdrpayIO.getTranno(), 0);
		chdrpayIO.setTranno(add(chdrpayIO.getTranno(), 1));
		chdrpayIO.setValidflag("1");
		chdrpayIO.setCurrfrom(drypDryprcRecInner.drypRunDate);
		chdrpayIO.setCurrto(varcom.vrcmMaxDate);
		chdrpayIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, chdrpayIO);
		if (isNE(chdrpayIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrpayIO.getParams());
			drylogrec.statuz.set(chdrpayIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void rewritePayr3300()
	{
		start3310();
	}

protected void start3310()
	{
		/* Obtain the PAYR record*/
		payrIO.setParams(SPACES);
		payrIO.setChdrcoy(chdrrgpIO.getChdrcoy());
		payrIO.setChdrnum(chdrrgpIO.getChdrnum());
		payrIO.setPayrseqno(ZERO);
		payrIO.setFunction(varcom.begnh);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)
		&& isNE(payrIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(payrIO.getChdrnum(), chdrrgpIO.getChdrnum())
		|| isNE(payrIO.getChdrcoy(), chdrrgpIO.getChdrcoy())
		|| isNE(payrIO.getValidflag(), "1")
		|| isEQ(payrIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Write validflag '2' Payer (PAYR) record*/
		payrIO.setValidflag("2");
		payrIO.setFunction(varcom.rewrt);
		payrIO.setFormat(formatsInner.payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Write new payer PAYR record with incremented TRANNO*/
		payrIO.setTranno(chdrpayIO.getTranno());
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void writePtrn3400()
	{
		start3410();
	}

protected void start3410()
	{
		/* Create a PTRN record*/
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setDataArea(SPACES);
		varcom.vrcmTermid.set("9999");
		ptrnIO.setTermid(varcom.vrcmTermid);
		ptrnIO.setTransactionDate(datcon1rec.intDate);
		ptrnIO.setTransactionTime(getCobolTime());
		ptrnIO.setUser(999999);
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setPtrneff(regrdteIO.getRevdte());
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setTranno(chdrpayIO.getTranno());
		ptrnIO.setChdrpfx("CH");
		ptrnIO.setChdrcoy(chdrpayIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrpayIO.getChdrnum());
		ptrnIO.setValidflag("1");
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(formatsInner.ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void rewriteRegp3500()
	{
		start3501();
	}

protected void start3501()
	{
		regprgpIO.setParams(SPACES);
		regprgpIO.setStatuz(varcom.oK);
		regprgpIO.setRrn(regrdteIO.getRrn());
		regprgpIO.setFormat(formatsInner.regprgprec);
		regprgpIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, regprgpIO);
		if (isNE(regprgpIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regprgpIO.getParams());
			drylogrec.statuz.set(regprgpIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		regprgpIO.setValidflag("2");
		regprgpIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, regprgpIO);
		if (isNE(regprgpIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regprgpIO.getParams());
			drylogrec.statuz.set(regprgpIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Change validflag back to '1', update the TRANNO with the newl*/
		/* value from the contract, and update the REGP status code*/
		/* with the corresponding code from T6693.*/
		regrdteIO.setValidflag("1");
		regrdteIO.setTranno(chdrpayIO.getTranno());
		regrdteIO.setAprvdate(varcom.vrcmMaxDate);
		regrdteIO.setRgpystat(wsaaNextPaystat);
		regrdteIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, regrdteIO);
		if (isNE(regrdteIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(regrdteIO.getParams());
			drylogrec.statuz.set(regrdteIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* write a non-exception record to the copybook reporting.*/
		dryrDryrptRecInner.r537002Excode.set(SPACES);
		dryrDryrptRecInner.r537002Exreport.set(SPACES);
		reportProcess2900();
		/* Increment the In Review Control Total.*/
		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void finish4000()
	{
		/*FINISH*/
		/** No processing required as yet.*/
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner { 
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
	private FixedLengthStringData chdrrgprec = new FixedLengthStringData(10).init("CHDRRGPREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData regrdterec = new FixedLengthStringData(10).init("REGRDTEREC");
	private FixedLengthStringData regprgprec = new FixedLengthStringData(10).init("REGPRGPREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).init("PTRNREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
	private FixedLengthStringData chdrpayrec = new FixedLengthStringData(10).init("CHDRPAYREC");
}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrReportName = new FixedLengthStringData(10);
	private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r537002DataArea = new FixedLengthStringData(493).isAPartOf(dryrGenarea, 0, REDEFINE);
	private ZonedDecimalData r537002Rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r537002DataArea, 0);
	private ZonedDecimalData r537002Pymt = new ZonedDecimalData(17, 2).isAPartOf(r537002DataArea, 5);
	private FixedLengthStringData r537002Currcd = new FixedLengthStringData(3).isAPartOf(r537002DataArea, 22);
	private ZonedDecimalData r537002Prcnt = new ZonedDecimalData(7, 2).isAPartOf(r537002DataArea, 25);
	private FixedLengthStringData r537002Payreason = new FixedLengthStringData(2).isAPartOf(r537002DataArea, 32);
	private FixedLengthStringData r537002Rgpystat = new FixedLengthStringData(2).isAPartOf(r537002DataArea, 34);
	private FixedLengthStringData r537002Excode = new FixedLengthStringData(4).isAPartOf(r537002DataArea, 149);
	private ZonedDecimalData r537002FirstPaydate = new ZonedDecimalData(8, 0).isAPartOf(r537002DataArea, 153);
	private ZonedDecimalData r537002LastPaydate = new ZonedDecimalData(8, 0).isAPartOf(r537002DataArea, 161);
	private ZonedDecimalData r537002Revdte = new ZonedDecimalData(8, 0).isAPartOf(r537002DataArea, 169);
	private ZonedDecimalData r537002Tranno = new ZonedDecimalData(5, 0).isAPartOf(r537002DataArea, 177);

	private FixedLengthStringData r537002SortKey = new FixedLengthStringData(38);
	private FixedLengthStringData r537002Exreport = new FixedLengthStringData(1).isAPartOf(r537002SortKey, 0);
	private FixedLengthStringData r537002Chdrnum = new FixedLengthStringData(8).isAPartOf(r537002SortKey, 1);
	private FixedLengthStringData r537002Rgpytype = new FixedLengthStringData(2).isAPartOf(r537002SortKey, 9);
	private FixedLengthStringData r537002Crtable = new FixedLengthStringData(4).isAPartOf(r537002SortKey, 11);
	private FixedLengthStringData r537002Life = new FixedLengthStringData(2).isAPartOf(r537002SortKey, 15);
	private FixedLengthStringData r537002Coverage = new FixedLengthStringData(2).isAPartOf(r537002SortKey, 17);
	private FixedLengthStringData r537002Rider = new FixedLengthStringData(2).isAPartOf(r537002SortKey, 19);
	private FixedLengthStringData r537002ReportName = new FixedLengthStringData(10).init("R537002  ");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
}
}
