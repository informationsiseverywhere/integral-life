package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;

import java.util.List;

import com.csc.diaryframework.parent.Maind;
import com.csc.life.productdefinition.dataaccess.dao.HitdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Hitdpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/*
*
      *(C) Copyright CSC Corporation Limited 1986 - 2000.
      *    All rights reserved. CSC Confidential.
      *
      *REMARKS.
      *
      * This is the transaction detail record change subroutine for
      * Contracts that are having an interest Calculation. It will set
      * the DTRD next processing date onto the next calculation date
      * based upon the HITDPF next interest calculation date.
      *
      ***********************************************************************
      *           AMENDMENT  HISTORY                                        *
      ***********************************************************************
      * DATE.... VSN/MOD  WORK UNIT    BY....                               *
      *                                                                     *
      * 21/02/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
      *           Initial Version.                                          *
      *                                                                     *
      **DD/MM/YY*************************************************************
      *
*/
public class Dryibfcal extends Maind{
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData().init("DRYIBFCAL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		
	private ZonedDecimalData wsaaNxtintdte = new ZonedDecimalData(8);
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(2);
	private PackedDecimalData wsaaT5679Max = new PackedDecimalData(2).init(12);
	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4).init(SPACE);
	
	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus,"Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus,"N");
	
	/* ERRORS */
	private static final String f321 = "F321";
	
	/* FORMATS */
	private static final String covrlnbrec = "COVRLNBREC";
	private static final String itemrec = "ITEMREC";
	private static final String hitddryrec = "HITDDRYREC";
		/* TABLES */
	private static final String t5679 = "T5679";	
	
	private T5679rec t5679rec = new T5679rec();
	private ItemTableDAM itemIO = new ItemTableDAM();
//	private HitddryTableDAM hitddryIO = new HitddryTableDAM();
	private Hitdpf hitddryIO = new Hitdpf();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private HitdpfDAO hitdpfDAO = getApplicationContext().getBean("hitdpfDAO" , HitdpfDAO.class);
	
	/**
	* Contains all possible labels used by goTo action.
	*/
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr610, 
		exit690
	}

	public Dryibfcal() {
		super();
	}
	
	/**
	* The mainline method is the default entry point to the class
	*/
	public void mainline(Object... parmArray){
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
	protected void mainline000() {
		main010();
		exit090();
	}

	protected void main010() {
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		wsaaNxtintdte.set(ZERO);
		/* Determine if an HITD Exists. */
		process200();
	}

	protected void exit090() {
		exitProgram();
	}

	protected void process200() {
		proc210();
	}

	protected void proc210() {
		/* Validate the Contract Risk and Premium status against T5679. */
		readT5679300();
		validateStatus400();
		if (validStatus.isTrue()) {
			checkHitds500();
		} else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}

	protected void readT5679300(){
		t5679310();
	}
	
	protected void t5679310(){
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);

		if(!isEQ(itemIO.getStatuz(),varcom.oK)&&!isEQ(itemIO.getStatuz(),varcom.mrnf)){
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		
		if(isEQ(itemIO.getStatuz(),varcom.mrnf)){
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f321);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

	protected void validateStatus400(){
		validate410();
	}
	
	protected void validate410(){
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
				|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
					if (isEQ(drypDryprcRecInner.drypStatcode, t5679rec.cnRiskStat[wsaaT5679Sub.toInt()])) {
						validStatus.setTrue();
					}
				}
		if (validStatus.isTrue()) {
				invalidStatus.setTrue();
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
				|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
					if (isEQ(drypDryprcRecInner.drypPstatcode, t5679rec.cnPremStat[wsaaT5679Sub.toInt()])) {
						validStatus.setTrue();
					}
				}
			}
	}

	private void checkHitds500(){
		check500();
	}
	
	protected void check500(){
/*		hitddryIO.setChdrcoy(drypDryprcRecInner.drypCompany.toString());
		hitddryIO.setChdrnum(drypDryprcRecInner.drypEntity.toString());
		hitddryIO.setLife(ZERO);
		hitddryIO.coverage.set(ZERO);
		hitddryIO.rider.set(ZERO);
		hitddryIO.planSuffix.set(ZERO);
		hitddryIO.setFormat(hitddryrec);
		hitddryIO.setFunction(varcom.begn);
		hitddryIO.setStatuz(varcom.oK);*/

/*		while(!(isEQ(hitddryIO.getStatuz(), varcom.endp))){
			readhitddryIO600();	
		}*/
		
		List<Hitdpf> hitdpfList = hitdpfDAO.searchHitddryRecord(drypDryprcRecInner.drypCompany.toString(),drypDryprcRecInner.drypEntity.toString());
		if(hitdpfList.isEmpty()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return;
		}
		for (Hitdpf pf : hitdpfList) {
			hitddryIO = pf;
			newDate700();
		}
		if (isEQ(wsaaNxtintdte, ZERO)) {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
		
		//ILPI-158
		if(drypDryprcRecInner.dtrdYes.isTrue() && isLTE(wsaaNxtintdte, drypDryprcRecInner.drypRunDate)){
			drypDryprcRecInner.dtrdYes.setTrue();
			drypDryprcRecInner.drypNxtprcdate.set(wsaaNxtintdte);
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		} else{
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}


	protected void readhitddryIO600(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					check610();
				case nextr610:
					nextr610();
				case exit690:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void check610() {
//		wsaaHitddryKey.set(hitddryIO.getRecKeyData());
//		SmartFileCode.execute(appVars, hitddryIO);
//		if (!isEQ(hitddryIO.getStatuz(), varcom.oK)
//				&& !isEQ(hitddryIO.getStatuz(), varcom.endp)) {
//			drylogrec.params.set(wsaaHitddryKey);
//			drylogrec.statuz.set(hitddryIO.getStatuz());
//			drylogrec.dryDatabaseError.setTrue();
//			a000FatalError();//ILPI-61
//		}

		/*
		 * Where there are no matches, or the WSAA-NXTINTDTE is still set to 0,
		 * then exit the section setting the flag to skip creating/updating the
		 * DTRD for the Interest Calculation.
		 */
//		if (!isEQ(hitddryIO.chdrcoy, drypDryprcRecInner.drypCompany)
//				|| !isEQ(hitddryIO.chdrnum, drypDryprcRecInner.drypEntity)
//				|| isEQ(hitddryIO.getStatuz(), varcom.endp)) {
//			if (isEQ(wsaaNxtintdte, ZERO)) {
//				hitddryIO.setStatuz(varcom.endp);
//				drypDryprcRecInner.dtrdNo.setTrue();
//				goTo(GotoLabel.exit690);
//			} else {
//				hitddryIO.setStatuz(varcom.endp);
//				goTo(GotoLabel.exit690);
//			}
//		} else {
//			newDate700();
//		}
	}

	protected void nextr610(){
//		hitddryIO.setFunction(varcom.nextr);
	}
	
	protected void newDate700(){
		start710();
	}
	
	protected void start710() {
		if (isGT(wsaaNxtintdte, hitddryIO.getZnxtintdte())
				|| isEQ(wsaaNxtintdte, ZERO)) {
			wsaaNxtintdte.set(hitddryIO.getZnxtintdte());
			//ILPI-158 STARTS
//			drypDryprcRecInner.drypNxtprcdate.set(hitddryIO.znxtintdte);
//			drypDryprcRecInner.drypNxtprctime.set(ZERO);
			//ILPI-158 ENDS
			drypDryprcRecInner.dtrdYes.setTrue();
		}
	}


//	protected void fatalError() {
//		/* A010-FATAL */
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		} else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		
//		callProgram(Drylog.class, drylogrec.drylogRec);
//		a000FatalError();
//	}
	
	/*
	 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {

		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private FixedLengthStringData drypDetailParams1 = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput1 = new FixedLengthStringData(71).isAPartOf(drypDetailParams1, 0);
		private FixedLengthStringData drypCnttype1 = new FixedLengthStringData(3).isAPartOf(drypDetailInput1, 0);
		private FixedLengthStringData drypBillfreq1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 3);
		private FixedLengthStringData drypBillchnl1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 5);
		private FixedLengthStringData drypStatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 7);
		private FixedLengthStringData drypPstatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 9);
		private PackedDecimalData drypBtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 11);
		private PackedDecimalData drypPtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 16);
		private PackedDecimalData drypBillcd1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 21);
		private PackedDecimalData drypOccdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 46);
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
}


