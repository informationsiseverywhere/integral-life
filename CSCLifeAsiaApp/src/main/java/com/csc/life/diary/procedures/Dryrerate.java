/*
 * File: Dryrerate.java
 * Date: December 3, 2013 2:27:40 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYRERATE.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.diary.dataaccess.CovrrrtTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the transaction detail record change subroutine for
* Coverage Rerate Processing.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryrerate extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYRERATE");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(2, 0).setUnsigned();
	private final int wsaaT5679Max = 12;

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaT5655Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5655Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 0);
	private FixedLengthStringData wsaaT5655Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5655Key, 4);
	private FixedLengthStringData filler1 = new FixedLengthStringData(1).isAPartOf(wsaaT5655Key, 7, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");

	private FixedLengthStringData wsaaCovrValid = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaCovrValid, "Y");
	private Validator invalidCoverage = new Validator(wsaaCovrValid, "N");
	private PackedDecimalData wsaaRrtdate = new PackedDecimalData(8, 0).init(ZERO);
		/* TABLES */
	private static final String t5655 = "T5655";
	private static final String t5679 = "T5679";
	private static final String tr517 = "TR517";
		/* FORMATS */
	private static final String covrrrtrec = "COVRRRTREC";
	private static final String itemrec = "ITEMREC";
		/* ERRORS */
	private static final String h038 = "H038";
	private static final String f321 = "F321";
	private CovrrrtTableDAM covrrrtIO = new CovrrrtTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Freqcpy freqcpy = new Freqcpy();
	private Datcon2rec datcon2rec = new Datcon2rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Itdmkey wsaaItdmKey = new Itdmkey();
	private T5655rec t5655rec = new T5655rec();
	private T5679rec t5679rec = new T5679rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		callCovrrrtio420, 
		exit490
	}

	public Dryrerate() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		/*MAIN*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		/* Check if the contract has a valid status for this transaction.*/
		validate100();
		if (invalidStatus.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		readCovr400();
		rerateDate500();
		/*EXIT*/
		exitProgram();
	}

protected void validate100()
	{
		/*VALD*/
		/* Skip Single Premium Contract                                    */
		if (isEQ(drypDryprcRecInner.drypBillchnl, "N")) {
			invalidStatus.setTrue();
			return ;
		}
		/* Validate the Contract Risk and Premium status against T5679.*/
		readT5679200();
		validateStatus300();
		/*EXIT*/
	}

protected void readT5679200()
	{
		t5679210();
	}

protected void t5679210()
	{
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void validateStatus300()
	{
		/*VALIDATE*/
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(drypDryprcRecInner.drypStatcode, t5679rec.cnRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(drypDryprcRecInner.drypPstatcode, t5679rec.cnPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}

protected void readCovr400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read410();
				case callCovrrrtio420: 
					callCovrrrtio420();
					readTr517430();
				case exit490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read410()
	{
		/* Read the COVRRRT logical to get the earliest Rerate Date*/
		/* for the contract details passed in linkage.*/
		covrrrtIO.setParams(SPACES);
		covrrrtIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covrrrtIO.setChdrnum(drypDryprcRecInner.drypEntity);
		covrrrtIO.setRerateDate(ZERO);
		covrrrtIO.setFormat(covrrrtrec);
		covrrrtIO.setFunction(varcom.begn);
	}

protected void callCovrrrtio420()
	{
		SmartFileCode.execute(appVars, covrrrtIO);
		if (isNE(covrrrtIO.getStatuz(), varcom.oK)
		&& isNE(covrrrtIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covrrrtIO.getParams());
			drylogrec.statuz.set(covrrrtIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		/* For contracts where each component has a rerate date of zero,*/
		/* no coverage records will be found.  In this situation, setting*/
		/* the rerate date to a maximum value ensures that a no processing*/
		/* status is returned to the calling program.*/
		if (isNE(covrrrtIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(covrrrtIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isEQ(covrrrtIO.getStatuz(), varcom.endp)) {
			covrrrtIO.setRerateDate(varcom.vrcmMaxDate);
			goTo(GotoLabel.exit490);
		}
		covrrrtIO.setFunction(varcom.nextr);
	}

protected void readTr517430()
	{
		/* Check if COVR is the WOP component, ignore it!                  */
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrrrtIO.getChdrcoy());
		itdmIO.setItemtabl(tr517);
		itdmIO.setItemitem(covrrrtIO.getCrtable());
		itdmIO.setItmfrm(covrrrtIO.getCrrcd());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), covrrrtIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), tr517)
		|| isNE(itdmIO.getItemitem(), covrrrtIO.getCrtable())) {
			/*NEXT_SENTENCE*/
		}
		else {
			goTo(GotoLabel.callCovrrrtio420);
		}
		invalidCoverage.setTrue();
		validateCovrStatus700();
		if (invalidCoverage.isTrue()) {
			goTo(GotoLabel.callCovrrrtio420);
		}
	}

protected void rerateDate500()
	{
		/*RERATE*/
		if (isEQ(covrrrtIO.getRerateDate(), varcom.vrcmMaxDate)){
			drypDryprcRecInner.dtrdNo.setTrue();
		}
		else{
			adjustRerateDate600();
			drypDryprcRecInner.drypNxtprcdate.set(wsaaRrtdate);
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		}
		/*EXIT*/
	}

protected void adjustRerateDate600()
	{
		adjust610();
	}

protected void adjust610()
	{
		/* Read table T5655 for Lead Days values.*/
		wsaaT5655Batctrcde.set(drypDryprcRecInner.drypSystParm[1]);
		wsaaT5655Cnttype.set(drypDryprcRecInner.drypCnttype);
		initialize(itdmIO.getItemrecKeyData());
		initialize(itdmIO.getItemrecNonKeyData());
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5655);
		itdmIO.setItemitem(wsaaT5655Key);
		itdmIO.setItmfrm(drypDryprcRecInner.drypOccdate);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		/* If the CNTTYPE is not part of the T5655 key, use the*/
		/* generic value of '***' as part of the T5655 key.*/
		if (isNE(itdmIO.getItemitem(), wsaaT5655Key)) {
			wsaaT5655Batctrcde.set(drypDryprcRecInner.drypSystParm[1]);
			wsaaT5655Cnttype.set("***");
			initialize(itdmIO.getItemrecKeyData());
			initialize(itdmIO.getItemrecNonKeyData());
			itdmIO.setStatuz(varcom.oK);
			itdmIO.setItempfx(smtpfxcpy.item);
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(t5655);
			itdmIO.setItemitem(wsaaT5655Key);
			itdmIO.setItmfrm(drypDryprcRecInner.drypOccdate);
			wsaaItdmKey.itdmKey.set(itdmIO.getDataKey());
			itdmIO.setFormat(itemrec);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				drylogrec.statuz.set(itdmIO.getStatuz());
				drylogrec.params.set(itdmIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61 
			}
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemitem(), wsaaT5655Key)
		|| isLT(itdmIO.getItmto(), drypDryprcRecInner.drypOccdate)) {
			drylogrec.statuz.set(h038);
			itdmIO.setDataKey(wsaaItdmKey.itdmKey);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t5655rec.t5655Rec.set(itdmIO.getGenarea());
		/* Call DATCON2 to calculate the RERATE processing date.*/
		compute(datcon2rec.freqFactor, 0).set(mult(t5655rec.leadDays, -1));
		datcon2rec.frequency.set(freqcpy.daily);
		datcon2rec.intDate1.set(covrrrtIO.getRerateDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61 
		}
		/* Pass the converted date to the DRYPRCREC copybook.*/
		wsaaRrtdate.set(datcon2rec.intDate2);
	}

protected void validateCovrStatus700()
	{
		validateStatus710();
	}

protected void validateStatus710()
	{
		/* Validate the coverage risk status.                              */
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrrrtIO.getStatcode())) {
				wsaaT5679Sub.set(13);
				validCoverage.setTrue();
			}
		}
		if (invalidCoverage.isTrue()) {
			return ;
		}
		invalidCoverage.setTrue();
		/* Validate the coverage premium status.                           */
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)); wsaaT5679Sub.add(1)){
			if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrrrtIO.getPstatcode())) {
				wsaaT5679Sub.set(13);
				validCoverage.setTrue();
			}
		}
	}
//Modify for ILPI-61 
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
