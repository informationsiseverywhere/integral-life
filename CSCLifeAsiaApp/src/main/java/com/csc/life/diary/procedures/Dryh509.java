      /*
      *     INTEREST BEARING GERERIC MOVEMENT DIARY PROGRAM
      *     -----------------------------------------------
      *
      * This program uses the contract number from the diary to read
      * the un-processed HITR's.
      *
      * It processes them by calling the Movement processing
      * subroutine.
      *
      * For percentage surrenders, the program calculates the amount to
      * surrender based on the Fund Summary balance on the HITS record.
      *
      * For one hundred percent surrenders, the program brings the
      * Interest Calculated and Capitalised up to date before
      * calculating the amount to surrender.
      *
      * For any sale of funds which is not a surrender, the program
      * checks that sufficient funds are available to effect the sale.
      * If not, depending on the cancellation option, the component may
      * be lapsed, its coverage debt may be increased or this may
      * simply be logged as an error.
      *
      * Any trigger module processing is performed following the call
      * to the movement subroutine.
      *
      *                                                                     *
      ***********************************************************************
      *           AMENDMENT  HISTORY                                        *
      ***********************************************************************
      * DATE.... VSN/MOD  WORK UNIT    BY....                               *
      *                                                                     *
      * 17/10/08  01/01   RETROF       Jacco Landskroon                     *
      *           Retrofit from Asia                                        *
      *                                                                     *
      * 19/11/08  01/01   LA4402       Jacco Landskroon                     *
      *           Introduce accumulation processing to avoid locking        *
      *           contention.                                               *
      *                                                                     *
      * 16/09/09  01/01   LA5191       Shamshir Ahmad                       *
      *           Bugzilla 414.                                             *
      *           Processing of HITRALO file is changed.                    *
      *                                                                     *
      **DD/MM/YY*************************************************************
      */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.diary.recordstructures.Dryrptrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.life.diary.recordstructures.Rh518rec;
import com.csc.life.interestbearing.dataaccess.HifdTableDAM;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrdryTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

public class Dryh509 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYH509");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted,"Y");

	private FixedLengthStringData wsaaDatimeInit = new FixedLengthStringData(26);
	private FixedLengthStringData wsaaDatimeDate = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 0);
	private FixedLengthStringData wsaaDatimeTime = new FixedLengthStringData(10).isAPartOf(wsaaDatimeInit, 10);
	private FixedLengthStringData wsaaUser = new FixedLengthStringData(6).isAPartOf(wsaaDatimeInit, 20);

	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4, 0);

		/* WSAA-HIFD-ARRAY */
	private FixedLengthStringData[] wsaaHifdRec = FLSInittedArray (50, 13);
	private FixedLengthStringData[] wsaaHifdFund = FLSDArrayPartOfArrayStructure(4, wsaaHifdRec, 0);
	private PackedDecimalData[] wsaaHifdZtotfndval = PDArrayPartOfArrayStructure(17, 2, wsaaHifdRec, 4);
	private static final int wsaaHifdSize = 50;

	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);
	private PackedDecimalData ix = new PackedDecimalData(5, 0);

	private FixedLengthStringData wsaaPlanSuff = new FixedLengthStringData (8);
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4).isAPartOf(wsaaPlanSuff, 0);
	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0, REDEFINE);
	private ZonedDecimalData filler = new ZonedDecimalData(2).isAPartOf(wsaaPlanR, 0);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2).isAPartOf(wsaaPlan, 2);
	
	/* ERRORS */
	private static final String g641 = "G641";
	private static final String g255 = "G255";
	private static final String h072 = "H072";
	private static final String h791 = "H791";

	/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t5645 = "T5645";
	private static final String t5688 = "T5688";
	
	/* FORMATS */
	private static final String itemRec = "ITEMREC";
	private static final String itdmRec = "ITDMREC";
	private static final String chdrlifRec = "CHDRLIFREC";
	private static final String hitraloRec = "HITRALOREC";
	private static final String hitrdryRec = "HITRDRYREC";
	private static final String hitsRec = "HITSREC";
	private static final String hifdRec = "HIFDREC";
	private static final String dacmRec = "DACMREC";

		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private static final int ct08 = 8;
	private static final int ct09 = 9;
	private static final int ct10 = 10;
	private static final int ct11 = 11;
	private static final int ct12 = 12;
	private static final int ct13 = 13;

    /*
    * DRYMAINB copybooks
    */
	private Dryrptrec dryrptrec = new Dryrptrec();
	private Rh518rec rh518rec = new Rh518rec();
	private Varcom varcom = new Varcom();

    /*
    * Subroutine copybooks for DRY5111.
    */
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Getdescrec getdescrec = new Getdescrec();
	private Batcuprec batcuprec = new Batcuprec();
	private Udtrigrec udtrigrec = new Udtrigrec();

    /*
    *  Database layouts
    */
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private HitrdryTableDAM hitrdryIO = new HitrdryTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private HifdTableDAM hifdIO = new HifdTableDAM();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();

    /*
    *  Table layouts
    */
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();

	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextr180,
		exit290,
		nextRecord280,
		exit3090,
		exit190,
		triggerProcessing3070,
	}

	public Dryh509() {
		super();
	}

	/**
	 * ILPI-97
	 */
	public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
    /*
    100-START-PROCESSING SECTION.
   ******************************
    110-START.
   */
	protected void startProcessing100() {
		try {
			start110();
		} catch (GOTOException e) {
			/* Expected exception for control flow purposes. */
		}
	}

	protected void start110() {
		drypDryprcRecInner.drypStatuz.set(varcom.oK);

		/*
		 * Read Table T5645
		 */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(SPACES);
		itemIO.setFormat(itemRec);
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itemIO.setFunction(varcom.begn);
		itemIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
				&& isNE(itemIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if (isNE(itemIO.getItemcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(itemIO.getItemtabl(), t5645)
				|| isNE(itemIO.getItemitem(), wsaaProg)
				|| isEQ(itemIO.getStatuz(), varcom.endp)) {
			itemIO.setItemitem(wsaaProg);
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		/*
		 * MOVE ITEM-GENAREA TO T5645-T5645-REC.
		 */
		t5645rec.t5645Rec.set(itemIO.getGenarea());

		/*
		 * Read the Contract Header record.
		 */
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFormat(chdrlifRec);
		chdrlifIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlifIO);

		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		/*
		 * Read T5688
		 */
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFormat(itdmRec);
		itdmIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itdmIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, itdmIO);

		if (isNE(itdmIO.getStatuz(), varcom.oK)
				&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if (isEQ(itdmIO.getStatuz(), varcom.endp)
				|| isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(itdmIO.getItemtabl(), t5688)
				|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())) {
			itdmIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit190);
		}

		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaComponLevelAccounted.set(t5688rec.comlvlacc);

		/*
		 * Initialise constant LIFACMV fields.
		 */
		lifacmvrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.batcbrn.set(drypDryprcRecInner.drypBranch);
		lifacmvrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifacmvrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifacmvrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifacmvrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.crate.set(ZERO);
		lifacmvrec.acctamt.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.contot.set(ZERO);
		lifacmvrec.rcamt.set(ZERO);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(drypDryprcRecInner.drypRunDate);
		lifacmvrec.transactionTime.set(varcom.vrcmTime);
		lifacmvrec.user.set(ZERO);

		/*
		 * Obtain the transaction description from T1688.
		 */
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);

		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(drypDryprcRecInner.drypLanguage);
		getdescrec.function.set("CHECK");

		callProgram(Getdesc.class, getdescrec.getdescRec);

		if (isNE(getdescrec.statuz, varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		} else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}

		hitrdryIO.setParams(SPACES);
		hitrdryIO.setStatuz(varcom.oK);
		hitrdryIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		hitrdryIO.setChdrnum(drypDryprcRecInner.drypEntity);
		hitrdryIO.setEffdate(ZERO);
		hitrdryIO.setFunction(varcom.begn);
		hitrdryIO.setFormat(hitrdryRec);

		while (isNE(hitrdryIO.getStatuz(), varcom.endp)) {
			readFile200();
		}
		updateHifd4000();
		finish5000();
	}

	   /*
	    200-READ-FILE SECTION.
	   ************************
	    210-READ-FILES.
	   */
	protected void readFile200() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readFile210();
				case nextRecord280:
					nextRecord280();
				case exit290:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void nextRecord280() {
		hitrdryIO.setFunction(varcom.nextr);
	}

	protected void readFile210() {
	      /*
	      * Read the records from HITRDRY file
	      * Set up the key for the WSAA- should a system error
	      * for this instalment occur.
	      */
		SmartFileCode.execute(appVars, hitrdryIO);
		if(isNE(hitrdryIO.getStatuz(), varcom.oK)
				&& isNE(hitrdryIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(hitrdryIO.getStatuz());
			drylogrec.params.set(hitrdryIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(isEQ(hitrdryIO.getStatuz(), varcom.endp)
				|| isNE(hitrdryIO.chdrcoy, drypDryprcRecInner.drypCompany)
				|| isNE(hitrdryIO.chdrnum, drypDryprcRecInner.drypEntity)) {
			hitrdryIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit290);
		}

	      /*
          IF  HITRDRY-EFFDATE      > DRYP-RUN-DATE
              GO TO 280-NEXT-RECORD
          END-IF.
          */
		if(isGT(hitrdryIO.effdate, drypDryprcRecInner.drypRunDate)) {
			goTo(GotoLabel.nextRecord280);
		}

	      /*
          MOVE CT01                   TO DRYC-CONTOT-NUMBER.
          MOVE 1                      TO DRYC-CONTOT-VALUE.
          PERFORM D000-CONTROL-TOTALS.
          */
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();

	      /*
	      * If we don't have a fund, we will do no 'dealing' for this
	      * HITR. However, we do still need to process it.
	      */
		if(isEQ(hitrdryIO.getZintbfnd(), SPACES)) {
			drycntrec.contotNumber.set(ct02);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
		}

		update3000();
		hitrdryIO.setFunction(varcom.nextr);
	}

    /*
    PERFORM 3000-UPDATE.
    */
	protected void update3000() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					update3010();
				case nextRecord280:
					nextRecord280();
				case triggerProcessing3070:
					triggerProcessing3070();
				case exit3090:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void update3010(){
	      /*
	      * Check for a reversal (when both fund and contract amounts
	      * were completed by the forward transaction).
	      */
		readhHitr3100();
		if(isEQ(hitraloIO.getStatuz(), varcom.mrnf)) {
			goTo(GotoLabel.exit3090);
		}

		  /*
	      * Process non-invested premium.
	      */
		if(isEQ(hitraloIO.getZintbfnd(), SPACES)) {
			processHitrWithoutFund3200();
			hitraloIO.setFormat(hitraloRec);
			hitraloIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, hitraloIO);
			if(isNE(hitraloIO.getStatuz(), varcom.oK)) {
				drylogrec.statuz.set(hitraloIO.getStatuz());
				drylogrec.params.set(hitraloIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			goTo(GotoLabel.triggerProcessing3070);
		}

	      /*
	      * Process invested premium.
	      */
		readhHits3300();

		/*    Calculate surrender/switch amount.*/
		if (isNE(hitraloIO.getSurrenderPercent(),ZERO)
		&& isEQ(hitsIO.getStatuz(),varcom.oK)) {
			setPrecision(hitraloIO.getFundAmount(), 6);
			hitraloIO.setFundAmount(mult(div(mult(mult(hitsIO.getZcurprmbal(),hitraloIO.getSvp()),hitraloIO.getSurrenderPercent()),100),-1), true);
		}
		
		if (isNE(hitraloIO.getFundAmount(), 0)) {
			zrdecplrec.amountIn.set(hitraloIO.getFundAmount());
			zrdecplrec.currency.set(hitraloIO.getFundCurrency());
			callRounding8000();
			hitraloIO.setFundAmount(zrdecplrec.amountOut);
		}

		completeHitrDetails3400();
		accountingPostings3500();
		hitraloIO.setScheduleName(wsaaProg);
		hitraloIO.setScheduleNumber(0);
		hitraloIO.setFeedbackInd("Y");
		hitraloIO.setFormat(hitraloRec);
		hitraloIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, hitraloIO);
		if(isNE(hitraloIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(hitraloIO.getStatuz());
			drylogrec.params.set(hitraloIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		createRh518Record3800();
		hitsIO.setZcurprmbal(add(hitsIO.getZcurprmbal(),hitraloIO.getFundAmount()));
		hitsIO.setZlstupdt(drypDryprcRecInner.drypRunDate);
		hitsIO.setFormat(hitsRec);
		SmartFileCode.execute(appVars, hitsIO);

		if(isNE(hitsIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(hitsIO.getStatuz());
			drylogrec.params.set(hitsIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		drycntrec.contotNumber.set(ct04);
		drycntrec.contotValue.set(1);
		d000ControlTotals();

		for (ix.set(1); !(isGT(ix,wsaaHifdSize)
		|| isEQ(wsaaHifdFund[ix.toInt()],hitraloIO.getZintbfnd())
		|| isEQ(wsaaHifdFund[ix.toInt()],SPACES)); ix.add(1))
		{
			/*CONTINUE_STMT*/
		}


		if(isGT(ix, wsaaHifdSize)) {
			drylogrec.params.set(hitraloIO.getParams());
			drylogrec.statuz.set(h791);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}


		if (isEQ(wsaaHifdFund[ix.toInt()],SPACES)) {
			wsaaHifdFund[ix.toInt()].set(hitraloIO.getZintbfnd());
			wsaaHifdZtotfndval[ix.toInt()].set(hitraloIO.getFundAmount());
		}
		else {
			wsaaHifdZtotfndval[ix.toInt()].add(hitraloIO.getFundAmount());
		}
	}

       /*
		3070-TRIGGER-PROCESSING.
       *
       * If a trigger module is present on the record, call it to
       * perform any required additional processing.
       */
	protected void triggerProcessing3070() {

		if (isNE(hitrdryIO.getTriggerModule(), SPACES)) {
			callTrigger3700();
		}
	}

	protected void readhHitr3100() {
		
		hitraloIO.setFunction(varcom.readh);
		hitraloIO.setChdrcoy(hitrdryIO.getChdrcoy());
		hitraloIO.setChdrnum(hitrdryIO.getChdrnum());
		hitraloIO.setLife(hitrdryIO.getLife());
		hitraloIO.setCoverage(hitrdryIO.getCoverage());
		hitraloIO.setRider(hitrdryIO.getRider());
		hitraloIO.setPlanSuffix(hitrdryIO.getPlanSuffix());
		hitraloIO.setZintbfnd(hitrdryIO.getZintbfnd());
		hitraloIO.setZrectyp(hitrdryIO.getZrectyp());
		hitraloIO.setProcSeqNo(hitrdryIO.getProcSeqNo());
		hitraloIO.setTranno(hitrdryIO.getTranno());
		hitraloIO.setEffdate(hitrdryIO.getEffdate());
		SmartFileCode.execute(appVars, hitraloIO);
		if(isNE(hitraloIO.getStatuz(), varcom.oK)
			&& isNE(hitraloIO.getStatuz(), varcom.mrnf)){
			drylogrec.statuz.set(hitraloIO.getStatuz());
			drylogrec.params.set(hitraloIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if(isEQ(hitraloIO.getStatuz(), varcom.mrnf)) {
			drycntrec.contotNumber.set(ct03);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
		} 
	}
	

	protected void processHitrWithoutFund3200() {
		processHitrWithoutFund3210();
	}

	protected void processHitrWithoutFund3210() {
		
		lifacmvrec.origcurr.set(hitraloIO.getCntcurr());
		lifacmvrec.origamt.set(hitraloIO.getContractAmount());
		
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(1);
			lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
			drycntrec.contotNumber.set(ct06);
			x1000CallLifacmv();
			t5645Ix.set(5);
			t5645rec.sacscode[5].set(hitraloIO.getSacscode());
			t5645rec.sacstype[5].set(hitraloIO.getSacstyp());
			t5645rec.glmap[5].set(hitraloIO.getGenlcde());
			drycntrec.contotNumber.set(ct07);
			x1000CallLifacmv();
			
		} else {
			t5645Ix.set(8);
			lifacmvrec.substituteCode[6].set(SPACES);
			drycntrec.contotNumber.set(ct06);
			x1000CallLifacmv();
			t5645Ix.set(12);
			t5645rec.sacscode[12].set(hitraloIO.getSacscode());
			t5645rec.sacstype[12].set(hitraloIO.getSacstyp());
			t5645rec.glmap[12].set(hitraloIO.getGenlcde());
			drycntrec.contotNumber.set(ct07);
			x1000CallLifacmv();
		}
		
		hitraloIO.setScheduleName(SPACES);
		hitraloIO.setScheduleNumber(ZERO);
		hitraloIO.setFeedbackInd("Y");
	}


	protected void readhHits3300(){
		readhHits3310();
	}

	protected void readhHits3310() {

		hitsIO.setChdrcoy(hitraloIO.getChdrcoy());
		hitsIO.setChdrnum(hitraloIO.getChdrnum());
		hitsIO.setLife(hitraloIO.getLife());
		hitsIO.setCoverage(hitraloIO.getCoverage());
		hitsIO.setRider(hitraloIO.getRider());
		hitsIO.setPlanSuffix(hitraloIO.getPlanSuffix());
		hitsIO.setZintbfnd(hitraloIO.getZintbfnd());
		hitsIO.setFormat(hitsRec);
		hitsIO.setFunction(varcom.readh);

		SmartFileCode.execute(appVars, hitsIO);

		if (isNE(hitsIO.getStatuz(), varcom.oK)
				&& isNE(hitsIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(hitsIO.getStatuz());
			drylogrec.params.set(hitsIO.getParams());
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

		if (isEQ(hitsIO.statuz, varcom.mrnf)) {
			hitsIO.setFunction(varcom.writr);
			hitsIO.zcurprmbal.set(ZERO);
			
		} else {
			hitsIO.setFunction(varcom.rewrt);
		}
	}

		/*
	    3400-COMPLETE-HITR-DETAILS SECTION.
	   ************************************
	   */


	protected void completeHitrDetails3400() {
		completeHitrDetails3410();
	}

	protected void completeHitrDetails3410() {
	    /*
	    3410-COMPLETE-HITR-DETAILS.
		 *
		 *    First, check if we have an amount in the Contract
		 *    Currency, but no amount in the Fund Currency.
		 *    If yes, then move the Contract Amount to the Fund Amount,
		 *    applying currency conversion if the Fund Currency is not
		 *    equal to the Contract Currency.
		 *
		 */

		if (isNE(hitraloIO.getContractAmount(),ZERO)
		&& isEQ(hitraloIO.getFundAmount(),ZERO)) {
			
			if (isEQ(hitraloIO.getFundCurrency(), hitraloIO.getCntcurr())) {
				hitraloIO.setFundAmount(hitraloIO.getContractAmount());
				hitraloIO.setFundRate(1);
				
			} else {
				conlinkrec.amountIn.set(hitraloIO.getContractAmount());
				conlinkrec.currIn.set(hitraloIO.getCntcurr());
				conlinkrec.currOut.set(hitraloIO.getFundCurrency());
				conlinkrec.cashdate.set(drypDryprcRecInner.drypRunDate);
				
				if (isLT(hitraloIO.getContractAmount(), ZERO)) {
					conlinkrec.function.set("SURR");
					
				} else {
					conlinkrec.function.set("CVRT");
				}
				
				x2000CallXcvrt();
				if (isNE(conlinkrec.amountOut, 0)) {
					zrdecplrec.amountIn.set(conlinkrec.amountOut);
					zrdecplrec.currency.set(hitraloIO.getFundCurrency());
					callRounding8000();
					conlinkrec.amountOut.set(zrdecplrec.amountOut);
				}
				hitraloIO.setFundAmount(conlinkrec.amountOut);
				hitraloIO.setFundRate(conlinkrec.rateUsed);
			}
		}

		/*    Next, check if we have an amount in the Fund*/
		/*    Currency, but no amount in the Contract Currency.*/
		/*    If yes, then move the Fund Amount to the Contract Amount,*/
		/*    applying currenct conversion if the Fund Currency is not*/
		/*    equal to the Contract Currency.*/
		if (isNE(hitraloIO.getFundAmount(),ZERO)
		&& isEQ(hitraloIO.getContractAmount(),ZERO)) {
			
			if (isEQ(hitraloIO.getFundCurrency(), hitraloIO.getCntcurr())) {
				hitraloIO.setContractAmount(hitraloIO.getFundAmount());
				hitraloIO.setFundRate(1);
				
			} else {
				conlinkrec.amountIn.set(hitraloIO.getFundAmount());
				conlinkrec.currIn.set(hitraloIO.getFundCurrency());
				conlinkrec.currOut.set(hitraloIO.getCntcurr());
				conlinkrec.cashdate.set(drypDryprcRecInner.drypRunDate);
				conlinkrec.function.set("CVRT");
				x2000CallXcvrt();
				if (isNE(conlinkrec.amountOut, 0)) {
					zrdecplrec.amountIn.set(conlinkrec.amountOut);
					zrdecplrec.currency.set(hitraloIO.getFundCurrency());
					callRounding8000();
					conlinkrec.amountOut.set(zrdecplrec.amountOut);
				}
				
				hitraloIO.setContractAmount(conlinkrec.amountOut);
				hitraloIO.setFundRate(conlinkrec.rateUsed);
			}
		}
	}


	protected void accountingPostings3500() {
		accountingPostings3510();
	}

	protected void accountingPostings3510() {
		
		if (isNE(hitraloIO.getCntcurr(), hitraloIO.getFundCurrency())) {
			acctForCurrConversion3600();
		}

		/* Investment premium/interest. */
		lifacmvrec.origcurr.set(hitraloIO.getCntcurr());
		lifacmvrec.origamt.set(hitraloIO.getContractAmount());
		
		if (isEQ(hitraloIO.getZrectyp(), "I")) {
			if (componLevelAccounted.isTrue()) {
				t5645Ix.set(6);
				lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
				t5645rec.sacscode[6].set(hitraloIO.getSacscode());
				t5645rec.sacstype[6].set(hitraloIO.getSacstyp());
				t5645rec.glmap[6].set(hitraloIO.getGenlcde());
				
			} else {
				t5645Ix.set(13);
				lifacmvrec.substituteCode[6].set(SPACES);
				t5645rec.sacscode[13].set(hitraloIO.getSacscode());
				t5645rec.sacstype[13].set(hitraloIO.getSacstyp());
				t5645rec.glmap[13].set(hitraloIO.getGenlcde());
			}
			drycntrec.contotNumber.set(ct13);
			
		} else {
			
			if (componLevelAccounted.isTrue()) {
				t5645Ix.set(5);
				lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
				t5645rec.sacscode[5].set(hitraloIO.getSacscode());
				t5645rec.sacstype[5].set(hitraloIO.getSacstyp());
				t5645rec.glmap[5].set(hitraloIO.getGenlcde());
				
			} else {
				t5645Ix.set(12);
				lifacmvrec.substituteCode[6].set(SPACES);
				t5645rec.sacscode[12].set(hitraloIO.getSacscode());
				t5645rec.sacstype[12].set(hitraloIO.getSacstyp());
				t5645rec.glmap[12].set(hitraloIO.getGenlcde());
			}
			
			drycntrec.contotNumber.set(ct08);
		}

		x1000CallLifacmv();

		/* Fund investment. */
		lifacmvrec.origamt.set(hitraloIO.getFundAmount());
		lifacmvrec.origcurr.set(hitraloIO.getFundCurrency());

		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(7);
			lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
			
		} else {
			t5645Ix.set(14);
			lifacmvrec.substituteCode[6].set(SPACES);
		}

		drycntrec.contotNumber.set(ct09);
		x1000CallLifacmv();

	}

	protected void acctForCurrConversion3600() {
		curr3610();
	}

	protected void curr3610() {
		/*    Post the fund amount in its currency. (LIFACMV will look*/
		/*    T3629 to get the rate to convert the ORIGAMT into the*/
		/*    ACCTAMT.)*/

		lifacmvrec.origcurr.set(hitraloIO.getFundCurrency());
		lifacmvrec.origamt.set(hitraloIO.getFundAmount());
		
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(2);
			lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
			
		} else {
			t5645Ix.set(9);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		
		drycntrec.contotNumber.set(ct10);
		x1000CallLifacmv();

		/*    Post the Contract amount in its currency. (ditto).*/
		lifacmvrec.origcurr.set(hitraloIO.getCntcurr());
		lifacmvrec.origamt.set(hitraloIO.getContractAmount());
		
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(3);
			lifacmvrec.substituteCode[6].set(hitraloIO.getCrtable());
			
		} else {
			t5645Ix.set(10);
			lifacmvrec.substituteCode[6].set(SPACES);
		}

		drycntrec.contotNumber.set(ct11);
		x1000CallLifacmv();
	}

	protected void callTrigger3700() {
		callTrigger3710();
	}

	protected void callTrigger3710() {
		udtrigrec.function.set(wsaaProg);
		udtrigrec.statuz.set(varcom.oK);
		udtrigrec.mode.set("BATCH");
		udtrigrec.callingProg.set(wsaaProg);
		udtrigrec.fsuco.set(drypDryprcRecInner.drypCompany);
		udtrigrec.language.set(drypDryprcRecInner.drypLanguage);
		udtrigrec.batchkey.set(drypDryprcRecInner.drypBatchKey);
		udtrigrec.effdate.set(drypDryprcRecInner.drypEffectiveDate);
		udtrigrec.chdrcoy.set(hitrdryIO.getChdrcoy());
		udtrigrec.chdrnum.set(hitrdryIO.getChdrnum());
		udtrigrec.life.set(hitraloIO.getLife());
		udtrigrec.coverage.set(hitraloIO.getCoverage());
		udtrigrec.rider.set(hitraloIO.getRider());
		udtrigrec.planSuffix.set(hitraloIO.getPlanSuffix());
		udtrigrec.unitVirtualFund.set(hitraloIO.getZintbfnd());
		udtrigrec.tranno.set(hitraloIO.getTranno());
		udtrigrec.procSeqNo.set(hitraloIO.getProcSeqNo());
		udtrigrec.triggerKey.set(hitraloIO.getTriggerKey());
		
		callProgram(hitraloIO.getTriggerModule(), udtrigrec.udtrigrecRec);
		
		if (isNE(udtrigrec.statuz,varcom.oK)
				|| isNE(hitraloIO.getTriggerKey(),udtrigrec.triggerKey)) {
			drylogrec.params.set(udtrigrec.udtrigrecRec);
			drylogrec.statuz.set(udtrigrec.statuz);
			drylogrec.drySubrtnError.setTrue();
			a000FatalError();
		}

//	      * Control total for number of Trigger Processing HHITS
		drycntrec.contotNumber.set(ct12);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}


	protected void createRh518Record3800() {
		start3810();
	}
	
	protected void start3810() {
		//ILPI-121 Starts
		dryrDryrptRecInner.dryrReportName.set("RH518");
		dryrDryrptRecInner.rh518Fundamnt.set(hitraloIO.getFundAmount());
		dryrDryrptRecInner.rh518Effdate.set(hitraloIO.getEffdate());
		dryrDryrptRecInner.rh51Fdbkind.set(hitraloIO.getFeedbackInd());
		dryrDryrptRecInner.rh518Chdrcoy.set(hitraloIO.getChdrcoy());
		dryrDryrptRecInner.rh518Chdrnum.set(hitraloIO.getChdrnum());
		dryrDryrptRecInner.rh518Zintbfnd.set(hitraloIO.getZintbfnd());
		dryrDryrptRecInner.rh518Batctrcde.set(hitraloIO.getBatctrcde());
		
		//Set the Sort key values
		dryrDryrptRecInner.rh518chdrcoy.set(hitraloIO.getChdrcoy());
		dryrDryrptRecInner.rh518zintbfnd.set(hitraloIO.getZintbfnd());
		dryrDryrptRecInner.rh518chdrnum.set(hitraloIO.getChdrnum());
		dryrDryrptRecInner.rh518batctrcde.set(hitraloIO.getBatctrcde());
		dryrptrec.sortkey.set(dryrDryrptRecInner.rh518Rh518Key);
		e000ReportRecords();
	}
	//ILPI-121 Ends
	protected void updateHifd4000(){
		hifd4010();
	}

	protected void hifd4010() {

		for (ix.set(1); !(isGT(ix, wsaaHifdSize)	|| isEQ(wsaaHifdFund[ix.toInt()],SPACES)); ix.add(1)) {

			hifdIO.setParams(SPACES);
			hifdIO.setChdrcoy(drypDryprcRecInner.drypCompany);
			hifdIO.setZintbfnd(wsaaHifdFund[ix.toInt()]);
			hifdIO.setZtotfndval(wsaaHifdZtotfndval[ix.toInt()]);
			
			dacmIO.setParams(SPACE);		
			dacmIO.setRecKeyData(SPACE);
			dacmIO.setRecNonKeyData(SPACE);

			dacmIO.setScheduleThreadNo(drypDryprcRecInner.drypThreadNumber);
			dacmIO.setCompany(drypDryprcRecInner.drypCompany);
			dacmIO.setRecformat(hifdRec);
			dacmIO.setDataarea(hifdIO.getDataArea());
			dacmIO.setFunction(varcom.writr);
			dacmIO.setFormat(dacmRec);
			SmartFileCode.execute(appVars, dacmIO);

			if(isNE(dacmIO.getStatuz(),varcom.oK)) {
				drylogrec.statuz.set(dacmIO.getStatuz());
				drylogrec.params.set(dacmIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}

		wsaaHifdFund[ix.toInt()].set(SPACES);
		wsaaHifdZtotfndval[ix.toInt()].set(ZERO);

	}

	/*
	5000-FINISH SECTION.
	 *********************
    5010-SET-PARAMS.
	 *
	 * Update the DRYP fields to determine next processing date.
	 */
	protected void finish5000() {
		setParams5010();
	}
	
	protected void setParams5010() {

		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypAplsupto.set(chdrlifIO.getAplspto());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
	}


	protected void x1000CallLifacmv() {
		x1010Call();
	}

	protected void x1010Call()
	{
			if (isEQ(lifacmvrec.origamt,ZERO)) {
				return ;
			}
			lifacmvrec.function.set("PSTW");
			lifacmvrec.jrnseq.set(wsaaJrnseq);

			if (componLevelAccounted.isTrue()) {
				lifacmvrec.rldgacct.set(SPACES);
				wsaaPlan.set(hitraloIO.getPlanSuffix());
				StringUtil stringVariable1 = new StringUtil();
				stringVariable1.addExpression(hitraloIO.getChdrnum());
				stringVariable1.addExpression(hitraloIO.getLife());
				stringVariable1.addExpression(hitraloIO.getCoverage());
				stringVariable1.addExpression(hitraloIO.getRider());
				stringVariable1.addExpression(wsaaPlan);
				stringVariable1.setStringInto(lifacmvrec.rldgacct);
			}
			else {
				lifacmvrec.rldgacct.set(hitraloIO.getChdrnum());
			}

			lifacmvrec.sacscode.set(t5645rec.sacscode[t5645Ix.toInt()]);
			lifacmvrec.sacstyp.set(t5645rec.sacstype[t5645Ix.toInt()]);
			lifacmvrec.glcode.set(t5645rec.glmap[t5645Ix.toInt()]);
			lifacmvrec.glsign.set(t5645rec.sign[t5645Ix.toInt()]);
			lifacmvrec.contot.set(t5645rec.cnttot[t5645Ix.toInt()]);
			lifacmvrec.rdocnum.set(hitraloIO.getChdrnum());
			lifacmvrec.tranno.set(hitraloIO.getTranno());
			lifacmvrec.tranref.set(hitraloIO.getTranno());
			lifacmvrec.effdate.set(hitraloIO.getEffdate());
			lifacmvrec.substituteCode[1].set(hitraloIO.getCnttyp());
			lifacmvrec.substituteCode[2].set(hitraloIO.getZintbfnd());
			callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);

			if (isNE(lifacmvrec.statuz,varcom.oK)) {
				drylogrec.params.set(lifacmvrec.lifacmvRec);
				drylogrec.statuz.set(lifacmvrec.statuz);
				drylogrec.drySubrtnError.setTrue();
				a000FatalError();
			}
			wsaaJrnseq.add(1);
			/*    Each invocation of this section will have set the control*/
			/*    total which will be used to log the accounting amount.*/
			/*    Simply set the appropriate sign and call contot.*/
			if (isEQ(lifacmvrec.glsign,"-")) {
				compute(drycntrec.contotValue, 2).set(sub(ZERO,lifacmvrec.origamt));
			}
			else {
				drycntrec.contotValue.set(lifacmvrec.origamt);
			}

			d000ControlTotals();

		      /*
		      *    Log number of ACMVs written.
		      */
			drycntrec.contotNumber.set(ct05);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
	}

	protected void x2000CallXcvrt()
	{
		/*
	       X2000-CALL-XCVRT SECTION.
		 **************************
	       X2010-CALL-XCVRT.
		 *
	           MOVE ZEROES                  TO CLNK-AMOUNT-OUT.
	           MOVE DRYP-COMPANY            TO CLNK-COMPANY.
		 *
	           CALL 'XCVRT'                 USING CLNK-CLNK002-REC.
		 *
	           IF CLNK-STATUZ               NOT = O-K
	              MOVE CLNK-CLNK002-REC     TO DRYL-PARAMS
	              MOVE CLNK-STATUZ          TO DRYL-STATUZ
	              SET  DRY-SUBRTN-ERROR     TO TRUE
	              PERFORM A000-FATAL-ERROR
	           END-IF.
		 *
	       X2090-EXIT.
	           EXIT.
		 */
		/*X2010-CALL-XCVRT*/
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(drypDryprcRecInner.drypCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz,varcom.oK)) {
			drylogrec.params.set(conlinkrec.clnk002Rec);
			drylogrec.statuz.set(conlinkrec.statuz);
			drylogrec.drySubrtnError.setTrue();
			a000FatalError();
		}
		/*X2090-EXIT*/
	}

/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
	private static final class DrypDryprcRecInner {

		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
		private FixedLengthStringData drypDetailParams1 = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput1 = new FixedLengthStringData(71).isAPartOf(drypDetailParams1, 0);
		private FixedLengthStringData drypCnttype1 = new FixedLengthStringData(3).isAPartOf(drypDetailInput1, 0);
		private FixedLengthStringData drypBillfreq1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 3);
		private FixedLengthStringData drypBillchnl1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 5);
		private FixedLengthStringData drypStatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 7);
		private FixedLengthStringData drypPstatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 9);
		private PackedDecimalData drypBtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 11);
		private PackedDecimalData drypPtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 16);
		private PackedDecimalData drypBillcd1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 21);
		private PackedDecimalData drypOccdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 46);
	}
	
	protected void callRounding8000()
	{
		/*CALL*/
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(drypDryprcRecInner.drypCompany);
		zrdecplrec.statuz.set(varcom.oK);
		zrdecplrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, varcom.oK)) {
			drylogrec.statuz.set(zrdecplrec.statuz);
			drylogrec.params.set(zrdecplrec.zrdecplRec);
			a000FatalError();
		}
		/*EXIT*/
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
}


