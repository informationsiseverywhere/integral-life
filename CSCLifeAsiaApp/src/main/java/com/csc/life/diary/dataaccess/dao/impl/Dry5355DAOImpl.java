/**
 * 
 */
package com.csc.life.diary.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.diary.dataaccess.dao.Dry5355DAO;
import com.csc.life.diary.dataaccess.model.Dry5355Dto;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * DAO Implementation for Dry5355
 * 
 * @author aashish4
 *
 */
public class Dry5355DAOImpl extends BaseDAOImpl<Dry5355Dto> implements Dry5355DAO {
	
	public Dry5355DAOImpl()
	{
		//Default Constructor
	}
	
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Dry5355DAOImpl.class);

	@Override
	public List<Dry5355Dto> getOverdue(String company, String effDate, String chdrNo, int ptdate) {

		StringBuilder qurery = new StringBuilder();

		qurery.append("SELECT PAYR.CHDRCOY, PAYR.CHDRNUM, PAYR.PAYRSEQNO, PAYR.BTDATE, PAYR.PTDATE, PAYR.BILLCHNL,");
		qurery.append("HD.ZNFOPT FROM PAYRPF AS PAYR LEFT JOIN VM1DTA.HPADPF AS HD ON HD.CHDRCOY = PAYR.CHDRCOY AND ");
		qurery.append("HD.CHDRNUM= PAYR.CHDRNUM AND HD.VALIDFLAG =? WHERE PAYR.BTDATE <> ? AND ");
		qurery.append("PAYR.BTDATE <> ? AND PAYR.BTDATE >= PTDATE AND PAYR.PTDATE <> ? AND PAYR.PTDATE <= ? AND ");
		qurery.append("PAYR.VALIDFLAG = ? AND PAYR.CHDRNUM= ? AND (PAYR.APLSUPR <> ? ");
		qurery.append("OR (PAYR.APLSUPR = ? AND ? > PAYR.APLSPFROM AND ? >=PAYR.APLSPTO)) ");
		qurery.append("AND (PAYR.NOTSSUPR <> ? OR ");
		qurery.append("(PAYR.NOTSSUPR = ? AND ? >  PAYR.NOTSSPFROM AND ? >=PAYR.NOTSSPTO)) AND PAYR.CHDRCOY = ? AND ");
		qurery.append("PAYR.BILLCHNL <> ? AND PAYR.PAYRSEQNO = ? ORDER BY PAYR.CHDRCOY, PAYR.CHDRNUM, PAYR.PAYRSEQNO");
		List<Dry5355Dto> dry5355list = new ArrayList<>();
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try {
			preparedStatement = getPrepareStatement(qurery.toString());
			preparedStatement.setString(1, "1");
			preparedStatement.setInt(2, 0);
			preparedStatement.setInt(3, 99999999);
			preparedStatement.setInt(4, 0);
			preparedStatement.setInt(5, ptdate);
			preparedStatement.setString(6, "1");
			preparedStatement.setString(7, chdrNo);
			preparedStatement.setString(8, "Y");
			preparedStatement.setString(9, "Y");
			preparedStatement.setString(10, effDate);
			preparedStatement.setString(11, effDate);
			preparedStatement.setString(12, "Y");
			preparedStatement.setString(13, "Y");
			preparedStatement.setString(14, effDate);
			preparedStatement.setString(15, effDate);
			preparedStatement.setString(16, company);
			preparedStatement.setString(17, "N");
			preparedStatement.setString(18, "1");
			resultSet = preparedStatement.executeQuery();
			while (resultSet.next()) {
				Dry5355Dto dry5355Dto = new Dry5355Dto();
				dry5355Dto.setChdrcoy(resultSet.getString("CHDRCOY"));
				dry5355Dto.setChdrnum(resultSet.getString("CHDRNUM"));
				dry5355Dto.setPayrseqno(resultSet.getInt("PAYRSEQNO"));
				dry5355Dto.setBtdate(resultSet.getInt("BTDATE"));
				dry5355Dto.setPtdate(resultSet.getInt("PTDATE"));
				dry5355Dto.setBillchnl(resultSet.getString("BILLCHNL"));
				dry5355Dto.setZnfopt(resultSet.getString("ZNFOPT"));
				dry5355list.add(dry5355Dto);

			}
			return dry5355list;
		} catch (SQLException e) {
			LOGGER.error("Error occurred when reading PAYRPF and HPADPF data", e);
			throw new SQLRuntimeException(e);
		}  finally {
			close(preparedStatement, resultSet);
		}
	}
}
