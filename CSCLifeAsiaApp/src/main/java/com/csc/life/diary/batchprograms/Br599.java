/*
 * File: Br599.java
 * Date: December 3, 2013 2:12:05 AM ICT
 * Author: CSC
 *
 * Class transformed from BR599.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.batchprograms;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DhdrchrTableDAM;
import com.csc.diary.procedures.Dryproces;
import com.csc.fsu.general.dataaccess.ChdypfTableDAM;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.flexiblepremium.tablestructures.T5729rec;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.regularprocessing.dataaccess.ChdrrnlTableDAM;
import com.csc.life.regularprocessing.recordstructures.P6671par;
import com.csc.smart.dataaccess.ItdmsmtTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.tablestructures.T7508rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.parent.Mainb;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*  Portfolio Conversion for Diary Batch Processing (Multi Thread)
*  ==============================================================
*
*  OVERVIEW.
*
*  This 'old' style batch program will convert customers entity
*  types over for use on the new Batch Diary System.
*  It will read thru all the  entities from extract file CHDYPF
*  and will then validate each entity with in the range and call
*  'DRYPROCES' for each.
*  This is similar to the way 'DRYPROCES' would be called
*  at Issue (See P5074AT).
*
*  The subroutine will create DHDR and DTRD records using
*  a table, T7500 and other subroutines held on table T7502.
*
*  Control totals used in this program:
*
*  CT01 - No. of Primary Records Read.
*  CT02 - No. of Converted Entities.
*  CT03 - No. of Entities Revisited.
*
*  Reconciliation Rule used in this process:
*
*  DCONVERT - Where CT01 = CT02 + CT03
*
****************************************************************** ****
* </pre>
*/
public class Br599 extends Mainb {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private ChdypfTableDAM chdypf = new ChdypfTableDAM();
	private ChdypfTableDAM chdypfRec = new ChdypfTableDAM();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("BR599");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);
		/*  CHDY parameters*/
	private PackedDecimalData wsaaQcmdexcLength = new PackedDecimalData(15, 5).init(100);
	private FixedLengthStringData wsaaQcmdexc = new FixedLengthStringData(100);

	private FixedLengthStringData wsaaChdyFn = new FixedLengthStringData(10);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaChdyFn, 0, FILLER).init("CHDY");
	private FixedLengthStringData wsaaChdyRunid = new FixedLengthStringData(2).isAPartOf(wsaaChdyFn, 4);
	private ZonedDecimalData wsaaChdyJobno = new ZonedDecimalData(4, 0).isAPartOf(wsaaChdyFn, 6).setUnsigned();

	private FixedLengthStringData wsaaThreadMember = new FixedLengthStringData(10);
	private FixedLengthStringData filler1 = new FixedLengthStringData(6).isAPartOf(wsaaThreadMember, 0, FILLER).init("THREAD");
	private ZonedDecimalData wsaaThreadNumber = new ZonedDecimalData(3, 0).isAPartOf(wsaaThreadMember, 6).setUnsigned();
	private FixedLengthStringData wsaaEntity = new FixedLengthStringData(8);
	private static final String wsaaDryenttp = "CH";
	private PackedDecimalData wsaaCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaRiskCessDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCbunstDate = new PackedDecimalData(8, 0);

	private FixedLengthStringData wsaaFlexPrem = new FixedLengthStringData(1);
	private Validator flexiblePremiumContract = new Validator(wsaaFlexPrem, "Y");
	private Validator notFlexiblePremiumContract = new Validator(wsaaFlexPrem, "N");

	private FixedLengthStringData wsaaDhdrCheck = new FixedLengthStringData(1);
	private Validator revisit = new Validator(wsaaDhdrCheck, "Y");
	private Validator newConversion = new Validator(wsaaDhdrCheck, "N");

	private FixedLengthStringData wsaaT7508Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT7508Transcode = new FixedLengthStringData(4).isAPartOf(wsaaT7508Key, 0);
	private FixedLengthStringData wsaaT7508Entity = new FixedLengthStringData(2).isAPartOf(wsaaT7508Key, 4);
		/* TABLES */
	private FixedLengthStringData t5729 = new FixedLengthStringData(5).init("T5729");
	private static final String t7508 = "T7508";
		/* ERRORS */
	private static final String ivrm = "IVRM";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaBsscrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBsprrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBprdrec = new FixedLengthStringData(1024);
	private FixedLengthStringData lsaaBuparec = new FixedLengthStringData(1024);
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private ChdrrnlTableDAM chdrrnlIO = new ChdrrnlTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private DhdrchrTableDAM dhdrchrIO = new DhdrchrTableDAM();
	private ItdmsmtTableDAM itdmsmtIO = new ItdmsmtTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private P6671par p6671par = new P6671par();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private T5729rec t5729rec = new T5729rec();
	private T7508rec t7508rec = new T7508rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FormatsInner formatsInner = new FormatsInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		covrNextr3165,
		exit3169
	}

	public Br599() {
		super();
	}


protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
	}

protected PackedDecimalData getWsaaCommitCnt() {
	return wsaaCommitCnt;
	}

protected PackedDecimalData getWsaaCycleCnt() {
	return wsaaCycleCnt;
	}

protected FixedLengthStringData getWsspEdterror() {
	return wsspEdterror;
	}

protected FixedLengthStringData getLsaaStatuz() {
	return lsaaStatuz;
	}

protected void setLsaaStatuz(FixedLengthStringData lsaaStatuz) {
	this.lsaaStatuz = lsaaStatuz;
	}

protected FixedLengthStringData getLsaaBsscrec() {
	return lsaaBsscrec;
	}

protected void setLsaaBsscrec(FixedLengthStringData lsaaBsscrec) {
	this.lsaaBsscrec = lsaaBsscrec;
	}

protected FixedLengthStringData getLsaaBsprrec() {
	return lsaaBsprrec;
	}

protected void setLsaaBsprrec(FixedLengthStringData lsaaBsprrec) {
	this.lsaaBsprrec = lsaaBsprrec;
	}

protected FixedLengthStringData getLsaaBprdrec() {
	return lsaaBprdrec;
	}

protected void setLsaaBprdrec(FixedLengthStringData lsaaBprdrec) {
	this.lsaaBprdrec = lsaaBprdrec;
	}

protected FixedLengthStringData getLsaaBuparec() {
	return lsaaBuparec;
	}

protected void setLsaaBuparec(FixedLengthStringData lsaaBuparec) {
	this.lsaaBuparec = lsaaBuparec;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		lsaaBuparec = convertAndSetParam(lsaaBuparec, parmArray, 4);
		lsaaBprdrec = convertAndSetParam(lsaaBprdrec, parmArray, 3);
		lsaaBsprrec = convertAndSetParam(lsaaBsprrec, parmArray, 2);
		lsaaBsscrec = convertAndSetParam(lsaaBsscrec, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			super.mainline();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void restart0900()
	{
		/*RESTART*/
		/*EXIT*/
	}

protected void initialise1000()
	{
		initialise1010();
	}

protected void initialise1010()
	{
		wsspEdterror.set(varcom.oK);
		if (isNE(bprdIO.getRestartMethod(), "1")) {
			syserrrec.statuz.set(ivrm);
			fatalError600();
		}
		/*  Point to correct member of FPCXPF.*/
		wsaaChdyRunid.set(bprdIO.getSystemParam04());
		wsaaChdyJobno.set(bsscIO.getScheduleNumber());
		wsaaThreadNumber.set(bsprIO.getProcessOccNum());
		StringUtil stringVariable1 = new StringUtil();
		stringVariable1.addExpression("OVRDBF FILE(CHDYPF) TOFILE(");
		stringVariable1.addExpression(bprdIO.getRunLibrary(), SPACES);
		stringVariable1.addExpression("/");
		stringVariable1.addExpression(wsaaChdyFn);
		stringVariable1.addExpression(") ");
		stringVariable1.addExpression("MBR(");
		stringVariable1.addExpression(wsaaThreadMember);
		stringVariable1.addExpression(")");
		stringVariable1.addExpression(" SEQONLY(*YES 1000)");
		stringVariable1.setStringInto(wsaaQcmdexc);
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		chdypf.openInput();
		wsaaEntity.set(SPACES);
		wsaaCpiDate.set(varcom.vrcmMaxDate);
		wsaaRiskCessDate.set(varcom.vrcmMaxDate);
		wsaaCbunstDate.set(varcom.vrcmMaxDate);
		bupaIO.setDataArea(lsaaBuparec);
		p6671par.parmRecord.set(bupaIO.getParmarea());
		/*    Read T7508 - Batch Diary System Online Parms*/
		wsaaT7508Key.set(SPACES);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(bsprIO.getCompany());
		itemIO.setItemtabl(t7508);
		wsaaT7508Transcode.set(bprdIO.getSystemParam01());
		wsaaT7508Entity.set(wsaaDryenttp);
		itemIO.setItemitem(wsaaT7508Key);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			syserrrec.params.set(itemIO.getParams());
			syserrrec.statuz.set(itemIO.getStatuz());
			fatalError600();
		}
		t7508rec.t7508Rec.set(itemIO.getGenarea());
	}

protected void readFile2000()
	{
		/*READ-FILE*/
		wsspEdterror.set(varcom.oK);
		/*  Read the records from the temporary CHDY file.*/
		chdypf.read(chdypfRec);
		if (chdypf.isAtEnd()) {
			wsspEdterror.set(varcom.endp);
			return ;
		}
		/*    What we have to read*/
		chdrRead2100();
		/*EXIT*/
	}

protected void chdrRead2100()
	{
		readChdr2110();
	}

protected void readChdr2110()
	{
		chdrrnlIO.setParams(SPACES);
		chdrrnlIO.setChdrcoy(chdypfRec.chdrcoy);
		chdrrnlIO.setChdrnum(chdypfRec.chdrnum);
		chdrrnlIO.setFormat(formatsInner.chdrrnlrec);
		chdrrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrrnlIO);
		if (isNE(chdrrnlIO.getStatuz(), varcom.oK)
		&& isNE(chdrrnlIO.getStatuz(), varcom.mrnf)) {
			syserrrec.statuz.set(chdrrnlIO.getStatuz());
			syserrrec.params.set(chdrrnlIO.getParams());
			fatalError600();
		}
		if (isEQ(chdrrnlIO.getStatuz(), varcom.mrnf)) {
			wsspEdterror.set(SPACES);
			return ;
		}
		wsaaEntity.set(chdrrnlIO.getChdrnum());
	}

protected void edit2500()
	{
		edit2510();
	}

protected void edit2510()
	{
		if (isNE(wsspEdterror, varcom.oK)) {
			return ;
		}
		/*    Increment Control Total 01: 'No. of Primary Records Read'*/
		contotrec.totno.set(ct01);
		contotrec.totval.set(1);
		callContot001();
		/*    Check whether this entity type has already been converted*/
		/*    If so then Control Total 03: 'No. of Entities Revisited'*/
		dhdrchrIO.setParams(SPACES);
		dhdrchrIO.setDiaryEntityCompany(bsprIO.getCompany());
		dhdrchrIO.setDiaryEntityBranch(bupaIO.getBranch());
		dhdrchrIO.setDiaryEntityType(wsaaDryenttp);
		dhdrchrIO.setDiaryEntity(wsaaEntity);
		dhdrchrIO.setFunction(varcom.readr);
		dhdrchrIO.setFormat(formatsInner.dhdrchrrec);
		SmartFileCode.execute(appVars, dhdrchrIO);
		if (isNE(dhdrchrIO.getStatuz(), varcom.oK)
		&& isNE(dhdrchrIO.getStatuz(), varcom.mrnf)) {
			syserrrec.params.set(dhdrchrIO.getParams());
			syserrrec.statuz.set(dhdrchrIO.getStatuz());
			fatalError600();
		}
		if (isEQ(dhdrchrIO.getStatuz(), varcom.oK)) {
			revisit.setTrue();
		}
		else {
			newConversion.setTrue();
		}
	}

protected void update3000()
	{
		update3010();
	}

protected void update3010()
	{
		/*    To carry out the conversion, fill the fields relevant to*/
		/*    the type of 'CH' being processed and then call DRYPROCES.*/
		initialize(drypDryprcRecInner.drypDetailInput);
		chdrFields3100();
		drypDryprcRecInner.drypEntity.set(wsaaEntity);
		drypDryprcRecInner.drypEntityType.set(wsaaDryenttp);
		drypDryprcRecInner.drypProcCode.set(t7508rec.proces01);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.onlineMode.setTrue();
		drypDryprcRecInner.drypCompany.set(bsprIO.getCompany());
		drypDryprcRecInner.drypFsuCompany.set(bsprIO.getCompany());
		drypDryprcRecInner.drypBranch.set(bupaIO.getBranch());
		drypDryprcRecInner.drypLanguage.set(bsscIO.getLanguage());
		drypDryprcRecInner.drypEffectiveDate.set(bsscIO.getEffectiveDate());
		drypDryprcRecInner.drypRunDate.set(bsscIO.getEffectiveDate());
		drypDryprcRecInner.drypEffectiveTime.set(ZERO);
		drypDryprcRecInner.drypEntityType.set(wsaaDryenttp);
		drypDryprcRecInner.drypBatchKey.set(SPACES);
		drypDryprcRecInner.drypBatcactyr.set(ZERO);
		drypDryprcRecInner.drypBatcactmn.set(ZERO);
		drypDryprcRecInner.drypProcSeqNo.set(100);
		callProgram(Dryproces.class, drypDryprcRecInner.drypDryprcRec);
		if (isNE(drypDryprcRecInner.drypStatuz, varcom.oK)) {
			syserrrec.params.set(drypDryprcRecInner.drypDryprcRec);
			syserrrec.statuz.set(drypDryprcRecInner.drypStatuz);
			fatalError600();
		}
		/*    Log this in Control Total 02: 'No. of Converted Entities'*/
		if (newConversion.isTrue()) {
			contotrec.totno.set(ct02);
			contotrec.totval.set(1);
			callContot001();
		}
		else {
			contotrec.totno.set(ct03);
			contotrec.totval.set(1);
			callContot001();
		}
	}

protected void chdrFields3100()
	{
		diaryProcessChdr3110();
	}

protected void diaryProcessChdr3110()
	{
		/*    Fill in any parameters specfic to contract entities*/
		drypDryprcRecInner.drypBillchnl.set(chdrrnlIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrrnlIO.getBillfreq());
		drypDryprcRecInner.drypStatcode.set(chdrrnlIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrrnlIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrrnlIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrrnlIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrrnlIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrrnlIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdypfRec.occdate);
		chdrlnbIO.setParams(SPACES);
		chdrlnbIO.setChdrcoy(bsprIO.getCompany());
		chdrlnbIO.setChdrnum(chdrrnlIO.getChdrnum());
		chdrlnbIO.setFormat(formatsInner.chdrlnbrec);
		chdrlnbIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(chdrlnbIO.getStatuz());
			syserrrec.params.set(chdrlnbIO.getParams());
			fatalError600();
		}
		drypDryprcRecInner.drypStmdte.set(chdrlnbIO.getStatementDate());
		readT57293150();
		if (flexiblePremiumContract.isTrue()) {
			datcon2rec.frequency.set("01");
			datcon2rec.intDate1.set(chdypfRec.occdate);
			datcon2rec.freqFactor.set(1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				syserrrec.params.set(datcon2rec.datcon2Rec);
				syserrrec.statuz.set(datcon2rec.statuz);
				fatalError600();
			}
			drypDryprcRecInner.drypTargto.set(datcon2rec.intDate2);
		}
		else {
			drypDryprcRecInner.drypTargto.set(varcom.vrcmMaxDate);
		}
		/*    Read through all the coverage records for the contract and*/
		/*    get the lowest CPI date for Auto Increases.*/
		wsaaCpiDate.set(varcom.vrcmMaxDate);
		wsaaRiskCessDate.set(varcom.vrcmMaxDate);
		wsaaCbunstDate.set(varcom.vrcmMaxDate);
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(chdrrnlIO.getChdrcoy());
		covrIO.setChdrnum(chdrrnlIO.getChdrnum());
		covrIO.setLife("01");
		covrIO.setCoverage("01");
		covrIO.setRider("00");
		covrIO.setPlanSuffix(0);
		covrIO.setFunction(varcom.begn);
		covrIO.setFormat(formatsInner.covrrec);
		while ( !(isEQ(covrIO.getStatuz(), varcom.endp))) {
			getCpiDate3160();
		}

		drypDryprcRecInner.drypCpiDate.set(wsaaCpiDate);
		drypDryprcRecInner.drypRcesdte.set(wsaaRiskCessDate);
		drypDryprcRecInner.drypCbunst.set(wsaaCbunstDate);
		/*    Read PAYR file.*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrrnlIO.getChdrcoy());
		payrIO.setChdrnum(chdrrnlIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(formatsInner.payrrec);
		payrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			syserrrec.statuz.set(payrIO.getStatuz());
			syserrrec.params.set(payrIO.getParams());
			fatalError600();
		}
		drypDryprcRecInner.drypAplsupto.set(payrIO.getAplspto());
	}

protected void readT57293150()
	{
		start3151();
	}

protected void start3151()
	{
		notFlexiblePremiumContract.setTrue();
		/*  Read T5729.*/
		itdmsmtIO.setStatuz(varcom.oK);
		itdmsmtIO.setItemcoy(chdrrnlIO.getChdrcoy());
		itdmsmtIO.setItemtabl(t5729);
		itdmsmtIO.setItemitem(chdrrnlIO.getCnttype());
		itdmsmtIO.setItmto(0);
		itdmsmtIO.setItmfrm(chdypfRec.occdate);
		itdmsmtIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmsmtIO);
		if (isNE(itdmsmtIO.getStatuz(), varcom.oK)
		&& isNE(itdmsmtIO.getStatuz(), varcom.endp)) {
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(chdrrnlIO.getChdrcoy(), SPACES);
			stringVariable1.addExpression(t5729, SPACES);
			stringVariable1.addExpression(chdrrnlIO.getCnttype(), SPACES);
			stringVariable1.setStringInto(syserrrec.params);
			syserrrec.statuz.set(itdmsmtIO.getStatuz());
			fatalError600();
		}
		if (isEQ(itdmsmtIO.getStatuz(), varcom.endp)
		|| isNE(itdmsmtIO.getItemcoy(), chdrrnlIO.getChdrcoy())
		|| isNE(itdmsmtIO.getItemtabl(), t5729)
		|| isNE(itdmsmtIO.getItemitem(), chdrrnlIO.getCnttype())
		|| isGT(itdmsmtIO.getItmfrm(), chdypfRec.occdate)
		|| isLT(itdmsmtIO.getItmto(), chdypfRec.occdate)) {
			return ;
		}
		t5729rec.t5729Rec.set(itdmsmtIO.getGenarea());
		flexiblePremiumContract.setTrue();
	}

protected void getCpiDate3160()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					cpi3161();
				case covrNextr3165:
					covrNextr3165();
				case exit3169:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void cpi3161()
	{
		/*    Read through all valid COVR records to get the lowest*/
		/*    CPI and Risk Cessation dates.*/
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)
		&& isNE(covrIO.getStatuz(), varcom.endp)) {
			syserrrec.params.set(covrIO.getParams());
			syserrrec.statuz.set(covrIO.getStatuz());
			fatalError600();
		}
		if (isNE(covrIO.getChdrcoy(), chdrrnlIO.getChdrcoy())
		|| isNE(covrIO.getChdrnum(), chdrrnlIO.getChdrnum())
		|| isEQ(covrIO.getStatuz(), varcom.endp)) {
			covrIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit3169);
		}
		if (isNE(covrIO.getValidflag(), "1")) {
			goTo(GotoLabel.covrNextr3165);
		}
		if (isLT(covrIO.getCpiDate(), wsaaCpiDate)) {
			wsaaCpiDate.set(covrIO.getCpiDate());
		}
		if (isLT(covrIO.getRiskCessDate(), wsaaRiskCessDate)) {
			wsaaRiskCessDate.set(covrIO.getRiskCessDate());
		}
		if (isLT(covrIO.getUnitStatementDate(), wsaaCbunstDate)
		&& isEQ(covrIO.getBonusInd(), "P")) {
			wsaaCbunstDate.set(covrIO.getUnitStatementDate());
		}
	}

protected void covrNextr3165()
	{
		covrIO.setFunction(varcom.nextr);
	}

protected void commit3500()
	{
		/*COMMIT*/
		/*EXIT*/
	}

protected void rollback3600()
	{
		/*ROLL*/
		/*EXIT*/
	}

protected void close4000()
	{
		/*CLOSE-FILES*/
		chdypf.close();
		wsaaQcmdexc.set("DLTOVR FILE(CHDYPF)");
		com.quipoz.COBOLFramework.command.CommandExecutor.execute(wsaaQcmdexc, wsaaQcmdexcLength);
		lsaaStatuz.set(varcom.oK);
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure FORMATS--INNER
 */
private static final class FormatsInner {
	private FixedLengthStringData chdrrnlrec = new FixedLengthStringData(10).init("CHDRRNLREC");
	private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).init("COVRREC");
	private FixedLengthStringData dhdrchrrec = new FixedLengthStringData(10).init("DHDRCHRREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).init("PAYRREC");
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypTargto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 26);
	private PackedDecimalData drypCpiDate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 31);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	private PackedDecimalData drypRcesdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 61);
	private PackedDecimalData drypCbunst = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 66);
}
}
