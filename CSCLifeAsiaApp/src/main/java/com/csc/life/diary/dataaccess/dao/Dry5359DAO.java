package com.csc.life.diary.dataaccess.dao;

import java.util.List;
import com.csc.life.diary.dataaccess.model.Dry5359Dto;

/**
 * DAO To Fetch Records For Flexible Premium Collection
 * 
 * @author svemula29
 *
 */
public interface Dry5359DAO {
	
	/**
	 * Get FPRM details
	 * 
	 * @param validFlag
	 *            - ValidFlag
	 * 
	 * @param chdrCoy
	 *            - Company
	 * 
	 * @param currFrom
	 *            - CurrFrom
	 * 
	 * @param currTo
	 *            - CurrTo
	 *            
	 * @param chdrNum
	 *            - Contract Number
	 *                      
	 * @return - List of Dry5359DTO
	 */
	public List<Dry5359Dto> getFprmDetails(String validFlag,String chdrCoy,int currFrom,int currTo,String chdrNum);

}
