package com.csc.life.diary.recordstructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Wed, 26 Mar 2014 15:27:50
 * Description:
 * Copybook name: R5138REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class R5138rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dataArea = new FixedLengthStringData(497);
  	public FixedLengthStringData cmdate = new FixedLengthStringData(10).isAPartOf(dataArea, 0);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(dataArea, 9);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(dataArea, 13);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(dataArea, 21);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(dataArea, 23);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(dataArea, 25);
  	public ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(dataArea, 27);
  	public FixedLengthStringData rasnum = new FixedLengthStringData(8).isAPartOf(dataArea, 31);
  	public FixedLengthStringData rngmnt = new FixedLengthStringData(4).isAPartOf(dataArea, 39);
  	public ZonedDecimalData raamount = new ZonedDecimalData(15, 2).isAPartOf(dataArea, 43);
  	public FixedLengthStringData filler = new FixedLengthStringData(439).isAPartOf(dataArea, 58, FILLER);
  
  	public FixedLengthStringData sortKey = new FixedLengthStringData(535);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(38).isAPartOf(sortKey, 497, FILLER);
  	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R5138     ");


	public void initialize() {
		COBOLFunctions.initialize(cmdate);
		COBOLFunctions.initialize(crtable);
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(life);
		COBOLFunctions.initialize(coverage);
		COBOLFunctions.initialize(rider);
		COBOLFunctions.initialize(plnsfx);
		COBOLFunctions.initialize(rasnum);
		COBOLFunctions.initialize(rngmnt);
		COBOLFunctions.initialize(raamount);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(filler1);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		cmdate.isAPartOf(baseString, true);
    		crtable.isAPartOf(baseString, true);
    		chdrnum.isAPartOf(baseString, true);
    		life.isAPartOf(baseString, true);
    		coverage.isAPartOf(baseString, true);
    		rider.isAPartOf(baseString, true);
    		plnsfx.isAPartOf(baseString, true);
    		rasnum.isAPartOf(baseString, true);
    		rngmnt.isAPartOf(baseString, true);
    		raamount.isAPartOf(baseString, true);
    		filler.isAPartOf(baseString, true);
    		filler1.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}