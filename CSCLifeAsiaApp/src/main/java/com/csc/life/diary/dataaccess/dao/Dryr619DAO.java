package com.csc.life.diary.dataaccess.dao;

import java.util.List;
import com.csc.life.diary.dataaccess.model.Dryr619Dto;

/**
 * DAO To Fetch Records For Anniversary Renewals Processing
 * 
 * @author ptrivedi8
 *
 */
public interface Dryr619DAO {

	/**
	 * Get coverage details
	 * 
	 * @param company
	 *            - Company
	 * 
	 * @param chdrNum
	 *            - Contract Number
	 * 
	 * @param effDate
	 *            - Effective date
	 * 
	 * @return - List of Dryr619DTO
	 */
	public List<Dryr619Dto> getCovrDetails(String company, String chdrNum, int effDate);
}
