/*
 * File: Dryr588.java
 * Date: December 3, 2013 2:26:01 AM ICT
 * Author: CSC
 *
 * Class transformed from DRYR588.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.AgcmbchTableDAM;
import com.csc.life.contractservicing.dataaccess.BfrqTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.tablestructures.T5541rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.LextTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.recordstructures.Mgfeelrec;
import com.csc.life.productdefinition.tablestructures.T5567rec;
import com.csc.life.productdefinition.tablestructures.T5664rec;
import com.csc.life.productdefinition.tablestructures.T5674rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.productdefinition.tablestructures.Tr517rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*
*REMARKS
*
*       APL - Advance Batch Frequency Change
*       ------------------------------------
* This program makes up part of the Diary System.
* Its function is to simulate the BR588 Batch process
* that is to make the frequency change on policies under
* APL-advance,
*
*****************************************************************
*
* </pre>
*/
public class Dryr588 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYR588");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaAplBfrq = new FixedLengthStringData(1);
	private Validator aplBfrqNotDone = new Validator(wsaaAplBfrq, "N");
	private Validator aplBfrqDone = new Validator(wsaaAplBfrq, "Y");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1);
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidBfrq = new FixedLengthStringData(1);
	private Validator validBfrq = new Validator(wsaaValidBfrq, "Y");
		/*  Subscripts*/
	private FixedLengthStringData wsaaTr517Key = new FixedLengthStringData(4).init(SPACES);
	private String wsaaWopRiderFlag = "N";
	private PackedDecimalData wsaaShortTransDate = new PackedDecimalData(6, 0).setUnsigned();
	private ZonedDecimalData wsaaRate = new ZonedDecimalData(8, 2).init(ZERO).setUnsigned();
	private String wsaaWaiverFlag = "N";
	private PackedDecimalData wsaaWopPrem = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaWopSi = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaNewFreqFee = new PackedDecimalData(17, 2).init(0);
	private PackedDecimalData wsaaAgerateTot = new PackedDecimalData(5, 0).init(0);
	private PackedDecimalData wsaaRatesPerMillieTot = new PackedDecimalData(7, 0).init(0);
	private PackedDecimalData wsaaAdjustedAge = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaOldFreqFee = new PackedDecimalData(17, 2).init(0);
	private String wsaaWopAccrFlag = "N";
	private String wsaaWopAccrFee = "N";

	private FixedLengthStringData wsaaT5567Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5567Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5567Key, 0);
	private FixedLengthStringData wsaaT5567Cntcurr = new FixedLengthStringData(3).isAPartOf(wsaaT5567Key, 3);

	private FixedLengthStringData wsaaT5664Key = new FixedLengthStringData(6);
	private FixedLengthStringData wsaaT5664Crtable = new FixedLengthStringData(4).isAPartOf(wsaaT5664Key, 0);
	private FixedLengthStringData wsaaT5664Mortcls = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 4);
	private FixedLengthStringData wsaaT5664Sex = new FixedLengthStringData(1).isAPartOf(wsaaT5664Key, 5);

		/* WSAA-LEXT-OPPC-RECS */
	private FixedLengthStringData[] wsaaLextOppcs = FLSInittedArray (8, 3);
	private PackedDecimalData[] wsaaLextOppc = PDArrayPartOfArrayStructure(5, 2, wsaaLextOppcs, 0);

		/* WSAA-R-TABLE */
	private FixedLengthStringData[] wsaaCompTable = FLSInittedArray (10, 27);
	private FixedLengthStringData[] wsaaLife = FLSDArrayPartOfArrayStructure(2, wsaaCompTable, 0);
	private FixedLengthStringData[] wsaaCoverage = FLSDArrayPartOfArrayStructure(2, wsaaCompTable, 2);
	private FixedLengthStringData[] wsaaCrtable = FLSDArrayPartOfArrayStructure(4, wsaaCompTable, 4);
	private FixedLengthStringData[] wsaaRider = FLSDArrayPartOfArrayStructure(2, wsaaCompTable, 8);
	private ZonedDecimalData[] wsaaPremAmt = ZDArrayPartOfArrayStructure(17, 2, wsaaCompTable, 10);
		/* WSAA-TRANSACTION-REC */
	private PackedDecimalData wsaaAtTransactionDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaAtTransactionTime = new PackedDecimalData(6, 0);
	private PackedDecimalData wsaaAtUser = new PackedDecimalData(6, 0);
	private FixedLengthStringData wsaaAtTermid = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaAtBillfreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaAtToday = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaTranid = new FixedLengthStringData(16);
	private ZonedDecimalData wsaaTermid = new ZonedDecimalData(4, 0).isAPartOf(wsaaTranid, 0).setUnsigned();
	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 4).setUnsigned();
	private ZonedDecimalData wsaaUser = new ZonedDecimalData(6, 0).isAPartOf(wsaaTranid, 10).setUnsigned();
		/* ERRORS */
	private static final String f151 = "F151";
	private static final String f290 = "F290";
	private static final String f294 = "F294";
	private static final String f358 = "F358";
	private static final String h966 = "H966";
		/* TABLES */
	private static final String t5541 = "T5541";
	private static final String t5567 = "T5567";
	private static final String t5664 = "T5664";
	private static final String t5674 = "T5674";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String tr517 = "TR517";
	private static final String itemrec = "ITEMREC";
	private static final String itdmrec = "ITEMREC";
	private static final String lextrec = "LEXTREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String agcmbchrec = "AGCMBCHREC";
	private static final String covrmjarec = "COVRMJAREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String payrrec = "PAYRREC";
	private static final String bfrqrec = "BFRQREC";
	private AgcmbchTableDAM agcmbchIO = new AgcmbchTableDAM();
	private BfrqTableDAM bfrqIO = new BfrqTableDAM();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LextTableDAM lextIO = new LextTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private Varcom varcom = new Varcom();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Mgfeelrec mgfeelrec = new Mgfeelrec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private T5541rec t5541rec = new T5541rec();
	private T5567rec t5567rec = new T5567rec();
	private T5664rec t5664rec = new T5664rec();
	private T5674rec t5674rec = new T5674rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr517rec tr517rec = new Tr517rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private WsaaMiscellaneousInner wsaaMiscellaneousInner = new WsaaMiscellaneousInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextrCovr1370,
		wopAccr1550,
		updateStatus1580,
		retrieveNextRecord1680,
		nextrCovr2060,
		a110Check
	}

	public Dryr588() {
		super();
	}


	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}



protected void startProcessing100()
	{
		start110();
	}

protected void start110()
	{
		initialise200();
		if (!validContract.isTrue()) {
			finish6000();
			return ;
		}
		processContractHeader1000();
		processPayrRecord1100();
		getFirstComponent1200();
		wsaaMiscellaneousInner.wsaaI.set(ZERO);
		wsaaWopRiderFlag = "N";
		wsaaWopAccrFlag = "N";
		wsaaWopAccrFee = "N";
		covrmjaIO.setStatuz(SPACES);
		while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
			processCompNwop1300();
		}

		if (isEQ(wsaaWopRiderFlag, "Y")) {
			getFirstComponent1200();
			covrmjaIO.setStatuz(SPACES);
			while ( !(isEQ(covrmjaIO.getStatuz(), varcom.endp))) {
				processCompWop2000();
			}

		}
		accumInstpremWrtChdr4000();
		writeHistory5000();
		a000Statistics();
		a500UpdatBfrq();
		finish6000();
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		/* Fetch the CHDR record for the entity parameters passed.*/
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Check if contract valid*/
		validateContract400();
		if (!validContract.isTrue()) {
			return ;
		}
		wsaaValidBfrq.set("N");
		bfrqIO.setParams(SPACES);
		bfrqIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		bfrqIO.setChdrnum(drypDryprcRecInner.drypEntity);
		bfrqIO.setEffdate(0);
		bfrqIO.setFunction(varcom.begn);
		bfrqIO.setFormat(bfrqrec);
		SmartFileCode.execute(appVars, bfrqIO);
		if (isNE(bfrqIO.getStatuz(), varcom.oK)
		&& isNE(bfrqIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(bfrqIO.getParams());
			drylogrec.statuz.set(bfrqIO.getStatuz());
			a000FatalError();
		}
		if (isNE(bfrqIO.getStatuz(), varcom.endp)
		&& isEQ(bfrqIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		&& isEQ(bfrqIO.getChdrnum(), drypDryprcRecInner.drypEntity)) {
			wsaaValidBfrq.set("Y");
			wsaaAtBillfreq.set(bfrqIO.getBillfreq());
		}
		if (!validBfrq.isTrue()) {
			return ;
		}
		/* Read the PAYR file*/
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		payrIO.setChdrnum(drypDryprcRecInner.drypEntity);
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.params.set(payrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*    Read Table T5679*/
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		else {
			t5679rec.t5679Rec.set(itemIO.getGenarea());
		}
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(chdrlifIO.getChdrcoy());
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), chdrlifIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5688)
		|| isNE(itdmIO.getItemitem(), chdrlifIO.getCnttype())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setStatuz(f290);
		}
		else {
			t5688rec.t5688Rec.set(itdmIO.getGenarea());
		}
		wsaaMiscellaneousInner.wsaaTotInstprem.set(ZERO);
		for (wsaaMiscellaneousInner.wsaaI.set(1); !(isGT(wsaaMiscellaneousInner.wsaaI, 10)); wsaaMiscellaneousInner.wsaaI.add(1)){
			wsaaCrtable[wsaaMiscellaneousInner.wsaaI.toInt()].set(SPACES);
			wsaaLife[wsaaMiscellaneousInner.wsaaI.toInt()].set(SPACES);
			wsaaPremAmt[wsaaMiscellaneousInner.wsaaI.toInt()].set(ZERO);
		}
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaAtTransactionDate.set(datcon1rec.intDate);
		wsaaAtTransactionTime.set(varcom.vrcmTime);
		wsaaAtUser.set(ZERO);
		wsaaAtTermid.set(SPACES);
		/* Truncate left two digits of date, the century, to leave YYMMDD*/
		/* format.  WSAA-AT-TRANSACTION-DATE then be in CCYYMMDD format,*/
		/* whilst WSAA-SHORT-TRANS-DATE will be in YYMMDD format.*/
		wsaaShortTransDate.set(wsaaAtTransactionDate);
		/* Initialise common PTRN fields*/
		ptrnIO.setParams(SPACES);
		ptrnIO.setUser(0);
		ptrnIO.setValidflag("1"); //IJTI-1765
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setTransactionDate(drypDryprcRecInner.drypRunDate);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		varcom.vrcmTime.set(getCobolTime());
		ptrnIO.setTransactionTime(varcom.vrcmTime);
		aplBfrqNotDone.setTrue();
	}

protected void calcFee300()
	{
		readSubroutineTable310();
	}

protected void readSubroutineTable310()
	{
		/*    Reference T5674 to obtain the subroutine required to work*/
		/*    out the Fee amount by the correct method.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getChdrcoy());
		itemIO.setItemtabl(t5674);
		itemIO.setItemitem(t5688rec.feemeth);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f151);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			return ;
		}
		t5674rec.t5674Rec.set(itemIO.getGenarea());
		/* Check subroutine NOT = SPACES before attempting call.*/
		if (isEQ(t5674rec.commsubr, SPACES)) {
			return ;
		}
		mgfeelrec.mgfeelRec.set(SPACES);
		mgfeelrec.effdate.set(ZERO);
		mgfeelrec.mgfee.set(ZERO);
		mgfeelrec.cnttype.set(chdrlifIO.getCnttype());
		mgfeelrec.billfreq.set(chdrlifIO.getBillfreq());
		mgfeelrec.effdate.set(chdrlifIO.getOccdate());
		mgfeelrec.cntcurr.set(chdrlifIO.getCntcurr());
		mgfeelrec.company.set(chdrlifIO.getChdrcoy());
		callProgram(t5674rec.commsubr, mgfeelrec.mgfeelRec);
		if (isNE(mgfeelrec.statuz, varcom.oK)
		&& isNE(mgfeelrec.statuz, varcom.endp)) {
			drylogrec.params.set(mgfeelrec.mgfeelRec);
			drylogrec.statuz.set(mgfeelrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		chdrlifIO.setSinstamt02(mgfeelrec.mgfee);
		payrIO.setSinstamt02(mgfeelrec.mgfee);
	}

protected void validateContract400()
	{
		/*START*/
		/*  PTDATE MUST BE EQUAL TO BTDATE.*/
		wsaaValidContract.set("Y");
		if (isNE(chdrlifIO.getPtdate(), chdrlifIO.getBtdate())) {
			wsaaValidContract.set("N");
		}
		/*EXIT*/
	}

protected void processContractHeader1000()
	{
		updateOriginalChdr1010();
		createNewContractHdr1040();
	}

protected void updateOriginalChdr1010()
	{
		chdrlifIO.setValidflag("2");
		chdrlifIO.setCurrto(chdrlifIO.getBtdate());
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void createNewContractHdr1040()
	{
		/*   Update the Status field*/
		if (isNE(t5679rec.setCnRiskStat, SPACES)) {
			chdrlifIO.setStatcode(t5679rec.setCnRiskStat);
		}
		/*   Update the Premium Status field*/
		if (isNE(t5679rec.setCnPremStat, SPACES)) {
			chdrlifIO.setPstatcode(t5679rec.setCnPremStat);
		}
		/*   Update relevant billing fields*/
		chdrlifIO.setValidflag("1");
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.setTranno(add(chdrlifIO.getTranno(), 1));
		wsaaDate.set(wsaaShortTransDate);
		wsaaUser.set(wsaaAtUser);
		chdrlifIO.setTranid(wsaaTranid);
		chdrlifIO.setCurrfrom(chdrlifIO.getBtdate());
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		wsaaMiscellaneousInner.wsaaCompFreq.set(chdrlifIO.getBillfreq());
		chdrlifIO.setBillfreq(wsaaAtBillfreq);
	}

protected void processPayrRecord1100()
	{
		updateOriginalPayr1110();
		createNewPayrRecord1140();
	}

protected void updateOriginalPayr1110()
	{
		payrIO.setValidflag("2");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void createNewPayrRecord1140()
	{
		/*   Update the Premium Status field*/
		if (isNE(t5679rec.setCnPremStat, SPACES)) {
			payrIO.setPstatcode(t5679rec.setCnPremStat);
		}
		/*   Update relevant billing fields*/
		payrIO.setValidflag("1");
		payrIO.setTranno(chdrlifIO.getTranno());
		payrIO.setTransactionDate(wsaaShortTransDate);
		payrIO.setTermid(wsaaAtTermid);
		payrIO.setUser(wsaaAtUser);
		wsaaMiscellaneousInner.wsaaCompFreq.set(payrIO.getBillfreq());
		payrIO.setBillfreq(wsaaAtBillfreq);
		payrIO.setEffdate(payrIO.getBtdate());
		/*EXIT*/
	}

protected void getFirstComponent1200()
	{
		begin1210();
	}

	/**
	* <pre>
	*    Retrieve first Coverage for this contract.
	* </pre>
	*/
protected void begin1210()
	{
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(chdrlifIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrlifIO.getChdrnum());
		covrmjaIO.setPlanSuffix(9999);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(chdrlifIO.getChdrnum(), covrmjaIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(), covrmjaIO.getChdrcoy())
		|| isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			covrmjaIO.setStatuz(varcom.endp);
		}
	}

protected void processCompNwop1300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readTr5171310();
					readT56871340();
					checkStatusAndAction1350();
				case nextrCovr1370:
					nextrCovr1370();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readTr5171310()
	{
		/*    Read Table TR517 to check for WOP riders, If item found*/
		/*    don't process.*/
		wsaaTr517Key.set(covrmjaIO.getCrtable());
		a300ReadTr517();
		if (isNE(tr517rec.tr517Rec, SPACES)) {
			if (isNE(tr517rec.zrwvflg04, "Y")) {
				wsaaWopRiderFlag = "Y";
				goTo(GotoLabel.nextrCovr1370);
			}
			else {
				wsaaWopAccrFlag = "Y";
				if (isEQ(tr517rec.zrwvflg03, "Y")) {
					wsaaWopAccrFee = "Y";
				}
			}
		}
	}

	/**
	* <pre>
	*    Read Table T5687 to obtain Single Premium Indicator and
	*    Frequency Alteration Basis.
	* </pre>
	*/
protected void readT56871340()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(f294);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkStatusAndAction1350()
	{
		/*    Check if component is to be updated.*/
		wsaaMiscellaneousInner.wsaaProcessComponent.set("N");
		if (isGT(covrmjaIO.getInstprem(), 0)
		&& isNE(t5687rec.singlePremInd, "Y")) {
			checkComponent1400();
			if (wsaaMiscellaneousInner.processComponent.isTrue()) {
				updateComponent1500();
			}
		}
	}

protected void nextrCovr1370()
	{
		/*    Get next coverage record*/
		covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(covrmjaIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrlifIO.getChdrnum())) {
			covrmjaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkComponent1400()
	{
		check1410();
	}

protected void check1410()
	{
		/*  Check for match on Premium and Risk Statii for Coverage*/
		wsaaMiscellaneousInner.wsaaStatCount.set(1);
		if (isEQ(covrmjaIO.getRider(), "00")) {
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| isEQ(t5679rec.covPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode()))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.covRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
		else {
			/*   Check for match on Premium and Risk Statii for Rider*/
			wsaaMiscellaneousInner.wsaaStatCount.set(1);
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| (isEQ(t5679rec.ridPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode())))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.ridRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
	}

protected void updateComponent1500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					updateOriginalCovr1510();
					createNewCovr1520();
				case wopAccr1550:
					wopAccr1550();
				case updateStatus1580:
					updateStatus1580();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void updateOriginalCovr1510()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(chdrlifIO.getBtdate());
		covrmjaIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void createNewCovr1520()
	{
		/*    For Accelerated Crisis Wavier, check whether it waives policy*/
		/*    fee, if yes, then we have to subtract old policy fee from old*/
		/*    SI.                                                          */
		if (isEQ(wsaaWopAccrFlag, "Y")
		&& isEQ(wsaaWopAccrFee, "Y")
		&& isNE(t5688rec.feemeth, SPACES)) {
			a400ReadT5567();
			setPrecision(covrmjaIO.getSumins(), 2);
			covrmjaIO.setSumins(sub(covrmjaIO.getSumins(), wsaaOldFreqFee));
		}
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5541);
		itdmIO.setItemitem(t5687rec.xfreqAltBasis);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)
		|| isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5541)
		|| isNE(itdmIO.getItemitem(), t5687rec.xfreqAltBasis)) {
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
			goTo(GotoLabel.updateStatus1580);
		}
		else {
			t5541rec.t5541Rec.set(itdmIO.getGenarea());
		}
		wsaaMiscellaneousInner.wsaaInstpremFound.set(SPACES);
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaAtBillfreq);
		wsaaMiscellaneousInner.wsaaCalcOldfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
		/*    Get the Frequency Conversion Multiplication Factor*/
		/*    Calculate the new INSTPREM*/
		/*    Calculate the accumulating INSTPREM for SINSTAMT01*/
		wsaaMiscellaneousInner.wsaaNewFreqFound.set(SPACES);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 12)
		|| isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaMiscellaneousInner.wsaaCalcNewfreq)) {
				wsaaMiscellaneousInner.wsaaNewLfact.set(t5541rec.lfact[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaNewFreqFound.set("Y");
			}
		}
		wsaaMiscellaneousInner.wsaaOldFreqFound.set(SPACES);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 12)
		|| isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5541rec.freqcy[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaMiscellaneousInner.wsaaCalcOldfreq)) {
				wsaaMiscellaneousInner.wsaaOldLfact.set(t5541rec.lfact[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaOldFreqFound.set("Y");
			}
		}
		if (isEQ(wsaaWopAccrFlag, "Y")) {
			goTo(GotoLabel.wopAccr1550);
		}
		if (isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")
		&& isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")) {
			wsaaMiscellaneousInner.wsaaInstpremFound.set("Y");
			setPrecision(covrmjaIO.getInstprem(), 3);
			covrmjaIO.setInstprem(mult((div((mult(covrmjaIO.getInstprem(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			setPrecision(covrmjaIO.getZbinstprem(), 3);
			covrmjaIO.setZbinstprem(mult((div((mult(covrmjaIO.getZbinstprem(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			setPrecision(covrmjaIO.getZlinstprem(), 3);
			covrmjaIO.setZlinstprem(mult((div((mult(covrmjaIO.getZlinstprem(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
		}
		else {
			compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
		}
		goTo(GotoLabel.updateStatus1580);
	}

protected void wopAccr1550()
	{
		if (isEQ(wsaaMiscellaneousInner.wsaaOldFreqFound, "Y")
		&& isEQ(wsaaMiscellaneousInner.wsaaNewFreqFound, "Y")) {
			setPrecision(covrmjaIO.getSumins(), 3);
			covrmjaIO.setSumins(mult((div((mult(covrmjaIO.getSumins(), wsaaMiscellaneousInner.wsaaCalcOldfreq)), wsaaMiscellaneousInner.wsaaCalcNewfreq)), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
			setPrecision(covrmjaIO.getSumins(), 2);
			covrmjaIO.setSumins(add(covrmjaIO.getSumins(), wsaaNewFreqFee));
		}
		wsaaWopSi.set(covrmjaIO.getSumins());
		a200CalcWopPrem();
		wsaaWopAccrFlag = "N";
	}

protected void updateStatus1580()
	{
		/*   Update the Risk Status Codes*/
		if (isEQ(covrmjaIO.getRider(), "00")) {
			if (isNE(t5679rec.setCovRiskStat, SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat, SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
			}
		}
		/*  Update Premium Status Code*/
		if (isEQ(covrmjaIO.getRider(), "00")) {
			if (isNE(t5679rec.setCovPremStat, SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidPremStat, SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		/*    Write the updated record*/
		covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrfrom(chdrlifIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setTranno(chdrlifIO.getTranno());
		covrmjaIO.setTransactionDate(wsaaShortTransDate);
		covrmjaIO.setTransactionTime(wsaaAtTransactionTime);
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.writr);
		wsaaMiscellaneousInner.wsaaI.add(1);
		wsaaCrtable[wsaaMiscellaneousInner.wsaaI.toInt()].set(covrmjaIO.getCrtable());
		wsaaPremAmt[wsaaMiscellaneousInner.wsaaI.toInt()].set(covrmjaIO.getInstprem());
		wsaaLife[wsaaMiscellaneousInner.wsaaI.toInt()].set(covrmjaIO.getLife());
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*   Update the Agent Commission Records   (AGCM)*/
		/*   Retrieve all AGCM records for each valid COVR record read*/
		agcmbchIO.setParams(SPACES);
		agcmbchIO.setChdrcoy(covrmjaIO.getChdrcoy());
		agcmbchIO.setChdrnum(covrmjaIO.getChdrnum());
		agcmbchIO.setLife(covrmjaIO.getLife());
		agcmbchIO.setCoverage(covrmjaIO.getCoverage());
		agcmbchIO.setRider(covrmjaIO.getRider());
		agcmbchIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		agcmbchIO.setFormat(agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)
		&& isNE(agcmbchIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(agcmbchIO.getParams());
			drylogrec.statuz.set(agcmbchIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*   Check if valid AGCM record retrived*/
		if (isEQ(agcmbchIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isNE(agcmbchIO.getChdrcoy(), covrmjaIO.getChdrcoy())
			|| isNE(agcmbchIO.getChdrnum(), covrmjaIO.getChdrnum())
			|| isNE(agcmbchIO.getLife(), covrmjaIO.getLife())
			|| isNE(agcmbchIO.getCoverage(), covrmjaIO.getCoverage())
			|| isNE(agcmbchIO.getRider(), covrmjaIO.getRider())
			|| isNE(agcmbchIO.getPlanSuffix(), covrmjaIO.getPlanSuffix())) {
				agcmbchIO.setStatuz(varcom.endp);
			}
		}
		/*   Process AGCM records until end of file*/
		while ( !(isEQ(agcmbchIO.getStatuz(), varcom.endp))) {
			processReadAgcmbch1600();
		}

	}

protected void processReadAgcmbch1600()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					setUpDataArea1610();
					createNewRecord1650();
				case retrieveNextRecord1680:
					retrieveNextRecord1680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	/**
	* <pre>
	*   Set up Fields for AGCM
	* </pre>
	*/
protected void setUpDataArea1610()
	{
		/*    Do not process single premium AGCMs.*/
		if (isEQ(agcmbchIO.getPtdate(), ZERO)) {
			goTo(GotoLabel.retrieveNextRecord1680);
		}
		agcmbchIO.setCurrto(payrIO.getBtdate());
		agcmbchIO.setValidflag("2");
		agcmbchIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(agcmbchIO.getParams());
			drylogrec.statuz.set(agcmbchIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	/**
	* <pre>
	*   Create new AGCM record
	* </pre>
	*/
protected void createNewRecord1650()
	{
		/*   Calculate new annual premium*/
		/*   The COVR calculation for the new premium uses the following*/
		/*   formula:*/
		/*   X = P*(A/C)*(D/B)   where P = the old premium*/
		/*                             A = old frequency*/
		/*                             B = old loading factor*/
		/*                             C = new frequency*/
		/*                             D = new loading factor*/
		/*                             X = new premium*/
		/*   the new annualised premium, Y, should therefore be X*C so*/
		/*   Y = P*(A/C)*(D/B)*C*/
		/*     = P*A*(D/B)*/
		/*     = Z*(D/B)         where Z = the old annualised premium*/
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaAtBillfreq);
		wsaaMiscellaneousInner.wsaaCalcOldfreq.set(wsaaMiscellaneousInner.wsaaCompFreq);
		if (isEQ(wsaaMiscellaneousInner.wsaaInstpremFound, "Y")) {
			setPrecision(agcmbchIO.getAnnprem(), 3);
			agcmbchIO.setAnnprem(mult(agcmbchIO.getAnnprem(), (div(wsaaMiscellaneousInner.wsaaNewLfact, wsaaMiscellaneousInner.wsaaOldLfact))), true);
		}
		/*   Write new AGCM record*/
		agcmbchIO.setTransactionDate(wsaaShortTransDate);
		agcmbchIO.setTransactionTime(wsaaAtTransactionTime);
		agcmbchIO.setUser(wsaaAtUser);
		agcmbchIO.setTranno(chdrlifIO.getTranno());
		agcmbchIO.setValidflag("1");
		agcmbchIO.setCurrto(99999999);
		agcmbchIO.setCurrfrom(payrIO.getBtdate());
		agcmbchIO.setFormat(agcmbchrec);
		agcmbchIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(agcmbchIO.getParams());
			drylogrec.statuz.set(agcmbchIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void retrieveNextRecord1680()
	{
		agcmbchIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)
		&& isNE(agcmbchIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(agcmbchIO.getParams());
			drylogrec.statuz.set(agcmbchIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*   Check if valid record retrieved*/
		if (isEQ(agcmbchIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isNE(covrmjaIO.getChdrcoy(), agcmbchIO.getChdrcoy())
			|| isNE(covrmjaIO.getChdrnum(), agcmbchIO.getChdrnum())
			|| isNE(covrmjaIO.getLife(), agcmbchIO.getLife())
			|| isNE(covrmjaIO.getCoverage(), agcmbchIO.getCoverage())
			|| isNE(covrmjaIO.getRider(), agcmbchIO.getRider())
			|| isNE(covrmjaIO.getPlanSuffix(), agcmbchIO.getPlanSuffix())) {
				agcmbchIO.setStatuz(varcom.endp);
			}
		}
		if (isEQ(agcmbchIO.getTranno(), chdrlifIO.getTranno())
		&& isNE(agcmbchIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.retrieveNextRecord1680);
		}
	}

protected void processCompWop2000()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					readTr5172010();
					readT56872040();
					checkStatusAndAction2050();
				case nextrCovr2060:
					nextrCovr2060();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void readTr5172010()
	{
		/*    Read Table TR517 to check for WOP riders, If item not found*/
		/*    don't process.*/
		wsaaTr517Key.set(covrmjaIO.getCrtable());
		a300ReadTr517();
		if (isEQ(tr517rec.tr517Rec, SPACES)) {
			goTo(GotoLabel.nextrCovr2060);
		}
		else {
			if (isEQ(tr517rec.zrwvflg04, "Y")) {
				goTo(GotoLabel.nextrCovr2060);
			}
		}
	}

	/**
	* <pre>
	*    Read Table T5687 to obtain Single Premium Indicator and
	*    Frequency Alteration Basis.
	* </pre>
	*/
protected void readT56872040()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covrmjaIO.getCrtable());
		itdmIO.setItmfrm(covrmjaIO.getCurrfrom());
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5687)
		|| isNE(itdmIO.getItemitem(), covrmjaIO.getCrtable())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(f294);
			a000FatalError();
		}
		else {
			t5687rec.t5687Rec.set(itdmIO.getGenarea());
		}
	}

protected void checkStatusAndAction2050()
	{
		/*    Check if component is to be updated.*/
		wsaaMiscellaneousInner.wsaaProcessComponent.set("N");
		if (isGT(covrmjaIO.getInstprem(), 0)
		&& isNE(t5687rec.singlePremInd, "Y")) {
			checkComponent2100();
			if (wsaaMiscellaneousInner.processComponent.isTrue()) {
				updateComponent2200();
			}
		}
	}

protected void nextrCovr2060()
	{
		/*    Get next coverage record*/
		covrmjaIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			a000FatalError();
		}
		if (isNE(covrmjaIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrlifIO.getChdrnum())) {
			covrmjaIO.setStatuz(varcom.endp);
		}
		/*EXIT*/
	}

protected void checkComponent2100()
	{
		check2110();
	}

protected void check2110()
	{
		/*  Check for match on Premium and Risk Statii for Coverage*/
		wsaaMiscellaneousInner.wsaaStatCount.set(1);
		if (isEQ(covrmjaIO.getRider(), "00")) {
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| isEQ(t5679rec.covPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode()))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.covRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
		else {
			/*   Check for match on Premium and Risk Statii for Rider*/
			wsaaMiscellaneousInner.wsaaStatCount.set(1);
			while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
			|| (isEQ(t5679rec.ridPremStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getPstatcode())))) {
				wsaaMiscellaneousInner.wsaaStatCount.add(1);
			}

			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.wsaaStatCount.set(1);
				while ( !((isGT(wsaaMiscellaneousInner.wsaaStatCount, 12))
				|| (isEQ(t5679rec.ridRiskStat[wsaaMiscellaneousInner.wsaaStatCount.toInt()], covrmjaIO.getStatcode())))) {
					wsaaMiscellaneousInner.wsaaStatCount.add(1);
				}

			}
			if (isLTE(wsaaMiscellaneousInner.wsaaStatCount, 12)) {
				wsaaMiscellaneousInner.processComponent.setTrue();
			}
		}
	}

protected void updateComponent2200()
	{
		component2210();
		createNewCovr2250();
		updateStatus2280();
	}

protected void component2210()
	{
		covrmjaIO.setValidflag("2");
		covrmjaIO.setCurrto(chdrlifIO.getBtdate());
		covrmjaIO.setFunction(varcom.updat);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void createNewCovr2250()
	{
		wsaaMiscellaneousInner.wsaaCalcNewfreq.set(wsaaAtBillfreq);
		wsaaWopSi.set(ZERO);
		wsaaOldFreqFee.set(ZERO);
		wsaaNewFreqFee.set(ZERO);
		if (isNE(t5688rec.feemeth, SPACES)) {
			a400ReadT5567();
		}
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 10)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(wsaaCrtable[wsaaMiscellaneousInner.wsaaSub.toInt()], SPACES)) {
				wsaaMiscellaneousInner.wsaaSub.set(11);
			}
			else {
				a100CheckCodeWaiver();
			}
		}
		if (isGT(wsaaWopSi, 0)) {
			if (isEQ(tr517rec.zrwvflg03, "Y")) {
				wsaaWopSi.add(wsaaNewFreqFee);
			}
			a200CalcWopPrem();
			covrmjaIO.setSumins(wsaaWopSi);
		}
	}

protected void updateStatus2280()
	{
		/*   Update the Risk Status Codes*/
		if (isEQ(covrmjaIO.getRider(), "00")) {
			if (isNE(t5679rec.setCovRiskStat, SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setCovRiskStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidRiskStat, SPACES)) {
				covrmjaIO.setStatcode(t5679rec.setRidRiskStat);
			}
		}
		/*  Update Premium Status Code*/
		if (isEQ(covrmjaIO.getRider(), "00")) {
			if (isNE(t5679rec.setCovPremStat, SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setCovPremStat);
			}
		}
		else {
			if (isNE(t5679rec.setRidPremStat, SPACES)) {
				covrmjaIO.setPstatcode(t5679rec.setRidPremStat);
			}
		}
		/*    Write the updated record*/
		covrmjaIO.setValidflag("1");
		covrmjaIO.setCurrfrom(chdrlifIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		covrmjaIO.setTranno(chdrlifIO.getTranno());
		covrmjaIO.setTransactionDate(wsaaShortTransDate);
		covrmjaIO.setTransactionTime(wsaaAtTransactionTime);
		covrmjaIO.setTermid(wsaaTermid);
		covrmjaIO.setUser(wsaaUser);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*   Update the Agent Commission Records   (AGCM)*/
		/*   Retrieve all AGCM records for each valid COVR record read*/
		agcmbchIO.setParams(SPACES);
		agcmbchIO.setChdrcoy(covrmjaIO.getChdrcoy());
		agcmbchIO.setChdrnum(covrmjaIO.getChdrnum());
		agcmbchIO.setLife(covrmjaIO.getLife());
		agcmbchIO.setCoverage(covrmjaIO.getCoverage());
		agcmbchIO.setRider(covrmjaIO.getRider());
		agcmbchIO.setPlanSuffix(covrmjaIO.getPlanSuffix());
		agcmbchIO.setFormat(agcmbchrec);
		agcmbchIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, agcmbchIO);
		if (isNE(agcmbchIO.getStatuz(), varcom.oK)
		&& isNE(agcmbchIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(agcmbchIO.getParams());
			drylogrec.statuz.set(agcmbchIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*   Check if valid AGCM record retrived*/
		if (isEQ(agcmbchIO.getStatuz(), varcom.endp)) {
			/*NEXT_SENTENCE*/
		}
		else {
			if (isNE(agcmbchIO.getChdrcoy(), covrmjaIO.getChdrcoy())
			|| isNE(agcmbchIO.getChdrnum(), covrmjaIO.getChdrnum())
			|| isNE(agcmbchIO.getLife(), covrmjaIO.getLife())
			|| isNE(agcmbchIO.getCoverage(), covrmjaIO.getCoverage())
			|| isNE(agcmbchIO.getRider(), covrmjaIO.getRider())
			|| isNE(agcmbchIO.getPlanSuffix(), covrmjaIO.getPlanSuffix())) {
				agcmbchIO.setStatuz(varcom.endp);
			}
		}
		/*   Process AGCM records until end of file*/
		while ( !(isEQ(agcmbchIO.getStatuz(), varcom.endp))) {
			processReadAgcmbch1600();
		}

	}

protected void accumInstpremWrtChdr4000()
	{
		accumInstPrem4010();
	}

protected void accumInstPrem4010()
	{
		/*    Read the contract definition details T5688 for the contract*/
		/*    type held on the contract header.*/
		if (isNE(t5688rec.feemeth, SPACES)) {
			calcFee300();
		}
		chdrlifIO.setSinstamt01(wsaaMiscellaneousInner.wsaaTotInstprem);
		setPrecision(chdrlifIO.getSinstamt06(), 2);
		chdrlifIO.setSinstamt06(add(add(add(add(chdrlifIO.getSinstamt01(), chdrlifIO.getSinstamt02()), chdrlifIO.getSinstamt03()), chdrlifIO.getSinstamt04()), chdrlifIO.getSinstamt05()));
		chdrlifIO.setFunction(varcom.writr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*  Write the PAYR record.*/
		payrIO.setSinstamt01(wsaaMiscellaneousInner.wsaaTotInstprem);
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), chdrlifIO.getSinstamt03()), chdrlifIO.getSinstamt04()), chdrlifIO.getSinstamt05()));
		payrIO.setFunction(varcom.writr);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void writeHistory5000()
	{
		writePtrnRecord5010();
	}

protected void writePtrnRecord5010()
	{
	String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setTermid(wsaaTermid);
		ptrnIO.setTransactionDate(wsaaShortTransDate);
		ptrnIO.setTransactionTime(wsaaAtTransactionTime);
		ptrnIO.setUser(wsaaUser);
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		/* MOVE WSAA-AT-TODAY          TO PTRN-PTRNEFF.                 */
		/* MOVE WSAA-AT-TODAY          TO PTRN-DATESUB.                 */
		ptrnIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setChdrnum(drypDryprcRecInner.drypEntity);
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

protected void finish6000()
	{
		start6010();
	}

protected void start6010()
	{
		/* Update the DRYP fields to determine next processing date.*/
		/* Now that only one instalment period at a time is being collected*/
		/* ensure that the diary processing date is updated correctly.*/
		/* If the collection has been performed then we should have the*/
		/* PAYR information already.  Otherwise we need to do a quick read*/
		/* of the PAYR to obtain the info required by subsequent Diary*/
		/* programs.*/
		if (aplBfrqDone.isTrue()) {
			/*CONTINUE_STMT*/
		}
		else {
			drypDryprcRecInner.processUnsuccesful.setTrue();
		}
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypAplsupto.set(ZERO);
	}

protected void a000Statistics()
	{
		a010Start();
	}

protected void a010Start()
	{
		lifsttrrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		lifsttrrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		lifsttrrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifsttrrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifsttrrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifsttrrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(chdrlifIO.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			drylogrec.params.set(lifsttrrec.lifsttrRec);
			drylogrec.statuz.set(lifsttrrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

protected void a100CheckCodeWaiver()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
				case a110Check:
					a110Check();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void a110Check()
	{
		wsaaWaiverFlag = "N";
		for (wsaaMiscellaneousInner.wsaaI.set(1); !(isGT(wsaaMiscellaneousInner.wsaaI, 50)
		|| isEQ(wsaaWaiverFlag, "Y")); wsaaMiscellaneousInner.wsaaI.add(1)){
			if (isNE(tr517rec.zrwvflg02, "Y")
			&& isNE(wsaaLife[wsaaMiscellaneousInner.wsaaSub.toInt()], covrmjaIO.getLife())) {
				/*NEXT_SENTENCE*/
			}
			else {
				if (isEQ(wsaaCrtable[wsaaMiscellaneousInner.wsaaSub.toInt()], tr517rec.ctable[wsaaMiscellaneousInner.wsaaI.toInt()])) {
					/*          IF TR517-ZRWVFLG-04   =  'Y'                           */
					/*             IF WSAA-RIDER(WSAA-SUB) = '00'                      */
					/*                ADD WSAA-PREM-AMT(WSAA-SUB) TO WSAA-WOP-SI       */
					/*             ELSE                                                */
					/*                SUBTRACT WSAA-PREM-AMT(WSAA-SUB) FROM WSAA-WOP-SI*/
					/*             END-IF                                              */
					/*          ELSE                                                   */
					wsaaWopSi.add(wsaaPremAmt[wsaaMiscellaneousInner.wsaaSub.toInt()]);
					/*          END-IF                                                 */
					wsaaWaiverFlag = "Y";
				}
			}
		}
		if (isEQ(wsaaWaiverFlag, "N")
		&& isNE(tr517rec.contitem, SPACES)) {
			wsaaTr517Key.set(tr517rec.contitem);
			a300ReadTr517();
			goTo(GotoLabel.a110Check);
		}
	}

protected void a200CalcWopPrem()
	{
		a210Lext();
		a220Calc();
		a230WithoutLoading();
		a240WithLoading();
	}

protected void a210Lext()
	{
		wsaaAgerateTot.set(ZERO);
		wsaaRatesPerMillieTot.set(ZERO);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 8)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()].set(ZERO);
		}
		lextIO.setParams(SPACES);
		lextIO.setChdrcoy(covrmjaIO.getChdrcoy());
		lextIO.setChdrnum(covrmjaIO.getChdrnum());
		lextIO.setLife(covrmjaIO.getLife());
		lextIO.setCoverage(covrmjaIO.getCoverage());
		lextIO.setRider(covrmjaIO.getRider());
		lextIO.setSeqnbr(ZERO);
		lextIO.setFormat(lextrec);
		lextIO.setFunction(varcom.begn);
		while ( !(isEQ(lextIO.getStatuz(), varcom.endp))) {
			SmartFileCode.execute(appVars, lextIO);
			if (isNE(lextIO.getStatuz(), varcom.oK)
			&& isNE(lextIO.getStatuz(), varcom.endp)) {
				drylogrec.params.set(lextIO.getParams());
				drylogrec.statuz.set(lextIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			if (isNE(lextIO.getChdrcoy(), covrmjaIO.getChdrcoy())
			|| isNE(lextIO.getChdrnum(), covrmjaIO.getChdrnum())
			|| isNE(lextIO.getLife(), covrmjaIO.getLife())
			|| isNE(lextIO.getCoverage(), covrmjaIO.getCoverage())
			|| isNE(lextIO.getRider(), covrmjaIO.getRider())
			|| isNE(lextIO.getStatuz(), varcom.endp)) {
				lextIO.setStatuz(varcom.endp);
			}
			else {
				if (isEQ(lextIO.getReasind(), "1")
				&& isLTE(drypDryprcRecInner.drypRunDate, lextIO.getExtCessDate())) {
					wsaaMiscellaneousInner.wsaaSub.add(1);
					wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()].set(lextIO.getOppc());
					wsaaRatesPerMillieTot.add(lextIO.getInsprm());
					wsaaAgerateTot.add(lextIO.getAgerate());
				}
				lextIO.setFunction(varcom.nextr);
			}
		}

	}

protected void a220Calc()
	{
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(t5664);
		wsaaT5664Crtable.set(covrmjaIO.getCrtable());
		wsaaT5664Mortcls.set(covrmjaIO.getMortcls());
		wsaaT5664Sex.set(covrmjaIO.getSex());
		itdmIO.setItemitem(wsaaT5664Key);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(wsaaT5664Key, itdmIO.getItemitem())
		|| isNE(covrmjaIO.getChdrcoy(), itdmIO.getItemcoy())
		|| isNE(itdmIO.getItemtabl(), t5664)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(wsaaT5664Key);
			drylogrec.statuz.set(f358);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		else {
			t5664rec.t5664Rec.set(itdmIO.getGenarea());
		}
	}

protected void a230WithoutLoading()
	{
		if (isEQ(covrmjaIO.getAnbAtCcd(), 0)){
			compute(wsaaRate, 2).set(div(t5664rec.insprem, t5664rec.premUnit));
			/**      WHEN 100                                                   */
			/**        COMPUTE WSAA-RATE = T5664-INSTPR / T5664-PREM-UNIT       */
			/**  Extend the age band to 110.                                    */
		}
		else if (isGTE(covrmjaIO.getAnbAtCcd(), 100)
		&& isLTE(covrmjaIO.getAnbAtCcd(), 110)){
			compute(wsaaRate, 2).set(div(t5664rec.instpr[sub(covrmjaIO.getAnbAtCcd(), 99).toInt()], t5664rec.premUnit));
		}
		else{
			compute(wsaaRate, 2).set(div(t5664rec.insprm[covrmjaIO.getAnbAtCcd().toInt()], t5664rec.premUnit));
		}
		compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopSi, wsaaRate)), t5664rec.unit)));
		covrmjaIO.setZbinstprem(wsaaWopPrem);
	}

protected void a240WithLoading()
	{
		compute(wsaaAdjustedAge, 0).set(add(wsaaAgerateTot, covrmjaIO.getAnbAtCcd()));
		if (isEQ(wsaaAdjustedAge, 0)){
			compute(wsaaRate, 2).set(div(t5664rec.insprem, t5664rec.premUnit));
			/**      WHEN 100                                                   */
			/**        COMPUTE WSAA-RATE = T5664-INSTPR / T5664-PREM-UNIT       */
			/**  Extend the age band to 110.                                    */
		}
		else if (isGTE(wsaaAdjustedAge, 100)
		&& isLTE(wsaaAdjustedAge, 110)){
			compute(wsaaRate, 2).set(div(t5664rec.instpr[sub(wsaaAdjustedAge, 99).toInt()], t5664rec.premUnit));
		}
		else{
			compute(wsaaRate, 2).set(div(t5664rec.insprm[wsaaAdjustedAge.toInt()], t5664rec.premUnit));
		}
		compute(wsaaRate, 2).set(add(wsaaRatesPerMillieTot, wsaaRate));
		compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopSi, wsaaRate)), t5664rec.unit)));
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 8)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isNE(wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()], 0)) {
				compute(wsaaWopPrem, 3).setRounded((div((mult(wsaaWopPrem, wsaaLextOppc[wsaaMiscellaneousInner.wsaaSub.toInt()])), 100)));
			}
		}
		covrmjaIO.setInstprem(wsaaWopPrem);
		setPrecision(covrmjaIO.getZlinstprem(), 2);
		covrmjaIO.setZlinstprem(sub(covrmjaIO.getInstprem(), covrmjaIO.getZbinstprem()));
		compute(wsaaMiscellaneousInner.wsaaTotInstprem, 2).set(add(wsaaMiscellaneousInner.wsaaTotInstprem, covrmjaIO.getInstprem()));
	}

protected void a300ReadTr517()
	{
		a310Read();
	}

protected void a310Read()
	{
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(covrmjaIO.getChdrcoy());
		itdmIO.setItemtabl(tr517);
		itdmIO.setItemitem(wsaaTr517Key);
		itdmIO.setItmfrm(covrmjaIO.getCrrcd());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), covrmjaIO.getChdrcoy())
		|| isNE(itdmIO.getItemitem(), wsaaTr517Key)
		|| isNE(itdmIO.getItemtabl(), tr517)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			tr517rec.tr517Rec.set(SPACES);
		}
		else {
			tr517rec.tr517Rec.set(itdmIO.getGenarea());
		}
	}

protected void a400ReadT5567()
	{
		a410Read();
	}

protected void a410Read()
	{
		itdmIO.setItemcoy(chdrlifIO.getChdrcoy());
		itdmIO.setItemtabl(t5567);
		wsaaT5567Cnttype.set(chdrlifIO.getCnttype());
		wsaaT5567Cntcurr.set(chdrlifIO.getCntcurr());
		itdmIO.setItemitem(wsaaT5567Key);
		itdmIO.setItmfrm(chdrlifIO.getOccdate());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			a000FatalError();
		}
		if (isNE(itdmIO.getItemcoy(), chdrlifIO.getChdrcoy())
		|| isNE(itdmIO.getItemtabl(), t5567)
		|| isNE(itdmIO.getItemitem(), wsaaT5567Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemitem(wsaaT5567Key);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(h966);
			a000FatalError();
		}
		else {
			t5567rec.t5567Rec.set(itdmIO.getGenarea());
		}
		wsaaNewFreqFee.set(ZERO);
		for (wsaaMiscellaneousInner.wsaaSub.set(1); !(isGT(wsaaMiscellaneousInner.wsaaSub, 10)); wsaaMiscellaneousInner.wsaaSub.add(1)){
			if (isEQ(t5567rec.billfreq[wsaaMiscellaneousInner.wsaaSub.toInt()], wsaaAtBillfreq)) {
				wsaaNewFreqFee.set(t5567rec.cntfee[wsaaMiscellaneousInner.wsaaSub.toInt()]);
				wsaaMiscellaneousInner.wsaaSub.set(11);
			}
		}
	}

protected void a500UpdatBfrq()
	{
		/*A510-UPDAT*/
		bfrqIO.setValidflag("2");
		bfrqIO.setFunction(varcom.writd);
		SmartFileCode.execute(appVars, bfrqIO);
		if (isNE(bfrqIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(bfrqIO.getParams());
			drylogrec.statuz.set(bfrqIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		aplBfrqDone.setTrue();
		/*A590-EXIT*/
	}
/*
 * Class transformed  from Data Structure WSAA-MISCELLANEOUS--INNER
 */
private static final class WsaaMiscellaneousInner {
		/* WSAA-MISCELLANEOUS */
	private ZonedDecimalData wsaaStatCount = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaCompFreq = new FixedLengthStringData(2);

	private FixedLengthStringData wsaaCalcFreqs = new FixedLengthStringData(4);
	private ZonedDecimalData wsaaCalcOldfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalcFreqs, 0).setUnsigned();
	private ZonedDecimalData wsaaCalcNewfreq = new ZonedDecimalData(2, 0).isAPartOf(wsaaCalcFreqs, 2).setUnsigned();

	private FixedLengthStringData wsaaProcessComponent = new FixedLengthStringData(1);
	private Validator processComponent = new Validator(wsaaProcessComponent, "Y");
	private Validator notProcessComponent = new Validator(wsaaProcessComponent, "N");
	private FixedLengthStringData wsaaInstpremFound = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTotInstprem = new PackedDecimalData(17, 2);
	private FixedLengthStringData wsaaNewFreqFound = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaOldFreqFound = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaNewLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaOldLfact = new ZonedDecimalData(5, 4);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaI = new ZonedDecimalData(2, 0).setUnsigned();
}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner {

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
}
}
