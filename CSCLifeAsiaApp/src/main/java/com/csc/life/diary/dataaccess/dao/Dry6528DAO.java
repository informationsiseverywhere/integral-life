package com.csc.life.diary.dataaccess.dao;

import java.util.List;
import com.csc.life.diary.dataaccess.model.Dry6528Dto;

/**
 * DAO To Fetch Records For Benefit Billing Processing
 * 
 * @author ptrivedi8
 *
 */
public interface Dry6528DAO {

	/**
	 * Get coverage records
	 * 
	 * @param company
	 *            - Company
	 * 
	 * @param effDate
	 *            - Effective date
	 * 
	 * @param chdrNum
	 *            - Contract Number
	 * 
	 * @return - List of Dry6528DTO
	 */
	public List<Dry6528Dto> getCovrRecords(String company, int effDate, String chdrNum);
}
