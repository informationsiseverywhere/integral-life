package com.csc.life.diary.recordstructures;


import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;

import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import com.quipoz.COBOLFramework.COBOLFunctions;
      /*
      *(C) Copyright CSC Corporation Limited 1986 - 2000.
      *    All rights reserved. CSC Confidential.
      *
      *    This copybook is used to map the R5107 detail fields
      *    onto the DRPT record for the Batch Diary System.
      *
      ***********************************************************************
      *           AMENDMENT  HISTORY                                        *
      ***********************************************************************
      * DATE.... VSN/MOD  WORK UNIT    BY....                               *
      *                                                                     *
      * 07/03/02  01/01   DRYAPL       Tom Whiffen                          *
      *           Initial Version.                                          *
      *                                                                     *
      * 15/02/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
      *           Retrofit.                                                 *
      *                                                                     *
      **DD/MM/YY*************************************************************
      *
	03  R5107-DATA-AREA             REDEFINES
		DRYR-GENAREA.
		05  R5107-LAPIND            PIC X(01).
		05  R5107-LIFE              PIC X(02).
		05  R5107-COVERAGE          PIC X(02).
		05  R5107-RIDER             PIC X(02).
		05  R5107-PLNSFX            PIC X(04).
		05  R5107-VRTFND            PIC X(04).
		05  R5107-UNITYP            PIC X(01).
		05  R5107-TRANNO            PIC 9(05).
		05  R5107-BATCTRCDE         PIC X(04).
		05  R5107-CNTAMNT           PIC S9(11)V9(02).
		05  R5107-CNTCURR           PIC X(03).
		05  R5107-FUNDAMNT          PIC S9(11)V9(02).
		05  R5107-FNDCURR           PIC X(03).
		05  R5107-FUNDRATE          PIC S9(09)V9(09).
		05  R5107-AGE               PIC 9(03).
		05  R5107-TRIGER            PIC X(10).
		05  FILLER                  PIC X(409).
	03  R5107-SORT-KEY.
		05  R5107-RECTYPE           PIC X(01).
		05  R5107-CHDRNUM           PIC X(08).
		05  FILLER                  PIC X(29).
	03  R5107-REPORT-NAME           PIC X(10)
		                            VALUE 'R5107     '.
		*/                            
		                      
public class R5107rec extends ExternalData {
	
	public FixedLengthStringData dataArea = new FixedLengthStringData(484);
	
	public FixedLengthStringData lapind = new FixedLengthStringData(1).isAPartOf(dataArea, 0);
	
	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(dataArea, 1);
	
	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(dataArea, 3);
	
	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(dataArea, 5);
	
	public FixedLengthStringData plnsfx = new FixedLengthStringData(4).isAPartOf(dataArea, 7);
	
	public FixedLengthStringData vrtfnd = new FixedLengthStringData(4).isAPartOf(dataArea, 11);
	
	public FixedLengthStringData unityp = new FixedLengthStringData(1).isAPartOf(dataArea, 15);
	
	public ZonedDecimalData tranno = new ZonedDecimalData(5).isAPartOf(dataArea, 16);
	
	public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(dataArea, 21);
	
	public ZonedDecimalData cntamnt = new ZonedDecimalData(11, 2).isAPartOf(dataArea, 25);
	
	public FixedLengthStringData cntcurr = new FixedLengthStringData(3).isAPartOf(dataArea, 36);
	
	public ZonedDecimalData fundamnt = new ZonedDecimalData(11, 2).isAPartOf(dataArea, 39);
	
	public FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(dataArea, 50);
	
	public ZonedDecimalData fundrate = new ZonedDecimalData(9, 9).isAPartOf(dataArea, 53);
	
	public ZonedDecimalData age = new ZonedDecimalData(3).isAPartOf(dataArea, 62);
	
	public FixedLengthStringData triger = new FixedLengthStringData(10).isAPartOf(dataArea, 65);
	
	public FixedLengthStringData filler = new FixedLengthStringData(409).isAPartOf(dataArea, 75, FILLER_REDEFINE);
		
	
	public FixedLengthStringData sortKey = new FixedLengthStringData(38);
	
	public FixedLengthStringData rectype = new FixedLengthStringData(1).isAPartOf(sortKey, 0);
	
	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(sortKey, 1);
	
	public FixedLengthStringData filler1 = new FixedLengthStringData(29).isAPartOf(dataArea, 9, FILLER_REDEFINE);
	
	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R5107");

	@Override
	public void initialize() {
		COBOLFunctions.initialize(lapind);
		COBOLFunctions.initialize(life);
		COBOLFunctions.initialize(coverage);
		COBOLFunctions.initialize(rider);
		COBOLFunctions.initialize(plnsfx);
		COBOLFunctions.initialize(vrtfnd);
		COBOLFunctions.initialize(unityp);
		COBOLFunctions.initialize(tranno);
		COBOLFunctions.initialize(batctrcde);
		COBOLFunctions.initialize(cntamnt);
		COBOLFunctions.initialize(cntcurr);
		COBOLFunctions.initialize(fundamnt);
		COBOLFunctions.initialize(fndcurr);
		COBOLFunctions.initialize(fundrate);
		COBOLFunctions.initialize(age);
		COBOLFunctions.initialize(triger);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(rectype);
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(filler1);

	}

	@Override
	public FixedLengthStringData getBaseString() {
		if (baseString == null) {
			baseString = new FixedLengthStringData(getLength());
			lapind.isAPartOf(baseString, true);
			life.isAPartOf(baseString, true);
			coverage.isAPartOf(baseString, true);
			rider.isAPartOf(baseString, true);
			plnsfx.isAPartOf(baseString, true);
			vrtfnd.isAPartOf(baseString, true);
			unityp.isAPartOf(baseString, true);
			tranno.isAPartOf(baseString, true);
			batctrcde.isAPartOf(baseString, true);
			cntamnt.isAPartOf(baseString, true);
			cntcurr.isAPartOf(baseString, true);
			fundamnt.isAPartOf(baseString, true);
			fndcurr.isAPartOf(baseString, true);
			fundrate.isAPartOf(baseString, true);
			age.isAPartOf(baseString, true);
			triger.isAPartOf(baseString, true);
			filler.isAPartOf(baseString, true);
			rectype.isAPartOf(baseString, true);
			chdrnum.isAPartOf(baseString, true);
			filler1.isAPartOf(baseString, true);
			baseString.resetIsAPartOfOffset();
		}
		return baseString;
	}

}