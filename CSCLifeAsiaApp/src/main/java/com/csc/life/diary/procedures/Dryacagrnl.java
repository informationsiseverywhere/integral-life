/*
 * File: Dryacagrnl.java
 * Date: May 13, 2015 12:40:15 PM IST
 * Author: CSC
 * 
 * Class transformed from DRYACAGRNL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import com.quipoz.framework.datatype.*;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;

import com.csc.fsu.general.dataaccess.AcagrnlTableDAM;
import com.csc.fsu.general.dataaccess.AcblTableDAM;

import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.parent.SMARTCodeModel;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the ACAGRNL Accumulation subroutine.
*
* It will update all Agent ACBL records.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryacagrnl extends SMARTCodeModel {

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(6).init("DRYACAGRNL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
		/* FORMATS */
	private String acagrnlrec = "ACAGRNLREC";
	private String acblrec = "ACBLREC";

	private FixedLengthStringData lsaaStatuz = new FixedLengthStringData(4);
	private FixedLengthStringData lsaaCompany = new FixedLengthStringData(1);
	private FixedLengthStringData lsaaDataArea = new FixedLengthStringData(1000);
	
	private AcagrnlTableDAM acagrnlIO = new AcagrnlTableDAM();
	private AcblTableDAM acblIO = new AcblTableDAM();
	private Varcom varcom = new Varcom();

	public Dryacagrnl() {
		super();
	}

	/**
	* The mainline method is the default entry point of the program when called by other programs using the
	* Quipoz runtime framework.
	*/
public void mainline(Object... parmArray)
	{
		lsaaDataArea = convertAndSetParam(lsaaDataArea, parmArray, 2);
		lsaaCompany = convertAndSetParam(lsaaCompany, parmArray, 1);
		lsaaStatuz = convertAndSetParam(lsaaStatuz, parmArray, 0);
		try {
			mainLine100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine100()
	{
		main110();
		exit190();
	}

protected void main110()
	{
		lsaaStatuz.set(varcom.oK);
		acagrnlIO.setDataArea(lsaaDataArea);
		
		acblIO.setParams(SPACES);
		acblIO.setRldgcoy(acagrnlIO.getRldgcoy());
		acblIO.setSacscode(acagrnlIO.getSacscode());
		acblIO.setRldgacct(acagrnlIO.getRldgacct());
		acblIO.setOrigcurr(acagrnlIO.getOrigcurr());
		acblIO.setSacstyp(acagrnlIO.getSacstyp());
		
		acblIO.setFormat(acblrec);
		acblIO.setFunction(varcom.readh);
		
		SmartFileCode.execute(appVars, acblIO);

		if (isNE(acblIO.getStatuz(), varcom.oK)
		&& isNE(acblIO.getStatuz(), varcom.mrnf)) {
			lsaaStatuz.set(acblIO.getStatuz());
			exit190();
		}
		
		if (isEQ(acblIO.getStatuz(), varcom.oK)) {
			acblIO.setFunction(varcom.rewrt);
		}else {
			acblIO.setFunction(varcom.writr);
			acblIO.setSacscurbal(ZEROS);
			acblIO.setRldgpfx(acagrnlIO.getRldgpfx());
		}

		if (isEQ(acagrnlIO.getGlsign(), '-')) {
			acblIO.setSacscurbal(sub(acblIO.getSacscurbal(),acagrnlIO.getSacscurbal()));
		}else {
			acblIO.setSacscurbal(add(acblIO.getSacscurbal(),acagrnlIO.getSacscurbal()));
		}
		
		SmartFileCode.execute(appVars, acblIO);

		if (isNE(acblIO.getStatuz(), varcom.oK)) {
			lsaaStatuz.set(acblIO.getStatuz());
			exit190();
		}
		
	}

protected void exit190()
	{
		exitProgram();
	}
}