package com.csc.life.diary.pojo;

/**
 * BenefitBilling - POJO For Benefit Billing Processing
 * 
 * @author ptrivedi8
 *
 */

public class BenefitBilling {

	/**
	 * Constructor
	 */
	public BenefitBilling() {
		// Calling default constructor
	}

	private String adfeemth;
	private String jlPremMeth;
	private String premMeth;
	private String subprog;
	private String svMethod;
	private String unitFreq;

	/**
	 * @return the adfeemth
	 */
	public String getAdfeemth() {
		return adfeemth;
	}

	/**
	 * @param adfeemth
	 *            the adfeemth to set
	 */
	public void setAdfeemth(String adfeemth) {
		this.adfeemth = adfeemth;
	}

	/**
	 * @return the jlPremMeth
	 */
	public String getJlPremMeth() {
		return jlPremMeth;
	}

	/**
	 * @param jlPremMeth
	 *            the jlPremMeth to set
	 */
	public void setJlPremMeth(String jlPremMeth) {
		this.jlPremMeth = jlPremMeth;
	}

	/**
	 * @return the premMeth
	 */
	public String getPremMeth() {
		return premMeth;
	}

	/**
	 * @param premMeth
	 *            the premMeth to set
	 */
	public void setPremMeth(String premMeth) {
		this.premMeth = premMeth;
	}

	/**
	 * @return the subprog
	 */
	public String getSubprog() {
		return subprog;
	}

	/**
	 * @param subprog
	 *            the subprog to set
	 */
	public void setSubprog(String subprog) {
		this.subprog = subprog;
	}

	/**
	 * @return the svMethod
	 */
	public String getSvMethod() {
		return svMethod;
	}

	/**
	 * @param svMethod
	 *            the svMethod to set
	 */
	public void setSvMethod(String svMethod) {
		this.svMethod = svMethod;
	}

	/**
	 * @return the unitFreq
	 */
	public String getUnitFreq() {
		return unitFreq;
	}

	/**
	 * @param unitFreq
	 *            the unitFreq to set
	 */
	public void setUnitFreq(String unitFreq) {
		this.unitFreq = unitFreq;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BenefitBilling [adfeemth=" + adfeemth + ", jlPremMeth=" + jlPremMeth + ", premMeth=" + premMeth
				+ ", subprog=" + subprog + ", svMethod=" + svMethod + ", unitFreq=" + unitFreq + "]";
	}

}
