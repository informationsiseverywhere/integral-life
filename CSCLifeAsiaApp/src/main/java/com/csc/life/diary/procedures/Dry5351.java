/*
 * File: Dry5351.java
 * Date: March 26, 2014 3:00:45 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY5351.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.life.contractservicing.dataaccess.dao.TaxdpfDAO;
import com.csc.life.contractservicing.dataaccess.model.Taxdpf;
import com.csc.life.contractservicing.tablestructures.Tr52drec;
import com.csc.life.contractservicing.tablestructures.Tr52erec;
import com.csc.life.contractservicing.tablestructures.Txcalcrec;
import com.csc.life.diary.dataaccess.dao.Dry5351DAO;
import com.csc.life.diary.dataaccess.model.Dry5351Dto;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.dataaccess.dao.LinspfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Linspf;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*                 REVENUE (DUE-DATE) ACCOUNTING.
*                 -----------------------------
*     The system must be able to handle both Cash and Due Date
* Accounting of contracts. Cash accounting requires that the premi ms
* are only accounted for when the money is available.
*
* Control totals used in this program:
*
*    01  -  No. of LINS  records read
*    02  -  Gross Prem Comp level acct
*    03  -  No. LINS recs produced.
*    04  -  Total Net Premium Due
*    05  -  Gross Prems Contract lvl
*    06  -  Total amount of fees
*    07  -  Total amount waived
*    08  -  No. of LINS CHDR's locked
*    09  -  No Invalid LINS CHDR's
*    10  -  No of COVR's not posted
*    11  -  No of COVR's posted
*
*     Revenue requires for the premiums to be accounted for as
* soon as they are due, the contra entry for the Revenue Account
* postings being made to a 'PREMIUMS DUE' account.
*
* (Collection (B5353) debits the Suspense account and updates the
* control account as soon as the  money is available).
*
****************************************************************** ****
*                                                                     *
 * </pre>
 */
public class Dry5351 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private static final int WSAAT5688SIZE = 1000;
	/* ERRORS */
	private static final String H791 = "H791";
	private static final String F781 = "F781";
	/* TABLES */
	private static final String T1688 = "T1688";
	private static final String T5679 = "T5679";
	private static final String T5688 = "T5688";
	private static final String T5645 = "T5645";

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5351");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	private PackedDecimalData wsaaCommitCnt = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaCycleCnt = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(COBOLFunctions.SPACES);
	/* WSAA-MISC-SUBSCRIPTS */
	protected ZonedDecimalData wsaaGlSub = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).setUnsigned();

	/* Status indicators */
	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");
	/* WSAA-PLAN-SUFF */
	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(4, 0).setUnsigned();

	private FixedLengthStringData wsaaPlanR = new FixedLengthStringData(4).isAPartOf(wsaaPlan, 0,
			COBOLFunctions.REDEFINE);
	private ZonedDecimalData wsaaPlansuff = new ZonedDecimalData(2, 0).isAPartOf(wsaaPlanR, 2).setUnsigned();

	private FixedLengthStringData wsaaRldgacct = new FixedLengthStringData(16);
	private FixedLengthStringData wsaaRldgChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaRldgacct, 0);
	private FixedLengthStringData wsaaRldgLife = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 8);
	private FixedLengthStringData wsaaRldgCoverage = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 10);
	private FixedLengthStringData wsaaRldgRider = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 12);
	private FixedLengthStringData wsaaRldgPlanSuff = new FixedLengthStringData(2).isAPartOf(wsaaRldgacct, 14);

	/* WSAA-T5688-ARRAY */
	private FixedLengthStringData[] wsaaT5688Rec = COBOLFunctions.FLSInittedArray(1000, 10);
	private FixedLengthStringData[] wsaaT5688Key = COBOLFunctions.FLSDArrayPartOfArrayStructure(3, wsaaT5688Rec, 0);
	private FixedLengthStringData[] wsaaT5688Cnttype = COBOLFunctions.FLSDArrayPartOfArrayStructure(3, wsaaT5688Key, 0,
			COBOLFunctions.SPACES);
	private FixedLengthStringData[] wsaaT5688Data = COBOLFunctions.FLSDArrayPartOfArrayStructure(7, wsaaT5688Rec, 3);
	private PackedDecimalData[] wsaaT5688Itmfrm = COBOLFunctions.PDArrayPartOfArrayStructure(8, 0, wsaaT5688Data, 0);
	private FixedLengthStringData[] wsaaT5688Comlvlacc = COBOLFunctions.FLSDArrayPartOfArrayStructure(1, wsaaT5688Data,
			5);
	private FixedLengthStringData[] wsaaT5688Revacc = COBOLFunctions.FLSDArrayPartOfArrayStructure(1, wsaaT5688Data, 6);

	private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(20);
	private FixedLengthStringData wsysLinrkey = new FixedLengthStringData(20).isAPartOf(wsysSystemErrorParams, 0);
	private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(2).isAPartOf(wsysLinrkey, 0);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysLinrkey, 3);
	private ZonedDecimalData wsysBillcd = new ZonedDecimalData(8, 0).isAPartOf(wsysLinrkey, 12).setUnsigned();

	private DescTableDAM descIO = new DescTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	protected Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Txcalcrec txcalcrec = new Txcalcrec();
	private T5679rec t5679rec = new T5679rec();
	private T5645rec t5645rec = new T5645rec();
	private T5688rec t5688rec = new T5688rec();
	private Tr52drec tr52drec = new Tr52drec();
	private Tr52erec tr52erec = new Tr52erec();

	private Map<String, List<Itempf>> t5688Map;
	private Map<String, List<Itempf>> tr52eMap;
	private Map<String, List<Itempf>> tr52dMap;
	private int ct01Value;
	private BigDecimal ct02Value = BigDecimal.ZERO;
	private int ct03Value;
	private BigDecimal ct04Value = BigDecimal.ZERO;
	private BigDecimal ct05Value = BigDecimal.ZERO;
	private BigDecimal ct06Value = BigDecimal.ZERO;
	private BigDecimal ct07Value = BigDecimal.ZERO;
	private int ct08Value;
	private int ct09Value;
	private int ct10Value;
	private int ct11Value;
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private LinspfDAO linspfDAO = getApplicationContext().getBean("linspfDAO", LinspfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private TaxdpfDAO taxdpfDAO = getApplicationContext().getBean("taxdpfDAO", TaxdpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Dry5351DAO dry5351DAO = getApplicationContext().getBean("dry5351DAO", Dry5351DAO.class);

	private String payrCurr;
	protected Linspf linsrnlIO;
	protected Chdrpf chdrlifIO;
	private int wsaaT5688Ix = 1;
	private Map<String, List<Chdrpf>> chdrLifMap;
	private Map<String, List<Payrpf>> payrMap;
	private Map<String, List<Linspf>> linsrnlMap;
	private Map<String, List<Covrpf>> covrenqMap;
	private Map<String, List<Taxdpf>> taxdbilMap;

	private List<Chdrpf> updateChdrlifList;
	private List<Ptrnpf> insertPtrnpfList;
	private List<Linspf> updateLinsrnlList;
	private List<Taxdpf> updateTaxdList;

	public Dry5351() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	protected PackedDecimalData getWsaaCommitCnt() {
		return wsaaCommitCnt;
	}

	protected PackedDecimalData getWsaaCycleCnt() {
		return wsaaCycleCnt;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void startProcessing() {
		initialise1000();
		List<Dry5351Dto> dry5351DtoList = readChunkRecord();	
		for (Dry5351Dto dry5351Dto : dry5351DtoList) {
			ct01Value++;
			wsysChdrcoy.set(dry5351Dto.getChdrCoy());
			wsysChdrnum.set(dry5351Dto.getChdrNum());

			edit2500(dry5351Dto);
			if (isEQ(wsspEdterror, Varcom.oK)) {
				update3000(dry5351Dto);

			}
		}
		updateDiaryRecords();
		commit3500();
	}

	protected void initialise1000() {
		wsspEdterror.set(Varcom.oK);
		varcom.vrcmDate.set(COBOLFunctions.getCobolDate());
		lifacmvrec.jrnseq.set(0);
		lifacmvrec.rcamt.set(0);
		lifacmvrec.crate.set(0);
		lifacmvrec.acctamt.set(0);
		lifacmvrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.drypCompany);
		lifacmvrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifacmvrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifacmvrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		lifacmvrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifacmvrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifacmvrec.frcdate.set(varcom.vrcmMaxDate);
		lifacmvrec.tranref.set("");
		lifacmvrec.user.set(0);
		descIO.setStatuz(Varcom.oK);
		descIO.setDescpfx("IT");
		descIO.setDesccoy(drypDryprcRecInner.drypCompany);
		descIO.setDesctabl(T1688);
		descIO.setDescitem(drypDryprcRecInner.drypBatctrcde);
		descIO.setLanguage(drypDryprcRecInner.drypLanguage);
		descIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), Varcom.oK)) {
			descIO.setLongdesc(COBOLFunctions.SPACES);
		}
		lifacmvrec.trandesc.set(descIO.getLongdesc());
		/* Look up the valid contract and coverage statuii. */
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(T5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat("ITEMREC");
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Read T5645 for ITEMSEQ = ' ' ONLY - since we only need the */
		/* first 7 entries. */
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(T5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setItemseq(COBOLFunctions.SPACES);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/* Load contract type details. */
		t5688Map = itemDAO.loadSmartTable("IT", drypDryprcRecInner.drypCompany.toString(), T5688);
		wsaaT5688Ix = 1;
		loadT56881100();

		tr52eMap = itemDAO.loadSmartTable("IT", drypDryprcRecInner.drypCompany.toString(), "TR52E");

		tr52dMap = itemDAO.loadSmartTable("IT", drypDryprcRecInner.drypCompany.toString(), "TR52D");
	}

	protected void loadT56881100() {
		if (t5688Map != null) {
			for (List<Itempf> itemList : t5688Map.values()) {
				for (Itempf i : itemList) {
					t5688rec.t5688Rec.set(StringUtil.rawToString(i.getGenarea()));
					wsaaT5688Itmfrm[wsaaT5688Ix].set(i.getItmfrm());
					wsaaT5688Cnttype[wsaaT5688Ix].set(i.getItemitem());
					wsaaT5688Comlvlacc[wsaaT5688Ix].set(t5688rec.comlvlacc);
					wsaaT5688Revacc[wsaaT5688Ix].set(t5688rec.revacc);
					wsaaT5688Ix++;
				}
				if (COBOLFunctions.isGT(wsaaT5688Ix, WSAAT5688SIZE)) {
					drylogrec.params.set(T5688);
					drylogrec.statuz.set(H791);
					drylogrec.dryDatabaseError.setTrue();
					a000FatalError();
				}
			}
		}
	}

	private List<Dry5351Dto> readChunkRecord() {

		List<Dry5351Dto> dry5351DtoList = dry5351DAO.getLinspfRecords(drypDryprcRecInner.drypRunDate.toInt(),
				drypDryprcRecInner.drypCompany.toString(), drypDryprcRecInner.drypEntity.toString());

		if (dry5351DtoList != null && !dry5351DtoList.isEmpty()) {

			List<String> chdrNumList = Collections.singletonList(drypDryprcRecInner.drypEntity.toString());
			chdrLifMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrNumList);
			payrMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(drypDryprcRecInner.drypCompany.toString(),
					chdrNumList);
			linsrnlMap = linspfDAO.searchLinsrnlRecord(chdrNumList);
			covrenqMap = covrpfDAO.searchValidCovrByChdrnum(chdrNumList);
			taxdbilMap = taxdpfDAO.searchTaxdbilRecord(chdrNumList);
		}

		return dry5351DtoList;
	}

	protected void edit2500(Dry5351Dto dry5351Dto) {
		wsspEdterror.set(Varcom.oK);
		readChdr2200(dry5351Dto);
		if (isNE(dry5351Dto.getChdrNum(), wsaaPrevChdrnum)) {
			validateContract2300(dry5351Dto);
			if (!validContract.isTrue()) {
				ct09Value++;
				wsspEdterror.set(COBOLFunctions.SPACES);
				return;
			}
		}
		readPayr2400(dry5351Dto);

	}

	protected void readChdr2200(Dry5351Dto dry5351Dto) {

		if (chdrLifMap != null && chdrLifMap.containsKey(dry5351Dto.getChdrNum())) {
			for (Chdrpf c : chdrLifMap.get(dry5351Dto.getChdrNum())) {
				if (c.getChdrcoy().toString().equals(dry5351Dto.getChdrCoy())) {
					chdrlifIO = c;
					break;
				}
			}
		}
		if (chdrlifIO == null) {
			drylogrec.params.set(dry5351Dto.getChdrNum());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsysBillcd.set(chdrlifIO.getBillcd());
		chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
	}

	protected void validateContract2300(Dry5351Dto dry5351Dto) {
		wsaaT5688Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT5688Ix, wsaaT5688Rec.length); wsaaT5688Ix++) {
				if (isEQ(wsaaT5688Key[wsaaT5688Ix], chdrlifIO.getCnttype())) {
					break searchlabel1;
				}
			}
			drylogrec.statuz.set(F781);
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(T5688);
			stringVariable1.addExpression(wsysSystemErrorParams);
			stringVariable1.setStringInto(drylogrec.params);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* We must find the effective T5688 entry for the contract */
		/* (T5688 entries will have been loaded in descending sequence */
		/* into the array). */
		while (!(isNE(chdrlifIO.getCnttype(), wsaaT5688Cnttype[wsaaT5688Ix])
				|| COBOLFunctions.isGT(wsaaT5688Ix, WSAAT5688SIZE) || dateFound.isTrue())) {
			if (COBOLFunctions.isGTE(chdrlifIO.getOccdate(), wsaaT5688Itmfrm[wsaaT5688Ix])) {
				wsaaDateFound.set("Y");
			} else {
				wsaaT5688Ix++;
			}
		}

		if (!dateFound.isTrue()) {
			drylogrec.params.set(wsysSystemErrorParams);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Validate the statii of the contract */
		wsaaValidContract.set("N");
		for (wsaaT5679Sub.set(1); !(COBOLFunctions.isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(COBOLFunctions.isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()], chdrlifIO.getPstcde())) {
						wsaaT5679Sub.set("13");
						wsaaValidContract.set("Y");
					}
				}
			}
		}
		/* We can now store this contract so that any subsequent LINRs */
		/* for the same contract need not have the contract locked, read */
		/* and re-validated. */
		wsaaPrevChdrnum.set(dry5351Dto.getChdrNum());
	}

	protected void readPayr2400(Dry5351Dto dry5351Dto) {
		if (payrMap != null && payrMap.containsKey(dry5351Dto.getChdrNum())) {
			for (Payrpf p : payrMap.get(dry5351Dto.getChdrNum())) {
				if (p.getChdrcoy().equals(dry5351Dto.getChdrCoy())) {
					payrCurr = p.getCntcurr();
					break;
				}
			}
		}
	}

	protected void update3000(Dry5351Dto dry5351Dto) {
		/* UPDATE */
		readLins3100(dry5351Dto);

		acmvPostings3200(dry5351Dto);
		rewrtChdrLins3300();
		a000PostTax(dry5351Dto);
		writePtrn3400(dry5351Dto);
		/* EXIT */
	}

	protected void readLins3100(Dry5351Dto dry5351Dto) {
		if (linsrnlMap != null && linsrnlMap.containsKey(dry5351Dto.getChdrNum())) {
			for (Linspf l : linsrnlMap.get(dry5351Dto.getChdrNum())) {
				if (dry5351Dto.getChdrCoy().equals(l.getChdrcoy()) && dry5351Dto.getInstFrom() == l.getInstfrom()) {
					linsrnlIO = l;
					break;
				}
			}
		}
	}

	protected void acmvPostings3200(Dry5351Dto dry5351Dto) {
		/* LIFA-RLDGACCT must be re-initialised in case the last entry */
		/* was for component level accounting. */
		lifacmvrec.rldgacct.set(COBOLFunctions.SPACES);
		lifacmvrec.rldgacct.set(dry5351Dto.getChdrNum());
		/* Post the Premium-due. */
		if (BigDecimal.ZERO.compareTo(linsrnlIO.getInstamt06()) != 0) {
			lifacmvrec.origamt.set(linsrnlIO.getInstamt06());
			wsaaGlSub.set(1);
			callLifacmv5000(dry5351Dto);
			ct04Value = ct04Value.add(linsrnlIO.getInstamt06());
		}
		/* Post the Premium-Income. */
		if (linsrnlIO.getInstamt01().compareTo(BigDecimal.ZERO) > 0 && isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], " ")) {
			lifacmvrec.origamt.set(linsrnlIO.getInstamt01());
			wsaaGlSub.set(2);
			callLifacmv5000(dry5351Dto);
			ct05Value = ct05Value.add(linsrnlIO.getInstamt01());
		}
		/* Post the Fees. */

		/* Post the Tolerance. */
		postCustomerSpecific3210(dry5351Dto);
		/* Post the Stamp-Duty. */
		if (linsrnlIO.getInstamt04().compareTo(BigDecimal.ZERO) > 0) {
			lifacmvrec.origamt.set(linsrnlIO.getInstamt04());
			wsaaGlSub.set(5);
		}
		/* Post the Waiver-holding. */
		if (BigDecimal.ZERO.compareTo(linsrnlIO.getInstamt05()) != 0) {
			lifacmvrec.origamt.set(linsrnlIO.getInstamt05());
			wsaaGlSub.set(6);
			callLifacmv5000(dry5351Dto);
			/* Log total amount of waiver */
			ct07Value = ct07Value.add(linsrnlIO.getInstamt05());
		}
		/* Create a posting for every COVR-INSTPREMs for component */
		/* level accounted contracts only. */
		if (covrenqMap != null && covrenqMap.containsKey(dry5351Dto.getChdrNum())
				&& isEQ(wsaaT5688Comlvlacc[wsaaT5688Ix], "Y")) {
			for (Covrpf c : covrenqMap.get(dry5351Dto.getChdrNum())) {
				if (dry5351Dto.getChdrCoy().equals(c.getChdrcoy())) {
					covrAcmvs3250(c, dry5351Dto);
				}
			}
		}
	}

	protected void postCustomerSpecific3210(Dry5351Dto dry5351Dto) {
		if (linsrnlIO.getInstamt02().compareTo(BigDecimal.ZERO) > 0) {
			lifacmvrec.origamt.set(linsrnlIO.getInstamt02());
			wsaaGlSub.set(3);
			callLifacmv5000(dry5351Dto);
			/* Log total amount of fees */
			ct06Value = ct06Value.add(linsrnlIO.getInstamt02());
		}

		if (linsrnlIO.getInstamt03().compareTo(BigDecimal.ZERO) > 0) {
			lifacmvrec.origamt.set(linsrnlIO.getInstamt03());
			wsaaGlSub.set(4);
			callLifacmv5000(dry5351Dto);
		}
	}

	protected void covrAcmvs3250(Covrpf covrenqIO, Dry5351Dto dry5351Dto) {
		wsaaValidCoverage.set("N");
		for (wsaaT5679Sub.set(1); !(COBOLFunctions.isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrenqIO.getStatcode())) {
				for (wsaaT5679Sub.set(1); !(COBOLFunctions.isGT(wsaaT5679Sub, 12)); wsaaT5679Sub.add(1)) {
					if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], covrenqIO.getPstatcode())) {
						wsaaT5679Sub.set("13");
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
		if (validCoverage.isTrue() && BigDecimal.ZERO.compareTo(covrenqIO.getInstprem()) != 0) {
			lifacmvrec.origamt.set(covrenqIO.getInstprem());
			wsaaRldgChdrnum.set(covrenqIO.getChdrnum());
			wsaaRldgLife.set(covrenqIO.getLife());
			wsaaRldgCoverage.set(covrenqIO.getCoverage());
			wsaaRldgRider.set(covrenqIO.getRider());
			wsaaPlan.set(covrenqIO.getPlanSuffix());
			lifacmvrec.substituteCode[6].set(covrenqIO.getCrtable());
			wsaaRldgPlanSuff.set(wsaaPlansuff);
			lifacmvrec.rldgacct.set(wsaaRldgacct);
			wsaaGlSub.set(7);
			callLifacmv5000(dry5351Dto);
			/* Log gross premiums - - comp level */
			ct02Value = ct02Value.add(covrenqIO.getInstprem());
			/* Log no. of COVRS posted */
			ct11Value++;
		} else {
			/* Log no. of COVRS not posted */
			ct10Value++;
		}
	}

	protected void callLifacmv5000(Dry5351Dto dry5351Dto) {
		/* These LIFACMV parameters are the same for both instalment */
		/* and coverage posting. The correct T5645 entries are pointed */
		/* to by the value in the subscript WSAA-GL-SUB. */
		lifacmvrec.rdocnum.set(dry5351Dto.getChdrNum());
		lifacmvrec.tranref.set(dry5351Dto.getChdrNum());
		lifacmvrec.origcurr.set(payrCurr);
		lifacmvrec.tranno.set(chdrlifIO.getTranno());
		lifacmvrec.effdate.set(linsrnlIO.getInstfrom());
		int one = 1;
		lifacmvrec.substituteCode[one].set(chdrlifIO.getCnttype());
		lifacmvrec.jrnseq.add(1);
		lifacmvrec.sacscode.set(t5645rec.sacscode[wsaaGlSub.toInt()]);
		lifacmvrec.sacstyp.set(t5645rec.sacstype[wsaaGlSub.toInt()]);
		lifacmvrec.glsign.set(t5645rec.sign[wsaaGlSub.toInt()]);
		lifacmvrec.glcode.set(t5645rec.glmap[wsaaGlSub.toInt()]);
		lifacmvrec.contot.set(t5645rec.cnttot[wsaaGlSub.toInt()]);
		lifacmvrec.function.set("PSTW");
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, Varcom.oK)) {
			drylogrec.params.set(lifacmvrec.lifacmvRec);
			drylogrec.statuz.set(lifacmvrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void rewrtChdrLins3300() {
		if (updateChdrlifList == null) {
			updateChdrlifList = new ArrayList<>();
		}
		Chdrpf c = new Chdrpf(chdrlifIO);
		c.setUniqueNumber(chdrlifIO.getUniqueNumber());
		updateChdrlifList.add(c);

		/* Update the installment DUEFLG. */
		if (updateLinsrnlList == null) {
			updateLinsrnlList = new ArrayList<>();
		}
		linsrnlIO.setDueflg("Y");
		updateLinsrnlList.add(linsrnlIO);
		/* Log no. LINS record processed */
		ct03Value++;
	}

	protected void writePtrn3400(Dry5351Dto dry5351Dto) {
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrcoy(dry5351Dto.getChdrCoy());
		ptrnIO.setChdrnum(dry5351Dto.getChdrNum());
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setTrtm(varcom.vrcmTime.toInt());
		ptrnIO.setPtrneff(linsrnlIO.getInstfrom());
		/* MOVE BSSC-EFFECTIVE-DATE TO PTRN-TRANSACTION-DATE. */
		ptrnIO.setTrdt(varcom.vrcmDate.toInt());
		ptrnIO.setUserT(0);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany.toString());
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn.toString());
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr.toInt());
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn.toInt());
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch.toString());
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx.toString());
		ptrnIO.setValidflag("1");
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate.toInt());
		if (insertPtrnpfList == null) {
			insertPtrnpfList = new ArrayList<>();
		}
		insertPtrnpfList.add(ptrnIO);
	}

	protected void commit3500() {
		/* COMMIT */
		/** Place any additional commitment processing in here. */
		commitControlTotals();
		if (updateChdrlifList != null && !updateChdrlifList.isEmpty()) {
			chdrpfDAO.updateChdrTrannoByUniqueNo(updateChdrlifList);
			updateChdrlifList.clear();
		}
		if (insertPtrnpfList != null && !insertPtrnpfList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(insertPtrnpfList);
			insertPtrnpfList.clear();
		}
		if (updateLinsrnlList != null && !updateLinsrnlList.isEmpty()) {
			linspfDAO.updateLinspfDueflg(updateLinsrnlList);
			updateLinsrnlList.clear();
		}
		if (updateTaxdList != null && !updateTaxdList.isEmpty()) {
			taxdpfDAO.updateTaxdpf(updateTaxdList);
			updateTaxdList.clear();
		}
		/* EXIT */
	}

	private void commitControlTotals() {

		drycntrec.contotNumber.set(1);
		drycntrec.contotValue.set(ct01Value);
		d000ControlTotals();
		ct01Value = 0;

		drycntrec.contotNumber.set(2);
		drycntrec.contotValue.set(ct02Value);
		d000ControlTotals();
		ct02Value = BigDecimal.ZERO;

		drycntrec.contotNumber.set(3);
		drycntrec.contotValue.set(ct03Value);
		d000ControlTotals();
		ct03Value = 0;

		drycntrec.contotNumber.set(4);
		drycntrec.contotValue.set(ct04Value);
		d000ControlTotals();
		ct04Value = BigDecimal.ZERO;

		drycntrec.contotNumber.set(5);
		drycntrec.contotValue.set(ct05Value);
		d000ControlTotals();
		ct05Value = BigDecimal.ZERO;

		drycntrec.contotNumber.set(6);
		drycntrec.contotValue.set(ct06Value);
		d000ControlTotals();
		ct06Value = BigDecimal.ZERO;

		drycntrec.contotNumber.set(7);
		drycntrec.contotValue.set(ct07Value);
		d000ControlTotals();
		ct07Value = BigDecimal.ZERO;

		drycntrec.contotNumber.set(8);
		drycntrec.contotValue.set(ct08Value);
		d000ControlTotals();
		ct08Value = 0;

		drycntrec.contotNumber.set(9);
		drycntrec.contotValue.set(ct09Value);
		d000ControlTotals();
		ct09Value = 0;

		drycntrec.contotNumber.set(10);
		drycntrec.contotValue.set(ct10Value);
		d000ControlTotals();
		ct10Value = 0;

		drycntrec.contotNumber.set(11);
		drycntrec.contotValue.set(ct11Value);
		d000ControlTotals();
		ct11Value = 0;
	}

	protected void a000PostTax(Dry5351Dto dry5351Dto) {
		/* Read table TR52D */
		Itempf item = null;
		if (tr52dMap != null) {
			if (tr52dMap.containsKey(chdrlifIO.getReg())) {
				item = tr52dMap.get(chdrlifIO.getReg()).get(0);
			} else if (tr52dMap.containsKey("***")) {
				item = tr52dMap.get("***").get(0);
			}
		}
		if (item == null) {
			drylogrec.params.set("tr52d:" + chdrlifIO.getReg());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		} else {
			tr52drec.tr52dRec.set(StringUtil.rawToString(item.getGenarea()));
		}
		/* Read TAXD records */
		if (taxdbilMap != null && taxdbilMap.containsKey(dry5351Dto.getChdrNum())) {
			for (Taxdpf t : taxdbilMap.get(dry5351Dto.getChdrNum())) {
				if (dry5351Dto.getChdrCoy().equals(t.getChdrcoy()) && dry5351Dto.getInstFrom() == t.getInstfrom()) {
					a100ProcessTax(t, dry5351Dto);
				}
			}
		}
	}

	protected void a100ProcessTax(Taxdpf taxdbilIO, Dry5351Dto dry5351Dto) {
		if (isNE(taxdbilIO.getPostflg(), COBOLFunctions.SPACES)) {
			return;
		}
		/* Read table TR52E */
		String tranref = COBOLFunctions.subString(taxdbilIO.getTranref(), 1, 8).toString();
		Itempf tr52eItem = null;
		if (tr52eMap != null && tr52eMap.containsKey(tranref)) {
			for (Itempf item : tr52eMap.get(tranref)) {
				if (item.getItmfrm().compareTo(new BigDecimal(taxdbilIO.getEffdate())) <= 0) {
					tr52eItem = item;
					break;
				}
			}
		} else {
			drylogrec.params.set("TR52E:" + tranref);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (tr52eItem == null) {
			drylogrec.params.set("TR52E:" + tranref);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		tr52erec.tr52eRec.set(StringUtil.rawToString(tr52eItem.getGenarea()));

		/* Call tax subroutine */
		COBOLFunctions.initialize(txcalcrec.linkRec);
		txcalcrec.function.set("POST");
		txcalcrec.statuz.set(Varcom.oK);
		txcalcrec.chdrcoy.set(taxdbilIO.getChdrcoy());
		txcalcrec.chdrnum.set(taxdbilIO.getChdrnum());
		txcalcrec.life.set(taxdbilIO.getLife());
		txcalcrec.coverage.set(taxdbilIO.getCoverage());
		txcalcrec.rider.set(taxdbilIO.getRider());
		txcalcrec.planSuffix.set(taxdbilIO.getPlansfx());
		/* get the component detail. */
		Covrpf covrenqIO = null;
		if (isNE(txcalcrec.coverage, COBOLFunctions.SPACES)) {
			if (covrenqMap != null && covrenqMap.containsKey(dry5351Dto.getChdrNum())) {
				for (Covrpf c : covrenqMap.get(taxdbilIO.getChdrnum())) {
					if (c.getChdrcoy().equals(taxdbilIO.getChdrcoy()) && c.getLife().equals(taxdbilIO.getLife())
							&& c.getCoverage().equals(taxdbilIO.getCoverage())
							&& c.getRider().equals(taxdbilIO.getRider()) && 0 == c.getPlanSuffix()) {
						covrenqIO = c;
						break;
					}
				}
			}
			if (covrenqIO == null) {
				drylogrec.params.set(taxdbilIO.getChdrnum());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
		}
		if (isEQ(taxdbilIO.getCoverage(), COBOLFunctions.SPACES)) {
			txcalcrec.crtable.set(COBOLFunctions.SPACES);
			txcalcrec.cntTaxInd.set("Y");
		} else {
			if (covrenqIO != null) {
				txcalcrec.crtable.set(covrenqIO.getCrtable());
			}
			txcalcrec.cntTaxInd.set(" ");
		}
		int one = 1;
		int two = 2;
		txcalcrec.cnttype.set(chdrlifIO.getCnttype());
		txcalcrec.register.set(chdrlifIO.getReg());
		txcalcrec.taxrule.set(COBOLFunctions.subString(taxdbilIO.getTranref(), 1, 8));
		txcalcrec.rateItem.set(COBOLFunctions.subString(taxdbilIO.getTranref(), 9, 8));
		txcalcrec.amountIn.set(taxdbilIO.getBaseamt());
		txcalcrec.effdate.set(taxdbilIO.getEffdate());
		txcalcrec.transType.set(taxdbilIO.getTrantype());
		txcalcrec.taxType[one].set(taxdbilIO.getTxtype01());
		txcalcrec.taxType[two].set(taxdbilIO.getTxtype02());
		txcalcrec.taxAmt[one].set(taxdbilIO.getTaxamt01());
		txcalcrec.taxAmt[two].set(taxdbilIO.getTaxamt02());
		txcalcrec.taxAbsorb[one].set(taxdbilIO.getTxabsind01());
		txcalcrec.taxAbsorb[two].set(taxdbilIO.getTxabsind02());
		txcalcrec.batckey.set(drypDryprcRecInner.drypBatchKey);
		txcalcrec.ccy.set(chdrlifIO.getCntcurr());
		txcalcrec.language.set(drypDryprcRecInner.drypLanguage);
		txcalcrec.tranno.set(chdrlifIO.getTranno());
		txcalcrec.jrnseq.set(0);
		callProgram(tr52drec.txsubr, txcalcrec.linkRec);
		if (isNE(txcalcrec.statuz, Varcom.oK)) {
			drylogrec.params.set(txcalcrec.linkRec);
			drylogrec.statuz.set(txcalcrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* Update TAXD record */
		/* <LA4758> */
		/* <LA4758> */
		/* PERFORM 600-FATAL-ERROR <LA4758> */
		/* END-IF. <LA4758> */
		if (updateTaxdList == null) {
			updateTaxdList = new ArrayList<>();
		}
		Taxdpf t = new Taxdpf();
		t.setUnique_number(taxdbilIO.getUnique_number());
		t.setPostflg("P");
		updateTaxdList.add(t);
	}
	
	protected void updateDiaryRecords() {
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstcde());
	}


	/*
	 * Class transformed from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		// cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = COBOLFunctions.FLSArrayPartOfStructure(25, 10, drypSystParams,
				0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0,
				COBOLFunctions.REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	}
}
