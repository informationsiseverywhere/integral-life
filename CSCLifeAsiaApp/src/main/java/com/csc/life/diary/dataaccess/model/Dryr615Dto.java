package com.csc.life.diary.dataaccess.model;

/**
 * Dryr615Dto
 * 
 * @author hmahajan6
 *
 */
public class Dryr615Dto {
	private String chdrcoy;
	private String chdrnum;
	private String life;
	private String coverage;
	private String rider;
	private int planSuffix;

	public Dryr615Dto() {
		// default no argument constructor
	}

	/**
	 * Getter for Company
	 * 
	 * @return chdrcoy - Company
	 */
	public String getChdrcoy() {
		return chdrcoy;
	}

	/**
	 * Setter for Company
	 * 
	 * @param chdrcoy
	 *            - Company
	 */
	public void setChdrcoy(String chdrcoy) {
		this.chdrcoy = chdrcoy;
	}

	/**
	 * Getter for Contract Number
	 * 
	 * @return chdrnum - Contract Number
	 */
	public String getChdrnum() {
		return chdrnum;
	}

	/**
	 * Setter for Contract Number
	 * 
	 * @param chdrnum
	 *            - Contract Number
	 */
	public void setChdrnum(String chdrnum) {
		this.chdrnum = chdrnum;
	}

	/**
	 * Getter for life
	 * 
	 * @return life
	 */
	public String getLife() {
		return life;
	}

	/**
	 * Setter for life
	 * 
	 * @param life
	 *            - Life
	 */
	public void setLife(String life) {
		this.life = life;
	}

	/**
	 * Getter for coverage
	 * 
	 * @return coverage
	 */
	public String getCoverage() {
		return coverage;
	}

	/**
	 * Setter for Coverage
	 * 
	 * @param coverage
	 *            - Coverage
	 */
	public void setCoverage(String coverage) {
		this.coverage = coverage;
	}

	/**
	 * Getter for rider
	 * 
	 * @return rider
	 */
	public String getRider() {
		return rider;
	}

	/**
	 * Setter for rider
	 * 
	 * @param rider
	 *            - Rider
	 */
	public void setRider(String rider) {
		this.rider = rider;
	}

	/**
	 * Getter for planSuffix
	 * 
	 * @return planSuffix
	 */
	public int getPlanSuffix() {
		return planSuffix;
	}

	/**
	 * Setter for planSuffix
	 * 
	 * @param planSuffix
	 *            - PlanSuffix
	 */
	public void setPlanSuffix(int planSuffix) {
		this.planSuffix = planSuffix;
	}

	@Override
	public String toString() {
		return "Dryr615Dto []";
	}
}
