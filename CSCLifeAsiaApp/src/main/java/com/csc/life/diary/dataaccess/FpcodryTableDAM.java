package com.csc.life.diary.dataaccess;

import com.csc.life.flexiblepremium.dataaccess.FpcopfTableDAM;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.BaseData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;


/**
 * 	
 * File: FpcodryTableDAM.java
 * Date: Tue, 3 Dec 2013 04:10:00
 * Class transformed from FPCODRY.LF
 * Author: CSC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class FpcodryTableDAM extends FpcopfTableDAM {

	public FpcodryTableDAM() {
		super();
		setKeyConstants();

	}
	
	public void setTable() {
		super.setTable();
		TABLE = getTableName("FPCODRY");
	}
	
	public String getTable() {
		return TABLE;
	}

	public void setKeyConstants() {
	
		KEYCOLUMNS = ""
		             +  "CHDRCOY"
		             + ", CHDRNUM"
		             + ", LIFE"
		             + ", COVERAGE"
		             + ", RIDER"
		             + ", PLNSFX";
		
		QUALIFIEDCOLUMNS = 
		            "CHDRCOY, " +
		            "CHDRNUM, " +
		            "LIFE, " +
		            "JLIFE, " +
		            "COVERAGE, " +
		            "RIDER, " +
		            "PLNSFX, " +
		            "VALIDFLAG, " +
		            "CURRFROM, " +
		            "CURRTO, " +
		            "PRMPER, " +
		            "PRMRCDP, " +
		            "TRANNO, " +
		            "ACTIND, " +
		            "BILLEDP, " +
		            "OVRMINREQ, " +
		            "MINOVRPRO, " +
		            "ANPROIND, " +
		            "TARGFROM, " +
		            "TARGTO, " +
		            "EFFDATE, " +
		            "CBANPR, " +
		            "JOBNM, " +
		            "USRPRF, " +
		            "DATIME, " +
					"UNIQUE_NUMBER";
		
		ORDERBY = 
		            "CHDRCOY ASC, " +
		            "CHDRNUM ASC, " +
		            "LIFE ASC, " +
		            "COVERAGE ASC, " +
		            "RIDER ASC, " +
		            "PLNSFX ASC, " +
					"UNIQUE_NUMBER DESC";
		
		REVERSEORDERBY = 
		            "CHDRCOY DESC, " +
		            "CHDRNUM DESC, " +
		            "LIFE DESC, " +
		            "COVERAGE DESC, " +
		            "RIDER DESC, " +
		            "PLNSFX DESC, " +
					"UNIQUE_NUMBER ASC";

		
		setStatics();
		
		qualifiedColumns = new BaseData[] {
                               chdrcoy,
                               chdrnum,
                               life,
                               jlife,
                               coverage,
                               rider,
                               planSuffix,
                               validflag,
                               currfrom,
                               currto,
                               targetPremium,
                               premRecPer,
                               tranno,
                               activeInd,
                               billedInPeriod,
                               overdueMin,
                               minOverduePer,
                               annProcessInd,
                               targfrom,
                               targto,
                               effdate,
                               annivProcDate,
                               jobName,
                               userProfile,
                               datime,
                               unique_number
		                       };
		
	}
	
	public FixedLengthStringData getBaseString() {
		FixedLengthStringData result = new FixedLengthStringData(getParams().getLength());
		result.set(getParams());
  		return result;
	}
	
	public String toString() {
  		return getBaseString().toString();
	}

	/*****************************************************************************************************/
	/* Getters and setters for SKM header - all other common getter/setters declared in SmartFileCode    */
	/*****************************************************************************************************/

	public FixedLengthStringData getHeader() {
		return getShortHeader();		
	}
	
	public FixedLengthStringData setHeader(Object what) {
		
		return setShortHeader(what);
	}

	private FixedLengthStringData keyFiller = new FixedLengthStringData(238);	// FILLER field under -KEY-DATA
	/****************************************************************/
	/* Getters and setters for key SKM group fields            */
	/****************************************************************/
	public FixedLengthStringData getRecKeyData() {
		FixedLengthStringData keyData = new FixedLengthStringData(256);
		
		keyData.set(getChdrcoy().toInternal()
					+ getChdrnum().toInternal()
					+ getLife().toInternal()
					+ getCoverage().toInternal()
					+ getRider().toInternal()
					+ getPlanSuffix().toInternal()
					+ keyFiller.toInternal());

		return keyData;
	}

	public FixedLengthStringData setRecKeyData(Object obj) {
		clearRecKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, chdrcoy);
			what = ExternalData.chop(what, chdrnum);
			what = ExternalData.chop(what, life);
			what = ExternalData.chop(what, coverage);
			what = ExternalData.chop(what, rider);
			what = ExternalData.chop(what, planSuffix);
			what = ExternalData.chop(what, keyFiller);
		
			return what;
		}
	}

	// FILLER fields under -NON-KEY-DATA
	private FixedLengthStringData nonKeyFiller10 = new FixedLengthStringData(1);
	private FixedLengthStringData nonKeyFiller20 = new FixedLengthStringData(8);
	private FixedLengthStringData nonKeyFiller30 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller50 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller60 = new FixedLengthStringData(2);
	private FixedLengthStringData nonKeyFiller70 = new FixedLengthStringData(3);


	/****************************************************************/
	/* After a DB read we need to set the FILLERs under             */
	/* -NON-KEY-DATA to equal the key values                        */
	/****************************************************************/
	public void setNonKeyFillers() {
	
	nonKeyFiller10.setInternal(chdrcoy.toInternal());
	nonKeyFiller20.setInternal(chdrnum.toInternal());
	nonKeyFiller30.setInternal(life.toInternal());
	nonKeyFiller50.setInternal(coverage.toInternal());
	nonKeyFiller60.setInternal(rider.toInternal());
	nonKeyFiller70.setInternal(planSuffix.toInternal());

	}

	/****************************************************************/
	/* Getters and setters for non-key SKM group fields             */
	/****************************************************************/
	public FixedLengthStringData getRecNonKeyData() {
		FixedLengthStringData nonKeyData = new FixedLengthStringData(140);
		
		nonKeyData.set(
					nonKeyFiller10.toInternal()
					+ nonKeyFiller20.toInternal()
					+ nonKeyFiller30.toInternal()
					+ getJlife().toInternal()
					+ nonKeyFiller50.toInternal()
					+ nonKeyFiller60.toInternal()
					+ nonKeyFiller70.toInternal()
					+ getValidflag().toInternal()
					+ getCurrfrom().toInternal()
					+ getCurrto().toInternal()
					+ getTargetPremium().toInternal()
					+ getPremRecPer().toInternal()
					+ getTranno().toInternal()
					+ getActiveInd().toInternal()
					+ getBilledInPeriod().toInternal()
					+ getOverdueMin().toInternal()
					+ getMinOverduePer().toInternal()
					+ getAnnProcessInd().toInternal()
					+ getTargfrom().toInternal()
					+ getTargto().toInternal()
					+ getEffdate().toInternal()
					+ getAnnivProcDate().toInternal()
					+ getJobName().toInternal()
					+ getUserProfile().toInternal()
					+ getDatime().toInternal());
		return nonKeyData;
	}

	public FixedLengthStringData setRecNonKeyData(Object obj) {
		clearRecNonKeyData();
		if (isClearOperation(obj)) {
			return null;
		} else {
			FixedLengthStringData what = new FixedLengthStringData(obj.toString());

			what = ExternalData.chop(what, nonKeyFiller10);
			what = ExternalData.chop(what, nonKeyFiller20);
			what = ExternalData.chop(what, nonKeyFiller30);
			what = ExternalData.chop(what, jlife);
			what = ExternalData.chop(what, nonKeyFiller50);
			what = ExternalData.chop(what, nonKeyFiller60);
			what = ExternalData.chop(what, nonKeyFiller70);
			what = ExternalData.chop(what, validflag);
			what = ExternalData.chop(what, currfrom);
			what = ExternalData.chop(what, currto);
			what = ExternalData.chop(what, targetPremium);
			what = ExternalData.chop(what, premRecPer);
			what = ExternalData.chop(what, tranno);
			what = ExternalData.chop(what, activeInd);
			what = ExternalData.chop(what, billedInPeriod);
			what = ExternalData.chop(what, overdueMin);
			what = ExternalData.chop(what, minOverduePer);
			what = ExternalData.chop(what, annProcessInd);
			what = ExternalData.chop(what, targfrom);
			what = ExternalData.chop(what, targto);
			what = ExternalData.chop(what, effdate);
			what = ExternalData.chop(what, annivProcDate);
			what = ExternalData.chop(what, jobName);
			what = ExternalData.chop(what, userProfile);
			what = ExternalData.chop(what, datime);		
			return what;
		}
	}
	
	/****************************************************************/
	/* Getters and setters for key SKM fields                       */
	/****************************************************************/

	public FixedLengthStringData getChdrcoy() {
		return chdrcoy;
	}
	public void setChdrcoy(Object what) {
		chdrcoy.set(what);
	}
	public FixedLengthStringData getChdrnum() {
		return chdrnum;
	}
	public void setChdrnum(Object what) {
		chdrnum.set(what);
	}
	public FixedLengthStringData getLife() {
		return life;
	}
	public void setLife(Object what) {
		life.set(what);
	}
	public FixedLengthStringData getCoverage() {
		return coverage;
	}
	public void setCoverage(Object what) {
		coverage.set(what);
	}
	public FixedLengthStringData getRider() {
		return rider;
	}
	public void setRider(Object what) {
		rider.set(what);
	}
	public PackedDecimalData getPlanSuffix() {
		return planSuffix;
	}
	public void setPlanSuffix(Object what) {
		setPlanSuffix(what, false);
	}
	public void setPlanSuffix(Object what, boolean rounded) {
		if (rounded)
			planSuffix.setRounded(what);
		else
			planSuffix.set(what);
	}
	/****************************************************************/
	/* Getters and setters for non-key SKM fields                   */
	/****************************************************************/

	public FixedLengthStringData getJlife() {
		return jlife;
	}
	public void setJlife(Object what) {
		jlife.set(what);
	}	
	public FixedLengthStringData getValidflag() {
		return validflag;
	}
	public void setValidflag(Object what) {
		validflag.set(what);
	}	
	public PackedDecimalData getCurrfrom() {
		return currfrom;
	}
	public void setCurrfrom(Object what) {
		setCurrfrom(what, false);
	}
	public void setCurrfrom(Object what, boolean rounded) {
		if (rounded)
			currfrom.setRounded(what);
		else
			currfrom.set(what);
	}	
	public PackedDecimalData getCurrto() {
		return currto;
	}
	public void setCurrto(Object what) {
		setCurrto(what, false);
	}
	public void setCurrto(Object what, boolean rounded) {
		if (rounded)
			currto.setRounded(what);
		else
			currto.set(what);
	}	
	public PackedDecimalData getTargetPremium() {
		return targetPremium;
	}
	public void setTargetPremium(Object what) {
		setTargetPremium(what, false);
	}
	public void setTargetPremium(Object what, boolean rounded) {
		if (rounded)
			targetPremium.setRounded(what);
		else
			targetPremium.set(what);
	}	
	public PackedDecimalData getPremRecPer() {
		return premRecPer;
	}
	public void setPremRecPer(Object what) {
		setPremRecPer(what, false);
	}
	public void setPremRecPer(Object what, boolean rounded) {
		if (rounded)
			premRecPer.setRounded(what);
		else
			premRecPer.set(what);
	}	
	public PackedDecimalData getTranno() {
		return tranno;
	}
	public void setTranno(Object what) {
		setTranno(what, false);
	}
	public void setTranno(Object what, boolean rounded) {
		if (rounded)
			tranno.setRounded(what);
		else
			tranno.set(what);
	}	
	public FixedLengthStringData getActiveInd() {
		return activeInd;
	}
	public void setActiveInd(Object what) {
		activeInd.set(what);
	}	
	public PackedDecimalData getBilledInPeriod() {
		return billedInPeriod;
	}
	public void setBilledInPeriod(Object what) {
		setBilledInPeriod(what, false);
	}
	public void setBilledInPeriod(Object what, boolean rounded) {
		if (rounded)
			billedInPeriod.setRounded(what);
		else
			billedInPeriod.set(what);
	}	
	public PackedDecimalData getOverdueMin() {
		return overdueMin;
	}
	public void setOverdueMin(Object what) {
		setOverdueMin(what, false);
	}
	public void setOverdueMin(Object what, boolean rounded) {
		if (rounded)
			overdueMin.setRounded(what);
		else
			overdueMin.set(what);
	}	
	public PackedDecimalData getMinOverduePer() {
		return minOverduePer;
	}
	public void setMinOverduePer(Object what) {
		setMinOverduePer(what, false);
	}
	public void setMinOverduePer(Object what, boolean rounded) {
		if (rounded)
			minOverduePer.setRounded(what);
		else
			minOverduePer.set(what);
	}	
	public FixedLengthStringData getAnnProcessInd() {
		return annProcessInd;
	}
	public void setAnnProcessInd(Object what) {
		annProcessInd.set(what);
	}	
	public PackedDecimalData getTargfrom() {
		return targfrom;
	}
	public void setTargfrom(Object what) {
		setTargfrom(what, false);
	}
	public void setTargfrom(Object what, boolean rounded) {
		if (rounded)
			targfrom.setRounded(what);
		else
			targfrom.set(what);
	}	
	public PackedDecimalData getTargto() {
		return targto;
	}
	public void setTargto(Object what) {
		setTargto(what, false);
	}
	public void setTargto(Object what, boolean rounded) {
		if (rounded)
			targto.setRounded(what);
		else
			targto.set(what);
	}	
	public PackedDecimalData getEffdate() {
		return effdate;
	}
	public void setEffdate(Object what) {
		setEffdate(what, false);
	}
	public void setEffdate(Object what, boolean rounded) {
		if (rounded)
			effdate.setRounded(what);
		else
			effdate.set(what);
	}	
	public PackedDecimalData getAnnivProcDate() {
		return annivProcDate;
	}
	public void setAnnivProcDate(Object what) {
		setAnnivProcDate(what, false);
	}
	public void setAnnivProcDate(Object what, boolean rounded) {
		if (rounded)
			annivProcDate.setRounded(what);
		else
			annivProcDate.set(what);
	}	
	public FixedLengthStringData getJobName() {
		return jobName;
	}
	public void setJobName(Object what) {
		jobName.set(what);
	}	
	public FixedLengthStringData getUserProfile() {
		return userProfile;
	}
	public void setUserProfile(Object what) {
		userProfile.set(what);
	}	
	public FixedLengthStringData getDatime() {
		return datime;
	}
	public void setDatime(Object what) {
		datime.set(what);
	}	

	
	/****************************************************************/
	/* Group Clear Methods                                          */
	/****************************************************************/
	public void clearRecKeyData() {

		chdrcoy.clear();
		chdrnum.clear();
		life.clear();
		coverage.clear();
		rider.clear();
		planSuffix.clear();
		keyFiller.clear();
	}

	public void clearRecNonKeyData() {

		nonKeyFiller10.clear();
		nonKeyFiller20.clear();
		nonKeyFiller30.clear();
		jlife.clear();
		nonKeyFiller50.clear();
		nonKeyFiller60.clear();
		nonKeyFiller70.clear();
		validflag.clear();
		currfrom.clear();
		currto.clear();
		targetPremium.clear();
		premRecPer.clear();
		tranno.clear();
		activeInd.clear();
		billedInPeriod.clear();
		overdueMin.clear();
		minOverduePer.clear();
		annProcessInd.clear();
		targfrom.clear();
		targto.clear();
		effdate.clear();
		annivProcDate.clear();
		jobName.clear();
		userProfile.clear();
		datime.clear();		
	}


}