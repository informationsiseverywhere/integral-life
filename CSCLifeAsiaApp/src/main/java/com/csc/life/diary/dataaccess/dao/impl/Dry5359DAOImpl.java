package com.csc.life.diary.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.diary.dataaccess.dao.Dry5359DAO;
import com.csc.life.diary.dataaccess.model.Dry5359Dto;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * DAO Implementation To Fetch Records For Flexible Premium COllection
 * 
 * @author svemula29
 *
 */
public class Dry5359DAOImpl extends BaseDAOImpl<Dry5359Dto> implements Dry5359DAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(Dry5359DAOImpl.class);
	
	/**
	 * Constructor
	 */
	public Dry5359DAOImpl() {
		super();
		// Calling default constructor
	}

	/*
	 * (non-Javadoc)
	 * @see com.csc.life.diary.dataaccess.dao.Dry5359#getFprmDetails(java.lang.String, java.lang.String, int, int, java.lang.String)
	 */
	@Override
	public List<Dry5359Dto> getFprmDetails(String validFlag, String chdrCoy, int currFrom, int currTo, String chdrNum) {
		StringBuilder query = new StringBuilder();

		query.append("SELECT  CHDRCOY, CHDRNUM, PAYRSEQNO, TOTRECD, TOTBILL, CURRFROM, CURRTO, VALIDFLAG ");
		query.append("FROM FPRMPF WHERE VALIDFLAG = ? AND CHDRCOY = ? AND CURRFROM <= ? ");
		query.append("AND CURRTO >= ? AND CHDRNUM = ? ");
		query.append("ORDER BY CHDRNUM, PAYRSEQNO");

		List<Dry5359Dto> dry5359List = new ArrayList<>();
		try (PreparedStatement preparedStatement = getPrepareStatement(query.toString())) {
			preparedStatement.setString(1, validFlag);
			preparedStatement.setString(2, chdrCoy);
			preparedStatement.setInt(3, currFrom);
			preparedStatement.setInt(4, currTo);
			preparedStatement.setString(5, chdrNum);
			
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Dry5359Dto dry5359Dto = new Dry5359Dto();
					dry5359Dto.setChdrCoy(resultSet.getString("CHDRCOY"));
					dry5359Dto.setChdrNum(resultSet.getString("CHDRNUM"));
					dry5359Dto.setPayrseqno(resultSet.getInt("PAYRSEQNO"));
					dry5359Dto.setTotalRecd(resultSet.getInt("TOTRECD"));
					dry5359Dto.setTotalBilled(resultSet.getInt("TOTBILL"));
					dry5359Dto.setCurrFrom(resultSet.getInt("CURRFROM"));
					dry5359Dto.setCurrTo(resultSet.getInt("CURRTO"));
					dry5359Dto.setValidFlag(resultSet.getString("VALIDFLAG"));
					
					dry5359List.add(dry5359Dto);
				}
			}
			return dry5359List;
		} catch (SQLException e) {
			LOGGER.error("Error occurred when reading FPRMPF data", e);
			throw new SQLRuntimeException(e);
		}

	}
}
