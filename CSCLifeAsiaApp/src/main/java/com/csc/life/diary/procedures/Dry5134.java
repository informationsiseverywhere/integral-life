/*
 * File: Dry5134.java
 * Date: March 26, 2014 2:59:30 PM ICT
 * Author: CSC
 *
 * Class transformed from DRY5134.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import com.csc.diaryframework.parent.Maind;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Letrqst;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Letrqstrec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.tablestructures.Tr384rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.agents.tablestructures.Tr695rec;
import com.csc.life.diary.dataaccess.dao.Dry5134DAO;
import com.csc.life.diary.dataaccess.model.Dry5134Dto;
import com.csc.life.general.procedures.Agecalc;
import com.csc.life.general.recordstructures.Agecalcrec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.ChdrlnbTableDAM;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.Vpxchdr;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxchdrrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.dataaccess.IncrrgpTableDAM;
import com.csc.life.regularprocessing.dataaccess.LifergpTableDAM;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.recordstructures.Incrsrec;
import com.csc.life.regularprocessing.tablestructures.T5654rec;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.terminationclaims.dataaccess.AnnyTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.ArraySearch;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*         PENDING AUTOMATIC INCREASE
*         ==========================
*
* This program comprises part of the Diary Batch System. It
* has been substantially restructured for this system.
*
* The program processes a single contract based on the COVR
* CPI date.
*
* It is up to each client to ensure that the Lead Time before
* billing on T5655 is greater than or equal to the Billing
* Lead Time on T6654.
*
* This process is executed daily and will process all
* COVR records that meet these criterion:
*
*
*     COVR-CHDRCOY             =  DRYP-COMPANY
*     COVR-CHDRNUM             =  DRYP-ENTITY
*     COVR-CPI-DATE            <  DRYP-EFFECTIVE-DATE
*     COVR-CPI-DATE        NOT =  0
*     COVR-INDEXATION-FLAG NOT =  'P'
*     COVR-VALIDFLAG           =  '1'
*
* If all these tests are passed, the COVR record is
* processed.
*
* Initialise
* ==========
*
* Setup the selection criteria.
* Setup the Report Heading detail.
* Read T5679 with the Batch Transaction Code as the key
* to get the Contract Statii.
* Read and hold CHDRLIF for the entity.
* Read and hold PAYR for the entity.
* Read TR384 with Contract Type + Batch Code as the key to
* get any letter type.
*
* NOTE:
* The Contract must have the correct status otherwise no
* further processing is performed.
*
* For Each Selected Component
* ===========================
*
* Read T5687 with COVR-CRTABLE as key to get any Anniversary
* Method.
* Read T6658 with the Anniversary Method as the key to get
* the Increase Subroutine.
* If there is an Increase Subroutine then call it.
*
* Use of past INCR records will assist in passing Original
* Premium and Sum Assured figures as well as the Premium
* and Sum Assured figures from the Last Increase. This
* will allow calculation of Simple or Compound increases.
*
* Rewrite the COVR record setting the VALIDFLAG to '2' and
* CURRTO to the schedule effective date.
*
* Write a new COVR record with the INDEXATION-FLAG set to
* 'P', VALIDFLAG set to '1', CURRFROM set to the schedule
* effective date, and CURRTO set to maxdate.
*
* Write an INCR (Increase Pending) record.
*
* Write a report detail line showing the old and new detail.
*
* NOTE: The program no longer writes the report file
* directly, instead it uses the new reporting interface.
* It calls the report routine ONCE for the Page Headings,
* and then for each detail line.
*
* Final Processing
* ================
*
* After all the coverages have been processed as necessary...
*
* If the T6658 Optional Indicator is not spaces then trigger
* a Letter request by calling LETRQST.
*
* Write a PTRN record.
*
* Rewrite the CHDRLIF and PAYR records held earlier.
*
* Housekeeping
* ============
*
* The following Control Totals are maintained:
*
* CT01 - Components Updated (COVR)
* CT02 - Letters Requested (LETC)
* CT04 - Increase Reccords Written (INCR)
*
****************************************************************** ****
*                                                                     *
 * </pre>
 */
public class Dry5134 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5134");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private PackedDecimalData wsaaOldCpiDate = new PackedDecimalData(8, 0);
	/* Table T6658 array variables. */

	/*
	 * T6634 array variables 01 WSAA-T6634-KEY2. 01 WSAA-T6634-ARRAY. 03
	 * WSAA-T6634-REC OCCURS 800 ASCENDING KEY WSAA-T6634-KEY INDEXED BY
	 * WSAA-T6634-IX. 05 WSAA-T6634-KEY VALUE HIGH-VALUES. 05 WSAA-T6634-DATA.
	 */

	private FixedLengthStringData wsaaTr384Key2 = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaTr384Cnttype2 = new FixedLengthStringData(3).isAPartOf(wsaaTr384Key2, 0);
	private FixedLengthStringData wsaaTr384Trcode2 = new FixedLengthStringData(4).isAPartOf(wsaaTr384Key2, 3);

	private FixedLengthStringData wsaaTr384Array = new FixedLengthStringData(37500);
	private FixedLengthStringData[] wsaaTr384Rec = FLSArrayPartOfStructure(2500, 15, wsaaTr384Array, 0);
	private FixedLengthStringData[] wsaaTr384Key = FLSDArrayPartOfArrayStructure(7, wsaaTr384Rec, 0, HIVALUES);
	private FixedLengthStringData[] wsaaTr384Cnttype = FLSDArrayPartOfArrayStructure(3, wsaaTr384Key, 0);
	private FixedLengthStringData[] wsaaTr384Trcode = FLSDArrayPartOfArrayStructure(4, wsaaTr384Key, 3);
	private FixedLengthStringData[] wsaaTr384Data = FLSDArrayPartOfArrayStructure(8, wsaaTr384Rec, 7);
	private FixedLengthStringData[] wsaaTr384LetterType = FLSDArrayPartOfArrayStructure(8, wsaaTr384Data, 0);

	private FixedLengthStringData wsaaT5655Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5655Trcode = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 0);
	private FixedLengthStringData wsaaT5655Asterisk = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 4)
			.init("****");

	private FixedLengthStringData wsaaTr695Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaTr695Coverage = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 0);
	private FixedLengthStringData wsaaTr695Rider = new FixedLengthStringData(4).isAPartOf(wsaaTr695Key, 4);
	private FixedLengthStringData wsaaBasicCommMeth = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaBascpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSrvcpy = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaRnwcpy = new FixedLengthStringData(4);

	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0).init(ZERO);

	private FixedLengthStringData wsaaValidCovr = new FixedLengthStringData(1).init("N");
	private Validator validComponent = new Validator(wsaaValidCovr, "Y");
	private Validator invalidComponent = new Validator(wsaaValidCovr, "N");

	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");
	private Validator invalidContract = new Validator(wsaaValidChdr, "N");

	private FixedLengthStringData wsaaProcessContract = new FixedLengthStringData(1).init("Y");
	private Validator processContract = new Validator(wsaaProcessContract, "Y");
	private Validator ignoreContract = new Validator(wsaaProcessContract, "N");

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("Y");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");

	private FixedLengthStringData wsaaNewCoverage = new FixedLengthStringData(1).init("Y");
	private Validator newContract = new Validator(wsaaNewCoverage, "Y");
	private Validator sameContract = new Validator(wsaaNewCoverage, "N");
	private PackedDecimalData wsaaTransDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaTransTime = new PackedDecimalData(6, 0);

	private FixedLengthStringData wsaaIncrWritten = new FixedLengthStringData(1);
	private Validator incrWritten = new Validator(wsaaIncrWritten, "Y");
	private Validator incrNotWritten = new Validator(wsaaIncrWritten, "N");

	private FixedLengthStringData wsaaIncrExist = new FixedLengthStringData(1);
	private Validator incrExist = new Validator(wsaaIncrExist, "Y");
	private Validator incrNotExist = new Validator(wsaaIncrExist, "N");

	private FixedLengthStringData wsaaDataProcessed = new FixedLengthStringData(1);
	private Validator dataProcessed = new Validator(wsaaDataProcessed, "Y");
	private Validator dataNotProcessed = new Validator(wsaaDataProcessed, "N");
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(3, 0);

	/* WSYS-SYSTEM-ERROR-PARAMS */
	private FixedLengthStringData wsysCovrkey = new FixedLengthStringData(22);
	private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysCovrkey, 0);
	private FixedLengthStringData wsysCoverage = new FixedLengthStringData(2).isAPartOf(wsysCovrkey, 8);

	private IntegerData wsaaT5687Ix = new IntegerData();
	private IntegerData wsaaT6658Ix = new IntegerData();
	private IntegerData wsaaTr384Ix = new IntegerData();
	private IncrrgpTableDAM incrrgpIO = new IncrrgpTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Incrsrec incrsrec = new Incrsrec();
	private Letrqstrec letrqstrec = new Letrqstrec();
	private T5654rec t5654rec = new T5654rec();
	private T5655rec t5655rec = new T5655rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6658rec t6658rec = new T6658rec();
	private Tr384rec tr384rec = new Tr384rec();
	private Tr695rec tr695rec = new Tr695rec();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	private static final String ITEMREC = "ITEMREC";
	private WsaaT5687ArrayInner wsaaT5687ArrayInner = new WsaaT5687ArrayInner();
	private WsaaT6658ArrayInner wsaaT6658ArrayInner = new WsaaT6658ArrayInner();
	private Premiumrec premiumrec = new Premiumrec();
	private T5675rec t5675rec = new T5675rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private AnnyTableDAM annyIO = new AnnyTableDAM();
	private LifergpTableDAM lifergpIO = new LifergpTableDAM();
	private ChdrlnbTableDAM chdrlnbIO = new ChdrlnbTableDAM();
	private String chdrlnbrec = "CHDRLNBREC";
	private Agecalcrec agecalcrec = new Agecalcrec();
	private boolean ispermission;

	private static final String H791 = "H791";
	private static final String T6658 = "T6658";

	private RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
	private Rcvdpf rcvdPFObject = new Rcvdpf();
	private ExternalisedRules er = new ExternalisedRules();

	private Map<String, List<Itempf>> t5687Map = new HashMap<>();
	private Map<String, List<Itempf>> t6658Map = new HashMap<>();
	private Map<String, List<Itempf>> tr384Map = new HashMap<>();
	private Map<String, List<Itempf>> tr695Map = new HashMap<>();
	private Map<String, List<Itempf>> t5654Map = new HashMap<>();
	private Map<String, List<Chdrpf>> chdrlifRgpMap = new HashMap<>();
	private Map<String, List<Covrpf>> covrincMap = new HashMap<>();
	private Map<String, List<Incrpf>> incrMap = new HashMap<>();
	private Map<String, List<Payrpf>> payrMap = new HashMap<>();
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAO.class);
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private Dry5134DAO dry5134DAO = getApplicationContext().getBean("dry5134DAO", Dry5134DAO.class);

	private List<Covrpf> updateCovrincDateList;
	private List<Covrpf> updateCovrincList;
	private List<Covrpf> insertCovrincList;
	private List<Payrpf> updatePayrList;
	private List<Incrpf> insertIncrList;
	private Map<Long, Chdrpf> updateChdrlifMapTranno;
	private int ct01Value = 1;
	private int ct02Value = 2;
	private int ct03Value = 3;
	private int ct04Value = 4;
	private int ct05Value = 5;
	private int ct06Value = 6;
	private int ct07Value = 7;
	private int ct08Value = 8;
	private int ct09Value = 9;
	private int ct10Value = 10;
	private Chdrpf chdrlifIO;
	private boolean stampDutyflag;
	private boolean isFoundPro;
	private boolean changeExist;
	private PackedDecimalData wsaaProCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaLastCpiDate = new PackedDecimalData(8, 0);
	private PackedDecimalData tranno = new PackedDecimalData(5, 0);
	private FixedLengthStringData wsaaFreq = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaOriginst01 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOriginst02 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOriginst03 = new PackedDecimalData(17, 2);
	private PackedDecimalData wsaaOrigsum = new PackedDecimalData(17, 2);
	private boolean autoIncrflag;
	private boolean reinstated;
	private boolean prmhldtrad;
	private List<Ptrnpf> insertPtrnList = null;

	public Dry5134() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void startProcessing() {
		initialise1000();
		List<Dry5134Dto> dry5134dtoList = readChunkRecord();
		int i = 1;
		for (Dry5134Dto dto : dry5134dtoList) {
			readFile2000(dto);
			edit2500(dto);
			if (isEQ(wsspEdterror, Varcom.oK)) {
				update3000(dto);
				if (i == dry5134dtoList.size()) {
					contractChange3900(dto);
				}
			}
			i++;
		}
		updateDates();
		commit3500();

	}

	protected void initialise1000() {
		initialise1001();
		setUpHeadingDates1002();
	}

	protected void initialise1001() {

		ispermission = FeaConfg.isFeatureExist(drypDryprcRecInner.drypCompany.toString(), "NBPROP08", appVars, "IT");
		wsspEdterror.set(Varcom.oK);
		initialize(wsaaT5687ArrayInner.wsaaT5687Array);
		initialize(wsaaT6658ArrayInner.wsaaT6658Array);
		initialize(wsaaTr384Array);
		autoIncrflag = FeaConfg.isFeatureExist("2", "CSCOM011", appVars, "IT");
	}

	protected void setUpHeadingDates1002() {
		headingDateInitialize();

		/* Load in the information from T5687. This will be searched */
		/* later to determine the anniversary method for the contract, */
		/* which will then be used to access the T6658-Array. */
		String coy = drypDryprcRecInner.drypCompany.toString();
		t5687Map = itemDAO.loadSmartTable("IT", coy, "T5687");
		wsaaT5687Ix.set(1);
		loadT56871100();
		/* Read T6658 and load in details of the subroutine. */
		t6658Map = itemDAO.loadSmartTable("IT", coy, T6658);
		wsaaT6658Ix.set(1);
		loadT66581200();

		/* Load T6634, Auto Letters, to get the letter type for each */
		/* contract. */
		tr384Map = itemDAO.loadSmartTable("IT", coy, "TR384");
		wsaaTr384Ix.set(1);
		loadTr3841300();

		/* Read T5655 for lead days and from it calculate */
		/* the Effective Date. */
		wsaaT5655Trcode.set(drypDryprcRecInner.drypBatctrcde);
		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(Varcom.oK);
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl("T5655");
		itemIO.setItemitem(wsaaT5655Key);
		itemIO.setFormat(ITEMREC);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		}
		t5655rec.t5655Rec.set(itemIO.getGenarea());
		if (isEQ(t5655rec.leadDays, SPACES)) {

			drylogrec.params.set(itemIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.freqFactor.set(t5655rec.leadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {

			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		processContract.setTrue();
		dataNotProcessed.setTrue();
		tr695Map = itemDAO.loadSmartTable("IT", coy, "TR695");
		t5654Map = itemDAO.loadSmartTable("IT", coy, "T5654");
		String stampdutyItem = "NBPROP01";
		stampDutyflag = FeaConfg.isFeatureExist(coy, stampdutyItem, appVars, "IT");
	}

	private void headingDateInitialize() {
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.function.set(Varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaTransDate.set(datcon1rec.intDate);
		wsaaTransTime.set(getCobolTime());
		/* Read T5679 for valid status codes for contract header and */
		/* coverage. */
		itemIO.setDataArea(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl("T5679");

		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(ITEMREC);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());

	}

	protected void loadT56871100() {
		/* If no item is retrieved or the item retrieved is not from */
		/* the correct table, if this is the first read, this is a */
		/* fatal error. If on a subsequent read, simply move ENDP to */
		/* the status to end the loop. */
		if (t5687Map == null || t5687Map.isEmpty()) {

			drylogrec.params.set("t5687");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* If the number of items stored exceeds the working storage */
		/* array size, this is a fatal error. */
		if (t5687Map.size() > 1000) {
			drylogrec.params.set("t5687");
			drylogrec.statuz.set(H791);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

		for (List<Itempf> items : t5687Map.values()) {
			int wsaaT5687IxInt = wsaaT5687Ix.toInt();
			for (Itempf item : items) {
				/* Move the necessary data from the table to the working */
				/* storage array. */

				t5687rec.t5687Rec.set(StringUtil.rawToString(item.getGenarea()));
				wsaaT5687ArrayInner.wsaaT5687Currfrom[wsaaT5687IxInt].set(item.getItmfrm());
				wsaaT5687ArrayInner.wsaaT5687Crtable[wsaaT5687IxInt].set(item.getItemitem());
				wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687IxInt].set(t5687rec.anniversaryMethod);
				wsaaT5687ArrayInner.wsaaT5687Bcmthd[wsaaT5687IxInt].set(t5687rec.basicCommMeth);
				wsaaT5687ArrayInner.wsaaT5687Bascpy[wsaaT5687IxInt].set(t5687rec.bascpy);
				wsaaT5687ArrayInner.wsaaT5687Rnwcpy[wsaaT5687IxInt].set(t5687rec.rnwcpy);
				wsaaT5687ArrayInner.wsaaT5687Srvcpy[wsaaT5687IxInt].set(t5687rec.srvcpy);
				wsaaT5687Ix.add(1);
			}

		}
	}

	protected void loadT66581200() {
		/* If no item is retrieved or the item retrieved is not from */
		/* the correct table, if this is the first read, this is a */
		/* fatal error. If on a subsequent read, simply move ENDP to */
		/* the status to end the loop. */
		if (t6658Map == null || t6658Map.isEmpty()) {
			drylogrec.params.set(T6658);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* If the number of items stored exceeds the working storage */
		/* array size, this is a fatal error. */
		if (t6658Map.size() > 1000) {
			drylogrec.params.set(T6658);
			drylogrec.statuz.set(H791);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		for (List<Itempf> items : t6658Map.values()) {
			for (Itempf item : items) {
				/* Move the necessary data from the table to the working */
				/* storage array. */
				t6658rec.t6658Rec.set(StringUtil.rawToString(item.getGenarea()));
				wsaaT6658ArrayInner.wsaaT6658Annmthd[wsaaT6658Ix.toInt()].set(item.getItemitem());
				wsaaT6658ArrayInner.wsaaT6658Currfrom[wsaaT6658Ix.toInt()].set(item.getItmfrm());
				wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()].set(t6658rec.billfreq);
				wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()].set(t6658rec.fixdtrm);
				wsaaT6658ArrayInner.wsaaT6658Manopt[wsaaT6658Ix.toInt()].set(t6658rec.manopt);
				wsaaT6658ArrayInner.wsaaT6658Maxpcnt[wsaaT6658Ix.toInt()].set(t6658rec.maxpcnt);
				wsaaT6658ArrayInner.wsaaT6658Minpcnt[wsaaT6658Ix.toInt()].set(t6658rec.minpcnt);
				wsaaT6658ArrayInner.wsaaT6658Optind[wsaaT6658Ix.toInt()].set(t6658rec.optind);
				wsaaT6658ArrayInner.wsaaT6658Subprog[wsaaT6658Ix.toInt()].set(t6658rec.subprog);
				wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()].set(t6658rec.premsubr);

				wsaaT6658ArrayInner.wsaaT6658SimpInd[wsaaT6658Ix.toInt()].set(t6658rec.simpleInd);
				wsaaT6658ArrayInner.wsaaT6658IncrFlg[wsaaT6658Ix.toInt()].set(t6658rec.incrFlg);
				wsaaT6658ArrayInner.wsaaT6658CompoundInd[wsaaT6658Ix.toInt()].set(t6658rec.compoundInd);
				wsaaT6658Ix.add(1);
			}
		}
	}

	/**
	 * <pre>
	*1300-LOAD-T6634 SECTION.
	 * </pre>
	 */
	protected void loadTr3841300() {
		/* If no item is retrieved or the item retrieved is not from */
		/* the correct table, if this is the first read, this is a */
		/* fatal error. If on a subsequent read, simply move ENDP to */
		/* the status to end the loop. */
		if (tr384Map == null || tr384Map.isEmpty()) {

			drylogrec.params.set("tr384");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* If the number of items stored exceeds the working storage */
		/* array size, this is a fatal error. */
		/* IF WSAA-T6634-IX > WSAA-T6634-SIZE */
		if (tr384Map.size() > 2500) {
			drylogrec.params.set("tr384");
			drylogrec.statuz.set(H791);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		int wsaaTr384IxInt = wsaaTr384Ix.toInt();
		for (List<Itempf> items : tr384Map.values()) {
			for (Itempf item : items) {
				/* Move the necessary data from the table to the working */
				/* storage array. */
				/* MOVE ITEM-GENAREA TO T6634-T6634-REC. */
				/* MOVE ITEM-ITEMITEM TO WSAA-T6634-KEY */
				/* (WSAA-T6634-IX). */
				/* MOVE T6634-LETTER-TYPE TO WSAA-T6634-LETTER-TYPE */
				/* (WSAA-T6634-IX). */
				/* SET WSAA-T6634-IX-MAX TO WSAA-T6634-IX. */
				/* SET WSAA-T6634-IX UP BY 1. */
				tr384rec.tr384Rec.set(StringUtil.rawToString(item.getGenarea()));
				wsaaTr384Key[wsaaTr384IxInt].set(item.getItemitem());
				wsaaTr384LetterType[wsaaTr384Ix.toInt()].set(tr384rec.letterType);
				wsaaTr384Ix.add(1);
			}

		}
	}

	protected void readFile2000(Dry5134Dto dry5134dto) {

		ct01Value++;

		newContract.setTrue();
		prmhldtrad = FeaConfg.isFeatureExist(dry5134dto.getChdrcoy(), "CSOTH010", appVars, "IT");/* IJTI-1523 */
		if (prmhldtrad) {
			if (isEQ(dry5134dto.getChdrnum(), wsysChdrnum))
				sameContract.setTrue();
		} else {
			if (isEQ(dry5134dto.getChdrnum(), wsysChdrnum) && isEQ(dry5134dto.getCoverage(), wsysCoverage)) {// ILIFE-8037
				sameContract.setTrue();
			}
		}
		wsysChdrnum.set(dry5134dto.getChdrnum());
		wsysCoverage.set(dry5134dto.getCoverage());

	}

	protected void contractChange3900(Dry5134Dto dry5134dto) {
		/* CHANGE */
		/* FOR THE OLD CONTRACT... */
		/* Check to see if an INCR was written. */
		/* If an INCR does exist for the old contract */
		/* write a PTRN and check the "Optional" indicator */
		/* if the optional indicator on T6658 not = spaces */
		/* generate a letter request for later */
		if (incrWritten.isTrue() || (prmhldtrad && reinstated)) {
			writePtrn3980(dry5134dto);
			if (isFoundPro) {
				while (isLT(wsaaLastCpiDate, wsaaProCpiDate)) {
					datcon2rec.datcon2Rec.set(SPACES);
					datcon2rec.intDate1.set(wsaaLastCpiDate);
					datcon2rec.frequency.set("01");
					datcon2rec.freqFactor.set(wsaaFreq);
					callProgram(Datcon2.class, datcon2rec.datcon2Rec);
					if (isNE(datcon2rec.statuz, Varcom.oK)) {
						drylogrec.params.set(datcon2rec.datcon2Rec);
						drylogrec.statuz.set(datcon2rec.statuz);
						drylogrec.drySystemError.setTrue();
						a000FatalError();
					}
					wsaaLastCpiDate.set(datcon2rec.intDate2.toInt());
				}
			}
			if (isNE(wsaaT6658ArrayInner.wsaaT6658Optind[wsaaT6658Ix.toInt()], SPACES)
					|| isNE(wsaaT6658ArrayInner.wsaaT6658Manopt[wsaaT6658Ix.toInt()], SPACES)) {
				letterRequest3930(dry5134dto);
			}
		} else {
			writePtrn3980(dry5134dto);
		}
		/* FOR THE NEW CONTRACT... */
		/* Set various flags and save contract details for later. */
		processContract.setTrue();
		incrNotWritten.setTrue();
		/* EXIT */
	}

	protected void letterRequest3930(Dry5134Dto dry5134dto) {
		if (searchT6634Array3931()) {
			ct02Value++;
		}
		/* Expected exception for control flow purposes. */
	}

	protected void writePtrn3980(Dry5134Dto dry5134dto) {

		/* Write PTRN record. */
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrpfx(chdrlifIO.getChdrpfx());
		ptrnIO.setChdrcoy(dry5134dto.getChdrcoy());

		ptrnIO.setChdrnum(dry5134dto.getChdrnum());
		if (isFoundPro) {
			ptrnIO.setTranno(tranno.toInt());
			compute(tranno, 0).set(add(tranno, 1));
		} else {
			ptrnIO.setTranno(chdrlifIO.getTranno());
		}
		ptrnIO.setTrdt(drypDryprcRecInner.drypEffectiveDate.toInt());
		ptrnIO.setTrtm(wsaaTransTime.toInt());
		if (isFoundPro) {
			ptrnIO.setPtrneff(wsaaLastCpiDate.toInt());
		} else {
			ptrnIO.setPtrneff(wsaaOldCpiDate.toInt());
		}
		ptrnIO.setDatesub(drypDryprcRecInner.drypEffectiveDate.toInt());
		ptrnIO.setUserT(drypDryprcRecInner.drypUser.toInt());
		varcom.vrcmTranid.set(SPACES);
		ptrnIO.setTermid(SPACES.toString());
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx.toString());
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany.toString());
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBranch.toString());
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr.toInt());
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn.toInt());
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch.toString());
		if (insertPtrnList == null) {
			insertPtrnList = new ArrayList<>();
		}
		insertPtrnList.add(ptrnIO);
		ct05Value++;
	}

	private List<Dry5134Dto> readChunkRecord() {

		List<Dry5134Dto> dry5134dtoList = dry5134DAO.getPendingAutoIncDetails(drypDryprcRecInner.drypCompany.toString(),
				datcon2rec.intDate2.toString(), drypDryprcRecInner.drypEntity.toString());

		if (!dry5134dtoList.isEmpty()) {
			List<String> chdrNumList = Collections.singletonList(drypDryprcRecInner.drypEntity.toString());
			chdrlifRgpMap = chdrpfDAO.searchChdrRecordByChdrnum(chdrNumList);
			covrincMap = covrpfDAO.searchValidCovrByChdrnum(chdrNumList);
			incrMap = incrpfDAO.searchIncrRecordByChdrnum(chdrNumList);
			payrMap = payrpfDAO.searchPayrRecordByChdrnumChdrcoy(drypDryprcRecInner.drypCompany.trim(), chdrNumList);
		}

		return dry5134dtoList;
	}

	protected void edit2500(Dry5134Dto dry5134dto) {
		/* Check record is required for processing. */
		/* Note that if an individual component does not have */
		/* the right status, that component is ignored but */
		/* the rest of the contract is processed. */
		if (ignoreContract.isTrue()) {
			wsspEdterror.set(SPACES);
			ct09Value++;
			return;

		}
		Chdrpf chdrrgpIO = readChdrrgp2700(dry5134dto);
		invalidStatus.setTrue();
		checkContractStatus2800(chdrrgpIO);
		if (invalidStatus.isTrue()) {
			ignoreContract.setTrue();
			wsspEdterror.set(SPACES);

			ct09Value++;

			return;
		}
		invalidStatus.setTrue();
		checkComponentStatus2600(dry5134dto);
		if (invalidStatus.isTrue()) {
			wsspEdterror.set(SPACES);

			ct10Value++;

			return;
		}
		wsspEdterror.set(Varcom.oK);
	}

	protected void checkComponentStatus2600(Dry5134Dto dry5134dto) {
		if (risk2601(dry5134dto)) {
			prem2602(dry5134dto);
		}
	}

	protected boolean risk2601(Dry5134Dto dry5134dto) {
		invalidComponent.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12) || validComponent.isTrue()); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], dry5134dto.getStatcode())) {
				validComponent.setTrue();
			}
		}
		if (!validComponent.isTrue()) {
			wsspEdterror.set(SPACES);
			return false;
		}
		return true;
	}

	protected void prem2602(Dry5134Dto dry5134dto) {
		invalidComponent.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12) || validComponent.isTrue()); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()], dry5134dto.getPstatcode())) {
				validComponent.setTrue();
			}
		}
		if (!validComponent.isTrue()) {
			wsspEdterror.set(SPACES);
			return;
		}
		validStatus.setTrue();
	}

	protected Chdrpf readChdrrgp2700(Dry5134Dto dry5134dto) {
		Chdrpf chdrrgpIO = null;
		if (chdrlifRgpMap != null) {
			for (Chdrpf c : chdrlifRgpMap.get(dry5134dto.getChdrnum())) {
				if (c.getChdrcoy().toString().equals(dry5134dto.getChdrcoy())) {/* IJTI-1523 */
					chdrrgpIO = c;
					break;
				}
			}
		}
		if (chdrrgpIO == null) {
			drylogrec.params.set(dry5134dto.getChdrnum());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		return chdrrgpIO;
	}

	protected void checkContractStatus2800(Chdrpf chdrrgpIO) {
		if (risk2801(chdrrgpIO)) {
			prem2802(chdrrgpIO);
		}
		/* Expected exception for control flow purposes. */
	}

	protected boolean risk2801(Chdrpf chdrrgpIO) {
		invalidContract.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12) || validContract.isTrue()); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()], chdrrgpIO.getStatcode())) {
				validContract.setTrue();
			}
		}
		if (!validContract.isTrue()) {
			wsspEdterror.set(SPACES);
			return false;
		}
		return true;
	}

	protected void prem2802(Chdrpf chdrrgpIO) {
		invalidContract.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12) || validContract.isTrue()); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()], chdrrgpIO.getPstcde())) {
				validContract.setTrue();
			}
		}
		if (!validContract.isTrue()) {
			wsspEdterror.set(SPACES);
			return;
		}
		validStatus.setTrue();
	}

	protected void update3000(Dry5134Dto dry5134dto) {

		processContract.setTrue();
		readChdrlif3960(dry5134dto);
		dataProcessed.setTrue();
		boolean reinstflag = FeaConfg.isFeatureExist(drypDryprcRecInner.drypCompany.toString(), "CSLRI003", appVars,
				"IT");
		if (reinstflag) {
			wsaaSub.set(ZERO);
			markprorated(dry5134dto);
		}
		if (isFoundPro) {
			Payrpf payrobj = update3001(dry5134dto);
			if (changeExist) {
				updateCovr3800(dry5134dto);
				changeExist = false;
			}
			rewriteChdrlifPayr3950(payrobj);
		} else {
			searchT56875100(dry5134dto);
			if (isNE(dry5134dto.getRider(), "00")) {
				a100ReadTableTr695(dry5134dto);
			}
			searchT66585200(dry5134dto);
			Payrpf p = callIncPremsubr3100(dry5134dto);
			/* If there has been no increase on the component even though */
			/* one is allowed, the CPI-DATE on the COVR must still be */
			/* updated. This will prevent selection until the next time an */
			/* increase is due. */
			if (isEQ(incrsrec.currsum, incrsrec.newsum) && isEQ(incrsrec.currinst01, incrsrec.newinst01)) {
				noIncreaseOnCovr3050(dry5134dto);
				rewriteChdrlifPayr3950(p);
				return;
			}
			if (isEQ(incrsrec.currsum, ZERO) && isEQ(incrsrec.currinst01, ZERO)) {
				noIncreaseOnCovr3050(dry5134dto);
				rewriteChdrlifPayr3950(p);
				return;
			}
			existingIncrease3400();
			writeIncr3300(dry5134dto);
			write3710(dry5134dto);
			updateCovr3800(dry5134dto);
			rewriteChdrlifPayr3950(p);
		}
	}

	private Payrpf update3001(Dry5134Dto dry5134dto) {
		Payrpf payrobj = null;
		while (isLT(dry5134dto.getCpiDate(), wsaaProCpiDate)) {
			searchT56875100(dry5134dto);
			if (isNE(dry5134dto.getRider(), "00")) {
				a100ReadTableTr695(dry5134dto);
			}
			searchT66585200(dry5134dto);
			payrobj = callIncPremsubr3100(dry5134dto);
			if ((isEQ(incrsrec.currsum, incrsrec.newsum) && isEQ(incrsrec.currinst01, incrsrec.newinst01))
					|| (isEQ(incrsrec.currsum, ZERO) && isEQ(incrsrec.currinst01, ZERO))) {
				setupNextCall(dry5134dto);
				noIncreaseOnCovr3050(dry5134dto);
			} else {
				changeExist = true;
				wsaaSub.add(1);
				existingIncrease3400();
				writeIncr3300(dry5134dto);
				write3710(dry5134dto);
				setupNextCall(dry5134dto);
			}
		}
		return payrobj;
	}

	protected void markprorated(Dry5134Dto dry5134dto) {
		List<Ptrnpf> ptrnrecords = null;
		ptrnrecords = ptrnpfDAO.searchPtrnenqRecord(dry5134dto.getChdrcoy(), dry5134dto.getChdrnum());
		if (ptrnrecords != null) {
			for (Ptrnpf ptrn : ptrnrecords) {
				if (isNE(ptrn.getBatctrcde(), "TA85") && isNE(ptrn.getBatctrcde(), "B523")) {
					continue;
				}
				if (isEQ(ptrn.getBatctrcde(), "B523")) {
					break;
				}
				if (isEQ(ptrn.getBatctrcde(), "TA85")) {
					isFoundPro = true;
					calcProCpi();
				}
			}
		}
	}

	protected void calcProCpi() {

		datcon2rec.freqFactor.set(t5655rec.leadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(drypDryprcRecInner.drypEffectiveDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaProCpiDate.set(datcon2rec.intDate2);
		if (isEQ(wsaaLastCpiDate, ZERO)) {
			wsaaLastCpiDate.set(chdrlifIO.getBtdate());
			tranno.set(chdrlifIO.getTranno());
			wsaaFreq.set(chdrlifIO.getBillfreq());
		}

	}

	protected void setupNextCall(Dry5134Dto dry5134dto) {

		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.intDate1.set(dry5134dto.getCpiDate());
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		dry5134dto.setCpiDate(datcon2rec.intDate2.toInt());
		dry5134dto.setInstprem(incrsrec.newinst01.getbigdata());
		dry5134dto.setZbinstprem(incrsrec.newinst02.getbigdata());
		dry5134dto.setZlinstprem(incrsrec.newinst03.getbigdata());
		dry5134dto.setSumins(incrsrec.newsum.getbigdata());
		wsaaOriginst01.set(incrsrec.originst01);
		wsaaOriginst02.set(incrsrec.originst02);
		wsaaOriginst03.set(incrsrec.originst03);
		wsaaOrigsum.set(incrsrec.origsum);

	}

	protected void noIncreaseOnCovr3050(Dry5134Dto dry5134dto) {
		Covrpf covrincIO = readCovrinc3051(dry5134dto);
		rewriteCovrinc3052(covrincIO);
	}

	protected Covrpf readCovrinc3051(Dry5134Dto dry5134dto) {
		Covrpf covrincIO = setCovrinc(dry5134dto);

		/* Update the COVR record with a new CPI-DATE so that it does */
		/* not get selected until the next increase 'Anniversary'. */
		datcon2rec.datcon2Rec.set(SPACES);
		if (covrincIO != null) {
			datcon2rec.intDate1.set(covrincIO.getCpiDate());
		}
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(wsaaT6658ArrayInner.wsaaT6658Billfreq[wsaaT6658Ix.toInt()]);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		return covrincIO;
	}

	protected void rewriteCovrinc3052(Covrpf covrincIO) {
		covrincIO.setCpiDate(datcon2rec.intDate2.toInt());
		if (updateCovrincDateList == null) {
			updateCovrincDateList = new ArrayList<>();
		}
		updateCovrincDateList.add(covrincIO);
		ct08Value++;
	}

	protected Payrpf callIncPremsubr3100(Dry5134Dto dry5134dto) {

		setIncrsrecVariable(dry5134dto);

		Payrpf payrIO = setPayrpf(dry5134dto);
		if (payrIO != null) {
			incrsrec.billfreq.set(payrIO.getBillfreq());
			incrsrec.mop.set(payrIO.getBillchnl());
		}
		incrsrec.occdate.set(chdrlifIO.getOccdate());
		incrsrec.cntcurr.set(dry5134dto.getPremCurrency());
		incrsrec.language.set(drypDryprcRecInner.drypLanguage);
		if (stampDutyflag) {
			incrsrec.zstpduty01.set(ZERO);
			incrsrec.zstpduty02.set(ZERO);
			incrsrec.zstpduty03.set(ZERO);
		}
		if (isEQ(wsaaT6658ArrayInner.wsaaT6658Maxpcnt[wsaaT6658Ix.toInt()], ZERO)) {
			incrsrec.maxpcnt.set(+99.999);
		} else {
			incrsrec.maxpcnt.set(wsaaT6658ArrayInner.wsaaT6658Maxpcnt[wsaaT6658Ix.toInt()]);
		}
		incrsrec.minpcnt.set(wsaaT6658ArrayInner.wsaaT6658Minpcnt[wsaaT6658Ix.toInt()]);
		incrsrec.fixdtrm.set(wsaaT6658ArrayInner.wsaaT6658Fixdtrm[wsaaT6658Ix.toInt()]);
		incrsrec.annvmeth.set(wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()]);

		checkPermission();

		if (!(AppVars.getInstance().getAppConfig().isVpmsEnable()
				&& ExternalisedRules
						.isCallExternal(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()].toString())
				&& er.isExternalized(chdrlifIO.getCnttype(), incrsrec.crtable.toString()))) /* IJTI-1523 */
		{
			callProgramX(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], incrsrec.increaseRec);
		}

		else {
			ifVpmsEnable(dry5134dto);
		}

		premiumrec.riskPrem.set(BigDecimal.ZERO);
		boolean riskPremflag = FeaConfg.isFeatureExist(dry5134dto.getChdrcoy().trim(), "NBPRP094", appVars, "IT");
		if (riskPremflag) {
			premiumrec.riskPrem.set(ZERO);
			premiumrec.cnttype.set(chdrlifIO.getCnttype());
			premiumrec.crtable.set(dry5134dto.getCrtable());
			premiumrec.calcTotPrem.set(dry5134dto.getInstprem());
			callProgram("RISKPREMIUM", premiumrec.premiumRec);

		}
		if (isEQ(incrsrec.statuz, Varcom.bomb)) {
			drylogrec.params.set(incrsrec.increaseRec);
			drylogrec.statuz.set(incrsrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		setValuesInincrsrec();

		return payrIO;
	}

	private void setIncrsrecVariable(Dry5134Dto dry5134dto) {

		incrsrec.origsum.set(ZERO);
		incrsrec.lastsum.set(ZERO);
		incrsrec.newsum.set(ZERO);
		incrsrec.originst01.set(ZERO);
		incrsrec.lastinst01.set(ZERO);
		incrsrec.newinst01.set(ZERO);
		incrsrec.currinst01.set(ZERO);
		incrsrec.originst02.set(ZERO);
		incrsrec.lastinst02.set(ZERO);
		incrsrec.newinst02.set(ZERO);
		incrsrec.currinst02.set(ZERO);
		incrsrec.originst03.set(ZERO);
		incrsrec.lastinst03.set(ZERO);
		incrsrec.newinst03.set(ZERO);
		incrsrec.currinst03.set(ZERO);
		incrsrec.pctinc.set(ZERO);
		incrsrec.calcprem.set(ZERO);
		incrsrec.inputPrevPrem.set(ZERO);
		incrsrec.currinst01.set(dry5134dto.getInstprem());
		incrsrec.currinst02.set(dry5134dto.getZbinstprem());
		incrsrec.currinst03.set(dry5134dto.getZlinstprem());
		incrsrec.currsum.set(dry5134dto.getSumins());
		getOrigLastDetails3200(dry5134dto);
		incrsrec.function.set(SPACES);
		incrsrec.statuz.set(SPACES);
		incrsrec.chdrcoy.set(dry5134dto.getChdrcoy());
		incrsrec.chdrnum.set(dry5134dto.getChdrnum());
		incrsrec.coverage.set(dry5134dto.getCoverage());
		incrsrec.life.set(dry5134dto.getLife());
		incrsrec.rider.set(dry5134dto.getRider());
		incrsrec.plnsfx.set(dry5134dto.getPlanSuffix());
		incrsrec.effdate.set(dry5134dto.getCpiDate());
		incrsrec.crtable.set(dry5134dto.getCrtable());
		incrsrec.rcesdte.set(dry5134dto.getRiskCessDate());
		incrsrec.mortcls.set(dry5134dto.getMortcls());

	}

	private void checkPermission() {
		if (ispermission) {
			incrsrec.autoincreaseindicator.set("");
			readT5654();
			if (isNE(t5654rec.indxflg, "N")) {
				if (isNE(t5654rec.cpidef, SPACES) || isNE(t5654rec.cpiallwd, SPACES))
					incrsrec.autoincreaseindicator.set("C");
				if (isNE(t5654rec.predef, SPACES) || isNE(t5654rec.predefallwd, SPACES))
					incrsrec.autoincreaseindicator.set("P");
			}

		} else {
			incrsrec.autoincreaseindicator.set("P");
		}
	}

	private void setValuesInincrsrec() {
		if (isEQ(incrsrec.newsum, ZERO)) {
			incrsrec.newsum.set(incrsrec.currsum);
		}
		if (isEQ(incrsrec.newinst01, ZERO)) {
			incrsrec.newinst01.set(incrsrec.currinst01);
		}
		if (isEQ(incrsrec.newinst02, ZERO)) {
			incrsrec.newinst02.set(incrsrec.currinst02);
		}
		if (isEQ(incrsrec.newinst03, ZERO)) {
			incrsrec.newinst03.set(incrsrec.currinst03);
		}
		if (stampDutyflag) {
			ifStmpDutyFlagEnable();

		}
	}

	private void ifStmpDutyFlagEnable() {
		if (isNE(incrsrec.newinst01, ZERO) && isNE(incrsrec.zstpduty01, ZERO)) {
			incrsrec.newinst01.set(add(incrsrec.newinst01, incrsrec.zstpduty01));
		}
		if (isNE(incrsrec.newinst02, ZERO) && isNE(incrsrec.zstpduty02, ZERO)) {
			incrsrec.newinst02.set(add(incrsrec.newinst02, incrsrec.zstpduty02));
		}
		if (isNE(incrsrec.newinst03, ZERO) && isNE(incrsrec.zstpduty03, ZERO)) {
			incrsrec.newinst03.set(add(incrsrec.newinst03, incrsrec.zstpduty03));
		}
	}

	private Payrpf setPayrpf(Dry5134Dto dry5134dto) {
		Payrpf payrIO = null;
		if (!payrMap.isEmpty()) {
			for (Payrpf p : payrMap.get(dry5134dto.getChdrnum())) {
				if (dry5134dto.getChdrcoy().equals(p.getChdrcoy()) && 1 == p.getPayrseqno()) {
					payrIO = p;
					break;
				}
			}
		}
		if (payrIO == null) {
			drylogrec.params.set(dry5134dto.getChdrnum());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		return payrIO;
	}

	private void ifVpmsEnable(Dry5134Dto dry5134dto) {

		premiumrec.waitperiod.set(SPACES);
		premiumrec.bentrm.set(SPACES);
		premiumrec.prmbasis.set(SPACES);
		premiumrec.poltyp.set(SPACES);
		premiumrec.occpcode.set(SPACES);
		if (ispermission) {
			callReadRcvdpf(dry5134dto);

			if (rcvdPFObject != null) {
				ifRcvdPFObjectIsNotNull();
			}
		}
		/* ILIFE-3915 End */
		calc6005();
		premiumrec.cnttype.set(chdrlifIO.getCnttype());
		Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
		vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
		Vpxlextrec vpxlextrec = new Vpxlextrec();
		vpxlextrec.function.set("INIT");
		callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec, vpxlextrec);

		Vpxchdrrec vpxchdrrec = new Vpxchdrrec();
		vpxchdrrec.function.set("INIT");
		callProgram(Vpxchdr.class, vpmcalcrec.vpmcalcRec, vpxchdrrec);
		premiumrec.rstaflag.set(vpxchdrrec.rstaflag);

		Vpxacblrec vpxacblrec = new Vpxacblrec();
		callProgram(Vpxacbl.class, premiumrec.premiumRec, vpxacblrec.vpxacblRec);

		premiumrec.function.set("INCR");

		itemIO.setDataArea(SPACES);
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(incrsrec.chdrcoy);
		itemIO.setItemtabl("T5687");
		itemIO.setItemitem(incrsrec.crtable);
		itemIO.setFormat(ITEMREC);
		itemIO.setFunction(Varcom.readr);
		itemIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		itemIO.setFitKeysSearch("ITEMCOY", "ITEMTABL", "ITEMITEM");
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.endp)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

		ifVpmsEnable1000(dry5134dto, vpxlextrec, vpxacblrec);

	}

	private void ifVpmsEnable1000(Dry5134Dto dry5134dto, Vpxlextrec vpxlextrec, Vpxacblrec vpxacblrec) {
		t5687rec.t5687Rec.set(itemIO.getGenarea());
		if (isEQ(lifergpIO.getStatuz(), Varcom.mrnf)) {
			premiumrec.premMethod.set(t5687rec.premmeth);
		} else {
			premiumrec.premMethod.set(t5687rec.jlPremMeth);
		}

		if (isEQ(incrsrec.annvmeth, "AN14")) {
			incrsrec.annvmeth.set("AN03");
		}
		if (isEQ(incrsrec.autoincreaseindicator, SPACE)) {
			incrsrec.autoincreaseindicator.set("P");
		}

		incrsrec.simpleInd.set(wsaaT6658ArrayInner.wsaaT6658SimpInd[wsaaT6658Ix.toInt()]);
		incrsrec.compoundInd.set(wsaaT6658ArrayInner.wsaaT6658CompoundInd[wsaaT6658Ix.toInt()]);
		Covrpf covrpf = setCoverpf(dry5134dto);
		if (covrpf != null && covrpf.getReinstated() != null && "Y".equals(covrpf.getReinstated()))// ILIFE-8509
			reinstated = true;
		if (reinstated) {
			setIncrsrec(dry5134dto);
		}
		ifVpmsEnable2000(vpxlextrec, vpxacblrec);

	}

	private void ifVpmsEnable2000(Vpxlextrec vpxlextrec, Vpxacblrec vpxacblrec) {
		callProgram(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], incrsrec.increaseRec, premiumrec,
				vpxlextrec, vpxacblrec);

		if (isEQ(incrsrec.statuz, Varcom.bomb)) {
			drylogrec.params.set(incrsrec.increaseRec);
			drylogrec.statuz.set(incrsrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isNE(incrsrec.newsum, ZERO)) {
			callProgram(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], incrsrec.increaseRec, premiumrec,
					vpxlextrec, vpxacblrec);
		}

	}

	private Covrpf setCoverpf(Dry5134Dto dry5134dto) {

		Covrpf covrpf = setCoverpf1000(dry5134dto);

		if (covrpf != null) {
			premiumrec.rstate01.set(covrpf.getZclstate() == null ? SPACE : covrpf.getZclstate());
			if (stampDutyflag) {
				LinkageInfoService linkgService = new LinkageInfoService();
				premiumrec.linkcov.set(linkgService.getLinkageInfo(covrpf.getLnkgno()));
			}
		}
		return covrpf;
	}

	private Covrpf setCoverpf1000(Dry5134Dto dry5134dto) {
		Covrpf covrpf = null;
		if (covrincMap != null) {
			for (Covrpf c : covrincMap.get(dry5134dto.getChdrnum())) {
				if (dry5134dto.getChdrcoy().equals(c.getChdrcoy()) && dry5134dto.getLife().equals(c.getLife())
						&& dry5134dto.getCoverage().equals(c.getCoverage())
						&& dry5134dto.getRider().equals(c.getRider())
						&& dry5134dto.getPlanSuffix() == c.getPlanSuffix()) {
					covrpf = c;
					break;
				}
			}
		}
		return covrpf;
	}

	private void ifRcvdPFObjectIsNotNull() {

		premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
		premiumrec.bentrm.set(rcvdPFObject.getBentrm());
		if (rcvdPFObject.getPrmbasis() != null && isEQ(rcvdPFObject.getPrmbasis(), "S"))/* ILIFE-4171 */
			premiumrec.prmbasis.set("Y");
		else
			premiumrec.prmbasis.set("");
		premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
		boolean dialdownflag = FeaConfg.isFeatureExist(chdrlifIO.getChdrcoy().toString().trim(), "NBPRP012", appVars,
				"IT");
		if (dialdownflag && rcvdPFObject.getDialdownoption() != null) {
			if (rcvdPFObject.getDialdownoption().startsWith("0")) {
				premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption().substring(1));
			} else {
				premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
			}

		}
	}

	private void readT5654() {
		Itempf item5654 = null;
		if (t5654Map != null) {
			item5654 = t5654Map.get(chdrlifIO.getCnttype()).get(0);
		}

		if (item5654 == null) {
			drylogrec.params.set("t5654:" + chdrlifIO.getCnttype());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		} else {
			t5654rec.t5654Rec.set(StringUtil.rawToString(item5654.getGenarea())); // IJTI-462
		}
	}

	protected void callReadRcvdpf(Dry5134Dto dry5134dto) {
		if (rcvdPFObject == null)
			rcvdPFObject = new Rcvdpf();
		rcvdPFObject.setChdrcoy(dry5134dto.getChdrcoy());
		rcvdPFObject.setChdrnum(dry5134dto.getChdrnum());
		rcvdPFObject.setLife(dry5134dto.getLife());
		rcvdPFObject.setCoverage(dry5134dto.getCoverage());
		rcvdPFObject.setRider(dry5134dto.getRider());
		rcvdPFObject.setCrtable(dry5134dto.getCrtable());
		rcvdPFObject = rcvdDAO.readRcvdpf(rcvdPFObject);
	}

	protected void calc6005() {
		premiumrec.function.set("INCR");
		premiumrec.chdrChdrcoy.set(incrsrec.chdrcoy);
		premiumrec.chdrChdrnum.set(incrsrec.chdrnum);
		premiumrec.lifeLife.set(incrsrec.life);
		premiumrec.lifeJlife.set("00");
		premiumrec.covrCoverage.set(incrsrec.coverage);
		premiumrec.covrRider.set(incrsrec.rider);
		premiumrec.crtable.set(incrsrec.crtable);
		premiumrec.effectdt.set(incrsrec.effdate);
		premiumrec.termdate.set(incrsrec.rcesdte);
		premiumrec.language.set(incrsrec.language);
		getLifeDetails7000();
		datcon3rec.intDate1.set(incrsrec.effdate);
		datcon3rec.intDate2.set(premiumrec.termdate);
		datcon3rec.frequency.set("01");
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if (isNE(datcon3rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		datcon3rec.freqFactor.add(0.99999);
		premiumrec.duration.set(datcon3rec.freqFactor);
		premiumrec.currcode.set(incrsrec.cntcurr);
		premiumrec.sumin.set(incrsrec.origsum);
		premiumrec.mortcls.set(incrsrec.mortcls);
		premiumrec.billfreq.set(incrsrec.billfreq);
		premiumrec.mop.set(incrsrec.mop);
		premiumrec.ratingdate.set(incrsrec.occdate);
		premiumrec.reRateDate.set(incrsrec.occdate);
		premiumrec.calcPrem.set(incrsrec.currinst01);// ILIFE-7336
		premiumrec.calcBasPrem.set(ZERO);
		premiumrec.calcLoaPrem.set(ZERO);
		if (isEQ(t5675rec.premsubr, SPACES)) {
			incrsrec.newinst01.set(incrsrec.currinst01);
		}
		getAnny8000();

	}

	protected void getLifeDetails7000() {
		lifergpIO.setChdrcoy(incrsrec.chdrcoy);
		lifergpIO.setChdrnum(incrsrec.chdrnum);
		lifergpIO.setLife(incrsrec.life);
		lifergpIO.setJlife("00");
		lifergpIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, lifergpIO);
		if (isNE(lifergpIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(lifergpIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();

		}
		calculateAnb7500();
		premiumrec.lage.set(wsaaAnb);
		premiumrec.lsex.set(lifergpIO.getCltsex());
		lifergpIO.setChdrcoy(incrsrec.chdrcoy);
		lifergpIO.setChdrnum(incrsrec.chdrnum);
		lifergpIO.setLife(incrsrec.life);
		lifergpIO.setJlife("01");
		lifergpIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, lifergpIO);
		if (isNE(lifergpIO.getStatuz(), Varcom.oK) && isNE(lifergpIO.getStatuz(), Varcom.mrnf)) {
			drylogrec.params.set(lifergpIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(lifergpIO.getStatuz(), Varcom.mrnf)) {
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(0);
		} else {
			calculateAnb7500();
			premiumrec.jlage.set(wsaaAnb);
			premiumrec.jlsex.set(lifergpIO.getCltsex());
		}
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(incrsrec.chdrcoy);
		itemIO.setItemtabl("T5675");
		itemIO.setFunction(Varcom.readr);
		if (isEQ(lifergpIO.getStatuz(), Varcom.mrnf)) {
			itemIO.setItemitem(t5687rec.premmeth);
		} else {
			itemIO.setItemitem(t5687rec.jlPremMeth);
		}
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK) && isNE(itemIO.getStatuz(), Varcom.mrnf)) {

			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(itemIO.getStatuz(), Varcom.mrnf)) {
			t5675rec.premsubr.set(SPACES);
			return;
		}
		t5675rec.t5675Rec.set(itemIO.getGenarea());
	}

	protected void getAnny8000() {
		annyIO.setChdrcoy(incrsrec.chdrcoy);
		annyIO.setChdrnum(incrsrec.chdrnum);
		annyIO.setLife(incrsrec.life);
		annyIO.setCoverage(incrsrec.coverage);
		annyIO.setRider(incrsrec.rider);
		annyIO.setPlanSuffix(incrsrec.plnsfx);
		annyIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, annyIO);
		if (isNE(annyIO.getStatuz(), Varcom.oK) && isNE(annyIO.getStatuz(), Varcom.mrnf)) {

			drylogrec.params.set(annyIO.getParams());
			drylogrec.statuz.set(annyIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(annyIO.getStatuz(), Varcom.oK)) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		} else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

	protected void calculateAnb7500() {

		chdrlnbIO.setFormat(chdrlnbrec);
		chdrlnbIO.setChdrcoy(incrsrec.chdrcoy);
		chdrlnbIO.setChdrnum(incrsrec.chdrnum);
		chdrlnbIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, chdrlnbIO);
		if (isNE(chdrlnbIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(chdrlnbIO.getParams());
			drylogrec.statuz.set(chdrlnbIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy("0");
		itemIO.setItemtabl("T1693");
		itemIO.setItemitem(incrsrec.chdrcoy);
		itemIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		T1693rec t1693rec = new T1693rec();
		t1693rec.t1693Rec.set(itemIO.getGenarea());
		initialize(agecalcrec.agecalcRec);
		agecalcrec.function.set("CALCP");
		agecalcrec.cnttype.set(chdrlnbIO.getCnttype());
		agecalcrec.intDate1.set(lifergpIO.getCltdob());
		agecalcrec.intDate2.set(incrsrec.effdate);
		agecalcrec.company.set(t1693rec.fsuco);
		agecalcrec.language.set(incrsrec.language);
		callProgram(Agecalc.class, agecalcrec.agecalcRec);
		if (isNE(agecalcrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(agecalcrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaAnb.set(agecalcrec.agerating);
	}

	protected void getOrigLastDetails3200(Dry5134Dto dry5134dto) {
		setToCurrent3201(dry5134dto);
		read3201(dry5134dto);
	}

	/**
	 * <pre>
	**** Before starting set the Last and Original figures to the
	**** current COVR details.
	 * </pre>
	 */
	protected void setToCurrent3201(Dry5134Dto dry5134dto) {
		if (isFoundPro && changeExist) {
			incrsrec.originst01.set(wsaaOriginst01);
			incrsrec.originst02.set(wsaaOriginst02);
			incrsrec.originst03.set(wsaaOriginst03);
			incrsrec.origsum.set(wsaaOrigsum);
		} else {
			incrsrec.originst01.set(dry5134dto.getInstprem());
			incrsrec.originst02.set(dry5134dto.getZbinstprem());
			incrsrec.originst03.set(dry5134dto.getZlinstprem());
			incrsrec.origsum.set(dry5134dto.getSumins());
		}
		incrsrec.lastinst01.set(dry5134dto.getInstprem());
		incrsrec.lastinst02.set(dry5134dto.getZbinstprem());
		incrsrec.lastinst03.set(dry5134dto.getZlinstprem());
		incrsrec.lastsum.set(dry5134dto.getSumins());
		incrsrec.calcprem.set(dry5134dto.getInstprem());
	}

	/**
	 * <pre>
	**** Read the original INCR records to obtain the last and
	**** original premium / sum assured details.
	 * </pre>
	 */
	protected void read3201(Dry5134Dto dry5134dto) {
		if (isFoundPro && changeExist) {
			return;
		}
		if (!incrMap.isEmpty()) {
			for (Incrpf c : incrMap.get(dry5134dto.getChdrnum())) {
				if (dry5134dto.getChdrcoy().equals(c.getChdrcoy()) && dry5134dto.getLife().equals(c.getLife())
						&& dry5134dto.getCoverage().equals(c.getCoverage())
						&& dry5134dto.getRider().equals(c.getRider()) && dry5134dto.getPlanSuffix() == c.getPlnsfx()
						&& isEQ(c.getRefusalFlag(), SPACES)) {
					if (autoIncrflag) {

						if (isEQ(wsaaT6658ArrayInner.wsaaT6658IncrFlg[wsaaT6658Ix.toInt()], "Y")) {
							incrsrec.lastinst01.set(dry5134dto.getInstprem());
							incrsrec.currinst01.set(dry5134dto.getInstprem());
							incrsrec.lastinst02.set(dry5134dto.getZbinstprem());
							incrsrec.currinst02.set(dry5134dto.getZbinstprem());
							incrsrec.lastinst03.set(dry5134dto.getZlinstprem());
							incrsrec.currinst03.set(dry5134dto.getZlinstprem());
							incrsrec.lastsum.set(dry5134dto.getSumins());
							incrsrec.currsum.set(dry5134dto.getSumins());
							incrsrec.calcprem.set(dry5134dto.getInstprem());
							incrsrec.inputPrevPrem.set(ZERO);
						} else {
							incrsrec.lastinst01.set(c.getNewinst());
							incrsrec.currinst01.set(c.getNewinst());
							incrsrec.lastinst02.set(c.getZbnewinst());
							incrsrec.currinst02.set(c.getZbnewinst());
							incrsrec.lastinst03.set(c.getZlnewinst());
							incrsrec.currinst03.set(c.getZlnewinst());
							incrsrec.lastsum.set(c.getNewsum());
							incrsrec.currsum.set(c.getNewsum());
							incrsrec.calcprem.set(c.getNewinst());
							incrsrec.inputPrevPrem.set(c.getLastInst());

							if (isEQ(dry5134dto.getInstprem(), incrsrec.originst01)
									&& isEQ(dry5134dto.getSumins(), incrsrec.origsum)) {
								incrsrec.originst01.set(c.getOrigInst());
								incrsrec.originst02.set(c.getZboriginst());
								incrsrec.originst03.set(c.getZboriginst());
								incrsrec.origsum.set(c.getOrigSum());
								break;
							}
						}
					} else {
						incrsrec.lastinst01.set(c.getNewinst());
						incrsrec.currinst01.set(c.getNewinst());
						incrsrec.lastinst02.set(c.getZbnewinst());
						incrsrec.currinst02.set(c.getZbnewinst());
						incrsrec.lastinst03.set(c.getZlnewinst());
						incrsrec.currinst03.set(c.getZlnewinst());
						incrsrec.lastsum.set(c.getNewsum());
						incrsrec.currsum.set(c.getNewsum());
						/* If the contract retrieved is not the required one, (after */
						/* using BEGN), move ENDP to INCR-STATUZ to end the loop, */
						/* then exit the section. */
						if (isEQ(dry5134dto.getInstprem(), incrsrec.originst01)
								&& isEQ(dry5134dto.getSumins(), incrsrec.origsum)) {
							incrsrec.originst01.set(c.getOrigInst());
							incrsrec.originst02.set(c.getZboriginst());
							incrsrec.originst03.set(c.getZboriginst());
							incrsrec.origsum.set(c.getOrigSum());
							break;
						}
					}
				}
			}
		}
	}

	protected void writeIncr3300(Dry5134Dto dry5134dto) {
		if (incrExist.isTrue()) {
			return;
		}

		/* Set up INCR. */
		Incrpf incrIO = setIncrVariable(dry5134dto);

		if (stampDutyflag) {
			String vpmModule = "INCRSUM";
			if ((vpmModule).equals((wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()] != null
					&& isNE(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], SPACES))
							? wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()].toString().trim()
							: getStatus())) {
				incrIO.setZstpduty01(incrsrec.zstpduty01.getbigdata());
			} else {
				BigDecimal zstpduty03BigDecimal = incrsrec.zstpduty03.getbigdata();
				if (zstpduty03BigDecimal.compareTo(BigDecimal.ZERO) != 0)
					incrIO.setZstpduty01(zstpduty03BigDecimal);
				else
					incrIO.setZstpduty01(incrsrec.zstpduty02.getbigdata());
			}
		}
		rounding6000(incrIO, dry5134dto);
		incrIO.setTransactionDate(wsaaTransDate.toInt());
		incrIO.setTransactionTime(wsaaTransTime.toInt());
		varcom.vrcmTranid.set(SPACES);
		incrIO.setTermid(varcom.vrcmTermid.toString());
		if (insertIncrList == null) {
			insertIncrList = new ArrayList<>();
		}
		insertIncrList.add(incrIO);

		ct04Value++;

		/* Update flag to indicate an INCR has been written for this */
		/* component so that we write a PTRN for the contract later on */
		incrWritten.setTrue();
	}

	private Incrpf setIncrVariable(Dry5134Dto dry5134dto) {
		Incrpf incrIO = new Incrpf();
		incrIO.setTranno(chdrlifIO.getTranno());
		incrIO.setValidflag("1");
		incrIO.setStatcode(dry5134dto.getStatcode());
		incrIO.setPstatcode(dry5134dto.getPstatcode());
		incrIO.setCrtable(dry5134dto.getCrtable());
		incrIO.setChdrcoy(incrsrec.chdrcoy.toString());
		incrIO.setChdrnum(incrsrec.chdrnum.toString());
		incrIO.setLife(incrsrec.life.toString());
		incrIO.setCoverage(incrsrec.coverage.toString());
		incrIO.setRider(incrsrec.rider.toString());
		incrIO.setPlnsfx(incrsrec.plnsfx.toInt());
		incrIO.setCrrcd(incrsrec.effdate.toInt());
		incrIO.setCrtable(incrsrec.crtable.toString());
		incrIO.setAnniversaryMethod(incrsrec.annvmeth.toString());
		/* MOVE WSAA-T5687-BCMTHD(WSAA-T5687-IX) */
		/* TO INCR-BASIC-COMM-METH. */
		/* MOVE WSAA-T5687-BASCPY(WSAA-T5687-IX) */
		/* TO INCR-BASCPY. */
		/* MOVE WSAA-T5687-RNWCPY(WSAA-T5687-IX) */
		/* TO INCR-RNWCPY */
		/* MOVE WSAA-T5687-SRVCPY(WSAA-T5687-IX) */
		/* TO INCR-SRVCPY. */
		incrIO.setBasicCommMeth(wsaaBasicCommMeth.toString());
		incrIO.setBascpy(wsaaBascpy.toString());
		incrIO.setRnwcpy(wsaaRnwcpy.toString());
		incrIO.setSrvcpy(wsaaSrvcpy.toString());
		incrIO.setOrigSum(incrsrec.origsum.getbigdata());
		incrIO.setLastSum(incrsrec.lastsum.getbigdata());
		incrIO.setNewsum(incrsrec.newsum.getbigdata());
		incrIO.setOrigInst(incrsrec.originst01.getbigdata());
		incrIO.setLastInst(incrsrec.lastinst01.getbigdata());
		incrIO.setNewinst(incrsrec.newinst01.getbigdata());
		incrIO.setZboriginst(incrsrec.originst02.getbigdata());
		incrIO.setZblastinst(incrsrec.lastinst02.getbigdata());
		incrIO.setZbnewinst(incrsrec.newinst02.getbigdata());
		incrIO.setZloriginst(incrsrec.originst03.getbigdata());
		incrIO.setZllastinst(incrsrec.lastinst03.getbigdata());
		incrIO.setZlnewinst(incrsrec.newinst03.getbigdata());
		incrIO.setPctinc(incrsrec.pctinc.toInt());
		incrIO.setRefusalFlag(SPACES.toString());
		return incrIO;
	}

	private boolean getStatus() {
		return false;
	}

	protected void existingIncrease3400() {
		incrNotExist.setTrue();
		incrrgpIO.setChdrcoy(incrsrec.chdrcoy);
		incrrgpIO.setChdrnum(incrsrec.chdrnum);
		incrrgpIO.setLife(incrsrec.life);
		incrrgpIO.setCoverage(incrsrec.coverage);
		incrrgpIO.setRider(incrsrec.rider);
		incrrgpIO.setPlanSuffix(incrsrec.plnsfx);
		incrrgpIO.setFormat("INCRRGPREC");
		incrrgpIO.setFunction(Varcom.readr);
		SmartFileCode.execute(appVars, incrrgpIO);
		if (isNE(incrrgpIO.getStatuz(), Varcom.oK) && isNE(incrrgpIO.getStatuz(), Varcom.mrnf)) {

			drylogrec.params.set(incrrgpIO.getParams());
			drylogrec.statuz.set(incrrgpIO.getStatuz());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isEQ(incrrgpIO.getStatuz(), Varcom.oK) && isEQ(incrrgpIO.getCrrcd(), incrsrec.effdate)) {
			incrExist.setTrue();
		}
	}

	protected void commit3500() {
		if (insertPtrnList != null && !insertPtrnList.isEmpty()) {
			ptrnpfDAO.insertPtrnPF(insertPtrnList);
			insertPtrnList.clear();
		}
		if (updateCovrincDateList != null && !updateCovrincDateList.isEmpty()) {
			covrpfDAO.updateCpiDate(updateCovrincDateList);
			updateCovrincDateList.clear();
		}
		if (updateCovrincList != null && !updateCovrincList.isEmpty()) {
			covrpfDAO.updateCovrRecord(updateCovrincList, 0);
			updateCovrincList.clear();
		}
		if (insertCovrincList != null && !insertCovrincList.isEmpty()) {
			covrpfDAO.insertCovrRecordForL2POLRNWL(insertCovrincList);
			insertCovrincList.clear();
		}

		clearMap();
		commitControlTotals();
		/* EXIT */
	}

	private void clearMap() {
		if (updatePayrList != null && !updatePayrList.isEmpty()) {
			payrpfDAO.updatePayrTranno(updatePayrList);
			updatePayrList.clear();
		}
		if (!reinstated && insertIncrList != null && !insertIncrList.isEmpty()) {
			incrpfDAO.insertIncrList(insertIncrList);
			insertIncrList.clear();
		}
		if (updateChdrlifMapTranno != null && !updateChdrlifMapTranno.isEmpty()) {
			chdrpfDAO.updateChdrTrannoByUniqueNo(new ArrayList<>(updateChdrlifMapTranno.values()));
			updateChdrlifMapTranno.clear();
		}
	}

	private void commitControlTotals() {
		drycntrec.contotNumber.set(ct01Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct01Value = 0;

		drycntrec.contotNumber.set(ct02Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct02Value = 0;

		drycntrec.contotNumber.set(ct03Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct03Value = 0;

		drycntrec.contotNumber.set(ct04Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct04Value = 0;

		drycntrec.contotNumber.set(ct05Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct05Value = 0;

		drycntrec.contotNumber.set(ct06Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct06Value = 0;

		drycntrec.contotNumber.set(ct07Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct07Value = 0;

		drycntrec.contotNumber.set(ct08Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct08Value = 0;

		drycntrec.contotNumber.set(ct09Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct09Value = 0;

		drycntrec.contotNumber.set(ct10Value);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		ct10Value = 0;

	}

	protected void updateCovr3800(Dry5134Dto dry5134dto) {
		Covrpf covrincIO = null;
		if (covrincMap != null) {
			for (Covrpf c : covrincMap.get(dry5134dto.getChdrnum())) {
				if (dry5134dto.getChdrcoy().equals(c.getChdrcoy()) && dry5134dto.getLife().equals(c.getLife())
						&& dry5134dto.getCoverage().equals(c.getCoverage())
						&& dry5134dto.getRider().equals(c.getRider())
						&& dry5134dto.getPlanSuffix() == c.getPlanSuffix()) {
					covrincIO = c;
					break;
				}
			}
		}
		if (covrincIO == null) {
			drylogrec.params.set(dry5134dto.getChdrnum());
			drylogrec.drySystemError.setTrue();
			a000FatalError();

		} // IJTI-462 START
		else {
			updateCovrWithValidFlag2(covrincIO);
		}
	}

	private void updateCovrWithValidFlag2(Covrpf covrincIO) {

		/* Update the COVR record with a valid flag of 2 and set */
		/* 'COVR-CURRTO' date to the effective date of the Increase. */
		Covrpf updateCovrincIO = new Covrpf(covrincIO);
		updateCovrincIO.setValidflag("2");
		updateCovrincIO.setCurrto(covrincIO.getCpiDate());
		if (updateCovrincList == null) {
			updateCovrincList = new ArrayList<>();
		}
		updateCovrincList.add(updateCovrincIO);
		if (chdrlifIO != null && newContract.isTrue()) {
			if (isFoundPro) {
				compute(wsaaSub, 0).set(add(chdrlifIO.getTranno(), wsaaSub));
				chdrlifIO.setTranno(wsaaSub.toInt());
			} else {
				chdrlifIO.setTranno(chdrlifIO.getTranno() + 1);
			}
		}
		covrincIO.setValidflag("1");
		if (chdrlifIO != null) {
			covrincIO.setTranno(chdrlifIO.getTranno());// ILIFE-8472
		}
		covrincIO.setCurrfrom(covrincIO.getCpiDate());
		wsaaOldCpiDate.set(covrincIO.getCpiDate());
		covrincIO.setCurrto(varcom.vrcmMaxDate.toInt());
		covrincIO.setTransactionDate(wsaaTransDate.toInt());
		covrincIO.setTransactionTime(wsaaTransTime.toInt());
		varcom.vrcmTranid.set(SPACES);
		covrincIO.setTermid(varcom.vrcmTermid.toString());
		covrincIO.setIndexationInd("P");
		covrincIO.setRiskprem(premiumrec.riskPrem.getbigdata()); // ILIFE-7845

		if (insertCovrincList == null) {
			insertCovrincList = new ArrayList<>();
		}
		insertCovrincList.add(covrincIO);

		ct06Value++;

	}

	protected boolean searchT6634Array3931() {
		/* Search T6634 the Auto Letters table to get the letter type. */
		/* Build key to T6634 from contract type & transaction code. */
		/* MOVE CHDRLIF-CNTTYPE TO WSAA-T6634-CNTTYPE2. */
		/* MOVE BPRD-AUTH-CODE TO WSAA-T6634-TRCODE2. */
		wsaaTr384Cnttype2.set(chdrlifIO.getCnttype());

		wsaaTr384Trcode2.set(drypDryprcRecInner.drypBatctrcde);

		/* SEARCH ALL WSAA-T6634-REC */
		ArraySearch as1 = ArraySearch.getInstance(wsaaTr384Rec);
		as1.setIndices(wsaaTr384Ix);
		as1.addSearchKey(wsaaTr384Key, wsaaTr384Key2, true);
		if (!as1.binarySearch()) {

			/* MOVE '***' TO WSAA-T6634-CNTTYPE2 */
			/* SEARCH ALL WSAA-T6634-REC */
			wsaaTr384Cnttype2.set("***");
			ArraySearch as3 = ArraySearch.getInstance(wsaaTr384Rec);
			as3.setIndices(wsaaTr384Ix);
			as3.addSearchKey(wsaaTr384Key, wsaaTr384Key2, true);
			if (!as3.binarySearch()) {
				return false;
			}

		}
		/* Now call LETRQST to generate the LETCPF record. */
		initialize(letrqstrec.params);
		letrqstrec.requestCompany.set(drypDryprcRecInner.drypCompany);
		/* MOVE WSAA-T6634-LETTER-TYPE(WSAA-T6634-IX) */
		letrqstrec.letterType.set(wsaaTr384LetterType[wsaaTr384Ix.toInt()]);
		letrqstrec.letterRequestDate.set(wsaaTransDate);
		letrqstrec.clntcoy.set(chdrlifIO.getCowncoy());
		letrqstrec.clntnum.set(chdrlifIO.getCownnum());
		letrqstrec.rdocpfx.set(chdrlifIO.getChdrpfx());
		letrqstrec.rdoccoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		letrqstrec.rdocnum.set(chdrlifIO.getChdrnum());
		letrqstrec.chdrnum.set(chdrlifIO.getChdrnum());
		letrqstrec.tranno.set(chdrlifIO.getTranno());
		letrqstrec.branch.set(chdrlifIO.getCntbranch());
		letrqstrec.function.set("ADD");
		callProgram(Letrqst.class, letrqstrec.params);
		if (isNE(letrqstrec.statuz, Varcom.oK)) {

			drylogrec.params.set(letrqstrec.params);
			drylogrec.statuz.set(letrqstrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		return true;
	}

	protected void rewriteChdrlifPayr3950(Payrpf payrIO) {
		/* Rewrite the CHDRLIF and PAYR records. */
		if (updateChdrlifMapTranno == null) {
			updateChdrlifMapTranno = new LinkedHashMap<>();
		}
		if (chdrlifIO != null) {
			updateChdrlifMapTranno.put(chdrlifIO.getUniqueNumber(), chdrlifIO);
		}

		/* Rewrite the CHDRLIF and PAYR records. */
		if (updatePayrList == null) {
			updatePayrList = new ArrayList<>();
		}
		/* Update the transaction number on the PAYR file */
		/* using the transaction number from CHDRLIF. */
		if (chdrlifIO != null) {
			payrIO.setTranno(chdrlifIO.getTranno());
		}
		updatePayrList.add(payrIO);

		ct03Value++;

	}

	protected void readChdrlif3960(Dry5134Dto dry5134dto) {
		if (chdrlifRgpMap != null) {
			for (Chdrpf c : chdrlifRgpMap.get(dry5134dto.getChdrnum())) {
				if (dry5134dto.getChdrcoy().equals(c.getChdrcoy().toString())) {
					chdrlifIO = c;
					break;
				}
			}
			/**
			 * <pre>
			**** Read and hold contract header using CHDRLIF.
			 * </pre>
			 */
		}
		if (chdrlifIO == null) {

			drylogrec.params.set(dry5134dto.getChdrnum());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

	}

	protected void searchT56875100(Dry5134Dto dry5134dto) {

		/* Search table T5687 to get the anniversary method, using */
		/* CRTABLE as the key. */
		wsaaT5687Ix.set(1);
		searchlabel1: {
			for (; isLT(wsaaT5687Ix, wsaaT5687ArrayInner.wsaaT5687Rec.length); wsaaT5687Ix.add(1)) {
				if (isEQ(wsaaT5687ArrayInner.wsaaT5687Key[wsaaT5687Ix.toInt()], dry5134dto.getCrtable())
						&& isLTE(wsaaT5687ArrayInner.wsaaT5687Currfrom[wsaaT5687Ix.toInt()], dry5134dto.getCpiDate())) {
					break searchlabel1;
				}
			}

			drylogrec.statuz.set("H053");
			itdmIO.setParams(SPACES);
			itdmIO.setFunction(Varcom.begn);
			itdmIO.setStatuz(Varcom.endp);
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(T6658);
			itdmIO.setItemitem(dry5134dto.getCrtable());
			itdmIO.setItmfrm(dry5134dto.getCpiDate());

			drylogrec.params.set(itdmIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isEQ(wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()], SPACES)) {
			drylogrec.statuz.set("H053");
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(dry5134dto.getChdrcoy());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dry5134dto.getChdrnum());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dry5134dto.getLife());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dry5134dto.getCoverage());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dry5134dto.getRider());
			stringVariable1.addExpression("|");
			stringVariable1.addExpression(dry5134dto.getCrtable());
			stringVariable1.setStringInto(drylogrec.params);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaBasicCommMeth.set(wsaaT5687ArrayInner.wsaaT5687Bcmthd[wsaaT5687Ix.toInt()]);
		wsaaBascpy.set(wsaaT5687ArrayInner.wsaaT5687Bascpy[wsaaT5687Ix.toInt()]);
		wsaaRnwcpy.set(wsaaT5687ArrayInner.wsaaT5687Rnwcpy[wsaaT5687Ix.toInt()]);
		wsaaSrvcpy.set(wsaaT5687ArrayInner.wsaaT5687Srvcpy[wsaaT5687Ix.toInt()]);
	}

	protected void searchT66585200(Dry5134Dto dry5134dto) {

		/* Search table T6658 with the anniversary method, to get the */
		/* Increase Subroutine. */
		wsaaT6658Ix.set(1);
		searchlabel1: {
			for (; isLT(wsaaT6658Ix, wsaaT6658ArrayInner.wsaaT6658Rec.length); wsaaT6658Ix.add(1)) {
				if (isEQ(wsaaT6658ArrayInner.wsaaT6658Annmthd[wsaaT6658Ix.toInt()],
						wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()])
						&& isLT(wsaaT6658ArrayInner.wsaaT6658Currfrom[wsaaT6658Ix.toInt()], dry5134dto.getCpiDate())) {
					break searchlabel1;
				}
			}
			drylogrec.statuz.set("H036");
			itdmIO.setParams(SPACES);
			itdmIO.setFunction(Varcom.begn);
			itdmIO.setStatuz(Varcom.endp);
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(T6658);
			itdmIO.setItemitem(wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()]);
			itdmIO.setItmfrm(dry5134dto.getCpiDate());

			drylogrec.params.set(itdmIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (isEQ(wsaaT6658ArrayInner.wsaaT6658Premsubr[wsaaT6658Ix.toInt()], SPACES)) {
			drylogrec.params.set(wsaaT5687ArrayInner.wsaaT5687Annmthd[wsaaT5687Ix.toInt()]);
			drylogrec.statuz.set("E512");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void rounding6000(Incrpf incrIO, Dry5134Dto dry5134dto) {
		/* MOVE INCR-ORIG-SUM TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ORIG-SUM. */
		if (isNE(incrIO.getOrigSum(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getOrigSum());
			callRounding7000(dry5134dto);
			incrIO.setOrigSum(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-LAST-SUM TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-LAST-SUM. */
		if (isNE(incrIO.getLastSum(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getLastSum());
			callRounding7000(dry5134dto);
			incrIO.setLastSum(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-NEWSUM TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-NEWSUM. */
		if (isNE(incrIO.getNewsum(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getNewsum());
			callRounding7000(dry5134dto);
			incrIO.setNewsum(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ORIG-INST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ORIG-INST. */
		if (isNE(incrIO.getOrigInst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getOrigInst());
			callRounding7000(dry5134dto);
			incrIO.setOrigInst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-LAST-INST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-LAST-INST. */
		if (isNE(incrIO.getLastInst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getLastInst());
			callRounding7000(dry5134dto);
			incrIO.setLastInst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-NEWINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-NEWINST. */
		if (isNE(incrIO.getNewinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getNewinst());
			callRounding7000(dry5134dto);
			incrIO.setNewinst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZBORIGINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZBORIGINST. */
		if (isNE(incrIO.getZboriginst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZboriginst());
			callRounding7000(dry5134dto);
			incrIO.setZboriginst(zrdecplrec.amountOut.getbigdata());
		}

		rounding6100(incrIO, dry5134dto);

	}

	private void rounding6100(Incrpf incrIO, Dry5134Dto dry5134dto) {
		/* MOVE INCR-ZBLASTINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZBLASTINST. */
		if (isNE(incrIO.getZblastinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZblastinst());
			callRounding7000(dry5134dto);
			incrIO.setZblastinst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZBNEWINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZBNEWINST. */
		if (isNE(incrIO.getZbnewinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZbnewinst());
			callRounding7000(dry5134dto);
			incrIO.setZbnewinst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZLORIGINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZLORIGINST. */
		if (isNE(incrIO.getZloriginst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZloriginst());
			callRounding7000(dry5134dto);
			incrIO.setZloriginst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZLLASTINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZLLASTINST. */
		if (isNE(incrIO.getZllastinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZllastinst());
			callRounding7000(dry5134dto);
			incrIO.setZllastinst(zrdecplrec.amountOut.getbigdata());
		}
		/* MOVE INCR-ZLNEWINST TO ZRDP-AMOUNT-IN. */
		/* PERFORM 7000-CALL-ROUNDING. */
		/* MOVE ZRDP-AMOUNT-OUT TO INCR-ZLNEWINST. */
		if (isNE(incrIO.getZlnewinst(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZlnewinst());
			callRounding7000(dry5134dto);
			incrIO.setZlnewinst(zrdecplrec.amountOut.getbigdata());
		}
		if (stampDutyflag && isNE(incrIO.getZstpduty01(), 0)) {
			zrdecplrec.amountIn.set(incrIO.getZstpduty01());
			callRounding7000(dry5134dto);
			incrIO.setZstpduty01(zrdecplrec.amountOut.getbigdata());
		}

	}

	protected void callRounding7000(Dry5134Dto dry5134dto) {
		/* CALL */
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(drypDryprcRecInner.drypCompany);
		zrdecplrec.statuz.set(Varcom.oK);
		zrdecplrec.currency.set(dry5134dto.getPremCurrency());
		zrdecplrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			drylogrec.params.set(zrdecplrec.zrdecplRec);
			drylogrec.statuz.set(zrdecplrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* EXIT */
	}

	protected void a100ReadTableTr695(Dry5134Dto dry5134dto) {
		Covrpf covrincIO = setCovrinc(dry5134dto);

		if (covrincIO != null) {
			wsaaTr695Coverage.set(covrincIO.getCrtable());
		}
		wsaaTr695Rider.set(dry5134dto.getCrtable());

		Itempf tr695Item = putValueIntr695Item(dry5134dto);

		if (tr695Item == null) {
			return;
		}
		tr695rec.tr695Rec.set(StringUtil.rawToString(tr695Item.getGenarea()));
		wsaaBasicCommMeth.set(tr695rec.basicCommMeth);
		wsaaBascpy.set(tr695rec.bascpy);
		wsaaSrvcpy.set(tr695rec.srvcpy);
		wsaaRnwcpy.set(tr695rec.rnwcpy);
	}

	private Covrpf setCovrinc(Dry5134Dto dry5134dto) {
		Covrpf covrincIO = null;
		if (covrincMap != null) {
			for (Covrpf c : covrincMap.get(dry5134dto.getChdrnum())) {
				if (dry5134dto.getChdrcoy().equals(c.getChdrcoy()) && dry5134dto.getLife().equals(c.getLife())
						&& dry5134dto.getJlife().equals(c.getJlife())
						&& dry5134dto.getCoverage().equals(c.getCoverage())
						&& dry5134dto.getRider().equals(c.getRider())
						&& dry5134dto.getPlanSuffix() == c.getPlanSuffix()) {
					covrincIO = c;
					break;
				}

			}
		}
		return covrincIO;
	}

	private Itempf putValueIntr695Item(Dry5134Dto dry5134dto) {
		Itempf tr695Item = null;

		if (tr695Map == null) {
			return tr695Item;
		}

		tr695Item = getTr695ItempfRow(wsaaTr695Key.toString(), dry5134dto);

		if (tr695Item == null) {
			wsaaTr695Rider.set("****");
			String s = wsaaTr695Key.toString();
			tr695Item = getTr695ItempfRow(s, dry5134dto);
		}
		return tr695Item;
	}

	private Itempf getTr695ItempfRow(String inputItem, Dry5134Dto dry5134dto) {
		Itempf tr695Item = null;
		List<Itempf> tr695Items = tr695Map.get(inputItem);

		if (tr695Items == null) {
			return tr695Item;
		}

		for (Itempf item : tr695Items) {
			if (item.getItmfrm().compareTo(new BigDecimal(dry5134dto.getCpiDate())) <= 0) {
				tr695Item = item;
				break;
			}
		}

		return tr695Item;
	}

	protected void setIncrsrec(Dry5134Dto dry5134dto) {
		Incrpf incr = null;
		if (incrMap != null) {
			for (Incrpf incrpf : incrMap.get(dry5134dto.getChdrnum())) {
				if (dry5134dto.getChdrcoy().equals(incrpf.getChdrcoy()) && dry5134dto.getLife().equals(incrpf.getLife())
						&& dry5134dto.getCoverage().equals(incrpf.getCoverage())
						&& dry5134dto.getRider().equals(incrpf.getRider())) {
					incr = incrpf;
					break;
				}
			}
		}
		if (incr != null) {
			incrsrec.currinst01.set(incr.getLastInst());
			incrsrec.currinst02.set(incr.getZblastinst());
			incrsrec.currinst03.set(incr.getZllastinst());
			incrsrec.currsum.set(incr.getLastSum());
			incrsrec.originst01.set(incr.getOrigInst());
			incrsrec.originst02.set(incr.getZboriginst());
			incrsrec.originst03.set(incr.getZloriginst());
			incrsrec.origsum.set(incr.getOrigSum());
			incrsrec.lastinst01.set(incr.getLastInst());
			incrsrec.lastinst02.set(incr.getZblastinst());
			incrsrec.lastinst03.set(incr.getZllastinst());
			incrsrec.lastsum.set(incr.getLastSum());
		}
	}

	/*
	 * Class transformed from Data Structure WSAA-T5687-ARRAY--INNER
	 */
	private static final class WsaaT5687ArrayInner {

		public WsaaT5687ArrayInner() {
			// constructor
		}

		private FixedLengthStringData wsaaT5687Array = new FixedLengthStringData(17000);
		private FixedLengthStringData[] wsaaT5687Rec = FLSArrayPartOfStructure(500, 34, wsaaT5687Array, 0);
		private PackedDecimalData[] wsaaT5687Currfrom = PDArrayPartOfArrayStructure(8, 0, wsaaT5687Rec, 0);
		private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 10, HIVALUES);
		private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0);
		private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(20, wsaaT5687Rec, 14);
		private FixedLengthStringData[] wsaaT5687Annmthd = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 0);
		private FixedLengthStringData[] wsaaT5687Bcmthd = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 4);
		private FixedLengthStringData[] wsaaT5687Bascpy = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 8);
		private FixedLengthStringData[] wsaaT5687Rnwcpy = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 12);
		private FixedLengthStringData[] wsaaT5687Srvcpy = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 16);
	}

	/*
	 * Class transformed from Data Structure WSAA-T6658-ARRAY--INNER
	 */
	private static final class WsaaT6658ArrayInner {

		public WsaaT6658ArrayInner() {
			// constructor
		}

		private FixedLengthStringData wsaaT6658Array = new FixedLengthStringData(1440);
		private FixedLengthStringData[] wsaaT6658Rec = FLSArrayPartOfStructure(30, 48, wsaaT6658Array, 0);
		private PackedDecimalData[] wsaaT6658Currfrom = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Rec, 0);
		private FixedLengthStringData[] wsaaT6658Key = FLSDArrayPartOfArrayStructure(4, wsaaT6658Rec, 10, HIVALUES);
		private FixedLengthStringData[] wsaaT6658Annmthd = FLSDArrayPartOfArrayStructure(4, wsaaT6658Key, 0);
		private FixedLengthStringData[] wsaaT6658Data = FLSDArrayPartOfArrayStructure(34, wsaaT6658Rec, 14);
		private FixedLengthStringData[] wsaaT6658Billfreq = FLSDArrayPartOfArrayStructure(2, wsaaT6658Data, 0);
		private ZonedDecimalData[] wsaaT6658Fixdtrm = ZDArrayPartOfArrayStructure(3, 0, wsaaT6658Data, 2);
		private FixedLengthStringData[] wsaaT6658Manopt = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 5);
		private PackedDecimalData[] wsaaT6658Maxpcnt = PDArrayPartOfArrayStructure(5, 2, wsaaT6658Data, 6);
		private PackedDecimalData[] wsaaT6658Minpcnt = PDArrayPartOfArrayStructure(5, 2, wsaaT6658Data, 9);
		private FixedLengthStringData[] wsaaT6658Optind = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 12);
		private FixedLengthStringData[] wsaaT6658Subprog = FLSDArrayPartOfArrayStructure(10, wsaaT6658Data, 13);
		private FixedLengthStringData[] wsaaT6658Premsubr = FLSDArrayPartOfArrayStructure(8, wsaaT6658Data, 23);
		// ILIFE-6569
		private FixedLengthStringData[] wsaaT6658SimpInd = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 31);
		private FixedLengthStringData[] wsaaT6658IncrFlg = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 32);
		private FixedLengthStringData[] wsaaT6658CompoundInd = FLSDArrayPartOfArrayStructure(1, wsaaT6658Data, 33);
	}
	
	private void updateDates() {
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstcde());
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
	}
	
	protected void write3710(Dry5134Dto dry5134dto)
	{
		//ILPI-61 set report name
		dryrDryrptRecInner.dryrReportName.set("R5135");
		dryrDryrptRecInner.r5135Chdrnum.set(dry5134dto.getChdrnum());
		dryrDryrptRecInner.r5135Life.set(dry5134dto.getLife());
		dryrDryrptRecInner.r5135Coverage.set(dry5134dto.getCoverage());
		dryrDryrptRecInner.r5135Rider.set(dry5134dto.getRider());
		dryrDryrptRecInner.r5135Crtable.set(dry5134dto.getCrtable());
		dryrDryrptRecInner.r5135PlanSuffix.set(dry5134dto.getPlanSuffix());
		dryrDryrptRecInner.r5135Oldsum.set(incrsrec.currsum);
		dryrDryrptRecInner.r5135Newsumi.set(incrsrec.newsum);
		dryrDryrptRecInner.r5135Oldinst.set(incrsrec.currinst01);
		dryrDryrptRecInner.r5135Newinst.set(incrsrec.newinst01);
		datcon1rec.function.set(Varcom.conv);
		datcon1rec.intDate.set(dry5134dto.getCpiDate());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		dryrDryrptRecInner.r5135Rcesdte.set(datcon1rec.extDate);
		dryrDryrptRecInner.r5135Currency.set(dry5134dto.getPremCurrency());
		e000ReportRecords();
		ct07Value++;
	}

	/*
	 * Class transformed from Data Structure DRYP-DRYPRC-REC--INNER
	 */

	private static final class DrypDryprcRecInner {

		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();

		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);

		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		// ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0,
				REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	}
}
