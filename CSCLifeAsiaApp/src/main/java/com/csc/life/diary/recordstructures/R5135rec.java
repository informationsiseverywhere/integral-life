package com.csc.life.diary.recordstructures;

import com.quipoz.framework.datatype.*;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import static com.quipoz.COBOLFramework.COBOLFunctions.*;
import com.quipoz.COBOLFramework.COBOLFunctions;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Wed, 26 Mar 2014 15:27:50
 * Description:
 * Copybook name: R5135REC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class R5135rec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData dataArea = new FixedLengthStringData(499);
  	public FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(dataArea, 0);
  	public FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(dataArea, 7);
  	public FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(dataArea, 9);
  	public FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(dataArea, 11);
  	public FixedLengthStringData crtable = new FixedLengthStringData(4).isAPartOf(dataArea, 13);
  	public ZonedDecimalData planSuffix = new ZonedDecimalData(4, 0).isAPartOf(dataArea, 17);
  	public ZonedDecimalData oldsum = new ZonedDecimalData(13, 2).isAPartOf(dataArea, 21);
  	public ZonedDecimalData newsumi = new ZonedDecimalData(13, 2).isAPartOf(dataArea, 34);
  	public ZonedDecimalData oldinst = new ZonedDecimalData(13, 2).isAPartOf(dataArea, 47);
  	public ZonedDecimalData newinst = new ZonedDecimalData(13, 2).isAPartOf(dataArea, 60);
  	public FixedLengthStringData rcesdte = new FixedLengthStringData(10).isAPartOf(dataArea, 73);
  	public FixedLengthStringData currency = new FixedLengthStringData(3).isAPartOf(dataArea, 83);
  	public FixedLengthStringData filler = new FixedLengthStringData(413).isAPartOf(dataArea, 86, FILLER);
  
  	public FixedLengthStringData sortKey = new FixedLengthStringData(537);
  	public FixedLengthStringData filler1 = new FixedLengthStringData(38).isAPartOf(sortKey, 499, FILLER);
  	public FixedLengthStringData reportName = new FixedLengthStringData(10).init("R5135     ");


	public void initialize() {
		COBOLFunctions.initialize(chdrnum);
		COBOLFunctions.initialize(life);
		COBOLFunctions.initialize(coverage);
		COBOLFunctions.initialize(rider);
		COBOLFunctions.initialize(crtable);
		COBOLFunctions.initialize(planSuffix);
		COBOLFunctions.initialize(oldsum);
		COBOLFunctions.initialize(newsumi);
		COBOLFunctions.initialize(oldinst);
		COBOLFunctions.initialize(newinst);
		COBOLFunctions.initialize(rcesdte);
		COBOLFunctions.initialize(currency);
		COBOLFunctions.initialize(filler);
		COBOLFunctions.initialize(filler1);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		chdrnum.isAPartOf(baseString, true);
    		life.isAPartOf(baseString, true);
    		coverage.isAPartOf(baseString, true);
    		rider.isAPartOf(baseString, true);
    		crtable.isAPartOf(baseString, true);
    		planSuffix.isAPartOf(baseString, true);
    		oldsum.isAPartOf(baseString, true);
    		newsumi.isAPartOf(baseString, true);
    		oldinst.isAPartOf(baseString, true);
    		newinst.isAPartOf(baseString, true);
    		rcesdte.isAPartOf(baseString, true);
    		currency.isAPartOf(baseString, true);
    		filler.isAPartOf(baseString, true);
    		filler1.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}