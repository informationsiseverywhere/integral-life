package com.csc.life.diary.dataaccess.dao;

import java.util.List;
import com.csc.life.diary.dataaccess.model.Dry5351Dto;

/**
 * DAO To Fetch Records For Revenue (Due-Date) Accounting
 * 
 * @author ptrivedi8
 *
 */
public interface Dry5351DAO {

	/**
	 * Get Linspf record details
	 * 
	 * @param effDate
	 *            - Effective date
	 * 
	 * @param company
	 *            - Company
	 * 
	 * @param chdrNum
	 *            - Contract Number
	 * 
	 * @return - List of Dry5351Dto
	 */
	public List<Dry5351Dto> getLinspfRecords(int effDate, String company, String chdrNum);

}
