/*
 * File: Dryr617.java
 * Date: March 26, 2014 3:06:55 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYR617.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.dataaccess.UtrnrnlTableDAM;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6659rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
*   DIARY/400:  Fund Statement Processing
*   -------------------------------------
*
*   This is the Fund Statement Processing Subroutine written for
*   the DIARY/400.
*
*   This subroutine replicates the processing carried out by the
*   Batch Program - BR617.
*
*   100 - This is the main processing section called from MAIND.
*
*         For each invocation of this subroutine:
*
*             Perform 200-INITIALISE
*             Perform 300-PROCESS-CHDR
*             Perform 1000-FINISH
*
*         Exit Subroutine
*
*   200 - Initialises Working Storage Variables and does
*         Component level statii checks.
*
*   300-  Process CHDR section.
*
*         Perform 400-VALIDATION
*
*         If validation is passed
*         Perform 500-PROCESS-STATEMENT
*         Perform 600-REWRITE-CHDRLIF
*
*   400-  Read T5655 using Transaction Code and Product Type
*         Read T5679 using Transaction Code
*         Read T6647 using Transaction Code and Product Type
*         Read T6659 using Unit Statement Method
*         If we get here then validation passed.
*
*   500-  Fill all ANNP-ANNPLL-REC Linkage fields
*         Call the Generic Anniversary Processing Subroutine
*         Add one year to the CHDRLIF statement date use DATCON2
*
*   600-  Rewrite the Contract Header with the new date
*
*   800-  Read UTRNRNL. If record found then Outstanding UTRN's.
*         Read HITRALO. If record found then set flag.
*
*  1000-  No processing currently in here.
*
*  Control Totals
*  --------------
*
*  1 - Total number of CHDRs read
*  2 - Total number of CHDRs updated
*  3 - Total number of Payments outstanding
*  4 - Total number of Fund Transactions Outstanding
*  5 - Number of Statements Produced
*  6 - Number of Invalid Records
*  7 - Number of Statements not ready for processing
*  8 - Process subroutine on T6659 not defined
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryr617 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYR617");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaStmdte = new PackedDecimalData(8, 0).setUnsigned();
	private FixedLengthStringData wsaaOutstandingRecs = new FixedLengthStringData(1);
	private final String wsaaWildcard = "***";

	private FixedLengthStringData wsaaNogo = new FixedLengthStringData(1).init(SPACES);
	private Validator gonofurther = new Validator(wsaaNogo, "Y");
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String utrnrnlrec = "UTRNRNLREC";
	private static final String hitralorec = "HITRALOREC";
	private static final String h965 = "H965";
	private static final String h038 = "H038";
		/* TABLES */
	private static final String t5655 = "T5655";
	private static final String t5679 = "T5679";
	private static final String t6647 = "T6647";
	private static final String t6659 = "T6659";
		/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private static final int ct08 = 7;

	private FixedLengthStringData wsaaValidChdr = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidChdr, "Y");

	private FixedLengthStringData wsaaT5655Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT5655Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 0);
	private FixedLengthStringData wsaaT5655Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5655Key, 4);

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(7);
	private FixedLengthStringData wsaaT6647Batctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private UtrnrnlTableDAM utrnrnlIO = new UtrnrnlTableDAM();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Annprocrec annprocrec = new Annprocrec();
	private T5655rec t5655rec = new T5655rec();
	private T5679rec t5679rec = new T5679rec();
	private T6647rec t6647rec = new T6647rec();
	private T6659rec t6659rec = new T6659rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		tryAgain420, 
		check430, 
		exit490
	}

	public Dryr617() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}


protected void startProcessing100()
	{
		/*START*/
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(drypDryprcRecInner.drypEffectiveDate);
		initialise200();
		processChdr300();
		finish1000();
		/*EXIT*/
	}

protected void initialise200()
	{
		initialise210();
	}

protected void initialise210()
	{
		/* Initialise any Working Storage variables.*/
		wsaaSub.set(ZERO);
		wsaaStmdte.set(varcom.vrcmMaxDate);
		wsaaOutstandingRecs.set(SPACES);
		wsaaT5655Key.set(SPACES);
		wsaaT6647Key.set(SPACES);
		wsaaNogo.set(SPACES);
		/* Read T5679 for valid statii.*/
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void processChdr300()
	{
		process310();
	}

protected void process310()
	{
		chdrlifIO.setDataArea(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)
		&& isNE(chdrlifIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		validation400();
		if (gonofurther.isTrue()) {
			return ;
		}
		processStatement500();
		rewriteChdrlif600();
	}

protected void validation400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					validation410();
				case tryAgain420: 
					tryAgain420();
				case check430: 
					check430();
					t6659440();
				case exit490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void validation410()
	{
		/* Get the lead days on T5655 and verify whether the statement*/
		/* is due to process. i.e. the statment due date is earlier*/
		/* than the run date plus the number of lead days from T5655.*/
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5655);
		wsaaT5655Batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		wsaaT5655Cnttype.set(drypDryprcRecInner.drypCnttype);
		itdmIO.setItemitem(wsaaT5655Key);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			goTo(GotoLabel.exit490);
		}
		/* Check Key, if not found then search for wildcard*/
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5655)
		|| isNE(wsaaT5655Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setParams(SPACES);
			itdmIO.setStatuz(varcom.oK);
			itdmIO.setItempfx(smtpfxcpy.item);
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(t5655);
			wsaaT5655Batctrcde.set(drypDryprcRecInner.drypBatctrcde);
			wsaaT5655Cnttype.set(wsaaWildcard);
			itdmIO.setItemitem(wsaaT5655Key);
			itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
			itdmIO.setFormat(itemrec);
			itdmIO.setFunction(varcom.begn);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				drylogrec.statuz.set(itdmIO.getStatuz());
				drylogrec.params.set(itdmIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
				goTo(GotoLabel.exit490);
			}
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5655)
		|| isNE(wsaaT5655Key, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(h038);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			goTo(GotoLabel.exit490);
		}
		t5655rec.t5655Rec.set(itdmIO.getGenarea());
		compute(t5655rec.leadDays, 0).set(mult(t5655rec.leadDays, -1));
		datcon2rec.freqFactor.set(t5655rec.leadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(chdrlifIO.getStatementDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			goTo(GotoLabel.exit490);
		}
		wsaaStmdte.set(datcon2rec.intDate2);
		if (isGT(wsaaStmdte, drypDryprcRecInner.drypRunDate)) {
			drypDryprcRecInner.processUnsuccesful.setTrue();
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(drypDryprcRecInner.drypEntity);
			stringVariable1.addExpression(" ERROR - STATMENT DATE > DIARY RUN DATE.");
			stringVariable1.setStringInto(drylogrec.params);
			b000LogMessage();
			drycntrec.contotNumber.set(ct07);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			wsaaNogo.set("Y");
			goTo(GotoLabel.exit490);
		}
		/* Read the contract header and validate the status against those*/
		/* on T5679. If the status is invalid, add 1 to CT06, which is a*/
		/* log of the Contract Header Records which have invalid statii*/
		/* and not allow to proceed fro unit statment.*/
		validateChdr700();
		/* Log CHDRs with invalid statii.*/
		if (isEQ(wsaaValidChdr, "N")) {
			drypDryprcRecInner.processUnsuccesful.setTrue();
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(drypDryprcRecInner.drypEntity);
			stringVariable2.addExpression(" ERROR - INVALID CHDR STATUS.");
			stringVariable2.setStringInto(drylogrec.params);
			b000LogMessage();
			drycntrec.contotNumber.set(ct06);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			wsaaNogo.set("Y");
			goTo(GotoLabel.exit490);
		}
		/* Read T6647 to get the corresponding statment method as per*/
		/* particular transaction per contract type.*/
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6647);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		wsaaT6647Batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		wsaaT6647Cnttype.set(chdrlifIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			goTo(GotoLabel.exit490);
		}
		if (isEQ(itdmIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.tryAgain420);
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t6647)
		|| isNE(itdmIO.getItemitem(), wsaaT6647Key)) {
			goTo(GotoLabel.tryAgain420);
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		goTo(GotoLabel.check430);
	}

	/**
	* <pre>
	* Check Key, if not found then search for wildcard
	* </pre>
	*/
protected void tryAgain420()
	{
		itdmIO.setDataKey(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6647);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		wsaaT6647Batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		wsaaT6647Cnttype.set(wsaaWildcard);
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.endp)
		&& isNE(itdmIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			goTo(GotoLabel.exit490);
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t6647)
		|| isNE(itdmIO.getItemitem(), wsaaT6647Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			goTo(GotoLabel.exit490);
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
	}

protected void check430()
	{
		if (isEQ(t6647rec.unitStatMethod, SPACES)) {
			drylogrec.statuz.set(h965);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.drySystemError.setTrue();
			a000FatalError();
			goTo(GotoLabel.exit490);
		}
	}

protected void t6659440()
	{
		/* Read T6659 to get the frequency by using the unit statement*/
		/* method from T6647 as the key.*/
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6659);
		itdmIO.setItemitem(t6647rec.unitStatMethod);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			return ;
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t6659)
		|| isNE(itdmIO.getItemitem(), t6647rec.unitStatMethod)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(t6659);
			itdmIO.setItemitem(t6647rec.unitStatMethod);
			itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
			return ;
		}
		else {
			t6659rec.t6659Rec.set(itdmIO.getGenarea());
		}
		if (isEQ(t6659rec.subprog, SPACES)) {
			drypDryprcRecInner.processUnsuccesful.setTrue();
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(drypDryprcRecInner.drypEntity);
			stringVariable1.addExpression(" ERROR - MISSING SUBROUTINE ENTRY IN T6659 ITEM.");
			stringVariable1.setStringInto(drylogrec.params);
			b000LogMessage();
			drycntrec.contotNumber.set(ct08);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			wsaaNogo.set("Y");
			return ;
		}
		/* Check for scenario of PTD less than Statement Date.*/
		if (isEQ(t6659rec.annOrPayInd, "P")
		&& isLT(chdrlifIO.getPtdate(), chdrlifIO.getStatementDate())) {
			drypDryprcRecInner.processUnsuccesful.setTrue();
			StringUtil stringVariable2 = new StringUtil();
			stringVariable2.addExpression(drypDryprcRecInner.drypEntity);
			stringVariable2.addExpression(" ERROR - PTD EARLIER THAN STMT DATE.");
			stringVariable2.setStringInto(drylogrec.params);
			b000LogMessage();
			drycntrec.contotNumber.set(ct03);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			wsaaNogo.set("Y");
			return ;
		}
		/* Check for any outstanding deals.*/
		if (isEQ(t6659rec.osUtrnInd, "N")) {
			outstandingUnitTrans800();
			if (isEQ(wsaaOutstandingRecs, "Y")) {
				drypDryprcRecInner.processUnsuccesful.setTrue();
				StringUtil stringVariable3 = new StringUtil();
				stringVariable3.addExpression(drypDryprcRecInner.drypEntity);
				stringVariable3.addExpression(" ERROR - UNDEALT UTRNS/HITRS.");
				stringVariable3.setStringInto(drylogrec.params);
				b000LogMessage();
				drycntrec.contotNumber.set(ct04);
				drycntrec.contotValue.set(1);
				d000ControlTotals();
				wsaaNogo.set("Y");
				return ;
			}
		}
	}

protected void processStatement500()
	{
		statement510();
	}

protected void statement510()
	{
		/* If we get to here then all validation has been passed.*/
		/* Process the statement and calculate the CHDRLIF-STATEMENT-DATE*/
		/* as CHDRLIF-STATEMENT-DATE incremented by the frequency stored*/
		/* on the T6659 item.*/
		drycntrec.contotNumber.set(ct05);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		annprocrec.annpllRec.set(SPACES);
		annprocrec.company.set(drypDryprcRecInner.drypCompany);
		annprocrec.chdrnum.set(chdrlifIO.getChdrnum());
		annprocrec.life.set(SPACES);
		annprocrec.coverage.set(SPACES);
		annprocrec.rider.set(SPACES);
		annprocrec.planSuffix.set(0);
		annprocrec.effdate.set(drypDryprcRecInner.drypRunDate);
		annprocrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		annprocrec.crdate.set(0);
		annprocrec.crtable.set(SPACES);
		annprocrec.user.set(999999);
		annprocrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		annprocrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		annprocrec.batccoy.set(drypDryprcRecInner.drypBatccoy);
		annprocrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		annprocrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		annprocrec.batcpfx.set(drypDryprcRecInner.drypBatcpfx);
		callProgram(t6659rec.subprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, varcom.oK)) {
			drylogrec.statuz.set(annprocrec.statuz);
			drylogrec.params.set(annprocrec.annpllRec);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Read and hold the record in the Buffer before re-writing it.*/
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		datcon2rec.datcon2Rec.set(SPACES);
		datcon2rec.frequency.set(t6659rec.freq);
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(chdrlifIO.getStatementDate());
		datcon2rec.intDate2.set(0);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		chdrlifIO.setStatementDate(datcon2rec.intDate2);
	}

protected void rewriteChdrlif600()
	{
		rewrite610();
	}

protected void rewrite610()
	{
		/* Rewrite CHDRLIF record.*/
		chdrlifIO.setFunction(varcom.rewrt);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

protected void validateChdr700()
	{
		/*VALIDATE*/
		/* Validate the Contract status against T5679.*/
		wsaaValidChdr.set("N");
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
		|| validContract.isTrue()); wsaaSub.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()], chdrlifIO.getStatcode())) {
				for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
				|| validContract.isTrue()); wsaaSub.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaSub.toInt()], chdrlifIO.getPstatcode())) {
						wsaaValidChdr.set("Y");
					}
				}
			}
		}
		/*EXIT*/
	}

protected void outstandingUnitTrans800()
	{
		outstanding810();
	}

protected void outstanding810()
	{
		/*  If the outstanding UTRN indicator stored in the T6659 w/s*/
		/*  array is not set to 'Y' we need to read the unit transacion*/
		/*  file (UTRNPF) with the logical view UTRNRNL and a key of*/
		/*  company/contract. If a record is found (view only selects*/
		/*  outstanding records), set a flag and do not process this*/
		/*  contract- add one to CT04 and process the next record.*/
		wsaaOutstandingRecs.set("N");
		utrnrnlIO.setParams(SPACES);
		utrnrnlIO.setChdrcoy(chdrlifIO.getChdrcoy());
		utrnrnlIO.setChdrnum(chdrlifIO.getChdrnum());
		utrnrnlIO.setFeedbackInd(SPACES); //ILIFE-8113
		utrnrnlIO.setFormat(utrnrnlrec);
		utrnrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, utrnrnlIO);
		if (isNE(utrnrnlIO.getStatuz(), varcom.oK)
		&& isNE(utrnrnlIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(utrnrnlIO.getStatuz());
			drylogrec.params.set(utrnrnlIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* If no unprocessed UTRNS were found, check to see if there are*/
		/* any unprocessed HITRS.*/
		if (isEQ(utrnrnlIO.getStatuz(), varcom.oK)) {
			wsaaOutstandingRecs.set("Y");
			return ;
		}
		hitraloIO.setRecKeyData(SPACES);
		hitraloIO.setRecNonKeyData(SPACES);
		hitraloIO.setChdrnum(chdrlifIO.getChdrnum());
		hitraloIO.setChdrcoy(chdrlifIO.getChdrcoy());
		hitraloIO.setFormat(hitralorec);
		hitraloIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitraloIO.getStatuz(), varcom.oK)
		&& isNE(hitraloIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(hitraloIO.getStatuz());
			drylogrec.params.set(hitraloIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isNE(hitraloIO.getChdrnum(), chdrlifIO.getChdrnum())
		|| isNE(hitraloIO.getChdrcoy(), chdrlifIO.getChdrcoy())) {
			hitraloIO.setStatuz(varcom.endp);
		}
		if (isEQ(hitraloIO.getStatuz(), varcom.oK)) {
			wsaaOutstandingRecs.set("Y");
		}
	}

protected void finish1000()
	{
		/* Move in the DRYP- fields required to calculate the following*/
		/* fund statement date.*/
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		/*EXIT*/
	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
	private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
}
}
