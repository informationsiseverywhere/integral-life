/*
 * File: Overbildry.java
 * Date: December 3, 2013 2:34:32 AM ICT
 * Author: CSC
 * 
 * Class transformed from OVERBILDRY.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.diary.procedures.Dryreport;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.tablestructures.T3699rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.contractservicing.dataaccess.PtrnrevTableDAM;
import com.csc.life.contractservicing.recordstructures.Reverserec;
import com.csc.life.contractservicing.tablestructures.T6661rec;
import com.csc.life.diary.recordstructures.Ovbldryrec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.tablestructures.T6654rec;
import com.csc.life.regularprocessing.dataaccess.LinsovrTableDAM;
import com.csc.life.regularprocessing.tablestructures.T6626rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ErorTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Dryprcrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.quipoz.COBOLFramework.util.StringUtil;

/**
* <pre>
*REMARKS.
*
*
*        OVERDUE PROCESSING - BTDATE CORRECTION PROCESSING
*        -------------------------------------------------
*
* Overview
* ~~~~~~~~
*    This subroutine is called from Overdue processing. When a
*     contract is about to go Overdue and the Billed-to and
*     Paid-to dates don't match, then this subroutine is called
*     to Reverse all transactions that have occurred since the
*     last Paid-to date.
*     The theory is to reverse all PTRNs back to the last
*     'Collection-type' PTRN, ie. Collection or Paid-to-date
*     advance, so that the Billed-to and Paid-to dates will match
*     at the time the Contract is put through Non-forfeiture
*     processing.
*     The revsesal process will stop IF
*       a) A Collection-type PTRN is found (specified on T6626)
*       b) The Issue PTRN is reached
*       c) a 'Show-stopper' type PTRN is found. eg Full Surrender
*
*     This subroutine basically works in the same way as the
*      Full Contract Reversal AT module, REVGENAT.
*
* Initial Processing
* ~~~~~~~~~~~~~~~~~~
* Firstly read contract  header  record  using the logical view
* CHDRLIF. The details of minor alteration is then saved to
* working storage.
*
* The Payer Details record is then read and similarly saved to
* working storage.
*
* Get the PTRN records by reading the PTRNREV logical view.
* (PTRNREV logical view is used because it omits all valid
*  flag '2' records).
*
* For each PTRN record found - carry out the following
* processing :
* (Once  all  the  records  have  been processed go to update
*  contract header.)
*
*
* Generic Reversal processing
* ~~~~~~~~~~~~~~~~~~~~~~~~~~~
* All  updating will be carried out by generic subroutines as
* set up  in table T6661. The key into this table is transaction
* code which we get from the relevant PTRNREV record.
* (If no entry  is  found on the table get the next record)
*
* The  copybook  to  be  set  up  for the calls will be REVERSEREC 
* the fields are to be set up as follows:
*
*     Component key with just the contract number and
*     company.
*     Effective date 1 as reverse to date.
*     TRANNO from the PTRN record
*     New TRANNO = Contract Header TRANNO + 1
*
* A detail line also has to be produced for the report using the
* following details:
*
* </pre>
*/
public class Overbildry extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("OVERBILDRY");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private String wsaaCollection = "";
	private String wsaaIssue = "";
	private FixedLengthStringData wsaaStop = new FixedLengthStringData(1);
	private String wsaaBillTran = "N";
	private FixedLengthStringData wsaaNoLinsFound = new FixedLengthStringData(1);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private FixedLengthStringData wsaaT6654Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6654Billchnl = new FixedLengthStringData(1).isAPartOf(wsaaT6654Item, 0);
	private FixedLengthStringData wsaaT6654Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6654Item, 1);
	private FixedLengthStringData wsaaChdrAplsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrBillsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrCommsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrNotssupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrRnwlsupr = new FixedLengthStringData(1);
	private PackedDecimalData wsaaChdrAplspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrBillspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrCommspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrNotsspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrRnwlspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrAplspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrBillspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrCommspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrNotsspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaChdrRnwlspto = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaChdrCownpfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrCowncoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrCownnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrJownnum = new FixedLengthStringData(8);
	private PackedDecimalData wsaaChdrPolsum = new PackedDecimalData(4, 0);
	private FixedLengthStringData wsaaChdrDesppfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrDespcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrDespnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaChdrAgntpfx = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaChdrAgntcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaChdrAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaPayrAplsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayrBillsupr = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaPayrNotssupr = new FixedLengthStringData(1);
	private PackedDecimalData wsaaPayrAplspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrBillspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrNotsspfrom = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrAplspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrBillspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrNotsspto = new PackedDecimalData(8, 0);
	private PackedDecimalData wsaaPayrBillcd = new PackedDecimalData(8, 0);
	private FixedLengthStringData wsaaDtaintFlag = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaDtaintError = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaIncchkFlag = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaIncchkError = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaEroreror = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsaaThreeYears = new FixedLengthStringData(1116);
	private FixedLengthStringData wsaa3Year = new FixedLengthStringData(1116).isAPartOf(wsaaThreeYears, 0);
	private FixedLengthStringData wsaa3YearR = new FixedLengthStringData(1116).isAPartOf(wsaa3Year, 0, REDEFINE);
	private FixedLengthStringData[] wsaa3YearR1 = FLSArrayPartOfStructure(36, 31, wsaa3YearR, 0);
	private FixedLengthStringData[][] wsaa3Dd = FLSDArrayPartOfArrayStructure(31, 1, wsaa3YearR1, 0);

	private FixedLengthStringData wsaaThreeYearsR = new FixedLengthStringData(1116).isAPartOf(wsaaThreeYears, 0, REDEFINE);
	private FixedLengthStringData wsaaLastYy = new FixedLengthStringData(372).isAPartOf(wsaaThreeYearsR, 0);
	private FixedLengthStringData[] wsaaLastMm = FLSArrayPartOfStructure(12, 31, wsaaLastYy, 0);
	private FixedLengthStringData wsaaThisYy = new FixedLengthStringData(372).isAPartOf(wsaaThreeYearsR, 372);
	private FixedLengthStringData[] wsaaThisMm = FLSArrayPartOfStructure(12, 31, wsaaThisYy, 0);
	private FixedLengthStringData wsaaNextYy = new FixedLengthStringData(372).isAPartOf(wsaaThreeYearsR, 744);
	private FixedLengthStringData[] wsaaNextMm = FLSArrayPartOfStructure(12, 31, wsaaNextYy, 0);
	private ZonedDecimalData wsaaLastYear = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaThisYear = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaNextYear = new ZonedDecimalData(4, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData lastStart = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private static final int thisStart = 12;
	private static final int nextStart = 24;
	private ZonedDecimalData wsaaCountMm = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCountDd = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();
	private ZonedDecimalData wsaaCnt = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaValyd = new FixedLengthStringData(1);
	private Validator valyd = new Validator(wsaaValyd, ".","W","E","D","X","H");
	private ZonedDecimalData wsaaAddToDate = new ZonedDecimalData(8, 0).setUnsigned();

	private FixedLengthStringData filler1 = new FixedLengthStringData(8).isAPartOf(wsaaAddToDate, 0, FILLER_REDEFINE);
	private FixedLengthStringData wsaaYyyy = new FixedLengthStringData(4).isAPartOf(filler1, 0);
	private ZonedDecimalData wsaaYear = new ZonedDecimalData(4, 0).isAPartOf(wsaaYyyy, 0).setUnsigned();
	private ZonedDecimalData wsaaMm = new ZonedDecimalData(2, 0).isAPartOf(filler1, 4).setUnsigned();
	private ZonedDecimalData wsaaDd = new ZonedDecimalData(2, 0).isAPartOf(filler1, 6).setUnsigned();
		/* TABLES */
	private static final String t1688 = "T1688";
	private static final String t6626 = "T6626";
	private static final String t6654 = "T6654";
	private static final String t6661 = "T6661";
	private static final String t3699 = "T3699";
		/* ERRORS */
	private static final String e587 = "E587";
	private static final String e511 = "E511";
	private static final String t084 = "T084";
	private static final String h842 = "H842";
		/* FORMATS */
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String linsovrrec = "LINSOVRREC";
	private static final String payrrec = "PAYRREC";
	private static final String ptrnrevrec = "PTRNREVREC";
	private static final String erorrec = "ERORREC";
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private ErorTableDAM erorIO = new ErorTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private LinsovrTableDAM linsovrIO = new LinsovrTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnrevTableDAM ptrnrevIO = new PtrnrevTableDAM();
	private T6626rec t6626rec = new T6626rec();
	private T6654rec t6654rec = new T6654rec();
	private T6661rec t6661rec = new T6661rec();
	private T3699rec t3699rec = new T3699rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Reverserec reverserec = new Reverserec();
	private Varcom varcom = new Varcom();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Dryprcrec dryprcrec = new Dryprcrec();
	private Ovbldryrec ovbldryrec = new Ovbldryrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private boolean BTPRO028Permission = false;
	private static final String BTPRO028 = "BTPRO028";
	private ItemDAO itempfDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	protected Itempf itempf = null;
/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit2090, 
		continue3340, 
		startCheck3420, 
		setDate3480
	}

	public Overbildry() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		ovbldryrec.overbillRec = convertAndSetParam(ovbldryrec.overbillRec, parmArray, 1);
		dryprcrec.dryprcRec = convertAndSetParam(dryprcrec.dryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(dryprcrec.dryprcRec);
			mainLine100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine100()
	{
		/*START*/
		initialise1000();
		while ( !(isEQ(ptrnrevIO.getStatuz(), varcom.endp))) {
			processPtrn2000();
		}
		
		processChdrPayr3000();
		if (isNE(ovbldryrec.btdate, ovbldryrec.ptdate)) {
			dryrDryrptRecInner.r5062Chdrnum.set(dryprcrec.entity);
			dryrDryrptRecInner.r5062ErrorLine.set("Y");
			dryrDryrptRecInner.dryrSortkey.set(dryrDryrptRecInner.r5062SortKey);
			e000ReportRecords();
		}
		/* Reverse any Statistics processing that may have happened*/
		statistics5000();
		/*EXIT*/
		exitProgram();
	}

protected void initialise1000()
	{
		/*START*/
	    BTPRO028Permission  = FeaConfg.isFeatureExist("2", BTPRO028, appVars, "IT");
		ovbldryrec.statuz.set(varcom.oK);
		ptrnrevIO.setParams(SPACES);
		wsaaNoLinsFound.set(SPACES);
		wsaaStop.set(SPACES);
		readChdr1100();
		readPayr1200();
		readPtrn1300();
		readT66261600();
		getLins1700();
		/*EXIT*/
	}

protected void readChdr1100()
	{
		start1110();
	}

protected void start1110()
	{
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(dryprcrec.company);
		chdrlifIO.setChdrnum(dryprcrec.entity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		saveChdrMinorAlt1400();
	}

protected void readPayr1200()
	{
		start1200();
	}

protected void start1200()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFunction(varcom.readr);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		savePayrMinorAlt1500();
		wsaaPayrBillcd.set(payrIO.getBillcd());
	}

protected void readPtrn1300()
	{
		start1310();
	}

protected void start1310()
	{
		ptrnrevIO.setDataArea(SPACES);
		ptrnrevIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnrevIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnrevIO.setTranno(99999);
		ptrnrevIO.setFunction(varcom.begn);
		ptrnrevIO.setFormat(ptrnrevrec);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(ptrnrevIO.getParams());
			drylogrec.statuz.set(ptrnrevIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isNE(chdrlifIO.getChdrnum(), ptrnrevIO.getChdrnum())
		|| isNE(chdrlifIO.getChdrcoy(), ptrnrevIO.getChdrcoy())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
	}

protected void saveChdrMinorAlt1400()
	{
		start1410();
	}

protected void start1410()
	{
		wsaaChdrAplsupr.set(chdrlifIO.getAplsupr());
		wsaaChdrBillsupr.set(chdrlifIO.getBillsupr());
		wsaaChdrCommsupr.set(chdrlifIO.getCommsupr());
		wsaaChdrNotssupr.set(chdrlifIO.getNotssupr());
		wsaaChdrRnwlsupr.set(chdrlifIO.getRnwlsupr());
		wsaaChdrAplspfrom.set(chdrlifIO.getAplspfrom());
		wsaaChdrBillspfrom.set(chdrlifIO.getBillspfrom());
		wsaaChdrCommspfrom.set(chdrlifIO.getCommspfrom());
		wsaaChdrNotsspfrom.set(chdrlifIO.getNotsspfrom());
		wsaaChdrRnwlspfrom.set(chdrlifIO.getRnwlspfrom());
		wsaaChdrAplspto.set(chdrlifIO.getAplspto());
		wsaaChdrBillspto.set(chdrlifIO.getBillspto());
		wsaaChdrCommspto.set(chdrlifIO.getCommspto());
		wsaaChdrNotsspto.set(chdrlifIO.getNotsspto());
		wsaaChdrRnwlspto.set(chdrlifIO.getRnwlspto());
		wsaaChdrCownpfx.set(chdrlifIO.getCownpfx());
		wsaaChdrCowncoy.set(chdrlifIO.getCowncoy());
		wsaaChdrCownnum.set(chdrlifIO.getCownnum());
		wsaaChdrJownnum.set(chdrlifIO.getJownnum());
		wsaaChdrPolsum.set(chdrlifIO.getPolsum());
		wsaaChdrDesppfx.set(chdrlifIO.getDesppfx());
		wsaaChdrDespcoy.set(chdrlifIO.getDespcoy());
		wsaaChdrDespnum.set(chdrlifIO.getDespnum());
		wsaaChdrAgntpfx.set(chdrlifIO.getAgntpfx());
		wsaaChdrAgntcoy.set(chdrlifIO.getAgntcoy());
		wsaaChdrAgntnum.set(chdrlifIO.getAgntnum());
	}

protected void savePayrMinorAlt1500()
	{
		/*START*/
		wsaaPayrAplsupr.set(payrIO.getAplsupr());
		wsaaPayrBillsupr.set(payrIO.getBillsupr());
		wsaaPayrNotssupr.set(payrIO.getNotssupr());
		wsaaPayrAplspfrom.set(payrIO.getAplspfrom());
		wsaaPayrBillspfrom.set(payrIO.getBillspfrom());
		wsaaPayrNotsspfrom.set(payrIO.getNotsspfrom());
		wsaaPayrAplspto.set(payrIO.getAplspto());
		wsaaPayrBillspto.set(payrIO.getBillspto());
		wsaaPayrNotsspto.set(payrIO.getNotsspto());
		/*EXIT*/
	}

protected void readT66261600()
	{
		start1610();
	}

protected void start1610()
	{
		/* Load in the list of 'Collection-type' Transaction codes and*/
		/*  the list of 'Issue' transaction codes.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dryprcrec.company);
		itemIO.setItemtabl(t6626);
		itemIO.setItemitem(dryprcrec.batctrcde);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t6626rec.t6626Rec.set(itemIO.getGenarea());
	}

protected void getLins1700()
	{
		start1710();
	}

protected void start1710()
	{
		/* Use the PTDATE on the PAYR to enable us to match on the last*/
		/* 'Paid' LINS record.*/
		linsovrIO.setParams(SPACES);
		linsovrIO.setChdrcoy(dryprcrec.company);
		linsovrIO.setChdrnum(dryprcrec.entity);
		linsovrIO.setInstto(99999999);
		linsovrIO.setFunction(varcom.begn);
		linsovrIO.setFormat(linsovrrec);
		SmartFileCode.execute(appVars, linsovrIO);
		if (isNE(linsovrIO.getStatuz(), varcom.oK)
		&& isNE(linsovrIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(linsovrIO.getParams());
			drylogrec.statuz.set(linsovrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isNE(dryprcrec.company, linsovrIO.getChdrcoy())
		|| isNE(dryprcrec.entity, linsovrIO.getChdrnum())
		|| isEQ(linsovrIO.getStatuz(), varcom.endp)) {
			wsaaNoLinsFound.set("Y");
			return ;
		}
		/* The most recent LINS should have the INSTTO date equal*/
		/*   to the PTDATE on the PAYR - if not, then problems !!*/
		if (isNE(linsovrIO.getInstto(), payrIO.getPtdate())) {
			drylogrec.params.set(linsovrIO.getParams());
			drylogrec.statuz.set(t084);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61 
		}
	}

protected void processPtrn2000()
	{
		try {
			start2000();
			readNext2020();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void start2000()
	{
		/* Check PTRN details before processing*/
		wsaaBillTran = "N";
		validateBatctrcde2200();
		if (isEQ(wsaaIssue, "Y")
		|| isEQ(wsaaCollection, "Y")) {
			wsaaStop.set("Y");
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		callGenericProcessing2100();
		if (isEQ(wsaaStop, "Y")) {
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isNE(t6661rec.contRevFlag, "N")) {
			printDetailLine2300();
			rewritePtrn2500();
		}
		if (isEQ(wsaaBillTran, "Y")
		&& isLTE(ptrnrevIO.getPtrneff(), payrIO.getPtdate())) {
			ptrnrevIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit2090);
		}
		if (isEQ(wsaaDtaintError, "Y")) {
			wsaaDtaintFlag.set("Y");
			printDetailLine2300();
			wsaaDtaintFlag.set("N");
			wsaaDtaintError.set("N");
		}
		if (isEQ(wsaaIncchkError, "Y")) {
			wsaaIncchkFlag.set("Y");
			printDetailLine2300();
			wsaaIncchkFlag.set("N");
			wsaaIncchkError.set("N");
		}
	}

protected void readNext2020()
	{
		ptrnrevIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)
		&& isNE(ptrnrevIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(ptrnrevIO.getParams());
			drylogrec.statuz.set(ptrnrevIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isNE(ptrnrevIO.getChdrcoy(), chdrlifIO.getChdrcoy())
		|| isNE(ptrnrevIO.getChdrnum(), chdrlifIO.getChdrnum())) {
			ptrnrevIO.setStatuz(varcom.endp);
		}
	}

protected void callGenericProcessing2100()
	{
		start2110();
	}

protected void start2110()
	{
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dryprcrec.company);
		itemIO.setItemtabl(t6661);
		itemIO.setItemitem(ptrnrevIO.getBatctrcde());
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t6661rec.t6661Rec.set(itemIO.getGenarea());
		/* Check for a minor-alt type transaction that doesn't need*/
		/*   reversing*/
		if (isEQ(t6661rec.contRevFlag, "N")) {
			return ;
		}
		/* Check if the PTRN we are reversing is a 'Show-stopper'*/
		/*  ie. It is not used by Full Contract Reversal, it has its own*/
		/*       reversal processing*/
		if (isEQ(t6661rec.contRevFlag, SPACES)) {
			wsaaStop.set("Y");
			return ;
		}
		reverserec.chdrnum.set(chdrlifIO.getChdrnum());
		reverserec.company.set(chdrlifIO.getChdrcoy());
		reverserec.tranno.set(ptrnrevIO.getTranno());
		reverserec.ptrneff.set(ptrnrevIO.getPtrneff());
		reverserec.oldBatctrcde.set(ptrnrevIO.getBatctrcde());
		reverserec.language.set(dryprcrec.language);
		reverserec.newTranno.set(ovbldryrec.newTranno);
		reverserec.effdate1.set(ptrnrevIO.getPtrneff());
		reverserec.effdate2.set(ZERO);
		reverserec.batchkey.set(dryprcrec.batchKey);
		reverserec.transDate.set(ovbldryrec.tranDate);
		reverserec.transTime.set(ovbldryrec.tranTime);
		reverserec.user.set(ovbldryrec.user);
		reverserec.termid.set(ovbldryrec.termid);
		reverserec.planSuffix.set(ZERO);
		reverserec.ptrnBatcpfx.set(ptrnrevIO.getBatcpfx());
		reverserec.ptrnBatccoy.set(ptrnrevIO.getBatccoy());
		reverserec.ptrnBatcbrn.set(ptrnrevIO.getBatcbrn());
		reverserec.ptrnBatcactyr.set(ptrnrevIO.getBatcactyr());
		reverserec.ptrnBatcactmn.set(ptrnrevIO.getBatcactmn());
		reverserec.ptrnBatctrcde.set(ptrnrevIO.getBatctrcde());
		reverserec.ptrnBatcbatch.set(ptrnrevIO.getBatcbatch());
		reverserec.statuz.set(varcom.oK);
		wsaaIncchkFlag.set("N");
		wsaaDtaintFlag.set("N");
		wsaaIncchkError.set("N");
		wsaaDtaintError.set("N");
		if (isNE(t6661rec.subprog01, SPACES)) {
			callProgram(t6661rec.subprog01, reverserec.reverseRec);
			if (isNE(reverserec.statuz, varcom.oK)
			&& isNE(reverserec.statuz, "ITCK")) {
				drylogrec.params.set(reverserec.reverseRec);
				drylogrec.statuz.set(reverserec.statuz);
				drylogrec.drySystemError.setTrue();
				a000FatalError();//ILPI-61 
			}
		}
		if (isEQ(reverserec.statuz, "ITCK")) {
			wsaaIncchkError.set("Y");
		}
		reverserec.statuz.set(varcom.oK);
		if (isNE(t6661rec.subprog02, SPACES)) {
			callProgram(t6661rec.subprog02, reverserec.reverseRec);
			if (isNE(reverserec.statuz, varcom.oK)
			&& isNE(reverserec.statuz, "ITCK")) {
				drylogrec.params.set(reverserec.reverseRec);
				drylogrec.statuz.set(reverserec.statuz);
				drylogrec.drySystemError.setTrue();
				a000FatalError();//ILPI-61 
			}
		}
		if (isEQ(reverserec.statuz, "ITCK")) {
			wsaaIncchkError.set("Y");
		}
		reverserec.statuz.set(varcom.oK);
		/*    Life/Asia does not retrofit D9811 (EURO), so skip this part. */
		/*     IF  T6661-SUBPROG-03    NOT = SPACES                        */
		/*         IF  OVBL-TRANSCD    NOT = SPACES                        */
		/*             MOVE OVBL-TRANSCD  TO REVE-BATCTRCDE                */
		/*         END-IF                                                  */
		/*         CALL T6661-SUBPROG-03                                   */
		/*                             USING REVE-REVERSE-REC              */
		/*         IF  REVE-STATUZ         = BOMB                          */
		/*             MOVE REVE-REVERSE-REC TO DRYL-PARAMS                */
		/*             MOVE REVE-STATUZ      TO DRYL-STATUZ                */
		/*             SET DRY-DATABASE-ERROR TO TRUE                      */
		/*             PERFORM A000-FATAL-ERROR                            */
		/*         END-IF                                                  */
		/*     END-IF.                                                     */
		if (isNE(reverserec.statuz, varcom.oK)) {
			wsaaDtaintError.set("Y");
		}
	}

protected void validateBatctrcde2200()
	{
		start2210();
	}

protected void start2210()
	{
		wsaaCollection = "N";
		wsaaIssue = "N";
		if (isEQ(ptrnrevIO.getBatctrcde(), t6626rec.trcode)) {
			wsaaBillTran = "Y";
		}
		if (isEQ(wsaaNoLinsFound, "Y")) {
			/* We want to reverse back to but NOT including the Issue*/
			/* transaction - when we reach 'Issue', then stop.*/
			/* Just check that the PTRN we are about to reverse is*/
			/* not 'Issue' or 'Fast-track'.*/
			for (wsaaSub.set(6); !(isGT(wsaaSub, 10)); wsaaSub.add(1)){
				if (isEQ(ptrnrevIO.getBatctrcde(), t6626rec.transcd[wsaaSub.toInt()])) {
					wsaaIssue = "Y";
					wsaaSub.set(11);
				}
			}
			return ;
		}
		/*  If we get to here, then we need to see whether the transaction*/
		/*    we are going to reverse is a 'Collection' type or not.*/
		/*  If the PTRN was written by a 'Collection' type transaction,*/
		/*    then we want to stop.*/
		for (wsaaSub.set(1); !(isGT(wsaaSub, 5)); wsaaSub.add(1)){
			if (isEQ(ptrnrevIO.getBatctrcde(), t6626rec.transcd[wsaaSub.toInt()])) {
				wsaaCollection = "Y";
				wsaaSub.set(6);
			}
		}
	}

protected void printDetailLine2300()
	{
		start2310();
	}

protected void start2310()
	{
		dryrDryrptRecInner.r5062Chdrnum.set(chdrlifIO.getChdrnum());
		/* Get the Transaction Description from T1688*/
		dryrDryrptRecInner.r5062Batctrcde.set(ptrnrevIO.getBatctrcde());
		descIO.setParams(SPACES);
		descIO.setDescitem(ptrnrevIO.getBatctrcde());
		descIO.setDescpfx("IT");
		descIO.setDesccoy(ptrnrevIO.getChdrcoy());
		descIO.setDesctabl(t1688);
		descIO.setLanguage(dryprcrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(descIO.getParams());
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			dryrDryrptRecInner.r5062Trandesc.fill("?");
		}
		else {
			if (isEQ(wsaaDtaintFlag, "Y")) {
				dryrDryrptRecInner.r5062Batctrcde.set(SPACES);
				dryrDryrptRecInner.r5062Chdrnum.set(SPACES);
				wsaaEroreror.set(e587);
				getErrorDesc2400();
			}
			else {
				if (isEQ(wsaaIncchkFlag, "Y")) {
					dryrDryrptRecInner.r5062Batctrcde.set(SPACES);
					dryrDryrptRecInner.r5062Chdrnum.set(SPACES);
					wsaaEroreror.set(e511);
					getErrorDesc2400();
				}
				else {
					dryrDryrptRecInner.r5062Trandesc.set(descIO.getLongdesc());
				}
			}
		}
		dryrDryrptRecInner.r5062Tranno.set(ptrnrevIO.getTranno());
		/* Convert PTRN Effective Date*/
		datcon1rec.function.set("CONV");
		datcon1rec.intDate.set(ptrnrevIO.getPtrneff());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		dryrDryrptRecInner.r5062Effdate.set(datcon1rec.extDate);
		dryrDryrptRecInner.r5062ErrorLine.set("N");
		dryrDryrptRecInner.dryrSortkey.set(dryrDryrptRecInner.r5062SortKey);
		/* Write the DRPT record.*/
		e000ReportRecords();
	}

protected void getErrorDesc2400()
	{
		getErrorDesc2410();
	}

protected void getErrorDesc2410()
	{
		erorIO.setErorpfx("ER");
		erorIO.setErorcoy(SPACES);
		erorIO.setErorlang(dryprcrec.language);
		erorIO.setErorprog(SPACES);
		erorIO.setEroreror(wsaaEroreror);
		erorIO.setFunction(varcom.begn);
		erorIO.setFormat(erorrec);
		SmartFileCode.execute(appVars, erorIO);
		if (isNE(erorIO.getStatuz(), varcom.oK)
		&& isNE(erorIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(erorIO.getStatuz());
			drylogrec.params.set(erorIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isNE(erorIO.getErorlang(), dryprcrec.language)
		|| isNE(erorIO.getEroreror(), wsaaEroreror)) {
			erorIO.setStatuz(varcom.endp);
		}
		if (isEQ(erorIO.getStatuz(), varcom.oK)) {
			dryrDryrptRecInner.r5062Trandesc.set(erorIO.getErordesc());
		}
		else {
			dryrDryrptRecInner.r5062Trandesc.set(erorIO.getParams());
		}
	}

protected void rewritePtrn2500()
	{
		start2510();
	}

protected void start2510()
	{
		ptrnrevIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ptrnrevIO.getParams());
			drylogrec.statuz.set(ptrnrevIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnrevIO.setUserProfile(username);
		ptrnrevIO.setValidflag("2");
		ptrnrevIO.setFunction(varcom.rewrt);
		ptrnrevIO.setFormat(ptrnrevrec);
		SmartFileCode.execute(appVars, ptrnrevIO);
		if (isNE(ptrnrevIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ptrnrevIO.getParams());
			drylogrec.statuz.set(ptrnrevIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
	}

protected void processChdrPayr3000()
	{
		readhChdr3010();
		readhPayr3030();
		updateFields3040();
		rewrtChdr3050();
		rewrtPayr3070();
	}

protected void readhChdr3010()
	{
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, chdrlifIO);
		/* Rewrite I/O statuz check below to cater for when an*/
		/* Alter from Inception has been done, since this only leaves*/
		/* one CHDR record at proposal stage i.e. Validflag = '3'.*/
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		restoreChdrMinorAlt3100();
	}

protected void readhPayr3030()
	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(chdrlifIO.getChdrcoy());
		payrIO.setChdrnum(chdrlifIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setValidflag("1");
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		restorePayrMinorAlt3200();
	}

protected void updateFields3040()
	{
		if (isNE(payrIO.getBillcd(), wsaaPayrBillcd)) {
			updatePayrNextdate3300();
		}
	}

protected void rewrtChdr3050()
	{
		chdrlifIO.setTranno(ovbldryrec.newTranno);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		varcom.vrcmDate.set(ovbldryrec.tranDate);
		varcom.vrcmTime.set(ovbldryrec.tranTime);
		varcom.vrcmUser.set(ovbldryrec.user);
		varcom.vrcmCompTranidN.set(varcom.vrcmTranidN);
		varcom.vrcmCompTermid.set(ovbldryrec.termid);
		chdrlifIO.setTranid(varcom.vrcmCompTranid);
		chdrlifIO.setFormat(chdrlifrec);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
	}

protected void rewrtPayr3070()
	{
		payrIO.setTranno(ovbldryrec.newTranno);
		payrIO.setTransactionDate(ovbldryrec.tranDate);
		payrIO.setTransactionTime(ovbldryrec.tranTime);
		payrIO.setTermid(ovbldryrec.termid);
		payrIO.setUser(ovbldryrec.user);
		payrIO.setFunction(varcom.rewrt);
		payrIO.setFormat(payrrec);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		/* Return BTDATE and PTDATE to calling process*/
		ovbldryrec.btdate.set(payrIO.getBtdate());
		ovbldryrec.ptdate.set(payrIO.getPtdate());
	}

protected void restoreChdrMinorAlt3100()
	{
		start3101();
	}

protected void start3101()
	{
		chdrlifIO.setAplsupr(wsaaChdrAplsupr);
		chdrlifIO.setBillsupr(wsaaChdrBillsupr);
		chdrlifIO.setCommsupr(wsaaChdrCommsupr);
		chdrlifIO.setNotssupr(wsaaChdrNotssupr);
		chdrlifIO.setRnwlsupr(wsaaChdrRnwlsupr);
		chdrlifIO.setAplspfrom(wsaaChdrAplspfrom);
		chdrlifIO.setBillspfrom(wsaaChdrBillspfrom);
		chdrlifIO.setCommspfrom(wsaaChdrCommspfrom);
		chdrlifIO.setNotsspfrom(wsaaChdrNotsspfrom);
		chdrlifIO.setRnwlspfrom(wsaaChdrRnwlspfrom);
		chdrlifIO.setAplspto(wsaaChdrAplspto);
		chdrlifIO.setBillspto(wsaaChdrBillspto);
		chdrlifIO.setCommspto(wsaaChdrCommspto);
		chdrlifIO.setNotsspto(wsaaChdrNotsspto);
		chdrlifIO.setRnwlspto(wsaaChdrRnwlspto);
		chdrlifIO.setCownpfx(wsaaChdrCownpfx);
		chdrlifIO.setCowncoy(wsaaChdrCowncoy);
		chdrlifIO.setCownnum(wsaaChdrCownnum);
		chdrlifIO.setJownnum(wsaaChdrJownnum);
		chdrlifIO.setPolsum(wsaaChdrPolsum);
		chdrlifIO.setDesppfx(wsaaChdrDesppfx);
		chdrlifIO.setDespcoy(wsaaChdrDespcoy);
		chdrlifIO.setDespnum(wsaaChdrDespnum);
		chdrlifIO.setAgntpfx(wsaaChdrAgntpfx);
		chdrlifIO.setAgntcoy(wsaaChdrAgntcoy);
		chdrlifIO.setAgntnum(wsaaChdrAgntnum);
	}

protected void restorePayrMinorAlt3200()
	{
		/*START*/
		payrIO.setAplsupr(wsaaPayrAplsupr);
		payrIO.setBillsupr(wsaaPayrBillsupr);
		payrIO.setNotssupr(wsaaPayrNotssupr);
		payrIO.setAplspfrom(wsaaPayrAplspfrom);
		payrIO.setBillspfrom(wsaaPayrBillspfrom);
		payrIO.setNotsspfrom(wsaaPayrNotsspfrom);
		payrIO.setAplspto(wsaaPayrAplspto);
		payrIO.setBillspto(wsaaPayrBillspto);
		payrIO.setNotsspto(wsaaPayrNotsspto);
		/*EXIT*/
	}

protected void updatePayrNextdate3300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start3301();
				case continue3340: 
					continue3340();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void start3301()
	{
		String key;
		if(BTPRO028Permission) {
			key = payrIO.getBillchnl().toString().trim() + chdrlifIO.getCnttype().toString().trim() + payrIO.getBillfreq().toString().trim();
			if(!readT6654(key)) {
				key = payrIO.getBillchnl().toString().trim().concat(chdrlifIO.getCnttype().toString().trim()).concat("**");
				if(!readT6654(key)) {
					key = payrIO.getBillchnl().toString().trim().concat("*****");
					if(!readT6654(key)) {
						drylogrec.statuz.set(itemIO.getStatuz());
						drylogrec.params.set(itemIO.getParams());
						drylogrec.dryDatabaseError.setTrue();
						a000FatalError();
					}
				}
			}
		}
		else {
		itemIO.setDataKey(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(payrIO.getChdrcoy());
		itemIO.setItemtabl(t6654);
		wsaaT6654Item.set(SPACES);
		wsaaT6654Billchnl.set(payrIO.getBillchnl());
		wsaaT6654Cnttype.set(chdrlifIO.getCnttype());
		itemIO.setItemitem(wsaaT6654Item);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		if (isEQ(itemIO.getStatuz(), varcom.oK)) {
			t6654rec.t6654Rec.set(itemIO.getGenarea());
			goTo(GotoLabel.continue3340);
		}
		wsaaT6654Cnttype.set("***");
		itemIO.setItemitem(wsaaT6654Item);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61 
		}
		t6654rec.t6654Rec.set(itemIO.getGenarea());
		}
	}

protected boolean readT6654(String key) {
	itempf = new Itempf();
	wsaaT6654Billchnl.set(payrIO.getBillchnl());
	wsaaT6654Cnttype.set(chdrlifIO.getCnttype());
	itempf.setItempfx("IT");
	itempf.setItemcoy("2");
	itempf.setItemtabl(t6654);
	itempf.setItemitem(key);
	itempf = itempfDAO.getItempfRecord(itempf);
	if (null != itempf) {
		t6654rec.t6654Rec.set(StringUtil.rawToString(itempf.getGenarea()));
		return true;
	}
	return false;
}

protected void continue3340()
	{
		/*    MOVE PAYR-BILLCD            TO DTC2-INT-DATE-1.              */
		/*    MOVE 'DY'                   TO DTC2-FREQUENCY.               */
		/*    COMPUTE  DTC2-FREQ-FACTOR    =  T6654-LEAD-DAYS * -1.        */
		/*    CALL 'DATCON2'           USING DTC2-DATCON2-REC.             */
		/*    MOVE DTC2-INT-DATE-2        TO PAYR-NEXTDATE.                */
		/* Get current year holidays.                                      */
		wsaaAddToDate.set(payrIO.getBillcd());
		wsaaThisYear.set(wsaaYear);
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(chdrlifIO.getCowncoy());
		itemIO.setItemtabl(t3699);
		itemIO.setItemitem(wsaaYyyy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			a000FatalError();//ILPI-61 
		}
		t3699rec.t3699Rec.set(itemIO.getGenarea());
		wsaaThisYy.set(t3699rec.t3699Rec);
		/* Get last year's holidays.                                       */
		wsaaAddToDate.set(payrIO.getBillcd());
		wsaaYear.subtract(1);
		wsaaLastYear.set(wsaaYear);
		itemIO.setItemitem(wsaaYyyy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			a000FatalError();//ILPI-61 
		}
		t3699rec.t3699Rec.set(itemIO.getGenarea());
		wsaaLastYy.set(t3699rec.t3699Rec);
		/* Get next year's holidays.                                       */
		wsaaAddToDate.set(payrIO.getBillcd());
		wsaaYear.add(1);
		wsaaNextYear.set(wsaaYear);
		itemIO.setItemitem(wsaaYyyy);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			a000FatalError();//ILPI-61 
		}
		t3699rec.t3699Rec.set(itemIO.getGenarea());
		wsaaNextYy.set(t3699rec.t3699Rec);
		wsaaAddToDate.set(payrIO.getBillcd());
		for (wsaaCnt.set(1); !(isGT(wsaaCnt, t6654rec.leadDays)); wsaaCnt.add(1)){
			datcon2rec.intDate1.set(wsaaAddToDate);
			datcon2rec.frequency.set("DY");
			datcon2rec.freqFactor.set(-1);
			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
			if (isNE(datcon2rec.statuz, varcom.oK)) {
				drylogrec.params.set(datcon2rec.datcon2Rec);
				drylogrec.statuz.set(datcon2rec.statuz);
				a000FatalError();//ILPI-61 
			}
			wsaaAddToDate.set(datcon2rec.intDate2);
			checkHolidays3400();
		}
		payrIO.setNextdate(wsaaAddToDate);
	}

protected void checkHolidays3400()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					setUp3410();
				case startCheck3420: 
					startCheck3420();
				case setDate3480: 
					setDate3480();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void setUp3410()
	{
		if (isEQ(wsaaYear, wsaaLastYear)) {
			wsaaCountMm.set(lastStart);
		}
		if (isEQ(wsaaYear, wsaaThisYear)) {
			wsaaCountMm.set(thisStart);
		}
		if (isEQ(wsaaYear, wsaaNextYear)) {
			wsaaCountMm.set(nextStart);
		}
		wsaaCountMm.add(wsaaMm);
		wsaaCountDd.set(wsaaDd);
	}

protected void startCheck3420()
	{
		wsaaValyd.set(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()]);
		if (!valyd.isTrue()) {
			drylogrec.statuz.set(h842);
			drylogrec.params.set(t3699);
			a000FatalError();//ILPI-61 
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()], ".")
		|| isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()], "D")) {
			goTo(GotoLabel.setDate3480);
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()], "W")) {
			wsaaCountDd.subtract(1);
			if (isLT(wsaaCountDd, 1)) {
				wsaaCountDd.set(31);
				wsaaCountMm.subtract(1);
			}
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()], "E")) {
			wsaaCountDd.subtract(1);
			if (isLT(wsaaCountDd, 1)) {
				wsaaCountDd.set(31);
				wsaaCountMm.subtract(1);
			}
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()], "H")) {
			wsaaCountDd.subtract(1);
			if (isLT(wsaaCountDd, 1)) {
				wsaaCountDd.set(31);
				wsaaCountMm.subtract(1);
			}
		}
		if (isEQ(wsaa3Dd[wsaaCountMm.toInt()][wsaaCountDd.toInt()], "X")) {
			wsaaCountDd.subtract(1);
			if (isLT(wsaaCountDd, 1)) {
				wsaaCountDd.set(31);
				wsaaCountMm.subtract(1);
			}
		}
		goTo(GotoLabel.startCheck3420);
	}

protected void setDate3480()
	{
		wsaaDd.set(wsaaCountDd);
		wsaaMm.set(wsaaCountMm);
		if (isLT(wsaaCountMm, 13)) {
			wsaaMm.set(wsaaCountMm);
			wsaaYear.set(wsaaLastYear);
		}
		if (isGT(wsaaCountMm, 12)
		&& isLT(wsaaCountMm, 25)) {
			compute(wsaaMm, 0).set(sub(wsaaCountMm, 12));
			wsaaYear.set(wsaaThisYear);
		}
		if (isGT(wsaaCountMm, 24)) {
			compute(wsaaMm, 0).set(sub(wsaaCountMm, 24));
			wsaaYear.set(wsaaNextYear);
		}
	}

protected void statistics5000()
	{
		start5010();
	}

protected void start5010()
	{
		lifsttrrec.batccoy.set(dryprcrec.batccoy);
		lifsttrrec.batcbrn.set(dryprcrec.batcbrn);
		lifsttrrec.batcactyr.set(dryprcrec.batcactyr);
		lifsttrrec.batcactmn.set(dryprcrec.batcactmn);
		lifsttrrec.batctrcde.set(dryprcrec.batctrcde);
		lifsttrrec.batcbatch.set(dryprcrec.batcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(ovbldryrec.newTranno);
		lifsttrrec.trannor.set(ptrnrevIO.getTranno());
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, varcom.oK)) {
			drylogrec.params.set(lifsttrrec.lifsttrRec);
			drylogrec.statuz.set(lifsttrrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61 
		}
	}
//Modify for ILPI-61 
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(dryprcrec.runDate);
//		drylogrec.entityKey.set(dryprcrec.entityKey);
//		drylogrec.runNumber.set(dryprcrec.runNumber);
//		ovbldryrec.statuz.set(drylogrec.statuz);
//		dryprcrec.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}

protected void e000ReportRecords()
	{
		e010Call();
	}

protected void e010Call()
	{
		dryrDryrptRecInner.dryrReportName.set(dryrDryrptRecInner.r5062ReportName);
		dryrDryrptRecInner.dryrDiarySubroutine.set(wsaaProg);
		dryrDryrptRecInner.dryrEffectiveDate.set(dryprcrec.runDate);
		dryrDryrptRecInner.dryrEntityKey.set(dryprcrec.entityKey);
		dryrDryrptRecInner.dryrThreadNumber.set(dryprcrec.threadNumber);
		dryrDryrptRecInner.dryrRunNumber.set(dryprcrec.runNumber);
		callProgram(Dryreport.class, dryrDryrptRecInner.dryrDryrptRec);
		if (isNE(dryrDryrptRecInner.dryrStatuz, varcom.oK)) {
			drylogrec.params.set(dryrDryrptRecInner.dryrDryrptRec);
			drylogrec.statuz.set(dryrDryrptRecInner.dryrStatuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61 
		}
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 

	private FixedLengthStringData dryrDryrptRec = new FixedLengthStringData(638);
	private FixedLengthStringData dryrStatuz = new FixedLengthStringData(4).isAPartOf(dryrDryrptRec, 0);
	private FixedLengthStringData dryrDiarySubroutine = new FixedLengthStringData(10).isAPartOf(dryrDryrptRec, 4);
	//cluster support by vhukumagrawa
	private PackedDecimalData dryrThreadNumber = new PackedDecimalData(3, 0).isAPartOf(dryrDryrptRec, 14);
	private ZonedDecimalData dryrRunNumber = new ZonedDecimalData(8, 0).isAPartOf(dryrDryrptRec, 16).setUnsigned();
	private PackedDecimalData dryrEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(dryrDryrptRec, 24);
	private FixedLengthStringData dryrReportName = new FixedLengthStringData(10).isAPartOf(dryrDryrptRec, 29);
	private FixedLengthStringData dryrEntityKey = new FixedLengthStringData(13).isAPartOf(dryrDryrptRec, 39);
	private FixedLengthStringData dryrCompany = new FixedLengthStringData(1).isAPartOf(dryrEntityKey, 0);
	private FixedLengthStringData dryrBranch = new FixedLengthStringData(2).isAPartOf(dryrEntityKey, 1);
	private FixedLengthStringData dryrEntityType = new FixedLengthStringData(2).isAPartOf(dryrEntityKey, 3);
	private FixedLengthStringData dryrEntity = new FixedLengthStringData(8).isAPartOf(dryrEntityKey, 5);
	private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38).isAPartOf(dryrDryrptRec, 52);
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500).isAPartOf(dryrDryrptRec, 90);
	private FixedLengthStringData r5062DataArea = new FixedLengthStringData(500).isAPartOf(dryrGenarea, 0, REDEFINE);
	private FixedLengthStringData r5062Batctrcde = new FixedLengthStringData(4).isAPartOf(r5062DataArea, 0);
	private FixedLengthStringData r5062Trandesc = new FixedLengthStringData(30).isAPartOf(r5062DataArea, 4);
	private ZonedDecimalData r5062Tranno = new ZonedDecimalData(5, 0).isAPartOf(r5062DataArea, 34).setUnsigned();
	private FixedLengthStringData r5062Effdate = new FixedLengthStringData(10).isAPartOf(r5062DataArea, 39);
	private FixedLengthStringData r5062ErrorLine = new FixedLengthStringData(1).isAPartOf(r5062DataArea, 49);
	private FixedLengthStringData r5062SortKey = new FixedLengthStringData(38).isAPartOf(dryrDryrptRec, 590);
	private FixedLengthStringData r5062Chdrnum = new FixedLengthStringData(8).isAPartOf(r5062SortKey, 0);
	private FixedLengthStringData r5062ReportName = new FixedLengthStringData(10).isAPartOf(dryrDryrptRec, 628).init("R5062     ");
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
