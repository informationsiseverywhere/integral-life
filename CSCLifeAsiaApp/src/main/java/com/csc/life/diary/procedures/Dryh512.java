package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;
import com.csc.diary.recordstructures.Dryrptrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.general.procedures.Datcon4;
import com.csc.fsu.general.procedures.Zrdecplc;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsu.general.recordstructures.Datcon4rec;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.interestbearing.dataaccess.HitdTableDAM;
import com.csc.life.interestbearing.dataaccess.HitddryTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrintTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.interestbearing.recordstructures.Ibincalrec;
import com.csc.life.interestbearing.tablestructures.Th510rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.procedures.Vpxibin;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxibinrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.tablestructures.T6626rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;

/*
*       INTEREST BEARING INTEREST PROCESSING
*       ------------------------------------
*
* Interest Bearing Interest processes the Application of
* Interest Bearing Fund Interest Details(HITDPF).
*
* Processing.
* ----------
*
* The HITDPF will be read where
*   Validflag       = '1'
*   Contract Header Number  = DIARY Entity
*   Contract Header drypCompany = DIARY drypCompany
*   Next Interest Due Date <= DIARY Effective Date
*
*  -  Read the hitddryIO records keeping a count of
*     the number read and storing the present hitddryIO details for
*     the WSYS- error record.
*
*     Validate the CHDR and Coverage details.
*
*  -  Check the status code
*
*  -  Validate the contract and coverage
*
*  600-UPDATE
*  -----------
*
*  -  Call the relevant Interest Calculation subroutine.
*     All the associated HITDPF data will be specified in the
*     linkage record. The subroutine will loop around all
*     eligible HITRPF records (i.e. where Feedback Indicator = Y,
*     Interest Applied Indicator = spaces, Type = P and Effective
*     Date is less than or equal to the Next Interest Date).
*
*       Write a PTRN
*       Update the Contract Header
*
* Control totals used in this program:
*
*    01  -  No. of hitdIO records read
*    02  -  No. of PTRN records created
*    03  -  No. of CHDR records modified
***********************************************************************
*           AMENDMENT  HISTORY                                        *
***********************************************************************
* DATE.... VSN/MOD  WORK UNIT    BY....                               *
*                                                                     *
* 24/03/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
*           Initial Version.                                          *
*                                                                     *
* 20/07/09  01/01   LA5184       CSC - Patricia Starrs                *
*           Bugzilla 361.                                             *
*           Use todays date to set DATESUB on PTRN                    *
*                                                                     *
**DD/MM/YY*************************************************************
*/

public class Dryh512 extends Maind {
	
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYH512");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private ZonedDecimalData wsaaToday = new ZonedDecimalData(8).init(0);
	private FixedLengthStringData wsaaContractVariables = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).isAPartOf(wsaaContractVariables, 0).init("N");
	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).isAPartOf(wsaaContractVariables, 1).init("N");
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(3).isAPartOf(wsaaContractVariables,2);
	private FixedLengthStringData wsaaHitddryKey = new FixedLengthStringData(265);
	private FixedLengthStringData wsaaChdrcoy = new FixedLengthStringData(1).isAPartOf(wsaaHitddryKey, 0);
	private FixedLengthStringData wsaaChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaHitddryKey, 1);
	private FixedLengthStringData wsaaLife = new FixedLengthStringData(2).isAPartOf(wsaaHitddryKey, 9);
	private FixedLengthStringData wsaaCoverage = new FixedLengthStringData(2).isAPartOf(wsaaHitddryKey, 11);
	private FixedLengthStringData wsaaRider = new FixedLengthStringData(2).isAPartOf(wsaaHitddryKey, 13);
	private PackedDecimalData wsaaPlanSuffix = new PackedDecimalData(4).isAPartOf(wsaaHitddryKey, 15);
	private FixedLengthStringData wsaaZintbfnd = new FixedLengthStringData(4).isAPartOf(wsaaHitddryKey, 19);
	private PackedDecimalData wsysZnxtintdte = new PackedDecimalData(8).isAPartOf(wsaaHitddryKey, 23);
	private FixedLengthStringData wsaaFiller = new FixedLengthStringData(234).isAPartOf(wsaaHitddryKey, 31);
	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted,"Y");
	private PackedDecimalData wsaaZlstintdte = new PackedDecimalData(8);
	private PackedDecimalData wsaaZnxtintdte = new PackedDecimalData(8);
	private PackedDecimalData wsaaZlstfndval = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaIntRound = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaInterestDate = new PackedDecimalData(8);
	private PackedDecimalData wsaaInterestTot = new PackedDecimalData(13, 4);
	private PackedDecimalData wsaaIntOnFundAmt = new PackedDecimalData(13, 4);
	private PackedDecimalData wsaaIntOnFundBal = new PackedDecimalData(13, 4);
	private PackedDecimalData wsaaFundAmount = new PackedDecimalData(15, 2);
	private FixedLengthStringData wsaaNoMoreInt = new FixedLengthStringData(1).init(SPACE);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3);
	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6647Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaT6647Key, 7).init(SPACES);
	private FixedLengthStringData wsaaTriggerFound = new FixedLengthStringData(1);
	private PackedDecimalData wsaaTranno = new PackedDecimalData(5);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).init(SPACE);
	private FixedLengthStringData errors = new FixedLengthStringData(8);
	private FixedLengthStringData e308 = new FixedLengthStringData(4).isAPartOf(errors, 0).init("E308");
	private FixedLengthStringData h791 = new FixedLengthStringData(4).isAPartOf(errors, 4).init("H791");
	private FixedLengthStringData tables = new FixedLengthStringData(41);
	private FixedLengthStringData t5515 = new FixedLengthStringData(6).isAPartOf(tables, 0).init("T5515");
	private FixedLengthStringData t5645 = new FixedLengthStringData(6).isAPartOf(tables, 6).init("T5645");
	private FixedLengthStringData t5679 = new FixedLengthStringData(6).isAPartOf(tables, 12).init("T5679");
	private FixedLengthStringData t5688 = new FixedLengthStringData(6).isAPartOf(tables, 18).init("T5688");
	private FixedLengthStringData t6647 = new FixedLengthStringData(6).isAPartOf(tables, 24).init("T6647");
	private FixedLengthStringData th510 = new FixedLengthStringData(6).isAPartOf(tables, 30).init("TH510");
	private FixedLengthStringData t6626 = new FixedLengthStringData(5).isAPartOf(tables, 36).init("T6626");
	private static final String itemrec = "ITEMREC";
	private static final String chdrlifrec = "CHDRLIFREC";
	private static final String ptrnrec = "PTRNREC";
	private static final String covrrec = "COVRREC";
	private static final String hitdrec = "HITDREC";
	private static final String hitddryrec = "HITDDRYREC";
	private static final String hitralorec = "HITRALOREC";
	private static final String hitrintrec = "HITRINTREC";
	private static final String hitrrec = "HITRREC";
	private static final String hitsrec = "HITSREC";
	private ZonedDecimalData controlTotals = new ZonedDecimalData(6);
	private ZonedDecimalData ct01 = new ZonedDecimalData(2).isAPartOf(controlTotals, 0).init(01);
	private ZonedDecimalData ct02 = new ZonedDecimalData(2).isAPartOf(controlTotals, 2).init(02);
	private ZonedDecimalData ct03 = new ZonedDecimalData(2).isAPartOf(controlTotals, 4).init(03);
	/*
      * DRYMAINB copybooks
     */
	private Dryrptrec dryrptrec = new Dryrptrec();
	private Varcom varcom = new Varcom();
	/*
      * Subroutine copybooks for DRY5111.
     */
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon4rec datcon4rec = new Datcon4rec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Ibincalrec ibincalrec = new Ibincalrec();
	private Batcuprec batcuprec = new Batcuprec();
	/*  Database layouts */
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private HitddryTableDAM hitddryIO = new HitddryTableDAM();
	private HitdTableDAM hitdIO = new HitdTableDAM();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private HitrintTableDAM hitrintIO = new HitrintTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	/*  Table layouts */
	private T6626rec t6626rec = new T6626rec();
	private T5515rec t5515rec = new T5515rec();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5688rec t5688rec = new T5688rec();
	private T6647rec t6647rec = new T6647rec();
	private Th510rec th510rec = new Th510rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private Zrdecplrec zrdecplrec = new Zrdecplrec();
	
	private ExternalisedRules er = new ExternalisedRules();
	
	public Dryh512() {
		super();
	}
	
	/**
	* Contains all possible labels used by goTo action.
	*/
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		nextRecord280,
		skipLast2320,
		skipInterest2450,
		nextHitr2480,
		exit2490,
		exit190,
		exit690,
		exit290,
		exit2290,
		exit2590,
		updateHitd2680,
		calc2680
	}
	
	
	/**
	* The mainline method is the default entry point to the class
	*/
	public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

	protected void startProcessing100(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start110();
				case exit190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start110(){
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
		initialize(wsaaContractVariables);
		/* Access today's date using DATCON1. */
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
	
		if(!isEQ(datcon1rec.statuz,Varcom.oK)){
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaToday.set(datcon1rec.intDate);
		/*
		 *  Read Contract Header
		 */
		checkTransaction300();
		/*
		 *  Read Table T5679 for Contract statii.
		 */
		itemIO.setStatuz(Varcom.oK);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
	
		if(!isEQ(itemIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
	
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		validateContract400();
		if(isEQ(wsaaValidContract,"N")){
			goTo(GotoLabel.exit190);
		}
		
		readTables1000();
		hitddryIO.setParams(SPACES);
		hitddryIO.chdrcoy.set(drypDryprcRecInner.drypCompany);
		hitddryIO.chdrnum.set(drypDryprcRecInner.drypEntity);
		hitddryIO.life.set("01");
		hitddryIO.coverage.set("01");
		hitddryIO.rider.set("00");
		hitddryIO.zintbfnd.set(SPACES);
		hitddryIO.planSuffix.set(ZERO);
		hitddryIO.setFunction(Varcom.begn);
		hitddryIO.setFormat(hitddryrec);
		hitddryIO.statuz.set(Varcom.oK);
		wsaaHitddryKey.set(hitddryIO.getDataKey());
		while (isNE(hitddryIO.statuz, Varcom.endp)) {
			readFile200();
		}
		/*
		 * A PTRN is written and Contract Header updated for each contract
		 */
		dryProcessing3000();
	}


	protected void readFile200(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readFiles210();
				case nextRecord280:
					hitddryIO.setFunction(varcom.nextr);
				case exit290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
			
	}
	
	protected void readFiles210(){
		/*
		 * Read the records from hitddryIO file
		 */
		SmartFileCode.execute(appVars, hitddryIO);
		if(!isEQ(hitddryIO.statuz,Varcom.oK)&&!isEQ(hitddryIO.statuz,Varcom.endp)){
			drylogrec.statuz.set(hitddryIO.statuz);
			drylogrec.params.set(wsaaHitddryKey);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(isEQ(hitddryIO.getStatuz(),Varcom.endp)||!isEQ(hitddryIO.getChdrcoy(),drypDryprcRecInner.drypCompany)||!isEQ(hitddryIO.getChdrnum(),drypDryprcRecInner.drypEntity)){
			hitddryIO.statuz.set(Varcom.endp);
			goTo(GotoLabel.exit290);

		}
		
		if(isGT(hitddryIO.znxtintdte,drypDryprcRecInner.drypRunDate)){
			goTo(GotoLabel.nextRecord280);
		}
		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		/*
		 *  Here we must validate the hitsIO details.
		 */
		checkHits2000();
		if(isEQ(hitsIO.getStatuz(),Varcom.mrnf)){
			goTo(GotoLabel.nextRecord280);
		}
		validateCoverage500();
		if(isEQ(wsaaValidCoverage,"N")){
			goTo(GotoLabel.nextRecord280);
		}
		checkHitr2100();
		wsaaZlstintdte.set(hitddryIO.getZlstintdte());
		wsaaZnxtintdte.set(hitddryIO.getZnxtintdte());
		wsaaZlstfndval.set(hitddryIO.getZlstfndval());
		update600();
	}
	
	protected void checkTransaction300(){
		start310();
	}
	
	protected void start310(){
		/*
		* Read the Contract Header
		*/
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
	
		if(!isEQ(chdrlifIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}


	protected void validateContract400(){
		start410();
	}
	
	protected void start410(){
		/*
		*  Validate the statii of the contract
		*/
		wsaaValidContract.set("N");

		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12));wsaaT5679Sub.add(1)){
			if(isEQ(t5679rec.cnRiskStat[wsaaT5679Sub.toInt()],chdrlifIO.getStatcode())){
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12));wsaaT5679Sub.add(1)){
					if(isEQ(t5679rec.cnPremStat[wsaaT5679Sub.toInt()],chdrlifIO.getPstatcode())){
						wsaaT5679Sub.set(13);
						wsaaValidContract.set("Y");
					}
				}
			}
		}
	}


	protected void validateCoverage500(){
		start510();
	}
	
	protected void start510(){
		/*
		* Check to see if coverage is of a valid status
		*/
		covrIO.setChdrcoy(hitddryIO.getChdrcoy());
		covrIO.setChdrnum(hitddryIO.getChdrnum());
		covrIO.setLife(hitddryIO.getLife());
		covrIO.setCoverage(hitddryIO.getCoverage());
		covrIO.setRider(hitddryIO.getRider());
		covrIO.setPlanSuffix(hitddryIO.getPlanSuffix());
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);

		if(!isEQ(covrIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();

		}
		wsaaValidCoverage.set("N");

		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12));wsaaT5679Sub.add(1)){
			if(isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()],covrIO.getStatcode())){
				for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12));wsaaT5679Sub.add(1)){
					if(isEQ(t5679rec.covPremStat[wsaaT5679Sub.toInt()],covrIO.getPstatcode())){
						wsaaT5679Sub.set(13);
						wsaaValidCoverage.set("Y");
					}
				}
			}
		}
	}

	protected void update600(){
		update610();
	}
	
	protected void update610(){
		if(isEQ(wsaaZlstintdte,varcom.vrcmMaxDate)){
			calculateInterest2300();
			return;
		}
		
		while (!isGT(wsaaZnxtintdte,drypDryprcRecInner.drypRunDate)) {
			calculateInterest2300();
		}
	}


	protected void readTables1000(){
		read1100();
	}
	
	protected void read1100(){
		/*
		*    Read T5645 for the Accounting Rules.
		*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5645);
		itemIO.setItemitem(wsaaProg);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		
		if(!isEQ(itemIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5645rec.t5645Rec.set(itemIO.getGenarea());
		/*
		*    Get Contract Processing Rules.
		*/
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(varcom.vrcmMaxDate);
		itdmIO.setFunction(Varcom.begn);
		itdmIO.setStatuz(Varcom.oK);
		SmartFileCode.execute(appVars, itdmIO);

		if(!isEQ(itdmIO.getStatuz(),Varcom.oK)&&!isEQ(itdmIO.getStatuz(),Varcom.endp)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(!isEQ(itdmIO.getItemtabl(),t5688)||!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.drypCompany)||!isEQ(itdmIO.getItemitem(),chdrlifIO.getCnttype())){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaComponLevelAccounted.set(t5688rec.comlvlacc);
		/*
		*    Load Non-Traditional Contract Details.
		*/
		itdmIO.setStatuz(Varcom.oK);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6647);
		wsaaT6647Trancode.set(drypDryprcRecInner.drypBatctrcde);
		wsaaT6647Cnttype.set(chdrlifIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(Varcom.begn);
		itdmIO.setStatuz(Varcom.oK);
		SmartFileCode.execute(appVars, itdmIO);

		if(!isEQ(itdmIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(!isEQ(itdmIO.getItemtabl(),t6647)||!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.drypCompany)||!isEQ(itdmIO.getItemitem(),wsaaT6647Key)||isLT(itdmIO.getItmto(),drypDryprcRecInner.drypRunDate)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			itdmIO.setItemitem(wsaaT6647Key);
			itdmIO.setItemtabl(t6647);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		
		/* Read T6626 to find transaction codes that requires no further
		* interest allocation.
		*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t6626);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);

		if(!isEQ(itemIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t6626rec.t6626Rec.set(itemIO.getGenarea());
	}

	protected void checkHits2000(){
		hits2010();
	}
	
	protected void hits2010(){
		hitsIO.setParams(SPACES);
		hitsIO.chdrcoy.set(hitddryIO.getChdrcoy());
		hitsIO.chdrnum.set(hitddryIO.getChdrnum());
		hitsIO.life.set(hitddryIO.getLife());
		hitsIO.coverage.set(hitddryIO.getCoverage());
		hitsIO.rider.set(hitddryIO.getRider());
		hitsIO.planSuffix.set(hitddryIO.getPlanSuffix());
		hitsIO.zintbfnd.set(hitddryIO.getZintbfnd());
		hitsIO.setFormat(hitsrec);
		hitsIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, hitsIO);

		if(!isEQ(hitsIO.getStatuz(),Varcom.oK)&&!isEQ(hitsIO.getStatuz(),Varcom.mrnf)){
			drylogrec.statuz.set(hitsIO.getStatuz());
			drylogrec.params.set(hitsIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void checkHitr2100(){
		hitr2110();
	}
	
	protected void hitr2110(){
		/*
		 *    Here we are searching for any unprocessed hitrIO records that
		 *    are awaiting completion by a trigger module in unit deal. If
		 *    one is found, then we must attach the hitrIO and hitdIO records
		 *    created by this program to the incomplete transaction, as
		 *    opposed to creating a new PTRN here. The reason for this is
		 *    to ensure that the two transactions (i.e. the one awaiting
		 *    completion and this one) are reversed together.
		 */
		hitraloIO.setParams(SPACES);
		hitraloIO.chdrcoy.set(hitddryIO.getChdrcoy());
		hitraloIO.chdrnum.set(hitddryIO.getChdrnum());
		hitraloIO.life.set(hitddryIO.getLife());
		hitraloIO.coverage.set(hitddryIO.getCoverage());
		hitraloIO.rider.set(hitddryIO.getRider());
		hitraloIO.planSuffix.set(hitddryIO.getPlanSuffix());
		hitraloIO.procSeqNo.set(ZERO);
		hitraloIO.tranno.set(ZERO);
		hitraloIO.effdate.set(ZERO);
		hitraloIO.setFormat(hitralorec);
		hitraloIO.setFunction(Varcom.begn);

		wsaaTriggerFound.set("N");

		while ( !(isEQ(hitraloIO.getStatuz(), Varcom.endp))) {
				readHitr2200();
			}
	}

	protected void readHitr2200(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					hitr2210();
				case exit2290: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
		
	}
	
	protected void hitr2210(){
		SmartFileCode.execute(appVars, hitraloIO);
		
		if(!isEQ(hitraloIO.getStatuz(),Varcom.oK)&&!isEQ(hitraloIO.getStatuz(),Varcom.endp)){
			drylogrec.params.set(hitraloIO.getParams());
			drylogrec.statuz.set(hitraloIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if(!isEQ(hitraloIO.getChdrcoy(),hitddryIO.getChdrcoy())||!isEQ(hitraloIO.getChdrnum(),hitddryIO.getChdrnum())||!isEQ(hitraloIO.getLife(),hitddryIO.getLife())||!isEQ(hitraloIO.getCoverage(),hitddryIO.getCoverage())||!isEQ(hitraloIO.getRider(),hitddryIO.getRider())||!isEQ(hitraloIO.getPlanSuffix(),hitddryIO.getPlanSuffix())||isEQ(hitraloIO.getStatuz(),Varcom.endp)){
			hitraloIO.statuz.set(Varcom.endp);
			goTo(GotoLabel.exit2290);
		}

		if(!isEQ(hitraloIO.getTriggerModule(),SPACES)){
			wsaaTriggerFound.set("Y");
			wsaaTranno.set(hitraloIO.getTranno());
			wsaaBatctrcde.set(hitraloIO.getBatctrcde());
			hitraloIO.statuz.set(Varcom.endp);
			goTo(GotoLabel.exit2290);
		}
		hitraloIO.setFunction(varcom.nextr);
	}


	protected void calculateInterest2300(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					calc2310();
				case skipLast2320:
					skipLast2320();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
		
	}
	
	protected void calc2310(){
		if(isEQ(wsaaTriggerFound,"N")){
			updates2800();
		}
		
		/*
		*   Get Interest Bearing Fund Definitions
		*/
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(th510);
		itdmIO.setItemitem(hitddryIO.getZintbfnd());
		itdmIO.setItmfrm(hitddryIO.getZlstintdte());
		itdmIO.setFunction(Varcom.begn);
		itdmIO.setStatuz(Varcom.oK);
		SmartFileCode.execute(appVars, itdmIO);

		if(!isEQ(itdmIO.getStatuz(),Varcom.oK)&&!isEQ(itdmIO.getStatuz(),Varcom.endp)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.drypCompany)||!isEQ(itdmIO.getItemtabl(),th510)||!isEQ(itdmIO.getItemitem(),hitddryIO.getZintbfnd())||isEQ(itdmIO.getStatuz(),Varcom.endp)){
			itdmIO.setItemtabl(th510);
			itdmIO.setItemitem(hitddryIO.getZintbfnd());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		th510rec.th510Rec.set(itdmIO.getGenarea());
		
		/*    The first thing to do here is to check whether there are
		  *    any hitrIO records that should have been allocated interest
		  *    prior to today. This may happen if a transaction is reversed
		  *    and causing the effective date of the original hitrIO to be
		  *    in the past. If any such hitrIO records are found, we must
		  *    calculate the interest due on each of these records up to
		  *    the last interest allocation date, then add the interest
		  *    calculated and the fund amounts on these HITRs into the
		  *    current fund balance. Once this has been done, the fund
		  *    balance will be accurate and up to date, and we will be
		  *    ready to calculate interest for the current allocation
		  *    period.
		  */
		if(isEQ(hitddryIO.zlstintdte,varcom.vrcmMaxDate)){
			goTo(GotoLabel.skipLast2320);
		}
		wsaaIntOnFundAmt.set(ZERO);
		wsaaFundAmount.set(ZERO);
		hitrintIO.setParams(SPACES);
		hitrintIO.chdrcoy.set(hitddryIO.getChdrcoy());
		hitrintIO.chdrnum.set(hitddryIO.getChdrnum());
		hitrintIO.life.set(hitddryIO.getLife());
		hitrintIO.coverage.set(hitddryIO.getCoverage());
		hitrintIO.rider.set(hitddryIO.getRider());
		hitrintIO.planSuffix.set(hitddryIO.getPlanSuffix());
		hitrintIO.zintbfnd.set(hitddryIO.getZintbfnd());
		hitrintIO.effdate.set(wsaaZlstintdte);
		wsaaInterestDate.set(wsaaZlstintdte);
		hitrintIO.setFormat(hitrintrec);
		hitrintIO.setFunction(Varcom.begn);
		hitrintIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrintIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX","ZINTBFND");
		while ( !(isEQ(hitrintIO.getStatuz(), Varcom.endp))) {
			readHitr2400();
		}

		wsaaIntRound.setRounded(wsaaIntOnFundAmt);

		if(isLT(wsaaIntRound,0)){
			wsaaZlstfndval.add(wsaaIntOnFundAmt);
		}else{
			wsaaZlstfndval.add(add(wsaaIntRound,wsaaFundAmount));
		}
		wsaaInterestTot.set(wsaaIntOnFundAmt);
		if(isGT(wsaaInterestTot,0)){
			writeHitr2500();
		}
	}
	
	protected void skipLast2320(){
		/*
		 *    Calculate interest on the current fund balance.
		 */
		ibincalrec.ibincalRec.set(SPACES);
		ibincalrec.company.set(drypDryprcRecInner.drypCompany);

		if(isEQ(wsaaZlstintdte,varcom.vrcmMaxDate)){
			ibincalrec.lastIntApp.set(hitddryIO.effdate);
		}else{
			ibincalrec.lastIntApp.set(wsaaZlstintdte);
		}
		
		ibincalrec.nextIntDue.set(wsaaZnxtintdte);
		ibincalrec.capAmount.set(wsaaZlstfndval);
		ibincalrec.intAmount.set(ZERO);
		ibincalrec.intRate.set(ZERO);
		ibincalrec.fund.set(hitddryIO.zintbfnd);
		ibincalrec.crrcd.set(covrIO.getCrrcd());
		
		if(!(AppVars.getInstance().getAppConfig().isVpmsEnable() && er.isCallExternal(th510rec.zintcalc.toString()) && 
  				er.isExternalized(chdrlifIO.getCnttype().toString(), covrIO.crtable.toString())))
  		{
			callProgramX(th510rec.zintcalc,ibincalrec.ibincalRec);
  		}
		else
		{
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxibinrec vpxibinrec  = new Vpxibinrec();

			vpmcalcrec.linkageArea.set(ibincalrec.ibincalRec);
			vpxibinrec.function.set("INIT");
			callProgram(Vpxibin.class, vpmcalcrec.vpmcalcRec, vpxibinrec);
			ibincalrec.ibincalRec.set(vpmcalcrec.linkageArea);
			//ILIFE-7300
			ibincalrec.cnttype.set(chdrlifIO.getCnttype());
			ibincalrec.crtable.set(covrIO.getCrtable());

			callProgram(th510rec.zintcalc, ibincalrec.ibincalRec);			
		}

		if(!isEQ(ibincalrec.statuz,Varcom.oK)){
			drylogrec.params.set(ibincalrec.ibincalRec);
			drylogrec.statuz.set(ibincalrec.statuz);
			drylogrec.drySubrtnError.setTrue();
			a000FatalError();
		}
		
		if (isNE(ibincalrec.intAmount, 0)) {
			zrdecplrec.amountIn.set(ibincalrec.intAmount);
			callRounding8000();
			ibincalrec.intAmount.set(zrdecplrec.amountOut);
		}

		wsaaIntOnFundBal.set(ibincalrec.intAmount);
		
		/*    Now we calculate the interest due on all hitrIO records which
		 *    have been completed by Unit Deal, but which have not yet
		 *    been allocated interest for the current period.
		 *   */
		wsaaIntOnFundAmt.set(ZERO);
		wsaaFundAmount.set(ZERO);
		hitrintIO.setParams(SPACES);
		hitrintIO.chdrcoy.set(hitddryIO.chdrcoy);
		hitrintIO.chdrnum.set(hitddryIO.chdrnum);
		hitrintIO.life.set(hitddryIO.life);
		hitrintIO.coverage.set(hitddryIO.coverage);
		hitrintIO.rider.set(hitddryIO.rider);
		hitrintIO.planSuffix.set(hitddryIO.planSuffix);
		hitrintIO.zintbfnd.set(hitddryIO.zintbfnd);
		hitrintIO.effdate.set(wsaaZnxtintdte);
		wsaaInterestDate.set(wsaaZnxtintdte);
		hitrintIO.setFormat(hitrintrec);
		hitrintIO.setFunction(Varcom.begn);
		hitrintIO.setSelectStatementType(SmartFileCode.SELECT_FIT);
		hitrintIO.setFitKeysSearch("CHDRCOY", "CHDRNUM", "LIFE", "COVERAGE", "RIDER", "PLNSFX","ZINTBFND");
		while ( !(isEQ(hitrintIO.getStatuz(), Varcom.endp))) {
			readHitr2400();
		}
	
		compute(wsaaInterestTot, 4).set(add(wsaaIntOnFundBal,wsaaIntOnFundAmt));
			
		if(isGT(wsaaInterestTot,0)){
			writeHitr2500();
		}
		updateHitd2600();
	}

	protected void readHitr2400(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					hitr2410();
				case skipInterest2450:
					skipInterest2450();
				case nextHitr2480:
					nextHitr2480();
				case exit2490: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
		
	}
	
	protected void hitr2410(){
		SmartFileCode.execute(appVars, hitrintIO);

		if(!isEQ(hitrintIO.getStatuz(),Varcom.oK)&&!isEQ(hitrintIO.getStatuz(),Varcom.endp)){
			drylogrec.statuz.set(hitrintIO.getStatuz());
			drylogrec.params.set(hitrintIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(!isEQ(hitrintIO.chdrcoy,hitddryIO.chdrcoy)||!isEQ(hitrintIO.chdrnum,hitddryIO.chdrnum)||!isEQ(hitrintIO.life,hitddryIO.life)||!isEQ(hitrintIO.coverage,hitddryIO.coverage)||!isEQ(hitrintIO.rider,hitddryIO.rider)||!isEQ(hitrintIO.planSuffix,hitddryIO.planSuffix)||!isEQ(hitrintIO.zintbfnd,hitddryIO.zintbfnd)||isEQ(hitrintIO.statuz,Varcom.endp)){
			hitrintIO.statuz.set(Varcom.endp);
			goTo(GotoLabel.exit2490);
		}
		
		/*    If the interest allocation field is set to 'N', then
		  *    interest allocation is not required for that particular
		  *    transaction. However, we must still include the amount
		  *    of the hitrIO when updating the hitdIO record with the latest
		  *    fund balance.
		  *
		  *    Note: there is no interest allocation for the following
		  *    transactions:
		  *
		  *    1. Fund switching (out only)
		  *    2. Reversal of fund switch (in and out)
		  *    3. Surrenders and claims
		  *    4. Reversal of surrenders and claims
		  *    5. Debt recovery
		  */

		if(isEQ(hitrintIO.zintalloc,"N")){
			ibincalrec.intEffdate.set(varcom.vrcmMaxDate);
			ibincalrec.intRate.set(ZERO);
			goTo(GotoLabel.skipInterest2450);
		}

		/* If the effective date on the hitrIO is the same as the current
		  * interest date, then this record is not due for processing yet.
		  */

		if(isEQ(hitrintIO.effdate,wsaaInterestDate)){
			goTo(GotoLabel.nextHitr2480);
		}
		
		ibincalrec.ibincalRec.set(SPACES);
		ibincalrec.company.set(drypDryprcRecInner.drypCompany);
		ibincalrec.lastIntApp.set(hitrintIO.effdate);
		ibincalrec.nextIntDue.set(wsaaInterestDate);
		ibincalrec.capAmount.set(hitrintIO.fundAmount);
		ibincalrec.intAmount.set(ZERO);
		ibincalrec.intRate.set(ZERO);
		ibincalrec.fund.set(hitddryIO.zintbfnd);
		ibincalrec.crrcd.set(covrIO.getCrrcd());
		
		
		//IJTI-1758 starts
		// Interest Bearing calculation is not working when VPMS is on so corrected it by comparing it with Bh512 
		if (!(AppVars.getInstance().getAppConfig().isVpmsEnable()
				&& ExternalisedRules.isCallExternal(th510rec.zintcalc.toString()))) {
			callProgramX(th510rec.zintcalc, ibincalrec.ibincalRec);
		} else {
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			Vpxibinrec vpxibinrec = new Vpxibinrec();

			vpmcalcrec.linkageArea.set(ibincalrec.ibincalRec);
			vpxibinrec.function.set("INIT");
			callProgram(Vpxibin.class, vpmcalcrec.vpmcalcRec, vpxibinrec);
			ibincalrec.ibincalRec.set(vpmcalcrec.linkageArea);
			ibincalrec.cnttype.set(chdrlifIO.getCnttype());
			ibincalrec.crtable.set(covrIO.getCrtable());
			callProgram(th510rec.zintcalc, ibincalrec.ibincalRec);
		}
		// IJTI-1758 ends
		if(!isEQ(ibincalrec.statuz,Varcom.oK)){
			drylogrec.params.set(ibincalrec.ibincalRec);
			drylogrec.statuz.set(ibincalrec.statuz);
			drylogrec.drySubrtnError.setTrue();
			a000FatalError();
		}
		if (isNE(ibincalrec.intAmount, 0)) {
			zrdecplrec.amountIn.set(ibincalrec.intAmount);
			callRounding8000();
			ibincalrec.intAmount.set(zrdecplrec.amountOut);
		}
		wsaaIntOnFundAmt.add(ibincalrec.intAmount);
	}
	
	protected void skipInterest2450(){
		wsaaFundAmount.add(hitrintIO.getFundAmount());
		hitrIO.setParams(SPACES);
		hitrIO.setRrn(hitrintIO.getRrn());
		hitrIO.setFormat(hitrrec);
		hitrIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, hitrIO);

		if(!isEQ(hitrIO.statuz,Varcom.oK)){
			drylogrec.statuz.set(hitrIO.getStatuz());
			drylogrec.params.set(hitrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		hitrIO.zintappind.set("Y");
		hitrIO.zlstintdte.set(wsaaInterestDate);
		hitrIO.zinteffdt.set(ibincalrec.intEffdate);
		hitrIO.zintrate.set(ibincalrec.intRate);
		hitrIO.setFunction(Varcom.writd);
		SmartFileCode.execute(appVars, hitrIO);

		if(!isEQ(hitrIO.statuz,Varcom.oK)){
			drylogrec.statuz.set(hitrIO.getStatuz());
			drylogrec.params.set(hitrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}
	
	protected void nextHitr2480(){
		hitrintIO.setFunction(varcom.nextr);
	}

	protected void writeHitr2500(){

		hitrIO.setParams(SPACES);
		hitrIO.tranno.set(ZERO);
		hitrIO.ustmno.set(ZERO);
		hitrIO.planSuffix.set(ZERO);
		hitrIO.fundRate.set(ZERO);
		hitrIO.inciNum.set(ZERO);
		hitrIO.inciPerd01.set(ZERO);
		hitrIO.inciPerd02.set(ZERO);
		hitrIO.inciprm01.set(ZERO);
		hitrIO.inciprm02.set(ZERO);
		hitrIO.contractAmount.set(ZERO);
		hitrIO.fundAmount.set(ZERO);
		hitrIO.procSeqNo.set(ZERO);
		hitrIO.svp.set(ZERO);
		hitrIO.surrenderPercent.set(ZERO);
		hitrIO.chdrcoy.set(drypDryprcRecInner.drypCompany);
		hitrIO.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		hitrIO.batccoy.set(drypDryprcRecInner.drypBatccoy);
		hitrIO.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		hitrIO.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		hitrIO.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		hitrIO.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		hitrIO.chdrnum.set(hitddryIO.getChdrnum());
		hitrIO.planSuffix.set(hitddryIO.getPlanSuffix());
		hitrIO.life.set(hitddryIO.getLife());
		hitrIO.coverage.set(hitddryIO.getCoverage());
		hitrIO.rider.set(hitddryIO.getRider());
		hitrIO.crtable.set(covrIO.crtable);
		hitrIO.procSeqNo.set(t6647rec.procSeqNo);
		hitrIO.fundAmount.set(0);
		hitrIO.zintbfnd.set(hitddryIO.getZintbfnd());
		hitrIO.cnttyp.set(chdrlifIO.getCnttype());

		if(componLevelAccounted.isTrue()){
			hitrIO.sacscode.set(t5645rec.sacscode02);
			hitrIO.sacstyp.set(t5645rec.sacstype02);
			hitrIO.genlcde.set(t5645rec.glmap02);
			}else{
				hitrIO.sacscode.set(t5645rec.sacscode01);
				hitrIO.sacstyp.set(t5645rec.sacstype01);
				hitrIO.genlcde.set(t5645rec.glmap01);
			}
		hitrIO.tranno.set(wsaaTranno);
		hitrIO.svp.set(1);
		hitrIO.zrectyp.set("I");
		
		/*    Read Table T5515 to find the Fund currency. */
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(hitrIO.getChdrcoy());
		itemIO.setItemtabl(t5515);
		itemIO.setItemitem(hitrIO.getZintbfnd());
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);

		if(!isEQ(itemIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5515rec.t5515Rec.set(itemIO.getGenarea());
		hitrIO.fundCurrency.set(t5515rec.currcode);
		hitrIO.cntcurr.set(chdrlifIO.getCntcurr());
		hitrIO.effdate.set(wsaaInterestDate);
		hitrIO.zlstintdte.set(varcom.vrcmMaxDate);
		hitrIO.zinteffdt.set(varcom.vrcmMaxDate);
		hitrIO.zintappind.set("Y");
		hitrIO.zintalloc.set("N");
		hitrIO.zintrate.set(ZERO);
		hitrIO.zlstintdte.set(wsaaInterestDate);
		hitrIO.zinteffdt.set(ibincalrec.intEffdate);
		hitrIO.zintrate.set(ibincalrec.intRate);
		wsaaIntRound.setRounded(wsaaInterestTot);
		hitrIO.fundAmount.set(wsaaIntRound);

		if(isEQ(wsaaIntRound,0)){
			return;
		}

		hitrIO.zlstintdte.set(wsaaInterestDate);
		hitrIO.zinteffdt.set(ibincalrec.intEffdate);
		hitrIO.zintrate.set(ibincalrec.intRate);
		hitrIO.setFormat(hitrrec);
		hitrIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, hitrIO);

		if(!isEQ(hitrIO.statuz,Varcom.oK)){
			drylogrec.statuz.set(hitrIO.getStatuz());
			drylogrec.params.set(hitrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	
	}
	
	protected void updateHitd2600(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					hitd2610();
				case updateHitd2680:
					updateHitd2680();
				case calc2680:
					calc2680();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	
	}
	
	protected void hitd2610(){
		hitdIO.setParams(SPACES);
		hitdIO.chdrcoy.set(hitddryIO.getChdrcoy());
		hitdIO.chdrnum.set(hitddryIO.getChdrnum());
		hitdIO.life.set(hitddryIO.getLife());
		hitdIO.coverage.set(hitddryIO.getCoverage());
		hitdIO.rider.set(hitddryIO.getRider());
		hitdIO.planSuffix.set(hitddryIO.getPlanSuffix());
		hitdIO.zintbfnd.set(hitddryIO.getZintbfnd());
		hitdIO.setFunction(varcom.readh);
		hitdIO.setFormat(hitdrec);
		SmartFileCode.execute(appVars, hitdIO);

		if(!isEQ(hitdIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(hitdIO.getStatuz());
			drylogrec.params.set(hitdIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(isEQ(hitdIO.getTranno(),wsaaTranno)){
			hitdIO.setFunction(Varcom.rewrt);
			goTo(GotoLabel.updateHitd2680);
		}
		hitdIO.validflag.set("2");
		hitdIO.setFunction(Varcom.rewrt);
		SmartFileCode.execute(appVars, hitdIO);

		if(!isEQ(hitdIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(hitdIO.getStatuz());
			drylogrec.params.set(hitdIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		hitdIO.setFunction(Varcom.writr);
	}
	
	protected void updateHitd2680(){
		/*
	      * If O/S hitrIO is one of the transaction defined on T6626, no
	      * more interest in future.
	      */
		if(isEQ(wsaaTriggerFound,"Y")&&!isEQ(wsaaBatctrcde,SPACE)){
			wsaaNoMoreInt.set("N");

			for (wsaaSub.set(1); !(isGT(wsaaSub, 10)); wsaaSub.add(1)){
				if(isEQ(wsaaBatctrcde,t6626rec.trncd[wsaaSub.toInt()])){
					wsaaNoMoreInt.set("Y");
					wsaaSub.set(11);
			}
		}

		if(isEQ(wsaaNoMoreInt,'Y')){
			hitdIO.znxtintdte.set(varcom.vrcmMaxDate);
			goTo(GotoLabel.calc2680);
		}
	}

		getNextFreq2700();
		hitdIO.znxtintdte.set(datcon4rec.intDate2);
	}
	
	protected void calc2680(){
		wsaaIntRound.setRounded(wsaaInterestTot);

		if(isLT(wsaaIntRound,0)){
			setPrecision(hitddryIO.getZlstfndval(), 2);
			hitdIO.setZlstfndval(add(wsaaZlstfndval,wsaaFundAmount));
			
		}else{
			setPrecision(hitddryIO.getZlstfndval(), 2);
			hitdIO.setZlstfndval(add(wsaaIntRound,add(wsaaZlstfndval, wsaaFundAmount)));
		}	
		hitdIO.zlstintdte.set(wsaaZnxtintdte);
		hitdIO.tranno.set(wsaaTranno);
		hitdIO.effdate.set(drypDryprcRecInner.drypRunDate);
		hitdIO.validflag.set(1);
		SmartFileCode.execute(appVars, hitdIO);

		if(!isEQ(hitdIO.statuz,Varcom.oK)){
			drylogrec.statuz.set(hitdIO.getStatuz());
			drylogrec.params.set(hitdIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaZnxtintdte.set(hitdIO.getZnxtintdte());
		wsaaZlstintdte.set(hitdIO.getZlstintdte());
		wsaaZlstfndval.set(hitdIO.getZlstfndval());
	}

	protected void getNextFreq2700(){
		next2710();
	}
	
	protected void next2710(){
//		datcon2rec.datcon2Rec.set(SPACES);
//		datcon2rec.intDate1.set(hitddryIO.getZlstintdte());
//		datcon2rec.freqFactor.set(1);
//		datcon2rec.frequency.set(th510rec.zintalofrq);
//		callProgram(Datcon2.class,datcon2rec);
//
//		if(!isEQ(datcon2rec.statuz,Varcom.oK)){
//			drylogrec.params.set(datcon2rec.datcon2Rec);
//			drylogrec.statuz.set(datcon2rec.statuz);
//			drylogrec.drySubrtnError.setTrue();
//			a000FatalError();
//		}
//		
//		if(!isEQ(th510rec.zintfixdd,ZERO)){
//			datcon2rec.intDate2.setSub1String(5, 2, th510rec.zintfixdd); 
//		}
		datcon4rec.datcon4Rec.set(SPACES);
		datcon4rec.intDate1.set(wsaaZnxtintdte);
		datcon4rec.freqFactor.set(1);
		datcon4rec.frequency.set(th510rec.zintalofrq);
		datcon4rec.intDate2.set(0);
		datcon4rec.billdayNum.set(subString(datcon4rec.intDate1, 7, 2));
		datcon4rec.billmonthNum.set(subString(datcon4rec.intDate1, 5, 2));
		callProgram(Datcon4.class, datcon4rec.datcon4Rec);
		if (isNE(datcon4rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon4rec.datcon4Rec);
			drylogrec.statuz.set(datcon4rec.statuz);
			drylogrec.drySubrtnError.setTrue();
			a000FatalError();
		}
		if (!isEQ(th510rec.zintfixdd,ZERO)) {
			datcon4rec.intDate2.setSub1String(7, 2, th510rec.zintfixdd);
		}
	}


	protected void updates2800(){
		chdrptrn2810();
	}
	
	protected void chdrptrn2810(){
		
		checkTransaction300(); //IJTI-1761
		chdrlifIO.setValidflag(2);
		chdrlifIO.setCurrto(wsaaZnxtintdte);
		chdrlifIO.setFunction(Varcom.writd);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);

		if(!isEQ(chdrlifIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		chdrlifIO.setValidflag(1);
		chdrlifIO.setCurrfrom(wsaaZnxtintdte);
		chdrlifIO.setCurrto(varcom.vrcmMaxDate);
		setPrecision(chdrlifIO.getTranno(), 0);
		chdrlifIO.tranno.add(1);
		wsaaTranno.set(chdrlifIO.getTranno());
		chdrlifIO.setFunction(Varcom.writr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);

		if(!isEQ(chdrlifIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		
		ptrnIO.setParams(SPACES);
		ptrnIO.setUser(0);
		ptrnIO.setTermid(SPACES);
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBranch);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setPtrneff(wsaaZnxtintdte);
		ptrnIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		ptrnIO.setChdrnum(drypDryprcRecInner.drypEntity);
		ptrnIO.setTranno(chdrlifIO.getTranno());
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.transactionDate.set(getCobolDate());
		ptrnIO.transactionTime.set(getCobolTime());
		String username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(Varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);

		if(!isEQ(ptrnIO.getStatuz(),Varcom.oK)){
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}


	protected void dryProcessing3000(){
		dryProc3010();
	}
	
	protected void dryProc3010(){
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstatcode());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypAplsupto.set(chdrlifIO.getAplspto());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStatementDate());
	}
	
	/*
	 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner { 
		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypAplsupto = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 36);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
		private FixedLengthStringData drypDetailParams1 = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput1 = new FixedLengthStringData(71).isAPartOf(drypDetailParams1, 0);
		private FixedLengthStringData drypCnttype1 = new FixedLengthStringData(3).isAPartOf(drypDetailInput1, 0);
		private FixedLengthStringData drypBillfreq1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 3);
		private FixedLengthStringData drypBillchnl1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 5);
		private FixedLengthStringData drypStatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 7);
		private FixedLengthStringData drypPstatcode1 = new FixedLengthStringData(2).isAPartOf(drypDetailInput1, 9);
		private PackedDecimalData drypBtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 11);
		private PackedDecimalData drypPtdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 16);
		private PackedDecimalData drypBillcd1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 21);
		private PackedDecimalData drypOccdate1 = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput1, 46);
	}

	
	protected void callRounding8000()
	{
		zrdecplrec.function.set(SPACES);
		zrdecplrec.company.set(drypDryprcRecInner.drypCompany);
		zrdecplrec.statuz.set(Varcom.oK);
		zrdecplrec.currency.set(hitrintIO.getFundCurrency());
		zrdecplrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		callProgram(Zrdecplc.class, zrdecplrec.zrdecplRec);
		if (isNE(zrdecplrec.statuz, Varcom.oK)) {
			drylogrec.params.set(zrdecplrec.zrdecplRec);
			drylogrec.statuz.set(zrdecplrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}
	
	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

}