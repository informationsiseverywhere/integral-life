/**
 * 
 */
package com.csc.life.diary.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.diary.dataaccess.dao.Dry5137DAO;
import com.csc.life.diary.dataaccess.model.Dry5137Dto;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * DAO Implementation for Dry5137
 * 
 * @author aashish4
 *
 */
public class Dry5137DAOImpl extends BaseDAOImpl<Dry5137Dto> implements Dry5137DAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(Dry5137DAOImpl.class);

	public Dry5137DAOImpl() {
		// Default constructor
	}

	@Override
	public List<Dry5137Dto> getActualAutoIncrease(String company, String effDate, String chdrNum, String validFlag) {

		StringBuilder qurery = new StringBuilder();

		qurery.append("SELECT  T02.LIFCNUM, T01.CHDRCOY, T01.CHDRNUM, T01.LIFE, T01.JLIFE, T01.COVERAGE, T01.RIDER, ");
		qurery.append("T01.PLNSFX, T01.CRRCD, T01.CRTABLE, T01.VALIDFLAG, T01.STATCODE, T01.PSTATCODE, ");
		qurery.append("T01.ANNVRY, T01.BASCMETH, T01.BASCPY, T01.RNWCPY, T01.SRVCPY, ");
		qurery.append("T01.NEWINST, T01.LASTINST, T01.NEWSUM, T01.ZBNEWINST, T01.ZBLASTINST, T01.ZLNEWINST, ");
		qurery.append("T01.ZLLASTINST FROM INCRPF AS T01 INNER JOIN LIFEPF AS T02 ON T01.CHDRNUM = T02.CHDRNUM ");
		qurery.append("WHERE T01.CHDRCOY = ? AND T01.CRRCD <> ? AND T01.CRRCD <= ? AND T01.CHDRNUM = ? ");
		qurery.append("AND T01.VALIDFLAG = ? AND T02.VALIDFLAG = ? AND T02.LIFE = ? AND T02.JLIFE = ? ");
		qurery.append(" ORDER BY T02.LIFCNUM, T01.CHDRCOY, T01.CHDRNUM, T01.LIFE, T01.COVERAGE, T01.RIDER, T01.PLNSFX");
		List<Dry5137Dto> dry5137list = new ArrayList<>();
		try (PreparedStatement preparedStatement = getPrepareStatement(qurery.toString())) {
			preparedStatement.setString(1, company);
			preparedStatement.setString(2, "0");
			preparedStatement.setString(3, effDate);
			preparedStatement.setString(4, chdrNum);
			preparedStatement.setString(5, validFlag);
			preparedStatement.setString(6, validFlag);
			preparedStatement.setString(7, "01");
			preparedStatement.setString(8, "00");
			try (ResultSet resultSet = preparedStatement.executeQuery()) {
				while (resultSet.next()) {
					Dry5137Dto dry5137dto = new Dry5137Dto();
					dry5137dto.setLifcnum(resultSet.getString("LIFCNUM"));
					dry5137dto.setChdrcoy(resultSet.getString("CHDRCOY"));
					dry5137dto.setChdrnum(resultSet.getString("CHDRNUM"));
					dry5137dto.setLife(resultSet.getString("LIFE"));
					dry5137dto.setJlife(resultSet.getString("JLIFE"));
					dry5137dto.setCoverage(resultSet.getString("COVERAGE"));
					dry5137dto.setRider(resultSet.getString("RIDER"));
					dry5137dto.setPlnsfx(resultSet.getInt("PLNSFX"));
					dry5137dto.setCrrcd(resultSet.getInt("CRRCD"));
					dry5137dto.setCrtable(resultSet.getString("CRTABLE"));
					dry5137dto.setValidflag(resultSet.getString("VALIDFLAG"));
					dry5137dto.setStatcode(resultSet.getString("STATCODE"));
					dry5137dto.setPstatcode(resultSet.getString("PSTATCODE"));
					dry5137dto.setAnnvry(resultSet.getString("ANNVRY"));
					dry5137dto.setBascmeth(resultSet.getString("BASCMETH"));
					dry5137dto.setBascpy(resultSet.getString("BASCPY"));
					dry5137dto.setRnwcpy(resultSet.getString("RNWCPY"));
					dry5137dto.setSrvcpy(resultSet.getString("SRVCPY"));
					dry5137dto.setNewinst(resultSet.getBigDecimal("NEWINST"));
					dry5137dto.setLastInst(resultSet.getBigDecimal("LASTINST"));
					dry5137dto.setNewsum(resultSet.getBigDecimal("NEWSUM"));
					dry5137dto.setZbnewinst(resultSet.getBigDecimal("ZBNEWINST"));
					dry5137dto.setZblastinst(resultSet.getBigDecimal("ZBLASTINST"));
					dry5137dto.setZlnewinst(resultSet.getBigDecimal("ZLNEWINST"));
					dry5137dto.setZllastinst(resultSet.getBigDecimal("ZLLASTINST"));

					dry5137list.add(dry5137dto);
				}
			}
			return dry5137list;
		} catch (SQLException e) {
			LOGGER.error("Error occurred when reading INCRPF and LIFEPF data", e);
			throw new SQLRuntimeException(e);
		}
	}
}
