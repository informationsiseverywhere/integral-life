package com.csc.life.diary.dataaccess.dao;

import java.util.List;

import com.csc.life.diary.dataaccess.model.Zraepf;
import com.csc.smart400framework.dataaccess.dao.BaseDAO;

public interface ZraepfDAO  extends BaseDAO<Zraepf>{

	public List<Zraepf> getZraeData(Zraepf zraepf);
	
	public boolean updateZrae(List<Zraepf> zraeBulkOpList) ;
	
	public boolean updateZraeRecIntDate(List<Zraepf> zraeBulkOpList) ;
	
	public boolean updateZraeRecCapDate(List<Zraepf> zraeBulkOpList) ;
	
	public List<Zraepf> searchZraepfRecord(List<String> chdrnumList, String coy) ;
	
	public Zraepf getAllItem(String chdrnum);
	
	public List<Zraepf> getAllData(String chdrnum);
	
	public Zraepf getItemByContractNum(String chdrnum);
	
	public boolean updateZraeWithBankDetails(List<Zraepf> zraeBulkOpList) ;
	
	public boolean updateValidflag(List<Zraepf> zraeBulkOpList);
	public void insertZraeRecord(Zraepf zraepf);

	
}
