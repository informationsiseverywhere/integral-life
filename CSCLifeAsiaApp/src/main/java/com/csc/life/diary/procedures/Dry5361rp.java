/*
 * File: Dry5361rp.java
 * Date: December 3, 2013 2:22:21 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5361RP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DrptsrtTableDAM;
import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.flexiblepremium.reports.R5361Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This subroutine will create the R5361 report, listing
* any FPCO record which did not meet it's target premium.
*
* The report is generated using the records stored for the
* R5361 report type in the diary report file DRPT. The
* DRPTSRT logical is used to read through these records
* because a sort key has been used : CHDRNUM, COVERAGE.
*
* The report will be created using the report file R5361
* the only changes to this report has been to include
* an INDARA.
*
* This subroutine has been built as a replacement for B6686
* and is only to be used in the batch diary system.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5361rp extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5361Report printFile = new R5361Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5361RP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0);
		/* TABLES */
	private static final String t1693 = "T1693";
		/* FORMATS */
	private static final String drptsrtrec = "DRPTSRTREC";
	private static final String descrec = "DESCREC";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5361H01Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5361h01O = new FixedLengthStringData(31).isAPartOf(r5361H01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5361h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5361h01O, 1);

	private FixedLengthStringData r5361T01Record = new FixedLengthStringData(2);
	private DescTableDAM descIO = new DescTableDAM();
	private DrptsrtTableDAM drptsrtIO = new DrptsrtTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Dryoutrec dryoutrec = new Dryoutrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private R5361D01RecordInner r5361D01RecordInner = new R5361D01RecordInner();

	public Dry5361rp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryoutrec.dryoutRec = convertAndSetParam(dryoutrec.dryoutRec, parmArray, 0);
		try {
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
		dryoutrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		wsaaOverflow.set("Y");
		printFile.openOutput();
		/* Set up today's date...*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Set up the company description..*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(dryoutrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		companynm.set(descIO.getLongdesc());
		company.set(dryoutrec.company);
		/* Set up the DRPTSRT ready to read...*/
		drptsrtIO.setRecKeyData(SPACES);
		drptsrtIO.setStatuz(varcom.oK);
		drptsrtIO.setEffdate(dryoutrec.effectiveDate);
		drptsrtIO.setDiaryEntityCompany(dryoutrec.company);
		drptsrtIO.setDiaryEntityBranch(dryoutrec.branch);
		drptsrtIO.setDiaryReportName(dryoutrec.reportName);
		drptsrtIO.setDiaryRunNumber(dryoutrec.runNumber);
		drptsrtIO.setSortkey(SPACES);
		drptsrtIO.setFormat(drptsrtrec);
		drptsrtIO.setFunction(varcom.begn);
		/* Loop through each DRPTSRT recordS*/
		while ( !(isEQ(drptsrtIO.getStatuz(), varcom.endp))) {
			callDrpt200();
		}
		
		endReport500();
		printFile.close();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void callDrpt200()
	{
		call210();
	}

protected void call210()
	{
		SmartFileCode.execute(appVars, drptsrtIO);
		if (isNE(drptsrtIO.getStatuz(), varcom.oK)
		&& isNE(drptsrtIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(drptsrtIO.getParams());
			drylogrec.statuz.set(drptsrtIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		if (isEQ(drptsrtIO.getStatuz(), varcom.endp)
		|| isNE(drptsrtIO.getDiaryEntityCompany(), dryoutrec.company)
		|| isNE(drptsrtIO.getDiaryEntityBranch(), dryoutrec.branch)
		|| isNE(drptsrtIO.getDiaryReportName(), dryoutrec.reportName)) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(dryoutrec.runNumber, ZERO)
		&& isNE(dryoutrec.runNumber, drptsrtIO.getDiaryRunNumber())) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		dryrDryrptRecInner.r5361SortKey.set(drptsrtIO.getSortkey());
		writeLine300();
		drptsrtIO.setFunction(varcom.nextr);
	}

protected void writeLine300()
	{
		write310();
	}

protected void write310()
	{
		/* Write New Page if required making sure the correct VRTFND*/
		/* is written to the header record*/
		if (pageOverflow.isTrue()) {
			wsaaOverflow.set("N");
			newPage400();
		}
		/* Fill the detail record.*/
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		r5361D01RecordInner.chdrnum.set(dryrDryrptRecInner.r5361Chdrnum);
		r5361D01RecordInner.life.set(dryrDryrptRecInner.r5361Life);
		r5361D01RecordInner.coverage.set(dryrDryrptRecInner.r5361Coverage);
		r5361D01RecordInner.plnsfx.set(dryrDryrptRecInner.r5361PlanSuffix);
		r5361D01RecordInner.prmper.set(dryrDryrptRecInner.r5361TargetPremium);
		r5361D01RecordInner.prmrcdp.set(dryrDryrptRecInner.r5361PremRecPer);
		r5361D01RecordInner.billedp.set(dryrDryrptRecInner.r5361BilledInPeriod);
		r5361D01RecordInner.ovrminreq.set(dryrDryrptRecInner.r5361OverdueMin);
		/* format the dates*/
		datcon1rec.intDate.set(dryrDryrptRecInner.r5361Targfrom);
		convDate600();
		r5361D01RecordInner.currfrom.set(datcon1rec.extDate);
		datcon1rec.intDate.set(dryrDryrptRecInner.r5361Targto);
		convDate600();
		r5361D01RecordInner.currto.set(datcon1rec.extDate);
		/* Write the detail line to the report ....*/
		printRecord.set(SPACES);
		printFile.printR5361d01(r5361D01RecordInner.r5361D01Record, indicArea);
	}

protected void newPage400()
	{
		/*NEW*/
		/*  Write the first set of header details.....*/
		printRecord.set(SPACES);
		printFile.printR5361h01(r5361H01Record, indicArea);
		/*EXIT*/
	}

protected void endReport500()
	{
		/*NEW*/
		/* Write New Page if required.*/
		if (pageOverflow.isTrue()) {
			wsaaOverflow.set("N");
			newPage400();
		}
		/*  Write the end of report line*/
		printFile.printR5361t01(r5361T01Record, indicArea);
		/*EXIT*/
	}

protected void convDate600()
	{
		/*DATES*/
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.drySystemError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		/*EXIT*/
	}
//Modify for ILPI-65 
protected void fatalError()
	{
		/*A010-FATAL*/
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		drylogrec.runNumber.set(dryoutrec.runNumber);
		drylogrec.entityKey.set(SPACES);
		dryoutrec.statuz.set(drylogrec.statuz);
		//callProgram(Drylog.class, drylogrec.drylogRec);
		/*A090-EXIT*/
		//exitProgram();
		a000FatalError();
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r5361DataArea = new FixedLengthStringData(440).isAPartOf(dryrGenarea, 0, REDEFINE);
	private FixedLengthStringData r5361Life = new FixedLengthStringData(2).isAPartOf(r5361DataArea, 0);
	private ZonedDecimalData r5361PlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(r5361DataArea, 2);
	private ZonedDecimalData r5361Targfrom = new ZonedDecimalData(8, 0).isAPartOf(r5361DataArea, 6);
	private ZonedDecimalData r5361Targto = new ZonedDecimalData(8, 0).isAPartOf(r5361DataArea, 14);
	private ZonedDecimalData r5361TargetPremium = new ZonedDecimalData(17, 2).isAPartOf(r5361DataArea, 22);
	private ZonedDecimalData r5361PremRecPer = new ZonedDecimalData(17, 2).isAPartOf(r5361DataArea, 39);
	private ZonedDecimalData r5361BilledInPeriod = new ZonedDecimalData(17, 2).isAPartOf(r5361DataArea, 56);
	private ZonedDecimalData r5361OverdueMin = new ZonedDecimalData(17, 2).isAPartOf(r5361DataArea, 73);

	private FixedLengthStringData r5361SortKey = new FixedLengthStringData(38);
	private FixedLengthStringData r5361Chdrnum = new FixedLengthStringData(8).isAPartOf(r5361SortKey, 0);
	private FixedLengthStringData r5361Coverage = new FixedLengthStringData(2).isAPartOf(r5361SortKey, 8);
}
/*
 * Class transformed  from Data Structure R5361-D01-RECORD--INNER
 */
private static final class R5361D01RecordInner { 

	private FixedLengthStringData r5361D01Record = new FixedLengthStringData(104);
	private FixedLengthStringData r5361d01O = new FixedLengthStringData(104).isAPartOf(r5361D01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5361d01O, 0);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5361d01O, 8);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5361d01O, 10);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5361d01O, 12);
	private FixedLengthStringData currfrom = new FixedLengthStringData(10).isAPartOf(r5361d01O, 16);
	private FixedLengthStringData currto = new FixedLengthStringData(10).isAPartOf(r5361d01O, 26);
	private ZonedDecimalData prmper = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 36);
	private ZonedDecimalData prmrcdp = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 53);
	private ZonedDecimalData billedp = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 70);
	private ZonedDecimalData ovrminreq = new ZonedDecimalData(17, 2).isAPartOf(r5361d01O, 87);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
