package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;

import java.util.List;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/*
 *(c) Copyright Continuum Corporation Ltd.  1986....1995.
 *    All rights reserved.  Continuum Confidential.
 *
 *REMARKS.
 *
 * This is the transaction detail record change subroutine for
 * unit dealing transactions.
 *
 ***********************************************************************
 *                                                                     *
 * ......... New Version of the Amendment History.                     *
 *                                                                     *
 ***********************************************************************
 *           AMENDMENT  HISTORY                                        *
 ***********************************************************************
 * DATE.... VSN/MOD  WORK UNIT    BY....                               *
 *                                                                     *
 * 30/03/00  01/01   DRYAPL       Jacco Landskroon                     *
 *           Initial version.                                          *
 *                                                                     *
 * 16/02/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
 *           Retrofit.                                                 *
 *                                                                     *
 **DD/MM/YY*************************************************************
 */

public class Dryuntprc extends Maind {

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYUNTPRC");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	/* FORMATS */
	//ILPI-254 starts
	//	private static final String utrndryrec = "UTRNDRYREC";

	//	private Utrndrykey wsaaUtrndryKey = new Utrndrykey();
	//	private UtrndryTableDAM utrndryIO = new UtrndryTableDAM();
	//ILPI-254 ends

	private Varcom varcom = new Varcom();

	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	private Datcon2rec datcon2rec = new Datcon2rec();
	private Freqcpy freqcpy = new Freqcpy();

	//ILPI-254 starts
	private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);

	/**
	 * Contains all possible labels used by goTo action.
	 */
	//	private enum GotoLabel implements GOTOInterface {
	//		DEFAULT, 
	//		exit190
	//	}
	//ILPI-254 ends

	public Dryuntprc() {
		super();
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray){
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainline000();
		}
		catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void mainline000(){
		main010();
		exitProgram();
	}

	protected void main010() {		
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		//ILPI-254 starts
		//		checkUtrns100();
		drypDryprcRecInner.drypNxtprcdate.set(varcom.vrcmMaxDate);
		drypDryprcRecInner.drypNxtprctime.set(ZERO);
		Utrnpf utrnpfRead = new Utrnpf();
		utrnpfRead.setChdrcoy(drypDryprcRecInner.drypCompany.toString().trim());
		utrnpfRead.setChdrnum(drypDryprcRecInner.drypEntity.toString().trim());
		List<Utrnpf> utrnpfList = utrnpfDAO.readUtrnpf(utrnpfRead);
		for(Utrnpf utrnpf : utrnpfList){
			if (isGT(utrnpf.getMoniesDate(), drypDryprcRecInner.drypRunDate)) {
				datcon2rec.function.set(SPACE);
				datcon2rec.intDate1.set(utrnpf.getMoniesDate());
				datcon2rec.frequency.set(freqcpy.daily);
				datcon2rec.freqFactor.set(1);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				drypDryprcRecInner.drypNxtprcdate.set(datcon2rec.intDate2);
				drypDryprcRecInner.drypNxtprctime.set(ZERO);
			} else {
				datcon2rec.function.set(SPACE);
				datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
				datcon2rec.frequency.set(freqcpy.daily);
				datcon2rec.freqFactor.set(1);
				callProgram(Datcon2.class, datcon2rec.datcon2Rec);
				drypDryprcRecInner.drypNxtprcdate.set(datcon2rec.intDate2);
				drypDryprcRecInner.drypNxtprctime.set(ZERO);
			}
		}
		//ILPI-254 ends
	}

	//ILPI-254 starts
	//	protected void exit090(){
	//		exitProgram();
	//	}


	//	protected void checkUtrns100() {
	//		GotoLabel nextMethod = GotoLabel.DEFAULT;
	//		while (true) {
	//			try {
	//				switch (nextMethod) {
	//				case DEFAULT:
	//					check100();
	//				case exit190:
	//				}
	//				break;
	//			} catch (GOTOException e) {
	//				nextMethod = (GotoLabel) e.getNextMethod();
	//			}
	//		}
	//	}
	//	
	//
	//	protected void check100() {
	//		utrndryIO.setParams(SPACE);		
	//		utrndryIO.setRecKeyData(SPACE);
	//		utrndryIO.setRecNonKeyData(SPACE);
	//		utrndryIO.chdrcoy.set(drypDryprcRecInner.drypCompany);
	//		utrndryIO.chdrnum.set(drypDryprcRecInner.drypEntity);
	//		utrndryIO.moniesDate.set(ZERO);
	//		utrndryIO.setFormat(utrndryrec);
	//		utrndryIO.setFunction(varcom.begn);
	//		wsaaUtrndryKey.set(utrndryIO.getRecKeyData());
	//		SmartFileCode.execute(appVars, utrndryIO);
	//
	//		/*
	//		 * Abort if a database error has been found
	//		 */
	//		if (!isEQ(utrndryIO.statuz, varcom.oK)
	//				&& !isEQ(utrndryIO.statuz, varcom.endp)) {
	//			drylogrec.params.set(wsaaUtrndryKey);
	//			drylogrec.statuz.set(utrndryIO.getStatuz());
	//			drylogrec.dryDatabaseError.setTrue();
	//			a000FatalError();//ILPI-61
	//			
	//		}
	//		/*
	//		 * Where there are no matches, update the DTRD record setting the next
	//		 * processing date to max date. The next collection will update the DTRD
	//		 * record if a UTRN has been created during the collection process.
	//		 */
	//		if (!isEQ(utrndryIO.chdrcoy, drypDryprcRecInner.drypCompany)
	//				|| !isEQ(utrndryIO.chdrnum, drypDryprcRecInner.drypEntity)
	//				|| isEQ(utrndryIO.statuz, varcom.endp)) {
	//			
	//			drypDryprcRecInner.drypNxtprcdate.set(varcom.vrcmMaxDate);
	//			drypDryprcRecInner.drypNxtprctime.set(ZERO);
	//			goTo(GotoLabel.exit190);
	//		}
	//		/*
	//		 * An unprocessed UTRN exists for the contract... Check the
	//		 * UTRNDRY-MONIES-DATE against the diary run-date. The next processing
	//		 * date will be determine on these values.
	//		 */
	//		drypDryprcRecInner.drypNxtprcdate.set(add(utrndryIO.moniesDate, 1));
	//		drypDryprcRecInner.drypNxtprctime.set(ZERO);
	//
	//		/*
	//		 * If the UTRN has a monies date greater then the run date then update
	//		 * the DTRD record using the monies date.
	//		 */
	//		if (isGT(utrndryIO.moniesDate, drypDryprcRecInner.drypRunDate)) {
	//			datcon2rec.function.set(SPACE);
	//			datcon2rec.intDate1.set(utrndryIO.moniesDate);
	//			datcon2rec.frequency.set(freqcpy.daily);
	//			datcon2rec.freqFactor.set(1);
	//			callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	//			drypDryprcRecInner.drypNxtprcdate.set(datcon2rec.intDate2);
	//			drypDryprcRecInner.drypNxtprctime.set(ZERO);
	//			goTo(GotoLabel.exit190);
	//		}
	//		/*
	//		 * The UTRN monies date is less than or equal to the run date so we need
	//		 * to update the DTRD processing date to the run date + 1 day.
	//		 */
	//		datcon2rec.function.set(SPACE);
	//		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
	//		datcon2rec.frequency.set(freqcpy.daily);
	//		datcon2rec.freqFactor.set(1);
	//		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
	//		
	//		drypDryprcRecInner.drypNxtprcdate.set(datcon2rec.intDate2);
	//		drypDryprcRecInner.drypNxtprctime.set(ZERO);
	//		
	//	}



	//	protected void fatalError() {
	//		/* A010-FATAL */
	//		drylogrec.subrname.set(wsaaProg);
	//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
	//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
	//
	//		if (drypDryprcRecInner.onlineMode.isTrue()) {
	//			drylogrec.runNumber.set(ZERO);
	//		} else {
	//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
	//		}
	//
	//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
	//		drypDryprcRecInner.fatalError.setTrue();
	//
	//		callProgram(Drylog.class, drylogrec.drylogRec);
	//		a000FatalError();
	//		
	//	}

	//ILPI-254 ends

	private static final class DrypDryprcRecInner {
		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

}