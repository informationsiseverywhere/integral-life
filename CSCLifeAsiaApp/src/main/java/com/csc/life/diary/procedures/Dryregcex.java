/*
 * File: Dryregcex.java
 * Date: December 3, 2013 2:27:22 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYREGCEX.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.diary.dataaccess.RegcdteTableDAM;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
* This is the transaction detail record change subroutine to
* produce a Certificate of Existence.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryregcex extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYREGCEX");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT6693Key1 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6693Paystat1 = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 0);
	private FixedLengthStringData wsaaT6693Crtable1 = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key1, 2);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 6, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidTranCode = new FixedLengthStringData(1).init("N");
	private Validator validTranCode = new Validator(wsaaValidTranCode, "Y");

	private FixedLengthStringData wsaaRegcStatuz = new FixedLengthStringData(1).init("N");
	private Validator validRegc = new Validator(wsaaRegcStatuz, "Y");
	private PackedDecimalData index1 = new PackedDecimalData(3, 0);
	private ZonedDecimalData wsaaTempDtNum = new ZonedDecimalData(3, 0).init(0).setUnsigned();
	private ZonedDecimalData wsaaCertdate = new ZonedDecimalData(8, 0);

	private FixedLengthStringData wsaaT5655Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaBatctrcde = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 0);
	private FixedLengthStringData wsaaGeneric = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 4).init("****");
		/* TABLES */
	private static final String t5655 = "T5655";
	private static final String t6693 = "T6693";
		/* FORMATS */
	private static final String chdrrgprec = "CHDRRGPREC";
	private static final String itemrec = "ITEMREC";
	private static final String regcdterec = "REGCDTEREC";
		/* ERRORS */
	private static final String h038 = "H038";
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RegcdteTableDAM regcdteIO = new RegcdteTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private Datcon2rec datcon2rec = new Datcon2rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Freqcpy freqcpy = new Freqcpy();
	private T5655rec t5655rec = new T5655rec();
	private T6693rec t6693rec = new T6693rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr180, 
		exit190
	}

	public Dryregcex() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdNo.setTrue();
		wsaaRegcStatuz.set("N");
		/* Determine if a Certificate of Existence is needed.*/
		regcdteIO.setParams(SPACES);
		regcdteIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		regcdteIO.setChdrnum(drypDryprcRecInner.drypEntity);
		regcdteIO.setCertdate(ZERO);
		regcdteIO.setFormat(regcdterec);
		regcdteIO.setFunction(varcom.begn);
		regcdteIO.setStatuz(varcom.oK);
		while ( !(isEQ(regcdteIO.getStatuz(), varcom.endp)
		|| validRegc.isTrue())) {
			readRegc100();
		}
		
		if (validRegc.isTrue()) {
			drypDryprcRecInner.dtrdYes.setTrue();
			calculateDate400();
			drypDryprcRecInner.drypNxtprcdate.set(wsaaCertdate);
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		}
		else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readRegc100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read110();
				case nextr180: 
					nextr180();
				case exit190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read110()
	{
		SmartFileCode.execute(appVars, regcdteIO);
		if (isNE(regcdteIO.getStatuz(), varcom.oK)
		&& isNE(regcdteIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(regcdteIO.getParams());
			drylogrec.statuz.set(regcdteIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isNE(regcdteIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(regcdteIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isEQ(regcdteIO.getStatuz(), varcom.endp)) {
			regcdteIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit190);
		}
		if (isEQ(regcdteIO.getCertdate(), varcom.vrcmMaxDate)
		|| isEQ(regcdteIO.getCertdate(), ZERO)) {
			goTo(GotoLabel.nextr180);
		}
		readT6693300();
		if (!validTranCode.isTrue()) {
			goTo(GotoLabel.nextr180);
		}
		validRegc.setTrue();
	}

protected void nextr180()
	{
		regcdteIO.setFunction(varcom.nextr);
	}

protected void readT6693300()
	{
		start310();
	}

protected void start310()
	{
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrrgpIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrrgpIO.setFunction(varcom.readr);
		chdrrgpIO.setFormat(chdrrgprec);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrrgpIO.getStatuz());
			drylogrec.params.set(chdrrgpIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		/* Read T6693 to find allowable actions for current status.*/
		/* If actions for current status found, read through array of*/
		/* allowable transactions to determine if current transaction*/
		/* valid.*/
		wsaaValidTranCode.set("N");
		wsaaT6693Paystat1.set(regcdteIO.getRgpystat());
		wsaaT6693Crtable1.set(regcdteIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6693);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemitem(wsaaT6693Key1);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isNE(drypDryprcRecInner.drypCompany, itdmIO.getItemcoy())
		|| isNE(t6693, itdmIO.getItemtabl())
		|| isNE(wsaaT6693Key1, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaT6693Crtable1.set("****");
			itdmIO.setItemitem(wsaaT6693Key1);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				drylogrec.params.set(itdmIO.getParams());
				drylogrec.statuz.set(itdmIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61
			}
			if (isNE(drypDryprcRecInner.drypCompany, itdmIO.getItemcoy())
			|| isNE(t6693, itdmIO.getItemtabl())
			|| isNE(wsaaT6693Key1, itdmIO.getItemitem())
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				itdmIO.setStatuz(varcom.endp);
				itdmIO.setGenarea(SPACES);
				return ;
			}
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		for (index1.set(1); !(isGT(index1, 12)); index1.add(1)){
			if (isEQ(t6693rec.trcode[index1.toInt()], drypDryprcRecInner.drypSystParm[1])) {
				validTranCode.setTrue();
				index1.set(15);
			}
		}
	}

protected void calculateDate400()
	{
		start410();
	}

protected void start410()
	{
		getLeadDays500();
		datcon2rec.intDate1.set(regcdteIO.getCertdate());
		datcon2rec.frequency.set(freqcpy.daily);
		compute(datcon2rec.freqFactor, 0).set(mult(wsaaTempDtNum, -1));
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		wsaaCertdate.set(datcon2rec.intDate2);
	}

protected void getLeadDays500()
	{
		start510();
	}

protected void start510()
	{
		itdmIO.setDataArea(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5655);
		wsaaBatctrcde.set(drypDryprcRecInner.drypSystParm[1]);
		itdmIO.setItemitem(wsaaT5655Key);
		itdmIO.setItmfrm(regcdteIO.getCertdate());
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemtabl(), t5655)
		|| isNE(itdmIO.getItemitem(), wsaaT5655Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
			itdmIO.setItemtabl(t5655);
			itdmIO.setItemitem(wsaaT5655Key);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(h038);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		t5655rec.t5655Rec.set(itdmIO.getGenarea());
		wsaaTempDtNum.set(t5655rec.leadDays);
	}
//Modify for ILPI-61 
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
