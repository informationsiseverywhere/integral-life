/*
 * File: Dryflxcol.java
 * Date: March 26, 2014 3:04:53 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYFLXCOL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.flexiblepremium.dataaccess.FprmTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the transaction detail record change subroutine for
* Flexible Premium Collections.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryflxcol extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYFLXCOL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(2, 0).setUnsigned();
	private final int wsaaT5679Max = 12;

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");
	private FixedLengthStringData wsaaIssueTrcde = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSysParmTrcde3 = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSysParmTrcde4 = new FixedLengthStringData(4);
		/* ERRORS */
	private static final String f321 = "F321";
		/* FORMATS */
	private static final String itemrec = "ITEMREC";
	private static final String fprmrec = "FPRMREC";
		/* TABLES */
	private static final String t5679 = "T5679";
	private FprmTableDAM fprmIO = new FprmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private T5679rec t5679rec = new T5679rec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	public Dryflxcol() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		/* Determine if a Collection transaction is needed.*/
		validate100();
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61  
		}
		if (validStatus.isTrue()) {
			/*        PERFORM 400-READ-ACBLENQ                                 */
			checkFprm400();
		}
		else {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void validate100()
	{
		/*VALD*/
		/* Validate the Contract Risk and Premium status against T5679.*/
		readT5679200();
		validateStatus300();
		/*EXIT*/
	}

protected void readT5679200()
	{
		t5679210();
	}

protected void t5679210()
	{
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		wsaaIssueTrcde.set(drypDryprcRecInner.drypSystParm[2]);
		wsaaSysParmTrcde3.set(drypDryprcRecInner.drypSystParm[3]);
		wsaaSysParmTrcde4.set(drypDryprcRecInner.drypSystParm[4]);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61  
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61  
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void validateStatus300()
	{
		/*VALIDATE*/
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(drypDryprcRecInner.drypStatcode, t5679rec.cnRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(drypDryprcRecInner.drypPstatcode, t5679rec.cnPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}

	/**
	* <pre>
	*400-READ-ACBLENQ SECTION.                                        
	* </pre>
	*/
protected void checkFprm400()
	{
		read410();
	}

protected void read410()
	{
		/* Determine if a Collection transaction is needed.*/
		/*    INITIALIZE                     ACBLENQ-PARAMS.               */
		/*    MOVE DRYP-COMPANY           TO ACBLENQ-RLDGCOY.              */
		/*    MOVE DRYP-ENTITY            TO ACBLENQ-RLDGACCT.             */
		/*    MOVE 'LP'                   TO ACBLENQ-SACSCODE.             */
		/*    MOVE 'S'                    TO ACBLENQ-SACSTYP.              */
		/*    MOVE SPACES                 TO ACBLENQ-ORIGCURR.             */
		/*    MOVE BEGN                   TO ACBLENQ-FUNCTION.             */
		/*    MOVE ACBLENQREC             TO ACBLENQ-FORMAT.               */
		/*    CALL 'ACBLENQIO'         USING ACBLENQ-PARAMS.               */
		/*    IF  ACBLENQ-STATUZ       NOT = O-K                           */
		/*    AND ACBLENQ-STATUZ       NOT = ENDP                          */
		/*        MOVE ACBLENQ-STATUZ     TO DRYL-STATUZ                   */
		/*        MOVE ACBLENQ-PARAMS     TO DRYL-PARAMS                   */
		/*        SET DRY-DATABASE-ERROR  TO TRUE                          */
		/*        PERFORM A000-FATAL-ERROR                                 */
		/*    END-IF.                                                      */
		/*    IF  ACBLENQ-STATUZ           = ENDP                          */
		/*    OR  ACBLENQ-RLDGCOY      NOT = DRYP-COMPANY                  */
		/*    OR  ACBLENQ-RLDGACCT     NOT = DRYP-ENTITY                   */
		/*    OR  ACBLENQ-SACSCODE     NOT = 'LP'                          */
		/*    OR  ACBLENQ-SACSTYP      NOT = 'S'                           */
		/*    OR  ACBLENQ-SACSCURBAL       = ZEROES                        */
		/*        SET DTRD-NO             TO TRUE                          */
		/*        GO TO 490-EXIT                                           */
		/*    END-IF.                                                      */
		/* Set up key to read FPRM record for this contract.*/
		fprmIO.setParams(SPACES);
		fprmIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		fprmIO.setChdrnum(drypDryprcRecInner.drypEntity);
		fprmIO.setPayrseqno(ZERO);
		fprmIO.setFunction(varcom.begn);
		fprmIO.setFormat(fprmrec);
		SmartFileCode.execute(appVars, fprmIO);
		if (isNE(fprmIO.getStatuz(), varcom.oK)
		&& isNE(fprmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(fprmIO.getStatuz());
			drylogrec.params.set(fprmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61  
		}
		if (isEQ(fprmIO.getStatuz(), varcom.endp)
		|| isNE(fprmIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isNE(fprmIO.getChdrcoy(), drypDryprcRecInner.drypCompany)) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		if(isNE(wsaaSysParmTrcde3.trim(),SPACES) && isEQ(drypDryprcRecInner.drypBatctrcde.trim(),wsaaSysParmTrcde3))
		{
			drypDryprcRecInner.drypNxtprcdate.set(drypDryprcRecInner.drypPtdate);
		}
		else if(isNE(wsaaSysParmTrcde4.trim(),SPACES) && isEQ(drypDryprcRecInner.drypBatctrcde.trim(),wsaaSysParmTrcde4) )
		{
			drypDryprcRecInner.drypNxtprcdate.set(drypDryprcRecInner.drypBillcd);
		}
		else
		{
		/*    MOVE DRYP-RUN-DATE          TO DRYP-NXTPRCDATE.              */
		datcon2rec.frequency.set("DY");
		if (isLT(datcon1rec.intDate, drypDryprcRecInner.drypRunDate)) {
			datcon2rec.intDate1.set(datcon1rec.intDate);
		}
		else {
			datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		}
		datcon2rec.freqFactor.set(1);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(drypDryprcRecInner.drypBatctrcde, wsaaIssueTrcde)) {
			drypDryprcRecInner.drypNxtprcdate.set(datcon2rec.intDate2);
		}
		else {
			drypDryprcRecInner.drypNxtprcdate.set(drypDryprcRecInner.drypRunDate);
		}
		drypDryprcRecInner.drypNxtprctime.set(ZERO);
	}
	}
//Modify for ILPI-61 
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
	private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
