package com.csc.life.diary.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.diary.dataaccess.dao.Dry5351DAO;
import com.csc.life.diary.dataaccess.model.Dry5351Dto;
import com.csc.life.diary.dataaccess.model.Dryr619Dto;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * DAO Implementation To Fetch Records For Revenue (Due-Date) Accounting
 * 
 * @author ptrivedi8
 *
 */
public class Dry5351DAOImpl extends BaseDAOImpl<Dryr619Dto> implements Dry5351DAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Dry5351DAOImpl.class);

	/**
	 * Constructor
	 */
	public Dry5351DAOImpl() {
		// Calling default constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.csc.life.diary.dataaccess.dao.Dry5351DAO#getLinspfRecords(int,
	 * java.lang.String, java.lang.String)
	 */
	@Override
	public List<Dry5351Dto> getLinspfRecords(int effDate, String company, String chdrNum) {
		StringBuilder sql = new StringBuilder(
				"SELECT  CHDRCOY, CHDRNUM, PAYRSEQNO, INSTFROM FROM LINSPF WHERE ACCTMETH = ? AND PAYFLAG <> ?");
		sql.append(" AND DUEFLG <> ? AND VALIDFLAG = ? AND BILLCHNL <> ? AND INSTFROM <= ?");
		sql.append(" AND CHDRCOY = ? AND CHDRNUM = ? ORDER BY CHDRCOY, CHDRNUM, INSTFROM, PAYRSEQNO");

		List<Dry5351Dto> dry5351List = new ArrayList<>();
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, "R");
			ps.setString(2, "P");
			ps.setString(3, "Y");
			ps.setString(4, "1");
			ps.setString(5, "N");
			ps.setInt(6, effDate);
			ps.setString(7, company);
			ps.setString(8, chdrNum);
			rs = ps.executeQuery();
			while (rs.next()) {
				Dry5351Dto dry5351Dto = new Dry5351Dto();
				dry5351Dto.setChdrCoy(rs.getString("CHDRCOY"));
				dry5351Dto.setChdrNum(rs.getString("CHDRNUM"));
				dry5351Dto.setPayrSeqNo(rs.getInt("PAYRSEQNO"));
				dry5351Dto.setInstFrom(rs.getInt("INSTFROM"));

				dry5351List.add(dry5351Dto);
			}

		} catch (SQLException e) {
			LOGGER.error("Error occured when reading Linspf data", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return dry5351List;
	}

}
