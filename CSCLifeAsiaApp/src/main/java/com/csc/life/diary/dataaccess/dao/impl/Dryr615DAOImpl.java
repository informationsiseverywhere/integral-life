package com.csc.life.diary.dataaccess.dao.impl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.csc.life.diary.dataaccess.dao.Dryr615DAO;
import com.csc.life.diary.dataaccess.model.Dryr615Dto;
import com.csc.smart400framework.dataaccess.dao.impl.BaseDAOImpl;
import com.quipoz.framework.util.jdbc.SQLRuntimeException;

/**
 * DAO implementation to fetch records for Wavier of Premium.
 * 
 * @author hmahajan6
 *
 */
public class Dryr615DAOImpl extends BaseDAOImpl<Dryr615Dto> implements Dryr615DAO {

	private static final Logger LOGGER = LoggerFactory.getLogger(Dryr615DAOImpl.class);

	public Dryr615DAOImpl() {
		// constructor
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.csc.life.diary.dataaccess.dao.Dryr615DAO#getRerateWavierPremiumDetails(
	 * java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<Dryr615Dto> getRerateWavierPremiumDetails(String company, String chdrNum, String sqlDate) {
		List<Dryr615Dto> dryr615DtoList = new ArrayList<>();
		StringBuilder sql = new StringBuilder(
				" SELECT  CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX FROM COVRPF WHERE ");
		sql.append(" CHDRCOY = ? AND CHDRNUM = ? AND VALIDFLAG = ? AND RRTDAT <> ? AND ");
		sql.append(" RRTDAT < ? ORDER BY CHDRCOY, CHDRNUM, LIFE, COVERAGE, RIDER, PLNSFX ");
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			ps = getPrepareStatement(sql.toString());
			ps.setString(1, company);
			ps.setString(2, chdrNum);
			ps.setString(3, "1");
			ps.setString(4, "0");
			ps.setString(5, sqlDate);
			rs = ps.executeQuery();
			while (rs.next()) {
				Dryr615Dto dryr615Dto = new Dryr615Dto();
				dryr615Dto.setChdrcoy(rs.getString("CHDRCOY"));
				dryr615Dto.setChdrnum(rs.getString("CHDRNUM"));
				dryr615Dto.setLife(rs.getString("LIFE"));
				dryr615Dto.setCoverage(rs.getString("COVERAGE"));
				dryr615Dto.setRider(rs.getString("RIDER"));
				dryr615Dto.setPlanSuffix(rs.getInt("PLNSFX"));
				dryr615DtoList.add(dryr615Dto);
			}
		} catch (SQLException e) {
			LOGGER.error("Error occured while reading COVRPF", e);
			throw new SQLRuntimeException(e);
		} finally {
			close(ps, rs);
		}
		return dryr615DtoList;
	}

}
