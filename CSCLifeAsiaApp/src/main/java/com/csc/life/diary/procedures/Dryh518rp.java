/*
 * File: Dryh518rp.java
 * Date: November 23, 2015 12:00 AM
 * Author: sbatra9
 * 
 * Class transformed from DRYH518RP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;

import com.csc.diary.dataaccess.DrptTableDAM;
import com.csc.diary.dataaccess.DrptsrtTableDAM;
import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.tablestructures.T3629rec;
//import com.csc.life.diary.procedures.Dryh509.DrypDryprcRecInner;
import com.csc.life.interestbearing.reports.Rh518Report;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart.tablestructures.T1693rec;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryh518rp extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private Rh518Report printFile = new Rh518Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYH518RP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private PackedDecimalData wsaaTotFundAmount = new PackedDecimalData(15, 2);
	
	private FixedLengthStringData wsaaSortKey = new FixedLengthStringData(100);
	private FixedLengthStringData wsaaSortChdrcoy = new FixedLengthStringData(1);
	private FixedLengthStringData wsaaSortZintbfnd = new FixedLengthStringData(4);
	private FixedLengthStringData wsaaSortChdrnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaSortBatctrcde = new FixedLengthStringData(4);
	
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t1688 = "T1688";
	private static final String t5515 = "T5515";
	private static final String t3629 = "T3629";
		/* FORMATS */
	private static final String drptrec = "DRPTREC";
	private static final String descrec = "DESCREC";
	private static final String itemrec = "ITEMREC";
	private static final String drptsrtrec = "DRPTSRTREC";
	private Getdescrec getdescrec = new Getdescrec();
	
	private T5515rec t5515rec = new T5515rec();
	private T1693rec t1693rec = new T1693rec();
	private T3629rec t3629rec = new T3629rec();
	
	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData rh518H01Record = new FixedLengthStringData(116);
	private FixedLengthStringData rh518h01O = new FixedLengthStringData(116).isAPartOf(rh518H01Record, 0);
	private FixedLengthStringData bschednam = new FixedLengthStringData(10).isAPartOf(rh518h01O, 0);
	private FixedLengthStringData bschednum = new FixedLengthStringData(8).isAPartOf(rh518h01O, 10);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(rh518h01O, 18);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(rh518h01O, 19);
	private FixedLengthStringData zintbfnd = new FixedLengthStringData(4).isAPartOf(rh518h01O, 49);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(rh518h01O, 53);
	private FixedLengthStringData fndcurr = new FixedLengthStringData(3).isAPartOf(rh518h01O, 83);
	private FixedLengthStringData curdesc = new FixedLengthStringData(30).isAPartOf(rh518h01O, 86);
	
	private FixedLengthStringData rh518H02Record = new FixedLengthStringData(116);
	private FixedLengthStringData rh518h02O = new FixedLengthStringData(116).isAPartOf(rh518H02Record, 0);
	
	private FixedLengthStringData rh518T01Record = new FixedLengthStringData(73);
	private FixedLengthStringData rh518t01O = new FixedLengthStringData(73).isAPartOf(rh518T01Record, 0);
	private ZonedDecimalData totfndamt = new ZonedDecimalData(18,2).isAPartOf(rh518t01O, 0);
	
	private FixedLengthStringData rh518E01Record = new FixedLengthStringData(73);
	private FixedLengthStringData rh518e01O = new FixedLengthStringData(73).isAPartOf(rh518E01Record, 0);
	
	private DescTableDAM descIO = new DescTableDAM();
	private DrptTableDAM drptIO = new DrptTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private DrptsrtTableDAM drptsrtIO = new DrptsrtTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Dryoutrec dryoutrec = new Dryoutrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private Rh518D01RecordInner rh518D01RecordInner = new Rh518D01RecordInner();
	private Rh518N01RecordInner rh518N01RecordInner = new Rh518N01RecordInner();
	private Rh518H02RecordInner rh518H02RecordInner = new Rh518H02RecordInner();
	private Rh518E01RecordInner r5106E01RecordInner = new Rh518E01RecordInner();

	public Dryh518rp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryoutrec.dryoutRec = convertAndSetParam(dryoutrec.dryoutRec, parmArray, 0);
		try {
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
		printFile.openOutput();	
		dryoutrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		
		wsaaTotFundAmount.set(ZERO);
		wsaaSortKey.set(ZERO);
		wsaaOverflow.set("Y");
		
		//Get the company description..
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(dryoutrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();
		}
		companynm.set(descIO.getLongdesc());
		
		//Set up the DRPTSRT ready to read...
		drptsrtIO.setStatuz(varcom.oK);
		drptsrtIO.setEffdate(dryoutrec.effectiveDate);
		drptsrtIO.setDiaryEntityCompany(dryoutrec.company);
		drptsrtIO.setDiaryEntityBranch(dryoutrec.branch);
		drptsrtIO.setDiaryReportName(dryoutrec.reportName);
		drptsrtIO.setDiaryRunNumber(dryoutrec.runNumber);
		drptsrtIO.setFormat(drptsrtrec);
		drptsrtIO.setFunction(varcom.begn);
		
		//Loop through each DRPTSRT record printing the R5106
		//report ..                                          
		while ( !(isEQ(drptsrtIO.getStatuz(), varcom.endp))) {
			callDrpt200();
		}
		endReport500();
		printFile.close();
		
	}

protected void exit190()
	{
		exitProgram();
	}

protected void callDrpt200()
	{
		call210();
	}

protected void call210()
	{
			SmartFileCode.execute(appVars, drptsrtIO);
			if (isNE(drptsrtIO.getStatuz(), varcom.oK)
			&& isNE(drptsrtIO.getStatuz(), varcom.endp)) {
				drylogrec.params.set(drptsrtIO.getParams());
				drylogrec.statuz.set(drptsrtIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				fatalError();//Modify for ILPI-65 
			}
			if (isEQ(drptsrtIO.getStatuz(), varcom.endp)
			|| isNE(drptsrtIO.getDiaryEntityCompany(), dryoutrec.company)
			|| isNE(drptsrtIO.getDiaryEntityBranch(), dryoutrec.branch)
			|| isNE(drptsrtIO.getDiaryReportName(), dryoutrec.reportName)) {
				drptsrtIO.setStatuz(varcom.endp);
				return ;
			}
			if (isNE(dryoutrec.runNumber, ZERO)
			&& isNE(dryoutrec.runNumber, drptsrtIO.getDiaryRunNumber())) {
				drptsrtIO.setStatuz(varcom.endp);
				return ;
			}		
			writeLine300();	
			drptsrtIO.setFunction(varcom.nextr);
	}

protected void writeLine300()
	{
		write310();
	}

protected void write310()
	{
		//Bring the record details in.
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		
		if (isNE(dryrDryrptRecInner.rh518Zintbfnd,wsaaSortZintbfnd)){
				if (isNE(wsaaSortZintbfnd,SPACES)){
					summaryline600();
			}
		wsaaSortZintbfnd.set(dryrDryrptRecInner.rh518Zintbfnd);
		wsaaOverflow.set("N");
		newPage400();
	}
		/* Fill detail record.*/
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		
		rh518D01RecordInner.chdrnum.set(dryrDryrptRecInner.rh518Chdrnum);
		rh518D01RecordInner.fundamnt.set(dryrDryrptRecInner.rh518Fundamnt);
		rh518D01RecordInner.effdate.set(dryrDryrptRecInner.rh518Effdate);
		rh518D01RecordInner.batctrcde.set(dryrDryrptRecInner.rh518Batctrcde);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dryrDryrptRecInner.rh518Chdrcoy);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(dryrDryrptRecInner.rh518Batctrcde);

		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(dryoutrec.language);
		getdescrec.function.set("CHECK");

		callProgram(Getdesc.class, getdescrec.getdescRec);

		if (isNE(getdescrec.statuz, varcom.oK)) {
			rh518D01RecordInner.trandesc.set(SPACES);
		} else {
			rh518D01RecordInner.trandesc.set(getdescrec.longdesc);
		}
	
		//Accumulate totals for the fund, currency and unit type.
		wsaaTotFundAmount.set(add(dryrDryrptRecInner.rh518Fundamnt,wsaaTotFundAmount));
		
		//Write the detail line to the report ....  
		printRecord.set(SPACES);
		printFile.printRh518d01(rh518D01RecordInner.rh518D01Record, indicArea);
		wsaaOverflow.set("Y");
	}

protected void newPage400()
	{
		new400();
	}

protected void new400()
	{
		/* Fill header record.*/
		company.set(dryoutrec.company);
		bschednam.set("L2NEWUNITD");
		bschednum.set(dryoutrec.runNumber);
		//Set up the virtual fund details...  
		zintbfnd.set(dryrDryrptRecInner.rh518Zintbfnd);
		
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(dryoutrec.company);
		descIO.setDesctabl(t5515);
		descIO.setDescitem(dryrDryrptRecInner.rh518Zintbfnd);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();
		}
		longdesc.set(descIO.getLongdesc());
		
		itdmIO.setDataKey(SPACES);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setItemcoy(dryoutrec.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(dryrDryrptRecInner.rh518Zintbfnd);
		itdmIO.setItmfrm(dryoutrec.effectiveDate);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getItemcoy(), dryoutrec.company)
		|| isNE(itdmIO.getItemtabl(),t5515)
		|| isNE(itdmIO.getItemitem(),dryrDryrptRecInner.rh518Zintbfnd)
		|| isEQ(itdmIO.getStatuz(),varcom.endp))
		{
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();
		}
		else
		{
			t5515rec.t5515Rec.set(itdmIO.getGenarea());
		}
		fndcurr.set(t5515rec.currcode);

		itemIO.setItempfx("IT");
		itemIO.setItemcoy(dryrDryrptRecInner.rh518Chdrcoy);
		itemIO.setItemtabl(t3629);
		itemIO.setItemitem(t5515rec.currcode);

		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(dryoutrec.language);
		getdescrec.function.set("CHECK");

		callProgram(Getdesc.class, getdescrec.getdescRec);

		if (isNE(getdescrec.statuz, varcom.oK)) {
			curdesc.set('!');
		} else {
			curdesc.set(getdescrec.longdesc);
		}
		
		printRecord.set(SPACES);
		printFile.printRh518h01(rh518H01Record, indicArea);
		
		printRecord.set(SPACES);
		printFile.printRh518h02(rh518H02Record, indicArea);
	}

protected void endReport500()
	{
		/*NEW*/

	summaryline600();
	
	printFile.printRh518e01(rh518E01Record, indicArea);
		/*EXIT*/
	}

protected void summaryline600()
{
	totfndamt.set(wsaaTotFundAmount);
	
	printFile.printRh518t01(rh518T01Record, indicArea);
	wsaaTotFundAmount.set(ZERO);
}

protected void fatalError()
	{
		/*A010-FATAL*/
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		dryoutrec.statuz.set(drylogrec.statuz);
		drylogrec.runNumber.set(dryoutrec.runNumber);
		drylogrec.entityKey.set(SPACES);
		a001FatalError();
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);
	private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);

	private FixedLengthStringData rh518DataArea = new FixedLengthStringData(500).isAPartOf(dryrGenarea, 0, REDEFINE);
	private ZonedDecimalData rh518Fundamnt = new ZonedDecimalData(17, 2).isAPartOf(rh518DataArea, 0);
	private ZonedDecimalData rh518Effdate = new ZonedDecimalData(8).isAPartOf(rh518DataArea, 17);
	private FixedLengthStringData rh51Fdbkind = new FixedLengthStringData(1).isAPartOf(rh518DataArea, 25);
	private FixedLengthStringData rh518Chdrcoy = new FixedLengthStringData(1).isAPartOf(rh518DataArea, 26);
	private FixedLengthStringData rh518Chdrnum = new FixedLengthStringData(8).isAPartOf(rh518DataArea, 27);
	private FixedLengthStringData rh518Zintbfnd = new FixedLengthStringData(4).isAPartOf(rh518DataArea, 35);
	private FixedLengthStringData rh518Batctrcde = new FixedLengthStringData(4).isAPartOf(rh518DataArea, 39);
	private FixedLengthStringData rh518Trandesc = new FixedLengthStringData(30).isAPartOf(rh518DataArea, 43);
	
	public FixedLengthStringData rh518Rh518Key = new FixedLengthStringData(38).isAPartOf(dryrSortkey, 0, REDEFINE);
	public FixedLengthStringData rh518chdrcoy = new FixedLengthStringData(1).isAPartOf(rh518Rh518Key, 0);
	public FixedLengthStringData rh518zintbfnd = new FixedLengthStringData(4).isAPartOf(rh518Rh518Key, 1);
	public FixedLengthStringData rh518chdrnum = new FixedLengthStringData(8).isAPartOf(rh518Rh518Key, 5);
	public FixedLengthStringData rh518batctrcde = new FixedLengthStringData(4).isAPartOf(rh518Rh518Key, 13);
	
}
/*
 * Class transformed  from Data Structure RH5106-D01-RECORD--INNER
 */
private static final class Rh518D01RecordInner { 

	private FixedLengthStringData rh518D01Record = new FixedLengthStringData(99);
	private FixedLengthStringData rh518d01O = new FixedLengthStringData(99).isAPartOf(rh518D01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(rh518d01O, 0);
	private FixedLengthStringData fundamnt = new FixedLengthStringData(17).isAPartOf(rh518d01O, 8);
	private FixedLengthStringData effdate = new FixedLengthStringData(10).isAPartOf(rh518d01O, 25);
	private FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(rh518d01O, 35);
	private FixedLengthStringData trandesc = new FixedLengthStringData(30).isAPartOf(rh518d01O, 39);
}

private static final class Rh518N01RecordInner { 

	private FixedLengthStringData rh518N01Record = new FixedLengthStringData(99);
	private FixedLengthStringData rh518n01O = new FixedLengthStringData(99).isAPartOf(rh518N01Record, 0);
	private FixedLengthStringData schalph = new FixedLengthStringData(10).isAPartOf(rh518n01O, 0);
}

private static final class Rh518E01RecordInner { 

	private FixedLengthStringData rh518E01Record = new FixedLengthStringData(99);
	private FixedLengthStringData rh518e01O = new FixedLengthStringData(99).isAPartOf(rh518E01Record, 0);
	private FixedLengthStringData totfndamt = new FixedLengthStringData(8).isAPartOf(rh518e01O, 0);
}
private static final class Rh518H02RecordInner { 

	private FixedLengthStringData rh518H02Record = new FixedLengthStringData(99);
	private FixedLengthStringData rh518h02O = new FixedLengthStringData(99).isAPartOf(rh518H02Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(8).isAPartOf(rh518h02O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(16).isAPartOf(rh518h02O, 8);
	private FixedLengthStringData vrtfnd = new FixedLengthStringData(10).isAPartOf(rh518h02O, 24);
	private FixedLengthStringData longdesc = new FixedLengthStringData(10).isAPartOf(rh518h02O, 30);
	private ZonedDecimalData fndcurr = new ZonedDecimalData(4, 0).isAPartOf(rh518h02O, 40);
	private FixedLengthStringData curdesc = new FixedLengthStringData(4).isAPartOf(rh518h02O, 50);
	private FixedLengthStringData fundtyp = new FixedLengthStringData(4).isAPartOf(rh518h02O, 54);
	
}
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
