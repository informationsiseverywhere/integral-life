package com.csc.life.diary.recordstructures;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:09:04
 * Description:
 * Copybook name: OVBLDRYREC
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Ovbldryrec extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  
  	public FixedLengthStringData overbillRec = new FixedLengthStringData(39);
  	public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(overbillRec, 0);
  	public PackedDecimalData newTranno = new PackedDecimalData(5, 0).isAPartOf(overbillRec, 4);
  	public PackedDecimalData ptdate = new PackedDecimalData(8, 0).isAPartOf(overbillRec, 7);
  	public PackedDecimalData btdate = new PackedDecimalData(8, 0).isAPartOf(overbillRec, 12);
  	public PackedDecimalData tranDate = new PackedDecimalData(6, 0).isAPartOf(overbillRec, 17);
  	public PackedDecimalData tranTime = new PackedDecimalData(6, 0).isAPartOf(overbillRec, 21);
  	public ZonedDecimalData user = new ZonedDecimalData(6, 0).isAPartOf(overbillRec, 25).setUnsigned();
  	public FixedLengthStringData termid = new FixedLengthStringData(4).isAPartOf(overbillRec, 31);
  	public FixedLengthStringData transcd = new FixedLengthStringData(4).isAPartOf(overbillRec, 35);


	public void initialize() {
		COBOLFunctions.initialize(overbillRec);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		overbillRec.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}