/*
 * File: Dryr613.java
 * Date: March 26, 2014 3:06:04 PM ICT
 * Author: CSC
 *
 * Class transformed from DRYR613.CBL
 *
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER_REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.NUMERIC;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.csc.diaryframework.parent.Maind;
import com.csc.dip.jvpms.web.ExternalisedRules;
import com.csc.fsu.clients.dataaccess.dao.ClntpfDAO;
import com.csc.fsu.clients.dataaccess.model.Clntpf;
import com.csc.fsu.general.dataaccess.dao.ChdrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PayrpfDAO;
import com.csc.fsu.general.dataaccess.dao.PtrnpfDAO;
import com.csc.fsu.general.dataaccess.model.Payrpf;
import com.csc.fsu.general.dataaccess.model.Ptrnpf;
import com.csc.fsu.general.procedures.AgecalcPojo;
import com.csc.fsu.general.procedures.AgecalcUtils;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.procedures.Datcon2Pojo;
import com.csc.fsu.general.procedures.Datcon2Utils;
import com.csc.fsu.general.procedures.Datcon3Pojo;
import com.csc.fsu.general.procedures.Datcon3Utils;
import com.csc.fsu.general.procedures.ZrdecplcPojo;
import com.csc.fsu.general.procedures.ZrdecplcUtils;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.fsuframework.core.FeaConfg;
import com.csc.life.diary.dataaccess.dao.Dryr613DAO;
import com.csc.life.diary.dataaccess.model.Dryr613Dto;
import com.csc.life.enquiries.procedures.Crtundwrt;
import com.csc.life.enquiries.recordstructures.Crtundwrec;
import com.csc.life.linkage.procedures.LinkageInfoService;
import com.csc.life.newbusiness.dataaccess.dao.AgcmpfDAO;
import com.csc.life.newbusiness.dataaccess.model.Agcmpf;
import com.csc.life.newbusiness.dataaccess.model.Pcddpf;
import com.csc.life.newbusiness.recordstructures.Isuallrec;
import com.csc.life.productdefinition.dataaccess.dao.AnnypfDAO;
import com.csc.life.productdefinition.dataaccess.dao.Br613TempDAO;
import com.csc.life.productdefinition.dataaccess.dao.LextpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.LifepfDAO;
import com.csc.life.productdefinition.dataaccess.dao.MbnspfDAO;
import com.csc.life.productdefinition.dataaccess.dao.RcvdpfDAO;
import com.csc.life.productdefinition.dataaccess.model.Annypf;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.dataaccess.model.Lextpf;
import com.csc.life.productdefinition.dataaccess.model.Lifepf;
import com.csc.life.productdefinition.dataaccess.model.Mbnspf;
import com.csc.life.productdefinition.dataaccess.model.Rcvdpf;
import com.csc.life.productdefinition.procedures.Vpxacbl;
import com.csc.life.productdefinition.procedures.VpxchdrPojo;
import com.csc.life.productdefinition.procedures.VpxchdrUtil;
import com.csc.life.productdefinition.procedures.Vpxlext;
import com.csc.life.productdefinition.recordstructures.Premiumrec;
import com.csc.life.productdefinition.recordstructures.Vpmcalcrec;
import com.csc.life.productdefinition.recordstructures.Vpxacblrec;
import com.csc.life.productdefinition.recordstructures.Vpxlextrec;
import com.csc.life.productdefinition.tablestructures.T5671rec;
import com.csc.life.productdefinition.tablestructures.T5675rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.dataaccess.dao.IncrpfDAO;
import com.csc.life.regularprocessing.dataaccess.model.Incrpf;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.DAOFactory;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Chdrpf;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.AppVars;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
*
*
* Rerate of Instalment premiums.
*
*      Firstly, the contract header will be validated for risk
* and premium status codes from T5679.  Any which do not have the
* valid risk and premium status codes will be rejected.
*      The program then reads through all of the COVR records
* linked to the contract header until it arrives at the first COVR
* record fitting the selection criteria.
*      Further validation will be performed upon the COVR record
* for risk and premium status codes and those without valid risk
* and premium status codes will be rejected.  Records will also be
* rejected when the rerate date is less than the effective date
* of the batch run plus the lead days on T5655.
*        (COVR-RRTDAT < PARM-EFFDATE + T5655-LEAD-DAYS).
*      All COVR records linked to the contract will be checked to
* see if the premium cessation date is earlier than the rerate
* date.  If this is the case, the contract header premium status
* is updated when the contract header file is rewritten.
*      Assuming that the rerate date on the COVR does not equal
* the premium cessation date, the premium will be rerated by
* calling the premium calculation subroutine on T5675 if one
* exists.
*      The COVR record is then held for updating.  If the premium
* cessation date equals the rerate date, update the premium status
* code, add 1 to Control Total 4 and leave the section. Otherwise,
* the rerate date - 1 day is moved to the current to date and the
* record is updated validflag '2'.  The LEXTBRR file is then read
* until one of the following conditions is met:
*
*    (1) - End of file or wrong record
*    (2) - Cessation date on the LEXTBRR is not equal to the
*          rerate from date on the COVR incremented by the
*          premium recalculation frequency on T5687.
*
*     When (1),
*              If premium recalculation freq. on T5687 = zeroes,
*                 move COVR premium cessation date to
*                      COVR rerate date
*              Else
*                 move incremented rerate from date to
*                      COVR rerate and rerate from dates.
*     When (2),
*              If incremented rerate from date < LEXTBRR cessation
*              date,
*                   If COVR rerate date > LEXTBBR cessation date
*                      move LEXTBBR cessation date to
*                           COVR rerate date
*                   Else
*                      continue the loop
*              Else
*                 move incremented rerate from date to
*                      COVR rerate and rerate from dates.
*
*      Write a new COVR record, updating the COVR instalment
* premium if a premium method exists on T5687, moving the rerate
* date to the current from date, 'maxdate' to the current to date
* and updating the transaction number.  Add 1 to Control Total 2
* and perform the statistics section.
*      Once all of the coverages for the contract have been
* processed, update the contract header file with a valid flag '2'
* record, moving the rerate date - 1 day to the current to date.
*      Write a new valid flag '1' record with the rerate date as
* the current from date, 'maxdate' as the current to date, a new
* transaction number and update standing instalment amounts 1 and
* 6.  If all coverages (and riders) attached to the contract have
* expired, update the premium status code from T5679 and remove
* the contract fee from the contract header (subtract from
* standing instalment amounts 2 and 6).
*      Write a PTRN record for the transaction, increment Control
* Total 1 and add the difference in premiums to Control Total 3.
*      Update the PAYR file with a valid flag '2' record and a new
* PAYR record with the new premium amounts.
*
****************************************************************** ****
 * </pre>
 */
public class Dryr613 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYR613");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaAgntnum = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaCedagent = new FixedLengthStringData(8);

	private FixedLengthStringData wsaaAgntFound = new FixedLengthStringData(1);
	private Validator agentFound = new Validator(wsaaAgntFound, "Y");
	private ZonedDecimalData wsaaInstPrem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaZbinstprem = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaZlinstprem = new ZonedDecimalData(17, 2);
	private String wsaaFullyPaid = "";
	private FixedLengthStringData wsaaBillfreqx = new FixedLengthStringData(2);

	private FixedLengthStringData filler4 = new FixedLengthStringData(2).isAPartOf(wsaaBillfreqx, 0, FILLER_REDEFINE);
	private ZonedDecimalData wsaaBillfreqn = new ZonedDecimalData(2, 0).isAPartOf(filler4, 0).setUnsigned();
	private ZonedDecimalData wsaaNewTranno = new ZonedDecimalData(5, 0);

	private FixedLengthStringData wsaaRerateType = new FixedLengthStringData(1);
	private Validator lextRerate = new Validator(wsaaRerateType, "2");

	private String wsaaCovrUpdated = "";
	private String wsaaCovrsLive = "";
	private ZonedDecimalData wsaaPremDiff = new ZonedDecimalData(17, 2);
	private ZonedDecimalData wsaaBillDate = new ZonedDecimalData(8, 0);
	private FixedLengthStringData wsaaZsredtrm = new FixedLengthStringData(2);
	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();

	private String wsaaChdrValid = "";
	private String wsaaCovrValid = "";
	private ZonedDecimalData wsaaLeadDays = new ZonedDecimalData(3, 0).setUnsigned();
	private ZonedDecimalData wsaaDurationInt = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDurationRem = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaDuration = new ZonedDecimalData(2, 0).setUnsigned();
	private PackedDecimalData wsaaSumins = new PackedDecimalData(17, 2);

	private static final String TERMINAL_ID = "";
	private static final int TRAN_DATE = 0;
	private static final int TRAN_TIME = 0;
	private static final int USER = 0;

	private FixedLengthStringData wsaaT5671Item = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaItemTranCode = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 0);
	private FixedLengthStringData wsaaItemCrtable = new FixedLengthStringData(4).isAPartOf(wsaaT5671Item, 4);
	private ZonedDecimalData wsaaLastRrtDate = new ZonedDecimalData(8, 0);
	private ZonedDecimalData wsaaAnb = new ZonedDecimalData(3, 0);
	private ZonedDecimalData wsaaRateFrom = new ZonedDecimalData(8, 0);
	private PackedDecimalData wsaaRerateStore = new PackedDecimalData(8, 0);

	/* CONTROL-TOTALS */
	private static final int CT01 = 1;
	private static final int CT02 = 2;
	private static final int CT03 = 3;
	private static final int CT04 = 4;
	private static final int CT05 = 5;

	private T5655rec t5655rec = new T5655rec();
	private T5671rec t5671rec = new T5671rec();
	private T5687rec t5687rec = new T5687rec();
	private T5675rec t5675rec = new T5675rec();
	private Premiumrec premiumrec = new Premiumrec();
	private T5679rec t5679rec = new T5679rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private Isuallrec isuallrec = new Isuallrec();
	private Crtundwrec crtundwrec = new Crtundwrec();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();

	private Dryr613DAO dryr613DAO = getApplicationContext().getBean("dryr613DAO", Dryr613DAO.class);
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private ChdrpfDAO chdrpfDAO = getApplicationContext().getBean("chdrpfDAO", ChdrpfDAO.class);
	private PayrpfDAO payrpfDAO = getApplicationContext().getBean("payrpfDAO", PayrpfDAO.class);
	private IncrpfDAO incrpfDAO = getApplicationContext().getBean("incrpfDAO", IncrpfDAO.class);
	private PtrnpfDAO ptrnpfDAO = getApplicationContext().getBean("ptrnpfDAO", PtrnpfDAO.class);
	private Clntpf clntpf = new Clntpf();// ILIFE-8502
	private ClntpfDAO clntpfDAO = getApplicationContext().getBean("clntpfDAO", ClntpfDAO.class);// ILIFE-8502
	private int ct01Val;
	private int ct02Val;
	private int ct04Val;
	private int ct05Val;
	private ZonedDecimalData ct03Val = new ZonedDecimalData(17, 2);
	private Chdrpf chdrlifIO = new Chdrpf();
	private Payrpf payrIO = new Payrpf();
	private com.csc.smart400framework.dataaccess.model.Clntpf clnt;// ILIFE-8502
	private com.csc.smart400framework.dataaccess.dao.ClntpfDAO clntDao; // ILIFE-8502

	private Map<String, List<Chdrpf>> chdrpfMap = new HashMap<String, List<Chdrpf>>();
	private List<Covrpf> covrpfListInst = new ArrayList<Covrpf>();
	private List<Covrpf> covrpfListUpdt = new ArrayList<Covrpf>();
	private Map<String, List<Payrpf>> payrpfMap = new HashMap<String, List<Payrpf>>();
	private Map<String, List<Incrpf>> incrpfMap = new HashMap<String, List<Incrpf>>();

	private List<Ptrnpf> ptrnpfList = new ArrayList<Ptrnpf>();
	private List<Chdrpf> chdrpfListUpdt = new ArrayList<Chdrpf>();
	private List<Chdrpf> chdrpfListInst = new ArrayList<Chdrpf>();
	private List<Payrpf> payrpfListUpdt = new ArrayList<Payrpf>();
	private List<Payrpf> payrpfListInst = new ArrayList<Payrpf>();
	private List<Incrpf> incrpfList = new ArrayList<Incrpf>();
	private List<Agcmpf> agcmpfListUpdt = new ArrayList<Agcmpf>();
	private List<Agcmpf> agcmppfListInst = new ArrayList<Agcmpf>();

	// add by wli31 ILIFE-7308
	private MbnspfDAO mbnsDAO = getApplicationContext().getBean("mbnspfDAO", MbnspfDAO.class);
	private LifepfDAO lifepfDAO = getApplicationContext().getBean("lifepfDAO", LifepfDAO.class);
	private Br613TempDAO br613TempDAO = getApplicationContext().getBean("br613TempDAO", Br613TempDAO.class);
	private LextpfDAO lextpfDAO = getApplicationContext().getBean("lextpfDAO", LextpfDAO.class);
	private AgcmpfDAO agcmpfDAO = getApplicationContext().getBean("agcmpfDAO1", AgcmpfDAO.class);
	private AnnypfDAO annypfDAO = getApplicationContext().getBean("annypfDAO", AnnypfDAO.class);
	private Lifepf lifepf;
	private FixedLengthStringData wsaaT5675Item = new FixedLengthStringData(8);
	private Map<String, List<Itempf>> t5655Map;
	private Map<String, List<Itempf>> t5671ListMap;
	private Map<String, List<Itempf>> t5687ListMap;
	private Map<String, List<Itempf>> t5675ListMap;
	private Map<String, List<Itempf>> tr517ListMap;
	private Map<String, List<Covrpf>> covrMap = new HashMap<String, List<Covrpf>>();
	private Map<String, List<Agcmpf>> agcmMap = new HashMap<String, List<Agcmpf>>();
	private Map<String, List<Pcddpf>> pcddpfMap = new HashMap<String, List<Pcddpf>>();
	private Map<String, List<Lextpf>> lextpfMap = new HashMap<String, List<Lextpf>>();
	private Map<String, List<Annypf>> annyMap = new HashMap<String, List<Annypf>>();
	private PackedDecimalData wsaaAnnprem = new PackedDecimalData(17, 2);

	private Datcon3Utils datcon3Utils = getApplicationContext().getBean("datcon3Utils", Datcon3Utils.class);
	private Datcon2Utils datcon2Utils = getApplicationContext().getBean("datcon2Utils", Datcon2Utils.class);
	private ZrdecplcUtils zrdecplcUtils = getApplicationContext().getBean("zrdecplcUtils", ZrdecplcUtils.class);
	private AgecalcUtils agecalcUtils = getApplicationContext().getBean("agecalcUtils", AgecalcUtils.class);
	private ZrdecplcPojo zrdecplcPojo = new ZrdecplcPojo();
	// ILIFE-7577
	private boolean ausRerate = false;
	private String prevChdrnum;
	private FixedLengthStringData isUpdated = new FixedLengthStringData(1).init("N");
	private boolean riskPremflag; // ILIFE-7845
	private static final String RISKPREM_FEATURE_ID = "NBPRP094";// ILIFE-7845
	private Datcon2rec datcon2rec = new Datcon2rec();
	private VpxchdrUtil vpxchdrUitl = new VpxchdrUtil();
	private VpxchdrPojo vpxchdrPojo = new VpxchdrPojo();
	private boolean stampDutyflag = false;
	/* ILIFE-8248 start */
	private boolean lnkgFlag = false;
	/* ILIFE-8248 end */
	private boolean prmhldtrad = false;// ILIFE-8509
	private Map<String, Itempf> ta524Map;

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsspEdterror() {
		return wsspEdterror;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	public Dryr613() {
		super();
	}

	/**
	 * The mainline method is the default entry point to the class
	 * 
	 */
	@Override
	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			// ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void startProcessing() {
		initialise1000();
		List<Dryr613Dto> dryr613dtoList = readChunkRecord();
		for (Dryr613Dto dryr613dto : dryr613dtoList) {
			readFile2000(dryr613dto);
			edit2500(dryr613dto);
			if (isEQ(wsspEdterror, Varcom.oK)) {
				update3000(dryr613dto);
			}
		}
		updateDiaryData();
		commit3500();

	}

	protected void initialise1000() {
		wsspEdterror.set(Varcom.oK);
		loadTables();
	}

	private List<Dryr613Dto> readChunkRecord() {
		chdrpfMap.clear();
		String company = drypDryprcRecInner.drypCompany.toString();
		List<String> chdrNumList = Collections.singletonList(drypDryprcRecInner.drypEntity.toString());
		chdrpfMap = chdrpfDAO.searchChdrpf(company, chdrNumList);
		readChdrpf(drypDryprcRecInner.drypEntity.toString());
		calculateDate();
		List<Dryr613Dto> dryr613DtoList = dryr613DAO.getReRate(drypDryprcRecInner.drypCompany.toString(),
				wsaaBillDate.toString(), drypDryprcRecInner.drypEntity.toString());
		if (!dryr613DtoList.isEmpty()) {
			agcmMap.clear();
			covrMap.clear();
			payrpfMap.clear();
			incrpfMap.clear();
			pcddpfMap.clear();
			annyMap.clear();
			covrMap = dryr613DAO.searchCovrRecord(company, chdrNumList);
			payrpfMap = payrpfDAO.getPayrLifMap(company, chdrNumList);
			incrpfMap = incrpfDAO.searchIncrRecordByChdrnum(chdrNumList);
			agcmMap = dryr613DAO.getAgcmRecords(company, chdrNumList);
			pcddpfMap = dryr613DAO.getPcddpfRecords(company, chdrNumList);
			lextpfMap = lextpfDAO.searchLextpfMap(company, chdrNumList);
			annyMap = annypfDAO.searchAnnyRecordByChdrnum(chdrNumList);
		}
		return dryr613DtoList;

	}

	private void readChdrpf(String chdrNum) {
		if (chdrpfMap != null && chdrpfMap.containsKey(chdrNum)) {
			chdrlifIO = chdrpfMap.get(chdrNum).get(0);
		} else {
			drylogrec.params.set(chdrNum);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	private void calculateDate() {
		calculateLeadDays();
		/* Increment the effective date of the batch run by the lead */
		/* days on T5655 for use in validating the coverage. */
		wsaaLeadDays.set(t5655rec.leadDays);
		datcon2rec.freqFactor.set(wsaaLeadDays);
		datcon2rec.frequency.set("DY");
		datcon2rec.intDate1.set(drypDryprcRecInner.drypRunDate);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {

			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		wsaaBillDate.set(datcon2rec.intDate2);
	}

	private void calculateLeadDays() {
		String authCode = drypDryprcRecInner.drypBatctrcde.trim();
		List<Itempf> t5655List = t5655Map.get(authCode + chdrlifIO.getCnttype());
		if (t5655List == null) {
			t5655List = t5655Map.get(authCode + "***");
		}

		if (t5655List == null) {
			logT5655NotFoundError();
		}

		boolean isItemFound = false;
		for (Itempf itempf : t5655List) {
			if (isGTE(chdrlifIO.getOccdate(), itempf.getItmfrm()) && isLTE(chdrlifIO.getOccdate(), itempf.getItmto())) {
				t5655rec.t5655Rec.set(StringUtil.rawToString(itempf.getGenarea()));
				isItemFound = true;
				break;
			}
		}

		if (!isItemFound) {
			logT5655NotFoundError();
		}
	}

	private void logT5655NotFoundError() {
		drylogrec.params.set("No data found in T5655");
		drylogrec.dryDatabaseError.setTrue();
		a000FatalError();
	}

	private void loadTables() {
		String itemcoy = drypDryprcRecInner.drypCompany.toString();
		String itemitem = drypDryprcRecInner.drypBatctrcde.toString();
		List<Itempf> items = itemDAO.getAllItemitem(smtpfxcpy.item.toString(), itemcoy, "T5679", itemitem);
		if (null == items || items.isEmpty()) {
			drylogrec.params.set("T5679");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		} else {// IJTI-320 START
			t5679rec.t5679Rec.set(StringUtil.rawToString(items.get(0).getGenarea()));
		}
		// IJTI-320 END
		t5687ListMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "T5687");
		t5675ListMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "T5675");
		tr517ListMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "TR517");
		t5655Map = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "T5655");
		t5671ListMap = itemDAO.loadSmartTable(smtpfxcpy.item.toString(), itemcoy, "T5671");

		String ausRerateItem = "BTPRO014"; // ILIFE-7577
		String stampdutyItem = "NBPROP01";
		String linkageItem = "NBPRP055";// ILIFE-8248
		ausRerate = FeaConfg.isFeatureExist(itemcoy, ausRerateItem, appVars, "IT"); // ILIFE-7577
		stampDutyflag = FeaConfg.isFeatureExist(itemcoy, stampdutyItem, appVars, "IT");
		/* ILIFE-8248 start */
		lnkgFlag = FeaConfg.isFeatureExist(itemcoy, linkageItem, appVars, "IT");
		String prmhldtradItem = "CSOTH010";// ILIFE-8509
		/* ILIFE-8248 start */
		prmhldtrad = FeaConfg.isFeatureExist(itemcoy, prmhldtradItem, appVars, "IT");// ILIFE-8509
		if (prmhldtrad) {
			ta524Map = itemDAO.getItemMap("IT", itemcoy, "TA524");
		}
	}

	protected void readFile2000(Dryr613Dto dryr613dto) {
		if (ausRerate && prevChdrnum != null && !prevChdrnum.equals(dryr613dto.getChdrNum())) {
			isUpdated.set("N");
		}
		ct01Val++;
	}

	protected void edit2500(Dryr613Dto dryr613dto) {
		wsaaChdrValid = "Y";
		for (int i = 1; i <= 12; i++) {
			validateChdrStatus2520(i);
			if (isEQ(wsaaChdrValid, "Y")) {
				break;
			}
		}
		pendingIncrease2700(dryr613dto);
		if (isEQ(wsaaChdrValid, "N")) {
			wsspEdterror.set(SPACES);
			ct05Val++;
		} else {
			wsspEdterror.set(Varcom.oK);
		}

	}

	protected void validateChdrStatus2520(int i) {
		if (isEQ(t5679rec.cnRiskStat[i], chdrlifIO.getStatcode())) {
			for (int j = 1; j <= 12; j++) {
				if (isEQ(t5679rec.cnPremStat[j], chdrlifIO.getPstcde())) {
					wsaaChdrValid = "Y";
					break;
				}
			}
		}
	}

	protected void pendingIncrease2700(Dryr613Dto dryr613dto) {
		if (!prmhldtrad || ta524Map.get(chdrlifIO.getCnttype()) == null) {// ILIFE-8509
			List<Covrpf> covrpfList = covrMap.get(dryr613dto.getChdrNum());
			if (covrpfList != null) {
				processCovrpf(drypDryprcRecInner.drypEntity.toString(), covrpfList);
			}
		}
	}

	private void processCovrpf(String chdrnum, List<Covrpf> covrpfList) {
		for (Covrpf covr : covrpfList) {
			if ((incrpfMap != null && incrpfMap.containsKey(chdrnum))
					&& (covr.getReinstated() == null || !(("Y").equals(covr.getReinstated())))) {
				checkIncrpf(chdrnum);
			}
		}
	}

	private void checkIncrpf(String chdrnum) {
		List<Incrpf> incrpfRecords = incrpfMap.get(chdrnum);
		if (incrpfRecords != null) {
			for (Incrpf c : incrpfRecords) {
				if (c.getValidflag().equals("1")) {
					wsaaChdrValid = "N";
					break;
				}
			}
		}
	}

	protected void update3000(Dryr613Dto dryr613dto) {

		/* 3010-UPDATE. */
		/* Perform a begin on the COVRRNL and then enter the COVR loop. */
		/* The Wsaa-covrs-live indicator is set to N initially. This */
		/* is set to Y if a valid cover/rider is found, on the contract */
		/* which is alive. This indicator is then checked in section */
		/* 4000 to determine whether the whole contract has matured or */
		/* whether there are valid covers/riders existing. */
		List<Covrpf> covrList = covrMap.get(dryr613dto.getChdrNum());
		List<Agcmpf> agcmList = agcmMap.get(dryr613dto.getChdrNum());
		wsaaCovrsLive = "N";
		wsaaCovrUpdated = "N";
		wsaaPremDiff.set(ZERO);
		// START OF ILIFE-7577
		if (ausRerate) {
			if (isEQ(isUpdated, "N")) {
				for (Covrpf covrpf : covrList) {
					prevChdrnum = dryr613dto.getChdrNum();
					validateCovr3100(covrpf);
					if (isEQ(wsaaCovrValid, "Y")) {
						updateCovr3200(covrpf);
						if (agcmList != null && agcmList.size() > 0) {
							updateAgcm3700(covrpf, agcmList, dryr613dto);
						}
						genericProcessing3900(covrpf);
					}
				}

				if (isEQ(wsaaCovrUpdated, "Y")) {
					updateChdrPtrn4100();
					updatePayr4200();
					isUpdated.set("Y");
				}
			}
		} else {
			for (Covrpf covrpf : covrList) {
				validateCovr3100(covrpf);
				if (isEQ(wsaaCovrValid, "Y")) {
					updateCovr3200(covrpf);
					if (agcmList != null && agcmList.size() > 0) {
						updateAgcm3700(covrpf, agcmList, dryr613dto);
					}
					genericProcessing3900(covrpf);
				}
			}

			if (isEQ(wsaaCovrUpdated, "Y")) {
				updateChdrPtrn4100();
				updatePayr4200();//
			}
		}
	}

	protected void validateCovr3100(Covrpf covrpf) {
		/* Check that the rerate date is not equal to zeroes */
		/* and less than both the billing effective date + 30 days and */
		/* the billing effective date + the number of lead days on */
		/* T5655. */
		/* If a valid cover/rider has a premium cessation date beyond */
		/* the billing date then it is still 'live' and hence set */
		/* set indicator accordingly. */
		/* Validate the coverage risk and premium statii. */
		wsaaCovrValid = "N";
		/* Check if COVR is the WOP component, ignore it! */
		if (isNE(covrpf.getRerateDate(), ZERO) && isEQ(covrpf.getValidflag(), "1")) {
			if (isGTE(covrpf.getPremCessDate(), wsaaBillDate)) {
				wsaaCovrsLive = "Y";
			}
			validateCoverage(covrpf);
		}
//		if (isEQ(wsaaCovrsLive, "N")) {//IBPLIFE-2531
//			return;
//		}
		String keyItemitem = covrpf.getCrtable().trim();
		List<Itempf> tr517List = tr517ListMap.get(keyItemitem);
		if (tr517List != null && tr517List.size() > 0) {
			wsaaCovrValid = "N";
		}
	}

	private void validateCoverage(Covrpf covrpf) {
		if (isLTE(covrpf.getRerateDate(), wsaaBillDate)) {
			for (int i = 1; i <= 12; i++) {
				validateCovrStatus3150(covrpf, i);
				if (isEQ(wsaaCovrValid, "Y")) {
					break;
				}
			}
		}
	}

	protected void validateCovrStatus3150(Covrpf covrpf, int i) {
		if (isEQ(t5679rec.covRiskStat[i], covrpf.getStatcode())) {
			for (int j = 1; j <= 12; j++) {
				if (isEQ(t5679rec.covPremStat[j], covrpf.getPstatcode())) {
					wsaaCovrValid = "Y";
					break;
				}
			}
		}
	}

	protected void updateCovr3200(Covrpf covrpf) {
		wsaaCovrUpdated = "Y";
		/* Store the rerate date. */
		wsaaRerateStore.set(covrpf.getRerateDate());
		/* Default to a TRUE rerate. */
		wsaaRerateType.set("1");
		wsaaInstPrem.set(ZERO);
		wsaaZbinstprem.set(ZERO);
		wsaaZlinstprem.set(ZERO);
		/* Read the COVR record before rewriting the validflag '2' */
		/* record using a different logical view to avoid picking up the */
		/* new COVR record when reading the next record. */
		lifepf = lifepfDAO.getLifeEnqRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(),
				"00");/* IJTI-1386 */
		if (lifepf == null) {
			drylogrec.statuz.set("lifepf MRNF");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(covrpf.getPremCessDate(), covrpf.getRerateDate())) {
			wsaaFullyPaid = "Y";
			pastCeaseDate3290(covrpf);
			return;
		}

		wsaaFullyPaid = "N";

		if (prmhldtrad && "Y".equals(covrpf.getReinstated())) {
			readT56873255(covrpf);
		} else if (!prmhldtrad || covrpf.getReinstated() == null || !"Y".equals(covrpf.getReinstated())) {
			calculatePremium3250(covrpf);
		}
		
		Covrpf covrpfUpdate = new Covrpf(covrpf);
		covrpfUpdate.setCurrto(wsaaRerateStore.toInt());
		covrpfUpdate.setValidflag("2");
		covrpfListUpdt.add(covrpfUpdate);
		checkLexts3300(covrpfUpdate);
		statistics5000();
	}

	protected void calculatePremium3250(Covrpf covrpf) {
		readT56873255(covrpf);
	}

	protected void readT56873255(Covrpf covrpf) {
		String keyItemitem = covrpf.getCrtable().trim();/* IJTI-1386 */
		String effDate = Integer.toString(covrpf.getCrrcd());
		boolean itemFound = false;
		if (t5687ListMap.containsKey(keyItemitem)) {
			List<Itempf> itempfList = t5687ListMap.get(keyItemitem);
			Iterator<Itempf> iterator = itempfList.iterator();
			while (iterator.hasNext()) {
				Itempf itempf = iterator.next();
				if (Integer.parseInt(itempf.getItmfrm().toString()) > 0) {
					if ((Integer.parseInt(effDate) >= Integer.parseInt(itempf.getItmfrm().toString()))
							&& Integer.parseInt(effDate) <= Integer.parseInt(itempf.getItmto().toString())) {
						t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
						wsaaZsredtrm.set(t5687rec.zsredtrm);
						itemFound = true;
					}
				} else {
					t5687rec.t5687Rec.set(StringUtil.rawToString(itempf.getGenarea()));
					wsaaZsredtrm.set(t5687rec.zsredtrm);
					itemFound = true;
				}
			}
		}
		if (!itemFound) {
			drylogrec.params.set(t5687rec.t5687Rec);
			drylogrec.params.set("T5687" + keyItemitem);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if (isEQ(t5687rec.premmeth, SPACES)) {
			payr3256(covrpf);
			return;
		}

		setRerateType3410(covrpf);
		premiumrec.premiumRec.set(SPACES);

		premiumrec.lsex.set(lifepf.getCltsex());
		calculateAnb3260(lifepf);
		premiumrec.lage.set(wsaaAnb);
		/* Read T5675 for premium calculation subroutine call. */

		checkJointLife3270(covrpf);
		/*
		 * ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product
		 * calculations] Start
		 */
		if (AppVars.getInstance().getAppConfig().isVpmsEnable()) {
			premiumrec.premMethod.set(wsaaT5675Item);
		}
		/* ILIFE-3142 End */
		
		List<Itempf> t5675List = t5675ListMap.get(wsaaT5675Item.toString());
		if (t5675List != null && !t5675List.isEmpty()) {
			t5675rec.t5675Rec.set(StringUtil.rawToString(t5675List.get(0).getGenarea()));
			payr3256(covrpf);
		} else {
			drylogrec.params.set("T5675" + wsaaT5675Item);
			drylogrec.statuz.set("MRNF");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		
	}

	protected void payr3256(Covrpf covrpf) {
		/* The PAYR file must be read to get the billing frequency. */
		getPayrpf(); // ILIFE-4323 by dpuhawan
		payrIO = null;
		if (payrpfMap != null && payrpfMap.containsKey(chdrlifIO.getChdrnum())) {
			for (Payrpf c : payrpfMap.get(chdrlifIO.getChdrnum())) {
				if (c.getPayrseqno() == 1 && "1".equals(c.getValidflag())) {
					payrIO = c;
					break;
				}
			}
		}

		if (payrIO == null) {
			drylogrec.params.set(chdrlifIO.getChdrnum());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		wsaaBillfreqx.set(payrIO.getBillfreq());
		if (isEQ(t5687rec.premmeth, SPACES)) {
			return;
		}
		premiumrec.crtable.set(covrpf.getCrtable());
		premiumrec.chdrChdrcoy.set(covrpf.getChdrcoy());
		premiumrec.chdrChdrnum.set(covrpf.getChdrnum());
		premiumrec.lifeLife.set(covrpf.getLife());
		premiumrec.lifeJlife.set(covrpf.getJlife());
		premiumrec.covrRider.set(covrpf.getRider());
		premiumrec.covrCoverage.set(covrpf.getCoverage());
		premiumrec.mop.set(payrIO.getBillchnl());
		premiumrec.billfreq.set(payrIO.getBillfreq());
		premiumrec.effectdt.set(wsaaLastRrtDate);
		premiumrec.mortcls.set(covrpf.getMortcls());
		premiumrec.currcode.set(covrpf.getPremCurrency());
		premiumrec.termdate.set(covrpf.getPremCessDate());
		/* Calculate the remaining term. This is re-rate type */
		/* dependent. For TRUE re-rates this is the re-rate date. */
		/* For LEXT re-rates this is the LAST TRUE RERATE DATE. */
		/* Either way the correct result is stored as */
		/* WSAA-LAST-RRT-DATE. */
		// changed by yy
		Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
		datcon3Pojo.setIntDate1(premiumrec.effectdt.toString());
		datcon3Pojo.setIntDate2(premiumrec.termdate.toString());
		datcon3Pojo.setFrequency("01");
		datcon3Utils.calDatcon3(datcon3Pojo);
		String statuz = datcon3Pojo.getStatuz();
		if (isNE(statuz, Varcom.oK)) {
			drylogrec.statuz.set(statuz);
			drylogrec.params.set(datcon3Pojo.toString());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		compute(premiumrec.duration, 5).set(add(datcon3Pojo.getFreqFactor(), .99999));
		premiumrec.ratingdate.set(covrpf.getRerateFromDate());
		premiumrec.reRateDate.set(covrpf.getRerateDate());
		premiumrec.calcPrem.set(covrpf.getInstprem());
		premiumrec.calcBasPrem.set(covrpf.getZbinstprem());
		premiumrec.calcLoaPrem.set(covrpf.getZlinstprem());
		if (isEQ(covrpf.getPlanSuffix(), ZERO) && isNE(chdrlifIO.getPolinc(), 1)) {
			compute(premiumrec.sumin, 3).setRounded(div(covrpf.getSumins(), chdrlifIO.getPolsum()));
		} else {
			premiumrec.sumin.set(covrpf.getSumins());
		}
		compute(premiumrec.sumin, 3).setRounded(mult(premiumrec.sumin, chdrlifIO.getPolinc()));
		premiumrec.riskPrem.set(BigDecimal.ZERO); // ILIFE-9223
		if (isEQ(wsaaZsredtrm, "Y")) {
			/* MOVE COVR-INSTPREM TO CPRM-BASE-PREM */
			return;
		}
		premiumrec.function.set("CALC");
		/* Get any Annuity values required for the calculation. If the */
		/* coverage is not an annuity, initialise these values. */
		getAnny3280(covrpf);
		premiumrec.language.set(drypDryprcRecInner.drypLanguage);
		// ILIFE-8248
		if (lnkgFlag == true && "PMEX".equals(t5675rec.premsubr.toString().trim())) {

			if (null == covrpf.getLnkgsubrefno() || covrpf.getLnkgsubrefno().trim().isEmpty()) {
				premiumrec.lnkgSubRefNo.set(SPACE);
			} else {
				premiumrec.lnkgSubRefNo.set(covrpf.getLnkgsubrefno().trim());/* IJTI-1386 */
			}

			if (null == covrpf.getLnkgno() || covrpf.getLnkgno().trim().isEmpty()) {
				premiumrec.linkcov.set(SPACE);
			} else {
				LinkageInfoService linkgService = new LinkageInfoService();
				FixedLengthStringData linkgCov = new FixedLengthStringData(
						linkgService.getLinkageInfo(covrpf.getLnkgno().trim()));/* IJTI-1386 */
				premiumrec.linkcov.set(linkgCov);
			}
		}
		// ILIFE-8248
		/*
		 * Ticket #ILIFE-2005 - [Code Promotion to Life TRUNK Repo for VPMS
		 * externalization changes related to TRM calculation] Start
		 */
		/*
		 * Ticket #IVE-792 - Premium Calculation - Integration with latest PA compatible
		 * models Start
		 */
		// ILIFE-3142 [Code Promotion for VPMS externalization of LIFE ULP product
		// calculations]
		// ILIFE-8502
		premiumrec.commTaxInd.set("Y");
		List<Incrpf> incrpfRecords = incrpfDAO.getIncrpfList(covrpf.getChdrcoy(), covrpf.getChdrnum());
		if (incrpfRecords != null && !incrpfRecords.isEmpty()) {
			if (incrpfRecords.get(0).getLastSum().intValue() != 0) {
				premiumrec.prevSumIns.set(incrpfRecords.get(0).getLastSum().intValue());
			} else {
				premiumrec.prevSumIns.set(ZERO);
			}
		} else {
			premiumrec.prevSumIns.set(ZERO);
		}
		clntpf = clntpfDAO.searchClntRecord("CN", drypDryprcRecInner.drypFsuCompany.toString(), lifepf.getLifcnum());
		clntDao = DAOFactory.getClntpfDAO();
		clnt = clntDao.getClientByClntnum(lifepf.getLifcnum());
		if (null != clnt && null != clnt.getOldclntstatecd() && !clnt.getOldclntstatecd().isEmpty()) {
			premiumrec.stateAtIncep.set(clnt.getOldclntstatecd());
		} else {
			premiumrec.stateAtIncep.set(clntpf.getClntStateCd());/* IJTI-1386 */
		}

		String premiumSubroutine = t5675rec.premsubr.trim();
		if (!(AppVars.getInstance().getAppConfig().isVpmsEnable()
				&& (ExternalisedRules.isCallExternal(premiumSubroutine)
						|| "PMEX".equals(premiumSubroutine)))) {
			callProgram(t5675rec.premsubr, premiumrec.premiumRec);
		} else {
			Vpmcalcrec vpmcalcrec = new Vpmcalcrec();
			vpmcalcrec.linkageArea.set(premiumrec.premiumRec);
			Vpxlextrec vpxlextrec = new Vpxlextrec();
			vpxlextrec.function.set("INIT");
			callProgram(Vpxlext.class, vpmcalcrec.vpmcalcRec, vpxlextrec);

			vpxchdrPojo = vpxchdrUitl.callInit(vpmcalcrec);
			premiumrec.rstaflag.set(vpxchdrPojo.getRstaflag());
			premiumrec.cnttype.set(chdrlifIO.getCnttype());
			Vpxacblrec vpxacblrec = new Vpxacblrec();
			callProgram(Vpxacbl.class, premiumrec.premiumRec, vpxacblrec.vpxacblRec);
			// ILIFE-7123 : Start
			boolean dialdownFlag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPRP012", appVars,
					"IT");/* IJTI-1386 */
			boolean incomeProtectionflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP02", appVars,
					"IT");/* IJTI-1386 */
			boolean premiumflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), "NBPROP03", appVars,
					"IT");/* IJTI-1386 */
			/* IJTI-1386 START */
			if (incomeProtectionflag || premiumflag || dialdownFlag) {
				Rcvdpf rcvdPFObject = new Rcvdpf();
				rcvdPFObject.setChdrcoy(covrpf.getChdrcoy());
				rcvdPFObject.setChdrnum(covrpf.getChdrnum());
				rcvdPFObject.setLife(covrpf.getLife());
				rcvdPFObject.setCoverage(covrpf.getCoverage());
				rcvdPFObject.setRider(covrpf.getRider());
				rcvdPFObject.setCrtable(covrpf.getCrtable());
				RcvdpfDAO rcvdDAO = getApplicationContext().getBean("rcvdpfDAO", RcvdpfDAO.class);
				rcvdPFObject = rcvdDAO.readRcvdpf(rcvdPFObject);
				/* IJTI-1386 END */
				premiumrec.bentrm.set(rcvdPFObject.getBentrm());
				if (rcvdPFObject.getPrmbasis() != null && isEQ("S", rcvdPFObject.getPrmbasis())) {
					premiumrec.prmbasis.set("Y");
				} else {
					premiumrec.prmbasis.set("");
				}
				premiumrec.poltyp.set(rcvdPFObject.getPoltyp());
				premiumrec.occpcode.set("");
				premiumrec.dialdownoption.set(rcvdPFObject.getDialdownoption());
				premiumrec.waitperiod.set(rcvdPFObject.getWaitperiod());
			}
			// START OF ILIFE-7584
			premiumrec.cownnum.set(SPACES);
			premiumrec.occdate.set(ZERO);
			if (t5675rec.premsubr.toString().trim().equals("PMEX")) {
				premiumrec.setPmexCall.set("Y");
				premiumrec.inputPrevPrem.set(ZERO);// ILIFE-8537
				premiumrec.updateRequired.set("N");
				premiumrec.validflag.set("Y");
				premiumrec.cownnum.set(chdrlifIO.getCownnum());
				premiumrec.occdate.set(chdrlifIO.getOccdate());
				premiumrec.batcBatctrcde.set(drypDryprcRecInner.drypBatctrcde);

			}
			// END OF ILIFE-7584
			callProgram(t5675rec.premsubr, premiumrec.premiumRec, vpxlextrec, vpxacblrec.vpxacblRec);
		}
		/* Ticket #IVE-792 - End */

		/* Ticket #ILIFE-2005 - End */
		// ILIFE-7845
		premiumrec.riskPrem.set(BigDecimal.ZERO); // ILIFE-7922 by dpuhawan
		riskPremflag = FeaConfg.isFeatureExist(covrpf.getChdrcoy().trim(), RISKPREM_FEATURE_ID, appVars,
				"IT");/* IJTI-1386 */
		if (riskPremflag) {
			// premiumrec.riskPrem.set(ZERO); //ILIFE-7922 by dpuhawan
			premiumrec.cnttype.set(chdrlifIO.getCnttype());
			premiumrec.crtable.set(covrpf.getCrtable());
			premiumrec.calcTotPrem.set(add(premiumrec.calcPrem, premiumrec.zstpduty01));
			callProgram("RISKPREMIUM", premiumrec.premiumRec);

		}
		// ILIFE-7845 end
		if (isNE(premiumrec.statuz, Varcom.oK)) {
			drylogrec.params.set(premiumrec.premiumRec);
			drylogrec.statuz.set(premiumrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	private void getPayrpf() {
		payrIO = null;
		if (payrpfMap != null && payrpfMap.containsKey(chdrlifIO.getChdrnum())) {
			for (Payrpf c : payrpfMap.get(chdrlifIO.getChdrnum())) {
				if (c.getPayrseqno() == 1 && "1".equals(c.getValidflag())) {
					payrIO = c;
					break;
				}
			}
		}

		if (payrIO == null) {
			drylogrec.params.set(chdrlifIO.getChdrnum());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void calculateAnb3260(Lifepf lifelnb) {
		wsaaAnb.set(ZERO);
		AgecalcPojo agecalcPojo = new AgecalcPojo();
		agecalcPojo.setFunction("CALCP");
		agecalcPojo.setLanguage(drypDryprcRecInner.drypLanguage.toString());
		agecalcPojo.setCnttype(chdrlifIO.getCnttype());
		agecalcPojo.setIntDate1(Integer.toString(lifelnb.getCltdob()));
		agecalcPojo.setIntDate2(wsaaLastRrtDate.toString());
		agecalcPojo.setCompany(drypDryprcRecInner.drypFsuCompany.toString());
		agecalcUtils.calcAge(agecalcPojo);
		if (isNE(agecalcPojo.getStatuz(), Varcom.oK)) {
			drylogrec.params.set(agecalcPojo.toString());
			drylogrec.statuz.set(agecalcPojo.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaAnb.set(agecalcPojo.getAgerating());
	}

	protected void checkJointLife3270(Covrpf covrpf) {
		Lifepf lifelnb = lifepfDAO.getLifelnbRecord(covrpf.getChdrcoy(), covrpf.getChdrnum(), covrpf.getLife(),
				"01");/* IJTI-1386 */
		if (lifelnb != null) {
			wsaaT5675Item.set(t5687rec.jlPremMeth);
			premiumrec.jlsex.set(lifelnb.getCltsex());
			calculateAnb3260(lifelnb);
			premiumrec.jlage.set(wsaaAnb);
		} else {
			wsaaT5675Item.set(t5687rec.premmeth);
			premiumrec.jlsex.set(SPACES);
			premiumrec.jlage.set(ZERO);
		}
	}

	protected void getAnny3280(Covrpf covrpf) {
		Annypf annyIO = null;
		if (annyMap != null && annyMap.containsKey(covrpf.getChdrnum())) {
			for (Annypf a : annyMap.get(covrpf.getChdrnum())) {
				if (a.getChdrcoy().equals(covrpf.getChdrcoy()) && a.getLife().equals(covrpf.getLife())
						&& a.getCoverage().equals(covrpf.getCoverage()) && a.getRider().equals(covrpf.getRider())
						&& a.getPlanSuffix() == covrpf.getPlanSuffix()) {
					annyIO = a;
					break;
				}
			}
		}
		if (annyIO != null) {
			premiumrec.freqann.set(annyIO.getFreqann());
			premiumrec.advance.set(annyIO.getAdvance());
			premiumrec.arrears.set(annyIO.getArrears());
			premiumrec.guarperd.set(annyIO.getGuarperd());
			premiumrec.intanny.set(annyIO.getIntanny());
			premiumrec.capcont.set(annyIO.getCapcont());
			premiumrec.withprop.set(annyIO.getWithprop());
			premiumrec.withoprop.set(annyIO.getWithoprop());
			premiumrec.ppind.set(annyIO.getPpind());
			premiumrec.nomlife.set(annyIO.getNomlife());
			premiumrec.dthpercn.set(annyIO.getDthpercn());
			premiumrec.dthperco.set(annyIO.getDthperco());
		} else {
			premiumrec.advance.set(SPACES);
			premiumrec.arrears.set(SPACES);
			premiumrec.freqann.set(SPACES);
			premiumrec.withprop.set(SPACES);
			premiumrec.withoprop.set(SPACES);
			premiumrec.ppind.set(SPACES);
			premiumrec.nomlife.set(SPACES);
			premiumrec.guarperd.set(ZERO);
			premiumrec.intanny.set(ZERO);
			premiumrec.capcont.set(ZERO);
			premiumrec.dthpercn.set(ZERO);
			premiumrec.dthperco.set(ZERO);
		}
	}

	protected void pastCeaseDate3290(Covrpf covrpf) {
		Covrpf covrpfUpdate = new Covrpf(covrpf);
		covrpfUpdate.setCurrto(wsaaRerateStore.toInt());
		covrpfUpdate.setValidflag("2");
		covrpfListUpdt.add(covrpfUpdate);
		/* Write a new COVR with an incremented transaction number, a */
		/* new premium status code, and new current from and current to */
		/* dates. */
		compute(wsaaPremDiff, 3).setRounded(add(wsaaPremDiff, (mult(covrpf.getInstprem(), -1))));
		if (isEQ(chdrlifIO.getBillfreq(), "00")) {
			singlePrem3295(covrpf);
			return;
		}
		if (isEQ(covrpf.getCoverage(), "01") && isEQ(covrpf.getLife(), "01") && isEQ(covrpf.getRider(), "00")) {
			if (isNE(t5679rec.setCovPremStat, SPACES)) {
				covrpf.setPstatcode(t5679rec.setCovPremStat.toString());
			}
		} else {
			if (isNE(t5679rec.setRidPremStat, SPACES)) {
				covrpf.setPstatcode(t5679rec.setRidPremStat.toString());
			}
		}
		continue3296(covrpf);
	}

	protected void singlePrem3295(Covrpf covrpf) {
		if (isEQ(covrpf.getRider(), "00") || isEQ(covrpf.getRider(), "  ")) {
			if (isNE(t5679rec.setSngpCovStat, SPACES)) {
				covrpf.setPstatcode(t5679rec.setSngpCovStat.toString());
			}
		} else {
			if (isNE(t5679rec.setSngpRidStat, SPACES)) {
				covrpf.setPstatcode(t5679rec.setSngpRidStat.toString());
			}
		}
	}

	protected void continue3296(Covrpf covrpf) {
		Covrpf covrpfInsert = new Covrpf(covrpf);
		compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(), 1));
		covrpfInsert.setTranno(wsaaNewTranno.toInt());
		covrpfInsert.setCurrfrom(wsaaRerateStore.toInt());
		covrpfInsert.setCurrto(varcom.vrcmMaxDate.toInt());
		covrpfInsert.setValidflag("1");
		covrpfInsert.setRiskprem(premiumrec.riskPrem.getbigdata()); // ILIFE-7845
		covrpfListInst.add(covrpfInsert);

		ct04Val++;
	}

	protected void checkLexts3300(Covrpf covrpf) {
		Covrpf covrpfInsert = new Covrpf(covrpf);
		if (isNE(t5687rec.rtrnwfreq, NUMERIC) || isEQ(t5687rec.rtrnwfreq, 0)) {
			t5687rec.rtrnwfreq.set(ZERO);
			wsaaRateFrom.set(covrpf.getPremCessDate());
		} else {
			Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(t5687rec.rtrnwfreq.toInt());
			datcon2Pojo.setIntDate1(wsaaLastRrtDate.toString());
			datcon2Pojo.setFrequency("01");
			datcon2Utils.calDatcon2(datcon2Pojo);

			if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
				drylogrec.statuz.set(datcon2Pojo.getStatuz());
				drylogrec.params.set(datcon2Pojo.toString());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			wsaaRateFrom.set(datcon2Pojo.getIntDate2());
		}

		if (lextpfMap != null && lextpfMap.containsKey(covrpf.getChdrnum())) {
			for (Lextpf c : lextpfMap.get(covrpf.getChdrnum())) {
				if (isNE(c.getChdrcoy(), covrpf.getChdrcoy()) || isNE(c.getChdrnum(), covrpf.getChdrnum())
						|| isNE(c.getRider(), covrpf.getRider()) || isNE(c.getCoverage(), covrpf.getCoverage())
						|| isNE(c.getLife(), covrpf.getLife())) {
					break; // ILIFE-7577
				}
				if (isLT(c.getExtCessDate(), wsaaRateFrom) && isGT(c.getExtCessDate(), covrpf.getRerateDate())) {
					wsaaRateFrom.set(c.getExtCessDate());
				}
			}
		}
		covrpfInsert.setRerateDate(wsaaRateFrom.toInt());
		covrpfInsert.setRerateFromDate(covrpf.getRerateDate());
		checkMgp3360(covrpfInsert);
		if (isNE(t5687rec.premmeth, SPACES)) {
			if (!prmhldtrad || (covrpf.getReinstated() == null || !"Y".equals(covrpf.getReinstated()))) {// ILIFE-8509
				if (!stampDutyflag)
					compute(wsaaInstPrem, 3).setRounded(div(premiumrec.calcPrem, chdrlifIO.getPolinc()));
				else
					compute(wsaaInstPrem, 3)
							.setRounded(div(add(premiumrec.calcPrem, premiumrec.zstpduty01), chdrlifIO.getPolinc()));
				compute(wsaaZbinstprem, 3).setRounded(div(premiumrec.calcBasPrem, chdrlifIO.getPolinc()));
				compute(wsaaZlinstprem, 3).setRounded(div(premiumrec.calcLoaPrem, chdrlifIO.getPolinc()));
				if (isEQ(covrpf.getPlanSuffix(), 0) && isNE(chdrlifIO.getPolinc(), 1)) {
					compute(wsaaInstPrem, 3).setRounded(mult(wsaaInstPrem, chdrlifIO.getPolsum()));
					compute(wsaaZbinstprem, 3).setRounded(mult(wsaaZbinstprem, chdrlifIO.getPolsum()));
					compute(wsaaZlinstprem, 3).setRounded(mult(wsaaZlinstprem, chdrlifIO.getPolsum()));
				}
				if (isNE(wsaaInstPrem, 0)) {
					zrdecplcPojo.setAmountIn(wsaaInstPrem.getbigdata());
					callRounding6000();
					wsaaInstPrem.set(zrdecplcPojo.getAmountOut());
				}
				if (isNE(wsaaZbinstprem, 0)) {
					zrdecplcPojo.setAmountIn(wsaaZbinstprem.getbigdata());
					callRounding6000();
					wsaaZbinstprem.set(zrdecplcPojo.getAmountOut());
				}
				if (isNE(wsaaZlinstprem, 0)) {
					zrdecplcPojo.setAmountIn(wsaaZlinstprem.getbigdata());
					callRounding6000();
					wsaaZlinstprem.set(zrdecplcPojo.getAmountOut());
				}
				compute(wsaaPremDiff, 3).setRounded(add(wsaaPremDiff, (sub(wsaaInstPrem, covrpf.getInstprem()))));
				if (isNE(wsaaZsredtrm, "Y")) {
					covrpfInsert.setInstprem(wsaaInstPrem.getbigdata());
				}
				covrpfInsert.setInstprem(wsaaInstPrem.getbigdata());
				covrpfInsert.setZbinstprem(wsaaZbinstprem.getbigdata());
				covrpfInsert.setZlinstprem(wsaaZlinstprem.getbigdata());
			}

			checkForIncrease3800(covrpfInsert);
			if (isGT(covrpfInsert.getRerateDate(), covrpfInsert.getPremCessDate())) {
				covrpfInsert.setRerateDate(covrpfInsert.getPremCessDate());
			}
			if (isGT(covrpfInsert.getRerateFromDate(), covrpfInsert.getPremCessDate())) {
				covrpfInsert.setRerateFromDate(covrpfInsert.getPremCessDate());
			}
			if (isEQ(wsaaZsredtrm, "Y")) {
				m100CheckReducing(covrpfInsert);
				m200UpdateUndr(covrpfInsert);
				covrpfInsert.setSumins(wsaaSumins.getbigdata());
			}
			covrpfInsert.setValidflag("1");
			compute(wsaaNewTranno, 0).set(add(chdrlifIO.getTranno(), 1));
			covrpfInsert.setTranno(wsaaNewTranno.toInt());
			covrpfInsert.setCurrfrom(wsaaRerateStore.toInt());
			covrpfInsert.setCurrto(varcom.vrcmMaxDate.toInt());
			if (!prmhldtrad || (covrpf.getReinstated() == null || !"Y".equals(covrpf.getReinstated()))) {// ILIFE-8509
				covrpfInsert.setRiskprem(premiumrec.riskPrem.getbigdata()); // ILIFE-7845
				if (stampDutyflag) {
					covrpfInsert.setZstpduty01(premiumrec.zstpduty01.getbigdata());
					covrpfInsert.setZclstate(premiumrec.rstate01.toString());
				}
			} else if (prmhldtrad && "Y".equals(covrpf.getReinstated()))// ILIFE-8509
				covrpfInsert.setRiskprem(BigDecimal.ZERO);
			covrpfListInst.add(covrpfInsert);

			ct02Val++;
		}
	}

	protected void checkMgp3360(Covrpf covrpf) {
		if (isEQ(t5687rec.premGuarPeriod, 0)) {
			covrpf.setRerateFromDate(covrpf.getRerateDate());
		} else {
			// changed by yy
			Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor(t5687rec.premGuarPeriod.toInt());
			datcon2Pojo.setIntDate1(Integer.toString(covrpf.getCrrcd()));
			datcon2Pojo.setFrequency("01");
			datcon2Utils.calDatcon2(datcon2Pojo);

			if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
				drylogrec.statuz.set(datcon2Pojo.getStatuz());
				drylogrec.params.set(datcon2Pojo.toString());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}

			if (isLT(datcon2Pojo.getIntDate2(), covrpf.getRerateDate())) {
				covrpf.setRerateFromDate(covrpf.getRerateDate());
			} else {
				covrpf.setRerateFromDate(covrpf.getCrrcd());
			}
		}

	}

	protected void setRerateType3410(Covrpf covrpf) {
		wsaaDurationInt.set(ZERO);
		if (isNE(t5687rec.rtrnwfreq, NUMERIC) || isEQ(t5687rec.rtrnwfreq, 0)) {
			wsaaRerateType.set("2");
		} else {
			// changed by yy
			Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
			datcon3Pojo.setIntDate1(Integer.toString(covrpf.getCrrcd()));
			datcon3Pojo.setIntDate2(Integer.toString(covrpf.getRerateDate()));
			datcon3Pojo.setFrequency("01");
			datcon3Utils.calDatcon3(datcon3Pojo);
			String statuz = datcon3Pojo.getStatuz();
			if (isNE(statuz, Varcom.oK)) {
				drylogrec.statuz.set(statuz);
				drylogrec.params.set(datcon3Pojo.toString());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}

			compute(wsaaDurationInt, 5).setDivide(datcon3Pojo.getFreqFactor().intValue(), (t5687rec.rtrnwfreq));
			wsaaDurationRem.setRemainder(wsaaDurationInt);
			if (isNE(wsaaDurationRem, 0)) {
				checkLextRerate3420(covrpf);
			}
		}
		if (lextRerate.isTrue()) {
			Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
			datcon2Pojo.setFreqFactor((mult(t5687rec.rtrnwfreq, wsaaDurationInt)).toInt());
			datcon2Pojo.setIntDate1(Integer.toString(covrpf.getCrrcd()));
			datcon2Pojo.setFrequency("01");
			datcon2Utils.calDatcon2(datcon2Pojo);

			if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
				drylogrec.statuz.set(datcon2Pojo.getStatuz());
				drylogrec.params.set(datcon2Pojo.toString());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			wsaaLastRrtDate.set(datcon2Pojo.getIntDate2());
		} else {
			wsaaLastRrtDate.set(covrpf.getRerateDate());
		}
	}

	protected void checkLextRerate3420(Covrpf covrpf) {
		Lextpf lextbrr = new Lextpf();
		/* IJTI-1386 START */
		lextbrr.setChdrcoy(covrpf.getChdrcoy());
		lextbrr.setChdrnum(covrpf.getChdrnum());
		lextbrr.setLife(covrpf.getLife());
		lextbrr.setCoverage(covrpf.getCoverage());
		lextbrr.setRider(covrpf.getRider());
		/* IJTI-1386 END */
		lextbrr.setExtCessDate(covrpf.getRerateDate());
		lextbrr = lextpfDAO.getLextbrrRecord(lextbrr);
		if (lextbrr != null) {
			wsaaRerateType.set("2");
		}
	}

	protected void m100CheckReducing(Covrpf covrpf) {

		Datcon3Pojo datcon3Pojo = new Datcon3Pojo();
		datcon3Pojo.setIntDate1(chdrlifIO.getOccdate().toString());
		datcon3Pojo.setIntDate2(Integer.toString(covrpf.getRerateDate()));
		datcon3Pojo.setFrequency("01");
		datcon3Utils.calDatcon3(datcon3Pojo);
		String statuz = datcon3Pojo.getStatuz();
		if (isNE(statuz, Varcom.oK)) {
			drylogrec.statuz.set(statuz);
			drylogrec.params.set(datcon3Pojo.toString());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		compute(wsaaDuration, 5).set(add(datcon3Pojo.getFreqFactor(), .99999));
		Mbnspf mbnspf = mbnsDAO.getmbnsRecord(covrpf.getChdrcoy().trim(), covrpf.getChdrnum().trim(),
				covrpf.getLife().trim(), covrpf.getCoverage().trim(), covrpf.getRider().trim(),
				wsaaDuration.toInt());/* IJTI-1386 */

		if (mbnspf != null) {
			wsaaSumins.set(mbnspf.getSumins());
		}
	}

	protected void m200UpdateUndr(Covrpf covrpf) {
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifepf.getLifcnum());
		crtundwrec.coy.set(covrpf.getChdrcoy());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.chdrnum.set(covrpf.getChdrnum());
		crtundwrec.crtable.set(covrpf.getCrtable());
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.function.set("DEL");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, Varcom.oK)) {
			drylogrec.params.set(crtundwrec.parmRec);
			drylogrec.statuz.set(crtundwrec.status);
			drylogrec.iomod.set("CRTUNDWRT");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		initialize(crtundwrec.parmRec);
		crtundwrec.clntnum.set(lifepf.getLifcnum());
		crtundwrec.coy.set(covrpf.getChdrcoy());
		crtundwrec.chdrnum.set(covrpf.getChdrnum());
		crtundwrec.life.set(covrpf.getLife());
		crtundwrec.crtable.set(covrpf.getCrtable());
		crtundwrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		crtundwrec.sumins.set(wsaaSumins);
		crtundwrec.cnttyp.set(chdrlifIO.getCnttype());
		crtundwrec.currcode.set(chdrlifIO.getCntcurr());
		crtundwrec.function.set("ADD");
		callProgram(Crtundwrt.class, crtundwrec.parmRec);
		if (isNE(crtundwrec.status, Varcom.oK)) {
			drylogrec.params.set(crtundwrec.parmRec);
			drylogrec.statuz.set(crtundwrec.status);
			drylogrec.iomod.set("CRTUNDWRT");
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void commit3500() {
		commitControlTotals();
		chdrpfDAO.updateInvalidChdrRecord(chdrpfListUpdt);
		chdrpfListUpdt.clear();
		chdrpfDAO.insertChdrAmt(chdrpfListInst);
		chdrpfListInst.clear();

		payrpfDAO.updatePayrRecord(payrpfListUpdt);
		payrpfListUpdt.clear();
		payrpfDAO.insertPayrpfList(payrpfListInst);
		updatePayrCustomerSpecific(payrpfListInst);
		payrpfListInst.clear();

		incrpfDAO.insertIncrList(incrpfList);
		incrpfList.clear();
		insertCovrCustomerspecific();

		ptrnpfDAO.insertPtrnPF(ptrnpfList);
		ptrnpfList.clear();

		br613TempDAO.updateAgcmRecord(agcmpfListUpdt);
		agcmpfListUpdt.clear();
		br613TempDAO.insertAgcmRecord(agcmppfListInst);
		agcmppfListInst.clear();
		close4000();

	}

	protected void updateAgcm3700(Covrpf covrpf, List<Agcmpf> agcmList, Dryr613Dto dryr613dto) {
		for (Agcmpf agcmpf : agcmList) {
			if (agcmpf.getChdrnum().equals(covrpf.getChdrnum()) && agcmpf.getLife().equals(covrpf.getLife())
					&& agcmpf.getRider().equals(covrpf.getRider()) && agcmpf.getCoverage().equals(covrpf.getCoverage())
					&& agcmpf.getPlanSuffix() == covrpf.getPlanSuffix()) {
				agcmUpdateLoop3710(covrpf, agcmpf, dryr613dto);
				return;
			}
		}
	}

	protected void agcmUpdateLoop3710(Covrpf covrpf, Agcmpf agcmpf, Dryr613Dto dryr613dto) {
		if (isNE(agcmpf.getDormantFlag(), "Y")) {
			Agcmpf agcmpfUpdate = new Agcmpf(agcmpf);
			agcmpfUpdate.setCurrto(wsaaRerateStore.toInt());
			agcmpfUpdate.setValidflag("2");
			agcmpfListUpdt.add(agcmpfUpdate);
			writeNewAgcm3720(covrpf, agcmpf, dryr613dto);
		}
	}

	protected void writeNewAgcm3720(Covrpf covrpf, Agcmpf agcmpf, Dryr613Dto dryr613dto) {
		Agcmpf agcmpfInsert = new Agcmpf(agcmpf);
		agcmpfInsert.setTranno(wsaaNewTranno.toInt());
		agcmpfInsert.setCurrfrom(wsaaRerateStore.toInt());
		agcmpfInsert.setCurrto(varcom.vrcmMaxDate.toInt());
		agcmpfInsert.setValidflag("1");
		if (isEQ(covrpf.getPremCessDate(), covrpf.getRerateDate())) {
			/* CONTINUE_STMT */
		} else {
			agcmpfInsert.setAnnprem(agentAnnprem3730(covrpf, agcmpfInsert, dryr613dto));
		}
		agcmppfListInst.add(agcmpfInsert);

	}

	protected BigDecimal agentAnnprem3730(Covrpf covrpf, Agcmpf agcmpf, Dryr613Dto dryr613dto) {
		wsaaAgntnum.set(agcmpf.getAgntnum());
		if (isEQ(agcmpf.getOvrdcat(), "O")) {
			wsaaAgntFound.set("N");
			wsaaCedagent.set(agcmpf.getCedagent());
			while (!(agentFound.isTrue())) {
				findBasicAgent3740(agcmpf);
			}

		}
		List<Pcddpf> pcddpfList = pcddpfMap.get(dryr613dto.getChdrNum());
		for (Pcddpf pcddpf : pcddpfList) {
			if (pcddpf.getAgntNum().equals(wsaaAgntnum.toString())) {
				wsaaAnnprem.set(ZERO);
				wsaaAnnprem.set(agcmpf.getAnnprem());
				if (isNE(t5687rec.zrrcombas, SPACES)) {
					setPrecision(wsaaAnnprem, 3);
					wsaaAnnprem.setRounded(
							mult((mult(covrpf.getZbinstprem(), wsaaBillfreqn)), (div(pcddpf.getSplitC(), 100))));
				} else {
					setPrecision(wsaaAnnprem, 3);
					wsaaAnnprem.setRounded(
							mult((mult(covrpf.getInstprem(), wsaaBillfreqn)), (div(pcddpf.getSplitC(), 100))));
				}
				/* MOVE AGCMBCH-ANNPREM TO ZRDP-AMOUNT-IN. */
				/* PERFORM 6000-CALL-ROUNDING. */
				/* MOVE ZRDP-AMOUNT-OUT TO AGCMBCH-ANNPREM. */
				if (isNE(wsaaAnnprem, 0)) {
					zrdecplcPojo.setAmountIn(agcmpf.getAnnprem());
					callRounding6000();
					wsaaAnnprem.set(zrdecplcPojo.getAmountIn());
				}
				return wsaaAnnprem.getbigdata();
			}
		}
		if (pcddpfList.isEmpty()) {
			drylogrec.params.set("PCDDPF");
			drylogrec.statuz.set(Varcom.mrnf);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		return null;
	}

	protected void findBasicAgent3740(Agcmpf agcmpf) {
		agcmpf.setAgntnum(wsaaCedagent.toString());
		Agcmpf agcm = agcmpfDAO.searchAgcmRecord(agcmpf);
		if (agcm != null) {
			if (isEQ(agcm.getOvrdcat(), "B")) {
				wsaaAgntFound.set("Y");
				wsaaAgntnum.set(agcm.getAgntnum());
			} else {
				wsaaCedagent.set(agcm.getCedagent());
			}
		} else {
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void checkForIncrease3800(Covrpf covrpf) {
		Incrpf incrhstIO = null;
		if (incrpfMap != null && incrpfMap.containsKey(covrpf.getChdrnum())) {
			for (Incrpf c : incrpfMap.get(covrpf.getChdrnum())) {
				/* IJTI-1386 START */
				if (covrpf.getChdrcoy().equals(c.getChdrcoy()) && covrpf.getLife().equals(c.getLife())
						&& covrpf.getCoverage().equals(c.getCoverage()) && covrpf.getRider().equals(c.getRider())
						/* IJTI-1386 END */
						&& covrpf.getPlanSuffix() == c.getPlnsfx() && "2".equals(c.getValidflag())
						&& "".equals(c.getRefusalFlag().trim())) {
					incrhstIO = c;
					break;
				}
			}
		}

		if (incrhstIO == null) {
			return;
		}

		/* Write a new historical INCR record based on the details of */
		/* the last record. The main changes from the previous record */
		/* are that the LASTINST comes from the NEWINST on the previous */
		/* record and that the increase in premium caused by the */
		/* rerate is added to the NEWINST to give the 'new' NEWINST. */
		/* The TRANNO and date/time fields are also updated. */
		incrhstIO.setLastInst(incrhstIO.getNewinst());
		incrhstIO.setZblastinst(incrhstIO.getZbnewinst());
		incrhstIO.setZllastinst(incrhstIO.getZlnewinst());
		setPrecision(incrhstIO.getNewinst(), 2);
		incrhstIO.setNewinst(
				add(incrhstIO.getNewinst(), (sub(covrpf.getInstprem(), covrpf.getInstprem()))).getbigdata());
		setPrecision(incrhstIO.getZbnewinst(), 2);
		incrhstIO.setZbnewinst(
				add(incrhstIO.getZbnewinst(), (sub(covrpf.getZbinstprem(), covrpf.getZbinstprem()))).getbigdata());
		setPrecision(incrhstIO.getZlnewinst(), 2);
		incrhstIO.setZlnewinst(
				add(incrhstIO.getZlnewinst(), (sub(covrpf.getZlinstprem(), covrpf.getZlinstprem()))).getbigdata());
		setPrecision(incrhstIO.getTranno(), 0);
		incrhstIO.setTranno(add(chdrlifIO.getTranno(), 1).toInt());
		incrhstIO.setStatcode(covrpf.getStatcode());/* IJTI-1386 */
		incrhstIO.setPstatcode(covrpf.getPstatcode());/* IJTI-1386 */
		incrhstIO.setCrrcd(drypDryprcRecInner.drypRunDate.toInt());
		incrhstIO.setTransactionDate(TRAN_DATE);
		incrhstIO.setTransactionTime(TRAN_TIME);
		incrhstIO.setTermid(TERMINAL_ID);
		incrhstIO.setUser(USER);
		setPrecision(incrhstIO.getZstpduty01(), 2);
		incrhstIO.setZstpduty01(
				add(incrhstIO.getNewinst(), (sub(covrpf.getZstpduty01(), covrpf.getZstpduty01()))).getbigdata());
		incrpfList.add(incrhstIO);
	}

	protected void genericProcessing3900(Covrpf covrpf) {
		wsaaT5671Item.set(SPACES);
		wsaaItemTranCode.set(drypDryprcRecInner.drypBatctrcde);
		wsaaItemCrtable.set(covrpf.getCrtable());

		List<Itempf> t5671List = t5671ListMap.get(wsaaT5671Item.toString());
		if (t5671List != null && wsaaT5671Item.length() != 8) {
			drylogrec.params.set("T5671" + wsaaT5671Item);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (t5671List == null || t5671List.size() == 0) {
			t5671rec.t5671Rec.set(SPACES);
		} else {
			t5671rec.t5671Rec.set(StringUtil.rawToString(t5671List.get(0).getGenarea()));
		}
		isuallrec.isuallRec.set(SPACES);
		isuallrec.function.set("ACTIN");
		isuallrec.company.set(covrpf.getChdrcoy());
		isuallrec.chdrnum.set(covrpf.getChdrnum());
		isuallrec.life.set(covrpf.getLife());
		isuallrec.coverage.set(covrpf.getCoverage());
		isuallrec.rider.set(covrpf.getRider());
		isuallrec.planSuffix.set(covrpf.getPlanSuffix());
		isuallrec.oldcovr.set(covrpf.getCoverage());
		isuallrec.oldrider.set(covrpf.getRider());
		if (isEQ(wsaaFullyPaid, "Y")) {
			isuallrec.freqFactor.set(0);
		} else {
			isuallrec.freqFactor.set(1);
		}
		isuallrec.batchkey.set(drypDryprcRecInner.drypBatchKey);
		isuallrec.transactionDate.set(TRAN_DATE);
		isuallrec.transactionTime.set(TRAN_TIME);
		isuallrec.user.set(USER);
		isuallrec.termid.set(TERMINAL_ID);
		isuallrec.effdate.set(covrpf.getCrrcd());
		isuallrec.newTranno.set(covrpf.getTranno());
		isuallrec.covrSingp.set(ZERO);
		isuallrec.covrInstprem.set(covrpf.getInstprem());
		isuallrec.language.set(drypDryprcRecInner.drypLanguage);
		if (isGT(chdrlifIO.getPolinc(), 1)) {
			isuallrec.convertUnlt.set("Y");
		}
		for (wsaaSub.set(1); !(isGT(wsaaSub, 4)); wsaaSub.add(1)) {
			subroutineCall3950();
		}
	}

	protected void subroutineCall3950() {
		/* CALL */
		isuallrec.statuz.set(Varcom.oK);
		if (isNE(t5671rec.subprog[wsaaSub.toInt()], SPACES)) {
			callProgram(t5671rec.subprog[wsaaSub.toInt()], isuallrec.isuallRec);
		}
		if (isNE(isuallrec.statuz, Varcom.oK)) {
			drylogrec.params.set(isuallrec.isuallRec);
			drylogrec.statuz.set(isuallrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		/* EXIT */
	}

	protected void close4000() {
		chdrpfMap.clear();
		chdrpfMap = null;
		payrpfMap.clear();
		payrpfMap = null;
		incrpfMap.clear();
		incrpfMap = null;
		ptrnpfList.clear();
		ptrnpfList = null;
		chdrpfListUpdt.clear();
		chdrpfListUpdt = null;
		chdrpfListInst.clear();
		chdrpfListInst = null;
		payrpfListUpdt.clear();
		payrpfListUpdt = null;
		payrpfListInst.clear();
		payrpfListInst = null;
		incrpfList.clear();
		incrpfList = null;
		agcmpfListUpdt.clear();
		agcmpfListUpdt = null;

		agcmppfListInst.clear();
		agcmppfListInst = null;
	}

	protected void updateChdrPtrn4100() {
		Ptrnpf ptrnIO = new Ptrnpf();
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy().toString());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(wsaaNewTranno.toInt());
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate.toInt());
		ptrnIO.setTrdt(TRAN_DATE);
		ptrnIO.setTrtm(TRAN_TIME);
		ptrnIO.setUserT(USER);
		ptrnIO.setTermid(TERMINAL_ID);
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx.toString());
		ptrnIO.setBatccoy(drypDryprcRecInner.drypCompany.toString());
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBranch.toString());
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr.toInt());
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn.toInt());
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch.toString());
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate.toInt());
		ptrnpfList.add(ptrnIO);
		Chdrpf chdrpf = new Chdrpf(chdrlifIO);
		chdrpf.setValidflag('2');
		chdrpf.setCurrto(wsaaRerateStore.toInt());
		chdrpf.setUniqueNumber(chdrlifIO.getUniqueNumber());
		chdrpfListUpdt.add(chdrpf);
		chdrlifIO.setValidflag('1');
		chdrlifIO.setCurrto(varcom.vrcmMaxDate.toInt());
		chdrlifIO.setCurrfrom(wsaaRerateStore.toInt());
		chdrlifIO.setTranno(wsaaNewTranno.toInt());
		// ILIFE-8763
		if (isNE(t5679rec.setCnPremStat, SPACES)) {
			chdrlifIO.setPstcde(t5679rec.setCnPremStat.toString());
		}
		// ILIFE-8763
		setChdrCustomerSpecific();
		chdrpfListInst.add(chdrlifIO);
		ct03Val.add(wsaaPremDiff);
	}

	protected void allCovrsExpired4150() {
		/* START */
		compute(wsaaPremDiff, 2).set(sub(wsaaPremDiff, chdrlifIO.getSinstamt02()));
		chdrlifIO.setSinstamt02(BigDecimal.ZERO);
		if (isNE(t5679rec.setCnPremStat, SPACES)) {
			chdrlifIO.setPstcde(t5679rec.setCnPremStat.toString());
		}
		/* EXIT */
	}

	protected void updatePayr4200() {
		getPayrpf(); // ILIFE-4323 by dpuhawan
		Payrpf payrpf = new Payrpf(payrIO);
		payrpf.setValidflag("2");
		payrpfListUpdt.add(payrpf);
		payrIO.setValidflag("1");
		payrIO.setEffdate(wsaaRerateStore.toInt());
		payrIO.setTranno(wsaaNewTranno.toInt());

		setPayrCustomerSpecific();
		Datcon2Pojo datcon2Pojo = new Datcon2Pojo();
		datcon2Pojo.setFreqFactor(mult(wsaaLeadDays, -1).toInt());
		datcon2Pojo.setIntDate1(chdrlifIO.getBillcd().toString());
		datcon2Pojo.setFrequency("DY");
		datcon2Utils.calDatcon2(datcon2Pojo);

		if (!Varcom.oK.toString().equals(datcon2Pojo.getStatuz())) {
			drylogrec.statuz.set(datcon2Pojo.getStatuz());
			drylogrec.params.set(datcon2Pojo.toString());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		payrIO.setNextdate(Integer.parseInt(datcon2Pojo.getIntDate2()));
		payrpfListInst.add(payrIO);
	}

	protected void statistics5000() {
		lifsttrrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifsttrrec.batcbrn.set(drypDryprcRecInner.drypBranch);
		lifsttrrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifsttrrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifsttrrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifsttrrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifsttrrec.chdrcoy.set(chdrlifIO.getChdrcoy());
		lifsttrrec.chdrnum.set(chdrlifIO.getChdrnum());
		lifsttrrec.tranno.set(wsaaNewTranno);
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, Varcom.oK)) {
			drylogrec.params.set(lifsttrrec.lifsttrRec);
			drylogrec.statuz.set(lifsttrrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void callRounding6000() {
		/* CALL */
		zrdecplcPojo.setFunction(SPACES.toString());
		zrdecplcPojo.setStatuz(Varcom.oK.toString());
		zrdecplcPojo.setCompany(drypDryprcRecInner.drypCompany.toString());
		zrdecplcPojo.setCurrency(chdrlifIO.getCntcurr());/* IJTI-1386 */
		zrdecplcPojo.setBatctrcde(drypDryprcRecInner.drypBatctrcde.toString());
		zrdecplcUtils.calcZrdecplc(zrdecplcPojo);
		if (isNE(zrdecplcPojo.getStatuz(), Varcom.oK)) {
			drylogrec.statuz.set(zrdecplcPojo.getStatuz());
			drylogrec.params.set(zrdecplcPojo.toString());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* EXIT */
	}

	private void commitControlTotals() {
		drycntrec.contotNumber.set(CT01);
		drycntrec.contotValue.set(ct01Val);
		d000ControlTotals();
		ct01Val = 0;

		drycntrec.contotNumber.set(CT05);
		drycntrec.contotValue.set(ct05Val);
		d000ControlTotals();
		ct05Val = 0;

		drycntrec.contotNumber.set(CT04);
		drycntrec.contotValue.set(ct04Val);
		d000ControlTotals();
		ct04Val = 0;

		drycntrec.contotNumber.set(CT02);
		drycntrec.contotValue.set(ct02Val);
		d000ControlTotals();
		ct02Val = 0;

		drycntrec.contotNumber.set(CT03);
		drycntrec.contotValue.set(wsaaPremDiff);
		d000ControlTotals();
		ct03Val.set(ZERO);
	}

	protected void insertCovrCustomerspecific() {
		br613TempDAO.updateCovrRecord(covrpfListUpdt);
		covrpfListUpdt.clear();
		br613TempDAO.insertCovrList(covrpfListInst);
		covrpfListInst.clear();
	}

	protected void setChdrCustomerSpecific() {
		setPrecision(chdrlifIO.getSinstamt01(), 2);
		chdrlifIO.setSinstamt01(add(chdrlifIO.getSinstamt01(), wsaaPremDiff).getbigdata());
		if (isEQ(wsaaCovrsLive, "N")) {
			allCovrsExpired4150();
		}
		setPrecision(chdrlifIO.getSinstamt06(), 2);
		chdrlifIO.setSinstamt06(add(chdrlifIO.getSinstamt06(), wsaaPremDiff).getbigdata());

	}

	protected void updatePayrCustomerSpecific(List<Payrpf> payrpfListInst) {
		// default blank implementation for customer extension
	}

	protected void setPayrCustomerSpecific() {
		setPrecision(payrIO.getSinstamt06(), 2);
		payrIO.setSinstamt06(add(payrIO.getSinstamt06(), wsaaPremDiff).getbigdata());
		if (isEQ(wsaaCovrsLive, "N")) {
			payrIO.setSinstamt02(BigDecimal.ZERO);
			payrIO.setSinstamt01(BigDecimal.ZERO);
		} else {
			setPrecision(payrIO.getSinstamt01(), 2);
			payrIO.setSinstamt01(add(payrIO.getSinstamt01(), wsaaPremDiff).getbigdata());
		}
	}

	private void updateDiaryData() {
		drypDryprcRecInner.drypBillcd.set(chdrlifIO.getBillcd());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypBillfreq.set(chdrlifIO.getBillfreq());
		drypDryprcRecInner.drypBillchnl.set(chdrlifIO.getBillchnl());
		drypDryprcRecInner.drypBtdate.set(chdrlifIO.getBtdate());
		drypDryprcRecInner.drypPtdate.set(chdrlifIO.getPtdate());
		drypDryprcRecInner.drypTranno.set(chdrlifIO.getTranno());
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
		drypDryprcRecInner.drypOccdate.set(chdrlifIO.getOccdate());
		drypDryprcRecInner.drypStatcode.set(chdrlifIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrlifIO.getPstcde());
		drypDryprcRecInner.drypStmdte.set(chdrlifIO.getStmdte());
		drypDryprcRecInner.drypStatuz.set(Varcom.oK);
	}

	/*
	 * Class transformed from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {

		// ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		// cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		// ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0,
				REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	}
}