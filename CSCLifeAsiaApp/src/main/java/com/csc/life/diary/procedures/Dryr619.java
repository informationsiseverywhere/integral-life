/*
 * File: Dryr619.java
 * Date: March 26, 2014 3:07:06 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRYR619.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.diary.dataaccess.dao.Dryr619DAO;
import com.csc.life.diary.dataaccess.model.Dryr619Dto;
import com.csc.life.productdefinition.dataaccess.dao.CovrpfDAO;
import com.csc.life.productdefinition.dataaccess.dao.impl.CovrpfDAOImpl;
import com.csc.life.productdefinition.dataaccess.model.Covrpf;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.regularprocessing.recordstructures.Annprocrec;
import com.csc.life.regularprocessing.tablestructures.T6658rec;
import com.csc.life.statistics.procedures.Lifsttr;
import com.csc.life.statistics.recordstructures.Lifsttrrec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
 * <pre>
 *
 *(c) Copyright Continuum Corporation Ltd.  1986....1995.
 *    All rights reserved.  Continuum Confidential.
 *
 *REMARKS.
 *
 *   DIARY/400: Anniversary Renewals Processing
 *   ------------------------------------------
 *
 *   This is the Anniversary Renewal Processing Subroutine written
 *   for DIARY/400.
 *   This subroutine replicates the processing carried out by the
 *   'Old Style' Batch Program - B140. This subroutine will be
 *   called per component.
 *
 *   100 - This is the main processing section called from MAIND.
 *
 *         For each invocation of this subroutine:
 *
 *             Perform 200-INITIALISE
 *             Set up Key to read COVRRNW using a BEGN
 *             Perform 300-PROCESS-COMPONENTS UNTIL ENDP
 *             Perform 1000-FINISH
 *
 *         Exit Subroutine
 *
 *   200 - Initialises Working Storage Variables and does
 *         Component level statii checks.
 *
 *   300 - This section loops around the Coverage and associated
 *         Riders. If there's a change of key or a change of
 *         Anniversary Renewals Processing date then the loop is
 *         terminated.
 *
 *         For each COVRRNW read:
 *
 *             Perform 400-TABLES
 *             Perform 500-CALLANNPROC
 *             Perform 600-SHIFTDATE
 *             Perform 700-REWRITE-COVR
 *             Move NEXTR to COVRRNW-FUNCTION
 *         Exit.
 *
 *   400 - Read T5679 - Coverage Statii
 *         Read T5687 - Get Anniversary Method
 *         Read T6658 - Generic Anniversary Processing Subroutine
 *         Read T5540 - Get Initial Units Discount Basis
 *         Read T5519 - Get relevant processing details
 *
 *   500 - Fill all ANNP-ANNPLL-REC Linkage fields
 *         Call the Generic Anniversary Processing Subroutine
 *
 *   600 - Calculate the next CBANPR before incrementing one year
 *
 *   700 - Rewrite the Coverage with the new date
 *
 *   1000- This will usually contain code that passes back any
 *         fields needed by DRYPROCES to call DRYANNRNW.
 *         However we only need to pass back Company and Entity
 *         which have already been filled. DRYP-STATUZ was set to
 *         O-K in 200- section. Therefore this section is blank.
 *
 *   Control Totals:
 *   ---------------
 *
 *   CT01 - COVR Records Read
 *   CT02 - Invalid Status on COVR
 *   CT03 - Valid Status on COVR
 *   CT04 - No Anniversary Method
 *   CT05 - No Anniversary Subroutine
 *   CT06 - Non U/L Product
 *   CT07 - No Initial Units Discount Basis
 *   CT08 - No Initial Unit Charge Freq
 *   CT09 - COVR Records updated.
 *
 ****************************************************************** ****
 *                                                                     *
 * </pre>
 */
public class Dryr619 extends Maind {

	public static final String ROUTINE = QPUtilities.getThisClass();
	/* Storage for T5687 table items. */
	// ILIFE-2628 fixed--Array size increased
	private static final int WSAAT5687SIZE = 1000;
	/* Storage for T6658 table items. */
	// ILIFE-2628 fixed--Array size increased
	private static final int WSAAT6658SIZE = 1000;
	/* Storage for T5540 table items. */
	// ILIFE-2628 fixed--Array size increased
	private static final int WSAAT5540SIZE = 1000;
	/* Storage for T5519 table items. */
	// ILIFE-2628 fixed--Array size increased
	private static final int WSAAT5519SIZE = 1000;

	/* CONTROL-TOTALS */
	private static final int CT01 = 1;
	private static final int CT02 = 2;
	private static final int CT05 = 5;
	/* ERRORS */
	private static final String H791 = "H791";
	/* TABLES */
	private static final String T5679 = "T5679";
	private static final String T6658 = "T6658";
	private static final String T5540 = "T5540";
	private FixedLengthStringData t5519 = new FixedLengthStringData(6).init("T5519");
	private FixedLengthStringData t5687 = new FixedLengthStringData(6).init("T5687");

	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRYR619");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	/*
	 * These fields are required by MAINB processing and should not be deleted.
	 */
	private FixedLengthStringData wsspEdterror = new FixedLengthStringData(4);

	private FixedLengthStringData wsaaDateFound = new FixedLengthStringData(1).init("N");
	private Validator dateFound = new Validator(wsaaDateFound, "Y");

	/* WSAA-T5687-ARRAY */
	private FixedLengthStringData[] wsaaT5687Rec = FLSInittedArray(1000, 13);
	private FixedLengthStringData[] wsaaT5687Key = FLSDArrayPartOfArrayStructure(4, wsaaT5687Rec, 0);
	private FixedLengthStringData[] wsaaT5687Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5687Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5687Data = FLSDArrayPartOfArrayStructure(9, wsaaT5687Rec, 4);
	private PackedDecimalData[] wsaaT5687Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5687Data, 0);
	private FixedLengthStringData[] wsaaT5687Annvry = FLSDArrayPartOfArrayStructure(4, wsaaT5687Data, 5);

	/* WSAA-T6658-ARRAY */
	private FixedLengthStringData[] wsaaT6658Rec = FLSInittedArray(1000, 19);
	private FixedLengthStringData[] wsaaT6658Key = FLSDArrayPartOfArrayStructure(4, wsaaT6658Rec, 0);
	private FixedLengthStringData[] wsaaT6658Annvry = FLSDArrayPartOfArrayStructure(4, wsaaT6658Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT6658Data = FLSDArrayPartOfArrayStructure(15, wsaaT6658Rec, 4);
	private PackedDecimalData[] wsaaT6658Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT6658Data, 0);
	private FixedLengthStringData[] wsaaT6658Subprog = FLSDArrayPartOfArrayStructure(10, wsaaT6658Data, 5);

	/* WSAA-T5540-ARRAY */
	private FixedLengthStringData[] wsaaT5540Rec = FLSInittedArray(1000, 13);
	private FixedLengthStringData[] wsaaT5540Key = FLSDArrayPartOfArrayStructure(4, wsaaT5540Rec, 0);
	private FixedLengthStringData[] wsaaT5540Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5540Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5540Data = FLSDArrayPartOfArrayStructure(9, wsaaT5540Rec, 4);
	private PackedDecimalData[] wsaaT5540Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5540Data, 0);
	private FixedLengthStringData[] wsaaT5540Iudiscbas = FLSDArrayPartOfArrayStructure(4, wsaaT5540Data, 5);

	/* WSAA-T5519-ARRAY */
	private FixedLengthStringData[] wsaaT5519Rec = FLSInittedArray(1000, 28);
	private FixedLengthStringData[] wsaaT5519Key = FLSDArrayPartOfArrayStructure(4, wsaaT5519Rec, 0);
	private FixedLengthStringData[] wsaaT5519Iudiscbas = FLSDArrayPartOfArrayStructure(4, wsaaT5519Key, 0, SPACES);
	private FixedLengthStringData[] wsaaT5519Data = FLSDArrayPartOfArrayStructure(24, wsaaT5519Rec, 4);
	private PackedDecimalData[] wsaaT5519Itmfrm = PDArrayPartOfArrayStructure(8, 0, wsaaT5519Data, 0);
	private FixedLengthStringData[] wsaaT5519Iufreq = FLSDArrayPartOfArrayStructure(2, wsaaT5519Data, 5);
	private ZonedDecimalData[] wsaaT5519Annchg = ZDArrayPartOfArrayStructure(17, 2, wsaaT5519Data, 7);
	/* WSAA-VARIABLES */
	private ZonedDecimalData wsaaT5679Sub = new ZonedDecimalData(2, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaUpdateCovrOnly = new FixedLengthStringData(1).init(SPACES);
	private Validator updateCovrOnly = new Validator(wsaaUpdateCovrOnly, "Y");

	private FixedLengthStringData wsaaT5540Found = new FixedLengthStringData(1).init(SPACES);
	private FixedLengthStringData wsaaAnnvry = new FixedLengthStringData(4).init(SPACES);
	private FixedLengthStringData wsaaSubprog = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaIudiscbas = new FixedLengthStringData(4).init(SPACES);

	private FixedLengthStringData wsaaValidComp = new FixedLengthStringData(1).init("N");
	private Validator validComp = new Validator(wsaaValidComp, "Y");
	private int wsaaT5687Ix;
	private int wsaaT6658Ix;
	private int wsaaT5540Ix;
	private int wsaaT5519Ix;

	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T6658rec t6658rec = new T6658rec();
	private T5540rec t5540rec = new T5540rec();
	private T5519rec t5519rec = new T5519rec();
	private Annprocrec annprocrec = new Annprocrec();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Lifsttrrec lifsttrrec = new Lifsttrrec();
	private WsysSystemErrorParamsInner wsysSystemErrorParamsInner = new WsysSystemErrorParamsInner();

	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private Map<String, List<Itempf>> t5687ListMap;
	private Map<String, List<Itempf>> t5519ListMap;
	private Map<String, List<Itempf>> t6658ListMap;
	private Map<String, List<Itempf>> t5540ListMap;
	private Map<String, List<Itempf>> t5679ListMap;
	private List<Itempf> t5679List;

	private Dryr619DAO dryr619DAO = getApplicationContext().getBean("dryr619DAO", Dryr619DAO.class);
	private CovrpfDAO covrpfDAO = getApplicationContext().getBean("covrpfDAO", CovrpfDAOImpl.class);

	private List<Covrpf> covrpfInsertList = new ArrayList<Covrpf>();
	private Covrpf covrpf;

	private int ctrCT01;
	private int ctrCT02;

	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	public Dryr619() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	@Override
	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			// ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	protected void startProcessing100() {

		initialise1000();
		List<Dryr619Dto> dryr619DtoList = dryr619DAO.getCovrDetails(drypDryprcRecInner.drypCompany.toString(),
				drypDryprcRecInner.drypEntity.toString(), drypDryprcRecInner.drypRunDate.toInt());
		for (Dryr619Dto dryr619Dto : dryr619DtoList) {
			ctrCT01++;
			/* Set up the key for the SYSR- copybook, should a system error */
			/* for this record occur. */
			wsysSystemErrorParamsInner.wsysChdrcoy.set(dryr619Dto.getChdrCoy());
			wsysSystemErrorParamsInner.wsysChdrnum.set(dryr619Dto.getChdrNum());
			wsysSystemErrorParamsInner.wsysLife.set(dryr619Dto.getLife());
			wsysSystemErrorParamsInner.wsysCoverage.set(dryr619Dto.getCoverage());
			wsysSystemErrorParamsInner.wsysRider.set(dryr619Dto.getRider());
			wsysSystemErrorParamsInner.wsysPlnsfx.set(dryr619Dto.getPlanSuffix());
			wsysSystemErrorParamsInner.wsysCrtable.set(dryr619Dto.getCrTable());
			wsysSystemErrorParamsInner.wsysCrrcd.set(dryr619Dto.getCrrcd());
			wsysSystemErrorParamsInner.wsysStatcode.set(dryr619Dto.getStatCode());

			edit2500(dryr619Dto);
			if (isEQ(wsspEdterror, Varcom.oK)) {
				update3000(dryr619Dto);
			}
		}
		commit3500();
	}

	protected void initialise1000() {
		initialise1010();
		loadT5679();
		loadT5687();
		loadT6658();
		loadT5540();
		loadT5519();
	}

	protected void initialise1010() {

		String company = drypDryprcRecInner.drypCompany.toString();
		t5687ListMap = itemDAO.loadSmartTable("IT", company, t5687.toString().trim());
		t6658ListMap = itemDAO.loadSmartTable("IT", company, T6658);
		t5540ListMap = itemDAO.loadSmartTable("IT", company, T5540);
		t5519ListMap = itemDAO.loadSmartTable("IT", company, t5519.toString().trim());
		t5679ListMap = itemDAO.loadSmartTable("IT", company, T5679);
		t5679List = t5679ListMap.get(drypDryprcRecInner.drypBatctrcde.toString());
		ctrCT01 = 0;
		ctrCT02 = 0;
	}

	protected void loadT5679() {
		if (t5679List == null || t5679List.isEmpty()) {
			drylogrec.params.set(T5679 + drypDryprcRecInner.drypBatctrcde);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(StringUtil.rawToString(t5679List.get(0).getGenarea()));
	}

	protected void loadT5687() {
		if (t5687ListMap == null || t5687ListMap.isEmpty()) {
			drylogrec.params.set("TH510");
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (t5687ListMap.size() > WSAAT5687SIZE) {
			drylogrec.params.set(t5687);
			drylogrec.statuz.set(H791);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaT5687Ix = 1;
		for (List<Itempf> items : t5687ListMap.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					t5687rec.t5687Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5687Itmfrm[wsaaT5687Ix].set(item.getItmfrm());
					wsaaT5687Crtable[wsaaT5687Ix].set(item.getItemitem());
					wsaaT5687Annvry[wsaaT5687Ix].set(t5687rec.anniversaryMethod);
					wsaaT5687Ix++;
				}

			}

		}

	}

	protected void loadT6658() {
		if (t6658ListMap == null || t6658ListMap.isEmpty()) {
			drylogrec.params.set(T6658);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
		if (t6658ListMap.size() > WSAAT6658SIZE) {
			drylogrec.params.set(T6658);
			drylogrec.statuz.set(H791);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaT6658Ix = 1;
		for (List<Itempf> items : t5687ListMap.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					t6658rec.t6658Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT6658Itmfrm[wsaaT6658Ix].set(item.getItmfrm());
					wsaaT6658Annvry[wsaaT6658Ix].set(item.getItemitem());
					wsaaT6658Subprog[wsaaT6658Ix].set(t6658rec.subprog);
					wsaaT6658Ix++;
				}
			}

		}
	}

	protected void loadT5540() {
		if (t5540ListMap == null || t5540ListMap.isEmpty()) {
			drylogrec.params.set(T5540);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (t5540ListMap.size() > WSAAT5540SIZE) {
			drylogrec.params.set(T5540);
			drylogrec.statuz.set(H791);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaT5540Ix = 1;
		for (List<Itempf> items : t5540ListMap.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					t5540rec.t5540Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5540Itmfrm[wsaaT5540Ix].set(item.getItmfrm());
					wsaaT5540Crtable[wsaaT5540Ix].set(item.getItemitem());
					wsaaT5540Iudiscbas[wsaaT5540Ix].set(t5540rec.iuDiscBasis);
					wsaaT5540Ix++;
				}
			}
		}
	}

	protected void loadT5519() {
		wsaaT5519Ix = 1;
		if (t5519ListMap == null || t5519ListMap.isEmpty()) {
			drylogrec.params.set(t5519);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (t5519ListMap.size() > WSAAT5519SIZE) {
			drylogrec.params.set(t5519);
			drylogrec.statuz.set(H791);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		for (List<Itempf> items : t5519ListMap.values()) {
			for (Itempf item : items) {
				if (item.getItmfrm().compareTo(varcom.vrcmMaxDate.getbigdata()) <= 0) {
					t5519rec.t5519Rec.set(StringUtil.rawToString(item.getGenarea()));
					wsaaT5519Itmfrm[wsaaT5519Ix].set(item.getItmfrm());
					wsaaT5519Iudiscbas[wsaaT5519Ix].set(item.getItemitem());
					wsaaT5519Iufreq[wsaaT5519Ix].set(t5519rec.initUnitChargeFreq);
					wsaaT5519Annchg[wsaaT5519Ix].set(t5519rec.annchg);
					wsaaT5519Ix++;
				}
			}
		}
	}

	protected void edit2500(Dryr619Dto dryr619Dto) {
		wsspEdterror.set(Varcom.oK);
		validateComp2510(dryr619Dto);
		if (!validComp.isTrue()) {
			/* No of COVRs with invalid status */

			ctrCT02++;
			wsspEdterror.set(SPACES);
			return;
		}
		searchT56872520(dryr619Dto);
		searchT66582530();
		searchT55402540(dryr619Dto);
		searchT55192550();
				
	}

	protected void validateComp2510(Dryr619Dto dryr619Dto) {
		para2511(dryr619Dto);
	}

	protected void para2511(Dryr619Dto dryr619Dto) {
		/* Validate Coverage/Rider status against T5679. */
		wsaaValidComp.set("N");
		if (isNE(dryr619Dto.getRider(), "00") && isNE(dryr619Dto.getRider(), SPACES)) {
			validateRider2515(dryr619Dto);
		}
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12) || validComp.isTrue()); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], dryr619Dto.getStatCode())) {
				wsaaValidComp.set("Y");
			}
		}
	}

	protected void validateRider2515(Dryr619Dto dryr619Dto) {
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, 12) || validComp.isTrue()); wsaaT5679Sub.add(1)) {
			if (isEQ(t5679rec.ridRiskStat[wsaaT5679Sub.toInt()], dryr619Dto.getStatCode())) {
				wsaaValidComp.set("Y");
			}
		}
	}

	protected void searchT56872520(Dryr619Dto dryr619Dto) {
		wsaaUpdateCovrOnly.set(SPACES);
		wsaaT5687Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT5687Ix, wsaaT5687Rec.length); wsaaT5687Ix++) {
				if (isEQ(wsaaT5687Key[wsaaT5687Ix], dryr619Dto.getCrTable())) {
					break searchlabel1;
				}
			}
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaDateFound.set("N");
		while (!(isNE(dryr619Dto.getCrTable(), wsaaT5687Crtable[wsaaT5687Ix]) || isGT(wsaaT5687Ix, WSAAT5687SIZE)
				|| dateFound.isTrue())) {
			if (isGTE(drypDryprcRecInner.drypRunDate, wsaaT5687Itmfrm[wsaaT5687Ix])) {
				wsaaDateFound.set("Y");
				wsaaAnnvry.set(wsaaT5687Annvry[wsaaT5687Ix]);
			} else {
				wsaaT5687Ix++;
			}
		}

		if (!dateFound.isTrue()) {

			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(wsaaAnnvry, SPACES)) {
			wsaaUpdateCovrOnly.set("Y");
		}
	}

	protected void searchT66582530() {
		wsaaUpdateCovrOnly.set(SPACES);
		wsaaT6658Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT6658Ix, wsaaT6658Rec.length); wsaaT6658Ix++) {
				if (isEQ(wsaaT6658Key[wsaaT6658Ix], wsaaAnnvry)) {
					break searchlabel1;
				}
			}
			wsaaUpdateCovrOnly.set("Y");
			return;
		}
		wsaaDateFound.set("N");
		while (!(isNE(wsaaAnnvry, wsaaT6658Annvry[wsaaT6658Ix]) || isGT(wsaaT6658Ix, WSAAT6658SIZE)
				|| dateFound.isTrue())) {
			if (isGTE(drypDryprcRecInner.drypRunDate, wsaaT6658Itmfrm[wsaaT6658Ix])) {
				wsaaDateFound.set("Y");
				wsaaSubprog.set(wsaaT6658Subprog[wsaaT6658Ix]);
			} else {
				wsaaT6658Ix++;
			}
		}

		if (!dateFound.isTrue() || isEQ(wsaaSubprog, SPACES)) {
			wsaaUpdateCovrOnly.set("Y");
		}
	}

	protected void searchT55402540(Dryr619Dto dryr619Dto) {
		wsaaT5540Found.set(SPACES);
		wsaaT5540Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT5540Ix, wsaaT5540Rec.length); wsaaT5540Ix++) {
				if (isEQ(wsaaT5540Key[wsaaT5540Ix], dryr619Dto.getCrTable())) {
					break searchlabel1;
				}
			}
			return;
		}
		wsaaDateFound.set("N");
		while (!(isNE(dryr619Dto.getCrTable(), wsaaT5540Crtable[wsaaT5540Ix]) || isGT(wsaaT5540Ix, WSAAT5540SIZE)
				|| dateFound.isTrue())) {
			if (isGTE(drypDryprcRecInner.drypRunDate, wsaaT5540Itmfrm[wsaaT5540Ix])) {
				wsaaDateFound.set("Y");
				wsaaT5540Found.set("Y");
				wsaaIudiscbas.set(wsaaT5540Iudiscbas[wsaaT5540Ix]);
			} else {
				wsaaT5540Ix++;
			}
		}

	}

	protected void searchT55192550() {
		wsaaT5519Ix = 1;
		searchlabel1: {
			for (; isLT(wsaaT5519Ix, wsaaT5519Rec.length); wsaaT5519Ix++) {
				if (isEQ(wsaaT5519Key[wsaaT5519Ix], wsaaIudiscbas)) {
					break searchlabel1;
				}
			}

			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaDateFound.set("N");
		while (!(isNE(wsaaIudiscbas, wsaaT5519Iudiscbas[wsaaT5519Ix]) || isGT(wsaaT5519Ix, WSAAT5519SIZE)
				|| dateFound.isTrue())) {
			if (isGTE(drypDryprcRecInner.drypRunDate, wsaaT5519Itmfrm[wsaaT5519Ix])) {
				wsaaDateFound.set("Y");
			} else {
				wsaaT5519Ix++;
			}
		}

		if (!dateFound.isTrue()) {

			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void update3000(Dryr619Dto dryr619Dto) {
		update3010(dryr619Dto);
		updateCovr3050();
	}

	protected void update3010(Dryr619Dto dryr619Dto) {
		covrpf = dryr619Dto.getCovrpf();
		if (covrpf == null) {
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (updateCovrOnly.isTrue()) {
			return;
		}
		initialize(annprocrec.annpllRec);
		annprocrec.company.set(dryr619Dto.getChdrCoy());
		annprocrec.chdrnum.set(dryr619Dto.getChdrNum());
		annprocrec.life.set(dryr619Dto.getLife());
		annprocrec.coverage.set(dryr619Dto.getCoverage());
		annprocrec.rider.set(dryr619Dto.getRider());
		annprocrec.planSuffix.set(dryr619Dto.getPlanSuffix());
		annprocrec.effdate.set(drypDryprcRecInner.drypRunDate);
		annprocrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		annprocrec.crdate.set(dryr619Dto.getCrrcd());
		annprocrec.crtable.set(dryr619Dto.getCrTable());
		annprocrec.user.set(varcom.vrcmUser);
		annprocrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		annprocrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		annprocrec.batccoy.set(drypDryprcRecInner.drypCompany);
		annprocrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		annprocrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		annprocrec.batcpfx.set(drypDryprcRecInner.drypBatcpfx);
		callProgram(wsaaSubprog, annprocrec.annpllRec);
		if (isNE(annprocrec.statuz, Varcom.oK)) {
			drylogrec.statuz.set(annprocrec.statuz);
			drylogrec.params.set(annprocrec.annpllRec);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Statictic *********************** */
		initialize(lifsttrrec.lifsttrRec);
		lifsttrrec.batccoy.set(drypDryprcRecInner.drypCompany);
		lifsttrrec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		lifsttrrec.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		lifsttrrec.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		lifsttrrec.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		lifsttrrec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		lifsttrrec.chdrcoy.set(dryr619Dto.getChdrCoy());
		lifsttrrec.chdrnum.set(dryr619Dto.getChdrNum());
		lifsttrrec.tranno.set(covrpf.getTranno());
		lifsttrrec.trannor.set(99999);
		lifsttrrec.agntnum.set(SPACES);
		lifsttrrec.oldAgntnum.set(SPACES);
		callProgram(Lifsttr.class, lifsttrrec.lifsttrRec);
		if (isNE(lifsttrrec.statuz, Varcom.oK)) {
			drylogrec.params.set(lifsttrrec.lifsttrRec);
			drylogrec.statuz.set(lifsttrrec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}
	}

	protected void updateCovr3050() {
		initialize(datcon2rec.datcon2Rec);
		datcon2rec.frequency.set("01");
		datcon2rec.freqFactor.set(1);
		datcon2rec.intDate1.set(covrpf.getAnnivProcDate());
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, Varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		covrpf.setAnnivProcDate(datcon2rec.intDate2.toInt());
		covrpfInsertList.add(covrpf);

	}

	protected void commit3500() {
		if (covrpfInsertList != null && covrpfInsertList.size() > 0) {
			covrpfDAO.updateChunkRecordToCovr(covrpfInsertList);
			drycntrec.contotNumber.set(CT05);
			drycntrec.contotValue.set(covrpfInsertList.size());
			d000ControlTotals();
			covrpfInsertList.clear();
		}
		drycntrec.contotNumber.set(CT01);
		drycntrec.contotValue.set(ctrCT01);
		d000ControlTotals();
		ctrCT01 = 0;

		drycntrec.contotNumber.set(CT02);
		drycntrec.contotValue.set(ctrCT02);
		d000ControlTotals();
		ctrCT02 = 0;

	}

	protected String formatThreadName(int threadNo) {
		String threadName = "";
		if (threadNo >= 10 && threadNo < 100) {
			threadName = "0" + String.valueOf(threadNo);
		} else if (threadNo >= 0 && threadNo < 10) {
			threadName = "00" + String.valueOf(threadNo);
		} else {
			threadName = String.valueOf(threadNo);
		}
		return threadName;
	}

	/*
	 * Class transformed from Data Structure WSYS-SYSTEM-ERROR-PARAMS--INNER
	 */
	private static final class WsysSystemErrorParamsInner {

		private FixedLengthStringData wsysSystemErrorParams = new FixedLengthStringData(100);
		private FixedLengthStringData wsysSysparams = new FixedLengthStringData(100).isAPartOf(wsysSystemErrorParams,
				0);
		private FixedLengthStringData wsysAvyxData = new FixedLengthStringData(33).isAPartOf(wsysSysparams, 0,
				REDEFINE);
		private FixedLengthStringData wsysChdrcoy = new FixedLengthStringData(1).isAPartOf(wsysAvyxData, 0);
		private FixedLengthStringData wsysChdrnum = new FixedLengthStringData(8).isAPartOf(wsysAvyxData, 1);
		private FixedLengthStringData wsysLife = new FixedLengthStringData(2).isAPartOf(wsysAvyxData, 9);
		private FixedLengthStringData wsysCoverage = new FixedLengthStringData(2).isAPartOf(wsysAvyxData, 11);
		private FixedLengthStringData wsysRider = new FixedLengthStringData(2).isAPartOf(wsysAvyxData, 13);
		private ZonedDecimalData wsysPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsysAvyxData, 15).setUnsigned();
		private FixedLengthStringData wsysCrtable = new FixedLengthStringData(4).isAPartOf(wsysAvyxData, 19);
		private ZonedDecimalData wsysCrrcd = new ZonedDecimalData(8, 0).isAPartOf(wsysAvyxData, 23).setUnsigned();
		private FixedLengthStringData wsysStatcode = new FixedLengthStringData(2).isAPartOf(wsysAvyxData, 31);
	}

	/*
	 * Class transformed from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {

		// ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		// cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		// ILPI-97 ends
	}
}
