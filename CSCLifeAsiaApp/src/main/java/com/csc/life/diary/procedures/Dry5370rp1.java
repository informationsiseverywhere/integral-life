/*
 * File: Dry5370rp1.java
 * Date: December 3, 2013 2:22:58 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5370RP1.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.dataaccess.DrptsrtTableDAM;
import com.csc.diary.recordstructures.Drylogrec;
import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.clients.recordstructures.Fsupfxcpy;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.life.diary.reports.R537001Report;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
*    Diary Regular Payment - Process Report.
*    ---------------------------------------
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5370rp1 extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R537001Report printFile = new R537001Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(220);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5370RP1");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private Validator pageGood = new Validator(wsaaOverflow, "N");
	private FixedLengthStringData wsaaRptToday = new FixedLengthStringData(10).init(SPACES);
	private FixedLengthStringData wsaaRptCompanynm = new FixedLengthStringData(30).init(SPACES);
	private FixedLengthStringData wsaaRptBranchnm = new FixedLengthStringData(30).init(SPACES);
	private ZonedDecimalData wsaaAmountOut = new ZonedDecimalData(17, 2);
		/* WSAA-PRINTER-CONTROL */
	private ZonedDecimalData wsaaLineCount = new ZonedDecimalData(2, 0).setUnsigned();
	private ZonedDecimalData wsaaLineCount02 = new ZonedDecimalData(2, 0).setUnsigned();
	private static final int wsaaPageSize = 59;

	private FixedLengthStringData wsaaT6689Msg = new FixedLengthStringData(25);
	private FixedLengthStringData filler = new FixedLengthStringData(21).isAPartOf(wsaaT6689Msg, 0, FILLER).init("T6689 : MRNF : VAL : ");
	private FixedLengthStringData wsaaT6689Item = new FixedLengthStringData(4).isAPartOf(wsaaT6689Msg, 21);
		/* WSAA-LONGSTR */
	private static final String wsaaNoPayment = "No Payments Records";
	private static final String wsaaNoExcep = "No Exception Records";
	private static final String wsaaEndReport = "End Of Report";
	private String wsaa1stNonExcepRead = "";
	private String wsaa1stExcepRead = "";

	private FixedLengthStringData wsaaReportTitle = new FixedLengthStringData(10);
	private Validator payment = new Validator(wsaaReportTitle, "PROCESS");
	private Validator excep = new Validator(wsaaReportTitle, "EXCEPTION");
	private ZonedDecimalData wsaaNonExcepDate = new ZonedDecimalData(8, 0).init(ZERO).setUnsigned();
	private static final String wsaaAll = "*ALL";
		/* FORMATS */
	private static final String descrec = "DESCREC";
	private static final String drptsrtrec = "DRPTSRTREC";
	private static final String chdrrgprec = "CHDRRGPREC";
		/* TABLES */
	private static final String t1692 = "T1692";
	private static final String t1693 = "T1693";
	private static final String t6689 = "T6689";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r537001H01Record = new FixedLengthStringData(101);
	private FixedLengthStringData r537001h01O = new FixedLengthStringData(101).isAPartOf(r537001H01Record, 0);
	private FixedLengthStringData rstate = new FixedLengthStringData(10).isAPartOf(r537001h01O, 0);
	private FixedLengthStringData repdate = new FixedLengthStringData(10).isAPartOf(r537001h01O, 10);
	private FixedLengthStringData jnumb = new FixedLengthStringData(8).isAPartOf(r537001h01O, 20);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r537001h01O, 28);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r537001h01O, 29);
	private FixedLengthStringData sdate = new FixedLengthStringData(10).isAPartOf(r537001h01O, 59);
	private FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(r537001h01O, 69);
	private FixedLengthStringData branchnm = new FixedLengthStringData(30).isAPartOf(r537001h01O, 71);

	private FixedLengthStringData r537001H02Record = new FixedLengthStringData(101);
	private FixedLengthStringData r537001h02O = new FixedLengthStringData(101).isAPartOf(r537001H02Record, 0);
	private FixedLengthStringData rstate1 = new FixedLengthStringData(10).isAPartOf(r537001h02O, 0);
	private FixedLengthStringData repdate1 = new FixedLengthStringData(10).isAPartOf(r537001h02O, 10);
	private FixedLengthStringData jnumb1 = new FixedLengthStringData(8).isAPartOf(r537001h02O, 20);
	private FixedLengthStringData company1 = new FixedLengthStringData(1).isAPartOf(r537001h02O, 28);
	private FixedLengthStringData companynm1 = new FixedLengthStringData(30).isAPartOf(r537001h02O, 29);
	private FixedLengthStringData sdate1 = new FixedLengthStringData(10).isAPartOf(r537001h02O, 59);
	private FixedLengthStringData branch1 = new FixedLengthStringData(2).isAPartOf(r537001h02O, 69);
	private FixedLengthStringData branchnm1 = new FixedLengthStringData(30).isAPartOf(r537001h02O, 71);

	private FixedLengthStringData r537001D03Record = new FixedLengthStringData(30);
	private FixedLengthStringData r537001d03O = new FixedLengthStringData(30).isAPartOf(r537001D03Record, 0);
	private FixedLengthStringData longstr = new FixedLengthStringData(30).isAPartOf(r537001d03O, 0);
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private DescTableDAM descIO = new DescTableDAM();
	private DrptsrtTableDAM drptsrtIO = new DrptsrtTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Fsupfxcpy fsupfxcpy = new Fsupfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Varcom varcom = new Varcom();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Dryoutrec dryoutrec = new Dryoutrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private R537001D01RecordInner r537001D01RecordInner = new R537001D01RecordInner();
	private R537001D02RecordInner r537001D02RecordInner = new R537001D02RecordInner();

	public Dry5370rp1() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryoutrec.dryoutRec = convertAndSetParam(dryoutrec.dryoutRec, parmArray, 0);
		try {
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
		printFile.openOutput();
		pageOverflow.setTrue();
		dryoutrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		/* Set up today's date.*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		/* Get the company description.*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(dryoutrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		wsaaRptCompanynm.set(descIO.getLongdesc());
		/* Get the Branch description.*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(dryoutrec.company);
		descIO.setDesctabl(t1692);
		descIO.setDescitem(dryoutrec.branch);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		wsaaRptBranchnm.set(descIO.getLongdesc());
		wsaa1stNonExcepRead = "Y";
		wsaa1stExcepRead = "Y";
		/* Set up the DRPTSRT logical to read the Report file.*/
		drptsrtIO.setRecKeyData(SPACES);
		drptsrtIO.setStatuz(varcom.oK);
		drptsrtIO.setEffdate(dryoutrec.effectiveDate);
		drptsrtIO.setDiaryEntityCompany(dryoutrec.company);
		drptsrtIO.setDiaryEntityBranch(dryoutrec.branch);
		drptsrtIO.setDiaryReportName(dryoutrec.reportName);
		drptsrtIO.setDiaryRunNumber(dryoutrec.runNumber);
		drptsrtIO.setSortkey(SPACES);
		drptsrtIO.setFormat(drptsrtrec);
		drptsrtIO.setFunction(varcom.begn);
		/* Loop thru DRPTSRT records writing the R537001 report.*/
		while ( !(isEQ(drptsrtIO.getStatuz(), varcom.endp))) {
			callDrpt200();
		}
		
		close1700();
		printFile.close();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void callDrpt200()
	{
		call210();
	}

protected void call210()
	{
		/* Call the DRPTSRT logical to retrieve the DRPT record.*/
		SmartFileCode.execute(appVars, drptsrtIO);
		if (isNE(drptsrtIO.getStatuz(), varcom.oK)
		&& isNE(drptsrtIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(drptsrtIO.getParams());
			drylogrec.statuz.set(drptsrtIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		/* Check that we are working with the R537001 data.*/
		if (isEQ(drptsrtIO.getStatuz(), varcom.endp)
		|| isNE(drptsrtIO.getDiaryEntityCompany(), dryoutrec.company)
		|| isNE(drptsrtIO.getDiaryEntityBranch(), dryoutrec.branch)
		|| isNE(drptsrtIO.getDiaryReportName(), dryoutrec.reportName)
		|| isNE(drptsrtIO.getEffdate(), dryoutrec.effectiveDate)) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(dryoutrec.runNumber, ZERO)
		&& isNE(dryoutrec.runNumber, drptsrtIO.getDiaryRunNumber())) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		/* Move the sort key details to the report copybook.*/
		dryrDryrptRecInner.r537001SortKey.set(drptsrtIO.getSortkey());
		/* Start writing the report details.*/
		writeLine300();
		drptsrtIO.setFunction(varcom.nextr);
	}

protected void writeLine300()
	{
		/*WRITE*/
		/* Fill the detail record.*/
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		if (isEQ(dryrDryrptRecInner.r537001Exreport, SPACES)) {
			nonExceptRpt1000();
		}
		else {
			excepRpt1300();
		}
		/*EXIT*/
	}

protected void nonExceptRpt1000()
	{
		/*NON-EXCEPT-RPT*/
		if (isEQ(wsaa1stNonExcepRead, "Y")) {
			writeNonExcepHeader2000();
			wsaa1stNonExcepRead = "N";
		}
		writeNonExcepDetail1100();
		/*EXIT*/
	}

protected void writeNonExcepDetail1100()
	{
		/*START*/
		wsaaLineCount.add(1);
		/* Check if new page required*/
		if (isGTE(wsaaLineCount, wsaaPageSize)) {
			writeNonExcepHeader2000();
		}
		/* Move regr fields to report fields*/
		moveProcessRptFields1200();
		printRecord.set(SPACES);
		printFile.printR537001d01(r537001D01RecordInner.r537001D01Record, indicArea);
		/*EXIT*/
	}

protected void moveProcessRptFields1200()
	{
		start1210();
	}

protected void start1210()
	{
		processConvertCurrency2400();
		/* Move regr fields to report fields*/
		r537001D01RecordInner.chdrnum.set(dryrDryrptRecInner.r537001Chdrnum);
		r537001D01RecordInner.life.set(dryrDryrptRecInner.r537001Life);
		r537001D01RecordInner.coverage.set(dryrDryrptRecInner.r537001Coverage);
		r537001D01RecordInner.rider.set(dryrDryrptRecInner.r537001Rider);
		r537001D01RecordInner.rgpynum.set(dryrDryrptRecInner.r537001Rgpynum);
		r537001D01RecordInner.pymt.set(wsaaAmountOut);
		r537001D01RecordInner.currcd.set(dryrDryrptRecInner.r537001Currcd);
		r537001D01RecordInner.prcnt.set(dryrDryrptRecInner.r537001Prcnt);
		r537001D01RecordInner.rgpytype.set(dryrDryrptRecInner.r537001Rgpytype);
		r537001D01RecordInner.payreason.set(dryrDryrptRecInner.r537001Payreason);
		r537001D01RecordInner.rgpystat.set(dryrDryrptRecInner.r537001Rgpystat);
	}

protected void excepRpt1300()
	{
		/*START*/
		if (isEQ(wsaa1stExcepRead, "Y")) {
			if (isEQ(wsaa1stNonExcepRead, "Y")) {
				nonExcepRptNorecs1400();
			}
			else {
				nonExcepRptYesrecs2100();
			}
			writeExcepHeader2200();
			wsaa1stExcepRead = "N";
		}
		/* write exception detail line*/
		writeExcepDetail1500();
		/*EXIT*/
	}

protected void nonExcepRptNorecs1400()
	{
		/*START*/
		writeNonExcepHeader2000();
		writeNonNorecTrl2300();
		/*EXIT*/
	}

protected void writeExcepDetail1500()
	{
		start1510();
	}

protected void start1510()
	{
		/* Check new page required.*/
		wsaaLineCount02.add(1);
		if (isGTE(wsaaLineCount02, wsaaPageSize)) {
			writeExcepHeader2200();
		}
		processConvertCurrency2400();
		getExceptionCodeDesc1600();
		/* Move regr fields to report fields*/
		r537001D02RecordInner.chdrnum.set(dryrDryrptRecInner.r537001Chdrnum);
		r537001D02RecordInner.life.set(dryrDryrptRecInner.r537001Life);
		r537001D02RecordInner.coverage.set(dryrDryrptRecInner.r537001Coverage);
		r537001D02RecordInner.rider.set(dryrDryrptRecInner.r537001Rider);
		r537001D02RecordInner.rgpynum.set(dryrDryrptRecInner.r537001Rgpynum);
		r537001D02RecordInner.pymt.set(wsaaAmountOut);
		r537001D02RecordInner.currcd.set(dryrDryrptRecInner.r537001Currcd);
		r537001D02RecordInner.prcnt.set(dryrDryrptRecInner.r537001Prcnt);
		r537001D02RecordInner.rgpytype.set(dryrDryrptRecInner.r537001Rgpytype);
		r537001D02RecordInner.payreason.set(dryrDryrptRecInner.r537001Payreason);
		r537001D02RecordInner.rgpystat.set(dryrDryrptRecInner.r537001Rgpystat);
		printRecord.set(SPACES);
		printFile.printR537001d02(r537001D02RecordInner.r537001D02Record, indicArea);
	}

protected void getExceptionCodeDesc1600()
	{
		start1610();
	}

protected void start1610()
	{
		r537001D02RecordInner.longdesc.set(SPACES);
		descIO.setDataKey(SPACES);
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy(dryoutrec.company);
		descIO.setDesctabl(t6689);
		descIO.setDescitem(dryrDryrptRecInner.r537001Excode);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)
		&& isNE(descIO.getStatuz(), varcom.mrnf)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		if (isEQ(descIO.getStatuz(), varcom.mrnf)) {
			wsaaT6689Item.set(dryrDryrptRecInner.r537001Excode);
			r537001D02RecordInner.longdesc.set(wsaaT6689Msg);
		}
		else {
			r537001D02RecordInner.longdesc.set(descIO.getLongdesc());
		}
	}

protected void close1700()
	{
		/*CLOSE-FILES*/
		processReportTrailers1800();
		/*EXIT*/
	}

protected void processReportTrailers1800()
	{
		start1810();
	}

protected void start1810()
	{
		if (isEQ(wsaa1stNonExcepRead, "Y")
		&& isEQ(wsaa1stExcepRead, "Y")) {
			writeNonExcepHeader2000();
			writeNonNorecTrl2300();
			writeExcepHeader2200();
			writeExcepNorecTrl1900();
			return ;
		}
		if (isEQ(wsaa1stExcepRead, "Y")) {
			nonExcepRptYesrecs2100();
			writeExcepHeader2200();
			writeExcepNorecTrl1900();
			return ;
		}
		if (isEQ(wsaa1stExcepRead, "N")) {
			wsaaLineCount02.add(1);
			if (isGTE(wsaaLineCount02, wsaaPageSize)) {
				writeExcepHeader2200();
			}
			longstr.set(SPACES);
			longstr.set(wsaaEndReport);
			printRecord.set(SPACES);
			printFile.printR537001d03(r537001D03Record, indicArea);
		}
	}

protected void writeExcepNorecTrl1900()
	{
		/*START*/
		longstr.set(SPACES);
		longstr.set(wsaaNoExcep);
		printRecord.set(SPACES);
		printFile.printR537001d03(r537001D03Record, indicArea);
		/*EXIT*/
	}

protected void writeNonExcepHeader2000()
	{
		/*START*/
		payment.setTrue();
		rstate.set(wsaaReportTitle);
		wsaaLineCount.set(12);
		newPage400();
		/*EXIT*/
	}

protected void nonExcepRptYesrecs2100()
	{
		/*START*/
		/* write 'end of report' trailer.*/
		wsaaLineCount.add(1);
		/* Check if new page required*/
		if (isGTE(wsaaLineCount, wsaaPageSize)) {
			writeNonExcepHeader2000();
		}
		longstr.set(SPACES);
		longstr.set(wsaaEndReport);
		printRecord.set(SPACES);
		printFile.printR537001d03(r537001D03Record, indicArea);
		/*EXIT*/
	}

protected void writeExcepHeader2200()
	{
		/*START*/
		rstate1.set(SPACES);
		excep.setTrue();
		rstate1.set(wsaaReportTitle);
		wsaaLineCount02.set(12);
		printRecord.set(SPACES);
		printFile.printR537001h02(r537001H02Record, indicArea);
		/*EXIT*/
	}

protected void writeNonNorecTrl2300()
	{
		/*START*/
		longstr.set(SPACES);
		longstr.set(wsaaNoPayment);
		printRecord.set(SPACES);
		printFile.printR537001d03(r537001D03Record, indicArea);
		/*EXIT*/
	}

protected void processConvertCurrency2400()
	{
		start2410();
	}

protected void start2410()
	{
		wsaaNonExcepDate.set(dryrDryrptRecInner.r537001LastPaydate);
		/* convert date format*/
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(wsaaNonExcepDate);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		r537001D01RecordInner.date_var.set(datcon1rec.extDate);
		/* Read in the relevant contract details*/
		chdrrgpIO.setChdrpfx(fsupfxcpy.chdr);
		chdrrgpIO.setChdrcoy(dryoutrec.company);
		chdrrgpIO.setChdrnum(dryrDryrptRecInner.r537001Chdrnum);
		chdrrgpIO.setFunction(varcom.readr);
		chdrrgpIO.setFormat(chdrrgprec);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrrgpIO.getStatuz());
			drylogrec.params.set(chdrrgpIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		/* If the currencies differ convert to payment currency*/
		if (isNE(dryrDryrptRecInner.r537001Currcd, chdrrgpIO.getCntcurr())) {
			conlinkrec.clnk002Rec.set(SPACES);
			conlinkrec.currOut.set(dryrDryrptRecInner.r537001Currcd);
			conlinkrec.currIn.set(chdrrgpIO.getCntcurr());
			conlinkrec.amountIn.set(dryrDryrptRecInner.r537001Pymt);
			conlinkrec.amountOut.set(ZERO);
			conlinkrec.company.set(dryoutrec.company);
			conlinkrec.cashdate.set(datcon1rec.intDate);
			conlinkrec.function.set("CVRT");
			callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
			if (isNE(conlinkrec.statuz, varcom.oK)) {
				drylogrec.statuz.set(conlinkrec.statuz);
				drylogrec.params.set(conlinkrec.clnk002Rec);
				drylogrec.dryDatabaseError.setTrue();
				fatalError();//Modify for ILPI-65 
			}
			wsaaAmountOut.set(conlinkrec.amountOut);
		}
		else {
			wsaaAmountOut.set(dryrDryrptRecInner.r537001Pymt);
		}
	}

protected void newPage400()
	{
		new400();
	}

protected void new400()
	{
		/* Convert the Effective Date for display.*/
		initialize(datcon1rec.datcon1Rec);
		datcon1rec.intDate.set(dryoutrec.effectiveDate);
		datcon1rec.function.set(varcom.conv);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		repdate.set(datcon1rec.extDate);
		repdate1.set(datcon1rec.extDate);
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		sdate.set(datcon1rec.extDate);
		sdate1.set(datcon1rec.extDate);
		if (isGT(dryoutrec.runNumber, 0)) {
			jnumb.set(dryoutrec.runNumber);
			jnumb1.set(dryoutrec.runNumber);
		}
		else {
			jnumb.set(wsaaAll);
			jnumb1.set(wsaaAll);
		}
		company.set(dryoutrec.company);
		company1.set(dryoutrec.company);
		companynm.set(wsaaRptCompanynm);
		companynm1.set(wsaaRptCompanynm);
		branch.set(dryoutrec.branch);
		branch1.set(dryoutrec.branch);
		branchnm.set(wsaaRptBranchnm);
		branchnm1.set(wsaaRptBranchnm);
		printRecord.set(SPACES);
		printFile.printR537001h01(r537001H01Record, indicArea);
	}
//Modify for ILPI-65 
protected void fatalError()
	{
		/*A010-FATAL*/
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		drylogrec.runNumber.set(dryoutrec.runNumber);
		drylogrec.entityKey.set(SPACES);
		dryoutrec.statuz.set(drylogrec.statuz);
		//callProgram(Drylog.class, drylogrec.drylogRec);
		//exitProgram();
		a000FatalError();
	}
/*
 * Class transformed  from Data Structure R537001-D01-RECORD--INNER
 */
private static final class R537001D01RecordInner { 

	private FixedLengthStringData r537001D01Record = new FixedLengthStringData(60);
	private FixedLengthStringData r537001d01O = new FixedLengthStringData(60).isAPartOf(r537001D01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r537001d01O, 0);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r537001d01O, 8);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r537001d01O, 10);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r537001d01O, 12);
	private ZonedDecimalData rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r537001d01O, 14);
	private ZonedDecimalData pymt = new ZonedDecimalData(17, 2).isAPartOf(r537001d01O, 19);
	private FixedLengthStringData currcd = new FixedLengthStringData(3).isAPartOf(r537001d01O, 36);
	private ZonedDecimalData prcnt = new ZonedDecimalData(5, 2).isAPartOf(r537001d01O, 39);
	private FixedLengthStringData rgpytype = new FixedLengthStringData(2).isAPartOf(r537001d01O, 44);
	private FixedLengthStringData payreason = new FixedLengthStringData(2).isAPartOf(r537001d01O, 46);
	private FixedLengthStringData rgpystat = new FixedLengthStringData(2).isAPartOf(r537001d01O, 48);
	private FixedLengthStringData date_var = new FixedLengthStringData(10).isAPartOf(r537001d01O, 50);
}
/*
 * Class transformed  from Data Structure R537001-D02-RECORD--INNER
 */
private static final class R537001D02RecordInner { 

	private FixedLengthStringData r537001D02Record = new FixedLengthStringData(80);
	private FixedLengthStringData r537001d02O = new FixedLengthStringData(80).isAPartOf(r537001D02Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r537001d02O, 0);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r537001d02O, 8);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r537001d02O, 10);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r537001d02O, 12);
	private ZonedDecimalData rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r537001d02O, 14);
	private ZonedDecimalData pymt = new ZonedDecimalData(17, 2).isAPartOf(r537001d02O, 19);
	private FixedLengthStringData currcd = new FixedLengthStringData(3).isAPartOf(r537001d02O, 36);
	private ZonedDecimalData prcnt = new ZonedDecimalData(5, 2).isAPartOf(r537001d02O, 39);
	private FixedLengthStringData rgpytype = new FixedLengthStringData(2).isAPartOf(r537001d02O, 44);
	private FixedLengthStringData payreason = new FixedLengthStringData(2).isAPartOf(r537001d02O, 46);
	private FixedLengthStringData rgpystat = new FixedLengthStringData(2).isAPartOf(r537001d02O, 48);
	private FixedLengthStringData longdesc = new FixedLengthStringData(30).isAPartOf(r537001d02O, 50);
}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r537001DataArea = new FixedLengthStringData(493).isAPartOf(dryrGenarea, 0, REDEFINE);
	private ZonedDecimalData r537001Rgpynum = new ZonedDecimalData(5, 0).isAPartOf(r537001DataArea, 0);
	private ZonedDecimalData r537001Pymt = new ZonedDecimalData(17, 2).isAPartOf(r537001DataArea, 5);
	private FixedLengthStringData r537001Currcd = new FixedLengthStringData(3).isAPartOf(r537001DataArea, 22);
	private ZonedDecimalData r537001Prcnt = new ZonedDecimalData(7, 2).isAPartOf(r537001DataArea, 25);
	private FixedLengthStringData r537001Payreason = new FixedLengthStringData(2).isAPartOf(r537001DataArea, 32);
	private FixedLengthStringData r537001Rgpystat = new FixedLengthStringData(2).isAPartOf(r537001DataArea, 34);
	private FixedLengthStringData r537001Excode = new FixedLengthStringData(4).isAPartOf(r537001DataArea, 149);
	private ZonedDecimalData r537001LastPaydate = new ZonedDecimalData(8, 0).isAPartOf(r537001DataArea, 161);

	private FixedLengthStringData r537001SortKey = new FixedLengthStringData(38);
	private FixedLengthStringData r537001Exreport = new FixedLengthStringData(1).isAPartOf(r537001SortKey, 0);
	private FixedLengthStringData r537001Chdrnum = new FixedLengthStringData(8).isAPartOf(r537001SortKey, 1);
	private FixedLengthStringData r537001Rgpytype = new FixedLengthStringData(2).isAPartOf(r537001SortKey, 9);
	private FixedLengthStringData r537001Crtable = new FixedLengthStringData(4).isAPartOf(r537001SortKey, 11);
	private FixedLengthStringData r537001Life = new FixedLengthStringData(2).isAPartOf(r537001SortKey, 15);
	private FixedLengthStringData r537001Coverage = new FixedLengthStringData(2).isAPartOf(r537001SortKey, 17);
	private FixedLengthStringData r537001Rider = new FixedLengthStringData(2).isAPartOf(r537001SortKey, 19);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
