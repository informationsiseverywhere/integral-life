/*
 * File: Drybenbil.java
 * Date: December 3, 2013 2:23:58 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYBENBIL.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diary.recordstructures.Drylogrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.dataaccess.CovrbblTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This subroutine is used by the batch diary system to set
* up the trigger record for benefit billing processing.
*
* The trigger date for benefit billing will be passed
* through the linkage, it has been calculated in the
* calling program (e.g. P5074AT, DRY6528).
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Drybenbil extends Maind { //Modify for ILPI-65

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYBENBIL");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(2, 0).setUnsigned();
	private final int wsaaT5679Max = 12;

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");
		/* ERRORS */
	private static final String f321 = "F321";
		/* FORMATS */
	private static final String covrrec = "COVRREC";
	private static final String covrbblrec = "COVRBBLREC";
	private static final String itemrec = "ITEMREC";
		/* TABLES */
	private static final String t5679 = "T5679";
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrbblTableDAM covrbblIO = new CovrbblTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5679rec t5679rec = new T5679rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		exit590
	}

	public Drybenbil() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		/* Firstly check the status of the contract against T5679.*/
		validate100();
		if (invalidStatus.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		/* Now read the Coverage file for this contract using the COVRBBL*/
		/* logical. If there are no valid coverages for this contract then*/
		/* set the flag to no (which will remove benefit billing from this*/
		/* contract) and exit.*/
		readCovrbbl400();
		if (invalidStatus.isTrue()) {
			drypDryprcRecInner.dtrdNo.setTrue();
			return ;
		}
		if (isGT(covrbblIO.getBenBillDate(), ZERO)
		&& isLT(covrbblIO.getBenBillDate(), varcom.maxdate)) {
			drypDryprcRecInner.drypNxtprcdate.set(covrbblIO.getBenBillDate());
			drypDryprcRecInner.drypNxtprctime.set(0);
		}
		else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void validate100()
	{
		/*VALD*/
		/* Validate the Contract Risk status against T5679.*/
		readT5679200();
		validateStatus300();
		/*EXIT*/
	}

protected void readT5679200()
	{
		t5679210();
	}

protected void t5679210()
	{
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}

protected void validateStatus300()
	{
		/*VALIDATE*/
		/* For this transaction we are not concerned with the premium*/
		/* status of the contract.*/
		invalidStatus.setTrue();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(drypDryprcRecInner.drypStatcode, t5679rec.cnRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		/*EXIT*/
	}

protected void readCovrbbl400()
	{
		/*START*/
		invalidStatus.setTrue();
		initialize(covrbblIO.getRecKeyData());
		initialize(covrbblIO.getRecNonKeyData());
		covrbblIO.setStatuz(varcom.oK);
		covrbblIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covrbblIO.setChdrnum(drypDryprcRecInner.drypEntity);
		covrbblIO.setBenBillDate(ZERO);
		covrbblIO.setFormat(covrbblrec);
		covrbblIO.setFunction(varcom.begn);
		while ( !(isEQ(covrbblIO.getStatuz(), varcom.endp)
		|| validStatus.isTrue())) {
			loopCovrbbl500();
		}
		
		/*EXIT*/
	}

protected void loopCovrbbl500()
	{
		try {
			loop510();
			nextr580();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

protected void loop510()
	{
		SmartFileCode.execute(appVars, covrbblIO);
		if (isNE(covrbblIO.getStatuz(), varcom.oK)
		&& isNE(covrbblIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covrbblIO.getParams());
			drylogrec.statuz.set(covrbblIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isEQ(covrbblIO.getStatuz(), varcom.endp)) {
			goTo(GotoLabel.exit590);
		}
		if (isNE(covrbblIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(covrbblIO.getChdrnum(), drypDryprcRecInner.drypEntity)) {
			covrbblIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit590);
		}
		getCoverageStatus600();
		validateCov700();
	}

protected void nextr580()
	{
		covrbblIO.setFunction(varcom.nextr);
	}

protected void getCoverageStatus600()
	{
		start600();
	}

protected void start600()
	{
		/* First retrieve the status codes from the COVR file.*/
		initialize(covrIO.getRecKeyData());
		initialize(covrIO.getRecNonKeyData());
		covrIO.setStatuz(varcom.oK);
		covrIO.setRrn(covrbblIO.getRrn());
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readd);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrIO.getParams());
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
	}

protected void validateCov700()
	{
		/*VALIDATE*/
		/* Finally compare the coverage statii to the valid statii on*/
		/* T5679.*/
		if (isNE(covrIO.getCoverage(), SPACES)
		&& isEQ(covrIO.getRider(), SPACES)
		|| isEQ(covrIO.getRider(), "00")) {
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(t5679rec.covRiskStat[wsaaT5679Sub.toInt()], covrIO.getStatcode())) {
					validStatus.setTrue();
				}
			}
		}
		/*    If we have a rider, check we have the correct status*/
		if (isNE(covrIO.getCoverage(), SPACES)
		&& isNE(covrIO.getRider(), "00")) {
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(t5679rec.ridRiskStat[wsaaT5679Sub.toInt()], covrIO.getStatcode())) {
					validStatus.setTrue();
				}
			}
		}
		/*EXIT*/
	}
//Modify for ILPI-61
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 
	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
}
@Override
protected FixedLengthStringData getWsaaProg() {
	// TODO Auto-generated method stub
	return wsaaProg;
}



@Override
protected FixedLengthStringData getWsaaVersion() {
	// TODO Auto-generated method stub
	return wsaaVersion;
}
}
