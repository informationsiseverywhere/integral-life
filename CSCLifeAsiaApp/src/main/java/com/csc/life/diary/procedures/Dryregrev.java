/*
 * File: Dryregrev.java
 * Date: December 3, 2013 2:27:32 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYREGREV.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;

import com.csc.diaryframework.parent.Maind;
import com.csc.life.diary.dataaccess.RegrdteTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.terminationclaims.dataaccess.ChdrrgpTableDAM;
import com.csc.life.terminationclaims.tablestructures.T6693rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(C) Copyright CSC Corporation Limited 1986 - 1999.
*    All rights reserved. CSC Confidential.
*
* This is the transaction detail record change subroutine for
* Regular Payment Review.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryregrev extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(9).init("DRYREGREV");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaT6693Key1 = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6693Paystat1 = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 0);
	private FixedLengthStringData wsaaT6693Crtable1 = new FixedLengthStringData(4).isAPartOf(wsaaT6693Key1, 2);
	private FixedLengthStringData filler = new FixedLengthStringData(2).isAPartOf(wsaaT6693Key1, 6, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidTranCode = new FixedLengthStringData(1).init("N");
	private Validator validTranCode = new Validator(wsaaValidTranCode, "Y");

	private FixedLengthStringData wsaaRegrStatuz = new FixedLengthStringData(1).init("N");
	private Validator validRegr = new Validator(wsaaRegrStatuz, "Y");

	private FixedLengthStringData wsaaValidContract = new FixedLengthStringData(1).init("N");
	private Validator validContract = new Validator(wsaaValidContract, "Y");

	private FixedLengthStringData wsaaValidCoverage = new FixedLengthStringData(1).init("N");
	private Validator validCoverage = new Validator(wsaaValidCoverage, "Y");
	private PackedDecimalData index1 = new PackedDecimalData(3, 0);
	private PackedDecimalData wsaaT5679Ix = new PackedDecimalData(3, 0).init(0);
	private PackedDecimalData wsaaSub = new PackedDecimalData(3, 0);
	private FixedLengthStringData wsaaStatcode = new FixedLengthStringData(2);
	private FixedLengthStringData wsaaPstcde = new FixedLengthStringData(2);
		/* FORMATS */
	private static final String chdrrgprec = "CHDRRGPREC";
	private static final String covrrec = "COVRREC";
	private static final String itemrec = "ITEMREC   ";
	private static final String regrdterec = "REGRDTEREC";
		/* TABLES */
	private static final String t5679 = "T5679";
	private static final String t6693 = "T6693";
	private ChdrrgpTableDAM chdrrgpIO = new ChdrrgpTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private RegrdteTableDAM regrdteIO = new RegrdteTableDAM();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Varcom varcom = new Varcom();
//	private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private T5679rec t5679rec = new T5679rec();
	private T6693rec t6693rec = new T6693rec();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

/**
 * Contains all possible labels used by goTo action.
 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr180, 
		exit190
	}

	public Dryregrev() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdNo.setTrue();
		/* Validate contract status*/
		validateContract200();
		if (!validContract.isTrue()) {
			return ;
		}
		wsaaRegrStatuz.set("N");
		/* Read the REGRDTE logical to get the earliest Regular*/
		/* Review Date for the contract details passed in linkage.*/
		regrdteIO.setParams(SPACES);
		regrdteIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		regrdteIO.setChdrnum(drypDryprcRecInner.drypEntity);
		regrdteIO.setRevdte(ZERO);
		regrdteIO.setFormat(regrdterec);
		regrdteIO.setFunction(varcom.begn);
		regrdteIO.setStatuz(varcom.oK);
		while ( !(isEQ(regrdteIO.getStatuz(), varcom.endp)
		|| validRegr.isTrue())) {
			readRegr100();
		}
		
		if (validRegr.isTrue()) {
			drypDryprcRecInner.dtrdYes.setTrue();
			drypDryprcRecInner.drypNxtprcdate.set(regrdteIO.getRevdte());
			drypDryprcRecInner.drypNxtprctime.set(ZERO);
		}
		else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void readRegr100()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read110();
				case nextr180: 
					nextr180();
				case exit190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

protected void read110()
	{
		SmartFileCode.execute(appVars, regrdteIO);
		if (isNE(regrdteIO.getStatuz(), varcom.oK)
		&& isNE(regrdteIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(regrdteIO.getParams());
			drylogrec.statuz.set(regrdteIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isNE(regrdteIO.getChdrcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(regrdteIO.getChdrnum(), drypDryprcRecInner.drypEntity)
		|| isEQ(regrdteIO.getStatuz(), varcom.endp)) {
			regrdteIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit190);
		}
		if (isEQ(regrdteIO.getRevdte(), varcom.vrcmMaxDate)
		|| isEQ(regrdteIO.getRevdte(), ZERO)) {
			goTo(GotoLabel.nextr180);
		}
		readT6693300();
		if (!validTranCode.isTrue()) {
			goTo(GotoLabel.nextr180);
		}
		validateCoverage400();
		if (!validCoverage.isTrue()) {
			goTo(GotoLabel.nextr180);
		}
		validRegr.setTrue();
	}

protected void nextr180()
	{
		regrdteIO.setFunction(varcom.nextr);
	}

protected void validateContract200()
	{
		start210();
	}

protected void start210()
	{
		chdrrgpIO.setParams(SPACES);
		chdrrgpIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrrgpIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrrgpIO.setFunction(varcom.readr);
		chdrrgpIO.setFormat(chdrrgprec);
		SmartFileCode.execute(appVars, chdrrgpIO);
		if (isNE(chdrrgpIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrrgpIO.getStatuz());
			drylogrec.params.set(chdrrgpIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		/* Read T5679 for valid status requirements for transactions.*/
		itemIO.setParams(SPACES);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypSystParm[1]);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		/* Validate the new contract status against T5679.*/
		wsaaValidContract.set("N");
		for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
		|| validContract.isTrue()); wsaaT5679Ix.add(1)){
			if (isEQ(t5679rec.cnRiskStat[wsaaT5679Ix.toInt()], chdrrgpIO.getStatcode())) {
				for (wsaaT5679Ix.set(1); !(isGT(wsaaT5679Ix, 12)
				|| validContract.isTrue()); wsaaT5679Ix.add(1)){
					if (isEQ(t5679rec.cnPremStat[wsaaT5679Ix.toInt()], chdrrgpIO.getPstatcode())) {
						wsaaValidContract.set("Y");
					}
				}
			}
		}
	}

protected void readT6693300()
	{
		start310();
	}

protected void start310()
	{
		/* Read T6693 to find allowable actions for current status.*/
		/* If actions for current status found, read through array of*/
		/* allowable transactions to determine if current transaction*/
		/* valid.*/
		wsaaValidTranCode.set("N");
		wsaaT6693Paystat1.set(regrdteIO.getRgpystat());
		wsaaT6693Crtable1.set(regrdteIO.getCrtable());
		itdmIO.setDataKey(SPACES);
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6693);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemitem(wsaaT6693Key1);
		itdmIO.setItmfrm(chdrrgpIO.getOccdate());
		itdmIO.setFormat(itemrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isNE(drypDryprcRecInner.drypCompany, itdmIO.getItemcoy())
		|| isNE(t6693, itdmIO.getItemtabl())
		|| isNE(wsaaT6693Key1, itdmIO.getItemitem())
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaT6693Crtable1.set("****");
			itdmIO.setItemitem(wsaaT6693Key1);
			SmartFileCode.execute(appVars, itdmIO);
			if (isNE(itdmIO.getStatuz(), varcom.oK)
			&& isNE(itdmIO.getStatuz(), varcom.endp)) {
				drylogrec.params.set(itdmIO.getParams());
				drylogrec.statuz.set(itdmIO.getStatuz());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();//ILPI-61
			}
			if (isNE(drypDryprcRecInner.drypCompany, itdmIO.getItemcoy())
			|| isNE(t6693, itdmIO.getItemtabl())
			|| isNE(wsaaT6693Key1, itdmIO.getItemitem())
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				itdmIO.setStatuz(varcom.endp);
				itdmIO.setGenarea(SPACES);
				return ;
			}
		}
		t6693rec.t6693Rec.set(itdmIO.getGenarea());
		for (index1.set(1); !(isGT(index1, 12)); index1.add(1)){
			if (isEQ(t6693rec.trcode[index1.toInt()], drypDryprcRecInner.drypSystParm[1])) {
				validTranCode.setTrue();
				index1.set(15);
			}
		}
	}

protected void validateCoverage400()
	{
		start410();
	}

protected void start410()
	{
		/* Read the coverage record and validate the status against*/
		/* those on T5679.*/
		/* Read in the relevant COVR record.*/
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covrIO.setChdrnum(regrdteIO.getChdrnum());
		covrIO.setLife(regrdteIO.getLife());
		covrIO.setCoverage(regrdteIO.getCoverage());
		covrIO.setRider(regrdteIO.getRider());
		covrIO.setPlanSuffix(regrdteIO.getPlanSuffix());
		covrIO.setFormat(covrrec);
		covrIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, covrIO);
		if (isNE(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrIO.getParams());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		/* Validate the coverage statii against T5679.*/
		wsaaValidCoverage.set("N");
		wsaaStatcode.set(covrIO.getStatcode());
		wsaaPstcde.set(covrIO.getPstatcode());
		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
			validateCovrStatus500();
		}
	}

protected void validateCovrStatus500()
	{
		/*START*/
		/* Validate coverage risk status*/
		if (isEQ(t5679rec.covRiskStat[wsaaSub.toInt()], wsaaStatcode)) {
			for (wsaaSub.set(1); !(isGT(wsaaSub, 12)); wsaaSub.add(1)){
				validateCovrPremStatus600();
			}
		}
		/*EXIT*/
	}

protected void validateCovrPremStatus600()
	{
		/*START*/
		/* Validate coverage premium status*/
		if (isEQ(t5679rec.covPremStat[wsaaSub.toInt()], wsaaPstcde)) {
			wsaaSub.set(13);
			validCoverage.setTrue();
		}
		/*EXIT*/
	}
//Modify for ILPI-61 
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
