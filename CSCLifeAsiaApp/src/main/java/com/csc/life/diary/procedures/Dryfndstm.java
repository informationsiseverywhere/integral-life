/*
 * File: Dryfndstm.java
 * Date: December 3, 2013 2:24:44 AM ICT
 * Author: CSC
 * 
 * Class transformed from DRYFNDSTM.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;

import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon2;
import com.csc.fsu.general.recordstructures.Datcon2rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.regularprocessing.tablestructures.T5655rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Freqcpy;
import com.csc.smart.recordstructures.Itdmkey;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*REMARKS.
*
* This is the transaction detail record change subroutine for
* Fund Statement Processing.
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dryfndstm extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYFNDSTM");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private PackedDecimalData wsaaAdjustedDate = new PackedDecimalData(8, 0);
	private final String wsaaWildcard = "***";

	private FixedLengthStringData wsaaT5655Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5655Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT5655Key, 0);
	private FixedLengthStringData wsaaT5655Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT5655Key, 4);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaT5655Key, 7, FILLER).init(SPACES);
	private ZonedDecimalData wsaaT5655Leaddays = new ZonedDecimalData(3, 0);
	private PackedDecimalData wsaaT5679Sub = new PackedDecimalData(2, 0).setUnsigned();
	private static final int wsaaT5679Max = 12;

	private FixedLengthStringData wsaaT5679Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT5679Trcde = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 0);
	private FixedLengthStringData filler1 = new FixedLengthStringData(4).isAPartOf(wsaaT5679Key, 4, FILLER).init(SPACES);

	private FixedLengthStringData wsaaValidStatus = new FixedLengthStringData(1).init("N");
	private Validator validStatus = new Validator(wsaaValidStatus, "Y");
	private Validator invalidStatus = new Validator(wsaaValidStatus, "N");
		/* FORMATS */
	private static final String itdmrec = "ITEMREC";
	private static final String itemrec = "ITEMREC";
		/* ERRORS */
	private static final String h038 = "H038";
	private static final String f321 = "F321";
		/* TABLES */
	private static final String t5655 = "T5655";
	private static final String t5679 = "T5679";
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private T5655rec t5655rec = new T5655rec();
	private T5679rec t5679rec = new T5679rec();
	private Itdmkey wsaaItdmkey = new Itdmkey();
	private Datcon2rec datcon2rec = new Datcon2rec();
	private Varcom varcom = new Varcom();
	private Freqcpy freqcpy = new Freqcpy();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	public Dryfndstm() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainLine000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void mainLine000()
	{
		main010();
		exit090();
	}

protected void main010()
	{
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		readT5679500();
		for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
		|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
			if (isEQ(drypDryprcRecInner.drypStatcode, t5679rec.cnRiskStat[wsaaT5679Sub.toInt()])) {
				validStatus.setTrue();
			}
		}
		if (validStatus.isTrue()) {
			invalidStatus.setTrue();
			for (wsaaT5679Sub.set(1); !(isGT(wsaaT5679Sub, wsaaT5679Max)
			|| validStatus.isTrue()); wsaaT5679Sub.add(1)){
				if (isEQ(drypDryprcRecInner.drypPstatcode, t5679rec.cnPremStat[wsaaT5679Sub.toInt()])) {
					validStatus.setTrue();
				}
			}
		}
		if (invalidStatus.isTrue()) {
			drypDryprcRecInner.drypStmdte.set(varcom.vrcmMaxDate);
		}
		/* Determine if Fund Statement Processing is needed.*/
		if (isEQ(drypDryprcRecInner.drypStmdte, varcom.vrcmMaxDate)){
			drypDryprcRecInner.dtrdNo.setTrue();
		}
		else if (isEQ(drypDryprcRecInner.drypStmdte, ZERO)){
			drypDryprcRecInner.dtrdNo.setTrue();
		}
		else{
			validate100();
		}
	}

protected void exit090()
	{
		exitProgram();
	}

protected void validate100()
	{
		/*VALID*/
		getAdjustedDate200();
		drypDryprcRecInner.drypNxtprcdate.set(wsaaAdjustedDate);
		drypDryprcRecInner.drypNxtprctime.set(ZERO);
		/*EXIT*/
	}

protected void getAdjustedDate200()
	{
		begn201();
	}

protected void begn201()
	{
		initialize(wsaaAdjustedDate);
		getT5655Details300();
		wsaaT5655Leaddays.set(t5655rec.leadDays);
		compute(wsaaT5655Leaddays, 0).set(mult(wsaaT5655Leaddays, -1));
		datcon2rec.freqFactor.set(wsaaT5655Leaddays);
		datcon2rec.frequency.set(freqcpy.daily);
		datcon2rec.intDate1.set(drypDryprcRecInner.drypStmdte);
		callProgram(Datcon2.class, datcon2rec.datcon2Rec);
		if (isNE(datcon2rec.statuz, varcom.oK)) {
			drylogrec.params.set(datcon2rec.datcon2Rec);
			drylogrec.statuz.set(datcon2rec.statuz);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61
		}
		wsaaAdjustedDate.set(datcon2rec.intDate2);
	}

protected void getT5655Details300()
	{
		get310();
	}

protected void get310()
	{
		wsaaT5655Key.set(SPACES);
		wsaaT5655Trancode.set(drypDryprcRecInner.drypSystParm[1]);
		wsaaT5655Cnttype.set(drypDryprcRecInner.drypCnttype);
		readT5655400();
		if (isNE(itdmIO.getItemtabl(), t5655)
		|| isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
		|| isNE(itdmIO.getItemitem(), wsaaT5655Key)
		|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
			wsaaT5655Trancode.set(drypDryprcRecInner.drypSystParm[1]);
			wsaaT5655Cnttype.set(wsaaWildcard);
			readT5655400();
			if (isNE(itdmIO.getItemtabl(), t5655)
			|| isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
			|| isNE(itdmIO.getItemitem(), wsaaT5655Key)
			|| isEQ(itdmIO.getStatuz(), varcom.endp)) {
				drylogrec.statuz.set(h038);
				itdmIO.setDataKey(wsaaItdmkey.itdmKey);
				drylogrec.params.set(itdmIO.getParams());
				drylogrec.drySystemError.setTrue();
				a000FatalError();//ILPI-61
			}
		}
		t5655rec.t5655Rec.set(itdmIO.getGenarea());
	}

protected void readT5655400()
	{
		read410();
	}

protected void read410()
	{
		initialize(itdmIO.getItemrecKeyData());
		initialize(itdmIO.getItemrecNonKeyData());
		itdmIO.setStatuz(varcom.oK);
		itdmIO.setItempfx(smtpfxcpy.item);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5655);
		itdmIO.setItemitem(wsaaT5655Key);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		wsaaItdmkey.itdmKey.set(itdmIO.getDataKey());
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if (isNE(itdmIO.getStatuz(), varcom.oK)
		&& isNE(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
	}

protected void readT5679500()
	{
		t5679510();
	}

protected void t5679510()
	{
		wsaaT5679Trcde.set(drypDryprcRecInner.drypSystParm[1]);
		initialize(itemIO.getRecKeyData());
		initialize(itemIO.getRecNonKeyData());
		itemIO.setStatuz(varcom.oK);
		itemIO.setItempfx(smtpfxcpy.item);
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(wsaaT5679Key);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if (isNE(itemIO.getStatuz(), varcom.oK)
		&& isNE(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61
		}
		if (isEQ(itemIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(f321);
			drylogrec.drySystemError.setTrue();
			a000FatalError();//ILPI-61
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
	}
//Modify for ILPI-61 
//protected void fatalError()
//	{
//		/*A010-FATAL*/
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		}
//		else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		//callProgram(Drylog.class, drylogrec.drylogRec);
//		/*A090-EXIT*/
//		//exitProgram();
//		a000FatalError();
//	}
/*
 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
 */
private static final class DrypDryprcRecInner { 

	//ILPI-97 starts
	private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
	private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
	private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
	private Validator noError = new Validator(drypError, "0");
	private Validator nonFatalError = new Validator(drypError, "1");
	private Validator fatalError = new Validator(drypError, "2");
	private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
	private Validator batchMode = new Validator(drypMode, "B");
	private Validator onlineMode = new Validator(drypMode, "O");
	private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
	private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
	private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
	private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
	private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
	private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
	private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
	private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
	private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
	private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
	private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
	private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
	private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
	private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
	private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
	private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
	private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
	private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
	private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
	private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
	//cluster support by vhukumagrawa
	private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
	private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
	private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
	private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
	private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
	private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
	private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
	private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
	private Validator rollForwardMode = new Validator(drypRollForward, "R");
	private Validator normalMode = new Validator(drypRollForward, " ");
	private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
	private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
	private Validator dtrdYes = new Validator(drypRequired, "Y");
	private Validator dtrdNo = new Validator(drypRequired, "N");
	private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
	private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
	private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
	private Validator processSuccesful = new Validator(drypProcessResult, "Y");
	private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
	//ILPI-97 ends
	private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
	private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
	private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
	private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
	private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
	private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
	private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
