/*
 * File: Dry5363rp.java
 * Date: March 26, 2014 3:03:33 PM ICT
 * Author: CSC
 * 
 * Class transformed from DRY5363RP.CBL
 * 
 * Copyright (2007) CSC Asia, all rights reserved.
 */
package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import com.csc.diary.dataaccess.DrptsrtTableDAM;
import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.flexiblepremium.reports.R5363Report;
import com.csc.smart.dataaccess.DescTableDAM;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Smtpfxcpy;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
import com.quipoz.framework.util.QPUtilities;

/**
* <pre>
*
*(c) Copyright Continuum Corporation Ltd.  1986....1995.
*    All rights reserved.  Continuum Confidential.
*
*           REPORT ON UNDERTARGET FLEXIBLE PREMIUMS.
*           ----------------------------------------
*
****************************************************************** ****
*                                                                     *
* </pre>
*/
public class Dry5363rp extends Maind {//Modify for ILPI-65 

	public static final String ROUTINE = QPUtilities.getThisClass();
	public int numberOfParameters = 0;
	private R5363Report printFile = new R5363Report();
	private FixedLengthStringData printRecord = new FixedLengthStringData(132);
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRY5363RP");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");

	private FixedLengthStringData wsaaOverflow = new FixedLengthStringData(1);
	private Validator pageOverflow = new Validator(wsaaOverflow, "Y");
	private PackedDecimalData wsaaToday = new PackedDecimalData(8, 0).init(ZERO);
	private PackedDecimalData wsaaTotReqd = new PackedDecimalData(18, 2).init(ZERO);
	private ZonedDecimalData wsaaRptCount = new ZonedDecimalData(5, 0).init(ZERO).setUnsigned();

	private FixedLengthStringData wsaaSortKey = new FixedLengthStringData(23);
	private FixedLengthStringData wsaaSortChdrnum = new FixedLengthStringData(8).isAPartOf(wsaaSortKey, 0);
	private FixedLengthStringData wsaaSortLife = new FixedLengthStringData(2).isAPartOf(wsaaSortKey, 8);
	private FixedLengthStringData wsaaSortCoverage = new FixedLengthStringData(2).isAPartOf(wsaaSortKey, 10);
	private FixedLengthStringData wsaaSortRider = new FixedLengthStringData(2).isAPartOf(wsaaSortKey, 12);
	private ZonedDecimalData wsaaSortPlnsfx = new ZonedDecimalData(4, 0).isAPartOf(wsaaSortKey, 14);
	private PackedDecimalData wsaaSortTargto = new PackedDecimalData(8, 0).isAPartOf(wsaaSortKey, 18).setUnsigned();
	private static final String t1693 = "T1693";
		/* FORMATS */
	private static final String drptsrtrec = "DRPTSRTREC";
	private static final String descrec = "DESCREC";

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");

	private FixedLengthStringData r5363h01Record = new FixedLengthStringData(31);
	private FixedLengthStringData r5363h01O = new FixedLengthStringData(31).isAPartOf(r5363h01Record, 0);
	private FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(r5363h01O, 0);
	private FixedLengthStringData companynm = new FixedLengthStringData(30).isAPartOf(r5363h01O, 1);

	private FixedLengthStringData r5363d02Record = new FixedLengthStringData(18);
	private FixedLengthStringData r5363d02O = new FixedLengthStringData(18).isAPartOf(r5363d02Record, 0);
	private ZonedDecimalData minreqd = new ZonedDecimalData(18, 2).isAPartOf(r5363d02O, 0);

	private FixedLengthStringData r5363t01Record = new FixedLengthStringData(2);
	private DescTableDAM descIO = new DescTableDAM();
	private DrptsrtTableDAM drptsrtIO = new DrptsrtTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private Varcom varcom = new Varcom();
	private Smtpfxcpy smtpfxcpy = new Smtpfxcpy();
	private Datcon1rec datcon1rec = new Datcon1rec();
	//private Drylogrec drylogrec = new Drylogrec();//Modify for ILPI-65 
	private Dryoutrec dryoutrec = new Dryoutrec();
	private DryrDryrptRecInner dryrDryrptRecInner = new DryrDryrptRecInner();
	private R5363d01RecordInner r5363d01RecordInner = new R5363d01RecordInner();

	public Dry5363rp() {
		super();
	}



	/**
	* The mainline method is the default entry point to the class
	*/
public void mainline(Object... parmArray)
	{
		dryoutrec.dryoutRec = convertAndSetParam(dryoutrec.dryoutRec, parmArray, 0);
		try {
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}

protected void startProcessing100()
	{
		start110();
		exit190();
	}

protected void start110()
	{
		printFile.openOutput();
		wsaaOverflow.set("Y");
		/* Initialise any working storage fields.*/
		wsaaSortKey.set(SPACES);
		wsaaSortPlnsfx.set(ZERO);
		wsaaTotReqd.set(ZERO);
		/* Set up today's date...*/
		datcon1rec.function.set(varcom.tday);
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		dryoutrec.statuz.set(varcom.oK);
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		/* Set up the DRPTSRT ready to read...*/
		drptsrtIO.setRecKeyData(SPACES);
		drptsrtIO.setStatuz(varcom.oK);
		drptsrtIO.setEffdate(dryoutrec.effectiveDate);
		drptsrtIO.setDiaryEntityCompany(dryoutrec.company);
		drptsrtIO.setDiaryEntityBranch(dryoutrec.branch);
		drptsrtIO.setDiaryReportName(dryoutrec.reportName);
		drptsrtIO.setDiaryRunNumber(dryoutrec.runNumber);
		drptsrtIO.setSortkey(SPACES);
		drptsrtIO.setFormat(drptsrtrec);
		drptsrtIO.setFunction(varcom.begn);
		/* Loop through each DRPTSRT record printing the R5363*/
		/* report ..*/
		while ( !(isEQ(drptsrtIO.getStatuz(), varcom.endp))) {
			callDrpt200();
		}
		
		endReport500();
		printFile.close();
	}

protected void exit190()
	{
		exitProgram();
	}

protected void callDrpt200()
	{
		call210();
	}

protected void call210()
	{
		SmartFileCode.execute(appVars, drptsrtIO);
		if (isNE(drptsrtIO.getStatuz(), varcom.oK)
		&& isNE(drptsrtIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(drptsrtIO.getParams());
			drylogrec.statuz.set(drptsrtIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		if (isEQ(drptsrtIO.getStatuz(), varcom.endp)
		|| isNE(drptsrtIO.getDiaryEntityCompany(), dryoutrec.company)
		|| isNE(drptsrtIO.getDiaryEntityBranch(), dryoutrec.branch)
		|| isNE(drptsrtIO.getDiaryReportName(), dryoutrec.reportName)) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		if (isNE(dryoutrec.runNumber, ZERO)
		&& isNE(dryoutrec.runNumber, drptsrtIO.getDiaryRunNumber())) {
			drptsrtIO.setStatuz(varcom.endp);
			return ;
		}
		/* Move the sort key details to the report copybook.*/
		dryrDryrptRecInner.r5363SortKey.set(drptsrtIO.getSortkey());
		writeLine300();
		/* Set up the working storage sort key.*/
		wsaaSortChdrnum.set(dryrDryrptRecInner.r5363Chdrnum);
		wsaaSortLife.set(dryrDryrptRecInner.r5363Life);
		wsaaSortCoverage.set(dryrDryrptRecInner.r5363Coverage);
		wsaaSortRider.set(dryrDryrptRecInner.r5363Rider);
		wsaaSortPlnsfx.set(dryrDryrptRecInner.r5363Plnsfx);
		wsaaSortTargto.set(dryrDryrptRecInner.r5363Targto);
		drptsrtIO.setFunction(varcom.nextr);
	}

protected void writeLine300()
	{
		write310();
	}

protected void write310()
	{
		/* Check first if totals need to be written.*/
		checkTotals600();
		/* Write New Page if required.*/
		if (pageOverflow.isTrue()) {
			wsaaOverflow.set("N");
			newPage400();
		}
		/* Fill the detail record.*/
		dryrDryrptRecInner.dryrGenarea.set(drptsrtIO.getGenarea());
		r5363d01RecordInner.chdrnum.set(dryrDryrptRecInner.r5363Chdrnum);
		r5363d01RecordInner.life.set(dryrDryrptRecInner.r5363Life);
		r5363d01RecordInner.coverage.set(dryrDryrptRecInner.r5363Coverage);
		r5363d01RecordInner.rider.set(dryrDryrptRecInner.r5363Rider);
		r5363d01RecordInner.plnsfx.set(dryrDryrptRecInner.r5363Plnsfx);
		r5363d01RecordInner.prmper.set(dryrDryrptRecInner.r5363Prmper);
		r5363d01RecordInner.billedp.set(dryrDryrptRecInner.r5363Billedp);
		r5363d01RecordInner.prmrcdp.set(dryrDryrptRecInner.r5363Prmrcdp);
		r5363d01RecordInner.minovrpro.set(dryrDryrptRecInner.r5363Minovrpro);
		compute(r5363d01RecordInner.ovrminreq, 0).set(sub(dryrDryrptRecInner.r5363Ovrminreq, dryrDryrptRecInner.r5363Prmrcdp));
		wsaaTotReqd.add(dryrDryrptRecInner.r5363Ovrminreq);
		datcon1rec.intDate.set(dryrDryrptRecInner.r5363Targto);
		datcon1rec.function.set(varcom.conv);
		callDatcon1700();
		r5363d01RecordInner.targto.set(datcon1rec.extDate);
		/* Increment the report record count..*/
		wsaaRptCount.add(1);
		/* Write the detail line to the report ....*/
		printRecord.set(SPACES);
		printFile.printR5363d01(r5363d01RecordInner.r5363d01Record, indicArea);
	}

protected void newPage400()
	{
		new400();
	}

protected void new400()
	{
		/* Fill header record.*/
		company.set(dryoutrec.company);
		/* Get the company description..*/
		descIO.setDescpfx(smtpfxcpy.item);
		descIO.setDesccoy("0");
		descIO.setDesctabl(t1693);
		descIO.setDescitem(dryoutrec.company);
		descIO.setItemseq(SPACES);
		descIO.setLanguage(dryoutrec.language);
		descIO.setFunction(varcom.readr);
		descIO.setFormat(descrec);
		SmartFileCode.execute(appVars, descIO);
		if (isNE(descIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(descIO.getStatuz());
			drylogrec.params.set(descIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		companynm.set(descIO.getLongdesc());
		/*  Write the header details.....*/
		printRecord.set(SPACES);
		printFile.printR5363h01(r5363h01Record, indicArea);
	}

protected void endReport500()
	{
		/*NEW*/
		/* Write New Page if required.*/
		if (pageOverflow.isTrue()) {
			wsaaOverflow.set("N");
			newPage400();
		}
		/*  Write the end of report line*/
		printFile.printR5363t01(r5363t01Record, indicArea);
		/*EXIT*/
	}

protected void checkTotals600()
	{
		new610();
	}

protected void new610()
	{
		/* Report totals will need to be printed on a change of key,*/
		/* if the total required balance is not zero.*/
		/* If there are no records printed, then exit this section..*/
		if (isEQ(wsaaRptCount, 0)) {
			return ;
		}
		/* If the key has changed, proceed.*/
		if (isEQ(dryrDryrptRecInner.r5363Chdrnum, wsaaSortChdrnum)
		&& isEQ(dryrDryrptRecInner.r5363Life, wsaaSortLife)
		&& isEQ(dryrDryrptRecInner.r5363Coverage, wsaaSortCoverage)
		&& isEQ(dryrDryrptRecInner.r5363Rider, wsaaSortRider)
		&& isEQ(dryrDryrptRecInner.r5363Plnsfx, wsaaSortPlnsfx)
		&& isEQ(dryrDryrptRecInner.r5363Targto, wsaaSortTargto)) {
			return ;
		}
		/* Check the total required balance.*/
		if (isEQ(wsaaTotReqd, ZERO)) {
			return ;
		}
		/* If a balance remains, write detail line 2.*/
		minreqd.set(wsaaTotReqd);
		wsaaTotReqd.set(ZERO);
		printRecord.set(SPACES);
		printFile.printR5363d02(r5363d02Record, indicArea);
	}

protected void callDatcon1700()
	{
		/*CALL*/
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		if (isNE(datcon1rec.statuz, varcom.oK)) {
			drylogrec.statuz.set(datcon1rec.statuz);
			drylogrec.params.set(datcon1rec.datcon1Rec);
			drylogrec.dryDatabaseError.setTrue();
			fatalError();//Modify for ILPI-65 
		}
		/*EXIT*/
	}
//Modify for ILPI-65 
protected void fatalError()
	{
		/*A010-FATAL*/
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(dryoutrec.effectiveDate);
		drylogrec.runNumber.set(dryoutrec.runNumber);
		drylogrec.entityKey.set(SPACES);
		dryoutrec.statuz.set(drylogrec.statuz);
		//callProgram(Drylog.class, drylogrec.drylogRec);
		/*A090-EXIT*/
		//exitProgram();
		a000FatalError();
	}
/*
 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
 */
private static final class DryrDryrptRecInner { 
	private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);

	private FixedLengthStringData r5363DataArea = new FixedLengthStringData(495).isAPartOf(dryrGenarea, 0, REDEFINE);
	private ZonedDecimalData r5363Prmper = new ZonedDecimalData(17, 2).isAPartOf(r5363DataArea, 0);
	private ZonedDecimalData r5363Billedp = new ZonedDecimalData(17, 2).isAPartOf(r5363DataArea, 17);
	private ZonedDecimalData r5363Prmrcdp = new ZonedDecimalData(17, 2).isAPartOf(r5363DataArea, 34);
	private ZonedDecimalData r5363Ovrminreq = new ZonedDecimalData(17, 2).isAPartOf(r5363DataArea, 51);
	private ZonedDecimalData r5363Minovrpro = new ZonedDecimalData(3, 0).isAPartOf(r5363DataArea, 68);

	private FixedLengthStringData r5363SortKey = new FixedLengthStringData(32);
	private FixedLengthStringData r5363Chdrnum = new FixedLengthStringData(8).isAPartOf(r5363SortKey, 0);
	private FixedLengthStringData r5363Life = new FixedLengthStringData(2).isAPartOf(r5363SortKey, 8);
	private FixedLengthStringData r5363Coverage = new FixedLengthStringData(2).isAPartOf(r5363SortKey, 10);
	private FixedLengthStringData r5363Rider = new FixedLengthStringData(2).isAPartOf(r5363SortKey, 12);
	private ZonedDecimalData r5363Plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5363SortKey, 14);
	private PackedDecimalData r5363Targto = new PackedDecimalData(8, 0).isAPartOf(r5363SortKey, 18).setUnsigned();
}
/*
 * Class transformed  from Data Structure R5363D01-RECORD--INNER
 */
private static final class R5363d01RecordInner { 

	private FixedLengthStringData r5363d01Record = new FixedLengthStringData(99);
	private FixedLengthStringData r5363d01O = new FixedLengthStringData(99).isAPartOf(r5363d01Record, 0);
	private FixedLengthStringData chdrnum = new FixedLengthStringData(8).isAPartOf(r5363d01O, 0);
	private FixedLengthStringData life = new FixedLengthStringData(2).isAPartOf(r5363d01O, 8);
	private FixedLengthStringData coverage = new FixedLengthStringData(2).isAPartOf(r5363d01O, 10);
	private FixedLengthStringData rider = new FixedLengthStringData(2).isAPartOf(r5363d01O, 12);
	private ZonedDecimalData plnsfx = new ZonedDecimalData(4, 0).isAPartOf(r5363d01O, 14);
	private FixedLengthStringData targto = new FixedLengthStringData(10).isAPartOf(r5363d01O, 18);
	private ZonedDecimalData prmper = new ZonedDecimalData(17, 2).isAPartOf(r5363d01O, 28);
	private ZonedDecimalData billedp = new ZonedDecimalData(17, 2).isAPartOf(r5363d01O, 45);
	private ZonedDecimalData prmrcdp = new ZonedDecimalData(17, 2).isAPartOf(r5363d01O, 62);
	private ZonedDecimalData ovrminreq = new ZonedDecimalData(17, 2).isAPartOf(r5363d01O, 79);
	private ZonedDecimalData minovrpro = new ZonedDecimalData(3, 0).isAPartOf(r5363d01O, 96);
}
//Added for ILPI-65 
@Override
protected FixedLengthStringData getWsaaProg() {
	return wsaaProg;
}

@Override
protected FixedLengthStringData getWsaaVersion() {
	return wsaaVersion;
}
}
