package com.csc.life.diary.recordstructures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FILLER;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;

import com.quipoz.COBOLFramework.COBOLFunctions;
import com.quipoz.COBOLFramework.datatype.ExternalData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;

/**
 * 	
 * @author: CSC
 * @version
 * Creation Date: Tue, 3 Dec 2013 04:08:46
 * Description:
 * Copybook name: COVRRNWKEY
 *
 * Copyright (2007) CSC Asia, all rights reserved
 *
 */
public class Covrrnwkey extends ExternalData {


  //*******************************
  //Attribute Declarations
  //*******************************
  	public FixedLengthStringData covrrnwFileKey = new FixedLengthStringData(256);
  
  	public FixedLengthStringData covrrnwKey = new FixedLengthStringData(256).isAPartOf(covrrnwFileKey, 0, REDEFINE);
  	public FixedLengthStringData covrrnwChdrcoy = new FixedLengthStringData(1).isAPartOf(covrrnwKey, 0);
  	public FixedLengthStringData covrrnwChdrnum = new FixedLengthStringData(8).isAPartOf(covrrnwKey, 1);
  	public PackedDecimalData covrrnwAnnivProcDate = new PackedDecimalData(8, 0).isAPartOf(covrrnwKey, 9);
  	public FixedLengthStringData filler = new FixedLengthStringData(242).isAPartOf(covrrnwKey, 14, FILLER);


	public void initialize() {
		COBOLFunctions.initialize(covrrnwFileKey);
	}	

	
	public FixedLengthStringData getBaseString() {
  		if (baseString == null) {
   			baseString = new FixedLengthStringData(getLength());
    		covrrnwFileKey.isAPartOf(baseString, true);
   			baseString.resetIsAPartOfOffset();
  		}
  		return baseString;
	}


}