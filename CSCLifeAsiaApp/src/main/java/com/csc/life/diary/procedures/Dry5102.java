package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.IndicatorArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZEROES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZEROS;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.initialize;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;
import static com.quipoz.COBOLFramework.COBOLFunctions.subString;

import java.util.List;

import com.csc.diary.dataaccess.DacmTableDAM;
import com.csc.diary.recordstructures.Dryrptrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Dryprclnk;
import com.csc.fsu.general.recordstructures.Zrdecplrec;
import com.csc.fsu.general.tablestructures.T3629rec;
import com.csc.life.diary.recordstructures.R5106rec;
import com.csc.life.diary.recordstructures.R5107rec;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrlifTableDAM;
import com.csc.life.productdefinition.procedures.Lifacmv;
import com.csc.life.productdefinition.recordstructures.Lifacmvrec;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovrudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UdivTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UfndudlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnalocTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.dao.UtrnpfDAO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Dry5102DTO;
import com.csc.life.unitlinkedprocessing.dataaccess.model.Utrnpf;
import com.csc.life.unitlinkedprocessing.procedures.Ufprice;
import com.csc.life.unitlinkedprocessing.recordstructures.Udtrigrec;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufndudlkey;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5544rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.procedures.Getdesc;
import com.csc.smart.recordstructures.Batcdorrec;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Datcon1rec;
import com.csc.smart.recordstructures.Getdescrec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Datcon1;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.Indicator;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;
/**
 *REMARKS.
 *
 *  Diary System Unit Dealing Subroutine
 *  ====================================
 *
 *  OVERVIEW.
 *
 *  This program will check if any undealt UTRNs exist for a
 *  given contract, and "deal" each record found.
 *  Each "deal" involves buying and selling units
 *  in the fund identified on the UTRN.
 *
 ***********************************************************************
 *                                                                     *
 * ......... New Version of the Amendment History.                     *
 *                                                                     *
 ***********************************************************************
 *           AMENDMENT  HISTORY                                        *
 ***********************************************************************
 * DATE.... VSN/MOD  WORK UNIT    BY....                               *
 *                                                                     *
 * 30/03/00  01/01   DRYAPL       Jacco Landskroon                     *
 *           Initial Version.                                          *
 *                                                                     *
 * 18/10/05  01/01   A07653       Warren Ornstein                      *
 *           Do not allow UTRN to be dealt if the amount cannot        *
 *           be realised for part surrenders and switches.             *
 *                                                                     *
 * 16/02/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
 *           Comment out the OVRD-BRANCH.                              *
 * LA1188    Fund amount and no of units equal zeroes resulted from    *
 *           currency conversion. Not due to dummy trans with          *
 *           'Trigger' module for delay processing.                    *
 * LA1189    Set up value for ULTG-USER.                               *
 *                                                                     *
 *           Have to keep DRYPRCREC info before calling the Trigger    *
 *           subroutine, as now UNLPRTT also create DTRD for Partial   *
 *           Withdrawl and using the same copybook.                    *
 *                                                                     *
 * 26/07/06  01/01   A07653       Karen McLeod                         *
 *           Check for DUPR when writing a UDIV record.                *
 *                                                                     *
 * 27/11/08  01/01   LA4404       Jacco Landskroon                     *
 *           Use READD to find the UTRNALO record as same key          *
 *           processing can get incorrect UTRN record.                 *
 *                                                                     *
 * 14/01/09  01/01   ZM0023       Jacco Landskroon                     *
 *           Remove redundant UDEL processing.                         *
 *                                                                     *
 * 18/02/09  01/01   LA4515       Kristin Mcleish                      *
 *           After looping through all UTRNEXT records, loop           *
 *           through again to see if there are any switch              *
 *           UTRNs to deal that were created in the first loop         *
 *           by the fund switch trigger module.                        *
 *                                                                     *
 **DD/MM/YY*************************************************************
 *                                                                     */
public class Dry5102 extends Maind {
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5102");

	private PackedDecimalData ix = new PackedDecimalData(5);

	private PackedDecimalData iy = new PackedDecimalData(5);

	private ZonedDecimalData wsaaPlan = new ZonedDecimalData(2);

	private ZonedDecimalData wsaaJrnseq = new ZonedDecimalData(4);

	private PackedDecimalData wsaaPrice = new PackedDecimalData(9, 5);

	private PackedDecimalData wsaaUfprBarePrice = new PackedDecimalData(16, 5);

	private PackedDecimalData wsaaUfprPriceDate = new PackedDecimalData(8,0);

	private PackedDecimalData wsaaWithinRange = new PackedDecimalData(5);

	private FixedLengthStringData wsaaInsuffUnitsMeth = new FixedLengthStringData(1);

	private PackedDecimalData wsaaDebt = new PackedDecimalData(15, 2);
	private PackedDecimalData wsaaFund = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaBidoffer = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaBarebid = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaCharge = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaPostFund = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostBidoffer = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostBarebid = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostCharge = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBalanceCheck = new PackedDecimalData(13, 2);

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaReversalUtrn = new FixedLengthStringData(1);
	private Validator reversalUtrn = new Validator(wsaaReversalUtrn, "Y");

	//	private FixedLengthStringData wsaaFirstTime = new FixedLengthStringData(1);
	//	private Validator firstTime = new Validator(wsaaFirstTime, "Y");

	private PackedDecimalData wsaaPostFundAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostBidofferAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostBarebidAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaPostChargeAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaBalanceCheckAcct = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaUtrnaloFundAmount = new PackedDecimalData(13, 2);
	private PackedDecimalData wsaaNominalRate = new PackedDecimalData(18, 9);

	//	private PackedDecimalData wsaaT5645Size = new PackedDecimalData(5).init(45);

	private PackedDecimalData t5645Ix = new PackedDecimalData(5, 0);	

	private FixedLengthStringData wsaaDryprcrec = new FixedLengthStringData(600);

	private ZonedDecimalData controlTotals = new ZonedDecimalData(70);
	private ZonedDecimalData ct01 = new ZonedDecimalData(2).isAPartOf(controlTotals, 0).init(01);
	private ZonedDecimalData ct02 = new ZonedDecimalData(2).isAPartOf(controlTotals, 2).init(02);
	private ZonedDecimalData ct03 = new ZonedDecimalData(2).isAPartOf(controlTotals, 4).init(03);
	private ZonedDecimalData ct04 = new ZonedDecimalData(2).isAPartOf(controlTotals, 6).init(04);
	private ZonedDecimalData ct05 = new ZonedDecimalData(2).isAPartOf(controlTotals, 8).init(05);
	private ZonedDecimalData ct06 = new ZonedDecimalData(2).isAPartOf(controlTotals, 10).init(06);
	private ZonedDecimalData ct07 = new ZonedDecimalData(2).isAPartOf(controlTotals, 12).init(07);
	private ZonedDecimalData ct08 = new ZonedDecimalData(2).isAPartOf(controlTotals, 14).init(8);
	private ZonedDecimalData ct09 = new ZonedDecimalData(2).isAPartOf(controlTotals, 16).init(9);
	private ZonedDecimalData ct10 = new ZonedDecimalData(2).isAPartOf(controlTotals, 18).init(10);
	private ZonedDecimalData ct11 = new ZonedDecimalData(2).isAPartOf(controlTotals, 20).init(11);
	private ZonedDecimalData ct12 = new ZonedDecimalData(2).isAPartOf(controlTotals, 22).init(12);
	private ZonedDecimalData ct13 = new ZonedDecimalData(2).isAPartOf(controlTotals, 24).init(13);
	private ZonedDecimalData ct14 = new ZonedDecimalData(2).isAPartOf(controlTotals, 26).init(14);
	private ZonedDecimalData ct15 = new ZonedDecimalData(2).isAPartOf(controlTotals, 28).init(15);
	private ZonedDecimalData ct16 = new ZonedDecimalData(2).isAPartOf(controlTotals, 30).init(16);
	private ZonedDecimalData ct17 = new ZonedDecimalData(2).isAPartOf(controlTotals, 32).init(17);
	private ZonedDecimalData ct18 = new ZonedDecimalData(2).isAPartOf(controlTotals, 34).init(18);
	private ZonedDecimalData ct19 = new ZonedDecimalData(2).isAPartOf(controlTotals, 36).init(19);
	private ZonedDecimalData ct20 = new ZonedDecimalData(2).isAPartOf(controlTotals, 38).init(20);
	private ZonedDecimalData ct21 = new ZonedDecimalData(2).isAPartOf(controlTotals, 40).init(21);
	private ZonedDecimalData ct22 = new ZonedDecimalData(2).isAPartOf(controlTotals, 42).init(22);
	private ZonedDecimalData ct23 = new ZonedDecimalData(2).isAPartOf(controlTotals, 44).init(23);
	private ZonedDecimalData ct24 = new ZonedDecimalData(2).isAPartOf(controlTotals, 46).init(24);
	private ZonedDecimalData ct25 = new ZonedDecimalData(2).isAPartOf(controlTotals, 48).init(25);
	private ZonedDecimalData ct26 = new ZonedDecimalData(2).isAPartOf(controlTotals, 50).init(26);
	private ZonedDecimalData ct27 = new ZonedDecimalData(2).isAPartOf(controlTotals, 52).init(27);
	private ZonedDecimalData ct28 = new ZonedDecimalData(2).isAPartOf(controlTotals, 54).init(28);
	private ZonedDecimalData ct29 = new ZonedDecimalData(2).isAPartOf(controlTotals, 56).init(29);
	private ZonedDecimalData ct30 = new ZonedDecimalData(2).isAPartOf(controlTotals, 58).init(30);
	private ZonedDecimalData ct31 = new ZonedDecimalData(2).isAPartOf(controlTotals, 60).init(31);
	private ZonedDecimalData ct32 = new ZonedDecimalData(2).isAPartOf(controlTotals, 62).init(32);
	private ZonedDecimalData ct33 = new ZonedDecimalData(2).isAPartOf(controlTotals, 64).init(33);
	private ZonedDecimalData ct34 = new ZonedDecimalData(2).isAPartOf(controlTotals, 66).init(34);
	private ZonedDecimalData ct35 = new ZonedDecimalData(2).isAPartOf(controlTotals, 68).init(35);

	private static final String t1688 = "T1688";
	private static final String t5515 = "T5515";
	private static final String t5544 = "T5544";
	private static final String t5645 = "T5645";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t6597 = "T6597";
	private static final String t6647 = "T6647";
	private static final String t3629 = "T3629";

	private FixedLengthStringData errors = new FixedLengthStringData(28);
	private FixedLengthStringData f070 = new FixedLengthStringData(4).isAPartOf(errors, 0).init("F070");
	private FixedLengthStringData ivrm = new FixedLengthStringData(4).isAPartOf(errors, 4).init("IVRM");
	private FixedLengthStringData h791 = new FixedLengthStringData(4).isAPartOf(errors, 8).init("H791");
	private FixedLengthStringData ovrf = new FixedLengthStringData(4).isAPartOf(errors, 12).init("OVRF");
	private FixedLengthStringData g418 = new FixedLengthStringData(4).isAPartOf(errors, 16).init("G418");
	private FixedLengthStringData e308 = new FixedLengthStringData(4).isAPartOf(errors, 20).init("E308");
	private FixedLengthStringData g117 = new FixedLengthStringData(4).isAPartOf(errors, 24).init("G117");

	private FixedLengthStringData indicArea = new FixedLengthStringData(99);
	private Indicator[] indicTable = IndicatorArrayPartOfStructure(99, 1, indicArea, 0);
	private Validator indOff = new Validator(indicTable, "0");
	private Validator indOn = new Validator(indicTable, "1");


	private Batcdorrec batcdorrec = new Batcdorrec();
	private Batcuprec batcuprec = new Batcuprec();
	private T5515rec t5515rec = new T5515rec();
	private T5544rec t5544rec = new T5544rec();
	private T5645rec t5645rec = new T5645rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6597rec t6597rec = new T6597rec();
	private T6647rec t6647rec = new T6647rec();
	private T3629rec t3629rec = new T3629rec();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private CovrudlTableDAM covrudlIO = new CovrudlTableDAM();

	private Datcon1rec datcon1rec = new Datcon1rec();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Getdescrec getdescrec = new Getdescrec();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private Lifacmvrec lifacmvrec = new Lifacmvrec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private PayrlifTableDAM payrlifIO = new PayrlifTableDAM();
	private UfndudlTableDAM ufndudlIO = new UfndudlTableDAM();
	private Ufpricerec ufpricerec = new Ufpricerec();
	private Udtrigrec udtrigrec = new Udtrigrec();
	//ILPI-145
	private UtrnalocTableDAM utrnaloIO = new UtrnalocTableDAM();
	//	private UtrnextTableDAM utrnextIO = new UtrnextTableDAM();
	//ILPI-254
	//	private UtrnupdTableDAM utrnupdIO = new UtrnupdTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private Varcom varcom = new Varcom();
	//	private Drylogrec drylogrec = new Drylogrec();
	private DacmTableDAM dacmIO = new DacmTableDAM();
	//	private Drycntrec drycntrec = new Drycntrec();

	private R5107rec r5107rec = new R5107rec();
	private UdivTableDAM udivIO = new UdivTableDAM();
	private Dryprclnk dryprclnk = new Dryprclnk();

	private ZonedDecimalData wsaaTotalUnits = new ZonedDecimalData(11, 5);
	private ItemTableDAM itemIO = new ItemTableDAM();
	/* WSAA-T5645 */
	private FixedLengthStringData[] wsaaT5645Rec = FLSInittedArray (30, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaT5645Rec, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaT5645Rec, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaT5645Rec, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaT5645Rec, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaT5645Rec, 20);
	private FormatsInner formatsInner = new FormatsInner();
	private R5106rec r5106rec = new R5106rec();
	private R5107rec r5107 = new R5107rec();
	private Dryrptrec dryrptrec = new Dryrptrec();

	/* WSAA-UFND-ARRAY */
	private FixedLengthStringData[] wsaaUfndRec = FLSInittedArray (50, 14);
	private FixedLengthStringData[] wsaaUfndFund = FLSDArrayPartOfArrayStructure(4, wsaaUfndRec, 0);
	private FixedLengthStringData[] wsaaUfndType = FLSDArrayPartOfArrayStructure(1, wsaaUfndRec, 4);
	private PackedDecimalData[] wsaaUfndNofUnits = PDArrayPartOfArrayStructure(16, 5, wsaaUfndRec, 5);
	private static final int wsaaUfndSize = 50;

	private Conlinkrec conlinkrec = new Conlinkrec();
	private PackedDecimalData wsaaX = new PackedDecimalData(2, 0).setUnsigned();
	private Ufndudlkey ufndudlkey = new Ufndudlkey();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private Zrdecplrec zrdecplrec = new Zrdecplrec(); //ILPI-135

	//ILPI-254
	private UtrnpfDAO utrnpfDAO = getApplicationContext().getBean("utrnpfDAO", UtrnpfDAO.class);
	private FixedLengthStringData Fundpool = new FixedLengthStringData(1); //ILIFE-8110


	//ILPI-254
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	private List<Itempf> itempfList;
	private Itempf itempf = null;	
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
		DEFAULT,
		updates330,
		triggerProcessing370,
		exit159,
		exit390,
		updates300,
		exit790,
		exit990,
		exit1090,
		exit1190,
		exit1290,
		unitsToSellForDebt1250,
		x2020CallPayrlifio,
		exit1390,
		exit2090
	}

	public Dry5102() {
		super();
	}

	/**
	 * The mainline method is the default entry point to the class
	 */
	public void mainline(Object... parmArray) {
		drypDryprcRecInner.dryprcRec = convertAndSetParam(drypDryprcRecInner.dryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.dryprcRec);
			startProcessing100();
		} catch (COBOLExitProgramException e) {
			// Expected exception for control flow purposes
		}
	}

	private void startProcessing100(){
		initialise110();
	}

	private void initialise110(){		
		drypDryprcRecInner.statuz.set(varcom.oK);	
		wsaaDryprcrec.set(SPACE);
		datcon1rec.function.set(varcom.tday);	
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		wsaaTotalUnits.set(ZEROES);
		wsaaDebt.set(ZEROES);

		//ILPI-254 starts
		//		itemIO.setItempfx("IT");
		//		itemIO.setItemcoy(drypDryprcRecInner.company);
		//		itemIO.setItemtabl(t5645);
		//		itemIO.setItemitem(wsaaProg);
		//		itemIO.setItemseq(SPACES);
		//		itemIO.setFunction(varcom.begn);
		//		itemIO.setStatuz(varcom.oK);
		iy.set(1);
		List<Itempf> t5645List = itemDAO.loadSmartTable("IT", drypDryprcRecInner.company.toString().trim(), "T5645").get(wsaaProg.toString().trim());
		//		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {
		//			loadT5645700();
		//		}
		for(Itempf itempf : t5645List){
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			ix.set(1);

			while ( !(isGT(ix,15))) {
				wsaaT5645Cnttot[iy.toInt()].set(t5645rec.cnttot[ix.toInt()]);
				wsaaT5645Glmap[iy.toInt()].set(t5645rec.glmap[ix.toInt()]);
				wsaaT5645Sacscode[iy.toInt()].set(t5645rec.sacscode[ix.toInt()]);
				wsaaT5645Sacstype[iy.toInt()].set(t5645rec.sacstype[ix.toInt()]);
				wsaaT5645Sign[iy.toInt()].set(t5645rec.sign[ix.toInt()]);
				ix.add(1);
				iy.add(1);
			}
		}
		//ILPI-254 ends

		lifacmvrec.batccoy.set(drypDryprcRecInner.batccoy);
		lifacmvrec.batcbrn.set(drypDryprcRecInner.batcbrn);
		lifacmvrec.batcactyr.set(drypDryprcRecInner.batcactyr);
		lifacmvrec.batcactmn.set(drypDryprcRecInner.batcactmn);
		lifacmvrec.batctrcde.set(drypDryprcRecInner.batctrcde);
		lifacmvrec.batcbatch.set(drypDryprcRecInner.batcbatch);
		lifacmvrec.rldgcoy.set(drypDryprcRecInner.company);
		lifacmvrec.genlcoy.set(drypDryprcRecInner.company);
		lifacmvrec.postyear.set(SPACES);
		lifacmvrec.postmonth.set(SPACES);
		lifacmvrec.frcdate.set(varcom.maxdate);
		lifacmvrec.crate.set(ZEROES);
		lifacmvrec.acctamt.set(ZEROES);
		lifacmvrec.rcamt.set(ZEROES);
		lifacmvrec.contot.set(ZEROES);
		lifacmvrec.rcamt.set(ZEROES);
		lifacmvrec.termid.set(SPACES);
		lifacmvrec.transactionDate.set(drypDryprcRecInner.runDate);
		lifacmvrec.transactionTime.set(999999);
		lifacmvrec.user.set(ZEROES);

		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.company);
		itemIO.setItemtabl(t1688);
		itemIO.setItemitem(drypDryprcRecInner.batctrcde);

		getdescrec.itemkey.set(itemIO.getDataKey());
		getdescrec.language.set(drypDryprcRecInner.language);
		getdescrec.function.set("CHECK");
		callProgram(Getdesc.class, getdescrec.getdescRec);

		if (isNE(getdescrec.statuz, varcom.oK)) {
			lifacmvrec.trandesc.set(SPACES);
		} else {
			lifacmvrec.trandesc.set(getdescrec.longdesc);
		}

		X8100DeleteUdiv();

		//ILPI-254 STARTS
		//		utrnupdIO.format.set(formatsInner.utrnupdrec);
		//		utrnupdIO.termid.set(SPACES);
		//		utrnupdIO.transactionDate.set(drypDryprcRecInner.runDate);
		//		utrnupdIO.scheduleName.set(wsaaProg);
		//		utrnupdIO.scheduleNumber.set(1);
		//		utrnupdIO.transactionTime.set(999999);
		//		utrnupdIO.user.set(ZEROES);
		//		utrnupdIO.batccoy.set(drypDryprcRecInner.batccoy);
		//		utrnupdIO.batcbrn.set(drypDryprcRecInner.batcbrn);
		//		utrnupdIO.batcactyr.set(drypDryprcRecInner.batcactyr);
		//		utrnupdIO.batcactmn.set(drypDryprcRecInner.batcactmn);
		//		utrnupdIO.batctrcde.set(drypDryprcRecInner.batctrcde);
		//		utrnupdIO.batcbatch.set(drypDryprcRecInner.batcbatch);
		//		utrnupdIO.unitSubAccount.set("DEBT");
		//		utrnupdIO.covdbtind.set("D");
		//		utrnupdIO.feedbackInd.set("Y");
		//		utrnupdIO.sacscode.set(wsaaT5645Sacscode[25]);
		//		utrnupdIO.sacstyp.set(wsaaT5645Sacstype[25]);
		//		utrnupdIO.genlcde.set(wsaaT5645Glmap[25]);
		//	
		//		utrnupdIO.jobnoPrice.set(ZEROES);
		//		utrnupdIO.fundRate.set(ZEROES);
		//		utrnupdIO.strpdate.set(ZEROES);
		//		utrnupdIO.ustmno.set(ZEROES);
		//		utrnupdIO.nofUnits.set(ZEROES);
		//		utrnupdIO.nofDunits.set(ZEROES);
		//		utrnupdIO.moniesDate.set(ZEROES);
		//		utrnupdIO.priceDateUsed.set(ZEROES);
		//		utrnupdIO.priceUsed.set(ZEROES);
		//		utrnupdIO.unitBarePrice.set(ZEROES);
		//		utrnupdIO.inciNum.set(ZEROES);
		//		utrnupdIO.inciPerd01.set(ZEROES);
		//		utrnupdIO.inciPerd02.set(ZEROES);
		//		utrnupdIO.inciprm01.set(ZEROES);
		//		utrnupdIO.inciprm02.set(ZEROES);
		//		utrnupdIO.fundAmount.set(ZEROES);
		//		utrnupdIO.procSeqNo.set(ZEROES);
		//		utrnupdIO.svp.set(ZEROES);
		//		utrnupdIO.discountFactor.set(ZEROES);
		//		utrnupdIO.surrenderPercent.set(ZEROES);
		//		utrnupdIO.crComDate.set(ZEROES);
		//		utrnupdIO.nowDeferInd.set(SPACES);
		//		utrnupdIO.crtable.set(SPACES);
		//		utrnupdIO.switchIndicator.set(SPACES);
		//		utrnupdIO.triggerModule.set(SPACES);
		//		utrnupdIO.triggerKey.set(SPACES);
		//		wsaaFirstTime.set("Y");

		//		//ILIFE-2040 STARTS
		////		initialize(utrnextIO.getParams());
		//		utrnextIO.setParams(SPACES);
		//		//ILIFE-2040 ENDS
		//		utrnextIO.statuz.set(varcom.oK);
		//		utrnextIO.chdrcoy.set(drypDryprcRecInner.company);
		//		utrnextIO.chdrnum.set(drypDryprcRecInner.entity);
		//		utrnextIO.procSeqNo.set(ZEROES);
		//		utrnextIO.unitVirtualFund.set(SPACES);
		//		utrnextIO.unitType.set(SPACES);
		//		utrnextIO.function.set(varcom.begn);
		//		utrnextIO.setFormat(formatsInner.utrnextrec);
		//
		//		while ( !(isEQ(utrnextIO.getStatuz(), varcom.endp))) {
		//			readFile150();
		//		}

		Utrnpf utrnpfRead = new Utrnpf();
		utrnpfRead.setChdrcoy(drypDryprcRecInner.company.toString().trim());
		utrnpfRead.setChdrnum(drypDryprcRecInner.entity.toString().trim());
		List<Utrnpf> utrnpfList = utrnpfDAO.readUtrnpfForUnitDealProcess(utrnpfRead);

		drycntrec.contotNumber.set(ct01);
		drycntrec.contotValue.set(utrnpfList.size());
		d000ControlTotals();

		for(Utrnpf utrnpf : utrnpfList){
			Fundpool.set(utrnpf.getFundPool()); //ILIFE-8110
			readFile150(utrnpf);
		}
		//		fillParams3000();
		//ILPI-254 ENDS
	}

	/**
	 * ILPI-254
	 */
	private void readFile150(Utrnpf utrnpf) {
		//		SmartFileCode.execute(appVars, utrnextIO);
		//	
		//		if(!isEQ(utrnextIO.statuz,varcom.oK)&&!isEQ(utrnextIO.statuz,varcom.endp)){
		//			drylogrec.params.set(utrnextIO.getParams());
		//			drylogrec.statuz.set(utrnextIO.getStatuz());
		//			drylogrec.dryDatabaseError.setTrue();
		//			a000FatalError();
		//		}
		//
		//		if(isEQ(utrnextIO.statuz,varcom.endp)||!isEQ(utrnextIO.chdrcoy,drypDryprcRecInner.company)||!isEQ(utrnextIO.chdrnum,drypDryprcRecInner.entity)){
		//			utrnextIO.statuz.set(varcom.endp);
		//		}

		//		if(isEQ(utrnextIO.statuz,varcom.endp)){
		//			if(firstTime.isTrue()){
		//				initialize(utrnextIO.getParams());
		//			
		//				utrnextIO.statuz.set(varcom.oK);
		//				utrnextIO.chdrcoy.set(drypDryprcRecInner.company);
		//				utrnextIO.chdrnum.set(drypDryprcRecInner.entity);
		//				utrnextIO.procSeqNo.set(999);
		//				utrnextIO.unitVirtualFund.set(SPACES);
		//				utrnextIO.unitType.set(SPACES);
		//				utrnextIO.function.set(varcom.begn);
		//				wsaaFirstTime.set("N");
		//			}
		//
		////			goTo(GotoLabel.exit159);
		//			return;
		//		}

		//		drycntrec.contotNumber.set(ct01);
		//		drycntrec.contotValue.set(1);
		//	
		//		d000ControlTotals();

		//		utrnextIO.function.set(varcom.nextr);	
		//		if(isEQ(utrnpf.getVrtfnd(),SPACES)){
		if("".equals(utrnpf.getUnitVirtualFund().trim())){
			update300(utrnpf);
			drycntrec.contotNumber.set(ct04);
			drycntrec.contotValue.set(1);

			d000ControlTotals();
			//			goTo(GotoLabel.exit159);
			return;
		}

		getFundPrice200(utrnpf);

		if(isEQ(ufpricerec.statuz,varcom.endp)){
			drycntrec.contotNumber.set(ct05);
			drycntrec.contotValue.set(1);
			d000ControlTotals();

			drypDryprcRecInner.processUnsuccesful.setTrue();		
			//			goTo(GotoLabel.exit159);
			return;
		}
		update300(utrnpf);
	}

	/**
	 * ILPI-254
	 * 
	 * @param utrnpf
	 */
	private void getFundPrice200(Utrnpf utrnpf){
		ufpricerec.function.set("DEALP");
		ufpricerec.mode.set("BATCH");
		ufpricerec.company.set(utrnpf.getChdrcoy());
		ufpricerec.unitVirtualFund.set(utrnpf.getUnitVirtualFund());
		ufpricerec.unitType.set(utrnpf.getUnitType());
		ufpricerec.effdate.set(utrnpf.getMoniesDate());
		ufpricerec.nowDeferInd.set(utrnpf.getNowDeferInd());

		callProgram(Ufprice.class,ufpricerec.ufpriceRec);
		if (isNE(ufpricerec.statuz, varcom.oK)
				&& isNE(ufpricerec.statuz, varcom.endp)){
			drylogrec.params.set(ufpricerec.ufpriceRec);
			drylogrec.statuz.set(ufpricerec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		wsaaUfprPriceDate.set(ufpricerec.priceDate);
		wsaaUfprBarePrice.set(ufpricerec.barePrice);
	}

	/**
	 * ILPI-254
	 * 
	 * @param utrnpf
	 */
	private void update300(Utrnpf utrnpf) {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					update310(utrnpf);
					dealTheUtrn320(utrnpf); //ILIFE-3704
				case updates330:
					updates330();
				case triggerProcessing370:
					triggerProcessing370();
					triggerProcessing380();
				case exit390:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}	

	/**
	 * ILPI-254
	 */
	private void update310(Utrnpf utrnpf){
		readhUtrn400(utrnpf);

		if(isEQ(utrnaloIO.statuz,varcom.mrnf)){
			goTo(GotoLabel.exit390);	
		}

		setAcctLvlForPostings500();

		if(isEQ(utrnaloIO.unitVirtualFund,SPACES)){
			processUtrnWithoutFund600();
			rewrtUtrn800();
			goTo(GotoLabel.triggerProcessing370);
		}
	}

	private void dealTheUtrn320(Utrnpf utrnpf){ //ILIFE-3704
		readhUtrs1000();

		if(isEQ(utrnaloIO.canInitUnitInd,"Y")){
			drycntrec.contotNumber.set(ct07);
			drycntrec.contotValue.set(1);

			d000ControlTotals();
			goTo(GotoLabel.updates300);
		}

		if(!isEQ(utrnaloIO.nofUnits,ZERO)&&!isEQ(utrnaloIO.nofDunits,ZERO)&&!isEQ(utrnaloIO.fundAmount,ZERO)&&!isEQ(utrnaloIO.contractAmount,ZERO)&&!isEQ(utrnaloIO.priceUsed,ZERO)&&!isEQ(utrnaloIO.priceDateUsed,ZERO)&&!isEQ(utrnaloIO.unitBarePrice,ZERO)){
			wsaaReversalUtrn.set("Y");
		}else{
			wsaaReversalUtrn.set("N");
		}

		if(reversalUtrn.isTrue()){
			wsaaPrice.set(utrnaloIO.priceUsed);
			wsaaUfprPriceDate.set(utrnaloIO.priceDateUsed);
			wsaaUfprBarePrice.set(utrnaloIO.unitBarePrice);
		}else{			
			getBuySellPrice2200();
		}

		if(!isEQ(utrnaloIO.surrenderPercent,ZERO)&&isEQ(utrnaloIO.statuz,varcom.oK)&& (!reversalUtrn.isTrue())){
			calcSurrenderPrcnt1100();
		}

		if(!reversalUtrn.isTrue()){
			completeUtrnDetails1200();
		}

		if(isEQ(utrnaloIO.nofUnits,ZERO)&&isEQ(utrnaloIO.fundAmount,ZERO)&&!isEQ(utrnaloIO.triggerModule,SPACES)){
			utrnaloIO.feedbackInd.set("Y");
			rewrtUtrn800();
			drycntrec.contotNumber.set(ct08);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			goTo(GotoLabel.triggerProcessing370);
		}else{
			drycntrec.contotNumber.set(ct09);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
		}


		if(isLTE(utrnaloIO.surrenderPercent,ZERO)&&isLT(utrnaloIO.fundAmount,ZERO)&&(!reversalUtrn.isTrue())){
			wsaaInsuffUnitsMeth.set(SPACES);
			checkSuffCovUnits900(utrnpf); //ILIFE-3704

			if(isEQ(wsaaInsuffUnitsMeth,"E")){
				X8000InsuffValueProcess();			
				goTo(GotoLabel.exit390);

			}

			if(isEQ(wsaaInsuffUnitsMeth,"L")){
				if(isEQ(utrnaloIO.unitType,"A")){
					utrnaloIO.priceUsed.set(wsaaPrice);
					utrnaloIO.feedbackInd.set("Y");
					utrnaloIO.priceDateUsed.set(wsaaUfprPriceDate);
					utrnaloIO.unitBarePrice.set(wsaaUfprBarePrice);
				}

				rewrtUtrn800();
				goTo(GotoLabel.triggerProcessing370);
			}
		}

		accountingPostings1300();
		if(!isEQ(utrnaloIO.moniesDate,datcon1rec.intDate)){
			fluctuationCheck1400();	
		}
	}

	private void updates330(){
		utrnaloIO.priceDateUsed.set(wsaaUfprPriceDate);
		utrnaloIO.priceUsed.set(wsaaPrice);
		utrnaloIO.unitBarePrice.set(wsaaUfprBarePrice);
		utrnaloIO.feedbackInd.set("Y");
		rewrtUtrn800();

		createReportRecord1500();

		setPrecision(utrsIO.getCurrentUnitBal(), 5);
		utrsIO.setCurrentUnitBal(add(utrsIO.getCurrentUnitBal(), utrnaloIO.getNofUnits()));
		setPrecision(utrsIO.getCurrentDunitBal(), 5);
		utrsIO.setCurrentDunitBal(add(utrsIO.getCurrentDunitBal(), utrnaloIO.getNofDunits()));
		utrsIO.setFundPool(Fundpool); //ILIFE-8110
		updateUtrs1600();

		/**
		 * We must now update the UFND which has a total of all
		 * units in a fund. Instead of performing reads and rewrites
		 * for each UTRN we process, we store the values in working
		 * storage. When MainB is ready to commit the 3500 section
		 * will be performed where we will flush the working storage
		 * values to UFND.
		 */
		for (ix.set(1); !(isGT(ix, wsaaUfndSize)|| (isEQ(wsaaUfndFund[ix.toInt()], utrnaloIO.getUnitVirtualFund())
				&& isEQ(wsaaUfndType[ix.toInt()], utrnaloIO.getUnitType()))	|| isEQ(wsaaUfndType[ix.toInt()], SPACES)); ix.add(1))
		{
			/*CONTINUE_STMT*/
		}

		if(isGT(ix,wsaaUfndSize)){
			drylogrec.params.set(utrnaloIO.getParams());
			drylogrec.statuz.set(h791);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}	

		if (isEQ(wsaaUfndType[ix.toInt()], SPACES)) {
			wsaaUfndFund[ix.toInt()].set(utrnaloIO.getUnitVirtualFund());
			wsaaUfndType[ix.toInt()].set(utrnaloIO.getUnitType());
			wsaaUfndNofUnits[ix.toInt()].set(utrnaloIO.getNofUnits());
		} else {
			wsaaUfndNofUnits[ix.toInt()].add(utrnaloIO.getNofUnits());
		}
	}

	private void triggerProcessing370(){
		if(!isEQ(utrnaloIO.triggerModule,SPACES)){
			callTrigger1700();
		}
	}

	private void triggerProcessing380(){
		//		if(drypDryprcRecInner.onlineMode.isTrue()){
		//			accumulateFundChanges1850();	
		//		}else{
		accumulateFundChanges1800();	
		//		}		
	}

	/**
	 * ILPI-254
	 */
	private void readhUtrn400(Utrnpf utrnpf){
		initialize(utrnaloIO.getParams());
		utrnaloIO.chdrcoy.set(utrnpf.getChdrcoy());
		utrnaloIO.chdrnum.set(utrnpf.getChdrnum());
		utrnaloIO.life.set(utrnpf.getLife());
		utrnaloIO.planSuffix.set(utrnpf.getPlanSuffix());
		utrnaloIO.rider.set(utrnpf.getRider());
		utrnaloIO.coverage.set(utrnpf.getCoverage());
		utrnaloIO.unitType.set(utrnpf.getUnitType());
		utrnaloIO.moniesDate.set(utrnpf.getMoniesDate());
		utrnaloIO.tranno.set(utrnpf.getTranno());
		utrnaloIO.procSeqNo.set(utrnpf.getProcSeqNo());
		utrnaloIO.unitVirtualFund.set(utrnpf.getUnitVirtualFund());

		//ILPI-145
		//		utrnaloIO.getRrn().set(utrnextIO.getRrn());
		utrnaloIO.function.set(varcom.readh);
		utrnaloIO.format.set(formatsInner.utrnupdrec);

		SmartFileCode.execute(appVars, utrnaloIO);

		if(isEQ(utrnaloIO.statuz,varcom.mrnf)){
			drycntrec.contotNumber.set(ct06);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
		}

		if(!isEQ(utrnaloIO.statuz,varcom.oK)){
			drylogrec.params.set(utrnaloIO.getParams());
			drylogrec.statuz.set(utrnaloIO.statuz);
			a000FatalError();
		}

	}

	private void setAcctLvlForPostings500(){
		//ILIFE-3704 Start by dpuhawan
		/*
		itdmIO.setItemcoy(drypDryprcRecInner.company);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(utrnaloIO.contractType);
		itdmIO.setItmfrm(utrnaloIO.moniesDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);

		SmartFileCode.execute(appVars, itdmIO);

		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.company)||!isEQ(itdmIO.getItemtabl(),t5688)||!isEQ(itdmIO.getItemitem(),utrnaloIO.contractType)||isGT(itdmIO.getItmfrm(),utrnaloIO.moniesDate)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(e308);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		*/
		t5688rec.t5688Rec.set(SPACES);
		itempf = new Itempf();
		itempf.setItempfx("IT");
		itempf.setItemtabl(t5688);
		itempf.setItemcoy(drypDryprcRecInner.company.toString());
		itempf.setItemitem(utrnaloIO.contractType.toString());
		itempf.setItmfrm(utrnaloIO.moniesDate.getbigdata());
		itempf.setItmto(utrnaloIO.moniesDate.getbigdata());
		itempfList = itemDAO.findByItemDates(itempf);
		if(itempfList.size() > 0) {
			for (Itempf it : itempfList) {			
				t5688rec.t5688Rec.set(StringUtil.rawToString(it.getGenarea()));				
			}
		} 		
		//ILIFE-3704 End
		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaComponLevelAccounted.set(t5688rec.comlvlacc);
	}


	private void processUtrnWithoutFund600(){
		lifacmvrec.origcurr.set(utrnaloIO.cntcurr);
		lifacmvrec.origamt.set(utrnaloIO.contractAmount);

		if(componLevelAccounted.isTrue()){
			t5645Ix.set(5);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
			x1000CallLifacmv();

			t5645Ix.set(29);
			wsaaT5645Sacscode[29].set(utrnaloIO.getSacscode());
			wsaaT5645Sacstype[29].set(utrnaloIO.getSacstyp());
			wsaaT5645Glmap[29].set(utrnaloIO.getGenlcde());
			x1000CallLifacmv();

		}else{
			t5645Ix.set(22);
			lifacmvrec.substituteCode[6].set(SPACES);
			x1000CallLifacmv();

			t5645Ix.set(16);
			wsaaT5645Sacscode[12].set(utrnaloIO.getSacscode());
			wsaaT5645Sacstype[12].set(utrnaloIO.getSacstyp());
			wsaaT5645Glmap[12].set(utrnaloIO.getGenlcde());

			x1000CallLifacmv();
		}

		utrnaloIO.feedbackInd.set("Y");
	}

	//ILPI-254 starts
	//	private void loadT5645700(){
	//		SmartFileCode.execute(appVars, itemIO);
	//		
	//		if(!isEQ(itemIO.getStatuz(),varcom.oK)){
	//			drylogrec.statuz.set(itemIO.getStatuz());
	//			drylogrec.params.set(itemIO.getParams());
	//			drylogrec.dryDatabaseError.setTrue();
	//			a000FatalError();
	//		}
	//	
	//		if(!isEQ(itemIO.getItemcoy(),drypDryprcRecInner.company)||!isEQ(itemIO.getItemtabl(),t5645)||!isEQ(itemIO.getItemitem(),wsaaProg)){
	//			itemIO.setStatuz(varcom.endp);
	//			return;
	//		}
	//	
	//		t5645rec.t5645Rec.set(itemIO.getGenarea());
	//		ix.set(1);
	//
	//		while ( !(isGT(ix,15))) {
	//			wsaaT5645Cnttot[iy.toInt()].set(t5645rec.cnttot[ix.toInt()]);
	//			wsaaT5645Glmap[iy.toInt()].set(t5645rec.glmap[ix.toInt()]);
	//			wsaaT5645Sacscode[iy.toInt()].set(t5645rec.sacscode[ix.toInt()]);
	//			wsaaT5645Sacstype[iy.toInt()].set(t5645rec.sacstype[ix.toInt()]);
	//			wsaaT5645Sign[iy.toInt()].set(t5645rec.sign[ix.toInt()]);
	//			ix.add(1);
	//			iy.add(1);
	//		}
	//		
	//		itemIO.setFunction(varcom.nextr);
	//	}
	//ILPI-254 ends

	private void rewrtUtrn800(){
		utrnaloIO.scheduleName.set(wsaaProg);
		utrnaloIO.scheduleNumber.set(1);
		utrnaloIO.function.set(varcom.rewrt);
		SmartFileCode.execute(appVars, utrnaloIO);

		if(!isEQ(utrnaloIO.statuz,varcom.oK)){
			drylogrec.params.set(utrnaloIO.getParams());
			drylogrec.statuz.set(utrnaloIO.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		drycntrec.contotNumber.set(ct16);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	private void checkSuffCovUnits900(Utrnpf utrnpf){ //ILIFE-3704
		try {
			checkSuffCovUnits910();
			moreUtrnUnitsUnitsThan920();
			findCancellationOption930(utrnpf); //ILIFE-3704
		} catch (GOTOException e){

		}
	}

	private void checkSuffCovUnits910(){
		if(isEQ(utrnaloIO.getSwitchIndicator(), "Y") && 
				isEQ(utrnaloIO.getNowDeferInd(),"D") && isLT(mult(utrsIO.currentUnitBal, wsaaPrice), sub(0, utrnaloIO.fundAmount))){
			drylogrec.params.set(utrsIO.getDataKey());
			drylogrec.statuz.set(f070);
			b000LogMessage();
		}

		if(isGTE(utrnaloIO.getNofUnits(), mult(utrsIO.getCurrentUnitBal(), -1))){
			goTo(GotoLabel.exit990);
		}

		drycntrec.contotNumber.set(ct10);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	private void moreUtrnUnitsUnitsThan920(){
		datcon3rec.datcon3Rec.set(SPACES);
		datcon3rec.intDate1.set(utrnaloIO.crComDate);
		datcon3rec.intDate2.set(drypDryprcRecInner.runDate);
		datcon3rec.frequency.set(12);
		callProgram(Datcon3.class, datcon3rec.datcon3Rec);
		if(!isEQ(datcon3rec.statuz,varcom.oK)){
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.statuz.set(datcon3rec.statuz);
			a000FatalError();
		}
	}

	private void findCancellationOption930(Utrnpf utrnpf){
		wsaaWithinRange.set(1000);

		if(isGTE(t6647rec.monthsNegUnits, datcon3rec.freqFactor) && !isEQ(t6647rec.monthsNegUnits, 0)){
			wsaaWithinRange.set(t6647rec.monthsNegUnits);
			wsaaInsuffUnitsMeth.set("N");
		}		

		if(isGTE(t6647rec.monthsDebt,datcon3rec.freqFactor) && isLT(t6647rec.monthsDebt, wsaaWithinRange) && !isEQ(t6647rec.monthsDebt,0)){
			wsaaWithinRange.set(t6647rec.monthsDebt);
			wsaaInsuffUnitsMeth.set("D");
		}

		if(isGTE(t6647rec.monthsLapse,datcon3rec.freqFactor) && isLT(t6647rec.monthsLapse,wsaaWithinRange) && !isEQ(t6647rec.monthsLapse,0)){
			wsaaWithinRange.set(t6647rec.monthsLapse);
			wsaaInsuffUnitsMeth.set("L");
		}

		if(isGTE(t6647rec.monthsError, datcon3rec.freqFactor) && isLT(t6647rec.monthsError,wsaaWithinRange) && !isEQ(t6647rec.monthsError,ZERO)){
			wsaaWithinRange.set(t6647rec.monthsError);
			wsaaInsuffUnitsMeth.set("E");
		}		

		if (isEQ(wsaaInsuffUnitsMeth,"GO")){
			goTo(GotoLabel.exit990);
		}else if (isEQ(wsaaInsuffUnitsMeth,"N")){
			wsaaInsuffUnitsMeth.set(SPACES);
			goTo(GotoLabel.exit990);
		}else if (isEQ(wsaaInsuffUnitsMeth,"D")){
			increaseCoverageDebt1900();
		}else if (isEQ(wsaaInsuffUnitsMeth,"L")){
			lapseCoverage2000(utrnpf); //ILIFE-3704
			errorOnCoverage2100();
		}else if (isEQ(wsaaInsuffUnitsMeth,"E")){
			errorOnCoverage2100();
		}		
	}

	private void readhUtrs1000(){
		readhUtrs1010();
	}

	private void readhUtrs1010(){
		utrsIO.function.set(varcom.readh);
		utrsIO.chdrcoy.set(utrnaloIO.chdrcoy);
		utrsIO.chdrnum.set(utrnaloIO.chdrnum);
		utrsIO.life.set(utrnaloIO.life);
		utrsIO.coverage.set(utrnaloIO.coverage);
		utrsIO.rider.set(utrnaloIO.rider);
		utrsIO.planSuffix.set(utrnaloIO.planSuffix);
		utrsIO.unitVirtualFund.set(utrnaloIO.unitVirtualFund);
		utrsIO.unitType.set(utrnaloIO.unitType);

		SmartFileCode.execute(appVars, utrsIO);

		if(isEQ(utrsIO.statuz,varcom.mrnf)){
			utrsIO.function.set(varcom.writr);
			utrsIO.currentUnitBal.set(ZERO);
			utrsIO.currentDunitBal.set(ZERO);
			//			goTo(GotoLabel.exit1090);
			return;
		}

		if(!isEQ(utrsIO.statuz,varcom.oK)){
			drylogrec.params.set(utrsIO.getParams());
			drylogrec.statuz.set(utrsIO.statuz);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

		utrsIO.function.set(varcom.rewrt);
	}

	private void calcSurrenderPrcnt1100(){
		calcSurrenderPrcnt1110();
	}

	private void calcSurrenderPrcnt1110(){
		if(isEQ(utrnaloIO.getSwitchIndicator(),"Y") && isEQ(utrsIO.currentDunitBal, ZERO) && isEQ(utrsIO.currentUnitBal,ZERO)){
			wsaaPrice.set(ZERO);
			goTo(GotoLabel.exit1190);
		}
		//ILPI-135, Part surrender transaction, Start
		setPrecision(utrnaloIO.getNofDunits(), 6);
		utrnaloIO.setNofDunits(mult(mult(div(utrnaloIO.getSurrenderPercent(), 100), utrsIO.getCurrentDunitBal()), -1), true);
		setPrecision(utrnaloIO.getNofUnits(), 6);
		utrnaloIO.setNofUnits(mult(mult(div(utrnaloIO.getSurrenderPercent(), 100), utrsIO.getCurrentUnitBal()), -1), true);
		//ILPI-135, Part surrender transaction, End
		setPrecision(utrnaloIO.getFundAmount(), 6);
		utrnaloIO.setFundAmount(div(mult(utrnaloIO.getNofUnits(), wsaaPrice), utrnaloIO.getDiscountFactor()), true);
		/*  Apply Surrender value penalty if one exists.                   */
		if (isNE(utrnaloIO.getSvp(), 1)) {
			setPrecision(utrnaloIO.getFundAmount(), 6);
			utrnaloIO.setFundAmount(mult(utrnaloIO.getFundAmount(), (sub(1, utrnaloIO.getSvp()))), true);
		}
		if (isNE(utrnaloIO.getFundAmount(), 0)) {
			zrdecplrec.currency.set(utrnaloIO.getFundCurrency());
			zrdecplrec.amountIn.set(utrnaloIO.getFundAmount());
			utrnaloIO.setFundAmount(zrdecplrec.amountOut);
		}
		//ILPI-135, Part surrender transaction, End
	}

	private void completeUtrnDetails1200(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					completeUtrnDetails1210();	
					break;
				case unitsToSellForDebt1250: 
					unitsToSellForDebt1250();			
				case exit390:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	private void completeUtrnDetails1210(){
		if(!isEQ(utrnaloIO.contractAmount,ZERO)&&isEQ(utrnaloIO.fundAmount,ZERO)){

			if(isEQ(utrnaloIO.fundCurrency,utrnaloIO.cntcurr)){
				utrnaloIO.fundAmount.set(utrnaloIO.contractAmount);
				utrnaloIO.fundRate.set(1);
			}else{
				conlinkrec.amountIn.set(utrnaloIO.contractAmount);
				conlinkrec.currIn.set(utrnaloIO.cntcurr);
				conlinkrec.currOut.set(utrnaloIO.fundCurrency);
				conlinkrec.cashdate.set(ufpricerec.priceDate);

				if(isLT(utrnaloIO.contractAmount,ZERO)){
					conlinkrec.function.set("SURR");
				}else{
					conlinkrec.function.set("CVRT");
				}

				x3000CallXcvrt();

				utrnaloIO.fundAmount.set(conlinkrec.amountOut);
				utrnaloIO.fundRate.set(conlinkrec.rateUsed);
			}
		}

		if(!isEQ(utrnaloIO.fundAmount,ZERO)&&isEQ(utrnaloIO.contractAmount,ZERO)){
			if(isEQ(utrnaloIO.fundCurrency,utrnaloIO.cntcurr)){
				//utrnaloIO.contractAmount.set(utrnaloIO.fundAmount);
				utrnaloIO.fundRate.set(1);
			}else{
				conlinkrec.amountIn.set(utrnaloIO.fundAmount);
				conlinkrec.currIn.set(utrnaloIO.fundCurrency);
				conlinkrec.currOut.set(utrnaloIO.cntcurr);
				conlinkrec.cashdate.set(ufpricerec.priceDate);
				conlinkrec.function.set("CVRT");

				x3000CallXcvrt();

				utrnaloIO.contractAmount.set(conlinkrec.amountOut);
				utrnaloIO.fundRate.set(conlinkrec.rateUsed);
			}
		}

		if(isEQ(utrnaloIO.discountFactor, ZERO)){
			utrnaloIO.discountFactor.set(1);
		}			

		if(isEQ(utrnaloIO.covdbtind,"D")){
			goTo(GotoLabel.unitsToSellForDebt1250);
		}			

		if (isNE(utrnaloIO.getNofUnits(), ZERO)&& isEQ(utrnaloIO.getFundAmount(), ZERO)) {
			setPrecision(utrnaloIO.getFundAmount(), 6);
			utrnaloIO.setFundAmount(div(mult(utrnaloIO.getNofUnits(), wsaaPrice), utrnaloIO.getDiscountFactor()), true);
			if (utrnaloIO.fundAmount.isTruncated()) {
				drylogrec.params.set(utrnaloIO.getParams());
				drylogrec.statuz.set(ovrf);
				drylogrec.drySystemError.setTrue();
				//ILPI-97		
				//				fatalError600();
				a000FatalError();
			}
			if (isNE(utrnaloIO.getSvp(), 1)) {
				setPrecision(utrnaloIO.getFundAmount(), 6);
				utrnaloIO.setFundAmount(mult(utrnaloIO.getFundAmount(), (sub(1, utrnaloIO.getSvp()))), true);
			}
		}

		if (isNE(utrnaloIO.getNofDunits(), ZERO) && isEQ(utrnaloIO.getFundAmount(), ZERO)) {
			setPrecision(utrnaloIO.getFundAmount(), 6);
			utrnaloIO.setFundAmount(mult(utrnaloIO.getNofDunits(), wsaaPrice), true);
			if (isNE(utrnaloIO.getSvp(), 1)) {
				setPrecision(utrnaloIO.getFundAmount(), 6);
				utrnaloIO.setFundAmount(mult(utrnaloIO.getFundAmount(), (sub(1, utrnaloIO.getSvp()))), true);
			}
		}

		if (isNE(utrnaloIO.getFundAmount(), ZERO) && isEQ(utrnaloIO.getContractAmount(), ZERO)) {
			if (isEQ(utrnaloIO.getFundCurrency(), utrnaloIO.getCntcurr())) {
				utrnaloIO.setContractAmount(utrnaloIO.getFundAmount());
				utrnaloIO.setFundRate(1);
			}
			else {
				conlinkrec.amountIn.set(utrnaloIO.getFundAmount());
				conlinkrec.currIn.set(utrnaloIO.getFundCurrency());
				conlinkrec.currOut.set(utrnaloIO.getCntcurr());
				/*         MOVE UFPR-PRICE-DATE       TO CLNK-CASHDATE          */
				conlinkrec.cashdate.set(utrnaloIO.getMoniesDate());
				conlinkrec.function.set("CVRT");
				x3000CallXcvrt();
				utrnaloIO.setContractAmount(conlinkrec.amountOut);
				utrnaloIO.setFundRate(conlinkrec.rateUsed);
			}
		}	

		if (isEQ(utrnaloIO.getUnitType(), "I")&& isLT(utrnaloIO.getFundAmount(), ZERO) && isEQ(utrnaloIO.getNofUnits(), ZERO)) {
			setPrecision(utrnaloIO.getNofUnits(), 6);
			utrnaloIO.setNofUnits(mult(div(utrnaloIO.getFundAmount(), wsaaPrice), utrnaloIO.getDiscountFactor()), true);
			if (isNE(utrnaloIO.getSvp(), 1)) {
				setPrecision(utrnaloIO.getNofUnits(), 5);
				utrnaloIO.setNofUnits(mult(utrnaloIO.getNofUnits(), (add(1, utrnaloIO.getSvp()))));
			}
			setPrecision(utrnaloIO.getNofDunits(), 6);
			utrnaloIO.setNofDunits(mult(utrnaloIO.getNofUnits(), (div(utrsIO.getCurrentDunitBal(), utrsIO.getCurrentUnitBal()))), true);
			goTo(GotoLabel.exit1290);
		}

		if (isNE(utrnaloIO.getFundAmount(), ZERO) && isEQ(utrnaloIO.getNofDunits(), ZERO)) {
			setPrecision(utrnaloIO.getNofDunits(), 6);
			utrnaloIO.setNofDunits(div(utrnaloIO.getFundAmount(), wsaaPrice), true);
			if (isNE(utrnaloIO.getSvp(), 1)) {
				setPrecision(utrnaloIO.getNofUnits(), 5);
				utrnaloIO.setNofUnits(mult(utrnaloIO.getNofUnits(), (add(1, utrnaloIO.getSvp()))));
			}
		}

		if (isNE(utrnaloIO.getNofDunits(), ZERO) && isEQ(utrnaloIO.getNofUnits(), ZERO)) {
			setPrecision(utrnaloIO.getNofUnits(), 6);
			utrnaloIO.setNofUnits(mult(utrnaloIO.getNofDunits(), utrnaloIO.getDiscountFactor()), true);			
		}

		if(!isEQ(utrnaloIO.nofUnits,ZERO)&&isEQ(utrnaloIO.nofDunits,ZERO)){
			setPrecision(utrnaloIO.getNofUnits(), 6);
			utrnaloIO.setNofUnits(div(utrnaloIO.getNofUnits(), utrnaloIO.getDiscountFactor()), true);
		}		
	}

	private void unitsToSellForDebt1250(){
		if (isNE(utrnaloIO.getFundAmount(), ZERO)
				&& isEQ(utrnaloIO.getNofUnits(), ZERO)) {
			setPrecision(utrnaloIO.getNofUnits(), 6);
			utrnaloIO.setNofUnits(mult(div(utrnaloIO.getFundAmount(), wsaaPrice), utrnaloIO.getDiscountFactor()), true);
			if (isNE(utrnaloIO.getSvp(), 1)) {
				setPrecision(utrnaloIO.getNofUnits(), 5);
				utrnaloIO.setNofUnits(mult(utrnaloIO.getNofUnits(), (add(1, utrnaloIO.getSvp()))));
			}
		}

		/* Let's see if there is still any negative units left once        */
		/* they have been sold to clear a debt.                            */
		compute(wsaaTotalUnits, 5).set(sub(utrnaloIO.getNofUnits(), (mult(utrsIO.getCurrentUnitBal(), -1))));
		if (isLT(wsaaTotalUnits, 0)) {
			setPrecision(utrnaloIO.getNofUnits(), 5);
			utrnaloIO.setNofUnits(sub(utrnaloIO.getNofUnits(), wsaaTotalUnits));
		}
		if (isEQ(utrnaloIO.getUnitType(), "A")) {
			utrnaloIO.setNofDunits(utrnaloIO.getNofUnits());
		}
		else {
			if (isNE(utrnaloIO.getNofUnits(), ZERO)
					&& isEQ(utrnaloIO.getNofDunits(), ZERO)) {
				setPrecision(utrnaloIO.getNofDunits(), 6);
				utrnaloIO.setNofDunits(div(utrnaloIO.getNofUnits(), utrnaloIO.getDiscountFactor()), true);
			}
		}
	}

	private void accountingPostings1300(){
		accountingPostings1310();	
	}

	private void accountingPostings1310(){
		wsaaPostFundAcct.set(0);
		wsaaPostBidofferAcct.set(0);
		wsaaPostBarebidAcct.set(0);
		wsaaPostChargeAcct.set(0);
		wsaaUtrnaloFundAmount.set(0);
		wsaaNominalRate.set(0);
		wsaaX.set(0);

		postedAmountCalcs1320();//B5102

		if (isNE(utrnaloIO.getCntcurr(), utrnaloIO.getFundCurrency())) {
			acctForCurrConversion1330();
		}
		fundPostings1340();
		if (isNE(wsaaPostFund, ZERO)) {
			fundInvestment1350();
		}
		if (isNE(wsaaPostBidoffer, ZERO)) {
			bidOfferSpread1360();
		}
		if (isNE(wsaaPostBarebid, ZERO)) {
			bidBareSpread1370();
		}
		if (isNE(wsaaPostCharge, ZERO)) {
			chargesCosts1380();
		}
	}	

	private void postedAmountCalcs1320()
	{
		compute(wsaaFund, 6).setRounded(mult(utrnaloIO.getNofUnits(), ufpricerec.barePrice));
		wsaaPostFund.setRounded(wsaaFund);
		compute(wsaaBidoffer, 6).setRounded(mult(utrnaloIO.getNofUnits(), (sub(wsaaPrice, ufpricerec.bidPrice))));
		wsaaPostBidoffer.setRounded(wsaaBidoffer);

		if (isEQ(utrnaloIO.getUnitType(), "A")) {
			compute(wsaaBarebid, 6).setRounded(sub(sub(utrnaloIO.getFundAmount(), wsaaBidoffer), wsaaFund));
		}
		else {
			compute(wsaaBarebid, 6).setRounded(mult(utrnaloIO.getNofUnits(), (sub(ufpricerec.bidPrice, ufpricerec.barePrice))));
		}
		wsaaPostBarebid.setRounded(wsaaBarebid);
		compute(wsaaCharge, 6).setRounded(sub(sub(sub(utrnaloIO.getFundAmount(), wsaaFund), wsaaBidoffer), wsaaBarebid));
		wsaaPostCharge.setRounded(wsaaCharge);
		compute(wsaaBalanceCheck, 2).set(sub(add(add(add(wsaaPostFund, wsaaPostBidoffer), wsaaPostBarebid), wsaaPostCharge), utrnaloIO.getFundAmount()));

		x6000PostedAcctAmountCalcs();//referB5102

		if (isEQ(wsaaBalanceCheck, ZERO)) {
			/*NEXT_SENTENCE*/
		} else {
			if (isNE(wsaaPostCharge, ZERO)) {
				wsaaPostCharge.subtract(wsaaBalanceCheck);
			}
			else {
				if (isNE(wsaaPostBarebid, ZERO)) {
					wsaaPostBarebid.subtract(wsaaBalanceCheck);
				}
				else {
					if (isNE(wsaaPostBidoffer, ZERO)) {
						wsaaPostBidoffer.subtract(wsaaBalanceCheck);
					}
					else {
						if (isNE(wsaaPostFund, ZERO)) {
							wsaaPostFund.subtract(wsaaBalanceCheck);
						}
					}
				}
			}
		}
	}

	private void acctForCurrConversion1330()
	{
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		lifacmvrec.origamt.set(utrnaloIO.getFundAmount());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(10);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		} else {
			t5645Ix.set(27);
			lifacmvrec.substituteCode[6].set(SPACES);
		}		

		x1000CallLifacmv();
		/*    Post the Contract amount in its currency. (ditto).*/
		lifacmvrec.origcurr.set(utrnaloIO.getCntcurr());
		lifacmvrec.origamt.set(utrnaloIO.getContractAmount());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(9);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		} else {
			t5645Ix.set(26);
			lifacmvrec.substituteCode[6].set(SPACES);
		}

		x1000CallLifacmv();
	}

	protected void fundPostings1340()	{
		lifacmvrec.origcurr.set(utrnaloIO.getCntcurr());
		lifacmvrec.origamt.set(utrnaloIO.getContractAmount());

		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(29);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
			wsaaT5645Sacscode[29].set(utrnaloIO.getSacscode());
			wsaaT5645Sacstype[29].set(utrnaloIO.getSacstyp());
			wsaaT5645Glmap[29].set(utrnaloIO.getGenlcde());
		} else {
			t5645Ix.set(16);
			lifacmvrec.substituteCode[6].set(SPACES);
			wsaaT5645Sacscode[16].set(utrnaloIO.getSacscode());
			wsaaT5645Sacstype[16].set(utrnaloIO.getSacstyp());
			wsaaT5645Glmap[16].set(utrnaloIO.getGenlcde());
		}

		x1000CallLifacmv();
	}

	private void fundInvestment1350() {
		lifacmvrec.origamt.set(wsaaPostFund);
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(30);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		} else {
			t5645Ix.set(17);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.acctamt.set(0);
		if (isNE(wsaaPostFundAcct, ZERO)) {
			lifacmvrec.acctamt.set(wsaaPostFundAcct);
		}

		x1000CallLifacmv();
		lifacmvrec.acctamt.set(ZERO);
	}

	private void bidOfferSpread1360() {
		lifacmvrec.origamt.set(wsaaPostBidoffer);
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(1);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		} else {
			t5645Ix.set(18);
			lifacmvrec.substituteCode[6].set(SPACES);
		}

		lifacmvrec.acctamt.set(0);
		if (isNE(wsaaPostBidofferAcct, ZERO)) {
			lifacmvrec.acctamt.set(wsaaPostBidofferAcct);
		}

		x1000CallLifacmv();
		lifacmvrec.acctamt.set(ZEROS);
	}

	protected void bidBareSpread1370() {
		lifacmvrec.origamt.set(wsaaPostBarebid);
		if ((setPrecision(ZERO, 5) && isLT((sub(ufpricerec.bidPrice, ufpricerec.barePrice)), ZERO))) {
			compute(lifacmvrec.origamt, 2).set(sub(ZERO, lifacmvrec.origamt));
		}
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(2);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(19);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		lifacmvrec.acctamt.set(0);
		if (isNE(wsaaPostBarebidAcct, ZERO)) {
			lifacmvrec.acctamt.set(wsaaPostBarebidAcct);
		}

		x1000CallLifacmv();
		lifacmvrec.acctamt.set(ZERO);
	}

	private void chargesCosts1380()
	{
		lifacmvrec.origamt.set(wsaaPostCharge);
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());
		if (isGT(wsaaPostCharge, ZERO)) {
			if (componLevelAccounted.isTrue()) {
				t5645Ix.set(3);
				lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
			}
			else {
				t5645Ix.set(20);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
		}
		else {
			if (componLevelAccounted.isTrue()) {
				t5645Ix.set(4);
				lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
			}
			else {
				t5645Ix.set(21);
				lifacmvrec.substituteCode[6].set(SPACES);
			}
		}

		lifacmvrec.acctamt.set(0);
		if (isNE(wsaaPostChargeAcct, ZERO)) {
			lifacmvrec.acctamt.set(wsaaPostChargeAcct);
		}

		x1000CallLifacmv();
		lifacmvrec.acctamt.set(ZERO);
	}


	private void fluctuationCheck1400(){
		ufpricerec.function.set("FLUCT");
		ufpricerec.mode.set("BATCH");
		ufpricerec.effdate.set(datcon1rec.intDate);
		callProgram(Ufprice.class, ufpricerec.ufpriceRec);

		if (isNE(ufpricerec.statuz, varcom.oK)) {
			drylogrec.params.set(ufpricerec.ufpriceRec);
			drylogrec.statuz.set(ufpricerec.statuz);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();			
		}

		if (isEQ(ufpricerec.barePrice, wsaaUfprBarePrice)) {
			return ;// GO TO 1490-EXIT
		}

		compute(lifacmvrec.origamt, 6).setRounded(mult(utrnaloIO.getNofUnits(), (sub(ufpricerec.barePrice, wsaaUfprBarePrice))));
		lifacmvrec.origcurr.set(utrnaloIO.getFundCurrency());

		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(6);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(23);
			lifacmvrec.substituteCode[6].set(SPACES);
		}

		x1000CallLifacmv();
		/*    Update Negative Fluctuation ledger posting.*/
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(7);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		}
		else {
			t5645Ix.set(24);
			lifacmvrec.substituteCode[6].set(SPACES);
		}
		x1000CallLifacmv();

		/*    Control total for number of Fluctuations.*/
		drycntrec.contotNumber.set(ct14);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}
	//ILPI-121 Starts
	private void createReportRecord1500(){	
		dryrDryrptRecInner.dryrReportName.set("R5106");
		dryrDryrptRecInner.r5106Fundamt.set(utrnaloIO.getFundAmount());
		dryrDryrptRecInner.r5106Priceused.set(utrnaloIO.getPriceUsed());
		dryrDryrptRecInner.r5106NoFrunts.set(utrnaloIO.getNofUnits());
		datcon1rec.function.set(varcom.conv);
		datcon1rec.intDate.set(utrnaloIO.getPriceDateUsed());
		Datcon1.getInstance().mainline(datcon1rec.datcon1Rec);
		dryrDryrptRecInner.r5106PriceDt.set(datcon1rec.extDate);
		dryrDryrptRecInner.r5106Dmdunts.set(utrnaloIO.getNofDunits());
		dryrDryrptRecInner.r5106Vrtfnd.set(utrnaloIO.getUnitVirtualFund());
		dryrDryrptRecInner.r5106Unityp.set(utrnaloIO.getUnitType());
		dryrDryrptRecInner.r5106Chdrnum.set(utrnaloIO.getChdrnum());
		dryrDryrptRecInner.r5106Batctrcde.set(utrnaloIO.getBatctrcde());
		//  Set the Sort key values
		dryrDryrptRecInner.r5106vrtfnd.set(utrnaloIO.getUnitVirtualFund());
		dryrDryrptRecInner.r5106unityp.set(utrnaloIO.getUnitType());
		dryrDryrptRecInner.r5106chdrnum.set(utrnaloIO.getChdrnum());
		dryrDryrptRecInner.r5106batctrcde.set(utrnaloIO.getBatctrcde());
		dryrptrec.sortkey.set(dryrDryrptRecInner.r5106R5106Key);
		e000ReportRecords();
	}
	//ILPI-121 Ends
	private void updateUtrs1600() {
		updateUtrs1610();
	}

	private void updateUtrs1610()
	{
		/*UPDATE-UTRS*/
		utrsIO.setFormat(formatsInner.utrsrec);
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(utrsIO.statuz);
			drylogrec.params.set(utrsIO.getParams());
			a000FatalError();			
		}

		drycntrec.contotNumber.set(ct17);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	private void callTrigger1700() {
		callTrigger1710();
	}	

	private void callTrigger1710()
	{
		udtrigrec.function.set(wsaaProg);
		udtrigrec.statuz.set(varcom.oK);
		udtrigrec.mode.set("BATCH");
		udtrigrec.callingProg.set(wsaaProg);
		udtrigrec.fsuco.set(drypDryprcRecInner.fsuCompany);
		udtrigrec.language.set(drypDryprcRecInner.language);
		udtrigrec.batchkey.set(drypDryprcRecInner.batchKey);
		udtrigrec.effdate.set(drypDryprcRecInner.effectiveDate);
		udtrigrec.chdrcoy.set(utrnaloIO.getChdrcoy());
		udtrigrec.chdrnum.set(utrnaloIO.getChdrnum());
		udtrigrec.life.set(utrnaloIO.getLife());
		udtrigrec.coverage.set(utrnaloIO.getCoverage());
		udtrigrec.rider.set(utrnaloIO.getRider());
		udtrigrec.planSuffix.set(utrnaloIO.getPlanSuffix());
		udtrigrec.unitVirtualFund.set(utrnaloIO.getUnitVirtualFund());
		udtrigrec.unitType.set(utrnaloIO.getUnitType());
		udtrigrec.tranno.set(utrnaloIO.getTranno());
		udtrigrec.procSeqNo.set(utrnaloIO.getProcSeqNo());
		udtrigrec.triggerKey.set(utrnaloIO.getTriggerKey());
		udtrigrec.user.set(drypDryprcRecInner.user);
		wsaaDryprcrec.set(drypDryprcRecInner.dryprcRec);

		callProgram(utrnaloIO.getTriggerModule(), udtrigrec.udtrigrecRec);
		drypDryprcRecInner.dryprcRec.set(wsaaDryprcrec);
		if (isNE(udtrigrec.statuz, varcom.oK)) {
			drylogrec.statuz.set(udtrigrec.udtrigrecRec);
			drylogrec.params.set(udtrigrec.statuz);
			a000FatalError();				
		}


		if (isNE(utrnaloIO.getTriggerKey(), udtrigrec.triggerKey)) {
			getAppVars().addDiagnostic("UTRN RECORD FOR TRIGGER CALL "+utrnaloIO.getDataKey());
			getAppVars().addDiagnostic("UTRN TRIGGER "+utrnaloIO.getTriggerModule().toString() +" "+udtrigrec.triggerKey);
			drylogrec.statuz.set(udtrigrec.udtrigrecRec);
			drylogrec.params.set(udtrigrec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		drycntrec.contotNumber.set(ct35);
		drycntrec.contotValue.set(1);
		d000ControlTotals();		
	}

	private void accumulateFundChanges1800(){
		accumulate1810();
	}

	private void accumulate1810(){
		for (ix.set(1); !(isGT(ix, wsaaUfndSize) || isEQ(wsaaUfndType[ix.toInt()], SPACES)); ix.add(1)) { 
			ufndudlIO.company.set(drypDryprcRecInner.company);
			ufndudlIO.virtualFund.set(wsaaUfndFund[ix.toInt()]);
			ufndudlIO.unitType.set(wsaaUfndType[ix.toInt()]);
			ufndudlIO.nofunts.set(wsaaUfndNofUnits[ix.toInt()]);

			dacmIO.setParams(SPACES);
			dacmIO.setRecKeyData(SPACES);
			dacmIO.setRecNonKeyData(SPACES);

			dacmIO.setScheduleThreadNo(drypDryprcRecInner.threadNumber);
			dacmIO.setCompany(drypDryprcRecInner.company);
			dacmIO.setRecformat(formatsInner.ufndudlrec);
			dacmIO.setDataarea(ufndudlIO.getDataArea());
			dacmIO.setFunction(varcom.writr);
			dacmIO.setFormat(formatsInner.dacmrec);

			SmartFileCode.execute(appVars, dacmIO);
			if(!isEQ(dacmIO.getStatuz(),varcom.oK)){
				drylogrec.statuz.set(dacmIO.getStatuz());
				drylogrec.params.set(dacmIO.getParams());
				a000FatalError();

			}

			wsaaUfndFund[ix.toInt()].set(SPACES);
			wsaaUfndType[ix.toInt()].set(SPACES);
			wsaaUfndNofUnits[ix.toInt()].set(ZERO);
		}
	}

	



	private void increaseCoverageDebt1900() {
		try {
			increaseCoverageDebt1910();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	private void increaseCoverageDebt1910()
	{
		//ilpi-168
		dryrDryrptRecInner.r5107Lapind.set("D");		
		computeNewUtrnValues5812();
		postCovDebtAcmvDebit1930();
		postCovDebtAcmvCredit1940();
		updateCovrDebt1950();

		/* PERFORM 5816-CREATE-UTRN-MOVEMENT.                           */
		drycntrec.contotNumber.set(ct11);
		drycntrec.contotValue.set(1);
		d000ControlTotals();	

		return;		
	}

	private void computeNewUtrnValues5812() {
		setPrecision(utrnaloIO.getNofDunits(), 6);
		utrnaloIO.setNofDunits(sub(ZERO, utrsIO.getCurrentDunitBal()), true);
		setPrecision(utrnaloIO.getNofUnits(), 6);
		utrnaloIO.setNofUnits(sub(ZERO, utrsIO.getCurrentUnitBal()), true);
		setPrecision(utrnaloIO.getFundAmount(), 6);
		utrnaloIO.setFundAmount(mult(mult(utrnaloIO.getNofDunits(), wsaaPrice), utrnaloIO.getDiscountFactor()), true);

		compute(wsaaDebt, 2).set(mult(utrnaloIO.getContractAmount(), (-1)));

		if (isEQ(utrnaloIO.getCntcurr(), utrnaloIO.getFundCurrency())) {
			utrnaloIO.setContractAmount(utrnaloIO.getFundAmount());
			utrnaloIO.setFundRate(1);
		} else {
			conlinkrec.currIn.set(utrnaloIO.getFundCurrency());
			conlinkrec.currOut.set(utrnaloIO.getCntcurr());
			/*    MOVE UFPR-PRICE-DATE         TO CLNK-CASHDATE             */
			conlinkrec.cashdate.set(utrnaloIO.getMoniesDate());
			conlinkrec.amountIn.set(utrnaloIO.getFundAmount());
			conlinkrec.function.set("SURR");

			x3000CallXcvrt();
			utrnaloIO.setContractAmount(conlinkrec.amountOut);
			utrnaloIO.setFundRate(conlinkrec.rateUsed);
		}

		compute(wsaaDebt, 3).setRounded(add(wsaaDebt, utrnaloIO.getContractAmount()));
	}

	private void postCovDebtAcmvDebit1930()	{
		lifacmvrec.origcurr.set(utrnaloIO.getCntcurr());
		if (isLT(utrnaloIO.getContractAmount(), ZERO)) {
			compute(lifacmvrec.origamt, 2).set(sub(ZERO, wsaaDebt));
		} else {
			lifacmvrec.origamt.set(wsaaDebt);
		}

		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(8);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		} else {
			t5645Ix.set(25);
			lifacmvrec.substituteCode[6].set(SPACES);
		}

		x1000CallLifacmv();
	}

	private void postCovDebtAcmvCredit1940() {
		if (componLevelAccounted.isTrue()) {
			t5645Ix.set(11);
			lifacmvrec.substituteCode[6].set(utrnaloIO.getCrtable());
		} else {
			t5645Ix.set(28);
			lifacmvrec.substituteCode[6].set(SPACES);
		}

		x1000CallLifacmv();
	}

	private void updateCovrDebt1950(){
		covrudlIO.setChdrcoy(utrnaloIO.getChdrcoy());
		covrudlIO.setChdrnum(utrnaloIO.getChdrnum());
		covrudlIO.setLife(utrnaloIO.getLife());
		covrudlIO.setCoverage(utrnaloIO.getCoverage());
		covrudlIO.setRider(utrnaloIO.getRider());
		covrudlIO.setPlanSuffix(utrnaloIO.getPlanSuffix());
		covrudlIO.setFunction(varcom.readh);

		SmartFileCode.execute(appVars, covrudlIO);
		if (isNE(covrudlIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrudlIO.getParams());
			drylogrec.statuz.set(covrudlIO.statuz);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();			
		}

		setPrecision(covrudlIO.getCoverageDebt(), 2);
		covrudlIO.setCoverageDebt(add(covrudlIO.getCoverageDebt(), wsaaDebt));
		covrudlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrudlIO);
		if (isNE(covrudlIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrudlIO.getParams());
			drylogrec.statuz.set(covrudlIO.statuz);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}
	}

	private void lapseCoverage2000(Utrnpf utrnpf) //ILIFE-3704
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					lapseCoverage2010();
					callSubroutine2020(utrnpf); //ILIFE-3704
				case exit2090:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	private void lapseCoverage2010() {
		drycntrec.contotNumber.set(ct12);
		drycntrec.contotValue.set(1);
		d000ControlTotals();	
		//ILPI-168
		dryrDryrptRecInner.r5107Lapind.set("L");

		itdmIO.setItemcoy(drypDryprcRecInner.company);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(utrnaloIO.getCrtable());
		itdmIO.setItmfrm(drypDryprcRecInner.runDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);

		SmartFileCode.execute(appVars, itdmIO);

		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.statuz);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

		if(!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.company)||!isEQ(itdmIO.getItemtabl(),t5687)||!isEQ(itdmIO.getItemitem(),utrnaloIO.crtable)||isGT(itdmIO.getItmfrm(),drypDryprcRecInner.runDate)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(e308);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

		t5687rec.t5687Rec.set(itdmIO.getGenarea());
		loadT6597IntoT56872700();

		if (isEQ(t5687rec.nonForfeitMethod, SPACES)) {
			goTo(GotoLabel.exit2090);
		}

		if(isEQ(t6597rec.premsubr04,SPACES)){
			goTo(GotoLabel.exit2090);
		}
	}

	private void callSubroutine2020(Utrnpf utrnpf) { //ILIFe-3704
		ovrduerec.ovrdueRec.set(SPACES);
		ovrduerec.language.set(drypDryprcRecInner.language);
		ovrduerec.chdrcoy.set(utrnaloIO.getChdrcoy());
		ovrduerec.chdrnum.set(utrnaloIO.getChdrnum());
		ovrduerec.life.set(utrnaloIO.getLife());
		ovrduerec.coverage.set(utrnaloIO.getCoverage());
		ovrduerec.rider.set(utrnaloIO.getRider());
		ovrduerec.planSuffix.set(utrnaloIO.getPlanSuffix());
		ovrduerec.tranno.set(utrnaloIO.getTranno());
		ovrduerec.cntcurr.set(utrnaloIO.getCntcurr());
		ovrduerec.effdate.set(utrnaloIO.getMoniesDate());
		ovrduerec.outstamt.set(utrnaloIO.getFundAmount());
		ovrduerec.aloind.set("N");
		ovrduerec.cnttype.set(utrnaloIO.getContractType());
		ovrduerec.crtable.set(utrnaloIO.getCrtable());
		ovrduerec.company.set(batcdorrec.company);
		ovrduerec.batcbrn.set(batcdorrec.branch);
		ovrduerec.acctyear.set(batcdorrec.actyear);
		ovrduerec.acctmonth.set(batcdorrec.actmonth);
		ovrduerec.trancode.set(batcdorrec.trcde);
		//ILIFE-3704 Start by dpuhawan
		//x2000GetPayr();
		Dry5102DTO dryd102Dto = new Dry5102DTO();
		dryd102Dto = utrnpf.getDry5101Dto();

		//ovrduerec.ptdate.set(payrlifIO.getPtdate());
		ovrduerec.ptdate.set(dryd102Dto.getPtdate());
		//ILIFE-3704 End
		ovrduerec.btdate.set(ZERO);
		ovrduerec.occdate.set(ZERO);
		ovrduerec.agntnum.set(ZERO);
		ovrduerec.cownnum.set(ZERO);
		ovrduerec.statcode.set(ZERO);
		ovrduerec.billfreq.set(ZERO);
		ovrduerec.crrcd.set(ZERO);
		ovrduerec.termid.set(SPACES);
		//ILIFE-3704 Start by dpuhawan
		//ovrduerec.polsum.set(chdrlifIO.getPolsum());
		ovrduerec.polsum.set(dryd102Dto.getPolsum());
		//ILIFE-3704 End
		ovrduerec.actualVal.set(ZERO);

		ovrduerec.tranDate.set(drypDryprcRecInner.runDate);
		ovrduerec.runDate.set(drypDryprcRecInner.runDate);
		ovrduerec.tranTime.set(999999);
		ovrduerec.user.set(ZERO);
		ovrduerec.sumins.set(ZERO);
		ovrduerec.newSumins.set(ZERO);
		ovrduerec.instprem.set(ZERO);
		ovrduerec.premCessDate.set(ZERO);
		ovrduerec.pupfee.set(ZERO);
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.pupfee.set(ZERO);
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.pumeth.set(SPACES);
		ovrduerec.efdcode.set(SPACES);
		ovrduerec.pstatcode.set(SPACES);
		ovrduerec.occdate.set(utrnaloIO.getCrComDate());

		callProgram(t6597rec.premsubr04, ovrduerec.ovrdueRec);
		if (isNE(ovrduerec.statuz, varcom.oK) && isNE(ovrduerec.statuz, "OMIT")) {
			drylogrec.params.set(ovrduerec.ovrdueRec);
			drylogrec.statuz.set(ovrduerec.statuz);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();			
		}

		if (isEQ(ovrduerec.statuz, "OMIT")) {
			goTo(GotoLabel.exit2090);
		}

		if(isEQ(t6597rec.crstat04,SPACES) && isEQ(t6597rec.crstat04,SPACES)){
			goTo(GotoLabel.exit2090);
		}

		covrudlIO.setChdrcoy(utrnaloIO.getChdrcoy());
		covrudlIO.setChdrnum(utrnaloIO.getChdrnum());
		covrudlIO.setLife(utrnaloIO.getLife());
		covrudlIO.setCoverage(utrnaloIO.getCoverage());
		covrudlIO.setRider(utrnaloIO.getRider());
		covrudlIO.setPlanSuffix(utrnaloIO.getPlanSuffix());
		covrudlIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, covrudlIO);
		if (isNE(covrudlIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrudlIO.getParams());
			drylogrec.statuz.set(covrudlIO.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		covrudlIO.setPstatcode(t6597rec.cpstat04);
		covrudlIO.setStatcode(t6597rec.crstat04);
		covrudlIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrudlIO);
		if (isNE(covrudlIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrudlIO.getParams());
			drylogrec.statuz.set(covrudlIO.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	private void errorOnCoverage2100() {
		errorOnCoverage2110();
	}

	private void errorOnCoverage2110() {
		//ILPI-168
		dryrDryrptRecInner.dryrReportName.set("R5107");
		dryrDryrptRecInner.r5107Rectype.set("UTRN");
		dryrDryrptRecInner.r5107Chdrnum.set(utrnaloIO.chdrnum);
		dryrDryrptRecInner.r5107Life.set(utrnaloIO.life);
		dryrDryrptRecInner.r5107Coverage.set(utrnaloIO.coverage);
		dryrDryrptRecInner.r5107Rider.set(utrnaloIO.rider);
		dryrDryrptRecInner.r5107Plnsfx.set(utrnaloIO.planSuffix);
		dryrDryrptRecInner.r5107Vrtfnd.set(SPACES);
		dryrDryrptRecInner.r5107Unityp.set(SPACES);
		dryrDryrptRecInner.r5107Tranno.set(utrnaloIO.tranno);
		dryrDryrptRecInner.r5107Batctrcde.set(SPACES);
		dryrDryrptRecInner.r5107Cntamnt.set(utrnaloIO.contractAmount);
		dryrDryrptRecInner.r5107Cntcurr.set(utrnaloIO.cntcurr);
		dryrDryrptRecInner.r5107Fundamnt.set(ZERO);
		dryrDryrptRecInner.r5107Fndcurr.set(SPACES);
		dryrDryrptRecInner.r5107Fundrate.set(ZERO);
		dryrDryrptRecInner.r5107Age.set(datcon3rec.freqFactor);
		dryrDryrptRecInner.r5107Triger.set("UNPAID DBT");

		dryrDryrptRecInner.r5107vrtfnd.set(SPACES);
		dryrDryrptRecInner.r5107unityp.set(SPACES);
		dryrDryrptRecInner.r5107chdrnum.set(utrnaloIO.chdrnum);
		dryrDryrptRecInner.r5107batctrcde.set(SPACES);
		dryrptrec.sortkey.set(dryrDryrptRecInner.r5107R5107Key);

		drycntrec.contotNumber.set(ct13);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		e000ReportRecords();
		drypDryprcRecInner.processUnsuccesful.setTrue();
	}

	private void getBuySellPrice2200() {
		getBuySellPrice2210();
	}

	private void getBuySellPrice2210() {
		/*GET-BUY-SELL-PRICE*/
		wsaaPrice.set(ZERO);
		getT66472300();

		if (isEQ(utrnaloIO.getSwitchIndicator(), "Y")) {
			getFundSwitchPrice2400();
		}

		if (isNE(utrnaloIO.getSwitchIndicator(), "Y")) {
			if (isEQ(t6647rec.bidoffer, "B")) {
				wsaaPrice.set(ufpricerec.bidPrice);
			} else {
				wsaaPrice.set(ufpricerec.offerPrice);
			}
		}
	}

	private void getT66472300() {
		getT66472310();
	}

	private void getT66472310() {
		itdmIO.setItemcoy(drypDryprcRecInner.company);
		itdmIO.setItemtabl(t6647);
		itdmIO.getItemitem().setSub1String(1, 4, utrnaloIO.getBatctrcde());
		itdmIO.getItemitem().setSub1String(5, 4, utrnaloIO.getContractType());

		itdmIO.setItmfrm(drypDryprcRecInner.runDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);

		SmartFileCode.execute(appVars, itdmIO);		
		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

		if(!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.company)||!isEQ(itdmIO.getItemtabl(),t6647)||
				!isEQ(subString(itdmIO.getItemitem(), 1, 4),utrnaloIO.batctrcde) || !isEQ(subString(itdmIO.getItemitem(), 5, 4),utrnaloIO.contractType)
				||isGT(itdmIO.getItmfrm(),drypDryprcRecInner.runDate)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();			
		}
		t6647rec.t6647Rec.set(itdmIO.genarea);
	}

	private void getFundSwitchPrice2400() {
		getFundSwitchPrice2410();
	}

	private void getFundSwitchPrice2410() {
		itdmIO.setItemcoy(drypDryprcRecInner.company);
		itdmIO.setItemtabl(t5544);
		itdmIO.getItemitem().setSub1String(1, 4, t6647rec.swmeth);
		itdmIO.getItemitem().setSub1String(5, 4, utrnaloIO.getCntcurr());

		itdmIO.setItmfrm(drypDryprcRecInner.runDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);

		SmartFileCode.execute(appVars, itdmIO);
		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();			
			a000FatalError();
		}
		//ILPI-135, Part surrender transaction
		if(!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.company)||!isEQ(itdmIO.getItemtabl(),t5544)||
				!isEQ(subString(itdmIO.getItemitem(), 1, 4),t6647rec.swmeth) || !isEQ(subString(itdmIO.getItemitem(), 5, 4),utrnaloIO.getCntcurr())
				||isGT(itdmIO.getItmfrm(),drypDryprcRecInner.runDate)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();			
		}

		t5544rec.t5544Rec.set(itdmIO.genarea);
		if(isGT(utrnaloIO.contractAmount,ZERO)||isLT(utrnaloIO.surrenderPercent,ZERO)){
			fundSwitchBuyPrice2500();
		}else{
			fundSwitchSellPrice2600();			
		}
	}

	private void fundSwitchBuyPrice2500() {
		try {
			fundSwitchBuyPrice2510();
		}
		catch (GOTOException e){
			/* Expected exception for control flow purposes. */
		}
	}

	private void fundSwitchBuyPrice2510() {
		if (isEQ(t5544rec.btobid, "Y")) {
			wsaaPrice.set(ufpricerec.bidPrice);
			return;
		}

		if (isEQ(t5544rec.btooff, "Y")) {
			wsaaPrice.set(ufpricerec.offerPrice);
			return;
		}

		if (isEQ(t5544rec.btodisc, "Y")) {
			compute(wsaaPrice, 6).setRounded(div(mult(ufpricerec.offerPrice, (sub(100, t5544rec.discountOfferPercent))), 100));
			return;
		}

		itdmIO.setItemcoy(drypDryprcRecInner.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(utrnaloIO.getUnitVirtualFund());
		itdmIO.setItmfrm(drypDryprcRecInner.runDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);

		SmartFileCode.execute(appVars, itdmIO);		
		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();			
			a000FatalError();		
		}

		if(!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.company)||!isEQ(itdmIO.getItemtabl(),t5515)||!isEQ(itdmIO.getItemitem(),utrnaloIO.unitVirtualFund)||isGT(itdmIO.getItmfrm(),drypDryprcRecInner.runDate)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

		t5515rec.t5515Rec.set(itdmIO.getGenarea());		

		if(isEQ(t5515rec.btobid,"Y")){
			wsaaPrice.set(ufpricerec.bidPrice);
			return;
		}

		if(isEQ(t5515rec.btooff,"Y") || isEQ(t5515rec.otoff,"Y")){
			wsaaPrice.set(ufpricerec.offerPrice);
			return;
		}

		if (isEQ(t5515rec.btodisc, "Y")) {
			compute(wsaaPrice, 6).setRounded(div(mult(ufpricerec.offerPrice, (sub(100, t5515rec.discountOfferPercent))), 100));
			return;
		}	

		drycntrec.contotNumber.set(ct15);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	protected void fundSwitchSellPrice2600() {
		fundSwitchSellPrice2610();
	}

	protected void fundSwitchSellPrice2610() {
		/* Start with T5544 flags.*/
		if (isNE(t5544rec.bidToFund, "Y")) {
			if (isEQ(t5544rec.btobid, "Y")
					|| isEQ(t5544rec.btodisc, "Y")
					|| isEQ(t5544rec.btooff, "Y")) {
				wsaaPrice.set(ufpricerec.bidPrice);
			}
			else {
				wsaaPrice.set(ufpricerec.offerPrice);
			}

			return;
		}

		itdmIO.setItemcoy(drypDryprcRecInner.company);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(utrnaloIO.getUnitVirtualFund());
		itdmIO.setItmfrm(drypDryprcRecInner.runDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setStatuz(varcom.oK);

		SmartFileCode.execute(appVars, itdmIO);		
		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();			
			a000FatalError();		
		}

		if(!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.company)||!isEQ(itdmIO.getItemtabl(),t5515)||!isEQ(itdmIO.getItemitem(),utrnaloIO.unitVirtualFund)||isGT(itdmIO.getItmfrm(),drypDryprcRecInner.runDate)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

		t5515rec.t5515Rec.set(itdmIO.getGenarea());		

		if (isEQ(t5515rec.btobid, "Y") || isEQ(t5515rec.btodisc, "Y") || isEQ(t5515rec.btooff, "Y")) {
			wsaaPrice.set(ufpricerec.bidPrice);
		} else {
			wsaaPrice.set(ufpricerec.offerPrice);
		}	
	}	

	private void loadT6597IntoT56872700() {
		loadT6597IntoT56872710();
	}

	private void loadT6597IntoT56872710() {
		if (isEQ(t5687rec.nonForfeitMethod, SPACES)) {
			return;
		}

		itdmIO.setItemcoy(drypDryprcRecInner.company);
		itdmIO.setItemtabl(t6597);
		itdmIO.setItemitem(t5687rec.nonForfeitMethod);
		itdmIO.setItmfrm(drypDryprcRecInner.runDate);
		itdmIO.setFunction(varcom.begn);

		SmartFileCode.execute(appVars, itdmIO);
		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.company)||!isEQ(itdmIO.getItemtabl(),t6597rec)||!isEQ(itdmIO.getItemitem(),t5687rec.nonForfeitMethod)
				|| isGT(itdmIO.getItmfrm(),drypDryprcRecInner.runDate)){
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();			
		}		

		t6597rec.t6597Rec.set(itdmIO.getGenarea());
	}

	//ILPI-254
	//	private void fillParams3000() {
	//		try {
	//			if(!isEQ(utrnextIO.statuz,varcom.endp)){
	//				drypDryprcRecInner.drypCnttype.set(utrnextIO.contractType);
	//			}
	//		} catch (GOTOException e){
	//			
	//		}
	//	}

	private void x1000CallLifacmv() {
		x1010Call();
	}

	private void x1010Call() {
		if (isEQ(lifacmvrec.origamt, ZERO)) {
			return;
		}

		lifacmvrec.function.set("PSTW");
		lifacmvrec.jrnseq.set(wsaaJrnseq);

		if (componLevelAccounted.isTrue()) {
			lifacmvrec.rldgacct.set(SPACES);
			wsaaPlan.set(utrnaloIO.getPlanSuffix());
			StringUtil stringVariable1 = new StringUtil();
			stringVariable1.addExpression(utrnaloIO.getChdrnum());
			stringVariable1.addExpression(utrnaloIO.getLife());
			stringVariable1.addExpression(utrnaloIO.getCoverage());
			stringVariable1.addExpression(utrnaloIO.getRider());
			stringVariable1.addExpression(wsaaPlan);
			stringVariable1.setStringInto(lifacmvrec.rldgacct);
		}
		else {
			lifacmvrec.rldgacct.set(utrnaloIO.getChdrnum());
		}
		lifacmvrec.sacscode.set(wsaaT5645Sacscode[t5645Ix.toInt()]);
		lifacmvrec.sacstyp.set(wsaaT5645Sacstype[t5645Ix.toInt()]);
		lifacmvrec.glcode.set(wsaaT5645Glmap[t5645Ix.toInt()]);
		lifacmvrec.glsign.set(wsaaT5645Sign[t5645Ix.toInt()]);
		lifacmvrec.contot.set(wsaaT5645Cnttot[t5645Ix.toInt()]);
		lifacmvrec.rdocnum.set(utrnaloIO.getChdrnum());
		lifacmvrec.tranno.set(utrnaloIO.getTranno());
		lifacmvrec.tranref.set(utrnaloIO.getTranno());
		lifacmvrec.effdate.set(utrnaloIO.getMoniesDate());
		lifacmvrec.substituteCode[1].set(utrnaloIO.getContractType());
		lifacmvrec.substituteCode[2].set(utrnaloIO.getUnitVirtualFund());
		callProgram(Lifacmv.class, lifacmvrec.lifacmvRec);
		if (isNE(lifacmvrec.statuz, varcom.oK)) {
			drylogrec.params.set(lifacmvrec.lifacmvRec);
			drylogrec.statuz.set(lifacmvrec.statuz);
			drylogrec.dryDatabaseError.setTrue();	
			a000FatalError();
		}
		wsaaJrnseq.add(1);

		drycntrec.contotNumber.set(ct20);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	//ILIFE-3704 Start by dpuhawan
	/*
	private void x2000GetPayr() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					x2010GetPayr();
				case x2020CallPayrlifio:
					x2020CallPayrlifio();
					x2080ReadChdrRecord();
				case exit390:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	private void x2010GetPayr()	{
		payrlifIO.setChdrcoy(utrnaloIO.chdrcoy);
		payrlifIO.setChdrnum(utrnaloIO.chdrnum);
		payrlifIO.setValidflag(1);
		payrlifIO.setPayrseqno(1);
		payrlifIO.setFunction(varcom.begn);
	}

	private void x2020CallPayrlifio()	{
		SmartFileCode.execute(appVars, payrlifIO);
		if (isNE(payrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrlifIO.getParams());
			drylogrec.statuz.set(payrlifIO.statuz);
			a000FatalError();
		}
		if (isNE(payrlifIO.getChdrcoy(), utrnaloIO.getChdrcoy())
				|| isNE(payrlifIO.getChdrnum(), utrnaloIO.getChdrnum())
				|| isNE(payrlifIO.getValidflag(), "1")
				|| isNE(payrlifIO.getPayrseqno(), 1)) {
			payrlifIO.setFunction(varcom.nextr);
			x2020CallPayrlifio();
		}

	}

	private void x2080ReadChdrRecord()	{
		chdrlifIO.chdrcoy.set(utrnaloIO.chdrcoy);
		chdrlifIO.chdrnum.set(utrnaloIO.chdrnum);
		chdrlifIO.function.set(varcom.readh);
		chdrlifIO.format.set(formatsInner.chdrlifrec);

		SmartFileCode.execute(appVars, chdrlifIO);
		if (isNE(chdrlifIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.statuz.set(chdrlifIO.statuz);
			a000FatalError();
		}		
	}
	*/
	//ILIFE-3704 End
	private void x3000CallXcvrt() {
		x3010CallXcvrt();
	}

	private void x3010CallXcvrt() {
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(drypDryprcRecInner.company);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);

		if (isNE(conlinkrec.statuz, varcom.oK)) {
			drylogrec.params.set(conlinkrec.clnk002Rec);
			drylogrec.statuz.set(conlinkrec.statuz);
			drylogrec.drySystemError.setTrue();

			a000FatalError();
		}
	}

	private void x6000PostedAcctAmountCalcs() {
		x6010GetRate();
		x6020CheckBalance();
	}

	private void x6010GetRate() {
		itemIO.setItemitem(utrnaloIO.getFundCurrency());		
		x6100GetRate();

		if (isEQ(wsaaNominalRate, ZERO)) {
			if (isNE(t3629rec.contitem, SPACES)) {
				itemIO.setItemitem(t3629rec.contitem);
				x6100GetRate();
			} else {				
				drylogrec.statuz.set(g418);
				drylogrec.drySystemError.setTrue();				
				a000FatalError();
			}
		}

		if (isNE(utrnaloIO.getFundAmount(), ZERO)) {
			compute(wsaaUtrnaloFundAmount, 10).setRounded(mult(wsaaNominalRate, utrnaloIO.getFundAmount()));
		}

		if (isNE(wsaaPostFund, ZERO)) {
			compute(wsaaPostFundAcct, 10).setRounded(mult(wsaaNominalRate, wsaaPostFund));			
		}

		if (isNE(wsaaPostBidoffer, ZERO)) {
			compute(wsaaPostBidofferAcct, 10).setRounded(mult(wsaaNominalRate, wsaaPostBidoffer));			
		}

		if (isNE(wsaaPostBarebid, ZERO)) {
			compute(wsaaPostBarebidAcct, 10).setRounded(mult(wsaaNominalRate, wsaaPostBarebid));
		}
		if (isNE(wsaaPostCharge, ZERO)) {
			compute(wsaaPostChargeAcct, 10).setRounded(mult(wsaaNominalRate, wsaaPostCharge));
		}
	}

	private void x6020CheckBalance() {
		compute(wsaaBalanceCheckAcct, 2).set(sub(add(add(add(wsaaPostFundAcct, wsaaPostBidofferAcct), wsaaPostBarebidAcct), wsaaPostChargeAcct), wsaaUtrnaloFundAmount));
		if (isEQ(wsaaBalanceCheckAcct, ZERO)) {
			wsaaPostFundAcct.set(0);
			wsaaPostBidofferAcct.set(0);
			wsaaPostBarebidAcct.set(0);
			wsaaPostChargeAcct.set(0);
			wsaaUtrnaloFundAmount.set(0);
			wsaaNominalRate.set(0);
			wsaaX.set(0);
		} else {
			if (isNE(wsaaPostChargeAcct, ZERO)) {
				wsaaPostChargeAcct.subtract(wsaaBalanceCheckAcct);
				wsaaPostFundAcct.set(0);
				wsaaPostBidofferAcct.set(0);
				wsaaPostBarebidAcct.set(0);
			} else {
				if (isNE(wsaaPostBarebidAcct, ZERO)) {
					wsaaPostBarebidAcct.subtract(wsaaBalanceCheckAcct);
					wsaaPostFundAcct.set(0);
					wsaaPostBidofferAcct.set(0);
					wsaaPostChargeAcct.set(0);
				} else {
					if (isNE(wsaaPostBidofferAcct, ZERO)) {
						wsaaPostBidofferAcct.subtract(wsaaBalanceCheckAcct);
						wsaaPostFundAcct.set(0);
						wsaaPostBarebidAcct.set(0);
						wsaaPostChargeAcct.set(0);
					} else {
						if (isNE(wsaaPostFundAcct, ZERO)) {
							wsaaPostFundAcct.subtract(wsaaBalanceCheckAcct);
							wsaaPostChargeAcct.set(0);
							wsaaPostBidofferAcct.set(0);
							wsaaPostBarebidAcct.set(0);
						}
					}
				}
			}
		}
	}

	private void x6100GetRate() {
		x6110Readr();
	}

	private void x6110Readr() {
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.batccoy);
		itemIO.setItemtabl(t3629);
		itemIO.setFunction(varcom.readr);

		SmartFileCode.execute(appVars, itemIO);
		if(!isEQ(itemIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(itemIO.getParams());
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

		t3629rec.t3629Rec.set(itemIO.getGenarea());

		wsaaNominalRate.set(0);
		wsaaX.set(1);

		while ( !(isNE(wsaaNominalRate, ZERO)
				|| isGT(wsaaX, 7))) {	
			if (isGTE(utrnaloIO.getMoniesDate(), t3629rec.frmdate[wsaaX.toInt()])
					&& isLTE(utrnaloIO.getMoniesDate(), t3629rec.todate[wsaaX.toInt()])) {
				wsaaNominalRate.set(t3629rec.scrate[wsaaX.toInt()]);
			} else {
				wsaaX.add(1);
			}
		}
	}

	private void X8000InsuffValueProcess() {
		X8010Init();
	}

	private void X8010Init() {
		udivIO.setDataArea(SPACES);
		udivIO.setFormat(formatsInner.udivrec);
		udivIO.setChdrcoy(utrnaloIO.getChdrcoy());
		udivIO.setChdrnum(utrnaloIO.getChdrnum());
		udivIO.setTranno(utrnaloIO.getTranno());
		udivIO.setFunction(varcom.writr);

		SmartFileCode.execute(appVars, udivIO);
		if (isNE(udivIO.getStatuz(), varcom.oK)	&& isNE(udivIO.getStatuz(), varcom.dupr)) {
			drylogrec.params.set(udivIO.getParams());
			drylogrec.statuz.set(udivIO.statuz);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}
	}

	private void X8100DeleteUdiv() {
		X8110Delete();
	}

	private void X8110Delete() {
		udivIO.setChdrcoy(drypDryprcRecInner.company);
		udivIO.setChdrnum(drypDryprcRecInner.entity);
		udivIO.setFormat(formatsInner.udivrec);
		udivIO.setFunction(varcom.readh);

		SmartFileCode.execute(appVars, udivIO);
		if (isNE(udivIO.getStatuz(), varcom.oK)	&& isNE(udivIO.getStatuz(), varcom.mrnf)) {
			drylogrec.params.set(udivIO.getParams());
			drylogrec.statuz.set(udivIO.statuz);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

		if (isEQ(udivIO.getStatuz(), varcom.mrnf)) {
			return;
		}

		udivIO.setFormat(formatsInner.udivrec);
		udivIO.setFunction(varcom.delet);
		SmartFileCode.execute(appVars, udivIO);
		if (isNE(udivIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(udivIO.getParams());
			drylogrec.statuz.set(udivIO.statuz);
			drylogrec.dryDatabaseError.setTrue();

			a000FatalError();
		}

	}

	/*
	 * Class transformed  from Data Structure FORMATS--INNER
	 */
	private static final class FormatsInner { 
		/* FORMATS */
		private FixedLengthStringData acmvrevrec = new FixedLengthStringData(10).init("ACMVREVREC");
		private FixedLengthStringData chdrlnbrec = new FixedLengthStringData(10).init("CHDRLNBREC");
		private FixedLengthStringData itemrec = new FixedLengthStringData(10).init("ITEMREC");
		private FixedLengthStringData hitrrnlrec = new FixedLengthStringData(10).init("HITRRNLREC");
		private FixedLengthStringData utrnextrec = new FixedLengthStringData(10).init("UTRNEXTREC");

		private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).init("CHDRLIFREC");
		private FixedLengthStringData utrnupdrec = new FixedLengthStringData(10).init("UTRNUPDREC");
		private FixedLengthStringData utrsrec = new FixedLengthStringData(10).init("UTRSREC");
		private FixedLengthStringData ufndudlrec = new FixedLengthStringData(10).init("UFNDUDLREC");
		private FixedLengthStringData dacmrec = new FixedLengthStringData(10).init("DACMREC");
		private FixedLengthStringData utrnalorec = new FixedLengthStringData(10).init("UTRNALOREC");
		private FixedLengthStringData udivrec = new FixedLengthStringData(10).init("UDIVREC");
	}

	/*
	 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {
		//ILPI-97 starts
		public FixedLengthStringData dryprcRec = new FixedLengthStringData(621);
		public FixedLengthStringData statuz = new FixedLengthStringData(4).isAPartOf(dryprcRec, 0);
		public FixedLengthStringData error = new FixedLengthStringData(1).isAPartOf(dryprcRec, 4);
		public Validator noError = new Validator(error, "0");
		public Validator nonFatalError = new Validator(error, "1");
		public Validator fatalError = new Validator(error, "2");
		public FixedLengthStringData mode = new FixedLengthStringData(1).isAPartOf(dryprcRec, 5);
		public Validator batchMode = new Validator(mode, "B");
		public Validator onlineMode = new Validator(mode, "O");
		public PackedDecimalData runDate = new PackedDecimalData(8, 0).isAPartOf(dryprcRec, 6);
		public ZonedDecimalData runNumber = new ZonedDecimalData(8, 0).isAPartOf(dryprcRec, 11).setUnsigned();
		public FixedLengthStringData entityKey = new FixedLengthStringData(13).isAPartOf(dryprcRec, 19);
		public FixedLengthStringData company = new FixedLengthStringData(1).isAPartOf(entityKey, 0);
		public FixedLengthStringData branch = new FixedLengthStringData(2).isAPartOf(entityKey, 1);
		public FixedLengthStringData entityType = new FixedLengthStringData(2).isAPartOf(entityKey, 3);
		public FixedLengthStringData entity = new FixedLengthStringData(8).isAPartOf(entityKey, 5);
		public FixedLengthStringData language = new FixedLengthStringData(1).isAPartOf(dryprcRec, 32);
		public FixedLengthStringData procCode = new FixedLengthStringData(6).isAPartOf(dryprcRec, 33);
		public PackedDecimalData effectiveDate = new PackedDecimalData(8, 0).isAPartOf(dryprcRec, 39);
		public PackedDecimalData effectiveTime = new PackedDecimalData(6, 0).isAPartOf(dryprcRec, 44);
		public FixedLengthStringData batchKey = new FixedLengthStringData(19).isAPartOf(dryprcRec, 48);
		public FixedLengthStringData batcpfx = new FixedLengthStringData(2).isAPartOf(batchKey, 0);
		public FixedLengthStringData batccoy = new FixedLengthStringData(1).isAPartOf(batchKey, 2);
		public FixedLengthStringData batcbrn = new FixedLengthStringData(2).isAPartOf(batchKey, 3);
		public PackedDecimalData batcactyr = new PackedDecimalData(4, 0).isAPartOf(batchKey, 5);
		public PackedDecimalData batcactmn = new PackedDecimalData(2, 0).isAPartOf(batchKey, 8);
		public FixedLengthStringData batctrcde = new FixedLengthStringData(4).isAPartOf(batchKey, 10);
		public FixedLengthStringData batcbatch = new FixedLengthStringData(5).isAPartOf(batchKey, 14);
		public FixedLengthStringData fsuCompany = new FixedLengthStringData(1).isAPartOf(dryprcRec, 67);
		//cluster support by vhukumagrawa
		public PackedDecimalData threadNumber = new PackedDecimalData(3, 0).isAPartOf(dryprcRec, 68);
		public PackedDecimalData procSeqNo = new PackedDecimalData(3, 0).isAPartOf(dryprcRec, 70);
		public PackedDecimalData user = new PackedDecimalData(6, 0).isAPartOf(dryprcRec, 72);
		public FixedLengthStringData systParams = new FixedLengthStringData(250).isAPartOf(dryprcRec, 76);
		public FixedLengthStringData[] systParm = FLSArrayPartOfStructure(25, 10, systParams, 0);
		public PackedDecimalData tranno = new PackedDecimalData(5, 0).isAPartOf(dryprcRec, 326);
		public FixedLengthStringData additionalKey = new FixedLengthStringData(30).isAPartOf(dryprcRec, 329);
		public FixedLengthStringData rollForward = new FixedLengthStringData(1).isAPartOf(dryprcRec, 359);
		public Validator rollForwardMode = new Validator(rollForward, "R");
		public Validator normalMode = new Validator(rollForward, " ");
		public FixedLengthStringData detailOutput = new FixedLengthStringData(10).isAPartOf(dryprcRec, 360);
		public FixedLengthStringData required = new FixedLengthStringData(1).isAPartOf(detailOutput, 0);
		public Validator dtrdYes = new Validator(required, "Y");
		public Validator dtrdNo = new Validator(required, "N");
		public PackedDecimalData nxtprcdate = new PackedDecimalData(8, 0).isAPartOf(detailOutput, 1);
		public PackedDecimalData nxtprctime = new PackedDecimalData(6, 0).isAPartOf(detailOutput, 6);
		public FixedLengthStringData processResult = new FixedLengthStringData(1).isAPartOf(dryprcRec, 370);
		public Validator processSuccesful = new Validator(processResult, "Y");
		public Validator processUnsuccesful = new Validator(processResult, "N");
		//ILPI-97 ends

		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(dryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);

	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}


}