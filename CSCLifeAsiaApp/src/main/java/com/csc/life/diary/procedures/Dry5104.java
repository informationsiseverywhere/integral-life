package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.setPrecision;
import static com.quipoz.COBOLFramework.COBOLFunctions.BDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.FLSInittedArray;
import static com.quipoz.COBOLFramework.COBOLFunctions.HIVALUE;
import static com.quipoz.COBOLFramework.COBOLFunctions.PDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZDArrayPartOfArrayStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.add;
import static com.quipoz.COBOLFramework.COBOLFunctions.compute;
import static com.quipoz.COBOLFramework.COBOLFunctions.div;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolDate;
import static com.quipoz.COBOLFramework.COBOLFunctions.getCobolTime;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLTE;
import static com.quipoz.COBOLFramework.COBOLFunctions.isNE;
import static com.quipoz.COBOLFramework.COBOLFunctions.mult;
import static com.quipoz.COBOLFramework.COBOLFunctions.sub;

import java.util.List;

import com.csc.diary.recordstructures.Dryoutrec;
import com.csc.diary.recordstructures.Dryrptrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.fsu.general.procedures.Datcon3;
import com.csc.fsu.general.procedures.Xcvrt;
import com.csc.fsu.general.recordstructures.Conlinkrec;
import com.csc.fsu.general.recordstructures.Datcon3rec;
import com.csc.fsu.general.recordstructures.Dryprclnk;
import com.csc.fsu.printing.dataaccess.PtrnTableDAM;
import com.csc.life.contractservicing.dataaccess.ChdrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.CovrmjaTableDAM;
import com.csc.life.contractservicing.dataaccess.UtrnrevTableDAM;
import com.csc.life.diary.recordstructures.R5107rec;
import com.csc.life.interestbearing.dataaccess.HitrTableDAM;
import com.csc.life.interestbearing.dataaccess.HitraloTableDAM;
import com.csc.life.interestbearing.dataaccess.HitrrevTableDAM;
import com.csc.life.interestbearing.dataaccess.HitsTableDAM;
import com.csc.life.newbusiness.dataaccess.ChdrlifTableDAM;
import com.csc.life.newbusiness.dataaccess.PayrTableDAM;
import com.csc.life.productdefinition.dataaccess.CovrTableDAM;
import com.csc.life.productdefinition.tablestructures.T5645rec;
import com.csc.life.productdefinition.tablestructures.T5679rec;
import com.csc.life.productdefinition.tablestructures.T5687rec;
import com.csc.life.productdefinition.tablestructures.T5688rec;
import com.csc.life.regularprocessing.recordstructures.Ovrduerec;
import com.csc.life.terminationclaims.tablestructures.T6597rec;
import com.csc.life.unitlinkedprocessing.dataaccess.CovruddTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UlnkrnlTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnaloTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrnuddTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.UtrsTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrstTableDAM;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrsttrnTableDAM;
import com.csc.life.unitlinkedprocessing.procedures.Ufprice;
import com.csc.life.unitlinkedprocessing.recordstructures.Ufpricerec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5515rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5519rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5539rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T5540rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6646rec;
import com.csc.life.unitlinkedprocessing.tablestructures.T6647rec;
import com.csc.smart.dataaccess.ItdmTableDAM;
import com.csc.smart.dataaccess.ItemTableDAM;
import com.csc.smart.recordstructures.Batcuprec;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.SMARTAppVars;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.dataaccess.dao.ItemDAO;
import com.csc.smart400framework.dataaccess.model.Itempf;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.COBOLFramework.job.JobInfo;
import com.quipoz.COBOLFramework.util.StringUtil;
import com.quipoz.framework.datatype.BinaryData;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.IntegerData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

/*
  *(c) Copyright Continuum Corporation Ltd.  1986....1995.
      *    All rights reserved.  Continuum Confidential.
      *
      *REMARKS .
      *
      *   This is the coverage debt processing routine.
      *
      ***********************************************************************
      *                                                                     *
      * ......... New Version of the Amendment History.                     *
      *                                                                     *
      ***********************************************************************
      *           AMENDMENT  HISTORY                                        *
      ***********************************************************************
      * DATE.... VSN/MOD  WORK UNIT    BY....                               *
      *                                                                     *
      * 04/03/02  01/01   DRYAPL       Jacco Landskroon                     *
      *           Initial Version.                                          *
      *                                                                     *
      * 15/02/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
      * LIF2.1    Unit statement Enhancement.                               *
      * INTBR     Life/Asia interest bearing fund Enhancement.              *
      *           If process is unsuccesful, do not update CHDR & PTRN.     *
      *                                                                     *
      * 21/11/08  01/01   ZD0009       Kristin Mcleish                      *
      *           Unlimited Funds.                                          *
      *           Use ULKD file instead of ULNK.                            *
      *           Change WSAA-UTRS-ARRAY to allow storage of 999 funds      *
      *           instead of only 50. Only load UTRS with non-zero          *
      *           balances.                                                 *
      *           Increase field sizes as per D96NUM in B5104.              *
      *           Some changes required to ensure interest bearing fund     *
      *           processing is same as per INTBR changes in B5104.         *
      *                                                                     *
      *                                                                     *
      **DD/MM/YY*************************************************************
      * 
 * */

public class Dry5104 extends Maind {

	private ZonedDecimalData wsaaSub = new ZonedDecimalData(2, 0).setUnsigned();
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(7).init("DRY5104");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	
	private FixedLengthStringData wsaaChdrValid = new FixedLengthStringData(1);
	private Validator validContract = new Validator(wsaaChdrValid, "Y");
	private Validator invalidContract = new Validator(wsaaChdrValid, "N");

	private FixedLengthStringData wsaaComponLevelAccounted = new FixedLengthStringData(1);
	private Validator componLevelAccounted = new Validator(wsaaComponLevelAccounted, "Y");

	private FixedLengthStringData wsaaChdrLapsed = new FixedLengthStringData(1).init("N");
	private Validator chdrLapsed = new Validator(wsaaChdrLapsed, "Y");

	private FixedLengthStringData wsaaChdrWritten = new FixedLengthStringData(1).init("N");
	private Validator chdrWritten = new Validator(wsaaChdrWritten, "Y");

	private FixedLengthStringData wsaaValidPstatcode = new FixedLengthStringData(1).init("N");
	private Validator validPstatcode = new Validator(wsaaValidPstatcode, "Y");

	private FixedLengthStringData wsaaEndOfCovr = new FixedLengthStringData(1).init("N");
	private Validator endOfCovr = new Validator(wsaaEndOfCovr, "Y");

	private FixedLengthStringData wsaaValidStatuz = new FixedLengthStringData(1).init("N");
	private Validator validStatuz = new Validator(wsaaValidStatuz, "Y");


	private FixedLengthStringData wsaaPtrn = new FixedLengthStringData(1).init("N");
	private Validator ptrnWritten = new Validator(wsaaPtrn, "Y");

	private FixedLengthStringData wsaaChdrUpdated = new FixedLengthStringData(1).init("N");
	private Validator chdrUpdated = new Validator(wsaaChdrUpdated, "Y");

	private ZonedDecimalData wsaaDate = new ZonedDecimalData(6);

	private PackedDecimalData wsaaTranno = new PackedDecimalData(5);

	private PackedDecimalData wsaaValAcumUnitsP = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaValInitUnitsP = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaValAcumUnitsN = new PackedDecimalData(17, 5);
	private PackedDecimalData wsaaValInitUnitsN = new PackedDecimalData(16, 5);

	private FixedLengthStringData wsaaInsuffUnitsMeth = new FixedLengthStringData(1);
	
	private PackedDecimalData wsaaTotCovrpuInstprem = new PackedDecimalData(15, 2);

	private PackedDecimalData wsaaOccdate = new PackedDecimalData(8);

	private PackedDecimalData wsaaBalanceCheck = new PackedDecimalData(15, 2);

	private PackedDecimalData wsaaAloTot = new PackedDecimalData(15, 2);

	private PackedDecimalData wsaaWithinRange = new PackedDecimalData(4).init(ZERO);

	private PackedDecimalData wsaaOrigCovrDebt = new PackedDecimalData(15, 2);

	private FixedLengthStringData wsaaCovrmjaDataKey = new FixedLengthStringData(64);

	private FixedLengthStringData wsaaCovrmjaStatuz = new FixedLengthStringData(4);

	private PackedDecimalData wsaaSurrAmount = new PackedDecimalData(13, 5);

	private PackedDecimalData wsaaCurrfrom = new PackedDecimalData(8, 0);
	
	private PackedDecimalData wsaaRiskCessTerm = new PackedDecimalData(3);

	private PackedDecimalData wsaaInitUnitDiscFact = new PackedDecimalData(5, 5);
	private PackedDecimalData wsaaFundValue = new PackedDecimalData(16, 5);
	private PackedDecimalData wsaaSurrPercent = new PackedDecimalData(14, 11);

	private PackedDecimalData wsaaNewCovrDebt = new PackedDecimalData(17, 2).init(0);
	
	private PackedDecimalData wsaaLapseTranno = new PackedDecimalData(5, 0);
	
	private PackedDecimalData ix = new PackedDecimalData(5);
	
	/* WSAA-UTRS-ARRAY */
	private FixedLengthStringData[] wsaaUtrsRec = FLSInittedArray (50, 58);
	private BinaryData[] wsaaUtrsRrn = BDArrayPartOfArrayStructure(9, 0, wsaaUtrsRec, 0, 0);
	private FixedLengthStringData[] wsaaUtrsUnitVirtualFund = FLSDArrayPartOfArrayStructure(4, wsaaUtrsRec, 4);
	private FixedLengthStringData[] wsaaUtrsUnitType = FLSDArrayPartOfArrayStructure(1, wsaaUtrsRec, 8);
	private FixedLengthStringData[] wsaaUtrsFundCurr = FLSDArrayPartOfArrayStructure(4, wsaaUtrsRec, 9);
	private PackedDecimalData[] wsaaUtrsCovFundAmtP = PDArrayPartOfArrayStructure(16, 5, wsaaUtrsRec, 13);
	private PackedDecimalData[] wsaaUtrsCovFundAmtN = PDArrayPartOfArrayStructure(16, 5, wsaaUtrsRec, 22);
	private PackedDecimalData[] wsaaUtrsCurrentUnitBal = PDArrayPartOfArrayStructure(16, 5, wsaaUtrsRec, 31);
	private PackedDecimalData[] wsaaUtrsCurrentDunitBal = PDArrayPartOfArrayStructure(16, 5, wsaaUtrsRec, 40);
	private PackedDecimalData[] wsaaUtrsSurrAmt = PDArrayPartOfArrayStructure(17, 2, wsaaUtrsRec, 49);
	
	private int t5645Ix;
	
	private IntegerData iu = new IntegerData();

	private FixedLengthStringData wsaaT6647Key = new FixedLengthStringData(8);
	private FixedLengthStringData wsaaT6647Trancode = new FixedLengthStringData(4).isAPartOf(wsaaT6647Key, 0);
	private FixedLengthStringData wsaaT6647Cnttype = new FixedLengthStringData(3).isAPartOf(wsaaT6647Key, 4);
	private FixedLengthStringData filler = new FixedLengthStringData(1).isAPartOf(wsaaT6647Key, 7).init(SPACES);

	private FixedLengthStringData wsaaAdditionalKey = new FixedLengthStringData(30);
	private FixedLengthStringData filler1 = new FixedLengthStringData(30).isAPartOf(wsaaAdditionalKey, 0);

	/* WSAA-T5645 */
	private FixedLengthStringData[] wsaaStoredT5645 = FLSInittedArray (60, 21);
	private ZonedDecimalData[] wsaaT5645Cnttot = ZDArrayPartOfArrayStructure(2, 0, wsaaStoredT5645, 0);
	private FixedLengthStringData[] wsaaT5645Glmap = FLSDArrayPartOfArrayStructure(14, wsaaStoredT5645, 2);
	private FixedLengthStringData[] wsaaT5645Sacscode = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 16);
	private FixedLengthStringData[] wsaaT5645Sacstype = FLSDArrayPartOfArrayStructure(2, wsaaStoredT5645, 18);
	private FixedLengthStringData[] wsaaT5645Sign = FLSDArrayPartOfArrayStructure(1, wsaaStoredT5645, 20);
	private static final int wsaaT5688Size = 100;
	private static final int wsaaT5645Size = 60;

	private FixedLengthStringData formats = new FixedLengthStringData(200);
	private FixedLengthStringData chdrlifrec = new FixedLengthStringData(10).isAPartOf(formats, 0).init("CHDRLIFREC");
	private FixedLengthStringData chdrmjarec = new FixedLengthStringData(10).isAPartOf(formats, 10).init("CHDRMJAREC");
	private FixedLengthStringData covrrec = new FixedLengthStringData(10).isAPartOf(formats, 20).init("COVRREC");
	private FixedLengthStringData covrmjarec = new FixedLengthStringData(10).isAPartOf(formats, 30).init("COVRMJAREC");
	private FixedLengthStringData covruddrec = new FixedLengthStringData(10).isAPartOf(formats, 40).init("COVRUDDREC");
	private FixedLengthStringData itdmrec = new FixedLengthStringData(10).isAPartOf(formats, 50).init("ITEMREC");
	private FixedLengthStringData itemrec = new FixedLengthStringData(10).isAPartOf(formats, 60).init("ITEMREC");
	private FixedLengthStringData payrrec = new FixedLengthStringData(10).isAPartOf(formats, 70).init("PAYRREC");
	private FixedLengthStringData ptrnrec = new FixedLengthStringData(10).isAPartOf(formats, 80).init("PTRNREC");
	private FixedLengthStringData utrnalorec = new FixedLengthStringData(10).isAPartOf(formats, 90).init("UTRNALOREC");
	private FixedLengthStringData utrnrevrec = new FixedLengthStringData(10).isAPartOf(formats, 100).init("UTRNREVREC");
	private FixedLengthStringData utrnuddrec = new FixedLengthStringData(10).isAPartOf(formats, 110).init("UTRNUDDREC");
	private FixedLengthStringData utrsrec = new FixedLengthStringData(10).isAPartOf(formats, 120).init("UTRSREC");
	private FixedLengthStringData zrstrec = new FixedLengthStringData(10).isAPartOf(formats, 130).init("ZRSTREC");
	private FixedLengthStringData zrsttrnrec = new FixedLengthStringData(10).isAPartOf(formats, 140).init("ZRSTTRNREC");
	private FixedLengthStringData hitsrec = new FixedLengthStringData(10).isAPartOf(formats, 150).init("HITSREC");
	private FixedLengthStringData hitrrec = new FixedLengthStringData(10).isAPartOf(formats, 160).init("HITRREC");
	private FixedLengthStringData hitralorec = new FixedLengthStringData(10).isAPartOf(formats, 170).init("HITRALOREC");
	private FixedLengthStringData hitrrevrec = new FixedLengthStringData(10).isAPartOf(formats, 180).init("HITRREVREC");
	private FixedLengthStringData ulnkrnlrec = new FixedLengthStringData(10).isAPartOf(formats, 190).init("ULNKRNLREC");

		/* TABLES */
	private static final String t5515 = "T5515";
	private static final String t5519 = "T5519";
	private static final String t5539 = "T5539";
	private static final String t5540 = "T5540";
	private static final String t5645 = "T5645";
	private static final String t5679 = "T5679";
	private static final String t5687 = "T5687";
	private static final String t5688 = "T5688";
	private static final String t6597 = "T6597";
	private static final String t6646 = "T6646";
	private static final String t6647 = "T6647";

		/* ERRORS */
	private static final String g027 = "G027";
	private static final String h791 = "H791";
	
	/* CONTROL-TOTALS */
	private static final int ct01 = 1;
	private static final int ct02 = 2;
	private static final int ct03 = 3;
	private static final int ct04 = 4;
	private static final int ct05 = 5;
	private static final int ct06 = 6;
	private static final int ct07 = 7;
	private static final int ct08 = 8;
	private static final int ct09 = 9;
	private static final int ct10 = 10;
	private static final int ct11 = 11;
	private static final int ct12 = 12;
	private static final int ct13 = 13;
	private static final int ct14 = 14;
	private static final int ct15 = 15;
	private static final int ct16 = 16;
	private static final int ct17 = 17;
	private static final int ct18 = 18;
	private static final int ct19 = 19;
	private static final int ct20 = 20;
	private static final int ct21 = 21;
	private static final int ct22 = 22;
	private static final int ct23 = 23;
	private static final int ct24 = 24;
	private static final int ct25 = 25;
	private static final int ct26 = 26;
	private static final int ct27 = 27;

	/* WSAA-T5540-ARRAY */
	private FixedLengthStringData[] wsaaT5540Rec = FLSInittedArray (50, 17);
	private FixedLengthStringData[] wsaaT5540Key = FLSDArrayPartOfArrayStructure(4, wsaaT5540Rec, 0);
	private FixedLengthStringData[] wsaaT5540Crtable = FLSDArrayPartOfArrayStructure(4, wsaaT5540Key, 0, HIVALUE);
	private FixedLengthStringData[] wsaaT5540Data = FLSDArrayPartOfArrayStructure(13, wsaaT5540Rec, 4);
	private FixedLengthStringData[] wsaaT5540UnitCancInit = FLSDArrayPartOfArrayStructure(1, wsaaT5540Data, 0);
	private FixedLengthStringData[] wsaaT5540WholeIuDiscFact = FLSDArrayPartOfArrayStructure(4, wsaaT5540Data, 1);
	private FixedLengthStringData[] wsaaT5540IuDiscBasis = FLSDArrayPartOfArrayStructure(4, wsaaT5540Data, 5);
	private FixedLengthStringData[] wsaaT5540IuDiscFact = FLSDArrayPartOfArrayStructure(4, wsaaT5540Data, 9);
	private FixedLengthStringData wsaaT5540StoredItem = new FixedLengthStringData(8).init(SPACES);
	private static final int wsaaT5515Size = 50;

	private Dryoutrec dryoutrec = new Dryoutrec(); 
	private Batcuprec batcuprec = new Batcuprec();
//	private Drylogrec drylogrec = new Drylogrec();
//	private Drycntrec drycntrec = new Drycntrec();
	private Dryrptrec dryrptrec = new Dryrptrec();
	private R5107rec r5107rec = new R5107rec();
	private Varcom varcom = new Varcom();
	private Datcon3rec datcon3rec = new Datcon3rec();
	private Ufpricerec ufpricerec = new Ufpricerec();
	private Conlinkrec conlinkrec = new Conlinkrec();
	private Ovrduerec ovrduerec = new Ovrduerec();
	private ChdrlifTableDAM chdrlifIO = new ChdrlifTableDAM();
	private ChdrmjaTableDAM chdrmjaIO = new ChdrmjaTableDAM();
	private CovrTableDAM covrIO = new CovrTableDAM();
	private CovrmjaTableDAM covrmjaIO = new CovrmjaTableDAM();
	private CovruddTableDAM covruddIO = new CovruddTableDAM();
	private ItdmTableDAM itdmIO = new ItdmTableDAM();
	private ItemTableDAM itemIO = new ItemTableDAM();
	private PayrTableDAM payrIO = new PayrTableDAM();
	private PtrnTableDAM ptrnIO = new PtrnTableDAM();
	private UlnkrnlTableDAM ulnkrnlIO = new UlnkrnlTableDAM();
	private UtrnaloTableDAM utrnaloIO = new UtrnaloTableDAM();
	private UtrnrevTableDAM utrnrevIO = new UtrnrevTableDAM();
	private UtrnuddTableDAM utrnuddIO = new UtrnuddTableDAM();
	private UtrsTableDAM utrsIO = new UtrsTableDAM();
	private ZrstTableDAM zrstIO = new ZrstTableDAM();
	private ZrsttrnTableDAM zrsttrnIO = new ZrsttrnTableDAM();
	private HitsTableDAM hitsIO = new HitsTableDAM();
	private HitrTableDAM hitrIO = new HitrTableDAM();
	private HitraloTableDAM hitraloIO = new HitraloTableDAM();
	private HitrrevTableDAM hitrrevIO = new HitrrevTableDAM();
	private T5515rec t5515rec = new T5515rec();
	private T5519rec t5519rec = new T5519rec();
	private T5539rec t5539rec = new T5539rec();
	private T5540rec t5540rec = new T5540rec();
	private IntegerData t5540Ix = new IntegerData();
	private T5645rec t5645rec = new T5645rec();
	private T5679rec t5679rec = new T5679rec();
	private T5687rec t5687rec = new T5687rec();
	private T5688rec t5688rec = new T5688rec();
	private T6597rec t6597rec = new T6597rec();
	private T6646rec t6646rec = new T6646rec();
	private T6647rec t6647rec = new T6647rec();
	private Dryprclnk dryprclnk = new Dryprclnk();
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	
	private PackedDecimalData wsaaTotalSurrAmt = new PackedDecimalData(18, 5).init(0);
	private FixedLengthStringData wsaaPrevChdrnum = new FixedLengthStringData(8).init(SPACES);
	private String username;
	
	//ILPI-254
	private ItemDAO itemDAO = getApplicationContext().getBean("itemDao", ItemDAO.class);
	
	/**
	 * Contains all possible labels used by goTo action.
	 */
	private enum GotoLabel implements GOTOInterface {
			DEFAULT,
			exit190,
			exit390,
			exit490,
			exit590,
			exit690,
			balanceCheck730,
			updates740,
			noUnitsSold760,
			exit790,
			fixedTerm903,
			wholeOfLife905,
			exit909,
			
			exit929,
			
			next1180,
			exit1190,
			
			exit1390,
			
			initials1420,
			buyUnits1430,
			exit1490,
			
			fixedTerm1630,
			wholeOfLife1640,
			exit1690,
			
			exit1990,
			
			initials2320,
			exit2390,
			
			exit2059,
			
			exit2159,
			
			nextr2181,
			
			exit2194,
			
			
			writeNewContractHeader2196,
			writeRecord2196,
			exit2197,
			
			checkStatuz2198,
			exit2198,
			
			rewrite2199,
			writeRecord2199,
			
			a190Exit
			
			
	}

	public Dry5104() {
		super();
	}

	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}

	/**
	* The mainline method is the default entry point to the class
	*/

	public void mainline(Object... parmArray) {			
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-97
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			startProcessing100();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
			
	private void startProcessing100(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start110();
				case exit190: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		} 
		
	}

	private void start110(){
		username = ((SMARTAppVars) SMARTAppVars.getInstance()).getJobInfo().retrieveJobInfo(JobInfo.USER);
		wsaaPrevChdrnum.set(SPACES);
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		wsaaAdditionalKey.set(drypDryprcRecInner.drypAdditionalKey);
		/*Start the coding of the processing subroutine here.*/

		initialise200();
		if(isNE(drypDryprcRecInner.drypEntity, wsaaPrevChdrnum)){
			validateChdr300();
			if(invalidContract.isTrue()){
				drypDryprcRecInner.processUnsuccesful.setTrue();
				drycntrec.contotNumber.set(ct01);
				drycntrec.contotValue.set(1);
				d000ControlTotals();
//				chdrlifIO.setFunction(varcom.rewrt);
//				SmartFileCode.execute(appVars, chdrlifIO);
//	
//				if(!isEQ(chdrlifIO.getStatuz(),varcom.oK)){
//					drylogrec.statuz.set(chdrlifIO.getStatuz());
//					drylogrec.params.set(chdrlifIO.getParams());
//					drylogrec.dryDatabaseError.setTrue();
//					a000FatalError();
//				}
				goTo(GotoLabel.exit190);
			}
		}

		/* Fill COVR key and read to find COVR records with debt.*/

		covruddIO.setParams(SPACES);
		covruddIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		covruddIO.setChdrnum(drypDryprcRecInner.drypEntity);
		covruddIO.setLife(SPACES);
		covruddIO.setCoverage(SPACES);
		covruddIO.setRider(SPACES);
	
		covruddIO.planSuffix.set(ZERO);
		
		covruddIO.setFunction(varcom.begn);
		covruddIO.format.set(covruddrec);
		covruddIO.statuz.set(varcom.oK);

		while (!(isEQ(covruddIO.getStatuz(), varcom.endp))) {					
				processCovr500();	
		}		
		
		if(drypDryprcRecInner.processUnsuccesful.isTrue()){		
			chdrlifIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, chdrlifIO);
			
			if(!isEQ(chdrlifIO.getStatuz(),varcom.oK)){
				drylogrec.statuz.set(chdrlifIO.getStatuz());
				drylogrec.params.set(chdrlifIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			goTo(GotoLabel.exit190);
		}
		
//		updateChdr2400();
//		writePtrn2500();
		fillParams3000();
	}


	protected void initialise200(){		
		initialise210();
	}

	protected void initialise210(){
		
		wsaaDate.set(getCobolDate());
		
		/*Table T5679 : Statii required by this transaction.*/
		
		itemIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5679);
		itemIO.setItemitem(drypDryprcRecInner.drypBatctrcde);
		itemIO.setFormat(itemrec);
		itemIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, itemIO);
		if(!isEQ(itemIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5679rec.t5679Rec.set(itemIO.getGenarea());
		
		/* Table T5645 : Accounting rules. */
	
		//ILPI-254 starts
//		itemIO.setStatuz(varcom.oK);
//		itemIO.setItempfx("IT");
//		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
//		itemIO.setItemtabl(t5645);
//		itemIO.setItemitem(wsaaProg);
//		itemIO.setItemseq(SPACES);
//		itemIO.setFunction(varcom.begn);
//		wsaaT5645Sub=1;
		t5645Ix=1;
//		while ( !(isEQ(itemIO.getStatuz(), varcom.endp))) {				
// 			loadT5645400();		
//		}		
		List<Itempf> t5645List = itemDAO.loadSmartTable("IT", drypDryprcRecInner.drypCompany.toString().trim(), "T5645").get(wsaaProg.toString().trim());
		for(Itempf itempf : t5645List){
			t5645rec.t5645Rec.set(StringUtil.rawToString(itempf.getGenarea()));
			for(int wsaaT5645Sub =1; wsaaT5645Sub <= 15; wsaaT5645Sub++) {		
				wsaaT5645Cnttot[t5645Ix].set(t5645rec.cnttot[wsaaT5645Sub]);
				wsaaT5645Glmap[t5645Ix].set(t5645rec.glmap[wsaaT5645Sub]);
				wsaaT5645Sacscode[t5645Ix].set(t5645rec.sacscode[wsaaT5645Sub]);
				wsaaT5645Sacstype[t5645Ix].set(t5645rec.sacstype[wsaaT5645Sub]);
				wsaaT5645Sign[t5645Ix].set(t5645rec.sign[wsaaT5645Sub]);
				t5645Ix++;
			}
		}
		
	}

	protected void validateChdr300(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					readr310();
				case exit390: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void readr310(){

		invalidContract.setTrue();
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readr);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		
		if(!isEQ(chdrlifIO.getStatuz(),varcom.oK)
				||!isEQ(chdrlifIO.getValidflag(),1)){
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

//		compute(wsaaTranno, 0).set(add(1, chdrlifIO.getTranno()));


		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
				|| validContract.isTrue()); wsaaSub.add(1)){						
			if(isEQ(t5679rec.cnRiskStat[wsaaSub.toInt()],chdrlifIO.getStatcode())){
				validContract.setTrue();
			}	
		}						
		if(invalidContract.isTrue()){
			goTo(GotoLabel.exit390);
		}

		invalidContract.setTrue();

		for (wsaaSub.set(1); !(isGT(wsaaSub, 12)
				|| validContract.isTrue()); wsaaSub.add(1)){						
			if(isEQ(t5679rec.cnPremStat[wsaaSub.toInt()],chdrlifIO.getPstatcode())){
				validContract.setTrue();
			}	
		}
		goTo(GotoLabel.exit390);
	}

//	private void loadT5645400(){
//		GotoLabel nextMethod = GotoLabel.DEFAULT;
//		while (true) {
//			try {
//				switch (nextMethod) {
//				case DEFAULT: 
//					start410();
//				case exit490: 
//				}
//				break;
//			}
//			catch (GOTOException e){
//				nextMethod = (GotoLabel) e.getNextMethod();
//			}
//		}
//	}
//	
//	protected void start410(){
//		SmartFileCode.execute(appVars, itemIO);
//
//		if(!isEQ(itemIO.getStatuz(),varcom.oK)
//				&&!isEQ(itemIO.getStatuz(),varcom.endp)){
//			drylogrec.params.set(itemIO.getParams());
//			drylogrec.statuz.set(itemIO.getStatuz());
//			drylogrec.dryDatabaseError.setTrue();
//			a000FatalError();
//		}
//
//		if(!isEQ(itemIO.getItemcoy(),drypDryprcRecInner.drypCompany)
//				||!isEQ(itemIO.getItemtabl(),t5645)
//				||!isEQ(itemIO.getItemitem(),wsaaProg)
//				||isEQ(itemIO.getStatuz(),varcom.endp)){
//			
//			itemIO.setStatuz(varcom.endp);
//			goTo(GotoLabel.exit490);
//		}
//
//		t5645rec.t5645Rec.set(itemIO.getGenarea());
//		wsaaT5645Sub=1;
//
//		if(wsaaT5645Sub > wsaaT5645Size){
//			drylogrec.statuz.set(h791);
//			drylogrec.params.set(t5645);
//			drylogrec.drySystemError.setTrue(); 
//			a000FatalError();
//		}
//
//		while (wsaaT5645Sub <= 15) {		
//			wsaaT5645Cnttot[t5645Ix].set(t5645rec.cnttot[wsaaT5645Sub]);
//			wsaaT5645Glmap[t5645Ix].set(t5645rec.glmap[wsaaT5645Sub]);
//			wsaaT5645Sacscode[t5645Ix].set(t5645rec.sacscode[wsaaT5645Sub]);
//			wsaaT5645Sacstype[t5645Ix].set(t5645rec.sacstype[wsaaT5645Sub]);
//			wsaaT5645Sign[t5645Ix].set(t5645rec.sign[wsaaT5645Sub]);
//			wsaaT5645Sub++;
//			t5645Ix++;
//		}
//		itemIO.setFunction(varcom.nextr);
//	}


	private void processCovr500(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					start510();
				case exit590: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void start510(){
		SmartFileCode.execute(appVars, covruddIO);
	
		if(!isEQ(covruddIO.getStatuz(),varcom.oK)
				&&!isEQ(covruddIO.statuz,varcom.endp)){
			drylogrec.params.set(covruddIO.getParams());
			drylogrec.statuz.set(covruddIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		
		if(isEQ(covruddIO.getStatuz(),varcom.endp)
				||!isEQ(covruddIO.chdrcoy,drypDryprcRecInner.drypCompany)
				||!isEQ(covruddIO.chdrnum,drypDryprcRecInner.drypEntity)){
			covruddIO.statuz.set(varcom.endp);
			goTo(GotoLabel.exit590);
		}
		checkDebtCovr600();
		covruddIO.setFunction(varcom.nextr);
		
	}
	
	protected void nextr580(){
		covruddIO.function.set(varcom.nextr);
	}


	private void checkDebtCovr600(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					check610();
				case exit690: 
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	/*
	 *  *  If a UTRN record exists where the feedback indicator
	      *  has not been set to 'Y' ignore this record, add 1 to
	      *  CT21 and read the next input record.
	 */
	protected void check610(){
		
		utrnuddIO.setParams(SPACE);
		utrnuddIO.chdrcoy.set(covruddIO.getChdrcoy());
		utrnuddIO.chdrnum.set(covruddIO.getChdrnum());
		utrnuddIO.life.set(covruddIO.getLife());
	
		utrnuddIO.coverage.set(covruddIO.getCoverage());
		utrnuddIO.rider.set(covruddIO.getRider());
		utrnuddIO.planSuffix.set(covruddIO.getPlanSuffix());
		utrnuddIO.function.set(varcom.begn);
		utrnuddIO.format.set(utrnuddrec);
		SmartFileCode.execute(appVars, utrnuddIO);
	
		if(!isEQ(utrnuddIO.getStatuz(),varcom.endp)
				&&!isEQ(utrnuddIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(utrnuddIO.getStatuz());
			drylogrec.params.set(utrnuddIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}


		if(isEQ(utrnuddIO.getStatuz(),varcom.oK)
				&&isEQ(utrnuddIO.getChdrcoy(),covruddIO.getChdrcoy())
				&&isEQ(utrnuddIO.getChdrnum(),covruddIO.getChdrnum())
				&&isEQ(utrnuddIO.getLife(),covruddIO.getLife())
				&&isEQ(utrnuddIO.getCoverage(),covruddIO.getCoverage())
				&&isEQ(utrnuddIO.getRider(),covruddIO.getRider())
				&&isEQ(utrnuddIO.getPlanSuffix(),covruddIO.getPlanSuffix())){
			drycntrec.contotNumber.set(ct21);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			drypDryprcRecInner.processUnsuccesful.setTrue();
			goTo(GotoLabel.exit690);
		}
		/*
		 *  If we have not found any UTRNs, attempt to read an
		      ***ITRN where the feedback indicator has not been set.           
		      ***If a ITRN record exists, ignore this record, add 1 to         
		      *  HITR where the feedback indicator has not been set.           
		      *  If a HITR record exists, ignore this record, add 1 to         
		      *  CT21 and read the next input record.
	
		      *    MOVE COVRUDD-CHDRCOY        TO ITRNALO-CHDRCOY.             
		      *    MOVE COVRUDD-CHDRNUM        TO ITRNALO-CHDRNUM.             
		      *    MOVE COVRUDD-LIFE           TO ITRNALO-LIFE.                
		      *    MOVE COVRUDD-COVERAGE       TO ITRNALO-COVERAGE.            
		      *    MOVE COVRUDD-RIDER          TO ITRNALO-RIDER.               
		      *    MOVE COVRUDD-PLAN-SUFFIX    TO ITRNALO-PLAN-SUFFIX.         
		      *    MOVE SPACES                 TO ITRNALO-UNIT-VIRTUAL-FUND    
		      *                                   ITRNALO-FUNDTYPE.            
		      *    MOVE ZERO                 TO ITRNALO-TRANNO               
		      *                                   ITRNALO-MONIES-DATE          
		      *                                   ITRNALO-PROC-SEQ-NO.         
		      *                                                                
		      *    MOVE ITRNALOREC             TO ITRNALO-FORMAT.              
		      *    MOVE BEGN                   TO ITRNALO-FUNCTION.            
		      *                                                                
		      *    CALL 'ITRNALOIO'         USING ITRNALO-PARAMS.              
		      *               
		 * */
		hitraloIO.setChdrcoy(covruddIO.getChdrcoy());
		hitraloIO.setChdrnum(covruddIO.getChdrnum());
		hitraloIO.setLife(covruddIO.getLife());
		hitraloIO.setCoverage(covruddIO.getCoverage());
		hitraloIO.setRider(covruddIO.getRider());
		hitraloIO.setPlanSuffix(covruddIO.getPlanSuffix());
		hitraloIO.setZintbfnd(SPACES);
		hitraloIO.setZrectyp(SPACES);
		hitraloIO.setTranno(ZERO);
		hitraloIO.setEffdate(ZERO);
		hitraloIO.setProcSeqNo(ZERO);
		hitraloIO.setFormat(hitralorec);
		hitraloIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, hitraloIO);

		if(!isEQ(hitraloIO.getStatuz(),varcom.oK)
				&&!isEQ(hitraloIO.getStatuz(),varcom.endp)){
			drylogrec.statuz.set(hitraloIO.getStatuz());
			drylogrec.params.set(hitraloIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
		
			a000FatalError();
		}

		if(isEQ(hitraloIO.getStatuz(),varcom.oK)
				&&isEQ(hitraloIO.getChdrcoy(),covruddIO.getChdrcoy())
				&&isEQ(hitraloIO.getChdrnum(),covruddIO.getChdrnum())
				&&isEQ(hitraloIO.getCoverage(),covruddIO.getCoverage())
				&&isEQ(hitraloIO.getLife(),covruddIO.getLife())
				&&isEQ(hitraloIO.getRider(),covruddIO.getRider())
				&&isEQ(hitraloIO.getPlanSuffix(),covruddIO.getPlanSuffix())){
			drycntrec.contotNumber.set(ct27);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
		
			drypDryprcRecInner.processUnsuccesful.setTrue();
			goTo(GotoLabel.exit690);
		}
		
		processDebt700();
	}


	private void processDebt700(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					update710();
				case balanceCheck730:
					balanceCheck730();
				case updates740:
					updates740();
				case noUnitsSold760:
					noUnitsSold760();
				case exit790:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	
	protected void update710(){	
		if (isNE(wsaaPrevChdrnum, drypDryprcRecInner.drypEntity)) {
			readhChdrlif();
			readTables800();
		}
		readT5540();
		wsaaOrigCovrDebt.set(covruddIO.getCoverageDebt());

		/*
		 *
	      * The next section reads through the UTRSs for this coverage
	      * and loads the values found into a working storage table that
	      * forms the centre point for all further processing. At the
	      * same time the total value of init and accum units is calc'ed   .
	      *
	      * */
		loadUtrsCalcFundVals1000();

		/*
		 *
	      * Now we have loaded the UTRS records into the w/s tables
	      * load the ITRS records into the same table starting where the
	      * UTRS left off. This means that all the ITRS & UTRS balances
	      * will be held in the same table & it will be easier to
	      * proportion the debt via 1 table.
	      *
	      *    PERFORM 1200-LOAD-ITRS-FUND-VALS.  
	      */
		loadHitsCalcFundVals1200();

		/*
		 OK.... Now we know three very important things:
	      *
	      * a) The value of the debt to be settled
	      * b) The total value of all positive fund holdings
	      * c) The value of each fund holding
	      *
	      * Firstly check whether we are selling units to settle the
	      * coverage debt or whether (and unusally so) we are buying
	      * units to clear the coverage debt.
	      */

		if (isLT(covruddIO.getCoverageDebt(), ZERO)) {
			buyUnits1400();
			goTo(GotoLabel.balanceCheck730);
		}

		/*
		 * We can now calculate the value of the units which must be
	      * surrendered from each fund to satisfy the coverage debt.
	      * We start with accumulation units and if allowed by T5540,
	      * and, if the coverage debt is still not zero, finish with
	      * initial units. Note that we only sell units from positive
	      * fund balances as to do so from negitive fund values would
	      * constitute a fund switch.
		 */
		if(isGT(covruddIO.coverageDebt,ZERO)){
			sellUnits2300();
			goTo(GotoLabel.balanceCheck730);
		}
	}
	
	private void readhChdrlif(){
		chdrlifIO.setParams(SPACES);
		chdrlifIO.setChdrcoy(drypDryprcRecInner.drypCompany);
		chdrlifIO.setChdrnum(drypDryprcRecInner.drypEntity);
		chdrlifIO.setFunction(varcom.readh);
		chdrlifIO.setFormat(chdrlifrec);
		SmartFileCode.execute(appVars, chdrlifIO);
		
		if(!isEQ(chdrlifIO.getStatuz(),varcom.oK)
				||!isEQ(chdrlifIO.getValidflag(),1)){
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		compute(wsaaTranno, 0).set(add(1, chdrlifIO.getTranno()));
	}
	
	protected void balanceCheck730(){
		/*
		  *
	      * Were we able to settle any debt? If not, we have no
	      * balancing or updates to do so head off to the non-forfeiture
	      * processing.
	      *
	      */
		if (isEQ(covruddIO.getCoverageDebt(), wsaaOrigCovrDebt)) {
			goTo(GotoLabel.noUnitsSold760);
		}

		/*
		  *
	      * If the coverage debt is not ZERO then the units that could
	      * be sold were not sufficient to completely settle the debt.
	      * Thus we sum up the value of the what can be sold and subtrac   t
	      * it from the original coverage debt to arrive at the new debt   .
	      *
	      * */
		if(!isEQ(covruddIO.getCoverageDebt(),ZERO)){
			wsaaBalanceCheck.set(ZERO);

			for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
				wsaaBalanceCheck.add(wsaaUtrsSurrAmt[iu.toInt()]);
			}
			
			setPrecision(covruddIO.getCoverageDebt(), 3);
			covruddIO.setCoverageDebt(sub(wsaaOrigCovrDebt, wsaaBalanceCheck), true);
			goTo(GotoLabel.updates740);
		}

		/*
		 *
	      * If the debt is zero the loop through the table and perform t   he
	      * neccessary corrections. The emphasis here is to ensure the
	      * coverage debt is zero (if not we'll be doing this again
	      * next time with 0.01 widgits).
	      * */
		wsaaBalanceCheck.set(ZERO);
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			wsaaBalanceCheck.add(wsaaUtrsSurrAmt[iu.toInt()]);
		}
		
		compute(wsaaBalanceCheck, 2).set(sub(wsaaBalanceCheck, wsaaOrigCovrDebt));
		/*    Loop thru each surrender amount, adding 0.01 widgits, until*/
		/*    the balancing amount is zero.*/

		iu.set(1);
		while ( !(isEQ(wsaaBalanceCheck, ZERO))) {
			if (isEQ(wsaaUtrsRrn[iu.toInt()], 0)) {
				iu.set(1);
			}
			if (isNE(wsaaUtrsSurrAmt[iu.toInt()], ZERO)) {
				if (isGT(wsaaBalanceCheck, ZERO)) {
					wsaaBalanceCheck.subtract(0.01);
					wsaaUtrsSurrAmt[iu.toInt()].subtract(0.01);
				}
				else {
					wsaaBalanceCheck.add(0.01);
					wsaaUtrsSurrAmt[iu.toInt()].add(0.01);
				}
			}
			iu.add(1);
		}
	}
	
	/*
	*
      *  Total up the value of the units possible to be
      *  surrendered for the funds per coverage since the
      *  WSAA-ORIG-COVR-DEBT holds the total coverage debt. 
      * */
	protected void updates740(){
		wsaaTotalSurrAmt.set(0);
		/*  It is possible that the first Component(s) read would have  */
		/*  been Lapsed in which case the CHDRLIF Record would have     */
		/*  been rewritten and will now need READHing again.            */
		/*  Also, if this is the case then original increment of the    */
		/*  TRANNO will have been utilised for the Lapse, therefore     */
		/*  increment again.                                            */
		if (chdrWritten.isTrue()
				&& !chdrUpdated.isTrue()) {
			readhChdrlif();
		}

		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			wsaaTotalSurrAmt.add(wsaaUtrsSurrAmt[iu.toInt()]);
		}

		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if (isNE(wsaaUtrsSurrAmt[iu.toInt()], ZERO)) {
				if (isEQ(wsaaUtrsUnitType[iu.toInt()], "A")) {
					utrnaloIO.setUnitSubAccount("ACUM");
					completeAndWriteUtrn1500();
				}
				else {
					if (isEQ(wsaaUtrsUnitType[iu.toInt()], "I")) {
						utrnaloIO.setUnitSubAccount("INIT");
						completeAndWriteUtrn1500();				
					}
					else {
						completeAndWriteHitr1700();
					}
				}
			}
		}

		/*
	       * If the coverage debt has been reduced then rewrite the COVR
	      * record.                                                        act
	      */
		if(isNE(wsaaOrigCovrDebt,covruddIO.getCoverageDebt())){
			rewriteCovr1800();
			if (isNE(wsaaPrevChdrnum, drypDryprcRecInner.drypEntity)
				|| (chdrWritten.isTrue()
				&& !chdrUpdated.isTrue())) {
				updateChdr2400();
				writePtrn2500();
			}
		}

		if(!isEQ(wsaaOrigCovrDebt,covruddIO.getCoverageDebt())){
			zrstIO.setParams(SPACE);
			zrstIO.setChdrcoy(covruddIO.getChdrcoy());
			zrstIO.setChdrnum(covruddIO.getChdrnum());
			zrstIO.setLife(covruddIO.getLife());
			zrstIO.setCoverage(covruddIO.getCoverage());
			zrstIO.setJlife(SPACES);
			zrstIO.setRider(SPACES);
			zrstIO.setFormat(zrstrec);
			zrstIO.setFunction(varcom.begn);
			
			while ( !(isEQ(zrstIO.getStatuz(), varcom.endp))) {
				updateZrstA100();
			}
	
			if (isGT(covruddIO.getCoverageDebt(), ZERO)) {
				createNewZrstB100();
			}
		}
	}
	/* *
      * If we have sold all the units we can and a debt still exists
      * then there are four options: debt, lapse, error and negative
      * units.*/
	protected void noUnitsSold760(){
		if(isGT(covruddIO.coverageDebt,ZERO)){
			processUnpaidDebt1900();
		}
		wsaaPrevChdrnum.set(drypDryprcRecInner.drypEntity);
	}



	private void readTables800(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT: 
					read810();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	protected void read810(){

		wsaaPtrn.set('N');
		wsaaChdrLapsed.set('N');
		wsaaChdrWritten.set('N');
		wsaaChdrUpdated.set('N');
		/* Read table T6647. */
		itdmIO.setParams(SPACES);
		
		
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6647);
		wsaaT6647Trancode.set(drypDryprcRecInner.drypBatctrcde);
		wsaaT6647Cnttype.set(chdrlifIO.getCnttype());
		itdmIO.setItemitem(wsaaT6647Key);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);
		
		SmartFileCode.execute(appVars, itdmIO);
		
		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();

		}
		if(isNE(itdmIO.getItemtabl(),t6647)
				|| isNE(itdmIO.getItemcoy(),drypDryprcRecInner.drypCompany)
				|| isNE(itdmIO.getItemitem(),wsaaT6647Key)
				|| isLT(itdmIO.getItmto(),drypDryprcRecInner.drypRunDate)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t6647rec.t6647Rec.set(itdmIO.getGenarea());
		/*Read table T5688.  */
		
		itdmIO.setParams(SPACES);
		
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5688);
		itdmIO.setItemitem(chdrlifIO.getCnttype());
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);

		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if(!isEQ(itdmIO.getItemtabl(),t5688)
			||!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.drypCompany)
			||!isEQ(itdmIO.getItemitem(),chdrlifIO.getCnttype())
			||isLT(itdmIO.getItmto(),drypDryprcRecInner.drypRunDate)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			
			a000FatalError();
		}

		t5688rec.t5688Rec.set(itdmIO.getGenarea());
		wsaaComponLevelAccounted.set(t5688rec.comlvlacc);
	}
	
	private void readT5540(){
		/*Read table T5540.*/
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5540);
		itdmIO.setItemitem(covruddIO.crtable);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);


		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		
		}
		

		if(!isEQ(itdmIO.getItemtabl(),t5540)
				||!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.drypCompany)
				||!isEQ(itdmIO.getItemitem(),covruddIO.crtable)
				||isEQ(itdmIO.getStatuz(),varcom.endp)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
	
		}
		t5540rec.t5540Rec.set(itdmIO.getGenarea());
		datcon3rec.datcon3Rec.set(SPACE);
		datcon3rec.intDate1.set(covruddIO.crrcd);
		datcon3rec.intDate2.set(covruddIO.riskCessDate);
		datcon3rec.frequency.set(01);
		callProgram(Datcon3.class,datcon3rec.datcon3Rec);

		/*
	   	*Store the integer of the factor in a field to be used
	      *  as the index to T5539.  If the DTC3-FREQ-FACTOR contain
	      *  a decimal value, correct the index up to the next whole
	      *  number.
	      */
		wsaaRiskCessTerm.set(datcon3rec.freqFactor);


		if (isGT(datcon3rec.freqFactor, wsaaRiskCessTerm)) {
			wsaaRiskCessTerm.add(1);
		}

		if(!isEQ(wsaaRiskCessTerm,ZERO)){
			findIuDiscountFactor900();
		}
		drycntrec.contotNumber.set(ct03);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}

	private void findIuDiscountFactor900(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start901();
				case fixedTerm903:
					fixedTerm903();
				case wholeOfLife905:
					wholeOfLife905();
				case exit909:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start901(){
		wsaaInitUnitDiscFact.set(1);

		/*
		 *  Table T5540: When Discount-Basis is set, Discount-Factor
	      *               should also be set, otherwise the table has
	      *               been set incorrectly.
	      */


		if (isNE(wsaaT5540IuDiscBasis, SPACES)) {
			readT5519910();
		}

		if (isNE(wsaaT5540WholeIuDiscFact, SPACES)) {
			goTo(GotoLabel.wholeOfLife905);
		}


		if(!isEQ(wsaaT5540IuDiscBasis,SPACES)){
			if(!isEQ(t5519rec.fixdtrm,ZERO)&&isGT(wsaaRiskCessTerm,t5519rec.fixdtrm)){
				goTo(GotoLabel.fixedTerm903);
			}
			readT5539920();
			wsaaInitUnitDiscFact.set(t5539rec.dfact[wsaaRiskCessTerm.toInt()]);
			utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000), true);
		}
		goTo(GotoLabel.exit909);
	}

	protected void fixedTerm903(){		
		readT5539920();				
		wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);		
		utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000), true);		
		goTo(GotoLabel.exit909);
	}

/* * Use the age at next birthday to read off T6646 to obtain
      * the discount factor.*/
	protected void wholeOfLife905(){
		if(isEQ(covruddIO.getAnbAtCcd(),ZERO)){
		goTo(GotoLabel.exit909);
	
		}
	
		if(!isEQ(t5540rec.wholeIuDiscFact,SPACES)){
		readT6646930();
	
		wsaaInitUnitDiscFact.set(t6646rec.dfact[covruddIO.getAnbAtCcd().toInt()]);
		utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000), true);
	
		}
	}

	private void readT5519910(){
		
		itdmIO.setParams(SPACES);

		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5519);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setItemitem(wsaaT5540IuDiscBasis);		
		itdmIO.setFormat(itdmrec);
		itdmIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, itdmIO);
		if(!isEQ(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if(!isEQ(itdmIO.getItemcoy(),drypDryprcRecInner.drypCompany)
				||!isEQ(itdmIO.getItemtabl(),t5519)
				||!isEQ(itdmIO.getItemitem(),wsaaT5540IuDiscBasis)
				||isLT(itdmIO.getItmto(),drypDryprcRecInner.drypRunDate)){
			itdmIO.setItemitem(wsaaT5540IuDiscBasis);
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.statuz.set(g027);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();

		}
		t5519rec.t5519Rec.set(itdmIO.getGenarea());
	}
	
	private void readT5539920(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start921();
				case exit929:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
		
	}
	protected void start921(){
		if(isEQ(wsaaT5540IuDiscBasis,SPACES)){
			goTo(GotoLabel.exit929);
		}
		itdmIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t5539);
		itemIO.setItemitem(wsaaT5540IuDiscFact);
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if(!isEQ(itemIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5539rec.t5539Rec.set(itemIO.getGenarea());
	}

	private void readT6646930(){

		itdmIO.setParams(SPACES);
		itemIO.setItempfx("IT");
		itemIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itemIO.setItemtabl(t6646);
		itemIO.setItemitem(wsaaT5540WholeIuDiscFact); 
		itemIO.setFunction(varcom.readr);
		itemIO.setFormat(itemrec);
		SmartFileCode.execute(appVars, itemIO);
		if(!isEQ(itemIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(itemIO.getStatuz());
			drylogrec.params.set(itemIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t6646rec.t6646Rec.set(itemIO.getGenarea());
	}
	
	protected void loadUtrsCalcFundVals1000(){

		utrsIO.setParams(SPACES);
		utrsIO.setChdrcoy(covruddIO.getChdrcoy());
		utrsIO.setChdrnum(covruddIO.getChdrnum());
		utrsIO.setLife(covruddIO.getLife());
		utrsIO.setCoverage(covruddIO.getCoverage());
		utrsIO.setRider(covruddIO.getRider());
		utrsIO.setPlanSuffix(covruddIO.getPlanSuffix());
		utrsIO.setUnitVirtualFund(SPACES);
		utrsIO.setUnitType(SPACES);
		wsaaValInitUnitsP.set(ZERO);
		wsaaValAcumUnitsP.set(ZERO);
		wsaaValInitUnitsN.set(ZERO);
		wsaaValAcumUnitsN.set(ZERO);
		utrsIO.setFunction(varcom.begn);
		utrsIO.setFormat(utrsrec);
		iu.set(1);
		while ( !(isEQ(utrsIO.getStatuz(), varcom.endp))) {
			loadRecords1100();
		}
	
	}
	
	protected void loadRecords1100() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begin1110();
					utrsToProcess1130();
				case next1180:
					next1180();
				case exit1190:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void begin1110() {
		SmartFileCode.execute(appVars, utrsIO);
		if (isNE(utrsIO.getStatuz(), varcom.oK) && isNE(utrsIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(utrsIO.getStatuz());
			drylogrec.params.set(utrsIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(utrsIO.getStatuz(), varcom.endp) || isNE(utrsIO.getChdrcoy(), covruddIO.getChdrcoy())
				|| isNE(utrsIO.getChdrnum(), covruddIO.getChdrnum()) || isNE(utrsIO.getLife(), covruddIO.getLife())
				|| isNE(utrsIO.getCoverage(), covruddIO.getCoverage()) || isNE(utrsIO.getRider(), covruddIO.getRider())
				|| isNE(utrsIO.getPlanSuffix(), covruddIO.getPlanSuffix())) {
			wsaaUtrsRrn[iu.toInt()].set(ZERO);
			utrsIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit1190);
		}
		drycntrec.contotNumber.set(ct04);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		
	}

	protected void utrsToProcess1130() 
	{
		ufpricerec.function.set("DEALP");
		ufpricerec.company.set(drypDryprcRecInner.drypCompany);
		ufpricerec.unitVirtualFund.set(utrsIO.getUnitVirtualFund());
		ufpricerec.unitType.set(utrsIO.getUnitType());
		ufpricerec.effdate.set(drypDryprcRecInner.drypRunDate);
		ufpricerec.nowDeferInd.set("N");
		callProgram(Ufprice.class, ufpricerec.ufpriceRec);
		if (isNE(ufpricerec.statuz, varcom.oK)
		&& isNE(ufpricerec.statuz, varcom.endp)) {
			drylogrec.params.set(ufpricerec.ufpriceRec);
			drylogrec.statuz.set(ufpricerec.statuz);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(ufpricerec.statuz, varcom.endp)) {
			drycntrec.contotNumber.set(ct05);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			goTo(GotoLabel.next1180);
		}
		/*    Using either the Bid or Offer price calculate the value of*/
		/*    units of this fund type attached to the coverage.*/
		if (isEQ(t6647rec.bidoffer, "B")) {
			compute(wsaaFundValue, 6).setRounded(mult(ufpricerec.bidPrice, utrsIO.getCurrentUnitBal()));
		} else {
			compute(wsaaFundValue, 6).setRounded(mult(ufpricerec.offerPrice, utrsIO.getCurrentUnitBal()));
		}
		/*    The price will be in the currency of the fund. But before we*/
		/*    know the value to the coverage we may need to convert it into*/
		/*    the currency of the contract. Note that XCVRT is used only*/
		/*    to get the rate as allowing XCVRT to calculate would result*/
		/*    in a loss of precision.*/
		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(utrsIO.getUnitVirtualFund());
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if(isNE(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if(isNE(itdmIO.getItemtabl(),t5515)
				||isNE(itdmIO.getItemcoy(),drypDryprcRecInner.drypCompany)
				||isNE(itdmIO.getItemitem(),utrsIO.getUnitVirtualFund())
				||isEQ(itdmIO.getStatuz(),varcom.endp)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		wsaaUtrsFundCurr[iu.toInt()].set(t5515rec.currcode);
		if(isEQ(chdrlifIO.getCntcurr(),t5515rec.currcode)){
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.rateUsed.set(1);
		} else{
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.currIn.set(t5515rec.currcode);
			conlinkrec.cashdate.set(ufpricerec.priceDate);
			conlinkrec.currOut.set(chdrlifIO.getCntcurr());
			conlinkrec.function.set("CVRT");
			x1000CallXcvrt();
		}
		if (isGT(wsaaFundValue, ZERO)) {
			compute(wsaaUtrsCovFundAmtP[iu.toInt()], 10).setRounded(mult(wsaaFundValue, conlinkrec.rateUsed));
			wsaaUtrsCovFundAmtN[iu.toInt()].set(ZERO);
		} else {
			compute(wsaaUtrsCovFundAmtN[iu.toInt()], 10).setRounded(mult(wsaaFundValue, conlinkrec.rateUsed));
			wsaaUtrsCovFundAmtP[iu.toInt()].set(ZERO);
		}
		if (isEQ(utrsIO.getUnitType(), "A")) {
			wsaaValAcumUnitsP.add(wsaaUtrsCovFundAmtP[iu.toInt()]);
			wsaaValAcumUnitsN.add(wsaaUtrsCovFundAmtN[iu.toInt()]);
		} else {
			wsaaValInitUnitsP.add(wsaaUtrsCovFundAmtP[iu.toInt()]);
			wsaaValInitUnitsN.add(wsaaUtrsCovFundAmtN[iu.toInt()]);
		}
		wsaaUtrsRrn[iu.toInt()].set(utrsIO.getRrn());
		wsaaUtrsUnitVirtualFund[iu.toInt()].set(utrsIO.getUnitVirtualFund());
		wsaaUtrsUnitType[iu.toInt()].set(utrsIO.getUnitType());
		wsaaUtrsCurrentUnitBal[iu.toInt()].set(utrsIO.getCurrentUnitBal());
		wsaaUtrsCurrentDunitBal[iu.toInt()].set(utrsIO.getCurrentDunitBal());
		wsaaUtrsSurrAmt[iu.toInt()].set(ZERO);
		iu.add(1);
		drycntrec.contotNumber.set(ct06);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		goTo(GotoLabel.next1180);
	}
	
	protected void next1180(){
		utrsIO.function.set(varcom.nextr);
	}


	//1200-LOAD-HITS-CALC-FUND-VALS SECTION.                           <DRYAP2>
	protected void loadHitsCalcFundVals1200() {
		hitsIO.setParams(SPACES);
		hitsIO.setChdrcoy(covruddIO.getChdrcoy());
		hitsIO.setChdrnum(covruddIO.getChdrnum());
		hitsIO.setLife(covruddIO.getLife());
		hitsIO.setCoverage(covruddIO.getCoverage());
		hitsIO.setRider(covruddIO.getRider());
		hitsIO.setPlanSuffix(covruddIO.getPlanSuffix());
		hitsIO.setZintbfnd(SPACES);
		hitsIO.setFunction(varcom.begn);
		hitsIO.setFormat(hitsrec);
		hitsIO.setStatuz(varcom.oK);
		while (isNE(hitsIO.getStatuz(), varcom.endp)) {
			processHits1300();
		}
	}

	protected void processHits1300() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					call1310();
					process1330();
					nextr1380();
				case exit1390:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void call1310() {
		SmartFileCode.execute(appVars, hitsIO);
		if(isNE(hitsIO.getStatuz(), varcom.oK)
				&&isNE(hitsIO.getStatuz(), varcom.endp)){
			drylogrec.statuz.set(hitsIO.getStatuz());
			drylogrec.params.set(hitsIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(hitsIO.getStatuz(), varcom.endp)
		|| isNE(hitsIO.getChdrcoy(), covruddIO.getChdrcoy())
		|| isNE(hitsIO.getChdrnum(), covruddIO.getChdrnum())
		|| isNE(hitsIO.getLife(), covruddIO.getLife())
		|| isNE(hitsIO.getCoverage(), covruddIO.getCoverage())
		|| isNE(hitsIO.getRider(), covruddIO.getRider())
		|| isNE(hitsIO.getPlanSuffix(), covruddIO.getPlanSuffix())) {
			hitsIO.setStatuz(varcom.endp);
			wsaaUtrsRrn[iu.toInt()].set(ZERO);
			goTo(GotoLabel.exit1390);
		}
	}
	protected void process1330() {
		drycntrec.contotNumber.set(ct23);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		wsaaFundValue.set(hitsIO.getZcurprmbal());

//	    * The price will be in the currency of the fund. But before we    <DRYAP2>
//	    * know the value to the coverage we may need to convert it into   <DRYAP2>
//	    * the currency of the contract. Note that XCVRT is used only      <DRYAP2>
//	    * to get the rate as allowing XCVRT to calculate would result     <DRYAP2>
//	    * in a loss of precision.

		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(hitsIO.getZintbfnd());
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);
		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);
		SmartFileCode.execute(appVars, itdmIO);
		if(isNE(itdmIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if(isNE(itdmIO.getItemtabl(),t5515)
				||isNE(itdmIO.getItemcoy(),drypDryprcRecInner.drypCompany)
				||isNE(itdmIO.getItemitem(),hitsIO.getZintbfnd())
				||isEQ(itdmIO.getStatuz(),varcom.endp)){
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		wsaaUtrsFundCurr[iu.toInt()].set(t5515rec.currcode);
		if(isEQ(chdrlifIO.getCntcurr(),t5515rec.currcode)){
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.rateUsed.set(1);
		}else{
			conlinkrec.amountIn.set(wsaaFundValue);
			conlinkrec.currIn.set(t5515rec.currcode);
			conlinkrec.cashdate.set(drypDryprcRecInner.drypRunDate);
			conlinkrec.currOut.set(chdrlifIO.getCntcurr());
			conlinkrec.function.set("CVRT");
			x1000CallXcvrt();
		}
		if(isGT(wsaaFundValue,ZERO)){
			compute(wsaaUtrsCovFundAmtP[iu.toInt()], 10).setRounded(mult(wsaaFundValue, conlinkrec.rateUsed));
			wsaaUtrsCovFundAmtN[iu.toInt()].set(ZERO);
		}else{
			compute(wsaaUtrsCovFundAmtN[iu.toInt()], 10).setRounded(mult(wsaaFundValue, conlinkrec.rateUsed));
			wsaaUtrsCovFundAmtP[iu.toInt()].set(ZERO);
		}
		wsaaValAcumUnitsP.add(wsaaUtrsCovFundAmtP[iu.toInt()]);
		wsaaValAcumUnitsN.add(wsaaUtrsCovFundAmtN[iu.toInt()]);
		wsaaUtrsRrn[iu.toInt()].set(hitsIO.getRrn());
		wsaaUtrsUnitVirtualFund[iu.toInt()].set(hitsIO.getZintbfnd());
		wsaaUtrsUnitType[iu.toInt()].set("D");
		wsaaUtrsSurrAmt[iu.toInt()].set(ZERO);
		wsaaUtrsCurrentUnitBal[iu.toInt()].set(ZERO);
		wsaaUtrsCurrentDunitBal[iu.toInt()].set(ZERO);
		wsaaUtrsSurrAmt[iu.toInt()].set(ZERO);
		drycntrec.contotNumber.set(ct06);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		iu.add(1);
	}

	protected void nextr1380() {
		hitsIO.setFunction(varcom.nextr);
	}

	protected void buyUnits1400() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					buyUnits1410();
				case initials1420:
					initials1420();
				case buyUnits1430:
					buyUnits1430();
					begnUlkdrnl1450();
				case exit1490:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	private void begnUlkdrnl1450() {
	}

	protected void buyUnits1410() {
		drycntrec.contotNumber.set(ct20);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		/*
		    To have a negitive coverage debt is unusual although it*/
				/*    can happen. (The old version of this program, B5463, had*/
				/*    a bug which sometimes resulted in small negative debts)*/
				/*    It can also arise when units are surrendered, and due to*/
				/*    a price fluctuation, the value of the units sold no longer*/
				/*    equals the value attached to the coverage. In this case*/
				/*    the non fortfieture processing can make up this usualy small*/
				/*    shortfall by increasing the coverage debt.*/
				/*    The basic processing is to settle all negitive unit*/
				/*    holding first, thus reducing their value to zero and then*/
				/*    purchase units depending upon the latest ULNK values.*/

		if (isGTE(wsaaValAcumUnitsN, ZERO)) {
			goTo(GotoLabel.initials1420);
		}
		if (isLTE(wsaaValAcumUnitsN, covruddIO.getCoverageDebt())) {
			wsaaSurrAmount.set(covruddIO.getCoverageDebt());
			covruddIO.setCoverageDebt(ZERO);
		}
		else {
			wsaaSurrAmount.set(wsaaValAcumUnitsN);
			covruddIO.setCoverageDebt(sub(covruddIO.getCoverageDebt(), wsaaSurrAmount), true);
		}

		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if (isEQ(wsaaUtrsUnitType[iu.toInt()], "I")) {
				compute(wsaaSurrPercent, 12).setRounded(div(wsaaUtrsCovFundAmtN[iu.toInt()], wsaaValAcumUnitsN));
				compute(wsaaUtrsSurrAmt[iu.toInt()], 12).setRounded(mult(wsaaSurrPercent, wsaaSurrAmount));
			}
		}
		if (isEQ(covruddIO.getCoverageDebt(), ZERO)) {
			goTo(GotoLabel.exit1490);
		}
	}

	protected void initials1420() {
		if (isGTE(wsaaValInitUnitsN, ZERO)) {
			goTo(GotoLabel.buyUnits1430);
		}
		if (isLTE(wsaaValInitUnitsN, covruddIO.getCoverageDebt())) {
			wsaaSurrAmount.set(covruddIO.getCoverageDebt());
			covruddIO.setCoverageDebt(ZERO);
		}
		else {
			wsaaSurrAmount.set(wsaaValInitUnitsN);
			covruddIO.setCoverageDebt(sub(covruddIO.getCoverageDebt(), wsaaSurrAmount), true);
		}
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if (isEQ(wsaaUtrsUnitType[iu.toInt()], "I")) {
				compute(wsaaSurrPercent, 12).setRounded(div(wsaaUtrsCovFundAmtN[iu.toInt()], wsaaValInitUnitsN));
				compute(wsaaUtrsSurrAmt[iu.toInt()], 12).setRounded(mult(wsaaSurrPercent, wsaaSurrAmount));
			}
		}
		if (isEQ(covruddIO.getCoverageDebt(), ZERO)) {
			goTo(GotoLabel.exit1490);
		}
	}
	
	protected void buyUnits1430() {
	//1430-BUY-UNITS.
	//
	//COMPUTE WSAA-SURR-AMOUNT     = COVRUDD-COVERAGE-DEBT * -1.
	//*
	//* Read ULNKRNL to get the fund split for the coverage/rider.
	//*
		compute(wsaaSurrAmount, 5).set(mult(covruddIO.getCoverageDebt(), -1));
		ulnkrnlIO.setChdrcoy(covruddIO.getChdrcoy());
		ulnkrnlIO.setChdrnum(covruddIO.getChdrnum());
		ulnkrnlIO.setLife(covruddIO.getLife());
		ulnkrnlIO.setCoverage(covruddIO.getCoverage());
		ulnkrnlIO.setRider(covruddIO.getRider());
		ulnkrnlIO.setPlanSuffix(covruddIO.getPlanSuffix());
		ulnkrnlIO.setFunction(varcom.readr);
		SmartFileCode.execute(appVars, ulnkrnlIO);
		if (isNE(ulnkrnlIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(ulnkrnlIO.getParams());
			drylogrec.statuz.set(ulnkrnlIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaAloTot.set(ZERO);
		for (ix.set(1); !(isGT(ix, 10)
		|| isEQ(ulnkrnlIO.getUalfnd(ix), SPACES)); ix.add(1)){
			completeAndWriteUtrn1500();
		}
	}
	
	protected void completeAndWriteUtrn1500() {
		completeAndWriteUtrn1510();
	}
	
	protected void completeAndWriteUtrn1510() {
		/*
		 * We dont have a great deal to do here as Unit Deal (B5102) can manage
		 * to complete most of the UTRN details thus we do only what is
		 * necessary.
		 */
		utrnaloIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		utrnaloIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		utrnaloIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		utrnaloIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		utrnaloIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		utrnaloIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		utrnaloIO.setJobnoPrice(ZERO);
		utrnaloIO.setUstmno(ZERO);
		utrnaloIO.setInciPerd01(ZERO);
		utrnaloIO.setInciprm01(ZERO);
		utrnaloIO.setSurrenderPercent(ZERO);
		utrnaloIO.setStrpdate(ZERO);
		utrnaloIO.setInciNum(ZERO);
		utrnaloIO.setInciPerd02(ZERO);
		utrnaloIO.setInciprm02(ZERO);
		utrnaloIO.setNowDeferInd("N");
		utrnaloIO.setCovdbtind("D");
		utrnaloIO.setMoniesDate(drypDryprcRecInner.drypRunDate);
		utrnaloIO.setSwitchIndicator(SPACES);
		utrnaloIO.setTriggerKey(SPACES);
		utrnaloIO.setTriggerModule(SPACES);
		utrnaloIO.setTermid(SPACES);
		utrnaloIO.setUser(999999);
		utrnaloIO.setFundAmount(ZERO);
		utrnaloIO.setNofUnits(ZERO);
		utrnaloIO.setNofDunits(ZERO);
		utrnaloIO.setPriceDateUsed(ZERO);
		utrnaloIO.setPriceUsed(ZERO);
		utrnaloIO.setUnitBarePrice(ZERO);
		utrnaloIO.setFeedbackInd(SPACES);
		utrnaloIO.setSvp(1);
		hitrIO.setSvp(1);
		utrnaloIO.setDiscountFactor(1);
		utrnaloIO.setFundRate(1);
		utrnaloIO.setScheduleName(wsaaProg);
		utrnaloIO.setScheduleNumber(drypDryprcRecInner.drypRunNumber);
		utrnaloIO.setUnitVirtualFund(wsaaUtrsUnitVirtualFund[iu.toInt()]);
		utrnaloIO.setFundCurrency(wsaaUtrsFundCurr[iu.toInt()]);
		utrnaloIO.setUnitType(wsaaUtrsUnitType[iu.toInt()]);
		if (isEQ(utrnaloIO.getUnitType(), "A")) {
			utrnaloIO.setDiscountFactor(1);
		} else {
			getDfactForInit1600();
		}
		utrnaloIO.setContractAmount(sub(ZERO, wsaaUtrsSurrAmt[iu.toInt()]));
		if (isGT(wsaaOrigCovrDebt, wsaaTotalSurrAmt)) {
			utrnaloIO.setNofUnits(sub(0, wsaaUtrsCurrentUnitBal[iu.toInt()]));
			if (isLT(utrnaloIO.getContractAmount(), wsaaUtrsCurrentUnitBal[iu.toInt()])) {
				utrnaloIO.setNofDunits(sub(0, wsaaUtrsCurrentDunitBal[iu.toInt()]));
			}
			if (isEQ(utrnaloIO.getUnitType(), "A")) {
				utrnaloIO.setNofDunits(utrnaloIO.getNofUnits());
			}
		} else {
			utrnaloIO.setNofUnits(0);
			utrnaloIO.setNofDunits(0);
		}
		utrnaloIO.setTranno(wsaaTranno);
		utrnaloIO.setCntcurr(chdrlifIO.getCntcurr());
		utrnaloIO.setContractType(chdrlifIO.getCnttype());
		utrnaloIO.setChdrcoy(covruddIO.getChdrcoy());
		utrnaloIO.setChdrnum(covruddIO.getChdrnum());
		utrnaloIO.setLife(covruddIO.getLife());
		utrnaloIO.setCoverage(covruddIO.getCoverage());
		utrnaloIO.setRider(covruddIO.getRider());
		utrnaloIO.setPlanSuffix(covruddIO.getPlanSuffix());
		utrnaloIO.setCrComDate(covruddIO.getCrrcd());
		utrnaloIO.setCrtable(covruddIO.getCrtable());
		utrnaloIO.setProcSeqNo(t6647rec.procSeqNo);
		utrnaloIO.setTransactionDate(wsaaDate);
		utrnaloIO.setTransactionTime(getCobolTime());
		if (componLevelAccounted.isTrue()) {
			utrnaloIO.setSacscode(wsaaT5645Sacscode[2]);
			utrnaloIO.setSacstyp(wsaaT5645Sacstype[2]);
			utrnaloIO.setGenlcde(wsaaT5645Glmap[2]);
		} else {
			utrnaloIO.setSacscode(wsaaT5645Sacscode[1]);
			utrnaloIO.setGenlcde(wsaaT5645Glmap[1]);
			utrnaloIO.setSacstyp(wsaaT5645Sacstype[1]);
		}
		utrnaloIO.setFormat(utrnalorec);
		utrnaloIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, utrnaloIO);
		if (isNE(utrnaloIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(utrnaloIO.getStatuz());
			drylogrec.params.set(utrnaloIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		drycntrec.contotNumber.set(ct07);
		drycntrec.contotValue.set(1);
		d000ControlTotals();

		drycntrec.contotNumber.set(ct08);
		drycntrec.contotValue.set(utrnaloIO.getContractAmount());
		d000ControlTotals();
	}

	protected void getDfactForInit1600() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					begin1610();
				case fixedTerm1630:
					fixedTerm1630();
				case wholeOfLife1640:
					wholeOfLife1640();
				case exit1690:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void begin1610() {
		if (!isEQ(wsaaT5540WholeIuDiscFact, SPACES)) {
			goTo(GotoLabel.wholeOfLife1640);
		}
		if (!isEQ(wsaaT5540IuDiscBasis, SPACES)) {
			if (!isEQ(t5519rec.fixdtrm, ZERO) && isGT(wsaaRiskCessTerm, t5519rec.fixdtrm)) {
				goTo(GotoLabel.fixedTerm1630);
			}
			readT5539920();
			wsaaInitUnitDiscFact.set(t5539rec.dfact[wsaaRiskCessTerm.toInt()]);
			utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000), true);
		}
		goTo(GotoLabel.exit1690);
	}

	protected void fixedTerm1630() {
		readT5539920();
		wsaaInitUnitDiscFact.set(t5539rec.dfact[t5519rec.fixdtrm.toInt()]);
		utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000), true);
		goTo(GotoLabel.exit1690);
	}

	protected void wholeOfLife1640() {
		if (isEQ(covruddIO.getAnbAtCcd(), ZERO)) {
			goTo(GotoLabel.exit1690);
		}
		if (!isEQ(wsaaT5540WholeIuDiscFact, SPACES)) {
			readT6646930();
			wsaaInitUnitDiscFact.set(t6646rec.dfact[covruddIO.getAnbAtCcd().toInt()]);
			utrnaloIO.setDiscountFactor(div(wsaaInitUnitDiscFact, 100000), true);
		}
	}

	private void completeAndWriteHitr1700() {
		hitraloIO.setParams(SPACES);
		hitraloIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		hitraloIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		hitraloIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		hitraloIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		hitraloIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		hitraloIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		hitraloIO.setZintrate(ZERO);
		hitraloIO.setUstmno(ZERO);
		hitraloIO.setInciPerd01(ZERO);
		hitraloIO.setInciprm01(ZERO);
		hitraloIO.setSurrenderPercent(ZERO);
		hitraloIO.setInciNum(ZERO);
		hitraloIO.setInciPerd02(ZERO);
		hitraloIO.setInciprm02(ZERO);
		hitraloIO.setFundAmount(ZERO);
		hitraloIO.setZinteffdt(varcom.vrcmMaxDate);
		hitraloIO.setZlstintdte(varcom.vrcmMaxDate);
		hitraloIO.setCovdbtind("D");
		hitraloIO.setEffdate(drypDryprcRecInner.drypRunDate);
		hitraloIO.setSwitchIndicator(SPACES);
		hitraloIO.setTriggerModule(SPACES);
		hitraloIO.setTriggerKey(SPACES);
		hitraloIO.setFundRate(1);
		hitraloIO.setFeedbackInd(SPACES);
		hitraloIO.setZintappind(SPACES);
		hitraloIO.setSvp(1);
		hitraloIO.setFundRate(1);
		hitraloIO.setZintbfnd(wsaaUtrsUnitVirtualFund[iu.toInt()]);
		hitraloIO.setFundCurrency(wsaaUtrsFundCurr[iu.toInt()]);
		hitraloIO.setZrectyp("P");
		setPrecision(hitraloIO.getContractAmount(), 2);
		hitraloIO.setContractAmount(sub(ZERO, wsaaUtrsSurrAmt[iu.toInt()]));
		hitraloIO.setTranno(wsaaTranno);
		hitraloIO.setCntcurr(chdrlifIO.getCntcurr());
		hitraloIO.setCnttyp(chdrlifIO.getCnttype());
		hitraloIO.setChdrcoy(covruddIO.getChdrcoy());
		hitraloIO.setChdrnum(covruddIO.getChdrnum());
		hitraloIO.setLife(covruddIO.getLife());
		hitraloIO.setCoverage(covruddIO.getCoverage());
		hitraloIO.setRider(covruddIO.getRider());
		hitraloIO.setPlanSuffix(covruddIO.getPlanSuffix());
		hitraloIO.setCrtable(covruddIO.getCrtable());
		hitraloIO.setProcSeqNo(t6647rec.procSeqNo); 
		if (componLevelAccounted.isTrue()) {
			hitraloIO.setSacscode(wsaaT5645Sacscode[2]);
			hitraloIO.setSacstyp(wsaaT5645Sacstype[2]);
			hitraloIO.setGenlcde(wsaaT5645Glmap[2]);
		} else {
			hitraloIO.setSacscode(wsaaT5645Sacscode[1]);
			hitraloIO.setGenlcde(wsaaT5645Glmap[1]);
			hitraloIO.setSacstyp(wsaaT5645Sacstype[1]);
		}
		hitraloIO.setFormat(hitralorec);
		hitraloIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitraloIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(utrnaloIO.getStatuz());
			drylogrec.params.set(utrnaloIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct07);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		drycntrec.contotNumber.set(ct08);
		drycntrec.contotValue.set(utrnaloIO.getContractAmount());
		d000ControlTotals();
	}

	protected void rewriteCovr1800() {
		rewrite1810();
	}

	protected void rewrite1810() {
		covrIO.setParams(SPACES);
		covrIO.setChdrcoy(covruddIO.getChdrcoy());
		covrIO.setChdrnum(covruddIO.getChdrnum());
		covrIO.setLife(covruddIO.getLife());
		covrIO.setCoverage(covruddIO.getCoverage());
		covrIO.setRider(covruddIO.getRider());
		covrIO.setPlanSuffix(covruddIO.getPlanSuffix());
		covrIO.setFunction(varcom.readh);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (!isEQ(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		covrIO.setCoverageDebt(covruddIO.getCoverageDebt());
		covrIO.setFunction(varcom.rewrt);
		covrIO.setFormat(covrrec);
		SmartFileCode.execute(appVars, covrIO);
		if (!isEQ(covrIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrIO.getStatuz());
			drylogrec.params.set(covrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct09);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		drycntrec.contotNumber.set(ct10);
		compute(drycntrec.contotValue, 2).set(sub(wsaaOrigCovrDebt, covruddIO.getCoverageDebt()));
		d000ControlTotals();
	}

	// ------ SON
	
	// ------> NHUT

	protected void processUnpaidDebt1900() {

		// 1900-PROCESS-UNPAID-DEBT SECTION.
		// **********************************
		// 1910-START.

		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start1910();
				case exit1990:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}


	protected void start1910() {

		if (isGT(covruddIO.getCrrcd(), drypDryprcRecInner.drypRunDate)) {
			goTo(GotoLabel.exit1990);
		}

		// * Read table T5687 for the non-forfeiture method.
		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5687);
		itdmIO.setItemitem(covruddIO.getCrtable());
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);

		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);

		SmartFileCode.execute(appVars, itdmIO);

		if (isNE(itdmIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if (isNE(itdmIO.getItemtabl(), t5687) || isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(itdmIO.getItemitem(), covruddIO.getCrtable()) || isEQ(itdmIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		t5687rec.t5687Rec.set(itdmIO.getGenarea());

		if (isEQ(t5687rec.nonForfeitMethod, SPACES)) {
			goTo(GotoLabel.exit1990);
		}

		// *
		// * Read table T6597 for the non-forfeiture details.
		// *

		itdmIO.setParams(SPACES);
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t6597);
		itdmIO.setItemitem(t5687rec.nonForfeitMethod);
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);

		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);

		SmartFileCode.execute(appVars, itdmIO);

		if (isNE(itdmIO.getStatuz(), varcom.oK)) {

			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if (isNE(itdmIO.getItemtabl(), t6597) || isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(itdmIO.getItemitem(), t5687rec.nonForfeitMethod) || isEQ(itdmIO.getStatuz(), varcom.endp)) {

			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		t6597rec.t6597Rec.set(itdmIO.getGenarea());

		// *
		// * Already Lapsed
		// *
		if (isEQ(t6597rec.crstat04, covruddIO.getStatcode())) {

			drycntrec.contotNumber.set(ct22);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			goTo(GotoLabel.exit1990);
		}
//ILPI-168
		dryrDryrptRecInner.r5107Lapind.set(SPACES);

		// *
		// * Call the subroutine DATCON3 to establish how many months
		// * have elapsed since coverage commence date.
		// *

		datcon3rec.function.set(SPACES);
		datcon3rec.intDate1.set(covruddIO.getCrrcd());
		datcon3rec.intDate2.set(drypDryprcRecInner.drypRunDate);
		datcon3rec.frequency.set("12");

		callProgram(Datcon3.class, datcon3rec.datcon3Rec);

		if (isNE(datcon3rec.statuz, varcom.oK)) {
			drylogrec.statuz.set(datcon3rec.statuz);
			drylogrec.params.set(datcon3rec.datcon3Rec);
			drylogrec.drySystemError.setTrue();
			a000FatalError();
		}

		// *
		// * Find the method with which to deal with the unpaid debt.
		// * Read through the four options in the cancellation order set
		// * out in T6647. Ignoring any items which are spaces or
		// * less than the frequency factor returned from DATCON3 find
		// * the one which is the closest to that number. Depending on
		// * the option found carry out the following processing.

		wsaaWithinRange.set("1000");
		wsaaInsuffUnitsMeth.set(SPACES);

		if (isGTE(t6647rec.monthsNegUnits, datcon3rec.freqFactor) && isNE(t6647rec.monthsNegUnits, ZERO)) {
			wsaaWithinRange.set(t6647rec.monthsNegUnits);
			wsaaInsuffUnitsMeth.set("N");
		}

		if (isGTE(t6647rec.monthsDebt, datcon3rec.freqFactor) && isLT(t6647rec.monthsDebt, wsaaWithinRange)
				&& isNE(t6647rec.monthsDebt, ZERO)) {
			wsaaWithinRange.set(t6647rec.monthsDebt);
			wsaaInsuffUnitsMeth.set("D");
		}

		if (isGTE(t6647rec.monthsLapse, datcon3rec.freqFactor) && isLT(t6647rec.monthsLapse, wsaaWithinRange)
				&& isNE(t6647rec.monthsLapse, ZERO)) {
			wsaaWithinRange.set(t6647rec.monthsLapse);
			wsaaInsuffUnitsMeth.set("L");
		}

		if (isGTE(t6647rec.monthsError, datcon3rec.freqFactor) && isLT(t6647rec.monthsError, wsaaWithinRange)
				&& isNE(t6647rec.monthsError, ZERO)) {
			wsaaWithinRange.set(t6647rec.monthsError);
			wsaaInsuffUnitsMeth.set("E");
		}

		if (isEQ(wsaaInsuffUnitsMeth, " ")) {
			drycntrec.contotNumber.set(ct12);
			drycntrec.contotValue.set("1");
			d000ControlTotals();
			goTo(GotoLabel.exit1990);

		} else if (isEQ(wsaaInsuffUnitsMeth, "N")) {
			drycntrec.contotNumber.set(ct13);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			useNegUnitsToPayDebt2000();

		} else if (isEQ(wsaaInsuffUnitsMeth, "D")) {
			drycntrec.contotNumber.set(ct14);
			drycntrec.contotValue.set(1);
			d000ControlTotals();

		} else if (isEQ(wsaaInsuffUnitsMeth, "L")) {
			drycntrec.contotNumber.set(ct15);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			//ILPI-168
			dryrDryrptRecInner.r5107Lapind.set("L");
			lapseOption2100();
			errorOption2200();

		} else if (isEQ(wsaaInsuffUnitsMeth, "E")) {
			drycntrec.contotNumber.set(ct16);
			drycntrec.contotValue.set(1);
			d000ControlTotals();
			//ILPI-168
			dryrDryrptRecInner.r5107Lapind.set("E");
			errorOption2200();
		}

	}

	protected void useNegUnitsToPayDebt2000() {
		compute(wsaaSurrAmount, 5).set(mult(covruddIO.getCoverageDebt(), -1));
		
		ulnkrnlIO.setChdrcoy(covruddIO.getChdrcoy());
		ulnkrnlIO.setChdrnum(covruddIO.chdrnum);
		ulnkrnlIO.setLife(covruddIO.life);
		ulnkrnlIO.setCoverage(covruddIO.coverage);
		ulnkrnlIO.setRider(covruddIO.rider);
		ulnkrnlIO.setPlanSuffix(covruddIO.planSuffix);
		ulnkrnlIO.setFunction(varcom.readr);
		ulnkrnlIO.setFormat(ulnkrnlrec);

		SmartFileCode.execute(appVars, ulnkrnlIO);

		if (isNE(ulnkrnlIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(ulnkrnlIO.getStatuz());
			drylogrec.params.set(ulnkrnlIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		wsaaAloTot.set(ZERO);

		for (ix.set(1); !(isGT(ix, 10) || isEQ(ulnkrnlIO.getUalfnd(ix), SPACES)); ix.add(1)) {
			completeAndWriteUtrn2050();
		}

		covruddIO.coverageDebt.set(ZERO);
		rewriteCovr1800();
		if (isNE(wsaaPrevChdrnum, drypDryprcRecInner.drypEntity)
			&& !chdrUpdated.isTrue()) {
			updateChdr2400();
			writePtrn2500();
		}
	
	}



	//2050-COMPLETE-AND-WRITE-UTRN SECTION.
	//**************************************
	//2051-START.
	//*
	//* We dont have a great deal to do here as Unit Deal (B5102)
	//* can manage to complete most of the UTRN details thus we
	//* do only what is necessary.
	protected void completeAndWriteUtrn2050() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2051();
				case exit2059:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start2051() {

		utrnaloIO.setContractAmount(div((mult(wsaaSurrAmount, ulnkrnlIO.getUalprc(ix))), 100), true);
		wsaaAloTot.add(utrnaloIO.getContractAmount());

		// *
		// * If the next fund on the UNLK is spaces then we have come to
		// * the end. Thus we need to check that the amount of money buying
		// * units equals the total of the coverage debt. If not, a balancing
		// * error results between the units purchased and the debt written
		// * off.
		// *

		if (isEQ(ulnkrnlIO.getUalfnd(add(ix, 1)), SPACES)) {

			if (isNE(wsaaAloTot, wsaaSurrAmount)) {
				compute(wsaaBalanceCheck, 5).set(sub(wsaaAloTot, wsaaSurrAmount));
				utrnaloIO.setContractAmount(sub(utrnaloIO.getContractAmount(), wsaaBalanceCheck));
			}
		}

		/****
		 * MOVE NEXTP TO ULKDRNL-FUNCTION. PERFORM 2700-UKLDRNL-CALL.
		 */

		covruddIO.setCoverageDebt(sub(covruddIO.getCoverageDebt(), utrnaloIO.getContractAmount()));
		utrnaloIO.setNofUnits(ZERO);
		utrnaloIO.setNofDunits(ZERO);
		utrnaloIO.setUnitVirtualFund(ulnkrnlIO.getUalfnd(ix));

		itdmIO.setParams(SPACES);
		itdmIO.setItempfx("IT");
		itdmIO.setItemcoy(drypDryprcRecInner.drypCompany);
		itdmIO.setItemtabl(t5515);
		itdmIO.setItemitem(utrnaloIO.getUnitVirtualFund());
		itdmIO.setItmfrm(drypDryprcRecInner.drypRunDate);

		itdmIO.setFunction(varcom.begn);
		itdmIO.setFormat(itdmrec);

		SmartFileCode.execute(appVars, itdmIO);

		if (isNE(itdmIO.getStatuz(), varcom.oK)) {

			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if (isNE(itdmIO.getItemtabl(), t5515) || isNE(itdmIO.getItemcoy(), drypDryprcRecInner.drypCompany)
				|| isNE(itdmIO.getItemitem(), utrnaloIO.getUnitVirtualFund()) || isEQ(itdmIO.getStatuz(), varcom.endp)) {

			drylogrec.statuz.set(itdmIO.getStatuz());
			drylogrec.params.set(itdmIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/*
		 * At this point we can determine whether the Fund is Interest Bearing
		 * or not by checking the Interest Fund Method on T5515. If it is not =
		 * spaces, it is interest bearing. In this case****we write an ITRN as
		 * opposed to a UTRN we write an HITR as opposed to a UTRN
		 * 
		 * IF T5515-INT-METHOD NOT = SPACES PERFORM 2060-WRITE-ITRN
		 */

		t5515rec.t5515Rec.set(itdmIO.getGenarea());
		utrnaloIO.setFundCurrency(t5515rec.currcode);

		if (isEQ(t5515rec.zfundtyp, "D")) {

			writeHitr2060();
			goTo(GotoLabel.exit2059);
		}

		utrnaloIO.batccoy.set(drypDryprcRecInner.drypBatccoy);
		utrnaloIO.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		utrnaloIO.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		utrnaloIO.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		utrnaloIO.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		utrnaloIO.jobnoPrice.set(ZERO);
		utrnaloIO.ustmno.set(ZERO);
		utrnaloIO.inciPerd01.set(ZERO);
		utrnaloIO.inciprm01.set(ZERO);
		utrnaloIO.surrenderPercent.set(ZERO);
		utrnaloIO.strpdate.set(ZERO);
		utrnaloIO.inciNum.set(ZERO);
		utrnaloIO.inciPerd02.set(ZERO);
		utrnaloIO.inciprm02.set(ZERO);
		utrnaloIO.nowDeferInd.set("N");
		utrnaloIO.covdbtind.set("D");
		utrnaloIO.moniesDate.set(drypDryprcRecInner.drypRunDate);
		utrnaloIO.switchIndicator.set(SPACES);
		utrnaloIO.triggerKey.set(SPACES);
		utrnaloIO.triggerModule.set(SPACES);
		utrnaloIO.termid.set(SPACES);
		utrnaloIO.user.set("999999");
		utrnaloIO.fundAmount.set(ZERO);
		utrnaloIO.nofUnits.set(ZERO);
		utrnaloIO.nofDunits.set(ZERO);
		utrnaloIO.priceDateUsed.set(ZERO);
		utrnaloIO.priceUsed.set(ZERO);
		utrnaloIO.unitBarePrice.set(ZERO);
		utrnaloIO.feedbackInd.set(SPACES);
		utrnaloIO.svp.set(1);

		hitrIO.svp.set(1);

		utrnaloIO.discountFactor.set(1);
		utrnaloIO.fundRate.set(1);
		utrnaloIO.scheduleName.set(wsaaProg);
		utrnaloIO.scheduleNumber.set(drypDryprcRecInner.drypRunNumber);
		utrnaloIO.unitType.set("A");

		utrnaloIO.unitSubAccount.set("ACUM");
		utrnaloIO.tranno.set(wsaaTranno);
		utrnaloIO.cntcurr.set(chdrlifIO.getCntcurr());
		utrnaloIO.contractType.set(chdrlifIO.getCnttype());
		utrnaloIO.chdrcoy.set(covruddIO.getChdrcoy());
		utrnaloIO.chdrnum.set(covruddIO.getChdrnum());
		utrnaloIO.life.set(covruddIO.getLife());
		utrnaloIO.coverage.set(covruddIO.getCoverage());
		utrnaloIO.rider.set(covruddIO.getRider());
		utrnaloIO.planSuffix.set(covruddIO.getPlanSuffix());
		utrnaloIO.crtable.set(covruddIO.getCrtable());
		utrnaloIO.crComDate.set(covruddIO.getCrrcd());
		utrnaloIO.procSeqNo.set(t6647rec.procSeqNo);
		utrnaloIO.transactionDate.set(wsaaDate);

		/*
		 * MOVE WSAA-DATE TO UTRNALO-TRANSACTION-DATE. ACCEPT
		 * UTRNALO-TRANSACTION-TIME FROM TIME.
		 */

		utrnaloIO.setTransactionDate(wsaaDate);
		utrnaloIO.setTransactionTime(getCobolTime());

		if (componLevelAccounted.isTrue()) {
			utrnaloIO.setSacscode(wsaaT5645Sacscode[2]);
			utrnaloIO.setSacstyp(wsaaT5645Sacstype[2]);
			utrnaloIO.setGenlcde(wsaaT5645Glmap[2]);
		} else {
			utrnaloIO.setSacscode(wsaaT5645Sacscode[1]);
			utrnaloIO.setSacstyp(wsaaT5645Sacstype[1]);
			utrnaloIO.setGenlcde(wsaaT5645Glmap[1]);
		}

		utrnaloIO.setFormat(utrnalorec);
		utrnaloIO.setFunction(varcom.writr);

		SmartFileCode.execute(appVars, utrnaloIO);

		if (isNE(utrnaloIO.statuz, varcom.oK)) {
			drylogrec.statuz.set(utrnaloIO.getStatuz());
			drylogrec.params.set(utrnaloIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		drycntrec.contotNumber.set(ct17);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		drycntrec.contotNumber.set(ct18);
		drycntrec.contotValue.set(utrnaloIO.getContractAmount());
		d000ControlTotals();

	}


	//*************************                                         <DRYAP2>

	protected void writeHitr2060() {
		write2061();
	}

	//2061-WRITE.                                                      <DRYAP2>
	//*                                                                 <DRYAP2>
	//* Write ITRNs when the interest fund method is not =              <DRYAP2>
	//* spaces.                                                         <DRYAP2>
	//*
	//2060-WRITE-HITR SECTION.                                         <DRYAP2>

	protected void write2061() {
		hitrIO.setParams(SPACES);

		hitrIO.batccoy.set(drypDryprcRecInner.drypBatccoy);
		hitrIO.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		hitrIO.batcactyr.set(drypDryprcRecInner.drypBatcactyr);
		hitrIO.batcactmn.set(drypDryprcRecInner.drypBatcactmn);
		hitrIO.batctrcde.set(drypDryprcRecInner.drypBatctrcde);
		hitrIO.batcbatch.set(drypDryprcRecInner.drypBatcbatch);

		hitrIO.setZintrate(ZERO);
		hitrIO.setUstmno(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciprm01(ZERO);
		hitrIO.setSurrenderPercent(ZERO);
		hitrIO.setInciNum(ZERO);
		hitrIO.setInciPerd02(ZERO);
		hitrIO.setInciprm02(ZERO);
		hitrIO.setFundAmount(ZERO);

		hitrIO.setZinteffdt(varcom.maxdate);
		hitrIO.setCovdbtind("D");
		hitrIO.setEffdate(drypDryprcRecInner.drypRunDate);
		hitrIO.setSwitchIndicator(SPACES);
		hitrIO.setTriggerKey(SPACES);
		hitrIO.setTriggerModule(SPACES);
		hitrIO.setFeedbackInd(SPACES);
		hitrIO.setZintappind(SPACES);

		hitrIO.setSvp(1);
		hitrIO.setFundRate(1);

		hitrIO.setContractAmount(utrnaloIO.getContractAmount());
		hitrIO.setZintbfnd(utrnaloIO.getUnitVirtualFund());
		hitrIO.setFundCurrency(utrnaloIO.getFundCurrency());
		hitraloIO.setZrectyp("P");
		hitrIO.setTranno(wsaaTranno);
		hitrIO.setCntcurr(chdrlifIO.getCntcurr());
		hitrIO.setCnttyp(chdrlifIO.getCnttype());
		hitrIO.setChdrcoy(covruddIO.getChdrcoy());
		hitrIO.setChdrnum(covruddIO.getChdrnum());
		hitrIO.setLife(covruddIO.getLife());
		hitrIO.setCoverage(covruddIO.getCoverage());
		hitrIO.setRider(covruddIO.getRider());
		hitrIO.setPlanSuffix(covruddIO.getPlanSuffix());
		hitrIO.setCrtable(covruddIO.getCrtable());
		hitrIO.setProcSeqNo(t6647rec.procSeqNo);

		if (componLevelAccounted.isTrue()) {
			hitrIO.setSacscode(wsaaT5645Sacscode[2]);
			hitrIO.setSacstyp(wsaaT5645Sacstype[2]);
			hitrIO.setGenlcde(wsaaT5645Glmap[2]);
		} else {
			hitrIO.setSacscode(wsaaT5645Sacscode[1]);
			hitrIO.setGenlcde(wsaaT5645Glmap[1]);
			hitrIO.setSacstyp(wsaaT5645Sacstype[1]);
		}

		hitrIO.setFormat(hitralorec);
		hitrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, hitraloIO);
		if (isNE(hitrIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(hitrIO.getStatuz());
			drylogrec.params.set(hitrIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		drycntrec.contotNumber.set(ct25);
		drycntrec.contotValue.set(1);

		d000ControlTotals();

		drycntrec.contotNumber.set(ct26);
		drycntrec.contotValue.set(hitrIO.getContractAmount());
		d000ControlTotals();
	}


	private void lapseOption2100() {

		readChdr2110();
		setUpCovrKey2120();
		wsaaCovrmjaStatuz.set(SPACES);
		while (!(isEQ(wsaaCovrmjaStatuz, varcom.endp))) {
			readhCovr2130();
			rewriteHistoryCovr2140();
			lapseMethod2150();
			writeNewCovr2160();
			repositionCovr2170();
			readNextCovr2180();
		}

		lapseWritePtrn2190();
		lapseUpdateChdr2195();
		
		//ILPI-2040 STARTS
		drypDryprcRecInner.drypStatcode.set(chdrmjaIO.getStatcode());
		drypDryprcRecInner.drypPstatcode.set(chdrmjaIO.getPstatcode());
		//ILPI-2040 ENDS

	}


	private void readChdr2110() {

		chdrmjaIO.chdrcoy.set(chdrlifIO.getChdrcoy());
		chdrmjaIO.chdrnum.set(chdrlifIO.getChdrnum());
		chdrmjaIO.function.set(varcom.readr);
		chdrmjaIO.format.set(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (!isEQ(chdrmjaIO.statuz, varcom.oK)) {
			drylogrec.statuz.set(chdrmjaIO.statuz);
			drylogrec.params.set(chdrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		wsaaOccdate.set(chdrmjaIO.occdate);
		wsaaCurrfrom.set(chdrmjaIO.currfrom);
		
		//ILPI-2040 STARTS
		if (chdrUpdated.isTrue()) {
			compute(wsaaLapseTranno, 0).set(add(1, wsaaTranno));
		}
		else {
			wsaaLapseTranno.set(wsaaTranno);
		}
		//ILPI-2040 ENDS
	}


	private void setUpCovrKey2120() {
		wsaaTotCovrpuInstprem.set(ZERO);
		covrmjaIO.setDataArea(SPACES);
		covrmjaIO.setChdrcoy(covruddIO.getChdrcoy());
		covrmjaIO.setChdrnum(covruddIO.getChdrnum());
		covrmjaIO.setLife(covruddIO.getLife());
		covrmjaIO.setCoverage(covruddIO.getCoverage());
		covrmjaIO.setRider(covruddIO.getRider());
		covrmjaIO.setPlanSuffix(covruddIO.getPlanSuffix());
		covrmjaIO.setFunction(varcom.readh); 
	}


	private void readhCovr2130() {

		covrmjaIO.function.set(varcom.readh);
		covrmjaIO.format.set(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (!isEQ(covrmjaIO.statuz, varcom.oK)) {
			drylogrec.statuz.set(covrmjaIO.statuz);
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaNewCovrDebt.set(covrmjaIO.getCoverageDebt());
	}


	private void rewriteHistoryCovr2140() {
		covrmjaIO.validflag.set(2);

		if (isEQ(covrmjaIO.rider, 00)) {
			covrmjaIO.coverageDebt.set(wsaaOrigCovrDebt);
		}

		covrmjaIO.currto.set(chdrmjaIO.btdate);
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (!isEQ(covrmjaIO.statuz, varcom.oK)) {
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		wsaaTotCovrpuInstprem.add(covrmjaIO.getInstprem());
		covrmjaIO.tranno.set(wsaaLapseTranno);
		covrmjaIO.validflag.set(1);
		covrmjaIO.premCessDate.set(chdrmjaIO.btdate);
		covrmjaIO.coverageDebt.set(wsaaNewCovrDebt);
	}

	private void lapseMethod2150(){
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2151();
				case exit2159:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void start2151() {
		if (isEQ(t5687rec.nonForfeitMethod, SPACES)) {
			goTo(GotoLabel.exit2159);

		}
		if (isEQ(t6597rec.premsubr04, SPACES)) {
			goTo(GotoLabel.exit2159);

		}
		ovrduerec.ovrdueRec.set(SPACE);
		ovrduerec.company.set(chdrmjaIO.cowncoy);
		ovrduerec.language.set(drypDryprcRecInner.drypLanguage);
		ovrduerec.trancode.set(drypDryprcRecInner.drypBatctrcde);
		ovrduerec.acctyear.set(drypDryprcRecInner.drypBatcactyr);
		ovrduerec.acctmonth.set(drypDryprcRecInner.drypBatcactmn);
		ovrduerec.batcbrn.set(drypDryprcRecInner.drypBatcbrn);
		ovrduerec.batcbatch.set(drypDryprcRecInner.drypBatcbatch);
		ovrduerec.user.set(ZERO);
		ovrduerec.tranDate.set(wsaaDate);
		ovrduerec.tranTime.set(getCobolTime());
		ovrduerec.chdrnum.set(chdrmjaIO.getChdrnum());
		ovrduerec.chdrcoy.set(chdrmjaIO.getChdrcoy());
		ovrduerec.cntcurr.set(chdrmjaIO.getCntcurr());
		ovrduerec.effdate.set(chdrmjaIO.getCcdate());
		ovrduerec.outstamt.set(chdrmjaIO.getOutstamt());
		ovrduerec.btdate.set(chdrmjaIO.getBtdate());
		ovrduerec.ptdate.set(chdrmjaIO.getPtdate());
		ovrduerec.ptdate.set(drypDryprcRecInner.drypRunDate);
		ovrduerec.runDate.set(drypDryprcRecInner.drypRunDate);
		ovrduerec.agntnum.set(chdrmjaIO.getAgntnum());
		ovrduerec.cownnum.set(chdrmjaIO.getCownnum());
		ovrduerec.tranno.set(wsaaLapseTranno);
		ovrduerec.ovrdueDays.set(ZERO);
		ovrduerec.life.set(covrmjaIO.getLife());
		ovrduerec.coverage.set(covrmjaIO.getCoverage());
		ovrduerec.rider.set(covrmjaIO.getRider());
		ovrduerec.planSuffix.set(covrmjaIO.getPlanSuffix());
		ovrduerec.crtable.set(covrmjaIO.getCrtable());
		ovrduerec.pumeth.set(covrmjaIO.crtable);
		ovrduerec.billfreq.set(chdrmjaIO.getBillfreq());
		ovrduerec.instprem.set(covrmjaIO.getInstprem());
		ovrduerec.sumins.set(covrmjaIO.getSumins());
		ovrduerec.crrcd.set(covrmjaIO.getCrrcd());
		ovrduerec.premCessDate.set(covrmjaIO.getPremCessDate());
		ovrduerec.pupfee.set(ZERO);
		ovrduerec.cnttype.set(chdrmjaIO.getCnttype());
		ovrduerec.occdate.set(chdrmjaIO.getOccdate());
		ovrduerec.aloind.set(SPACES);
		ovrduerec.efdcode.set(SPACES);
		ovrduerec.procSeqNo.set(ZERO);
		ovrduerec.polsum.set(chdrmjaIO.polsum);
		//ILPI-168
		callProgram(t6597rec.premsubr04, ovrduerec.ovrdueRec);

		if (isNE(ovrduerec.statuz, varcom.oK)) {
			drylogrec.params.set(ovrduerec.ovrdueRec);
			drylogrec.statuz.set(ovrduerec.statuz);
			drylogrec.dryDatabaseError.setTrue();;

			a000FatalError();
		}

		/* Read through the UTRNs for this Coverage key/tranno */
		/* using LIFO on logical and delete those written by */
		/* above routine. */

		utrnrevIO.chdrcoy.set(chdrmjaIO.chdrcoy);
		utrnrevIO.chdrnum.set(chdrmjaIO.chdrnum);
		utrnrevIO.tranno.set(wsaaTranno);
		utrnrevIO.function.set(varcom.begn);

		while (!(isEQ(utrnrevIO.getStatuz(), varcom.endp))) {
			SmartFileCode.execute(appVars, utrnrevIO);

			if (isNE(utrnrevIO.statuz, varcom.oK) && isNE(utrnrevIO.statuz, varcom.endp)) {
				drylogrec.statuz.set(utrnrevIO.getStatuz());
				drylogrec.params.set(utrnrevIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();

			}

			if (isNE(utrnrevIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
					|| (isNE(utrnrevIO.getChdrnum(), chdrmjaIO.getChdrnum()))
					|| (isNE(utrnrevIO.getLife(), covrmjaIO.getLife()))
					|| (isNE(utrnrevIO.getCoverage(), covrmjaIO.getCoverage()))
					|| (isNE(utrnrevIO.getRider(), covrmjaIO.getRider()))
					|| (isNE(utrnrevIO.getPlanSuffix(), covrmjaIO.getPlanSuffix()))
					|| (isNE(utrnrevIO.getTranno(), wsaaTranno)) && isNE(utrnrevIO.getStatuz(), varcom.endp)) {
				utrnrevIO.statuz.set(varcom.endp);
			}
			/*
			 * When we have reached those previously written by this program,
			 * stop.
			 */
			if (isEQ(utrnrevIO.getCovdbtind(), "D")) {
				utrnrevIO.setStatuz(varcom.endp);
			}

			if (isNE(utrnrevIO.getStatuz(), varcom.endp)) {
				utrnrevIO.function.set(varcom.readh);
				SmartFileCode.execute(appVars, utrnrevIO);
				if (isNE(utrnrevIO.getStatuz(), varcom.oK)) {
					drylogrec.statuz.set(utrnrevIO.getStatuz());
					drylogrec.params.set(utrnrevIO.getParams());
					drylogrec.dryDatabaseError.setTrue();
					a000FatalError();

				}

				utrnrevIO.function.set(varcom.delet);
				SmartFileCode.execute(appVars, utrnrevIO);
				if (!isEQ(utrnrevIO.statuz, varcom.oK)) {
					drylogrec.statuz.set(utrnrevIO.getStatuz());
					drylogrec.params.set(utrnrevIO.getParams());
					drylogrec.dryDatabaseError.setTrue();
					a000FatalError();
				}

				utrnrevIO.function.set(varcom.nextr);
			}

		}

		/*
		 * Read through the ITRNs for this Coverage key/tranno*using LIFO on
		 * logical and delete those written by*above routine.* MOVE
		 * CHDRMJA-CHDRCOY TO ITRNREV-CHDRCOY. MOVE CHDRMJA-CHDRNUM TO
		 * ITRNREV-CHDRNUM. MOVE WSAA-TRANNO TO ITRNREV-TRANNO.
		 * 
		 * MOVE BEGNH TO ITRNREV-FUNCTION. MOVE ITRNREVREC TO ITRNREV-FORMAT.
		 * 
		 * PERFORM UNTIL ITRNREV-STATUZ = ENDP CALL 'ITRNREVIO' USING
		 * ITRNREV-PARAMS
		 * 
		 * IF ITRNREV-STATUZ NOT = O-K AND ITRNREV-STATUZ NOT = ENDP MOVE
		 * ITRNREV-STATUZ TO DRYL-STATUZ MOVE ITRNREV-PARAMS TO DRYL-PARAMS SET
		 * DRY-DATABASE-ERROR TO TRUE PERFORM A000-FATAL-ERROR END-IF
		 * 
		 * IF ITRNREV-CHDRCOY NOT = CHDRMJA-CHDRCOY OR (ITRNREV-CHDRNUM NOT =
		 * CHDRMJA-CHDRNUM) OR (ITRNREV-LIFE NOT = COVRMJA-LIFE) OR
		 * (ITRNREV-COVERAGE NOT = COVRMJA-COVERAGE) OR (ITRNREV-RIDER NOT =
		 * COVRMJA-RIDER) OR (ITRNREV-PLAN-SUFFIX NOT = COVRMJA-PLAN-SUFFIX) OR
		 * (ITRNREV-TRANNO NOT = WSAA-TRANNO) AND ITRNREV-STATUZ NOT = ENDP MOVE
		 * ENDP TO ITRNREV-STATUZ END-IF**When we have reached those previously
		 * written by this*program, stop.** IF ITRNREV-COVDBTIND = 'D' MOVE ENDP
		 * TO ITRNREV-STATUZ END-IF
		 * 
		 * IF ITRNREV-STATUZ NOT = ENDP MOVE DELET TO ITRNREV-FUNCTION CALL
		 * 'ITRNREVIO' USING ITRNREV-PARAMS IF ITRNREV-STATUZ NOT = O-K MOVE
		 * ITRNREV-STATUZ TO DRYL-STATUZ MOVE ITRNREV-PARAMS TO DRYL-PARAMS SET
		 * DRY-DATABASE-ERROR TO TRUE PERFORM A000-FATAL-ERROR END-IF MOVE NEXTR
		 * TO ITRNREV-FUNCTION END-IF END-PERFORM.
		 * 
		 * Read through the HITRs for this Coverage key/tranno using LIFO on
		 * logical and delete those written by above routine.
		 */

		hitrrevIO.chdrcoy.set(chdrmjaIO.getChdrcoy());
		hitrrevIO.chdrnum.set(chdrmjaIO.getChdrnum());
		hitrrevIO.tranno.set(wsaaTranno);
		hitrrevIO.function.set(varcom.begn);
		hitrrevIO.format.set(hitrrevrec);

		while (!(isEQ(hitrrevIO.getStatuz(), varcom.endp))) {
			SmartFileCode.execute(appVars, hitrrevIO);
			if (isNE(hitrrevIO.getStatuz(), varcom.oK) && isNE(hitrrevIO.getStatuz(), varcom.endp)) {
				drylogrec.statuz.set(hitrrevIO.getStatuz());
				drylogrec.params.set(hitrrevIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}
			if (isNE(hitrrevIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
					|| (isNE(hitrrevIO.getChdrnum(), chdrmjaIO.getChdrnum()))
					|| (isNE(hitrrevIO.getLife(), covrmjaIO.getLife()))
					|| (isNE(hitrrevIO.getCoverage(), covrmjaIO.getCoverage()))
					|| (isNE(hitrrevIO.getRider(), covrmjaIO.getRider()))
					|| (isNE(hitrrevIO.getPlanSuffix(), covrmjaIO.getPlanSuffix()))
					|| (isNE(hitrrevIO.getTranno(), wsaaTranno)) && isNE(hitrrevIO.getStatuz(), varcom.endp)) {
				hitrrevIO.setStatuz(varcom.endp);
			}

			/*
			 * When we have reached those previously written by this program,
			 * stop.
			 */
			if (isEQ(hitrrevIO.getCovdbtind(), 'D')) {
				hitrrevIO.statuz.set(varcom.endp);
			}

			if (!isEQ(hitrrevIO.getStatuz(), varcom.endp)) {
				hitrrevIO.function.set(varcom.readh);
				SmartFileCode.execute(appVars, hitrrevIO);
				if (!isEQ(hitrrevIO.getStatuz(), varcom.oK)) {
					drylogrec.statuz.set(hitrrevIO.getStatuz());
					drylogrec.params.set(hitrrevIO.getParams());
					drylogrec.dryDatabaseError.setTrue();
					a000FatalError();
				}

				hitrrevIO.function.set(varcom.delet);
				SmartFileCode.execute(appVars, hitrrevIO);
				if (!isEQ(hitrrevIO.getStatuz(), varcom.oK)) {
					drylogrec.statuz.set(hitrrevIO.getStatuz());
					drylogrec.params.set(hitrrevIO.getParams());
					drylogrec.dryDatabaseError.setTrue();
					a000FatalError();
				}
				hitrrevIO.function.set(varcom.nextr);
			}
		}
		

		if (isEQ(t6597rec.cpstat04, SPACES) && isEQ(t6597rec.crstat04, SPACES)) {
			goTo(GotoLabel.exit2159);
		}

		covrmjaIO.setPstatcode(t6597rec.cpstat04);
		covrmjaIO.setStatcode(t6597rec.crstat04);
	}

	// ------ NHUT
	
	// ------> DUC

	protected void writeNewCovr2160()
	{
		start2161();
	}

	protected void start2161() {
		covrmjaIO.setCurrfrom(chdrmjaIO.getBtdate());
		covrmjaIO.setCurrto(varcom.vrcmMaxDate);
		wsaaCovrmjaDataKey.set(covrmjaIO.getDataKey());
		covrmjaIO.setFormat(covrmjarec);
		covrmjaIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, covrmjaIO);

		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}


	protected void repositionCovr2170() {
		start2171();
	}
	/*Find if there are any riders attached to this coverage. If
	      * there are then loop back to 3471-NEXT and lapse them. If there
	      * are no more riders for this coverage then check the contract.
	*/
	protected void start2171() {
		wsaaCovrmjaStatuz.set(SPACES);
		covrmjaIO.setDataKey(wsaaCovrmjaDataKey);
		covrmjaIO.setFunction(varcom.begn);
		covrmjaIO.setFormat(covrmjarec);
		SmartFileCode.execute(appVars, covrmjaIO);

		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}


	protected void readNextCovr2180() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					nextr2181();
				case nextr2181:
					nextr2181();
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void nextr2181() {
		covrmjaIO.setFunction(varcom.nextr);

		SmartFileCode.execute(appVars, covrmjaIO);

		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
				&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			wsaaCovrmjaStatuz.set(varcom.endp);

			if (isNE(covrmjaIO.getChdrcoy(), covruddIO.getChdrcoy())
					|| isNE(covrmjaIO.getChdrnum(), covruddIO.getChdrnum())
					|| !isNE(covrmjaIO.getLife(), covruddIO.getLife())
					|| !isNE(covrmjaIO.getCoverage(), covruddIO.getCoverage())) {
				wsaaCovrmjaStatuz.set(varcom.endp);
			}
			/*
			 *  * If this record is not the same Plan Suffix then bypass and go
			 * to the next one.
			 */
			if (isNE(covrmjaIO.getPlanSuffix(), covruddIO.getPlanSuffix())
					&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
				goTo(GotoLabel.nextr2181);
				return;
			}

		}
	}

	protected void lapseWritePtrn2190()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					ptrn2191();
				case exit2194:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void ptrn2191() {
		if (ptrnWritten.isTrue()) {
			goTo(GotoLabel.exit2194);
		}
		ptrnIO.setParams(SPACES);
		ptrnIO.setUser(999999);
		ptrnIO.setTermid(SPACES);
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate);
		//ILPI-2040
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setValidflag("1");
		ptrnIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrmjaIO.getChdrnum());
		ptrnIO.setTranno(wsaaLapseTranno);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypSystParm[1]);
		ptrnIO.setTransactionDate(wsaaDate);
		ptrnIO.setTransactionTime(getCobolTime());
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if (isNE(ptrnIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct11);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		wsaaPtrn.set("Y");
	}

	protected void lapseUpdateChdr2195() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					update2196();
					updatePayr2196();
				case writeNewContractHeader2196:
					writeNewContractHeader2196();
				case writeRecord2196:	
					writeRecord2196();
				case exit2197:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

/*    Initialise flags for loop processing.*/

	private void update2196() {

		if (chdrLapsed.isTrue()) {
			goTo(GotoLabel.exit2197);
		}

		wsaaValidPstatcode.set("Y");

		while (!(endOfCovr.isTrue())) {
			checkChdrPaidup2198();
		}

		if (validStatuz.isTrue()) {
			wsaaChdrLapsed.set("Y");
		}

		if (chdrWritten.isTrue()) {
			update2199();
			goTo(GotoLabel.exit2197);
		}
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.chdrcoy.set(ptrnIO.getChdrcoy());
		chdrmjaIO.chdrnum.set(ptrnIO.getChdrnum());
		chdrmjaIO.function.set(varcom.readh);
		chdrmjaIO.format.set(chdrmjarec);

		SmartFileCode.execute(appVars, chdrmjaIO);

		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrmjaIO.getStatuz());
			drylogrec.params.set(chdrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

  /** Rewrite the original contract header for historical purposes.*/

		chdrmjaIO.validflag.set("2");

  /** Write the CURRTO and CURRFROM dates for valid flag '2'
  * records on the CHDR File*/

		chdrmjaIO.currto.set(chdrmjaIO.getBtdate());

		if (isGT(wsaaOccdate, wsaaCurrfrom)) {
			chdrmjaIO.currfrom.set(wsaaOccdate);
		} else {
			chdrmjaIO.currfrom.set(wsaaCurrfrom);
		}

		chdrmjaIO.function.set(varcom.rewrt);
		chdrmjaIO.format.set(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrmjaIO.getStatuz());
			drylogrec.params.set(chdrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}


    /*
    * Update current PAYR record with valid flag = '2' and write a
    * new PAYR record with valid flag = '1' and effective date is
    * the old BTDATE.
    */
	protected void updatePayr2196()	{
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(ptrnIO.getChdrcoy());
		payrIO.setChdrnum(ptrnIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);

		if(isNE(payrIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

	      /*
	      * Rewrite the Current PAYR record as Valid flag 2.
	      */
		payrIO.setValidflag(2);
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);

		if(isNE(payrIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if(isEQ(chdrmjaIO.getBillfreq(),"00")
				||isEQ(chdrmjaIO.getBillfreq(),"  ")){
			goTo(GotoLabel.writeNewContractHeader2196);
		}

		if(chdrLapsed.isTrue()){
//			chdrmjaIO.sinstamt01.set(ZERO);
//			chdrmjaIO.sinstamt02.set(ZERO);
//			chdrmjaIO.sinstamt03.set(ZERO);
//			chdrmjaIO.sinstamt04.set(ZERO);
//			chdrmjaIO.sinstamt05.set(ZERO);
//			chdrmjaIO.sinstamt06.set(ZERO);
//			payrIO.setSinstamt01(ZERO);
//			payrIO.setSinstamt02(ZERO);
//			payrIO.setSinstamt03(ZERO);
//			payrIO.setSinstamt04(ZERO);
//			payrIO.setSinstamt05(ZERO);
//			payrIO.setSinstamt06(ZERO);
			goTo(GotoLabel.writeNewContractHeader2196);
		}

		if(isNE(chdrmjaIO.sinstamt01,wsaaTotCovrpuInstprem)){
			/*NEXT_SENTENCE*/
		}else{
			chdrmjaIO.sinstamt02.set(ZERO);
		}
		if(isNE(payrIO.getSinstamt01(),wsaaTotCovrpuInstprem )){
			/*NEXT_SENTENCE*/
		}else{
			payrIO.setSinstamt02(ZERO);
		}

		chdrmjaIO.setSinstamt01(sub(chdrmjaIO.getSinstamt01(), wsaaTotCovrpuInstprem));
		payrIO.setSinstamt01(sub(payrIO.getSinstamt01(), wsaaTotCovrpuInstprem));
		chdrmjaIO.setSinstamt06(add(add(add(add(chdrmjaIO.getSinstamt01(), chdrmjaIO.getSinstamt02()), chdrmjaIO.getSinstamt03()), chdrmjaIO.getSinstamt04()), chdrmjaIO.getSinstamt05()));
		payrIO.setSinstamt06(add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05()));

		chdrmjaIO.insttot01.set(0);
		chdrmjaIO.insttot02.set(0);
		chdrmjaIO.insttot03.set(0);
		chdrmjaIO.insttot04.set(0);
		chdrmjaIO.insttot05.set(0);
		chdrmjaIO.insttot06.set(0);
	}

	protected void writeNewContractHeader2196() {
		chdrmjaIO.tranno.set(wsaaLapseTranno);
		chdrmjaIO.validflag.set("1");
     /*
      * Write the CURRTO and CURRFROM dates for valid flag '1'
      * records on the CHDR File
      */
		chdrmjaIO.currto.set(99999999);
		chdrmjaIO.currfrom.set(chdrmjaIO.getBtdate());

		if (chdrLapsed.isTrue()) {
			chdrmjaIO.pstatcode.set(t6597rec.cpstat04);
			chdrmjaIO.statcode.set(t6597rec.crstat04);
		}

		payrIO.setPstatcode(chdrmjaIO.getPstatcode());
		payrIO.setValidflag("1");
		payrIO.setTranno(chdrmjaIO.getTranno());
		payrIO.setEffdate(chdrmjaIO.getCurrfrom());
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			a000FatalError();
		}
		
	}

	protected void writeRecord2196()
	{
		chdrmjaIO.function.set(varcom.writr);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if(isNE(chdrmjaIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(chdrmjaIO.getStatuz());
			drylogrec.params.set(chdrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

//		payrIO.setEffdate(payrIO.getPtdate());
//		payrIO.setTranno(wsaaLapseTranno);
//		payrIO.setValidflag("1");
//		payrIO.setPstatcode(chdrmjaIO.getPstatcode());
//		payrIO.setFormat(payrrec);
//		payrIO.setFunction(varcom.writr);
//		SmartFileCode.execute(appVars, payrIO);
//
//		if(isNE(payrIO.getStatuz(),varcom.oK)){
//			drylogrec.params.set(payrIO.getParams());
//			drylogrec.statuz.set(payrIO.getStatuz());
//			drylogrec.dryDatabaseError.setTrue();
//			a000FatalError();
//		}
		wsaaChdrWritten.set("Y");
		transferCovrTranno6395();
	}
	
	protected void transferCovrTranno6395()
	{
		startCovr6395();
		callCovrmja6395();
	}

	protected void startCovr6395()
	{
		covrmjaIO.setDataKey(SPACES);
		covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
		covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
		covrmjaIO.setPlanSuffix(ZERO);
		covrmjaIO.setFunction(varcom.begnh);
	}

	protected void callCovrmja6395()
	{
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)
		&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			a000FatalError();
		}
		if (isEQ(covrmjaIO.getStatuz(), varcom.endp)) {
			return ;
		}
		if (isNE(covrmjaIO.getChdrcoy(), chdrmjaIO.getChdrcoy())
		|| isNE(covrmjaIO.getChdrnum(), chdrmjaIO.getChdrnum())) {
			covrmjaIO.setFunction(varcom.rewrt);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
				drylogrec.params.set(covrmjaIO.getParams());
				drylogrec.statuz.set(covrmjaIO.getStatuz());
				a000FatalError();
			}
			return ;
		}
		if (isEQ(covrmjaIO.getTranno(), wsaaTranno)) {
			covrmjaIO.setTranno(wsaaLapseTranno);
		}
		covrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, covrmjaIO);
		if (isNE(covrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(covrmjaIO.getParams());
			drylogrec.statuz.set(covrmjaIO.getStatuz());
			a000FatalError();
		}
		covrmjaIO.setFunction(varcom.nextr);
		callCovrmja6395();
		return ;
	}

	protected void checkChdrPaidup2198()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2198();
				case checkStatuz2198:
					checkStatuz2198();
				case exit2198:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

		protected void start2198()
		{
			wsaaValidStatuz.set("Y");
			covrmjaIO.setDataArea(SPACES);
			covrmjaIO.setChdrcoy(chdrmjaIO.getChdrcoy());
			covrmjaIO.setChdrnum(chdrmjaIO.getChdrnum());
			covrmjaIO.setPlanSuffix(ZERO);
			covrmjaIO.setFunction(varcom.begn);
			covrmjaIO.setFormat(covrmjarec);
			SmartFileCode.execute(appVars, covrmjaIO);
			if (isNE(covrmjaIO.getStatuz(), varcom.oK)
			&& isNE(covrmjaIO.getStatuz(), varcom.endp)) {
				drylogrec.statuz.set(covrmjaIO.getStatuz());
				drylogrec.params.set(covrmjaIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}

			if(isEQ(covrmjaIO.getStatuz(),varcom.endp)
					||isNE(chdrmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
					||isNE(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum())){
				wsaaEndOfCovr.set("Y");
				goTo(GotoLabel.exit2198);
			}
		}

	      /*
	      * Previous IF statement replaced. Checking should be carried
	      * out as 'VALID' by default and then look for invalid status
	      * on each of the components.
	      */
		protected void checkStatuz2198()
		{
			wsaaEndOfCovr.set("N");

			if(isNE(t6597rec.cpstat04, SPACES)
					&& isNE(t6597rec.crstat04, SPACES)
					&& (isNE(t6597rec.crstat04, covrmjaIO.getStatcode()) 
							|| isNE(t6597rec.cpstat04,covrmjaIO.getPstatcode()))) {
				wsaaValidStatuz.set("N");
			}

			if(validStatuz.isTrue()){
				/*NEXT SENTENCE*/
			}else{
				wsaaEndOfCovr.set("Y");
				goTo(GotoLabel.exit2198);
			}

			covrmjaIO.setFunction(varcom.nextr);

			SmartFileCode.execute(appVars, covrmjaIO);
//ILPI-168
			if(isNE(covrmjaIO.getStatuz(),varcom.oK)
					&& isNE(covrmjaIO.getStatuz(),varcom.endp)){
				drylogrec.statuz.set(covrmjaIO.getStatuz());
				drylogrec.params.set(covrmjaIO.getParams());
				drylogrec.dryDatabaseError.setTrue();
				a000FatalError();
			}

			if(isEQ(covrmjaIO.getStatuz(),varcom.endp)
					||isNE(chdrmjaIO.getChdrcoy(),covrmjaIO.getChdrcoy())
					||isNE(chdrmjaIO.getChdrnum(),covrmjaIO.getChdrnum())){
				wsaaEndOfCovr.set("Y");
			}else{
				goTo(GotoLabel.checkStatuz2198);
			}
		}

	protected void update2199()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					start2199();
				case rewrite2199:
					rewrite2199();
				case writeRecord2199:
					writeRecord2199();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	private void start2199()
	{
		chdrmjaIO.setDataArea(SPACES);
		chdrmjaIO.setChdrcoy(ptrnIO.getChdrcoy());
		chdrmjaIO.setChdrnum(ptrnIO.getChdrnum());
		chdrmjaIO.setFunction(varcom.readh);
		chdrmjaIO.setFormat(chdrmjarec);
		SmartFileCode.execute(appVars, chdrmjaIO);
		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrmjaIO.getStatuz());
			drylogrec.params.set(chdrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		/* Update the PAYR record. */
		payrIO.setDataArea(SPACES);
		payrIO.setChdrcoy(ptrnIO.getChdrcoy());
		payrIO.setChdrnum(ptrnIO.getChdrnum());
		payrIO.setPayrseqno(1);
		payrIO.setFormat(payrrec);
		payrIO.setFunction(varcom.readh);
		SmartFileCode.execute(appVars, payrIO);
		if(isNE(payrIO.getStatuz(),varcom.oK)){
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		if (isEQ(chdrmjaIO.getBillfreq(), "00")
				|| isEQ(chdrmjaIO.getBillfreq(), "  ")) {
					goTo(GotoLabel.rewrite2199);
				}
	if(chdrLapsed.isTrue()){
		chdrmjaIO.sinstamt01.set(ZERO);
		chdrmjaIO.sinstamt02.set(ZERO);
		chdrmjaIO.sinstamt03.set(ZERO);
		chdrmjaIO.sinstamt04.set(ZERO);
		chdrmjaIO.sinstamt05.set(ZERO);
		chdrmjaIO.sinstamt06.set(ZERO);
		payrIO.setSinstamt01(ZERO);
		payrIO.setSinstamt02(ZERO);
		payrIO.setSinstamt03(ZERO);
		payrIO.setSinstamt04(ZERO);
		payrIO.setSinstamt05(ZERO);
		payrIO.setSinstamt06(ZERO);
		goTo(GotoLabel.rewrite2199);
	}
	if (isNE(chdrmjaIO.getSinstamt01(), wsaaTotCovrpuInstprem)) {
		/*NEXT_SENTENCE*/
	}
	else{
		chdrmjaIO.setSinstamt02(ZERO);
	}
		compute(chdrmjaIO.sinstamt01, 2).set(sub(chdrmjaIO.getSinstamt01(), wsaaTotCovrpuInstprem));
		compute(payrIO.sinstamt01, 2).set(sub(payrIO.getSinstamt01(), wsaaTotCovrpuInstprem));
		compute(chdrmjaIO.sinstamt06, 2).set(add(add(add(add(chdrmjaIO.getSinstamt01(), chdrmjaIO.getSinstamt02()), chdrmjaIO.getSinstamt03()), chdrmjaIO.getSinstamt04()), chdrmjaIO.getSinstamt05()));
		compute(payrIO.sinstamt05, 2).set(add(add(add(add(payrIO.getSinstamt01(), payrIO.getSinstamt02()), payrIO.getSinstamt03()), payrIO.getSinstamt04()), payrIO.getSinstamt05()));
		chdrmjaIO.setInsttot01(0);
		chdrmjaIO.setInsttot02(0);
		chdrmjaIO.setInsttot03(0);
		chdrmjaIO.setInsttot04(0);
		chdrmjaIO.setInsttot05(0);
		chdrmjaIO.setInsttot06(0);
	}


	protected void rewrite2199()
	{
		if (chdrLapsed.isTrue()) {
			/*NEXT_SENTENCE*/
		}
		else{
			goTo(GotoLabel.writeRecord2199);
		}
		chdrmjaIO.pstatcode.set(t6597rec.cpstat04);
		chdrmjaIO.statcode.set(t6597rec.crstat04);
	}


	protected void writeRecord2199()
	{
		chdrmjaIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrmjaIO);

		if (isNE(chdrmjaIO.getStatuz(), varcom.oK)) {
			drylogrec.statuz.set(chdrmjaIO.getStatuz());
			drylogrec.params.set(chdrmjaIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}

		/* Update the PAYR record. */
		payrIO.setPstatcode(chdrmjaIO.getPstatcode());
		payrIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, payrIO);
		if (isNE(payrIO.getStatuz(), varcom.oK)) {
			drylogrec.params.set(payrIO.getParams());
			drylogrec.statuz.set(payrIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}

	protected void errorOption2200() {
		start2210();
	}

	protected void start2210() {
		//ILPI-168
		dryrDryrptRecInner.dryrReportName.set("R5107");
		dryrDryrptRecInner.r5107Rectype.set("COVR");
		dryrDryrptRecInner.r5107Chdrnum.set(covruddIO.getChdrnum());
		dryrDryrptRecInner.r5107Life.set(covruddIO.getLife());
		dryrDryrptRecInner.r5107Coverage.set(covruddIO.getCoverage());
		dryrDryrptRecInner.r5107Rider.set(covruddIO.getRider());
		dryrDryrptRecInner.r5107Plnsfx.set(covruddIO.getPlanSuffix());
		dryrDryrptRecInner.r5107Vrtfnd.set(SPACES);
		dryrDryrptRecInner.r5107Unityp.set(SPACES);
		dryrDryrptRecInner.r5107Tranno.set(utrnaloIO.tranno);
		dryrDryrptRecInner.r5107Batctrcde.set(SPACES);
		dryrDryrptRecInner.r5107Cntamnt.set(covruddIO.getCoverageDebt());
		dryrDryrptRecInner.r5107Cntcurr.set(chdrlifIO.getCntcurr());
		dryrDryrptRecInner.r5107Fundamnt.set(ZERO);
		dryrDryrptRecInner.r5107Fndcurr.set(SPACES);
		dryrDryrptRecInner.r5107Fundrate.set(ZERO);
		dryrDryrptRecInner.r5107Age.set(datcon3rec.freqFactor);
		dryrDryrptRecInner.r5107Triger.set("UNPAID DBT");
		
		dryrDryrptRecInner.r5107vrtfnd.set(SPACES);
		dryrDryrptRecInner.r5107unityp.set(SPACES);
		dryrDryrptRecInner.r5107chdrnum.set(covruddIO.getChdrnum());
		dryrDryrptRecInner.r5107batctrcde.set(SPACES);
		dryrptrec.sortkey.set(dryrDryrptRecInner.r5107R5107Key);
		
		

	   /*   * Write the DRPT record.  */
		e000ReportRecords();
		drycntrec.contotNumber.set(ct19);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
		//ILIFE-2040
//		drypDryprcRecInner.processUnsuccesful.setTrue();
	}

	protected void sellUnits2300()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					sellUnits2310();
				case initials2320:
					initials2320();
				case exit2390:
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void sellUnits2310() {
		drycntrec.contotNumber.set(ct02);
		drycntrec.contotValue.set(1);
		d000ControlTotals();

		if(isLTE(wsaaValAcumUnitsP,ZERO)){
			goTo(GotoLabel.initials2320);
		}
		if (isGTE(wsaaValAcumUnitsP, covruddIO.getCoverageDebt())) {
			wsaaSurrAmount.set(covruddIO.getCoverageDebt());
			covruddIO.setCoverageDebt(ZERO);
		}
		else {
			wsaaSurrAmount.set(wsaaValAcumUnitsP);
			compute(covruddIO.getCoverageDebt(), 6).setRounded(sub(covruddIO.getCoverageDebt(), wsaaSurrAmount));
		}
		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if(isNE(wsaaUtrsUnitType[iu.toInt()],"I")){
				compute(wsaaSurrPercent, 12).setRounded(div(wsaaUtrsCovFundAmtP[iu.toInt()], wsaaValAcumUnitsP));
				compute(wsaaUtrsSurrAmt[iu.toInt()], 12).setRounded(mult(wsaaSurrPercent, wsaaSurrAmount));
			}
		}
		if(isEQ(covruddIO.getCoverageDebt(),ZERO)){
			goTo(GotoLabel.exit2390);
		}
	}

	protected void initials2320() {
		if (isLTE(wsaaValInitUnitsP, ZERO)) {
			goTo(GotoLabel.exit2390);
		}

		if(isNE(t5540rec.unitCancInit,"Y")) {
			goTo(GotoLabel.exit2390);
		}

		if (isGTE(wsaaValInitUnitsP, covruddIO.getCoverageDebt())) {
			wsaaSurrAmount.set(covruddIO.getCoverageDebt());
			covruddIO.setCoverageDebt(ZERO);

		} else {
			wsaaSurrAmount.set(wsaaValInitUnitsP);
			compute(covruddIO.getCoverageDebt(), 6).setRounded(sub(covruddIO.getCoverageDebt(), wsaaSurrAmount));
		}

		for (iu.set(1); !(isEQ(wsaaUtrsRrn[iu.toInt()], 0)); iu.add(1)){
			if (isEQ(wsaaUtrsUnitType[iu.toInt()], "I")) {
				compute(wsaaSurrPercent, 12).setRounded(div(wsaaUtrsCovFundAmtP[iu.toInt()], wsaaValInitUnitsP));
				compute(wsaaUtrsSurrAmt[iu.toInt()], 12).setRounded(mult(wsaaSurrPercent, wsaaSurrAmount));
			}
		}
	}

	protected void updateChdr2400(){
		update2410();
	}

	protected void update2410()
	{
		chdrlifIO.setTranno(wsaaTranno);
		chdrlifIO.setFunction(varcom.rewrt);
		SmartFileCode.execute(appVars, chdrlifIO);
		if(isNE(chdrlifIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(chdrlifIO.getStatuz());
			drylogrec.params.set(chdrlifIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		wsaaChdrUpdated.set("Y");
	}

	protected void writePtrn2500()
	{
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					write2510();
				}
				break;
			}
			catch (GOTOException e){
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void write2510()
	{
		
		ptrnIO.setParams(SPACES);
		ptrnIO.setUser(999999);
		ptrnIO.setTermid(SPACES);
		ptrnIO.setBatcpfx(drypDryprcRecInner.drypBatcpfx);
		ptrnIO.setBatccoy(drypDryprcRecInner.drypBatccoy);
		ptrnIO.setBatcbrn(drypDryprcRecInner.drypBatcbrn);
		ptrnIO.setBatcactyr(drypDryprcRecInner.drypBatcactyr);
		ptrnIO.setBatcactmn(drypDryprcRecInner.drypBatcactmn);
		ptrnIO.setBatcbatch(drypDryprcRecInner.drypBatcbatch);
		ptrnIO.setPtrneff(drypDryprcRecInner.drypRunDate);
		ptrnIO.setValidflag("1");
		ptrnIO.setChdrcoy(chdrlifIO.getChdrcoy());
		ptrnIO.setChdrnum(chdrlifIO.getChdrnum());
		ptrnIO.setTranno(wsaaTranno);
		ptrnIO.setBatctrcde(drypDryprcRecInner.drypBatctrcde);
		ptrnIO.setDatesub(drypDryprcRecInner.drypRunDate);
		ptrnIO.setTransactionDate(wsaaDate);
		ptrnIO.setTransactionTime(getCobolTime());
		ptrnIO.setCrtuser(username); //IJS-523
		ptrnIO.setFormat(ptrnrec);
		ptrnIO.setFunction(varcom.writr);
		SmartFileCode.execute(appVars, ptrnIO);
		if(isNE(ptrnIO.getStatuz(),varcom.oK)){
			drylogrec.statuz.set(ptrnIO.getStatuz());
			drylogrec.params.set(ptrnIO.getParams());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
		drycntrec.contotNumber.set(ct24);
		drycntrec.contotValue.set(1);
		d000ControlTotals();
	}
	
	protected void fillParams3000(){
		drypDryprcRecInner.drypCnttype.set(chdrlifIO.getCnttype());
	}
	
	private void updateZrstA100() {

		SmartFileCode.execute(appVars, zrstIO);

		if (!isEQ(zrstIO.statuz, varcom.oK) && !isEQ(zrstIO.statuz, varcom.endp)) {
			drylogrec.statuz.set(zrstIO.statuz);
			drylogrec.params.set(zrstIO.getParams());
			a000FatalError();

		}

		if (!isEQ(zrstIO.chdrcoy, covruddIO.getChdrcoy()) || !isEQ(zrstIO.chdrnum, covruddIO.getChdrnum())
				|| !isEQ(zrstIO.life, covruddIO.life) || !isEQ(zrstIO.coverage, covruddIO.coverage)
				|| isEQ(zrstIO.statuz, varcom.endp)) {
			zrstIO.statuz.set(varcom.endp);
			return;

		}

		if (isEQ(zrstIO.feedbackInd, SPACES)) {
			zrstIO.xtranno.set(wsaaTranno);
			zrstIO.feedbackInd.set("Y");
			zrstIO.function.set(varcom.writd);
			zrstIO.format.set(zrstrec);
			SmartFileCode.execute(appVars, zrstIO);
			if (!isEQ(zrstIO.statuz, varcom.oK)) {
				drylogrec.statuz.set(zrstIO.statuz);
				drylogrec.params.set(zrstIO.getParams());
				a000FatalError();
			}
		}
		zrstIO.function.set(varcom.nextr);
	}
	
	protected void createNewZrstB100(){
		b100Start();
	}
	
	protected void b100Start(){
		zrsttrnIO.setParams(SPACES);
		zrsttrnIO.setChdrcoy(covruddIO.getChdrcoy());
		zrsttrnIO.setChdrnum(covruddIO.getChdrnum());
		zrsttrnIO.setLife(covruddIO.getLife());
		zrsttrnIO.setCoverage(covruddIO.getCoverage());
		zrsttrnIO.setXtranno(wsaaTranno);
		zrsttrnIO.setFormat(zrsttrnrec);
		zrsttrnIO.setFunction(varcom.begn);
		SmartFileCode.execute(appVars, zrsttrnIO);
		if (isNE(zrsttrnIO.getStatuz(), varcom.oK)
				&& isNE(zrsttrnIO.getStatuz(), varcom.endp)) {
			drylogrec.statuz.set(zrsttrnIO.getStatuz());
			drylogrec.params.set(zrsttrnIO.getParams());
			a000FatalError();
		}
		
		if (isNE(zrsttrnIO.getChdrcoy(), covruddIO.getChdrcoy())
				|| isNE(zrsttrnIO.getChdrnum(), covruddIO.getChdrnum())
				|| isNE(zrsttrnIO.getLife(), covruddIO.getLife())
				|| isNE(zrsttrnIO.getCoverage(), covruddIO.getCoverage())
				|| isNE(zrsttrnIO.getXtranno(), wsaaTranno)
				|| isEQ(zrsttrnIO.getStatuz(), varcom.endp)) {
					return;
				}
			
		while ( !(isEQ(zrsttrnIO.getStatuz(), varcom.endp))) {
			writeZrstB200();
		}
	}
	
	protected void writeZrstB200(){
		b200Start();
	}
	protected void b200Start(){
		if (isEQ(zrsttrnIO.getFeedbackInd(), "Y")) {
			zrstIO.setChdrcoy(zrsttrnIO.getChdrcoy());
			zrstIO.setChdrnum(zrsttrnIO.getChdrnum());
			zrstIO.setLife(zrsttrnIO.getLife());
			zrstIO.setJlife(zrsttrnIO.getJlife());
			zrstIO.setCoverage(zrsttrnIO.getCoverage());
			zrstIO.setRider(zrsttrnIO.getRider());
			zrstIO.setSeqno(zrsttrnIO.getSeqno());
			zrstIO.setTranno(zrsttrnIO.getTranno());
			zrstIO.setBatctrcde(zrsttrnIO.getBatctrcde());
			zrstIO.setSacstyp(zrsttrnIO.getSacstyp());
			zrstIO.setZramount01(zrsttrnIO.getZramount01());
			zrstIO.setZramount02(zrsttrnIO.getZramount02());
			zrstIO.setUstmno(zrsttrnIO.getUstmno());
			zrstIO.setTrandate(drypDryprcRecInner.drypRunDate);
			zrstIO.setFeedbackInd(SPACES);
			zrstIO.setXtranno(ZERO);
			zrstIO.setFunction(varcom.writr);
			zrstIO.setFormat(zrstrec);
			SmartFileCode.execute(appVars, zrstIO);
			if(isNE(zrstIO.statuz,varcom.oK))
			{
				drylogrec.statuz.set(zrstIO.statuz);
				drylogrec.params.set(zrstIO.getParams());
				a000FatalError();
			}
		}
		zrsttrnIO.setFunction(varcom.nextr);
		SmartFileCode.execute(appVars, zrsttrnIO);
	
	if(isNE(zrsttrnIO.getStatuz(),varcom.oK)&&isNE(zrsttrnIO.getStatuz(),varcom.endp)){
			drylogrec.statuz.set(zrsttrnIO.statuz);
			drylogrec.params.set(zrsttrnIO.getParams());
			a000FatalError();
	
	}
	
	
	if (isNE(zrsttrnIO.getChdrcoy(), covruddIO.getChdrcoy())
			|| isNE(zrsttrnIO.getChdrnum(), covruddIO.getChdrnum())
			|| isNE(zrsttrnIO.getLife(), covruddIO.getLife())
			|| isNE(zrsttrnIO.getCoverage(), covruddIO.getCoverage())
			|| isNE(zrsttrnIO.getXtranno(), wsaaTranno)
			|| isEQ(zrsttrnIO.getStatuz(), varcom.endp)) {
				zrsttrnIO.setStatuz(varcom.endp);
			}
	}
	
	protected void x1000CallXcvrt() {
		conlinkrec.amountOut.set(ZERO);
		conlinkrec.company.set(drypDryprcRecInner.drypCompany);
		callProgram(Xcvrt.class, conlinkrec.clnk002Rec);
		if (isNE(conlinkrec.statuz, varcom.oK)) {
			drylogrec.statuz.set(conlinkrec.statuz);
			drylogrec.params.set(conlinkrec.clnk002Rec);
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();
		}
	}
	
	// ------> DUC
	
	/*
	 * Class transformed  from Data Structure DRYR-DRYRPT-REC--INNER
	 */
	private static final class DryrDryrptRecInner {
		private FixedLengthStringData dryrReportName = new FixedLengthStringData(10);
		private FixedLengthStringData dryrSortkey = new FixedLengthStringData(38);
		private FixedLengthStringData dryrGenarea = new FixedLengthStringData(500);
	
		private FixedLengthStringData r5135DataArea = new FixedLengthStringData(500).isAPartOf(dryrGenarea, 0, REDEFINE);
		private FixedLengthStringData r5135Chdrnum = new FixedLengthStringData(8).isAPartOf(r5135DataArea, 0);
		private FixedLengthStringData r5135Life = new FixedLengthStringData(2).isAPartOf(r5135DataArea, 8);
		private FixedLengthStringData r5135Coverage = new FixedLengthStringData(2).isAPartOf(r5135DataArea, 10);
		private FixedLengthStringData r5135Rider = new FixedLengthStringData(2).isAPartOf(r5135DataArea, 12);
		private FixedLengthStringData r5135Crtable = new FixedLengthStringData(4).isAPartOf(r5135DataArea, 14);
		private ZonedDecimalData r5135PlanSuffix = new ZonedDecimalData(4, 0).isAPartOf(r5135DataArea, 18);
		private ZonedDecimalData r5135Oldsum = new ZonedDecimalData(13, 2).isAPartOf(r5135DataArea, 22);
		private ZonedDecimalData r5135Newsumi = new ZonedDecimalData(13, 2).isAPartOf(r5135DataArea, 35);
		private ZonedDecimalData r5135Oldinst = new ZonedDecimalData(13, 2).isAPartOf(r5135DataArea, 48);
		private ZonedDecimalData r5135Newinst = new ZonedDecimalData(13, 2).isAPartOf(r5135DataArea, 61);
		private FixedLengthStringData r5135Rcesdte = new FixedLengthStringData(10).isAPartOf(r5135DataArea, 74);
		private FixedLengthStringData r5135Currency = new FixedLengthStringData(3).isAPartOf(r5135DataArea, 84);
		private FixedLengthStringData r5135ReportName = new FixedLengthStringData(10).init("R5135     ");
	}
	/*
	 * Class transformed  from Data Structure DRYP-DRYPRC-REC--INNER
	 */
	private static final class DrypDryprcRecInner {
	
		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypBillfreq = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 3);
		private FixedLengthStringData drypBillchnl = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 5);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypBtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 11);
		private PackedDecimalData drypPtdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 16);
		private PackedDecimalData drypBillcd = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 21);
		private PackedDecimalData drypOccdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 46);
	}
}