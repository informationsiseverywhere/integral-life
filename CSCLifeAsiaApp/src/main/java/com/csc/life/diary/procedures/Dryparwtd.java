package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.REDEFINE;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;

import com.csc.diary.procedures.Drylog;
import com.csc.diary.recordstructures.Drylogrec;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.unitlinkedprocessing.dataaccess.ZrpwTableDAM;
import com.csc.life.unitlinkedprocessing.recordstructures.Zrpwkey;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

public class Dryparwtd extends Maind {
    /**
     *(c) Copyright Continuum Corporation Ltd.  1986....1995.
     *    All rights reserved.  Continuum Confidential.
     *
     *REMARKS.
     *
     * This is the transaction detail record change subroutine for
     * Partial Withdrawl transactions.
     *
     ***********************************************************************
     *                                                                     *
     * ......... New Version of the Amendment History.                     *
     *                                                                     *
     ***********************************************************************
     *           AMENDMENT  HISTORY                                        *
     ***********************************************************************
     * DATE.... VSN/MOD  WORK UNIT    BY....                               *
     *                                                                     *
     * 16/02/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
     *           Initial Version                                           *
     *                                                                     *
     **DD/MM/YY*************************************************************
     */
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYPRTWTD");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	private FixedLengthStringData wsaaZrpw = new FixedLengthStringData(1).init("N");
	
	private Validator zrpwFound = new Validator(wsaaZrpw, "Y");
	private Validator noZrpwFound = new Validator(wsaaZrpw, "N");
	
	private static final String zrpwrec = "ZRPWREC";
	
	private Zrpwkey wsaaZrpwKey = new Zrpwkey();
	private ZrpwTableDAM zrpwIO = new ZrpwTableDAM();
	
	private Varcom varcom = new Varcom();
	
	private Drylogrec drylogrec = new Drylogrec();
	
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();

	protected enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr280, 
		exit290
	}

	public Dryparwtd() {
		super();
	}

	public void mainline(Object... parmArray) {
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			mainLine000();
		} catch (COBOLExitProgramException e) {
		}
	}
	
	protected void mainLine000(){
		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		noZrpwFound.setTrue();
		/* Determine if a Partial Withdrawl SI update transaction is needed.
		* Check the ZRPW.
		*/
		checkZrpw100();
		if(zrpwFound.isTrue()){
			
		drypDryprcRecInner.drypNxtprcdate.set(drypDryprcRecInner.drypRunDate);
		drypDryprcRecInner.drypNxtprctime.set(ZERO);
		} else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
		exitProgram();
	}

	protected void checkZrpw100() {
		check100();
	}

	private void check100() {

		zrpwIO.setParams(SPACES);
		zrpwIO.setRecKeyData(SPACES);
		zrpwIO.setRecNonKeyData(SPACES);
		zrpwIO.chdrcoy.set(drypDryprcRecInner.drypCompany);
		zrpwIO.chdrnum.set(drypDryprcRecInner.drypEntity);
		zrpwIO.tranno.set(ZERO);
		zrpwIO.setFormat(zrpwrec);
		zrpwIO.setFunction(varcom.begn);
		zrpwIO.setStatuz(varcom.oK);
		while ((!(isEQ(zrpwIO.getStatuz(), varcom.endp)))) {
			readZrpw200();
		}
	}


	protected void readZrpw200() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					check210();
				case nextr280:
					nextr280();
				case exit290:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}

	protected void check210() {
		wsaaZrpwKey.set(zrpwIO.getRecKeyData());
		SmartFileCode.execute(appVars, zrpwIO);
		/* Abort if a database error has been found */
		if (!isEQ(zrpwIO.getStatuz(), varcom.oK)
				&& !isEQ(zrpwIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(wsaaZrpwKey);
			drylogrec.statuz.set(zrpwIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			fatalError();
		}
		/*
		 * Where there are no matches, then exit the section setting the flag to
		 * skip creating/updating the DTRD
		 */
		
		if (!isEQ(zrpwIO.chdrcoy, drypDryprcRecInner.drypCompany)
				|| !isEQ(zrpwIO.chdrnum, drypDryprcRecInner.drypEntity)
				|| isEQ(zrpwIO.getStatuz(), varcom.endp)) {
			
			zrpwIO.setStatuz(varcom.endp);
			drypDryprcRecInner.dtrdNo.setTrue();

			goTo(GotoLabel.exit290);
		}
		zrpwFound.setTrue();
		goTo(GotoLabel.nextr280);
	}

	protected void nextr280() {
		zrpwIO.setFunction(varcom.nextr);
	}

	protected void fatalError() {
		/* A010-FATAL */
		drylogrec.subrname.set(wsaaProg);
		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
		if (drypDryprcRecInner.onlineMode.isTrue()) {
			drylogrec.runNumber.set(ZERO);
		} else {
			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
		}
		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
		drypDryprcRecInner.fatalError.setTrue();
		callProgram(Drylog.class, drylogrec.drylogRec);
		a000FatalError();
	}
	private static final class DrypDryprcRecInner { 
	
		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
		private FixedLengthStringData drypProcParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 371);
		private FixedLengthStringData drypDetailParams = new FixedLengthStringData(71).isAPartOf(drypProcParams, 0, REDEFINE);
		private FixedLengthStringData drypDetailInput = new FixedLengthStringData(71).isAPartOf(drypDetailParams, 0);
		private FixedLengthStringData drypCnttype = new FixedLengthStringData(3).isAPartOf(drypDetailInput, 0);
		private FixedLengthStringData drypStatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 7);
		private FixedLengthStringData drypPstatcode = new FixedLengthStringData(2).isAPartOf(drypDetailInput, 9);
		private PackedDecimalData drypStmdte = new PackedDecimalData(8, 0).isAPartOf(drypDetailInput, 51);
	}
	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
}

