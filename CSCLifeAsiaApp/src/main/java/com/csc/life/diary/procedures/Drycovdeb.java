package com.csc.life.diary.procedures;

import static com.quipoz.COBOLFramework.COBOLFunctions.FLSArrayPartOfStructure;
import static com.quipoz.COBOLFramework.COBOLFunctions.SPACES;
import static com.quipoz.COBOLFramework.COBOLFunctions.ZERO;
import static com.quipoz.COBOLFramework.COBOLFunctions.goTo;
import static com.quipoz.COBOLFramework.COBOLFunctions.isEQ;
import static com.quipoz.COBOLFramework.COBOLFunctions.isGT;
import static com.quipoz.COBOLFramework.COBOLFunctions.isLT;

import com.csc.diary.procedures.Drylog;
import com.csc.diaryframework.parent.Maind;
import com.csc.life.unitlinkedprocessing.dataaccess.CovruddTableDAM;
import com.csc.smart.recordstructures.Varcom;
import com.csc.smart400framework.dataaccess.SmartFileCode;
import com.csc.smart400framework.utility.Validator;
import com.quipoz.COBOLFramework.GOTOException;
import com.quipoz.COBOLFramework.GOTOInterface;
import com.quipoz.framework.datatype.FixedLengthStringData;
import com.quipoz.framework.datatype.PackedDecimalData;
import com.quipoz.framework.datatype.ZonedDecimalData;

	 /*
      *(c) Copyright Continuum Corporation Ltd.  1986....1995.
      *    All rights reserved.  Continuum Confidential.
      *
      *REMARKS.
      *
      * This is the Diary Update subroutine for Coverage Debt
      * processing.
      *
      ***********************************************************************
      *                                                                     *
      * ......... New Version of the Amendment History.                     *
      *                                                                     *
      ***********************************************************************
      *           AMENDMENT  HISTORY                                        *
      ***********************************************************************
      * DATE.... VSN/MOD  WORK UNIT    BY....                               *
      *                                                                     *
      * 04/03/02  01/01   DRYAPL       Jacco Landskroon                     *
      *           Initial Version                                           *
      *                                                                     *
      * 15/02/06  01/01   DRYAP2       Wang Ge - CSC Singapore              *
      *           Retrofit.                                                 *
      *                                                                     *
      **DD/MM/YY*************************************************************
      */
public class Drycovdeb extends Maind {
	  
	private FixedLengthStringData wsaaProg = new FixedLengthStringData(10).init("DRYCOVDEB");
	private FixedLengthStringData wsaaVersion = new FixedLengthStringData(2).init("01");
	
	private PackedDecimalData wsaaMinProcesDate = new PackedDecimalData(8, 0);
	
	private FixedLengthStringData wsaaProcess = new FixedLengthStringData(1);
	private Validator procesRequired = new Validator(wsaaProcess,"Y");
	private Validator procesNotRequired = new Validator(wsaaProcess,"N");
	
	/* FORMATS */
	private static final String covruddrec = "COVRUDDREC";
	
	private Varcom varcom = new Varcom();
	private CovruddTableDAM  covruddIO = new CovruddTableDAM();
	
	private DrypDryprcRecInner drypDryprcRecInner = new DrypDryprcRecInner();
	
	/**
	* Contains all possible labels used by goTo action.
	*/
	private enum GotoLabel implements GOTOInterface {
		DEFAULT, 
		nextr180, 
		exit190
	}

	public Drycovdeb() {
		super();
	}
	
	/**
	* The mainline method is the default entry point to the class
	*/
	public void mainline(Object... parmArray){
		drypDryprcRecInner.drypDryprcRec = convertAndSetParam(drypDryprcRecInner.drypDryprcRec, parmArray, 0);
		try {
			//ILPI-61
			dryprcMaind.dryprcRec.set(drypDryprcRecInner.drypDryprcRec);
			mainline000();
		}
		catch (COBOLExitProgramException e) {
		// Expected exception for control flow purposes
		}
	}
	
	protected void mainline000(){
		main010();
		exit090();
	}
	
	protected void main010() {

		drypDryprcRecInner.drypStatuz.set(varcom.oK);
		drypDryprcRecInner.dtrdYes.setTrue();
		procesNotRequired.setTrue();
		wsaaMinProcesDate.set(varcom.vrcmMaxDate);

		/* Determine if the transaction is needed. */
		covruddIO.setParams(SPACES);

		covruddIO.chdrcoy.set(drypDryprcRecInner.drypCompany);
		covruddIO.chdrnum.set(drypDryprcRecInner.drypEntity);
		covruddIO.life.set(SPACES);
		covruddIO.coverage.set(SPACES);
		covruddIO.rider.set(SPACES);
		covruddIO.planSuffix.set(ZERO);
		covruddIO.setFunction(varcom.begn);
		covruddIO.setFormat(covruddrec);
		covruddIO.setStatuz(varcom.oK);
		while (!(isEQ(covruddIO.getStatuz(), varcom.endp))) {
			readCovrudd100();
		}
		if (procesRequired.isTrue()) {
			drypDryprcRecInner.drypNxtprcdate.set(wsaaMinProcesDate);
			drypDryprcRecInner.drypNxtprctime.set(ZERO);

		} else {
			drypDryprcRecInner.dtrdNo.setTrue();
		}
	}
	
	protected void exit090(){
		exitProgram();
	}
	
	protected void readCovrudd100() {
		GotoLabel nextMethod = GotoLabel.DEFAULT;
		while (true) {
			try {
				switch (nextMethod) {
				case DEFAULT:
					read110();
				case nextr180:
					nextr180();
				case exit190:
				}
				break;
			} catch (GOTOException e) {
				nextMethod = (GotoLabel) e.getNextMethod();
			}
		}
	}
	
	protected void read110() {
		SmartFileCode.execute(appVars, covruddIO);
		if (!isEQ(covruddIO.getStatuz(), varcom.oK)
				&& !isEQ(covruddIO.getStatuz(), varcom.endp)) {
			drylogrec.params.set(covruddIO.getParams());
			drylogrec.statuz.set(covruddIO.getStatuz());
			drylogrec.dryDatabaseError.setTrue();
			a000FatalError();//ILPI-61

		}
		if (isEQ(covruddIO.getStatuz(), varcom.endp)
				|| !isEQ(covruddIO.chdrcoy, drypDryprcRecInner.drypCompany)
				|| !isEQ(covruddIO.chdrnum, drypDryprcRecInner.drypEntity)) {
			covruddIO.setStatuz(varcom.endp);
			goTo(GotoLabel.exit190);
		}
		if (isGT(drypDryprcRecInner.drypRunDate, covruddIO.currto)) {
			goTo(GotoLabel.nextr180);
		}
		if (isGT(covruddIO.crrcd, drypDryprcRecInner.drypRunDate)
				&& isLT(covruddIO.crrcd, wsaaMinProcesDate)) {
			wsaaMinProcesDate.set(covruddIO.crrcd);
			procesRequired.setTrue();
			goTo(GotoLabel.nextr180);
		}
		if (isLT(covruddIO.crrcd, wsaaMinProcesDate)) {
			wsaaMinProcesDate.set(drypDryprcRecInner.drypRunDate);
			procesRequired.setTrue();
			goTo(GotoLabel.nextr180);
		}
	}
	
	protected void nextr180(){
		 covruddIO.setFunction(varcom.nextr);
	}
	
//	protected void fatalError() {
//		/* A010-FATAL */
//		drylogrec.subrname.set(wsaaProg);
//		drylogrec.effectiveDate.set(drypDryprcRecInner.drypRunDate);
//		drylogrec.entityKey.set(drypDryprcRecInner.drypEntityKey);
//		
//		if (drypDryprcRecInner.onlineMode.isTrue()) {
//			drylogrec.runNumber.set(ZERO);
//		} else {
//			drylogrec.runNumber.set(drypDryprcRecInner.drypRunNumber);
//		}
//		
//		drypDryprcRecInner.drypStatuz.set(drylogrec.statuz);
//		drypDryprcRecInner.fatalError.setTrue();
//		
//		callProgram(Drylog.class, drylogrec.drylogRec);
//		a000FatalError();
//	}
	
	private static final class DrypDryprcRecInner { 
		//ILPI-97 starts
		private FixedLengthStringData drypDryprcRec = new FixedLengthStringData(621);
		private FixedLengthStringData drypStatuz = new FixedLengthStringData(4).isAPartOf(drypDryprcRec, 0);
		private FixedLengthStringData drypError = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 4);
		private Validator noError = new Validator(drypError, "0");
		private Validator nonFatalError = new Validator(drypError, "1");
		private Validator fatalError = new Validator(drypError, "2");
		private FixedLengthStringData drypMode = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 5);
		private Validator batchMode = new Validator(drypMode, "B");
		private Validator onlineMode = new Validator(drypMode, "O");
		private PackedDecimalData drypRunDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 6);
		private ZonedDecimalData drypRunNumber = new ZonedDecimalData(8, 0).isAPartOf(drypDryprcRec, 11).setUnsigned();
		private FixedLengthStringData drypEntityKey = new FixedLengthStringData(13).isAPartOf(drypDryprcRec, 19);
		private FixedLengthStringData drypCompany = new FixedLengthStringData(1).isAPartOf(drypEntityKey, 0);
		private FixedLengthStringData drypBranch = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 1);
		private FixedLengthStringData drypEntityType = new FixedLengthStringData(2).isAPartOf(drypEntityKey, 3);
		private FixedLengthStringData drypEntity = new FixedLengthStringData(8).isAPartOf(drypEntityKey, 5);
		private FixedLengthStringData drypLanguage = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 32);
		private FixedLengthStringData drypProcCode = new FixedLengthStringData(6).isAPartOf(drypDryprcRec, 33);
		private PackedDecimalData drypEffectiveDate = new PackedDecimalData(8, 0).isAPartOf(drypDryprcRec, 39);
		private PackedDecimalData drypEffectiveTime = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 44);
		private FixedLengthStringData drypBatchKey = new FixedLengthStringData(19).isAPartOf(drypDryprcRec, 48);
		private FixedLengthStringData drypBatcpfx = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 0);
		private FixedLengthStringData drypBatccoy = new FixedLengthStringData(1).isAPartOf(drypBatchKey, 2);
		private FixedLengthStringData drypBatcbrn = new FixedLengthStringData(2).isAPartOf(drypBatchKey, 3);
		private PackedDecimalData drypBatcactyr = new PackedDecimalData(4, 0).isAPartOf(drypBatchKey, 5);
		private PackedDecimalData drypBatcactmn = new PackedDecimalData(2, 0).isAPartOf(drypBatchKey, 8);
		private FixedLengthStringData drypBatctrcde = new FixedLengthStringData(4).isAPartOf(drypBatchKey, 10);
		private FixedLengthStringData drypBatcbatch = new FixedLengthStringData(5).isAPartOf(drypBatchKey, 14);
		private FixedLengthStringData drypFsuCompany = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 67);
		//cluster support by vhukumagrawa
		private PackedDecimalData drypThreadNumber = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 68);
		private PackedDecimalData drypProcSeqNo = new PackedDecimalData(3, 0).isAPartOf(drypDryprcRec, 70);
		private PackedDecimalData drypUser = new PackedDecimalData(6, 0).isAPartOf(drypDryprcRec, 72);
		private FixedLengthStringData drypSystParams = new FixedLengthStringData(250).isAPartOf(drypDryprcRec, 76);
		private FixedLengthStringData[] drypSystParm = FLSArrayPartOfStructure(25, 10, drypSystParams, 0);
		private PackedDecimalData drypTranno = new PackedDecimalData(5, 0).isAPartOf(drypDryprcRec, 326);
		private FixedLengthStringData drypAdditionalKey = new FixedLengthStringData(30).isAPartOf(drypDryprcRec, 329);
		private FixedLengthStringData drypRollForward = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 359);
		private Validator rollForwardMode = new Validator(drypRollForward, "R");
		private Validator normalMode = new Validator(drypRollForward, " ");
		private FixedLengthStringData drypDetailOutput = new FixedLengthStringData(10).isAPartOf(drypDryprcRec, 360);
		private FixedLengthStringData drypRequired = new FixedLengthStringData(1).isAPartOf(drypDetailOutput, 0);
		private Validator dtrdYes = new Validator(drypRequired, "Y");
		private Validator dtrdNo = new Validator(drypRequired, "N");
		private PackedDecimalData drypNxtprcdate = new PackedDecimalData(8, 0).isAPartOf(drypDetailOutput, 1);
		private PackedDecimalData drypNxtprctime = new PackedDecimalData(6, 0).isAPartOf(drypDetailOutput, 6);
		private FixedLengthStringData drypProcessResult = new FixedLengthStringData(1).isAPartOf(drypDryprcRec, 370);
		private Validator processSuccesful = new Validator(drypProcessResult, "Y");
		private Validator processUnsuccesful = new Validator(drypProcessResult, "N");
		//ILPI-97 ends
	}

	@Override
	protected FixedLengthStringData getWsaaProg() {
		return wsaaProg;
	}

	@Override
	protected FixedLengthStringData getWsaaVersion() {
		return wsaaVersion;
	}
}

